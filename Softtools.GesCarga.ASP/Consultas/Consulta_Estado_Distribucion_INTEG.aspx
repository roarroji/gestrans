﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Consulta_Estado_Distribucion_INTEG.aspx.vb" Inherits="Softtools.GesCarga.ASP.Consulta_Estado_Distribucion_INTEG" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Consulta Fresenius</title>
    <style>
        html, body {
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
            font-weight: 400;
            font-size:12px;
        }

        .header {
            position: relative;
            display: block;
            width: 100%;
        }

        .imgheader {
            padding: 5px;
            width: 180px;
        }

        h3 {
            margin: 5px 0;
        }

        .content {
            position: relative;
            display: block;
            width: calc(100% - 30px);
            padding: 15px;
        }

        .footer {
            position: relative;
            display: block;
            width: calc(100% - 30px);
            padding: 15px;
        }
    </style>
</head>
<body>
    <div class="header">
        <img class="imgheader" src="../Content/img/Logo_INTEG.jpg" />
    </div>
    <div class="content">
        <form id="form1" runat="server">
            <h3>SEGUIMIENTO DESPACHO</h3>
            <div id="infoSeguimiento" runat="server" style="display:block;">
                <table>
                <tr>
                    <td><strong>Documento Cliente:</strong></td>
                    <td><asp:Label ID="DocumentoCliente" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Fecha Cargue:</strong></td>
                    <td><asp:Label ID="FechaCargue" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>No Remesa:</strong></td>
                    <td><asp:Label ID="NumRemesa" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Placa:</strong></td>
                    <td><asp:Label ID="Placa" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Ciudad:</strong></td>
                    <td><asp:Label ID="Ciudad" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Destinatario:</strong></td>
                    <td><asp:Label ID="Destinatario" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Cantidad:</strong></td>
                    <td><asp:Label ID="Cantidad" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Peso:</strong></td>
                    <td><asp:Label ID="Peso" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Estado:</strong></td>
                    <td><asp:Label ID="Estado" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Observaciones:</strong></td>
                    <td><asp:Label ID="Observaciones" runat="server"></asp:Label></td>
                </tr>
            </table>
            </div>
            
        </form>
        <asp:Label ID="lblMensajeError" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
    </div>
    <div class="footer">
        <strong>Copyright &copy; <asp:Label ID="Anio" runat="server"></asp:Label> <a href="#">Softtools</a>.</strong> Todos los derechos reservados.
    </div>
</body>
</html>
