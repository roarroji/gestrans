﻿Imports System.Data.SqlClient
Imports System.Globalization

Public Class Consulta_Estado_Distribucion_INTEG
    Inherits System.Web.UI.Page

#Region "Variables"
    Private strSQL As String
    Public strUser As String = ""
    Public strPass As String = ""
    Public intTercCodigo As Integer = 0
    Public strDocuCliente As String = ""
    Dim objGeneral As clsGeneral
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            strUser = Me.Request.QueryString("Usuario")
            strPass = Me.Request.QueryString("Clave")
            intTercCodigo = CInt(Me.Request.QueryString("Cliente"))
            strDocuCliente = Me.Request.QueryString("Documento_Cliente")
            Dim MyCultureInfo = New CultureInfo("en-US") ' es-ES
            lblMensajeError.Visible = True
            infoSeguimiento.Style.Add("display", "none")
            objGeneral = New clsGeneral
            Anio.Text = Date.Now.Year.ToString

            If strUser = String.Empty Or
               strPass = String.Empty Or
               intTercCodigo = 0 Or
               strDocuCliente = String.Empty Then
                lblMensajeError.Text = "Acceso no autorizado"
            Else
                GenerarConsultaDistribucion()
            End If

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = ex.Message.ToString
        End Try
    End Sub

    Private Sub GenerarConsultaDistribucion()
        Dim ComandoSQL As SqlCommand
        Dim habilitado As Integer = 0
        Dim ExisteRegistro As Boolean = False
        'Consulta generada para crear el archivo plano de Terceros
        strSQL = "SELECT Habilitado FROM Usuarios"
        strSQL += " WHERE EMPR_Codigo = 2 "
        strSQL += " AND Codigo_Usuario ='" & strUser & "'"
        strSQL += " AND CATA_APLI_Codigo = 202"
        strSQL += " AND Habilitado = 1 "
        strSQL += " AND Clave = '" & strPass & "'"

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrUser As SqlDataReader = ComandoSQL.ExecuteReader

        While sdrUser.Read
            habilitado = CInt(sdrUser("Habilitado"))
        End While

        sdrUser.Close()
        Me.objGeneral.ConexionSQL.Close()

        If habilitado <> 1 Then
            lblMensajeError.Text = "El Usuario no tiene permisos"
        Else
            strSQL = "EXEC gsp_consultar_estado_distribucion " & "2, " & "'" & strDocuCliente & "', " & intTercCodigo
            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrInfo As SqlDataReader = ComandoSQL.ExecuteReader
            While sdrInfo.Read
                ExisteRegistro = True
                DocumentoCliente.Text = sdrInfo("Documento_Cliente")
                FechaCargue.Text = sdrInfo("Fecha_Cargue")
                NumRemesa.Text = String.Format(CultureInfo.InvariantCulture, "{0:#,#}", sdrInfo("Numero_Remesa"))
                Placa.Text = sdrInfo("Placa")
                Ciudad.Text = sdrInfo("Ciudad_Destinatario")
                Destinatario.Text = sdrInfo("Nombre_Destinatario")
                Cantidad.Text = String.Format(CultureInfo.InvariantCulture, "{0:#,#}", sdrInfo("Cantidad"))
                Peso.Text = String.Format(CultureInfo.InvariantCulture, "{0:#,#.##}", sdrInfo("Peso"))
                Estado.Text = sdrInfo("Estado")
                Observaciones.Text = sdrInfo("Observaciones")
            End While

            sdrInfo.Close()
            Me.objGeneral.ConexionSQL.Close()

            If ExisteRegistro Then
                infoSeguimiento.Style.Add("display", "block")
            Else
                lblMensajeError.Text = "No se encontro la información solicitada"
            End If

        End If
    End Sub
End Class