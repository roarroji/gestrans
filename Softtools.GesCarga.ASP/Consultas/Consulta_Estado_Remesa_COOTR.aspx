﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Consulta_Estado_Remesa_COOTR.aspx.vb" Inherits="Softtools.GesCarga.ASP.Consulta_Estado_Remesa_COOTR1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Consulta Fresenius</title>
    <style>
        html, body {
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
            font-weight: 400;
            font-size:12px;
        }

        .header {
            position: relative;
            display: block;
            width: 100%;
        }

        .imgheader {
            padding: 5px;
            width: 180px;
        }

        h3 {
            margin: 5px 0;
        }

        .content {
            position: relative;
            display: block;
            width: calc(100% - 30px);
            padding: 15px;
        }

        .footer {
            position: relative;
            display: block;
            width: calc(100% - 30px);
            padding: 15px;

        }
        .auto-style1 {
            width: 220px;
        }
        .auto-style2 {
            width: 190px;
            text-align: center;
        }
        .auto-style3 {
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="header">
        <img class="imgheader" src="../Content/img/Logo_COOTR.jpg" />
    </div>
    <div class="content">
        <form id="form1" runat="server">
            <h3>CONSULTAR ESTADO REMESA</h3>
            <div id="infoSeguimiento" runat="server" style="display:block;">
                <table style="width: 327px">
               <tr>
                    <td><strong>No Remesa:</strong></td>
                    <td><asp:Label ID="Numero_Documento" runat="server"></asp:Label></td>
                </tr>
                        <tr>
                    <td><strong>Cliente:</strong></td>
                    <td><asp:Label ID="NombreCliente" runat="server"></asp:Label></td>
                </tr>
                  </table> 
         
            <br />

            <table style="width: 441px; height: 66px;" border="1">

                <thead >
                    <tr>
            <td class="auto-style2"><strong>Estado</strong></td>
            <td class="auto-style3"><strong style="text-align: center">Fecha Registro </strong></td>
            <td class="auto-style3"><strong>Oficina</strong></td> 
            </tr>
                </thead> 
                <tbody id="divID" runat="server" > 
                </tbody> 
            </table>    
                
</div>                
                <div id="infoEntrega" runat="server" style="display:block;"> 
                    <h3>INFORMACIÓN ENTREGA</h3>
                  <table> 
                <tr>
                    <td><strong>Identificacion:</strong></td>
                    <td class="auto-style1"><asp:Label ID="Numero_Identificacion_Recibe" runat="server" style="text-align: center"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Nombre:</strong></td>
                    <td class="auto-style1"><asp:Label ID="Nombre_Recibe" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Telefono:</strong></td>
                    <td class="auto-style1"><asp:Label ID="Telefonos_Recibe" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Cantidad:</strong></td>
                    <td class="auto-style1"><asp:Label ID="Cantidad_Recibe" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Peso:</strong></td>
                    <td class="auto-style1"><asp:Label ID="Peso" runat="server"></asp:Label></td>
                </tr>
                     <tr>
                    <td><strong>Observaciones:</strong></td>
                    <td class="auto-style1"><asp:Label ID="Observaciones_Recibe" runat="server"></asp:Label></td>
                </tr>
                        <tr>
                    <td><strong>Usuario Entrega:</strong></td>
                    <td class="auto-style1"><asp:Label ID="Usuario_Entrega" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><strong>Novedad:</strong></td>
                    <td class="auto-style1"><asp:Label ID="NovedadRecibe" runat="server"></asp:Label></td>
                </tr>  
                      
            
                       <br/>
                    <tr>
                    <td><strong>Firma:</strong></td>
                    <td class="auto-style1"> <asp:Image ID="imagenFirma" runat="server" Height="170px" Width="330px" /> </td> 
                </tr> 
                       <br/>
                        
                    <tr>
                    <td><strong>Foto:</strong></td>
                    <td class="auto-style1"> <asp:Image ID="imagenFoto" runat="server" Height="196px" Width="331px" /> </td> 
                </tr>
                
                    
            </table>
            </div>
            
        </form>
        <asp:Label ID="lblMensajeError" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
    </div>
    <div class="footer">
        <strong>Copyright &copy; <asp:Label ID="Anio" runat="server"></asp:Label> <a href="#">Softtools</a>.</strong> Todos los derechos reservados.
    </div>
</body>
</html>
