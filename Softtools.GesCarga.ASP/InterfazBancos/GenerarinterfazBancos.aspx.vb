﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Globalization

Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Repositorio.Basico.Tesoreria

Imports Softtools.GesCarga.ASP.clsGeneral


Public Class GenerarinterfazBancos

    Inherits System.Web.UI.Page

#Region "Constantes Generales"

    ' Constantes de Tipos de Campo
    Public Const PERFIL_CLIENTE As Byte = 1
    Public Const CODIGO_NATURALEZA_NATURAL As Integer = 2
    Public Const CODIGO_AUTORRETENEDOR As Integer = 3
    Public Const CODIGO_ESTADO_ACTIVO As String = "1"
    Public Const CAMPO_NUMERICO As Integer = 1
    Public Const CAMPO_CARACTER As Integer = 2
    Public Const CAMPO_DECIMAL As Integer = 3
    Public Const CAMPO_FECHA As Integer = 4
    Public Const CAMPO_CARACTER_CEROS As Integer = 5
    Public Const CAMPO_IDENTIFICACION As Integer = 6
    Public Const CAMPO_CEROS_IZQUIERDA As Integer = 7
    Private Const CAMPO_DECIMAL_CON_PUNTO_UNOE As Integer = 8
    Const CAMPO_ESPACIOS_IZQUIERDA As Integer = 9
    Const CAMPO_DECIMAL_CON_PUNTO As Integer = 10
    Public Const CAMPO_CEROS_DERECHA As Integer = 11
    Const SOLO_CAMPO_DECIMAL_CON_PUNTO As Integer = 12
    Private Const CAMPO_FECHA_UNOE As Short = 15
    Private Const CAMPO_DECIMAL_UNOE_85 As Short = 16
    Private Const CAMPO_NUMERICO_UNOE_85 As Short = 17
    Const CAMPO_ESPACIOS_DERECHA As Integer = 18
    Public Const CAMPO_DINERO As Integer = 19

    ' TIPOS DOCUMENTOS
    Public Const TIDO_TERCERO As Short = 100
    Public Const TIDO_COMPROBANTE_CONTABLE As Byte = 45
    Public Const TIDO_MANIFIESTO As Byte = 140
    Public Const TIDO_LIQUIDA_MANIFIESTO_TERCEROS As Integer = 160
    Public Const TIDO_LEGALIZACION_GASTOS As Integer = 230
    Public Const TIDO_FACTURAS As Byte = 170
    Public Const TIDO_NOTA_CREDITO As Byte = 190
    Public Const TIDO_NOTA_DEBITO As Byte = 195
    Public Const TIDO_COMPROBANTE_EGRESO As Byte = 30
    Public Const TIDO_COMPROBANTE_INGRESO As Byte = 40

    ' CATALOGO DOCUMENTO ORIGEN
    Public Const DOOR_ANTICIPO_MANIFIESTO As String = "2604"
    Public Const DOOR_LIQUIDACION_CONDUCTOR As String = "2602"

    ' SISTEMAS CONTABLES
    Const SISTEMA_CONTABLE_SIIGO As Double = 14001
    Const SISTEMA_CONTABLE_WORLD_OFFICE As Double = 14002
    Const SISTEMA_CONTABLE_UNO_EE As Double = 14003
    Const SISTEMA_CONTABLE_SIIGO_WINDOWS As Double = 14004

    'PREFIJOS EMPRESAS:
    Const CARGA = "CARGA"

    ' INTERFACES SISTEMAS CONTABLES
    Const INTERFAZ_TERCEROS As Double = 14501
    Const INTERFAZ_COMPROBANTES_EGRESO As Double = 14503
    Const INTERFAZ_COMPROBANTE_INGRESO As Double = 14504
    Const INTERFAZ_LIQUIDACIONES As Double = 14505
    Const INTERFAZ_FACTURAS As Double = 14506
    Const INTERFAZ_LEGALIZACION_GASTOS As Double = 14507
    Const INTERFAZ_FACTURAS_INVENTARIO As Double = 14508
    Const INTERFAZ_ANTICIPOS As Double = 14509
    Const INTERFAZ_NOTA_CREDITO As Double = 14510
    Const MOVIMIENTO_CONTABLE_TRANSPORTE_TERRESTRE As Double = 41450501

    ' ESTADOS INTERFAZ
    Const ESTADO_INTERFAZ_PENDIENTE As Double = 12001
    Const ESTADO_INTERFAZ_REPORTADO As Double = 12002
    Const ESTADO_INTERFAZ_NO_REPORTAR As Double = 12003

#End Region
    Const NOMBRE_ARCHIVO_BANCOLOMBIA As String = "ArchivoPlanoBancolombia.txt"
#Region "Constantes SIIGO"

    ' Nombres Archivos SIIGO
    Const NOMBRE_ARCHIVO_TERCEROS_SIIGO As String = "Terceros.txt"
    Const NOMBRE_ARCHIVO_LIQUIDACIONES_SIIGO As String = "Liquidaciones.txt"
    Const NOMBRE_ARCHIVO_FACTURAS_SIIGO As String = "Facturas.txt"
    Const NOMBRE_ARCHIVO_COMPROBANTESEGRESO As String = "ComprobanteEgreso.txt"
    Const NOMBRE_ARCHIVO_COMPROBANTINSEGRESO As String = "ComprobanteIngreso.txt"
    'Const NOMBRE_ARCHIVO_LEGALIZACION_GASTOS As String = "LegalizacionGastos.csv"

    Public Const NATURALEZA_CREDITO As String = "C"
    Public Const NATURALEZA_DEBITO As String = "D"

    ' TIPOS DE COMPROBANTES SIIGO GENERALES
    Const TIPO_COMPROBANTE_CAUSACION As String = "P"
    Const TIPO_COMPROBANTE_EGRESOS As String = "G"
    Const TIPO_COMPROBANTE_INGRESOS As String = "R"
    Const TIPO_COMPROBANTE_MANIFIESTOS As String = "L"
    Const TIPO_COMPROBANTE_LIQUIDACIONES As String = "P"
    Const TIPO_COMPROBANTE_FACTURAS As String = "F"
    Const TIPO_COMPROBANTE_NOTAS_CREDITO As String = "C"
    Const TIPO_COMPROBANTE_NOTAS_DEBITO As String = "D"
    Const TIPO_COMPROBANTE_NOTAS As String = "N"

    ' CODIGOS COMPROBANTES SIIGO GENERALES
    Const CODIGO_COMPROBANTE_EGRESO = 2
    Const CODIGO_COMPROBANTE_INGRESO = 2
    Const CODIGO_COMPROBANTE_LIQUIDACIONES As Integer = 2
    Const CODIGO_COMPROBANTE_FACTURAS As Integer = 1

    ' Constantes tipo de campo
    Const CAMPO_NUMERICO_SIIGO As Integer = 1
    Const CAMPO_ALFANUMERICO_SIIGO As Integer = 2
    Const CAMPO_DECIMAL_SIIGO As Integer = 3
    Const CAMPO_FECHA_SIIGO As Integer = 4
    Const CAMPO_CEROS_DERECHA_SIIGO As Integer = 5

    ' Constantes Terceros SIIGO
    Public Const SEPARADOR_SIIGO_WINDOWS As String = ""
    Public Const TIPO_IDENT_NIT_SIIGO_WINDOWS As String = "N"
    Public Const TIPO_IDENT_CEDULA_SIIGO_WINDOWS As String = "C"
    Public Const TIPO_IDENT_CEDULA_EXTRANJERIA_SIIGO_WINDOWS As String = "E"
    Public Const TIPO_NIT_CLIENTE_SIIGO_WINDOWS As String = "C"
    Public Const TIPO_NIT_PROVEEDOR_SIIGO_WINDOWS As String = "P"
    Public Const TIPO_NIT_OTROS_SIIGO_WINDOWS As String = "O"
    Public Const SEXO_EMPRESA_SIIGO_WINDOWS As String = "E"

    Public Const TIPO_PERSONA_NATURAL_SIIGO_WINDOWS As Integer = 1
    Public Const TIPO_PERSONA_JURIDICA_SIIGO_WINDOWS As Integer = 2
    Public Const SI_SIIGO_WINDOWS As String = "S"
    Public Const NO_SIIGO_WINDOWS As String = "N"
    Public Const ESTADO_ACTIVO_SIIGO_WINDOWS As String = "A"
    Public Const ESTADO_INACTIVO_SIIGO_WINDOWS As String = "I"
    Public Const FECHA_NULA_SIIGO_WINDOWS As String = "19000101"
    Public Const VACIO_NUMERICO_SIIGO_WINDOWS As Integer = 0

    Public Const VALOR_ZERO_SIIGO_WINDOWS As String = "0"
    Public Const VALOR_UNO_SIIGO_WINDOWS As String = "1"

    Public Const CODIGO_CIUDAD_SIIGO_WINDOWS As Integer = 1
    Public Const CODIGO_PAIS_SIIGO_WINDOWS As Integer = 1
    Public Const CLASIFICACION_TRIBUTARIA_REGIMEN_SIMPLIFICADO As Integer = 4
    Public Const CLASIFICACION_TRIBUTARIA_REGIMEN_COMUN As Integer = 3

    ' Todas
    Const DIRECTORIO_ARCHIVOS_PLANOS As String = "Archivos_Interfaz_Bancos\"
    Const NOMBRE_ARCHIVO_COMPRIMIDO As String = "ArchivosInterfaseContable"
    Const EXTENSION_ARCHIVO_COMPRIMIDO As String = ".zip"

    ' AUXILIARES ARCHIVOS
    Const CODIGO_VENDEDOR As String = "1"
    Const CARACTER_ENTER As String = Chr(13)
    Const ARCHIVO_DEFINITIVO As Integer = 1
    Const ARCHIVO_BORRADOR As Integer = 0
    Const ARCHIVO_BORRADOR_WORLD_OFFICE As Integer = 2
    Const CODIGO_CATALOGO_PUC_COBRO_CLIENTE As String = "11"
    Const CODIGO_CATALOGO_PUC_RETEFUENTE_CLIENTE As String = "12"
    Const CODIGO_CATALOGO_PUC_FLETE_LIQUIDACION_MANIFIESTO As String = "26"
    Const CODIGO_CATALOGO_PUC_NETO_LIQUIDACION_MANIFIESTO As String = "27"
#End Region

#Region "Constantes UNO EE"
    ' Nombres Archivos UNO EE
    Const NOMBRE_ARCHIVO_COMPROBANTES_EGRESO_UNO_EE As String = "ComprobanteEgresoUnoEE.txt"
    Const NOMBRE_ARCHIVO_LIQUIDACIONES_UNO_EE As String = "LiquidacionesUnoEE.txt"
    Const NOMBRE_ARCHIVO_FACTURAS_UNO_EE As String = "FacturasUnoEE.txt"
    Const NOMBRE_ARCHIVO_ANTICIPOS_UNO_EE As String = "AnticiposUnoEE.txt"
    Const NOMBRE_ARCHIVO_CUENTA_PAGAR_UNO_EE As String = "CuentaPorPagarUnoEE.txt"
    Const NOMBRE_ARCHIVO_NOTA_CREDITO_DOCUMENTO As String = "NotaCreditoDocumentoUnoEE.txt"
    Const NOMBRE_ARCHIVO_NOTA_CREDITO_MOVIMIENTO As String = "NotaCreditoUnoEE.txt"

    'Constantes Generales UNO EE
    Const TIPO_DOCUMENTO_DETALLE_UNO_EE As String = "350"
    Const TIPO_REGISTRO_UNO_EE As String = "351"
    Const SUBTIPO_REGISTRO_UNO_EE As String = "00"
    Const SUBTIPO_REGISTRO_CXC_UNO_EE As String = "01"
    Const SUBTIPO_REGISTRO_CXP_UNO_EE As String = "02"
    Const UNIDAD_NEGOCIO_UNO_EE As String = "00"
    Const VERSION_TIPO_REGISTRO_UNO_EE As String = "01"
    Const VERSION_TIPO_REGISTRO_CXP_UNO_EE As String = "02"
    Const MAESTRO_EMPRESA_UNO_EE As String = "005"
    Const MAESTRO_FLUJO_EFECTIVO_UNO_EE As String = "1203"
    'Const MAESTRO_CENTRO_OPERACION_COMPROBANTES_EGRESO_UNO_EE As String = "123"
    'Const MAESTRO_TIPO_DOCUMENTO_COMPROBANTES_EGRESO_UNO_EE As String = "123"
    'Const MAESTRO_TIPO_DOCUMENTO_FACTURAS_UNO_EE As String = "123"
    Const MAESTRO_TIPO_DOCUMENTO_LIQUIDACION_UNO_EE As String = "LIQ"
    Const MAESTRO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO_UNO_EE As String = "PEL"
    Const MAESTRO_TIPO_DOCUMENTO_FACTURA_VENTA_UNO_EE As String = "FCV"
    Const MAESTRO_TIPO_DOCUMENTO_NOTA_CREDITO_UNO_EE As String = "NEC"
    Const MAESTRO_ENCABEZADO_FACTURA_VENTA_UNO_EE As String = "FACTURA DE VENTA SERVICIO DE TRANSPORTE"
    Const MAESTRO_ENCABEZADO_LIQUIDACION_UNO_EE As String = "LIQUIDACION SERVICIO DE TRANSPORTE"

    'Constantes Alternativa Logistica UNO EE
    Const CONSTANTE_CONTRA_CUENTA_CXP_ALTERNATIVA_LOGISTICA As String = "23050505"
    Const CONSTANTE_CONTRA_CUENTA_CXC_ALTERNATIVA_LOGISTICA As String = "13050501"

    Const CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA As String = "28150510"
    Const CUENTA_MOVIMIENTO_REMESA_INTERMEDIACION_ALTERNATIVA_LOGISTICA As String = "41450501"

#End Region

#Region "Constantes SIIGO WINDOWS"
    Public Const FORMATO_FECHA_SIIGO_WINDOWS As String = "yyyyMMdd"
    Public Const TIPO_MOVIMIENTO_DEBITO_SIIGO_WINDOWS As String = "D"
    Public Const TIPO_MOVIMIENTO_CREDITO_SIIGO_WINDOWS As String = "C"
#End Region

#Region " Constantes WORLD_OFFICE"

    ' Nombres Archivos WORLD_OFFICE

    Const NOMBRE_ARCHIVO_TERCEROS_WORLD_OFFICE As String = "Terceros.txt"
    Const NOMBRE_ARCHIVO_LIQUIDACIONES_WORLD_OFFICE As String = "Liquidaciones.txt"
    Const NOMBRE_ARCHIVO_FACTURAS_WORLD_OFFICE As String = "Facturas.csv"
    Const NOMBRE_ARCHIVO_FACTURAS_INVENTARIO_WORLD_OFFICE As String = "FacturasInventario.csv"

    Const NOMBRE_ARCHIVO_COMPROBANTESEGRESO_WORLD_OFFICE As String = "ComprobanteEgresoWorldOffice.csv"
    Const NOMBRE_ARCHIVO_LEGALIZACION_GASTOS As String = "LegalizacionGastosWorldOffice.csv"
    'Const NOMBRE_ARCHIVO_COMPROBANTINSEGRESO As String = "ComprobanteIngreso.txt"
    'Const NOMBRE_ARCHIVO_LEGALIZACION_GASTOS As String = "LelizacionGastos.txt"

    Public Const SEPARADOR_WORLD_OFFICE As String = ";"


    'Contabilidad WORLD OFFICE
    Public Const COMPROBANTE_EGRESO_WORLD_OFFICE As Short = 90
    'Public Const GASTOS_CONDUCTOR_WORLD_OFFICE As Short = 91
    'Public Const FACTURA_WORLD_OFFICE As Short = 92
    'Public Const COMPROBANTE_INGRESO_WORLD_OFFICE As Short = 93
    'Public Const FACTURA_INVENTARIO_WORLD_OFFICE As Short = 124
    'Public Const CUENTA_ANTICIPO_WORLD_OFFICE As String = "133010"
    'Public Const CUENTA_ACPM_CHIP_WORLD_OFFICE As String = "83959505"
    Public Const CUENTA_PROVEEDORES_WORLD_OFFICE As String = "220505"
    Public Const CUENTA_TRANSPORTE_CARGA_TELLEVAMOS As String = " " '"41450501"
    Public Const INDICADOR_FACTURA_VENTA_FV As String = "FV"
    Public Const NOMBRE_CUENTA_TRANSPORTE_CARGA_TELLEVAMOS As String = "SERVICIOS DE TRANSPORTE DE CARGA"

    Public Const INDICADOR_COMPROBANTE_EGRESOS_WOLD_OFFICE As String = "CE"
    Public Const PREFIJO_COMPROBANTE_EGRESOS_WOLD_OFFICE As String = "GESCE"
    Public Const PREFIJO_LEGALIZACION_WORLD_OFFICE As String = "GEF2"


#End Region

#Region "Variables"
    Private intDefinitivo As Integer = 0
    Private strPath As String = ""
    Public strError As String
    Private strSQL As String
    Dim objGeneral As clsGeneral
    Dim objTercero As TerceroInterfazContable
    Dim objCuentaPuc As PlanUnicoCuentas
    Dim objOperaCuentaPuc As RepositorioPlanUnicoCuentas
    Public intSitemaContable As Integer
    Public intDocumentoContable As Integer
    Public intNumero As Double
    Public dtaFechaInicial As Date
    Public dtaFechaFinal As Date
    Public intEstado As Integer
    Public intOficina As Integer
    Public strNombreArchivo As String
    Public strNombreArchivoII As String
    Public strPathArchivo As String
    Public intEmpresa As Integer
    Public intUsuario As Integer
    Public intDocumentoOrigen As Integer
    Public lngTercero As Long
    Public strPrefijoEmpresa

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'intDocumentoOrigen = CInt(Me.Request.QueryString("DocumentoOrigen"))
            'intDocumentoContable = CInt(Me.Request.QueryString("TipoDocumento"))
            Dim MyCultureInfo = New CultureInfo("en-US") ' es-ES
            Dim strFecha As String
            If Me.Request.QueryString("FechInic") <> "" And Me.Request.QueryString("FechFina") <> "" Then
                strFecha = Me.Request.QueryString("FechInic")
                dtaFechaInicial = DateTime.ParseExact(strFecha, "d", MyCultureInfo)
                strFecha = Me.Request.QueryString("FechFina")
                dtaFechaFinal = DateTime.ParseExact(strFecha, "d", MyCultureInfo)
            End If

            'lngTercero = CLng(Me.Request.QueryString("Tercero"))
            intOficina = Me.Request.QueryString("Oficina")
            intEmpresa = Me.Request.QueryString("Empresa")
            strPrefijoEmpresa = Me.Request.QueryString("Prefijo")
            objGeneral = New clsGeneral
            Me.objTercero = New TerceroInterfazContable
            lblMensajeError.Visible = True

            'lblMensajeError.Text = "Call Generar_Archivo_Interfaz()"
            Call Generar_Archivo_Interfaz()

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = ex.Message.ToString
        End Try
    End Sub

    Private Sub Generar_Archivo_Interfaz()

        Try
            Select Case strPrefijoEmpresa
                ' Sistema Contable a Generar Documento
                Case CARGA
                    Call Me.Generar_Archivo_Interfaz_Bancolombia_Carga()
            End Select

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Genera_Archivo_Interfaz:" & ex.Message.ToString
        End Try

        If Len(Trim(strNombreArchivo)) > clsGeneral.CERO Then
            Dim strFiltro As String = "<script language='JavaScript'>window.open('../InterfazBancos/DescargaArchivoInterfazBancos.aspx?Archivo=" & strNombreArchivo & "')</script>"
            If Len(Trim(strNombreArchivoII)) > clsGeneral.CERO Then
                strFiltro += "<script language='JavaScript'>window.open('../InterfazBancos/DescargaArchivoInterfazBancos.aspx?Archivo=" & strNombreArchivoII & "')</script>"
            End If
            Me.ltrFiltro.Text = strFiltro
        End If

    End Sub

    Private Sub Generar_Archivo_Interfaz_SIIGO()
        Try
            Select Case intDocumentoContable

                Case INTERFAZ_TERCEROS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                   ' Call Me.Generar_Documento_Tercero_SIIGO()

                Case INTERFAZ_COMPROBANTES_EGRESO
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Comprobante_Egreso_SIIGO()

                Case INTERFAZ_COMPROBANTE_INGRESO
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Comprobante_Ingreso_SIIGO()

                Case INTERFAZ_LIQUIDACIONES
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Liquidacion_SIIGO()

                Case INTERFAZ_FACTURAS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Factura_SIIGO()

                Case INTERFAZ_LEGALIZACION_GASTOS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Legalizacion_Gastos_SIIGO()

            End Select

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Genera_Archivo_Interfaz:" & ex.Message.ToString
        End Try


    End Sub

    Private Sub Generar_Archivo_Interfaz_Bancolombia_Carga()
        Try

            Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
            Call Me.Generar_Documento_Bancolombia()





        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Genera_Archivo_Interfaz:" & ex.Message.ToString
        End Try


    End Sub

    Private Sub Generar_Archivo_Interfaz_UNO_EE()
        Try
            Select Case intDocumentoContable

                Case INTERFAZ_COMPROBANTES_EGRESO
                    ' Generar Documento UnoEE Movimiento Contable Comprobante Egreso
                    Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivo, strPathArchivo, 1)
                    Call Me.Generar_Documento_Comprobante_Egreso_UNO_EE()

                    ' Generar Documento UnoEE Cuentas Por Pagar Comprobante Egreso
                    Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivoII, strPathArchivo, 2)
                    Call Me.Generar_Documento_Cuentas_Pagar_UNO_EE()

                Case INTERFAZ_NOTA_CREDITO
                    ' Generar Documento UnoEE Movimiento Contable Comprobante Egreso
                    Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivo, strPathArchivo, 1)
                    Call Me.Generar_Documento_Nota_Credito_UNO_EE()

                    ' Generar Documento UnoEE Cuentas Por Pagar Comprobante Egreso
                    'Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivoII, strPathArchivo, 2)
                    'Call Me.Generar_Documento_Nota_Credito_Documental_UNO_EE()

                Case INTERFAZ_LIQUIDACIONES
                    ' Generar Documento UnoEE Movimiento Contable Liquidaciones
                    Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivo, strPathArchivo, 1)
                    Call Me.Generar_Documento_Liquidacion_UNO_EE()

                Case INTERFAZ_FACTURAS
                    ' Generar Documento UnoEE Movimiento Contable Facturas
                    Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivo, strPathArchivo, 1)
                    Call Me.Generar_Documento_Facturas_UNO_EE()

                Case INTERFAZ_ANTICIPOS
                    ' Generar Documento UnoEE Movimiento Contable Facturas
                    Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivo, strPathArchivo, 1)
                    Call Me.Generar_Documento_Anticipos_UNO_EE()

            End Select

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Genera_Archivo_Interfaz:" & ex.Message.ToString
        End Try


    End Sub

    Private Sub Generar_Archivo_Interfaz_WORLD_OFFICE()
        Try
            Select Case intDocumentoContable

                ' Interfaz Facturacion
                Case INTERFAZ_FACTURAS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Factura_WORLD_OFFICE()

                ' Interfaz Facturacion Electronica
                Case INTERFAZ_FACTURAS_INVENTARIO
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Factura_Inventario_WORLD_OFFICE()

                ' Interfaz Comprobantes Egreso
                Case INTERFAZ_COMPROBANTES_EGRESO
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Interfaz_Comprobantes_WORLD_OFFICE()

                ' Interfaz Legalizacion Gastos
                Case INTERFAZ_LEGALIZACION_GASTOS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Interfaz_Legalizacion_Gastos_Conductor_WORLD_OFFICE()

            End Select

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Genera_Archivo_Interfaz:" & ex.Message.ToString
        End Try


    End Sub

    Private Sub Generar_Archivo_Interfaz_SIIGO_WINDOWS()
        Try
            Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
            Me.Generar_Interfaz_SIIGO_WINDOWS(intDocumentoContable)

            'Select Case intDocumentoContable

            '    Case INTERFAZ_TERCEROS
            '        Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
            '        Call Me.Generar_Documento_Tercero_SIIGO()

            '    Case INTERFAZ_COMPROBANTES_EGRESO
            '        Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
            '        Call Me.Generar_Documento_Comprobante_Egreso_SIIGO()

            '    Case INTERFAZ_COMPROBANTE_INGRESO
            '        Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
            '        Call Me.Generar_Documento_Comprobante_Ingreso_SIIGO()

            '    Case INTERFAZ_LIQUIDACIONES
            '        Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
            '        Call Me.Generar_Documento_Liquidacion_SIIGO()

            '    Case INTERFAZ_FACTURAS
            '        Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
            '        Call Me.Generar_Documento_Factura_SIIGO()

            '    Case INTERFAZ_LEGALIZACION_GASTOS
            '        Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
            '        Call Me.Generar_Documento_Legalizacion_Gastos_SIIGO()

            'End Select

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Genera_Archivo_Interfaz:" & ex.Message.ToString
        End Try


    End Sub

    Private Sub Armar_Nombre_Archivo(ByRef strNombreArchivo As String, ByRef strPathArchivo As String)
        Try
            strPathArchivo = System.AppDomain.CurrentDomain.BaseDirectory() & DIRECTORIO_ARCHIVOS_PLANOS

            'strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_BANCOLOMBIA
            strNombreArchivo = NOMBRE_ARCHIVO_BANCOLOMBIA



            strNombreArchivo = strNombreArchivo.Replace("/", "")

            strPathArchivo += strNombreArchivo

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Armar_Nombre_Archivo:" & ex.Message.ToString
        End Try


    End Sub

    Private Sub Armar_Nombre_Archivo_Uno_EE(ByRef strNombreArchivo As String, ByRef strPathArchivo As String, ByVal intDocumento As Integer)
        Try
            strPathArchivo = System.AppDomain.CurrentDomain.BaseDirectory() & DIRECTORIO_ARCHIVOS_PLANOS

            Select Case intDocumentoContable

                Case INTERFAZ_LIQUIDACIONES
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_UNO_EE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_LIQUIDACIONES_UNO_EE
                    End Select

                Case INTERFAZ_FACTURAS
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_UNO_EE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_FACTURAS_UNO_EE
                    End Select

                Case INTERFAZ_COMPROBANTES_EGRESO
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_UNO_EE
                            Select Case intDocumento
                                Case 1
                                    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_COMPROBANTES_EGRESO_UNO_EE
                                Case 2
                                    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_CUENTA_PAGAR_UNO_EE
                            End Select

                    End Select

                Case INTERFAZ_NOTA_CREDITO
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_UNO_EE
                            Select Case intDocumento
                                Case 1
                                    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_NOTA_CREDITO_MOVIMIENTO
                                    'Case 2
                                    '    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_NOTA_CREDITO_DOCUMENTO
                            End Select

                    End Select

                Case INTERFAZ_ANTICIPOS
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_UNO_EE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_ANTICIPOS_UNO_EE
                    End Select

            End Select


            strNombreArchivo = strNombreArchivo.Replace("/", "")

            strPathArchivo += strNombreArchivo

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Armar_Nombre_Archivo:" & ex.Message.ToString
        End Try


    End Sub

    Private Function Generar_Documento_Bancolombia() As Boolean
        Dim ComandoSQL As SqlCommand
        Dim ComandoSqlActualizacion As SqlCommand
        Dim ComandoSqlSecuenciaLotes As SqlCommand
        Dim intNumeroItem As Integer
        Dim stmStreamW As Stream
        Dim stwStreamWriter As StreamWriter
        Dim strFila As String
        Dim strCampo As String
        Dim strIdentificacion As String
        Dim strTipoIdentificacion As String
        Dim strTipoNit As String
        Dim strSexo As String
        Dim strTipoPersona As String
        Dim strAutoretenedor As String
        Dim strEstado As String
        Dim strEsRazonSocial As String
        Dim strIdentExtranjero As String = ""
        Dim strNombreCompletoTercero As String = ""

        Const ESPACIO As String = " "
        Dim intAuxCont As Integer = 0
        Dim strPrimerNombreTercero As String = ""
        Dim strSegundoNombreTercero As String = ""

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        Date.TryParse(Request.QueryString("FechInic"), dtaFechaInicial)
        Date.TryParse(Request.QueryString("FechFina"), dtaFechaFinal)

        'Se actualizan los comprobantes para efectos de auditoría relacionados con los archivos planos:
        Dim strSqlActualizacion = "UPDATE Encabezado_Documento_Comprobantes SET USUA_Codigo_Exportar_Transferencia =" & Request.QueryString("UsuarioExporta") & ", Fecha_Exportar_Transferencia ='" & Request.QueryString("FechaExportar") & "', Numero_Procesos_Exportar_Transferencia = ISNULL(Numero_Procesos_Exportar_Transferencia,0) + 1 WHERE EMPR_Codigo = " & Request.QueryString("Empresa") & " AND Codigo IN(" & Request.QueryString("strCodigosComprobantes") & ")"
        Dim strConexion As String = Me.objGeneral.ConexionSQL.ConnectionString
        Me.objGeneral.ConexionSQL.Open()
        ComandoSqlActualizacion = New SqlCommand(strSqlActualizacion, Me.objGeneral.ConexionSQL)
        Dim sqlActualizacion As SqlDataReader = ComandoSqlActualizacion.ExecuteReader

        Me.objGeneral.ConexionSQL.Close()
        sqlActualizacion.Close()

        Dim LetraProceso As String
        Using conexion = New DatabaseFactoryConcrete(strConexion).CreateDataBaseFactory()
            conexion.CreateConnection()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", Request.QueryString("Empresa"), SqlDbType.Int)
            conexion.AgregarParametroSQL("@par_Fecha", DateTime.Now.ToString("yyyy-MM-d"), SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_USUA_Genera", Request.QueryString("UsuarioExporta"), SqlDbType.Int)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_secuencia_lotes_dia")

            While resultado.Read
                LetraProceso = resultado.Item("Letra")
            End While
        End Using

        'Me.objGeneral.ConexionSQL.Close()
        'sqlSecuenciaLotes.Close()
        ''Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        'Consulta generada para crear el archivo plano de Bancolombia
        strSQL = "SELECT DISTINCT *"
        strSQL += " FROM V_Generar_Archivo_Bancolombia"
        strSQL += " WHERE EMPR_Codigo = " & Request.QueryString("Empresa")


        If (Request.QueryString("Tercero") <> "") Then
            strSQL += " AND TERC_Codigo_Titular = " & Request.QueryString("Tercero")
        End If

        If Request.QueryString("Oficina") <> "-1" Then
            strSQL += " AND OFIC_Codigo = " & Request.QueryString("Oficina")
        End If
        strSQL += " AND Codigo IN( " & Request.QueryString("strCodigosComprobantes") & ")"
        If Request.QueryString("Documento_Origen") <> "2600" Then
            strSQL += "AND CATA_DOOR_Codigo =" & Request.QueryString("Documento_Origen")
        End If
        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        intNumeroItem = 0
        Try
            strFila = String.Empty
            Dim Cabecera As Boolean = False
            While sdrArchInte.Read
                intNumeroItem += 1
                Dim NITEntidad As String = ""
                Dim NombreEntidad As String = ""
                Dim FechaAplicacionTransacciones As String = ""
                Dim SumatoriaCreditos As String = ""
                Dim CuentaCliente As String = ""
                Dim TipoCuentaCliente As String = ""
                Dim Numero_Identificacion As String = ""
                Dim NombreBeneficiario As String = ""
                Dim NumeroCuentaBeneficiario As String = ""
                Dim BancoBeneficiario As String = ""
                Dim TipoTransaccion As String = ""
                Dim ValorTransaccion As String = ""
                Dim Concepto As String = ""
                Dim Referencia As String = ""

                ' Se hace un retorna campos por cada tercero que consulte
                Dim arrCampos(13), arrValoresCampos(13) As String


                arrCampos(0) = "NITEntidad"
                arrCampos(1) = "NombreEntidad"
                arrCampos(2) = "FechaAplicacionTransacciones"
                arrCampos(3) = "SumatoriaCreditos"
                arrCampos(4) = "CuentaCliente"
                arrCampos(5) = "TipoCuentaCliente"
                arrCampos(6) = "Numero_Identificacion"
                arrCampos(7) = "NombreBeneficiario"
                arrCampos(8) = "NumeroCuentaBeneficiario"
                arrCampos(9) = "BancoBeneficiario"
                arrCampos(10) = "TipoTransaccion"
                arrCampos(11) = "ValorTransaccion"
                arrCampos(12) = "Concepto"
                arrCampos(13) = "Referencia"


                If Me.objGeneral.Retorna_Campos(intEmpresa, "V_Generar_Archivo_Bancolombia", arrCampos, arrValoresCampos, 14, clsGeneral.CAMPO_NUMERICO, "Codigo", Val(sdrArchInte("Codigo")), "", Me.strError) Then
                    NITEntidad = arrValoresCampos(0)
                    NombreEntidad = arrValoresCampos(1)
                    FechaAplicacionTransacciones = arrValoresCampos(2)
                    SumatoriaCreditos = arrValoresCampos(3)
                    CuentaCliente = arrValoresCampos(4)
                    TipoCuentaCliente = arrValoresCampos(5)
                    Numero_Identificacion = arrValoresCampos(6)
                    NombreBeneficiario = arrValoresCampos(7)
                    NumeroCuentaBeneficiario = arrValoresCampos(8)
                    BancoBeneficiario = arrValoresCampos(9)
                    TipoTransaccion = arrValoresCampos(10)
                    ValorTransaccion = arrValoresCampos(11)
                    Concepto = arrValoresCampos(12)
                    Referencia = arrValoresCampos(13)
                End If



                '********* Comenzar a escribir los campos **************
                If Cabecera = False Then
                    ' Tipo Registro -> 001-013
                    strCampo = Me.Formatear_Campo("1", 1, CAMPO_NUMERICO)
                    strFila = strCampo & SEPARADOR_SIIGO_WINDOWS
                    'Nit entidad que envía
                    strCampo = Me.Formatear_Campo(NITEntidad, 10, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    strCampo = Me.Formatear_Campo(NombreEntidad, 16, CAMPO_ESPACIOS_DERECHA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    strCampo = Me.Formatear_Campo("238", 3, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    strCampo = Me.Formatear_Campo(DateTime.Now.ToString("HH:mm:ss").Replace(":", ""), 10, CAMPO_ESPACIOS_DERECHA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    strCampo = Me.Formatear_Campo(DateTime.Now.ToString("yy-MM-d").Replace("-", ""), 6, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    strCampo = Me.Formatear_Campo(LetraProceso, 1, CAMPO_ESPACIOS_DERECHA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    strCampo = Me.Formatear_Campo(DateTime.Now.ToString("yy-MM-d").Replace("-", ""), 6, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    strCampo = Me.Formatear_Campo(Request.QueryString("strCodigosComprobantes").Split({","}, StringSplitOptions.RemoveEmptyEntries).Count, 6, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    strCampo = Me.Formatear_Campo("000000000000", 12, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    strCampo = Me.Formatear_Campo(Request.QueryString("SumatoriaCreditos"), 12, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    strCampo = Me.Formatear_Campo(CuentaCliente, 11, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    strCampo = Me.Formatear_Campo(TipoCuentaCliente, 1, CAMPO_ESPACIOS_DERECHA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    stwStreamWriter.WriteLine(strFila, System.Text.Encoding.Default)
                    Cabecera = True
                End If

                strCampo = Me.Formatear_Campo("6", 1, CAMPO_CEROS_IZQUIERDA)
                strFila = strCampo & SEPARADOR_SIIGO_WINDOWS
                strCampo = Me.Formatear_Campo(Numero_Identificacion, 15, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                strCampo = Me.Formatear_Campo(NombreBeneficiario, 18, CAMPO_ESPACIOS_DERECHA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                strCampo = Me.Formatear_Campo(BancoBeneficiario, 9, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                strCampo = Me.Formatear_Campo(NumeroCuentaBeneficiario, 17, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                strCampo = Me.Formatear_Campo("S", 1, CAMPO_ESPACIOS_DERECHA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                strCampo = Me.Formatear_Campo(TipoTransaccion, 2, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                strCampo = Me.Formatear_Campo(ValorTransaccion, 10, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                strCampo = Me.Formatear_Campo(Concepto, 9, CAMPO_ESPACIOS_DERECHA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                strCampo = Me.Formatear_Campo(Referencia, 12, CAMPO_ESPACIOS_DERECHA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS

                stwStreamWriter.WriteLine(strFila, System.Text.Encoding.Default)

            End While
            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Me.strError = "En la clase " & Me.ToString & " se presentó un Error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        End Try


    End Function

    ' Autor: JESUS CUADROS
    ' Fecha Creación: 08-AGO-2019
    ' Obsevaciones Creación: 
    ' Autor: JESUS CUADROS
    ' Fecha Modificación: 17-SEP-2019
    ' Observaciones Modificación: Las facturas anuladas se reportan con valores en cero
    Private Function Generar_Documento_Factura_SIIGO() As Boolean
        Dim ComandoSQL As SqlCommand
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, intPos As Integer
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String
        Dim dblValoDebi As Double, dblValoCred As Double, dblValoBase As Double
        Dim strCodigoCuentaPUC As String, strClaseCuentaPUC As String, strGrupoCuentaPUC As String
        Dim strCuentaPUC As String, strSubCuenta As String, lonNumeFact As Long, strObservaciones As String, bolFactAnul As Boolean

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            strSQL = "SELECT ENCC.EMPR_Codigo, ENCC.TIDO_Codigo, ENCC.Numero, ENCC.TIDO_Documento, ENCC.Numero_Documento,"
            strSQL += "ENCC.Fecha_Documento, ENCC.TERC_Documento, ENCC.Año, ENCC.Periodo, ENCC.OFIC_Codigo,"
            strSQL += "ENCC.Valor_Comprobante, ENCC.Fecha, ENCC.CATA_ESIC_Codigo, ENCC.Anulado, ENCC.Observaciones,"
            strSQL += "DECC.ID, DECC.PLUC_Codigo, DECC.TERC_Codigo, DECC.Valor_Base, DECC.Valor_Debito, DECC.Valor_Credito,"
            strSQL += "PLUC.Codigo_Cuenta, TERC.Numero_Identificacion, ENCC.Codigo_Documento"
            strSQL += " FROM Encabezado_Comprobante_Contables As ENCC, Detalle_Comprobante_Contables As DECC, Plan_Unico_Cuentas AS PLUC, Terceros AS TERC"
            strSQL += " WHERE ENCC.EMPR_Codigo = DECC.EMPR_Codigo"
            strSQL += " AND ENCC.Numero  = DECC.ENCC_Numero"
            ' Plan Unico Cuentas
            strSQL += " AND DECC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND DECC.PLUC_Codigo  = PLUC.Codigo"
            ' Terceros
            strSQL += " AND DECC.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND DECC.TERC_Codigo  = TERC.Codigo"
            strSQL += " AND ENCC.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ENCC.Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND ENCC.Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
            strSQL += " AND ENCC.TIDO_Documento = " & TIDO_FACTURAS
            strSQL += " AND ENCC.CATA_ESIC_Codigo = " & ESTADO_INTERFAZ_PENDIENTE
            strSQL += " AND ENCC.Anulado = 0"
            strSQL += " ORDER BY ENCC.Numero_Documento"

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            While sdrArchInte.Read

                If lonNumeroComprobante = Val(sdrArchInte("Numero_Documento")) Then
                    intSecuencia += 1
                Else
                    lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                    intSecuencia = 1
                End If
                lonNumeFact = sdrArchInte("Numero_Documento") ' Número Factura
                strObservaciones = sdrArchInte("Observaciones")

                ' TEMPORAL
                ' En las Observaciones del Comprobante Contable viene la palabra ANULA y de esta manera se identifica que el comprobante corresponde a un Anulación de Factura
                intPos = InStr(strObservaciones, "ANULA")
                If intPos > 0 Then
                    bolFactAnul = True
                    strObservaciones = "MOVIMIENTO CONTABLE ANULACION FACTURA No. " & lonNumeFact.ToString
                Else
                    bolFactAnul = False
                    strObservaciones = "MOVIMIENTO CONTABLE FACTURA No. " & lonNumeFact.ToString
                End If

                ' INTEGRA solicita que las anulaciones de facturas se reporten con valores en 0
                If bolFactAnul Then
                    dblValoDebi = 0
                    dblValoCred = 0
                    dblValoBase = 0
                Else
                    dblValoDebi = sdrArchInte("Valor_Debito")
                    dblValoCred = sdrArchInte("Valor_Credito")
                    dblValoBase = sdrArchInte("Valor_Base")
                End If

                strFila = Formatear_Campo_SIIGO(TIPO_COMPROBANTE_FACTURAS, 1, CAMPO_ALFANUMERICO_SIIGO) '1-Tipo Comprobante
                strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_FACTURAS, 3, CAMPO_NUMERICO_SIIGO) '2-Codigo Comprobante
                strFila += Formatear_Campo_SIIGO(lonNumeroComprobante, 11, CAMPO_NUMERICO_SIIGO) '3-No Documento
                strFila += Formatear_Campo_SIIGO(intSecuencia, 5, CAMPO_NUMERICO_SIIGO) ' 4- Secuencia
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Numero_Identificacion"), 13, CAMPO_NUMERICO_SIIGO) '5-Identificacion Cliente
                strFila += Formatear_Campo_SIIGO("0", 3, CAMPO_NUMERICO_SIIGO) '6-Sucursal

                ' Código Cuenta Contable
                strCodigoCuentaPUC = sdrArchInte("Codigo_Cuenta")

                ' TEMPORAL 16-AGO-2019 Quedaron mal parametrizadas las cuentas PUC
                If strCodigoCuentaPUC = "13050501" Then
                    strCodigoCuentaPUC = "13050500" ' Cta PUC CxC
                ElseIf strCodigoCuentaPUC = "41450501" Then
                    strCodigoCuentaPUC = "41450100" ' Cta PUC Servicio Transporte
                ElseIf strCodigoCuentaPUC = "13551901" Then
                    strCodigoCuentaPUC = "13551802" ' Cta PUC ReteICA (Tener en cuenta que se arma con el código de la oficina)
                End If

                strClaseCuentaPUC = Mid$(strCodigoCuentaPUC, 1, 2)
                strFila += Formatear_Campo_SIIGO(strClaseCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '7 - 13
                strGrupoCuentaPUC = Mid$(strCodigoCuentaPUC, 3, 2)
                strFila += Formatear_Campo_SIIGO(strGrupoCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '8 - 30
                strCuentaPUC = Mid$(strCodigoCuentaPUC, 5, 2)
                strFila += Formatear_Campo_SIIGO(strCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '9 - 05
                strSubCuenta = Mid$(strCodigoCuentaPUC, 7, 2)
                strFila += Formatear_Campo_SIIGO(strSubCuenta, 2, CAMPO_NUMERICO_SIIGO) '10 - 01
                strFila += Formatear_Campo_SIIGO("00", 2, CAMPO_NUMERICO_SIIGO) '11 - 00

                ' Producto Transporte Carretera
                ' 300-Linea
                ' 0001-Grupo
                ' 000001-Elemento
                strFila += Formatear_Campo_SIIGO("300", 3, CAMPO_NUMERICO_SIIGO) '12
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '13
                strFila += Formatear_Campo_SIIGO("1", 6, CAMPO_NUMERICO_SIIGO) '14

                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '15-Año
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '16-Mes
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '17-Dia
                If strCodigoCuentaPUC = "41450100" Or strCodigoCuentaPUC = "61450500" Then ' Para estas cuentas se reporta Centro Costo: 11
                    strFila += Formatear_Campo_SIIGO("11", 4, CAMPO_NUMERICO_SIIGO) '18-Centro Costo
                Else
                    strFila += Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '18-Centro Costo
                End If
                strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '19-Sub Centro Costo
                strFila += Formatear_Campo_SIIGO(strObservaciones, 50, CAMPO_ALFANUMERICO_SIIGO) '20-Descripción

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO("D", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoDebi, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                Else
                    strFila += Formatear_Campo_SIIGO("C", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoCred, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                End If
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '23-Valor Decimales
                strFila += Formatear_Campo_SIIGO(dblValoBase, 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '25-Valor Base Retención Decimales

                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '26-Codigo Vendedor 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '27-Codigo Ciudad - FALTA
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '28-Codigo Zona 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '29-Codigo Bodega
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '30-Codigo Ubicación
                strFila += Formatear_Campo_SIIGO("0", 10, CAMPO_NUMERICO_SIIGO) '31-Cantidad
                strFila += Formatear_Campo_SIIGO("0", 5, CAMPO_NUMERICO_SIIGO) '32-Cantidad Decimales

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO(TIPO_COMPROBANTE_FACTURAS, 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_EGRESO, 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("", 1, CAMPO_NUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                End If

                ' Si cuenta PUC es 1305 reportar en documento cruce el Número Factura
                If strClaseCuentaPUC = "13" And strGrupoCuentaPUC = "05" Then
                    ' Reportar el Número de Factura
                    strFila += Formatear_Campo_SIIGO(lonNumeFact, 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("0", 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                End If

                strFila += Formatear_Campo_SIIGO(intSecuencia, 3, CAMPO_NUMERICO_SIIGO) '36- Secuencia Documento Cruce
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '37-Año Vencimiento
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '38-Mes Vencimiento
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '39-Dia Vencimiento
                strFila += Formatear_Campo_SIIGO("0", 4, CAMPO_NUMERICO_SIIGO) '40-Forma Pago
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '41-Código Banco

                stwStreamWriter.WriteLine(strFila)

            End While
            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
                Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un Error en la función Generar_Documento_Factura_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally
            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function
    'Private Function Generar_Documento_Liquidacion_SIIGO_OLD() As Boolean
    '    Dim ComandoSQL As SqlCommand
    '    Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, bolRes As Boolean
    '    Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String
    '    Dim dblValoDebi As Double, dblValoCred As Double, dblValoBase As Double
    '    Dim strCodigoCuentaPUC As String, strClaseCuentaPUC As String, strGrupoCuentaPUC As String, strCentroCosto As String
    '    Dim strCuentaPUC As String, strSubCuenta As String, lonNumeDocuEgre As Long ''lonCodiEgre As Long
    '    Dim lonNumeLiqu As Long, lonNumeDocuLiqu As Long, lonNumeDocuPlan As Long, strObservaciones As String

    '    ' Si el archivo existe se borra primero 
    '    If File.Exists(Me.strPathArchivo) Then
    '        File.Delete(Me.strPathArchivo)
    '    End If

    '    ' Se abre el archivo 
    '    stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
    '    stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

    '    Try

    '        strSQL = "SELECT ENCC.EMPR_Codigo, ENCC.TIDO_Codigo, ENCC.Numero, ENCC.TIDO_Documento, ENCC.Numero_Documento,"
    '        strSQL += "ENCC.Fecha_Documento, ENCC.TERC_Documento, ENCC.Año, ENCC.Periodo, ENCC.OFIC_Codigo,"
    '        strSQL += "ENCC.Valor_Comprobante, ENCC.Fecha, ENCC.CATA_ESIC_Codigo, ENCC.Anulado,"
    '        strSQL += "DECC.ID, DECC.PLUC_Codigo, DECC.TERC_Codigo, DECC.Valor_Base, DECC.Valor_Debito, DECC.Valor_Credito, DECC.Observaciones,"
    '        strSQL += "PLUC.Codigo_Cuenta, TERC.Numero_Identificacion, ENCC.Codigo_Documento"
    '        strSQL += " FROM Encabezado_Comprobante_Contables As ENCC, Detalle_Comprobante_Contables As DECC, Plan_Unico_Cuentas AS PLUC, Terceros AS TERC"
    '        strSQL += " WHERE ENCC.EMPR_Codigo = DECC.EMPR_Codigo"
    '        strSQL += " AND ENCC.Numero  = DECC.ENCC_Numero"
    '        ' Plan Unico Cuentas
    '        strSQL += " AND DECC.EMPR_Codigo = PLUC.EMPR_Codigo"
    '        strSQL += " AND DECC.PLUC_Codigo  = PLUC.Codigo"
    '        ' Terceros
    '        strSQL += " AND DECC.EMPR_Codigo = TERC.EMPR_Codigo"
    '        strSQL += " AND DECC.TERC_Codigo  = TERC.Codigo"

    '        strSQL += " AND ENCC.EMPR_Codigo = " & intEmpresa.ToString
    '        strSQL += " AND ENCC.Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
    '        strSQL += " AND ENCC.Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
    '        strSQL += " AND ENCC.TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
    '        strSQL += " AND ENCC.CATA_ESIC_Codigo = " & ESTADO_INTERFAZ_PENDIENTE
    '        strSQL += " AND ENCC.Anulado = 0"
    '        strSQL += " ORDER BY ENCC.Numero_Documento"

    '        ' FALTA Revisar si el comprobante solo se genera para Liquidaciones Aprobadas

    '        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
    '        Me.objGeneral.ConexionSQL.Open()
    '        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

    '        While sdrArchInte.Read

    '            lonNumeLiqu = sdrArchInte("Codigo_Documento") ' Número Liquidación

    '            If lonNumeroComprobante = Val(sdrArchInte("Numero_Documento")) Then
    '                intSecuencia += 1
    '            Else
    '                lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
    '                intSecuencia = 1
    '                bolRes = Retorna_Informacion_Liquidacion(lonNumeLiqu, lonNumeDocuLiqu, lonNumeDocuPlan)
    '            End If

    '            dblValoDebi = sdrArchInte("Valor_Debito")
    '            dblValoCred = sdrArchInte("Valor_Credito")
    '            dblValoBase = sdrArchInte("Valor_Base")

    '            strFila = Formatear_Campo_SIIGO(TIPO_COMPROBANTE_LIQUIDACIONES, 1, CAMPO_ALFANUMERICO_SIIGO) '1-Tipo Comprobante
    '            strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_LIQUIDACIONES, 3, CAMPO_NUMERICO_SIIGO) '2-Codigo Comprobante
    '            strFila += Formatear_Campo_SIIGO(lonNumeroComprobante, 11, CAMPO_NUMERICO_SIIGO) '3-No Documento
    '            strFila += Formatear_Campo_SIIGO(intSecuencia, 5, CAMPO_NUMERICO_SIIGO) ' 4- Secuencia
    '            strFila += Formatear_Campo_SIIGO(sdrArchInte("Numero_Identificacion"), 13, CAMPO_NUMERICO_SIIGO) '5-Identificacion
    '            strFila += Formatear_Campo_SIIGO("0", 3, CAMPO_NUMERICO_SIIGO) '6-Sucursal

    '            ' Código Cuenta Contable
    '            strCodigoCuentaPUC = sdrArchInte("Codigo_Cuenta")

    '            ' Observaciones
    '            strObservaciones = sdrArchInte("Observaciones") & " Planilla:" & lonNumeDocuPlan.ToString

    '            ' TEMPORAL: 15-AGO-2019 Por error en parametrización se cambian cuentas
    '            If strCodigoCuentaPUC = "61450501" Then ' CTA FLETE
    '                strCodigoCuentaPUC = "61450500"
    '                dblValoBase = dblValoDebi

    '            ElseIf strCodigoCuentaPUC = "13300501" Then ' CTA ANTICIPO
    '                strCodigoCuentaPUC = "13300500"

    '            ElseIf strCodigoCuentaPUC = "23680601" Then ' RETEICA
    '                strCodigoCuentaPUC = "23680502"
    '            End If

    '            strClaseCuentaPUC = Mid$(strCodigoCuentaPUC, 1, 2)
    '            strFila += Formatear_Campo_SIIGO(strClaseCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '7 - 13
    '            strGrupoCuentaPUC = Mid$(strCodigoCuentaPUC, 3, 2)
    '            strFila += Formatear_Campo_SIIGO(strGrupoCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '8 - 30
    '            strCuentaPUC = Mid$(strCodigoCuentaPUC, 5, 2)
    '            strFila += Formatear_Campo_SIIGO(strCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '9 - 05
    '            strSubCuenta = Mid$(strCodigoCuentaPUC, 7, 2)
    '            strFila += Formatear_Campo_SIIGO(strSubCuenta, 2, CAMPO_NUMERICO_SIIGO) '10 - 01
    '            strFila += Formatear_Campo_SIIGO("00", 2, CAMPO_NUMERICO_SIIGO) '11 - 00

    '            If strClaseCuentaPUC = "61" And strGrupoCuentaPUC = "45" Then
    '                strCentroCosto = "11"
    '            Else
    '                strCentroCosto = ""
    '            End If

    '            ' Producto Transporte Carretera
    '            ' 300-Linea
    '            ' 0001-Grupo
    '            ' 000001-Elemento
    '            strFila += Formatear_Campo_SIIGO("300", 3, CAMPO_NUMERICO_SIIGO) '12
    '            strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '13
    '            strFila += Formatear_Campo_SIIGO("1", 6, CAMPO_NUMERICO_SIIGO) '14

    '            strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '15-Año
    '            strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '16-Mes
    '            strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '17-Dia
    '            strFila += Formatear_Campo_SIIGO(strCentroCosto, 4, CAMPO_NUMERICO_SIIGO) '18-Centro Costo
    '            strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '19-Sub Centro Costo
    '            strObservaciones = Mid$(strObservaciones, 1, 50)
    '            strFila += Formatear_Campo_SIIGO(strObservaciones, 50, CAMPO_ALFANUMERICO_SIIGO) '20-Descripción

    '            If dblValoDebi > 0 Then
    '                strFila += Formatear_Campo_SIIGO("D", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
    '                strFila += Formatear_Campo_SIIGO(dblValoDebi, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
    '            Else
    '                strFila += Formatear_Campo_SIIGO("C", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
    '                strFila += Formatear_Campo_SIIGO(dblValoCred, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
    '            End If
    '            strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '23-Valor Decimales

    '            If strClaseCuentaPUC = "23" And strGrupoCuentaPUC = "65" Then
    '                strFila += Formatear_Campo_SIIGO(dblValoBase, 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención debe ser el Flete
    '            ElseIf strClaseCuentaPUC = "23" And strGrupoCuentaPUC = "68" Then
    '                strFila += Formatear_Campo_SIIGO(dblValoBase, 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención debe ser el Flete
    '            Else
    '                strFila += Formatear_Campo_SIIGO("0", 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención
    '            End If
    '            strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '25-Valor Base Retención Decimales

    '            strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '26-Codigo Vendedor 
    '            strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '27-Codigo Ciudad - FALTA
    '            strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '28-Codigo Zona 
    '            strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '29-Codigo Bodega
    '            strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '30-Codigo Ubicación
    '            strFila += Formatear_Campo_SIIGO("0", 10, CAMPO_NUMERICO_SIIGO) '31-Cantidad
    '            strFila += Formatear_Campo_SIIGO("0", 5, CAMPO_NUMERICO_SIIGO) '32-Cantidad Decimales

    '            If strClaseCuentaPUC = "13" And strGrupoCuentaPUC = "30" Then
    '                strFila += Formatear_Campo_SIIGO(TIPO_COMPROBANTE_EGRESOS, 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
    '                strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_EGRESO, 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
    '            ElseIf strClaseCuentaPUC = "22" And strGrupoCuentaPUC = "05" Then
    '                strFila += Formatear_Campo_SIIGO(TIPO_COMPROBANTE_LIQUIDACIONES, 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
    '                strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_LIQUIDACIONES, 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
    '            Else
    '                strFila += Formatear_Campo_SIIGO("", 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
    '                strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
    '            End If

    '            ' Si cuenta PUC es 1330 reportar en documento cruce el Comprobante Egreso del Anticipo
    '            If strClaseCuentaPUC = "13" And strGrupoCuentaPUC = "30" Then

    '                ' Consultar el Comprobante Egreso del Anticipo de la Planilla
    '                bolRes = Retorna_Egreso_Anticipo_Planilla(lonNumeDocuPlan, lonNumeDocuEgre)

    '                strFila += Formatear_Campo_SIIGO(lonNumeDocuEgre, 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce

    '            ElseIf strClaseCuentaPUC = "22" And strGrupoCuentaPUC = "05" Then
    '                ' No. Liquidación
    '                strFila += Formatear_Campo_SIIGO(lonNumeDocuLiqu, 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
    '            Else
    '                strFila += Formatear_Campo_SIIGO("0", 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
    '            End If

    '            strFila += Formatear_Campo_SIIGO(intSecuencia, 3, CAMPO_NUMERICO_SIIGO) '36- Secuencia Documento Cruce
    '            strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '37-Año Vencimiento
    '            strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '38-Mes Vencimiento
    '            strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '39-Dia Vencimiento
    '            strFila += Formatear_Campo_SIIGO("0", 4, CAMPO_NUMERICO_SIIGO) '40-Forma Pago
    '            strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '41-Código Banco

    '            stwStreamWriter.WriteLine(strFila)

    '        End While
    '        sdrArchInte.Close()

    '        stwStreamWriter.Close()
    '        stmStreamW.Close()

    '        If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
    '            Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
    '        End If

    '        Return True

    '    Catch ex As Exception
    '        stwStreamWriter.Close()
    '        stmStreamW.Close()
    '        Me.objGeneral.ConexionSQL.Close()
    '        Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
    '        Return False
    '    Finally
    '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
    '            Me.objGeneral.ConexionSQL.Close()
    '        End If
    '    End Try
    'End Function



    ' Autor: JESUS CUADROS
    ' Fecha Creación: 08-AGO-2019
    ' Obsevaciones Creación: Se copia la misma rutina de egresos
    ' Fecha Modificación: 15-AGO-2019
    ' Observaciones Modificación: Se hace un manejo para las cuentas PUC mal parametrizadas
    Private Function Generar_Documento_Liquidacion_SIIGO() As Boolean
        Dim ComandoSQL As SqlCommand
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, bolRes As Boolean
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String
        Dim dblValoDebi As Double, dblValoCred As Double, dblValoBase As Double
        Dim strCodigoCuentaPUC As String, strClaseCuentaPUC As String, strGrupoCuentaPUC As String, strCentroCosto As String
        Dim strCuentaPUC As String, strSubCuenta As String, lonNumeDocuEgre As Long ''lonCodiEgre As Long
        Dim lonNumeLiqu As Long, lonNumeDocuLiqu As Long, lonNumeDocuPlan As Long, strObservaciones As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            strSQL = "SELECT ENCC.EMPR_Codigo, ENCC.TIDO_Codigo, ENCC.Numero, ENCC.TIDO_Documento, ENCC.Numero_Documento,"
            strSQL += "ENCC.Fecha_Documento, ENCC.TERC_Documento, ENCC.Año, ENCC.Periodo, ENCC.OFIC_Codigo,"
            strSQL += "ENCC.Valor_Comprobante, ENCC.Fecha, ENCC.CATA_ESIC_Codigo, ENCC.Anulado,"
            strSQL += "DECC.ID, DECC.PLUC_Codigo, DECC.TERC_Codigo, DECC.Valor_Base, DECC.Valor_Debito, DECC.Valor_Credito, DECC.Observaciones,"
            strSQL += "PLUC.Codigo_Cuenta, TERC.Numero_Identificacion, ENCC.Codigo_Documento"
            strSQL += " FROM Encabezado_Comprobante_Contables As ENCC, Detalle_Comprobante_Contables As DECC, Plan_Unico_Cuentas AS PLUC, Terceros AS TERC"
            strSQL += " WHERE ENCC.EMPR_Codigo = DECC.EMPR_Codigo"
            strSQL += " AND ENCC.Numero  = DECC.ENCC_Numero"
            ' Plan Unico Cuentas
            strSQL += " AND DECC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND DECC.PLUC_Codigo  = PLUC.Codigo"
            ' Terceros
            strSQL += " AND DECC.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND DECC.TERC_Codigo  = TERC.Codigo"

            strSQL += " AND ENCC.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ENCC.Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND ENCC.Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
            strSQL += " AND ENCC.TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
            strSQL += " AND ENCC.CATA_ESIC_Codigo = " & ESTADO_INTERFAZ_PENDIENTE
            strSQL += " AND ENCC.Anulado = 0"
            strSQL += " ORDER BY ENCC.Numero_Documento"

            ' FALTA Revisar si el comprobante solo se genera para Liquidaciones Aprobadas

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            While sdrArchInte.Read

                lonNumeLiqu = sdrArchInte("Codigo_Documento") ' Número Liquidación

                If lonNumeroComprobante = Val(sdrArchInte("Numero_Documento")) Then
                    intSecuencia += 1
                Else
                    lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                    intSecuencia = 1
                    bolRes = Retorna_Informacion_Liquidacion(lonNumeLiqu, lonNumeDocuLiqu, lonNumeDocuPlan)
                End If

                dblValoDebi = sdrArchInte("Valor_Debito")
                dblValoCred = sdrArchInte("Valor_Credito")
                dblValoBase = sdrArchInte("Valor_Base")

                strFila = Formatear_Campo_SIIGO(TIPO_COMPROBANTE_LIQUIDACIONES, 1, CAMPO_ALFANUMERICO_SIIGO) '1-Tipo Comprobante
                strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_LIQUIDACIONES, 3, CAMPO_NUMERICO_SIIGO) '2-Codigo Comprobante
                strFila += Formatear_Campo_SIIGO(lonNumeroComprobante, 11, CAMPO_NUMERICO_SIIGO) '3-No Documento
                strFila += Formatear_Campo_SIIGO(intSecuencia, 5, CAMPO_NUMERICO_SIIGO) ' 4- Secuencia
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Numero_Identificacion"), 13, CAMPO_NUMERICO_SIIGO) '5-Identificacion
                strFila += Formatear_Campo_SIIGO("0", 3, CAMPO_NUMERICO_SIIGO) '6-Sucursal

                ' Código Cuenta Contable
                strCodigoCuentaPUC = sdrArchInte("Codigo_Cuenta")

                ' Observaciones
                strObservaciones = sdrArchInte("Observaciones") & " Planilla:" & lonNumeDocuPlan.ToString

                ' TEMPORAL: 15-AGO-2019 Por error en parametrización se cambian cuentas
                If strCodigoCuentaPUC = "61450501" Then ' CTA FLETE
                    strCodigoCuentaPUC = "61450500"
                    dblValoBase = dblValoDebi

                ElseIf strCodigoCuentaPUC = "13300501" Then ' CTA ANTICIPO
                    strCodigoCuentaPUC = "13300500"

                ElseIf strCodigoCuentaPUC = "23680601" Then ' RETEICA
                    strCodigoCuentaPUC = "23680502"
                End If

                strClaseCuentaPUC = Mid$(strCodigoCuentaPUC, 1, 2)
                strFila += Formatear_Campo_SIIGO(strClaseCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '7 - 13
                strGrupoCuentaPUC = Mid$(strCodigoCuentaPUC, 3, 2)
                strFila += Formatear_Campo_SIIGO(strGrupoCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '8 - 30
                strCuentaPUC = Mid$(strCodigoCuentaPUC, 5, 2)
                strFila += Formatear_Campo_SIIGO(strCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '9 - 05
                strSubCuenta = Mid$(strCodigoCuentaPUC, 7, 2)
                strFila += Formatear_Campo_SIIGO(strSubCuenta, 2, CAMPO_NUMERICO_SIIGO) '10 - 01
                strFila += Formatear_Campo_SIIGO("00", 2, CAMPO_NUMERICO_SIIGO) '11 - 00

                If strClaseCuentaPUC = "61" And strGrupoCuentaPUC = "45" Then
                    strCentroCosto = "11"
                Else
                    strCentroCosto = ""
                End If

                ' Producto Transporte Carretera
                ' 300-Linea
                ' 0001-Grupo
                ' 000001-Elemento
                strFila += Formatear_Campo_SIIGO("300", 3, CAMPO_NUMERICO_SIIGO) '12
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '13
                strFila += Formatear_Campo_SIIGO("1", 6, CAMPO_NUMERICO_SIIGO) '14

                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '15-Año
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '16-Mes
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '17-Dia
                strFila += Formatear_Campo_SIIGO(strCentroCosto, 4, CAMPO_NUMERICO_SIIGO) '18-Centro Costo
                strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '19-Sub Centro Costo
                strObservaciones = Mid$(strObservaciones, 1, 50)
                strFila += Formatear_Campo_SIIGO(strObservaciones, 50, CAMPO_ALFANUMERICO_SIIGO) '20-Descripción

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO("D", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoDebi, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                Else
                    strFila += Formatear_Campo_SIIGO("C", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoCred, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                End If
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '23-Valor Decimales

                If strClaseCuentaPUC = "23" And strGrupoCuentaPUC = "65" Then
                    strFila += Formatear_Campo_SIIGO(dblValoBase, 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención debe ser el Flete
                ElseIf strClaseCuentaPUC = "23" And strGrupoCuentaPUC = "68" Then
                    strFila += Formatear_Campo_SIIGO(dblValoBase, 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención debe ser el Flete
                Else
                    strFila += Formatear_Campo_SIIGO("0", 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención
                End If
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '25-Valor Base Retención Decimales

                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '26-Codigo Vendedor 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '27-Codigo Ciudad - FALTA
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '28-Codigo Zona 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '29-Codigo Bodega
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '30-Codigo Ubicación
                strFila += Formatear_Campo_SIIGO("0", 10, CAMPO_NUMERICO_SIIGO) '31-Cantidad
                strFila += Formatear_Campo_SIIGO("0", 5, CAMPO_NUMERICO_SIIGO) '32-Cantidad Decimales

                If strClaseCuentaPUC = "13" And strGrupoCuentaPUC = "30" Then
                    strFila += Formatear_Campo_SIIGO(TIPO_COMPROBANTE_EGRESOS, 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_EGRESO, 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                ElseIf strClaseCuentaPUC = "22" And strGrupoCuentaPUC = "05" Then
                    strFila += Formatear_Campo_SIIGO(TIPO_COMPROBANTE_LIQUIDACIONES, 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_LIQUIDACIONES, 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("", 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                End If

                ' Si cuenta PUC es 1330 reportar en documento cruce el Comprobante Egreso del Anticipo
                If strClaseCuentaPUC = "13" And strGrupoCuentaPUC = "30" Then

                    ' Consultar el Comprobante Egreso del Anticipo de la Planilla
                    bolRes = Retorna_Egreso_Anticipo_Planilla(lonNumeDocuPlan, lonNumeDocuEgre)

                    strFila += Formatear_Campo_SIIGO(lonNumeDocuEgre, 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce

                ElseIf strClaseCuentaPUC = "22" And strGrupoCuentaPUC = "05" Then
                    ' No. Liquidación
                    strFila += Formatear_Campo_SIIGO(lonNumeDocuLiqu, 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("0", 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                End If

                strFila += Formatear_Campo_SIIGO(intSecuencia, 3, CAMPO_NUMERICO_SIIGO) '36- Secuencia Documento Cruce
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '37-Año Vencimiento
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '38-Mes Vencimiento
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '39-Dia Vencimiento
                strFila += Formatear_Campo_SIIGO("0", 4, CAMPO_NUMERICO_SIIGO) '40-Forma Pago
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '41-Código Banco

                stwStreamWriter.WriteLine(strFila)

            End While
            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
                Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
            End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally
            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: JESUS CUADROS
    ' Fecha Creación: 07-JUN-2019
    ' Obsevaciones Creación: Se realiza la implementación con base en la especificación entregada por INTEGRA el 25-ABR-2019
    ' Autor: JESUS CUADROS
    ' Fecha Modificación: 24-JUL-2019
    ' Observaciones Modificación: Se realizan modificaciones con base en la especificación entregada por INTEGRA el 08-JUL-2019
    ' Autor: JESUS CUADROS
    ' Fecha Modificación: 15-AGO-2019
    ' Observaciones Modificación: Se hace un manejo para las cuentas PUC mal parametrizadas
    ' Autor: JESUS CUADROS
    ' Fecha Modificación: 17-SEP-2019
    ' Observaciones Modificación: Se adiciona la consulta del Tenedor de la Planilla para reportarla como Tercero
    Private Function Generar_Documento_Comprobante_Egreso_SIIGO() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, lonNumePlan As Long, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String
        Dim dblValoDebi As Double, dblValoCred As Double, dblValoBase As Double
        Dim strCodigoCuentaPUC As String, strClaseCuentaPUC As String, strGrupoCuentaPUC As String
        Dim strCuentaPUC As String, strSubCuenta As String, lonCodiEgre As Long, strObservaciones As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            strSQL = "SELECT ENCC.EMPR_Codigo, ENCC.TIDO_Codigo, ENCC.Numero, ENCC.TIDO_Documento, ENCC.Numero_Documento,"
            strSQL += "ENCC.Fecha_Documento, ENCC.TERC_Documento, ENCC.Año, ENCC.Periodo, ENCC.OFIC_Codigo,"
            strSQL += "ENCC.Valor_Comprobante, ENCC.Fecha, ENCC.CATA_ESIC_Codigo, ENCC.Anulado,"
            strSQL += "DECC.ID, DECC.PLUC_Codigo, DECC.TERC_Codigo, DECC.Valor_Base, DECC.Valor_Debito, DECC.Valor_Credito, DECC.Observaciones,"
            strSQL += "PLUC.Codigo_Cuenta, TERC.Numero_Identificacion, ENCC.Codigo_Documento"
            strSQL += " FROM Encabezado_Comprobante_Contables As ENCC, Detalle_Comprobante_Contables As DECC, Plan_Unico_Cuentas AS PLUC, Terceros AS TERC"
            strSQL += " WHERE ENCC.EMPR_Codigo = DECC.EMPR_Codigo"
            strSQL += " AND ENCC.Numero  = DECC.ENCC_Numero"
            ' Plan Unico Cuentas
            strSQL += " AND DECC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND DECC.PLUC_Codigo  = PLUC.Codigo"
            ' Terceros
            strSQL += " AND DECC.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND DECC.TERC_Codigo  = TERC.Codigo"

            strSQL += " AND ENCC.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ENCC.Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND ENCC.Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
            strSQL += " AND ENCC.TIDO_Documento = " & TIDO_COMPROBANTE_EGRESO
            strSQL += " AND ENCC.CATA_ESIC_Codigo = " & ESTADO_INTERFAZ_PENDIENTE
            strSQL += " AND ENCC.Anulado = 0"
            strSQL += " ORDER BY ENCC.Numero_Documento"

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            While sdrArchInte.Read

                If lonNumeroComprobante = Val(sdrArchInte("Numero_Documento")) Then
                    intSecuencia += 1
                Else
                    lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                    intSecuencia = 1
                End If
                lonCodiEgre = sdrArchInte("Codigo_Documento") ' Código Comprobante Egreso

                dblValoDebi = sdrArchInte("Valor_Debito")
                dblValoCred = sdrArchInte("Valor_Credito")
                dblValoBase = sdrArchInte("Valor_Base")

                strFila = Formatear_Campo_SIIGO(TIPO_COMPROBANTE_EGRESOS, 1, CAMPO_ALFANUMERICO_SIIGO) '1-Tipo Comprobante
                strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_EGRESO, 3, CAMPO_NUMERICO_SIIGO) '2-Codigo Comprobante
                strFila += Formatear_Campo_SIIGO(lonNumeroComprobante, 11, CAMPO_NUMERICO_SIIGO) '3-No Documento
                strFila += Formatear_Campo_SIIGO(intSecuencia, 5, CAMPO_NUMERICO_SIIGO) ' 4- Secuencia

                lonRes = Retorna_Planilla_Comprobante_Egreso(lonCodiEgre, lonNumePlan)
                lonRes = Retorna_Tenedor_Planilla(lonNumePlan, strIdenTene)

                strFila += Formatear_Campo_SIIGO(strIdenTene, 13, CAMPO_NUMERICO_SIIGO) '5-Identificacion Tenedor
                strFila += Formatear_Campo_SIIGO("0", 3, CAMPO_NUMERICO_SIIGO) '6-Sucursal

                ' Código Cuenta Contable
                strCodigoCuentaPUC = sdrArchInte("Codigo_Cuenta") ' 13300501

                ' TEMPORAL: 15-AGO-2019 Por problemas Parametrización se cambia cuenta 13300501 y 13050501 por 13300500 ya que estaba mal parametrizada en el Concepto
                If strCodigoCuentaPUC = "13300501" Or strCodigoCuentaPUC = "13050501" Then
                    strCodigoCuentaPUC = "13300500"
                End If

                strClaseCuentaPUC = Mid$(strCodigoCuentaPUC, 1, 2)
                strFila += Formatear_Campo_SIIGO(strClaseCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '7 - 13
                strGrupoCuentaPUC = Mid$(strCodigoCuentaPUC, 3, 2)
                strFila += Formatear_Campo_SIIGO(strGrupoCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '8 - 30
                strCuentaPUC = Mid$(strCodigoCuentaPUC, 5, 2)
                strFila += Formatear_Campo_SIIGO(strCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '9 - 05
                strSubCuenta = Mid$(strCodigoCuentaPUC, 7, 2)
                strFila += Formatear_Campo_SIIGO(strSubCuenta, 2, CAMPO_NUMERICO_SIIGO) '10 - 01
                strFila += Formatear_Campo_SIIGO("00", 2, CAMPO_NUMERICO_SIIGO) '11 - 00

                ' Producto Transporte Carretera
                ' 300-Linea
                ' 0001-Grupo
                ' 000001-Elemento
                strFila += Formatear_Campo_SIIGO("300", 3, CAMPO_NUMERICO_SIIGO) '12
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '13
                strFila += Formatear_Campo_SIIGO("1", 6, CAMPO_NUMERICO_SIIGO) '14

                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '15-Año
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '16-Mes
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '17-Dia
                strFila += Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '18-Centro Costo
                strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '19-Sub Centro Costo

                ' Consultar en el Comprobante Egreso el documento origen que en este caso es No. Planilla Despacho
                strObservaciones = "PLANILLA No. " & lonNumePlan.ToString
                ' sdrArchInte("Observaciones")
                strFila += Formatear_Campo_SIIGO(strObservaciones, 50, CAMPO_ALFANUMERICO_SIIGO) '20-Descripción

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO("D", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoDebi, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                Else
                    strFila += Formatear_Campo_SIIGO("C", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoCred, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                End If
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '23-Valor Decimales
                strFila += Formatear_Campo_SIIGO(dblValoBase, 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '25-Valor Base Retención Decimales

                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '26-Codigo Vendedor 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '27-Codigo Ciudad - FALTA
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '28-Codigo Zona 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '29-Codigo Bodega
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '30-Codigo Ubicación
                strFila += Formatear_Campo_SIIGO("0", 10, CAMPO_NUMERICO_SIIGO) '31-Cantidad
                strFila += Formatear_Campo_SIIGO("0", 5, CAMPO_NUMERICO_SIIGO) '32-Cantidad Decimales

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO(TIPO_COMPROBANTE_EGRESOS, 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_EGRESO, 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("", 1, CAMPO_NUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                End If

                If strClaseCuentaPUC = "13" And strGrupoCuentaPUC = "30" Then ' Si cuenta PUC es 1330 reportar en documento cruce el numero de comprobante
                    strFila += Formatear_Campo_SIIGO(lonNumeroComprobante, 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("0", 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                End If

                strFila += Formatear_Campo_SIIGO(intSecuencia, 3, CAMPO_NUMERICO_SIIGO) '36- Secuencia Documento Cruce
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '37-Año Vencimiento
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '38-Mes Vencimiento
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '39-Dia Vencimiento
                strFila += Formatear_Campo_SIIGO("0", 4, CAMPO_NUMERICO_SIIGO) '40-Forma Pago
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '41-Código Banco

                stwStreamWriter.WriteLine(strFila)

            End While
            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
                Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
            End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function


    ' Autor: JESUS CUADROS
    ' Fecha Creación: 08-AGO-2019
    ' Obsevaciones Creación: Se copia la misma rutina de egresos
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Function Generar_Documento_Comprobante_Ingreso_SIIGO() As Boolean
        Dim ComandoSQL As SqlCommand
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String
        Dim dblValoDebi As Double, dblValoCred As Double, dblValoBase As Double
        Dim strCodigoCuentaPUC As String, strClaseCuentaPUC As String, strGrupoCuentaPUC As String
        Dim strCuentaPUC As String, strSubCuenta As String, lonDocuOrig As Long, lonCodiEgre As Long

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            strSQL = "SELECT ENCC.EMPR_Codigo, ENCC.TIDO_Codigo, ENCC.Numero, ENCC.TIDO_Documento, ENCC.Numero_Documento,"
            strSQL += "ENCC.Fecha_Documento, ENCC.TERC_Documento, ENCC.Año, ENCC.Periodo, ENCC.OFIC_Codigo,"
            strSQL += "ENCC.Valor_Comprobante, ENCC.Fecha, ENCC.CATA_ESIC_Codigo, ENCC.Anulado,"
            strSQL += "DECC.ID, DECC.PLUC_Codigo, DECC.TERC_Codigo, DECC.Valor_Base, DECC.Valor_Debito, DECC.Valor_Credito, DECC.Observaciones,"
            strSQL += "PLUC.Codigo_Cuenta, TERC.Numero_Identificacion, ENCC.Codigo_Documento"
            strSQL += " FROM Encabezado_Comprobante_Contables As ENCC, Detalle_Comprobante_Contables As DECC, Plan_Unico_Cuentas AS PLUC, Terceros AS TERC"
            strSQL += " WHERE ENCC.EMPR_Codigo = DECC.EMPR_Codigo"
            strSQL += " AND ENCC.Numero  = DECC.ENCC_Numero"
            ' Plan Unico Cuentas
            strSQL += " AND DECC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND DECC.PLUC_Codigo  = PLUC.Codigo"
            ' Terceros
            strSQL += " AND DECC.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND DECC.TERC_Codigo  = TERC.Codigo"

            strSQL += " AND ENCC.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ENCC.Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND ENCC.Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
            strSQL += " AND ENCC.TIDO_Documento = " & TIDO_COMPROBANTE_INGRESO
            strSQL += " AND ENCC.CATA_ESIC_Codigo = " & ESTADO_INTERFAZ_PENDIENTE
            strSQL += " AND ENCC.Anulado = 0"
            strSQL += " ORDER BY ENCC.Numero_Documento"

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            While sdrArchInte.Read

                If lonNumeroComprobante = Val(sdrArchInte("Numero_Documento")) Then
                    intSecuencia += 1
                Else
                    lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                    intSecuencia = 1
                End If
                lonCodiEgre = sdrArchInte("Codigo_Documento") ' Código Comprobante Ingreso

                dblValoDebi = sdrArchInte("Valor_Debito")
                dblValoCred = sdrArchInte("Valor_Credito")
                dblValoBase = sdrArchInte("Valor_Base")

                strFila = Formatear_Campo_SIIGO(TIPO_COMPROBANTE_INGRESOS, 1, CAMPO_ALFANUMERICO_SIIGO) '1-Tipo Comprobante
                strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_INGRESO, 3, CAMPO_NUMERICO_SIIGO) '2-Codigo Comprobante
                strFila += Formatear_Campo_SIIGO(lonNumeroComprobante, 11, CAMPO_NUMERICO_SIIGO) '3-No Documento
                strFila += Formatear_Campo_SIIGO(intSecuencia, 5, CAMPO_NUMERICO_SIIGO) ' 4- Secuencia
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Numero_Identificacion"), 13, CAMPO_NUMERICO_SIIGO) '5-Identificacion
                strFila += Formatear_Campo_SIIGO("0", 3, CAMPO_NUMERICO_SIIGO) '6-Sucursal

                ' Código Cuenta Contable
                strCodigoCuentaPUC = sdrArchInte("Codigo_Cuenta") ' 1305

                strClaseCuentaPUC = Mid$(strCodigoCuentaPUC, 1, 2)
                strFila += Formatear_Campo_SIIGO(strClaseCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '7 - 13
                strGrupoCuentaPUC = Mid$(strCodigoCuentaPUC, 3, 2)
                strFila += Formatear_Campo_SIIGO(strGrupoCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '8 - 30
                strCuentaPUC = Mid$(strCodigoCuentaPUC, 5, 2)
                strFila += Formatear_Campo_SIIGO(strCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '9 - 05
                strSubCuenta = Mid$(strCodigoCuentaPUC, 7, 2)
                strFila += Formatear_Campo_SIIGO(strSubCuenta, 2, CAMPO_NUMERICO_SIIGO) '10 - 01
                strFila += Formatear_Campo_SIIGO("00", 2, CAMPO_NUMERICO_SIIGO) '11 - 00

                ' Producto Transporte Carretera
                ' 300-Linea
                ' 0001-Grupo
                ' 000001-Elemento
                strFila += Formatear_Campo_SIIGO("300", 3, CAMPO_NUMERICO_SIIGO) '12
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '13
                strFila += Formatear_Campo_SIIGO("1", 6, CAMPO_NUMERICO_SIIGO) '14

                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '15-Año
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '16-Mes
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '17-Dia
                strFila += Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '18-Centro Costo
                strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '19-Sub Centro Costo
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Observaciones"), 50, CAMPO_ALFANUMERICO_SIIGO) '20-Descripción

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO("D", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoDebi, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                Else
                    strFila += Formatear_Campo_SIIGO("C", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoCred, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                End If
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '23-Valor Decimales
                strFila += Formatear_Campo_SIIGO(dblValoBase, 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '25-Valor Base Retención Decimales

                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '26-Codigo Vendedor 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '27-Codigo Ciudad - FALTA
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '28-Codigo Zona 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '29-Codigo Bodega
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '30-Codigo Ubicación
                strFila += Formatear_Campo_SIIGO("0", 10, CAMPO_NUMERICO_SIIGO) '31-Cantidad
                strFila += Formatear_Campo_SIIGO("0", 5, CAMPO_NUMERICO_SIIGO) '32-Cantidad Decimales

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO(TIPO_COMPROBANTE_EGRESOS, 1, CAMPO_NUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_EGRESO, 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("", 1, CAMPO_NUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                End If

                If strClaseCuentaPUC = "13" And strGrupoCuentaPUC = "05" Then ' Si cuenta PUC es 1305 reportar en documento cruce el documento origen Ej: Factura

                    ' Consultar en el Comprobante Egreso el documento origen que en este caso es No. Planilla Despacho
                    If objGeneral.Retorna_Campo_BD(intEmpresa, "Encabezado_Documento_Comprobantes", "Documento_Origen", "Codigo", CAMPO_NUMERICO, lonCodiEgre, lonDocuOrig, "TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO) Then
                        strFila += Formatear_Campo_SIIGO(lonDocuOrig, 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                    Else
                        strFila += Formatear_Campo_SIIGO("0", 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                    End If

                Else
                    strFila += Formatear_Campo_SIIGO("0", 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                End If

                strFila += Formatear_Campo_SIIGO(intSecuencia, 3, CAMPO_NUMERICO_SIIGO) '36- Secuencia Documento Cruce
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '37-Año Vencimiento
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '38-Mes Vencimiento
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '39-Dia Vencimiento
                strFila += Formatear_Campo_SIIGO("0", 4, CAMPO_NUMERICO_SIIGO) '40-Forma Pago
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '41-Código Banco

                stwStreamWriter.WriteLine(strFila)

            End While
            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
                Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
            End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally
            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    Private Function Generar_Documento_Legalizacion_Gastos_SIIGO() As Boolean

        Dim ComandoSQL As SqlCommand

        Dim intSecuencia As Integer, lonNumeroLiquidacion As Long, lonCodigoTercero As Long
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter
        Dim strFila As String = "", strCuentaPUCNeto As String = "", strCuentaPUCFletes As String = ""
        Dim dblValCuePorPag As Double = 0, dblValor As Double = 0, strFechaComp As String = ""
        Dim dblValorBase As Double = 0, intAnulado As Integer = 0

        'Si el archivo existe se borra 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If
        Date.TryParse(Request.QueryString("FechInic"), dtaFechaInicial)
        Date.TryParse(Request.QueryString("FechFina"), dtaFechaFinal)
        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, Text.Encoding.Default)

        ' Consultar movimientos contables de liquidación manifiestos
        strSQL = " SELECT DISTINCT EMPR_Codigo, TIDO_Codigo, ENCC_Numero, PLUC_Codigo, ID, "
        strSQL += " TERC_Codigo, Valor_Debito * 100 AS Valor_Debito  , Valor_Credito * 100 AS Valor_Credito , Valor_Base * 100 AS Valor_Base, Observaciones, "
        strSQL += " Centro_Costo, Documento_Cruce, Prefijo, Codigo_Anexo, "
        strSQL += " Sufijo_Codigo_Anexo,   Numero, Fecha, "
        strSQL += " TIDO_Documento, Numero_Documento, Fecha_Documento, TERC_Documento, "
        strSQL += " OFIC_Documento, Ano, Periodo,  ObservacionesEncabezado, "
        strSQL += " Valor_Comprobante, TIDO_Documento_Cruce, Numero_Documento_Cruce, Anulado, Estado, "

        strSQL += " Fecha_Anula,  OFIC_Codigo, NombreCuenta"
        strSQL += " FROM V_Generar_Detalle_Comprobante_Contables "
        strSQL += " WHERE EMPR_Codigo = " & intEmpresa
        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
        strSQL += " AND TIDO_Codigo = " & TIDO_COMPROBANTE_CONTABLE
        strSQL += " AND Interfaz_Contable = " & ARCHIVO_BORRADOR
        strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO
        strSQL += " AND TIDO_Documento_Cruce = " & TIDO_LEGALIZACION_GASTOS
        strSQL += " ORDER BY Numero"

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        ' En R&R la contabilización se hace en el Manifiesto y el ajuste por mayor o menor valor lo manejan con notas contables directamente en SIIGO
        ' En la liquidación solo se contabilizan los conceptos de descuento al crédito y en la cuenta x pagar va el total de estos conceptos al débito
        ' Como es un manejo especial la cxp al débito se ingresa en la generación del archivo plano mediante la rutina Armar_Fila_CxP_Liquidacion_Manifiesto_TRRYR

        intSecuencia = 1
        lonNumeroLiquidacion = 0
        Try
            While sdrArchInte.Read

                If lonNumeroLiquidacion = Val(sdrArchInte("Documento_Cruce")) Then
                    intSecuencia += 1
                Else

                    If strFila <> "" Then
                        ' Insertar Linea Cuenta PUC CxP al débito para finalizar el movimiento
                        'Call Armar_Fila_CxP_Liquidacion_Manifiesto_TRRYR(strFila, dblValCuePorPag, lonNumeroLiquidacion, strFechaComp, intSecuencia)
                        stwStreamWriter.WriteLine(strFila)
                        dblValCuePorPag = 0
                    End If

                    lonNumeroLiquidacion = Val(sdrArchInte("Documento_Cruce"))
                    strFechaComp = sdrArchInte("Fecha_Documento")
                    intSecuencia = 1
                End If

                ' Consultar información adicional tercero
                lonCodigoTercero = sdrArchInte("TERC_Codigo")
                Dim arrCampos(2), arrValoresCampos(2) As String
                arrCampos(0) = "Numero_Identificacion"
                arrCampos(1) = "Digito_Chequeo"

                If Me.objGeneral.Retorna_Campos(intEmpresa, "Terceros", arrCampos, arrValoresCampos, 2, clsGeneral.CAMPO_NUMERICO, "Codigo", lonCodigoTercero, "", Me.strError) Then
                    Me.objTercero.NumeroIdentificacion = arrValoresCampos(0)
                    Me.objTercero.DigitoChequeo = Val(arrValoresCampos(1))
                End If

                strFila = Formatear_Campo_SIIGO(TIPO_COMPROBANTE_LIQUIDACIONES, 1, CAMPO_ALFANUMERICO_SIIGO) '1-Tipo Comprobante Liquidacion
                strFila = strFila & Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_LIQUIDACIONES, 3, CAMPO_NUMERICO_SIIGO) '2-Codigo Comprobante Liquidacion
                strFila = strFila & Formatear_Campo_SIIGO(sdrArchInte("Documento_Cruce"), 11, CAMPO_NUMERICO_SIIGO) '3-No Liquidacion                
                strFila = strFila & Formatear_Campo_SIIGO(intSecuencia.ToString, 5, CAMPO_NUMERICO_SIIGO) '4-Secuencia
                strFila = strFila & Formatear_Campo_SIIGO(Me.objTercero.NumeroIdentificacion, 13, CAMPO_NUMERICO_SIIGO) '5-NIT 

                strFila = strFila & Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '6-Sucursal
                strFila = strFila & Formatear_Campo_SIIGO(sdrArchInte("PLUC_Codigo"), 10, CAMPO_CEROS_DERECHA_SIIGO) '7-Cuenta Contable
                strFila = strFila & Formatear_Campo_SIIGO("", 13, CAMPO_NUMERICO_SIIGO) '8-Codigo Producto
                strFila = strFila & Formatear_Campo_SIIGO(sdrArchInte("Fecha"), 8, CAMPO_FECHA_SIIGO) '9-Fecha Documento
                strFila = strFila & Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '10-Centro Costo

                strFila = strFila & Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '11-Sub Centro de Costo
                strFila = strFila & Formatear_Campo_SIIGO("Lega No. " & sdrArchInte("Documento_Cruce"), 50, CAMPO_ALFANUMERICO_SIIGO) '12-Descripción del movimiento
                If sdrArchInte("Valor_Credito") > 0 Then
                    strFila = strFila & Formatear_Campo_SIIGO(NATURALEZA_CREDITO, 1, CAMPO_ALFANUMERICO_SIIGO) '13-Débito-Crédito
                ElseIf sdrArchInte("Valor_Debito") > 0 Then
                    strFila = strFila & Formatear_Campo_SIIGO(NATURALEZA_DEBITO, 1, CAMPO_ALFANUMERICO_SIIGO) '13-Débito-Crédito
                End If
                If intAnulado = clsGeneral.ESTADO_ANULADO Then
                    dblValor = clsGeneral.CERO
                    dblValorBase = clsGeneral.CERO
                Else
                    dblValor = Val(sdrArchInte("Valor_Debito")) + Val(sdrArchInte("Valor_Credito"))
                End If
                strFila = strFila & Formatear_Campo_SIIGO(dblValor, 15, CAMPO_NUMERICO_SIIGO) '14-Vr movimiento*100 credito
                strFila = strFila & Formatear_Campo_SIIGO("", 15, CAMPO_NUMERICO_SIIGO) '15-Base retencion
                dblValCuePorPag = dblValCuePorPag + dblValor

                strFila = strFila & Formatear_Campo_SIIGO(CODIGO_VENDEDOR, 4, CAMPO_NUMERICO_SIIGO) '16-Codigo Vendedor
                If sdrArchInte("OFIC_Codigo") IsNot DBNull.Value Then
                    strFila = strFila & Formatear_Campo_SIIGO(sdrArchInte("OFIC_Codigo"), 4, CAMPO_NUMERICO_SIIGO) '17-Codigo Ciudad
                Else
                    strFila = strFila & Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '17-Codigo Ciudad
                End If
                strFila = strFila & Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '18-Codigo Zona 
                strFila = strFila & Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '19-Codigo Bodega 
                strFila = strFila & Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '20-Codigo Ubicación

                strFila = strFila & Formatear_Campo_SIIGO("", 15, CAMPO_NUMERICO_SIIGO) '21-Cantidad
                strFila = strFila & Formatear_Campo_SIIGO("", 1, CAMPO_ALFANUMERICO_SIIGO) '22-Tipo Documento Cruce
                strFila = strFila & Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '23-Codigo Comprobante Cruce
                strFila = strFila & Formatear_Campo_SIIGO("", 11, CAMPO_NUMERICO_SIIGO) '24-Numero Documento Cruce
                strFila = strFila & Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '25-Secuencia Docuemento Cruce

                strFila = strFila & Formatear_Campo_SIIGO("", 8, CAMPO_FECHA_SIIGO) '26-Fecha Vencimiento Documento Cruce
                strFila = strFila & Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '27-Codigo Forma de Pago
                strFila = strFila & Formatear_Campo_SIIGO("", 2, CAMPO_NUMERICO_SIIGO) '28-Codigo del banco

                stwStreamWriter.WriteLine(strFila)

            End While



            sdrArchInte.Close()
            sdrArchInte.Dispose()
            stwStreamWriter.Close()
            stmStreamW.Close()

            Me.objGeneral.ConexionSQL.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Colocar_Interfaz_Definitivo("Encabezado_Liquida_Manifiestos", TIDO_LIQUIDA_MANIFIESTO_TERCEROS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Liquidaciones_Manifiesto_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False

        Finally
            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try




    End Function

    ' ITERFAZ CONTABLE UNO EE

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Comprobantes Egreso
    Private Function Generar_Documento_Comprobante_Egreso_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

            strSQL += " AND TIDO_Documento = " & TIDO_COMPROBANTE_EGRESO
            strSQL += " AND Anulado = 0"
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            Dim auxConsecutivo As Integer = 3

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            'Documento INTERFAZ
            strFila = Formatear_Campo_UNO_EE(2, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
            strFila += Formatear_Campo_UNO_EE("1001", 4, CAMPO_ALFANUMERICO) '6-Centro Operacion General

            strFila += Formatear_Campo_UNO_EE(MAESTRO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO_UNO_EE, 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
            strFila += Formatear_Campo_UNO_EE(DateTime.Now.ToString("yyyyMMdd"), 8, CAMPO_ALFANUMERICO) '8-Fecha Generacion Documento

            strFila += Formatear_Campo_UNO_EE("00030", 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '10-Sub Tipo Registro

            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
            strFila += Formatear_Campo_UNO_EE("COMPROBANTEDEEGRESO" & fechaAux, 270, CAMPO_ALFANUMERICO) '11 Observacion
            stwStreamWriter.WriteLine(strFila)

            While sdrArchInte.Read

                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 20, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                strFila += Formatear_Campo_UNO_EE(MAESTRO_FLUJO_EFECTIVO_UNO_EE, 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo

                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 8, CAMPO_NUMERICO) '21-Tipo Documento
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento

                stwStreamWriter.WriteLine(strFila)
                auxConsecutivo += 1

            End While

            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa

            stwStreamWriter.WriteLine(strFila)

            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Comprobantes Egreso
    Private Function Generar_Documento_Cuentas_Pagar_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_CPP_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

            strSQL += " AND TIDO_Documento = " & TIDO_COMPROBANTE_EGRESO
            strSQL += " AND Anulado = 0"
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            Dim auxConsecutivo As Integer = 3

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            'Documento INTERFAZ
            strFila = Formatear_Campo_UNO_EE(2, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
            strFila += Formatear_Campo_UNO_EE("1001", 4, CAMPO_ALFANUMERICO) '6-Centro Operacion General

            strFila += Formatear_Campo_UNO_EE(MAESTRO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO_UNO_EE, 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
            strFila += Formatear_Campo_UNO_EE(DateTime.Now.ToString("yyyyMMdd"), 8, CAMPO_ALFANUMERICO) '8-Fecha Generacion Documento

            strFila += Formatear_Campo_UNO_EE("00030", 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '10-Sub Tipo Registro

            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
            strFila += Formatear_Campo_UNO_EE("CUENTAPORPAGAR" & fechaAux, 270, CAMPO_ALFANUMERICO) '11 Observacion
            stwStreamWriter.WriteLine(strFila)

            While sdrArchInte.Read

                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 20, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos

                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '14-Valor Debito 
                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '15-Valor Credito
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '16-Valor Debito Moneda Alterna 
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Credito Moneda Alterna

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '18-Observaciones
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '19-Sucursal Prooveedor
                strFila += Formatear_Campo_UNO_EE("", 4, CAMPO_ALFANUMERICO) '20-Maestro Prefijo Cruce

                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Documento_Cruce")).ToString, 8, CAMPO_NUMERICO) '21-Documento Cruce
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 3, CAMPO_NUMERICO) '22-Cuota Cruce

                strFila += Formatear_Campo_UNO_EE(MAESTRO_FLUJO_EFECTIVO_UNO_EE, 10, CAMPO_ALFANUMERICO) '23-Maestro Flujo Efectivo

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Formato"), 8, CAMPO_ALFANUMERICO) '24-Fecha
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Formato"), 8, CAMPO_ALFANUMERICO) '25-Fecha Alternativa

                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Descuento Otorgado
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Descuento Aplicado
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Descuento Aplicado Alterno

                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Retencion

                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Retencion Alterna

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Nota"), 255, CAMPO_ALFANUMERICO) '31-Nota

                stwStreamWriter.WriteLine(strFila)
                auxConsecutivo += 1

            End While

            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa

            stwStreamWriter.WriteLine(strFila)

            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Anticipos
    Private Function Generar_Documento_Anticipos_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Anticipos_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

            'strSQL += " AND TIDO_Documento = " & TIDO_COMPROBANTE_EGRESO
            strSQL += " AND Anulado = 0"
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            Dim auxConsecutivo As Integer = 3

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            'Documento INTERFAZ
            strFila = Formatear_Campo_UNO_EE(2, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
            strFila += Formatear_Campo_UNO_EE("1001", 4, CAMPO_ALFANUMERICO) '6-Centro Operacion General

            strFila += Formatear_Campo_UNO_EE(MAESTRO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO_UNO_EE, 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
            strFila += Formatear_Campo_UNO_EE(DateTime.Now.ToString("yyyyMMdd"), 8, CAMPO_ALFANUMERICO) '8-Fecha Generacion Documento

            strFila += Formatear_Campo_UNO_EE("00030", 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '10-Sub Tipo Registro

            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
            strFila += Formatear_Campo_UNO_EE("ANTICIPOS" & fechaAux, 270, CAMPO_ALFANUMERICO) '11 Observacion
            stwStreamWriter.WriteLine(strFila)

            While sdrArchInte.Read

                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos

                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '14-Valor Debito 
                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '15-Valor Credito
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '16-Valor Debito Moneda Alterna 
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Credito Moneda Alterna

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '18-Observaciones
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '19-Sucursal Prooveedor
                strFila += Formatear_Campo_UNO_EE("", 4, CAMPO_ALFANUMERICO) '20-Maestro Prefijo Cruce

                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Documento_Cruce")).ToString, 8, CAMPO_NUMERICO) '21-Documento Cruce
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 3, CAMPO_NUMERICO) '22-Cuota Cruce

                strFila += Formatear_Campo_UNO_EE(MAESTRO_FLUJO_EFECTIVO_UNO_EE, 10, CAMPO_ALFANUMERICO) '23-Maestro Flujo Efectivo

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Formato"), 8, CAMPO_ALFANUMERICO) '24-Fecha
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Formato"), 8, CAMPO_ALFANUMERICO) '25-Fecha Alternativa

                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Descuento Otorgado
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Descuento Aplicado
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Descuento Aplicado Alterno

                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Retencion

                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Retencion Alterna

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Nota"), 255, CAMPO_ALFANUMERICO) '31-Nota

                stwStreamWriter.WriteLine(strFila)
                auxConsecutivo += 1

            End While

            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa

            stwStreamWriter.WriteLine(strFila)

            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Facturas
    'Private Function Generar_Documento_Facturas_UNO_EE() As Boolean
    '    Dim ComandoSQL As SqlCommand, lonRes As Long
    '    Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
    '    Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

    '    ' Si el archivo existe se borra primero 
    '    If File.Exists(Me.strPathArchivo) Then
    '        File.Delete(Me.strPathArchivo)
    '    End If

    '    ' Se abre el archivo 
    '    stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
    '    stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

    '    Try

    '        'Encabezado INTERFAZ
    '        strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
    '        strFila += CERO_STRING.ToString '2-Separador Encabezado
    '        strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
    '        strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
    '        stwStreamWriter.WriteLine(strFila)

    '        Dim auxConsecutivo As Integer = 2

    '        ' Consulta generada para crear los documentos del archivo plano de Facturas UNO EE
    '        strSQL = "SELECT * " & Chr(13)
    '        strSQL += " FROM V_Interfaz_Encabezado_Contable_UNO_EE " & Chr(13)
    '        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

    '        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
    '        strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

    '        strSQL += " AND TIDO_Documento = " & TIDO_FACTURAS
    '        strSQL += " AND Anulado = 0"
    '        If intOficina > 0 Then
    '            strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
    '        End If

    '        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
    '        Me.objGeneral.ConexionSQL.Open()
    '        Dim sdrDocuInte As SqlDataReader = ComandoSQL.ExecuteReader

    '        Dim dtsDocuInte As DataTable = New DataTable()
    '        dtsDocuInte.Load(sdrDocuInte)

    '        sdrDocuInte.Close()

    '        Me.objGeneral.ConexionSQL.Close()
    '        For Each row As DataRow In dtsDocuInte.Rows
    '            Dim DocuNumero As String = CStr(row("Numero_Documento"))

    '            'Documento INTERFAZ
    '            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
    '            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
    '            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
    '            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
    '            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
    '            strFila += Formatear_Campo_UNO_EE(1, 1, CAMPO_NUMERICO) '6-Tipo Consecutivo
    '            strFila += Formatear_Campo_UNO_EE(CStr(row("Centro_Operacion")), 3, CAMPO_ALFANUMERICO) '7-Centro Operacion General

    '            strFila += Formatear_Campo_UNO_EE(CStr(row("Tipo_Documento")), 3, CAMPO_ALFANUMERICO) '8-Tipo Documento
    '            strFila += Formatear_Campo_UNO_EE(CStr(row("Numero_Documento")), 8, CAMPO_NUMERICO) '9-Numero Documento
    '            strFila += Formatear_Campo_UNO_EE(CStr(row("Fecha_Documento")), 8, CAMPO_ALFANUMERICO) '10-Fecha Generacion Documento
    '            strFila += Formatear_Campo_UNO_EE(CStr(row("Identificacion_Tercero")), 15, CAMPO_ALFANUMERICO) '11-Identificacion Tercero
    '            strFila += Formatear_Campo_UNO_EE("00030", 5, CAMPO_ALFANUMERICO) '12-Maestro Codigo Cuenta Contable
    '            strFila += Formatear_Campo_UNO_EE(CStr(row("Concepto_Anula")), 1, CAMPO_NUMERICO) '13-Estado
    '            strFila += Formatear_Campo_UNO_EE(1, 1, CAMPO_NUMERICO) '14-Impreso

    '            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
    '            strFila += Formatear_Campo_UNO_EE(MAESTRO_ENCABEZADO_FACTURA_VENTA_UNO_EE & fechaAux, 255, CAMPO_ALFANUMERICO) '15 Observacion
    '            stwStreamWriter.WriteLine(strFila)

    '            auxConsecutivo += 1

    '            ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
    '            strSQL = "SELECT * " & Chr(13)
    '            strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
    '            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

    '            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
    '            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

    '            strSQL += " AND TIDO_Documento = " & TIDO_FACTURAS
    '            strSQL += " AND Anulado = 0"
    '            If intOficina > 0 Then
    '                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
    '            End If
    '            strSQL += " AND Numero_Documento = " & DocuNumero

    '            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
    '            Me.objGeneral.ConexionSQL.Open()
    '            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

    '            While sdrArchInte.Read
    '                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
    '                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
    '                If sdrArchInte("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXC_ALTERNATIVA_LOGISTICA Then ' Movimiento Contable
    '                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
    '                Else
    '                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_CXC_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
    '                End If
    '                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

    '                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
    '                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
    '                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

    '                lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
    '                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

    '                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
    '                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
    '                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
    '                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
    '                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
    '                If sdrArchInte("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXC_ALTERNATIVA_LOGISTICA Then ' Movimiento Contable
    '                    strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo

    '                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
    '                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
    '                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
    '                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
    '                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

    '                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
    '                    'strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 8, CAMPO_NUMERICO) '21-Tipo Documento
    '                    strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
    '                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento

    '                Else ' Cuenta Por Cobrar
    '                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
    '                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
    '                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
    '                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna

    '                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '19 Observaciones
    '                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '20- Sucursal
    '                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '21-Tipo Documento Cruce

    '                    lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
    '                    strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '22-No Documento Cruce
    '                    strFila += Formatear_Campo_UNO_EE(1, 3, CAMPO_NUMERICO) '23-Cuota
    '                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '24-Fecha Vence
    '                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '25-Fecha Dcto

    '                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Valor Credito Moneda Alterna
    '                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Valor Credito Moneda Alterna
    '                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Valor Credito Moneda Alterna
    '                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Valor Credito Moneda Alterna
    '                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Valor Credito Moneda Alterna
    '                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '31-Valor Credito Moneda Alterna
    '                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '32-Valor Credito Moneda Alterna

    '                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("EMPR_Numero_Identificacion")).ToString, 15, CAMPO_ALFANUMERICO) '33-Documento Tercero
    '                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '34-Observaciones
    '                End If

    '                stwStreamWriter.WriteLine(strFila)
    '                'sdrArchInte.Close()
    '                'Me.objGeneral.ConexionSQL.Close()
    '                auxConsecutivo += 1

    '            End While
    '            sdrArchInte.Close()
    '            Me.objGeneral.ConexionSQL.Close()

    '        Next

    '        'Pie de Pagina INTERFAZ
    '        strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
    '        strFila += "9999" '2-Separador Encabezado
    '        strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
    '        strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
    '        stwStreamWriter.WriteLine(strFila)

    '        stwStreamWriter.Close()
    '        stmStreamW.Close()

    '        'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
    '        '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
    '        'End If

    '        Return True

    '    Catch ex As Exception
    '        stwStreamWriter.Close()
    '        stmStreamW.Close()
    '        Me.objGeneral.ConexionSQL.Close()
    '        Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
    '        Return False
    '    Finally

    '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
    '            Me.objGeneral.ConexionSQL.Close()
    '        End If
    '    End Try
    'End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Facturas
    Private Function Generar_Documento_Facturas_UNO_EE_OLD() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            Dim auxConsecutivo As Integer = 2

            ' Consulta generada para crear los documentos del archivo plano de Facturas UNOEE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Factura_Contable_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND TIDO_Documento = " & TIDO_FACTURAS
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If
            'strSQL += " AND Numero >= 66156" ' Encabezado_Comprobante_Contables.Numero
            strSQL += " AND Numero_Documento IN (30217, 30218, 10527, 30219, 10528, 10529)"

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrDocuFact As SqlDataReader = ComandoSQL.ExecuteReader

            Dim dtsDocuFact As DataTable = New DataTable()
            dtsDocuFact.Load(sdrDocuFact)

            sdrDocuFact.Close()
            Me.objGeneral.ConexionSQL.Close()

            If dtsDocuFact.Rows.Count > 0 Then
                For Each rowFact As DataRow In dtsDocuFact.Rows

                    If rowFact("Numero") = 66156 Then

                        strSQL = ""
                        strSQL = ""

                    End If

                    ' Consulta generada para crear los documentos del archivo plano de Facturas UNO EE
                    strSQL = "SELECT * " & Chr(13)
                    strSQL += " FROM V_Interfaz_Encabezado_Contable_UNO_EE " & Chr(13)
                    strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

                    strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
                    strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

                    strSQL += " AND TIDO_Documento = " & TIDO_FACTURAS
                    strSQL += " AND Anulado = 0"
                    strSQL += " AND Numero = " & CStr(rowFact("Numero"))

                    strSQL += " AND Numero_Documento = " & CStr(rowFact("Numero_Documento"))

                    If intOficina > 0 Then
                        strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
                    End If

                    ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                    Me.objGeneral.ConexionSQL.Open()
                    Dim sdrDocuInte As SqlDataReader = ComandoSQL.ExecuteReader

                    Dim dtsDocuInte As DataTable = New DataTable()
                    dtsDocuInte.Load(sdrDocuInte)

                    sdrDocuInte.Close()
                    Me.objGeneral.ConexionSQL.Close()

                    If dtsDocuInte.Rows.Count > 0 Then

                        For Each row As DataRow In dtsDocuInte.Rows
                            Dim DocuNumero As String = CStr(row("Numero_Documento"))
                            Dim DocuNumeroInterno As String = CStr(row("Codigo_Documento"))
                            Dim DocuMovim As String = CStr(row("Numero"))

                            ' Documento INTERFAZ
                            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
                            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 1, CAMPO_ALFANUMERICO) '6-Tipo Consecutivo
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Centro_Operacion")), 3, CAMPO_ALFANUMERICO) '7-Centro Operacion General

                            strFila += Formatear_Campo_UNO_EE(CStr(row("Tipo_Documento")), 3, CAMPO_ALFANUMERICO) '8-Tipo Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Numero_Documento")), 8, CAMPO_NUMERICO) '9-Numero Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Fecha_Documento")), 8, CAMPO_ALFANUMERICO) '10-Fecha Generacion Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Identificacion_Tercero")), 15, CAMPO_ALFANUMERICO) '11-Identificacion Tercero
                            strFila += Formatear_Campo_UNO_EE("00030", 5, CAMPO_ALFANUMERICO) '12-Maestro Codigo Cuenta Contable
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Concepto_Anula")), 1, CAMPO_NUMERICO) '13-Estado
                            strFila += Formatear_Campo_UNO_EE(1, 1, CAMPO_NUMERICO) '14-Impreso

                            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
                            strFila += Formatear_Campo_UNO_EE(MAESTRO_ENCABEZADO_FACTURA_VENTA_UNO_EE & fechaAux, 255, CAMPO_ALFANUMERICO) '15 Observacion
                            stwStreamWriter.WriteLine(strFila)

                            auxConsecutivo += 1

                            'Consulta detalle movimientos contable 
                            strSQL = "SELECT * " & Chr(13)
                            strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
                            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

                            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
                            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

                            strSQL += " AND TIDO_Documento = " & TIDO_FACTURAS
                            strSQL += " AND Anulado = 0"
                            If intOficina > 0 Then
                                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
                            End If
                            strSQL += " AND Numero_Documento = " & DocuNumero
                            strSQL += " AND Numero = " & DocuMovim

                            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                            Me.objGeneral.ConexionSQL.Open()
                            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

                            Dim dtsArchInte As DataTable = New DataTable()
                            dtsArchInte.Load(sdrArchInte)
                            sdrArchInte.Close()
                            Me.objGeneral.ConexionSQL.Close()
                            'Consulta detalle movimientos contable
                            'Consulta detalle Remesas Factura
                            strSQL = "SELECT * FROM V_Detalle_Interfaz_Movimiento_Contable_Factura_UNO_EE" & Chr(13)
                            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & " AND ENFA_Numero = " & DocuNumeroInterno

                            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                            Me.objGeneral.ConexionSQL.Open()
                            Dim sdrRemesas As SqlDataReader = ComandoSQL.ExecuteReader

                            Dim dtsRemesas As DataTable = New DataTable()
                            dtsRemesas.Load(sdrRemesas)
                            sdrRemesas.Close()
                            Me.objGeneral.ConexionSQL.Close()
                            'Consulta detalle Remesas Factura

                            If dtsArchInte.Rows.Count > 0 Then ' Verifica si existe detalle de movimientos
                                For Each detmov As DataRow In dtsArchInte.Rows
                                    If dtsRemesas.Rows.Count > 0 And (CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Or CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_INTERMEDIACION_ALTERNATIVA_LOGISTICA) Then ' Verifica si existe un detalle de remesas y si la cuenta aplica
                                        For Each detrem As DataRow In dtsRemesas.Rows
                                            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                                            strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                                            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                                            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                                            strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                            strFila += Formatear_Campo_UNO_EE(detmov("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
                                            lonNumeroComprobante = Val(detmov("Numero_Documento"))
                                            strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento
                                            strFila += Formatear_Campo_UNO_EE(detmov("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                                            strFila += Formatear_Campo_UNO_EE(detmov("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                                            'strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                            If detrem("Centro_Operacion").ToString <> "" Then
                                                strFila += Formatear_Campo_UNO_EE(detrem("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                            Else
                                                strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                            End If
                                            strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                            strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                                            strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo
                                            'strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                            If Val(detmov("Valor_Debito")) > 0 Then
                                                If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                                    strFila += Formatear_Campo_UNO_EE(Val(detrem("Total_Flete_Transportador")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                                Else
                                                    strFila += Formatear_Campo_UNO_EE(Val(detrem("Intermediacion")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                                End If
                                            Else
                                                strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                            End If
                                            'strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                            If Val(detmov("Valor_Credito")) > 0 Then
                                                If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                                    strFila += Formatear_Campo_UNO_EE(Val(detrem("Total_Flete_Transportador")).ToString, 21, CAMPO_DINERO) '16-Valor Credito 
                                                Else
                                                    strFila += Formatear_Campo_UNO_EE(Val(detrem("Intermediacion")).ToString, 21, CAMPO_DINERO) '16-Valor Credito 
                                                End If
                                            Else
                                                strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                            End If
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                                            'strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base
                                            If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                                strFila += Formatear_Campo_UNO_EE(Val(detrem("Total_Flete_Transportador")).ToString, 21, CAMPO_DINERO) '19-Valor Base
                                            Else
                                                strFila += Formatear_Campo_UNO_EE(Val(detrem("Intermediacion")).ToString, 21, CAMPO_DINERO) '19-Valor Base 
                                            End If
                                            strFila += Formatear_Campo_UNO_EE(detmov("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                                            strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
                                            Dim observaciones As String = ""
                                            If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                                observaciones = "COSTO" & detrem("Numero_Documento").ToString
                                            Else
                                                observaciones = "INTERMEDIACIÓN" & detrem("Numero_Documento").ToString
                                            End If
                                            strFila += Formatear_Campo_UNO_EE(detmov("Observaciones") & observaciones, 255, CAMPO_ALFANUMERICO) '22-Tipo Documento
                                            stwStreamWriter.WriteLine(strFila)
                                            auxConsecutivo += 1
                                        Next
                                    Else
                                        strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                                        strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                                        If detmov("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXC_ALTERNATIVA_LOGISTICA Then ' Movimiento Contable
                                            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                        Else
                                            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_CXC_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                        End If
                                        strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                                        strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                                        strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                        strFila += Formatear_Campo_UNO_EE(detmov("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                                        lonNumeroComprobante = Val(detmov("Numero_Documento"))
                                        strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                                        strFila += Formatear_Campo_UNO_EE(detmov("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                                        strFila += Formatear_Campo_UNO_EE(detmov("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                                        strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                        strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                        strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos

                                        If detmov("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXC_ALTERNATIVA_LOGISTICA Then ' Movimiento Contable

                                            strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo
                                            strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                            strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

                                            strFila += Formatear_Campo_UNO_EE(detmov("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                                            'strFila += Formatear_Campo_UNO_EE(detmov("Codigo_Cuenta"), 8, CAMPO_NUMERICO) '21-Tipo Documento
                                            strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
                                            strFila += Formatear_Campo_UNO_EE(detmov("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento

                                        Else
                                            'Cuenta Por Cobrar
                                            strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                            strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna

                                            strFila += Formatear_Campo_UNO_EE(detmov("Observaciones"), 255, CAMPO_ALFANUMERICO) '19 Observaciones
                                            strFila += Formatear_Campo_UNO_EE("001", 3, CAMPO_ALFANUMERICO) '20- Sucursal
                                            strFila += Formatear_Campo_UNO_EE(detmov("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '21-Tipo Documento Cruce

                                            lonNumeroComprobante = Val(detmov("Numero_Documento"))
                                            strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '22-No Documento Cruce
                                            strFila += Formatear_Campo_UNO_EE(1, 3, CAMPO_NUMERICO) '23-Cuota
                                            strFila += Formatear_Campo_UNO_EE(detmov("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '24-Fecha Vence
                                            strFila += Formatear_Campo_UNO_EE(detmov("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '25-Fecha Dcto

                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '31-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '32-Valor Credito Moneda Alterna

                                            strFila += Formatear_Campo_UNO_EE(Val(detmov("EMPR_Numero_Identificacion")).ToString, 15, CAMPO_ALFANUMERICO) '33-Documento Tercero
                                            strFila += Formatear_Campo_UNO_EE(detmov("Observaciones"), 255, CAMPO_ALFANUMERICO) '34-Observaciones

                                        End If
                                        stwStreamWriter.WriteLine(strFila)
                                        auxConsecutivo += 1
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If

            ' Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 03-Mayo-2021
    ' Observaciones: Se modifico la rutina incial de Santiago Correa porque utilizaba una consulta inicial que no era requerida y que ademas no generaba la información correcta

    Private Function Generar_Documento_Facturas_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            Dim auxConsecutivo As Integer = 2

            ' Consulta generada para crear los documentos del archivo plano de Facturas UNO EE
            strSQL = "SELECT * "
            strSQL += " FROM V_Interfaz_Encabezado_Contable_UNO_EE "
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString
            strSQL += " AND TIDO_Documento = " & TIDO_FACTURAS
            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= DATEADD(DAY, 1, CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "
            strSQL += " AND Anulado = 0" 'ENCC.Anulado
            strSQL += " AND Anulado_Factura = 0"
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrDocuInte As SqlDataReader = ComandoSQL.ExecuteReader

            Dim dtsDocuInte As DataTable = New DataTable()
            dtsDocuInte.Load(sdrDocuInte)

            sdrDocuInte.Close()
            Me.objGeneral.ConexionSQL.Close()

            If dtsDocuInte.Rows.Count > 0 Then

                For Each row As DataRow In dtsDocuInte.Rows
                    Dim DocuNumero As String = CStr(row("Numero_Documento"))
                    Dim DocuNumeroInterno As String = CStr(row("Codigo_Documento"))
                    Dim DocuMovim As String = CStr(row("Numero"))

                    ' Documento INTERFAZ
                    strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                    strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                    strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                    strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 1, CAMPO_ALFANUMERICO) '6-Tipo Consecutivo
                    strFila += Formatear_Campo_UNO_EE(CStr(row("Centro_Operacion")), 3, CAMPO_ALFANUMERICO) '7-Centro Operacion General

                    strFila += Formatear_Campo_UNO_EE(CStr(row("Tipo_Documento")), 3, CAMPO_ALFANUMERICO) '8-Tipo Documento
                    strFila += Formatear_Campo_UNO_EE(CStr(row("Numero_Documento")), 8, CAMPO_NUMERICO) '9-Numero Documento
                    strFila += Formatear_Campo_UNO_EE(CStr(row("Fecha_Documento")), 8, CAMPO_ALFANUMERICO) '10-Fecha Generacion Documento
                    strFila += Formatear_Campo_UNO_EE(CStr(row("Identificacion_Tercero")), 15, CAMPO_ALFANUMERICO) '11-Identificacion Tercero
                    strFila += Formatear_Campo_UNO_EE("00030", 5, CAMPO_ALFANUMERICO) '12-Maestro Codigo Cuenta Contable
                    strFila += Formatear_Campo_UNO_EE(CStr(row("Concepto_Anula")), 1, CAMPO_NUMERICO) '13-Estado
                    strFila += Formatear_Campo_UNO_EE(1, 1, CAMPO_NUMERICO) '14-Impreso

                    Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
                    strFila += Formatear_Campo_UNO_EE(MAESTRO_ENCABEZADO_FACTURA_VENTA_UNO_EE & fechaAux, 255, CAMPO_ALFANUMERICO) '15 Observacion
                    stwStreamWriter.WriteLine(strFila)

                    auxConsecutivo += 1

                    'Consulta detalle movimientos contable 
                    strSQL = "SELECT * " & Chr(13)
                    strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
                    strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)
                    strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
                    strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "
                    strSQL += " AND TIDO_Documento = " & TIDO_FACTURAS
                    strSQL += " AND Anulado = 0"
                    If intOficina > 0 Then
                        strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
                    End If
                    strSQL += " AND Numero_Documento = " & DocuNumero
                    strSQL += " AND Numero = " & DocuMovim

                    ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                    Me.objGeneral.ConexionSQL.Open()
                    Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

                    Dim dtsArchInte As DataTable = New DataTable()
                    dtsArchInte.Load(sdrArchInte)
                    sdrArchInte.Close()
                    Me.objGeneral.ConexionSQL.Close()

                    'Consulta detalle movimientos contable
                    'Consulta detalle Remesas Factura
                    strSQL = "SELECT * FROM V_Detalle_Interfaz_Movimiento_Contable_Factura_UNO_EE" & Chr(13)
                    strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & " AND ENFA_Numero = " & DocuNumeroInterno

                    ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                    Me.objGeneral.ConexionSQL.Open()
                    Dim sdrRemesas As SqlDataReader = ComandoSQL.ExecuteReader

                    Dim dtsRemesas As DataTable = New DataTable()
                    dtsRemesas.Load(sdrRemesas)
                    sdrRemesas.Close()
                    Me.objGeneral.ConexionSQL.Close()
                    'Consulta detalle Remesas Factura

                    If dtsArchInte.Rows.Count > 0 Then ' Verifica si existe detalle de movimientos
                        For Each detmov As DataRow In dtsArchInte.Rows
                            If dtsRemesas.Rows.Count > 0 And (CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Or CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_INTERMEDIACION_ALTERNATIVA_LOGISTICA) Then ' Verifica si existe un detalle de remesas y si la cuenta aplica
                                For Each detrem As DataRow In dtsRemesas.Rows
                                    strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                                    strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                    strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                                    strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                                    strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                    strFila += Formatear_Campo_UNO_EE(detmov("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
                                    lonNumeroComprobante = Val(detmov("Numero_Documento"))
                                    strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento
                                    strFila += Formatear_Campo_UNO_EE(detmov("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                                    strFila += Formatear_Campo_UNO_EE(detmov("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                                    'strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                    If detrem("Centro_Operacion").ToString <> "" Then
                                        strFila += Formatear_Campo_UNO_EE(detrem("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                    Else
                                        strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                    End If
                                    strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                    strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                                    strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo
                                    'strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    If Val(detmov("Valor_Debito")) > 0 Then
                                        If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                            strFila += Formatear_Campo_UNO_EE(Val(detrem("Total_Flete_Transportador")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                        Else
                                            strFila += Formatear_Campo_UNO_EE(Val(detrem("Intermediacion")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                        End If
                                    Else
                                        strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    End If
                                    'strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    If Val(detmov("Valor_Credito")) > 0 Then
                                        If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                            strFila += Formatear_Campo_UNO_EE(Val(detrem("Total_Flete_Transportador")).ToString, 21, CAMPO_DINERO) '16-Valor Credito 
                                        Else
                                            strFila += Formatear_Campo_UNO_EE(Val(detrem("Intermediacion")).ToString, 21, CAMPO_DINERO) '16-Valor Credito 
                                        End If
                                    Else
                                        strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    End If
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                                    'strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base
                                    If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                        strFila += Formatear_Campo_UNO_EE(Val(detrem("Total_Flete_Transportador")).ToString, 21, CAMPO_DINERO) '19-Valor Base
                                    Else
                                        strFila += Formatear_Campo_UNO_EE(Val(detrem("Intermediacion")).ToString, 21, CAMPO_DINERO) '19-Valor Base 
                                    End If
                                    strFila += Formatear_Campo_UNO_EE(detmov("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
                                    Dim observaciones As String = ""
                                    If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                        observaciones = "COSTO" & detrem("Numero_Documento").ToString
                                    Else
                                        observaciones = "INTERMEDIACIÓN" & detrem("Numero_Documento").ToString
                                    End If
                                    strFila += Formatear_Campo_UNO_EE(detmov("Observaciones") & observaciones, 255, CAMPO_ALFANUMERICO) '22-Tipo Documento
                                    stwStreamWriter.WriteLine(strFila)
                                    auxConsecutivo += 1
                                Next
                            Else
                                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                                If detmov("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXC_ALTERNATIVA_LOGISTICA Then ' Movimiento Contable
                                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                Else
                                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_CXC_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                End If
                                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                                strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                strFila += Formatear_Campo_UNO_EE(detmov("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                                lonNumeroComprobante = Val(detmov("Numero_Documento"))
                                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                                strFila += Formatear_Campo_UNO_EE(detmov("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                                strFila += Formatear_Campo_UNO_EE(detmov("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                                strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos

                                If detmov("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXC_ALTERNATIVA_LOGISTICA Then ' Movimiento Contable

                                    strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo
                                    strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

                                    strFila += Formatear_Campo_UNO_EE(detmov("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                                    'strFila += Formatear_Campo_UNO_EE(detmov("Codigo_Cuenta"), 8, CAMPO_NUMERICO) '21-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE(detmov("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento

                                Else
                                    'Cuenta Por Cobrar
                                    strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna

                                    strFila += Formatear_Campo_UNO_EE(detmov("Observaciones"), 255, CAMPO_ALFANUMERICO) '19 Observaciones
                                    strFila += Formatear_Campo_UNO_EE("001", 3, CAMPO_ALFANUMERICO) '20- Sucursal
                                    strFila += Formatear_Campo_UNO_EE(detmov("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '21-Tipo Documento Cruce

                                    lonNumeroComprobante = Val(detmov("Numero_Documento"))
                                    strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '22-No Documento Cruce
                                    strFila += Formatear_Campo_UNO_EE(1, 3, CAMPO_NUMERICO) '23-Cuota
                                    strFila += Formatear_Campo_UNO_EE(detmov("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '24-Fecha Vence
                                    strFila += Formatear_Campo_UNO_EE(detmov("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '25-Fecha Dcto

                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '31-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '32-Valor Credito Moneda Alterna

                                    strFila += Formatear_Campo_UNO_EE(Val(detmov("EMPR_Numero_Identificacion")).ToString, 15, CAMPO_ALFANUMERICO) '33-Documento Tercero
                                    strFila += Formatear_Campo_UNO_EE(detmov("Observaciones"), 255, CAMPO_ALFANUMERICO) '34-Observaciones

                                End If
                                stwStreamWriter.WriteLine(strFila)
                                auxConsecutivo += 1
                            End If
                        Next
                    End If

                Next
            End If

            ' Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Liquidacion

    'Private Function Generar_Documento_Liquidacion_UNO_EE_OLD() As Boolean
    '    Dim ComandoSQL As SqlCommand, lonRes As Long
    '    Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
    '    Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

    '    ' Si el archivo existe se borra primero 
    '    If File.Exists(Me.strPathArchivo) Then
    '        File.Delete(Me.strPathArchivo)
    '    End If

    '    ' Se abre el archivo 
    '    stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
    '    stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

    '    Try

    '        'Encabezado INTERFAZ
    '        strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
    '        strFila += CERO_STRING.ToString '2-Separador Encabezado
    '        strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
    '        strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
    '        stwStreamWriter.WriteLine(strFila)

    '        Dim auxConsecutivo As Integer = 2

    '        ' Consulta generada para crear los documentos del archivo plano de Facturas UNO EE
    '        strSQL = "SELECT * " & Chr(13)
    '        strSQL += " FROM V_Interfaz_Liquidacion_Contable_UNO_EE " & Chr(13)
    '        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

    '        strSQL += " AND TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
    '        If intOficina > 0 Then
    '            strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
    '        End If

    '        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
    '        Me.objGeneral.ConexionSQL.Open()
    '        Dim sdrDocuLiqu As SqlDataReader = ComandoSQL.ExecuteReader

    '        Dim dtsDocuLiqu As DataTable = New DataTable()
    '        dtsDocuLiqu.Load(sdrDocuLiqu)

    '        sdrDocuLiqu.Close()
    '        Me.objGeneral.ConexionSQL.Close()
    '        If dtsDocuLiqu.Rows.Count > 0 Then
    '            For Each rowLiqu As DataRow In dtsDocuLiqu.Rows

    '                ' Consulta generada para crear los documentos del archivo plano de Facturas UNO EE
    '                strSQL = "SELECT * " & Chr(13)
    '                strSQL += " FROM V_Interfaz_Encabezado_Contable_UNO_EE " & Chr(13)
    '                strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

    '                strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
    '                strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

    '                strSQL += " AND TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
    '                strSQL += " AND Anulado = 0"
    '                strSQL += " AND Numero = " & CStr(rowLiqu("Numero"))
    '                strSQL += " AND Numero_Documento = " & CStr(rowLiqu("Numero_Documento"))
    '                If intOficina > 0 Then
    '                    strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
    '                End If

    '                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
    '                Me.objGeneral.ConexionSQL.Open()
    '                Dim sdrDocuInte As SqlDataReader = ComandoSQL.ExecuteReader

    '                Dim dtsDocuInte As DataTable = New DataTable()
    '                dtsDocuInte.Load(sdrDocuInte)


    '                sdrDocuInte.Close()
    '                Me.objGeneral.ConexionSQL.Close()
    '                If dtsDocuInte.Rows.Count > 0 Then
    '                    For Each row As DataRow In dtsDocuInte.Rows
    '                        Dim DocuNumero As String = CStr(row("Numero_Documento"))
    '                        Dim DocuMovim As String = CStr(row("Numero"))

    '                        'Documento INTERFAZ
    '                        strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
    '                        strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
    '                        strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
    '                        strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
    '                        strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
    '                        strFila += Formatear_Campo_UNO_EE(0, 1, CAMPO_NUMERICO) '6-Tipo Consecutivo
    '                        strFila += Formatear_Campo_UNO_EE(CStr(row("Centro_Operacion")), 3, CAMPO_ALFANUMERICO) '7-Centro Operacion General

    '                        strFila += Formatear_Campo_UNO_EE(CStr(row("Tipo_Documento")), 3, CAMPO_ALFANUMERICO) '8-Tipo Documento
    '                        'strFila += Formatear_Campo_UNO_EE(CStr(row("Numero_Documento")), 8, CAMPO_NUMERICO) '9-Numero Documento
    '                        strFila += Formatear_Campo_UNO_EE(CStr(row("ENMC_Numero")), 8, CAMPO_NUMERICO) '9-Numero Documento
    '                        strFila += Formatear_Campo_UNO_EE(CStr(row("ENMC_Fecha")), 8, CAMPO_ALFANUMERICO) '10-Fecha Generacion Documento
    '                        strFila += Formatear_Campo_UNO_EE(CStr(row("Identificacion_Tercero")), 15, CAMPO_ALFANUMERICO) '11-Identificacion Tercero
    '                        strFila += Formatear_Campo_UNO_EE("00030", 5, CAMPO_ALFANUMERICO) '12-Maestro Codigo Cuenta Contable
    '                        strFila += Formatear_Campo_UNO_EE(CStr(row("Concepto_Anula")), 1, CAMPO_NUMERICO) '13-Estado
    '                        strFila += Formatear_Campo_UNO_EE(1, 1, CAMPO_NUMERICO) '14-Impreso

    '                        Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
    '                        strFila += Formatear_Campo_UNO_EE(MAESTRO_ENCABEZADO_LIQUIDACION_UNO_EE & fechaAux, 255, CAMPO_ALFANUMERICO) '15 Observacion
    '                        stwStreamWriter.WriteLine(strFila)

    '                        auxConsecutivo += 1

    '                        ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
    '                        strSQL = "SELECT * " & Chr(13)
    '                        strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
    '                        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

    '                        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
    '                        strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

    '                        strSQL += " AND TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
    '                        strSQL += " AND Anulado = 0"
    '                        If intOficina > 0 Then
    '                            strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
    '                        End If
    '                        strSQL += " AND Numero = " & DocuMovim
    '                        strSQL += " AND Numero_Documento = " & DocuNumero

    '                        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
    '                        Me.objGeneral.ConexionSQL.Open()
    '                        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

    '                        While sdrArchInte.Read

    '                            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
    '                            strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
    '                            If sdrArchInte("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXP_ALTERNATIVA_LOGISTICA Then
    '                                strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
    '                                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
    '                            Else
    '                                strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_CXP_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
    '                                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_CXP_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
    '                            End If

    '                            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
    '                            strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
    '                            strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

    '                            'lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
    '                            lonNumeroComprobante = Val(sdrArchInte("ENMC_Numero"))
    '                            strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

    '                            strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
    '                            strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
    '                            strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento


    '                            If sdrArchInte("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXP_ALTERNATIVA_LOGISTICA Then
    '                                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
    '                                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
    '                                strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo

    '                                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
    '                                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
    '                                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
    '                                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
    '                                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

    '                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
    '                                strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
    '                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento
    '                            Else
    '                                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 20, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
    '                                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
    '                                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
    '                                Dim intRecaTota As Integer = Val(sdrArchInte("Valor_Credito")) + Val(sdrArchInte("Valor_Pago_Recaudo_Total"))
    '                                strFila += Formatear_Campo_UNO_EE(intRecaTota.ToString, 21, CAMPO_DINERO) '16-Valor Credito
    '                                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
    '                                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna

    '                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento
    '                                strFila += Formatear_Campo_UNO_EE("001", 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
    '                                strFila += Formatear_Campo_UNO_EE("", 4, CAMPO_ALFANUMERICO) '7-Tipo Documento

    '                                lonNumeroComprobante = Val(sdrArchInte("ENMC_Numero"))
    '                                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '22-No Documento Cruce
    '                                strFila += Formatear_Campo_UNO_EE(1, 3, CAMPO_NUMERICO) '23-Cuota
    '                                strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo
    '                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '24-Fecha Vence
    '                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '25-Fecha Dcto

    '                                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Valor Credito Moneda Alterna
    '                                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Valor Credito Moneda Alterna
    '                                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Valor Credito Moneda Alterna
    '                                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Valor Credito Moneda Alterna
    '                                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Valor Credito Moneda Alterna

    '                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '34-Observaciones
    '                            End If

    '                            stwStreamWriter.WriteLine(strFila)
    '                            'sdrArchInte.Close()
    '                            auxConsecutivo += 1

    '                        End While
    '                        sdrArchInte.Close()
    '                        Me.objGeneral.ConexionSQL.Close()

    '                    Next
    '                End If
    '            Next
    '        End If
    '        'Pie de Pagina INTERFAZ
    '        strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
    '        strFila += "9999" '2-Separador Encabezado
    '        strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
    '        strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
    '        stwStreamWriter.WriteLine(strFila)

    '        stwStreamWriter.Close()
    '        stmStreamW.Close()

    '        'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
    '        '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
    '        'End If

    '        Return True

    '    Catch ex As Exception
    '        stwStreamWriter.Close()
    '        stmStreamW.Close()
    '        Me.objGeneral.ConexionSQL.Close()
    '        Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
    '        Return False
    '    Finally

    '        If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
    '            Me.objGeneral.ConexionSQL.Close()
    '        End If
    '    End Try
    'End Function

    Private Function Generar_Documento_Liquidacion_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            Dim auxConsecutivo As Integer = 2

            ' Consulta generada para crear los documentos del archivo plano de Liquidaciones UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Liquidacion_Contable_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrDocuLiqu As SqlDataReader = ComandoSQL.ExecuteReader

            Dim dtsDocuLiqu As DataTable = New DataTable()
            dtsDocuLiqu.Load(sdrDocuLiqu)

            sdrDocuLiqu.Close()
            Me.objGeneral.ConexionSQL.Close()
            If dtsDocuLiqu.Rows.Count > 0 Then
                For Each rowLiqu As DataRow In dtsDocuLiqu.Rows

                    ' Consulta generada para crear los documentos del archivo plano de Liquidaciones UNO EE
                    strSQL = "SELECT * " & Chr(13)
                    strSQL += " FROM V_Interfaz_Encabezado_Contable_UNO_EE " & Chr(13)
                    strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

                    strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
                    strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

                    strSQL += " AND TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
                    strSQL += " AND Anulado = 0"
                    strSQL += " AND Numero = " & CStr(rowLiqu("Numero"))
                    strSQL += " AND Numero_Documento = " & CStr(rowLiqu("Numero_Documento"))
                    If intOficina > 0 Then
                        strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
                    End If

                    ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                    Me.objGeneral.ConexionSQL.Open()
                    Dim sdrDocuInte As SqlDataReader = ComandoSQL.ExecuteReader

                    Dim dtsDocuInte As DataTable = New DataTable()
                    dtsDocuInte.Load(sdrDocuInte)


                    sdrDocuInte.Close()
                    Me.objGeneral.ConexionSQL.Close()
                    If dtsDocuInte.Rows.Count > 0 Then
                        For Each row As DataRow In dtsDocuInte.Rows
                            Dim DocuNumero As String = CStr(row("Numero_Documento"))
                            Dim DocuMovim As String = CStr(row("Numero"))

                            'Documento INTERFAZ
                            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
                            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                            strFila += Formatear_Campo_UNO_EE(0, 1, CAMPO_NUMERICO) '6-Tipo Consecutivo
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Centro_Operacion")), 3, CAMPO_ALFANUMERICO) '7-Centro Operacion General

                            strFila += Formatear_Campo_UNO_EE(CStr(row("Tipo_Documento")), 3, CAMPO_ALFANUMERICO) '8-Tipo Documento
                            'strFila += Formatear_Campo_UNO_EE(CStr(row("Numero_Documento")), 8, CAMPO_NUMERICO) '9-Numero Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("ENMC_Numero")), 8, CAMPO_NUMERICO) '9-Numero Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("ENMC_Fecha")), 8, CAMPO_ALFANUMERICO) '10-Fecha Generacion Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Identificacion_Tercero")), 15, CAMPO_ALFANUMERICO) '11-Identificacion Tercero
                            strFila += Formatear_Campo_UNO_EE("00030", 5, CAMPO_ALFANUMERICO) '12-Maestro Codigo Cuenta Contable
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Concepto_Anula")), 1, CAMPO_NUMERICO) '13-Estado
                            strFila += Formatear_Campo_UNO_EE(1, 1, CAMPO_NUMERICO) '14-Impreso

                            'Control de Cambios #002 Eliminar Campo fecha 
                            strFila += Formatear_Campo_UNO_EE(MAESTRO_ENCABEZADO_LIQUIDACION_UNO_EE, 255, CAMPO_ALFANUMERICO) '15 Observacion
                            stwStreamWriter.WriteLine(strFila)

                            auxConsecutivo += 1

                            ' Consulta generada para crear el archivo plano de Liquidaciones UNO EE
                            strSQL = "SELECT * " & Chr(13)
                            strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
                            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

                            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
                            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

                            strSQL += " AND TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
                            strSQL += " AND Anulado = 0"
                            If intOficina > 0 Then
                                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
                            End If
                            strSQL += " AND Numero = " & DocuMovim
                            strSQL += " AND Numero_Documento = " & DocuNumero

                            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                            Me.objGeneral.ConexionSQL.Open()
                            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

                            While sdrArchInte.Read
                                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                                If sdrArchInte("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXP_ALTERNATIVA_LOGISTICA Then
                                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                    strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                                Else
                                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_CXP_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                    strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_CXP_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                                End If

                                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                                'lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                                lonNumeroComprobante = Val(sdrArchInte("ENMC_Numero"))
                                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento


                                If sdrArchInte("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXP_ALTERNATIVA_LOGISTICA Then
                                    strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                    strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                                    strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo

                                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento
                                Else
                                    strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 20, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                    strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    Dim intRecaTota As Integer = Val(sdrArchInte("Valor_Credito")) + Val(sdrArchInte("Valor_Pago_Recaudo_Total"))
                                    strFila += Formatear_Campo_UNO_EE(intRecaTota.ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna

                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE("001", 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                    strFila += Formatear_Campo_UNO_EE("", 4, CAMPO_ALFANUMERICO) '7-Tipo Documento

                                    lonNumeroComprobante = Val(sdrArchInte("ENMC_Numero"))
                                    strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '22-No Documento Cruce
                                    'Control Cambio #002  Inicializar Campo de 001 a 000
                                    strFila += Formatear_Campo_UNO_EE("000", 3, CAMPO_ALFANUMERICO) '23-Cuota
                                    strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo
                                    'Control Cambio #002 Cambio de fecha a  Fecha_manifiesto
                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Manifiesto"), 8, CAMPO_ALFANUMERICO) '24-Fecha Vence
                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Manifiesto"), 8, CAMPO_ALFANUMERICO) '25-Fecha Dcto

                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Valor Credito Moneda Alterna

                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '34-Observaciones
                                End If

                                stwStreamWriter.WriteLine(strFila)
                                'sdrArchInte.Close()
                                auxConsecutivo += 1

                            End While
                            sdrArchInte.Close()
                            Me.objGeneral.ConexionSQL.Close()

                        Next
                    End If
                Next
            End If
            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Nota Credito Movimiento
    Private Function Generar_Documento_Nota_Credito_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

            strSQL += " AND TIDO_Documento = " & TIDO_NOTA_CREDITO
            strSQL += " AND Anulado = 0"
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            Dim auxConsecutivo As Integer = 3

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            'Documento INTERFAZ
            strFila = Formatear_Campo_UNO_EE(2, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
            strFila += Formatear_Campo_UNO_EE("1001", 4, CAMPO_ALFANUMERICO) '6-Centro Operacion General

            strFila += Formatear_Campo_UNO_EE(MAESTRO_TIPO_DOCUMENTO_NOTA_CREDITO_UNO_EE, 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
            strFila += Formatear_Campo_UNO_EE(DateTime.Now.ToString("yyyyMMdd"), 8, CAMPO_ALFANUMERICO) '8-Fecha Generacion Documento

            strFila += Formatear_Campo_UNO_EE("00030", 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '10-Sub Tipo Registro

            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
            strFila += Formatear_Campo_UNO_EE("NOTACREDITO" & fechaAux, 270, CAMPO_ALFANUMERICO) '11 Observacion
            stwStreamWriter.WriteLine(strFila)

            While sdrArchInte.Read

                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                strFila += Formatear_Campo_UNO_EE(MAESTRO_FLUJO_EFECTIVO_UNO_EE, 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo

                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 8, CAMPO_NUMERICO) '21-Tipo Documento
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento

                stwStreamWriter.WriteLine(strFila)
                auxConsecutivo += 1

            End While

            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa

            stwStreamWriter.WriteLine(strFila)

            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Nota Credito Documento
    Private Function Generar_Documento_Nota_Credito_Documental_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

            strSQL += " AND TIDO_Documento = " & 190
            strSQL += " AND Anulado = 0"
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            Dim auxConsecutivo As Integer = 3

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            'Documento INTERFAZ
            strFila = Formatear_Campo_UNO_EE(2, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
            strFila += Formatear_Campo_UNO_EE("1001", 4, CAMPO_ALFANUMERICO) '6-Centro Operacion General

            strFila += Formatear_Campo_UNO_EE(MAESTRO_TIPO_DOCUMENTO_NOTA_CREDITO_UNO_EE, 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
            strFila += Formatear_Campo_UNO_EE(DateTime.Now.ToString("yyyyMMdd"), 8, CAMPO_ALFANUMERICO) '8-Fecha Generacion Documento

            strFila += Formatear_Campo_UNO_EE("00030", 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '10-Sub Tipo Registro

            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
            strFila += Formatear_Campo_UNO_EE("NOTACREDITO" & fechaAux, 270, CAMPO_ALFANUMERICO) '11 Observacion
            stwStreamWriter.WriteLine(strFila)

            While sdrArchInte.Read

                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                strFila += Formatear_Campo_UNO_EE(1, 1, CAMPO_NUMERICO) '6 - Tipo Consecutivo

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '7-Centro Operacion
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '8-Tipo Documento

                lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '9-No Documento

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Concepto"), 8, CAMPO_ALFANUMERICO) '10-Fecha Formateada
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '11-Maestro Tercer

                strFila += Formatear_Campo_UNO_EE("30", 5, CAMPO_ALFANUMERICO) '12-Clase Interna UNOEE
                strFila += Formatear_Campo_UNO_EE("0", 1, CAMPO_ALFANUMERICO) '13-Estado Elaboracion
                strFila += Formatear_Campo_UNO_EE("0", 1, CAMPO_ALFANUMERICO) '14-Impreso

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '15-Observacion

                stwStreamWriter.WriteLine(strFila)
                auxConsecutivo += 1

            End While

            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa

            stwStreamWriter.WriteLine(strFila)

            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function


    ' INTERFAZ CONTABLE WORLD_OFFICE

    ' Generar Interfaz World Office Facturas
    Private Function Generar_Documento_Factura_WORLD_OFFICE() As Boolean

        Dim ComandoSQL As SqlCommand
        Dim dteFechaAux As Date
        Dim dteFechaVenceAux As Date
        Dim stmStreamW As Stream
        Dim stwStreamWriter As StreamWriter
        Dim strFila As String
        Dim strCampo As String = ""
        Dim intCodigoTercUsuario As Integer
        Dim strIdentificacionTercUsuario As String = ""


        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)
        'stwStreamWriter = New StreamWriter(stmStreamW)

        ' Consulta generada para crear el archivo plano de Documentos
        strSQL = "SELECT * " & Chr(13)
        strSQL += " FROM V_Interfaz_Factura_WORLD_OFFICE " & Chr(13)
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

        strSQL += " AND ISNULL(Interfaz_Contable_Factura,0) = 0"
        If intOficina > 0 Then
            strSQL += " AND OFIC_Factura = " & intOficina & Chr(13)
        End If

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

        Me.objGeneral.ConexionSQL.Open()

        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        Try
            strFila = String.Empty

            While sdrArchInte.Read

                strFila = sdrArchInte("NombreEmpresa").ToString.Trim & SEPARADOR_WORLD_OFFICE

                Date.TryParse(sdrArchInte("Fecha"), dteFechaAux)
                strFila += Format(dteFechaAux, clsGeneral.FORMATO_FECHA_DIA_MES_ANO).ToString.Trim & SEPARADOR_WORLD_OFFICE

                Date.TryParse(sdrArchInte("Fecha_Vence"), dteFechaVenceAux)
                strFila += Format(dteFechaVenceAux, clsGeneral.FORMATO_FECHA_DIA_MES_ANO).ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += INDICADOR_FACTURA_VENTA_FV & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Prefijo_Ciudad").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("ENFA_Numero_Documento").ToString.Trim & SEPARADOR_WORLD_OFFICE

                objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Usuarios", "TERC_Codigo_Empleado", "Codigo", clsGeneral.CAMPO_NUMERICO, intUsuario, intCodigoTercUsuario, , Me.strError, )
                objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Terceros", "Numero_Identificacion", "Codigo", clsGeneral.CAMPO_ALFANUMERICO, intCodigoTercUsuario, strIdentificacionTercUsuario, , Me.strError, )

                'Tercero Interno (Corresponde al número de identificación del usuario que genera el archivo)
                strCampo = strIdentificacionTercUsuario
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'strFila += SEPARADOR_WORLD_OFFICE ' Espacio en Blanco _ Elaborado Por

                strFila += sdrArchInte("Cuenta_Contable").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("VEHI_Codigo").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Valor_Debito").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Valor_Credito").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Concepto").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("IdentCliente").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Concepto").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("IdentCliente").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += clsGeneral.CERO.ToString & SEPARADOR_WORLD_OFFICE
                strFila += sdrArchInte("FormaPago").ToString.Trim & SEPARADOR_WORLD_OFFICE
                strFila += sdrArchInte("Anulado").ToString.Trim & SEPARADOR_WORLD_OFFICE

                stwStreamWriter.WriteLine(strFila)

            End While

            sdrArchInte.Close()
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()

            If Me.intEstado = ARCHIVO_DEFINITIVO Then
                Call Marcar_Registros_Interfaz_Definitivo_Factura_WORLD_OFFICE(intOficina)
            End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

            End Try
            Return False
        End Try

    End Function

    ' Generar Interfaz World Office Facturas Inventario
    Private Function Generar_Documento_Factura_Inventario_WORLD_OFFICE() As Boolean

        Dim ComandoSQL As SqlCommand
        Dim dteFechaAux As Date
        Dim dteFechaVenceAux As Date
        Dim stmStreamW As Stream
        Dim stwStreamWriter As StreamWriter
        Dim strFila As String
        Dim strCampo As String = ""
        Dim intCodigoTercUsuario As Integer
        Dim strIdentificacionTercUsuario As String = ""


        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)
        'stwStreamWriter = New StreamWriter(stmStreamW)

        ' Consulta generada para crear el archivo plano de Documentos
        strSQL = "SELECT * " & Chr(13)
        strSQL += " FROM V_Interfaz_Factura_Inventario_WORLD_OFFICE " & Chr(13)
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha < DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

        strSQL += " AND ISNULL(Interfaz_Contable_Factura,0) = 0"
        If Me.intEstado = ARCHIVO_DEFINITIVO Or Me.intEstado = ARCHIVO_BORRADOR Then
            strSQL += " AND Estado = " & Me.intEstado.ToString
        End If

        If intOficina > 0 Then
            strSQL += " AND OFIC_Factura = " & intOficina & Chr(13)
        End If

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

        Me.objGeneral.ConexionSQL.Open()

        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        Try
            strFila = String.Empty

            While sdrArchInte.Read

                strFila = sdrArchInte("NombreEmpresa").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += INDICADOR_FACTURA_VENTA_FV & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Prefijo_Ciudad").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("ENFA_Numero_Documento").ToString.Trim & SEPARADOR_WORLD_OFFICE

                Date.TryParse(sdrArchInte("Fecha"), dteFechaAux)
                strFila += Format(dteFechaAux, clsGeneral.FORMATO_FECHA_DIA_MES_ANO).ToString.Trim & SEPARADOR_WORLD_OFFICE

                objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Usuarios", "TERC_Codigo_Empleado", "Codigo", clsGeneral.CAMPO_NUMERICO, intUsuario, intCodigoTercUsuario, , Me.strError, )
                objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Terceros", "Numero_Identificacion", "Codigo", clsGeneral.CAMPO_ALFANUMERICO, intCodigoTercUsuario, strIdentificacionTercUsuario, , Me.strError, )

                'Tercero Interno (Corresponde al número de identificación del usuario que genera el archivo)
                strCampo = strIdentificacionTercUsuario
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("IdentCliente").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Nota").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("FormaPago").ToString.Trim & SEPARADOR_WORLD_OFFICE

                Date.TryParse(sdrArchInte("Fecha"), dteFechaAux)
                strFila += Format(dteFechaAux, clsGeneral.FORMATO_FECHA_DIA_MES_ANO).ToString.Trim & SEPARADOR_WORLD_OFFICE

                strCampo = ""
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                strCampo = ""
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                strCampo = "-1"
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Anulado").ToString.Trim & SEPARADOR_WORLD_OFFICE

                ' Columnas Vacias desde O hasta AE
                For auxCount As Integer = 0 To 16 Step 1
                    strCampo = ""
                    strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE
                Next

                'Especificacion Oficina I
                'strFila += sdrArchInte("NombreOficina").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Columna Vacia AE
                'strCampo = " "
                'strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Movimiento Contable Transporte Terrestre
                'strFila += MOVIMIENTO_CONTABLE_TRANSPORTE_TERRESTRE.ToString & SEPARADOR_WORLD_OFFICE
                strFila += sdrArchInte("Codigo_Cuenta").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Especificacion Oficina II
                strFila += sdrArchInte("NombreOficina").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Unidad de Transporte
                strFila += sdrArchInte("UNME_Codigo").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Cantidad Remesas Factura - Otros Conceptos
                strFila += sdrArchInte("CantidadDetalle").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Impuesto
                strFila += sdrArchInte("Impuesto").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Valor Flete Cliente 
                strFila += sdrArchInte("Valor_Flete_Cliente_Facturar").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Descuento
                strFila += sdrArchInte("Descuento").ToString.Trim & SEPARADOR_WORLD_OFFICE

                Date.TryParse(sdrArchInte("Fecha_Vence"), dteFechaVenceAux)
                strFila += Format(dteFechaVenceAux, clsGeneral.FORMATO_FECHA_DIA_MES_ANO).ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Columna AN OBSERVACIONES FACTURA
                strCampo = sdrArchInte("Observaciones").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Placa Vehiculo 
                strFila += sdrArchInte("VEHI_Codigo").ToString.Trim & SEPARADOR_WORLD_OFFICE

                ' Columnas Vacias desde AP hasta BE
                For auxCount As Integer = 0 To 15 Step 1
                    strCampo = ""
                    strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE
                Next

                stwStreamWriter.WriteLine(strFila)

            End While

            sdrArchInte.Close()
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()

            'If Me.intEstado = ARCHIVO_DEFINITIVO Then
            '    Call Marcar_Registros_Interfaz_Definitivo_Factura_WORLD_OFFICE(intOficina)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

            End Try
            Return False
        End Try

    End Function

    ' Generar Interfaz World Office Comprobantes Egreso
    Private Function Generar_Interfaz_Comprobantes_WORLD_OFFICE() As Boolean

        Dim dteFecha As Date
        Dim strDiasPlazoFactura As String = ""
        Dim dteFechaVencimiento As Date = Date.MinValue
        Dim strPrefijoOficina As String = ""
        Dim intCodigoTercUsuario As Integer
        Dim strIdentificacionTercUsuario As String = ""


        Dim strNumeroDocumentoRemesa As String
        Dim arrCampos(2), arrValoresCampos(2) As String
        Dim strNumeroDocumentoFactura As String
        Dim strIdentificacionTerceroEncabezado As String = ""
        Dim strFormaPago As String = ""

        Dim strNombreConductor As String = ""
        Dim strNombreRuta As String = ""
        Dim strNumeroManifiestoGasto As String = ""
        Dim strPlaca As String = ""
        Dim strNombRuta As String = ""


        '----
        Dim ComandoSQL As SqlCommand
        Dim strObservaciones As String
        Dim strObservacionesEspecificas As String
        Dim strObservacionesEncabezado As String = ""
        Dim stmStreamW As Stream
        Dim stwStreamWriter As StreamWriter
        Dim strFila As String
        Dim strCampo As String

        Dim strIdentificacion As String = ""
        Dim strTipoNaturaleza As String = ""
        Dim strNombreTercero As String = ""

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        'Consulta generada para crear el archivo plano de Documentos
        strSQL = "SELECT * " & Chr(13)
        strSQL += " FROM V_Detalle_Encabezado_Comprobante_Contables " & Chr(13)
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)
        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha < DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "
        'strSQL += " AND ISNULL(Interfaz_Contable,0) = " & ARCHIVO_BORRADOR
        'strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO & Chr(13)
        strSQL += " AND TIDO_Documento_Cruce = " & TIDO_COMPROBANTE_EGRESO

        If intOficina > 0 Then
            strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
        End If
        strSQL += " ORDER BY Numero_Documento"

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

        Me.objGeneral.ConexionSQL.Open()

        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        strObservaciones = ""
        strObservacionesEspecificas = ""

        'Tercero Interno (Corresponde al número de identificación del usuario que genera el archivo)
        'El objetivo es tomar el número de identificación del funcionario con el qué esta asociado el usuario, sin embargo en estos momentos los
        'usuarios estan relacionados al tercero con código 1 ó 0, por tanto se hará uso del valor que existan en el campo Prefijo_Contable_Ingresos

        objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Usuarios", "TERC_Codigo_Empleado", "Codigo", clsGeneral.CAMPO_NUMERICO, intUsuario, intCodigoTercUsuario, , Me.strError, )
        objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Terceros", "Numero_Identificacion", "Codigo", clsGeneral.CAMPO_ALFANUMERICO, intCodigoTercUsuario, strIdentificacionTercUsuario, , Me.strError, )

        Try
            strFila = String.Empty
            While sdrArchInte.Read

                'Inicialización de variables
                dteFechaVencimiento = Date.MinValue
                strPrefijoOficina = ""
                strNumeroDocumentoRemesa = ""
                strNumeroDocumentoFactura = ""
                strDiasPlazoFactura = ""

                strObservaciones = sdrArchInte("Observaciones").ToString.Trim

                strPlaca = ""
                strPlaca = sdrArchInte("Placa").ToString
                '********* Comenzar a escribir los campos **************
                ' Nombre de la Empresa
                strFila = sdrArchInte("Nombre_Razon_Social").ToString & SEPARADOR_WORLD_OFFICE

                ' Tipo Documento 
                strCampo = sdrArchInte("Tipo_Documento").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Prefijo
                strCampo = sdrArchInte("Prefijo").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Numero Documento
                strCampo = sdrArchInte("Numero_Documento").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Fecha Creación
                Date.TryParse(sdrArchInte("Fecha_Documento"), dteFecha)
                'strCampo = Format(sdrArchInte("Fecha_Documento"), FORMATO_FECHA)
                strCampo = Format(sdrArchInte("Fecha_Documento"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE


                'TerceroInterno
                strCampo = strIdentificacionTercUsuario
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'TerceroExterno
                strCampo = sdrArchInte("EMPR_NIT").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Observaciones Comprobante Egreso
                strCampo = sdrArchInte("ObservacionesNota").ToString

                'If Len(strCampo) > 80 Then
                '    strCampo = Mid(strCampo, 1, 80)
                'End If

                strCampo = RTrim(strCampo)
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Verificado
                strCampo = sdrArchInte("Verificado").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Anulado
                strCampo = sdrArchInte("Anulado").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Sucursal
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Clasificación
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Caja Menor
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 1
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 2
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 3
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 4
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 5
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 6
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 7
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 8
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 9
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 10
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 11
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 12
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 13
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 14
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 15
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Cuenta PUC
                strCampo = sdrArchInte("Codigo_Cuenta").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Observaciones Comprobante Egreso
                strCampo = sdrArchInte("ObservacionesNota").ToString

                'If Len(strCampo) > 80 Then
                '    strCampo = Mid(strCampo, 1, 80)
                'End If

                strCampo = RTrim(strCampo)
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Tercero Externo
                strCampo = sdrArchInte("IdentificacionBenificiario").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Cheque
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Valor Débito
                strCampo = Val(sdrArchInte("Valor_Debito"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Valor Crédito
                strCampo = Val(sdrArchInte("Valor_Credito"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Fecha Vencimiento
                Date.TryParse(sdrArchInte("Fecha_Documento"), dteFechaVencimiento)
                'If TipoDocumento = clsTipoDocumento.TIDO_FACTURAS Then
                dteFechaVencimiento = dteFechaVencimiento.AddDays(clsGeneral.TREINTA_DIAS)
                strCampo = Format(dteFechaVencimiento)
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Centro Costos
                strCampo = strPlaca
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Activo Fijo
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Tipo_Base
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Porcentaje Rete-Fuente
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Base Retención
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Pago Retención
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'IVA al Costo
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Sucursal
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Importación
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Caja Menor
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Excluir NIIF
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'ImpoConsumo Costo
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'No deducible
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Codigo centro costos
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Abona_Cruce
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                stwStreamWriter.WriteLine(strFila)

            End While

            sdrArchInte.Close()
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

            End Try
            Return False
        End Try

    End Function

    ' Generar Interfaz World Office Legalizacion Gastos
    Private Function Generar_Interfaz_Legalizacion_Gastos_Conductor_WORLD_OFFICE() As Boolean

        Dim dteFecha As Date
        Dim strDiasPlazoFactura As String = ""
        Dim dteFechaVencimiento As Date = Date.MinValue
        Dim strPrefijoOficina As String = ""
        Dim intCodigoTercUsuario As Integer
        Dim strIdentificacionTercUsuario As String = ""

        Dim arrCampos(2), arrValoresCampos(2) As String
        Dim strNumeroDocumentoFactura As String
        Dim strIdentificacionTerceroEncabezado As String = ""
        Dim strFormaPago As String = ""

        Dim strNombreConductor As String = ""
        Dim strNombreRuta As String = ""
        Dim strNumeroManifiestoGasto As String = ""
        Dim strPlaca As String = ""
        Dim strNombRuta As String = ""


        '----
        Dim ComandoSQL As SqlCommand
        Dim strObservaciones As String
        Dim strObservacionesEspecificas As String
        Dim strObservacionesEncabezado As String = ""
        Dim stmStreamW As Stream
        Dim stwStreamWriter As StreamWriter
        Dim strFila As String
        Dim strCampo As String

        Dim strIdentificacion As String = ""
        Dim strTipoNaturaleza As String = ""
        Dim strNombreTercero As String = ""

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)
        'stwStreamWriter = New StreamWriter(stmStreamW)

        'Consulta generada para crear el archivo plano de Documentos
        strSQL = "SELECT * " & Chr(13)
        strSQL += " FROM V_Interfaz_Legalizacion_WORLD_OFFICE " & Chr(13)
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)
        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha < DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

        'strSQL += " AND ISNULL(Interfaz_Contable,0) = " & ARCHIVO_BORRADOR

        'strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO & Chr(13)
        'strSQL += " AND TIDO_Documento_Cruce = " & TIDO_COMPROBANTE_EGRESO
        If Me.intEstado = ARCHIVO_BORRADOR Then
            Me.intEstado = ARCHIVO_BORRADOR_WORLD_OFFICE
        End If
        If Me.intEstado = ARCHIVO_DEFINITIVO Or Me.intEstado = ARCHIVO_BORRADOR_WORLD_OFFICE Then
            strSQL += " AND Estado = " & Me.intEstado.ToString
        End If

        If intOficina > 0 Then
            strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
        End If

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

        Me.objGeneral.ConexionSQL.Open()

        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        strObservaciones = ""
        strObservacionesEspecificas = ""


        'Tercero Interno (Corresponde al número de identificación del usuario que genera el archivo)
        'El objetivo es tomar el número de identificación del funcionario con el qué esta asociado el usuario, sin embargo en estos momentos los
        'usuarios estan relacionados al tercero con código 1 ó 0, por tanto se hará uso del valor que existan en el campo Prefijo_Contable_Ingresos

        objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Usuarios", "TERC_Codigo_Empleado", "Codigo", clsGeneral.CAMPO_NUMERICO, intUsuario, intCodigoTercUsuario, , Me.strError, )
        objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Terceros", "Numero_Identificacion", "Codigo", clsGeneral.CAMPO_ALFANUMERICO, intCodigoTercUsuario, strIdentificacionTercUsuario, , Me.strError, )


        'Campos adicionales para comprobantes de egresos
        Dim strNombreBancoComprobante As String = ""
        Dim strNombreFormaPagoComprobante As String = ""
        Dim strNumeroPagoComprobante As String = ""


        Try
            strFila = String.Empty

            While sdrArchInte.Read

                ' Nombre de la Empresa
                strFila = sdrArchInte("Nombre_Razon_Social").ToString & SEPARADOR_WORLD_OFFICE

                ' Tipo Documento 
                strCampo = INDICADOR_COMPROBANTE_EGRESOS_WOLD_OFFICE
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Prefijo Documento 
                strCampo = PREFIJO_LEGALIZACION_WORLD_OFFICE
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Numero Legalizacion 
                strCampo = Val(sdrArchInte("Numero_Documento"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Fecha Creación
                Date.TryParse(sdrArchInte("Fecha"), dteFecha)
                'strCampo = Format(sdrArchInte("Fecha_Documento"), FORMATO_FECHA)
                strCampo = Format(sdrArchInte("Fecha"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Tercero Interno (Corresponde al número de identificación del usuario que genera el archivo)
                strCampo = strIdentificacionTercUsuario
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Tercero_Externo
                strCampo = Val(sdrArchInte("Tercero_Legalizado"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Concepto Observacion
                strCampo = sdrArchInte("Concepto_Encabezado").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Verificado
                strFila = strFila & "-1" & SEPARADOR_WORLD_OFFICE

                'Anulado
                If Val(sdrArchInte("Estado").ToString) = clsGeneral.UNO Then
                    strCampo = clsGeneral.CERO
                Else
                    strCampo = clsGeneral.CERO
                End If
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE


                'Sucursal
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Clasificación
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Caja Menor
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 1
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 2
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 3
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 4
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 5
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 6
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 7
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 8
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 9
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 10
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 11
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 12
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 13
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 14
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 15
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Numero CUENTA PUC 
                strCampo = Val(sdrArchInte("Codigo_Cuenta"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Concepto
                strCampo = sdrArchInte("Concepto").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Tercero_Externo
                strCampo = Val(sdrArchInte("Tercero_Externo"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Cheque
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Valor Débito
                strCampo = Val(sdrArchInte("Valor_Debito"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Valor Crédito
                strCampo = Val(sdrArchInte("Valor_Credito"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Fecha Vencimiento
                Date.TryParse(sdrArchInte("Fecha"), dteFecha)
                'strCampo = Format(sdrArchInte("Fecha_Documento"), FORMATO_FECHA)
                strCampo = Format(sdrArchInte("Fecha"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE


                ' Placa
                strPlaca = ""
                strPlaca = sdrArchInte("Placa").ToString
                strFila = strFila & strPlaca & SEPARADOR_WORLD_OFFICE


                'Activo Fijo
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Tipo_Base
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Porcentaje Rete-Fuente
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Base Retención
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Pago Retención
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'IVA al Costo
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Sucursal
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Importación
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Caja Menor
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Excluir NIIF
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'ImpoConsumo Costo
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'No deducible
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Codigo centro costos
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Abona_Cruce
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE


                stwStreamWriter.WriteLine(strFila)

            End While

            sdrArchInte.Close()
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Marcar_Registros_Interfaz_Definitivo("Encabezado_Comprobante_Contables", intDocumentoContable, intOficina)
            'End If

            Return True


        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

            End Try
            Return False
        End Try

    End Function

    Private Function Generar_Interfaz_SIIGO_WINDOWS(ByVal TipoDocumento As Integer) As Boolean

        Dim ComandoSQL As SqlCommand
        Dim stmStreamW As Stream
        Dim stwStreamWriter As StreamWriter
        Dim strFila As String
        Dim strCampo As String

        Dim intCantRegi As Integer
        Dim dblValorCredito As Double
        Dim dblValorDebito As Double
        Dim lonNumeroCheque As Long
        Dim dblValorCheque As Double
        Dim strIdentificacion As String
        Dim strFuenteAnulacion As String
        Dim strFuente As String
        Dim lonNumeroDocumento As Long
        Dim strObservaciones As String
        Dim lonDocumentoCruce As Long
        Dim lonNumeroComprobante As Long
        Dim intSecuencia As Integer
        Dim intNumeroItem As Integer
        Dim strFormaPagoContable As String
        Dim strCodigoBanco As String

        'Dim bolManejoPrefijoUsuario As Boolean = False
        Dim bolNoIncluyeAnulados As Boolean = False
        Dim strCodiCiud As String = ""
        Dim strUsuarioCreaDocumento As String = ""
        Dim strNumeroManifiesto As String = ""

        'liquidacion

        Dim strCodiOfic = "", strCodiContOfic = "", strNumeMani = "", strPrefUsua = "", strCodiDoor = "", strDocuOrig = "", strPlacVehi = ""
        Dim strCodiVehi As String = ""

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPath) Then
            File.Delete(Me.strPath)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPath)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)
        'stwStreamWriter = New StreamWriter(stmStreamW)

        'Consulta generada para crear el archivo plano de Documentos
        strSQL = "SELECT * " & Chr(13)
        strSQL += " FROM V_Detalle_Encabezado_Comprobante_Contables_SIIGO_WINDOWS " & Chr(13)
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)
        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha < DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "
        strSQL += " AND TIDO_Codigo = " & TIDO_COMPROBANTE_CONTABLE & Chr(13)
        strSQL += " AND Interfase_Contable = " & ARCHIVO_BORRADOR & Chr(13)
        'strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO & Chr(13)
        strSQL += " AND TIDO_Documento = " & TipoDocumento & Chr(13)
        If intOficina > 0 Then
            strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
        End If
        If bolNoIncluyeAnulados Then
            strSQL += " AND Anulado = " & clsGeneral.CERO & Chr(13) 'colocar validacion solo para Multigranel
            'pendiente confirmación Mayerly
        End If
        strSQL += " ORDER BY Numero_Documento, Fecha"

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        intSecuencia = 0
        lonNumeroComprobante = 0
        lonDocumentoCruce = 0
        strObservaciones = ""
        intNumeroItem = 0

        Try
            strFila = String.Empty

            While sdrArchInte.Read

                strFuente = String.Empty
                strFuenteAnulacion = String.Empty
                intCantRegi = 0
                dblValorCredito = 0
                dblValorDebito = 0
                lonNumeroCheque = 0
                dblValorCheque = 0
                intNumeroItem += 1
                strFormaPagoContable = String.Empty
                strCodigoBanco = String.Empty
                strNumeroManifiesto = ""

                If lonNumeroComprobante = Val(sdrArchInte("Numero")) Then
                    intSecuencia += 1
                Else
                    lonNumeroComprobante = Val(sdrArchInte("Numero"))
                    intSecuencia = 1
                End If

                strFuente = sdrArchInte("Prefijo").ToString()
                strFuenteAnulacion = sdrArchInte("Sufijo_Codigo_Anexo").ToString()
                dblValorDebito = Val(sdrArchInte("Valor_Debito"))
                dblValorCredito = Val(sdrArchInte("Valor_Credito"))
                lonNumeroDocumento = Val(sdrArchInte("Numero_Documento"))
                lonDocumentoCruce = Val(sdrArchInte("Documento_Cruce"))

                'MODIFICACION SC INNECESARIO ESTRUCTURA BD 50
                'If TipoDocumento = TIDO_LIQUIDA_MANIFIESTO_TERCEROS Then
                '    'trae el codigo contable de la oficina del manifiesto  PROF
                '    Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Encabezado_Liquida_Manifiestos", "ENMA_Numero", "Numero", clsGeneral.CAMPO_NUMERICO, Val(sdrArchInte("Numero_Documento")), strNumeMani)
                '    strNumeroManifiesto = "Manif. " & strNumeMani
                '    Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Encabezado_Manifiestos", "OFIC_Codigo", "Numero", clsGeneral.CAMPO_NUMERICO, Val(strNumeMani), strCodiOfic)
                '    Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Prefijo_Oficinas", "CodigoAlterno", "CATA_Codigo", clsGeneral.CAMPO_ALFANUMERICO, "PROF", strCodiContOfic, "CodigoOficinas=" & Val(strCodiOfic))
                '    strFuente = strCodiContOfic
                '    strFuenteAnulacion = strCodiContOfic
                'ElseIf (TipoDocumento = TIDO_COMPROBANTE_EGRESO Or TipoDocumento = TIDO_COMPROBANTE_INGRESO) Then ' And bolManejoPrefijoUsuario Then
                '    If TipoDocumento = TIDO_COMPROBANTE_EGRESO Then
                '        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Usuarios", "Prefijo_Contable_Egreso", "Codigo", clsGeneral.CAMPO_ALFANUMERICO, sdrArchInte("USUA_Crea"), strPrefUsua)
                '    Else
                '        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Usuarios", "Prefijo_Contable_Ingreso", "Codigo", clsGeneral.CAMPO_ALFANUMERICO, sdrArchInte("USUA_Crea"), strPrefUsua)
                '    End If
                '    strFuente = strPrefUsua
                '    strFuenteAnulacion = strPrefUsua

                'ElseIf TipoDocumento = TIDO_LEGALIZACION_GASTOS Then
                '    Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Encabezado_Gasto_Conductores", "USUA_Crea", "Numero", clsGeneral.CAMPO_NUMERICO, Val(sdrArchInte("Numero_Documento")), strUsuarioCreaDocumento)
                '        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Encabezado_Gasto_Conductores", "OFIC_Codigo", "Numero", clsGeneral.CAMPO_NUMERICO, Val(sdrArchInte("Numero_Documento")), strCodiOfic)
                '        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Encabezado_Gasto_Conductores", "ENMA_Numero", "Numero", clsGeneral.CAMPO_NUMERICO, Val(sdrArchInte("Numero_Documento")), strNumeroManifiesto)
                '        strNumeroManifiesto = "Manif. " & strNumeroManifiesto
                '    End If

                '    If Val(sdrArchInte("Anulado")) = clsGeneral.UNO Then
                '        strFuenteAnulacion = sdrArchInte("Sufijo_Codigo_Anexo").ToString()
                '    End If


                ' TIPO COMPROBANTE -> 001 - 001
                If Val(sdrArchInte("Anulado")) = 0 Then
                    strCampo = Mid(strFuente, 1, 1)
                Else
                    strCampo = Mid(strFuenteAnulacion, 1, 1)
                End If
                strFila = strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CODIGO COMPROBANTE -> 002 - 004
                If Val(sdrArchInte("Anulado")) = 0 Then
                    strCampo = Mid(strFuente, 2, 3)
                    Me.Formatear_Campo(strCampo, 3, CAMPO_CEROS_IZQUIERDA)
                Else
                    strCampo = Mid(strFuenteAnulacion, 2, 3)
                    Me.Formatear_Campo(strCampo, 3, CAMPO_CEROS_IZQUIERDA)
                End If
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' NUMERO DOCUMENTO -> 005 - 015
                strCampo = Me.Formatear_Campo(lonNumeroDocumento, 11, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' SECUENCIA -> 016 - 020
                strCampo = Me.Formatear_Campo(intSecuencia, 5, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' NIT -> 021 - 033
                If sdrArchInte("Exige_Tercero").ToString() = VALOR_UNO_SIIGO_WINDOWS Then
                    strIdentificacion = sdrArchInte("Numero_Identificacion").ToString() '& Me.objTercero.DigitoChequeo
                Else
                    strIdentificacion = VACIO_NUMERICO_SIIGO_WINDOWS
                End If
                strCampo = Me.Formatear_Campo(strIdentificacion, 13, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' SUCURSAL -> 034 - 036
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 3, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CUENTA CONTABLE -> 037 - 046
                strCampo = Me.Formatear_Campo(sdrArchInte("Codigo_Cuenta").ToString, 10, CAMPO_CEROS_DERECHA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CODIGO PRODUCTO -> 047 - 059
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 13, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' FECHA DEL DOCUMENTO -> 060 - 067
                strCampo = Format(sdrArchInte("Fecha_Documento"), FORMATO_FECHA_SIIGO_WINDOWS)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CENTRO DE COSTO -> 068 - 071
                If sdrArchInte("Exige_Centro_Costo").ToString() = VALOR_UNO_SIIGO_WINDOWS Then
                    strCampo = Me.Formatear_Campo(sdrArchInte("Centro_Costo"), 4, CAMPO_CEROS_IZQUIERDA)
                Else
                    strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 4, CAMPO_CEROS_IZQUIERDA)
                End If

                'If bolManejoPrefijoUsuario Then

                If TipoDocumento = TIDO_COMPROBANTE_EGRESO Then

                    'Información Remesa
                    strCodiDoor = sdrArchInte("CATA_DOOR_Codigo").ToString()
                    strDocuOrig = sdrArchInte("Documento_Origen").ToString()
                    'End If

                    If strCodiDoor = DOOR_ANTICIPO_MANIFIESTO Then  'trae placa y codigo de la placa
                        'Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Encabezado_Manifiestos", "VEHI_Placa", "Numero", clsGeneral.CAMPO_NUMERICO, Val(strDocuOrig), strPlacVehi)
                        'Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Vehiculos", "Codigo", "Placa", clsGeneral.CAMPO_ALFANUMERICO, strPlacVehi, strCodiVehi)
                        strPlacVehi = sdrArchInte("Placa").ToString()
                        'strCampo = Me.Formatear_Campo(strCodiVehi, 4, CAMPO_CEROS_IZQUIERDA)
                        strCampo = Me.Formatear_Campo(strPlacVehi, 4, CAMPO_CEROS_IZQUIERDA)
                        strNumeroManifiesto = "Manif. " & strDocuOrig

                        'ElseIf strCodiDoor = DOOR_LIQUIDACION_MANIFIESTO Then
                        '    Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Encabezado_Liquida_Manifiestos", "VEHI_Placa", "Numero", clsGeneral.CAMPO_NUMERICO, Val(strDocuOrig), strPlacVehi)
                        '    Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Vehiculos", "Codigo", "Placa", clsGeneral.CAMPO_ALFANUMERICO, strPlacVehi, strCodiVehi)
                        '    strCampo = Me.Formatear_Campo(strCodiVehi, 4, CAMPO_CEROS_IZQUIERDA)
                    ElseIf strCodiDoor = DOOR_LIQUIDACION_CONDUCTOR Then
                        'Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Detalle_Manifiesto_Liquida_Conductores", "ENMA_Numero", "ENLC_Numero", clsGeneral.CAMPO_NUMERICO, Val(strDocuOrig), strNumeMani)
                        'Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Encabezado_Manifiestos", "VEHI_Placa", "Numero", clsGeneral.CAMPO_NUMERICO, Val(strNumeMani), strPlacVehi)
                        'Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Vehiculos", "Codigo", "Placa", clsGeneral.CAMPO_ALFANUMERICO, strPlacVehi, strCodiVehi)
                        strPlacVehi = sdrArchInte("Placa").ToString()
                        strCampo = Me.Formatear_Campo(strPlacVehi, 4, CAMPO_CEROS_IZQUIERDA)
                    Else
                        strCampo = Me.Formatear_Campo(sdrArchInte("Centro_Costo"), 4, CAMPO_CEROS_IZQUIERDA)
                    End If

                ElseIf TipoDocumento = TIDO_COMPROBANTE_INGRESO Then
                    Dim arrCampos(1), arrValoresCampos(1) As String
                    strCodiDoor = sdrArchInte("CATA_DOOR_Codigo").ToString()
                    strDocuOrig = sdrArchInte("Documento_Origen").ToString()

                    If Val(strDocuOrig) > 1 Then ' cuando no existe un número de documento origen específico, por defecto se asigna el valor de 1
                        Dim strNomDocOrg As String = sdrArchInte("Nombre_Documento_Origen").ToString()
                        strNumeroManifiesto = strNomDocOrg & ": " & strDocuOrig
                    End If
                    strCampo = Me.Formatear_Campo(sdrArchInte("Centro_Costo"), 4, CAMPO_CEROS_IZQUIERDA)

                ElseIf TipoDocumento = TIDO_LEGALIZACION_GASTOS Then
                    strPlacVehi = sdrArchInte("Placa").ToString()
                    'Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Encabezado_Gasto_Conductores", "VEHI_Placa", "Numero", clsGeneral.CAMPO_NUMERICO, lonNumeroDocumento, strPlacVehi)
                    'Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Vehiculos", "Codigo", "Placa", clsGeneral.CAMPO_ALFANUMERICO, strPlacVehi, strCodiVehi)
                    strCampo = Me.Formatear_Campo(strPlacVehi, 4, CAMPO_CEROS_IZQUIERDA)

                ElseIf TipoDocumento = TIDO_LIQUIDA_MANIFIESTO_TERCEROS Then
                    strCodiContOfic = sdrArchInte("Prefijo_Oficina").ToString
                    'Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Prefijo_Oficinas", "CentroCosto", "CATA_Codigo", clsGeneral.CAMPO_ALFANUMERICO, "PROF", strCodiContOfic, "TipoDocumento=" & TipoDocumento & " AND CodigoOficinas=" & Val(strCodiOfic))
                    strCampo = Me.Formatear_Campo(strCodiContOfic, 3, CAMPO_CEROS_IZQUIERDA)
                Else
                    strCampo = Me.Formatear_Campo(sdrArchInte("Centro_Costo"), 4, CAMPO_CEROS_IZQUIERDA)
                End If
                'End If
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' SUBCENTRO DE COSTO -> 072 - 074
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 3, CAMPO_CEROS_IZQUIERDA)

                'Para Multigranel
                '001 - Soacha Oficina Principal
                '002 - Soacha Agencia 
                '100 - Soledad
                '105 - Buenaventura

                'COMENTARIOS SC PREFIJO POR USUARIO VERSION 20 
                'If TipoDocumento = TIDO_COMPROBANTE_EGRESO Or TipoDocumento = TIDO_COMPROBANTE_INGRESO Then
                '    'If bolManejoPrefijoUsuario Then
                '    strPrefUsua = ""
                '    Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Usuarios", "Prefijo_Contable_Egreso", "Codigo", clsGeneral.CAMPO_ALFANUMERICO, sdrArchInte("USUA_Crea"), strPrefUsua)
                '    If strPrefUsua = "" Then
                '        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Prefijo_Oficinas", "SubCentro", "CATA_Codigo", clsGeneral.CAMPO_ALFANUMERICO, "PROF", strCodiContOfic, "CodigoOficinas=" & Val(strCodiOfic))
                '        strCampo = Me.Formatear_Campo(strCodiContOfic, 3, CAMPO_CEROS_IZQUIERDA)
                '    Else
                '        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Valor_Catalogos", "Campo3", "CATA_Codigo", clsGeneral.CAMPO_ALFANUMERICO, "SUUS", strCodiContOfic, "Campo2='" & strPrefUsua & "'")
                '        'Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Prefijo_Oficinas", "SubCentro", "CATA_Codigo", clsGeneral.CAMPO_ALFANUMERICO, "PROF", strCodiContOfic, "CodigoOficinas=100")
                '        strCampo = Me.Formatear_Campo(strCodiContOfic, 3, CAMPO_CEROS_IZQUIERDA)
                '    End If
                '    'End If
                'ElseIf TipoDocumento = TIDO_LIQUIDA_MANIFIESTO_TERCEROS Then
                '    strCodiContOfic = ""
                '    Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Valor_Catalogos", "Campo3", "CATA_Codigo", clsGeneral.CAMPO_ALFANUMERICO, "SUUS", strCodiContOfic, "Campo2='" & strPrefUsua & "'")
                '    'Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Prefijo_Oficinas", "SubCentro", "CATA_Codigo", clsGeneral.CAMPO_ALFANUMERICO, "PROF", strCodiContOfic, "TipoDocumento=" & TipoDocumento & " AND CodigoOficinas=" & Val(strCodiOfic))
                '    strCampo = Me.Formatear_Campo(strCodiContOfic, 3, CAMPO_CEROS_IZQUIERDA)
                'ElseIf TipoDocumento = TIDO_LEGALIZACION_GASTOS Then
                '    strCodiContOfic = ""
                '        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Usuarios", "Prefijo_Contable_Egreso", "Codigo", clsGeneral.CAMPO_ALFANUMERICO, strUsuarioCreaDocumento, strPrefUsua)
                '        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Valor_Catalogos", "Campo3", "CATA_Codigo", clsGeneral.CAMPO_ALFANUMERICO, "SUUS", strCodiContOfic, "Campo2='" & strPrefUsua & "'")
                '    'Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Prefijo_Oficinas", "SubCentro", "CATA_Codigo", clsGeneral.CAMPO_ALFANUMERICO, "PROF", strCodiContOfic, "TipoDocumento=" & TipoDocumento & " AND CodigoOficinas=" & Val(strCodiOfic))
                '    strCampo = Me.Formatear_Campo(strCodiContOfic, 3, CAMPO_CEROS_IZQUIERDA)
                'End If

                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' DESCRIPCION DEL MOVIMIENTO -> 075 - 124
                If sdrArchInte("Observaciones") <> "" Then
                    strCampo = sdrArchInte("Observaciones")
                Else
                    strCampo = sdrArchInte("NombreCuenta")
                End If

                'validación para Multigranel para que prime el número del manifiesto en las observaciones

                If strNumeroManifiesto <> "" Then
                    strCampo = strNumeroManifiesto & " " & strCampo
                End If

                strCampo = Me.Formatear_Campo(strCampo, 50, CAMPO_CARACTER)

                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS

                ' DEBITO O CREDITO -> 125 - 125
                If dblValorDebito <> 0 Then
                    strCampo = TIPO_MOVIMIENTO_DEBITO_SIIGO_WINDOWS
                Else
                    strCampo = TIPO_MOVIMIENTO_CREDITO_SIIGO_WINDOWS
                End If

                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' VALOR DEL MOVIMIENTO -> 126 - 140
                If dblValorDebito <> 0 Then
                    strCampo = Me.Formatear_Campo(dblValorDebito, 15, CAMPO_DECIMAL, 2)
                Else
                    strCampo = Me.Formatear_Campo(dblValorCredito, 15, CAMPO_DECIMAL, 2)
                End If

                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' BASE DE RETENCIÓN -> 141 - 155
                If sdrArchInte("Exige_Valor_Base").ToString() = VALOR_UNO_SIIGO_WINDOWS Then
                    strCampo = Me.Formatear_Campo(sdrArchInte("Valor_Base"), 15, CAMPO_DECIMAL, 2)
                Else
                    strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 15, CAMPO_DECIMAL, 2)
                End If
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CÓDIGO DEL VENDEDOR -> 156 - 159
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 4, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CÓDIGO DE LA CIUDAD -> 160 - 163
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 4, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CÓDIGO DE LA ZONA -> 164 - 166
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 3, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CÓDIGO DE LA BODEGA -> 167 - 170
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 4, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CÓDIGO DE LA UBICACION -> 171 - 173
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 3, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CANTIDAD -> 174 - 188
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 15, CAMPO_DECIMAL, 5)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS

                If sdrArchInte("Exige_Documento_Cruce").ToString() = VALOR_UNO_SIIGO_WINDOWS Then

                    strCampo = Me.Formatear_Campo(Mid(strFuente, 1, 1), 1, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' CODIGO COMPROBANTE CRUCE -> 190 - 192
                    strCampo = Me.Formatear_Campo(Mid(strFuente, 2, 3), 3, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' NUMERO DOCUMENTO CRUCE -> 193 - 203
                    strCampo = Me.Formatear_Campo(sdrArchInte("Documento_Cruce"), 11, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' SECUENCIA DOCUMENTO CRUCE -> 204 - 206
                    strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 3, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' FECHA VENCIMIENTO DOCUMENTO CRUCE -> 207 - 214
                    strCampo = Format(sdrArchInte("Fecha_Documento"), FORMATO_FECHA_SIIGO_WINDOWS)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' CODIGO FORMA DE PAGO -> 215 - 218
                    strFormaPagoContable = clsGeneral.UNO
                    strCampo = Me.Formatear_Campo(strFormaPagoContable, 4, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                Else
                    ' TIPO COMPROBANTE CRUCE -> 189 - 189
                    strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 1, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' CODIGO COMPROBANTE CRUCE -> 190 - 192
                    strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 3, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' NUMERO DOCUMENTO CRUCE -> 193 - 203
                    strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 11, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' SECUENCIA DOCUMENTO CRUCE -> 204 - 206
                    strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 3, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS

                    strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 8, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' CODIGO FORMA DE PAGO -> 215 - 218
                    strFormaPagoContable = VACIO_NUMERICO_SIIGO_WINDOWS
                    strCampo = Me.Formatear_Campo(strFormaPagoContable, 4, CAMPO_CEROS_IZQUIERDA)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                End If

                ' CODIGO DEL BANCO -> 219 - 220
                strCodigoBanco = VACIO_NUMERICO_SIIGO_WINDOWS
                strCampo = Me.Formatear_Campo(strCodigoBanco, 2, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS

                stwStreamWriter.WriteLine(strFila)

            End While

            sdrArchInte.Close()
            stwStreamWriter.Close()
            stmStreamW.Close()
            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If

            If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
                Call Marcar_Registros_Interfaz_Definitivo("Encabezado_Comprobante_Contables", TipoDocumento, intOficina)
            End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
            Me.strError = ex.Message
            Return False
        End Try

    End Function

    Private Function Marcar_Registros_Interfaz_Definitivo(ByVal Tabla As String, ByVal TipoDocumento As Integer, ByVal Oficina As Integer) As Boolean
        Try

            Dim strSQL As String = ""

            strSQL = "UPDATE " & Tabla
            strSQL += " SET Interfaz_Contable = " & ARCHIVO_DEFINITIVO
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa
            strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO
            'strSQL += " AND DATEDIFF(d,Fecha,CONVERT(DATETIME, '" & objGeneral.Formatear_Fecha_SQL(Me.dteFechaInicial) & "', 101)) <=0" & Chr(13)
            'strSQL += " AND DATEDIFF(d,Fecha,CONVERT(DATETIME, '" & objGeneral.Formatear_Fecha_SQL(Me.dteFechaFinal) & "', 101)) >=0" & Chr(13)
            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
            strSQL += " AND TIDO_Codigo = " & intDocumentoContable
            strSQL += " AND TIDO_Documento = " & TipoDocumento
            strSQL += " AND Interfaz_Contable = " & ARCHIVO_BORRADOR

            If Oficina <> intOficina Then
                strSQL += " AND OFIC_Codigo = " & Oficina & Chr(13)
            End If

            Marcar_Registros_Interfaz_Definitivo = True
            Me.objGeneral.Ejecutar_SQL(strSQL)

        Catch ex As Exception
            Marcar_Registros_Interfaz_Definitivo = False
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
            End Try
        End Try

    End Function

    Private Function Marcar_Registros_Interfaz_Definitivo_Factura_WORLD_OFFICE(ByVal intOficina As Integer) As Boolean
        Try

            Dim strSQL As String = ""

            strSQL = "UPDATE Encabezado_Facturas"
            strSQL += " SET Interfaz_Contable_Factura = " & ARCHIVO_DEFINITIVO
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString
            strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO
            'strSQL += " AND Fecha >= CONVERT(DATETIME, '" & objGeneral.Formatear_Fecha_SQL(Me.dtaFechaInicial) & "', 101)"
            'strSQL += " AND Fecha <= CONVERT(DATETIME, '" & objGeneral.Formatear_Fecha_SQL(Me.dtaFechaFinal) & "', 101)"
            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
            strSQL += " AND Interfaz_Contable_Factura = " & ARCHIVO_BORRADOR

            If intOficina <> 0 Then
                strSQL += " AND OFIC_Factura = " & intOficina & Chr(13)
            End If

            Marcar_Registros_Interfaz_Definitivo_Factura_WORLD_OFFICE = True
            Me.objGeneral.Ejecutar_SQL(strSQL)

        Catch ex As Exception
            Marcar_Registros_Interfaz_Definitivo_Factura_WORLD_OFFICE = False
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
            End Try
        End Try

    End Function


#Region "Funciones Utilidades"

    Private Function Formatear_Campo(ByVal strValor As String, ByVal intLongitud As Integer, ByVal intTipoCampo As Integer, Optional ByVal intPresicion As Integer = 0) As String


        On Error Resume Next
        Dim intCon%, strFormato$, intPos%, strValorEntero$, strValorDecimal$, intLon%, strSignoNegativo$, intLongSignoNegativo%
        Dim strCampo$ = ""

        strFormato = ""
        Formatear_Campo = ""
        strSignoNegativo$ = ""
        strValorDecimal = 0

        strValor = strValor.Replace(Chr(10).ToString, "")
        strValor = strValor.Replace(CARACTER_ENTER, "")

        strValor = Trim$(strValor)

        If intTipoCampo = CAMPO_CEROS_IZQUIERDA Then
            strValor = Trim$(strValor)
            intLon = Len(strValor)

            For intCon = intLon To intLongitud - 1
                strValor = "0" & strValor
            Next
            Formatear_Campo = strValor

        ElseIf intTipoCampo = CAMPO_CEROS_DERECHA Then
            strValor = Trim$(strValor)
            intLon = Len(strValor)

            For intCon = intLon + 1 To intLongitud
                strValor = strValor & "0"
            Next
            Formatear_Campo = strValor

        ElseIf intTipoCampo = CAMPO_CARACTER_CEROS Then
            ' Si es vacio
            If strValor = "" Then
                For intCon = 1 To intLongitud
                    strValor = strValor & "0"
                Next
            End If
            Formatear_Campo = strValor

        ElseIf intTipoCampo = CAMPO_NUMERICO Then

            ' Si es vacio
            If strValor = "" Then
                For intCon = 1 To intLongitud
                    strFormato = strFormato & "0"
                Next
                Formatear_Campo = strFormato

            Else
                ' Buscar signo negativo
                intPos = InStr(strValor, "-")
                If intPos > 0 Then
                    strSignoNegativo = "-"
                    intLongSignoNegativo = 1
                    strValor = Mid$(strValor, 2)
                End If

                For intCon = 1 To intLongitud - intLongSignoNegativo
                    strFormato = strFormato & "0"
                Next

                Formatear_Campo = strSignoNegativo & Format$(strValor)

            End If

        ElseIf intTipoCampo = CAMPO_DECIMAL_CON_PUNTO_UNOE Then

            ' Si es vacio
            If strValor = "" Then
                For intCon = 1 To intLongitud
                    strFormato = strFormato & "0"
                Next
                Formatear_Campo = strFormato

            Else
                ' Buscar signo negativo
                intPos = InStr(strValor, "-")
                If intPos > 0 Then
                    strSignoNegativo = "-"
                    intLongSignoNegativo = 1
                    strValor = Mid$(strValor, 2)
                End If

                ' Buscar signo decimal
                intPos = InStr(strValor, ".")
                If intPos > 0 Then
                    strValorEntero = Mid$(strValor, 1, intPos - 1)
                    strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                Else
                    strValorEntero = strValor
                    strValorDecimal = "0000"
                End If
                strValorEntero = Trim$(strValorEntero)
                strValorDecimal = Trim$(strValorDecimal)

                ' Formatear Entero
                For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                    strFormato = strFormato & "0"
                Next
                If strValorEntero = "" Then
                    strValorEntero = strFormato
                Else
                    strValorEntero = Format(Val(strValorEntero), strFormato)
                End If

                ' Formatear Decimal
                strFormato = ""
                intLon = Len(strValorDecimal)
                If intLon < intPresicion Then
                    ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                    For intCon = intLon To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                ElseIf intLon = 0 Then
                    ' Si la longitud es 0 se llena el valor decimal con ceros
                    For intCon = 0 To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                End If

                Formatear_Campo = "+" & strSignoNegativo & strValorEntero & "." & strValorDecimal

            End If

        ElseIf intTipoCampo = CAMPO_NUMERICO_UNOE_85 Then
            ' Buscar signo negativo
            intPos = InStr(strValor, "-")
            If intPos > 0 Then
                strSignoNegativo = "-"
                intLongSignoNegativo = 1
                strValor = Mid$(strValor, 2)
            End If

            ' Buscar signo decimal
            intPos = InStr(strValor, ".")
            If intPos > 0 Then
                strValorEntero = Mid$(strValor, 1, intPos - 1)
                strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
            Else
                strValorEntero = strValor
                If strValorEntero = "0" Then
                    strValorDecimal = ""
                Else
                    strValorDecimal = "0000"
                End If
            End If
            strValorEntero = Trim$(strValorEntero)
            strValorDecimal = Trim$(strValorDecimal)

            ' Formatear Entero
            For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                strFormato = strFormato & "0"
            Next
            If strValorEntero = "" Then
                strValorEntero = strFormato
            Else
                strValorEntero = Format(Val(strValorEntero), strFormato)
            End If

            ' Formatear Decimal
            strFormato = ""
            intLon = Len(strValorDecimal)
            If intLon < intPresicion Then
                ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                For intCon = intLon To intPresicion - 1
                    strValorDecimal = strValorDecimal & "0"
                Next

            ElseIf intLon = 0 Then
                ' Si la longitud es 0 se llena el valor decimal con ceros
                For intCon = 0 To intPresicion - 1
                    strValorDecimal = strValorDecimal & "0"
                Next

            End If

            Formatear_Campo = strValorEntero & "+"

        ElseIf intTipoCampo = CAMPO_DECIMAL_UNOE_85 Then
            ' Buscar signo negativo
            intPos = InStr(strValor, "-")
            If intPos > 0 Then
                strSignoNegativo = "-"
                intLongSignoNegativo = 1
                strValor = Mid$(strValor, 2)
            End If

            ' Buscar signo decimal
            intPos = InStr(strValor, ".")
            If intPos > 0 Then
                strValorEntero = Mid$(strValor, 1, intPos - 1)
                strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
            Else
                strValorEntero = strValor
                If strValorEntero = "0" Then
                    strValorDecimal = ""
                Else
                    strValorDecimal = "0000"
                End If
            End If
            strValorEntero = Trim$(strValorEntero)
            strValorDecimal = Trim$(strValorDecimal)

            ' Formatear Entero
            For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                strFormato = strFormato & "0"
            Next
            If strValorEntero = "" Then
                strValorEntero = strFormato
            Else
                strValorEntero = Format(Val(strValorEntero), strFormato)
            End If

            ' Formatear Decimal
            strFormato = ""
            intLon = Len(strValorDecimal)
            If intLon < intPresicion Then
                ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                For intCon = intLon To intPresicion - 1
                    strValorDecimal = strValorDecimal & "0"
                Next

            ElseIf intLon = 0 Then
                ' Si la longitud es 0 se llena el valor decimal con ceros
                For intCon = 0 To intPresicion - 1
                    strValorDecimal = strValorDecimal & "0"
                Next

            End If

            Formatear_Campo = strValorEntero & "00+"


        ElseIf intTipoCampo = CAMPO_CARACTER Then ' Blancos a la derecha

            For intCon = 1 To intLongitud
                If Mid$(strValor, intCon, 1) <> Chr(13) Then
                    strCampo = strCampo & Mid$(strValor, intCon, 1)
                End If
            Next

            If Len(strCampo) > intLongitud Then
                strValor = Mid$(strCampo, 1, intLongitud)
            Else
                strValor = strCampo
            End If

            Formatear_Campo = strValor & Space$(intLongitud - Len(strValor))

        ElseIf intTipoCampo = CAMPO_DECIMAL Then ' Ceros a la izquierda, con el número de decimales, sin punto ni comas

            ' Si es vacio
            If strValor = "" Then
                For intCon = 1 To intLongitud
                    strFormato = strFormato & "0"
                Next
                Formatear_Campo = strFormato

            Else

                ' Buscar signo negativo
                intPos = InStr(strValor, "-")
                If intPos > 0 Then
                    strSignoNegativo = "-"
                    intLongSignoNegativo = 1
                    strValor = Mid$(strValor, 2)
                End If

                ' Buscar signo decimal
                intPos = InStr(strValor, ".")
                If intPos > 0 Then
                    strValorEntero = Mid$(strValor, 1, intPos - 1)
                    strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                Else
                    strValorEntero = strValor
                    strValorDecimal = "00"
                End If
                strValorEntero = Trim$(strValorEntero)
                strValorDecimal = Trim$(strValorDecimal)

                ' Formatear Entero
                For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                    strFormato = strFormato & "0"
                Next
                If strValorEntero = "" Then
                    strValorEntero = strFormato
                Else
                    strValorEntero = Format(Val(strValorEntero), strFormato)
                End If

                ' Formatear Decimal
                strFormato = ""
                intLon = Len(strValorDecimal)
                If intLon < intPresicion Then
                    ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                    For intCon = intLon To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                ElseIf intLon = 0 Then
                    ' Si la longitud es 0 se llena el valor decimal con ceros
                    For intCon = 0 To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                End If

                Formatear_Campo = strSignoNegativo & strValorEntero & strValorDecimal

            End If

        ElseIf intTipoCampo = CAMPO_FECHA Then
            Formatear_Campo = Format$(strValor, "DD/MM/YYYY")

        ElseIf intTipoCampo = CAMPO_IDENTIFICACION Then ' Espacios a la Derecha. (1-2) dependiendo del digito de chequeo
            Dim intCerosAgregar As Short
            intCerosAgregar = intLongitud - Len(strValor)
            For intCon = 1 To intCerosAgregar
                Formatear_Campo += " "
            Next
            Formatear_Campo += strValor

        ElseIf intTipoCampo = CAMPO_ESPACIOS_DERECHA Then
            Dim intEspacioAgregar As Short
            Dim strEspacios As String = ""
            intEspacioAgregar = intLongitud - Len(strValor)
            For intCon = 1 To intEspacioAgregar
                strEspacios += " "
            Next
            Formatear_Campo = strValor & strEspacios



        ElseIf intTipoCampo = CAMPO_ESPACIOS_IZQUIERDA Then
            Dim intEspacioAgregar As Short
            intEspacioAgregar = intLongitud - Len(strValor)
            For intCon = 1 To intEspacioAgregar
                Formatear_Campo += " "
            Next
            Formatear_Campo += strValor

        ElseIf intTipoCampo = CAMPO_DECIMAL_CON_PUNTO Then

            ' Si es vacio
            If strValor = "" Then
                For intCon = 1 To intLongitud
                    strFormato = strFormato & "0"
                Next
                Formatear_Campo = strFormato

            Else
                ' Buscar signo negativo
                intPos = InStr(strValor, "-")
                If intPos > 0 Then
                    strSignoNegativo = "-"
                    intLongSignoNegativo = 1
                    strValor = Mid$(strValor, 2)
                End If

                ' Buscar signo decimal
                intPos = InStr(strValor, ".")
                If intPos > 0 Then
                    strValorEntero = Mid$(strValor, 1, intPos - 1)
                    strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                Else
                    strValorEntero = strValor
                    strValorDecimal = Trim$(strValorDecimal)
                End If
                strValorEntero = Trim$(strValorEntero)
                strValorDecimal = Trim$(strValorDecimal)

                ' Formatear Entero
                For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                    strFormato = strFormato & "0"
                Next
                If strValorEntero = "" Then
                    strValorEntero = strFormato
                Else
                    strValorEntero = Format(Val(strValorEntero), strFormato)
                End If

                ' Formatear Decimal
                strFormato = ""
                intLon = Len(strValorDecimal)
                If intLon < intPresicion Then
                    ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                    For intCon = intLon To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                ElseIf intLon = 0 Then
                    ' Si la longitud es 0 se llena el valor decimal con ceros
                    For intCon = 0 To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                End If

                Formatear_Campo = strSignoNegativo & strValorEntero & "." & strValorDecimal
                Formatear_Campo = Trim(Formatear_Campo)

            End If

        ElseIf intTipoCampo = SOLO_CAMPO_DECIMAL_CON_PUNTO Then ' Formatea el campo con decimales con punto y con espacios a la izquierda

            ' Si es vacio
            If strValor = "" Then
                For intCon = 1 To intLongitud
                    strFormato = strFormato & " "
                Next
                Formatear_Campo = strFormato

            Else
                ' Buscar signo negativo
                intPos = InStr(strValor, "-")
                If intPos > 0 Then
                    strSignoNegativo = "-"
                    intLongSignoNegativo = 1
                    strValor = Mid$(strValor, 2)
                End If

                ' Buscar signo decimal
                intPos = InStr(strValor, ".")
                If intPos > 0 Then
                    strValorEntero = Mid$(strValor, 1, intPos - 1)
                    strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                Else
                    strValorEntero = strValor
                    strValorDecimal = Trim$(strValorDecimal)
                End If
                strValorEntero = Trim$(strValorEntero)
                strValorDecimal = Trim$(strValorDecimal)

                ' Formatear Entero
                Dim intEspacioAgregar As Short
                intEspacioAgregar = intLongitud - Len(strValorEntero)
                For intCon = 1 To intEspacioAgregar - intPresicion - intLongSignoNegativo - 1 '-1 POR EL PUNTO
                    strFormato = strFormato & " "
                Next
                If strValorEntero = "" Then
                    strValorEntero = strFormato
                Else
                    strValorEntero = strFormato & strValorEntero
                    ' strValorEntero = Format(Val(strValorEntero), strFormato)
                End If

                ' Formatear Decimal
                strFormato = ""
                intLon = Len(strValorDecimal)
                If intLon < intPresicion Then
                    ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                    For intCon = intLon To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                ElseIf intLon = 0 Then
                    ' Si la longitud es 0 se llena el valor decimal con ceros
                    For intCon = 0 To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                End If

                Formatear_Campo = strSignoNegativo & strValorEntero & "." & strValorDecimal

            End If

        End If

    End Function

    Private Function Formatear_Campo_SIIGO(ByVal strValor As String, ByVal intLongitud As Integer, ByVal intTipoCampo As Integer, Optional ByVal intPresicion As Integer = 0) As String
        Try
            Formatear_Campo_SIIGO = String.Empty
            strValor = strValor.Replace(Chr(10).ToString, "")
            strValor = strValor.Replace(CARACTER_ENTER, "")

            Dim intCon%, strFormato$, intPos%, strValorEntero$, strValorDecimal$, intLon%, intLongSignoNegativo%
            Dim strSignoNegativo$ = String.Empty

            strFormato = ""
            strValor = Trim$(strValor)

            If intTipoCampo = CAMPO_CEROS_DERECHA_SIIGO Then
                strValor = Trim$(strValor)
                intLon = Len(strValor)

                For intCon = intLon To intLongitud - 1
                    strValor = strValor & "0"
                Next
                Formatear_Campo_SIIGO = strValor

            ElseIf intTipoCampo = CAMPO_NUMERICO_SIIGO Then

                ' Si es vacio
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    Formatear_Campo_SIIGO = strFormato

                Else
                    ' Buscar signo negativo
                    intPos = InStr(strValor, "-")
                    If intPos > 0 Then
                        strSignoNegativo = "-"
                        intLongSignoNegativo = 1
                        strValor = Mid$(strValor, 2)
                    End If

                    For intCon = 1 To intLongitud - intLongSignoNegativo
                        strFormato = strFormato & "0"
                    Next

                    Formatear_Campo_SIIGO = strSignoNegativo & Format$(Val(strValor), strFormato)

                End If

            ElseIf intTipoCampo = CAMPO_ALFANUMERICO_SIIGO Then

                strValor = strValor.Replace(Chr(10).ToString, "")
                strValor = strValor.Replace(CARACTER_ENTER, "")

                If Len(strValor) > intLongitud Then
                    strValor = Mid$(strValor, 1, intLongitud)
                End If
                Formatear_Campo_SIIGO = strValor & Space$(intLongitud - Len(strValor))


            ElseIf intTipoCampo = CAMPO_DECIMAL_SIIGO Then

                ' Si es vacio
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    Formatear_Campo_SIIGO = strFormato

                Else

                    ' Buscar signo negativo
                    intPos = InStr(strValor, "-")
                    If intPos > 0 Then
                        strSignoNegativo = "-"
                        intLongSignoNegativo = 1
                        strValor = Mid$(strValor, 2)
                    End If

                    ' Buscar signo decimal
                    intPos = InStr(strValor, ".")
                    If intPos > 0 Then
                        strValorEntero = Mid$(strValor, 1, intPos - 1)
                        strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                    Else
                        strValorEntero = strValor
                        strValorDecimal = ""
                    End If
                    strValorEntero = Trim$(strValorEntero)
                    strValorDecimal = Trim$(strValorDecimal)

                    ' Formatear Entero
                    For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                        strFormato = strFormato & "0"
                    Next

                    If strValorEntero = "" Then
                        strValorEntero = strFormato
                    Else
                        strValorEntero = Format$(Val(strValorEntero), strFormato)
                    End If

                    ' Formatear Decimal
                    strFormato = ""
                    intLon = Len(strValorDecimal)
                    If intLon < intPresicion Then
                        ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                        For intCon = intLon To intPresicion - 1
                            strValorDecimal = strValorDecimal & "0"
                        Next

                    ElseIf intLon = 0 Then
                        ' Si la longitud es 0 se llena el valor decimal con ceros
                        For intCon = 0 To intPresicion - 1
                            strValorDecimal = strValorDecimal & "0"
                        Next

                    End If

                    Formatear_Campo_SIIGO = strSignoNegativo & strValorEntero & "." & strValorDecimal

                End If

            ElseIf intTipoCampo = CAMPO_FECHA_SIIGO Then
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    Formatear_Campo_SIIGO = strFormato
                Else
                    Dim dteAuxiliar = CDate(strValor)
                    Dim strAuxiliar = Year(dteAuxiliar).ToString

                    If Month(dteAuxiliar).ToString.Length < 2 Then
                        strAuxiliar += "0" & Month(dteAuxiliar).ToString
                    Else
                        strAuxiliar += Month(dteAuxiliar).ToString
                    End If

                    If Day(dteAuxiliar).ToString.Length < 2 Then
                        strAuxiliar += "0" & Day(dteAuxiliar).ToString
                    Else
                        strAuxiliar += Day(dteAuxiliar).ToString
                    End If

                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next

                    Formatear_Campo_SIIGO = Format$(Val(strAuxiliar), strFormato)

                End If

            End If

            Formatear_Campo_SIIGO = Mid(Formatear_Campo_SIIGO, 1, intLongitud)

            Return Formatear_Campo_SIIGO
        Catch ex As Exception
            Return ""
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Formatear_Campo_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
        End Try
    End Function

    Private Function Formatear_Campo_UNO_EE(ByVal strValor As String, ByVal intLongitud As Integer, ByVal intTipoCampo As Integer, Optional ByVal intPresicion As Integer = 0) As String
        Try
            Formatear_Campo_UNO_EE = String.Empty
            strValor = strValor.Replace(Chr(10).ToString, "")
            strValor = strValor.Replace(CARACTER_ENTER, "")

            Dim intCon%, strFormato$, intPos%, strValorEntero$, strValorDecimal$, intLon%, intLongSignoNegativo%
            Dim strSignoNegativo$ = String.Empty
            Dim strSignoPositivo$ = String.Empty
            Dim strCaracterPunto$ = String.Empty

            strFormato = ""
            strValor = Trim$(strValor)

            If intTipoCampo = CAMPO_NUMERICO Then

                strValor = Trim$(strValor)
                intLon = Len(strValor)

                For intCon = intLon To intLongitud - 1
                    strValor = "0" & strValor
                Next
                Formatear_Campo_UNO_EE = strValor

            ElseIf intTipoCampo = CAMPO_DINERO Then
                strSignoPositivo = "+"
                strCaracterPunto = "."
                ' Si es vacio
                If strValor = "0" Then
                    strFormato = ""
                    For intCon = 1 To 15
                        strFormato = strFormato & "0"
                    Next

                    strValor = strSignoPositivo & strFormato & strCaracterPunto

                    strFormato = ""
                    For intCon = 1 To 4
                        strFormato = strFormato & "0"
                    Next

                    strValor = strValor & strFormato

                    If intLongitud - Len(strValor) > 0 Then
                        Formatear_Campo_UNO_EE = strValor & Space$(intLongitud - Len(strValor))
                    Else
                        Formatear_Campo_UNO_EE = strValor
                    End If


                Else
                    strFormato = ""
                    For intCon = 1 To 15 - Len(strValor)
                        strFormato = strFormato & "0"
                    Next

                    strValor = strSignoPositivo & strFormato & strValor & strCaracterPunto

                    strFormato = ""
                    For intCon = 1 To 4
                        strFormato = strFormato & "0"
                    Next

                    strValor = strValor & strFormato

                    If intLongitud - Len(strValor) > 0 Then
                        Formatear_Campo_UNO_EE = strValor & Space$(intLongitud - Len(strValor))
                    Else
                        Formatear_Campo_UNO_EE = strValor
                    End If

                End If
            ElseIf intTipoCampo = CAMPO_ALFANUMERICO Then

                strValor = strValor.Replace(Chr(10).ToString, "")
                strValor = strValor.Replace(CARACTER_ENTER, "")

                If Len(strValor) > intLongitud Then
                    strValor = Mid$(strValor, 1, intLongitud)
                End If

                Formatear_Campo_UNO_EE = strValor & Space$(intLongitud - Len(strValor))


            ElseIf intTipoCampo = CAMPO_DECIMAL Then

                ' Si es vacio
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    Formatear_Campo_UNO_EE = strFormato

                Else

                    ' Buscar signo negativo
                    intPos = InStr(strValor, "-")
                    If intPos > 0 Then
                        strSignoNegativo = "-"
                        intLongSignoNegativo = 1
                        strValor = Mid$(strValor, 2)
                    End If

                    ' Buscar signo decimal
                    intPos = InStr(strValor, ".")
                    If intPos > 0 Then
                        strValorEntero = Mid$(strValor, 1, intPos - 1)
                        strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                    Else
                        strValorEntero = strValor
                        strValorDecimal = ""
                    End If
                    strValorEntero = Trim$(strValorEntero)
                    strValorDecimal = Trim$(strValorDecimal)

                    ' Formatear Entero
                    For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                        strFormato = strFormato & "0"
                    Next

                    If strValorEntero = "" Then
                        strValorEntero = strFormato
                    Else
                        strValorEntero = Format$(Val(strValorEntero), strFormato)
                    End If

                    ' Formatear Decimal
                    strFormato = ""
                    intLon = Len(strValorDecimal)
                    If intLon < intPresicion Then
                        ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                        For intCon = intLon To intPresicion - 1
                            strValorDecimal = strValorDecimal & "0"
                        Next

                    ElseIf intLon = 0 Then
                        ' Si la longitud es 0 se llena el valor decimal con ceros
                        For intCon = 0 To intPresicion - 1
                            strValorDecimal = strValorDecimal & "0"
                        Next

                    End If

                    Formatear_Campo_UNO_EE = strSignoNegativo & strValorEntero & "." & strValorDecimal

                End If

            ElseIf intTipoCampo = CAMPO_FECHA Then
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    Formatear_Campo_UNO_EE = strFormato
                Else
                    Dim dteAuxiliar = CDate(strValor)
                    Dim strAuxiliar = Year(dteAuxiliar).ToString

                    If Month(dteAuxiliar).ToString.Length < 2 Then
                        strAuxiliar += "0" & Month(dteAuxiliar).ToString
                    Else
                        strAuxiliar += Month(dteAuxiliar).ToString
                    End If

                    If Day(dteAuxiliar).ToString.Length < 2 Then
                        strAuxiliar += "0" & Day(dteAuxiliar).ToString
                    Else
                        strAuxiliar += Day(dteAuxiliar).ToString
                    End If

                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next

                    Formatear_Campo_UNO_EE = Format$(Val(strAuxiliar), strFormato)

                End If

            End If

            Formatear_Campo_UNO_EE = Mid(Formatear_Campo_UNO_EE, 1, intLongitud)

            Return Formatear_Campo_UNO_EE
        Catch ex As Exception
            Return ""
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Formatear_Campo_UNO_EE: " & Me.objGeneral.Traducir_Error(ex.Message)
        End Try
    End Function

    Private Function Actualizar_Interfaz_Definitivo(ByVal strTabla As String, ByVal intTIDOCodigo As Integer) As Boolean

        Try
            Dim strSQL As String = ""

            strSQL = "UPDATE " & strTabla
            strSQL += " SET Interfase_Contable = " & ARCHIVO_DEFINITIVO
            strSQL += " WHERE EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO
            strSQL += " AND Fecha >= CONVERT(DATETIME, '" & objGeneral.Formatear_Fecha_SQL(Me.dtaFechaInicial) & "', 101)"
            strSQL += " AND Fecha <= CONVERT(DATETIME, '" & objGeneral.Formatear_Fecha_SQL(Me.dtaFechaFinal) & "', 101)"
            strSQL += " AND TIDO_Codigo = " & intTIDOCodigo
            strSQL += " AND Interfase_Contable = " & ARCHIVO_BORRADOR

            Me.objGeneral.Ejecutar_SQL(strSQL)
        Catch ex As Exception
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name

            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

            End Try
        End Try

        Return True
    End Function

#End Region


#Region "Funciones Consulta Información"

    'Private Function Retorna_Informacion_Liquidacion_OLD(ByVal lonNumeLiqu As Long, ByRef lonNumeDocuLiqu As Long, ByRef lonNumeDocuPlan As Long) As Boolean
    '    Dim objGeneral As New clsGeneral
    '    Try
    '        Dim strSQL As String, dtsLiquida As DataSet

    '        strSQL = "SELECT ELPD.Numero_Documento AS Numero_Liquidacion, ENPD.Numero_Documento AS Numero_Planilla"
    '        strSQL += " FROM Encabezado_Liquidacion_Planilla_Despachos AS ELPD,"
    '        strSQL += " Encabezado_Planilla_Despachos AS ENPD"
    '        strSQL += " WHERE ELPD.EMPR_Codigo = ENPD.EMPR_Codigo"
    '        strSQL += " AND ELPD.ENPD_Numero = ENPD.Numero"
    '        strSQL += " AND ELPD.EMPR_Codigo = " & intEmpresa.ToString
    '        strSQL += " AND ELPD.Numero = " & lonNumeLiqu.ToString

    '        dtsLiquida = objGeneral.Retorna_Dataset(strSQL)

    '        If dtsLiquida.Tables(0).Rows.Count > 0 Then

    '            lonNumeDocuLiqu = dtsLiquida.Tables(0).Rows(0).Item("Numero_Liquidacion").ToString()
    '            lonNumeDocuPlan = dtsLiquida.Tables(0).Rows(0).Item("Numero_Planilla").ToString()

    '            Retorna_Informacion_Liquidacion = True
    '        Else
    '            Retorna_Informacion_Liquidacion = False
    '        End If

    '    Catch ex As Exception
    '        Retorna_Informacion_Liquidacion = False
    '    End Try
    '    Return Retorna_Informacion_Liquidacion()

    'End Function

    Private Function Retorna_Informacion_Liquidacion(ByVal lonNumeLiqu As Long, ByRef lonNumeDocuLiqu As Long, ByRef lonNumeDocuPlan As Long) As Boolean
        Dim objGeneral As New clsGeneral
        Try
            Dim strSQL As String, dtsLiquida As DataSet

            strSQL = "SELECT ELPD.Numero_Documento AS Numero_Liquidacion, ENPD.Numero_Documento AS Numero_Planilla"
            strSQL += " FROM Encabezado_Liquidacion_Planilla_Despachos AS ELPD,"
            strSQL += " Encabezado_Planilla_Despachos AS ENPD"
            strSQL += " WHERE ELPD.EMPR_Codigo = ENPD.EMPR_Codigo"
            strSQL += " AND ELPD.ENPD_Numero = ENPD.Numero"
            strSQL += " AND ELPD.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ELPD.Numero = " & lonNumeLiqu.ToString

            dtsLiquida = objGeneral.Retorna_Dataset(strSQL)

            If dtsLiquida.Tables(0).Rows.Count > 0 Then

                lonNumeDocuLiqu = dtsLiquida.Tables(0).Rows(0).Item("Numero_Liquidacion").ToString()
                lonNumeDocuPlan = dtsLiquida.Tables(0).Rows(0).Item("Numero_Planilla").ToString()

                Retorna_Informacion_Liquidacion = True
            Else
                Retorna_Informacion_Liquidacion = False
            End If

        Catch ex As Exception
            Retorna_Informacion_Liquidacion = False
        End Try
        Return Retorna_Informacion_Liquidacion

    End Function

    Private Function Retorna_Egreso_Anticipo_Planilla(ByVal lonNumePlan As Long, ByRef lonNumeDocuEgre As Long) As Boolean
        Dim objGeneral As New clsGeneral
        Try
            Dim strSQL As String, dtsEgresos As DataSet

            strSQL = "SELECT Codigo, Numero"
            strSQL += " FROM Encabezado_Documento_Comprobantes AS ENDC"
            strSQL += " WHERE ENDC.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ENDC.CATA_DOOR_Codigo IN (2604, 2605)" ' ANTICIPOS Y SOBREANTICIPOS
            strSQL += " AND ENDC.Documento_Origen = " & lonNumePlan.ToString
            strSQL += " AND ENDC.Estado = 1"
            strSQL += " AND ENDC.Anulado = 0"

            dtsEgresos = objGeneral.Retorna_Dataset(strSQL)

            If dtsEgresos.Tables(0).Rows.Count > 0 Then

                lonNumeDocuEgre = dtsEgresos.Tables(0).Rows(0).Item("Numero").ToString()
                Retorna_Egreso_Anticipo_Planilla = True
            Else
                lonNumeDocuEgre = 0
                Retorna_Egreso_Anticipo_Planilla = False
            End If

        Catch ex As Exception
            Retorna_Egreso_Anticipo_Planilla = False
        End Try
        Return Retorna_Egreso_Anticipo_Planilla

    End Function

    Private Function Retorna_Tenedor_Planilla(ByVal lonNumePlan As Long, ByRef strNumeIdenTene As String) As Boolean
        Dim objGeneral As New clsGeneral
        Try
            Dim strSQL As String, dtsEgresos As DataSet

            strSQL = "SELECT TERC.Numero_Identificacion"
            strSQL += " FROM Encabezado_Planilla_Despachos AS ENPD, Terceros AS TERC"
            strSQL += " WHERE ENPD.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ENPD.Numero_Documento = " & lonNumePlan.ToString
            strSQL += " AND ENPD.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND ENPD.TERC_Codigo_Tenedor = TERC.Codigo"

            dtsEgresos = objGeneral.Retorna_Dataset(strSQL)

            If dtsEgresos.Tables(0).Rows.Count > 0 Then
                strNumeIdenTene = dtsEgresos.Tables(0).Rows(0).Item("Numero_Identificacion").ToString()
                Retorna_Tenedor_Planilla = True
            Else
                strNumeIdenTene = ""
                Retorna_Tenedor_Planilla = False
            End If

        Catch ex As Exception
            Retorna_Tenedor_Planilla = False
        End Try
        Return Retorna_Tenedor_Planilla

    End Function

    Private Function Retorna_Planilla_Comprobante_Egreso(ByVal lonCodiEgre As Long, ByRef lonNumePlan As Long) As Boolean
        Dim objGeneral As New clsGeneral
        Try
            Dim strSQL As String, dtsEgresos As DataSet

            strSQL = "SELECT Documento_Origen"
            strSQL += " FROM Encabezado_Documento_Comprobantes"
            strSQL += " WHERE EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO
            strSQL += " AND Codigo = " & lonCodiEgre.ToString

            dtsEgresos = objGeneral.Retorna_Dataset(strSQL)

            If dtsEgresos.Tables(0).Rows.Count > 0 Then
                lonNumePlan = dtsEgresos.Tables(0).Rows(0).Item("Documento_Origen").ToString()
                Retorna_Planilla_Comprobante_Egreso = True
            Else
                lonNumePlan = 0
                Retorna_Planilla_Comprobante_Egreso = False
            End If

        Catch ex As Exception
            Retorna_Planilla_Comprobante_Egreso = False
        End Try
        Return Retorna_Planilla_Comprobante_Egreso
    End Function

#End Region

End Class