﻿Public Class DescargaArchivoInterfaseContable
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strRequest As String = "..\Archivos_Interfase_Contable\" & Request.QueryString("Archivo")
        If strRequest <> "" Then
            Dim mpRuta As String = Server.MapPath(strRequest)
            Dim fiArchivo As System.IO.FileInfo = New System.IO.FileInfo(mpRuta)
            If fiArchivo.Exists Then
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment; filename=" & fiArchivo.Name)
                Response.AddHeader("Content-Length", fiArchivo.Length.ToString())
                Response.ContentType = "application/octet-stream"
                Response.WriteFile(fiArchivo.FullName)
                Response.End()

            Else
                Response.Write("El o los archivos planos especificados no existen. Request: " & strRequest)
            End If
        Else
            Response.Write("No se ha especificado ningun archivo plano para generar.")
        End If
    End Sub

End Class