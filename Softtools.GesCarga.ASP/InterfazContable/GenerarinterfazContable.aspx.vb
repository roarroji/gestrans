﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Globalization
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Repositorio.Basico.Tesoreria

Imports Softtools.GesCarga.ASP.clsGeneral


Public Class GenerarinterfazContable

    Inherits System.Web.UI.Page

#Region "Constantes Generales"

    ' Constantes de Tipos de Campo
    Public Const PERFIL_CLIENTE As Byte = 1
    Public Const CODIGO_NATURALEZA_NATURAL As Integer = 2
    Public Const CODIGO_AUTORRETENEDOR As Integer = 3
    Public Const CODIGO_ESTADO_ACTIVO As String = "1"
    Public Const CAMPO_NUMERICO As Integer = 1
    Public Const CAMPO_CARACTER As Integer = 2
    Public Const CAMPO_DECIMAL As Integer = 3
    Public Const CAMPO_FECHA As Integer = 4
    Public Const CAMPO_CARACTER_CEROS As Integer = 5
    Public Const CAMPO_IDENTIFICACION As Integer = 6
    Public Const CAMPO_CEROS_IZQUIERDA As Integer = 7
    Private Const CAMPO_DECIMAL_CON_PUNTO_UNOE As Integer = 8
    Const CAMPO_ESPACIOS_IZQUIERDA As Integer = 9
    Const CAMPO_DECIMAL_CON_PUNTO As Integer = 10
    Public Const CAMPO_CEROS_DERECHA As Integer = 11
    Const SOLO_CAMPO_DECIMAL_CON_PUNTO As Integer = 12
    Private Const CAMPO_FECHA_UNOE As Short = 15
    Private Const CAMPO_DECIMAL_UNOE_85 As Short = 16
    Private Const CAMPO_NUMERICO_UNOE_85 As Short = 17
    Const CAMPO_ESPACIOS_DERECHA As Integer = 18
    Public Const CAMPO_DINERO As Integer = 19

    ' TIPOS DOCUMENTOS
    Public Const TIDO_TERCERO As Short = 100
    Public Const TIDO_COMPROBANTE_CONTABLE As Byte = 45
    Public Const TIDO_MANIFIESTO As Byte = 140
    Public Const TIDO_LIQUIDA_MANIFIESTO_TERCEROS As Integer = 160
    Public Const TIDO_LEGALIZACION_GASTOS As Integer = 230
    Public Const TIDO_FACTURAS As Byte = 170
    Public Const TIDO_NOTA_CREDITO As Byte = 190
    Public Const TIDO_NOTA_DEBITO As Byte = 195
    Public Const TIDO_COMPROBANTE_EGRESO As Byte = 30
    Public Const TIDO_COMPROBANTE_INGRESO As Byte = 40

    ' CATALOGO DOCUMENTO ORIGEN
    Public Const DOOR_ANTICIPO_MANIFIESTO As String = "2604"
    Public Const DOOR_LIQUIDACION_CONDUCTOR As String = "2602"

    ' SISTEMAS CONTABLES
    Const SISTEMA_CONTABLE_SIIGO As Integer = 14001
    Const SISTEMA_CONTABLE_WORLD_OFFICE As Integer = 14002
    Const SISTEMA_CONTABLE_UNO_EE As Integer = 14003
    Const SISTEMA_CONTABLE_SIIGO_WINDOWS As Integer = 14004
    Const SISTEMA_CONTABLE_CONTAI As Integer = 14005

    ' INTERFACES SISTEMAS CONTABLES
    Const INTERFAZ_TERCEROS As Double = 14501
    Const INTERFAZ_COMPROBANTES_EGRESO As Double = 14503
    Const INTERFAZ_COMPROBANTES_INGRESO As Double = 14504
    Const INTERFAZ_LIQUIDACION_DESPACHOS As Double = 14505
    Const INTERFAZ_FACTURAS As Double = 14506
    Const INTERFAZ_LEGALIZACION_GASTOS As Double = 14507
    Const INTERFAZ_FACTURAS_INVENTARIO As Double = 14508
    Const INTERFAZ_ANTICIPOS As Double = 14509
    Const INTERFAZ_NOTA_CREDITO_FACTURAS As Double = 14510
    Const INTERFAZ_CUENTAS_X_COBRAR As Double = 14511
    Const MOVIMIENTO_CONTABLE_TRANSPORTE_TERRESTRE As Double = 41450501

    ' ESTADOS INTERFAZ
    Const ESTADO_INTERFAZ_PENDIENTE As Double = 12001
    Const ESTADO_INTERFAZ_REPORTADO As Double = 12002
    Const ESTADO_INTERFAZ_NO_REPORTAR As Double = 12003

#End Region

#Region "Constantes SIIGO"

    ' Nombres Archivos SIIGO
    Const NOMBRE_ARCHIVO_TERCEROS_SIIGO As String = "Terceros.txt"
    Const NOMBRE_ARCHIVO_LIQUIDACIONES_SIIGO As String = "Liquidaciones.txt"
    Const NOMBRE_ARCHIVO_FACTURAS_SIIGO As String = "Facturas.txt"
    Const NOMBRE_ARCHIVO_COMPROBANTESEGRESO As String = "ComprobanteEgreso.txt"
    Const NOMBRE_ARCHIVO_COMPROBANTINSEGRESO As String = "ComprobanteIngreso.txt"
    'Const NOMBRE_ARCHIVO_LEGALIZACION_GASTOS As String = "LegalizacionGastos.csv"

    Public Const NATURALEZA_CREDITO As String = "C"
    Public Const NATURALEZA_DEBITO As String = "D"

    ' TIPOS DE COMPROBANTES SIIGO GENERALES
    Const TIPO_COMPROBANTE_CAUSACION As String = "P"
    Const TIPO_COMPROBANTE_EGRESOS As String = "G"
    Const TIPO_COMPROBANTE_INGRESOS As String = "R"
    Const TIPO_COMPROBANTE_MANIFIESTOS As String = "L"
    Const TIPO_COMPROBANTE_LIQUIDACIONES As String = "P"
    Const TIPO_COMPROBANTE_FACTURAS As String = "F"
    Const TIPO_COMPROBANTE_NOTAS_CREDITO As String = "C"
    Const TIPO_COMPROBANTE_NOTAS_DEBITO As String = "D"
    Const TIPO_COMPROBANTE_NOTAS As String = "N"

    ' CODIGOS COMPROBANTES SIIGO GENERALES
    Const CODIGO_COMPROBANTE_EGRESO = 2
    Const CODIGO_COMPROBANTE_INGRESO = 2
    Const CODIGO_COMPROBANTE_LIQUIDACIONES As Integer = 2
    Const CODIGO_COMPROBANTE_FACTURAS As Integer = 1

    ' Constantes tipo de campo
    Const CAMPO_NUMERICO_SIIGO As Integer = 1
    Const CAMPO_ALFANUMERICO_SIIGO As Integer = 2
    Const CAMPO_DECIMAL_SIIGO As Integer = 3
    Const CAMPO_FECHA_SIIGO As Integer = 4
    Const CAMPO_CEROS_DERECHA_SIIGO As Integer = 5

    ' Constantes Terceros SIIGO
    Public Const SEPARADOR_SIIGO_WINDOWS As String = ""
    Public Const TIPO_IDENT_NIT_SIIGO_WINDOWS As String = "N"
    Public Const TIPO_IDENT_CEDULA_SIIGO_WINDOWS As String = "C"
    Public Const TIPO_IDENT_CEDULA_EXTRANJERIA_SIIGO_WINDOWS As String = "E"
    Public Const TIPO_NIT_CLIENTE_SIIGO_WINDOWS As String = "C"
    Public Const TIPO_NIT_PROVEEDOR_SIIGO_WINDOWS As String = "P"
    Public Const TIPO_NIT_OTROS_SIIGO_WINDOWS As String = "O"
    Public Const SEXO_EMPRESA_SIIGO_WINDOWS As String = "E"

    Public Const TIPO_PERSONA_NATURAL_SIIGO_WINDOWS As Integer = 1
    Public Const TIPO_PERSONA_JURIDICA_SIIGO_WINDOWS As Integer = 2
    Public Const SI_SIIGO_WINDOWS As String = "S"
    Public Const NO_SIIGO_WINDOWS As String = "N"
    Public Const ESTADO_ACTIVO_SIIGO_WINDOWS As String = "A"
    Public Const ESTADO_INACTIVO_SIIGO_WINDOWS As String = "I"
    Public Const FECHA_NULA_SIIGO_WINDOWS As String = "19000101"
    Public Const VACIO_NUMERICO_SIIGO_WINDOWS As Integer = 0

    Public Const VALOR_ZERO_SIIGO_WINDOWS As String = "0"
    Public Const VALOR_UNO_SIIGO_WINDOWS As String = "1"

    Public Const CODIGO_CIUDAD_SIIGO_WINDOWS As Integer = 1
    Public Const CODIGO_PAIS_SIIGO_WINDOWS As Integer = 1
    Public Const CLASIFICACION_TRIBUTARIA_REGIMEN_SIMPLIFICADO As Integer = 4
    Public Const CLASIFICACION_TRIBUTARIA_REGIMEN_COMUN As Integer = 3

    ' Todas
    Const DIRECTORIO_ARCHIVOS_PLANOS As String = "Archivos_Interfase_Contable\"
    Const NOMBRE_ARCHIVO_COMPRIMIDO As String = "ArchivosInterfaseContable"
    Const EXTENSION_ARCHIVO_COMPRIMIDO As String = ".zip"

    ' AUXILIARES ARCHIVOS
    Const CODIGO_VENDEDOR As String = "1"
    Const CARACTER_ENTER As String = Chr(13)
    Const ARCHIVO_DEFINITIVO As Integer = 1
    Const ARCHIVO_BORRADOR As Integer = 0
    Const ARCHIVO_BORRADOR_WORLD_OFFICE As Integer = 2
    Const CODIGO_CATALOGO_PUC_COBRO_CLIENTE As String = "11"
    Const CODIGO_CATALOGO_PUC_RETEFUENTE_CLIENTE As String = "12"
    Const CODIGO_CATALOGO_PUC_FLETE_LIQUIDACION_MANIFIESTO As String = "26"
    Const CODIGO_CATALOGO_PUC_NETO_LIQUIDACION_MANIFIESTO As String = "27"
#End Region

#Region "Constantes UNO EE"
    ' Nombres Archivos UNO EE
    Const NOMBRE_ARCHIVO_COMPROBANTES_EGRESO_UNO_EE As String = "ComprobanteEgresoUnoEE.txt"
    Const NOMBRE_ARCHIVO_LIQUIDACIONES_UNO_EE As String = "LiquidacionesUnoEE.txt"
    Const NOMBRE_ARCHIVO_FACTURAS_UNO_EE As String = "FacturasUnoEE.txt"
    Const NOMBRE_ARCHIVO_ANTICIPOS_UNO_EE As String = "AnticiposUnoEE.txt"
    Const NOMBRE_ARCHIVO_CUENTA_PAGAR_UNO_EE As String = "CuentaPorPagarUnoEE.txt"
    Const NOMBRE_ARCHIVO_NOTA_CREDITO_DOCUMENTO As String = "NotaCreditoDocumentoUnoEE.txt"
    Const NOMBRE_ARCHIVO_NOTA_CREDITO_MOVIMIENTO As String = "NotaCreditoUnoEE.txt"

    'Constantes Generales UNO EE
    Const TIPO_DOCUMENTO_DETALLE_UNO_EE As String = "350"
    Const TIPO_REGISTRO_UNO_EE As String = "351"
    Const SUBTIPO_REGISTRO_UNO_EE As String = "00"
    Const SUBTIPO_REGISTRO_CXC_UNO_EE As String = "01"
    Const SUBTIPO_REGISTRO_CXP_UNO_EE As String = "02"
    Const UNIDAD_NEGOCIO_UNO_EE As String = "00"
    Const VERSION_TIPO_REGISTRO_UNO_EE As String = "01"
    Const VERSION_TIPO_REGISTRO_CXP_UNO_EE As String = "02"
    Const MAESTRO_EMPRESA_UNO_EE As String = "005"
    Const MAESTRO_FLUJO_EFECTIVO_UNO_EE As String = "1203"
    'Const MAESTRO_CENTRO_OPERACION_COMPROBANTES_EGRESO_UNO_EE As String = "123"
    'Const MAESTRO_TIPO_DOCUMENTO_COMPROBANTES_EGRESO_UNO_EE As String = "123"
    'Const MAESTRO_TIPO_DOCUMENTO_FACTURAS_UNO_EE As String = "123"
    Const MAESTRO_TIPO_DOCUMENTO_LIQUIDACION_UNO_EE As String = "LIQ"
    Const MAESTRO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO_UNO_EE As String = "PEL"
    Const MAESTRO_TIPO_DOCUMENTO_FACTURA_VENTA_UNO_EE As String = "FCV"
    Const MAESTRO_TIPO_DOCUMENTO_NOTA_CREDITO_UNO_EE As String = "NEC"
    Const MAESTRO_ENCABEZADO_FACTURA_VENTA_UNO_EE As String = "FACTURA DE VENTA SERVICIO DE TRANSPORTE"
    Const MAESTRO_ENCABEZADO_LIQUIDACION_UNO_EE As String = "LIQUIDACION SERVICIO DE TRANSPORTE"

    'Constantes Alternativa Logistica UNO EE
    Const CONSTANTE_CONTRA_CUENTA_CXP_ALTERNATIVA_LOGISTICA As String = "23050505"
    Const CONSTANTE_CONTRA_CUENTA_CXC_ALTERNATIVA_LOGISTICA As String = "13050501"

    Const CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA As String = "28150510"
    Const CUENTA_MOVIMIENTO_REMESA_INTERMEDIACION_ALTERNATIVA_LOGISTICA As String = "41450501"

#End Region

#Region "Constantes WORLD_OFFICE"

    ' Nombres Archivos WORLD_OFFICE
    Const NOMBRE_ARCHIVO_TERCEROS_WORLD_OFFICE As String = "Terceros.txt"
    Const NOMBRE_ARCHIVO_LIQUIDACIONES_WORLD_OFFICE As String = "Liquidaciones.txt"
    Const NOMBRE_ARCHIVO_FACTURAS_WORLD_OFFICE As String = "Facturas.csv"
    Const NOMBRE_ARCHIVO_FACTURAS_INVENTARIO_WORLD_OFFICE As String = "FacturasInventario.csv"
    Const NOMBRE_ARCHIVO_COMPROBANTESEGRESO_WORLD_OFFICE As String = "ComprobanteEgresoWorldOffice.csv"
    Const NOMBRE_ARCHIVO_LEGALIZACION_GASTOS As String = "LegalizacionGastosWorldOffice.csv"

    Public Const SEPARADOR_WORLD_OFFICE As String = ";"

    ' Contabilidad WORLD OFFICE
    Public Const COMPROBANTE_EGRESO_WORLD_OFFICE As Short = 90
    'Public Const GASTOS_CONDUCTOR_WORLD_OFFICE As Short = 91
    'Public Const FACTURA_WORLD_OFFICE As Short = 92
    'Public Const COMPROBANTE_INGRESO_WORLD_OFFICE As Short = 93
    'Public Const FACTURA_INVENTARIO_WORLD_OFFICE As Short = 124
    'Public Const CUENTA_ANTICIPO_WORLD_OFFICE As String = "133010"
    'Public Const CUENTA_ACPM_CHIP_WORLD_OFFICE As String = "83959505"
    Public Const CUENTA_PROVEEDORES_WORLD_OFFICE As String = "220505"
    Public Const CUENTA_TRANSPORTE_CARGA_TELLEVAMOS As String = " " '"41450501"
    Public Const INDICADOR_FACTURA_VENTA_FV As String = "FV"
    Public Const NOMBRE_CUENTA_TRANSPORTE_CARGA_TELLEVAMOS As String = "SERVICIOS DE TRANSPORTE DE CARGA"

    Public Const INDICADOR_COMPROBANTE_EGRESOS_WOLD_OFFICE As String = "CE"
    Public Const PREFIJO_COMPROBANTE_EGRESOS_WOLD_OFFICE As String = "GESCE"
    Public Const PREFIJO_LEGALIZACION_WORLD_OFFICE As String = "GEF2"

#End Region

#Region "Constantes CONTAI CARGA"

    Const NOMBRE_ARCHIVO_ANTICIPOS_CONTAI As String = "Anticipos.txt"
    Const NOMBRE_ARCHIVO_CXC_CONTAI As String = "Cuentas_por_Cobrar.txt"
    Const NOMBRE_ARCHIVO_LIQUIDACIONES_CONTAI As String = "Liquidaciones.txt"
    Const NOMBRE_ARCHIVO_LEGALIZACION_CONTAI As String = "Legalizaciones.txt"

    Const TIPO_COMPROBANTE_FACTURA_CONTAI As String = "3"
    Const TIPO_COMPROBANTE_LIQUIDACIONES_CONTAI As String = "28"
    Const TIPO_COMPROBANTE_LEGALIZACIONES_CONTAI As String = "12"
    Const TIPO_COMPROBANTE_EGRESOS_CONTAI As String = "2"
    Const TIPO_COMPROBANTE_INGRESOS_CONTAI As String = "1"
    Const TIPO_COMPROBANTE_ANTICIPOS_CONTAI As String = "26"
    Const TIPO_COMPROBANTE_CUENTAS_X_COBRAR_CONTAI As String = "20"

    ' CUENTAS CONTABLES CONTAI QUE MANEJAN COMO PRE-FIJO UN CODIGO PARA LA OFICINA
    Const CUENTA_PUC_ANTICIPO_CARGA As String = "137010"
    Const CUENTA_PUC_RETEICA_CARGA As String = "281505"

    ' CUENTAS CONTABLES CONTAI CON CENTRO-COSTOS
    Const CUENTA_PUC_VALORES_RECIBIDOS_TERCERO_CARGA As String = "28150535"
    Const CUENTA_PUC_FALTANTES_PROPIETARIO_CARGA As String = "28150530"
    Const CUENTA_PUC_VALORES_TERCEROS_FACTURA_CARGA As String = "28150506"
    Const CUENTA_PUC_INTERMEDIACION_TERCEROS_CARGA As String = "41450550"
    Const CUENTA_PUC_INTERMEDIACION_PROPIOS_CARGA As String = "41450556"

    ' CUENTA CONTABLE CONTAI CVT (Contrato Vinculacion Temporal)
    Const CUENTA_PUC_CVT_CARGA As String = "28150533"

    'CUENTAS POR COBRAR 
    'CREDITO
    Const CUENTA_PUC_CERDITO_DESCUENTO_FALTANTE As String = "28150530"
    Const CUENTA_PUC_CERDITO_MAYOR_VALOR_CANCELADO As String = "28150535"
    Const CUENTA_PUC_CERDITO_PRESTAMO_OTORGADO_TERCERO As String = "13809501"
    Const CUENTA_PUC_CERDITO_RECUPERACION_GASTOS_GERENCIA As String = "13809501"
    'DEBITO 
    Const CUENTA_PUC_DEBITO_GENERAL As String = "13802002"

#End Region


#Region "Variables"

    Private intDefinitivo As Integer = 0
    Private strPath As String = ""
    Public strError As String
    Private strSQL As String
    Dim objGeneral As clsGeneral
    Dim objTercero As TerceroInterfazContable
    Dim objCuentaPuc As PlanUnicoCuentas
    Dim objOperaCuentaPuc As RepositorioPlanUnicoCuentas
    Public intSitemaContable As Integer
    Public intDocumentoContable As Integer
    Public intNumero As Double
    Public dtaFechaInicial As Date
    Public dtaFechaFinal As Date
    Public intEstado As Integer
    Public intOficina As Integer
    Public strNombreArchivo As String
    Public strNombreArchivoII As String
    Public strPathArchivo As String
    Public intEmpresa As Integer
    Public intUsuario As Integer

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intSitemaContable = CInt(Me.Request.QueryString("SistemaContable"))
            intDocumentoContable = CInt(Me.Request.QueryString("TipoDocumento"))
            intNumero = Me.Request.QueryString("Numero")

            Dim MyCultureInfo = New CultureInfo("en-US") ' es-ES
            Dim strFecha As String
            strFecha = Me.Request.QueryString("FechInic")
            dtaFechaInicial = DateTime.ParseExact(strFecha, "d", MyCultureInfo)
            strFecha = Me.Request.QueryString("FechFina")
            dtaFechaFinal = DateTime.ParseExact(strFecha, "d", MyCultureInfo)

            'dtaFechaInicial = Date.Parse(Me.Request.QueryString("FechInic"))
            'dtaFechaFinal = Date.Parse(Me.Request.QueryString("FechFina"))

            intEstado = Me.Request.QueryString("Estado")
            intOficina = Me.Request.QueryString("Oficina")
            intEmpresa = Me.Request.QueryString("Empresa")
            intUsuario = Me.Request.QueryString("CodigoUsuario")
            objGeneral = New clsGeneral
            Me.objTercero = New TerceroInterfazContable
            lblMensajeError.Visible = True

            'lblMensajeError.Text = "Call Generar_Archivo_Interfaz()"
            Call Generar_Archivo_Interfaz()

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = ex.Message.ToString
        End Try
    End Sub

    Private Sub Generar_Archivo_Interfaz()

        Try
            Select Case intSitemaContable

                ' Sistema Contable 
                Case SISTEMA_CONTABLE_SIIGO
                    Call Me.Generar_Archivo_Interfaz_SIIGO()

                Case SISTEMA_CONTABLE_WORLD_OFFICE
                    Call Me.Generar_Archivo_Interfaz_WORLD_OFFICE()

                Case SISTEMA_CONTABLE_UNO_EE
                    Call Me.Generar_Archivo_Interfaz_UNO_EE()

                Case SISTEMA_CONTABLE_CONTAI
                    Call Me.Generar_Archivo_Interfaz_CONTAI()

            End Select

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Genera_Archivo_Interfaz:" & ex.Message.ToString
        End Try

        If Len(Trim(strNombreArchivo)) > clsGeneral.CERO Then
            Dim strFiltro As String = "<script language='JavaScript'>window.open('../InterfazContable/DescargaArchivoInterfaseContable.aspx?Archivo=" & strNombreArchivo & "')</script>"
            If Len(Trim(strNombreArchivoII)) > clsGeneral.CERO Then
                strFiltro += "<script language='JavaScript'>window.open('../InterfazContable/DescargaArchivoInterfaseContable.aspx?Archivo=" & strNombreArchivoII & "')</script>"
            End If
            Me.ltrFiltro.Text = strFiltro
        End If

    End Sub

    Private Sub Generar_Archivo_Interfaz_SIIGO()
        Try
            Select Case intDocumentoContable

                Case INTERFAZ_TERCEROS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Tercero_SIIGO()

                Case INTERFAZ_COMPROBANTES_EGRESO
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Comprobante_Egreso_SIIGO()

                Case INTERFAZ_COMPROBANTES_INGRESO
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Comprobante_Ingreso_SIIGO()

                Case INTERFAZ_LIQUIDACION_DESPACHOS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Liquidacion_SIIGO()

                Case INTERFAZ_FACTURAS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Factura_SIIGO()

                Case INTERFAZ_LEGALIZACION_GASTOS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Legalizacion_Gastos_SIIGO()

            End Select

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Genera_Archivo_Interfaz:" & ex.Message.ToString
        End Try


    End Sub

    Private Sub Generar_Archivo_Interfaz_UNO_EE()
        Try
            Select Case intDocumentoContable

                Case INTERFAZ_COMPROBANTES_EGRESO
                    ' Generar Documento UnoEE Movimiento Contable Comprobante Egreso
                    Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivo, strPathArchivo, 1)
                    Call Me.Generar_Documento_Comprobante_Egreso_UNO_EE()

                    ' Generar Documento UnoEE Cuentas Por Pagar Comprobante Egreso
                    Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivoII, strPathArchivo, 2)
                    Call Me.Generar_Documento_Cuentas_Pagar_UNO_EE()

                Case INTERFAZ_NOTA_CREDITO_FACTURAS
                    ' Generar Documento UnoEE Movimiento Contable Comprobante Egreso
                    Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivo, strPathArchivo, 1)
                    Call Me.Generar_Documento_Nota_Credito_UNO_EE()

                    ' Generar Documento UnoEE Cuentas Por Pagar Comprobante Egreso
                    'Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivoII, strPathArchivo, 2)
                    'Call Me.Generar_Documento_Nota_Credito_Documental_UNO_EE()

                Case INTERFAZ_LIQUIDACION_DESPACHOS
                    ' Generar Documento UnoEE Movimiento Contable Liquidaciones
                    Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivo, strPathArchivo, 1)
                    Call Me.Generar_Documento_Liquidacion_UNO_EE()

                Case INTERFAZ_FACTURAS
                    ' Generar Documento UnoEE Movimiento Contable Facturas
                    Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivo, strPathArchivo, 1)
                    Call Me.Generar_Documento_Facturas_UNO_EE()

                Case INTERFAZ_ANTICIPOS
                    ' Generar Documento UnoEE Movimiento Contable Facturas
                    Call Armar_Nombre_Archivo_Uno_EE(strNombreArchivo, strPathArchivo, 1)
                    Call Me.Generar_Documento_Anticipos_UNO_EE()

            End Select

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Genera_Archivo_Interfaz:" & ex.Message.ToString
        End Try


    End Sub

    Private Sub Generar_Archivo_Interfaz_WORLD_OFFICE()
        Try
            Select Case intDocumentoContable

                ' Interfaz Facturacion
                Case INTERFAZ_FACTURAS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Factura_WORLD_OFFICE()

                ' Interfaz Facturacion Electronica
                Case INTERFAZ_FACTURAS_INVENTARIO
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Factura_Inventario_WORLD_OFFICE()

                ' Interfaz Comprobantes Egreso
                Case INTERFAZ_COMPROBANTES_EGRESO
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Interfaz_Comprobantes_WORLD_OFFICE()

                ' Interfaz Legalizacion Gastos
                Case INTERFAZ_LEGALIZACION_GASTOS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Interfaz_Legalizacion_Gastos_Conductor_WORLD_OFFICE()

            End Select

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Genera_Archivo_Interfaz:" & ex.Message.ToString
        End Try


    End Sub

    Private Sub Generar_Archivo_Interfaz_CONTAI()
        Try
            Select Case intDocumentoContable

                Case INTERFAZ_ANTICIPOS ' (Prestamos) Encabezado_Comprobante_Contables
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Anticipos_CONTAI()

                Case INTERFAZ_COMPROBANTES_EGRESO
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Comprobante_Egresos_CONTAI()

                Case INTERFAZ_COMPROBANTES_INGRESO
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Comprobante_Ingreso_CONTAI()

                Case INTERFAZ_LIQUIDACION_DESPACHOS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Liquidacion_Planillas_CONTAI()

                Case INTERFAZ_LEGALIZACION_GASTOS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Legalizacion_Gastos_CONTAI()

                Case INTERFAZ_FACTURAS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Documento_Facturas_CONTAI()

                Case INTERFAZ_TERCEROS
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Terceros_CONTAI()
                Case INTERFAZ_CUENTAS_X_COBRAR
                    Call Armar_Nombre_Archivo(strNombreArchivo, strPathArchivo)
                    Call Me.Generar_Cuentas_x_Cobrar_CONTAI()

                Case INTERFAZ_NOTA_CREDITO_FACTURAS

            End Select

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Genera_Archivo_Interfaz:" & ex.Message.ToString
        End Try


    End Sub

#Region "Funciones INTERFAZ SIIGO"

    Private Function Generar_Documento_Tercero_SIIGO() As Boolean
        Dim ComandoSQL As SqlCommand
        Dim intNumeroItem As Integer
        Dim stmStreamW As Stream
        Dim stwStreamWriter As StreamWriter
        Dim strFila As String
        Dim strCampo As String
        Dim strIdentificacion As String
        Dim strTipoIdentificacion As String
        Dim strTipoNit As String
        Dim strSexo As String
        Dim strTipoPersona As String
        Dim strAutoretenedor As String
        Dim strEstado As String
        Dim strEsRazonSocial As String
        Dim strIdentExtranjero As String = ""
        Dim strNombreCompletoTercero As String = ""

        Const ESPACIO As String = " "
        Dim intAuxCont As Integer = 0
        Dim strPrimerNombreTercero As String = ""
        Dim strSegundoNombreTercero As String = ""

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        Date.TryParse(Request.QueryString("FechInic"), dtaFechaInicial)
        Date.TryParse(Request.QueryString("FechFina"), dtaFechaFinal)
        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        'Consulta generada para crear el archivo plano de Terceros
        strSQL = "SELECT DISTINCT(TERC_Codigo)"
        strSQL += " FROM V_Generar_Detalle_Comprobante_Contables"
        strSQL += " WHERE EMPR_Codigo = " & intEmpresa
        strSQL += " AND (Year(Fecha) = " & dtaFechaInicial.Year
        strSQL += " AND Month(Fecha) = " & dtaFechaInicial.Month & ")"
        strSQL += " AND Interfaz_Contable = " & ARCHIVO_BORRADOR
        strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO
        strSQL += " AND TERC_Codigo <> 0" ' Tercero del Sistema

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        intNumeroItem = 0
        Try
            strFila = String.Empty

            While sdrArchInte.Read
                intNumeroItem += 1

                ' Se hace un retorna campos por cada tercero que consulte
                Dim arrCampos(17), arrValoresCampos(17) As String
                arrCampos(0) = "Codigo"
                arrCampos(1) = "Numero_Identificacion"
                arrCampos(2) = "CATA_TIID_Codigo"
                arrCampos(3) = "CATA_TINT_Codigo"
                arrCampos(4) = "Autoretenedor"
                arrCampos(5) = "Estado"
                arrCampos(6) = "CATA_SETE_Codigo"
                arrCampos(7) = "Representante_Legal"
                arrCampos(8) = "Direccion"
                arrCampos(9) = "Telefonos"
                arrCampos(10) = "Telefonos"
                arrCampos(11) = "Celulares"
                arrCampos(12) = "Fax"
                arrCampos(13) = "Emails"
                arrCampos(14) = "Digito_Chequeo"
                arrCampos(15) = "Nombre"
                arrCampos(16) = "Apellido1"
                arrCampos(17) = "Apellido2"

                If Me.objGeneral.Retorna_Campos(intEmpresa, "Terceros", arrCampos, arrValoresCampos, 18, clsGeneral.CAMPO_NUMERICO, "Codigo", Val(sdrArchInte("TERC_Codigo")), "", Me.strError) Then
                    Me.objTercero.Codigo = Val(arrValoresCampos(0))
                    Me.objTercero.NumeroIdentificacion = arrValoresCampos(1)
                    Me.objTercero.CataTipoIdentificacion = arrValoresCampos(2)
                    Me.objTercero.CataTipoNaturaleza = Val(arrValoresCampos(3))
                    Me.objTercero.Autoretenedor = Val(arrValoresCampos(4))
                    Me.objTercero.CataEstado = arrValoresCampos(5)

                    Me.objTercero.CataSexo = arrValoresCampos(6)
                    Me.objTercero.RepresentanteLegal = arrValoresCampos(7)
                    Me.objTercero.Direccion = arrValoresCampos(8)
                    Me.objTercero.Telefono1 = arrValoresCampos(9)
                    Me.objTercero.Telefono2 = arrValoresCampos(10)
                    Me.objTercero.Celular = arrValoresCampos(11)
                    Me.objTercero.Fax = arrValoresCampos(12)
                    Me.objTercero.Email = arrValoresCampos(13)
                    Me.objTercero.DigitoChequeo = arrValoresCampos(14)
                    Me.objTercero.Nombre = arrValoresCampos(15)
                    Me.objTercero.Apellido1 = arrValoresCampos(16)
                    Me.objTercero.Apellido2 = arrValoresCampos(17)
                    strNombreCompletoTercero = Me.objTercero.Nombre & " " & Me.objTercero.Apellido1 & " " & Me.objTercero.Apellido2
                End If

                strIdentificacion = Me.objTercero.NumeroIdentificacion
                If Me.objTercero.CataTipoIdentificacion = clsGeneral.TIID_NIT Then
                    strTipoIdentificacion = TIPO_IDENT_NIT_SIIGO_WINDOWS
                    strSexo = SEXO_EMPRESA_SIIGO_WINDOWS
                Else
                    strTipoIdentificacion = TIPO_IDENT_CEDULA_SIIGO_WINDOWS
                    strSexo = Trim(Me.objTercero.CataSexo)
                End If

                If Me.objTercero.CataTipoIdentificacion = clsGeneral.TIID_CEDULA_EXTRANJERIA Then
                    strIdentExtranjero = strIdentificacion
                End If

                If Me.objTercero.Tiene_Perfil = PERFIL_CLIENTE Then
                    strTipoNit = TIPO_NIT_CLIENTE_SIIGO_WINDOWS
                Else
                    strTipoNit = TIPO_NIT_PROVEEDOR_SIIGO_WINDOWS
                End If

                If Val(Me.objTercero.CataTipoNaturaleza) = CODIGO_NATURALEZA_NATURAL Then
                    strTipoPersona = TIPO_PERSONA_NATURAL_SIIGO_WINDOWS
                    strEsRazonSocial = NO_SIIGO_WINDOWS
                Else
                    strTipoPersona = TIPO_PERSONA_JURIDICA_SIIGO_WINDOWS
                    strEsRazonSocial = SI_SIIGO_WINDOWS
                End If

                If Me.objTercero.Autoretenedor = CODIGO_AUTORRETENEDOR Then
                    strAutoretenedor = SI_SIIGO_WINDOWS
                Else
                    strAutoretenedor = NO_SIIGO_WINDOWS
                End If

                If Val(Me.objTercero.CataEstado) = CODIGO_ESTADO_ACTIVO Then
                    strEstado = ESTADO_ACTIVO_SIIGO_WINDOWS
                Else
                    strEstado = ESTADO_INACTIVO_SIIGO_WINDOWS
                End If

                '********* Comenzar a escribir los campos **************
                ' NIT -> 001-013
                strCampo = Me.Formatear_Campo(strIdentificacion, 13, CAMPO_CEROS_IZQUIERDA)
                strFila = strCampo & SEPARADOR_SIIGO_WINDOWS
                ' SUCURSAL -> 014-016
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 3, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' TIPO DE NIT -> 017-017
                strCampo = Me.Formatear_Campo(strTipoNit, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' NOMBRE -> 018-077
                strCampo = Me.Formatear_Campo(strNombreCompletoTercero, 60, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CONTACTO -> 078-127
                strCampo = Me.Formatear_Campo(Me.objTercero.RepresentanteLegal, 50, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' DIRECCION -> 128-227
                strCampo = Me.Formatear_Campo(Me.objTercero.Direccion, 100, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' TELEFONO 1 -> 228-238
                Me.objTercero.Telefono1 = Replace(Me.objTercero.Telefono1, "-", "")
                Me.objTercero.Telefono1 = Replace(Me.objTercero.Telefono1, " ", "")
                Me.objTercero.Telefono1 = Replace(Me.objTercero.Telefono1, "(", "")
                Me.objTercero.Telefono1 = Replace(Me.objTercero.Telefono1, ")", "")
                strCampo = Me.Formatear_Campo(Right(Me.objTercero.Telefono1, 11), 11, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' COMIENTZA LA ESTRUCTURA DEL SEGUNDO ARCHIVO PERO EN ESTE CASO SE GENERA UN SOLO ARCHIVO CON TODO
                ' TELEFONO 2 -> 239-249
                Me.objTercero.Telefono2 = Replace(Me.objTercero.Telefono2, "-", "")
                Me.objTercero.Telefono2 = Replace(Me.objTercero.Telefono2, " ", "")
                Me.objTercero.Telefono2 = Replace(Me.objTercero.Telefono2, "(", "")
                Me.objTercero.Telefono2 = Replace(Me.objTercero.Telefono2, ")", "")
                strCampo = Me.Formatear_Campo(Right(Me.objTercero.Telefono2, 11), 11, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' TELEFONO 3 -> 250-260
                Me.objTercero.Celular = Replace(Me.objTercero.Celular, "-", "")
                Me.objTercero.Celular = Replace(Me.objTercero.Celular, " ", "")
                Me.objTercero.Celular = Replace(Me.objTercero.Celular, "(", "")
                Me.objTercero.Celular = Replace(Me.objTercero.Celular, ")", "")
                strCampo = Me.Formatear_Campo(Right(Me.objTercero.Celular, 11), 11, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' TELEFONO 4 -> 261-271
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 11, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' FAX -> 272-282
                Me.objTercero.Fax = Replace(Me.objTercero.Fax, "-", "")
                Me.objTercero.Fax = Replace(Me.objTercero.Fax, " ", "")
                Me.objTercero.Fax = Replace(Me.objTercero.Fax, "(", "")
                Me.objTercero.Fax = Replace(Me.objTercero.Fax, ")", "")
                strCampo = Me.Formatear_Campo(Right(Me.objTercero.Fax, 11), 11, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' APARTADO AEREO -> 283-288
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 6, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' EMAIL -> 289-388
                strCampo = Me.Formatear_Campo(Me.objTercero.Email, 100, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' SEXO -> 389-389
                strCampo = Me.Formatear_Campo(strSexo, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CODIGO CLASIFICACION TRIBUTARIA -> 390-390
                If strTipoNit = TIPO_NIT_CLIENTE_SIIGO_WINDOWS Then
                    strCampo = Me.Formatear_Campo(CLASIFICACION_TRIBUTARIA_REGIMEN_COMUN, 1, CAMPO_CEROS_IZQUIERDA)
                Else
                    strCampo = Me.Formatear_Campo(CLASIFICACION_TRIBUTARIA_REGIMEN_SIMPLIFICADO, 1, CAMPO_CEROS_IZQUIERDA)
                End If
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' TIPO IDENTIFICACION -> 391-391
                strCampo = Me.Formatear_Campo(strTipoIdentificacion, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CUPO DE CREDITO -> 392-402
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 11, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' LISTA DE PRECIOS -> 403-404
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 2, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CODIGO DEL VENDEDOR -> 405-408
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 4, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CODIGO DE LA CIUDAD -> 409-412
                strCampo = Me.Formatear_Campo(CODIGO_CIUDAD_SIIGO_WINDOWS, 4, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' PORCENTAJE DE DESCUENTO -> 413-423
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 11, CAMPO_DECIMAL, 2)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' PERIODO DE PAGO -> 424-426
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 3, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' OBSERVACION -> 427-456
                strCampo = Me.Formatear_Campo(String.Empty, 30, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CODIGO DEL PAIS -> 457-459
                strCampo = Me.Formatear_Campo(CODIGO_PAIS_SIIGO_WINDOWS, 3, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' DIGITO DE VERIFICACION -> 460-460
                strCampo = Me.Formatear_Campo(Me.objTercero.DigitoChequeo, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CALIFICACION -> 461-463
                strCampo = Me.Formatear_Campo(String.Empty, 3, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' ACTIVIDAD ECONOMICA -> 464-468
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 5, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' FORMA DE PAGO -> 469-472
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 4, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' COBRADOR -> 473-476
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 4, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' TIPO PERSONA -> 477-478
                strCampo = Me.Formatear_Campo(strTipoPersona, 2, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' DECLARANTE -> 479-479
                strCampo = Me.Formatear_Campo(NO_SIIGO_WINDOWS, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' AGENTE RETENEDOR -> 480-480
                strCampo = Me.Formatear_Campo(strAutoretenedor, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' AUTORETENEDOR -> 481-481
                strCampo = Me.Formatear_Campo(strAutoretenedor, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' BENEFICIARIO RETEIVA 60% -> 482-482
                strCampo = Me.Formatear_Campo(NO_SIIGO_WINDOWS, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' AGENTE RETENEDOR ICA -> 483-483
                strCampo = Me.Formatear_Campo(NO_SIIGO_WINDOWS, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' ESTADO -> 484-484
                strCampo = Me.Formatear_Campo(strEstado, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' ENTE PUBLICO -> 485-485
                strCampo = Me.Formatear_Campo(NO_SIIGO_WINDOWS, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' CODIGO ENTE PUBLICO -> 486-495
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 10, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' ES RAZON SOCIAL -> 496-496
                strCampo = Me.Formatear_Campo(strEsRazonSocial, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                If strEsRazonSocial = NO_SIIGO_WINDOWS Then

                    'Separa primer y segundo nombre del tercero
                    intAuxCont = 0
                    strPrimerNombreTercero = ""
                    strSegundoNombreTercero = ""

                    If Me.objTercero.Nombre <> "" Then
                        intAuxCont = Me.objTercero.Nombre.IndexOf(ESPACIO)
                        If intAuxCont > clsGeneral.CERO Then
                            strSegundoNombreTercero = Me.objTercero.Nombre.Substring(intAuxCont + clsGeneral.UNO)
                            strPrimerNombreTercero = Me.objTercero.Nombre.Substring(clsGeneral.CERO, intAuxCont)
                        Else
                            strPrimerNombreTercero = Me.objTercero.Nombre
                        End If
                    End If

                    ' PRIMER NOMBRE -> 497-511
                    strCampo = Me.Formatear_Campo(strPrimerNombreTercero, 15, CAMPO_CARACTER)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' SEGUNDO NOMBRE -> 512-526
                    strCampo = Me.Formatear_Campo(strSegundoNombreTercero, 15, CAMPO_CARACTER)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' PRIMER APELLIDO -> 527-441
                    strCampo = Me.Formatear_Campo(Me.objTercero.Apellido1, 15, CAMPO_CARACTER)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' SEGUNDO APELLIDO -> 542-556
                    strCampo = Me.Formatear_Campo(Me.objTercero.Apellido2, 15, CAMPO_CARACTER)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                Else
                    ' PRIMER NOMBRE -> 497-511
                    strCampo = Me.Formatear_Campo(String.Empty, 15, CAMPO_CARACTER)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' SEGUNDO NOMBRE -> 512-526
                    strCampo = Me.Formatear_Campo(String.Empty, 15, CAMPO_CARACTER)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' PRIMER APELLIDO -> 527-441
                    strCampo = Me.Formatear_Campo(String.Empty, 15, CAMPO_CARACTER)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                    ' SEGUNDO APELLIDO -> 542-556
                    strCampo = Me.Formatear_Campo(String.Empty, 15, CAMPO_CARACTER)
                    strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                End If
                ' NUMERO DE IDENTIFICACION DEL EXTRANJERO -> 557-576
                If strIdentExtranjero <> "" Then
                    strCampo = Me.Formatear_Campo(strIdentExtranjero, 20, CAMPO_CEROS_IZQUIERDA)
                Else
                    strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 20, CAMPO_CEROS_IZQUIERDA)
                End If
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' RUTA -> 577-579
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 3, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' REGISTRO -> 580-589
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 10, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' FECHA VENCIMIENTO -> 590-597
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 8, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' FECHA CUMPLEAÑOS -> 598-605
                strCampo = Me.Formatear_Campo(VACIO_NUMERICO_SIIGO_WINDOWS, 8, CAMPO_CEROS_IZQUIERDA)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' TIPO SOCIEDAD-> 606-606
                strCampo = Me.Formatear_Campo(String.Empty, 1, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' AUTORIZACIÓN IMPRENTA-> 607-616
                strCampo = Me.Formatear_Campo(String.Empty, 10, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS
                ' AUTORIZACIÓN IMPRENTA-> 617-627
                strCampo = Me.Formatear_Campo(String.Empty, 11, CAMPO_CARACTER)
                strFila = strFila & strCampo & SEPARADOR_SIIGO_WINDOWS

                stwStreamWriter.WriteLine(strFila, System.Text.Encoding.Default)

            End While
            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        End Try


    End Function

    ' Autor: JESUS CUADROS
    ' Fecha Creación: 08-AGO-2019
    ' Obsevaciones Creación: 
    ' Autor: JESUS CUADROS
    ' Fecha Modificación: 17-SEP-2019
    ' Observaciones Modificación: Las facturas anuladas se reportan con valores en cero
    Private Function Generar_Documento_Factura_SIIGO() As Boolean
        Dim ComandoSQL As SqlCommand
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, intPos As Integer
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String
        Dim dblValoDebi As Double, dblValoCred As Double, dblValoBase As Double
        Dim strCodigoCuentaPUC As String, strClaseCuentaPUC As String, strGrupoCuentaPUC As String
        Dim strCuentaPUC As String, strSubCuenta As String, lonNumeFact As Long, strObservaciones As String, bolFactAnul As Boolean

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            strSQL = "SELECT ENCC.EMPR_Codigo, ENCC.TIDO_Codigo, ENCC.Numero, ENCC.TIDO_Documento, ENCC.Numero_Documento,"
            strSQL += "ENCC.Fecha_Documento, ENCC.TERC_Documento, ENCC.Año, ENCC.Periodo, ENCC.OFIC_Codigo,"
            strSQL += "ENCC.Valor_Comprobante, ENCC.Fecha, ENCC.CATA_ESIC_Codigo, ENCC.Anulado, ENCC.Observaciones,"
            strSQL += "DECC.ID, DECC.PLUC_Codigo, DECC.TERC_Codigo, DECC.Valor_Base, DECC.Valor_Debito, DECC.Valor_Credito,"
            strSQL += "PLUC.Codigo_Cuenta, TERC.Numero_Identificacion, ENCC.Codigo_Documento"
            strSQL += " FROM Encabezado_Comprobante_Contables As ENCC, Detalle_Comprobante_Contables As DECC, Plan_Unico_Cuentas AS PLUC, Terceros AS TERC"
            strSQL += " WHERE ENCC.EMPR_Codigo = DECC.EMPR_Codigo"
            strSQL += " AND ENCC.Numero  = DECC.ENCC_Numero"
            ' Plan Unico Cuentas
            strSQL += " AND DECC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND DECC.PLUC_Codigo  = PLUC.Codigo"
            ' Terceros
            strSQL += " AND DECC.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND DECC.TERC_Codigo  = TERC.Codigo"
            strSQL += " AND ENCC.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ENCC.Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND ENCC.Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
            strSQL += " AND ENCC.TIDO_Documento = " & TIDO_FACTURAS
            strSQL += " AND ENCC.CATA_ESIC_Codigo = " & ESTADO_INTERFAZ_PENDIENTE
            strSQL += " AND ENCC.Anulado = 0"
            strSQL += " ORDER BY ENCC.Numero_Documento"

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            While sdrArchInte.Read

                If lonNumeroComprobante = Val(sdrArchInte("Numero_Documento")) Then
                    intSecuencia += 1
                Else
                    lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                    intSecuencia = 1
                End If
                lonNumeFact = sdrArchInte("Numero_Documento") ' Número Factura
                strObservaciones = sdrArchInte("Observaciones")

                ' TEMPORAL
                ' En las Observaciones del Comprobante Contable viene la palabra ANULA y de esta manera se identifica que el comprobante corresponde a un Anulación de Factura
                intPos = InStr(strObservaciones, "ANULA")
                If intPos > 0 Then
                    bolFactAnul = True
                    strObservaciones = "MOVIMIENTO CONTABLE ANULACION FACTURA No. " & lonNumeFact.ToString
                Else
                    bolFactAnul = False
                    strObservaciones = "MOVIMIENTO CONTABLE FACTURA No. " & lonNumeFact.ToString
                End If

                ' INTEGRA solicita que las anulaciones de facturas se reporten con valores en 0
                If bolFactAnul Then
                    dblValoDebi = 0
                    dblValoCred = 0
                    dblValoBase = 0
                Else
                    dblValoDebi = sdrArchInte("Valor_Debito")
                    dblValoCred = sdrArchInte("Valor_Credito")
                    dblValoBase = sdrArchInte("Valor_Base")
                End If

                strFila = Formatear_Campo_SIIGO(TIPO_COMPROBANTE_FACTURAS, 1, CAMPO_ALFANUMERICO_SIIGO) '1-Tipo Comprobante
                strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_FACTURAS, 3, CAMPO_NUMERICO_SIIGO) '2-Codigo Comprobante
                strFila += Formatear_Campo_SIIGO(lonNumeroComprobante, 11, CAMPO_NUMERICO_SIIGO) '3-No Documento
                strFila += Formatear_Campo_SIIGO(intSecuencia, 5, CAMPO_NUMERICO_SIIGO) ' 4- Secuencia
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Numero_Identificacion"), 13, CAMPO_NUMERICO_SIIGO) '5-Identificacion Cliente
                strFila += Formatear_Campo_SIIGO("0", 3, CAMPO_NUMERICO_SIIGO) '6-Sucursal

                ' Código Cuenta Contable
                strCodigoCuentaPUC = sdrArchInte("Codigo_Cuenta")

                ' TEMPORAL 16-AGO-2019 Quedaron mal parametrizadas las cuentas PUC
                If strCodigoCuentaPUC = "13050501" Then
                    strCodigoCuentaPUC = "13050500" ' Cta PUC CxC
                ElseIf strCodigoCuentaPUC = "41450501" Then
                    strCodigoCuentaPUC = "41450100" ' Cta PUC Servicio Transporte
                ElseIf strCodigoCuentaPUC = "13551901" Then
                    strCodigoCuentaPUC = "13551802" ' Cta PUC ReteICA (Tener en cuenta que se arma con el código de la oficina)
                End If

                strClaseCuentaPUC = Mid$(strCodigoCuentaPUC, 1, 2)
                strFila += Formatear_Campo_SIIGO(strClaseCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '7 - 13
                strGrupoCuentaPUC = Mid$(strCodigoCuentaPUC, 3, 2)
                strFila += Formatear_Campo_SIIGO(strGrupoCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '8 - 30
                strCuentaPUC = Mid$(strCodigoCuentaPUC, 5, 2)
                strFila += Formatear_Campo_SIIGO(strCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '9 - 05
                strSubCuenta = Mid$(strCodigoCuentaPUC, 7, 2)
                strFila += Formatear_Campo_SIIGO(strSubCuenta, 2, CAMPO_NUMERICO_SIIGO) '10 - 01
                strFila += Formatear_Campo_SIIGO("00", 2, CAMPO_NUMERICO_SIIGO) '11 - 00

                ' Producto Transporte Carretera
                ' 300-Linea
                ' 0001-Grupo
                ' 000001-Elemento
                strFila += Formatear_Campo_SIIGO("300", 3, CAMPO_NUMERICO_SIIGO) '12
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '13
                strFila += Formatear_Campo_SIIGO("1", 6, CAMPO_NUMERICO_SIIGO) '14

                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '15-Año
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '16-Mes
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '17-Dia
                If strCodigoCuentaPUC = "41450100" Or strCodigoCuentaPUC = "61450500" Then ' Para estas cuentas se reporta Centro Costo: 11
                    strFila += Formatear_Campo_SIIGO("11", 4, CAMPO_NUMERICO_SIIGO) '18-Centro Costo
                Else
                    strFila += Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '18-Centro Costo
                End If
                strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '19-Sub Centro Costo
                strFila += Formatear_Campo_SIIGO(strObservaciones, 50, CAMPO_ALFANUMERICO_SIIGO) '20-Descripción

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO("D", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoDebi, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                Else
                    strFila += Formatear_Campo_SIIGO("C", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoCred, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                End If
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '23-Valor Decimales
                strFila += Formatear_Campo_SIIGO(dblValoBase, 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '25-Valor Base Retención Decimales

                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '26-Codigo Vendedor 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '27-Codigo Ciudad - FALTA
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '28-Codigo Zona 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '29-Codigo Bodega
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '30-Codigo Ubicación
                strFila += Formatear_Campo_SIIGO("0", 10, CAMPO_NUMERICO_SIIGO) '31-Cantidad
                strFila += Formatear_Campo_SIIGO("0", 5, CAMPO_NUMERICO_SIIGO) '32-Cantidad Decimales

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO(TIPO_COMPROBANTE_FACTURAS, 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_EGRESO, 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("", 1, CAMPO_NUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                End If

                ' Si cuenta PUC es 1305 reportar en documento cruce el Número Factura
                If strClaseCuentaPUC = "13" And strGrupoCuentaPUC = "05" Then
                    ' Reportar el Número de Factura
                    strFila += Formatear_Campo_SIIGO(lonNumeFact, 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("0", 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                End If

                strFila += Formatear_Campo_SIIGO(intSecuencia, 3, CAMPO_NUMERICO_SIIGO) '36- Secuencia Documento Cruce
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '37-Año Vencimiento
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '38-Mes Vencimiento
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '39-Dia Vencimiento
                strFila += Formatear_Campo_SIIGO("0", 4, CAMPO_NUMERICO_SIIGO) '40-Forma Pago
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '41-Código Banco

                stwStreamWriter.WriteLine(strFila)

            End While
            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
                Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un Error en la función Generar_Documento_Factura_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally
            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: JESUS CUADROS
    ' Fecha Creación: 08-AGO-2019
    ' Obsevaciones Creación: Se copia la misma rutina de egresos
    ' Fecha Modificación: 15-AGO-2019
    ' Observaciones Modificación: Se hace un manejo para las cuentas PUC mal parametrizadas
    Private Function Generar_Documento_Liquidacion_SIIGO() As Boolean
        Dim ComandoSQL As SqlCommand
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, bolRes As Boolean
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String
        Dim dblValoDebi As Double, dblValoCred As Double, dblValoBase As Double
        Dim strCodigoCuentaPUC As String, strClaseCuentaPUC As String, strGrupoCuentaPUC As String, strCentroCosto As String
        Dim strCuentaPUC As String, strSubCuenta As String, lonNumeDocuEgre As Long ''lonCodiEgre As Long
        Dim lonNumeLiqu As Long, lonNumeDocuLiqu As Long, lonNumeDocuPlan As Long, strObservaciones As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            strSQL = "SELECT ENCC.EMPR_Codigo, ENCC.TIDO_Codigo, ENCC.Numero, ENCC.TIDO_Documento, ENCC.Numero_Documento,"
            strSQL += "ENCC.Fecha_Documento, ENCC.TERC_Documento, ENCC.Año, ENCC.Periodo, ENCC.OFIC_Codigo,"
            strSQL += "ENCC.Valor_Comprobante, ENCC.Fecha, ENCC.CATA_ESIC_Codigo, ENCC.Anulado,"
            strSQL += "DECC.ID, DECC.PLUC_Codigo, DECC.TERC_Codigo, DECC.Valor_Base, DECC.Valor_Debito, DECC.Valor_Credito, DECC.Observaciones,"
            strSQL += "PLUC.Codigo_Cuenta, TERC.Numero_Identificacion, ENCC.Codigo_Documento"
            strSQL += " FROM Encabezado_Comprobante_Contables As ENCC, Detalle_Comprobante_Contables As DECC, Plan_Unico_Cuentas AS PLUC, Terceros AS TERC"
            strSQL += " WHERE ENCC.EMPR_Codigo = DECC.EMPR_Codigo"
            strSQL += " AND ENCC.Numero  = DECC.ENCC_Numero"
            ' Plan Unico Cuentas
            strSQL += " AND DECC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND DECC.PLUC_Codigo  = PLUC.Codigo"
            ' Terceros
            strSQL += " AND DECC.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND DECC.TERC_Codigo  = TERC.Codigo"

            strSQL += " AND ENCC.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ENCC.Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND ENCC.Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
            strSQL += " AND ENCC.TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
            strSQL += " AND ENCC.CATA_ESIC_Codigo = " & ESTADO_INTERFAZ_PENDIENTE
            strSQL += " AND ENCC.Anulado = 0"
            strSQL += " ORDER BY ENCC.Numero_Documento"

            ' FALTA Revisar si el comprobante solo se genera para Liquidaciones Aprobadas

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            While sdrArchInte.Read

                lonNumeLiqu = sdrArchInte("Codigo_Documento") ' Número Liquidación

                If lonNumeroComprobante = Val(sdrArchInte("Numero_Documento")) Then
                    intSecuencia += 1
                Else
                    lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                    intSecuencia = 1
                    bolRes = Retorna_Informacion_Liquidacion(lonNumeLiqu, lonNumeDocuLiqu, lonNumeDocuPlan)
                End If

                dblValoDebi = sdrArchInte("Valor_Debito")
                dblValoCred = sdrArchInte("Valor_Credito")
                dblValoBase = sdrArchInte("Valor_Base")

                strFila = Formatear_Campo_SIIGO(TIPO_COMPROBANTE_LIQUIDACIONES, 1, CAMPO_ALFANUMERICO_SIIGO) '1-Tipo Comprobante
                strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_LIQUIDACIONES, 3, CAMPO_NUMERICO_SIIGO) '2-Codigo Comprobante
                strFila += Formatear_Campo_SIIGO(lonNumeroComprobante, 11, CAMPO_NUMERICO_SIIGO) '3-No Documento
                strFila += Formatear_Campo_SIIGO(intSecuencia, 5, CAMPO_NUMERICO_SIIGO) ' 4- Secuencia
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Numero_Identificacion"), 13, CAMPO_NUMERICO_SIIGO) '5-Identificacion
                strFila += Formatear_Campo_SIIGO("0", 3, CAMPO_NUMERICO_SIIGO) '6-Sucursal

                ' Código Cuenta Contable
                strCodigoCuentaPUC = sdrArchInte("Codigo_Cuenta")

                ' Observaciones
                strObservaciones = sdrArchInte("Observaciones") & " Planilla:" & lonNumeDocuPlan.ToString

                ' TEMPORAL: 15-AGO-2019 Por error en parametrización se cambian cuentas
                If strCodigoCuentaPUC = "61450501" Then ' CTA FLETE
                    strCodigoCuentaPUC = "61450500"
                    dblValoBase = dblValoDebi

                ElseIf strCodigoCuentaPUC = "13300501" Then ' CTA ANTICIPO
                    strCodigoCuentaPUC = "13300500"

                ElseIf strCodigoCuentaPUC = "23680601" Then ' RETEICA
                    strCodigoCuentaPUC = "23680502"
                End If

                strClaseCuentaPUC = Mid$(strCodigoCuentaPUC, 1, 2)
                strFila += Formatear_Campo_SIIGO(strClaseCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '7 - 13
                strGrupoCuentaPUC = Mid$(strCodigoCuentaPUC, 3, 2)
                strFila += Formatear_Campo_SIIGO(strGrupoCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '8 - 30
                strCuentaPUC = Mid$(strCodigoCuentaPUC, 5, 2)
                strFila += Formatear_Campo_SIIGO(strCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '9 - 05
                strSubCuenta = Mid$(strCodigoCuentaPUC, 7, 2)
                strFila += Formatear_Campo_SIIGO(strSubCuenta, 2, CAMPO_NUMERICO_SIIGO) '10 - 01
                strFila += Formatear_Campo_SIIGO("00", 2, CAMPO_NUMERICO_SIIGO) '11 - 00

                If strClaseCuentaPUC = "61" And strGrupoCuentaPUC = "45" Then
                    strCentroCosto = "11"
                Else
                    strCentroCosto = ""
                End If

                ' Producto Transporte Carretera
                ' 300-Linea
                ' 0001-Grupo
                ' 000001-Elemento
                strFila += Formatear_Campo_SIIGO("300", 3, CAMPO_NUMERICO_SIIGO) '12
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '13
                strFila += Formatear_Campo_SIIGO("1", 6, CAMPO_NUMERICO_SIIGO) '14

                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '15-Año
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '16-Mes
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '17-Dia
                strFila += Formatear_Campo_SIIGO(strCentroCosto, 4, CAMPO_NUMERICO_SIIGO) '18-Centro Costo
                strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '19-Sub Centro Costo
                strObservaciones = Mid$(strObservaciones, 1, 50)
                strFila += Formatear_Campo_SIIGO(strObservaciones, 50, CAMPO_ALFANUMERICO_SIIGO) '20-Descripción

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO("D", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoDebi, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                Else
                    strFila += Formatear_Campo_SIIGO("C", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoCred, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                End If
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '23-Valor Decimales

                If strClaseCuentaPUC = "23" And strGrupoCuentaPUC = "65" Then
                    strFila += Formatear_Campo_SIIGO(dblValoBase, 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención debe ser el Flete
                ElseIf strClaseCuentaPUC = "23" And strGrupoCuentaPUC = "68" Then
                    strFila += Formatear_Campo_SIIGO(dblValoBase, 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención debe ser el Flete
                Else
                    strFila += Formatear_Campo_SIIGO("0", 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención
                End If
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '25-Valor Base Retención Decimales

                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '26-Codigo Vendedor 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '27-Codigo Ciudad - FALTA
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '28-Codigo Zona 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '29-Codigo Bodega
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '30-Codigo Ubicación
                strFila += Formatear_Campo_SIIGO("0", 10, CAMPO_NUMERICO_SIIGO) '31-Cantidad
                strFila += Formatear_Campo_SIIGO("0", 5, CAMPO_NUMERICO_SIIGO) '32-Cantidad Decimales

                If strClaseCuentaPUC = "13" And strGrupoCuentaPUC = "30" Then
                    strFila += Formatear_Campo_SIIGO(TIPO_COMPROBANTE_EGRESOS, 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_EGRESO, 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                ElseIf strClaseCuentaPUC = "22" And strGrupoCuentaPUC = "05" Then
                    strFila += Formatear_Campo_SIIGO(TIPO_COMPROBANTE_LIQUIDACIONES, 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_LIQUIDACIONES, 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("", 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                End If

                ' Si cuenta PUC es 1330 reportar en documento cruce el Comprobante Egreso del Anticipo
                If strClaseCuentaPUC = "13" And strGrupoCuentaPUC = "30" Then

                    ' Consultar el Comprobante Egreso del Anticipo de la Planilla
                    bolRes = Retorna_Egreso_Anticipo_Planilla(lonNumeDocuPlan, lonNumeDocuEgre)

                    strFila += Formatear_Campo_SIIGO(lonNumeDocuEgre, 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce

                ElseIf strClaseCuentaPUC = "22" And strGrupoCuentaPUC = "05" Then
                    ' No. Liquidación
                    strFila += Formatear_Campo_SIIGO(lonNumeDocuLiqu, 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("0", 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                End If

                strFila += Formatear_Campo_SIIGO(intSecuencia, 3, CAMPO_NUMERICO_SIIGO) '36- Secuencia Documento Cruce
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '37-Año Vencimiento
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '38-Mes Vencimiento
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '39-Dia Vencimiento
                strFila += Formatear_Campo_SIIGO("0", 4, CAMPO_NUMERICO_SIIGO) '40-Forma Pago
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '41-Código Banco

                stwStreamWriter.WriteLine(strFila)

            End While
            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
                Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
            End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally
            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: JESUS CUADROS
    ' Fecha Creación: 07-JUN-2019
    ' Obsevaciones Creación: Se realiza la implementación con base en la especificación entregada por INTEGRA el 25-ABR-2019
    ' Autor: JESUS CUADROS
    ' Fecha Modificación: 24-JUL-2019
    ' Observaciones Modificación: Se realizan modificaciones con base en la especificación entregada por INTEGRA el 08-JUL-2019
    ' Autor: JESUS CUADROS
    ' Fecha Modificación: 15-AGO-2019
    ' Observaciones Modificación: Se hace un manejo para las cuentas PUC mal parametrizadas
    ' Autor: JESUS CUADROS
    ' Fecha Modificación: 17-SEP-2019
    ' Observaciones Modificación: Se adiciona la consulta del Tenedor de la Planilla para reportarla como Tercero
    Private Function Generar_Documento_Comprobante_Egreso_SIIGO() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, lonNumePlan As Long, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String
        Dim dblValoDebi As Double, dblValoCred As Double, dblValoBase As Double
        Dim strCodigoCuentaPUC As String, strClaseCuentaPUC As String, strGrupoCuentaPUC As String
        Dim strCuentaPUC As String, strSubCuenta As String, lonCodiEgre As Long, strObservaciones As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            strSQL = "SELECT ENCC.EMPR_Codigo, ENCC.TIDO_Codigo, ENCC.Numero, ENCC.TIDO_Documento, ENCC.Numero_Documento,"
            strSQL += "ENCC.Fecha_Documento, ENCC.TERC_Documento, ENCC.Año, ENCC.Periodo, ENCC.OFIC_Codigo,"
            strSQL += "ENCC.Valor_Comprobante, ENCC.Fecha, ENCC.CATA_ESIC_Codigo, ENCC.Anulado,"
            strSQL += "DECC.ID, DECC.PLUC_Codigo, DECC.TERC_Codigo, DECC.Valor_Base, DECC.Valor_Debito, DECC.Valor_Credito, DECC.Observaciones,"
            strSQL += "PLUC.Codigo_Cuenta, TERC.Numero_Identificacion, ENCC.Codigo_Documento"
            strSQL += " FROM Encabezado_Comprobante_Contables As ENCC, Detalle_Comprobante_Contables As DECC, Plan_Unico_Cuentas AS PLUC, Terceros AS TERC"
            strSQL += " WHERE ENCC.EMPR_Codigo = DECC.EMPR_Codigo"
            strSQL += " AND ENCC.Numero  = DECC.ENCC_Numero"
            ' Plan Unico Cuentas
            strSQL += " AND DECC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND DECC.PLUC_Codigo  = PLUC.Codigo"
            ' Terceros
            strSQL += " AND DECC.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND DECC.TERC_Codigo  = TERC.Codigo"

            strSQL += " AND ENCC.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ENCC.Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND ENCC.Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
            strSQL += " AND ENCC.TIDO_Documento = " & TIDO_COMPROBANTE_EGRESO
            strSQL += " AND ENCC.CATA_ESIC_Codigo = " & ESTADO_INTERFAZ_PENDIENTE
            strSQL += " AND ENCC.Anulado = 0"
            strSQL += " ORDER BY ENCC.Numero_Documento"

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            While sdrArchInte.Read

                If lonNumeroComprobante = Val(sdrArchInte("Numero_Documento")) Then
                    intSecuencia += 1
                Else
                    lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                    intSecuencia = 1
                End If
                lonCodiEgre = sdrArchInte("Codigo_Documento") ' Código Comprobante Egreso

                dblValoDebi = sdrArchInte("Valor_Debito")
                dblValoCred = sdrArchInte("Valor_Credito")
                dblValoBase = sdrArchInte("Valor_Base")

                strFila = Formatear_Campo_SIIGO(TIPO_COMPROBANTE_EGRESOS, 1, CAMPO_ALFANUMERICO_SIIGO) '1-Tipo Comprobante
                strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_EGRESO, 3, CAMPO_NUMERICO_SIIGO) '2-Codigo Comprobante
                strFila += Formatear_Campo_SIIGO(lonNumeroComprobante, 11, CAMPO_NUMERICO_SIIGO) '3-No Documento
                strFila += Formatear_Campo_SIIGO(intSecuencia, 5, CAMPO_NUMERICO_SIIGO) ' 4- Secuencia

                lonRes = Retorna_Planilla_Comprobante_Egreso(lonCodiEgre, lonNumePlan)
                lonRes = Retorna_Tenedor_Planilla(lonNumePlan, strIdenTene)

                strFila += Formatear_Campo_SIIGO(strIdenTene, 13, CAMPO_NUMERICO_SIIGO) '5-Identificacion Tenedor
                strFila += Formatear_Campo_SIIGO("0", 3, CAMPO_NUMERICO_SIIGO) '6-Sucursal

                ' Código Cuenta Contable
                strCodigoCuentaPUC = sdrArchInte("Codigo_Cuenta") ' 13300501

                ' TEMPORAL: 15-AGO-2019 Por problemas Parametrización se cambia cuenta 13300501 y 13050501 por 13300500 ya que estaba mal parametrizada en el Concepto
                If strCodigoCuentaPUC = "13300501" Or strCodigoCuentaPUC = "13050501" Then
                    strCodigoCuentaPUC = "13300500"
                End If

                strClaseCuentaPUC = Mid$(strCodigoCuentaPUC, 1, 2)
                strFila += Formatear_Campo_SIIGO(strClaseCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '7 - 13
                strGrupoCuentaPUC = Mid$(strCodigoCuentaPUC, 3, 2)
                strFila += Formatear_Campo_SIIGO(strGrupoCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '8 - 30
                strCuentaPUC = Mid$(strCodigoCuentaPUC, 5, 2)
                strFila += Formatear_Campo_SIIGO(strCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '9 - 05
                strSubCuenta = Mid$(strCodigoCuentaPUC, 7, 2)
                strFila += Formatear_Campo_SIIGO(strSubCuenta, 2, CAMPO_NUMERICO_SIIGO) '10 - 01
                strFila += Formatear_Campo_SIIGO("00", 2, CAMPO_NUMERICO_SIIGO) '11 - 00

                ' Producto Transporte Carretera
                ' 300-Linea
                ' 0001-Grupo
                ' 000001-Elemento
                strFila += Formatear_Campo_SIIGO("300", 3, CAMPO_NUMERICO_SIIGO) '12
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '13
                strFila += Formatear_Campo_SIIGO("1", 6, CAMPO_NUMERICO_SIIGO) '14

                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '15-Año
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '16-Mes
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '17-Dia
                strFila += Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '18-Centro Costo
                strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '19-Sub Centro Costo

                ' Consultar en el Comprobante Egreso el documento origen que en este caso es No. Planilla Despacho
                strObservaciones = "PLANILLA No. " & lonNumePlan.ToString
                ' sdrArchInte("Observaciones")
                strFila += Formatear_Campo_SIIGO(strObservaciones, 50, CAMPO_ALFANUMERICO_SIIGO) '20-Descripción

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO("D", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoDebi, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                Else
                    strFila += Formatear_Campo_SIIGO("C", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoCred, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                End If
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '23-Valor Decimales
                strFila += Formatear_Campo_SIIGO(dblValoBase, 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '25-Valor Base Retención Decimales

                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '26-Codigo Vendedor 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '27-Codigo Ciudad - FALTA
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '28-Codigo Zona 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '29-Codigo Bodega
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '30-Codigo Ubicación
                strFila += Formatear_Campo_SIIGO("0", 10, CAMPO_NUMERICO_SIIGO) '31-Cantidad
                strFila += Formatear_Campo_SIIGO("0", 5, CAMPO_NUMERICO_SIIGO) '32-Cantidad Decimales

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO(TIPO_COMPROBANTE_EGRESOS, 1, CAMPO_ALFANUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_EGRESO, 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("", 1, CAMPO_NUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                End If

                If strClaseCuentaPUC = "13" And strGrupoCuentaPUC = "30" Then ' Si cuenta PUC es 1330 reportar en documento cruce el numero de comprobante
                    strFila += Formatear_Campo_SIIGO(lonNumeroComprobante, 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("0", 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                End If

                strFila += Formatear_Campo_SIIGO(intSecuencia, 3, CAMPO_NUMERICO_SIIGO) '36- Secuencia Documento Cruce
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '37-Año Vencimiento
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '38-Mes Vencimiento
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '39-Dia Vencimiento
                strFila += Formatear_Campo_SIIGO("0", 4, CAMPO_NUMERICO_SIIGO) '40-Forma Pago
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '41-Código Banco

                stwStreamWriter.WriteLine(strFila)

            End While
            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
                Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
            End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function


    ' Autor: JESUS CUADROS
    ' Fecha Creación: 08-AGO-2019
    ' Obsevaciones Creación: Se copia la misma rutina de egresos
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Function Generar_Documento_Comprobante_Ingreso_SIIGO() As Boolean
        Dim ComandoSQL As SqlCommand
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String
        Dim dblValoDebi As Double, dblValoCred As Double, dblValoBase As Double
        Dim strCodigoCuentaPUC As String, strClaseCuentaPUC As String, strGrupoCuentaPUC As String
        Dim strCuentaPUC As String, strSubCuenta As String, lonDocuOrig As Long, lonCodiEgre As Long

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            strSQL = "SELECT ENCC.EMPR_Codigo, ENCC.TIDO_Codigo, ENCC.Numero, ENCC.TIDO_Documento, ENCC.Numero_Documento,"
            strSQL += "ENCC.Fecha_Documento, ENCC.TERC_Documento, ENCC.Año, ENCC.Periodo, ENCC.OFIC_Codigo,"
            strSQL += "ENCC.Valor_Comprobante, ENCC.Fecha, ENCC.CATA_ESIC_Codigo, ENCC.Anulado,"
            strSQL += "DECC.ID, DECC.PLUC_Codigo, DECC.TERC_Codigo, DECC.Valor_Base, DECC.Valor_Debito, DECC.Valor_Credito, DECC.Observaciones,"
            strSQL += "PLUC.Codigo_Cuenta, TERC.Numero_Identificacion, ENCC.Codigo_Documento"
            strSQL += " FROM Encabezado_Comprobante_Contables As ENCC, Detalle_Comprobante_Contables As DECC, Plan_Unico_Cuentas AS PLUC, Terceros AS TERC"
            strSQL += " WHERE ENCC.EMPR_Codigo = DECC.EMPR_Codigo"
            strSQL += " AND ENCC.Numero  = DECC.ENCC_Numero"
            ' Plan Unico Cuentas
            strSQL += " AND DECC.EMPR_Codigo = PLUC.EMPR_Codigo"
            strSQL += " AND DECC.PLUC_Codigo  = PLUC.Codigo"
            ' Terceros
            strSQL += " AND DECC.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND DECC.TERC_Codigo  = TERC.Codigo"

            strSQL += " AND ENCC.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ENCC.Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND ENCC.Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
            strSQL += " AND ENCC.TIDO_Documento = " & TIDO_COMPROBANTE_INGRESO
            strSQL += " AND ENCC.CATA_ESIC_Codigo = " & ESTADO_INTERFAZ_PENDIENTE
            strSQL += " AND ENCC.Anulado = 0"
            strSQL += " ORDER BY ENCC.Numero_Documento"

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            While sdrArchInte.Read

                If lonNumeroComprobante = Val(sdrArchInte("Numero_Documento")) Then
                    intSecuencia += 1
                Else
                    lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                    intSecuencia = 1
                End If
                lonCodiEgre = sdrArchInte("Codigo_Documento") ' Código Comprobante Ingreso

                dblValoDebi = sdrArchInte("Valor_Debito")
                dblValoCred = sdrArchInte("Valor_Credito")
                dblValoBase = sdrArchInte("Valor_Base")

                strFila = Formatear_Campo_SIIGO(TIPO_COMPROBANTE_INGRESOS, 1, CAMPO_ALFANUMERICO_SIIGO) '1-Tipo Comprobante
                strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_INGRESO, 3, CAMPO_NUMERICO_SIIGO) '2-Codigo Comprobante
                strFila += Formatear_Campo_SIIGO(lonNumeroComprobante, 11, CAMPO_NUMERICO_SIIGO) '3-No Documento
                strFila += Formatear_Campo_SIIGO(intSecuencia, 5, CAMPO_NUMERICO_SIIGO) ' 4- Secuencia
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Numero_Identificacion"), 13, CAMPO_NUMERICO_SIIGO) '5-Identificacion
                strFila += Formatear_Campo_SIIGO("0", 3, CAMPO_NUMERICO_SIIGO) '6-Sucursal

                ' Código Cuenta Contable
                strCodigoCuentaPUC = sdrArchInte("Codigo_Cuenta") ' 1305

                strClaseCuentaPUC = Mid$(strCodigoCuentaPUC, 1, 2)
                strFila += Formatear_Campo_SIIGO(strClaseCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '7 - 13
                strGrupoCuentaPUC = Mid$(strCodigoCuentaPUC, 3, 2)
                strFila += Formatear_Campo_SIIGO(strGrupoCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '8 - 30
                strCuentaPUC = Mid$(strCodigoCuentaPUC, 5, 2)
                strFila += Formatear_Campo_SIIGO(strCuentaPUC, 2, CAMPO_NUMERICO_SIIGO) '9 - 05
                strSubCuenta = Mid$(strCodigoCuentaPUC, 7, 2)
                strFila += Formatear_Campo_SIIGO(strSubCuenta, 2, CAMPO_NUMERICO_SIIGO) '10 - 01
                strFila += Formatear_Campo_SIIGO("00", 2, CAMPO_NUMERICO_SIIGO) '11 - 00

                ' Producto Transporte Carretera
                ' 300-Linea
                ' 0001-Grupo
                ' 000001-Elemento
                strFila += Formatear_Campo_SIIGO("300", 3, CAMPO_NUMERICO_SIIGO) '12
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '13
                strFila += Formatear_Campo_SIIGO("1", 6, CAMPO_NUMERICO_SIIGO) '14

                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '15-Año
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '16-Mes
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '17-Dia
                strFila += Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '18-Centro Costo
                strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '19-Sub Centro Costo
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Observaciones"), 50, CAMPO_ALFANUMERICO_SIIGO) '20-Descripción

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO("D", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoDebi, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                Else
                    strFila += Formatear_Campo_SIIGO("C", 1, CAMPO_ALFANUMERICO_SIIGO) '21-D/C
                    strFila += Formatear_Campo_SIIGO(dblValoCred, 13, CAMPO_NUMERICO_SIIGO) '22-Valor Movimiento
                End If
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '23-Valor Decimales
                strFila += Formatear_Campo_SIIGO(dblValoBase, 13, CAMPO_NUMERICO_SIIGO) '24-Valor Base Retención
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '25-Valor Base Retención Decimales

                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '26-Codigo Vendedor 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '27-Codigo Ciudad - FALTA
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '28-Codigo Zona 
                strFila += Formatear_Campo_SIIGO("1", 4, CAMPO_NUMERICO_SIIGO) '29-Codigo Bodega
                strFila += Formatear_Campo_SIIGO("1", 3, CAMPO_NUMERICO_SIIGO) '30-Codigo Ubicación
                strFila += Formatear_Campo_SIIGO("0", 10, CAMPO_NUMERICO_SIIGO) '31-Cantidad
                strFila += Formatear_Campo_SIIGO("0", 5, CAMPO_NUMERICO_SIIGO) '32-Cantidad Decimales

                If dblValoDebi > 0 Then
                    strFila += Formatear_Campo_SIIGO(TIPO_COMPROBANTE_EGRESOS, 1, CAMPO_NUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_EGRESO, 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                Else
                    strFila += Formatear_Campo_SIIGO("", 1, CAMPO_NUMERICO_SIIGO) '33-Tipo Documento Cruce
                    strFila += Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '34-Código Comprobante Cruce
                End If

                If strClaseCuentaPUC = "13" And strGrupoCuentaPUC = "05" Then ' Si cuenta PUC es 1305 reportar en documento cruce el documento origen Ej: Factura

                    ' Consultar en el Comprobante Egreso el documento origen que en este caso es No. Planilla Despacho
                    If objGeneral.Retorna_Campo_BD(intEmpresa, "Encabezado_Documento_Comprobantes", "Documento_Origen", "Codigo", CAMPO_NUMERICO, lonCodiEgre, lonDocuOrig, "TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO) Then
                        strFila += Formatear_Campo_SIIGO(lonDocuOrig, 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                    Else
                        strFila += Formatear_Campo_SIIGO("0", 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                    End If

                Else
                    strFila += Formatear_Campo_SIIGO("0", 11, CAMPO_NUMERICO_SIIGO) '35-Número Documento Cruce
                End If

                strFila += Formatear_Campo_SIIGO(intSecuencia, 3, CAMPO_NUMERICO_SIIGO) '36- Secuencia Documento Cruce
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Año"), 4, CAMPO_NUMERICO_SIIGO) '37-Año Vencimiento
                strFila += Formatear_Campo_SIIGO(sdrArchInte("Periodo"), 2, CAMPO_NUMERICO_SIIGO) '38-Mes Vencimiento
                strFila += Formatear_Campo_SIIGO(Day(sdrArchInte("Fecha")), 2, CAMPO_NUMERICO_SIIGO) '39-Dia Vencimiento
                strFila += Formatear_Campo_SIIGO("0", 4, CAMPO_NUMERICO_SIIGO) '40-Forma Pago
                strFila += Formatear_Campo_SIIGO("0", 2, CAMPO_NUMERICO_SIIGO) '41-Código Banco

                stwStreamWriter.WriteLine(strFila)

            End While
            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
                Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
            End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally
            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    Private Function Generar_Documento_Legalizacion_Gastos_SIIGO() As Boolean

        Dim ComandoSQL As SqlCommand

        Dim intSecuencia As Integer, lonNumeroLiquidacion As Long, lonCodigoTercero As Long
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter
        Dim strFila As String = "", strCuentaPUCNeto As String = "", strCuentaPUCFletes As String = ""
        Dim dblValCuePorPag As Double = 0, dblValor As Double = 0, strFechaComp As String = ""
        Dim dblValorBase As Double = 0, intAnulado As Integer = 0

        'Si el archivo existe se borra 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If
        Date.TryParse(Request.QueryString("FechInic"), dtaFechaInicial)
        Date.TryParse(Request.QueryString("FechFina"), dtaFechaFinal)
        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, Text.Encoding.Default)

        ' Consultar movimientos contables de liquidación manifiestos
        strSQL = " SELECT DISTINCT EMPR_Codigo, TIDO_Codigo, ENCC_Numero, PLUC_Codigo, ID, "
        strSQL += " TERC_Codigo, Valor_Debito * 100 AS Valor_Debito  , Valor_Credito * 100 AS Valor_Credito , Valor_Base * 100 AS Valor_Base, Observaciones, "
        strSQL += " Centro_Costo, Documento_Cruce, Prefijo, Codigo_Anexo, "
        strSQL += " Sufijo_Codigo_Anexo,   Numero, Fecha, "
        strSQL += " TIDO_Documento, Numero_Documento, Fecha_Documento, TERC_Documento, "
        strSQL += " OFIC_Documento, Ano, Periodo,  ObservacionesEncabezado, "
        strSQL += " Valor_Comprobante, TIDO_Documento_Cruce, Numero_Documento_Cruce, Anulado, Estado, "

        strSQL += " Fecha_Anula,  OFIC_Codigo, NombreCuenta"
        strSQL += " FROM V_Generar_Detalle_Comprobante_Contables "
        strSQL += " WHERE EMPR_Codigo = " & intEmpresa
        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
        strSQL += " AND TIDO_Codigo = " & TIDO_COMPROBANTE_CONTABLE
        strSQL += " AND Interfaz_Contable = " & ARCHIVO_BORRADOR
        strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO
        strSQL += " AND TIDO_Documento_Cruce = " & TIDO_LEGALIZACION_GASTOS
        strSQL += " ORDER BY Numero"

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        ' En R&R la contabilización se hace en el Manifiesto y el ajuste por mayor o menor valor lo manejan con notas contables directamente en SIIGO
        ' En la liquidación solo se contabilizan los conceptos de descuento al crédito y en la cuenta x pagar va el total de estos conceptos al débito
        ' Como es un manejo especial la cxp al débito se ingresa en la generación del archivo plano mediante la rutina Armar_Fila_CxP_Liquidacion_Manifiesto_TRRYR

        intSecuencia = 1
        lonNumeroLiquidacion = 0
        Try
            While sdrArchInte.Read

                If lonNumeroLiquidacion = Val(sdrArchInte("Documento_Cruce")) Then
                    intSecuencia += 1
                Else

                    If strFila <> "" Then
                        ' Insertar Linea Cuenta PUC CxP al débito para finalizar el movimiento
                        'Call Armar_Fila_CxP_Liquidacion_Manifiesto_TRRYR(strFila, dblValCuePorPag, lonNumeroLiquidacion, strFechaComp, intSecuencia)
                        stwStreamWriter.WriteLine(strFila)
                        dblValCuePorPag = 0
                    End If

                    lonNumeroLiquidacion = Val(sdrArchInte("Documento_Cruce"))
                    strFechaComp = sdrArchInte("Fecha_Documento")
                    intSecuencia = 1
                End If

                ' Consultar información adicional tercero
                lonCodigoTercero = sdrArchInte("TERC_Codigo")
                Dim arrCampos(2), arrValoresCampos(2) As String
                arrCampos(0) = "Numero_Identificacion"
                arrCampos(1) = "Digito_Chequeo"

                If Me.objGeneral.Retorna_Campos(intEmpresa, "Terceros", arrCampos, arrValoresCampos, 2, clsGeneral.CAMPO_NUMERICO, "Codigo", lonCodigoTercero, "", Me.strError) Then
                    Me.objTercero.NumeroIdentificacion = arrValoresCampos(0)
                    Me.objTercero.DigitoChequeo = Val(arrValoresCampos(1))
                End If

                strFila = Formatear_Campo_SIIGO(TIPO_COMPROBANTE_LIQUIDACIONES, 1, CAMPO_ALFANUMERICO_SIIGO) '1-Tipo Comprobante Liquidacion
                strFila = strFila & Formatear_Campo_SIIGO(CODIGO_COMPROBANTE_LIQUIDACIONES, 3, CAMPO_NUMERICO_SIIGO) '2-Codigo Comprobante Liquidacion
                strFila = strFila & Formatear_Campo_SIIGO(sdrArchInte("Documento_Cruce"), 11, CAMPO_NUMERICO_SIIGO) '3-No Liquidacion                
                strFila = strFila & Formatear_Campo_SIIGO(intSecuencia.ToString, 5, CAMPO_NUMERICO_SIIGO) '4-Secuencia
                strFila = strFila & Formatear_Campo_SIIGO(Me.objTercero.NumeroIdentificacion, 13, CAMPO_NUMERICO_SIIGO) '5-NIT 

                strFila = strFila & Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '6-Sucursal
                strFila = strFila & Formatear_Campo_SIIGO(sdrArchInte("PLUC_Codigo"), 10, CAMPO_CEROS_DERECHA_SIIGO) '7-Cuenta Contable
                strFila = strFila & Formatear_Campo_SIIGO("", 13, CAMPO_NUMERICO_SIIGO) '8-Codigo Producto
                strFila = strFila & Formatear_Campo_SIIGO(sdrArchInte("Fecha"), 8, CAMPO_FECHA_SIIGO) '9-Fecha Documento
                strFila = strFila & Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '10-Centro Costo

                strFila = strFila & Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '11-Sub Centro de Costo
                strFila = strFila & Formatear_Campo_SIIGO("Lega No. " & sdrArchInte("Documento_Cruce"), 50, CAMPO_ALFANUMERICO_SIIGO) '12-Descripción del movimiento
                If sdrArchInte("Valor_Credito") > 0 Then
                    strFila = strFila & Formatear_Campo_SIIGO(NATURALEZA_CREDITO, 1, CAMPO_ALFANUMERICO_SIIGO) '13-Débito-Crédito
                ElseIf sdrArchInte("Valor_Debito") > 0 Then
                    strFila = strFila & Formatear_Campo_SIIGO(NATURALEZA_DEBITO, 1, CAMPO_ALFANUMERICO_SIIGO) '13-Débito-Crédito
                End If
                If intAnulado = clsGeneral.ESTADO_ANULADO Then
                    dblValor = clsGeneral.CERO
                    dblValorBase = clsGeneral.CERO
                Else
                    dblValor = Val(sdrArchInte("Valor_Debito")) + Val(sdrArchInte("Valor_Credito"))
                End If
                strFila = strFila & Formatear_Campo_SIIGO(dblValor, 15, CAMPO_NUMERICO_SIIGO) '14-Vr movimiento*100 credito
                strFila = strFila & Formatear_Campo_SIIGO("", 15, CAMPO_NUMERICO_SIIGO) '15-Base retencion
                dblValCuePorPag = dblValCuePorPag + dblValor

                strFila = strFila & Formatear_Campo_SIIGO(CODIGO_VENDEDOR, 4, CAMPO_NUMERICO_SIIGO) '16-Codigo Vendedor
                If sdrArchInte("OFIC_Codigo") IsNot DBNull.Value Then
                    strFila = strFila & Formatear_Campo_SIIGO(sdrArchInte("OFIC_Codigo"), 4, CAMPO_NUMERICO_SIIGO) '17-Codigo Ciudad
                Else
                    strFila = strFila & Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '17-Codigo Ciudad
                End If
                strFila = strFila & Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '18-Codigo Zona 
                strFila = strFila & Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '19-Codigo Bodega 
                strFila = strFila & Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '20-Codigo Ubicación

                strFila = strFila & Formatear_Campo_SIIGO("", 15, CAMPO_NUMERICO_SIIGO) '21-Cantidad
                strFila = strFila & Formatear_Campo_SIIGO("", 1, CAMPO_ALFANUMERICO_SIIGO) '22-Tipo Documento Cruce
                strFila = strFila & Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '23-Codigo Comprobante Cruce
                strFila = strFila & Formatear_Campo_SIIGO("", 11, CAMPO_NUMERICO_SIIGO) '24-Numero Documento Cruce
                strFila = strFila & Formatear_Campo_SIIGO("", 3, CAMPO_NUMERICO_SIIGO) '25-Secuencia Docuemento Cruce

                strFila = strFila & Formatear_Campo_SIIGO("", 8, CAMPO_FECHA_SIIGO) '26-Fecha Vencimiento Documento Cruce
                strFila = strFila & Formatear_Campo_SIIGO("", 4, CAMPO_NUMERICO_SIIGO) '27-Codigo Forma de Pago
                strFila = strFila & Formatear_Campo_SIIGO("", 2, CAMPO_NUMERICO_SIIGO) '28-Codigo del banco

                stwStreamWriter.WriteLine(strFila)

            End While

            sdrArchInte.Close()
            sdrArchInte.Dispose()
            stwStreamWriter.Close()
            stmStreamW.Close()

            Me.objGeneral.ConexionSQL.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Colocar_Interfaz_Definitivo("Encabezado_Liquida_Manifiestos", TIDO_LIQUIDA_MANIFIESTO_TERCEROS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_INTERFAZ_LIQUIDACION_DESPACHOS_Manifiesto_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False

        Finally
            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try

    End Function

#End Region

#Region "Funciones INTERFAZ UNOEE"

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Comprobantes Egreso
    Private Function Generar_Documento_Comprobante_Egreso_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

            strSQL += " AND TIDO_Documento = " & TIDO_COMPROBANTE_EGRESO
            strSQL += " AND Anulado = 0"
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            Dim auxConsecutivo As Integer = 3

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            'Documento INTERFAZ
            strFila = Formatear_Campo_UNO_EE(2, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
            strFila += Formatear_Campo_UNO_EE("1001", 4, CAMPO_ALFANUMERICO) '6-Centro Operacion General

            strFila += Formatear_Campo_UNO_EE(MAESTRO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO_UNO_EE, 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
            strFila += Formatear_Campo_UNO_EE(DateTime.Now.ToString("yyyyMMdd"), 8, CAMPO_ALFANUMERICO) '8-Fecha Generacion Documento

            strFila += Formatear_Campo_UNO_EE("00030", 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '10-Sub Tipo Registro

            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
            strFila += Formatear_Campo_UNO_EE("COMPROBANTEDEEGRESO" & fechaAux, 270, CAMPO_ALFANUMERICO) '11 Observacion
            stwStreamWriter.WriteLine(strFila)

            While sdrArchInte.Read


                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 20, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                strFila += Formatear_Campo_UNO_EE(MAESTRO_FLUJO_EFECTIVO_UNO_EE, 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo

                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 8, CAMPO_NUMERICO) '21-Tipo Documento
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento

                stwStreamWriter.WriteLine(strFila)
                auxConsecutivo += 1

            End While

            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa

            stwStreamWriter.WriteLine(strFila)

            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Comprobantes Egreso
    Private Function Generar_Documento_Cuentas_Pagar_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_CPP_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

            strSQL += " AND TIDO_Documento = " & TIDO_COMPROBANTE_EGRESO
            strSQL += " AND Anulado = 0"
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            Dim auxConsecutivo As Integer = 3

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            'Documento INTERFAZ
            strFila = Formatear_Campo_UNO_EE(2, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
            strFila += Formatear_Campo_UNO_EE("1001", 4, CAMPO_ALFANUMERICO) '6-Centro Operacion General

            strFila += Formatear_Campo_UNO_EE(MAESTRO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO_UNO_EE, 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
            strFila += Formatear_Campo_UNO_EE(DateTime.Now.ToString("yyyyMMdd"), 8, CAMPO_ALFANUMERICO) '8-Fecha Generacion Documento

            strFila += Formatear_Campo_UNO_EE("00030", 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '10-Sub Tipo Registro

            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
            strFila += Formatear_Campo_UNO_EE("CUENTAPORPAGAR" & fechaAux, 270, CAMPO_ALFANUMERICO) '11 Observacion
            stwStreamWriter.WriteLine(strFila)

            While sdrArchInte.Read

                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 20, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos

                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '14-Valor Debito 
                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '15-Valor Credito
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '16-Valor Debito Moneda Alterna 
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Credito Moneda Alterna

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '18-Observaciones
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '19-Sucursal Prooveedor
                strFila += Formatear_Campo_UNO_EE("", 4, CAMPO_ALFANUMERICO) '20-Maestro Prefijo Cruce

                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Documento_Cruce")).ToString, 8, CAMPO_NUMERICO) '21-Documento Cruce
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 3, CAMPO_NUMERICO) '22-Cuota Cruce

                strFila += Formatear_Campo_UNO_EE(MAESTRO_FLUJO_EFECTIVO_UNO_EE, 10, CAMPO_ALFANUMERICO) '23-Maestro Flujo Efectivo

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Formato"), 8, CAMPO_ALFANUMERICO) '24-Fecha
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Formato"), 8, CAMPO_ALFANUMERICO) '25-Fecha Alternativa

                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Descuento Otorgado
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Descuento Aplicado
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Descuento Aplicado Alterno

                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Retencion

                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Retencion Alterna

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Nota"), 255, CAMPO_ALFANUMERICO) '31-Nota

                stwStreamWriter.WriteLine(strFila)
                auxConsecutivo += 1

            End While

            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa

            stwStreamWriter.WriteLine(strFila)

            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Anticipos
    Private Function Generar_Documento_Anticipos_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Anticipos_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

            'strSQL += " AND TIDO_Documento = " & TIDO_COMPROBANTE_EGRESO
            strSQL += " AND Anulado = 0"
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            Dim auxConsecutivo As Integer = 3

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            'Documento INTERFAZ
            strFila = Formatear_Campo_UNO_EE(2, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
            strFila += Formatear_Campo_UNO_EE("1001", 4, CAMPO_ALFANUMERICO) '6-Centro Operacion General

            strFila += Formatear_Campo_UNO_EE(MAESTRO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO_UNO_EE, 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
            strFila += Formatear_Campo_UNO_EE(DateTime.Now.ToString("yyyyMMdd"), 8, CAMPO_ALFANUMERICO) '8-Fecha Generacion Documento

            strFila += Formatear_Campo_UNO_EE("00030", 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '10-Sub Tipo Registro

            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
            strFila += Formatear_Campo_UNO_EE("ANTICIPOS" & fechaAux, 270, CAMPO_ALFANUMERICO) '11 Observacion
            stwStreamWriter.WriteLine(strFila)

            While sdrArchInte.Read

                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos

                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '14-Valor Debito 
                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '15-Valor Credito
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '16-Valor Debito Moneda Alterna 
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Credito Moneda Alterna

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '18-Observaciones
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '19-Sucursal Prooveedor
                strFila += Formatear_Campo_UNO_EE("", 4, CAMPO_ALFANUMERICO) '20-Maestro Prefijo Cruce

                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Documento_Cruce")).ToString, 8, CAMPO_NUMERICO) '21-Documento Cruce
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 3, CAMPO_NUMERICO) '22-Cuota Cruce

                strFila += Formatear_Campo_UNO_EE(MAESTRO_FLUJO_EFECTIVO_UNO_EE, 10, CAMPO_ALFANUMERICO) '23-Maestro Flujo Efectivo

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Formato"), 8, CAMPO_ALFANUMERICO) '24-Fecha
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Formato"), 8, CAMPO_ALFANUMERICO) '25-Fecha Alternativa

                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Descuento Otorgado
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Descuento Aplicado
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Descuento Aplicado Alterno

                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Retencion

                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Retencion Alterna

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Nota"), 255, CAMPO_ALFANUMERICO) '31-Nota

                stwStreamWriter.WriteLine(strFila)
                auxConsecutivo += 1

            End While

            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa

            stwStreamWriter.WriteLine(strFila)

            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_COMPROBANTE_EGRESO)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Facturas
    Private Function Generar_Documento_Facturas_UNO_EE_OLD() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            Dim auxConsecutivo As Integer = 2

            ' Consulta generada para crear los documentos del archivo plano de Facturas UNOEE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Factura_Contable_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND TIDO_Documento = " & TIDO_FACTURAS
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If
            'strSQL += " AND Numero >= 66156" ' Encabezado_Comprobante_Contables.Numero
            strSQL += " AND Numero_Documento IN (30217, 30218, 10527, 30219, 10528, 10529)"

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrDocuFact As SqlDataReader = ComandoSQL.ExecuteReader

            Dim dtsDocuFact As DataTable = New DataTable()
            dtsDocuFact.Load(sdrDocuFact)

            sdrDocuFact.Close()
            Me.objGeneral.ConexionSQL.Close()

            If dtsDocuFact.Rows.Count > 0 Then
                For Each rowFact As DataRow In dtsDocuFact.Rows

                    If rowFact("Numero") = 66156 Then

                        strSQL = ""
                        strSQL = ""

                    End If

                    ' Consulta generada para crear los documentos del archivo plano de Facturas UNO EE
                    strSQL = "SELECT * " & Chr(13)
                    strSQL += " FROM V_Interfaz_Encabezado_Contable_UNO_EE " & Chr(13)
                    strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)
                    strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
                    strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

                    strSQL += " AND TIDO_Documento = " & TIDO_FACTURAS
                    strSQL += " AND Anulado = 0"
                    strSQL += " AND Numero = " & CStr(rowFact("Numero"))

                    strSQL += " AND Numero_Documento = " & CStr(rowFact("Numero_Documento"))

                    If intOficina > 0 Then
                        strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
                    End If

                    ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                    Me.objGeneral.ConexionSQL.Open()
                    Dim sdrDocuInte As SqlDataReader = ComandoSQL.ExecuteReader

                    Dim dtsDocuInte As DataTable = New DataTable()
                    dtsDocuInte.Load(sdrDocuInte)

                    sdrDocuInte.Close()
                    Me.objGeneral.ConexionSQL.Close()

                    If dtsDocuInte.Rows.Count > 0 Then

                        For Each row As DataRow In dtsDocuInte.Rows
                            Dim DocuNumero As String = CStr(row("Numero_Documento"))
                            Dim DocuNumeroInterno As String = CStr(row("Codigo_Documento"))
                            Dim DocuMovim As String = CStr(row("Numero"))

                            ' Documento INTERFAZ
                            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
                            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 1, CAMPO_ALFANUMERICO) '6-Tipo Consecutivo
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Centro_Operacion")), 3, CAMPO_ALFANUMERICO) '7-Centro Operacion General

                            strFila += Formatear_Campo_UNO_EE(CStr(row("Tipo_Documento")), 3, CAMPO_ALFANUMERICO) '8-Tipo Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Numero_Documento")), 8, CAMPO_NUMERICO) '9-Numero Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Fecha_Documento")), 8, CAMPO_ALFANUMERICO) '10-Fecha Generacion Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Identificacion_Tercero")), 15, CAMPO_ALFANUMERICO) '11-Identificacion Tercero
                            strFila += Formatear_Campo_UNO_EE("00030", 5, CAMPO_ALFANUMERICO) '12-Maestro Codigo Cuenta Contable
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Concepto_Anula")), 1, CAMPO_NUMERICO) '13-Estado
                            strFila += Formatear_Campo_UNO_EE(1, 1, CAMPO_NUMERICO) '14-Impreso

                            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
                            strFila += Formatear_Campo_UNO_EE(MAESTRO_ENCABEZADO_FACTURA_VENTA_UNO_EE & fechaAux, 255, CAMPO_ALFANUMERICO) '15 Observacion
                            stwStreamWriter.WriteLine(strFila)

                            auxConsecutivo += 1

                            ' Consulta detalle movimientos contable 
                            strSQL = "SELECT * " & Chr(13)
                            strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
                            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)
                            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
                            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "
                            strSQL += " AND TIDO_Documento = " & TIDO_FACTURAS
                            strSQL += " AND Anulado = 0"
                            If intOficina > 0 Then
                                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
                            End If
                            strSQL += " AND Numero_Documento = " & DocuNumero
                            strSQL += " AND Numero = " & DocuMovim

                            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                            Me.objGeneral.ConexionSQL.Open()

                            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader
                            Dim dtsArchInte As DataTable = New DataTable()

                            dtsArchInte.Load(sdrArchInte)
                            sdrArchInte.Close()
                            Me.objGeneral.ConexionSQL.Close()

                            ' Consulta detalle Remesas Factura
                            strSQL = "SELECT * FROM V_Detalle_Interfaz_Movimiento_Contable_Factura_UNO_EE" & Chr(13)
                            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & " AND ENFA_Numero = " & DocuNumeroInterno

                            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                            Me.objGeneral.ConexionSQL.Open()
                            Dim sdrRemesas As SqlDataReader = ComandoSQL.ExecuteReader

                            Dim dtsRemesas As DataTable = New DataTable()
                            dtsRemesas.Load(sdrRemesas)
                            sdrRemesas.Close()
                            Me.objGeneral.ConexionSQL.Close()

                            If dtsArchInte.Rows.Count > 0 Then ' Verifica si existe detalle de movimientos
                                For Each detmov As DataRow In dtsArchInte.Rows
                                    If dtsRemesas.Rows.Count > 0 And (CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Or CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_INTERMEDIACION_ALTERNATIVA_LOGISTICA) Then ' Verifica si existe un detalle de remesas y si la cuenta aplica
                                        For Each detrem As DataRow In dtsRemesas.Rows
                                            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                                            strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                                            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                                            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                                            strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                            strFila += Formatear_Campo_UNO_EE(detmov("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
                                            lonNumeroComprobante = Val(detmov("Numero_Documento"))
                                            strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento
                                            strFila += Formatear_Campo_UNO_EE(detmov("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                                            strFila += Formatear_Campo_UNO_EE(detmov("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                                            'strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                            If detrem("Centro_Operacion").ToString <> "" Then
                                                strFila += Formatear_Campo_UNO_EE(detrem("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                            Else
                                                strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                            End If
                                            strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                            strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                                            strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo
                                            'strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                            If Val(detmov("Valor_Debito")) > 0 Then
                                                If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                                    strFila += Formatear_Campo_UNO_EE(Val(detrem("Total_Flete_Transportador")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                                Else
                                                    strFila += Formatear_Campo_UNO_EE(Val(detrem("Intermediacion")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                                End If
                                            Else
                                                strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                            End If
                                            'strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                            If Val(detmov("Valor_Credito")) > 0 Then
                                                If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                                    strFila += Formatear_Campo_UNO_EE(Val(detrem("Total_Flete_Transportador")).ToString, 21, CAMPO_DINERO) '16-Valor Credito 
                                                Else
                                                    strFila += Formatear_Campo_UNO_EE(Val(detrem("Intermediacion")).ToString, 21, CAMPO_DINERO) '16-Valor Credito 
                                                End If
                                            Else
                                                strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                            End If
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                                            'strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base
                                            If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                                strFila += Formatear_Campo_UNO_EE(Val(detrem("Total_Flete_Transportador")).ToString, 21, CAMPO_DINERO) '19-Valor Base
                                            Else
                                                strFila += Formatear_Campo_UNO_EE(Val(detrem("Intermediacion")).ToString, 21, CAMPO_DINERO) '19-Valor Base 
                                            End If
                                            strFila += Formatear_Campo_UNO_EE(detmov("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                                            strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
                                            Dim observaciones As String = ""
                                            If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                                observaciones = "COSTO" & detrem("Numero_Documento").ToString
                                            Else
                                                observaciones = "INTERMEDIACIÓN" & detrem("Numero_Documento").ToString
                                            End If
                                            strFila += Formatear_Campo_UNO_EE(detmov("Observaciones") & observaciones, 255, CAMPO_ALFANUMERICO) '22-Tipo Documento
                                            stwStreamWriter.WriteLine(strFila)
                                            auxConsecutivo += 1
                                        Next
                                    Else
                                        strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                                        strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                                        If detmov("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXC_ALTERNATIVA_LOGISTICA Then ' Movimiento Contable
                                            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                        Else
                                            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_CXC_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                        End If
                                        strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                                        strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                                        strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                        strFila += Formatear_Campo_UNO_EE(detmov("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                                        lonNumeroComprobante = Val(detmov("Numero_Documento"))
                                        strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                                        strFila += Formatear_Campo_UNO_EE(detmov("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                                        strFila += Formatear_Campo_UNO_EE(detmov("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                                        strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                        strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                        strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos

                                        If detmov("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXC_ALTERNATIVA_LOGISTICA Then ' Movimiento Contable

                                            strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo
                                            strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                            strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

                                            strFila += Formatear_Campo_UNO_EE(detmov("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                                            'strFila += Formatear_Campo_UNO_EE(detmov("Codigo_Cuenta"), 8, CAMPO_NUMERICO) '21-Tipo Documento
                                            strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
                                            strFila += Formatear_Campo_UNO_EE(detmov("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento

                                        Else
                                            'Cuenta Por Cobrar
                                            strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                            strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna

                                            strFila += Formatear_Campo_UNO_EE(detmov("Observaciones"), 255, CAMPO_ALFANUMERICO) '19 Observaciones
                                            strFila += Formatear_Campo_UNO_EE("001", 3, CAMPO_ALFANUMERICO) '20- Sucursal
                                            strFila += Formatear_Campo_UNO_EE(detmov("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '21-Tipo Documento Cruce

                                            lonNumeroComprobante = Val(detmov("Numero_Documento"))
                                            strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '22-No Documento Cruce
                                            strFila += Formatear_Campo_UNO_EE(1, 3, CAMPO_NUMERICO) '23-Cuota
                                            strFila += Formatear_Campo_UNO_EE(detmov("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '24-Fecha Vence
                                            strFila += Formatear_Campo_UNO_EE(detmov("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '25-Fecha Dcto

                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '31-Valor Credito Moneda Alterna
                                            strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '32-Valor Credito Moneda Alterna

                                            strFila += Formatear_Campo_UNO_EE(Val(detmov("EMPR_Numero_Identificacion")).ToString, 15, CAMPO_ALFANUMERICO) '33-Documento Tercero
                                            strFila += Formatear_Campo_UNO_EE(detmov("Observaciones"), 255, CAMPO_ALFANUMERICO) '34-Observaciones

                                        End If
                                        stwStreamWriter.WriteLine(strFila)
                                        auxConsecutivo += 1
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If

            ' Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 03-Mayo-2021
    ' Observaciones: Se modifico la rutina incial de Santiago Correa porque utilizaba una consulta inicial que no era requerida y que ademas no generaba la información correcta

    Private Function Generar_Documento_Facturas_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            Dim auxConsecutivo As Integer = 2

            ' Consultar Comprobantes Contables Tipo Factura
            strSQL = "SELECT * "
            strSQL += " FROM V_Interfaz_Encabezado_Contable_UNO_EE "
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString
            strSQL += " AND TIDO_Documento = " & TIDO_FACTURAS
            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= DATEADD(DAY, 1, CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "
            strSQL += " AND Anulado = 0" 'ENCC.Anulado
            strSQL += " AND Anulado_Factura = 0"
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrDocuInte As SqlDataReader = ComandoSQL.ExecuteReader

            Dim dtsDocuInte As DataTable = New DataTable()
            dtsDocuInte.Load(sdrDocuInte)

            sdrDocuInte.Close()
            Me.objGeneral.ConexionSQL.Close()

            If dtsDocuInte.Rows.Count > 0 Then

                For Each row As DataRow In dtsDocuInte.Rows
                    Dim DocuNumero As String = CStr(row("Numero_Documento"))
                    Dim DocuNumeroInterno As String = CStr(row("Codigo_Documento"))
                    Dim DocuMovim As String = CStr(row("Numero"))
                    Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")

                    ' Encabezado Documento INTERFAZ
                    strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                    strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                    strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                    strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 1, CAMPO_ALFANUMERICO) '6-Tipo Consecutivo
                    strFila += Formatear_Campo_UNO_EE(CStr(row("Centro_Operacion")), 3, CAMPO_ALFANUMERICO) '7-Centro Operacion General
                    strFila += Formatear_Campo_UNO_EE(CStr(row("Tipo_Documento")), 3, CAMPO_ALFANUMERICO) '8-Tipo Documento
                    strFila += Formatear_Campo_UNO_EE(CStr(row("Numero_Documento")), 8, CAMPO_NUMERICO) '9-Numero Documento
                    strFila += Formatear_Campo_UNO_EE(CStr(row("Fecha_Documento")), 8, CAMPO_ALFANUMERICO) '10-Fecha Generacion Documento
                    strFila += Formatear_Campo_UNO_EE(CStr(row("Identificacion_Tercero")), 15, CAMPO_ALFANUMERICO) '11-Identificacion Tercero
                    strFila += Formatear_Campo_UNO_EE("00030", 5, CAMPO_ALFANUMERICO) '12-Maestro Codigo Cuenta Contable
                    strFila += Formatear_Campo_UNO_EE(CStr(row("Concepto_Anula")), 1, CAMPO_NUMERICO) '13-Estado
                    strFila += Formatear_Campo_UNO_EE(1, 1, CAMPO_NUMERICO) '14-Impreso
                    strFila += Formatear_Campo_UNO_EE(MAESTRO_ENCABEZADO_FACTURA_VENTA_UNO_EE & fechaAux, 255, CAMPO_ALFANUMERICO) '15 Observacion
                    stwStreamWriter.WriteLine(strFila)

                    auxConsecutivo += 1

                    ' Consultar Detalle Comprobante Contable 
                    strSQL = "SELECT *"
                    strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE"
                    strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString
                    strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
                    strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "
                    strSQL += " AND TIDO_Documento = " & TIDO_FACTURAS
                    strSQL += " AND Anulado = 0"
                    If intOficina > 0 Then
                        strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
                    End If
                    strSQL += " AND Numero_Documento = " & DocuNumero
                    strSQL += " AND Numero = " & DocuMovim

                    ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                    Me.objGeneral.ConexionSQL.Open()
                    Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

                    Dim dtsArchInte As DataTable = New DataTable()
                    dtsArchInte.Load(sdrArchInte)
                    sdrArchInte.Close()
                    Me.objGeneral.ConexionSQL.Close()

                    ' Consultar detalle Remesas Factura
                    strSQL = "SELECT * FROM V_Detalle_Interfaz_Movimiento_Contable_Factura_UNO_EE" & Chr(13)
                    strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & " AND ENFA_Numero = " & DocuNumeroInterno

                    ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                    Me.objGeneral.ConexionSQL.Open()
                    Dim sdrRemesas As SqlDataReader = ComandoSQL.ExecuteReader
                    Dim dtsRemesas As DataTable = New DataTable()

                    dtsRemesas.Load(sdrRemesas)
                    sdrRemesas.Close()
                    Me.objGeneral.ConexionSQL.Close()

                    If dtsArchInte.Rows.Count > 0 Then ' Verifica si existe detalle de movimientos
                        For Each detmov As DataRow In dtsArchInte.Rows
                            If dtsRemesas.Rows.Count > 0 And (CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Or CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_INTERMEDIACION_ALTERNATIVA_LOGISTICA) Then ' Verifica si existe un detalle de remesas y si la cuenta aplica
                                For Each detrem As DataRow In dtsRemesas.Rows
                                    strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                                    strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                    strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                                    strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                                    strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                    strFila += Formatear_Campo_UNO_EE(detmov("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
                                    lonNumeroComprobante = Val(detmov("Numero_Documento"))
                                    strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento
                                    strFila += Formatear_Campo_UNO_EE(detmov("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                                    strFila += Formatear_Campo_UNO_EE(detmov("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                                    'strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                    If detrem("Centro_Operacion").ToString <> "" Then
                                        strFila += Formatear_Campo_UNO_EE(detrem("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                    Else
                                        strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                    End If
                                    strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                    strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                                    strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo
                                    'strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    If Val(detmov("Valor_Debito")) > 0 Then
                                        If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                            strFila += Formatear_Campo_UNO_EE(Val(detrem("Total_Flete_Transportador")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                        Else
                                            strFila += Formatear_Campo_UNO_EE(Val(detrem("Intermediacion")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                        End If
                                    Else
                                        strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    End If
                                    'strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    If Val(detmov("Valor_Credito")) > 0 Then
                                        If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                            strFila += Formatear_Campo_UNO_EE(Val(detrem("Total_Flete_Transportador")).ToString, 21, CAMPO_DINERO) '16-Valor Credito 
                                        Else
                                            strFila += Formatear_Campo_UNO_EE(Val(detrem("Intermediacion")).ToString, 21, CAMPO_DINERO) '16-Valor Credito 
                                        End If
                                    Else
                                        strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    End If
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                                    'strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base
                                    If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                        strFila += Formatear_Campo_UNO_EE(Val(detrem("Total_Flete_Transportador")).ToString, 21, CAMPO_DINERO) '19-Valor Base
                                    Else
                                        strFila += Formatear_Campo_UNO_EE(Val(detrem("Intermediacion")).ToString, 21, CAMPO_DINERO) '19-Valor Base 
                                    End If
                                    strFila += Formatear_Campo_UNO_EE(detmov("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
                                    Dim observaciones As String = ""
                                    If CStr(detmov("Codigo_Cuenta")) = CUENTA_MOVIMIENTO_REMESA_ALTERNATIVA_LOGISTICA Then
                                        observaciones = "COSTO" & detrem("Numero_Documento").ToString
                                    Else
                                        observaciones = "INTERMEDIACIÓN" & detrem("Numero_Documento").ToString
                                    End If
                                    strFila += Formatear_Campo_UNO_EE(detmov("Observaciones") & observaciones, 255, CAMPO_ALFANUMERICO) '22-Tipo Documento
                                    stwStreamWriter.WriteLine(strFila)
                                    auxConsecutivo += 1
                                Next
                            Else
                                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                                If detmov("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXC_ALTERNATIVA_LOGISTICA Then ' Movimiento Contable
                                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                Else
                                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_CXC_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                End If
                                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                                strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                strFila += Formatear_Campo_UNO_EE(detmov("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                                lonNumeroComprobante = Val(detmov("Numero_Documento"))
                                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                                strFila += Formatear_Campo_UNO_EE(detmov("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                                strFila += Formatear_Campo_UNO_EE(detmov("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                                strFila += Formatear_Campo_UNO_EE(detmov("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos

                                If detmov("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXC_ALTERNATIVA_LOGISTICA Then ' Movimiento Contable

                                    strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo
                                    strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

                                    strFila += Formatear_Campo_UNO_EE(detmov("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                                    'strFila += Formatear_Campo_UNO_EE(detmov("Codigo_Cuenta"), 8, CAMPO_NUMERICO) '21-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE(detmov("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento

                                Else
                                    'Cuenta Por Cobrar
                                    strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    strFila += Formatear_Campo_UNO_EE(Val(detmov("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna

                                    strFila += Formatear_Campo_UNO_EE(detmov("Observaciones"), 255, CAMPO_ALFANUMERICO) '19 Observaciones
                                    strFila += Formatear_Campo_UNO_EE("001", 3, CAMPO_ALFANUMERICO) '20- Sucursal
                                    strFila += Formatear_Campo_UNO_EE(detmov("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '21-Tipo Documento Cruce

                                    lonNumeroComprobante = Val(detmov("Numero_Documento"))
                                    strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '22-No Documento Cruce
                                    strFila += Formatear_Campo_UNO_EE(1, 3, CAMPO_NUMERICO) '23-Cuota
                                    strFila += Formatear_Campo_UNO_EE(detmov("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '24-Fecha Vence
                                    strFila += Formatear_Campo_UNO_EE(detmov("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '25-Fecha Dcto

                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '31-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '32-Valor Credito Moneda Alterna

                                    strFila += Formatear_Campo_UNO_EE(Val(detmov("EMPR_Numero_Identificacion")).ToString, 15, CAMPO_ALFANUMERICO) '33-Documento Tercero
                                    strFila += Formatear_Campo_UNO_EE(detmov("Observaciones"), 255, CAMPO_ALFANUMERICO) '34-Observaciones

                                End If
                                stwStreamWriter.WriteLine(strFila)
                                auxConsecutivo += 1
                            End If
                        Next
                    End If

                Next
            End If

            ' Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Liquidacion

    Private Function Generar_Documento_Liquidacion_UNO_EE_OLD() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            Dim auxConsecutivo As Integer = 2

            ' Consulta generada para crear los documentos del archivo plano de Facturas UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Liquidacion_Contable_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrDocuLiqu As SqlDataReader = ComandoSQL.ExecuteReader

            Dim dtsDocuLiqu As DataTable = New DataTable()
            dtsDocuLiqu.Load(sdrDocuLiqu)

            sdrDocuLiqu.Close()
            Me.objGeneral.ConexionSQL.Close()
            If dtsDocuLiqu.Rows.Count > 0 Then
                For Each rowLiqu As DataRow In dtsDocuLiqu.Rows

                    ' Consulta generada para crear los documentos del archivo plano de Facturas UNO EE
                    strSQL = "SELECT * " & Chr(13)
                    strSQL += " FROM V_Interfaz_Encabezado_Contable_UNO_EE " & Chr(13)
                    strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

                    strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
                    strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

                    strSQL += " AND TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
                    strSQL += " AND Anulado = 0"
                    strSQL += " AND Numero = " & CStr(rowLiqu("Numero"))
                    strSQL += " AND Numero_Documento = " & CStr(rowLiqu("Numero_Documento"))
                    If intOficina > 0 Then
                        strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
                    End If

                    ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                    Me.objGeneral.ConexionSQL.Open()
                    Dim sdrDocuInte As SqlDataReader = ComandoSQL.ExecuteReader

                    Dim dtsDocuInte As DataTable = New DataTable()
                    dtsDocuInte.Load(sdrDocuInte)


                    sdrDocuInte.Close()
                    Me.objGeneral.ConexionSQL.Close()
                    If dtsDocuInte.Rows.Count > 0 Then
                        For Each row As DataRow In dtsDocuInte.Rows
                            Dim DocuNumero As String = CStr(row("Numero_Documento"))
                            Dim DocuMovim As String = CStr(row("Numero"))

                            'Documento INTERFAZ
                            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
                            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                            strFila += Formatear_Campo_UNO_EE(0, 1, CAMPO_NUMERICO) '6-Tipo Consecutivo
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Centro_Operacion")), 3, CAMPO_ALFANUMERICO) '7-Centro Operacion General

                            strFila += Formatear_Campo_UNO_EE(CStr(row("Tipo_Documento")), 3, CAMPO_ALFANUMERICO) '8-Tipo Documento
                            'strFila += Formatear_Campo_UNO_EE(CStr(row("Numero_Documento")), 8, CAMPO_NUMERICO) '9-Numero Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("ENMC_Numero")), 8, CAMPO_NUMERICO) '9-Numero Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("ENMC_Fecha")), 8, CAMPO_ALFANUMERICO) '10-Fecha Generacion Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Identificacion_Tercero")), 15, CAMPO_ALFANUMERICO) '11-Identificacion Tercero
                            strFila += Formatear_Campo_UNO_EE("00030", 5, CAMPO_ALFANUMERICO) '12-Maestro Codigo Cuenta Contable
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Concepto_Anula")), 1, CAMPO_NUMERICO) '13-Estado
                            strFila += Formatear_Campo_UNO_EE(1, 1, CAMPO_NUMERICO) '14-Impreso

                            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
                            strFila += Formatear_Campo_UNO_EE(MAESTRO_ENCABEZADO_LIQUIDACION_UNO_EE & fechaAux, 255, CAMPO_ALFANUMERICO) '15 Observacion
                            stwStreamWriter.WriteLine(strFila)

                            auxConsecutivo += 1

                            ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
                            strSQL = "SELECT * " & Chr(13)
                            strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
                            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

                            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
                            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

                            strSQL += " AND TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
                            strSQL += " AND Anulado = 0"
                            If intOficina > 0 Then
                                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
                            End If
                            strSQL += " AND Numero = " & DocuMovim
                            strSQL += " AND Numero_Documento = " & DocuNumero

                            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                            Me.objGeneral.ConexionSQL.Open()
                            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

                            While sdrArchInte.Read

                                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                                If sdrArchInte("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXP_ALTERNATIVA_LOGISTICA Then
                                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                    strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                                Else
                                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_CXP_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                    strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_CXP_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                                End If

                                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                                'lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                                lonNumeroComprobante = Val(sdrArchInte("ENMC_Numero"))
                                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento


                                If sdrArchInte("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXP_ALTERNATIVA_LOGISTICA Then
                                    strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                    strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                                    strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo

                                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento
                                Else
                                    strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 20, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                    strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    Dim intRecaTota As Integer = Val(sdrArchInte("Valor_Credito")) + Val(sdrArchInte("Valor_Pago_Recaudo_Total"))
                                    strFila += Formatear_Campo_UNO_EE(intRecaTota.ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna

                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE("001", 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                    strFila += Formatear_Campo_UNO_EE("", 4, CAMPO_ALFANUMERICO) '7-Tipo Documento

                                    lonNumeroComprobante = Val(sdrArchInte("ENMC_Numero"))
                                    strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '22-No Documento Cruce
                                    strFila += Formatear_Campo_UNO_EE(1, 3, CAMPO_NUMERICO) '23-Cuota
                                    strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo
                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '24-Fecha Vence
                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Vence"), 8, CAMPO_ALFANUMERICO) '25-Fecha Dcto

                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Valor Credito Moneda Alterna

                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '34-Observaciones
                                End If

                                stwStreamWriter.WriteLine(strFila)
                                'sdrArchInte.Close()
                                auxConsecutivo += 1

                            End While
                            sdrArchInte.Close()
                            Me.objGeneral.ConexionSQL.Close()

                        Next
                    End If
                Next
            End If
            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    Private Function Generar_Documento_Liquidacion_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            Dim auxConsecutivo As Integer = 2

            ' Consulta generada para crear los documentos del archivo plano de Liquidaciones UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Liquidacion_Contable_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrDocuLiqu As SqlDataReader = ComandoSQL.ExecuteReader

            Dim dtsDocuLiqu As DataTable = New DataTable()
            dtsDocuLiqu.Load(sdrDocuLiqu)

            sdrDocuLiqu.Close()
            Me.objGeneral.ConexionSQL.Close()
            If dtsDocuLiqu.Rows.Count > 0 Then
                For Each rowLiqu As DataRow In dtsDocuLiqu.Rows

                    ' Consulta generada para crear los documentos del archivo plano de Liquidaciones UNO EE
                    strSQL = "SELECT * " & Chr(13)
                    strSQL += " FROM V_Interfaz_Encabezado_Contable_UNO_EE " & Chr(13)
                    strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

                    strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
                    strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

                    strSQL += " AND TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
                    strSQL += " AND Anulado = 0"
                    strSQL += " AND Numero = " & CStr(rowLiqu("Numero"))
                    strSQL += " AND Numero_Documento = " & CStr(rowLiqu("Numero_Documento"))
                    If intOficina > 0 Then
                        strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
                    End If

                    ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                    Me.objGeneral.ConexionSQL.Open()
                    Dim sdrDocuInte As SqlDataReader = ComandoSQL.ExecuteReader

                    Dim dtsDocuInte As DataTable = New DataTable()
                    dtsDocuInte.Load(sdrDocuInte)


                    sdrDocuInte.Close()
                    Me.objGeneral.ConexionSQL.Close()
                    If dtsDocuInte.Rows.Count > 0 Then
                        For Each row As DataRow In dtsDocuInte.Rows
                            Dim DocuNumero As String = CStr(row("Numero_Documento"))
                            Dim DocuMovim As String = CStr(row("Numero"))

                            'Documento INTERFAZ
                            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
                            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                            strFila += Formatear_Campo_UNO_EE(0, 1, CAMPO_NUMERICO) '6-Tipo Consecutivo
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Centro_Operacion")), 3, CAMPO_ALFANUMERICO) '7-Centro Operacion General

                            strFila += Formatear_Campo_UNO_EE(CStr(row("Tipo_Documento")), 3, CAMPO_ALFANUMERICO) '8-Tipo Documento
                            'strFila += Formatear_Campo_UNO_EE(CStr(row("Numero_Documento")), 8, CAMPO_NUMERICO) '9-Numero Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("ENMC_Numero")), 8, CAMPO_NUMERICO) '9-Numero Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("ENMC_Fecha")), 8, CAMPO_ALFANUMERICO) '10-Fecha Generacion Documento
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Identificacion_Tercero")), 15, CAMPO_ALFANUMERICO) '11-Identificacion Tercero
                            strFila += Formatear_Campo_UNO_EE("00030", 5, CAMPO_ALFANUMERICO) '12-Maestro Codigo Cuenta Contable
                            strFila += Formatear_Campo_UNO_EE(CStr(row("Concepto_Anula")), 1, CAMPO_NUMERICO) '13-Estado
                            strFila += Formatear_Campo_UNO_EE(1, 1, CAMPO_NUMERICO) '14-Impreso

                            'Control de Cambios #002 Eliminar Campo fecha 
                            strFila += Formatear_Campo_UNO_EE(MAESTRO_ENCABEZADO_LIQUIDACION_UNO_EE, 255, CAMPO_ALFANUMERICO) '15 Observacion
                            stwStreamWriter.WriteLine(strFila)

                            auxConsecutivo += 1

                            ' Consulta generada para crear el archivo plano de Liquidaciones UNO EE
                            strSQL = "SELECT * " & Chr(13)
                            strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
                            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

                            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
                            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

                            strSQL += " AND TIDO_Documento = " & TIDO_LIQUIDA_MANIFIESTO_TERCEROS
                            strSQL += " AND Anulado = 0"
                            If intOficina > 0 Then
                                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
                            End If
                            strSQL += " AND Numero = " & DocuMovim
                            strSQL += " AND Numero_Documento = " & DocuNumero

                            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
                            Me.objGeneral.ConexionSQL.Open()
                            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

                            While sdrArchInte.Read
                                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                                If sdrArchInte("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXP_ALTERNATIVA_LOGISTICA Then
                                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                    strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                                Else
                                    strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_CXP_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                                    strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_CXP_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
                                End If

                                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                                'lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                                lonNumeroComprobante = Val(sdrArchInte("ENMC_Numero"))
                                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento


                                If sdrArchInte("Codigo_Cuenta").ToString <> CONSTANTE_CONTRA_CUENTA_CXP_ALTERNATIVA_LOGISTICA Then
                                    strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                    strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                                    strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo

                                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE(0, 8, CAMPO_NUMERICO) '21-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento
                                Else
                                    strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 20, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                                    strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                                    strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                                    Dim intRecaTota As Integer = Val(sdrArchInte("Valor_Credito")) + Val(sdrArchInte("Valor_Pago_Recaudo_Total"))
                                    strFila += Formatear_Campo_UNO_EE(intRecaTota.ToString, 21, CAMPO_DINERO) '16-Valor Credito
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna

                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento
                                    strFila += Formatear_Campo_UNO_EE("001", 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                                    strFila += Formatear_Campo_UNO_EE("", 4, CAMPO_ALFANUMERICO) '7-Tipo Documento

                                    lonNumeroComprobante = Val(sdrArchInte("ENMC_Numero"))
                                    strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '22-No Documento Cruce
                                    'Control Cambio #002  Inicializar Campo de 001 a 000
                                    strFila += Formatear_Campo_UNO_EE("000", 3, CAMPO_ALFANUMERICO) '23-Cuota
                                    strFila += Formatear_Campo_UNO_EE("    ", 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo
                                    'Control Cambio #002 Cambio de fecha a  Fecha_manifiesto
                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Manifiesto"), 8, CAMPO_ALFANUMERICO) '24-Fecha Vence
                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Manifiesto"), 8, CAMPO_ALFANUMERICO) '25-Fecha Dcto

                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '26-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '27-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '28-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '29-Valor Credito Moneda Alterna
                                    strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '30-Valor Credito Moneda Alterna

                                    strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '34-Observaciones
                                End If

                                stwStreamWriter.WriteLine(strFila)
                                'sdrArchInte.Close()
                                auxConsecutivo += 1

                            End While
                            sdrArchInte.Close()
                            Me.objGeneral.ConexionSQL.Close()

                        Next
                    End If
                Next
            End If
            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Nota Credito Movimiento
    Private Function Generar_Documento_Nota_Credito_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

            strSQL += " AND TIDO_Documento = " & TIDO_NOTA_CREDITO
            strSQL += " AND Anulado = 0"
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            Dim auxConsecutivo As Integer = 3

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            'Documento INTERFAZ
            strFila = Formatear_Campo_UNO_EE(2, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
            strFila += Formatear_Campo_UNO_EE("1001", 4, CAMPO_ALFANUMERICO) '6-Centro Operacion General

            strFila += Formatear_Campo_UNO_EE(MAESTRO_TIPO_DOCUMENTO_NOTA_CREDITO_UNO_EE, 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
            strFila += Formatear_Campo_UNO_EE(DateTime.Now.ToString("yyyyMMdd"), 8, CAMPO_ALFANUMERICO) '8-Fecha Generacion Documento

            strFila += Formatear_Campo_UNO_EE("00030", 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '10-Sub Tipo Registro

            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
            strFila += Formatear_Campo_UNO_EE("NOTACREDITO" & fechaAux, 270, CAMPO_ALFANUMERICO) '11 Observacion
            stwStreamWriter.WriteLine(strFila)

            While sdrArchInte.Read

                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '6-Centro Operacion
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '7-Tipo Documento

                lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '8-No Documento

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '10-Maestro Tercer
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '11-Maestro Centro Operacion Movimiento
                strFila += Formatear_Campo_UNO_EE(UNIDAD_NEGOCIO_UNO_EE, 2, CAMPO_ALFANUMERICO) '12-Maestro Unidad de Negocio
                strFila += Formatear_Campo_UNO_EE("", 15, CAMPO_ALFANUMERICO) '13-Maestro Centro de Costos
                strFila += Formatear_Campo_UNO_EE(MAESTRO_FLUJO_EFECTIVO_UNO_EE, 10, CAMPO_ALFANUMERICO) '14-Maestro Flujo Efectivo

                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Debito")).ToString, 21, CAMPO_DINERO) '15-Valor Debito 
                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Credito")).ToString, 21, CAMPO_DINERO) '16-Valor Credito
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '17-Valor Debito Moneda Alterna 
                strFila += Formatear_Campo_UNO_EE(CERO.ToString, 21, CAMPO_DINERO) '18-Valor Credito Moneda Alterna
                strFila += Formatear_Campo_UNO_EE(Val(sdrArchInte("Valor_Base")).ToString, 21, CAMPO_DINERO) '19-Valor Base

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Medio_Pago"), 2, CAMPO_ALFANUMERICO) '20-Tipo Documento
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Codigo_Cuenta"), 8, CAMPO_NUMERICO) '21-Tipo Documento
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '22-Tipo Documento

                stwStreamWriter.WriteLine(strFila)
                auxConsecutivo += 1

            End While

            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa

            stwStreamWriter.WriteLine(strFila)

            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    ' Autor: Santiago Correa
    ' Fecha Creación: 05-Mayo-2020
    ' Observaciones: Interfaz Contable UNO EE Nota Credito Documento
    Private Function Generar_Documento_Nota_Credito_Documental_UNO_EE() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        ' Si el archivo existe se borra primero 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo 
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        Try

            ' Consulta generada para crear el archivo plano de Comprobantes Egreso UNO EE
            strSQL = "SELECT * " & Chr(13)
            strSQL += " FROM V_Interfaz_Movimiento_Contable_UNO_EE " & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

            strSQL += " AND TIDO_Documento = " & 190
            strSQL += " AND Anulado = 0"
            If intOficina > 0 Then
                strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
            End If

            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
            Me.objGeneral.ConexionSQL.Open()
            Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

            Dim auxConsecutivo As Integer = 3

            'Encabezado INTERFAZ
            strFila = Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += CERO_STRING.ToString '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 7, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa
            stwStreamWriter.WriteLine(strFila)

            'Documento INTERFAZ
            strFila = Formatear_Campo_UNO_EE(2, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += Formatear_Campo_UNO_EE(TIPO_DOCUMENTO_DETALLE_UNO_EE, 4, CAMPO_NUMERICO) '2- Tipo Documento Encabezado
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
            strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
            strFila += Formatear_Campo_UNO_EE("1001", 4, CAMPO_ALFANUMERICO) '6-Centro Operacion General

            strFila += Formatear_Campo_UNO_EE(MAESTRO_TIPO_DOCUMENTO_NOTA_CREDITO_UNO_EE, 3, CAMPO_ALFANUMERICO) '7-Tipo Documento
            strFila += Formatear_Campo_UNO_EE(DateTime.Now.ToString("yyyyMMdd"), 8, CAMPO_ALFANUMERICO) '8-Fecha Generacion Documento

            strFila += Formatear_Campo_UNO_EE("00030", 20, CAMPO_ALFANUMERICO) '9-Maestro Codigo Cuenta Contable
            strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '10-Sub Tipo Registro

            Dim fechaAux As String = DateTime.Now.ToString("dd-MM-yyyy")
            strFila += Formatear_Campo_UNO_EE("NOTACREDITO" & fechaAux, 270, CAMPO_ALFANUMERICO) '11 Observacion
            stwStreamWriter.WriteLine(strFila)

            While sdrArchInte.Read

                strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
                strFila += Formatear_Campo_UNO_EE(TIPO_REGISTRO_UNO_EE, 4, CAMPO_NUMERICO) '2-Tipo Registro
                strFila += Formatear_Campo_UNO_EE(SUBTIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '3-Sub Tipo Registro
                strFila += Formatear_Campo_UNO_EE(VERSION_TIPO_REGISTRO_UNO_EE, 2, CAMPO_NUMERICO) '4-Version Tipo Registro

                strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '5-Codigo Empresa
                strFila += Formatear_Campo_UNO_EE(1, 1, CAMPO_NUMERICO) '6 - Tipo Consecutivo

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Centro_Operacion"), 3, CAMPO_ALFANUMERICO) '7-Centro Operacion
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Tipo_Documento"), 3, CAMPO_ALFANUMERICO) '8-Tipo Documento

                lonNumeroComprobante = Val(sdrArchInte("Numero_Documento"))
                strFila += Formatear_Campo_UNO_EE(lonNumeroComprobante, 8, CAMPO_NUMERICO) '9-No Documento

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Fecha_Concepto"), 8, CAMPO_ALFANUMERICO) '10-Fecha Formateada
                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Numero_Identificacion"), 15, CAMPO_ALFANUMERICO) '11-Maestro Tercer

                strFila += Formatear_Campo_UNO_EE("30", 5, CAMPO_ALFANUMERICO) '12-Clase Interna UNOEE
                strFila += Formatear_Campo_UNO_EE("0", 1, CAMPO_ALFANUMERICO) '13-Estado Elaboracion
                strFila += Formatear_Campo_UNO_EE("0", 1, CAMPO_ALFANUMERICO) '14-Impreso

                strFila += Formatear_Campo_UNO_EE(sdrArchInte("Observaciones"), 255, CAMPO_ALFANUMERICO) '15-Observacion

                stwStreamWriter.WriteLine(strFila)
                auxConsecutivo += 1

            End While

            'Pie de Pagina INTERFAZ
            strFila = Formatear_Campo_UNO_EE(auxConsecutivo, 7, CAMPO_NUMERICO) '1-Numero Consecutivo
            strFila += "9999" '2-Separador Encabezado
            strFila += Formatear_Campo_UNO_EE(1, 4, CAMPO_NUMERICO) '3-Empresa Encabezado
            strFila += Formatear_Campo_UNO_EE(MAESTRO_EMPRESA_UNO_EE, 3, CAMPO_NUMERICO) '4-Codigo Empresa

            stwStreamWriter.WriteLine(strFila)

            sdrArchInte.Close()

            stwStreamWriter.Close()
            stmStreamW.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Actualizar_Interfaz_Definitivo("Encabezado_Documento_Comprobantes", TIDO_FACTURAS)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

#End Region

#Region "Funciones INTERFAZ WORLD OFFICE"

    ' Generar Interfaz World Office Facturas
    Private Function Generar_Documento_Factura_WORLD_OFFICE() As Boolean

        Dim ComandoSQL As SqlCommand
        Dim dteFechaAux As Date
        Dim dteFechaVenceAux As Date
        Dim stmStreamW As Stream
        Dim stwStreamWriter As StreamWriter
        Dim strFila As String
        Dim strCampo As String = ""
        Dim intCodigoTercUsuario As Integer
        Dim strIdentificacionTercUsuario As String = ""


        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)
        'stwStreamWriter = New StreamWriter(stmStreamW)

        ' Consulta generada para crear el archivo plano de Documentos
        strSQL = "SELECT * " & Chr(13)
        strSQL += " FROM V_Interfaz_Factura_WORLD_OFFICE " & Chr(13)
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

        strSQL += " AND ISNULL(Interfaz_Contable_Factura,0) = 0"
        If intOficina > 0 Then
            strSQL += " AND OFIC_Factura = " & intOficina & Chr(13)
        End If

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

        Me.objGeneral.ConexionSQL.Open()

        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        Try
            strFila = String.Empty

            While sdrArchInte.Read

                strFila = sdrArchInte("NombreEmpresa").ToString.Trim & SEPARADOR_WORLD_OFFICE

                Date.TryParse(sdrArchInte("Fecha"), dteFechaAux)
                strFila += Format(dteFechaAux, clsGeneral.FORMATO_FECHA_DIA_MES_ANO).ToString.Trim & SEPARADOR_WORLD_OFFICE

                Date.TryParse(sdrArchInte("Fecha_Vence"), dteFechaVenceAux)
                strFila += Format(dteFechaVenceAux, clsGeneral.FORMATO_FECHA_DIA_MES_ANO).ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += INDICADOR_FACTURA_VENTA_FV & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Prefijo_Ciudad").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("ENFA_Numero_Documento").ToString.Trim & SEPARADOR_WORLD_OFFICE

                objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Usuarios", "TERC_Codigo_Empleado", "Codigo", clsGeneral.CAMPO_NUMERICO, intUsuario, intCodigoTercUsuario, , Me.strError, )
                objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Terceros", "Numero_Identificacion", "Codigo", clsGeneral.CAMPO_ALFANUMERICO, intCodigoTercUsuario, strIdentificacionTercUsuario, , Me.strError, )

                'Tercero Interno (Corresponde al número de identificación del usuario que genera el archivo)
                strCampo = strIdentificacionTercUsuario
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'strFila += SEPARADOR_WORLD_OFFICE ' Espacio en Blanco _ Elaborado Por

                strFila += sdrArchInte("Cuenta_Contable").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("VEHI_Codigo").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Valor_Debito").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Valor_Credito").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Concepto").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("IdentCliente").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Concepto").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("IdentCliente").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += clsGeneral.CERO.ToString & SEPARADOR_WORLD_OFFICE
                strFila += sdrArchInte("FormaPago").ToString.Trim & SEPARADOR_WORLD_OFFICE
                strFila += sdrArchInte("Anulado").ToString.Trim & SEPARADOR_WORLD_OFFICE

                stwStreamWriter.WriteLine(strFila)

            End While

            sdrArchInte.Close()
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()

            If Me.intEstado = ARCHIVO_DEFINITIVO Then
                Call Marcar_Registros_Interfaz_Definitivo_Factura_WORLD_OFFICE(intOficina)
            End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

            End Try
            Return False
        End Try

    End Function

    ' Generar Interfaz World Office Facturas Inventario
    Private Function Generar_Documento_Factura_Inventario_WORLD_OFFICE() As Boolean

        Dim ComandoSQL As SqlCommand
        Dim dteFechaAux As Date
        Dim dteFechaVenceAux As Date
        Dim stmStreamW As Stream
        Dim stwStreamWriter As StreamWriter
        Dim strFila As String
        Dim strCampo As String = ""
        Dim intCodigoTercUsuario As Integer
        Dim strIdentificacionTercUsuario As String = ""


        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)
        'stwStreamWriter = New StreamWriter(stmStreamW)

        ' Consulta generada para crear el archivo plano de Documentos
        strSQL = "SELECT * " & Chr(13)
        strSQL += " FROM V_Interfaz_Factura_Inventario_WORLD_OFFICE " & Chr(13)
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)

        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha < DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

        strSQL += " AND ISNULL(Interfaz_Contable_Factura,0) = 0"
        If Me.intEstado = ARCHIVO_DEFINITIVO Or Me.intEstado = ARCHIVO_BORRADOR Then
            strSQL += " AND Estado = " & Me.intEstado.ToString
        End If

        If intOficina > 0 Then
            strSQL += " AND OFIC_Factura = " & intOficina & Chr(13)
        End If

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

        Me.objGeneral.ConexionSQL.Open()

        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        Try
            strFila = String.Empty

            While sdrArchInte.Read

                strFila = sdrArchInte("NombreEmpresa").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += INDICADOR_FACTURA_VENTA_FV & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Prefijo_Ciudad").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("ENFA_Numero_Documento").ToString.Trim & SEPARADOR_WORLD_OFFICE

                Date.TryParse(sdrArchInte("Fecha"), dteFechaAux)
                strFila += Format(dteFechaAux, clsGeneral.FORMATO_FECHA_DIA_MES_ANO).ToString.Trim & SEPARADOR_WORLD_OFFICE

                objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Usuarios", "TERC_Codigo_Empleado", "Codigo", clsGeneral.CAMPO_NUMERICO, intUsuario, intCodigoTercUsuario, , Me.strError, )
                objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Terceros", "Numero_Identificacion", "Codigo", clsGeneral.CAMPO_ALFANUMERICO, intCodigoTercUsuario, strIdentificacionTercUsuario, , Me.strError, )

                'Tercero Interno (Corresponde al número de identificación del usuario que genera el archivo)
                strCampo = strIdentificacionTercUsuario
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("IdentCliente").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Nota").ToString.Trim & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("FormaPago").ToString.Trim & SEPARADOR_WORLD_OFFICE

                Date.TryParse(sdrArchInte("Fecha"), dteFechaAux)
                strFila += Format(dteFechaAux, clsGeneral.FORMATO_FECHA_DIA_MES_ANO).ToString.Trim & SEPARADOR_WORLD_OFFICE

                strCampo = ""
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                strCampo = ""
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                strCampo = "-1"
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                strFila += sdrArchInte("Anulado").ToString.Trim & SEPARADOR_WORLD_OFFICE

                ' Columnas Vacias desde O hasta AE
                For auxCount As Integer = 0 To 16 Step 1
                    strCampo = ""
                    strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE
                Next

                'Especificacion Oficina I
                'strFila += sdrArchInte("NombreOficina").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Columna Vacia AE
                'strCampo = " "
                'strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Movimiento Contable Transporte Terrestre
                'strFila += MOVIMIENTO_CONTABLE_TRANSPORTE_TERRESTRE.ToString & SEPARADOR_WORLD_OFFICE
                strFila += sdrArchInte("Codigo_Cuenta").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Especificacion Oficina II
                strFila += sdrArchInte("NombreOficina").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Unidad de Transporte
                strFila += sdrArchInte("UNME_Codigo").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Cantidad Remesas Factura - Otros Conceptos
                strFila += sdrArchInte("CantidadDetalle").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Impuesto
                strFila += sdrArchInte("Impuesto").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Valor Flete Cliente 
                strFila += sdrArchInte("Valor_Flete_Cliente_Facturar").ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Descuento
                strFila += sdrArchInte("Descuento").ToString.Trim & SEPARADOR_WORLD_OFFICE

                Date.TryParse(sdrArchInte("Fecha_Vence"), dteFechaVenceAux)
                strFila += Format(dteFechaVenceAux, clsGeneral.FORMATO_FECHA_DIA_MES_ANO).ToString.Trim & SEPARADOR_WORLD_OFFICE

                'Columna AN OBSERVACIONES FACTURA
                strCampo = sdrArchInte("Observaciones").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Placa Vehiculo 
                strFila += sdrArchInte("VEHI_Codigo").ToString.Trim & SEPARADOR_WORLD_OFFICE

                ' Columnas Vacias desde AP hasta BE
                For auxCount As Integer = 0 To 15 Step 1
                    strCampo = ""
                    strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE
                Next

                stwStreamWriter.WriteLine(strFila)

            End While

            sdrArchInte.Close()
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()

            'If Me.intEstado = ARCHIVO_DEFINITIVO Then
            '    Call Marcar_Registros_Interfaz_Definitivo_Factura_WORLD_OFFICE(intOficina)
            'End If

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

            End Try
            Return False
        End Try

    End Function

    ' Generar Interfaz World Office Comprobantes Egreso
    Private Function Generar_Interfaz_Comprobantes_WORLD_OFFICE() As Boolean

        Dim dteFecha As Date
        Dim strDiasPlazoFactura As String = ""
        Dim dteFechaVencimiento As Date = Date.MinValue
        Dim strPrefijoOficina As String = ""
        Dim intCodigoTercUsuario As Integer
        Dim strIdentificacionTercUsuario As String = ""


        Dim strNumeroDocumentoRemesa As String
        Dim arrCampos(2), arrValoresCampos(2) As String
        Dim strNumeroDocumentoFactura As String
        Dim strIdentificacionTerceroEncabezado As String = ""
        Dim strFormaPago As String = ""

        Dim strNombreConductor As String = ""
        Dim strNombreRuta As String = ""
        Dim strNumeroManifiestoGasto As String = ""
        Dim strPlaca As String = ""
        Dim strNombRuta As String = ""


        '----
        Dim ComandoSQL As SqlCommand
        Dim strObservaciones As String
        Dim strObservacionesEspecificas As String
        Dim strObservacionesEncabezado As String = ""
        Dim stmStreamW As Stream
        Dim stwStreamWriter As StreamWriter
        Dim strFila As String
        Dim strCampo As String

        Dim strIdentificacion As String = ""
        Dim strTipoNaturaleza As String = ""
        Dim strNombreTercero As String = ""

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)

        'Consulta generada para crear el archivo plano de Documentos
        strSQL = "SELECT * " & Chr(13)
        strSQL += " FROM V_Detalle_Encabezado_Comprobante_Contables " & Chr(13)
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)
        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha < DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "
        'strSQL += " AND ISNULL(Interfaz_Contable,0) = " & ARCHIVO_BORRADOR
        'strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO & Chr(13)
        strSQL += " AND TIDO_Documento_Cruce = " & TIDO_COMPROBANTE_EGRESO

        If intOficina > 0 Then
            strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
        End If
        strSQL += " ORDER BY Numero_Documento"

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

        Me.objGeneral.ConexionSQL.Open()

        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        strObservaciones = ""
        strObservacionesEspecificas = ""

        'Tercero Interno (Corresponde al número de identificación del usuario que genera el archivo)
        'El objetivo es tomar el número de identificación del funcionario con el qué esta asociado el usuario, sin embargo en estos momentos los
        'usuarios estan relacionados al tercero con código 1 ó 0, por tanto se hará uso del valor que existan en el campo Prefijo_Contable_Ingresos

        objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Usuarios", "TERC_Codigo_Empleado", "Codigo", clsGeneral.CAMPO_NUMERICO, intUsuario, intCodigoTercUsuario, , Me.strError, )
        objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Terceros", "Numero_Identificacion", "Codigo", clsGeneral.CAMPO_ALFANUMERICO, intCodigoTercUsuario, strIdentificacionTercUsuario, , Me.strError, )

        Try
            strFila = String.Empty
            While sdrArchInte.Read

                'Inicialización de variables
                dteFechaVencimiento = Date.MinValue
                strPrefijoOficina = ""
                strNumeroDocumentoRemesa = ""
                strNumeroDocumentoFactura = ""
                strDiasPlazoFactura = ""

                strObservaciones = sdrArchInte("Observaciones").ToString.Trim

                strPlaca = ""
                strPlaca = sdrArchInte("Placa").ToString
                '********* Comenzar a escribir los campos **************
                ' Nombre de la Empresa
                strFila = sdrArchInte("Nombre_Razon_Social").ToString & SEPARADOR_WORLD_OFFICE

                ' Tipo Documento 
                strCampo = sdrArchInte("Tipo_Documento").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Prefijo
                strCampo = sdrArchInte("Prefijo").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Numero Documento
                strCampo = sdrArchInte("Numero_Documento").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Fecha Creación
                Date.TryParse(sdrArchInte("Fecha_Documento"), dteFecha)
                'strCampo = Format(sdrArchInte("Fecha_Documento"), FORMATO_FECHA)
                strCampo = Format(sdrArchInte("Fecha_Documento"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE


                'TerceroInterno
                strCampo = strIdentificacionTercUsuario
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'TerceroExterno
                strCampo = sdrArchInte("EMPR_NIT").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Observaciones Comprobante Egreso
                strCampo = sdrArchInte("ObservacionesNota").ToString

                'If Len(strCampo) > 80 Then
                '    strCampo = Mid(strCampo, 1, 80)
                'End If

                strCampo = RTrim(strCampo)
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Verificado
                strCampo = sdrArchInte("Verificado").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Anulado
                strCampo = sdrArchInte("Anulado").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Sucursal
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Clasificación
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Caja Menor
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 1
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 2
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 3
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 4
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 5
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 6
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 7
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 8
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 9
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 10
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 11
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 12
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 13
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 14
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 15
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Cuenta PUC
                strCampo = sdrArchInte("Codigo_Cuenta").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Observaciones Comprobante Egreso
                strCampo = sdrArchInte("ObservacionesNota").ToString

                'If Len(strCampo) > 80 Then
                '    strCampo = Mid(strCampo, 1, 80)
                'End If

                strCampo = RTrim(strCampo)
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Tercero Externo
                strCampo = sdrArchInte("IdentificacionBenificiario").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Cheque
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Valor Débito
                strCampo = Val(sdrArchInte("Valor_Debito"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Valor Crédito
                strCampo = Val(sdrArchInte("Valor_Credito"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Fecha Vencimiento
                Date.TryParse(sdrArchInte("Fecha_Documento"), dteFechaVencimiento)
                'If TipoDocumento = clsTipoDocumento.TIDO_FACTURAS Then
                dteFechaVencimiento = dteFechaVencimiento.AddDays(clsGeneral.TREINTA_DIAS)
                strCampo = Format(dteFechaVencimiento)
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Centro Costos
                strCampo = strPlaca
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Activo Fijo
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Tipo_Base
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Porcentaje Rete-Fuente
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Base Retención
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Pago Retención
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'IVA al Costo
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Sucursal
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Importación
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Caja Menor
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Excluir NIIF
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'ImpoConsumo Costo
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'No deducible
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Codigo centro costos
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Abona_Cruce
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                stwStreamWriter.WriteLine(strFila)

            End While

            sdrArchInte.Close()
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

            End Try
            Return False
        End Try

    End Function

    ' Generar Interfaz World Office Legalizacion Gastos
    Private Function Generar_Interfaz_Legalizacion_Gastos_Conductor_WORLD_OFFICE() As Boolean

        Dim dteFecha As Date
        Dim strDiasPlazoFactura As String = ""
        Dim dteFechaVencimiento As Date = Date.MinValue
        Dim strPrefijoOficina As String = ""
        Dim intCodigoTercUsuario As Integer
        Dim strIdentificacionTercUsuario As String = ""

        Dim arrCampos(2), arrValoresCampos(2) As String
        Dim strNumeroDocumentoFactura As String
        Dim strIdentificacionTerceroEncabezado As String = ""
        Dim strFormaPago As String = ""

        Dim strNombreConductor As String = ""
        Dim strNombreRuta As String = ""
        Dim strNumeroManifiestoGasto As String = ""
        Dim strPlaca As String = ""
        Dim strNombRuta As String = ""


        '----
        Dim ComandoSQL As SqlCommand
        Dim strObservaciones As String
        Dim strObservacionesEspecificas As String
        Dim strObservacionesEncabezado As String = ""
        Dim stmStreamW As Stream
        Dim stwStreamWriter As StreamWriter
        Dim strFila As String
        Dim strCampo As String

        Dim strIdentificacion As String = ""
        Dim strTipoNaturaleza As String = ""
        Dim strNombreTercero As String = ""

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, System.Text.Encoding.Default)
        'stwStreamWriter = New StreamWriter(stmStreamW)

        'Consulta generada para crear el archivo plano de Documentos
        strSQL = "SELECT * " & Chr(13)
        strSQL += " FROM V_Interfaz_Legalizacion_WORLD_OFFICE " & Chr(13)
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)
        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha < DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

        'strSQL += " AND ISNULL(Interfaz_Contable,0) = " & ARCHIVO_BORRADOR

        'strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO & Chr(13)
        'strSQL += " AND TIDO_Documento_Cruce = " & TIDO_COMPROBANTE_EGRESO
        If Me.intEstado = ARCHIVO_BORRADOR Then
            Me.intEstado = ARCHIVO_BORRADOR_WORLD_OFFICE
        End If
        If Me.intEstado = ARCHIVO_DEFINITIVO Or Me.intEstado = ARCHIVO_BORRADOR_WORLD_OFFICE Then
            strSQL += " AND Estado = " & Me.intEstado.ToString
        End If

        If intOficina > 0 Then
            strSQL += " AND OFIC_Codigo = " & intOficina & Chr(13)
        End If

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

        Me.objGeneral.ConexionSQL.Open()

        Dim sdrArchInte As SqlDataReader = ComandoSQL.ExecuteReader

        strObservaciones = ""
        strObservacionesEspecificas = ""


        'Tercero Interno (Corresponde al número de identificación del usuario que genera el archivo)
        'El objetivo es tomar el número de identificación del funcionario con el qué esta asociado el usuario, sin embargo en estos momentos los
        'usuarios estan relacionados al tercero con código 1 ó 0, por tanto se hará uso del valor que existan en el campo Prefijo_Contable_Ingresos

        objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Usuarios", "TERC_Codigo_Empleado", "Codigo", clsGeneral.CAMPO_NUMERICO, intUsuario, intCodigoTercUsuario, , Me.strError, )
        objGeneral.Retorna_Campo_BD(Me.intEmpresa, "Terceros", "Numero_Identificacion", "Codigo", clsGeneral.CAMPO_ALFANUMERICO, intCodigoTercUsuario, strIdentificacionTercUsuario, , Me.strError, )


        'Campos adicionales para comprobantes de egresos
        Dim strNombreBancoComprobante As String = ""
        Dim strNombreFormaPagoComprobante As String = ""
        Dim strNumeroPagoComprobante As String = ""


        Try
            strFila = String.Empty

            While sdrArchInte.Read

                ' Nombre de la Empresa
                strFila = sdrArchInte("Nombre_Razon_Social").ToString & SEPARADOR_WORLD_OFFICE

                ' Tipo Documento 
                strCampo = INDICADOR_COMPROBANTE_EGRESOS_WOLD_OFFICE
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Prefijo Documento 
                strCampo = PREFIJO_LEGALIZACION_WORLD_OFFICE
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Numero Legalizacion 
                strCampo = Val(sdrArchInte("Numero_Documento"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Fecha Creación
                Date.TryParse(sdrArchInte("Fecha"), dteFecha)
                'strCampo = Format(sdrArchInte("Fecha_Documento"), FORMATO_FECHA)
                strCampo = Format(sdrArchInte("Fecha"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Tercero Interno (Corresponde al número de identificación del usuario que genera el archivo)
                strCampo = strIdentificacionTercUsuario
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Tercero_Externo
                strCampo = Val(sdrArchInte("Tercero_Legalizado"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Concepto Observacion
                strCampo = sdrArchInte("Concepto_Encabezado").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Verificado
                strFila = strFila & "-1" & SEPARADOR_WORLD_OFFICE

                'Anulado
                If Val(sdrArchInte("Estado").ToString) = clsGeneral.UNO Then
                    strCampo = clsGeneral.CERO
                Else
                    strCampo = clsGeneral.CERO
                End If
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE


                'Sucursal
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Clasificación
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Caja Menor
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 1
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 2
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 3
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 4
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 5
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 6
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 7
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 8
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 9
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 10
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 11
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 12
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 13
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 14
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Personalizado 15
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Numero CUENTA PUC 
                strCampo = Val(sdrArchInte("Codigo_Cuenta"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Concepto
                strCampo = sdrArchInte("Concepto").ToString
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Tercero_Externo
                strCampo = Val(sdrArchInte("Tercero_Externo"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Cheque
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Valor Débito
                strCampo = Val(sdrArchInte("Valor_Debito"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Valor Crédito
                strCampo = Val(sdrArchInte("Valor_Credito"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                ' Fecha Vencimiento
                Date.TryParse(sdrArchInte("Fecha"), dteFecha)
                'strCampo = Format(sdrArchInte("Fecha_Documento"), FORMATO_FECHA)
                strCampo = Format(sdrArchInte("Fecha"))
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE


                ' Placa
                strPlaca = ""
                strPlaca = sdrArchInte("Placa").ToString
                strFila = strFila & strPlaca & SEPARADOR_WORLD_OFFICE


                'Activo Fijo
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Tipo_Base
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Porcentaje Rete-Fuente
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Base Retención
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Pago Retención
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'IVA al Costo
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Sucursal
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Importación
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Caja Menor
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Excluir NIIF
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'ImpoConsumo Costo
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'No deducible
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Codigo centro costos
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE

                'Abona_Cruce
                strCampo = " "
                strFila = strFila & strCampo & SEPARADOR_WORLD_OFFICE


                stwStreamWriter.WriteLine(strFila)

            End While

            sdrArchInte.Close()
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()

            'If Me.intDefinitivo = ARCHIVO_DEFINITIVO Then
            '    Call Marcar_Registros_Interfaz_Definitivo("Encabezado_Comprobante_Contables", intDocumentoContable, intOficina)
            'End If

            Return True


        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

            End Try
            Return False
        End Try

    End Function

    Private Function Marcar_Registros_Interfaz_Definitivo_Factura_WORLD_OFFICE(ByVal intOficina As Integer) As Boolean
        Try

            Dim strSQL As String = ""

            strSQL = "UPDATE Encabezado_Facturas"
            strSQL += " SET Interfaz_Contable_Factura = " & ARCHIVO_DEFINITIVO
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString
            strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO
            'strSQL += " AND Fecha >= CONVERT(DATETIME, '" & objGeneral.Formatear_Fecha_SQL(Me.dtaFechaInicial) & "', 101)"
            'strSQL += " AND Fecha <= CONVERT(DATETIME, '" & objGeneral.Formatear_Fecha_SQL(Me.dtaFechaFinal) & "', 101)"
            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
            strSQL += " AND Interfaz_Contable_Factura = " & ARCHIVO_BORRADOR

            If intOficina <> 0 Then
                strSQL += " AND OFIC_Factura = " & intOficina & Chr(13)
            End If

            Marcar_Registros_Interfaz_Definitivo_Factura_WORLD_OFFICE = True
            Me.objGeneral.Ejecutar_SQL(strSQL)

        Catch ex As Exception
            Marcar_Registros_Interfaz_Definitivo_Factura_WORLD_OFFICE = False
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
            End Try
        End Try

    End Function
#End Region

#Region "Funciones INTERFAZ CONTAI"

    Private Function Generar_Documento_Comprobante_Egresos_CONTAI() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, Text.Encoding.Default)


        ' Consulta generada para crear el archivo plano de Comprobantes Egreso  CONTAI
        strSQL = "  SELECT * " & Chr(13)
        strSQL += " FROM V_Comprobantes_Egresos_CONTAI "
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString
        If Me.intOficina > -1 Then
            strSQL += "And Codigo_Oficina = " & Me.intOficina.ToString
        End If
        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

        Dim strDetalle As String = "TRANSPORTE DE MERCANCIA"
        Dim CentrCostos As String = " "
        Dim Trx As String = " "
        Dim Plaz As String = "30"
        Dim Documento As String
        Dim Referencia As String

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrArchEgre As SqlDataReader = ComandoSQL.ExecuteReader
        Try
            'Creacion de cabezera del documento
            strFila = Formatear_Campo_Alfanumerico_CONTAI("CodigoCuenta ", 20) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Compr", 8) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Fecha", 13) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Documento", 15) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Referencia", 16) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Nit", 18) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Detalle", 33) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("T", 5) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Valor", 28) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("ValorBase", 24) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("CentroCostos", 23) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Trx", 4) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Plaz", 12) & Chr(9)
            stwStreamWriter.WriteLine(strFila)
            While sdrArchEgre.Read
                strFila = ""
                'CodigoCuenta	
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Codigo_Cuenta"), 20) & Chr(9)
                'Compr
                strFila += Formatear_Campo_Alfanumerico_CONTAI(TIPO_COMPROBANTE_EGRESOS_CONTAI, 8) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Fecha"), 13) & Chr(9)

                Documento = sdrArchEgre("Codigo_Alterno_Oficina") & sdrArchEgre("Documento")
                Referencia = sdrArchEgre("Codigo_Alterno_Oficina") & sdrArchEgre("NumeroManifiesto")

                strFila += Formatear_Campo_Alfanumerico_CONTAI(Documento, 15) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Referencia, 16) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Numero_Identificacion"), 18) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("NombreCuenta"), 33) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Tipo"), 5) & Chr(9)
                'Condicion para insertar debito y credito 
                If sdrArchEgre("Tipo") = 1 Then
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Valor_Debito"), 28) & Chr(9)
                Else
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Valor_Credito"), 28) & Chr(9)
                End If
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Valor_Base"), 24) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(CentrCostos, 23) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Trx, 4) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Plaz, 12) & Chr(9)

                stwStreamWriter.WriteLine(strFila)
            End While

            sdrArchEgre.Close()
            sdrArchEgre.Dispose()
            stwStreamWriter.Close()
            stmStreamW.Close()

            Me.objGeneral.ConexionSQL.Close()
            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_CONTAI: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function
    Private Function Generar_Cuentas_x_Cobrar_CONTAI() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, Text.Encoding.Default)


        ' Consulta generada para crear el archivo plano de Comprobantes Egreso  CONTAI
        strSQL = "  SELECT * "
        strSQL += " FROM V_Comprobantes_Cuentas_Cobro_CONTAI "
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString
        If Me.intOficina > -1 Then
            strSQL += " AND Codigo_Oficina = " & Me.intOficina.ToString & Chr(13)
        End If
        strSQL += " AND TIDO_Codigo = 80 AND  Anulado= 0 AND  Estado = 1 " ' CODIGO  CXC
        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha <= DATEADD(DAY, 1, CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "
        strSQL += "order by Codigo"


        Dim Detalle As String = "CUENTA POR COBRAR"
        Dim CentrCostos As String = " "
        Dim Trx As String = " "
        Dim Plaz As String = "30"
        Dim CuentaPUC As String = ""
        Dim Documento As String
        Dim Referencia As String

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrArchCXC As SqlDataReader = ComandoSQL.ExecuteReader
        Try
            'Creacion de cabezera del documento
            strFila = Formatear_Campo_Alfanumerico_CONTAI("CodigoCuenta ", 20) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Compr", 8) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Fecha", 13) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Documento", 15) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Referencia", 16) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Nit", 18) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Detalle", 33) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("T", 5) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Valor", 28) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("ValorBase", 24) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("CentroCostos", 23) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Trx", 4) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Plaz", 12) & Chr(9)
            stwStreamWriter.WriteLine(strFila)
            While sdrArchCXC.Read
                strFila = ""
                'Dim num =
                If sdrArchCXC("CATA_DOOR_Codigo") = 2600 Then
                    If sdrArchCXC("Tipo") = 1 Then
                        CuentaPUC = CUENTA_PUC_DEBITO_GENERAL
                    Else
                        If sdrArchCXC("Codigo_Concepto") = 1 Then

                            CuentaPUC = CUENTA_PUC_CERDITO_DESCUENTO_FALTANTE
                        End If

                        If sdrArchCXC("Codigo_Concepto") = 2 Then
                            CuentaPUC = CUENTA_PUC_CERDITO_MAYOR_VALOR_CANCELADO
                        End If
                        If sdrArchCXC("Codigo_Concepto") = 3 Then
                            CuentaPUC = CUENTA_PUC_CERDITO_PRESTAMO_OTORGADO_TERCERO
                        End If
                        If sdrArchCXC("Codigo_Concepto") = 4 Then
                            CuentaPUC = CUENTA_PUC_CERDITO_RECUPERACION_GASTOS_GERENCIA
                        End If
                    End If

                Else
                    CuentaPUC = sdrArchCXC("Codigo_Cuenta")
                End If


                'CodigoCuenta	
                strFila += Formatear_Campo_Alfanumerico_CONTAI(CuentaPUC, 20) & Chr(9)
                CuentaPUC = ""
                'Compr
                strFila += Formatear_Campo_Alfanumerico_CONTAI(TIPO_COMPROBANTE_CUENTAS_X_COBRAR_CONTAI, 8) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchCXC("Fecha"), 13) & Chr(9)
                Documento = sdrArchCXC("Numero")
                Referencia = sdrArchCXC("Codigo")
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Documento, 15) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Referencia, 16) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchCXC("Numero_Identificacion"), 18) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Detalle, 33) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchCXC("Tipo"), 5) & Chr(9)
                'Condicion para insertar debito y credito 
                If sdrArchCXC("Tipo") = 1 Then
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchCXC("Valor_Debito"), 28) & Chr(9)
                Else
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchCXC("Valor_Credito"), 28) & Chr(9)
                End If
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchCXC("Valor_Base"), 24) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(CentrCostos, 23) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Trx, 4) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Plaz, 12) & Chr(9)
                stwStreamWriter.WriteLine(strFila)
            End While

            sdrArchCXC.Close()
            sdrArchCXC.Dispose()
            stwStreamWriter.Close()
            stmStreamW.Close()

            Me.objGeneral.ConexionSQL.Close()
            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_CONTAI: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function


    Private Function Generar_Documento_Comprobante_Ingreso_CONTAI() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, Text.Encoding.Default)


        ' Consulta generada para crear el archivo plano de Comprobantes Egreso  CONTAI
        strSQL = "  SELECT * " & Chr(13)
        strSQL += " FROM V_Comprobantes_Ingresos_CONTAI " & Chr(13)
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)
        'CODIGO COMPROBANTE INGRESOS
        strSQL += "And CATA_DOOR_Codigo = 2607 "
        If Me.intOficina > -1 Then
            strSQL += "And Codigo_Oficina = " & Me.intOficina.ToString & Chr(13)
        End If

        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

        Dim strDetalle As String = "TRANSPORTE DE MERCANCIA"
        Dim CentrCostos As String = " "
        Dim Trx As String = " "
        Dim Plaz As String = "30"
        Dim Documento As String
        Dim Referencia As String

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrArchEgre As SqlDataReader = ComandoSQL.ExecuteReader
        Try
            'Creacion de cabezera del documento
            strFila = Formatear_Campo_Alfanumerico_CONTAI("CodigoCuenta ", 20) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Compr", 8) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Fecha", 13) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Documento", 15) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Referencia", 16) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Nit", 18) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Detalle", 33) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("T", 5) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Valor", 28) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("ValorBase", 24) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("CentroCostos", 23) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Trx", 4) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Plaz", 12) & Chr(9)
            stwStreamWriter.WriteLine(strFila)
            While sdrArchEgre.Read
                strFila = ""
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Codigo_Cuenta"), 20) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(TIPO_COMPROBANTE_INGRESOS_CONTAI, 8) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Fecha"), 13) & Chr(9)
                'Documento
                Documento = sdrArchEgre("Codigo_Alterno_Oficina") & sdrArchEgre("Documento")
                'Referencia
                Referencia = sdrArchEgre("Codigo_Alterno_Oficina") & sdrArchEgre("NumeroManifiesto")
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Documento, 15) & Chr(9)
                'Referencia
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Referencia, 16) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Numero_Identificacion"), 18) & Chr(9)
                'NombreCuenta
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("NombreCuenta"), 33) & Chr(9)
                'Tipo
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Tipo"), 5) & Chr(9)
                'Condicion para insertar debito y credito 
                If sdrArchEgre("Tipo") = 1 Then
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Valor_Debito"), 28) & Chr(9)
                Else
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Valor_Credito"), 28) & Chr(9)
                End If
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Valor_Base"), 24) & Chr(9)
                'CentrCostos
                strFila += Formatear_Campo_Alfanumerico_CONTAI(CentrCostos, 23) & Chr(9)
                'Trx
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Trx, 4) & Chr(9)
                'Plaz
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Plaz, 12) & Chr(9)
                stwStreamWriter.WriteLine(strFila)
            End While

            sdrArchEgre.Close()
            sdrArchEgre.Dispose()
            stwStreamWriter.Close()
            stmStreamW.Close()

            Me.objGeneral.ConexionSQL.Close()
            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_CONTAI: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    Private Function Generar_Documento_Facturas_CONTAI() As Boolean

        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, Text.Encoding.Default)


        ' Consulta generada para crear el archivo plano de Comprobantes Egreso  CONTAI
        strSQL = "  SELECT * " & Chr(13)
        strSQL += " FROM V_Comprobantes_Contables_CONTAI " & Chr(13)
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)
        If Me.intOficina > -1 Then
            strSQL += "And Codigo_Oficina = " & Me.intOficina.ToString & Chr(13)
        End If
        strSQL += "And CATA_DOOR_Codigo = 2601" '----> CODIGO DOCUMENTO FACTURAS  

        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

        Dim Detalle As String = "TRANSPORTE DE MERCANCIA"
        Dim CentrCostos As String = " "
        Dim Trx As String = " "
        Dim Plaz As String = "30"
        Dim Referencia As String
        Dim Documento As String
        Dim strEmpresa = Me.intEmpresa.ToString
        Dim stroficinaCodigoAleteno As String
        Dim strCodigoPUC As String

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrArchFac As SqlDataReader = ComandoSQL.ExecuteReader
        Try
            'Creacion de cabezera del documentos
            strFila += Formatear_Campo_Alfanumerico_CONTAI("CodigoCuenta ", 20) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Compr", 8) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Fecha", 13) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Documento", 15) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Referencia", 16) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Nit", 18) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Detalle", 33) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("T", 5) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Valor", 28) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("ValorBase", 24) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("CentroCostos", 23) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Trx", 4) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Plaz", 12) & Chr(9)
            stwStreamWriter.WriteLine(strFila)
            While sdrArchFac.Read
                stroficinaCodigoAleteno = sdrArchFac("Codigo_Alterno_Oficina")
                strCodigoPUC = sdrArchFac("Cod_PUC")
                strFila = ""
                'CodigoCuenta	
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchFac("Codigo_Cuenta"), 20) & Chr(9)
                'Compr
                strFila += Formatear_Campo_Alfanumerico_CONTAI(TIPO_COMPROBANTE_FACTURA_CONTAI, 8) & Chr(9)
                'FECHA
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchFac("Fecha"), 13) & Chr(9)
                'Documento
                Documento = "CA" & sdrArchFac("Documento")
                'Si la cuenta empieza por 1305
                If sdrArchFac("Codigo_Cuenta").IndexOf("1305", 0, 4) >= 0 Then
                    Referencia = sdrArchFac("NumeroManifiesto")
                Else
                    Referencia = Documento
                End If

                'Documento	
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Documento, 15) & Chr(9)
                'Referencia
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Referencia, 16) & Chr(9)
                'NIT
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchFac("Numero_Identificacion"), 18) & Chr(9)
                'Detalle
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Detalle, 33) & Chr(9)
                'TIPO
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchFac("Tipo"), 5) & Chr(9)
                'VALOR 
                If sdrArchFac("Tipo") = 1 Then
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchFac("Valor_Debito"), 28) & Chr(9)
                Else
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchFac("Valor_Credito"), 28) & Chr(9)
                End If
                'VALOR BASE
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchFac("Valor_Base"), 24) & Chr(9)
                'CentroCostos	

                If sdrArchFac("Codigo_Cuenta") = CUENTA_PUC_VALORES_TERCEROS_FACTURA_CARGA Or sdrArchFac("Codigo_Cuenta") = CUENTA_PUC_INTERMEDIACION_TERCEROS_CARGA Or CUENTA_PUC_INTERMEDIACION_PROPIOS_CARGA Then
                    CentrCostos = Consultar_Equivalencia_Centro_Costo_Oficina(strEmpresa, stroficinaCodigoAleteno, strCodigoPUC)
                Else
                    CentrCostos = ""
                End If

                'CentrCostos
                strFila += Formatear_Campo_Alfanumerico_CONTAI(CentrCostos, 23) & Chr(9)
                'Trx
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Trx, 4) & Chr(9)
                'Plaz
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Plaz, 12) & Chr(9)
                stwStreamWriter.WriteLine(strFila)
            End While


            sdrArchFac.Close()
            sdrArchFac.Dispose()
            stwStreamWriter.Close()
            stmStreamW.Close()

            Me.objGeneral.ConexionSQL.Close()
            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_CONTAI: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    Private Function Generar_Documento_Anticipos_CONTAI() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, Text.Encoding.Default)


        ' Consulta generada para crear el archivo plano de Comprobantes Egreso  CONTAI
        strSQL = "  SELECT * " & Chr(13)
        strSQL += " FROM V_Comprobantes_Contables_CONTAI " & Chr(13)
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString & Chr(13)
        If Me.intOficina > -1 Then
            strSQL += "And Codigo_Oficina = " & Me.intOficina.ToString & Chr(13)
        End If
        'CODIGO ANTICIPOS 
        strSQL += "And CATA_DOOR_Codigo = 2604 "
        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha <= DATEADD(DAY,1,CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

        Dim Detalle = "TRANSPORTE DE MERCANCIA"
        Dim CentrCostos As String = " "
        Dim Trx As String = " "
        Dim Plaz As String = "30"
        Dim Documento As String
        Dim Referencia As String
        Dim strCodigoCuentaPUC As String
        Dim strCodigoOficinaAnticipo As String

        Dim strEmpresa = Me.intEmpresa.ToString
        Dim stroficinaCodigoAleteno As String
        Dim strCodigoPUC As String

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrArchEgre As SqlDataReader = ComandoSQL.ExecuteReader

        Try
            'Creacion de cabezera del documento
            strFila = Formatear_Campo_Alfanumerico_CONTAI("CodigoCuenta ", 20) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Compr", 8) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Fecha", 13) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Documento", 15) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Referencia", 16) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Nit", 18) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Detalle", 33) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("T", 5) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Valor", 28) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("ValorBase", 24) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("CentroCostos", 23) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Trx", 4) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Plaz", 12) & Chr(9)

            stwStreamWriter.WriteLine(strFila)
            While sdrArchEgre.Read
                strFila = ""
                'Codigo_Cuenta



                stroficinaCodigoAleteno = sdrArchEgre("Codigo_Alterno_Oficina")
                strCodigoPUC = sdrArchEgre("Cod_PUC")

                'pendiente cambiar el nombre del recorset
                If sdrArchEgre("Codigo_Cuenta") = CUENTA_PUC_ANTICIPO_CARGA Then

                    If sdrArchEgre("CATA_TIDV_Codigo") = 2101 Then
                        strCodigoOficinaAnticipo = Consultar_Equivalencia_Codigo_Contable_Oficina(strEmpresa, stroficinaCodigoAleteno, strCodigoPUC)
                        strCodigoCuentaPUC = sdrArchEgre("Codigo_Cuenta") & strCodigoOficinaAnticipo

                    ElseIf sdrArchEgre("CATA_TIDV_Codigo") = 2102 Then
                        If sdrArchEgre("Conductor_Afiliado") = 1 Then
                            strCodigoCuentaPUC = sdrArchEgre("Codigo_Cuenta") & "73"
                        End If
                        If sdrArchEgre("Conductor_Propio") = 1 Then
                            strCodigoCuentaPUC = sdrArchEgre("Codigo_Cuenta") & "70"
                        End If

                    End If


                ElseIf sdrArchEgre("Codigo_Cuenta") = CUENTA_PUC_RETEICA_CARGA Then

                    strCodigoOficinaAnticipo = Consultar_Equivalencia_Codigo_Contable_Oficina(strEmpresa, stroficinaCodigoAleteno, strCodigoPUC)
                    strCodigoCuentaPUC = sdrArchEgre("Codigo_Cuenta") & strCodigoOficinaAnticipo
                Else
                    strCodigoCuentaPUC = sdrArchEgre("Codigo_Cuenta")


                End If

                strFila += Formatear_Campo_Alfanumerico_CONTAI(strCodigoCuentaPUC, 20) & Chr(9)


                'Codigo Documento
                strFila += Formatear_Campo_Alfanumerico_CONTAI(TIPO_COMPROBANTE_ANTICIPOS_CONTAI, 8) & Chr(9)
                'Fecha
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Fecha"), 13) & Chr(9)

                Documento = sdrArchEgre("Codigo_Alterno_Oficina") & sdrArchEgre("Documento")
                Referencia = sdrArchEgre("Codigo_Alterno_Oficina") & sdrArchEgre("NumeroManifiesto")
                'Documento
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Documento, 15) & Chr(9)
                'Referencia
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Referencia, 16) & Chr(9)
                'Numero_Identificacion
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Numero_Identificacion"), 18) & Chr(9)
                'Detalle
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("NombreCuenta"), 33) & Chr(9)
                'Tipo
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Tipo"), 5) & Chr(9)
                'NATURALEZA_DEBITO CREDITO
                If sdrArchEgre("Tipo") = 1 Then
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Valor_Debito"), 28) & Chr(9)
                Else
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Valor_Credito"), 28) & Chr(9)
                End If
                'Valor_Base
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Valor_Base"), 24) & Chr(9)
                'CentrCostos
                strFila += Formatear_Campo_Alfanumerico_CONTAI(CentrCostos, 23) & Chr(9)
                'Trx
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Trx, 4) & Chr(9)
                'Plaz
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Plaz, 12) & Chr(9)

                stwStreamWriter.WriteLine(strFila)

            End While

            sdrArchEgre.Close()
            sdrArchEgre.Dispose()
            stwStreamWriter.Close()
            stmStreamW.Close()

            Me.objGeneral.ConexionSQL.Close()
            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_CONTAI: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    Private Function Generar_Documento_Liquidacion_Planillas_CONTAI() As Boolean
        Dim ComandoSQL As SqlCommand
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String
        Dim intSecuencia As Integer = 0, lonNumeComp As Long = 0, lonNumeCompAnte As Long = 0, lonNumeLiquPlan As Long = 0, dblValCVTLiq As Double, dblValorBase As Double

        ' Si el archivo existe se borra 
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        ' Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, Text.Encoding.Default)

        ' Consultar los comprobantes contables de Liquidación Despachos
        strSQL = "  SELECT * "
        strSQL += " FROM V_Comprobantes_Contables_Liquidacion_Planilla_CONTAI "
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString
        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
        strSQL += " AND CATA_DOOR_Codigo = 2602" ' Liquidaciones Despacho
        'strSQL += " AND Numero = 51608" ' TEMPORAL PRUEBAS NUMERO COMPROBANTE
        If Me.intOficina > -1 Then
            strSQL += " AND Codigo_Oficina = " & Me.intOficina.ToString
        End If

        Dim strDetalle As String = "TRANSPORTE DE MERCANCIA"
        Dim strCentroCostos As String = ""
        Dim strTrx As String = ""
        Dim strPlazo As String = "30"
        Dim strReferencia As String
        Dim strDocumento As String
        Dim strCodigoCuentaPUC As String
        Dim strCodigoContableOficina As String
        Dim strOficinaCodigoAlterno As String
        Dim strCodigoPUC As String

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrConsulta As SqlDataReader = ComandoSQL.ExecuteReader

        Try
            ' Creacion Cabezera Documento
            strFila = ""
            strFila += Formatear_Campo_Alfanumerico_CONTAI("CodigoCuenta ", 20) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Compr", 8) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Fecha", 13) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Documento", 15) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Referencia", 16) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Nit", 18) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Detalle", 33) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("T", 5) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Valor", 28) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("ValorBase", 24) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("CentroCostos", 23) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Trx", 4) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Plaz", 12) & Chr(9)
            stwStreamWriter.WriteLine(strFila)

            While sdrConsulta.Read
                strFila = ""

                lonNumeComp = sdrConsulta("Numero")
                lonNumeLiquPlan = sdrConsulta("Numero_Documento")

                If lonNumeComp <> lonNumeCompAnte Then
                    ' Si es un número de comprobante nuevo, se consulta si hay un valor CTV(Contrato Vinculación Temporal) para adicionar el movimiento correspondiente
                    dblValCVTLiq = Consultar_CVT_Liquidacion_Planilla(Me.intEmpresa, lonNumeLiquPlan)
                    If dblValCVTLiq > 0 Then
                        ' CodigoCuenta	
                        strCodigoCuentaPUC = CUENTA_PUC_CVT_CARGA
                        strFila += Formatear_Campo_Alfanumerico_CONTAI(strCodigoCuentaPUC, 20) & Chr(9)
                        ' Compr
                        strFila += Formatear_Campo_Alfanumerico_CONTAI(TIPO_COMPROBANTE_LIQUIDACIONES_CONTAI, 8) & Chr(9)
                        ' Fecha
                        strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrConsulta("Fecha"), 13) & Chr(9)
                        ' Documento	
                        strDocumento = sdrConsulta("Codigo_Alterno_Oficina") & sdrConsulta("Numero_Documento")
                        strFila += Formatear_Campo_Alfanumerico_CONTAI(strDocumento, 15) & Chr(9)
                        ' Referencia
                        strReferencia = sdrConsulta("Numero_Planilla")
                        strFila += Formatear_Campo_Alfanumerico_CONTAI(strReferencia, 16) & Chr(9)
                        ' Nit
                        strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrConsulta("Numero_Identificacion"), 18) & Chr(9)
                        ' Detalle
                        strDetalle = sdrConsulta("Nombre_Cuenta_PUC")
                        strFila += Formatear_Campo_Alfanumerico_CONTAI(strDetalle, 33) & Chr(9)
                        ' Tipo (Crédito)
                        strFila += Formatear_Campo_Alfanumerico_CONTAI("1", 5) & Chr(9)
                        ' Valor
                        strFila += Formatear_Campo_Alfanumerico_CONTAI(dblValCVTLiq, 28) & Chr(9)
                        ' Valor Base
                        dblValorBase = 0
                        strFila += Formatear_Campo_Alfanumerico_CONTAI(dblValorBase, 24) & Chr(9)
                        ' Centro Costos	
                        strCentroCostos = ""
                        strFila += Formatear_Campo_Alfanumerico_CONTAI(strCentroCostos, 23) & Chr(9)
                        ' Trx
                        strFila += Formatear_Campo_Alfanumerico_CONTAI(strTrx, 4) & Chr(9)
                        ' Plazo
                        strFila += Formatear_Campo_Alfanumerico_CONTAI(strPlazo, 12) & Chr(9)

                        stwStreamWriter.WriteLine(strFila)
                    End If
                End If

                ' CodigoCuenta	
                strFila = ""
                strOficinaCodigoAlterno = sdrConsulta("Codigo_Alterno_Oficina")
                strCodigoPUC = sdrConsulta("Codigo_PUC")

                If sdrConsulta("Codigo_Cuenta_PUC") = CUENTA_PUC_ANTICIPO_CARGA Or sdrConsulta("Codigo_Cuenta_PUC") = CUENTA_PUC_RETEICA_CARGA Then
                    strCodigoContableOficina = Consultar_Equivalencia_Codigo_Contable_Oficina(Me.intEmpresa, strOficinaCodigoAlterno, strCodigoPUC)
                    strCodigoCuentaPUC = sdrConsulta("Codigo_Cuenta_PUC") & strCodigoContableOficina
                Else
                    strCodigoCuentaPUC = sdrConsulta("Codigo_Cuenta_PUC")
                End If
                strFila += Formatear_Campo_Alfanumerico_CONTAI(strCodigoCuentaPUC, 20) & Chr(9)

                ' Compr
                strFila += Formatear_Campo_Alfanumerico_CONTAI(TIPO_COMPROBANTE_LIQUIDACIONES_CONTAI, 8) & Chr(9)
                ' Fecha
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrConsulta("Fecha"), 13) & Chr(9)
                ' Documento	
                strDocumento = sdrConsulta("Codigo_Alterno_Oficina") & sdrConsulta("Numero_Documento")
                strFila += Formatear_Campo_Alfanumerico_CONTAI(strDocumento, 15) & Chr(9)
                ' Referencia
                strReferencia = sdrConsulta("Numero_Planilla")
                strFila += Formatear_Campo_Alfanumerico_CONTAI(strReferencia, 16) & Chr(9)
                ' Nit
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrConsulta("Numero_Identificacion"), 18) & Chr(9)
                ' Detalle
                strDetalle = sdrConsulta("Nombre_Cuenta_PUC")
                strFila += Formatear_Campo_Alfanumerico_CONTAI(strDetalle, 33) & Chr(9)
                ' Tipo
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrConsulta("Tipo"), 5) & Chr(9)
                ' Valor
                If sdrConsulta("Tipo") = 1 Then
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrConsulta("Valor_Debito"), 28) & Chr(9)
                Else
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrConsulta("Valor_Credito"), 28) & Chr(9)
                End If
                ' Valor Base
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrConsulta("Valor_Base"), 24) & Chr(9)
                ' Centro Costo
                If sdrConsulta("Codigo_Cuenta_PUC") = CUENTA_PUC_VALORES_RECIBIDOS_TERCERO_CARGA Then
                    strCentroCostos = Consultar_Equivalencia_Centro_Costo_Oficina(Me.intEmpresa, strOficinaCodigoAlterno, strCodigoPUC)
                ElseIf sdrConsulta("Codigo_Cuenta_PUC") = CUENTA_PUC_FALTANTES_PROPIETARIO_CARGA Then
                    strCentroCostos = Consultar_Equivalencia_Centro_Costo_Oficina(Me.intEmpresa, strOficinaCodigoAlterno, strCodigoPUC)
                Else
                    strCentroCostos = ""
                End If
                strFila += Formatear_Campo_Alfanumerico_CONTAI(strCentroCostos, 23) & Chr(9)
                ' Trx
                strFila += Formatear_Campo_Alfanumerico_CONTAI(strTrx, 4) & Chr(9)
                ' Plazo
                strFila += Formatear_Campo_Alfanumerico_CONTAI(strPlazo, 12) & Chr(9)

                stwStreamWriter.WriteLine(strFila)

                lonNumeCompAnte = sdrConsulta("Numero")

            End While

            sdrConsulta.Close()
            sdrConsulta.Dispose()
            stwStreamWriter.Close()
            stmStreamW.Close()

            Me.objGeneral.ConexionSQL.Close()

            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Documento_Liquidacion_Planillas_CONTAI: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally
            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try

    End Function

    Private Function Generar_Documento_Legalizacion_Gastos_CONTAI() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String = ""

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, Text.Encoding.Default)


        ' Consulta generada para crear el archivo plano de Comprobantes Egreso  CONTAI
        strSQL = "  SELECT * "
        strSQL += " FROM V_Comprobantes_Contables_CONTAI "
        strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa.ToString
        If Me.intOficina > -1 Then
            strSQL += " AND Codigo_Oficina = " & Me.intOficina.ToString & Chr(13)
        End If
        strSQL += " AND CATA_DOOR_Codigo = 2608" ' CODIGO LEGALIZAICON  GASTOS CONDUCTOR
        strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
        strSQL += " AND Fecha <= DATEADD(DAY, 1, CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101)) "

        Dim Detalle As String = "TRANSPORTE DE MERCANCIA"
        Dim CentrCostos As String = " "
        Dim Trx As String = " "
        Dim Plaz As String = "30"
        Dim Referencia As String
        Dim Documento As String
        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrArchEgre As SqlDataReader = ComandoSQL.ExecuteReader
        Try
            'Creacion de cabezera del documento
            strFila += Formatear_Campo_Alfanumerico_CONTAI("CodigoCuenta ", 20)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Compr", 8)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Fecha", 13)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Documento", 15)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Referencia", 16)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Nit", 18)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Detalle", 33)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("T", 5)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Valor", 28)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("ValorBase", 24)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("CentroCostos", 23)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Trx", 4)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Plaz", 12)
            stwStreamWriter.WriteLine(strFila)
            While sdrArchEgre.Read
                strFila = ""
                'CodigoCuenta	
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Codigo_Cuenta"), 20)
                'Compr
                strFila += Formatear_Campo_Alfanumerico_CONTAI(TIPO_COMPROBANTE_LEGALIZACIONES_CONTAI, 8)
                'FECHA
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Fecha"), 13)

                Documento = Mid(sdrArchEgre("Placa"), 4) & sdrArchEgre("Documento")
                Referencia = sdrArchEgre("NumeroManifiesto")
                'Documento	 
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Documento, 15)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Referencia, 16)
                'Id  Conductor
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("ID_Conductor"), 18)
                'Detalle
                Detalle = sdrArchEgre("Ruta")
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Detalle, 33)
                'TIPO
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Tipo"), 5)
                'VALOR 
                If sdrArchEgre("Tipo") = 1 Then
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Valor_Debito"), 28)
                Else
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Valor_Credito"), 28)
                End If
                'VALOR BASE
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Valor_Base"), 24)
                'CentroCostos	

                Dim placa = sdrArchEgre("Placa")

                Dim numeroPlaca = placa.Substring(3, 3)
                Dim COdPlaca = placa.Substring(0, 3)
                CentrCostos = numeroPlaca + COdPlaca

                strFila += Formatear_Campo_Alfanumerico_CONTAI(CentrCostos, 23)
                'Trx
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Trx, 4)
                'Plaz
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Plaz, 12)
                stwStreamWriter.WriteLine(strFila)
            End While


            sdrArchEgre.Close()
            sdrArchEgre.Dispose()
            stwStreamWriter.Close()
            stmStreamW.Close()

            Me.objGeneral.ConexionSQL.Close()
            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_CONTAI: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function


    Private Function Generar_Terceros_CONTAI() As Boolean
        Dim ComandoSQL As SqlCommand, lonRes As Long
        Dim intSecuencia As Integer = 0, lonNumeroComprobante As Long = 0, strIdenTene As String = ""
        Dim stmStreamW As Stream, stwStreamWriter As StreamWriter, strFila As String

        'Si el archivo existe se borra primero para evitar incongruencias
        If File.Exists(Me.strPathArchivo) Then
            File.Delete(Me.strPathArchivo)
        End If

        'Se abre el archivo y si este no existe se crea
        stmStreamW = System.IO.File.OpenWrite(Me.strPathArchivo)
        stwStreamWriter = New StreamWriter(stmStreamW, Text.Encoding.Default)
        strSQL = "exec gps_obtener_Terceros " & Me.intEmpresa.ToString & Chr(13) & ",'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "'" & ",'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "'"

        Dim Vigencia As String = "S"
        Dim Tiene_RUT As String = "N"
        Dim NIT As String = "A"
        Dim CC As String = "C"
        Dim Codigo_país As String = 169
        Dim Razon_Soacial As String

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
        Me.objGeneral.ConexionSQL.Open()
        Dim sdrArchEgre As SqlDataReader = ComandoSQL.ExecuteReader

        Try
            'Creacion de cabezera del documento
            strFila += Formatear_Campo_Alfanumerico_CONTAI("NIT ", 11) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("D", 1) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("RazonSocial", 30) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Direccion", 30) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Ciudad", 15) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Tel ", 7) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("CDane", 5) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("V", 1) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("R", 1) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Pai", 3) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Nombre1", 30) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Nombre2", 30) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Apellido1", 30) & Chr(9)
            strFila += Formatear_Campo_Alfanumerico_CONTAI("Apellido2", 30) & Chr(9)
            stwStreamWriter.WriteLine(strFila)
            While sdrArchEgre.Read
                strFila = ""
                'IDENTIFICACION	
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Numero_Identificacion"), 11) & Chr(9)

                'TIPO ID
                If sdrArchEgre("CATA_TIID_Codigo") = 102 Then
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(NIT, 1) & Chr(9)
                Else
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(CC, 1) & Chr(9)
                End If
                'Razon Social 
                If sdrArchEgre("Razon_Social").length > 1 Then
                    strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Razon_Social"), 30) & Chr(9)
                Else
                    Razon_Soacial = sdrArchEgre("NOMBRE1") & sdrArchEgre("NOMBRE2") & sdrArchEgre("Apellido1") & sdrArchEgre("Apellido2")

                    strFila += Formatear_Campo_Alfanumerico_CONTAI(Razon_Soacial, 30) & Chr(9)
                End If
                'Direccion
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Direccion"), 30) & Chr(9)
                'Ciudad
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Ciudad"), 15) & Chr(9)
                'telefonos
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("telefonos"), 7) & Chr(9)
                'Dane Municipio 
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Codigo_dane"), 5) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Vigencia, 1) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Tiene_RUT, 1) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(Codigo_país, 3) & Chr(9)

                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("NOMBRE1"), 30) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("NOMBRE2"), 30) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Apellido1"), 30) & Chr(9)
                strFila += Formatear_Campo_Alfanumerico_CONTAI(sdrArchEgre("Apellido2"), 30) & Chr(9)
                stwStreamWriter.WriteLine(strFila)
            End While
            sdrArchEgre.Close()
            sdrArchEgre.Dispose()
            stwStreamWriter.Close()
            stmStreamW.Close()

            Me.objGeneral.ConexionSQL.Close()
            Return True

        Catch ex As Exception
            stwStreamWriter.Close()
            stmStreamW.Close()
            Me.objGeneral.ConexionSQL.Close()
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Generar_Interfaz_Comprobantes_Egreso_CONTAI: " & Me.objGeneral.Traducir_Error(ex.Message)
            Return False
        Finally

            If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
                Me.objGeneral.ConexionSQL.Close()
            End If
        End Try
    End Function

    Private Function Consultar_Equivalencia_Codigo_Contable_Oficina(ByVal intCodiEmpr As Integer, ByVal strCodigoAlternoOficina As String, ByVal strCodigoCuentaPUC As String) As String
        Dim ComandoSQL As SqlCommand

        strSQL = "SELECT * FROM Equivalencia_Cuentas_Contables_Codigos_Contables_Oficinas "
        strSQL += " WHERE EMPR_Codigo = " & intCodiEmpr.ToString()
        strSQL += " AND OFIC_Codigo_Alterno = '" & strCodigoAlternoOficina & "'"
        strSQL += " AND PLUC_Codigo = " & strCodigoCuentaPUC

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

        Dim sdrCodContableOficina As SqlDataReader = ComandoSQL.ExecuteReader
        Dim strCodigoContableOficina As String = ""

        Try
            While sdrCodContableOficina.Read
                strCodigoContableOficina = sdrCodContableOficina("Codigo_Contable_Oficina")
            End While

        Finally
            sdrCodContableOficina.Close()
            sdrCodContableOficina.Dispose()
        End Try

        Return strCodigoContableOficina

    End Function

    Private Function Consultar_Equivalencia_Centro_Costo_Oficina(ByVal intCodiEmpr As Integer, ByVal stroficinaCodigoAleteno As String, ByVal strCodigoCuentaPUC As String) As String
        Dim ComandoSQL As SqlCommand

        strSQL = "  SELECT * FROM Equivalencia_Centro_Costos_Oficinas "
        strSQL += " WHERE EMPR_Codigo = " & intCodiEmpr.ToString()
        strSQL += " AND  OFIC_Codigo_Alterno = '" & stroficinaCodigoAleteno & "'"
        strSQL += " AND PLUC_Codigo =" & strCodigoCuentaPUC

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

        Dim sdrConsulta As SqlDataReader = ComandoSQL.ExecuteReader
        Dim strCodCenCos As String = ""

        Try
            While sdrConsulta.Read
                strCodCenCos = sdrConsulta("Codigo_Centro_Costos")
            End While

        Finally
            sdrConsulta.Close()
            sdrConsulta.Dispose()
        End Try

        Return strCodCenCos

    End Function

    Private Function Consultar_CVT_Liquidacion_Planilla(ByVal intCodiEmpr As Integer, ByVal lonNumeLiqui As Long) As Double
        Dim ComandoSQL As SqlCommand

        strSQL = "SELECT ABS(Valor_CVT) AS Valor_CVT FROM Encabezado_Liquidacion_Planilla_Despachos "
        strSQL += " WHERE EMPR_Codigo = " & intCodiEmpr.ToString()
        strSQL += " AND TIDO_Codigo = 160 " ' Liquidacion Planilla Masivo
        strSQL += " AND Numero_Documento = " & lonNumeLiqui.ToString()

        ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

        Dim sdrConsulta As SqlDataReader = ComandoSQL.ExecuteReader
        Dim dblValorCVT As Double = 0

        Try
            While sdrConsulta.Read
                dblValorCVT = sdrConsulta("Valor_CVT")
            End While

        Finally
            sdrConsulta.Close()
            sdrConsulta.Dispose()
        End Try

        Return dblValorCVT

    End Function
#End Region

#Region "Funciones Generales"

    Private Function Marcar_Registros_Interfaz_Definitivo(ByVal Tabla As String, ByVal TipoDocumento As Integer, ByVal Oficina As Integer) As Boolean
        Try

            Dim strSQL As String = ""

            strSQL = "UPDATE " & Tabla
            strSQL += " SET Interfaz_Contable = " & ARCHIVO_DEFINITIVO
            strSQL += " WHERE EMPR_Codigo = " & Me.intEmpresa
            strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO
            'strSQL += " AND DATEDIFF(d,Fecha,CONVERT(DATETIME, '" & objGeneral.Formatear_Fecha_SQL(Me.dteFechaInicial) & "', 101)) <=0" & Chr(13)
            'strSQL += " AND DATEDIFF(d,Fecha,CONVERT(DATETIME, '" & objGeneral.Formatear_Fecha_SQL(Me.dteFechaFinal) & "', 101)) >=0" & Chr(13)
            strSQL += " AND Fecha >= CONVERT(DATE,'" & dtaFechaInicial.Month & "/" & dtaFechaInicial.Day & "/" & dtaFechaInicial.Year & "',101) "
            strSQL += " AND Fecha <= CONVERT(DATE,'" & dtaFechaFinal.Month & "/" & dtaFechaFinal.Day & "/" & dtaFechaFinal.Year & "',101) "
            strSQL += " AND TIDO_Codigo = " & intDocumentoContable
            strSQL += " AND TIDO_Documento = " & TipoDocumento
            strSQL += " AND Interfaz_Contable = " & ARCHIVO_BORRADOR

            If Oficina <> intOficina Then
                strSQL += " AND OFIC_Codigo = " & Oficina & Chr(13)
            End If

            Marcar_Registros_Interfaz_Definitivo = True
            Me.objGeneral.Ejecutar_SQL(strSQL)

        Catch ex As Exception
            Marcar_Registros_Interfaz_Definitivo = False
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name
            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
            End Try
        End Try

    End Function

    Private Sub Armar_Nombre_Archivo(ByRef strNombreArchivo As String, ByRef strPathArchivo As String)
        Try
            strPathArchivo = System.AppDomain.CurrentDomain.BaseDirectory() & DIRECTORIO_ARCHIVOS_PLANOS

            Select Case intDocumentoContable

                Case INTERFAZ_TERCEROS
                    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_TERCEROS_SIIGO

                Case INTERFAZ_LIQUIDACION_DESPACHOS
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_SIIGO
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_LIQUIDACIONES_SIIGO
                        Case SISTEMA_CONTABLE_UNO_EE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_LIQUIDACIONES_UNO_EE
                        Case SISTEMA_CONTABLE_SIIGO_WINDOWS
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_LIQUIDACIONES_SIIGO
                        Case SISTEMA_CONTABLE_CONTAI
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_LIQUIDACIONES_CONTAI


                    End Select

                Case INTERFAZ_FACTURAS
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_SIIGO
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_FACTURAS_SIIGO
                        Case SISTEMA_CONTABLE_WORLD_OFFICE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_FACTURAS_WORLD_OFFICE
                        Case SISTEMA_CONTABLE_UNO_EE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_FACTURAS_UNO_EE
                        Case SISTEMA_CONTABLE_SIIGO_WINDOWS
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_FACTURAS_SIIGO
                        Case SISTEMA_CONTABLE_CONTAI
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_FACTURAS_SIIGO
                    End Select

                Case INTERFAZ_FACTURAS_INVENTARIO
                    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_FACTURAS_INVENTARIO_WORLD_OFFICE

                Case INTERFAZ_COMPROBANTES_EGRESO
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_SIIGO
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_COMPROBANTESEGRESO
                        Case SISTEMA_CONTABLE_WORLD_OFFICE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_COMPROBANTESEGRESO_WORLD_OFFICE
                        Case SISTEMA_CONTABLE_UNO_EE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_COMPROBANTES_EGRESO_UNO_EE
                        Case SISTEMA_CONTABLE_SIIGO_WINDOWS
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_COMPROBANTESEGRESO
                        Case SISTEMA_CONTABLE_CONTAI
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_COMPROBANTESEGRESO
                    End Select


                Case INTERFAZ_COMPROBANTES_INGRESO
                    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_COMPROBANTINSEGRESO

                Case INTERFAZ_LEGALIZACION_GASTOS
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_SIIGO
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_LEGALIZACION_GASTOS
                        Case SISTEMA_CONTABLE_WORLD_OFFICE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_LEGALIZACION_GASTOS
                        Case SISTEMA_CONTABLE_UNO_EE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_LEGALIZACION_GASTOS
                        Case SISTEMA_CONTABLE_SIIGO_WINDOWS
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_LEGALIZACION_GASTOS
                        Case SISTEMA_CONTABLE_CONTAI
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_LEGALIZACION_CONTAI
                    End Select



                Case INTERFAZ_ANTICIPOS
                    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_ANTICIPOS_CONTAI

                Case INTERFAZ_CUENTAS_X_COBRAR
                    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_CXC_CONTAI

            End Select


            strNombreArchivo = strNombreArchivo.Replace("/", "")

            strPathArchivo += strNombreArchivo

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Armar_Nombre_Archivo:" & ex.Message.ToString
        End Try


    End Sub

    Private Sub Armar_Nombre_Archivo_Uno_EE(ByRef strNombreArchivo As String, ByRef strPathArchivo As String, ByVal intDocumento As Integer)
        Try
            strPathArchivo = System.AppDomain.CurrentDomain.BaseDirectory() & DIRECTORIO_ARCHIVOS_PLANOS

            Select Case intDocumentoContable

                Case INTERFAZ_LIQUIDACION_DESPACHOS
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_UNO_EE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_LIQUIDACIONES_UNO_EE
                    End Select

                Case INTERFAZ_FACTURAS
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_UNO_EE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_FACTURAS_UNO_EE
                    End Select

                Case INTERFAZ_COMPROBANTES_EGRESO
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_UNO_EE
                            Select Case intDocumento
                                Case 1
                                    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_COMPROBANTES_EGRESO_UNO_EE
                                Case 2
                                    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_CUENTA_PAGAR_UNO_EE
                            End Select

                    End Select

                Case INTERFAZ_NOTA_CREDITO_FACTURAS
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_UNO_EE
                            Select Case intDocumento
                                Case 1
                                    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_NOTA_CREDITO_MOVIMIENTO
                                    'Case 2
                                    '    strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_NOTA_CREDITO_DOCUMENTO
                            End Select

                    End Select

                Case INTERFAZ_ANTICIPOS
                    Select Case intSitemaContable
                        Case SISTEMA_CONTABLE_UNO_EE
                            strNombreArchivo = dtaFechaInicial & dtaFechaFinal & NOMBRE_ARCHIVO_ANTICIPOS_UNO_EE
                    End Select

            End Select


            strNombreArchivo = strNombreArchivo.Replace("/", "")

            strPathArchivo += strNombreArchivo

        Catch ex As Exception
            lblMensajeError.Visible = True
            lblMensajeError.Text = "En la página " & Me.ToString & " se presento un error en la función Armar_Nombre_Archivo:" & ex.Message.ToString
        End Try


    End Sub

    Private Function Formatear_Campo_Alfanumerico_CONTAI(ByVal strCampOrig As String, ByVal intLongCampContai As Integer) As String
        Try
            Dim intLongCampOrig As Integer, strCampForm As String

            intLongCampOrig = Len(strCampOrig)
            strCampForm = Space(intLongCampContai - intLongCampOrig) & strCampOrig


            Return strCampForm

        Catch ex As Exception

        End Try

    End Function




#End Region

#Region "Funciones Utilidades"

    Private Function Formatear_Campo(ByVal strValor As String, ByVal intLongitud As Integer, ByVal intTipoCampo As Integer, Optional ByVal intPresicion As Integer = 0) As String


        On Error Resume Next
        Dim intCon%, strFormato$, intPos%, strValorEntero$, strValorDecimal$, intLon%, strSignoNegativo$, intLongSignoNegativo%
        Dim strCampo$ = ""

        strFormato = ""
        Formatear_Campo = ""
        strSignoNegativo$ = ""
        strValorDecimal = 0

        strValor = strValor.Replace(Chr(10).ToString, "")
        strValor = strValor.Replace(CARACTER_ENTER, "")

        strValor = Trim$(strValor)

        If intTipoCampo = CAMPO_CEROS_IZQUIERDA Then
            strValor = Trim$(strValor)
            intLon = Len(strValor)

            For intCon = intLon To intLongitud - 1
                strValor = "0" & strValor
            Next
            Formatear_Campo = strValor

        ElseIf intTipoCampo = CAMPO_CEROS_DERECHA Then
            strValor = Trim$(strValor)
            intLon = Len(strValor)

            For intCon = intLon + 1 To intLongitud
                strValor = strValor & "0"
            Next
            Formatear_Campo = strValor

        ElseIf intTipoCampo = CAMPO_CARACTER_CEROS Then
            ' Si es vacio
            If strValor = "" Then
                For intCon = 1 To intLongitud
                    strValor = strValor & "0"
                Next
            End If
            Formatear_Campo = strValor

        ElseIf intTipoCampo = CAMPO_NUMERICO Then

            ' Si es vacio
            If strValor = "" Then
                For intCon = 1 To intLongitud
                    strFormato = strFormato & "0"
                Next
                Formatear_Campo = strFormato

            Else
                ' Buscar signo negativo
                intPos = InStr(strValor, "-")
                If intPos > 0 Then
                    strSignoNegativo = "-"
                    intLongSignoNegativo = 1
                    strValor = Mid$(strValor, 2)
                End If

                For intCon = 1 To intLongitud - intLongSignoNegativo
                    strFormato = strFormato & "0"
                Next

                Formatear_Campo = strSignoNegativo & Format$(strValor, strFormato)

            End If

        ElseIf intTipoCampo = CAMPO_DECIMAL_CON_PUNTO_UNOE Then

            ' Si es vacio
            If strValor = "" Then
                For intCon = 1 To intLongitud
                    strFormato = strFormato & "0"
                Next
                Formatear_Campo = strFormato

            Else
                ' Buscar signo negativo
                intPos = InStr(strValor, "-")
                If intPos > 0 Then
                    strSignoNegativo = "-"
                    intLongSignoNegativo = 1
                    strValor = Mid$(strValor, 2)
                End If

                ' Buscar signo decimal
                intPos = InStr(strValor, ".")
                If intPos > 0 Then
                    strValorEntero = Mid$(strValor, 1, intPos - 1)
                    strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                Else
                    strValorEntero = strValor
                    strValorDecimal = "0000"
                End If
                strValorEntero = Trim$(strValorEntero)
                strValorDecimal = Trim$(strValorDecimal)

                ' Formatear Entero
                For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                    strFormato = strFormato & "0"
                Next
                If strValorEntero = "" Then
                    strValorEntero = strFormato
                Else
                    strValorEntero = Format(Val(strValorEntero), strFormato)
                End If

                ' Formatear Decimal
                strFormato = ""
                intLon = Len(strValorDecimal)
                If intLon < intPresicion Then
                    ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                    For intCon = intLon To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                ElseIf intLon = 0 Then
                    ' Si la longitud es 0 se llena el valor decimal con ceros
                    For intCon = 0 To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                End If

                Formatear_Campo = "+" & strSignoNegativo & strValorEntero & "." & strValorDecimal

            End If

        ElseIf intTipoCampo = CAMPO_NUMERICO_UNOE_85 Then
            ' Buscar signo negativo
            intPos = InStr(strValor, "-")
            If intPos > 0 Then
                strSignoNegativo = "-"
                intLongSignoNegativo = 1
                strValor = Mid$(strValor, 2)
            End If

            ' Buscar signo decimal
            intPos = InStr(strValor, ".")
            If intPos > 0 Then
                strValorEntero = Mid$(strValor, 1, intPos - 1)
                strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
            Else
                strValorEntero = strValor
                If strValorEntero = "0" Then
                    strValorDecimal = ""
                Else
                    strValorDecimal = "0000"
                End If
            End If
            strValorEntero = Trim$(strValorEntero)
            strValorDecimal = Trim$(strValorDecimal)

            ' Formatear Entero
            For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                strFormato = strFormato & "0"
            Next
            If strValorEntero = "" Then
                strValorEntero = strFormato
            Else
                strValorEntero = Format(Val(strValorEntero), strFormato)
            End If

            ' Formatear Decimal
            strFormato = ""
            intLon = Len(strValorDecimal)
            If intLon < intPresicion Then
                ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                For intCon = intLon To intPresicion - 1
                    strValorDecimal = strValorDecimal & "0"
                Next

            ElseIf intLon = 0 Then
                ' Si la longitud es 0 se llena el valor decimal con ceros
                For intCon = 0 To intPresicion - 1
                    strValorDecimal = strValorDecimal & "0"
                Next

            End If

            Formatear_Campo = strValorEntero & "+"

        ElseIf intTipoCampo = CAMPO_DECIMAL_UNOE_85 Then
            ' Buscar signo negativo
            intPos = InStr(strValor, "-")
            If intPos > 0 Then
                strSignoNegativo = "-"
                intLongSignoNegativo = 1
                strValor = Mid$(strValor, 2)
            End If

            ' Buscar signo decimal
            intPos = InStr(strValor, ".")
            If intPos > 0 Then
                strValorEntero = Mid$(strValor, 1, intPos - 1)
                strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
            Else
                strValorEntero = strValor
                If strValorEntero = "0" Then
                    strValorDecimal = ""
                Else
                    strValorDecimal = "0000"
                End If
            End If
            strValorEntero = Trim$(strValorEntero)
            strValorDecimal = Trim$(strValorDecimal)

            ' Formatear Entero
            For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                strFormato = strFormato & "0"
            Next
            If strValorEntero = "" Then
                strValorEntero = strFormato
            Else
                strValorEntero = Format(Val(strValorEntero), strFormato)
            End If

            ' Formatear Decimal
            strFormato = ""
            intLon = Len(strValorDecimal)
            If intLon < intPresicion Then
                ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                For intCon = intLon To intPresicion - 1
                    strValorDecimal = strValorDecimal & "0"
                Next

            ElseIf intLon = 0 Then
                ' Si la longitud es 0 se llena el valor decimal con ceros
                For intCon = 0 To intPresicion - 1
                    strValorDecimal = strValorDecimal & "0"
                Next

            End If

            Formatear_Campo = strValorEntero & "00+"


        ElseIf intTipoCampo = CAMPO_CARACTER Then ' Blancos a la derecha

            For intCon = 1 To intLongitud
                If Mid$(strValor, intCon, 1) <> Chr(13) Then
                    strCampo = strCampo & Mid$(strValor, intCon, 1)
                End If
            Next

            If Len(strCampo) > intLongitud Then
                strValor = Mid$(strCampo, 1, intLongitud)
            Else
                strValor = strCampo
            End If

            Formatear_Campo = strValor & Space$(intLongitud - Len(strValor))

        ElseIf intTipoCampo = CAMPO_DECIMAL Then ' Ceros a la izquierda, con el número de decimales, sin punto ni comas

            ' Si es vacio
            If strValor = "" Then
                For intCon = 1 To intLongitud
                    strFormato = strFormato & "0"
                Next
                Formatear_Campo = strFormato

            Else

                ' Buscar signo negativo
                intPos = InStr(strValor, "-")
                If intPos > 0 Then
                    strSignoNegativo = "-"
                    intLongSignoNegativo = 1
                    strValor = Mid$(strValor, 2)
                End If

                ' Buscar signo decimal
                intPos = InStr(strValor, ".")
                If intPos > 0 Then
                    strValorEntero = Mid$(strValor, 1, intPos - 1)
                    strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                Else
                    strValorEntero = strValor
                    strValorDecimal = "00"
                End If
                strValorEntero = Trim$(strValorEntero)
                strValorDecimal = Trim$(strValorDecimal)

                ' Formatear Entero
                For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                    strFormato = strFormato & "0"
                Next
                If strValorEntero = "" Then
                    strValorEntero = strFormato
                Else
                    strValorEntero = Format(Val(strValorEntero), strFormato)
                End If

                ' Formatear Decimal
                strFormato = ""
                intLon = Len(strValorDecimal)
                If intLon < intPresicion Then
                    ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                    For intCon = intLon To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                ElseIf intLon = 0 Then
                    ' Si la longitud es 0 se llena el valor decimal con ceros
                    For intCon = 0 To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                End If

                Formatear_Campo = strSignoNegativo & strValorEntero & strValorDecimal

            End If

        ElseIf intTipoCampo = CAMPO_FECHA Then
            Formatear_Campo = Format$(strValor, "DD/MM/YYYY")

        ElseIf intTipoCampo = CAMPO_IDENTIFICACION Then ' Espacios a la Derecha. (1-2) dependiendo del digito de chequeo
            Dim intCerosAgregar As Short
            intCerosAgregar = intLongitud - Len(strValor)
            For intCon = 1 To intCerosAgregar
                Formatear_Campo += " "
            Next
            Formatear_Campo += strValor

        ElseIf intTipoCampo = CAMPO_ESPACIOS_DERECHA Then
            Dim intEspacioAgregar As Short
            Dim strEspacios As String = ""
            intEspacioAgregar = intLongitud - Len(strValor)
            For intCon = 1 To intEspacioAgregar
                strEspacios += " "
            Next
            Formatear_Campo = strValor & strEspacios



        ElseIf intTipoCampo = CAMPO_ESPACIOS_IZQUIERDA Then
            Dim intEspacioAgregar As Short
            intEspacioAgregar = intLongitud - Len(strValor)
            For intCon = 1 To intEspacioAgregar
                Formatear_Campo += " "
            Next
            Formatear_Campo += strValor

        ElseIf intTipoCampo = CAMPO_DECIMAL_CON_PUNTO Then

            ' Si es vacio
            If strValor = "" Then
                For intCon = 1 To intLongitud
                    strFormato = strFormato & "0"
                Next
                Formatear_Campo = strFormato

            Else
                ' Buscar signo negativo
                intPos = InStr(strValor, "-")
                If intPos > 0 Then
                    strSignoNegativo = "-"
                    intLongSignoNegativo = 1
                    strValor = Mid$(strValor, 2)
                End If

                ' Buscar signo decimal
                intPos = InStr(strValor, ".")
                If intPos > 0 Then
                    strValorEntero = Mid$(strValor, 1, intPos - 1)
                    strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                Else
                    strValorEntero = strValor
                    strValorDecimal = Trim$(strValorDecimal)
                End If
                strValorEntero = Trim$(strValorEntero)
                strValorDecimal = Trim$(strValorDecimal)

                ' Formatear Entero
                For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                    strFormato = strFormato & "0"
                Next
                If strValorEntero = "" Then
                    strValorEntero = strFormato
                Else
                    strValorEntero = Format(Val(strValorEntero), strFormato)
                End If

                ' Formatear Decimal
                strFormato = ""
                intLon = Len(strValorDecimal)
                If intLon < intPresicion Then
                    ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                    For intCon = intLon To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                ElseIf intLon = 0 Then
                    ' Si la longitud es 0 se llena el valor decimal con ceros
                    For intCon = 0 To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                End If

                Formatear_Campo = strSignoNegativo & strValorEntero & "." & strValorDecimal
                Formatear_Campo = Trim(Formatear_Campo)

            End If

        ElseIf intTipoCampo = SOLO_CAMPO_DECIMAL_CON_PUNTO Then ' Formatea el campo con decimales con punto y con espacios a la izquierda

            ' Si es vacio
            If strValor = "" Then
                For intCon = 1 To intLongitud
                    strFormato = strFormato & " "
                Next
                Formatear_Campo = strFormato

            Else
                ' Buscar signo negativo
                intPos = InStr(strValor, "-")
                If intPos > 0 Then
                    strSignoNegativo = "-"
                    intLongSignoNegativo = 1
                    strValor = Mid$(strValor, 2)
                End If

                ' Buscar signo decimal
                intPos = InStr(strValor, ".")
                If intPos > 0 Then
                    strValorEntero = Mid$(strValor, 1, intPos - 1)
                    strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                Else
                    strValorEntero = strValor
                    strValorDecimal = Trim$(strValorDecimal)
                End If
                strValorEntero = Trim$(strValorEntero)
                strValorDecimal = Trim$(strValorDecimal)

                ' Formatear Entero
                Dim intEspacioAgregar As Short
                intEspacioAgregar = intLongitud - Len(strValorEntero)
                For intCon = 1 To intEspacioAgregar - intPresicion - intLongSignoNegativo - 1 '-1 POR EL PUNTO
                    strFormato = strFormato & " "
                Next
                If strValorEntero = "" Then
                    strValorEntero = strFormato
                Else
                    strValorEntero = strFormato & strValorEntero
                    ' strValorEntero = Format(Val(strValorEntero), strFormato)
                End If

                ' Formatear Decimal
                strFormato = ""
                intLon = Len(strValorDecimal)
                If intLon < intPresicion Then
                    ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                    For intCon = intLon To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                ElseIf intLon = 0 Then
                    ' Si la longitud es 0 se llena el valor decimal con ceros
                    For intCon = 0 To intPresicion - 1
                        strValorDecimal = strValorDecimal & "0"
                    Next

                End If

                Formatear_Campo = strSignoNegativo & strValorEntero & "." & strValorDecimal

            End If

        End If

    End Function

    Private Function Formatear_Campo_SIIGO(ByVal strValor As String, ByVal intLongitud As Integer, ByVal intTipoCampo As Integer, Optional ByVal intPresicion As Integer = 0) As String
        Try
            Formatear_Campo_SIIGO = String.Empty
            strValor = strValor.Replace(Chr(10).ToString, "")
            strValor = strValor.Replace(CARACTER_ENTER, "")

            Dim intCon%, strFormato$, intPos%, strValorEntero$, strValorDecimal$, intLon%, intLongSignoNegativo%
            Dim strSignoNegativo$ = String.Empty

            strFormato = ""
            strValor = Trim$(strValor)

            If intTipoCampo = CAMPO_CEROS_DERECHA_SIIGO Then
                strValor = Trim$(strValor)
                intLon = Len(strValor)

                For intCon = intLon To intLongitud - 1
                    strValor = strValor & "0"
                Next
                Formatear_Campo_SIIGO = strValor

            ElseIf intTipoCampo = CAMPO_NUMERICO_SIIGO Then

                ' Si es vacio
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    Formatear_Campo_SIIGO = strFormato

                Else
                    ' Buscar signo negativo
                    intPos = InStr(strValor, "-")
                    If intPos > 0 Then
                        strSignoNegativo = "-"
                        intLongSignoNegativo = 1
                        strValor = Mid$(strValor, 2)
                    End If

                    For intCon = 1 To intLongitud - intLongSignoNegativo
                        strFormato = strFormato & "0"
                    Next

                    Formatear_Campo_SIIGO = strSignoNegativo & Format$(Val(strValor), strFormato)

                End If

            ElseIf intTipoCampo = CAMPO_ALFANUMERICO_SIIGO Then

                strValor = strValor.Replace(Chr(10).ToString, "")
                strValor = strValor.Replace(CARACTER_ENTER, "")

                If Len(strValor) > intLongitud Then
                    strValor = Mid$(strValor, 1, intLongitud)
                End If
                Formatear_Campo_SIIGO = strValor & Space$(intLongitud - Len(strValor))


            ElseIf intTipoCampo = CAMPO_DECIMAL_SIIGO Then

                ' Si es vacio
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    Formatear_Campo_SIIGO = strFormato

                Else

                    ' Buscar signo negativo
                    intPos = InStr(strValor, "-")
                    If intPos > 0 Then
                        strSignoNegativo = "-"
                        intLongSignoNegativo = 1
                        strValor = Mid$(strValor, 2)
                    End If

                    ' Buscar signo decimal
                    intPos = InStr(strValor, ".")
                    If intPos > 0 Then
                        strValorEntero = Mid$(strValor, 1, intPos - 1)
                        strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                    Else
                        strValorEntero = strValor
                        strValorDecimal = ""
                    End If
                    strValorEntero = Trim$(strValorEntero)
                    strValorDecimal = Trim$(strValorDecimal)

                    ' Formatear Entero
                    For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                        strFormato = strFormato & "0"
                    Next

                    If strValorEntero = "" Then
                        strValorEntero = strFormato
                    Else
                        strValorEntero = Format$(Val(strValorEntero), strFormato)
                    End If

                    ' Formatear Decimal
                    strFormato = ""
                    intLon = Len(strValorDecimal)
                    If intLon < intPresicion Then
                        ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                        For intCon = intLon To intPresicion - 1
                            strValorDecimal = strValorDecimal & "0"
                        Next

                    ElseIf intLon = 0 Then
                        ' Si la longitud es 0 se llena el valor decimal con ceros
                        For intCon = 0 To intPresicion - 1
                            strValorDecimal = strValorDecimal & "0"
                        Next

                    End If

                    Formatear_Campo_SIIGO = strSignoNegativo & strValorEntero & "." & strValorDecimal

                End If

            ElseIf intTipoCampo = CAMPO_FECHA_SIIGO Then
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    Formatear_Campo_SIIGO = strFormato
                Else
                    Dim dteAuxiliar = CDate(strValor)
                    Dim strAuxiliar = Year(dteAuxiliar).ToString

                    If Month(dteAuxiliar).ToString.Length < 2 Then
                        strAuxiliar += "0" & Month(dteAuxiliar).ToString
                    Else
                        strAuxiliar += Month(dteAuxiliar).ToString
                    End If

                    If Day(dteAuxiliar).ToString.Length < 2 Then
                        strAuxiliar += "0" & Day(dteAuxiliar).ToString
                    Else
                        strAuxiliar += Day(dteAuxiliar).ToString
                    End If

                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next

                    Formatear_Campo_SIIGO = Format$(Val(strAuxiliar), strFormato)

                End If

            End If

            Formatear_Campo_SIIGO = Mid(Formatear_Campo_SIIGO, 1, intLongitud)

            Return Formatear_Campo_SIIGO
        Catch ex As Exception
            Return ""
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Formatear_Campo_SIIGO: " & Me.objGeneral.Traducir_Error(ex.Message)
        End Try
    End Function

    Private Function Formatear_Campo_UNO_EE(ByVal strValor As String, ByVal intLongitud As Integer, ByVal intTipoCampo As Integer, Optional ByVal intPresicion As Integer = 0) As String
        Try
            Formatear_Campo_UNO_EE = String.Empty
            strValor = strValor.Replace(Chr(10).ToString, "")
            strValor = strValor.Replace(CARACTER_ENTER, "")

            Dim intCon%, strFormato$, intPos%, strValorEntero$, strValorDecimal$, intLon%, intLongSignoNegativo%
            Dim strSignoNegativo$ = String.Empty
            Dim strSignoPositivo$ = String.Empty
            Dim strCaracterPunto$ = String.Empty

            strFormato = ""
            strValor = Trim$(strValor)

            If intTipoCampo = CAMPO_NUMERICO Then

                strValor = Trim$(strValor)
                intLon = Len(strValor)

                For intCon = intLon To intLongitud - 1
                    strValor = "0" & strValor
                Next
                Formatear_Campo_UNO_EE = strValor

            ElseIf intTipoCampo = CAMPO_DINERO Then
                strSignoPositivo = "+"
                strCaracterPunto = "."
                ' Si es vacio
                If strValor = "0" Then
                    strFormato = ""
                    For intCon = 1 To 15
                        strFormato = strFormato & "0"
                    Next

                    strValor = strSignoPositivo & strFormato & strCaracterPunto

                    strFormato = ""
                    For intCon = 1 To 4
                        strFormato = strFormato & "0"
                    Next

                    strValor = strValor & strFormato

                    If intLongitud - Len(strValor) > 0 Then
                        Formatear_Campo_UNO_EE = strValor & Space$(intLongitud - Len(strValor))
                    Else
                        Formatear_Campo_UNO_EE = strValor
                    End If


                Else
                    strFormato = ""
                    For intCon = 1 To 15 - Len(strValor)
                        strFormato = strFormato & "0"
                    Next

                    strValor = strSignoPositivo & strFormato & strValor & strCaracterPunto

                    strFormato = ""
                    For intCon = 1 To 4
                        strFormato = strFormato & "0"
                    Next

                    strValor = strValor & strFormato

                    If intLongitud - Len(strValor) > 0 Then
                        Formatear_Campo_UNO_EE = strValor & Space$(intLongitud - Len(strValor))
                    Else
                        Formatear_Campo_UNO_EE = strValor
                    End If

                End If
            ElseIf intTipoCampo = CAMPO_ALFANUMERICO Then

                strValor = strValor.Replace(Chr(10).ToString, "")
                strValor = strValor.Replace(CARACTER_ENTER, "")

                If Len(strValor) > intLongitud Then
                    strValor = Mid$(strValor, 1, intLongitud)
                End If

                Formatear_Campo_UNO_EE = strValor & Space$(intLongitud - Len(strValor))


            ElseIf intTipoCampo = CAMPO_DECIMAL Then

                ' Si es vacio
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    Formatear_Campo_UNO_EE = strFormato

                Else

                    ' Buscar signo negativo
                    intPos = InStr(strValor, "-")
                    If intPos > 0 Then
                        strSignoNegativo = "-"
                        intLongSignoNegativo = 1
                        strValor = Mid$(strValor, 2)
                    End If

                    ' Buscar signo decimal
                    intPos = InStr(strValor, ".")
                    If intPos > 0 Then
                        strValorEntero = Mid$(strValor, 1, intPos - 1)
                        strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                    Else
                        strValorEntero = strValor
                        strValorDecimal = ""
                    End If
                    strValorEntero = Trim$(strValorEntero)
                    strValorDecimal = Trim$(strValorDecimal)

                    ' Formatear Entero
                    For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                        strFormato = strFormato & "0"
                    Next

                    If strValorEntero = "" Then
                        strValorEntero = strFormato
                    Else
                        strValorEntero = Format$(Val(strValorEntero), strFormato)
                    End If

                    ' Formatear Decimal
                    strFormato = ""
                    intLon = Len(strValorDecimal)
                    If intLon < intPresicion Then
                        ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                        For intCon = intLon To intPresicion - 1
                            strValorDecimal = strValorDecimal & "0"
                        Next

                    ElseIf intLon = 0 Then
                        ' Si la longitud es 0 se llena el valor decimal con ceros
                        For intCon = 0 To intPresicion - 1
                            strValorDecimal = strValorDecimal & "0"
                        Next

                    End If

                    Formatear_Campo_UNO_EE = strSignoNegativo & strValorEntero & "." & strValorDecimal

                End If

            ElseIf intTipoCampo = CAMPO_FECHA Then
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    Formatear_Campo_UNO_EE = strFormato
                Else
                    Dim dteAuxiliar = CDate(strValor)
                    Dim strAuxiliar = Year(dteAuxiliar).ToString

                    If Month(dteAuxiliar).ToString.Length < 2 Then
                        strAuxiliar += "0" & Month(dteAuxiliar).ToString
                    Else
                        strAuxiliar += Month(dteAuxiliar).ToString
                    End If

                    If Day(dteAuxiliar).ToString.Length < 2 Then
                        strAuxiliar += "0" & Day(dteAuxiliar).ToString
                    Else
                        strAuxiliar += Day(dteAuxiliar).ToString
                    End If

                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next

                    Formatear_Campo_UNO_EE = Format$(Val(strAuxiliar), strFormato)

                End If

            End If

            Formatear_Campo_UNO_EE = Mid(Formatear_Campo_UNO_EE, 1, intLongitud)

            Return Formatear_Campo_UNO_EE
        Catch ex As Exception
            Return ""
            Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Formatear_Campo_UNO_EE: " & Me.objGeneral.Traducir_Error(ex.Message)
        End Try
    End Function

    Private Function Actualizar_Interfaz_Definitivo(ByVal strTabla As String, ByVal intTIDOCodigo As Integer) As Boolean

        Try
            Dim strSQL As String = ""

            strSQL = "UPDATE " & strTabla
            strSQL += " SET Interfase_Contable = " & ARCHIVO_DEFINITIVO
            strSQL += " WHERE EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND Estado = " & clsGeneral.ESTADO_DEFINITIVO
            strSQL += " AND Fecha >= CONVERT(DATETIME, '" & objGeneral.Formatear_Fecha_SQL(Me.dtaFechaInicial) & "', 101)"
            strSQL += " AND Fecha <= CONVERT(DATETIME, '" & objGeneral.Formatear_Fecha_SQL(Me.dtaFechaFinal) & "', 101)"
            strSQL += " AND TIDO_Codigo = " & intTIDOCodigo
            strSQL += " AND Interfase_Contable = " & ARCHIVO_BORRADOR

            Me.objGeneral.Ejecutar_SQL(strSQL)
        Catch ex As Exception
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(clsGeneral.CERO).GetMethod().Name

            Try
                Me.objGeneral.ExcepcionSQL = ex
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
            Catch Exc As Exception
                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)

            End Try
        End Try

        Return True
    End Function

#End Region

#Region "Funciones Consulta Información"

    Private Function Retorna_Informacion_Liquidacion(ByVal lonNumeLiqu As Long, ByRef lonNumeDocuLiqu As Long, ByRef lonNumeDocuPlan As Long) As Boolean
        Dim objGeneral As New clsGeneral
        Try
            Dim strSQL As String, dtsLiquida As DataSet

            strSQL = "SELECT ELPD.Numero_Documento AS Numero_Liquidacion, ENPD.Numero_Documento AS Numero_Planilla"
            strSQL += " FROM Encabezado_Liquidacion_Planilla_Despachos AS ELPD,"
            strSQL += " Encabezado_Planilla_Despachos AS ENPD"
            strSQL += " WHERE ELPD.EMPR_Codigo = ENPD.EMPR_Codigo"
            strSQL += " AND ELPD.ENPD_Numero = ENPD.Numero"
            strSQL += " AND ELPD.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ELPD.Numero = " & lonNumeLiqu.ToString

            dtsLiquida = objGeneral.Retorna_Dataset(strSQL)

            If dtsLiquida.Tables(0).Rows.Count > 0 Then

                lonNumeDocuLiqu = dtsLiquida.Tables(0).Rows(0).Item("Numero_Liquidacion").ToString()
                lonNumeDocuPlan = dtsLiquida.Tables(0).Rows(0).Item("Numero_Planilla").ToString()

                Retorna_Informacion_Liquidacion = True
            Else
                Retorna_Informacion_Liquidacion = False
            End If

        Catch ex As Exception
            Retorna_Informacion_Liquidacion = False
        End Try
        Return Retorna_Informacion_Liquidacion

    End Function

    Private Function Retorna_Egreso_Anticipo_Planilla(ByVal lonNumePlan As Long, ByRef lonNumeDocuEgre As Long) As Boolean
        Dim objGeneral As New clsGeneral
        Try
            Dim strSQL As String, dtsEgresos As DataSet

            strSQL = "SELECT Codigo, Numero"
            strSQL += " FROM Encabezado_Documento_Comprobantes AS ENDC"
            strSQL += " WHERE ENDC.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ENDC.CATA_DOOR_Codigo IN (2604, 2605)" ' ANTICIPOS Y SOBREANTICIPOS
            strSQL += " AND ENDC.Documento_Origen = " & lonNumePlan.ToString
            strSQL += " AND ENDC.Estado = 1"
            strSQL += " AND ENDC.Anulado = 0"

            dtsEgresos = objGeneral.Retorna_Dataset(strSQL)

            If dtsEgresos.Tables(0).Rows.Count > 0 Then

                lonNumeDocuEgre = dtsEgresos.Tables(0).Rows(0).Item("Numero").ToString()
                Retorna_Egreso_Anticipo_Planilla = True
            Else
                lonNumeDocuEgre = 0
                Retorna_Egreso_Anticipo_Planilla = False
            End If

        Catch ex As Exception
            Retorna_Egreso_Anticipo_Planilla = False
        End Try
        Return Retorna_Egreso_Anticipo_Planilla

    End Function

    Private Function Retorna_Tenedor_Planilla(ByVal lonNumePlan As Long, ByRef strNumeIdenTene As String) As Boolean
        Dim objGeneral As New clsGeneral
        Try
            Dim strSQL As String, dtsEgresos As DataSet

            strSQL = "SELECT TERC.Numero_Identificacion"
            strSQL += " FROM Encabezado_Planilla_Despachos AS ENPD, Terceros AS TERC"
            strSQL += " WHERE ENPD.EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND ENPD.Numero_Documento = " & lonNumePlan.ToString
            strSQL += " AND ENPD.EMPR_Codigo = TERC.EMPR_Codigo"
            strSQL += " AND ENPD.TERC_Codigo_Tenedor = TERC.Codigo"

            dtsEgresos = objGeneral.Retorna_Dataset(strSQL)

            If dtsEgresos.Tables(0).Rows.Count > 0 Then
                strNumeIdenTene = dtsEgresos.Tables(0).Rows(0).Item("Numero_Identificacion").ToString()
                Retorna_Tenedor_Planilla = True
            Else
                strNumeIdenTene = ""
                Retorna_Tenedor_Planilla = False
            End If

        Catch ex As Exception
            Retorna_Tenedor_Planilla = False
        End Try
        Return Retorna_Tenedor_Planilla

    End Function

    Private Function Retorna_Planilla_Comprobante_Egreso(ByVal lonCodiEgre As Long, ByRef lonNumePlan As Long) As Boolean
        Dim objGeneral As New clsGeneral
        Try
            Dim strSQL As String, dtsEgresos As DataSet

            strSQL = "SELECT Documento_Origen"
            strSQL += " FROM Encabezado_Documento_Comprobantes"
            strSQL += " WHERE EMPR_Codigo = " & intEmpresa.ToString
            strSQL += " AND TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO
            strSQL += " AND Codigo = " & lonCodiEgre.ToString

            dtsEgresos = objGeneral.Retorna_Dataset(strSQL)

            If dtsEgresos.Tables(0).Rows.Count > 0 Then
                lonNumePlan = dtsEgresos.Tables(0).Rows(0).Item("Documento_Origen").ToString()
                Retorna_Planilla_Comprobante_Egreso = True
            Else
                lonNumePlan = 0
                Retorna_Planilla_Comprobante_Egreso = False
            End If

        Catch ex As Exception
            Retorna_Planilla_Comprobante_Egreso = False
        End Try
        Return Retorna_Planilla_Comprobante_Egreso
    End Function

#End Region

End Class