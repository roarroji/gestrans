﻿Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports Softtools.GesCarga.Conexion
Imports System.Drawing
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports QRCodeEncoderLibrary
Imports QRCodeSharedLibrary

Public Class Reportes
    Inherits System.Web.UI.Page

#Region "Atributos"
    Dim objGeneral As clsGeneral
    Dim repDocumento As ReportViewer
    Private Encoder As QRCodeEncoder
    Private QRCodeImage As Bitmap
    Private QRCodeImageArea As System.Drawing.Rectangle = New System.Drawing.Rectangle
#End Region

#Region "Costantes de reportes"
    'Dim QR_Generator As New MessagingToolkit.QRCode.Codec.QRCodeEncoder
    Const NOMBRE_REPORTE_GUIA As String = "RepGuia"
    Const NOMBRE_REPORTE_GUIA_PAQUETERIA As String = "RepGuiaPaqueteria"
    Const NOMBRE_REPORTE_LEGALIZACION_RECAUDO_GUIAS As String = "RepLegaRecauGuias"

    Const NOMBRE_REPORTE_ETIQUETAS_GUIA As String = "Rep_Etiquetas_Guia"
    Const NOMBRE_REPORTE_MANIFIESTO As String = "RepMani"
    Const NOMBRE_REPORTE_REMESA As String = "RepReme"
    Const NOMBRE_REPORTE_ORDEN_CARGUE As String = "RepOrca"
    Const NOMBRE_REPORTE_FACTURA As String = "RepFact"
    Const NOMBRE_REPORTE_NOTA_CREDITO As String = "RepNotCre"
    Const NOMBRE_REPORTE_PLANILLA_ENTREGA As String = "RepPlen"
    Const NOMBRE_REPORTE_PLANILLA_RECOLECCIONES As String = "RepPlanillaRecolecciones"
    Const NOMBRE_REPORTE_PLANILLA_DESPACHOS As String = "RepPldc"
    Const NOMBRE_REPORTE_PLCARGUE As String = "repPLResumenCargue"
    Const NOMBRE_REPORTE_PLANILLA_DESPACHOS_OTRAS_EMPRESAS As String = "RepPlDoE"
    Const NOMBRE_REPORTE_PLANILLA_DESPACHOS_PAQUETERIA As String = "RepPldp"
    Const NOMBRE_REPORTE_PLANILLA_DESPACHOS_PROVEEDORES As String = "RepPlDPro"

    Const NOMBRE_REPORTE_CUMPLIDO As String = "RepCmpl"
    Const NOMBRE_REPORTE_COMPROBANTE_EGRESO As String = "RepCoEgr"
    Const NOMBRE_REPORTE_COMPROBANTE_INGRESO As String = "RepCoIng"
    Const NOMBRE_REPORTE_ORDEN_SERVICIO As String = "RepOrSer"
    Const NOMBRE_REPORTE_SOLICITUD_ORDEN_SERVICIO As String = "RepSolOrSer"
    Const NOMBRE_REPORTE_PLAN_RUTA As String = "RepPlanRuta"
    Const NOMBRE_REPORTE_LIQUIDACION_PLANILLA_DESPACHO As String = "RepLiquPlan"
    Const NOMBRE_REPORTE_PLANILLA_POLIZA As String = "repPlanillaPoliza"
    Const NOMBRE_REPORTE_LEGALIZACION_GASTOS As String = "repLegalizacionGastos"
    Const NOMBRE_REPORTE_INSPECCIÓN_PREOPERACIONAL As String = "RepInspeccionPuntoGestion"
    Const NOMBRE_REPORTE_VEHICULOS_LISTA_NEGRA As String = "repVehiLstNegra"

    Const TIPO_ARCHIVO_GENERA As String = "text/pdf"
    Const DESCRIP_ARCHIVO_FACTURA_GENERA As String = "Reporte Factura base 64"
    Const DESCRIP_ARCHIVO_NOTA_GENERA As String = "Reporte Nota base 64"
    Const NOMBRE_FORMATO_GASTOS_CONDUCTOR = "repFormatoGastosConductors"
    Const NOMBRE_REPORTE_PLAN_MANTENIMIENTO As String = "RepPlanMantenimiento"
    Const NOMBRE_REPORTE_TAREA_MANTENIMIENTO As String = "RepTareaMantenimiento"
    Const NOMBRE_REPORTE_ORDEN_TRABAJO As String = "RepOrdenTrabajo"
    Const NOMBRE_REPORTE_PLANILLA_PAQUETERIA As String = "repPlanillaPaqueteria_Pv2_"
    Const NOMBRE_REPORTE_SOLICITUD_ANTICIPO As String = "repSolicitudAnticipo"

    Const NOMBRE_REPORTE_LEGALIZACION_GUIAS_RECAUDO As String = "RepLegalizacionGuiaRecaudo"
    Const NOMBRE_REPORTE_LEGALIZACION_GUIAS_ENTREGA_CARTERA As String = "RepEntregaCartera"
    Const NOMBRE_REPORTE_LEGALIZACION_GUIAS_ENTREGA_ARCHIVO As String = "RepEntregaArchivo"
    Public Enum TIPO_FACTURA
        FACTURA = 5201
        OTROS_CONCEPTOS = 5202
    End Enum

#Region "Constructores"

    Public Sub New()
        Me.objGeneral = New clsGeneral
        Me.repDocumento = New ReportViewer
    End Sub
#End Region
#End Region



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then


            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_PLANILLA_PAQUETERIA Then
                Call Configurar_Reporte_Planilla_Paqueteria()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_GUIA_PAQUETERIA Then
                Call Configurar_Reporte_GUIA_Paqueteria()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_LEGALIZACION_RECAUDO_GUIAS Then
                Call Configurar_Reporte_Legalizacion_Recaudo_Guias()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_SOLICITUD_ANTICIPO Then
                Call Configurar_Reporte_Solicitud_Anticipo()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_ORDEN_TRABAJO Then
                Call Configurar_Reporte_Orden_Trabajo()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_TAREA_MANTENIMIENTO Then
                Call Configurar_Reporte_Tarea_Mantenimiento()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_PLAN_MANTENIMIENTO Then
                Call Configurar_Reporte_Plan_Mantenimiento()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_FORMATO_GASTOS_CONDUCTOR Then
                Call Configurar_Reporte_Formato_Gastos()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_GUIA Then
                Call Configurar_Reporte_Guias()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_ETIQUETAS_GUIA Then
                Call Configurar_Reporte_Etiquetas_Guias()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_MANIFIESTO Then
                Call Configurar_Reporte_Manifiestos()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_REMESA Then
                Call Configurar_Reporte_Remesas()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_ORDEN_CARGUE Then
                Call Configurar_Reporte_Orden_Cargues()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_FACTURA Then
                Call Configurar_Reporte_Facturas()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_NOTA_CREDITO Then
                Call Configurar_Reporte_Notas_Credito()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_CUMPLIDO Then
                Call Configurar_Reporte_Cumplidos()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_PLANILLA_ENTREGA Then
                Call Configurar_Reporte_Planilla_Entregada()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_PLANILLA_RECOLECCIONES Then
                Call Configurar_Reporte_Planilla_Recolecciones()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_PLANILLA_DESPACHOS Then
                Call Configurar_Reporte_Planilla_Despacho()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_PLCARGUE Then
                Call Configurar_Reporte_Planilla_Resumen_Cargue()
            End If


            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_PLANILLA_POLIZA Then
                Call Configurar_Reporte_Planilla_Poliza()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_LEGALIZACION_GASTOS Then
                Call Configurar_Reporte_Legalizacion_Gastos_Conductro()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_COMPROBANTE_EGRESO Then
                Call Configurar_Reporte_Comprobante_Egreso()
            End If


            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_COMPROBANTE_INGRESO Then
                Call Configurar_Reporte_Comprobante_Ingreso()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_ORDEN_SERVICIO Then
                Call Configurar_Reporte_Orden_Servicio()
            End If


            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_SOLICITUD_ORDEN_SERVICIO Then
                Call Configurar_Reporte_Solicitud_Orden_Servicio()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_PLAN_RUTA Then
                Call Configurar_Reporte_Plan_Ruta()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_LIQUIDACION_PLANILLA_DESPACHO Then
                Call Configurar_Reporte_Liquidacion_Planilla_Despacho()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_PLANILLA_DESPACHOS_PAQUETERIA Then
                Call Configurar_Reporte_Planilla_Despacho_Paqueteria()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_PLANILLA_DESPACHOS_OTRAS_EMPRESAS Then
                Call Configurar_Reporte_Planilla_Despachos_Otras_Empresas()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_INSPECCIÓN_PREOPERACIONAL Then
                Call Configurar_Reporte_Inspeccion_Preoperacional()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_VEHICULOS_LISTA_NEGRA Then

                Call Configurar_Reporte_Vehiculos_Lista_Negra()
            End If

            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_PLANILLA_DESPACHOS_PROVEEDORES Then
                Call Configurar_Reporte_Planilla_Despachos_Proveedores()
            End If

            REM: Reportes Legalizacion Guias
            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_LEGALIZACION_GUIAS_RECAUDO Then
                Call Configurar_Reporte_Legalizacion_Guias_Recaudo()
            End If
            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_LEGALIZACION_GUIAS_ENTREGA_CARTERA Or
                Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_LEGALIZACION_GUIAS_ENTREGA_ARCHIVO Then
                Call Configurar_Reporte_Legalizacion_Guias_Entregas_Documentos()
            End If

        End If

    End Sub





#Region "Configuracion de reportes"
    ''' <summary>
    ''' Crea un archivo PDF a partir de un ReportViewer
    ''' </summary>
    ''' <param name="visorReporte"></param>
    ''' <returns></returns>
    Public Function SerializarReportePdf(visorReporte As ReportViewer) As Byte()
        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        Dim mimeType As String = String.Empty
        Dim encoding As String = String.Empty
        Dim filenameExtension As String = String.Empty

        visorReporte.ProcessingMode = ProcessingMode.Local

        Dim pdf As Byte() = visorReporte.LocalReport.Render("PDF", Nothing, mimeType, encoding, filenameExtension, streamids, warnings)
        Return pdf

    End Function

    Public Function SerializarReporteExcel(ByVal visorReporte As ReportViewer, ByRef NombreExtension As String, ByRef mimeType As String, ByVal NombreArchivo As String) As Byte()

        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        mimeType = String.Empty
        Dim encoding As String = String.Empty
        NombreExtension = String.Empty
        Dim deviceInfo As String = String.Empty
        Dim streams As String() = Nothing


        Dim excel As Byte() = visorReporte.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)
        NombreExtension = String.Format("{0}.{1}", NombreArchivo, "xls")

        Return excel

    End Function

    Private Sub Configurar_Reporte_Solicitud_Orden_Servicio()
        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/ServicioCliente/OrdenServicio/repSolicitudOrdenServicio_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Orden_Servicio "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If

            strConsulta += Request.QueryString("TipoDocumento")

            'Guardar el resultado del sp en un dataset
            Dim dtsEncabezadoOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEncabezadoOrdenServicio.Fill(dsReportes, "dtsEncabezadoOrdenServicio")

            'Pasar el dataset a un datatable
            Dim dtbEncabezadoOrdenServicio As New DataTable
            dtbEncabezadoOrdenServicio = dsReportes.Tables("dtsEncabezadoOrdenServicio")

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Orden_Servicio "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleOrdenServicio.Fill(dsReportes, "dtsDetalleOrdenServicio")

            'Pasar el dataset a un datatable
            Dim dtbDetalleOrdenServicio As New DataTable
            dtbDetalleOrdenServicio = dsReportes.Tables("dtbDetalleOrdenServicio")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsEncabezadoOrdenServicio", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsDetalleOrdenServicio", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsEncabezadoOrdenServicio.Dispose()
            dtsDetalleOrdenServicio.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Comprobante_Egreso :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try

    End Sub

    Private Sub Configurar_Reporte_Orden_Servicio()
        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/ServicioCliente/OrdenServicio/repOrdenServicio_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Orden_Servicio "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If

            strConsulta += Request.QueryString("TipoDocumento")

            'Guardar el resultado del sp en un dataset
            Dim dtsEncabezadoOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEncabezadoOrdenServicio.Fill(dsReportes, "dtsEncabezadoOrdenServicio")

            'Pasar el dataset a un datatable
            Dim dtbEncabezadoOrdenServicio As New DataTable
            dtbEncabezadoOrdenServicio = dsReportes.Tables("dtsEncabezadoOrdenServicio")

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Orden_Servicio "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleOrdenServicio As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleOrdenServicio.Fill(dsReportes, "dtsDetalleOrdenServicio")

            'Pasar el dataset a un datatable
            Dim dtbDetalleOrdenServicio As New DataTable
            dtbDetalleOrdenServicio = dsReportes.Tables("dtsDetalleOrdenServicio")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsEncabezadoOrdenServicio", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsDetalleOrdenServicio", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsEncabezadoOrdenServicio.Dispose()
            dtsDetalleOrdenServicio.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Comprobante_Egreso :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try

    End Sub


    Private Sub Configurar_Reporte_Comprobante_Egreso()
        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Tesoreria/Comprobantes/repComprobanteEgreso_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Encabezado_Comprobante_Egreso "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsEncabezadoComprobanteEgreso As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEncabezadoComprobanteEgreso.Fill(dsReportes, "dtsEncabezadoComprobanteEgreso")

            'Pasar el dataset a un datatable
            Dim dtbEncabezadoComprobanteEgreso As New DataTable
            dtbEncabezadoComprobanteEgreso = dsReportes.Tables("dtsEncabezadoComprobanteEgreso")

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Comprobante "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleComprobanteEgreso As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleComprobanteEgreso.Fill(dsReportes, "dtsDetalleComprobanteEgreso")

            'Pasar el dataset a un datatable
            Dim dtbDetalleComprobanteEgreso As New DataTable
            dtbDetalleComprobanteEgreso = dsReportes.Tables("dtsDetalleComprobanteEgreso")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsEncabezadoComprobanteEgreso", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsDetalleComprobanteEgreso", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtbEncabezadoComprobanteEgreso.Dispose()
            dtsDetalleComprobanteEgreso.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Comprobante_Egreso :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try

    End Sub
    Private Sub Configurar_Reporte_Comprobante_Ingreso()
        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Tesoreria/Comprobantes/repComprobanteIngreso_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Encabezado_Comprobante_Ingreso "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsEncabezadoComprobanteEgreso As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEncabezadoComprobanteEgreso.Fill(dsReportes, "dtsEncabezadoComprobanteEgreso")

            'Pasar el dataset a un datatable
            Dim dtbEncabezadoComprobanteEgreso As New DataTable
            dtbEncabezadoComprobanteEgreso = dsReportes.Tables("dtsEncabezadoComprobanteEgreso")

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Comprobante "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleComprobanteEgreso As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleComprobanteEgreso.Fill(dsReportes, "dtsDetalleComprobanteEgreso")

            'Pasar el dataset a un datatable
            Dim dtbDetalleComprobanteEgreso As New DataTable
            dtbDetalleComprobanteEgreso = dsReportes.Tables("dtsDetalleComprobanteEgreso")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsEncabezadoComprobanteEgreso", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsDetalleComprobanteEgreso", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtbEncabezadoComprobanteEgreso.Dispose()
            dtsDetalleComprobanteEgreso.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Comprobante_Egreso :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try
    End Sub

    Public Sub Configurar_Reporte_Guias()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Despachos/Guias/repGuias_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gps_Reporte_Guias "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteGuias.Fill(dsReportes, "dtsReporteGuias")

            'Pasar el dataset a un datatable
            Dim dtbReporteGuias As New DataTable
            dtbReporteGuias = dsReportes.Tables("dtsEncabezadoGuiaGeneral")
            Dim cadenaQR = ""
            Try
                cadenaQR = dsReportes.Tables("dtsReporteGuias").Rows(0)("CUFE").ToString() ' + vbNewLine


            Catch ex As Exception
                cadenaQR = dsReportes.Tables("dtsReporteGuias").Rows(0)("CUFE").ToString() ' + vbNewLine


            End Try
            dsReportes.Tables("dtsReporteGuias").Rows(0)("QRCUFE") = ImageToByte(EncodeQRIMG(cadenaQR))



            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReporteGuias", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReporteGuias.Dispose()
            dtbReporteGuias.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Guias :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try



    End Sub
    Public Sub Configurar_Reporte_GUIA_Paqueteria()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/PaqueteriaV2/repGuiasPaqueteria_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_reporte_guia_paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteGuias.Fill(dsReportes, "dtsReporteGuiasPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbReporteGuias As New DataTable
            dtbReporteGuias = dsReportes.Tables("dtsReporteGuiasPaqueteria")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReporteGuiasPaqueteria", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReporteGuias.Dispose()
            dtbReporteGuias.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Guias_Paqueteria :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try



    End Sub

    Public Sub Configurar_Reporte_Legalizacion_Recaudo_Guias()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/PaqueteriaV2/RepLegalizacionRecaudoGuias_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_reporte_encabezado_legalizacion_recaudo_guias "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteGuias.Fill(dsReportes, "dtsReporteEncabezadoLegalizarRecaudoGuias")

            'Pasar el dataset a un datatable
            Dim dtbReporteGuias As New DataTable
            dtbReporteGuias = dsReportes.Tables("dtsReporteEncabezadoLegalizarRecaudoGuias")

            'Armar la reporte

            strConsulta = "EXEC gsp_reporte_detalle_legalizacion_recaudo_guias "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetalleGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetalleGuias.Fill(dsReportes, "dtsReporteDetalleLegalizarRecaudoGuias")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetalleGuias As New DataTable
            dtbReporteDetalleGuias = dsReportes.Tables("dtsReporteDetalleLegalizarRecaudoGuias")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReporteEncabezadoLegalizarRecaudoGuias", dsReportes.Tables(0))
            Dim rds2 As New ReportDataSource("dtsReporteDetalleLegalizarRecaudoGuias", dsReportes.Tables(1))

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rds2)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReporteGuias.Dispose()
            dtbReporteGuias.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Legalizacion_Recaudo_Paqueteria :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try

    End Sub

    Public Sub Configurar_Reporte_Etiquetas_Guias()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/PaqueteriaV2/repEtiquetasGuiaPaqueteria.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Encabezado_Guia_Paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteGuias.Fill(dsReportes, "dtsEncabezadoGuiaGeneral")


            'Pasar el dataset a un datatable
            Dim dtbReporteGuias As New DataTable
            dtbReporteGuias = dsReportes.Tables("dtsEncabezadoGuiaGeneral")
            Dim cadenaQR = ""
            Try
                cadenaQR = dsReportes.Tables("dtsEncabezadoGuiaGeneral").Rows(0)("Numero_Documento").ToString() ' + vbNewLine


            Catch ex As Exception
                cadenaQR = dsReportes.Tables("dtsEncabezadoGuiaGeneral").Rows(0)("Numero_Documento").ToString() ' + vbNewLine


            End Try
            dsReportes.Tables("dtsEncabezadoGuiaGeneral").Rows(0)("QRNumeroDocumento") = ImageToByte(EncodeQRIMG(cadenaQR))


            'Armar la consulta
            strConsulta = "EXEC gsp_Detalle_Guia_Paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleGuias.Fill(dsReportes, "dtsDetalleUnidadesGuia")

            'Pasar el dataset a un datatable
            Dim dtbDetalleGuias As New DataTable
            dtbDetalleGuias = dsReportes.Tables("dtsDetalleUnidadesGuia")
            If dsReportes.Tables("dtsDetalleUnidadesGuia").Rows.Count > 0 Then
                For Each item In dsReportes.Tables("dtsDetalleUnidadesGuia").Rows
                    Try
                        cadenaQR = dsReportes.Tables("dtsDetalleUnidadesGuia").Rows(0)("Numero_Documento").ToString() + " - " + item("Numero_Unidad").ToString() ' + vbNewLine


                    Catch ex As Exception
                        cadenaQR = dsReportes.Tables("dtsDetalleUnidadesGuia").Rows(0)("Numero_Documento").ToString() + " - " + dsReportes.Tables("dtsDetalleUnidadesGuia").Rows(0)("Numero_Unidad").ToString()  ' + vbNewLine


                    End Try
                    item("QRNumeroDocumento") = ImageToByte(EncodeQRIMG(cadenaQR))
                Next

            End If



            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsEncabezadoGuiaGeneral", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsDetalleUnidadesGuia", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReporteGuias.Dispose()
            dtbReporteGuias.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Etiquetas_Guia :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try



    End Sub

    Public Sub Configurar_Reporte_Remesas()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""

        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Despachos/Remesa/repRemesa_" & Me.Request.QueryString("Prefijo") & ".rdlc")

            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Remesa "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteRemesa As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteRemesa.Fill(dsReportes, "dtsReporteRemesa")

            'Pasar el dataset a un datatable
            Dim dtbReporteRemesa As New DataTable
            dtbReporteRemesa = dsReportes.Tables("dtsReporteRemesa")

            'Armar la consulta
            strConsulta = "EXEC gsp_reporte_detalle_precintos_remesas "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleRemesa2 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleRemesa2.Fill(dsReportes, "dtsDetalleRemesa")

            'Pasar el dataset a un datatable
            Dim dtbDetalleRemesa2 As New DataTable
            dtbDetalleRemesa2 = dsReportes.Tables("dtsDetalleRemesa")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReporteRemesa", dsReportes.Tables(0))
            Dim rds2 As New ReportDataSource("dtsDetalleRemesa", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rds2)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReporteRemesa.Dispose()
            dtbReporteRemesa.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Remesas :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try



    End Sub

    Public Sub Configurar_Reporte_Orden_Cargues()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Despachos/OrdenCargue/repOrdenCargue_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_OrdenCargue "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteOrdenCargue As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteOrdenCargue.Fill(dsReportes, "dtsRepOrdenCargue")

            'Pasar el dataset a un datatable
            Dim dtbReporteOrdenCargue As New DataTable
            dtbReporteOrdenCargue = dsReportes.Tables("dtsRepOrdenCargue")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsRepOrdenCargue", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReporteOrdenCargue.Dispose()
            dtbReporteOrdenCargue.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Orden_Cargues :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try



    End Sub

    Public Sub Configurar_Reporte_Cumplidos()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Despachos/Cumplido/repCumplido_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Cumplido_Planilla_Despacho "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteCumplido As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteCumplido.Fill(dsReportes, "dtsReporteCumplido")

            'Pasar el dataset a un datatable
            Dim dtbReporteCumplido As New DataTable
            dtbReporteCumplido = dsReportes.Tables("dtsReporteCumplido")

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Cumplido_Planilla_Despacho "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetalleCumplido As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetalleCumplido.Fill(dsReportes, "dtsReporteDetalleCumplido")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetalleCumplido As New DataTable
            dtbReporteDetalleCumplido = dsReportes.Tables("dtsReporteDetalleCumplido")




            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReporteCumplido", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsReporteDetalleCumplido", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReporteCumplido.Dispose()
            dtsReporteDetalleCumplido.Dispose()
            dtsReporteDetalleCumplido.Dispose()
            dtbReporteDetalleCumplido.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Manifiestos :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try



    End Sub


    Public Sub Configurar_Reporte_Liquidacion_Planilla_Despacho()
        If Val(Request.QueryString("Masivo")) > 0 Then
            Configurar_Reporte_Liquidacion_Planilla_Despacho_Masivo()
        Else
            Dim strConsulta As String = ""
            Dim strConsultaDetalle As String = ""
            Dim strConsultaConcepto As String = ""
            Try
                'Definir variables
                Dim strPathRepo As String = Server.MapPath("Formatos/Despachos/Liquidacion/repLiquidacionPlanillaDespacho_" & Me.Request.QueryString("Prefijo") & ".rdlc")
                Dim dsReportes As New DataSet

                Dim raizAplicacion = HttpRuntime.AppDomainAppPath

                'Armar la consulta
                strConsulta = "EXEC gsp_Reporte_Liquidacion_Planilla_Despacho "

                strConsulta += Val(Request.QueryString("Empresa")) & ","
                If Not IsNothing(Request.QueryString("Numero")) Then
                    strConsulta += Request.QueryString("Numero")
                Else
                    strConsulta += "NULL"
                End If
                'Guardar el resultado del sp en un dataset
                Dim dtsReporteLiquidacionPlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
                dtsReporteLiquidacionPlanillaDespacho.Fill(dsReportes, "dtsReporteLiquidacionPlanillaDespacho")

                'Pasar el dataset a un datatable
                Dim dtbReporteLiquidacionPlanillaDespacho As New DataTable
                dtbReporteLiquidacionPlanillaDespacho = dsReportes.Tables("dtsReporteLiquidacionPlanillaDespacho")

                'Armar la consulta
                strConsulta = "EXEC gsp_Reporte_Detalle_Liquidacion_Planilla_Despacho "

                strConsulta += Val(Request.QueryString("Empresa")) & ","
                If Not IsNothing(Request.QueryString("Numero")) Then
                    strConsulta += Request.QueryString("Numero")
                Else
                    strConsulta += "NULL"
                End If
                'Guardar el resultado del sp en un dataset
                Dim dtsReporteDetalleLiquidacionPlanilla As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
                dtsReporteDetalleLiquidacionPlanilla.Fill(dsReportes, "dtsReporteDetalleLiquidacionPlanilla")

                'Pasar el dataset a un datatable
                Dim dtbReporteDetalleLiquidacionPlanilla As New DataTable
                dtbReporteDetalleLiquidacionPlanilla = dsReportes.Tables("dtsReporteDetalleLiquidacionPlanilla")

                'Armar la consulta
                strConsulta = "EXEC gsp_Reporte_Detalle_Impuestos_Liquidacion_Planilla_Despacho "

                strConsulta += Val(Request.QueryString("Empresa")) & ","
                If Not IsNothing(Request.QueryString("Numero")) Then
                    strConsulta += Request.QueryString("Numero")
                Else
                    strConsulta += "NULL"
                End If
                'Guardar el resultado del sp en un dataset
                Dim dtsReporteDetalleImpuestosLiquidacionPlanilla As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
                dtsReporteDetalleImpuestosLiquidacionPlanilla.Fill(dsReportes, "dtsDetalleImpuestos")

                'Pasar el dataset a un datatable
                Dim dtbReporteDetalleImpuestosLiquidacionPlanilla As New DataTable
                dtbReporteDetalleImpuestosLiquidacionPlanilla = dsReportes.Tables("dtsDetalleImpuestos")


                ' Configurar el objeto ReportViewer
                ReportViewer1.LocalReport.EnableExternalImages = True
                ReportViewer1.LocalReport.ReportPath = (strPathRepo)

                ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
                Dim rds As New ReportDataSource("dtsReporteLiquidacionPlanillaDespacho", dsReportes.Tables(0))
                ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

                ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
                Dim rd As New ReportDataSource("dtsReporteDetalleLiquidacionPlanilla", dsReportes.Tables(1))
                ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
                Dim rdi As New ReportDataSource("dtsDetalleImpuestos", dsReportes.Tables(2))
                ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

                ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
                ReportViewer1.LocalReport.DataSources.Add(rds)
                ReportViewer1.LocalReport.DataSources.Add(rd)
                ReportViewer1.LocalReport.DataSources.Add(rdi)
                ReportViewer1.LocalReport.Refresh()


                'Mostrar el ReportViewer como un pdf en el mismo navegador
                If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                    Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                    Response.ContentType = "application/pdf"
                    Response.BinaryWrite(pdf)
                    Response.End()
                    Exit Sub
                End If
                If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                    'Mostrar el ReportViewer en formato excel 
                    Dim warnings As Warning() = Nothing
                    Dim streamids As String() = Nothing
                    Dim mimeType As String = String.Empty
                    Dim encoding As String = String.Empty
                    Dim filenameExtension As String = String.Empty
                    Dim deviceInfo As String = String.Empty
                    Dim streams As String() = Nothing


                    Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                    filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                    Response.ClearHeaders()
                    Response.Clear()
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                    Response.ContentType = mimeType
                    Response.BinaryWrite(excel)
                    Response.Flush()
                    Response.End()
                    Exit Sub
                End If
                dtbReporteLiquidacionPlanillaDespacho.Dispose()
                dtbReporteDetalleLiquidacionPlanilla.Dispose()
            Catch ex As Exception
                Me.lblMensajeError.Text = "Error en Configurar_Reporte_Manifiestos :" & ex.Message.ToString & " | " & ex.InnerException.ToString
            End Try

        End If
    End Sub

    Public Sub Configurar_Reporte_Liquidacion_Planilla_Despacho_Masivo()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Se declaran las variables unitlizadas para la union de pdfs

            Dim document As New Document() 'documento temportan donde se ira almacendando el pdf'
            Dim MS As New MemoryStream() 'Variable para el manejo del archivo, lo cual permitira mostrarlo'
            Dim pdfCopy As New PdfCopy(document, MS) 'Variable propia de la libreria iTextSharp que facilita el manejo de los archivos pdfs
            document.Open()
            Dim Numeros As String = Request.QueryString("ListaNumeros") 'Se asigna la cadena de numeros enviado desde el JS'
            Dim ListaNumero As String() = Numeros.Split(",") 'Se crea un listado de los numeros usando el separador de ","'

            'Inicio Ciclo'
            For Each Numero In ListaNumero 'Se genera un ciclo dependiendo de la cantidad de objetos en el listado'
                Dim pdf As Byte() = Generar_reporte_liquidacion(Val(Request.QueryString("Empresa")), Numero) 'Funcion comun que genera el reporte a travez de report viwer y retorna el pdf en bytes'
                Dim PdfReader As New PdfReader(pdf) 'Lee y convierte los bytes en una varible pdf de la librera otextsharp'
                Dim n = PdfReader.NumberOfPages 'Lee el numero de paginas del arichivo, de tal modo genera un nuevo ciclo para adicionarle al documento temporal cada pagina del reporte'
                Dim Page = 0
                While Page < n
                    pdfCopy.AddPage(pdfCopy.GetImportedPage(PdfReader, System.Threading.Interlocked.Increment(Page))) 'Adiciona las paginas del reporte generado al documento temporal creado al inicio'
                End While
                pdfCopy.FreeReader(PdfReader)
                pdfCopy.Flush()
            Next
            'Fin Ciclo'
            document.Close()
            Response.ContentType = "application/pdf"
            Response.BinaryWrite(MS.GetBuffer()) 'Muestra el pdf en la pantalla'

        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Manifiestos :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try

    End Sub

    Public Function Generar_reporte_liquidacion(Empresa As String, Numero As String) As Byte()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        'Definir variables
        Dim strPathRepo As String = Server.MapPath("Formatos/Despachos/Liquidacion/repLiquidacionPlanillaDespacho_" & Me.Request.QueryString("Prefijo") & ".rdlc")
        Dim dsReportes As New DataSet

        Dim raizAplicacion = HttpRuntime.AppDomainAppPath

        'Armar la consulta
        strConsulta = "EXEC gsp_Reporte_Liquidacion_Planilla_Despacho "

        strConsulta += Empresa & ","
        strConsulta += Numero
        'Guardar el resultado del sp en un dataset
        Dim dtsReporteLiquidacionPlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsReporteLiquidacionPlanillaDespacho.Fill(dsReportes, "dtsReporteLiquidacionPlanillaDespacho")

        'Pasar el dataset a un datatable
        Dim dtbReporteLiquidacionPlanillaDespacho As New DataTable
        dtbReporteLiquidacionPlanillaDespacho = dsReportes.Tables("dtsReporteLiquidacionPlanillaDespacho")

        'Armar la consulta
        strConsulta = "EXEC gsp_Reporte_Detalle_Liquidacion_Planilla_Despacho "

        strConsulta += Empresa & ","
        strConsulta += Numero
        'Guardar el resultado del sp en un dataset
        Dim dtsReporteDetalleLiquidacionPlanilla As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsReporteDetalleLiquidacionPlanilla.Fill(dsReportes, "dtsReporteDetalleLiquidacionPlanilla")

        'Pasar el dataset a un datatable
        Dim dtbReporteDetalleLiquidacionPlanilla As New DataTable
        dtbReporteDetalleLiquidacionPlanilla = dsReportes.Tables("dtsReporteDetalleLiquidacionPlanilla")

        'Armar la consulta
        strConsulta = "EXEC gsp_Reporte_Detalle_Impuestos_Liquidacion_Planilla_Despacho "

        strConsulta += Empresa & ","
        strConsulta += Numero
        'Guardar el resultado del sp en un dataset
        Dim dtsReporteDetalleImpuestosLiquidacionPlanilla As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsReporteDetalleImpuestosLiquidacionPlanilla.Fill(dsReportes, "dtsDetalleImpuestos")

        'Pasar el dataset a un datatable
        Dim dtbReporteDetalleImpuestosLiquidacionPlanilla As New DataTable
        dtbReporteDetalleImpuestosLiquidacionPlanilla = dsReportes.Tables("dtsDetalleImpuestos")


        ' Configurar el objeto ReportViewer
        ReportViewer1.LocalReport.EnableExternalImages = True
        ReportViewer1.LocalReport.ReportPath = (strPathRepo)

        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        Dim rds As New ReportDataSource("dtsReporteLiquidacionPlanillaDespacho", dsReportes.Tables(0))
        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        Dim rd As New ReportDataSource("dtsReporteDetalleLiquidacionPlanilla", dsReportes.Tables(1))
        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        Dim rdi As New ReportDataSource("dtsDetalleImpuestos", dsReportes.Tables(2))
        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

        ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.DataSources.Add(rd)
        ReportViewer1.LocalReport.DataSources.Add(rdi)
        ReportViewer1.LocalReport.Refresh()

        Dim pdf = Me.SerializarReportePdf(ReportViewer1)

        Return pdf

    End Function


    Public Shared Function ImageToByte(ByVal img As System.Drawing.Image) As Byte()
        Dim converter As ImageConverter = New ImageConverter
        Return CType(converter.ConvertTo(img, GetType(System.Byte())), Byte())
    End Function



    Public Function EncodeQRIMG(ByVal Data As String) As Bitmap
        Try
            Me.Encoder = New QRCodeEncoder
            'Dim QRCodeImage As Bitmap
            Dim ModuleSize As Integer = 4
            Dim QuietZone As Integer = 16
            Dim EciValue As Integer = -1
            Me.Encoder.ErrorCorrection = ErrorCorrection.M
            Me.Encoder.ModuleSize = ModuleSize
            Me.Encoder.QuietZone = QuietZone
            Me.Encoder.ECIAssignValue = EciValue
            ' single segment
            ' encode data
            Me.Encoder.Encode(Data)
            ' create bitmap
            Me.QRCodeImage = Me.Encoder.CreateQRCodeBitmap
            Return Me.QRCodeImage
        Catch Ex As Exception
            Return Nothing
        End Try
    End Function



    Public Sub Configurar_Reporte_Manifiestos()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Despachos/Manifiesto/repManifiesto_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Encabezado_Manifiesto "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Usuario")) Then
                strConsulta += Request.QueryString("Usuario")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteManifiesto As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteManifiesto.Fill(dsReportes, "dtsEncabezadoReporteManifiesto")
            Dim dtbReporteManifiesto As New DataTable



            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Manifiesto "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleReporteManifiesto As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleReporteManifiesto.Fill(dsReportes, "dtsDetalleReporteManifiesto")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetalleManifiesto As New DataTable
            dtbReporteDetalleManifiesto = dsReportes.Tables("dtsDetalleReporteManifiesto")
            Dim cadenaQR = ""
            Try
                cadenaQR = "MEC:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Numero_Manifiesto_Electronico").ToString() + vbNewLine
                cadenaQR += "Fecha:" + Format(Date.Parse(dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("FechaExpedicion").ToString()), "yyyy/MM/dd") + vbNewLine
                cadenaQR += "Placa:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("PlacaVehiculo").ToString() + vbNewLine
                cadenaQR += "Remolque:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("PlacaSemiremolque").ToString() + vbNewLine
                cadenaQR += "Config:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Configuracion_Vehiculo").ToString() + vbNewLine
                cadenaQR += "Orig:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("CiudadOrigen").ToString() + vbNewLine
                cadenaQR += "Dest:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("CiudadDestino").ToString() + vbNewLine
                cadenaQR += "Mercancia:" + dsReportes.Tables("dtsDetalleReporteManifiesto").Rows(0)("NombreProducto").ToString() + vbNewLine
                cadenaQR += "Conductor:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("IdentificacionConductor").ToString() + vbNewLine
                cadenaQR += "Empresa:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("NombreEmpresa").ToString() + vbNewLine
                If dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Observacion_Manifiesto_Electronico").ToString() <> String.Empty Then
                    cadenaQR += "Obs:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Observacion_Manifiesto_Electronico").ToString() + vbNewLine
                End If
                cadenaQR += "Seguro:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Codigo_Seguridad_Manifiesto_Electronico").ToString()

            Catch ex As Exception
                cadenaQR = "MEC:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Numero_Manifiesto_Electronico").ToString() + vbNewLine
                cadenaQR += "Fecha:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("FechaExpedicion").ToString() + vbNewLine
                cadenaQR += "Placa:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("PlacaVehiculo").ToString() + vbNewLine
                cadenaQR += "Remolque:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("PlacaSemiremolque").ToString() + vbNewLine
                cadenaQR += "Config: Orig:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("CiudadOrigen").ToString() + vbNewLine
                cadenaQR += "Dest:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("CiudadDestino").ToString() + vbNewLine
                cadenaQR += "Conductor:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("TitutlarConductor").ToString() + vbNewLine
                cadenaQR += "Empresa:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("NombreEmpresa").ToString() + vbNewLine
                If dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Observacion_Manifiesto_Electronico").ToString() <> String.Empty Then
                    cadenaQR += "Obs:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Observacion_Manifiesto_Electronico").ToString() + vbNewLine
                End If
                cadenaQR += " Seguro:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Codigo_Seguridad_Manifiesto_Electronico").ToString() + vbNewLine

            End Try
            dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("QRManifiesto") = ImageToByte(EncodeQRIMG(cadenaQR))
            'Dim a = QR_Generator.Encode(cadenaQR.le)
            'Pasar el dataset a un datatable
            dtbReporteManifiesto = dsReportes.Tables("dtsEncabezadoReporteManifiesto")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsEncabezadoReporteManifiesto", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsDetalleReporteManifiesto", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsDetalleReporteManifiesto.Dispose()
            dtbReporteDetalleManifiesto.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Manifiestos :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try



    End Sub

    Public Sub Configurar_Reporte_Plan_Ruta()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Despachos/PlanRuta/repPlanRuta_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Encabezado_Manifiesto "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteManifiesto As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteManifiesto.Fill(dsReportes, "dtsEncabezadoManifiesto")

            'Pasar el dataset a un datatable
            Dim dtbReporteManifiesto As New DataTable
            dtbReporteManifiesto = dsReportes.Tables("dtsEncabezadoManifiesto")


            'Armar la consulta  
            strConsulta = "EXEC gsp_puntos_plan_ruta "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            'Posicion Incial
            strConsulta += "1,"
            'Posicion Final
            strConsulta += "6"


            'Guardar el resultado del sp en un dataset
            Dim dtsPlanRuta As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsPlanRuta.Fill(dsReportes, "dtsPlanRuta")

            'Pasar el dataset a un datatable
            Dim dtbPlanRuta As New DataTable
            dtbPlanRuta = dsReportes.Tables("dtsPlanRuta")



            'Armar la consulta
            strConsulta = "EXEC gsp_puntos_plan_ruta "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            'Posicion Incial
            strConsulta += "7,"
            'Posicion Final
            strConsulta += "12"

            'Guardar el resultado del sp en un dataset
            Dim dtsPlanRuta2 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsPlanRuta2.Fill(dsReportes, "dtsPlanRuta2")

            'Pasar el dataset a un datatable
            Dim dtbPlanRuta2 As New DataTable
            dtbPlanRuta2 = dsReportes.Tables("dtsPlanRuta2")



            'Armar la consulta
            strConsulta = "EXEC gsp_puntos_plan_ruta "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            'Posicion Incial
            strConsulta += "13,"
            'Posicion Final
            strConsulta += "18"

            'Guardar el resultado del sp en un dataset
            Dim dtsPlanRuta3 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsPlanRuta3.Fill(dsReportes, "dtsPlanRuta3")

            'Pasar el dataset a un datatable
            Dim dtbPlanRuta3 As New DataTable
            dtbPlanRuta3 = dsReportes.Tables("dtsPlanRuta3")



            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsEncabezadoManifiesto", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rdRuta1 As New ReportDataSource("dtsPlanRuta", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rdRuta2 As New ReportDataSource("dtsPlanRuta2", dsReportes.Tables(2))
            ''El prime parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rdRuta3 As New ReportDataSource("dtsPlanRuta3", dsReportes.Tables(3))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rdRuta1)
            ReportViewer1.LocalReport.DataSources.Add(rdRuta2)
            ReportViewer1.LocalReport.DataSources.Add(rdRuta3)

            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If

        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Manifiestos :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try



    End Sub

    Public Sub Configurar_Reporte_Planilla_Despacho()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Despachos/PlanillaDespachos/repPlanillaDespacho_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gps_Reporte_Planilla_Despacho "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReportePlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReportePlanillaDespacho.Fill(dsReportes, "dtsReportePlanillaDespacho")

            'Pasar el dataset a un datatable
            Dim dtbReportePlanillaDespacho As New DataTable
            dtbReportePlanillaDespacho = dsReportes.Tables("dtsReportePlanillaDespacho")

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Planilla_Despacho "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetallePlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetallePlanillaDespacho.Fill(dsReportes, "dtsReporteDetallePlanillaDespacho")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetallePlanillaDespacho As New DataTable
            dtbReporteDetallePlanillaDespacho = dsReportes.Tables("dtsReporteDetallePlanillaDespacho")



            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Distribucion_Remesa "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDistribucionRemesasPlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDistribucionRemesasPlanillaDespacho.Fill(dsReportes, "dtsReporteDistribucionRemesasPlanillaDespacho")

            'Pasar el dataset a un datatable
            Dim dtbReporteDistribucionRemesasPlanillaDespacho As New DataTable
            dtbReporteDistribucionRemesasPlanillaDespacho = dsReportes.Tables("dtsReporteDistribucionRemesasPlanillaDespacho")

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_detalle_conductores_planilla_guia_paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetalleConductoresPlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetalleConductoresPlanillaDespacho.Fill(dsReportes, "dtsReporteDetalleConductoresPlanillaDespacho")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetalleConductoresPlanillaDespacho As New DataTable
            dtbReporteDetalleConductoresPlanillaDespacho = dsReportes.Tables("dtsReporteDetalleConductoresPlanillaDespacho")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReportePlanillaDespacho", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsReporteDetallePlanillaDespacho", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rdx As New ReportDataSource("dtsReporteDistribucionRemesasPlanillaDespacho", dsReportes.Tables(2))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rdc As New ReportDataSource("dtsReporteDetalleConductoresPlanillaDespacho", dsReportes.Tables(3))

            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.DataSources.Add(rdx)
            ReportViewer1.LocalReport.DataSources.Add(rdc)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReportePlanillaDespacho.Dispose()
            dtbReportePlanillaDespacho.Dispose()
            dtsReporteDetallePlanillaDespacho.Dispose()
            dtbReporteDetallePlanillaDespacho.Dispose()
            dtsReporteDistribucionRemesasPlanillaDespacho.Dispose()
            dtbReporteDistribucionRemesasPlanillaDespacho.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Manifiestos :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try


    End Sub

    Public Sub Configurar_Reporte_Planilla_Despachos_Otras_Empresas()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/FPropia/PlanillasDespachosOtrasEmpresas/repPlanillaDespacho_OE_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gps_Reporte_Planilla_Despacho_Otras_Empresas "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReportePlanillaDespachoOtrasEmpresas As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReportePlanillaDespachoOtrasEmpresas.Fill(dsReportes, "dtsReportePlanillaDespachoOtrasEmpresas")

            'Pasar el dataset a un datatable
            Dim dtbReportePlanillaDespachoOtrasEmpresas As New DataTable
            dtbReportePlanillaDespachoOtrasEmpresas = dsReportes.Tables("dtsReportePlanillaDespachoOtrasEmpresas")




            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReportePlanillaDespachoOtrasEmpresas", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)





            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)



            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReportePlanillaDespachoOtrasEmpresas.Dispose()
            dtsReportePlanillaDespachoOtrasEmpresas.Dispose()

        Catch ex As Exception
            '  Me.lblMensajeError.Text = "Error en Configurar_Reporte_Otras_Empresas :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try


    End Sub

    Public Sub Configurar_Reporte_Planilla_Poliza()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Dim strPathRepo As String = ""
        Try
            'Definir variables
            strPathRepo = Server.MapPath("Formatos/Despachos/PlanillaDespachos/repPlanillaPoliza_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_reporte_poliza_planillas "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReportePlanillaPoliza As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReportePlanillaPoliza.Fill(dsReportes, "dtsReportePlanillaPoliza")

            'Pasar el dataset a un datatable
            Dim dtbReportePlanillaPoliza As New DataTable
            dtbReportePlanillaPoliza = dsReportes.Tables("dtsReportePlanillaPoliza")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReportePlanillaPoliza", dsReportes.Tables(0))

            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)

            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReportePlanillaPoliza.Dispose()
            dtbReportePlanillaPoliza.Dispose()

        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Planilla_Poliza :" & ex.Message.ToString & " | " & ex.InnerException.ToString & vbNewLine &
            "Acceso ruta reporte: " & strPathRepo & vbNewLine &
            "Query SQL Ejecutada: " & strConsulta
        End Try


    End Sub

    Public Sub Configurar_Reporte_Legalizacion_Gastos_Conductro()

        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/FlotaPropia/LegalizacionGastos/repLegalizacionGastos_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            strConsulta = "EXEC gps_reporte_encabezado_legalizacion_gastos "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL, "
            End If

            If Not IsNothing(Request.QueryString("usuario")) Then
                strConsulta += Request.QueryString("usuario")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsEncabezadoLegalizacion As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEncabezadoLegalizacion.Fill(dsReportes, "dtsReporteEncabezadoLegalizacionGastos")

            'Pasar el dataset a un datatable
            Dim dtdEncabezadoLegalizacion As New DataTable
            dtdEncabezadoLegalizacion = dsReportes.Tables("dtsReporteEncabezadoLegalizacionGastos")

            'Armar la consulta
            strConsulta = "EXEC gps_reporte_detalle_legalizacion_gastos "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleLegalizacion As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleLegalizacion.Fill(dsReportes, "dtsReporteDetalleLegalizacionGastos")

            'Pasar el dataset a un datatable
            Dim dtdDetalleLegalizacion As New DataTable
            dtdDetalleLegalizacion = dsReportes.Tables("dtsReporteDetalleLegalizacionGastos")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReporteEncabezadoLegalizacionGastos", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsReporteDetalleLegalizacionGastos", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If

            dtsEncabezadoLegalizacion.Dispose()
            dtdEncabezadoLegalizacion.Dispose()
            dtsDetalleLegalizacion.Dispose()
            dtdDetalleLegalizacion.Dispose()

        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Planilla_Entregada :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try



    End Sub

    Public Sub Configurar_Reporte_Planilla_Entregada()

        Dim strConsulta As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Paqueteria/PlanillaEntrega/repPlanillaEntrega_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            strConsulta = "EXEC gps_reporte_encabezado_planilla_entrega "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL, "
            End If

            If Not IsNothing(Request.QueryString("usuario")) Then
                strConsulta += Request.QueryString("usuario")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsEncabezadoReporteplanillaentrega As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEncabezadoReporteplanillaentrega.Fill(dsReportes, "dtsReportePlanillaEntrega")

            'Pasar el dataset a un datatable
            Dim dtdEncabezadoReporteplanillaentrega As New DataTable
            dtdEncabezadoReporteplanillaentrega = dsReportes.Tables("dtsReportePlanillaEntrega")

            'Armar la consulta
            strConsulta = "EXEC gps_reporte_detalle_guia_planilla_entrega "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleReporteplanillaentrega As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleReporteplanillaentrega.Fill(dsReportes, "dtsReporteDetalleGuiaPlanillaEntrega")

            'Pasar el dataset a un datatable
            Dim dtdDetalleReporteplanillaentrega As New DataTable
            dtdDetalleReporteplanillaentrega = dsReportes.Tables("dtsReporteDetalleGuiaPlanillaEntrega")

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Impuestos_Planilla_Entrega "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleImpuestosplanillaentrega As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleImpuestosplanillaentrega.Fill(dsReportes, "dtsReporteDetalleImpuestosPlanillaEntrega")

            'Pasar el dataset a un datatable
            Dim dtdDetalleImpuestosReporteplanillaentrega As New DataTable
            dtdDetalleImpuestosReporteplanillaentrega = dsReportes.Tables("dtsReporteDetalleImpuestosPlanillaEntrega")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReportePlanillaEntrega", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsReporteDetalleGuiaPlanillaEntrega", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rdx As New ReportDataSource("dtsReporteDetalleImpuestosPlanillaEntrega", dsReportes.Tables(2))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.DataSources.Add(rdx)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If

            dtsEncabezadoReporteplanillaentrega.Dispose()
            dtdEncabezadoReporteplanillaentrega.Dispose()
            dtsDetalleReporteplanillaentrega.Dispose()
            dtdDetalleReporteplanillaentrega.Dispose()
            dtsDetalleImpuestosplanillaentrega.Dispose()
            dtdDetalleImpuestosReporteplanillaentrega.Dispose()

        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Planilla_Entregada :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try



    End Sub

    Public Sub Configurar_Reporte_Planilla_Resumen_Cargue()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Paqueteria/ResumenCargue/RepPLResumenCargue_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Encabezado_Planilla_Resumen_Cargue "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReportePlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReportePlanillaDespacho.Fill(dsReportes, "dtsReporteEncabezadoPlanillaResumenCargue")

            'Pasar el dataset a un datatable
            Dim dtbReportePlanillaDespacho As New DataTable
            dtbReportePlanillaDespacho = dsReportes.Tables("dtsReporteEncabezadoPlanillaResumenCargue")

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Planilla_Resumen_Cargue "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetallePlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetallePlanillaDespacho.Fill(dsReportes, "dtsReporteDetallePlanillaResumenCargue")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetallePlanillaDespacho As New DataTable
            dtbReporteDetallePlanillaDespacho = dsReportes.Tables("dtsReporteDetallePlanillaResumenCargue")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReporteEncabezadoPlanillaResumenCargue", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsReporteDetallePlanillaResumenCargue", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)

            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReportePlanillaDespacho.Dispose()
            dtbReportePlanillaDespacho.Dispose()
            dtsReporteDetallePlanillaDespacho.Dispose()
            dtbReporteDetallePlanillaDespacho.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Planilla_Resumen_Cargue :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try


    End Sub

    Public Function Configurar_Reporte_Facturas()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Dim pdf As Byte()
        Dim strPathRepo As String
        Try
            'Definir variables
            If Me.Request.QueryString("TipoFactura") = TIPO_FACTURA.FACTURA Then
                strPathRepo = Server.MapPath("Formatos/Despachos/Factura/repFactura_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Else
                strPathRepo = Server.MapPath("Formatos/Despachos/Factura/repFacturaOtros_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            End If

            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Encabezado_Factura "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteFactura As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteFactura.Fill(dsReportes, "dtsReporteEncabezadoFactura")

            'Pasar el dataset a un datatable
            Dim dtbReporteFactura As New DataTable
            dtbReporteFactura = dsReportes.Tables("dtsReporteEncabezadoFactura")

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Factura "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleReporteFactura As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleReporteFactura.Fill(dsReportes, "dtsReporteDetalleFactura")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetalleFactura As New DataTable
            dtbReporteDetalleFactura = dsReportes.Tables("dtsReporteDetalleFactura")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Conceptos_Factura "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetalleConceptosFactura As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetalleConceptosFactura.Fill(dsReportes, "dtsReporteDetalleConceptosFactura")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetalleConceptosFactura As New DataTable
            dtbReporteDetalleConceptosFactura = dsReportes.Tables("dtsReporteDetalleConceptosFactura")

            'Armar la consulta
            strConsulta = "EXEC gsp_reporte_impuestos_factura_remesas "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteImpuestosFacturaRemesas As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteImpuestosFacturaRemesas.Fill(dsReportes, "dtsReporteImpuestosFacturaRemesas")

            'Pasar el dataset a un datatable
            Dim dtbReporteImpuestosFacturaRemesas As New DataTable
            dtbReporteImpuestosFacturaRemesas = dsReportes.Tables("dtsReporteImpuestosFacturaRemesas")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReporteEncabezadoFactura", dsReportes.Tables(0))
            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsReporteDetalleFactura", dsReportes.Tables(1))
            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rdx As New ReportDataSource("dtsReporteDetalleConceptosFactura", dsReportes.Tables(2))
            Dim rdimpuestosfactura As New ReportDataSource("dtsReporteImpuestosFacturaRemesas", dsReportes.Tables(3))


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.DataSources.Add(rdx)
            ReportViewer1.LocalReport.DataSources.Add(rdimpuestosfactura)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                pdf = Me.SerializarReportePdf(ReportViewer1)
                If Not IsNothing(Request.QueryString("TipoExportar")) Then
                    If Request.QueryString("TipoExportar") = "Electronic" Then
                        'reFactByte.Value = Convert.ToBase64String(pdf)
                        Dim strInserta As String = ""
                        strInserta = "EXEC gsp_insertar_Documento_Factura "
                        strInserta += Val(Request.QueryString("Empresa")) & ","
                        strInserta += Val(Request.QueryString("Numero")) & ","
                        strInserta += "'" & TIPO_ARCHIVO_GENERA & "',"
                        strInserta += "'" & Convert.ToBase64String(pdf) & "',"
                        strInserta += "'" & DESCRIP_ARCHIVO_FACTURA_GENERA & "'"
                        Using conexion = New DatabaseFactoryConcrete(objGeneral.strCadenaDeConexionSQLDocumentos).CreateDataBaseFactory()
                            conexion.CreateConnection()
                            Dim resultado As IDataReader = conexion.ExecuteReader(strInserta)
                            While resultado.Read
                                Dim numeroFactura As Integer = resultado.Item("ENFA_Numero")
                            End While
                            conexion.CloseConnection()
                        End Using
                        Exit Function
                    End If
                End If
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Function
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Function
            End If
            dtsReporteFactura.Dispose()
            dtbReporteFactura.Dispose()
            dtsDetalleReporteFactura.Dispose()
            dtbReporteDetalleFactura.Dispose()
            dtsReporteDetalleConceptosFactura.Dispose()
            dtbReporteDetalleConceptosFactura.Dispose()
            dtsReporteImpuestosFacturaRemesas.Dispose()
            dtbReporteImpuestosFacturaRemesas.Dispose()

        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Facturas :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try

    End Function

    Public Function Configurar_Reporte_Notas_Credito()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Dim pdf As Byte()
        Dim strPathRepo As String
        Try
            strPathRepo = Server.MapPath("Formatos/Despachos/Notas/repNota_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Encabezado_Nota_Credito "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteNotaCredito As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteNotaCredito.Fill(dsReportes, "dtsReporteNotaCredito")

            'Pasar el dataset a un datatable
            Dim dtbReporteNotaCredito As New DataTable
            dtbReporteNotaCredito = dsReportes.Tables("dtsReporteNotaCredito")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)


            'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("dtsReporteNotaCredito", CType(dtbReporteNotaCredito, DataTable)))
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                pdf = Me.SerializarReportePdf(ReportViewer1)
                If Not IsNothing(Request.QueryString("TipoExportar")) Then
                    If Request.QueryString("TipoExportar") = "Electronic" Then
                        'reFactByte.Value = Convert.ToBase64String(pdf)
                        Dim strInserta As String = ""
                        strInserta = "EXEC gsp_insertar_Documento_Nota_Factura "
                        strInserta += Val(Request.QueryString("Empresa")) & ","
                        strInserta += Val(Request.QueryString("Numero")) & ","
                        strInserta += "'" & TIPO_ARCHIVO_GENERA & "',"
                        strInserta += "'" & Convert.ToBase64String(pdf) & "',"
                        strInserta += "'" & DESCRIP_ARCHIVO_NOTA_GENERA & "'"
                        Using conexion = New DatabaseFactoryConcrete(objGeneral.strCadenaDeConexionSQLDocumentos).CreateDataBaseFactory()
                            conexion.CreateConnection()
                            conexion.ExecuteReader(strInserta)
                            conexion.CloseConnection()
                        End Using
                        Exit Function
                    End If
                End If
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Function
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Function
            End If
            dtsReporteNotaCredito.Dispose()
            dtbReporteNotaCredito.Dispose()


        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Nota :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try

    End Function

    Public Sub Configurar_Reporte_Planilla_Recolecciones()

        Dim strConsulta As String = ""

        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Paqueteria/PlanillaRecoleccion/RepPlanillaRecolecciones_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath


            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Encabezado_Planilla_Recolecciones "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL, "
            End If

            If Not IsNothing(Request.QueryString("usuario")) Then
                strConsulta += Request.QueryString("usuario")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsReportePlanilla As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReportePlanilla.Fill(dsReportes, "dtsPlanillaRecolecciones")

            'Pasar el dataset a un datatable
            Dim dtbReportePlanilla As New DataTable
            dtbReportePlanilla = dsReportes.Tables("dtsPlanillaRecolecciones")


            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Planilla_Recolecciones "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleRecolecciones As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleRecolecciones.Fill(dsReportes, "dtsReporteDetallePlanillaRecolecciones")

            'Pasar el dataset a un datatable
            Dim dtbDetalleRecolecciones As New DataTable
            dtbDetalleRecolecciones = dsReportes.Tables("dtsReporteDetallePlanillaRecolecciones")


            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Planilla_Guias_Recolecciones "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleGuias.Fill(dsReportes, "dtsReporteDetallePlanillaGuiasRecolecciones")

            'Pasar el dataset a un datatable
            Dim dtbDetalleGuias As New DataTable
            dtbDetalleGuias = dsReportes.Tables("dtsReporteDetallePlanillaGuiasRecolecciones")


            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Impuestos_Planilla_Recolecciones "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleImpuestosPlanilla As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleImpuestosPlanilla.Fill(dsReportes, "dtsReporteDetalleImpuestosPlanillaRecolecciones")

            'Pasar el dataset a un datatable
            Dim dtbDetalleImpuestosPlanilla As New DataTable
            dtbDetalleImpuestosPlanilla = dsReportes.Tables("dtsReporteDetalleImpuestosPlanillaRecolecciones")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)


            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsPlanillaRecolecciones", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rdr As New ReportDataSource("dtsReporteDetallePlanillaRecolecciones", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rdg As New ReportDataSource("dtsReporteDetallePlanillaGuiasRecolecciones", dsReportes.Tables(2))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rda As New ReportDataSource("dtsReporteDetalleImpuestosPlanillaRecolecciones", dsReportes.Tables(3))

            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rdr)
            ReportViewer1.LocalReport.DataSources.Add(rdg)
            ReportViewer1.LocalReport.DataSources.Add(rda)
            ReportViewer1.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing

                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If

            dtsReportePlanilla.Dispose()
            dtbReportePlanilla.Dispose()
            dtsDetalleRecolecciones.Dispose()
            dtbDetalleRecolecciones.Dispose()
            dtsDetalleGuias.Dispose()
            dtbDetalleGuias.Dispose()
            dtsDetalleImpuestosPlanilla.Dispose()
            dtbDetalleImpuestosPlanilla.Dispose()

        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Planilla_Recolecciones :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try

    End Sub

    Public Sub Configurar_Reporte_Planilla_Despacho_Paqueteria()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Paqueteria/PlanillaDespachos/repPlanillaDespachoPaqueteria_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gps_Reporte_Planilla_Despacho_Paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReportePlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReportePlanillaDespacho.Fill(dsReportes, "dtsReportePlanillaDespachoPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbReportePlanillaDespacho As New DataTable
            dtbReportePlanillaDespacho = dsReportes.Tables("dtsReportePlanillaDespachoPaqueteria")

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_Detalle_Planilla_Despacho_Paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetallePlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetallePlanillaDespacho.Fill(dsReportes, "dtsReporteDetallePlanillaDespachoPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetallePlanillaDespacho As New DataTable
            dtbReporteDetallePlanillaDespacho = dsReportes.Tables("dtsReporteDetallePlanillaDespachoPaqueteria")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReportePlanillaDespachoPaqueteria", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsReporteDetallePlanillaDespachoPaqueteria", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReportePlanillaDespacho.Dispose()
            dtbReportePlanillaDespacho.Dispose()
            dtsReporteDetallePlanillaDespacho.Dispose()
            dtbReporteDetallePlanillaDespacho.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Planilla_Despacho_Paqueteria :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try


    End Sub

    Public Sub Configurar_Reporte_Inspeccion_Preoperacional()
        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Inspecciones/repInspeccion_Preoperacional_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezado_inspeccion_preoperacional "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ""
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsReportePlanillaDespachoOtrasEmpresas As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReportePlanillaDespachoOtrasEmpresas.Fill(dsReportes, "dtsReporteInspeccionPreoperacional")

            'Pasar el dataset a un datatable
            Dim dtbReportePlanillaDespachoOtrasEmpresas As New DataTable
            dtbReportePlanillaDespachoOtrasEmpresas = dsReportes.Tables("dtsReporteInspeccionPreoperacional")

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_detalle_inspeccion_preoperacional_reporte "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ""
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleItemsPlanillaDespachoOtrasEmpresas As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsDetalleItemsPlanillaDespachoOtrasEmpresas.Fill(dsReportes, "dtsDetalleItemsInspeccionPreoperacional")

            'Pasar el dataset a un datatable
            Dim dtbDetalleItemsPlanillaDespachoOtrasEmpresas As New DataTable
            dtbDetalleItemsPlanillaDespachoOtrasEmpresas = dsReportes.Tables("dtsDetalleItemsInspeccionPreoperacional")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReporteInspeccionPreoperacional", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rdt As New ReportDataSource("dtsDetalleItemsInspeccionPreoperacional", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)



            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rdt)



            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReportePlanillaDespachoOtrasEmpresas.Dispose()
            dtsReportePlanillaDespachoOtrasEmpresas.Dispose()

        Catch ex As Exception
            '  Me.lblMensajeError.Text = "Error en Configurar_Reporte_Otras_Empresas :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try
    End Sub

    Public Sub Configurar_Reporte_Vehiculos_Lista_Negra()
        Dim strConsulta As String = ""

        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Basico/Despachos/repVehiculosListaNegra.rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_vehiculos_lista_negra "

            strConsulta += Val(Request.QueryString("Empresa")) & ","

            If Not IsNothing(Request.QueryString("Codigo")) Then
                strConsulta += Request.QueryString("Codigo") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Placa")) Then
                strConsulta += Request.QueryString("Placa") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("CodigoAlterno")) Then
                strConsulta += Request.QueryString("CodigoAlterno") & ","
            Else
                strConsulta += "NULL,"
            End If
            strConsulta += "NULL,"
            strConsulta += "NULL,"
            If Not IsNothing(Request.QueryString("Estado")) Then
                strConsulta += Request.QueryString("Estado")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteVehiculosListaNegra As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteVehiculosListaNegra.Fill(dsReportes, "dtsReporteVehiculosListaNegra")

            'Pasar el dataset a un datatable
            Dim dtbReporteVehiculosListaNegra As New DataTable
            dtbReporteVehiculosListaNegra = dsReportes.Tables("dtsReporteVehiculosListaNegra")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReporteVehiculosListaNegra", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)




            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReporteVehiculosListaNegra.Dispose()
            dtbReporteVehiculosListaNegra.Dispose()

        Catch ex As Exception
            'Me.lblMensajeError.Text = "Error en Configurar_Reporte_Vehículos_Lista_Negra :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try
    End Sub

    Public Sub Configurar_Reporte_Formato_Gastos()
        Dim strConsulta As String = ""

        Try
            Dim strPathRepo As String = Server.MapPath("Formatos/Despachos/Gastos/repFormatoGastosConductor_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_conceptos_legalizacion_gastos "

            strConsulta += Val(Request.QueryString("Empresa")) & ","


            strConsulta += "NULL, NULL, 1"


            Dim dtsFormatoGastosConductor As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsFormatoGastosConductor.Fill(dsReportes, "dtsConceptosGastos")

            'Pasar el dataset a un datatable
            Dim dtbFormatoGastosConductor As New DataTable
            dtbFormatoGastosConductor = dsReportes.Tables("dtsConceptosGastos")

            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_planilla_despacho_formato_gastos "

            strConsulta += Val(Request.QueryString("Empresa")) & ","


            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If


            If Not IsNothing(Request.QueryString("Usuario")) Then
                strConsulta += Request.QueryString("Usuario")
            Else
                strConsulta += "NULL"
            End If

            Dim dtsEncabezadoPlanilla As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEncabezadoPlanilla.Fill(dsReportes, "dtsEncabezadoPlanilla")

            'Pasar el dataset a un datatable
            Dim dtbEncabezadoPlanilla As New DataTable
            dtbEncabezadoPlanilla = dsReportes.Tables("dtsEncabezadoPlanilla")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsConceptosGastos", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rep As New ReportDataSource("dtsEncabezadoPlanilla", dsReportes.Tables(1))



            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rep)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsFormatoGastosConductor.Dispose()
            dtbFormatoGastosConductor.Dispose()
        Catch ex As Exception

        End Try
    End Sub
#End Region
    Public Sub Configurar_Reporte_Plan_Mantenimiento()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""

        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Mantenimiento/repPlanMantenimiento.rdlc")
            Dim dsReportes As New DataSet
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC sp_reporte_encabezado_tarea_plan_mantenimiento "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & " "
            Else
                strConsulta += "NULl"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsEncabezadoPlanMantenimiento As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEncabezadoPlanMantenimiento.Fill(dsReportes, "dtsEncabezadoPlanMantenimiento")

            'Pasar el dataset a un datatable
            Dim dtbEncabezadoFacturas As New DataTable
            dtbEncabezadoFacturas = dsReportes.Tables("dtsEncabezadoPlanMantenimiento")


            'Armar la consulta del detalle
            strConsultaDetalle = "EXEC sp_reporte_detallado_tarea_plan_mantenimiento "

            strConsultaDetalle += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsultaDetalle += Request.QueryString("Numero") & " "
            Else
                strConsultaDetalle += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsDetallePlanMantenimiento As New SqlDataAdapter(strConsultaDetalle, objGeneral.ConexionSQL)
            dtsDetallePlanMantenimiento.Fill(dsReportes, "dtsDetallePlanMantenimiento")

            'Pasar el dataset a un datatable
            Dim dtdDetallePlanMantenimiento As New DataTable
            dtdDetallePlanMantenimiento = dsReportes.Tables("dtsDetallePlanMantenimiento")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            Dim rds As New ReportDataSource("dtsEncabezadoPlanMantenimiento", dsReportes.Tables(0))
            Dim rd As New ReportDataSource("dtsDetallePlanMantenimiento", dsReportes.Tables(1)) 'Dataset utilizado para poblar el reporte y la tabla que se utilizara
            ReportViewer1.LocalReport.DataSources.Clear()              'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)   'Agrego el ReportDatasource
            ReportViewer1.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Plan_Mantenimiento :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try


    End Sub


    Public Sub Configurar_Reporte_Tarea_Mantenimiento()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""

        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Mantenimiento/repTareaMantenimiento.rdlc")
            Dim dsReportes As New DataSet
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC sp_reporte_encabezado_tarea_equipo_mantenimiento "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ""
            Else
                strConsulta += "NULl"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsEncabezadoTareaMantenimiento As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEncabezadoTareaMantenimiento.Fill(dsReportes, "dtsEncabezadoTareaMantenimiento")

            'Pasar el dataset a un datatable
            Dim dtbEncabezadoTareaMantenimientos As New DataTable
            dtbEncabezadoTareaMantenimientos = dsReportes.Tables("dtsEncabezadoTareaMantenimiento")


            'Armar la consulta del detalle
            strConsultaDetalle = "EXEC sp_reporte_detalle_tarea_equipo_mantenimiento "

            strConsultaDetalle += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsultaDetalle += Request.QueryString("Numero") & " "
            Else
                strConsultaDetalle += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleTareaMantenimiento As New SqlDataAdapter(strConsultaDetalle, objGeneral.ConexionSQL)
            dtsDetalleTareaMantenimiento.Fill(dsReportes, "dtsDetalleTareaMantenimiento")

            'Pasar el dataset a un datatable
            Dim dtbDetalleTareaMantenimiento As New DataTable
            dtbDetalleTareaMantenimiento = dsReportes.Tables("dtsDetalleTareaMantenimiento")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            'rvpDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtb_Encabezado_Factura", CType(dtsEncabezadoFacturas, DataTable)))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            'rvpDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtb_Detalle_Factura", CType(dtsDetalleFacturas, DataTable)))


            Dim rds As New ReportDataSource("dtsEncabezadoTareaMantenimiento", dsReportes.Tables(0))
            Dim rd As New ReportDataSource("dtsDetalleTareaMantenimiento", dsReportes.Tables(1)) 'Dataset utilizado para poblar el reporte y la tabla que se utilizara
            ReportViewer1.LocalReport.DataSources.Clear()              'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)   'Agrego el ReportDatasource
            ReportViewer1.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Facturacion_Otros_Conceptos :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try


    End Sub


    Public Sub Configurar_Reporte_Orden_Trabajo()


        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""

        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Mantenimiento/repOrdenTrabajo.rdlc")
            Dim dsReportes As New DataSet
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC sp_reporte_encabezado_orden_trabajo_mantenimiento "

            strConsulta += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & " "
            Else
                strConsulta += "NULl"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsEncabezadoOrdenTrabajo As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsEncabezadoOrdenTrabajo.Fill(dsReportes, "dtsEncabezadoOrdenTrabajo")

            'Pasar el dataset a un datatable
            Dim dtbEncabezadoOrdenTrabajo As New DataTable
            dtbEncabezadoOrdenTrabajo = dsReportes.Tables("dtsEncabezadoOrdenTrabajo")


            'Armar la consulta del detalle
            strConsultaDetalle = "EXEC sp_reporte_Detallado_orden_trabajo_mantenimiento "

            strConsultaDetalle += Val(Request.QueryString("Empresa")) & ", "

            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsultaDetalle += Request.QueryString("Numero") & " "
            Else
                strConsultaDetalle += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsDetalleOrdenTrabajo As New SqlDataAdapter(strConsultaDetalle, objGeneral.ConexionSQL)
            dtsDetalleOrdenTrabajo.Fill(dsReportes, "dtsDetalleOrdenTrabajo")

            'Pasar el dataset a un datatable
            Dim dtbDetalleOrdenTrabajo As New DataTable
            dtbDetalleOrdenTrabajo = dsReportes.Tables("dtsDetalleOrdenTrabajo")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            'rvpDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtb_Encabezado_Factura", CType(dtsEncabezadoFacturas, DataTable)))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            'rvpDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtb_Detalle_Factura", CType(dtsDetalleFacturas, DataTable)))


            Dim rds As New ReportDataSource("dtsEncabezadoOrdenTrabajo", dsReportes.Tables(0))
            Dim rd As New ReportDataSource("dtsDetalleOrdenTrabajo", dsReportes.Tables(1)) 'Dataset utilizado para poblar el reporte y la tabla que se utilizara
            ReportViewer1.LocalReport.DataSources.Clear()              'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)   'Agrego el ReportDatasource
            ReportViewer1.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.objGeneral.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
            End If
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Facturacion_Otros_Conceptos :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try


    End Sub



    Public Sub Configurar_Reporte_Planilla_Paqueteria()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/PaqueteriaV2/repPlanillaPaqueteria1_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet
            Dim dsReportesDetalle As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gps_Reporte_Planilla_Despacho_Paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReportePlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReportePlanillaDespacho.Fill(dsReportes, "dtsReportePlanillaDespachoPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbReportePlanillaDespacho As New DataTable
            dtbReportePlanillaDespacho = dsReportes.Tables("dtsReportePlanillaDespachoPaqueteria")

            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_guias_planilla_paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetallePlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetallePlanillaDespacho.Fill(dsReportesDetalle, "dtsDetalleGuiasPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetallePlanillaDespacho As New DataTable
            dtbReporteDetallePlanillaDespacho = dsReportesDetalle.Tables("dtsDetalleGuiasPaqueteria")


            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_guias_recogidas_planilla_paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetalleGuiasRecogidas As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetalleGuiasRecogidas.Fill(dsReportesDetalle, "dtsDetalleGuiasRecogidasPlanillaPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetalleGuiasRecogidas As New DataTable
            dtbReporteDetalleGuiasRecogidas = dsReportesDetalle.Tables("dtsDetalleGuiasRecogidasPlanillaPaqueteria")


            'Armar la consulta
            strConsulta = "EXEC gsp_consultar_detalle_recolecciones_planilla_paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetalleRecolecciones As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetalleRecolecciones.Fill(dsReportesDetalle, "dtsDetalleRecolecciones")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetalleRecolecciones As New DataTable
            dtbReporteDetalleRecolecciones = dsReportesDetalle.Tables("dtsDetalleRecolecciones")
            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReportePlanillaDespachoPaqueteria", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsDetalleGuiasPaqueteria", dsReportesDetalle.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rt As New ReportDataSource("dtsDetalleGuiasRecogidasPlanillaPaqueteria", dsReportesDetalle.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim re As New ReportDataSource("dtsDetalleRecolecciones", dsReportesDetalle.Tables(2))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.DataSources.Add(rt)
            ReportViewer1.LocalReport.DataSources.Add(re)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReportePlanillaDespacho.Dispose()
            dtbReportePlanillaDespacho.Dispose()
            dtsReporteDetallePlanillaDespacho.Dispose()
            dtbReporteDetallePlanillaDespacho.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_Planilla_Paqueteria :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try


    End Sub

    Public Sub Configurar_Reporte_Solicitud_Anticipo()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/FlotaPropia/SolicitudesAnticipo/repSolicitudAnticipo_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet
            Dim dsReportesDetalle As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_encabezado_solicitud_anticipo "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ""
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsReportePlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReportePlanillaDespacho.Fill(dsReportes, "dtsReporteEncabezadoSolicitudAnticipo")

            'Pasar el dataset a un datatable
            Dim dtbReportePlanillaDespacho As New DataTable
            dtbReportePlanillaDespacho = dsReportes.Tables("dtsReporteEncabezadoSolicitudAnticipo")

            'Armar la consulta
            strConsulta = "EXEC gsp_obtener_detalle_solicitud_anticipo "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetallePlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetallePlanillaDespacho.Fill(dsReportesDetalle, "dtsDetalleSolicitudAnticipo")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetallePlanillaDespacho As New DataTable
            dtbReporteDetallePlanillaDespacho = dsReportesDetalle.Tables("dtsDetalleSolicitudAnticipo")



            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReporteEncabezadoSolicitudAnticipo", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rd As New ReportDataSource("dtsDetalleSolicitudAnticipo", dsReportesDetalle.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.DataSources.Add(rd)
            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReportePlanillaDespacho.Dispose()
            dtbReportePlanillaDespacho.Dispose()
            dtsReporteDetallePlanillaDespacho.Dispose()
            dtbReporteDetallePlanillaDespacho.Dispose()
        Catch ex As Exception
            Me.lblMensajeError.Text = "Error en Configurar_Reporte_SolicitudAnticipo :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try


    End Sub

    Public Sub Configurar_Reporte_Planilla_Despachos_Proveedores()

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/Proveedores/PlanillaDespachosProveedores/repPlanillaDespachoProveedores_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_Reporte_planilla_despacho_proveedores "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReportePlanillaDespachoProveedores As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReportePlanillaDespachoProveedores.Fill(dsReportes, "dtsReportePlanillaDespachoProveedores")

            'Pasar el dataset a un datatable
            Dim dtbReportePlanillaDespachoProveedores As New DataTable
            dtbReportePlanillaDespachoProveedores = dsReportes.Tables("dtsReportePlanillaDespachoProveedores")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds As New ReportDataSource("dtsReportePlanillaDespachoProveedores", dsReportes.Tables(0))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)





            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds)



            ReportViewer1.LocalReport.Refresh()


            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then

                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing


                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)

                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReportePlanillaDespachoProveedores.Dispose()
            dtsReportePlanillaDespachoProveedores.Dispose()

        Catch ex As Exception
            '  Me.lblMensajeError.Text = "Error en Configurar_Reporte_Otras_Empresas :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try


    End Sub

    Public Sub Configurar_Reporte_Legalizacion_Guias_Recaudo()
        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Try
            'Definir variables
            Dim strPathRepo As String = Server.MapPath("Formatos/PaqueteriaV2/RepLegalizacionGuiaRecaudo_" & Me.Request.QueryString("Prefijo") & ".rdlc")
            Dim dsReportes As New DataSet

            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_reporte_encabezado_legalizacion_guias_paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReportePlanillaDespachoProveedores As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReportePlanillaDespachoProveedores.Fill(dsReportes, "dtsEncabezadoLegalizarGuiasPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbReportePlanillaDespachoProveedores As New DataTable
            dtbReportePlanillaDespachoProveedores = dsReportes.Tables("dtsEncabezadoLegalizarGuiasPaqueteria")

            'Armar la consulta
            strConsulta = "EXEC gsp_reporte_detalle_contra_entregas_legalizacion_guias_paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetalleContraEntrega As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetalleContraEntrega.Fill(dsReportes, "dtsDetalleContraEntregaLegalizarGuias")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetalleContraEntrega As New DataTable
            dtbReporteDetalleContraEntrega = dsReportes.Tables("dtsDetalleContraEntregaLegalizarGuias")

            'Armar la consulta
            strConsulta = "EXEC gsp_reporte_detalle_contado_legalizacion_guias_paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetalleContado As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetalleContado.Fill(dsReportes, "dtsDetalleContadoLegalizarGuias")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetalleContado As New DataTable
            dtbReporteDetalleContado = dsReportes.Tables("dtsDetalleContadoLegalizarGuias")

            'Armar la consulta
            strConsulta = "EXEC gsp_reporte_detalle_ingresos_terceros_legalizacion_guias_paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero")
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetalleIngresoTer As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetalleIngresoTer.Fill(dsReportes, "dtdDetalleIngresoTercerosLegalizarGuias")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetalleIngresoTer As New DataTable
            dtbReporteDetalleIngresoTer = dsReportes.Tables("dtdDetalleIngresoTercerosLegalizarGuias")

            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds1 As New ReportDataSource("dtsEncabezadoLegalizarGuiasPaqueteria", dsReportes.Tables(0))
            Dim rds2 As New ReportDataSource("dtsDetalleContraEntregaLegalizarGuias", dsReportes.Tables(1))
            Dim rds3 As New ReportDataSource("dtsDetalleContadoLegalizarGuias", dsReportes.Tables(2))
            Dim rds4 As New ReportDataSource("dtdDetalleIngresoTercerosLegalizarGuias", dsReportes.Tables(3))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds1)
            ReportViewer1.LocalReport.DataSources.Add(rds2)
            ReportViewer1.LocalReport.DataSources.Add(rds3)
            ReportViewer1.LocalReport.DataSources.Add(rds4)
            ReportViewer1.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then
                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing
                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)
                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If

        Catch ex As Exception
            '  Me.lblMensajeError.Text = "Error en Configurar_Reporte_Otras_Empresas :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try
    End Sub

    Public Sub Configurar_Reporte_Legalizacion_Guias_Entregas_Documentos()
        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        Dim intEntregaCartera As Integer = 0
        Dim intEntregaArchivo As Integer = 0
        Try
            'Definir variables
            Dim strPathRepo As String
            If Me.Request.QueryString("NombRepo") = NOMBRE_REPORTE_LEGALIZACION_GUIAS_ENTREGA_CARTERA Then
                strPathRepo = Server.MapPath("Formatos/PaqueteriaV2/RepEntregaCartera_" & Me.Request.QueryString("Prefijo") & ".rdlc")
                intEntregaCartera = 1
            Else
                strPathRepo = Server.MapPath("Formatos/PaqueteriaV2/RepEntregaArchivo_" & Me.Request.QueryString("Prefijo") & ".rdlc")
                intEntregaArchivo = 1
            End If

            Dim dsReportes As New DataSet
            Dim raizAplicacion = HttpRuntime.AppDomainAppPath

            'Armar la consulta
            strConsulta = "EXEC gsp_reporte_encabezado_legalizacion_guias_paqueteria "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If
            If Not IsNothing(Request.QueryString("USUA_Imprime")) Then
                strConsulta += Request.QueryString("USUA_Imprime")
            Else
                strConsulta += "NULL"
            End If
            'Guardar el resultado del sp en un dataset
            Dim dtsReportePlanillaDespachoProveedores As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReportePlanillaDespachoProveedores.Fill(dsReportes, "dtsEncabezadoLegalizarGuiasPaqueteria")

            'Pasar el dataset a un datatable
            Dim dtbReportePlanillaDespachoProveedores As New DataTable
            dtbReportePlanillaDespachoProveedores = dsReportes.Tables("dtsEncabezadoLegalizarGuiasPaqueteria")

            'Armar la consulta
            strConsulta = "EXEC gsp_reporte_entrega_documentos_legalizacion_guias "

            strConsulta += Val(Request.QueryString("Empresa")) & ","
            If Not IsNothing(Request.QueryString("Numero")) Then
                strConsulta += Request.QueryString("Numero") & ","
            Else
                strConsulta += "NULL,"
            End If

            If intEntregaCartera > 0 Then
                strConsulta += "1,"
            Else
                strConsulta += "NULL,"
            End If

            If intEntregaArchivo > 0 Then
                strConsulta += "1"
            Else
                strConsulta += "NULL"
            End If

            'Guardar el resultado del sp en un dataset
            Dim dtsReporteDetalleContraEntrega As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
            dtsReporteDetalleContraEntrega.Fill(dsReportes, "dtsEntregaDocumentos")

            'Pasar el dataset a un datatable
            Dim dtbReporteDetalleContraEntrega As New DataTable
            dtbReporteDetalleContraEntrega = dsReportes.Tables("dtsEntregaDocumentos")


            ' Configurar el objeto ReportViewer
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.ReportPath = (strPathRepo)

            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
            Dim rds1 As New ReportDataSource("dtsEncabezadoLegalizarGuiasPaqueteria", dsReportes.Tables(0))
            Dim rds2 As New ReportDataSource("dtsEntregaDocumentos", dsReportes.Tables(1))
            ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

            ReportViewer1.LocalReport.DataSources.Clear() 'limpio la fuente de datos
            ReportViewer1.LocalReport.DataSources.Add(rds1)
            ReportViewer1.LocalReport.DataSources.Add(rds2)
            ReportViewer1.LocalReport.Refresh()

            'Mostrar el ReportViewer como un pdf en el mismo navegador
            If Not IsNothing(Request.QueryString("OpcionExportarPdf")) Then
                Dim pdf = Me.SerializarReportePdf(ReportViewer1)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(pdf)
                Response.End()
                Exit Sub
            End If
            If Not IsNothing(Request.QueryString("OpcionExportarExcel")) Then
                'Mostrar el ReportViewer en formato excel 
                Dim warnings As Warning() = Nothing
                Dim streamids As String() = Nothing
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim filenameExtension As String = String.Empty
                Dim deviceInfo As String = String.Empty
                Dim streams As String() = Nothing
                Dim excel As Byte() = ReportViewer1.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)
                filenameExtension = String.Format("{0}.{1}", "ExportToExcel", "xls")
                Response.ClearHeaders()
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameExtension)
                Response.ContentType = mimeType
                Response.BinaryWrite(excel)
                Response.Flush()
                Response.End()
                Exit Sub
            End If
            dtsReportePlanillaDespachoProveedores.Dispose()
            dtsReportePlanillaDespachoProveedores.Dispose()

        Catch ex As Exception
            '  Me.lblMensajeError.Text = "Error en Configurar_Reporte_Otras_Empresas :" & ex.Message.ToString & " | " & ex.InnerException.ToString
        End Try
    End Sub
End Class