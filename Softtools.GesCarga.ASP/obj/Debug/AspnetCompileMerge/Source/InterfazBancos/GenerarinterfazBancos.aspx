﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GenerarinterfazBancos.aspx.vb" Inherits="Softtools.GesCarga.ASP.GenerarinterfazBancos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Generar Interfaz Contables</title>
    <link href="../Recursos/Estilos/Css/Bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        #form1 {
            width: 800px;
        }
    </style>
</head>
<body>

    <asp:Literal ID="ltrFiltro" runat="server"></asp:Literal>

    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="scmAjaSiteMaster" runat="server" AsyncPostBackTimeout="1205000"></asp:ScriptManager>
        </div>
    </form>
    <asp:Label ID="lblMensajeError" runat="server" ForeColor="Red" EnableViewState="false"></asp:Label>
</body>
</html>
