import { Component, OnInit, Input, NgModule } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
// Components
import { comCiudades } from './comCiudades';
// Services
import { serCiudades } from '../../../services/serCiudades';
// Interfaces
import { Filtro, Respuesta, EstadoRegistro, Ciudad, Departamento } from '../../../interfaces/ifaGeneral';

@Component({
  selector: 'app-detalle-ciudades',
  templateUrl: './comGestionarCiudades.html'
})
export class comGestionarCiudades implements OnInit{
  @Input() ciudad: Ciudad;
  form: FormGroup;

  public lstEstados: EstadoRegistro[] = [{ Codigo: 1, Nombre: "ACTIVO" }, { Codigo: 0, Nombre: "INACTIVO" }];
  public numEmpresa = 6; objCiudad: Ciudad; lstMensajesError: string[];
  public numCodiCiud: number; objDepartamento: Departamento;

  constructor(public http: HttpClient, protected serCiudades: serCiudades) {
    this.lstEstados = [{ Codigo: 1, Nombre: "ACTIVO" }, { Codigo: 0, Nombre: "INACTIVO" }];
    this.FormControles();
  }

  ngOnInit() {}

  funGuardar() {
    try {

      //if (typeof this.ciudad === "undefined") {
      //  // Opcion Nuevo
      //  this.numCodiCiud = 0;
      //} else {
      //  // Opcion Modificar
      //  this.numCodiCiud = this.ciudad.Codigo;
      //  //this.objDepartamento = this.ciudad.Departamento;
      //}

      this.objCiudad = {
        CodigoEmpresa: this.numEmpresa,
        Codigo: this.ciudad.Codigo,
        CodigoCiudad: this.ciudad.Codigo,
        CodigoAlterno: this.form.controls.txtCodiDane.value,
        Nombre: this.form.controls.txtNombre.value,
        Departamento: this.ciudad.Departamento,
        Estado: this.form.controls.cmbEstado.value,
        UsuarioCrea: 0,
        UsuarioModifica: 0
      };

      this.serCiudades.apiGuardar(this.objCiudad).subscribe(resp => {
        if (resp.ProcesoExitoso === true) {
          this.lstMensajesError.push("Guardó");
        } else {
          this.lstMensajesError.push(resp.MensajeOperacion);
        }
      }
      );
    }
    catch (error) {
      this.lstMensajesError.push(String(error));
    }
  }

  private FormControles() {
    this.form = new FormGroup({
      txtNombre: new FormControl('', []),
      txtCodiDane: new FormControl('', []),
      txtDepartamento: new FormControl('', []),
      cmbEstado: new FormControl('ACTIVO', []),
    });
  }

}
