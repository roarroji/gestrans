import { Component, OnInit } from '@angular/core';
// Interface
import { Marker } from '../../interfaces/ifaGeneral';
// Google
import { GoogleMapsModule, GoogleMap } from '@angular/google-maps';

@Component({
  selector: 'app-mapa',
  templateUrl: './comMapa.html',
  styleUrls: ['./comMapa.css']
})
export class comMapa implements OnInit {
  // Variables
  public map;
  public origen = { lat: 4.658383, lng: -74.093940 };
  public destino = { lat: 4.676802, lng: -74.048255 };
  public directionService = new google.maps.DirectionsService();
  public directionDisplay = new google.maps.DirectionsRenderer();
  
  public wayPoints: google.maps.DirectionsWaypoint[] = [
    {
      location: new google.maps.LatLng(4.667945, -74.099647), //  Jardin Botanico
      stopover: true,
    },
    {
      location: new google.maps.LatLng(4.676802, -74.048255), // Parque 93
      stopover: true,
    },
    {
      location: new google.maps.LatLng(4.655428, -74.109498), // Maloka
      stopover: true,
    },
  ];

  constructor() {

  }

  ngOnInit() {
    this.initMap();
  }

  private initMap() {
    const mapEle: HTMLElement = document.getElementById('map');
    const indicatorEle: HTMLElement = document.getElementById('indicator');

    // Crear mapa
    this.map = new google.maps.Map(mapEle, {
      center: this.origen,
      zoom: 13
    });

    this.directionDisplay.setMap(this.map);
    this.directionDisplay.setPanel(indicatorEle);

    google.maps.event.addListenerOnce(this.map, 'idle', () => {

      this.showMarker();
           
      //this.calculateRoute();
    });
  }

  public showMarker() {

    const marker = {
      position: {
        lat: 4.658383,
        lng: -74.093940,
      },
      title: 'Parque Simon Bolivar'
    };
    this.addMarker(marker);

    const marker1 = {
      position: {
        lat: 4.667945,
        lng: -74.099647,
      },
      title: 'Jardín Botánico'
    };
    this.addMarker(marker1);

    const marker2 = {
      position: {
        lat: 4.676802,
        lng: -74.048255,
      },
      title: 'Parque 93'
    };
    this.addMarker(marker2);

    const marker3 = {
      position: {
        lat: 4.655428,
        lng: -74.109498,
      },
      title: 'Maloka'
    };
    this.addMarker(marker3);

  }

  private addMarker(marker: Marker) {
    return new google.maps.Marker({
    position: marker.position,
    map: this.map,
    title: marker.title
    });
  }

  public calculateRoute() {
    this.directionService.route({
      origin: this.origen,
      destination: this.destino,
      waypoints: this.wayPoints,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING,
    }, (response, status) => {
      if (status === google.maps.DirectionsStatus.OK) {
        this.directionDisplay.setDirections(response);
      } else {
        alert('No encontro una ruta' + status);
      }
    });
  }

}
 



