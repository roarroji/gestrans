export interface Filtro {
  CodigoEmpresa: number;
  Codigo: number;
  CodigoAlterno: string;
  Nombre: string;
  Estado: number;
  Pagina: number;
  RegistrosPagina: number;
}

export interface Respuesta {
  Cantidad_Detalles: number;
  Datos; number;
  MensajeOperacion: string;
  Numero: number;
  ProcesoExitoso: boolean;
}

export interface EstadoRegistro {
  Codigo: number;
  Nombre: string;
}

export interface Ciudad {
  CodigoEmpresa: number;
  Codigo: number;
  CodigoCiudad: number;
  CodigoAlterno: string;
  Nombre: string;
  Departamento: Departamento;
  Estado: number;
  UsuarioCrea: number;
  UsuarioModifica: number;
}

export interface Departamento {
  CodigoEmpresa: number;
  Pais: Pais;
  Codigo: number;
  CodigoAlterno: string;
  Nombre: string;
  Estado: number;
  UsuarioCrea: Usuario;
  UsuarioModifica: number;
}

export interface Pais {
  Codigo: number;
  Nombre: string;
}

export interface Usuario {
  Codigo: number;
  Nombre: string;
}

export interface WayPoint {
  location: {
    lat: number,
    lng: number,
  };
  stopover: boolean;
}

export interface Marker {
  position: {
    lat: number,
    lng: number,
  };
  title: string;
}

