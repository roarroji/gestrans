import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// Interfaces
import { Respuesta, Filtro } from '../interfaces/ifaGeneral';
// Observable
import { Observable } from 'rxjs';
// Constantes
const UrlApi = "http://localhost:50395/api/v1/Paises/Consultar";
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
}

@Injectable({
  providedIn: 'root'
})
export class serPaises {

  constructor(protected http: HttpClient) { }

  public apiConsultar(objFiltro: Filtro): Observable<Respuesta> {

    return this.http.post<Respuesta>(UrlApi,
      {
        CodigoEmpresa: objFiltro.CodigoEmpresa,
        Codigo: objFiltro.Codigo,
        CodigoAlterno: objFiltro.CodigoAlterno,
        Nombre: objFiltro.Nombre,
        Estado: objFiltro.Estado,
        Pagina: objFiltro.Pagina,
        RegistrosPagina: objFiltro.RegistrosPagina
      }, httpOptions);

  }

}
