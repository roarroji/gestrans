﻿Imports System.Data.SqlClient

''' <summary>
''' Clase <see cref="DataBaseSqlServer"/> que hereda de la clase <see cref="DataBaseFactory"/>
''' </summary>
Public Class DataBaseSqlServer : Inherits DataBaseFactory

    ''' <summary>
    ''' The data base connection
    ''' </summary>
    Protected DataBaseConnection As SqlConnection

    ''' <summary>
    ''' The data base command
    ''' </summary>
    Protected DataBaseCommand As SqlCommand

    ''' <summary>
    ''' The data base Transaction connection
    ''' </summary>
    Protected DataBaseTransaction As SqlTransaction

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DataBaseFactory" /> class.
    ''' </summary>
    ''' <param name="connection">The connection.</param>
    Public Sub New(ByVal connection As String)
        MyBase.New(connection)
    End Sub

    ''' <summary>
    ''' Creates the connection.
    ''' </summary>
    Public Overrides Sub CreateConnection()
        Try
            DataBaseConnection = New SqlConnection(ConnectionStringDataBase)
            DataBaseCommand = DataBaseConnection.CreateCommand()
            DataBaseCommand.Connection = DataBaseConnection
            DataBaseConnection.Open()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Creates the connection transactional.
    ''' </summary>
    Public Overrides Sub BeginTransact()
        DataBaseTransaction = DataBaseConnection.BeginTransaction("Gesphone")
    End Sub

    ''' <summary>
    ''' Closes the connection.
    ''' </summary>
    Public Overrides Sub CommitTransact()
        DataBaseTransaction.Commit()
    End Sub

    ''' <summary>
    ''' Closes the connection.
    ''' </summary>
    Public Overrides Sub RollbackTransact()
        DataBaseTransaction.Rollback()
    End Sub

    ''' <summary>
    ''' Closes the connection.
    ''' </summary>
    Public Overrides Sub CloseConnection()

        If (Not DataBaseConnection Is Nothing And DataBaseConnection.State = ConnectionState.Open) Then

            DataBaseConnection.Close()
            DataBaseConnection.Dispose()

            If Not (DataBaseCommand Is Nothing) Then
                DataBaseCommand.Dispose()
            End If
        End If

    End Sub

    ''' <summary>
    ''' Executes the reader.
    ''' </summary>
    ''' <param name="commandText">The command text.</param>
    ''' <returns>IDataReader.</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")>
    Public Overrides Function ExecuteReader(commandText As String) As IDataReader
        DataBaseCommand.CommandType = CommandType.Text
        DataBaseCommand.CommandText = commandText
        Return DataBaseCommand.ExecuteReader()
    End Function

    ''' <summary>
    ''' Executes the store procedure.
    ''' </summary>
    ''' <param name="procedureName">Name of the procedure.</param>
    ''' <returns>IDataReader.</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")>
    Public Overrides Function ExecuteReaderStoreProcedure(ByVal procedureName As String) As IDataReader
        DataBaseCommand.CommandType = CommandType.StoredProcedure
        DataBaseCommand.CommandText = procedureName
        Return DataBaseCommand.ExecuteReader()
    End Function

    ''' <summary>
    ''' Executes the store procedure.
    ''' </summary>
    ''' <param name="procedureName">Name of the procedure.</param>
    ''' <returns>IDataReader.</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")>
    Public Overrides Async Function ExecuteReaderStoreProcedureAsync(ByVal procedureName As String) As Global.System.Threading.Tasks.Task(Of Global.System.Data.IDataReader)
        DataBaseCommand.CommandType = CommandType.StoredProcedure
        DataBaseCommand.CommandText = procedureName
        Return Await DataBaseCommand.ExecuteReaderAsync()
    End Function

    ''' <summary>
    ''' Executes the non query store procedure.
    ''' </summary>
    ''' <param name="procedureName">Name of the procedure.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")>
    Public Overrides Function ExecuteNonQueryStoreProcedureReturnResult(ByVal procedureName As String) As Integer
        DataBaseCommand.CommandType = CommandType.StoredProcedure
        DataBaseCommand.CommandText = procedureName
        Return DataBaseCommand.ExecuteNonQuery()
    End Function

    ''' <summary>
    ''' Executes the non query store procedure.
    ''' </summary>
    ''' <param name="procedureName">Name of the procedure.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")>
    Public Overrides Sub ExecuteNonQueryStoreProcedure(ByVal procedureName As String)
        DataBaseCommand.CommandType = CommandType.StoredProcedure
        DataBaseCommand.CommandText = procedureName
        DataBaseCommand.ExecuteNonQuery()
    End Sub

    ''' <summary>
    ''' Executes the non query store procedure.
    ''' </summary>
    ''' <param name="procedureName">Name of the procedure.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")>
    Public Overrides Sub ExecuteNonQueryStoreProcedureTransact(ByVal procedureName As String)
        DataBaseCommand.CommandType = CommandType.StoredProcedure
        DataBaseCommand.CommandText = procedureName
        DataBaseCommand.Transaction = DataBaseTransaction
        DataBaseCommand.ExecuteNonQuery()
    End Sub

    ''' <summary>
    ''' Executes the non query.
    ''' </summary>
    ''' <param name="commandText">The command text.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")>
    Public Overrides Sub ExecuteNonQuery(commandText As String)
        DataBaseCommand = DataBaseConnection.CreateCommand()
        DataBaseCommand.CommandType = CommandType.Text
        DataBaseCommand.CommandText = commandText
        DataBaseCommand.ExecuteNonQuery()
    End Sub

    ''' <summary>
    ''' Agregar parametros a un objeto sql para ejecutar una insrucción en la base de datos
    ''' </summary>
    ''' <param name="nombre">Nombre del parametro</param>
    ''' <param name="valor">Valor del parametro</param>
    Public Overrides Sub AgregarParametroObjetoSQL(ByVal nombre As String, ByVal valor As Object)
        DataBaseCommand.Parameters.AddWithValue(nombre, valor)
        DataBaseCommand.Parameters(nombre).IsNullable = True
    End Sub

    ''' <summary>
    ''' Agregar parametros sql para ejecutar una insrucción en la base de datos
    ''' </summary>
    ''' <param name="nombre">Nombre del parametro</param>
    ''' <param name="valor">Valor del parametro</param>
    ''' <param name="tipo">Tipo de dato del parametro</param>
    Public Overrides Sub AgregarParametroSQL(ByVal nombre As String, ByVal valor As Object, Optional ByVal tipo As SqlDbType = Nothing)
        If tipo = Nothing Then
            DataBaseCommand.Parameters.Add(nombre, SqlDbType.VarChar).Value = valor
        Else
            DataBaseCommand.Parameters.Add(nombre, tipo).Value = valor
        End If
        DataBaseCommand.Parameters(nombre).IsNullable = True
    End Sub

    ''' <summary>
    ''' Executes the store procedure.
    ''' </summary>
    ''' <param name="procedureName">Name of the procedure.</param>
    ''' <returns>IDataReader.</returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")>
    Public Overrides Function ExecuteReaderStoreProcedureTransact(procedureName As String) As IDataReader
        DataBaseCommand.CommandType = CommandType.StoredProcedure
        DataBaseCommand.CommandText = procedureName
        DataBaseCommand.Transaction = DataBaseTransaction
        Return DataBaseCommand.ExecuteReader()
    End Function
    ''' <summary>
    ''' Limpia los parametros del dbCommand
    ''' </summary>
    ''' <remarks></remarks>
    Public Overrides Sub CleanParameters()
        DataBaseCommand.Parameters.Clear()
    End Sub

End Class
