﻿Print 'Catalogo 12 Tipo Contrato'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 12
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 12
GO
DELETE Catalogos WHERE Codigo = 12
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,12,'TICE','Tipo Contrato Empleado',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,12,1200,UPPER('(No Aplica)'),1,GETDATE()),
(1,12,1201,UPPER('Indefinido'),1,GETDATE()),
(1,12,1202,UPPER('Integral'),1,GETDATE()),
(1,12,1203,UPPER('Servicios'),1,GETDATE()),
(1,12,1204,UPPER('Termino Fijo'),1,GETDATE())
GO

-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,12,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,12,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 