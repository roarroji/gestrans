﻿Print 'Catalogo 15 Forma Pago Clientes'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 15
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 15
GO
DELETE Catalogos WHERE Codigo = 15
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,15,'FPCL ','Forma Pago Clientes',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,15,1500,UPPER('(No Aplica)'),1,GETDATE()),
(1,15,1501,UPPER('Anticipo'),1,GETDATE()),
(1,15,1502,UPPER('Contado'),1,GETDATE()),
(1,15,1503,UPPER('Crédito'),1,GETDATE())

GO

-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,15,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,15,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 