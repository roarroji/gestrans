﻿Print 'Catalogo 16 Regimen Tributario'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 16
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 16
GO
DELETE Catalogos WHERE Codigo = 16
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,16,'RETR','Regimen Tributario',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,16,1600,UPPER('(No Aplica)'),1,GETDATE()),
(1,16,1601,UPPER('Común'),1,GETDATE()),
(1,16,1602,UPPER('Simplificado'),1,GETDATE()),
(1,16,1603,UPPER('Autoretenedores'),1,GETDATE()),
(1,16,1604,UPPER('Excento'),1,GETDATE()),
(1,16,1605,UPPER('Sin Animo de Lucro'),1,GETDATE())
GO

-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,16,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,16,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 