﻿Print 'Catalogo 18 categoria licencia conducción'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 18
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 18
GO
DELETE Catalogos WHERE Codigo = 18
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,18,'CALC','Categorías Licencias de Conducción',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,18,1800,UPPER('(No Aplica)'),1,GETDATE()),
(1,18,1801,'A1',1,GETDATE()),
(1,18,1802,'A2',1,GETDATE()),
(1,18,1803,'B1',1,GETDATE()),
(1,18,1804,'B2',1,GETDATE()),
(1,18,1805,'B3',1,GETDATE()),
(1,18,1806,'C1',1,GETDATE()),
(1,18,1807,'C2',1,GETDATE()),
(1,18,1808,'C3',1,GETDATE())
GO


-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,18,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,18,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 