﻿Print 'Catalogo 19 Tipo Sangre'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 19
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 19
GO
DELETE Catalogos WHERE Codigo = 19
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,19,'TISA','Tipo de Sangre',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,19,1900,UPPER('(No Aplica)'),1,GETDATE()),
(1,19,1901,'O+',1,GETDATE()),
(1,19,1902,'O-',1,GETDATE()),
(1,19,1903,'A-',1,GETDATE()),
(1,19,1904,'A+',1,GETDATE()),
(1,19,1905,'B-',1,GETDATE()),
(1,19,1906,'B+',1,GETDATE()),
(1,19,1907,'AB-',1,GETDATE()),
(1,19,1908,'AB+',1,GETDATE())
GO


-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,19,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,19,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 