﻿PRINT 'Estado Enturnamiento Despacho - 190 - ESED'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 190
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 190
GO
DELETE Catalogos WHERE Codigo = 190
GO
INSERT INTO Catalogos (EMPR_Codigo, Codigo, Nombre_Corto, Nombre, Actualizable, Numero_Columnas, Numero_Campos_Llave)
SELECT Codigo, 190, 'ESED','Estado Enturnamiento Despacho', 0, 2, 1 FROM Empresas
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO Valor_Catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 190, 19001, 'PENDIENTE', '', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO Valor_Catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 190, 19002, 'ETURNADO', '', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO Valor_Catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 190, 19003, 'CANCELADO', '', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO Valor_Catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 190, 19004, 'CERRADO', '', '', '', 1, 1, GETDATE() FROM Empresas
GO
