﻿PRINT 'Catalogo Tipo Validación Cupo - 192 - TIVC'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 192
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 192
GO
DELETE Catalogos WHERE Codigo = 192
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos(EMPR_Codigo, Codigo, Nombre_Corto, Nombre, Actualizable, Numero_Columnas, Numero_Campos_Llave)
SELECT Codigo, 192, 'TIVC','Tipo Validación Cupo', 0, 2, 1 FROM Empresas
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 192, 19201, 'NO VALIDAR', '', '', '', 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 192, 19202, 'CUPO GENERAL', '', '', '', 1, GETDATE() FROM Empresas
GO
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 192, 19203, 'CUPO POR OFICINA', '', '', '', 1, GETDATE() FROM Empresas
GO