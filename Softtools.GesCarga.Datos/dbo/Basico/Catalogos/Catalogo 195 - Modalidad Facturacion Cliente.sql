﻿INSERT INTO Catalogos VALUES (4, 195, 'MFPC', 'Modalidad Facturación Peso Cliente', 0, 2, 1)
GO
INSERT INTO Configuracion_Catalogos VALUES(4, 195, 1, 'Codigo', 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0)
GO
INSERT INTO Configuracion_Catalogos VALUES(4, 195, 2, 'Nombre', 1, 1, 2, 1, 0, 0, 0, 0, 0, 1, 0)
GO
---------------------------------------------------------------------

INSERT INTO Valor_Catalogos (EMPR_Codigo, Codigo, CATA_Codigo,Campo1, Estado, USUA_Codigo_Crea, Fecha_Crea)
VALUES (4, 19501, 195, 'Peso Origen', 1, 0, GETDATE())
GO
INSERT INTO Valor_Catalogos (EMPR_Codigo, Codigo, CATA_Codigo,Campo1, Estado, USUA_Codigo_Crea, Fecha_Crea)
VALUES (4, 19502, 195, 'Peso Destino', 1, 0, GETDATE())
GO
INSERT INTO Valor_Catalogos (EMPR_Codigo, Codigo, CATA_Codigo,Campo1, Estado, USUA_Codigo_Crea, Fecha_Crea)
VALUES (4, 19503, 195, 'Reporte Cliente', 1, 0, GETDATE())
GO
INSERT INTO Valor_Catalogos (EMPR_Codigo, Codigo, CATA_Codigo,Campo1, Estado, USUA_Codigo_Crea, Fecha_Crea)
VALUES (4, 19504, 195, 'Menor Peso', 1, 0, GETDATE())
GO
INSERT INTO Valor_Catalogos (EMPR_Codigo, Codigo, CATA_Codigo,Campo1, Estado, USUA_Codigo_Crea, Fecha_Crea)
VALUES (4, 19505, 195, 'Mayor Peso', 1, 0, GETDATE())
GO
---------------------------------------------------------
Print 'V_Modalidad_Facturacion_Peso_Cliente'
GO
DROP VIEW V_Modalidad_Facturacion_Peso_Cliente
GO
CREATE VIEW V_Modalidad_Facturacion_Peso_Cliente
AS
SELECT EMPR_Codigo, Codigo, CATA_Codigo, Campo1 As Nombre
FROM Valor_Catalogos
WHERE CATA_Codigo = 195
GO