﻿Print 'Catalogo 21 Tipo Dueño Vehiculo'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 21
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 21
GO
DELETE Catalogos WHERE Codigo = 21
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,21,'TIDV','Tipo Dueño Vehiculo',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,21,2100,UPPER('(No Aplica)'),1,GETDATE()),
(1,21,2101,UPPER('Tercero'),1,GETDATE()),
(1,21,2102,UPPER('Propio'),1,GETDATE()),
(1,21,2103,UPPER('Socios'),1,GETDATE())
GO

-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,21,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,21,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 