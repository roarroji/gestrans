﻿Print 'Catalogo 22 Tipo Vehiculo'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 22
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 22
GO
DELETE Catalogos WHERE Codigo = 22
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,22,'TIVE','Tipo Vehiculo',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,22,2200,UPPER('(No Aplica)'),1,GETDATE()),
(1,22,2201,UPPER('CUATRO MANOS'),1,GETDATE()),
(1,22,2202,UPPER('DOBLETROQUE'),1,GETDATE()),
(1,22,2203,UPPER('MINI MULA'),1,GETDATE()),
(1,22,2204,UPPER('SENCILLO'),1,GETDATE()),
(1,22,2205,UPPER('TRACTOMULA 2 TROQUES'),1,GETDATE()),
(1,22,2206,UPPER('TRACTOMULA 3 TROQUES'),1,GETDATE()),
(1,22,2207,UPPER('TURBO'),1,GETDATE())
GO

-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,22,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,22,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 