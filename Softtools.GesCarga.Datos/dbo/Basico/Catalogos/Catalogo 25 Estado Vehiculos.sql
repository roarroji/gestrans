﻿Print 'Catalogo 25 Estado Vehiculos'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 25
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 25
GO
DELETE Catalogos WHERE Codigo = 25
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,25,'ESVE','Estado Vehículos',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,25,2500,UPPER('(NO APLICA)'),1,GETDATE()),
(1,25,2501,UPPER('ACTIVO'),1,GETDATE()),
(1,25,2502,UPPER('INACTIVO'),1,GETDATE()),
(1,25,2503,UPPER('BLOQUEADO'),1,GETDATE())

GO

-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,25,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,25,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 
----------------------------------------------------------------------------------------------------------