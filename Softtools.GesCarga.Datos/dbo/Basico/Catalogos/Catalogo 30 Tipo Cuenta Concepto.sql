﻿----------------------------------------------------------------------------------------------------------
Print 'Catalogo 30 Tipo Cuenta Concepto Contable'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 30
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 30
GO
DELETE Catalogos WHERE Codigo = 30
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,30,'TCCC','Tipo Cuenta Concepto Contable',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,30,3000,'(NO APLICA)','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,30,3001,'Retención','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,30,3002,'Gasto','',1,GETDATE())
GO
-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,30,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0)
GO 
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,30,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 
----------------------------------------------------------------------------------------------------------