﻿----------------------------------------------------------------------------------------------------------
Print 'Catalogo 31 Tercero Parametrización Contable'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 31
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 31
GO
DELETE Catalogos WHERE Codigo = 31
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,31,'TEPC','Tercero Parametrización Contable',0,3,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,31,3100,'(NO APLICA)','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,31,3101,'Afiliado','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,31,3103,'Cliente','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,31,3104,'Banco','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,31,3105,'Facturar A','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,31,3106,'Tenedor','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,31,3107,'Conductor','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,31,3108,'Tercero Base','',1,GETDATE())
GO
-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,31,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0)
GO 
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,31,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,31,3,'Tipo Documento Genera',1,1,1,1,0,0,0,0,0,1,0)
GO 
----------------------------------------------------------------------------------------------------------