﻿----------------------------------------------------------------------------------------------------------
Print 'Catalogo 32 Centro Costo Parametrización Contable'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 32
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 32
GO
DELETE Catalogos WHERE Codigo = 32
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,32,'CCPC','Centro Costo Parametrización Contable',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,32,3200,'(NO APLICA)','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,32,3201,'Oficina','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,32,3202,'Número Vehículo','',1,GETDATE())
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2, USUA_Codigo_Crea,Fecha_Crea) 
VALUES (1,32,3203,'Placa Vehículo','',1,GETDATE())
GO
-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,32,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0)
GO 
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES (1,32,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 
----------------------------------------------------------------------------------------------------------