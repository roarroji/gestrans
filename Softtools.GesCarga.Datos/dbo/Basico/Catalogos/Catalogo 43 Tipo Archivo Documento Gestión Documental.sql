﻿Print 'Catalogo 43 Tipo Archivo Documento Gestión Documental'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 43
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 43
GO
DELETE Catalogos WHERE Codigo = 43
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,43,'TIAD','Tipo Archivo Documento Gestión Documental',1,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea,Campo2) 
VALUES 
(1,43,4300,UPPER('(NO APLICA)'),1,GETDATE(),''),
(1,43,4301,UPPER('Imagen'),1,GETDATE(),'.BMP, .GIF, .JPG, .JPEG, .TIF, .TIFF, .PNG'),
(1,43,4302,UPPER('PDF'),1,GETDATE(),'.pdf'),
(1,43,4303,UPPER('Word/Txt'),1,GETDATE(),'.doc, .docx, .docm, .dotx, .dotm, .dot, .xps, .txt, .xml, .odt'),
(1,43,4304,UPPER('Hojas de calculo'),1,GETDATE(),'.xlsx, .xlsm, .xlsb, .xltx, .xltm, .xls, .xlt, .xml, .xlam, .xla, .xlw, .xlr'),
(1,43,4305,UPPER('PDF/Word/Txt/Excel/Imagen'),1,GETDATE(),'.BMP, .GIF, .JPG, .JPEG, .TIF, .TIFF, .PNG, .doc, .docx, .docm, .dotx, .dotm, .dot, .xps, .txt, .xml, .odt, .xlsx, .xlsm, .xlsb, .xltx, .xltm, .xls, .xlt, .xml, .xlam, .xla, .xlw, .xlr'),
(1,43,4306,UPPER('Word/Txt/Excel'),1,GETDATE(),'.doc, .docx, .docm, .dotx, .dotm, .dot, .xps, .txt, .xml, .odt, .xlsx, .xlsm, .xlsb, .xltx, .xltm, .xls, .xlt, .xml, .xlam, .xla, .xlw, .xlr')
GO
-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,43,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,43,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0),
(1,43,3,'Formato_Aplicacion',1,1,1,1,0,0,0,0,0,1,0)
GO 