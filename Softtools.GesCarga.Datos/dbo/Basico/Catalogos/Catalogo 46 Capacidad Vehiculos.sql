﻿Print 'Catalogo 46 Capacidad Vehiculos'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 46
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 46
GO
DELETE Catalogos WHERE Codigo = 46
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos VALUES (1,46,'CAVE','Capacidad Vehiculos',0,2,1)
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1, USUA_Codigo_Crea,Fecha_Crea) 
VALUES 
(1,46,4600,UPPER('(NO APLICA)'),1,GETDATE()),
(1,46,4601,UPPER('SENCILLO 28 BULTOS'),1,GETDATE()),
(1,46,4602,UPPER('TURBO 28 BULTOS'),1,GETDATE()),
(1,46,4603,UPPER('TURBO 30 BULTOS'),1,GETDATE()),
(1,46,4604,UPPER('TURBO 32 BULTOS'),1,GETDATE()),
(1,46,4605,UPPER('TURBO 36 BULTOS'),1,GETDATE()),
(1,46,4606,UPPER('TURBO 38 BULTOS'),1,GETDATE()),
(1,46,4607,UPPER('TURBO 40 BULTOS'),1,GETDATE()),
(1,46,4608,UPPER('TURBO 42 BULTOS'),1,GETDATE()),
(1,46,4609,UPPER('TURBO 44 BULTOS'),1,GETDATE()),
(1,46,4610,UPPER('TURBO 84 BULTOS'),1,GETDATE()),
(1,46,4611,UPPER('SENCILLO 30 BULTOS'),1,GETDATE()),
(1,46,4612,UPPER('SENCILLO 32 BULTOS'),1,GETDATE()),
(1,46,4613,UPPER('SENCILLO 36 BULTOS'),1,GETDATE()),
(1,46,4614,UPPER('SENCILLO 38 BULTOS'),1,GETDATE()),
(1,46,4615,UPPER('SENCILLO 40 BULTOS'),1,GETDATE()),
(1,46,4616,UPPER('SENCILLO 42 BULTOS'),1,GETDATE()),
(1,46,4617,UPPER('SENCILLO 44 BULTOS'),1,GETDATE()),
(1,46,4618,UPPER('SENCILLO 84 BULTOS'),1,GETDATE())

GO

-----------------------------------------CONFIGURACION CATALOGOS------------------------------------------
INSERT INTO configuracion_catalogos (EMPR_Codigo,CATA_Codigo,Secuencia,Nombre,Obligatorio,Parte_Llave,Tipo,Longitud_Campo,Numero_Decimales,Utilizar_Combo,Codigo_Catalogo_Combo,Campo_Codigo_Combo,Campo_Nombre_Combo,Visible,Sugerir_Codigo)
VALUES 
(1,46,1,'Codigo',1,1,1,1,0,0,0,0,0,1,0),
(1,46,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0)
GO 
