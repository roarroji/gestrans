﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 07/12/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_insertar_cierre_contable'
GO
DROP PROCEDURE gsp_insertar_cierre_contable
GO 
CREATE PROCEDURE gsp_insertar_cierre_contable 
(      
 @par_EMPR_Codigo SMALLINT,      
 @par_Ano NUMERIC,      
 @par_Mes NUMERIC,      
 @par_Tipo_Documento NUMERIC,       
 @par_Estado_Cierre NUMERIC,        
 @par_USUA_Codigo_Crea SMALLINT      
)      
AS      
BEGIN      
 INSERT INTO      
  Cierre_Contable_Documentos      
  (      
  EMPR_Codigo,    
  Ano,    
  CATA_MESE_Codigo,    
  TIDO_Codigo,    
  CATA_ESPC_Codigo,    
  Fecha_Crea,    
  USUA_Codigo_Crea    
  )      
 VALUES      
 (      
  @par_EMPR_Codigo,      
  @par_Ano,      
  @par_Mes,        
  @par_Tipo_Documento,    
  @par_Estado_Cierre,       
  GETDATE(),    
  @par_USUA_Codigo_Crea    
      
 )      
SELECT Codigo = @@ROWCOUNT     
END      
GO