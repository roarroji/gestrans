﻿	/*Crea:Miguel Ortega
	Fecha_Crea: 14/02/2019
	Descripción_Crea: Obtiene el cierre contable de un documento creado en una fecha
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


Print 'CREATE PROCEDURE sp_obtener_cierre_contable_documento'
GO
DROP PROCEDURE sp_obtener_cierre_contable_documento
GO

CREATE PROCEDURE sp_obtener_cierre_contable_documento (
	@par_EMPR_Codigo smallint,
	@par_TIPO_Codigo NUMERIC,
	@par_Fecha_Docu	 DATE
)
AS
BEGIN
	DECLARE @Mes smallint = MONTH(@par_Fecha_Docu); -- Obtiene Mes Del Documento
	DECLARE @Anio smallint = YEAR(@par_Fecha_Docu);-- Otiene Año del Documento

	SELECT DISTINCT
	CCDO.EMPR_Codigo      
	,CCDO.Ano      
	,CCDO.CATA_MESE_Codigo   
	,MESE.Campo1 AS Nombre_Mes
	,MESE.Campo2 AS Numero_Mes
	,CCDO.CATA_ESPC_Codigo       
	,ESPC.Campo1 AS Nombre_Estado_Cierre
	,ESPC.Campo2 AS Numero_Estado_Cierre
	,CCDO.TIDO_Codigo       
	,TIDO.Nombre AS Nombre_Tipo_Documento      
      
	FROM     
	Cierre_Contable_Documentos as CCDO      
      
	LEFT JOIN Valor_Catalogos AS MESE      
	ON CCDO.EMPR_Codigo = MESE.EMPR_Codigo      
	AND CCDO.CATA_MESE_Codigo = MESE.Codigo      
    
	LEFT JOIN Valor_Catalogos AS ESPC      
	ON CCDO.EMPR_Codigo = ESPC.EMPR_Codigo      
	AND CCDO.CATA_ESPC_Codigo = ESPC.Codigo   
    
	LEFT JOIN Tipo_Documentos AS TIDO      
	ON CCDO.EMPR_Codigo = TIDO.EMPR_Codigo      
	AND CCDO.TIDO_Codigo = TIDO.Codigo     
              
	WHERE     
    
	CCDO.EMPR_Codigo = @par_EMPR_Codigo         
	AND CCDO.Ano = @Anio
	AND MESE.Campo2 = @Mes
	AND CCDO.TIDO_Codigo = @par_TIPO_Codigo
END
GO