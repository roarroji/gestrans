﻿PRINT 'gsp_insertar_legalizacion_gastos_ruta'
GO
DROP PROCEDURE gsp_insertar_legalizacion_gastos_ruta  
GO
CREATE PROCEDURE gsp_insertar_legalizacion_gastos_ruta  
(
	@par_EMPR_Codigo smallint,  
	@par_RUTA_Codigo NUMERIC,  
	@par_CATA_TIVE_Codigo NUMERIC,
	@par_CLGC_Codigo NUMERIC,
	@par_Valor MONEY,
	@par_USUA_Crea smallint
)  
AS  
BEGIN  
	INSERT INTO Plantilla_Ruta_Legalizacion_Gastos_Conductor(  
		EMPR_Codigo,
		RUTA_Codigo,
		CATA_TIVE_Codigo,
		CLGC_Codigo,
		Valor,
		Estado,
		USUA_Codigo_Crea,
		Fecha_Crea
	)  
	VALUES (
	@par_EMPR_Codigo,
	@par_RUTA_Codigo,
	@par_CATA_TIVE_Codigo,
	@par_CLGC_Codigo,
	@par_Valor,
	1,
	@par_USUA_Crea,
	GETDATE()
	)  
	
	SELECT RUTA_Codigo FROM Plantilla_Ruta_Legalizacion_Gastos_Conductor
	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND RUTA_Codigo = @par_RUTA_Codigo
	AND CATA_TIVE_Codigo = @par_CATA_TIVE_Codigo
	AND CLGC_Codigo = @par_CLGC_Codigo

END
GO