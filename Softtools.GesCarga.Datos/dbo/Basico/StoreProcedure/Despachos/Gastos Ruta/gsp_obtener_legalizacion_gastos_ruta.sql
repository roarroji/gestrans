﻿PRINT 'gsp_obtener_legalizacion_gastos_ruta'
GO
DROP PROCEDURE gsp_obtener_legalizacion_gastos_ruta
GO
CREATE PROCEDURE gsp_obtener_legalizacion_gastos_ruta
(        
	@par_EMPR_Codigo SMALLINT,        
	@par_Codigo numeric = NULL    
)        
AS        
BEGIN
	SELECT 
	PRLG.EMPR_Codigo,
	PRLG.CATA_TIVE_Codigo,
	CATA_TIVE.Campo1 AS CATA_TIVE_Nombre,
	PRLG.Valor,
	PRLG.CLGC_Codigo,
	CLGC.Nombre AS CLGC_Nombre

	FROM Plantilla_Ruta_Legalizacion_Gastos_Conductor PRLG

	LEFT JOIN Valor_Catalogos CATA_TIVE ON
	PRLG.EMPR_Codigo = CATA_TIVE.EMPR_Codigo
	AND PRLG.CATA_TIVE_Codigo = CATA_TIVE.Codigo

	LEFT JOIN Conceptos_Legalizacion_Gastos_Conductor CLGC ON
	PRLG.EMPR_Codigo = CLGC.EMPR_Codigo
	AND PRLG.CLGC_Codigo = CLGC.Codigo

	WHERE
	PRLG.EMPR_Codigo = @par_EMPR_Codigo
	AND PRLG.RUTA_Codigo = ISNULL(@par_Codigo, PRLG.RUTA_Codigo)
END
GO 