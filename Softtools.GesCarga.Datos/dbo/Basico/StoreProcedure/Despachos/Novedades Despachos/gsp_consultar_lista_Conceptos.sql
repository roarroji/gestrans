﻿PRINT 'gsp_consultar_lista_Conceptos'
GO
DROP PROCEDURE gsp_consultar_lista_Conceptos
GO
CREATE PROCEDURE gsp_consultar_lista_Conceptos     
(@par_EMPR_Codigo SMALLINT,      
@par_Codigo NUMERIC)      
      
AS    
BEGIN    
SELECT        
EMPR_Codigo      
,NODE_Codigo      
,COVE_Codigo      
,CLPD_Codigo          
      
FROM Novedades_Conceptos 
WHERE       
EMPR_Codigo = @par_EMPR_Codigo      
AND NODE_Codigo = @par_Codigo     
      
END      
GO