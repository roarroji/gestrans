﻿PRINT 'gsp_consultar_novedades_despachos'
GO
DROP PROCEDURE gsp_consultar_novedades_despachos
GO
CREATE PROCEDURE gsp_consultar_novedades_despachos 
 (    
    
  @par_EMPR_Codigo SMALLINT,    
  @par_Codigo NUMERIC = NULL,   
  @par_Codigo_Alterno VARCHAR(50) = NULL,   
  @par_Nombre VARCHAR(50) = NULL,        
  @par_Estado SMALLINT = NULL,    
  @par_NumeroPagina INT = NULL,    
  @par_RegistrosPagina INT = NULL    
 )    
 AS    
 BEGIN    
  SET NOCOUNT ON;    
   DECLARE    
    @CantidadRegistros INT    
   SELECT @CantidadRegistros = (    
     SELECT DISTINCT     
      COUNT(1)     
      FROM    
      Novedades_Despacho    
      WHERE    
      EMPR_Codigo = @par_EMPR_Codigo    
      AND Codigo = ISNULL(@par_Codigo, Codigo)    
      AND Estado = ISNULL(@par_Estado, Estado)    
      AND ((Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))    
      AND ((Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))    
      );    
                
     WITH Pagina AS    
     (    
    
      SELECT     
      EMPR_Codigo,    
      Codigo,    
      ISNULL(Codigo_Alterno, 0) AS Codigo_Alterno,    
      Nombre,    
      Estado,      
      ROW_NUMBER() OVER(ORDER BY Nombre) AS RowNumber    
      FROM    
      Novedades_Despacho    
      WHERE    
      EMPR_Codigo = @par_EMPR_Codigo    
      AND Codigo = ISNULL(@par_Codigo, Codigo)    
      AND Estado = ISNULL(@par_Estado, Estado)    
      AND ((Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))    
      AND ((Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))    
    
   )    
   SELECT DISTINCT    
   0 AS Obtener,    
   EMPR_Codigo,    
   Codigo,    
   Codigo_Alterno,    
   Nombre,    
   Estado,     
   @CantidadRegistros AS TotalRegistros,    
   @par_NumeroPagina AS PaginaObtener,    
   @par_RegistrosPagina AS RegistrosPagina    
   FROM    
    Pagina    
   WHERE    
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
   ORDER BY Nombre    
   END    
  
GO