﻿PRINT 'gsp_eliminar_lista_Conceptos'
GO
DROP PROCEDURE gsp_eliminar_lista_Conceptos
GO
CREATE PROCEDURE gsp_eliminar_lista_Conceptos 
(@par_EMPR_Codigo SMALLINT,      
@par_Codigo NUMERIC)      
AS    
BEGIN      
DELETE Novedades_Conceptos      
WHERE       
EMPR_Codigo = @par_EMPR_Codigo      
AND       
NODE_Codigo = @par_Codigo      
END      
GO