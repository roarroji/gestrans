﻿PRINT 'gsp_obtener_novedades_despachos'
GO
DROP PROCEDURE gsp_obtener_novedades_despachos
GO
CREATE PROCEDURE gsp_obtener_novedades_despachos 
(    
@par_EMPR_Codigo SMALLINT,    
@par_Codigo NUMERIC    
)    
AS    
BEGIN    
SELECT     
1 AS Obtener,    
EMPR_Codigo,    
Codigo,    
ISNULL(Codigo_Alterno, 0) Codigo_Alterno,    
Nombre,     
Estado  
FROM    
Novedades_Despacho    
WHERE     
EMPR_Codigo = @par_EMPR_Codigo    
AND Codigo = @par_Codigo    
    
END    
GO