﻿PRINT 'gsp_consultar_precintos'
GO
DROP PROCEDURE gsp_consultar_precintos
GO
CREATE PROCEDURE gsp_consultar_precintos 
(                
@par_empr_codigo numeric,   
@par_Numero numeric = null,                
@par_oficina numeric = null,                
@par_Oficina_Destino numeric  = null,                 
@par_Tipo_Precinto numeric  = null,                
@par_fecha DATE = null,                 
@par_numeropagina numeric,                
@par_registrospagina numeric                
)                  
as                  
begin                  
           SET @par_fecha = CONVERT(DATE , @par_fecha)    
           SET @par_fecha = CONVERT(DATE, @par_fecha)           
          
 IF @par_Tipo_Precinto = 0      
  BEGIN       
 SET @par_Tipo_Precinto = NULL      
 END       
      
      
declare                  
  @cantidadregistros int                  
  select @cantidadregistros = (                  
    select distinct                   
     count(1)                   
          
from encabezado_asignacion_precintos_oficina eapo                
                
left join oficinas ofic on                 
eapo.empr_codigo = ofic.empr_codigo                 
and eapo.ofic_codigo= ofic.codigo                
                
left join oficinas ofde on                 
eapo.empr_codigo = ofde.empr_codigo                 
and eapo.ofic_codigo_destino= ofde.codigo                
                
left join terceros resp on                 
eapo.empr_codigo = resp.empr_codigo                 
and eapo.terc_codigo_responsable = resp.codigo                 
                
left join valor_catalogos tapr on                 
eapo.empr_codigo = tapr.empr_codigo                 
and eapo.cata_tapr_codigo = tapr.codigo                 
                
left join valor_catalogos tpre on                 
eapo.empr_codigo = tpre.empr_codigo                 
and eapo.CATA_TPRE_Codigo = tpre.codigo                 
                
where eapo.empr_codigo = @par_empr_codigo      
and  eapo.Numero = isnull (@par_Numero,eapo.Numero)            
and  eapo.ofic_codigo = isnull (@par_oficina,eapo.ofic_codigo)                
and eapo.OFIC_Codigo_Destino = isnull(@par_Oficina_Destino ,eapo.OFIC_Codigo_Destino)                
and eapo.CATA_TPRE_Codigo = isnull(@par_Tipo_Precinto ,eapo.CATA_TPRE_Codigo)             
AND eapo.Fecha_Entrega >= ISNULL(@par_Fecha , eapo.Fecha_Entrega)     
AND eapo.Fecha_Entrega <= ISNULL(@par_Fecha , eapo.Fecha_Entrega)                
     );                  
                              
    with pagina as                  
    (                  
                  
select                 
eapo.empr_codigo ,                
eapo.numero,                
tapr.campo1 nombreproceso,                
ofic.nombre nombreoficina,                
ofde.nombre nombreoficinadestino,                
eapo.Fecha_Entrega fecha_entrega,                
eapo.numero_precinto_inicial ,                
eapo.numero_precinto_final ,                
tpre.campo1 tipoprecinto ,                
isnull(resp.razon_social,'')+' '+isnull(resp.nombre,'')                        
+' '+isnull(resp.apellido1,'')+' '+isnull(resp.apellido2,'') as nombreresponsable   ,                
isnull (eapo.anulado,0) anulado,                
    row_number() over(order by eapo.numero) as rownumber                  
from encabezado_asignacion_precintos_oficina eapo                
                
left join oficinas ofic on                 
eapo.empr_codigo = ofic.empr_codigo                 
and eapo.ofic_codigo= ofic.codigo                
                
left join oficinas ofde on                 
eapo.empr_codigo = ofde.empr_codigo                 
and eapo.ofic_codigo_destino= ofde.codigo                
                
left join terceros resp on                 
eapo.empr_codigo = resp.empr_codigo                 
and eapo.terc_codigo_responsable = resp.codigo                 
                
left join valor_catalogos tapr on                 
eapo.empr_codigo = tapr.empr_codigo                 
and eapo.cata_tapr_codigo = tapr.codigo                 
                
left join valor_catalogos tpre on                 
eapo.empr_codigo = tpre.empr_codigo                 
and eapo.CATA_TPRE_Codigo = tpre.codigo                 
                
where eapo.empr_codigo = @par_empr_codigo  
and  eapo.Numero = isnull (@par_Numero,eapo.Numero)                
and  eapo.ofic_codigo = isnull (@par_oficina,eapo.ofic_codigo)                
and eapo.OFIC_Codigo_Destino = isnull(@par_Oficina_Destino ,eapo.OFIC_Codigo_Destino)                
and eapo.CATA_TPRE_Codigo = isnull(@par_Tipo_Precinto ,eapo.CATA_TPRE_Codigo)            
AND eapo.Fecha_Entrega >= ISNULL(@par_Fecha , eapo.Fecha_Entrega)     
AND eapo.Fecha_Entrega <= ISNULL(@par_Fecha , eapo.Fecha_Entrega)           
  )                  
  select distinct                  
  0 as obtener,                  
  EMPR_Codigo ,                
 Numero,                
 NombreProceso,                
 NombreOficina,                
 NombreOficinaDestino,                
 Fecha_Entrega,                
 Numero_Precinto_Inicial ,                
 Numero_Precinto_Final ,                
 TipoPrecinto,                
 NombreResponsable,             
 Anulado,                
 @cantidadregistros as TotalRegistros,                  
 @par_numeropagina as PaginaObtener,                  
 @par_registrospagina as RegistrosPagina                  
 from                  
 pagina                  
 where                  
 rownumber > (isnull(@par_numeropagina, 1) -1) * isnull(@par_registrospagina, @cantidadregistros)                  
 and rownumber <= isnull(@par_numeropagina, 1) * isnull(@par_registrospagina, @cantidadregistros)                  
 order by numero                  
                   
END                
GO