﻿PRINT 'gsp_consultar_puesto_controles        '
go
DROP PROCEDURE gsp_consultar_puesto_controles        
go
CREATE PROCEDURE gsp_consultar_puesto_controles        
(        
 @par_EMPR_Codigo SMALLINT,        
 @par_Codigo numeric = NULL,        
 @par_Codigo_Alterno varchar(20) = NULL,        
 @par_Nombre VARCHAR(100) = NULL,        
 @par_Estado SMALLINT = NULL,        
 @par_Proveedor VARCHAR(50) = NULL,        
 @par_NumeroPagina INT = NULL,      
 @par_RegistrosPagina INT = NULL      
)        
AS        
BEGIN        
 SET NOCOUNT ON;        
 DECLARE        
 @CantidadRegistros INT        
 SELECT @CantidadRegistros = (        
 SELECT DISTINCT         
  COUNT(1)         
 FROM        
  Puesto_Controles  as PECO    
  LEFT JOIN Terceros AS PROV    
  ON PECO.EMPR_Codigo = PROV.EMPR_Codigo      
  AND PECO.TERC_Codigo_Proveedor = PROV.Codigo      
 WHERE        
  PECO.EMPR_Codigo = @par_EMPR_Codigo        
  --AND Numero = ISNULL(@par_Codigo,Numero)        
  AND PECO.Codigo = ISNULL(@par_Codigo,PECO.Codigo)        
  AND ((CONCAT(isnull(PROV.Razon_Social,''),isnull(PROV.Nombre,''),' ',PROV.Apellido1,' ',PROV.Apellido2) LIKE '%' + @par_Proveedor + '%') OR (@par_Proveedor IS NULL))    
  AND ((PECO.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))        
  AND PECO.Estado = ISNULL(@par_Estado,PECO.Estado   )        
  AND PECO.Codigo_Alterno = ISNULL(@par_Codigo_Alterno,PECO.Codigo_Alterno   )        
  AND PECO.Codigo != 0        
  );               
 WITH Pagina AS        
 (        
        
 SELECT        
  PECO.EMPR_Codigo,        
  PECO.Codigo,    
  ISNULL(PECO.Codigo_Alterno,'') AS Codigo_Alterno,     
  ISNULL(PECO.Nombre,'')AS Nombre,         
  ISNULL(PECO.Estado,'') AS Estado ,     
  ISNULL(PROV.Razon_Social,'') + ' ' + ISNULL(PROV.Nombre,'') + ' ' + ISNULL(PROV.Apellido1,'') + ' ' + ISNULL(PROV.Apellido2,'') As NombreProveedor,    
  ISNULL(PECO.Ubicacion,'') AS Ubicacion,     
  ROW_NUMBER() OVER(ORDER BY PECO.Codigo) AS RowNumber        
 FROM        
  Puesto_Controles  as PECO    
  LEFT JOIN Terceros AS PROV    
  ON PECO.EMPR_Codigo = PROV.EMPR_Codigo      
  AND PECO.TERC_Codigo_Proveedor = PROV.Codigo      
 WHERE        
  PECO.EMPR_Codigo = @par_EMPR_Codigo        
  --AND Numero = ISNULL(@par_Codigo,Numero)        
  AND PECO.Codigo = ISNULL(@par_Codigo,PECO.Codigo)        
  AND ((CONCAT(isnull(PROV.Razon_Social,''),isnull(PROV.Nombre,''),' ',PROV.Apellido1,' ',PROV.Apellido2) LIKE '%' + @par_Proveedor + '%') OR (@par_Proveedor IS NULL))    
  AND ((PECO.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))        
  AND PECO.Estado = ISNULL(@par_Estado,PECO.Estado   )        
  AND PECO.Codigo_Alterno = ISNULL(@par_Codigo_Alterno,PECO.Codigo_Alterno   )        
  AND PECO.Codigo != 0        
  )       
 SELECT DISTINCT        
  0 As Obtener,    
  Codigo,    
  EMPR_Codigo,        
  Codigo_Alterno,    
  Nombre,        
  Estado,    
  NombreProveedor,    
  Ubicacion,    
 @CantidadRegistros AS TotalRegistros,        
 @par_NumeroPagina AS PaginaObtener,        
 @par_RegistrosPagina AS RegistrosPagina        
 FROM        
 Pagina        
 WHERE        
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)      
 ORDER BY Codigo asc        
        
END        
GO