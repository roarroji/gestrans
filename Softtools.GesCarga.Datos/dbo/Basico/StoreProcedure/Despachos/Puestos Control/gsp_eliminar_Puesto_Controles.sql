﻿PRINT 'gsp_eliminar_Puesto_Controles'
GO
DROP PROCEDURE gsp_eliminar_Puesto_Controles
GO
CREATE PROCEDURE gsp_eliminar_Puesto_Controles( 
  @par_EMPR_Codigo SMALLINT,    
  @par_Codigo NUMERIC    
  )    
AS    
BEGIN    
      
  DELETE Puesto_Controles   
      
  WHERE EMPR_Codigo = @par_EMPR_Codigo     
    AND Codigo = @par_Codigo    
    
    SELECT @@ROWCOUNT AS Codigo    
    
END   
GO