﻿PRINT 'gsp_obtener_puestos_controles_rutas'
go
drop procedure gsp_obtener_puestos_controles_rutas
go
CREATE PROCEDURE gsp_obtener_puestos_controles_rutas      
(      
 @par_EMPR_Codigo SMALLINT,      
 @par_Codigo numeric = NULL  
)      
AS      
BEGIN      
SELECT        
  PECO.EMPR_Codigo,        
  PECO.Codigo,    
  ISNULL(PECO.Codigo_Alterno,'') AS Codigo_Alterno,     
  ISNULL(PECO.Nombre,'')AS Nombre,         
  ISNULL(PECO.Estado,'') AS Estado ,     
  ISNULL(PROV.Razon_Social,'') + ' ' + ISNULL(PROV.Nombre,'') + ' ' + ISNULL(PROV.Apellido1,'') + ' ' + ISNULL(PROV.Apellido2,'') As NombreProveedor,    
  ISNULL(PECO.Ubicacion,'') AS Ubicacion,     
  ISNULL(PECR.Orden,0) AS Orden,  
  ISNULL(PECR.Horas_Estimadas_Arribo,0) AS Horas_Estimadas_Arribo,  
  2 as Obtener,  
  1 as TotalRegistros  
 FROM        
  Puesto_Controles  as PECO    
  LEFT JOIN Terceros AS PROV    
  ON PECO.EMPR_Codigo = PROV.EMPR_Codigo      
  AND PECO.TERC_Codigo_Proveedor = PROV.Codigo      
    
  LEFT JOIN Puesto_Control_Rutas AS PECR  
   ON PECO.EMPR_Codigo = PECR.EMPR_Codigo      
  AND PECO.Codigo = PECR.PUCO_Codigo      
  
  LEFT JOIN Rutas AS RUTA  
   ON PECR.EMPR_Codigo = RUTA.EMPR_Codigo      
  AND PECR.RUTA_Codigo = RUTA.Codigo   
  
  
 WHERE        
  PECO.EMPR_Codigo = @par_EMPR_Codigo        
  --AND Numero = ISNULL(@par_Codigo,Numero)        
  AND RUTA.Codigo = ISNULL(@par_Codigo,RUTA.Codigo)        
  
END  
go