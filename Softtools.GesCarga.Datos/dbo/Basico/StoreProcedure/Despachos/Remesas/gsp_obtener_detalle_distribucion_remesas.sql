﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 17/06/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_obtener_detalle_distribucion_remesas   '
GO
DROP PROCEDURE gsp_obtener_detalle_distribucion_remesas
GO
CREATE PROCEDURE gsp_obtener_detalle_distribucion_remesas 
(              
 @par_EMPR_Codigo SMALLINT,         
 @par_ENRE_Numero NUMERIC,      
 @par_Codigo INT              
)              
AS            
BEGIN            
 SELECT            
  1 AS Obtener,            
  DEDR.EMPR_Codigo,                  
  DEDR.ID,                  
  DEDR.ENRE_Numero,             
  ISNULL(DEDR.PRTR_Codigo, 0) AS PRTR_Codigo,          
  ISNULL(PRTR.Nombre, '') AS Nombre_Producto,              
  ISNULL(DEDR.Cantidad, 0) AS Cantidad,          
  ISNULL(DEDR.Peso, 0) AS Peso,                 
  ISNULL(DEDR.TERC_Codigo_Destinatario, 0) AS TERC_Codigo_Destinatario,           
  ISNULL(DEDR.CATA_TIID_Codigo_Destinatario, 0) AS CATA_TIID_Codigo,      
  ISNULL(DEDR.Numero_Identificacion_Destinatario, '') AS Numero_Identificacion_Destinatario,           
  ISNULL(DEDR.Nombre_Destinatario, '') AS Nombre_Destinatario,           
  ISNULL(DEDR.CIUD_Codigo_Destinatario, 0) AS CIUD_Codigo_Destinatario,          
  ISNULL(CIUD.Nombre, '') AS Nombre_Ciudad,          
  ISNULL(DEDR.ZOCI_Codigo_Destinatario, 0) AS ZOCI_Codigo_Destinatario,          
  ISNULL(ZOCI.Nombre, '') AS Nombre_Zona,    
  ISNULL(DEDR.SICD_Codigo_Entrega, 0) AS SICD_Codigo_Entrega,          
  ISNULL(SICD.Nombre, '') AS Nombre_Sitio_Entrega,      
  ISNULL(DEDR.Barrio_Destinatario, '') AS Barrio_Destinatario,          
  ISNULL(DEDR.Direccion_Destinatario, 0) AS Direccion_Destinatario,           
  CASE WHEN DEDR.Codigo_Postal_Destinatario = '' THEN 0 ELSE ISNULL(DEDR.Codigo_Postal_Destinatario, 0) END AS Codigo_Postal_Destinatario,           
  ISNULL(DEDR.Telefonos_Destinatario, '') AS Telefonos_Destinatario,    
  ISNULL(DEDR.Fecha_Recibe, 0) AS Fecha_Recibe,       
  ISNULL(DEDR.Cantidad_Recibe, 0) AS Cantidad_Recibe,          
  ISNULL(DEDR.Peso_Recibe, 0) AS Peso_Recibe,          
  ISNULL(DEDR.CATA_TIID_Codigo_Recibe, 0) AS CATA_TIID_Codigo_Recibe,             
  ISNULL(DEDR.Numero_Identificacion_Recibe, 0) AS Numero_Identificacion_Recibe,            
  ISNULL(DEDR.Nombre_Recibe, '') AS Nombre_Recibe,               
  ISNULL(DEDR.Telefonos_Recibe, 0) AS Telefonos_Recibe,         
  ISNULL(DEDR.Observaciones_Recibe, '') AS Observaciones_Recibe,      
  DEDR.Firma_Recibe,        
  ISNULL(DEDR.USUA_Codigo_Modifica, 0) AS USUA_Codigo_Modifica    
           
 FROM            
   Detalle_Distribucion_Remesas as DEDR            
         
 LEFT JOIN Producto_Transportados AS PRTR            
 ON DEDR.EMPR_Codigo = PRTR.EMPR_Codigo            
 AND DEDR.PRTR_Codigo = PRTR.Codigo                 
            
 LEFT JOIN Ciudades AS CIUD            
 ON DEDR.EMPR_Codigo = CIUD.EMPR_Codigo            
 AND DEDR.CIUD_Codigo_Destinatario = CIUD.Codigo            
        
 LEFT JOIN Zona_Ciudades AS ZOCI            
 ON DEDR.EMPR_Codigo = ZOCI.EMPR_Codigo            
 AND DEDR.ZOCI_Codigo_Destinatario = ZOCI.Codigo       
 
 LEFT JOIN Sitios_Cargue_Descargue AS SICD            
 ON DEDR.EMPR_Codigo = SICD.EMPR_Codigo            
 AND DEDR.SICD_Codigo_Entrega = SICD.Codigo     
              
  WHERE                  
   DEDR.EMPR_Codigo = @par_EMPR_Codigo      
   AND DEDR.ENRE_Numero = @par_ENRE_Numero                   
   AND DEDR.ID = @par_Codigo         
END       
GO   