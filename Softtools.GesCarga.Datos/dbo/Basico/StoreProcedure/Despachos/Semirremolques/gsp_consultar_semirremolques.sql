﻿Print 'gsp_consultar_semirremolques'
GO
DROP PROCEDURE gsp_consultar_semirremolques
GO
CREATE PROCEDURE gsp_consultar_semirremolques  
(  
 @par_EMPR_Codigo SMALLINT,  
 @par_Codigo NUMERIC = NULL,  
 @par_Placa VARCHAR(15) = NULL,  
 @par_Codigo_Alterno VARCHAR(15) = NULL,  
 @par_Propietario VARCHAR(100) = NULL,  
 @par_Marca VARCHAR(100) = NULL,  
   
 @par_Estado NUMERIC = NULL,  
 @par_NumeroPagina INT = NULL,  
 @par_RegistrosPagina INT = null  
)  
AS  
BEGIN  
  SET NOCOUNT ON;  
  DECLARE  
   @CantidadRegistros INT  
  SELECT @CantidadRegistros = (  
    SELECT DISTINCT   
     COUNT(1)   
    FROM  
     Semirremolques SEMI,  
     Terceros PROP,  
     Marca_Semirremolques MASE  
    WHERE  
     PROP.EMPR_Codigo = SEMI.EMPR_Codigo  
     AND PROP.Codigo = SEMI.TERC_Codigo_Propietario  
       
     AND SEMI.EMPR_Codigo = MASE.EMPR_Codigo  
     AND SEMI.MASE_Codigo = MASE.Codigo  
     and SEMI.Codigo != 0  
     and SEMI.Codigo = isnull(@par_Codigo,SEMI.Codigo)  
     AND SEMI.EMPR_Codigo = @par_EMPR_Codigo  
     AND ((SEMI.Placa LIKE '%' + @par_Placa + '%') OR (@par_Placa IS NULL))  
     AND ISNULL(SEMI.Codigo_Alterno,'') = ISNULL(@par_Codigo_Alterno,ISNULL(SEMI.Codigo_Alterno,''))  
     AND ((CONCAT(isnull(PROP.Razon_Social,''),isnull(PROP.Nombre,''),' ',PROP.Apellido1,' ',PROP.Apellido2) LIKE '%' + @par_Propietario + '%') OR (@par_Propietario IS NULL))  
       
     AND SEMI.Estado = ISNULL(@par_Estado,SEMI.Estado)  
     AND ((MASE.Nombre LIKE '%' + @par_Marca + '%') OR (@par_Marca IS NULL))  
     );         
    WITH Pagina AS  
    (  
  
    SELECT  
  PROP.EMPR_Codigo,  
  SEMI.Placa,  
  SEMI.Codigo,  
  SEMI.Codigo_Alterno,  
  MASE.Nombre AS Marca,  
  ISNULL(PROP.Razon_Social,'') + ' ' + ISNULL(PROP.Nombre,'') + ' ' + ISNULL(PROP.Apellido1,'') + ' ' + ISNULL(PROP.Apellido2,'') As NombrePropietario,  
  SEMI.Modelo,  
  SEMI.Estado,  
  SEMI.Estado AS EstadoSemirremolque,  
  ROW_NUMBER() OVER(ORDER BY SEMI.Codigo) AS RowNumber  
 FROM  
  Semirremolques SEMI,  
  Terceros PROP,  
  Marca_Semirremolques MASE  
 WHERE  
  PROP.EMPR_Codigo = SEMI.EMPR_Codigo  
  AND PROP.Codigo = SEMI.TERC_Codigo_Propietario  
    
  AND SEMI.EMPR_Codigo = MASE.EMPR_Codigo  
  AND SEMI.MASE_Codigo = MASE.Codigo  
  and SEMI.Codigo != 0  
  and SEMI.Codigo = isnull(@par_Codigo,SEMI.Codigo)  
  AND SEMI.EMPR_Codigo = @par_EMPR_Codigo  

  AND ((SEMI.Placa LIKE '%' + @par_Placa + '%') OR (@par_Placa IS NULL))  
  AND ISNULL(SEMI.Codigo_Alterno,'') = ISNULL(@par_Codigo_Alterno,ISNULL(SEMI.Codigo_Alterno,''))  
  AND ((CONCAT(isnull(PROP.Razon_Social,''),isnull(PROP.Nombre,''),' ',PROP.Apellido1,' ',PROP.Apellido2) LIKE '%' + @par_Propietario + '%') OR (@par_Propietario IS NULL))  
    
  AND SEMI.Estado = ISNULL(@par_Estado,SEMI.Estado)  
  AND ((MASE.Nombre LIKE '%' + @par_Marca + '%') OR (@par_Marca IS NULL))  
  
  )  
    
    
  SELECT DISTINCT  
  0 AS Obtener,  
  EMPR_Codigo,  
  Placa,  
  Codigo,  
  isnull(Codigo_Alterno,'') as Codigo_Alterno,  
  Marca,  
  NombrePropietario,  
  isnull(Modelo,0) as Modelo,  
  Estado,  
  EstadoSemirremolque,  
  @CantidadRegistros AS TotalRegistros,  
  @par_NumeroPagina AS PaginaObtener,  
  @par_RegistrosPagina AS RegistrosPagina  
  FROM  
   Pagina  
  WHERE  
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
  ORDER BY Codigo ASC  
  
END  
GO