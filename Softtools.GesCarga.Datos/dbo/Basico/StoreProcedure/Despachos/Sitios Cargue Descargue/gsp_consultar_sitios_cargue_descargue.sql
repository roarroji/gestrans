﻿PRINT 'gsp_consultar_sitios_cargue_descargue'
GO
DROP PROCEDURE gsp_consultar_sitios_cargue_descargue
GO  
CREATE PROCEDURE gsp_consultar_sitios_cargue_descargue    
(    
 @par_EMPR_Codigo SMALLINT,    
 @par_Codigo NUMERIC = NULL,    
 @par_Nombre VARCHAR(50) = NULL,     
 @par_Codigo_Tipo_Sitio NUMERIC = NULL,    
 @par_Codigo_Pais NUMERIC = NULL,    
 @par_Codigo_Ciudad NUMERIC = NULL,   
 @par_Estado SMALLINT = NULL,    
 @par_NumeroPagina INT = NULL,    
 @par_RegistrosPagina INT = NULL    
)    
AS    
BEGIN    
 SET NOCOUNT ON;    
  DECLARE    
   @CantidadRegistros INT    
  SELECT @CantidadRegistros = (    
    SELECT DISTINCT     
     COUNT(1)     
       FROM    
     Sitios_Cargue_Descargue AS SICD     
  
     LEFT JOIN Valor_Catalogos AS VACA                     
     ON SICD.EMPR_Codigo = VACA.EMPR_Codigo                      
     AND SICD.CATA_TSCD_Codigo = VACA.Codigo    
  
  LEFT JOIN Paises AS PAIS                     
     ON SICD.EMPR_Codigo = PAIS.EMPR_Codigo                      
     AND SICD.PAIS_Codigo = PAIS.Codigo    
  
  LEFT JOIN Ciudades AS CIUD                     
     ON SICD.EMPR_Codigo = CIUD.EMPR_Codigo                      
     AND SICD.CIUD_Codigo = CIUD.Codigo    
    WHERE       
     SICD.EMPR_Codigo = @par_EMPR_Codigo    
  AND SICD.Codigo = ISNULL(@par_Codigo, SICD.Codigo)   
  AND ((SICD.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))   
  AND SICD.CATA_TSCD_Codigo = ISNULL(@par_Codigo_Tipo_Sitio, SICD.CATA_TSCD_Codigo)   
  AND SICD.PAIS_Codigo = ISNULL(@par_Codigo_Pais,SICD.PAIS_Codigo)    
  AND SICD.CIUD_Codigo = ISNULL(@par_Codigo_Ciudad,SICD.CIUD_Codigo)   
     AND SICD.Estado = ISNULL(@par_Estado, SICD.Estado)   
	 AND PAIS.Codigo = ISNULL(@par_Codigo_Pais, PAIS.Codigo)             
     );                  
    WITH Pagina AS    
    (      
    SELECT    
     0 AS Obtener,    
     SICD.EMPR_Codigo,    
     SICD.Codigo,        
     SICD.Nombre,   
  SICD.CATA_TSCD_Codigo,    
  SICD.PAIS_Codigo,  
  PAIS.Nombre AS Nombre_Pais,   
  SICD.CIUD_Codigo,  
  CIUD.Nombre AS Nombre_Ciudad,   
  SICD.Direccion,  
  ISNULL(SICD.Codigo_Postal, '') AS Codigo_Postal,  
  ISNULL(SICD.Contacto, '') AS Contacto,  
     SICD.Estado,    
     ROW_NUMBER() OVER(ORDER BY SICD.Nombre) AS RowNumber    
         FROM    
     Sitios_Cargue_Descargue AS SICD     
  
     LEFT JOIN Valor_Catalogos AS VACA                     
     ON SICD.EMPR_Codigo = VACA.EMPR_Codigo                      
     AND SICD.CATA_TSCD_Codigo = VACA.Codigo    
  
  LEFT JOIN Paises AS PAIS                     
     ON SICD.EMPR_Codigo = PAIS.EMPR_Codigo                      
     AND SICD.PAIS_Codigo = PAIS.Codigo    
  
  LEFT JOIN Ciudades AS CIUD                     
     ON SICD.EMPR_Codigo = CIUD.EMPR_Codigo                      
     AND SICD.CIUD_Codigo = CIUD.Codigo    
  
    WHERE    
    SICD.EMPR_Codigo = @par_EMPR_Codigo  
     AND SICD.Codigo = ISNULL(@par_Codigo, SICD.Codigo)   
  AND ((SICD.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))   
  AND SICD.CATA_TSCD_Codigo = ISNULL(@par_Codigo_Tipo_Sitio, SICD.CATA_TSCD_Codigo)   
  AND SICD.PAIS_Codigo = ISNULL(@par_Codigo_Pais,SICD.PAIS_Codigo)    
  AND SICD.CIUD_Codigo = ISNULL(@par_Codigo_Ciudad,SICD.CIUD_Codigo)   
     AND SICD.Estado = ISNULL(@par_Estado, SICD.Estado)   
	 AND PAIS.Codigo = ISNULL(@par_Codigo_Pais, PAIS.Codigo)    
  )    
  SELECT DISTINCT    
  Obtener,    
  EMPR_Codigo,    
  Codigo,     
  Nombre,   
  CATA_TSCD_Codigo,  
  PAIS_Codigo,  
  Nombre_Pais,  
  CIUD_Codigo,  
  Nombre_Ciudad,  
  Direccion,  
  Codigo_Postal,  
  Contacto,     
  Estado,    
  @CantidadRegistros AS TotalRegistros,    
  @par_NumeroPagina AS PaginaObtener,    
  @par_RegistrosPagina AS RegistrosPagina    
  FROM    
   Pagina    
  WHERE    
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
   AND RowNumber<= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
  order by Nombre    
END 
GO