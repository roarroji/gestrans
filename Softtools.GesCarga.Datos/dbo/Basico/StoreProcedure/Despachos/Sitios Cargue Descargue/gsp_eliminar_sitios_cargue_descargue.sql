﻿PRINT 'gsp_eliminar_sitios_cargue_descargue'
GO
DROP PROCEDURE gsp_eliminar_sitios_cargue_descargue
GO
CREATE PROCEDURE gsp_eliminar_sitios_cargue_descargue(  
  @par_EMPR_Codigo SMALLINT,  
  @par_Codigo NUMERIC  
  )  
AS  
BEGIN  
    
  DELETE Sitios_Cargue_Descargue   
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
    AND Codigo = @par_Codigo  
  
    SELECT @@ROWCOUNT AS Codigo  
  
END  
GO