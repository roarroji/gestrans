﻿Print 'gsp_consultar_vehiculos_Autocomplete'
GO
DROP PROCEDURE gsp_consultar_vehiculos_Autocomplete
GO
CREATE PROCEDURE gsp_consultar_vehiculos_Autocomplete      
(              
@par_EMPR_Codigo SMALLINT,              
@par_Filtro VARCHAR(100) = NULL,                                
@par_Codigo NUMERIC = null       ,    
@par_Conductor NUMERIC = null           
)              
AS              
BEGIN              
SELECT TOP(20)             
 COND.EMPR_Codigo,              
 VEHI.Placa,              
 VEHI.Codigo,              
 VEHI.Codigo_Alterno,              
 ISNULL(COND.Razon_Social,'') + ' ' + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') As NombreConductor,              
 COND.Numero_Identificacion As IdentificacionConductor,              
 VEHI.TERC_Codigo_Conductor,              
 ISNULL(TENE.Razon_Social,'') + ' ' + ISNULL(TENE.Nombre,'') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2,'') As NombreTenedor,              
 TENE.Numero_Identificacion As IdentificacionTenedor,              
 VEHI.TERC_Codigo_Tenedor,              
 VEHI.Estado,              
 VEHI.Capacidad_Kilos AS Capacidad,              
 CASE WHEN VEHI.Estado = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END AS EstadoVehiculo,              
 VEHI.CATA_TIVE_Codigo,              
 TIVE.Campo1 as TipoVehiculo,  
 ISNULL(TIVE.Campo3,0) as AplicaSemirremolque,            
 SEMI.Placa as PlacaSemirremolque,              
 SEMI.Codigo as CodigoSemirremolque,              
 ROW_NUMBER() OVER(ORDER BY VEHI.Codigo) AS RowNumber              
FROM              
 Vehiculos AS VEHI             
 LEFT JOIN Semirremolques SEMI ON            
 VEHI.EMPR_Codigo = SEMI.EMPR_Codigo              
 AND VEHI.SEMI_Codigo = SEMI.Codigo ,              
 Terceros COND,              
 Terceros TENE,              
 Valor_Catalogos TIVE              
WHERE              
 COND.EMPR_Codigo = VEHI.EMPR_Codigo              
 AND COND.Codigo = VEHI.TERC_Codigo_Conductor              
 AND TENE.EMPR_Codigo = VEHI.EMPR_Codigo              
 AND TENE.Codigo = VEHI.TERC_Codigo_Tenedor              
 AND VEHI.EMPR_Codigo = TIVE.EMPR_Codigo              
 AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo              
 and VEHI.Codigo != 0              
 AND (VEHI.Codigo = @par_Codigo OR @par_Codigo IS NULL)        
 AND VEHI.EMPR_Codigo = @par_EMPR_Codigo              
 AND ((VEHI.Placa LIKE '%' + @par_Filtro + '%') OR  (VEHI.Codigo_Alterno LIKE '%' + @par_Filtro + '%') OR (@par_Filtro IS NULL))              
 AND (VEHI.TERC_Codigo_Conductor = @par_Conductor OR @par_Conductor IS NULL)    
END              
GO