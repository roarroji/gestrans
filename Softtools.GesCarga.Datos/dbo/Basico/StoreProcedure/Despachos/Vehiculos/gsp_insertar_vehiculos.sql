﻿Print 'gsp_insertar_vehiculos'
GO
DROP PROCEDURE gsp_insertar_vehiculos
GO
CREATE PROCEDURE gsp_insertar_vehiculos          
(          
@par_EMPR_Codigo SMALLINT,          
@Codigo NUMERIC = 0,          
@par_Placa varchar(20),          
@par_Codigo_Alterno varchar(20) = null,          
@par_Emergencias SMALLINT = null,          
@par_SEMI_Codigo numeric = null,          
@par_TERC_Codigo_Propietario numeric = null,          
@par_TERC_Codigo_Tenedor numeric = null,          
@par_TERC_Codigo_Conductor numeric = null,          
@par_TERC_Codigo_Afiliador numeric = null,          
@par_COVE_Codigo numeric = null,          
@par_MAVE_Codigo numeric = null,          
@par_LIVE_Codigo numeric = null,          
@par_Modelo smallint = null,          
@par_Modelo_Repotenciado smallint = null,          
@par_CATA_TIVE_Codigo numeric = null,          
@par_Cilindraje numeric = null,          
@par_CATA_TICA_Codigo numeric = null,          
@par_Numero_Ejes smallint = null,          
@par_Numero_Motor varchar(50) = null,          
@par_Numero_Serie varchar(50) = null,          
@par_Peso_Bruto numeric = null,          
@par_Kilometraje numeric = null,          
@par_Fecha_Actuliza_Kilometraje datetime = null,          
@par_ESSE_Numero numeric = null,          
@par_Fecha_Ultimo_Cargue datetime = null,          
@par_Estado smallint = null,          
      
@par_USUA_Codigo_Crea smallint = null,          
@par_CATA_TIDV_Codigo numeric = null,          
@par_Tarjeta_Propiedad varchar(50) = null,          
@par_Capacidad numeric = null,          
@par_Capacidad_Galones numeric = null,          
@par_Peso_Tara numeric = null,          
@par_TERC_Codigo_Proveedor_GPS numeric = null,          
@par_Usuario_GPS varchar(50) = null,          
@par_Clave_GPS varchar(50) = null,          
@par_TelefonoGPS numeric = null,          
@par_CATA_CAIV_Codigo numeric = null,          
@par_Justificacion_Bloqueo varchar (150) = null   ,       
@par_TERC_Codigo_Aseguradora_SOAT numeric = null   ,       
@par_Referencia_SOAT varchar (50) = null   ,       
@par_Fecha_Vencimiento_SOAT DATE = null        ,  
@par_URL_GPS varchar (200) = null          
)          
AS          
BEGIN          
IF EXISTS(SELECT * FROM dbo.Vehiculos where placa = @par_Placa)          
BEGIN          
 select 1 as Codigo          
END          
ELSE          
BEGIN          
 EXEC gsp_generar_consecutivo  @par_EMPR_Codigo, 20, 0, @Codigo OUTPUT            
 INSERT INTO Vehiculos          
      (EMPR_Codigo          
      ,Codigo          
      ,Placa          
      ,Codigo_Alterno          
      
      ,SEMI_Codigo          
      ,TERC_Codigo_Propietario          
      ,TERC_Codigo_Tenedor          
      ,TERC_Codigo_Conductor          
      ,TERC_Codigo_Afiliador          
      ,COVE_Codigo          
      ,MAVE_Codigo          
      ,LIVE_Codigo          
      ,Modelo          
      ,Modelo_Repotenciado          
      ,CATA_TIVE_Codigo          
      ,Cilindraje          
      ,CATA_TICA_Codigo          
      ,Numero_Ejes          
      ,Numero_Motor          
      ,Numero_Serie          
      ,Peso_Bruto          
      ,Kilometraje          
      ,Fecha_Actuliza_Kilometraje          
      ,ESSE_Numero          
      ,Fecha_Ultimo_Cargue          
      ,Estado          
                
      ,USUA_Codigo_Crea          
      ,Fecha_Crea          
      ,CATA_TIDV_Codigo          
      ,Tarjeta_Propiedad          
      ,Capacidad_Kilos          
      ,Capacidad_Galones          
      ,Peso_Tara          
      ,TERC_Codigo_Proveedor_GPS          
      ,Usuario_GPS          
      ,Clave_GPS          
      ,CATA_CAIV_Codigo          
      ,Justificacion_Bloqueo          
      ,Telefono_GPS          
  ,TERC_Codigo_Aseguradora_SOAT     
 ,Referencia_SOAT      
 ,Fecha_Vencimiento_SOAT     
 , URL_GPS  
      )          
   VALUES          
      (@par_EMPR_Codigo          
      ,@Codigo          
      ,@par_Placa          
      ,ISNULL(@par_Codigo_Alterno,'')          
      
      ,ISNULL(@par_SEMI_Codigo,0)          
      ,ISNULL(@par_TERC_Codigo_Propietario,0)          
      ,ISNULL(@par_TERC_Codigo_Tenedor,0)          
    ,ISNULL(@par_TERC_Codigo_Conductor,0)          
      ,ISNULL(@par_TERC_Codigo_Afiliador,0)          
      ,ISNULL(@par_COVE_Codigo,0)          
      ,ISNULL(@par_MAVE_Codigo,0)          
      ,ISNULL(@par_LIVE_Codigo,0)          
      ,ISNULL(@par_Modelo,0)          
      ,@par_Modelo_Repotenciado          
      ,ISNULL(@par_CATA_TIVE_Codigo,2200)          
      ,ISNULL(@par_Cilindraje,0)          
      ,ISNULL(@par_CATA_TICA_Codigo,2300)          
      ,ISNULL(@par_Numero_Ejes,0)          
      ,ISNULL(@par_Numero_Motor,'')          
      ,ISNULL(@par_Numero_Serie,0)          
      ,ISNULL(@par_Peso_Bruto,0)          
      ,ISNULL(@par_Kilometraje,0)          
      ,ISNULL(@par_Fecha_Actuliza_Kilometraje,'01/01/1900')          
      ,ISNULL(@par_ESSE_Numero,0)            ,ISNULL(@par_Fecha_Ultimo_Cargue,'01/01/1900')          
      ,ISNULL(@par_Estado,0)          
                
      ,ISNULL(@par_USUA_Codigo_Crea,0)          
      ,GETDATE()          
      ,ISNULL(@par_CATA_TIDV_Codigo,2100)          
      ,isnull(@par_Tarjeta_Propiedad,'')          
      ,isnull(@par_Capacidad,0)          
      ,@par_Capacidad_Galones          
      ,@par_Peso_Tara          
      ,ISNULL(@par_TERC_Codigo_Proveedor_GPS,0)          
      ,@par_Usuario_GPS          
      ,@par_Clave_GPS          
      ,ISNULL(@par_CATA_CAIV_Codigo,2400)          
      ,@par_Justificacion_Bloqueo          
      ,@par_TelefonoGPS          
        , @par_TERC_Codigo_Aseguradora_SOAT    
  ,@par_Referencia_SOAT    
  ,@par_Fecha_Vencimiento_SOAT    
  ,@par_URL_GPS  
      )          
 select @Codigo as Codigo          
 END          
END          
GO