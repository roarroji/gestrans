﻿PRINT 'gsp_obtener_vehiculo'
GO
DROP PROCEDURE gsp_obtener_vehiculo
GO	
CREATE PROCEDURE gsp_obtener_vehiculo 
(        
@par_EMPR_Codigo SMALLINT,        
@par_Codigo NUMERIC = null,        
@par_Placa VARCHAR(20) = null,      
@par_Emergencias SMALLINT = 0      
)        
AS         
BEGIN        
        
SELECT         
1 AS Obtener,        
VEHI.EMPR_Codigo,        
VEHI.Codigo,        
VEHI.Placa,        
VEHI.Codigo_Alterno,        
VEHI.SEMI_Codigo,  
SEMI.Placa as PlacaSemirremolque,       
VEHI.TERC_Codigo_Propietario,        
VEHI.TERC_Codigo_Tenedor,  
ISNULL(TENE.Razon_Social,'') + ' ' + ISNULL(TENE.Nombre,'') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2,'') As NombreTenedor,       
VEHI.TERC_Codigo_Conductor,   
ISNULL(COND.Razon_Social,'') + ' ' + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') As NombreConductor,   
COND.Numero_Identificacion As IdentificacionConductor,  
ISNULL(COND.Celulares, '') AS Celulares,    
VEHI.TERC_Codigo_Afiliador,        
VEHI.COVE_Codigo,        
VEHI.MAVE_Codigo,        
VEHI.LIVE_Codigo,        
VEHI.Modelo,        
ISNULL(VEHI.Modelo_Repotenciado,0) AS Modelo_Repotenciado,        
VEHI.CATA_TIVE_Codigo,        
TIVE.Campo1 AS TipoVehiculo,        
VEHI.Cilindraje,        
VEHI.CATA_TICA_Codigo,        
VEHI.Numero_Ejes,        
VEHI.Numero_Motor,        
VEHI.Numero_Serie,        
VEHI.Peso_Bruto,        
VEHI.Kilometraje,        
VEHI.Fecha_Actuliza_Kilometraje,        
VEHI.ESSE_Numero,        
Fecha_Ultimo_Cargue,        
VEHI.Estado,        
'' AS EstadoVehiculo,        
ISNULL(VEHI.Tarjeta_Propiedad,0) AS Tarjeta_Propiedad,        
ISNULL(VEHI.Capacidad_Kilos,0) AS Capacidad,        
ISNULL(VEHI.Capacidad_Galones,0) AS CapacidadGalones,        
ISNULL(VEHI.Peso_Tara,0) AS Peso_Tara,        
ISNULL(VEHI.TERC_Codigo_Proveedor_GPS,0) AS TERC_Codigo_Proveedor_GPS,        
ISNULL(VEHI.Usuario_GPS,'') AS Usuario_GPS,        
ISNULL(VEHI.Clave_GPS,'') AS Clave_GPS,        
ISNULL(VEHI.CATA_CAIV_Codigo,2400) AS CATA_CAIV_Codigo,        
ISNULL(VEHI.Justificacion_Bloqueo,'') AS Justificacion_Bloqueo,        
ISNULL(VEHI.Telefono_GPS,0) AS TelefonoGPS,        
ISNULL(VEHI.CATA_TIDV_Codigo,2100) AS CATA_TIDV_Codigo,   
VEHI.TERC_Codigo_Aseguradora_SOAT,  
VEHI.Referencia_SOAT,  
VEHI.Fecha_Vencimiento_SOAT  
FROM         
Vehiculos AS VEHI

LEFT JOIN Semirremolques SEMI ON          
VEHI.EMPR_Codigo = SEMI.EMPR_Codigo            
AND VEHI.SEMI_Codigo = SEMI.Codigo        

LEFT JOIN Valor_Catalogos TIVE ON          
VEHI.EMPR_Codigo = TIVE.EMPR_Codigo            
AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo  

LEFT JOIN Terceros TENE ON          
VEHI.EMPR_Codigo = TENE.EMPR_Codigo            
AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo  

LEFT JOIN Terceros COND ON          
VEHI.EMPR_Codigo = COND.EMPR_Codigo            
AND VEHI.TERC_Codigo_Conductor = COND.Codigo  
        
WHERE VEHI.EMPR_Codigo = @par_EMPR_Codigo        
AND VEHI.Codigo = ISNULL(@par_Codigo,VEHI.Codigo)        
AND VEHI.Placa = ISNULL (@par_Placa,VEHI.Placa)        
            
END        
GO