﻿Print 'gsp_consultar_tipo_tarifa_transporte'
GO
DROP PROCEDURE gsp_consultar_tipo_tarifa_transporte
GO
CREATE PROCEDURE gsp_consultar_tipo_tarifa_transporte  
(  
@par_EMPR_Codigo smallint,  
@par_Estado smallint,
@par_TATC_Codigo smallint = NULL  
)  
AS BEGIN  
	SELECT   
	TTTC.EMPR_Codigo,  
	CONT.Campo1 +DECO.Campo1 +TIVE.Campo1 +CAVE.Campo1+RPVK.Campo1+RPVJ.Campo1  AS Nombre,  
	TTTC.Codigo,  
	TTTC.Estado,  
	TTTC.TATC_Codigo  
	FROM Tipo_Tarifa_Transporte_Carga TTTC  
  
	LEFT JOIN Valor_Catalogos AS CONT  
	ON TTTC.EMPR_Codigo = CONT.EMPR_Codigo  
	AND TTTC.CATA_CONT_Codigo = CONT.Codigo  
  
	LEFT JOIN Valor_Catalogos AS DECO  
	ON TTTC.EMPR_Codigo = DECO.EMPR_Codigo  
	AND TTTC.CATA_DECO_Codigo = DECO.Codigo  
  
	LEFT JOIN Valor_Catalogos AS TIVE  
	ON TTTC.EMPR_Codigo = TIVE.EMPR_Codigo  
	AND TTTC.CATA_TIVE_Codigo = TIVE.Codigo  
  
	LEFT JOIN Valor_Catalogos AS CAVE  
	ON TTTC.EMPR_Codigo = CAVE.EMPR_Codigo  
	AND TTTC.CATA_CAVE_Codigo = CAVE.Codigo  
  
	LEFT JOIN Valor_Catalogos AS RPVK  
	ON TTTC.EMPR_Codigo = RPVK.EMPR_Codigo  
	AND TTTC.CATA_RPVK_Codigo = RPVK.Codigo  
  
	LEFT JOIN Valor_Catalogos AS RPVJ  
	ON TTTC.EMPR_Codigo = RPVJ.EMPR_Codigo  
	AND TTTC.CATA_RPVJ_Codigo = RPVJ.Codigo  
  
	WHERE TTTC.EMPR_Codigo = @par_EMPR_Codigo  
	and TTTC.Estado = @par_Estado  
	AND TTTC.TATC_Codigo = ISNULL(@par_TATC_Codigo, TTTC.TATC_Codigo)
END  
GO