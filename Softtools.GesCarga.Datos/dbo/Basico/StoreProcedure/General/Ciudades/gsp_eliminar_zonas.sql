﻿PRINT 'gsp_eliminar_zonas'
GO
DROP PROCEDURE gsp_eliminar_zonas
GO
CREATE PROCEDURE gsp_eliminar_zonas(  
 @par_EMPR_Codigo SMALLINT,    
 @par_Codigo SMALLINT = NULL,
 @par_CIUD_Codigo SMALLINT = NULL  
  )  
AS  
BEGIN  
    
  DELETE Zona_Ciudades   
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
    AND Codigo = @par_Codigo  
    AND CIUD_Codigo = @par_CIUD_Codigo  
  
    SELECT @@ROWCOUNT AS Codigo  
  
END  
  
GO