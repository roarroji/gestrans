﻿PRINT 'gsp_insertar_zonas'
GO
DROP PROCEDURE gsp_insertar_zonas
GO
CREATE PROCEDURE  gsp_insertar_zonas 
(  
@par_EMPR_Codigo SMALLINT,   
@par_Codigo NUMERIC, 
@par_CIUD_Codigo SMALLINT, 
@par_Nombre VARCHAR(50),  
@par_Estado SMALLINT,  
@par_USUA_Codigo_Crea SMALLINT,
@par_USUA_Modifica SMALLINT   
)  
AS   
BEGIN   

IF @par_Codigo <= 0
BEGIN

 INSERT INTO Zona_Ciudades  
 (  
 EMPR_Codigo,  
 CIUD_Codigo,
 Nombre,
 Estado,
 USUA_Codigo_Crea,  
 Fecha_Crea  
 )  
 VALUES  
 (  
 @par_EMPR_Codigo, 
 @par_CIUD_Codigo,
 @par_Nombre,  
 @par_Estado,  
 @par_USUA_Codigo_Crea,  
 GETDATE()  
 )  
  
SELECT @@IDENTITY AS Codigo  
  END
  ELSE
  BEGIN
   UPDATE Zona_Ciudades SET  
 CIUD_Codigo = @par_CIUD_Codigo,  
 Nombre = @par_Nombre, 
 Estado = @par_Estado,  
 USUA_Codigo_Modifica = @par_USUA_Modifica,  
 Fecha_Modifica = GETDATE()  
 WHERE   
 EMPR_Codigo = @par_EMPR_Codigo   
 AND Codigo = @par_Codigo  
  
SELECT Codigo   
FROM  
Zona_Ciudades  
WHERE   
EMPR_Codigo = @par_EMPR_Codigo   
AND Codigo = @par_Codigo   
  END
END  
GO