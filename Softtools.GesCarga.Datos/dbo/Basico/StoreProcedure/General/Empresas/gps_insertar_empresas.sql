﻿DROP PROCEDURE gps_insertar_empresas
GO
CREATE PROCEDURE gps_insertar_empresas(       
@par_Codigo_Alterno VARCHAR(20) = NULL,        
@par_Nombre_Razon_Social VARCHAR(100),      
@par_Nombre_Corto_Razon_Social VARCHAR(50),      
@par_CATA_TIID_Codigo NUMERIC,        
@par_Numero_Identificacion VARCHAR(30),        
@par_Digito_Chequeo SMALLINT,        
@par_PAIS_Codigo NUMERIC,        
@par_CIUD_Codigo NUMERIC,        
@par_Direccion VARCHAR(150),        
@par_Telefonos VARCHAR(100),        
@par_Codigo_Postal VARCHAR(20) = NULL,        
@par_Emails VARCHAR(250) = NULL,        
@par_Pagina_Web VARCHAR(150),         
@par_Mensaje_Banner VARCHAR(350),
@par_Aceptacion_Electronica numeric = NULL
)         
AS BEGIN         
        
DECLARE @intCodigo NUMERIC = (select isnull(MAX(Codigo), 0) + 1 from Empresas)        
           
INSERT INTO Empresas(        
  Codigo,      
  Codigo_Alterno,        
  Nombre_Razon_Social,        
  Nombre_Corto_Razon_Social,      
  CATA_TIID_Codigo,        
  Numero_Identificacion,        
  Digito_Chequeo,        
  PAIS_Codigo,        
  CIUD_Codigo,        
  Direccion,        
  Telefonos,        
  Codigo_Postal,        
  Emails,        
  Pagina_Web,        
  Mensaje_Banner,
  Aceptacion_Electronica
)          
  VALUES      
(        
  @intCodigo,      
  @par_Codigo_Alterno,        
  @par_Nombre_Razon_Social,        
  @par_Nombre_Corto_Razon_Social,      
  @par_CATA_TIID_Codigo,        
  @par_Numero_Identificacion,        
  @par_Digito_Chequeo,        
  @par_PAIS_Codigo,        
  @par_CIUD_Codigo,        
  @par_Direccion,        
  @par_Telefonos,        
  @par_Codigo_Postal,        
  @par_Emails,        
  @par_Pagina_Web,          
  @par_Mensaje_Banner,
  @par_Aceptacion_Electronica
)        
      
  SELECT @intCodigo AS Codigo        
         
END  
GO