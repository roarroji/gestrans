﻿DROP PROCEDURE gps_obtener_empresas
GO
CREATE PROCEDURE gps_obtener_empresas             
(                        
@par_Codigo SMALLINT = NULL                
)                    
AS                    
BEGIN                    
 SELECT                   
  2 AS Obtener,                 
  EMPR.Codigo,                    
  ISNULL(EMPR.Codigo_Alterno, '') AS Codigo_Alterno,                    
  EMPR.Nombre_Razon_Social,                       
  ISNULL(EMPR.Nombre_Corto_Razon_Social,'') AS Nombre_Corto_Razon_Social,                
  ISNULL(EMPR.CATA_TIID_Codigo, 0) AS CATA_TIID_Codigo,                 
  ISNULL(EMPR.Numero_Identificacion,'') AS Numero_Identificacion,                       
  ISNULL(EMPR.Digito_Chequeo, 0) AS Digito_Chequeo,                
  ISNULL(EMPR.PAIS_Codigo, 0) AS PAIS_Codigo,                     
  ISNULL(EMPR.CIUD_Codigo, 0) AS CIUD_Codigo,                  
  ISNULL(EMPR.Direccion, '') AS Direccion,                
  ISNULL(EMPR.Telefonos, '') AS Telefonos,                  
  ISNULL(EMPR.Codigo_Postal,'') AS Codigo_Postal,                
  ISNULL(EMPR.Emails, '') AS Email,                       
  ISNULL(EMPR.Pagina_Web,'') AS Pagina_Web,                
  ISNULL(EMPR.Estado, 0) AS Estado,                       
  ISNULL(EMPR.Mensaje_Banner, '') AS Mensaje_Banner,                
  ISNULL(EMPR.Aprobar_Liquidacion_Despachos, 0) AS Aprobar_Liquidacion_Despachos  ,              
  ISNULL(EMPR.Reportar_RNDC,0) AS ReportaRNDC  ,        
  TERC_Codigo_Aseguradora,        
Numero_Poliza,        
Fecha_Vencimiento_Poliza,      
Valor_Poliza,    
Valor_Galon_Combustilbe  ,  
EMPR.Codigo_UIAF  , 
ISNULL(EMPR.Aceptacion_Electronica,0) AS AceptacionElectronica
 FROM                   
                  
 Empresas AS EMPR                 
                       
 WHERE                    
  EMPR.Codigo = @par_Codigo                  
END  
GO