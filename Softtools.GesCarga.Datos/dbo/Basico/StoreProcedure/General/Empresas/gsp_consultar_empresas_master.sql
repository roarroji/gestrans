﻿PRINT 'gsp_consultar_empresas_master'
GO
DROP PROCEDURE gsp_consultar_empresas_master
GO 
CREATE PROCEDURE gsp_consultar_empresas_master    
(    
  @par_Codigo SMALLINT = NULL,        
  @par_Codigo_Alterno VARCHAR(20) = NULL,        
  @par_Razon_Social VARCHAR(100) = NULL,           
  @par_NumeroIdentificacion VARCHAR(30) = NULL,      
  @par_Nombre_Ciudad VARCHAR(100) = NULL    
)    
AS     
BEGIN    
      
    SELECT     
     1 AS Obtener,    
  EMPR.Codigo,     
  ISNULL(EMPR.Codigo_Alterno, '') AS Codigo_Alterno,    
  EMPR.Nombre_Razon_Social,   
  EMPR.Nombre_Corto_Razon_Social,       
  EMPR.Numero_Identificacion,      
  EMPR.Digito_Chequeo,      
  EMPR.CIUD_Codigo,       
        CIUD.Nombre AS Nombre_Ciudad,       
        EMPR.Telefonos,    
  ISNULL(Prefijo,'') AS Prefijo    
        FROM Empresas AS EMPR    
     INNER JOIN Ciudades AS CIUD                       
        ON EMPR.Codigo = CIUD.EMPR_Codigo                        
        AND EMPR.CIUD_Codigo = CIUD.Codigo       
  WHERE    
  EMPR.Codigo = ISNULL(@par_Codigo, EMPR.Codigo)           
        AND ((EMPR.Codigo_Alterno LIKE '%' + RTRIM(LTRIM(@par_Codigo_Alterno)) + '%' OR (@par_Codigo_Alterno IS NULL)))       
        AND ((EMPR.Nombre_Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Razon_Social)) + '%' OR (@par_Razon_Social IS NULL)))       
        AND ((EMPR.Numero_Identificacion LIKE '%' + RTRIM(LTRIM(@par_NumeroIdentificacion)) + '%' OR (@par_NumeroIdentificacion IS NULL)))     
        AND ((CIUD.Nombre LIKE '%' + RTRIM(LTRIM(@par_Nombre_Ciudad)) + '%' OR (@par_Nombre_Ciudad IS NULL)))     
    
END    
GO