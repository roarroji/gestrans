﻿PRINT 'gsp_insertar_lista_rndc_empresa'
GO
DROP PROCEDURE gsp_insertar_lista_rndc_empresa
GO   
CREATE PROCEDURE gsp_insertar_lista_rndc_empresa 
(@par_EMPR_Codigo SMALLINT,      
@par_Codigo_Regional_Ministerio VARCHAR(50) = NULL,     
@par_Codigo_Empresa_Ministerio NUMERIC = NULL,     
@par_Usuario_Manifiesto_Electronico VARCHAR(50) = NULL,      
@par_Numero_Resolucion_Habilitacion_Empresa NUMERIC = NULL,    
@par_Clave_Manifiesto_Electronico VARCHAR(50) = NULL    
)      
AS      
BEGIN      
      
INSERT INTO [dbo].Empresa_Manifiesto_Electronico      
           (EMPR_Codigo    
           ,Codigo_Regional_Ministerio      
           ,Codigo_Empresa_Ministerio    
           ,Usuario_Manifiesto_Electronico    
           ,Numero_Resolucion_Habilitacion_Empresa    
           ,Clave_Manifiesto_Electronico      
         )      
     VALUES      
           (@par_EMPR_Codigo     
           ,@par_Codigo_Regional_Ministerio      
           ,@par_Codigo_Empresa_Ministerio      
           ,@par_Usuario_Manifiesto_Electronico      
           ,@par_Numero_Resolucion_Habilitacion_Empresa    
           ,@par_Clave_Manifiesto_Electronico)  		    
    
END      
GO 