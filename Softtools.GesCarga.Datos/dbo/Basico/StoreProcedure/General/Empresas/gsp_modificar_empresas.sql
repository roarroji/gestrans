﻿DROP PROCEDURE gsp_modificar_empresas
GO
CREATE PROCEDURE gsp_modificar_empresas        
(        
@par_Codigo SMALLINT,                 
@par_Codigo_Alterno VARCHAR(20) = NULL,                
@par_Nombre_Razon_Social VARCHAR(100),                
@par_Nombre_Corto_Razon_Social VARCHAR(50),              
@par_CATA_TIID_Codigo NUMERIC,                
@par_Numero_Identificacion VARCHAR(30),                
@par_Digito_Chequeo SMALLINT,                
@par_PAIS_Codigo NUMERIC,                
@par_CIUD_Codigo NUMERIC,                
@par_Direccion VARCHAR(150),                
@par_Telefonos VARCHAR(100),                
@par_Codigo_Postal VARCHAR(20) = NULL,                
@par_Emails VARCHAR(250) = NULL,                
@par_Pagina_Web VARCHAR(150),                   
@par_Mensaje_Banner VARCHAR(350),              
@par_USUA_Codigo_Modifica SMALLINT  ,         
@par_TERC_Codigo_Aseguradora numeric= NULL,          
@par_Numero_Poliza varchar(50)= NULL,          
@par_Fecha_Vencimiento_Poliza date  = NULL,      
@par_Valor_Poliza numeric= NULL,          
@par_Valor_Combustible numeric= NULL ,  
@par_CodigoUIAF numeric = NULL ,
@par_Aceptacion_Electronica numeric = NULL
  )                
AS BEGIN                   
        
UPDATE Empresas  SET                
Codigo_Alterno = @par_Codigo_Alterno ,                
Nombre_Razon_Social = @par_Nombre_Razon_Social ,               
Nombre_Corto_Razon_Social = @par_Nombre_Corto_Razon_Social,               
CATA_TIID_Codigo = @par_CATA_TIID_Codigo ,                
Numero_Identificacion = @par_Numero_Identificacion ,                 
Digito_Chequeo =  @par_Digito_Chequeo ,                
PAIS_Codigo = @par_PAIS_Codigo ,                
CIUD_Codigo = @par_CIUD_Codigo ,                
Direccion = @par_Direccion ,                
Telefonos = @par_Telefonos ,                
Codigo_Postal = @par_Codigo_Postal ,                
Emails = @par_Emails ,                
Pagina_Web = @par_Pagina_Web ,                
Mensaje_Banner = @par_Mensaje_Banner ,                
USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica ,                
Fecha_Modifica = GETDATE()      ,        
TERC_Codigo_Aseguradora = @par_TERC_Codigo_Aseguradora,        
Numero_Poliza = @par_Numero_Poliza,        
Fecha_Vencimiento_Poliza = @par_Fecha_Vencimiento_Poliza,        
Valor_Poliza = @par_Valor_Poliza,        
Valor_Galon_Combustilbe = @par_Valor_Combustible ,       
Codigo_UIAF = @par_CodigoUIAF ,
Aceptacion_Electronica = @par_Aceptacion_Electronica 
WHERE                
 Codigo = @par_Codigo                  
                
 SELECT @par_Codigo as Codigo                 
END
GO