﻿PRINT 'gsp_consultar_encabezado_formularios_inspecciones'
GO
DROP PROCEDURE gsp_consultar_encabezado_formularios_inspecciones
GO
CREATE PROCEDURE gsp_consultar_encabezado_formularios_inspecciones 
(  
  @par_EMPR_Codigo SMALLINT,  
  @par_Codigo NUMERIC = NULL,  
  @par_Nombre VARCHAR(50) = NULL,  
  @par_Codigo_Alterno VARCHAR(20) = NULL,  
  @par_Estado SMALLINT = NULL,  
  @par_TIDO_Codigo_Origen SMALLINT = NULL, 
  @par_NumeroPagina INT = NULL,  
  @par_RegistrosPagina INT = NULL  
 )  
 AS  
 BEGIN  
  SET NOCOUNT ON;  
   DECLARE  
    @CantidadRegistros INT  
   SELECT @CantidadRegistros = (  
     SELECT DISTINCT   
      COUNT(1)   
      FROM  
      Encabezado_Formularios_Inspecciones ENFI 
  
      LEFT JOIN Valor_Catalogos TIDO ON
	  ENFI.EMPR_Codigo = TIDO.EMPR_Codigo
	  AND ENFI.TIDO_Codigo_Origen = TIDO.Codigo
      WHERE  
      ENFI.EMPR_Codigo = @par_EMPR_Codigo  
      AND ENFI.Codigo > 0  
      AND ENFI.Codigo = ISNULL(@par_Codigo, ENFI.Codigo)  
      AND ENFI.Estado = ISNULL(@par_Estado, ENFI.Estado)  
	  AND ENFI.TIDO_Codigo_Origen = ISNULL(@par_TIDO_Codigo_Origen, ENFI.TIDO_Codigo_Origen) 
      AND ((ENFI.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  
      AND ((ENFI.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))  
      );  
              
     WITH Pagina AS  
     (  
  
      SELECT   
      ENFI.EMPR_Codigo,  
      ENFI.Codigo,  
      ENFI.Codigo_Alterno,  
      ENFI.Nombre,  
      ENFI.Estado,  
      ENFI.Version,  
      ENFI.TIDO_Codigo_Origen,
	  TIDO.Campo1 AS Nombre_Tipo_Documento_Origen,  
      ROW_NUMBER() OVER(ORDER BY ENFI.Nombre) AS RowNumber  
      FROM  
      Encabezado_Formularios_Inspecciones ENFI
	  
	  LEFT JOIN Valor_Catalogos TIDO ON
	  ENFI.EMPR_Codigo = TIDO.EMPR_Codigo
	  AND ENFI.TIDO_Codigo_Origen = TIDO.Codigo 
      WHERE  
      ENFI.EMPR_Codigo = @par_EMPR_Codigo  
      AND ENFI.Codigo > 0  
      AND ENFI.Codigo = ISNULL(@par_Codigo, ENFI.Codigo)  
      AND ENFI.Estado = ISNULL(@par_Estado, ENFI.Estado) 
	  AND ENFI.TIDO_Codigo_Origen = ISNULL(@par_TIDO_Codigo_Origen, ENFI.TIDO_Codigo_Origen)  
      AND ((ENFI.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  
      AND ((ENFI.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))  
   )  
   SELECT DISTINCT  
   0 AS Obtener,  
   EMPR_Codigo,  
   Codigo,  
   Codigo_Alterno,  
   Nombre,  
   Estado,  
   Version,  
   TIDO_Codigo_Origen, 
   Nombre_Tipo_Documento_Origen, 
   @CantidadRegistros AS TotalRegistros,  
   @par_NumeroPagina AS PaginaObtener,  
   @par_RegistrosPagina AS RegistrosPagina  
   FROM  
    Pagina  
   WHERE  
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
   order by Nombre  
   END  
GO