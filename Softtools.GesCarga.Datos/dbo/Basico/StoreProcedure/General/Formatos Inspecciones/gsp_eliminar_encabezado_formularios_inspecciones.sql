﻿PRINT 'gsp_eliminar_encabezado_formularios_inspecciones'
GO
DROP PROCEDURE gsp_eliminar_encabezado_formularios_inspecciones
GO
CREATE PROCEDURE gsp_eliminar_encabezado_formularios_inspecciones 
(  
  @par_EMPR_Codigo SMALLINT,  
  @par_Codigo NUMERIC  
)  
AS  
BEGIN  
  
  DELETE Detalle_Items_Formularios_Inspecciones   
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
    AND ENFI_Codigo = @par_Codigo  
  
  DELETE Detalle_Secciones_Formularios_Inspecciones   
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
    AND ENFI_Codigo = @par_Codigo  
  
  DELETE Encabezado_Formularios_Inspecciones   
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
    AND Codigo = @par_Codigo  
  
    SELECT @@ROWCOUNT AS Codigo  
END  
GO