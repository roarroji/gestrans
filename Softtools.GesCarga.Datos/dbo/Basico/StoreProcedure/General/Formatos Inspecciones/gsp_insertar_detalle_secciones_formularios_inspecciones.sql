﻿PRINT 'gsp_insertar_detalle_secciones_formularios_inspecciones'
GO
DROP PROCEDURE gsp_insertar_detalle_secciones_formularios_inspecciones
GO
CREATE PROCEDURE gsp_insertar_detalle_secciones_formularios_inspecciones 
(  
 @par_EMPR_Codigo SMALLINT,  
 @par_ENFI_Codigo NUMERIC,  
 @par_Orden_Formulario SMALLINT,  
 @par_Nombre VARCHAR (50),  
 @par_Estado SMALLINT,  
 @par_USUA_Codigo_Crea SMALLINT   
)  
AS  
BEGIN  
INSERT INTO Detalle_Secciones_Formularios_Inspecciones	  
(  
EMPR_Codigo,  
ENFI_Codigo,  
Orden_Formulario,  
Nombre,  
Estado,  
Fecha_Crea,  
USUA_Codigo_Crea  
)   
VALUES   
(  
@par_EMPR_Codigo,  
@par_ENFI_Codigo,  
@par_Orden_Formulario,  
@par_Nombre,  
@par_Estado,  
GETDATE(),  
@par_USUA_Codigo_Crea  
)  
  
SELECT @@IDENTITY AS Codigo   
  
END  
GO