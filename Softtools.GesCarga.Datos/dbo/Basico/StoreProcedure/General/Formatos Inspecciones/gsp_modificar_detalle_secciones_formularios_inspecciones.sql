﻿PRINT 'gsp_modificar_detalle_secciones_formularios_inspecciones'
GO
DROP PROCEDURE gsp_modificar_detalle_secciones_formularios_inspecciones
GO
CREATE PROCEDURE gsp_modificar_detalle_secciones_formularios_inspecciones 
(  
 @par_EMPR_Codigo SMALLINT,  
 @par_Codigo NUMERIC,  
 @par_ENFI_Codigo NUMERIC,  
 @par_Orden_Formulario SMALLINT,  
 @par_Nombre VARCHAR (50),  
 @par_Estado SMALLINT,  
 @par_USUA_Codigo_Modifica SMALLINT   
)  
AS  
BEGIN  
  
IF  @par_Codigo > 0  
 BEGIN  
  
 UPDATE Detalle_Secciones_Formularios_Inspecciones  
 SET  
 ENFI_Codigo = @par_ENFI_Codigo,  
 Orden_Formulario =  @par_Orden_Formulario,  
 Nombre = @par_Nombre,  
 Estado = @par_Estado,  
 USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica,  
 Fecha_Modifica = GETDATE()  
 WHERE   
 EMPR_Codigo = @par_EMPR_Codigo  
 AND Codigo = @par_Codigo  
  
 SELECT @par_Codigo AS Codigo  
  
 END  
 ELSE   
 BEGIN  
  
 INSERT INTO Detalle_Secciones_Formularios_Inspecciones  
 (  
 EMPR_Codigo,  
 ENFI_Codigo,  
 Orden_Formulario,  
 Nombre,  
 Estado,  
 Fecha_Crea,  
 USUA_Codigo_Crea  
 )   
 VALUES   
 (  
 @par_EMPR_Codigo,  
 @par_ENFI_Codigo,  
 @par_Orden_Formulario,  
 @par_Nombre,  
 @par_Estado,  
 GETDATE(),  
 @par_USUA_Codigo_Modifica  
 )  
  
 SELECT @@IDENTITY AS Codigo   
 END  
END  
GO