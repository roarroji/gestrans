﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 13/08/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_obtener_paises'
GO
DROP PROCEDURE gsp_obtener_paises
GO
CREATE PROCEDURE gsp_obtener_paises 
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo NUMERIC  
)  
AS  
BEGIN  
SELECT   
1 AS Obtener,  
EMPR_Codigo,  
Codigo,  
Codigo_Alterno,  
Nombre,  
MONE_Codigo,  
Estado,  
ISNULL(Nombre_Corto, '') AS Nombre_Corto 
FROM  
Paises  
WHERE   
EMPR_Codigo = @par_EMPR_Codigo  
AND Codigo = @par_Codigo  
  
END  
GO
