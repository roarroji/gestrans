﻿PRINT 'gsp_consultar_producto_transportados'
GO
DROP PROCEDURE gsp_consultar_producto_transportados
GO
CREATE PROCEDURE gsp_consultar_producto_transportados 
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo INT = NULL,  
@par_Codigo_Alterno VARCHAR(20) = NULL,  
@par_Nombre VARCHAR(100) = NULL, 
@par_Estado INT = NULL,  
@par_NumeroPagina INT =NULL,
@par_RegistrosPagina INT = NULL
)  
AS  
BEGIN  
SET NOCOUNT ON;  
 DECLARE  
  @CantidadRegistros INT  
 SELECT @CantidadRegistros = (  
   SELECT DISTINCT   
    COUNT(1)   
   FROM  
   Producto_Transportados PRTR LEFT JOIN Productos_Ministerio_Transporte PRMT ON
   PRTR.EMPR_Codigo = PRMT.EMPR_Codigo
   AND PRTR.Codigo_Alterno = PRMT.Codigo
  WHERE  
    PRTR.EMPR_Codigo = @par_EMPR_Codigo
	AND PRTR.Codigo = ISNULL(@par_Codigo, PRTR.Codigo)
	AND PRTR.Codigo_Alterno = ISNULL(@par_Codigo_Alterno, PRTR.Codigo_Alterno) 
	AND ((PRTR.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  
    AND PRTR.Estado = ISNULL(@par_Estado, PRTR.Estado)  
    );  
              
   WITH Pagina AS  
   (  
  
   SELECT  
    0 AS Obtener,  
    PRTR.EMPR_Codigo,  
    PRTR.Codigo,  
	PRTR.Codigo_Alterno, 
	PRMT.Nombre AS Producto_Ministerio,
	PRTR.Nombre,
	PRTR.Descripcion,
	PRTR.UMPT_Codigo AS Unidad_Medida,
	PRTR.UEPT_Codigo AS Unidad_Empaque,
	PRTR.CATA_LIPT_Codigo AS Linea_Producto,
	PRTR.CATA_NAPT_Codigo AS Naturaleza_Producto,     
    PRTR.Estado,    
    ROW_NUMBER() OVER(ORDER BY PRTR.Nombre) AS RowNumber 
   FROM  
    Producto_Transportados PRTR LEFT JOIN Productos_Ministerio_Transporte PRMT ON
    PRTR.EMPR_Codigo = PRMT.EMPR_Codigo
    AND PRTR.Codigo_Alterno = PRMT.Codigo
   WHERE  
    PRTR.EMPR_Codigo = @par_EMPR_Codigo
	AND PRTR.Codigo = ISNULL(@par_Codigo, PRTR.Codigo)
	AND PRTR.Codigo_Alterno = ISNULL(@par_Codigo_Alterno, PRTR.Codigo_Alterno) 
	AND ((PRTR.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  
    AND PRTR.Estado = ISNULL(@par_Estado, PRTR.Estado)  
       
 )  
 SELECT DISTINCT  
 0 AS Obtener,  
 EMPR_Codigo,  
 Codigo, 
 Codigo_Alterno,
 Producto_Ministerio, 
 Nombre,  
 Descripcion,
 Unidad_Medida,
 Unidad_Empaque,
 Linea_Producto,
 Naturaleza_Producto,    
 Estado,     
 @CantidadRegistros AS TotalRegistros,  
 @par_NumeroPagina AS PaginaObtener,  
 @par_RegistrosPagina AS RegistrosPagina  
 FROM  
  Pagina  
 WHERE  
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
 order by Nombre  
  
END  
GO