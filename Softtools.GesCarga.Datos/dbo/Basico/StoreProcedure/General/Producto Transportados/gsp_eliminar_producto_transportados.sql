﻿PRINT 'gsp_eliminar_producto_transportados'
GO
DROP PROCEDURE gsp_eliminar_producto_transportados
GO
CREATE PROCEDURE gsp_eliminar_producto_transportados(  
  @par_EMPR_Codigo SMALLINT,  
  @par_Codigo NUMERIC  
  )  
AS  
BEGIN  
    
  DELETE Producto_Transportados 
    
  WHERE EMPR_Codigo = @par_EMPR_Codigo   
    AND Codigo = @par_Codigo  
  
    SELECT @@ROWCOUNT AS Codigo  
  
END  
GO