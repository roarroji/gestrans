﻿PRINT 'gsp_modificar_producto_transportados'
GO
DROP PROCEDURE gsp_modificar_producto_transportados
GO
CREATE PROCEDURE gsp_modificar_producto_transportados  
(  
 @par_EMPR_Codigo SMALLINT,  
 @par_Codigo NUMERIC,  
 @par_Codigo_Alterno NUMERIC,  
 @par_Nombre VARCHAR(100),  
 @par_Descripcion VARCHAR(250),   
 @par_UMPT_Codigo NUMERIC,  
 @par_UEPT_Codigo NUMERIC,  
 @par_CATA_LIPT_Codigo NUMERIC,  
 @par_CATA_NAPT_Codigo NUMERIC,  
 @par_Estado SMALLINT,  
 @par_USUA_Codigo_Modifica SMALLINT  
)  
AS  
BEGIN  
 UPDATE  
  Producto_Transportados  
 SET  
  EMPR_Codigo = @par_EMPR_Codigo,
  Codigo_Alterno = @par_Codigo_Alterno,
  Nombre = @par_Nombre,
  Descripcion = @par_Descripcion,
  UMPT_Codigo = @par_UMPT_Codigo,
  UEPT_Codigo = @par_UEPT_Codigo,
  CATA_LIPT_Codigo = @par_CATA_LIPT_Codigo,
  CATA_NAPT_Codigo = @par_CATA_NAPT_Codigo,
  Estado = @par_Estado,  
  Fecha_Modifica = GETDATE(),
  USUA_Modifica = @par_USUA_Codigo_Modifica
 WHERE  
  EMPR_Codigo = @par_EMPR_codigo 
  AND Codigo = @par_Codigo    
  SELECT @par_Codigo AS Codigo  
  
END  
GO