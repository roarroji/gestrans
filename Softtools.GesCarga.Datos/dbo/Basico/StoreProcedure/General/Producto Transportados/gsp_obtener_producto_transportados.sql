﻿PRINT 'gsp_obtener_producto_transportados'
GO
DROP PROCEDURE gsp_obtener_producto_transportados
GO
CREATE PROCEDURE gsp_obtener_producto_transportados  
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo NUMERIC  
)  
AS  
BEGIN  
 SELECT  
  1 AS Obtener,  
  PRTR.EMPR_Codigo,
  PRTR.Codigo,
  PRTR.Codigo_Alterno,
  PRMT.Nombre AS Producto_Ministerio,
  0 AS TotalRegistros,
  PRTR.Nombre,
  PRTR.Descripcion,
  PRTR.UMPT_Codigo AS Unidad_Medida,
  PRTR.UEPT_Codigo AS Unidad_Empaque,
  PRTR.CATA_LIPT_Codigo AS Linea_Producto,
  PRTR.CATA_NAPT_Codigo AS Naturaleza_Producto,
  PRTR.Estado
  
 FROM  
  Producto_Transportados PRTR LEFT JOIN Productos_Ministerio_Transporte PRMT ON
  PRTR.EMPR_Codigo = PRMT.EMPR_Codigo
  AND PRTR.Codigo_Alterno = PRMT.Codigo

 WHERE  
  PRTR.EMPR_Codigo = @par_EMPR_Codigo
  AND PRTR.Codigo = @par_Codigo  
  
END  
GO