﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 04/10/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_modificar_puertos_paises'
GO
DROP PROCEDURE gsp_modificar_puertos_paises
GO
CREATE PROCEDURE gsp_modificar_puertos_paises 
(         
@par_EMPR_Codigo SMALLINT, 
@par_Codigo SMALLINT,     
@par_PAIS_Codigo NUMERIC,          
@par_Nombre VARCHAR(50),
@par_Contacto VARCHAR(50),     
@par_Nombre_Ciudad VARCHAR(50),       
@par_Direccion VARCHAR(150),        
@par_Telefono VARCHAR(20),   
@par_Observaciones VARCHAR(500),         
@par_Estado SMALLINT,        
@par_USUA_Modifica SMALLINT     
)        
AS         
BEGIN         
 UPDATE Puertos_Paises        
 SET        
  EMPR_Codigo = @par_EMPR_Codigo,
  PAIS_Codigo = @par_PAIS_Codigo,              
  Nombre = @par_Nombre,        
  Contacto = @par_Contacto,
  Ciudad = @par_Nombre_Ciudad,        
  Direccion = @par_Direccion,        
  Telefono = @par_Telefono,  
  Observaciones = @par_Observaciones,           
  Estado = @par_Estado,            
  USUA_Codigo_Modifica = @par_USUA_Modifica,        
  Fecha_Modifica = GETDATE()        
 WHERE         
 EMPR_Codigo = @par_EMPR_Codigo        
 AND Codigo = @par_Codigo        
        
 SELECT @par_Codigo AS Codigo        
END        
GO