﻿	/*Crea: Geferson Latorre
	Fecha_Crea: 04/10/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	

PRINT 'gsp_obtener_puertos_paises'
GO
DROP PROCEDURE gsp_obtener_puertos_paises
GO
CREATE PROCEDURE gsp_obtener_puertos_paises 
(        
@par_EMPR_Codigo SMALLINT,        
@par_Codigo NUMERIC      
)        
AS         
BEGIN         
 SELECT         
  1 AS Obtener,        
  PUPA.EMPR_Codigo,        
  PUPA.Codigo,               
  PUPA.Nombre,   
  PUPA.Contacto,      
  PUPA.PAIS_Codigo,
  PAIS.Nombre AS Nombre_Pais,  
  ISNULL(PUPA.Ciudad, '') AS Nombre_Ciudad,        
  ISNULL(PUPA.Direccion, '') AS Direccion,        
  ISNULL(PUPA.Telefono, '') AS Telefono,  
  ISNULL(PUPA.Observaciones, '') AS Observaciones,        
  PUPA.Estado       
  FROM        
  Puertos_Paises PUPA       
	    
  LEFT JOIN Paises PAIS    ON     
  PUPA.EMPR_Codigo = PAIS.EMPR_Codigo      
  AND PUPA.PAIS_Codigo = PAIS.Codigo  
        
  WHERE    
   PUPA.EMPR_Codigo = @par_EMPR_Codigo             
   AND PUPA.Codigo = @par_Codigo        
END       

GO