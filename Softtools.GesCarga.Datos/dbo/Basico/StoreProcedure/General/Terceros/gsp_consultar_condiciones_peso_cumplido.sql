﻿PRINT 'gsp_consultar_condiciones_peso_cumplido'
GO
DROP PROCEDURE gsp_consultar_condiciones_peso_cumplido
GO
CREATE PROCEDURE gsp_consultar_condiciones_peso_cumplido
(
@par_EMPR_Codigo SMALLINT,        
@par_TERC_Codigo NUMERIC
)        
        
AS      
BEGIN      
SELECT    
    
TCPC.EMPR_Codigo,  
TCPC.TERC_Codigo,  
TCPC.PRTR_Codigo,  
PRTR.Nombre AS PRTR_Nombre,
TCPC.UMPT_Codigo,
UMPT.Descripcion AS UMPT_Nombre,
TCPC.CATA_BCPC_Codigo,
CATA_BCPC.Campo1 AS CATA_BCPC_Nombre,
TCPC.Valor_Unidad,  
TCPC.CATA_TCPC_Codigo,
CATA_TCPC.Campo1 AS CATA_TCPC_Nombre,
TCPC.Tolerancia,  
TCPC.CATA_CCPC_Codigo,
CATA_CCPC.Campo1 AS CATA_CCPC_Nombre
    
FROM     
    
Tercero_Clientes_Condiciones_Peso_Cumplido TCPC

LEFT JOIN Producto_Transportados PRTR ON
TCPC.EMPR_Codigo = PRTR.EMPR_Codigo
AND TCPC.PRTR_Codigo = PRTR.Codigo

LEFT JOIN Unidad_Medida_Producto_Transportados UMPT ON
TCPC.EMPR_Codigo = UMPT.EMPR_Codigo
AND TCPC.UMPT_Codigo = UMPT.Codigo

LEFT JOIN Valor_Catalogos CATA_BCPC ON
TCPC.EMPR_Codigo = CATA_BCPC.EMPR_Codigo
AND TCPC.CATA_BCPC_Codigo = CATA_BCPC.Codigo

LEFT JOIN Valor_Catalogos CATA_TCPC ON
TCPC.EMPR_Codigo = CATA_TCPC.EMPR_Codigo
AND TCPC.CATA_TCPC_Codigo = CATA_TCPC.Codigo

LEFT JOIN Valor_Catalogos CATA_CCPC ON
TCPC.EMPR_Codigo = CATA_CCPC.EMPR_Codigo
AND TCPC.CATA_CCPC_Codigo = CATA_CCPC.Codigo

        
WHERE       
TCPC.EMPR_Codigo = @par_EMPR_Codigo    
AND TCPC.TERC_Codigo = @par_TERC_Codigo
END        
GO


