﻿PRINT 'gsp_consultar_terceros_sitios_cargue_descargue'
GO
DROP PROCEDURE gsp_consultar_terceros_sitios_cargue_descargue
GO
CREATE PROCEDURE gsp_consultar_terceros_sitios_cargue_descargue 
(@par_EMPR_Codigo SMALLINT,  
@par_TERC_Codigo NUMERIC  = NULL
)    
AS 
BEGIN 
SELECT   
TSCD.EMPR_Codigo  
,TSCD.TERC_Codigo  
,CONCAT(ISNULL(TERC.Nombre, ''),' ',ISNULL(TERC.Apellido1, ''),' ',ISNULL(TERC.Apellido2, ''),' ',ISNULL(TERC.Razon_Social, '')) AS Nombre_Cliente 
,TSCD.SICD_Codigo
,ISNULL(SICD.Nombre, '') AS Nombre_Sitio
,ISNULL(SICD.Direccion, '') AS Direccion_Sitio
  
FROM Terceros_Sitos_Cargue_Descargue TSCD   
  
LEFT JOIN Terceros TERC ON  
TERC.EMPR_Codigo = TSCD.EMPR_Codigo  
AND TERC.Codigo = TSCD.TERC_Codigo  

LEFT JOIN Sitios_Cargue_Descargue SICD ON  
SICD.EMPR_Codigo = TSCD.EMPR_Codigo  
AND SICD.Codigo = TSCD.SICD_Codigo  
  
WHERE   
TSCD.EMPR_Codigo = @par_EMPR_Codigo  
AND TSCD.TERC_Codigo = @par_TERC_Codigo  
  
END   
GO