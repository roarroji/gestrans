﻿PRINT 'gsp_eliminar_listas_correos_terceros'
GO
DROP PROCEDURE gsp_eliminar_listas_correos_terceros
GO
CREATE PROCEDURE gsp_eliminar_listas_correos_terceros  
(@par_EMPR_Codigo SMALLINT,  
@par_TERC_Codigo NUMERIC)  
AS
BEGIN  
DELETE Tercero_Listas_Correos  
WHERE   
EMPR_Codigo = @par_EMPR_Codigo  
AND   
TERC_Codigo = @par_TERC_Codigo  
END  
GO  
