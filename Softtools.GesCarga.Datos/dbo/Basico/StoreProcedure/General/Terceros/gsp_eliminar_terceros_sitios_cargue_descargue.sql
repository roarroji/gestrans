﻿PRINT 'gsp_eliminar_terceros_sitios_cargue_descargue'
GO
DROP PROCEDURE gsp_eliminar_terceros_sitios_cargue_descargue
GO
CREATE PROCEDURE gsp_eliminar_terceros_sitios_cargue_descargue 
(@par_EMPR_Codigo SMALLINT,    
@par_TERC_Codigo NUMERIC 
)    
AS    
BEGIN    
DELETE Terceros_Sitos_Cargue_Descargue WHERE EMPR_Codigo = @par_EMPR_Codigo and  TERC_Codigo = @par_TERC_Codigo  
END  
GO 
