﻿print 'gsp_insertar_direcciones_terceros'
go
drop procedure gsp_insertar_direcciones_terceros
go
create procedure gsp_insertar_direcciones_terceros
(@par_EMPR_Codigo SMALLINT,
@par_TERC_Codigo numeric,
@par_Direccion varchar(150),
@par_Telefonos varchar(100),
@par_CIUD_Codigo_Direccion smallint
)
as
begin

INSERT INTO [dbo].[Tercero_Direcciones]
           ([EMPR_Codigo]
           ,[TERC_Codigo]
           ,[Direccion]
           ,Telefonos
           ,[CIUD_Codigo])
     VALUES
           (@par_EMPR_Codigo
           ,@par_TERC_Codigo
           ,@par_Direccion
           ,ISNULL(@par_Telefonos,'')
           ,@par_CIUD_Codigo_Direccion)
end
go