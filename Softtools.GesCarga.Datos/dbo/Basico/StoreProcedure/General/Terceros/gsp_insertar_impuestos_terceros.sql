﻿print ('gsp_insertar_impuestos_terceros') 
go
drop procedure gsp_insertar_impuestos_terceros
go
create procedure gsp_insertar_impuestos_terceros
(@par_EMPR_Codigo smallint,
@par_TERC_Codigo numeric,
@par_ENIM_Codigo numeric
)
as
begin

INSERT INTO [dbo].[Tercero_Impuestos]
           ([EMPR_Codigo]
           ,[TERC_Codigo]
           ,[ENIM_Codigo]
           )
     VALUES
           (@par_EMPR_Codigo
           ,@par_TERC_Codigo
           ,@par_ENIM_Codigo)
end
go