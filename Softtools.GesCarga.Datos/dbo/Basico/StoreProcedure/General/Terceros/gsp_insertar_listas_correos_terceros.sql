﻿PRINT 'gsp_insertar_listas_correos_terceros'
GO
DROP PROCEDURE gsp_insertar_listas_correos_terceros
GO
create procedure gsp_insertar_listas_correos_terceros  
(
@par_EMPR_Codigo SMALLINT,  
@par_TERC_Codigo numeric,  
@par_EVCO_Codigo numeric,  
@par_TEDI_Codigo numeric,  
@par_Email varchar(100)  
)  
AS 
BEGIN  
  
INSERT INTO [dbo].[Tercero_Listas_Correos]  
           ([EMPR_Codigo]  
           ,[TERC_Codigo]  
           ,[EVCO_Codigo]  
           ,[Email]
		   ,[TEDI_Codigo]
         )  
     VALUES  
           (@par_EMPR_Codigo  
           ,@par_TERC_Codigo  
           ,@par_EVCO_Codigo
           ,@par_Email
		   ,@par_TEDI_Codigo)  
END
GO  