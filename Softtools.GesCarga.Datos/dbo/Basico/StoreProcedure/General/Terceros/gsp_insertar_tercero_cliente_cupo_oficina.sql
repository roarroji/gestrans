﻿PRINT 'gsp_insertar_tercero_cliente_cupo_oficina'
GO
DROP PROCEDURE gsp_insertar_tercero_cliente_cupo_oficina
GO
CREATE PROCEDURE gsp_insertar_tercero_cliente_cupo_oficina
(@par_EMPR_Codigo SMALLINT,    
@par_TERC_Codigo NUMERIC,    
@par_OFIC_Codigo NUMERIC,
@par_Cupo NUMERIC,
@par_Saldo NUMERIC
)    
as    
BEGIN
INSERT INTO Tercero_Clientes_Cupo_Oficinas(
EMPR_Codigo,
TERC_Codigo,
OFIC_Codigo,
Cupo,
Fecha_Actualiza_Cupo,
Saldo_Cupo,
Fecha_Actualiza_Saldo_Cupo
)
VALUES(
@par_EMPR_Codigo,
@par_TERC_Codigo,
@par_OFIC_Codigo,
@par_Cupo,
GETDATE(),
@par_Saldo,
GETDATE()
)
END
GO
