﻿PRINT 'gsp_insertar_tercero_condicion_peso_cumplido'
GO
DROP PROCEDURE gsp_insertar_tercero_condicion_peso_cumplido
GO
CREATE PROCEDURE gsp_insertar_tercero_condicion_peso_cumplido    
(  
	@par_EMPR_Codigo SMALLINT,        
	@par_TERC_Codigo NUMERIC,        
	@par_PRTR_Codigo NUMERIC,    
	@par_UMPT_Codigo NUMERIC,    
	@par_CATA_BCPC_Codigo NUMERIC,  
	@par_Valor_Unidad MONEY,
	@par_CATA_TCPC_Codigo NUMERIC,
	@par_Tolerancia MONEY,
	@par_CATA_CCPC_Codigo NUMERIC,
	@par_Usuario_Crea_Codigo NUMERIC
)        
AS        
BEGIN    
	INSERT INTO Tercero_Clientes_Condiciones_Peso_Cumplido(    
		EMPR_Codigo,
		TERC_Codigo,
		PRTR_Codigo,
		UMPT_Codigo,
		CATA_BCPC_Codigo,
		Valor_Unidad,
		CATA_TCPC_Codigo,
		Tolerancia,
		CATA_CCPC_Codigo,
		USUA_Codigo_Crea,
		Fecha_Crea
	)
	VALUES(    
		@par_EMPR_Codigo,
		@par_TERC_Codigo,
		@par_PRTR_Codigo,
		@par_UMPT_Codigo,
		@par_CATA_BCPC_Codigo,
		@par_Valor_Unidad,
		@par_CATA_TCPC_Codigo,
		@par_Tolerancia,
		@par_CATA_CCPC_Codigo,
		@par_Usuario_Crea_Codigo,
		GETDATE()
	)    
END
GO