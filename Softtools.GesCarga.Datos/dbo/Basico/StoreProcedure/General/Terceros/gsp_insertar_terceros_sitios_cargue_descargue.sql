﻿PRINT 'gsp_insertar_terceros_sitios_cargue_descargue'
GO
DROP PROCEDURE gsp_insertar_terceros_sitios_cargue_descargue
GO 
CREATE PROCEDURE gsp_insertar_terceros_sitios_cargue_descargue 
(    
@par_EMPR_Codigo SMALLINT,    
@par_TERC_Codigo NUMERIC,    
@par_SICD_Codigo NUMERIC
)    
AS    
BEGIN    
 INSERT INTO Terceros_Sitos_Cargue_Descargue    
  (    
  EMPR_Codigo,    
  TERC_Codigo,    
  SICD_Codigo  
  )    
  VALUES    
  (    
  @par_EMPR_Codigo,    
  @par_TERC_Codigo,    
  @par_SICD_Codigo
  )   
      
SELECT @@ROWCOUNT AS Codigo     
    
END 
GO