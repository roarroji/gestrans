﻿Print 'gsp_obtener_cliente'
GO
DROP PROCEDURE gsp_obtener_cliente
GO
CREATE PROCEDURE gsp_obtener_cliente      
(      
@par_EMPR_Codigo smallint,      
@par_Codigo Numeric  = null,    
@par_Identificacion VARCHAR(20) = NULL

)      
AS       
BEGIN      
SELECT       
TECI.EMPR_Codigo,      
ISNULL(TECI.TERC_Codigo_Comercial,  0) AS TERC_Codigo_Comercial,
TECI.CATA_FPCL_Codigo,
ISNULL(TECI.Dias_Plazo_Pago  ,0) AS Dias_Plazo_Pago,    
ISNULL(TECI.ETCV_Numero,0) AS ETCV_Numero,
TECI.Margen_Utilidad,
TECI.Maneja_Condiciones_Peso_Cumplido,
TECI.CATA_MFPC_Codigo,
TECI.Dia_Cierre_Facturacion,
MFPC.Nombre As ModalidadFacturacion  

FROM Tercero_Clientes   AS TECI 

LEFT JOIN Terceros AS TERC ON     
TERC.EMPR_Codigo = TECI.EMPR_Codigo    
AND TERC.Codigo = TECI.TERC_Codigo

LEFT JOIN V_Modalidad_Facturacion_Peso_Cliente As MFPC ON
TECI.EMPR_Codigo = MFPC.EMPR_Codigo
AND TECI.CATA_MFPC_Codigo = MFPC.Codigo

WHERE TECI.EMPR_Codigo = @par_EMPR_Codigo      
AND TERC.Codigo = isnull(@par_Codigo  ,TERC.Codigo)    
AND TERC.Numero_Identificacion =  isnull(@par_Identificacion  ,TERC.Numero_Identificacion)    
      
END      
GO