﻿PRINT 'gsp_obtener_conductor'
GO
DROP PROCEDURE gsp_obtener_conductor
GO
CREATE PROCEDURE gsp_obtener_conductor  
(  
@par_EMPR_Codigo smallint,  
@par_Codigo Numeric  = null,
@par_Identificacion VARCHAR(20) = NULL

)  
AS   
BEGIN  
SELECT   
TECO.EMPR_Codigo,
Conductor_Propio,
ISNULL(CATA_TCCO_Codigo,1700) AS CATA_TCCO_Codigo,
Numero_Licencia,
Fecha_Vencimiento_Licencia,
CATA_CALC_Codigo,
CATA_TISA_Codigo,
Fecha_Ultimo_Viaje
  
FROM Tercero_Conductores  AS TECO LEFT JOIN Terceros AS TERC ON 
TERC.EMPR_Codigo = TECO.EMPR_Codigo
AND TERC.Codigo = TECO.TERC_Codigo
WHERE TECO.EMPR_Codigo = @par_EMPR_Codigo  
AND TERC.Codigo = isnull(@par_Codigo  ,TERC.Codigo)
AND TERC.Numero_Identificacion =  isnull(@par_Identificacion  ,TERC.Numero_Identificacion) 
  
END  
GO