﻿PRINT 'gsp_obtener_empleado'
GO
DROP PROCEDURE gsp_obtener_empleado
GO
CREATE PROCEDURE gsp_obtener_empleado  
(  
@par_EMPR_Codigo smallint,  
@par_Codigo Numeric  = null,
@par_Identificacion VARCHAR(20) = NULL

)  
AS   
BEGIN  
SELECT   
TEEM.EMPR_Codigo,
ISNULL(Fecha_Vinculacion,'') AS Fecha_Vinculacion,
ISNULL(Fecha_Finalizacion,'') AS Fecha_Finalizacion,
ISNULL(CATA_TICE_Codigo,1200) AS CATA_TICE_Codigo,
ISNULL(CATA_CARG_Codigo,2000) AS CATA_CARG_Codigo,
ISNULL(Porcentaje_Comision,0) AS Porcentaje_Comision,
ISNULL(Salario,0) AS Salario,
ISNULL(Valor_Auxilio_Transporte,0) AS Valor_Auxilio_Transporte,
ISNULL(Valor_Seguridad_Social,0) AS Valor_Seguridad_Social,
ISNULL(Valor_Aporte_Parafiscales,0) AS Valor_Aporte_Parafiscales,
ISNULL(Valor_Seguro_Vida,0) AS Valor_Seguro_Vida,
ISNULL(Valor_Provision_Prestaciones_Sociales,0) AS Valor_Provision_Prestaciones_Sociales,
ISNULL(CATA_DEEM_Codigo,8700) AS CATA_DEEM_Codigo
  
FROM Tercero_Empleados AS TEEM LEFT JOIN Terceros AS TERC ON 
TERC.EMPR_Codigo = TEEM.EMPR_Codigo
AND TERC.Codigo = TEEM.TERC_Codigo
WHERE TEEM.EMPR_Codigo = @par_EMPR_Codigo  
AND TERC.Codigo = isnull(@par_Codigo  ,TERC.Codigo)
AND TERC.Numero_Identificacion =  isnull(@par_Identificacion  ,TERC.Numero_Identificacion) 
  
END  
GO