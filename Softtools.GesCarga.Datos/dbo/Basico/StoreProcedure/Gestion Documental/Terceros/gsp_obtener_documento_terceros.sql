﻿print 'gsp_obtener_documento_terceros'
go
drop procedure gsp_obtener_documento_terceros
go
create procedure gsp_obtener_documento_terceros
(
@par_EMPR_Codigo smallint ,
@par_TERC_Codigo numeric,
@par_CDGD_Codigo numeric
)
as
begin
select
	EMPR_Codigo ,
	TERC_Codigo ,
	CDGD_Codigo ,
	Referencia,
	Emisor,
	Fecha_Emision,
	Fecha_Vence,
	CASE WHEN Archivo IS NULL THEN 0 ELSE 1 END AS ValorDocumento,
	Archivo,
	Nombre_Documento,
	Extension,
	Tipo
from Tercero_Documentos
WHERE EMPR_Codigo = @par_EMPR_Codigo  AND TERC_Codigo = @par_TERC_Codigo  AND CDGD_Codigo = @par_CDGD_Codigo
end
go