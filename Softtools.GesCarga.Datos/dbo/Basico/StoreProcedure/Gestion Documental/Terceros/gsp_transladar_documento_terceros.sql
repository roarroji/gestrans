﻿print 'gsp_transladar_documento_terceros'
go
drop procedure gsp_transladar_documento_terceros
go
create procedure gsp_transladar_documento_terceros
(
	@par_EMPR_Codigo smallint ,
	@par_TERC_Codigo numeric,
	@par_CDGD_Codigo numeric,
	@par_USUA_Codigo_Crea numeric
)
as
begin

IF EXISTS(SELECT * FROM dbo.T_Archivo WHERE EMPR_Codigo = @par_EMPR_Codigo AND CDGD_Codigo = @par_CDGD_Codigo AND USUA_Codigo_Crea = @par_USUA_Codigo_Crea )
BEGIN
UPDATE Tercero_Documentos SET 
Nombre_Documento = (select Nombre_Documento FROM dbo.T_Archivo WHERE EMPR_Codigo = @par_EMPR_Codigo AND CDGD_Codigo = @par_CDGD_Codigo AND  USUA_Codigo_Crea = @par_USUA_Codigo_Crea ) , 
Archivo =  (select Archivo FROM dbo.T_Archivo WHERE EMPR_Codigo = @par_EMPR_Codigo AND CDGD_Codigo = @par_CDGD_Codigo AND  USUA_Codigo_Crea = @par_USUA_Codigo_Crea ) , 
Extension = (select Extension_Documento FROM dbo.T_Archivo WHERE EMPR_Codigo = @par_EMPR_Codigo AND CDGD_Codigo = @par_CDGD_Codigo AND  USUA_Codigo_Crea = @par_USUA_Codigo_Crea ) , 
Tipo = (select Tipo FROM dbo.T_Archivo WHERE EMPR_Codigo = @par_EMPR_Codigo AND CDGD_Codigo = @par_CDGD_Codigo AND USUA_Codigo_Crea = @par_USUA_Codigo_Crea ) 
 WHERE 
EMPR_Codigo = @par_EMPR_Codigo 
AND CDGD_Codigo = @par_CDGD_Codigo 

delete dbo.T_Archivo WHERE EMPR_Codigo = @par_EMPR_Codigo AND CDGD_Codigo = @par_CDGD_Codigo AND USUA_Codigo_Crea = @par_USUA_Codigo_Crea 
select @@ROWCOUNT as RegistrosAfectados
END
	ELSE
	SELECT 1 as RegistrosAfectados
END
GO