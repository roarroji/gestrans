﻿print 'gsp_consultar_documento_vehiculos'
go
drop procedure gsp_consultar_documento_vehiculos
go
create procedure gsp_consultar_documento_vehiculos
(
@par_EMPR_Codigo smallint ,
@par_VEHI_Codigo numeric
)
as
begin
select
	EMPR_Codigo ,
	VEHI_Codigo ,
	CDGD_Codigo ,
	Referencia,
	Emisor,
	Fecha_Emision,
	Fecha_Vence,
	CASE WHEN Archivo IS NULL THEN 0 ELSE 1 END AS ValorDocumento,
	NULL AS Archivo 
from Vehiculo_Documentos
WHERE EMPR_Codigo = @par_EMPR_Codigo  AND VEHI_Codigo = @par_VEHI_Codigo 
end
go