﻿print 'gsp_eliminar_t_archivos'
go
drop procedure gsp_eliminar_t_archivos
go
create procedure gsp_eliminar_t_archivos
(
@par_EMPR_Codigo smallint,
@par_CDGD_Codigo	numeric = null,
@par_USUA_Codigo_Crea	numeric
)
AS
BEGIN
delete T_Archivo where EMPR_Codigo = @par_EMPR_Codigo AND CDGD_Codigo = isnull(@par_CDGD_Codigo,CDGD_Codigo) AND  USUA_Codigo_Crea = @par_USUA_Codigo_Crea 
SELECT @@ROWCOUNT AS RegistrosAfectados
END
go