﻿print 'gsp_insertar_t_archivos'
go
drop procedure gsp_insertar_t_archivos
go
create procedure gsp_insertar_t_archivos
(
@par_EMPR_Codigo smallint,
@par_CDGD_Codigo	numeric,
@par_Archivo	varbinary(max),
@par_Nombre_Documento varchar (150),
@par_Extension_Documento	varchar(20),
@par_Tipo	varchar(200),
@par_USUA_Codigo_Crea	smallint
)
AS
BEGIN
delete T_Archivo where EMPR_Codigo = @par_EMPR_Codigo AND CDGD_Codigo = @par_CDGD_Codigo AND USUA_Codigo_Crea = @par_USUA_Codigo_Crea

INSERT INTO T_Archivo
(
EMPR_Codigo
,CDGD_Codigo
,Archivo
,Nombre_Documento
,Extension_Documento
,Tipo
,USUA_Codigo_Crea
,Fecha_Crea
)
VALUES
(
@par_EMPR_Codigo
,@par_CDGD_Codigo
,@par_Archivo
,@par_Nombre_Documento
,@par_Extension_Documento
,@par_Tipo
,@par_USUA_Codigo_Crea
,GETDATE()
)
SELECT @@ROWCOUNT AS RegistrosAfectados
END
go