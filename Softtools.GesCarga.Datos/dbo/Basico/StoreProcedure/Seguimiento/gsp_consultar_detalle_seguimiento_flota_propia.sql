﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 17/06/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


PRINT 'gsp_consultar_detalle_seguimiento_flota_propia'
GO
DROP PROCEDURE gsp_consultar_detalle_seguimiento_flota_propia
GO
CREATE PROCEDURE gsp_consultar_detalle_seguimiento_flota_propia 
(                                    
@par_EMPR_Codigo SMALLINT,                                
@par_Numero_Manifiesto NUMERIC = NULL,                                
@par_Puesto_Control NUMERIC = NULL,                                    
@par_Vehiculo VARCHAR(10) = NULL,                                         
@par_Conductor VARCHAR(100) = NULL,                                                 
@par_Ruta NUMERIC = NULL,                        
@par_CATA_TOSV_Codigo NUMERIC = NULL,                                          
@par_CATA_SRSV_Codigo NUMERIC = NULL,                                   
@par_Mostrar NUMERIC = NULL,                                    
@par_Estado NUMERIC = NULL,                                    
@par_Anulado NUMERIC = NULL,     
@par_Codigos_Oficinas VARCHAR(150),                         
@par_NumeroPagina INT = NULL,                                        
@par_RegistrosPagina INT = NULL                                        
)                                    
AS                                    
BEGIN                                    
                                    
DECLARE @IDMAX NUMERIC  = 0                                    
                                    
SET NOCOUNT ON;                                       
  DECLARE                                          
   @CantidadRegistros INT                                          
  SELECT @CantidadRegistros = (                                          
    SELECT DISTINCT                                           
     COUNT(1)                                           
         FROM                                          
   Detalle_Seguimiento_Vehiculos DESV                                    
                                  
   LEFT JOIN Vehiculos AS VEHI                                    
   ON DESV.EMPR_Codigo = VEHI.EMPR_Codigo                              
   AND DESV.VEHI_Codigo  = VEHI.Codigo                      
                                                      
   LEFT JOIN (SELECT EMPR_Codigo, ISNULL(MAX(Orden),0) AS Orden, VEHI_Codigo FROM Detalle_Seguimiento_Vehiculos GROUP BY EMPR_Codigo, VEHI_Codigo) AS DESE                                    
   ON DESV.EMPR_Codigo = DESE.EMPR_Codigo                                     
   AND DESV.VEHI_Codigo = DESE.VEHI_Codigo                                     
   AND DESV.Orden > (DESE.Orden - ISNULL(@par_Mostrar, DESE.Orden))                                         
                                    
   LEFT JOIN Valor_Catalogos AS TOSV                                    
   ON DESV.EMPR_Codigo = TOSV.EMPR_Codigo                                    
   AND DESV.CATA_TOSV_Codigo  = TOSV.Codigo                                    
                                    
   LEFT JOIN Valor_Catalogos AS SRSV                                    
   ON DESV.EMPR_Codigo = SRSV.EMPR_Codigo                                    
   AND DESV.CATA_SRSV_Codigo  = SRSV.Codigo                                    
                                    
   LEFT JOIN Valor_Catalogos AS NOSV                                    
   ON DESV.EMPR_Codigo = NOSV.EMPR_Codigo                                    
   AND DESV.CATA_NOSV_Codigo  = NOSV.Codigo                                    
                                  
   LEFT JOIN Puesto_Controles AS PUCO                                    
   ON DESV.EMPR_Codigo = PUCO.EMPR_Codigo                                    
   AND DESV.PUCO_Codigo = PUCO.Codigo                        
                      
   LEFT JOIN Terceros AS COND                                    
   ON VEHI.EMPR_Codigo = COND.EMPR_Codigo                                    
   AND VEHI.TERC_Codigo_Conductor  = COND.Codigo       
                                      
   LEFT JOIN Terceros AS TENE                                    
   ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo                                    
   AND VEHI.TERC_Codigo_Tenedor  = TENE.Codigo    
    
   LEFT JOIN Terceros AS PGPS                                
   ON VEHI.EMPR_Codigo = PGPS.EMPR_Codigo                                
   AND VEHI.TERC_Codigo_Proveedor_GPS  = PGPS.Codigo             
                                    
   LEFT JOIN Semirremolques AS SEMI                          
   ON VEHI.EMPR_Codigo = SEMI.EMPR_Codigo                                    
   AND VEHI.SEMI_Codigo  = SEMI.Codigo                           
                         
   LEFT JOIN Rutas AS RUTA                                    
   ON DESV.EMPR_Codigo = RUTA.EMPR_Codigo                                    
   AND DESV.RUTA_Codigo  = RUTA.Codigo     
                                    
     WHERE                                    
   DESV.EMPR_Codigo = @par_EMPR_Codigo                                                           
   AND (DESV.PUCO_Codigo = @par_Puesto_Control or @par_Puesto_Control is null)                    
   AND (VEHI.Placa = @par_Vehiculo or @par_Vehiculo is null)                                 
   AND(ISNULL(COND.Nombre, '') + ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') + ' ' + ISNULL(COND.Razon_Social, '')  LIKE                       
   CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Conductor, ISNULL(COND.Nombre, '') + ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') + ' ' + ISNULL(COND.Razon_Social, '')))), '%'))                         
   AND (RUTA.Codigo = @par_Ruta or @par_Ruta is null)                    
   AND (TOSV.Codigo = @par_CATA_TOSV_Codigo or @par_CATA_TOSV_Codigo is null)                    
   AND (SRSV.Codigo = @par_CATA_SRSV_Codigo or @par_CATA_SRSV_Codigo is null)                      
   AND DESV.Anulado = ISNULL(@par_Anulado, 0)    
   AND DESV.ENPD_Numero = 0  
   AND DESV.ENOC_Numero = 0                
   AND DESV.Orden > (DESE.Orden - ISNULL(@par_Mostrar, DESE.Orden))    
   AND DESV.OFIC_Codigo IN (SELECT * FROM dbo.Func_Dividir_String(@par_Codigos_Oficinas,','))   
   AND ISNULL(DESV.Fin_Viaje, 0) = 0  
     );                                                 
    WITH Pagina AS                                          
    (                                          
                                    
 SELECT                                    
  DESV.EMPR_Codigo,                                      
  DESV.ENPD_Numero,                      
  DESV.ENOC_Numero,                                                       
  DESV.ID,                                    
  DESV.Fecha_Reporte,                            
  DESV.Ubicacion,                                    
  DESV.Longitud,                                    
  DESV.Latitud,                                    
  DESV.PUCO_Codigo,                          
  ISNULL(DESV.CATA_TOSV_Codigo, 0) as CATA_TOSV_Codigo,                                  
  DESV.CATA_SRSV_Codigo,                                    
  DESV.CATA_NOSV_Codigo,                                    
  DESV.Observaciones,                                    
  DESV.Kilometros_Vehiculo,                                    
  DESV.Reportar_Cliente,          
  DESV.Prioritario,                                   
  DESV.Envio_Reporte_Cliente,                                    
  DESV.Fecha_Reporte_Cliente,                      
  DESV.Anulado,                                                 
  VEHI.TERC_Codigo_Conductor,                        
  VEHI.TERC_Codigo_Tenedor,                                     
  DESV.VEHI_Codigo,                                    
  ISNULL(VEHI.SEMI_Codigo, 0) AS SEMI_Codigo,                                    
  DESV.RUTA_Codigo,                                                         
  ISNULL(DESV.Fecha_Crea,'')AS Fecha_Ultimo_Reporte,                                          
  ISNULL(COND.Razon_Social, '')+ISNULL(COND.Nombre, '') + ' '+ ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') + ' (' + ISNULL(COND.Celulares, '') + ')' AS Nombre_Conductor,                                    
  ISNULL(TENE.Razon_Social, '')+ISNULL(TENE.Nombre, '') + ' '+ ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2, '')  AS Nombre_Tenedor,               
  CASE WHEN ISNULL(VEHI.TERC_Codigo_Proveedor_GPS, 0) > 0 THEN          
  ISNULL(PGPS.Razon_Social, '')+ISNULL(PGPS.Nombre, '') + ' '+ ISNULL(PGPS.Apellido1,'') + ' ' + ISNULL(PGPS.Apellido2, '')          
  ELSE '' END AS Nombre_Proveedor_GPS,                 
  ISNULL(VEHI.Placa, '') AS Vehiculo,                                      
  ISNULL(SEMI.Placa, '') AS Semirremolque,                        
  ISNULL(RUTA.Nombre, '') AS Ruta,                       
  ISNULL(PUCO.Nombre, ' ') AS Puesto_Control,                                  
  ISNULL(TOSV.Campo1, ' ') AS Tipo_Origen_Seguimiento,                                    
  ISNULL(SRSV.Campo1, ' ') AS Sitio_Reporte_Seguimiento,                                    
  ISNULL(NOSV.Campo1, ' ') AS Novedad_Seguimiento,                                        
  DESE.Orden,                                            
  ISNULL(CASE WHEN ((CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, DESV.Fecha_Reporte, GETDATE())))) < 30 THEN 1                        
  WHEN ((CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, DESV.Fecha_Reporte, GETDATE())))) >= 30                      
  AND ((CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, DESV.Fecha_Reporte, GETDATE())))) < 60 THEN 2              
  WHEN ((CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, DESV.Fecha_Reporte, GETDATE())))) >= 60                      
  AND ((CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, DESV.Fecha_Reporte, GETDATE())))) < 90 THEN 3           
  WHEN ((CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, DESV.Fecha_Reporte, GETDATE())))) >= 90                      
  AND ((CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, DESV.Fecha_Reporte, GETDATE())))) < 120 THEN 4                    
  WHEN ((CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, DESV.Fecha_Reporte, GETDATE())))) >= 120 THEN 5 END, 1) AS Desfase_Seguimiento,                          
  ROW_NUMBER() OVER(ORDER BY DESV.ID DESC) AS RowNumber                                          
           FROM                                          
   Detalle_Seguimiento_Vehiculos DESV                                    
                                  
   LEFT JOIN Vehiculos AS VEHI                                    
   ON DESV.EMPR_Codigo = VEHI.EMPR_Codigo                              
   AND DESV.VEHI_Codigo  = VEHI.Codigo                      
                                                      
   LEFT JOIN (SELECT EMPR_Codigo, ISNULL(MAX(Orden),0) AS Orden, VEHI_Codigo FROM Detalle_Seguimiento_Vehiculos GROUP BY EMPR_Codigo, VEHI_Codigo) AS DESE                                    
   ON DESV.EMPR_Codigo = DESE.EMPR_Codigo                                     
   AND DESV.VEHI_Codigo = DESE.VEHI_Codigo                                     
   AND DESV.Orden > (DESE.Orden - ISNULL(@par_Mostrar, DESE.Orden))                                         
                                    
   LEFT JOIN Valor_Catalogos AS TOSV                                    
   ON DESV.EMPR_Codigo = TOSV.EMPR_Codigo                                    
   AND DESV.CATA_TOSV_Codigo  = TOSV.Codigo                                    
                                    
   LEFT JOIN Valor_Catalogos AS SRSV                                    
   ON DESV.EMPR_Codigo = SRSV.EMPR_Codigo                                    
   AND DESV.CATA_SRSV_Codigo  = SRSV.Codigo                                    
                                    
   LEFT JOIN Valor_Catalogos AS NOSV                                    
   ON DESV.EMPR_Codigo = NOSV.EMPR_Codigo                                    
   AND DESV.CATA_NOSV_Codigo  = NOSV.Codigo                                    
                                  
   LEFT JOIN Puesto_Controles AS PUCO                                    
   ON DESV.EMPR_Codigo = PUCO.EMPR_Codigo                                    
   AND DESV.PUCO_Codigo = PUCO.Codigo                        
                      
   LEFT JOIN Terceros AS COND                                    
   ON VEHI.EMPR_Codigo = COND.EMPR_Codigo                                    
   AND VEHI.TERC_Codigo_Conductor  = COND.Codigo       
                                      
   LEFT JOIN Terceros AS TENE                                    
   ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo                                    
   AND VEHI.TERC_Codigo_Tenedor  = TENE.Codigo    
                
   LEFT JOIN Terceros AS PGPS                                
   ON VEHI.EMPR_Codigo = PGPS.EMPR_Codigo                                
   AND VEHI.TERC_Codigo_Proveedor_GPS  = PGPS.Codigo             
                                    
   LEFT JOIN Semirremolques AS SEMI                          
   ON VEHI.EMPR_Codigo = SEMI.EMPR_Codigo                                    
   AND VEHI.SEMI_Codigo  = SEMI.Codigo                           
                         
   LEFT JOIN Rutas AS RUTA                                    
   ON DESV.EMPR_Codigo = RUTA.EMPR_Codigo                                    
   AND DESV.RUTA_Codigo  = RUTA.Codigo     
                                    
     WHERE                                    
   DESV.EMPR_Codigo = @par_EMPR_Codigo                                                           
   AND (DESV.PUCO_Codigo = @par_Puesto_Control or @par_Puesto_Control is null)                    
   AND (VEHI.Placa = @par_Vehiculo or @par_Vehiculo is null)                                 
   AND(ISNULL(COND.Nombre, '') + ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') + ' ' + ISNULL(COND.Razon_Social, '')  LIKE                       
   CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Conductor, ISNULL(COND.Nombre, '') + ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') + ' ' + ISNULL(COND.Razon_Social, '')))), '%'))                         
   AND (RUTA.Codigo = @par_Ruta or @par_Ruta is null)                    
   AND (TOSV.Codigo = @par_CATA_TOSV_Codigo or @par_CATA_TOSV_Codigo is null)                    
   AND (SRSV.Codigo = @par_CATA_SRSV_Codigo or @par_CATA_SRSV_Codigo is null)                      
   AND DESV.Anulado = ISNULL(@par_Anulado, 0)    
   AND DESV.ENPD_Numero = 0  
   AND DESV.ENOC_Numero = 0                
   AND DESV.Orden > (DESE.Orden - ISNULL(@par_Mostrar, DESE.Orden))     
   AND DESV.OFIC_Codigo IN (SELECT * FROM dbo.Func_Dividir_String(@par_Codigos_Oficinas,','))   
   AND ISNULL(DESV.Fin_Viaje, 0) = 0  
   )                      
  SELECT                                           
  0 As Obtener,                      
  1 AS Tipo_Consulta,                         
  EMPR_Codigo,                                               
  ID,                                    
  Fecha_Reporte,                                             
  Ubicacion,                                    
  Longitud,                                    
  Latitud,                                    
  PUCO_Codigo,                             
  CATA_TOSV_Codigo,                               
  CATA_SRSV_Codigo,                                    
  CATA_NOSV_Codigo,             
  Observaciones,                                    
  Kilometros_Vehiculo,                                    
  Reportar_Cliente,      
  Prioritario,                                  
  Envio_Reporte_Cliente,                                    
  Fecha_Reporte_Cliente,                      
  Anulado,                                                 
  TERC_Codigo_Conductor,                                    
  TERC_Codigo_Tenedor,                                     
  VEHI_Codigo,                                    
  SEMI_Codigo,                                    
  RUTA_Codigo,                                                         
  Fecha_Ultimo_Reporte,                                          
  Nombre_Conductor,                                    
  Nombre_Tenedor,            
  Nombre_Proveedor_GPS,                                     
  Vehiculo,                                      
  Semirremolque,                        
  Ruta,                       
  Puesto_Control,                                  
  Tipo_Origen_Seguimiento,                            
  Sitio_Reporte_Seguimiento,                                    
  Novedad_Seguimiento,                           
  Orden,                                              
  ISNULL(Desfase_Seguimiento, 1) AS Desfase_Seguimiento,                        
  @CantidadRegistros AS TotalRegistros,                                          
  @par_NumeroPagina AS PaginaObtener,                                          
  @par_RegistrosPagina AS RegistrosPagina                                          
   FROM                                          
    Pagina                                      
   WHERE                           
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                        
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                        
 ORDER BY Fecha_Reporte DESC                                 
END           
GO