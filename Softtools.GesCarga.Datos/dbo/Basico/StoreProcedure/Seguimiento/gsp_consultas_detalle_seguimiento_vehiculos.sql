﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 13/08/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


PRINT 'gsp_consultas_detalle_seguimiento_vehiculos'
GO
DROP PROCEDURE gsp_consultas_detalle_seguimiento_vehiculos
GO
CREATE PROCEDURE gsp_consultas_detalle_seguimiento_vehiculos 
(              
@par_EMPR_Codigo SMALLINT,              
@par_ENPD_Numero NUMERIC = NULL,    
@par_Numero_Manifiesto NUMERIC = NULL,          
@par_Puesto_Control NUMERIC = NULL,              
@par_Vehiculo VARCHAR(10) = NULL,              
@par_Semirremolque VARCHAR(10) = NULL,              
@par_Conductor VARCHAR(100) = NULL,              
@par_Tenedor VARCHAR(100) = NULL,                          
@par_Ruta NUMERIC = NULL,  
@par_CATA_TOSV_Codigo NUMERIC = NULL,                    
@par_CATA_SRSV_Codigo NUMERIC = NULL,             
@par_Mostrar NUMERIC = NULL,              
@par_Estado NUMERIC = NULL,              
@par_Anulado NUMERIC = NULL,            
@par_NumeroPagina INT = NULL,                  
@par_RegistrosPagina INT = NULL                  
)              
AS              
BEGIN              
              
DECLARE @IDMAX NUMERIC  = 0              
              
SET NOCOUNT ON;                 
  DECLARE                    
   @CantidadRegistros INT                    
  SELECT @CantidadRegistros = (                    
    SELECT DISTINCT                     
     COUNT(1)                     
         FROM                    
   Detalle_Seguimiento_Vehiculos DESV              
            
   LEFT JOIN Encabezado_Planilla_Despachos AS ENPD            
   ON DESV.EMPR_Codigo = ENPD.EMPR_Codigo              
   AND DESV.ENPD_Numero = ENPD.Numero              
                                
   LEFT JOIN (SELECT EMPR_Codigo, ISNULL(MAX(Orden),0) AS Orden, ENPD_Numero FROM Detalle_Seguimiento_Vehiculos GROUP BY EMPR_Codigo,ENPD_Numero)  AS DESE              
   ON DESV.EMPR_Codigo = DESE.EMPR_Codigo               
   AND DESV.ENPD_Numero = DESE.ENPD_Numero               
   AND DESV.Orden > (DESE.Orden - ISNULL(@par_Mostrar,DESE.Orden))      
             
              
   LEFT JOIN Valor_Catalogos AS TOSV              
   ON DESV.EMPR_Codigo = TOSV.EMPR_Codigo              
   AND DESV.CATA_TOSV_Codigo  = TOSV.Codigo              
              
   LEFT JOIN Valor_Catalogos AS SRSV              
   ON DESV.EMPR_Codigo = SRSV.EMPR_Codigo              
   AND DESV.CATA_SRSV_Codigo  = SRSV.Codigo              
              
   LEFT JOIN Valor_Catalogos AS NOSV              
   ON DESV.EMPR_Codigo = NOSV.EMPR_Codigo              
   AND DESV.CATA_NOSV_Codigo  = NOSV.Codigo              
            
   LEFT JOIN Puesto_Controles AS PUCO              
   ON DESV.EMPR_Codigo = PUCO.EMPR_Codigo              
   AND DESV.PUCO_Codigo = PUCO.Codigo  

   LEFT JOIN Terceros AS COND              
   ON (ENPD.EMPR_Codigo = COND.EMPR_Codigo              
   AND ENPD.TERC_Codigo_Conductor  = COND.Codigo)               
                
   LEFT JOIN Terceros AS TENE              
   ON (ENPD.EMPR_Codigo = TENE.EMPR_Codigo              
   AND ENPD.TERC_Codigo_Tenedor  = TENE.Codigo)              
              
   LEFT JOIN Vehiculos AS VEHI              
   ON (ENPD.EMPR_Codigo = VEHI.EMPR_Codigo              
   AND ENPD.VEHI_Codigo  = VEHI.Codigo)               
              
   LEFT JOIN Semirremolques AS SEMI              
   ON (ENPD.EMPR_Codigo = SEMI.EMPR_Codigo              
   AND ENPD.SEMI_Codigo  = SEMI.Codigo)     
   
   LEFT JOIN Rutas AS RUTA              
   ON (ENPD.EMPR_Codigo = RUTA.EMPR_Codigo              
   AND ENPD.RUTA_Codigo  = RUTA.Codigo)           
              
     WHERE              
   DESV.EMPR_Codigo = @par_EMPR_Codigo 
   AND ENPD.Numero = ISNULL(@par_ENPD_Numero,ENPD.Numero)  
   AND ENPD.ENMC_Numero = ISNULL(@par_Numero_Manifiesto, ENPD.ENMC_Numero)    
   AND DESV.PUCO_Codigo = ISNULL(@par_Puesto_Control, DESV.PUCO_Codigo) 
   AND VEHI.Placa = ISNULL(@par_Vehiculo, VEHI.Placa)      
   AND SEMI.Placa = ISNULL(@par_Semirremolque, SEMI.Placa)   
   AND(COND.Nombre + ' ' + COND.Apellido1 + ' ' + COND.Apellido2 + ' ' + COND.Razon_Social  LIKE 
   CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Conductor, COND.Nombre + ' ' + COND.Apellido1 + ' ' + COND.Apellido2 + ' ' + COND.Razon_Social))), '%'))             
   AND(TENE.Nombre + ' ' + TENE.Apellido1 + ' ' + TENE.Apellido2 + ' ' + TENE.Razon_Social  LIKE 
   CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Tenedor, TENE.Nombre + ' ' + TENE.Apellido1 + ' ' + TENE.Apellido2 + ' ' + TENE.Razon_Social))), '%'))  
   AND RUTA.Codigo = ISNULL(@par_Semirremolque, RUTA.Codigo)    
   AND TOSV.Codigo = ISNULL(@par_CATA_TOSV_Codigo, TOSV.Codigo) 
   AND SRSV.Codigo = ISNULL(@par_CATA_SRSV_Codigo, SRSV.Codigo) 
   AND DESV.Anulado = ISNULL(@par_Anulado, 0)    
   AND ENPD.Estado =  ISNULL(@par_Estado,ENPD.Estado)          
   AND DESV.Orden > (DESE.Orden - isnull(@par_Mostrar,DESE.Orden))           
               
     );                           
    WITH Pagina AS                    
    (                    
              
 SELECT              
  DESV.EMPR_Codigo,                
  DESV.ENPD_Numero, 
  ISNULL(DESV.ENMC_Numero,0)AS Numero_Manifiesto,               
  DESV.ID,              
  DESV.Fecha_Reporte,        
  DESV.Ubicacion,              
  DESV.Longitud,              
  DESV.Latitud,              
  DESV.PUCO_Codigo,  
  DESV.CATA_TOSV_Codigo,             
  DESV.CATA_SRSV_Codigo,              
  DESV.CATA_NOSV_Codigo,              
  DESV.Observaciones,              
  DESV.Kilometros_Vehiculo,              
  DESV.Reportar_Cliente,              
  DESV.Envio_Reporte_Cliente,              
  DESV.Fecha_Reporte_Cliente,
  DESV.Anulado,                           
  ENPD.TERC_Codigo_Conductor,              
  ENPD.TERC_Codigo_Tenedor,               
  ENPD.VEHI_Codigo,              
  ENPD.SEMI_Codigo,              
  ENPD.RUTA_Codigo,                                   
  ISNULL(DESV.Fecha_Crea,'')AS Fecha_Ultimo_Reporte,                    
  ISNULL(COND.Razon_Social, '')+ISNULL(COND.Nombre, '') + ' '+ ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') AS Nombre_Conductor,              
  ISNULL(TENE.Razon_Social, '')+ISNULL(TENE.Nombre, '') + ' '+ ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2, '')  AS Nombre_Tenedor,                
  ISNULL(VEHI.Placa, '') AS Vehiculo,                
  ISNULL(SEMI.Placa, '') AS Semirremolque,  
  ISNULL(RUTA.Nombre, '') AS Ruta, 
  ISNULL(PUCO.Nombre, '') AS Puesto_Control,            
  TOSV.Campo1 AS Tipo_Origen_Seguimiento,              
  SRSV.Campo1 AS Sitio_Reporte_Seguimiento,              
  NOSV.Campo1 AS Novedad_Seguimiento,              
  DESV.Orden,              
  DESE.Orden as orden1,             
  ROW_NUMBER() OVER(ORDER BY DESV.ID DESC) AS RowNumber                    
                       FROM                    
   Detalle_Seguimiento_Vehiculos DESV              
            
   LEFT JOIN Encabezado_Planilla_Despachos AS ENPD            
   ON DESV.EMPR_Codigo = ENPD.EMPR_Codigo              
   AND DESV.ENPD_Numero = ENPD.Numero              
                                
   LEFT JOIN (SELECT EMPR_Codigo, ISNULL(MAX(Orden),0) AS Orden, ENPD_Numero FROM Detalle_Seguimiento_Vehiculos GROUP BY EMPR_Codigo,ENPD_Numero)  AS DESE              
   ON DESV.EMPR_Codigo = DESE.EMPR_Codigo               
   AND DESV.ENPD_Numero = DESE.ENPD_Numero               
   AND DESV.Orden > (DESE.Orden - ISNULL(@par_Mostrar,DESE.Orden))      
             
              
   LEFT JOIN Valor_Catalogos AS TOSV              
   ON DESV.EMPR_Codigo = TOSV.EMPR_Codigo              
   AND DESV.CATA_TOSV_Codigo  = TOSV.Codigo              
              
   LEFT JOIN Valor_Catalogos AS SRSV              
   ON DESV.EMPR_Codigo = SRSV.EMPR_Codigo              
   AND DESV.CATA_SRSV_Codigo  = SRSV.Codigo              
              
   LEFT JOIN Valor_Catalogos AS NOSV              
   ON DESV.EMPR_Codigo = NOSV.EMPR_Codigo              
   AND DESV.CATA_NOSV_Codigo  = NOSV.Codigo   
   
   LEFT JOIN Puesto_Controles AS PUCO              
   ON DESV.EMPR_Codigo = PUCO.EMPR_Codigo              
   AND DESV.PUCO_Codigo = PUCO.Codigo            
            
   LEFT JOIN Terceros AS COND              
   ON (ENPD.EMPR_Codigo = COND.EMPR_Codigo              
   AND ENPD.TERC_Codigo_Conductor  = COND.Codigo)               
                
   LEFT JOIN Terceros AS TENE              
   ON (ENPD.EMPR_Codigo = TENE.EMPR_Codigo              
   AND ENPD.TERC_Codigo_Tenedor  = TENE.Codigo)              
              
   LEFT JOIN Vehiculos AS VEHI              
   ON (ENPD.EMPR_Codigo = VEHI.EMPR_Codigo              
   AND ENPD.VEHI_Codigo  = VEHI.Codigo)               
              
   LEFT JOIN Semirremolques AS SEMI              
   ON (ENPD.EMPR_Codigo = SEMI.EMPR_Codigo              
   AND ENPD.SEMI_Codigo  = SEMI.Codigo)     
   
   LEFT JOIN Rutas AS RUTA              
   ON (ENPD.EMPR_Codigo = RUTA.EMPR_Codigo              
   AND ENPD.RUTA_Codigo  = RUTA.Codigo)           
              
     WHERE              
   DESV.EMPR_Codigo = @par_EMPR_Codigo 
   AND ENPD.Numero = ISNULL(@par_ENPD_Numero,ENPD.Numero)  
   AND ENPD.ENMC_Numero = ISNULL(@par_Numero_Manifiesto, ENPD.ENMC_Numero)    
   AND DESV.PUCO_Codigo = ISNULL(@par_Puesto_Control, DESV.PUCO_Codigo) 
   AND VEHI.Placa = ISNULL(@par_Vehiculo, VEHI.Placa)      
   AND SEMI.Placa = ISNULL(@par_Semirremolque, SEMI.Placa)   
   AND(COND.Nombre + ' ' + COND.Apellido1 + ' ' + COND.Apellido2 + ' ' + COND.Razon_Social  LIKE 
   CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Conductor, COND.Nombre + ' ' + COND.Apellido1 + ' ' + COND.Apellido2 + ' ' + COND.Razon_Social))), '%'))             
   AND(TENE.Nombre + ' ' + TENE.Apellido1 + ' ' + TENE.Apellido2 + ' ' + TENE.Razon_Social  LIKE 
   CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Tenedor, TENE.Nombre + ' ' + TENE.Apellido1 + ' ' + TENE.Apellido2 + ' ' + TENE.Razon_Social))), '%'))  
   AND RUTA.Codigo = ISNULL(@par_Semirremolque, RUTA.Codigo)    
   AND TOSV.Codigo = ISNULL(@par_CATA_TOSV_Codigo, TOSV.Codigo) 
   AND SRSV.Codigo = ISNULL(@par_CATA_SRSV_Codigo, SRSV.Codigo) 
   AND DESV.Anulado = ISNULL(@par_Anulado, 0)    
   AND ENPD.Estado =  ISNULL(@par_Estado,ENPD.Estado)          
   AND DESV.Orden > (DESE.Orden - isnull(@par_Mostrar,DESE.Orden)) 
   )
  SELECT                     
  0 As Obtener, 
  2 AS Tipo_Consulta,                   
 EMPR_Codigo,                
  ENPD_Numero, 
  0 AS ENOC_Numero,
  Numero_Manifiesto,               
  ID,              
  Fecha_Reporte,       
  Ubicacion,              
  Longitud,              
  Latitud,              
  PUCO_Codigo,   
  CATA_TOSV_Codigo,              
  CATA_SRSV_Codigo,              
  CATA_NOSV_Codigo,              
  Observaciones,              
  Kilometros_Vehiculo,              
  Reportar_Cliente,              
  Envio_Reporte_Cliente,              
  Fecha_Reporte_Cliente,
  Anulado,                           
  TERC_Codigo_Conductor,              
  TERC_Codigo_Tenedor,               
  VEHI_Codigo,              
  SEMI_Codigo,              
  RUTA_Codigo,                                   
  Fecha_Ultimo_Reporte,                    
  Nombre_Conductor,              
  Nombre_Tenedor,                
  Vehiculo,                
  Semirremolque,  
  Ruta, 
  Puesto_Control,            
  Tipo_Origen_Seguimiento,              
  Sitio_Reporte_Seguimiento,              
  Novedad_Seguimiento,              
  Orden,              
  orden1,     
  @CantidadRegistros AS TotalRegistros,                    
  @par_NumeroPagina AS PaginaObtener,                    
  @par_RegistrosPagina AS RegistrosPagina                    
   FROM                    
    Pagina                
   WHERE                    
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                  
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                  
 ORDER BY ID DESC           
END          
GO