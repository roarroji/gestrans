﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 17/06/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


PRINT 'gsp_obtener_detalle_seguimiento_flota_propia'
GO
DROP PROCEDURE gsp_obtener_detalle_seguimiento_flota_propia
GO
CREATE PROCEDURE gsp_obtener_detalle_seguimiento_flota_propia 
(                    
@par_EMPR_Codigo SMALLINT,                    
@par_Codigo NUMERIC = NULL,      
@par_Tipo_Consulta NUMERIC = NULL                  
)                    
AS                    
BEGIN 
                 
 SELECT TOP 1          
  7 AS Tipo_Consulta, 
  DESV.ID,
  DESV.VEHI_Codigo,
  VEHI.Placa AS Vehiculo,
  DESV.RUTA_Codigo,
  RUTA.Nombre AS Nombre_Ruta
                  
   FROM                                        
   Detalle_Seguimiento_Vehiculos DESV                                  
                                
   LEFT JOIN Vehiculos AS VEHI                                  
   ON DESV.EMPR_Codigo = VEHI.EMPR_Codigo                            
   AND DESV.VEHI_Codigo  = VEHI.Codigo                            
                       
   LEFT JOIN Rutas AS RUTA                                  
   ON DESV.EMPR_Codigo = RUTA.EMPR_Codigo                                  
   AND DESV.RUTA_Codigo  = RUTA.Codigo                 
                 
   WHERE            
   DESV.EMPR_Codigo = @par_EMPR_Codigo              
   AND DESV.ID = ISNULL(@par_Codigo, DESV.ID)        
         
END  
GO     