﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 17/06/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


PRINT 'sp_anular_detalle_seguimiento_vehiculos'
GO
DROP PROCEDURE sp_anular_detalle_seguimiento_vehiculos
GO
CREATE PROCEDURE sp_anular_detalle_seguimiento_vehiculos 
(      
@par_EMPR_Codigo SMALLINT,      
@par_Codigo NUMERIC,      
@par_Numero_Documento_Remesa NUMERIC = NULL,  
@par_USUA_Codigo_Anula SMALLINT,      
@par_Causa_Anula VARCHAR(150)      
)      
AS      
BEGIN      
  
IF @par_Numero_Documento_Remesa > 0       
BEGIN  
UPDATE Detalle_Seguimiento_Remesas SET       
      
Anulado = 1,      
USUA_Codigo_Anula = @par_USUA_Codigo_Anula,      
Fecha_Anula = GETDATE(),      
Causa_Anula = @par_Causa_Anula      
      
WHERE       
EMPR_Codigo = @par_EMPR_Codigo      
AND ENPD_Numero = @par_Numero_Documento_Remesa        
      
SELECT @@ROWCOUNT AS Codigo     
END  
ELSE  
BEGIN  
UPDATE Detalle_Seguimiento_Vehiculos SET       
      
Anulado = 1,      
USUA_Codigo_Anula = @par_USUA_Codigo_Anula,      
Fecha_Anula = GETDATE(),      
Causa_Anula = @par_Causa_Anula      
      
WHERE       
EMPR_Codigo = @par_EMPR_Codigo      
AND ID =  @par_Codigo        
      
SELECT @@ROWCOUNT AS Codigo      
      
END      
END   
GO