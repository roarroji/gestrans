﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */



	PRINT 'gsp_Anular_Impuestos'
GO

DROP PROCEDURE gsp_Anular_Impuestos
GO
CREATE PROCEDURE gsp_Anular_Impuestos(  
@par_EMPR_Codigo NUMERIC,  
@par_Codigo NUMERIC
) 
AS BEGIN  
DECLARE @smaRetornar smallint = 0 		
UPDATE Encabezado_Impuestos SET Estado=0  WHERE EMPR_Codigo= @par_EMPR_Codigo AND CODIGO=@par_Codigo  
SET @smaRetornar = @@ROWCOUNT
SELECT @smaRetornar as Codigo

END  
GO