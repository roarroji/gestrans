﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	PRINT 'gsp_Eliminar_TRM'
GO
DROP PROCEDURE gsp_Eliminar_TRM
GO
CREATE PROCEDURE gsp_Eliminar_TRM(  
@par_EMPR_Codigo NUMERIC,  
@par_MONE_Codigo NUMERIC,   
@par_FECHA DATETIME )  
AS BEGIN   
DECLARE @smaRetornar smallint = 0 
DELETE Tasa_Cambio_Monedas WHERE EMPR_Codigo= @par_EMPR_Codigo and MONE_Codigo= @par_MONE_Codigo and Fecha=@par_FECHA   
		
SET @smaRetornar = @@ROWCOUNT
SELECT @smaRetornar as MONE_Codigo

END  
GO