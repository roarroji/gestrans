﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_Existe_Moneda_Local'
GO
DROP PROCEDURE gsp_Existe_Moneda_Local
GO
CREATE PROCEDURE gsp_Existe_Moneda_Local (  
@par_EMPR_Codigo NUMERIC
) 
AS BEGIN  
 DECLARE @ExisteLocal TINYINT = (SELECT COUNT(*) FROM Monedas WHERE EMPR_Codigo= @par_EMPR_Codigo AND Local = 1)
  SELECT @ExisteLocal AS ExisteLocal 
END  
GO