﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */



	
PRINT 'gsp_Modificar_Impuestos'
GO
DROP PROCEDURE gsp_Modificar_Impuestos
GO

CREATE PROCEDURE gsp_Modificar_Impuestos(  
	@par_EMPR_Codigo NUMERIC,  
	@par_Codigo NUMERIC,
	@par_Codigo_Alterno VARCHAR(20),   
	@par_Nombre VARCHAR(100),   
	@par_Label VARCHAR(50),   
	@par_CATA_TRAI_Codigo NUMERIC,
	@par_CATA_TIIM_Codigo NUMERIC,
	@par_Operacion CHAR,
	@par_PLUC_Codigo NUMERIC,
	@par_Valor_Tarifa NUMERIC(18,6),
	@par_Valor_Base NUMERIC,
	@par_Estado SMALLINT,
	@par_USUA_Codigo_Modi SMALLINT)  
  
AS BEGIN   
UPDATE Encabezado_Impuestos SET
		Codigo = @par_Codigo,
        Codigo_Alterno = @par_Codigo_Alterno ,
		Nombre = @par_Nombre,
		Label= @par_Label,
		CATA_TRAI_Codigo= @par_CATA_TRAI_Codigo,
		CATA_TIIM_Codigo= @par_CATA_TIIM_Codigo,
		Operacion = @par_Operacion ,
		PLUC_Codigo = @par_PLUC_Codigo ,
		Valor_Tarifa = @par_Valor_Tarifa ,
		Valor_Base = @par_Valor_Base ,
		Estado = @par_Estado ,
		USUA_Codigo_Modifica = @par_USUA_Codigo_Modi, 
		Fecha_Modifica = GETDATE()
WHERE EMPR_Codigo= @par_EMPR_Codigo 
AND Codigo= @par_Codigo  

	SELECT @@ROWCOUNT as Codigo 

END  
GO