﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

		PRINT 'gsp_Obtener_Impuestos'
GO

DROP PROCEDURE gsp_Obtener_Impuestos
GO 

CREATE PROCEDURE gsp_Obtener_Impuestos(  
@par_EMPR_Codigo NUMERIC,  
@par_Codigo NUMERIC)
AS BEGIN   
  
SELECT 
        INPU.EMPR_Codigo,
		INPU.Codigo,
		INPU.Codigo_Alterno,
		INPU.Nombre,
		INPU.Label,
		TRAI.Campo1 Tipo_Recaudo ,
		TIIM.Campo1 Tipo_Impuesto,
		INPU.CATA_TRAI_Codigo,
		INPU.CATA_TIIM_Codigo,
		INPU.Operacion,
		INPU.PLUC_Codigo,
		INPU.Valor_Tarifa,
		INPU.Valor_Base,
		INPU.Estado,
		INPU.USUA_Codigo_Crea,
		1 AS Obtener
FROM 
	Encabezado_Impuestos INPU,
	VALOR_CATALOGOS TRAI,
	Valor_Catalogos TIIM
WHERE 
	INPU.EMPR_Codigo = @par_EMPR_Codigo  
	AND INPU.CODIGO = @par_Codigo
	AND INPU.EMPR_Codigo = TRAI.EMPR_Codigo
	AND INPU.CATA_TRAI_Codigo = TRAI.Codigo
	AND INPU.EMPR_Codigo = TIIM.EMPR_Codigo
   AND INPU.CATA_TRAI_Codigo = TIIM.Codigo
	
END  
GO