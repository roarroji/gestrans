﻿		/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_Obtener_TRM'
GO
DROP PROCEDURE gsp_Obtener_TRM
GO
CREATE PROCEDURE gsp_Obtener_TRM(  
@par_EMPR_Codigo NUMERIC,  
@par_MONE_Codigo NUMERIC,  
@par_Fecha DATE)
AS BEGIN   
  
SELECT 
	MONE.Nombre,
	TRM.MONE_Codigo,
	TRM.Fecha,
	TRM.Valor_Moneda_Local 
FROM 
	Tasa_Cambio_Monedas TRM , 
	Monedas MONE 
WHERE 
	TRM.EMPR_Codigo = @par_EMPR_Codigo  
	AND MONE.CODIGO = TRM.MONE_Codigo 
	AND TRM.Fecha = @par_Fecha
	OR TRM.MONE_Codigo = ISNULL(@par_MONE_Codigo,0)   
END  
GO