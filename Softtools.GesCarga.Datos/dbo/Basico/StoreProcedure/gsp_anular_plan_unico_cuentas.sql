﻿PRINT 'gsp_anular_plan_unico_cuentas'
GO
DROP PROCEDURE gsp_anular_plan_unico_cuentas
GO
CREATE PROCEDURE  gsp_anular_plan_unico_cuentas
(
  @par_EMPR_Codigo SMALLINT,
  @par_Codigo NUMERIC
  )
AS
BEGIN
  DELETE Plan_Unico_Cuentas 
  WHERE EMPR_Codigo = @par_EMPR_Codigo 
    AND Codigo = @par_Codigo

    SELECT @@ROWCOUNT AS Numero
END
GO