﻿PRINT 'gsp_cerrar_sesion'
GO
DROP PROCEDURE gsp_cerrar_sesion
GO
CREATE PROCEDURE gsp_cerrar_sesion
(
@par_EMPR_Codigo SMALLINT,
@par_USUA_Codigo NUMERIC
)
AS
BEGIN 

UPDATE USUARIOS SET Login = 0 
WHERE 
EMPR_Codigo = @par_EMPR_Codigo
AND Codigo =  @par_USUA_Codigo

SELECT Codigo AS Codigo FROM Usuarios WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_USUA_Codigo

END
GO	