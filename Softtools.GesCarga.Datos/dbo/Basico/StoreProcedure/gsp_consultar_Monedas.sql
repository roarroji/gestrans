﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
		PRINT 'gsp_consultar_Monedas'
GO
DROP PROCEDURE gsp_consultar_Monedas 
GO
CREATE PROCEDURE gsp_consultar_Monedas  
 (  
  
  @par_EMPR_Codigo SMALLINT,  
  @par_Codigo NUMERIC = NULL,  
  @par_Nombre VARCHAR(50) = NULL,  
  @par_Estado SMALLINT = NULL,  
  @par_NumeroPagina INT = NULL,  
  @par_RegistrosPagina INT = NULL  
 )  
 AS  
 BEGIN  
  SET NOCOUNT ON;  
   DECLARE  
    @CantidadRegistros INT  
   SELECT @CantidadRegistros = (  
     SELECT DISTINCT   
      COUNT(1)   
      FROM  
      Monedas MONE 
	LEFT OUTER JOIN
	(SELECT TRMA.EMPR_Codigo, TRMA.MONE_Codigo, TRMA.Valor_Moneda_Local, TRMA.Fecha FROM Tasa_Cambio_Monedas TRMA,
	 (SELECT EMPR_Codigo, MONE_Codigo, MAX(Fecha) As Fecha FROM Tasa_Cambio_Monedas GROUP BY EMPR_Codigo, MONE_Codigo) TRMU
	 WHERE TRMA.EMPR_Codigo = TRMU.EMPR_Codigo
	 AND TRMA.MONE_Codigo = TRMU.MONE_Codigo
	 AND TRMA.Fecha = TRMU.Fecha) TRMA 
	  ON
	  MONE.EMPR_Codigo = TRMA.EMPR_Codigo
	  AND MONE.Codigo = TRMA.MONE_Codigo
	  WHERE 
      Codigo > 0  
      AND Codigo = ISNULL(@par_Codigo, Codigo)  
      AND Estado = ISNULL(@par_Estado, Estado)  
      AND ((Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  

      );  
              
     WITH Pagina AS  
     (  
  
      SELECT   
      MONE.EMPR_Codigo,  
      MONE.Codigo,  
      MONE.Nombre,  
      MONE.Nombre_Corto,  
      MONE.Simbolo,  
      MONE.Local,  
      MONE.Estado,  
	  ISNULL(TRMA.Valor_Moneda_Local,0) Valor_Moneda_Local,
	  TRMA.Fecha ,
      ROW_NUMBER() OVER(ORDER BY Nombre) AS RowNumber  
      FROM  
      Monedas  MONE 
	   LEFT OUTER JOIN
	(SELECT TRMA.EMPR_Codigo, TRMA.MONE_Codigo, TRMA.Valor_Moneda_Local, TRMA.Fecha FROM Tasa_Cambio_Monedas TRMA,
	 (SELECT EMPR_Codigo, MONE_Codigo, MAX(Fecha) As Fecha FROM Tasa_Cambio_Monedas GROUP BY EMPR_Codigo, MONE_Codigo) TRMU
	 WHERE TRMA.EMPR_Codigo = TRMU.EMPR_Codigo
	 AND TRMA.MONE_Codigo = TRMU.MONE_Codigo
	 AND TRMA.Fecha = TRMU.Fecha) TRMA 
	  ON
	  MONE.EMPR_Codigo = TRMA.EMPR_Codigo
	  AND MONE.Codigo = TRMA.MONE_Codigo
	   where
      Codigo > 0  
      AND Codigo = ISNULL(@par_Codigo, Codigo)  
      AND Estado = ISNULL(@par_Estado, Estado)  
      AND ((Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  

   )  
   SELECT DISTINCT  
   0 AS Obtener,  
   EMPR_Codigo,  
   Codigo,  
   Nombre,  
   Nombre_Corto,  
   Simbolo,  
   Local,  
   Estado,  
   Valor_Moneda_Local, 
   Fecha,
   @CantidadRegistros AS TotalRegistros,  
   @par_NumeroPagina AS PaginaObtener,  
   @par_RegistrosPagina AS RegistrosPagina  
   FROM  
    Pagina  
   WHERE  
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
   order by Nombre  
   END  
  
GO