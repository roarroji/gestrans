﻿PRINT 'gsp_consultar_bancos'
GO
DROP PROCEDURE gsp_consultar_bancos
GO  
CREATE PROCEDURE gsp_consultar_bancos 
(      
 @par_EMPR_Codigo SMALLINT,      
 @par_Codigo SMALLINT = NULL,      
 @par_Nombre VARCHAR(50) = NULL,  
 @par_Codigo_Alterno VARCHAR (20) =NULL,  
 @par_Estado SMALLINT = NULL,      
 @par_NumeroPagina INT = NULL,    
 @par_RegistrosPagina INT = NULL    
)      
AS      
BEGIN      
 SET NOCOUNT ON;      
  DECLARE      
   @CantidadRegistros INT      
  SELECT @CantidadRegistros = (      
    SELECT DISTINCT       
     COUNT(1)       
    FROM      
     Bancos      
    WHERE      
     EMPR_Codigo = @par_EMPR_Codigo      
	 AND Codigo <> 30   
     AND Codigo = ISNULL(@par_Codigo,Codigo)      
     AND Codigo_Alterno = ISNULL(@par_Codigo_Alterno,Codigo_Alterno)      
     AND ((Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))      
     AND Estado = ISNULL(@par_Estado,Estado)      
     );             
    WITH Pagina AS      
    (      
      
    SELECT      
     EMPR_Codigo,      
     Codigo,      
     Codigo_Alterno,      
     Nombre,      
     Estado,      
     ROW_NUMBER() OVER(ORDER BY Nombre) AS RowNumber      
    FROM      
     Bancos      
    WHERE      
     EMPR_Codigo = @par_EMPR_Codigo 
	 AND Codigo <> 30        
     AND Codigo = ISNULL(@par_Codigo, Codigo) 	      
     AND Codigo_Alterno = ISNULL(@par_Codigo_Alterno,Codigo_Alterno)     
     AND Nombre LIKE  ISNULL('%' + @par_Nombre + '%', Nombre)       
     AND Estado = ISNULL(@par_Estado,Estado)      
  )       
  SELECT DISTINCT      
      0 As Obtener, --EG: Consultar      
   EMPR_Codigo,      
   Codigo_Alterno,      
   Codigo,      
   Nombre,      
   Estado,      
   @CantidadRegistros AS TotalRegistros,      
   @par_NumeroPagina AS PaginaObtener,      
   @par_RegistrosPagina AS RegistrosPagina      
  FROM      
   Pagina      
  WHERE      
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, 1000)    
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, 1000)    
  ORDER BY Nombre ASC      
      
END      
GO