﻿PRINT 'gsp_consultar_codigo_dane_ciudades'
GO
DROP PROCEDURE gsp_consultar_codigo_dane_ciudades
GO
CREATE PROCEDURE gsp_consultar_codigo_dane_ciudades
(
@par_EMPR_Codigo SMALLINT
)
AS
BEGIN
SELECT 
EMPR_Codigo,
Codigo,
Nombre,
Codigo_Ciudad,
Codigo_Departamento
FROM
Codigo_DANE_Ciudades
WHERE 
EMPR_Codigo = @par_EMPR_Codigo
END
GO