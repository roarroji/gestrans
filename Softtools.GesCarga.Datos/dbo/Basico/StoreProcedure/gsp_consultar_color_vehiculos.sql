﻿PRINT 'gsp_consultar_color_vehiculos'
GO
DROP PROCEDURE gsp_consultar_color_vehiculos
GO
CREATE PROCEDURE gsp_consultar_color_vehiculos
(
	@par_EMPR_Codigo SMALLINT,
	@par_Codigo NUMERIC = NULL,
	@par_Codigo_Alterno VARCHAR(20) = NULL,
	@par_Nombre VARCHAR(50) = NULL,
	@par_Estado SMALLINT = NULL,
	@par_NumeroPagina INT = NULL,
	@par_RegistrosPagina INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
		DECLARE
			@CantidadRegistros	INT
		SELECT @CantidadRegistros = (
				SELECT DISTINCT 
					COUNT(1) 
				FROM
					Color_Vehiculos COVE
				WHERE
					COVE.Codigo <> 0
					AND COVE.EMPR_Codigo = @par_EMPR_Codigo
					AND COVE.Estado = ISNULL(@par_Estado, COVE.Estado)
					AND COVE.Codigo = ISNULL(@par_Codigo, COVE.Codigo)
					AND ((COVE.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))
					AND ((COVE.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))
					);
					       
				WITH Pagina AS
				(

				SELECT
					0 AS Obtener,
					COVE.EMPR_Codigo,
					COVE.Codigo,
					COVE.Codigo_Alterno,
					COVE.Nombre,
					COVE.Estado,
					ROW_NUMBER() OVER(ORDER BY COVE.Nombre) AS RowNumber
				FROM
					Color_Vehiculos COVE
				WHERE
					COVE.Codigo <> 0
					AND COVE.EMPR_Codigo = @par_EMPR_Codigo
					AND COVE.Estado = ISNULL(@par_Estado, COVE.Estado)
					AND COVE.Codigo = ISNULL(@par_Codigo, COVE.Codigo)
					AND ((COVE.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))
					AND ((COVE.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))
		)
		SELECT DISTINCT
		0 AS Obtener,
		EMPR_Codigo,
		Codigo,
		Codigo_Alterno,
		Nombre,
		Estado,
		@CantidadRegistros AS TotalRegistros,
		@par_NumeroPagina AS PaginaObtener,
		@par_RegistrosPagina AS RegistrosPagina
		FROM
			Pagina
		WHERE
			RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
			AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
		order by Nombre
END
GO