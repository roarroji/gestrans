﻿PRINT 'gsp_consultar_detalle_concepto_contables'
GO
DROP PROCEDURE gsp_consultar_detalle_concepto_contables
GO
CREATE PROCEDURE gsp_consultar_detalle_concepto_contables
(
@par_EMPR_Codigo SMALLINT,
@par_ECCO_Codigo NUMERIC
)
AS
BEGIN

	SELECT 
	DCCO.EMPR_Codigo,
	DCCO.Codigo,
	DCCO.ECCO_Codigo,
	DCCO.PLUC_Codigo,
	DCCO.CATA_NACC_Codigo,
	DCCO.CATA_TCUC_Codigo,
	DCCO.CATA_DOCR_Codigo,
	DCCO.CATA_TEPC_Codigo,
	DCCO.CATA_CCPC_Codigo,
	DCCO.Valor_Base,
	DCCO.Porcentaje_Tarifa,
	DCCO.Genera_Cuenta,
	DCCO.Prefijo,
	DCCO.Codigo_Anexo,
	DCCO.Sufijo_Codigo_Anexo,
	DCCO.Campo_Auxiliar,
	PLUC.Nombre AS CuentaPUC,
	PLUC.Codigo_Cuenta AS CodigoCuentaPuc,
	NACC.Nombre AS NaturalezaConcepto,
	TCUC.Nombre AS TipoCuenta,
	DOCR.Nombre AS DocuentoCruce,
	TEPC.Nombre AS TerceroParametrizable,
	CCPC.Nombre AS CentroCostoParametrizable

	FROM
	Detalle_Concepto_Contables DCCO INNER JOIN Plan_Unico_Cuentas PLUC
	ON DCCO.EMPR_Codigo = PLUC.EMPR_Codigo 
	AND DCCO.PLUC_Codigo = PLUC.Codigo

	LEFT JOIN 
	V_Tipo_Naturaleza_Concepto_Contable NACC ON
	DCCO.EMPR_Codigo = NACC.EMPR_Codigo 
	AND DCCO.CATA_NACC_Codigo = NACC.Codigo

	LEFT JOIN 
	V_Tipo_Cuenta_Concepto TCUC ON
	DCCO.EMPR_Codigo = TCUC.EMPR_Codigo
	AND DCCO.CATA_TCUC_Codigo = TCUC.Codigo

	LEFT JOIN 
	V_Tipo_Documento_Cruce DOCR ON
	DCCO.EMPR_Codigo = DOCR.EMPR_Codigo
	AND DCCO.CATA_DOCR_Codigo = DOCR.Codigo

	LEFT JOIN 
	V_Tercero_Parametrizacion_Contable TEPC ON
	DCCO.EMPR_Codigo = TEPC.EMPR_Codigo
	AND DCCO.CATA_TEPC_Codigo = TEPC.Codigo

	LEFT JOIN 
	V_Centro_Costo_Parametrizacion_Contable CCPC ON
	DCCO.EMPR_Codigo = CCPC.EMPR_Codigo
	AND DCCO.CATA_CCPC_Codigo = CCPC.Codigo

	WHERE 
	DCCO.EMPR_Codigo = @par_EMPR_Codigo
	AND DCCO.ECCO_Codigo = @par_ECCO_Codigo

END
GO