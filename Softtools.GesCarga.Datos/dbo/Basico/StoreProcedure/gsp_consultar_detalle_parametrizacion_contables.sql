﻿PRINT 'gsp_consultar_detalle_parametrizacion_contables'
GO
DROP PROCEDURE gsp_consultar_detalle_parametrizacion_contables
GO
CREATE PROCEDURE gsp_consultar_detalle_parametrizacion_contables
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo NUMERIC
)
AS
BEGIN

	SELECT 
	DEPC.EMPR_Codigo,
	DEPC.Codigo,
	DEPC.ENPC_Codigo,
	DEPC.Orden,
	DEPC.CATA_NACC_Codigo,
	DEPC.PLUC_Codigo,
	DEPC.Descripcion,
	DEPC.CATA_TIPC_Codigo,
	DEPC.CATA_VTPC_Codigo,
	DEPC.CATA_DCPC_Codigo,
	DEPC.CATA_TEPC_Codigo,
	DEPC.CATA_CCPC_Codigo,
	DEPC.CATA_SIPC_Codigo,
	DEPC.CATA_TIDU_Codigo,
	DEPC.CATA_TICD_Codigo,
	DEPC.Prefijo,
	DEPC.Codigo_Anexo,
	DEPC.Sufijo_Codigo_Anexo,
	DEPC.Campo_Auxiliar,
	DEPC.CATA_TINA_Codigo,
	DEPC.Tercero_Filial,
	NACC.Nombre AS NaturalezaConcepto,
	TIPC.Nombre AS TipoParametrizacion,
	VTPC.Nombre AS ValorParametrizacion,
	DCPC.Nombre AS DocumentoCruce,
	TEPC.Nombre AS TerceroParametrizacion,
	CCPC.Nombre AS CentroCostoParametrizacion,
	SIPC.Nombre AS SifijoICAParametrizacion,
	TIDU.Nombre AS TipoDueño,
	TICD.Nombre AS TipoConductor,
	PLUC.Codigo_Cuenta AS CodigoCuentaPuc, 
	PLUC.Nombre AS CuentaPUC


	FROM
	Detalle_Parametrizacion_Contables DEPC INNER JOIN   V_Tipo_Naturaleza_Concepto_Contable NACC 
	ON DEPC.EMPR_Codigo = NACC.EMPR_Codigo
	AND DEPC.CATA_NACC_Codigo = NACC.Codigo


	LEFT JOIN  Plan_Unico_Cuentas PLUC
	ON DEPC.EMPR_Codigo = PLUC.EMPR_Codigo
	AND DEPC.PLUC_Codigo = PLUC.Codigo

	LEFT JOIN  V_Tipo_Parametrizacion_Contable TIPC
	ON DEPC.EMPR_Codigo = TIPC.EMPR_Codigo
	AND DEPC.CATA_TIPC_Codigo = TIPC.Codigo

	LEFT JOIN  V_Valor_Parametrizacion_Contable VTPC
	ON DEPC.EMPR_Codigo = VTPC.EMPR_Codigo
	AND DEPC.CATA_VTPC_Codigo = VTPC.Codigo

	LEFT JOIN  V_Documento_Cruce_Parametrizacion_Contable DCPC
	ON DEPC.EMPR_Codigo = DCPC.EMPR_Codigo
	AND DEPC.CATA_DCPC_Codigo = DCPC.Codigo

	LEFT JOIN  V_Tercero_Parametrizacion_Contable TEPC
	ON DEPC.EMPR_Codigo = TEPC.EMPR_Codigo
	AND DEPC.CATA_TEPC_Codigo = TEPC.Codigo

	LEFT JOIN  V_Centro_Costo_Parametrizacion_Contable CCPC
	ON DEPC.EMPR_Codigo = CCPC.EMPR_Codigo
	AND DEPC.CATA_CCPC_Codigo = CCPC.Codigo

	LEFT JOIN  V_Sufijo_ICA_Parametrizacion_Contable SIPC
	ON DEPC.EMPR_Codigo = SIPC.EMPR_Codigo
	AND DEPC.CATA_SIPC_Codigo = SIPC.Codigo

	LEFT JOIN  V_Tipo_Dueño_vehiculos TIDU
	ON DEPC.EMPR_Codigo = TIDU.EMPR_Codigo
	AND DEPC.CATA_TIDU_Codigo = TIDU.Codigo

	LEFT JOIN  V_Tipo_Tipo_Conductor TICD
	ON DEPC.EMPR_Codigo = TICD.EMPR_Codigo
	AND DEPC.CATA_TICD_Codigo = TICD.Codigo
	
	WHERE 

	DEPC.EMPR_Codigo = @par_EMPR_Codigo
	AND DEPC.ENPC_Codigo = @par_Codigo

END
GO