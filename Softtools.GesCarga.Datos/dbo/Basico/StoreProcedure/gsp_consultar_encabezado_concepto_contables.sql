﻿PRINT 'gsp_consultar_encabezado_concepto_contables'
GO
DROP PROCEDURE gsp_consultar_encabezado_concepto_contables
GO
CREATE PROCEDURE gsp_consultar_encabezado_concepto_contables
(
	@par_EMPR_Codigo SMALLINT,
	@par_Codigo NUMERIC = NULL,
	@par_OFAP_Codigo NUMERIC = NULL,
	@par_DOOR_Codigo NUMERIC = NULL,
	@par_TICC_Codigo NUMERIC = NULL,
	@par_Codigo_Alterno VARCHAR(20) = NULL,
	@par_Nombre VARCHAR(50) = NULL,
	@par_Estado SMALLINT = NULL,
	@par_NumeroPagina INT = NULL,
	@par_RegistrosPagina INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
		DECLARE
			@CantidadRegistros	INT
		SELECT @CantidadRegistros = (
				SELECT DISTINCT 
					COUNT(1) 
					FROM
						Encabezado_Concepto_Contables ENCC INNER JOIN V_Documentos_Origen DOOR ON 
						ENCC.EMPR_Codigo = DOOR.EMPR_Codigo
						AND ENCC.CATA_DOOR_Codigo= DOOR.Codigo

						LEFT JOIN 
						V_Tipo_Concepto_Contable TICC ON 
						ENCC.EMPR_Codigo = TICC.EMPR_Codigo
						AND ENCC.CATA_TICC_Codigo = TICC.Codigo

						LEFT JOIN 
						Valor_Catalogos TINA ON
						ENCC.EMPR_Codigo = TINA.EMPR_Codigo
						AND ENCC.CATA_TINA_Codigo = TINA.Codigo

						LEFT JOIN 
						Oficinas OFAP ON 
						ENCC.EMPR_Codigo = OFAP.EMPR_Codigo
						AND ENCC.OFIC_Codigo_Aplica = OFAP.Codigo	

					WHERE

					ENCC.EMPR_Codigo = @par_EMPR_Codigo
					AND ENCC.Codigo = ISNULL(@par_Codigo, ENCC.Codigo)
					AND ENCC.Estado = ISNULL(@par_Estado, ENCC.Estado)
					AND ((ENCC.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))
					AND ((ENCC.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))
					AND TICC.Codigo = ISNULL(@par_TICC_Codigo, TICC.Codigo)
					AND DOOR.Codigo = ISNULL(@par_DOOR_Codigo, DOOR.Codigo)
					AND OFAP.Codigo = ISNULL(@par_OFAP_Codigo, OFAP.Codigo)
					);
					       
				WITH Pagina AS
				(

					SELECT 
						ENCC.EMPR_Codigo,
						ENCC.Codigo,
						ENCC.Codigo_Alterno,
						ENCC.Nombre,
						ENCC.TIDO_Codigo,
						ENCC.CATA_DOOR_Codigo,
						ENCC.CATA_TICC_Codigo,
						ENCC.CATA_TINA_Codigo,
						ENCC.OFIC_Codigo_Aplica,
						ENCC.Observaciones,
						ENCC.OFIC_Codigo,
						ENCC.Fuente,
						ENCC.Fuente_Anulacion,
						ENCC.Estado,
						DOOR.Nombre AS DocumentoOrigen ,
						TICC.Nombre AS TipoConceptoContable,
						OFAP.Nombre AS OficinaAPlica,
						ROW_NUMBER() OVER(ORDER BY ENCC.Nombre) AS RowNumber
					FROM
						Encabezado_Concepto_Contables ENCC INNER JOIN V_Documentos_Origen DOOR ON 
						ENCC.EMPR_Codigo = DOOR.EMPR_Codigo
						AND ENCC.CATA_DOOR_Codigo= DOOR.Codigo

						LEFT JOIN 
						V_Tipo_Concepto_Contable TICC ON 
						ENCC.EMPR_Codigo = TICC.EMPR_Codigo
						AND ENCC.CATA_TICC_Codigo = TICC.Codigo

						LEFT JOIN 
						Valor_Catalogos TINA ON
						ENCC.EMPR_Codigo = TINA.EMPR_Codigo
						AND ENCC.CATA_TINA_Codigo = TINA.Codigo

						LEFT JOIN 
						Oficinas OFAP ON 
						ENCC.EMPR_Codigo = OFAP.EMPR_Codigo
						AND ENCC.OFIC_Codigo_Aplica = OFAP.Codigo	

					WHERE

					ENCC.EMPR_Codigo = @par_EMPR_Codigo
					AND ENCC.Codigo = ISNULL(@par_Codigo, ENCC.Codigo)
					AND ENCC.Estado = ISNULL(@par_Estado, ENCC.Estado)
					AND ((ENCC.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))
					AND ((ENCC.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))
					AND TICC.Codigo = ISNULL(@par_TICC_Codigo, TICC.Codigo)
					AND DOOR.Codigo = ISNULL(@par_DOOR_Codigo, DOOR.Codigo)
					AND OFAP.Codigo = ISNULL(@par_OFAP_Codigo, OFAP.Codigo)
		)
		SELECT DISTINCT
		0 AS Obtener,
		EMPR_Codigo,
		Codigo,
		Codigo_Alterno,
		Nombre,
		TIDO_Codigo,
		CATA_DOOR_Codigo,
		CATA_TICC_Codigo,
		CATA_TINA_Codigo,
		OFIC_Codigo_Aplica,
		Observaciones,
		OFIC_Codigo,
		Fuente,
		Fuente_Anulacion,
		Estado,
		DocumentoOrigen ,
		TipoConceptoContable,
		OficinaAPlica,
		@CantidadRegistros AS TotalRegistros,
		@par_NumeroPagina AS PaginaObtener,
		@par_RegistrosPagina AS RegistrosPagina
		FROM
			Pagina
		WHERE
			RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
			AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
		order by Nombre
	END
GO