﻿PRINT 'gsp_consultar_encabezado_parametrizacion_contables'
GO
DROP PROCEDURE gsp_consultar_encabezado_parametrizacion_contables
GO
CREATE PROCEDURE gsp_consultar_encabezado_parametrizacion_contables
(
	@par_EMPR_Codigo SMALLINT,
	@par_Codigo NUMERIC = NULL,
	@par_Codigo_Alterno VARCHAR(20) = NULL,
	@par_Nombre VARCHAR(50) = NULL,
	@par_Estado SMALLINT = NULL,
	@par_NumeroPagina INT = NULL,
	@par_RegistrosPagina INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
		DECLARE
			@CantidadRegistros	INT
		SELECT @CantidadRegistros = (
				SELECT DISTINCT 
					COUNT(1) 
					FROM
					Encabezado_Parametrizacion_Contables ENPC,
					V_Tipo_Documento_Genera TIDG,
					V_Tipo_Generacion_Detalle_Contable TGDC

					WHERE 

					ENPC.EMPR_Codigo = @par_EMPR_Codigo
					AND ENPC.Codigo = ISNULL(@par_Codigo, ENPC.Codigo)
					AND ENPC.Estado = ISNULL(@par_Estado, ENPC.Estado)
					AND ((ENPC.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))
					AND ((ENPC.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))

					AND ENPC.EMPR_Codigo = TIDG.EMPR_Codigo
					AND ENPC.CATA_TIDG_Codigo = TIDG.Codigo

					AND ENPC.EMPR_Codigo = TGDC.EMPR_Codigo
					AND ENPC.CATA_TGDC_Codigo = TGDC.Codigo
					);
					       
				WITH Pagina AS
				(
					SELECT 
					ENPC.EMPR_Codigo,
					ENPC.Codigo,
					ENPC.Codigo_Alterno,
					ENPC.Nombre,
					ENPC.CATA_TIDG_Codigo,
					ENPC.Fuente,
					ENPC.Fuente_Anulacion,
					ENPC.CATA_TGDC_Codigo,
					ENPC.Observaciones,
					ENPC.Provision,
					ENPC.Estado,
					TIDG.Nombre AS TipoDocumentoGenera,
					TGDC.Nombre AS TipoGeneraDetalleContable,
					ROW_NUMBER() OVER(ORDER BY ENPC.Codigo) AS RowNumber

					FROM
					Encabezado_Parametrizacion_Contables ENPC,
					V_Tipo_Documento_Genera TIDG,
					V_Tipo_Generacion_Detalle_Contable TGDC

					WHERE 

					ENPC.EMPR_Codigo = @par_EMPR_Codigo
					AND ENPC.Codigo = ISNULL(@par_Codigo, ENPC.Codigo)
					AND ENPC.Estado = ISNULL(@par_Estado, ENPC.Estado)
					AND ((ENPC.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))
					AND ((ENPC.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))

					AND ENPC.EMPR_Codigo = TIDG.EMPR_Codigo
					AND ENPC.CATA_TIDG_Codigo = TIDG.Codigo

					AND ENPC.EMPR_Codigo = TGDC.EMPR_Codigo
					AND ENPC.CATA_TGDC_Codigo = TGDC.Codigo

		)
		SELECT DISTINCT
		0 AS Obtener,
		EMPR_Codigo,
		Codigo,
		Codigo_Alterno,
		Nombre,
		CATA_TIDG_Codigo,
		Fuente,
		Fuente_Anulacion,
		CATA_TGDC_Codigo,
		Observaciones,
		Provision,
		Estado,
		TipoDocumentoGenera,
		TipoGeneraDetalleContable,
		@CantidadRegistros AS TotalRegistros,
		@par_NumeroPagina AS PaginaObtener,
		@par_RegistrosPagina AS RegistrosPagina
		FROM
			Pagina
		WHERE
			RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
			AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
		order by Nombre
	END
GO