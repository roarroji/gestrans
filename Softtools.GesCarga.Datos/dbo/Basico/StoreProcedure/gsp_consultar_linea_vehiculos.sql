﻿PRINT 'gsp_consultar_linea_vehiculos'
GO
DROP PROCEDURE gsp_consultar_linea_vehiculos
GO
CREATE PROCEDURE gsp_consultar_linea_vehiculos 
(  
 @par_EMPR_Codigo SMALLINT,  
 @par_Codigo NUMERIC = NULL,  
 @par_Codigo_Alterno VARCHAR(20) = NULL,  
 @par_Nombre VARCHAR(50) = NULL,  
 @par_Nombre_Marca VARCHAR(50) = NULL,  
 @par_MAVE_Codigo NUMERIC = NULL,  
 @par_Estado SMALLINT = NULL,  
 @par_NumeroPagina INT = NULL,  
 @par_RegistrosPagina INT = NULL  
)  
AS  
BEGIN  
 SET NOCOUNT ON;  
  DECLARE  
   @CantidadRegistros INT  
  SELECT @CantidadRegistros = (  
    SELECT DISTINCT   
     COUNT(1)   
    FROM  
     Linea_Vehiculos LIVE,  
     Marca_Vehiculos MAVE  
    WHERE  
     LIVE.Codigo <> 0  
     AND LIVE.Codigo <> 18263  
     AND LIVE.EMPR_Codigo = MAVE.EMPR_Codigo   
     AND LIVE.MAVE_Codigo = MAVE.Codigo  
  
     AND LIVE.EMPR_Codigo = @par_EMPR_Codigo  
     AND LIVE.Estado = ISNULL(@par_Estado, LIVE.Estado)  
     AND LIVE.Codigo = ISNULL(@par_Codigo, LIVE.Codigo)  
     AND ((LIVE.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  
     AND ((LIVE.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))  
     AND ((MAVE.Nombre LIKE '%' + @par_Nombre_Marca + '%') OR (@par_Nombre_Marca IS NULL))  
     AND LIVE.MAVE_Codigo = ISNULL(@par_MAVE_Codigo,LIVE.MAVE_Codigo)  
     );  
              
    WITH Pagina AS  
    (  
  
    SELECT  
     0 AS Obtener,  
     LIVE.EMPR_Codigo,  
     LIVE.Codigo,  
     LIVE.Codigo_Alterno,  
     LIVE.Nombre,  
     LIVE.Estado,  
     LIVE.MAVE_Codigo,  
     MAVE.Nombre AS NombreMarca,  
     ROW_NUMBER() OVER(ORDER BY LIVE.Nombre) AS RowNumber  
    FROM  
     Linea_Vehiculos LIVE,  
     Marca_Vehiculos MAVE  
    WHERE  
     LIVE.Codigo <> 0  
     AND LIVE.Codigo <> 18263 
     AND LIVE.EMPR_Codigo = MAVE.EMPR_Codigo   
     AND LIVE.MAVE_Codigo = MAVE.Codigo  
  
     AND LIVE.EMPR_Codigo = @par_EMPR_Codigo  
     AND LIVE.Estado = ISNULL(@par_Estado, LIVE.Estado)  
     AND LIVE.Codigo = ISNULL(@par_Codigo, LIVE.Codigo)  
     AND ((LIVE.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))  
     AND ((LIVE.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))  
     AND ((MAVE.Nombre LIKE '%' + @par_Nombre_Marca + '%') OR (@par_Nombre_Marca IS NULL))  
     AND LIVE.MAVE_Codigo = ISNULL(@par_MAVE_Codigo,LIVE.MAVE_Codigo)  
  )  
  SELECT DISTINCT  
  0 AS Obtener,  
  EMPR_Codigo,  
  Codigo,  
  Codigo_Alterno,  
  Nombre,  
  Estado,  
  MAVE_Codigo,  
  NombreMarca,  
  @CantidadRegistros AS TotalRegistros,  
  @par_NumeroPagina AS PaginaObtener,  
  @par_RegistrosPagina AS RegistrosPagina  
  FROM  
   Pagina  
  WHERE  
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
   AND RowNumber<= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
  order by Nombre  
END  
GO