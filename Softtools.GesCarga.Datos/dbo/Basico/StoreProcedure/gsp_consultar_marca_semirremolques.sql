﻿PRINT 'gsp_consultar_marca_semirremolques'
GO
DROP PROCEDURE gsp_consultar_marca_semirremolques
GO
CREATE PROCEDURE gsp_consultar_marca_semirremolques 
(  
 @par_EMPR_Codigo SMALLINT,    
 @par_Codigo NUMERIC = NULL,    
 @par_Codigo_Alterno VARCHAR(20) = NULL,    
 @par_Nombre VARCHAR(50) = NULL,    
 @par_Estado INT = NULL,    
 @par_NumeroPagina INT = NULL,    
 @par_RegistrosPagina INT = NULL    
)  
AS  
BEGIN  
 SET NOCOUNT ON;    
  DECLARE    
   @CantidadRegistros INT    
  SELECT @CantidadRegistros = (    
    SELECT DISTINCT     
     COUNT(1)     
    FROM    
     Marca_Semirremolques MASE    
    WHERE
	 MASE.Codigo <> 0      
     AND MASE.Codigo <> 992    
     AND MASE.EMPR_Codigo = @par_EMPR_Codigo    
     AND MASE.Estado = ISNULL(@par_Estado, MASE.Estado)    
     AND MASE.Codigo = ISNULL(@par_Codigo, MASE.Codigo)    
     AND ((MASE.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))    
     AND ((MASE.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))    
     );    
                
    WITH Pagina AS    
    (    
    
    SELECT    
     0 AS Obtener,    
     MASE.EMPR_Codigo,    
     MASE.Codigo,    
     MASE.Codigo_Alterno,    
     MASE.Nombre,    
     MASE.Estado,    
     ROW_NUMBER() OVER(ORDER BY MASE.Nombre) AS RowNumber    
    FROM    
     Marca_Semirremolques MASE    
     WHERE
	 MASE.Codigo <> 0      
     AND MASE.Codigo <> 992     
     AND MASE.EMPR_Codigo = @par_EMPR_Codigo    
     AND MASE.Estado = ISNULL(@par_Estado, MASE.Estado)    
     AND MASE.Codigo = ISNULL(@par_Codigo, MASE.Codigo)    
     AND ((MASE.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))    
     AND ((MASE.Nombre LIKE '%' + @par_Nombre + '%') OR (@par_Nombre IS NULL))    
  )    
  SELECT DISTINCT    
  0 AS Obtener,    
  EMPR_Codigo,    
  Codigo,    
  Codigo_Alterno,    
  Nombre,    
  Estado,    
  @CantidadRegistros AS TotalRegistros,    
  @par_NumeroPagina AS PaginaObtener,    
  @par_RegistrosPagina AS RegistrosPagina    
  FROM    
   Pagina    
  WHERE    
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
  order by Nombre    
END  
GO