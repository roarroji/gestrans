﻿PRINT 'gsp_consultar_oficinas'
GO
DROP PROCEDURE gsp_consultar_oficinas
GO
CREATE PROCEDURE gsp_consultar_oficinas
(                    
	@par_EMPR_Codigo SMALLINT,                    
	@par_Codigo INT = NULL,                    
	@par_Codigo_Alterno VARCHAR(20) = NULL,                    
	@par_Nombre VARCHAR(40) = NULL,                    
	@par_Ciudad VARCHAR(35) = NULL,                    
	@par_Codigo_Ciudad INT = NULL,                  
	@par_Estado SMALLINT = NULL,      
	@par_Codigo_Region NUMERIC = NULL,  
	@par_Usuario INT = NULL,
	@par_NumeroPagina INT = NULL,                    
	@par_RegistrosPagina INT = NULL
)                    
AS                    
BEGIN
	
	IF(@par_Usuario is not null)
	BEGIN
		--Para Combos con filtro de usuario
		SELECT DISTINCT
		OFIC.EMPR_Codigo,                    
		OFIC.Codigo,                    
		OFIC.Codigo_Alterno,                    
		OFIC.Nombre,
		OFIC.Estado
		FROM Oficinas OFIC

		INNER JOIN Usuario_Oficinas USOF ON
		OFIC.EMPR_Codigo = USOF.EMPR_Codigo
		AND OFIC.Codigo = USOF.OFIC_Codigo

		WHERE
		OFIC.Codigo > 0
		AND OFIC.EMPR_Codigo = @par_EMPR_Codigo
		AND USOF.USUA_Codigo = @par_Usuario
	   
	END
	ELSE
	BEGIN
		SET NOCOUNT ON;                    
		DECLARE @CantidadRegistros INT                    
		SELECT @CantidadRegistros = (
			SELECT DISTINCT                  
			COUNT(1)                     
			FROM                    
			Oficinas OFIC         
			LEFT JOIN          
			Zona_Ciudades ZONA    ON         
			OFIC.EMPR_Codigo = ZONA.EMPR_Codigo          
			AND OFIC.ZOCI_Codigo = ZONA.Codigo,
			Ciudades CIUD
		
			WHERE                    
			(OFIC.EMPR_Codigo = CIUD.EMPR_Codigo OR CIUD.EMPR_Codigo IS NULL)  
			AND OFIC.Codigo > 0    
			AND (OFIC.CIUD_Codigo = CIUD.Codigo OR CIUD.Codigo IS NULL)  
			AND OFIC.EMPR_Codigo = @par_EMPR_Codigo                    
			AND ((OFIC.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))                    
			AND ((OFIC.Nombre LIKE '%' + ISNULL(@par_Nombre, OFIC.Nombre) + '%') OR (@par_Nombre IS NULL))                   
			AND (OFIC.CIUD_Codigo = ISNULL(@par_Codigo_Ciudad, OFIC.CIUD_Codigo) OR @par_Codigo_Ciudad IS NULL)  
			AND OFIC.Estado = ISNULL(@par_Estado, OFIC.Estado)                    
			AND (OFIC.Codigo = ISNULL(@par_Codigo, OFIC.Codigo) OR @par_Codigo IS NULL)  
			AND ((CIUD.Nombre LIKE '%' + ISNULL(@par_Ciudad, CIUD.Nombre) + '%') OR (@par_Ciudad IS NULL))             
			AND (OFIC.REPA_Codigo = @par_Codigo_Region OR @par_Codigo_Region IS NULL)
		);                   
		WITH Pagina AS
		(
			SELECT DISTINCT               
			OFIC.EMPR_Codigo,                    
			OFIC.Codigo,                    
			OFIC.Codigo_Alterno,                    
			OFIC.Nombre,                    
			OFIC.CIUD_Codigo,                    
			OFIC.Direccion,                    
			OFIC.Telefono,                    
			OFIC.Email,                    
			OFIC.Resolucion_Facturacion,                    
			OFIC.Estado,                    
			CIUD.Nombre AS Ciudad,                    
			OFIC.CATA_TIOF_Codigo,              
			ISNULL(ZONA.Codigo,0) AS Codigo_Zona,          
			ISNULL(ZONA.Nombre,'') AS Nombre_Zona  ,          
			ROW_NUMBER() OVER(ORDER BY OFIC.Codigo) AS RowNumber                    
			FROM                    
			Oficinas OFIC              
			LEFT JOIN          
			Zona_Ciudades ZONA    ON         
			OFIC.EMPR_Codigo = ZONA.EMPR_Codigo          
			AND OFIC.ZOCI_Codigo = ZONA.Codigo
			,
			Ciudades CIUD        

			WHERE                    
			(OFIC.EMPR_Codigo = CIUD.EMPR_Codigo OR CIUD.EMPR_Codigo IS NULL)  
			AND OFIC.Codigo > 0    
			AND (OFIC.CIUD_Codigo = CIUD.Codigo OR CIUD.Codigo IS NULL)  
			AND OFIC.EMPR_Codigo = @par_EMPR_Codigo                    
			AND ((OFIC.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))                    
			AND ((OFIC.Nombre LIKE '%' + ISNULL(@par_Nombre, OFIC.Nombre) + '%') OR (@par_Nombre IS NULL))                   
			AND (OFIC.CIUD_Codigo = ISNULL(@par_Codigo_Ciudad, OFIC.CIUD_Codigo) OR @par_Codigo_Ciudad IS NULL)  
			AND OFIC.Estado = ISNULL(@par_Estado, OFIC.Estado)                    
			AND (OFIC.Codigo = ISNULL(@par_Codigo, OFIC.Codigo) OR @par_Codigo IS NULL)  
			AND ((CIUD.Nombre LIKE '%' + ISNULL(@par_Ciudad, CIUD.Nombre) + '%') OR (@par_Ciudad IS NULL))             
			AND (OFIC.REPA_Codigo = @par_Codigo_Region OR @par_Codigo_Region IS NULL)
		)
		SELECT                   
		0 AS Obtener,                    
		EMPR_Codigo,                    
		Codigo,                    
		Codigo_Alterno,                    
		Nombre,                    
		CIUD_Codigo,                    
		Direccion,                    
		Telefono,           
		Email,                    
		ISNULL(Resolucion_Facturacion,'') AS Resolucion_Facturacion,                    
		Estado,                    
		Ciudad,                    
		CATA_TIOF_Codigo,               
		Codigo_Zona,          
		Nombre_Zona,         
          
		@CantidadRegistros AS TotalRegistros,                    
		@par_NumeroPagina AS PaginaObtener,                    
		@par_RegistrosPagina AS RegistrosPagina                    
		FROM                    
		Pagina                    
		WHERE                    
		RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                    
		AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                    
		ORDER BY Nombre
	END               
END      
GO
