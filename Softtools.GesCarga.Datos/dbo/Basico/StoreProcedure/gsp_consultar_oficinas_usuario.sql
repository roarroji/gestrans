﻿Print 'gsp_consultar_oficinas_usuario'
GO
DROP PROCEDURE gsp_consultar_oficinas_usuario
GO
CREATE PROCEDURE gsp_consultar_oficinas_usuario(  
  @par_EMPR_Codigo SMALLINT,  
  @par_USUA_Codigo SMALLINT  
  )  
AS  
BEGIN    
    SELECT 1 As Obtener,   
  USOF.EMPR_Codigo,  
  USOF.OFIC_Codigo AS Codigo,  
  OFIC.Codigo_Alterno,  
  OFIC.Nombre,  
  OFIC.Direccion,  
  OFIC.Email,  
  OFIC.Telefono,  
  OFIC.CATA_TIOF_Codigo,
  ISNULL(OFIC.Resolucion_Facturacion,'')AS Resolucion_Facturacion,  
  OFIC.Estado,  
  OFIC.CIUD_Codigo,  
  ''as Ciudad  
    FROM Usuario_oficinas  USOF,  
 Oficinas OFIC  
    WHERE USOF.EMPR_Codigo = @par_EMPR_Codigo   
    AND USOF.USUA_Codigo = @par_USUA_Codigo  
 AND USOF.EMPR_Codigo = OFIC.EMPR_Codigo  
 AND USOF.OFIC_Codigo = OFIC.Codigo  
  
END  
GO