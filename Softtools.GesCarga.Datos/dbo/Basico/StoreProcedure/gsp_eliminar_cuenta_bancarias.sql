﻿PRINT 'gsp_eliminar_cuenta_bancarias'
GO
DROP PROCEDURE gsp_eliminar_cuenta_bancarias
GO
CREATE PROCEDURE gsp_eliminar_cuenta_bancarias
(
  @par_EMPR_Codigo SMALLINT,
  @par_Codigo NUMERIC
  )
AS
BEGIN
  DELETE Cuenta_Bancarias 
  WHERE EMPR_Codigo = @par_EMPR_Codigo 
    AND Codigo = @par_Codigo

    SELECT @@ROWCOUNT AS Numero
END
GO