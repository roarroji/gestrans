﻿PRINT 'gsp_eliminar_rutas'
GO
DROP PROCEDURE gsp_eliminar_rutas
GO
CREATE PROCEDURE gsp_eliminar_rutas(
  @par_EMPR_Codigo SMALLINT,
  @par_Codigo NUMERIC
  )
AS
BEGIN
  DELETE Rutas 
  WHERE EMPR_Codigo = @par_EMPR_Codigo 
    AND Codigo = @par_Codigo

    SELECT @@ROWCOUNT AS Numero

END
GO