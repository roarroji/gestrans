﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_guardar_TRM'
GO
DROP PROCEDURE gsp_guardar_TRM
GO
CREATE PROCEDURE gsp_guardar_TRM(  
@par_EMPR_Codigo NUMERIC,  
@par_MONE_Codigo NUMERIC,   
@par_FECHA DATE,   
@par_Valor_Moneda_local MONEY,   
@par_Usua_Modifica NUMERIC)  
  
AS BEGIN   

SET @par_FECHA =CONVERT (date,@par_FECHA,101)
DECLARE @Exite_Registro NUMERIC

SET @Exite_Registro = (SELECT COUNT (*) FROM Tasa_Cambio_Monedas WHERE EMPR_Codigo= @par_EMPR_Codigo 
AND MONE_Codigo = @par_MONE_Codigo AND Fecha = @par_FECHA )

	IF @Exite_Registro > 0 BEGIN 
		
		UPDATE Tasa_Cambio_Monedas SET    
		Valor_Moneda_Local=@par_Valor_Moneda_local,  
		USUA_Codigo_Modifica=@par_Usua_Modifica,  
		Fecha_Modifica = GETDATE()  
		WHERE  EMPR_Codigo= @par_EMPR_Codigo and MONE_Codigo= @par_MONE_Codigo and Fecha=@par_FECHA   
		
		SELECT @@ROWCOUNT as MONE_Codigo

	END

	IF @Exite_Registro = 0 BEGIN 
		
		INSERT INTO Tasa_Cambio_Monedas (EMPR_Codigo,MONE_Codigo,Fecha,Valor_Moneda_Local,USUA_Codigo_Crea,Fecha_Crea)  
		VALUES(@par_EMPR_Codigo,@par_MONE_Codigo,@par_FECHA,@par_Valor_Moneda_local,@par_Usua_Modifica,GETDATE())  
		
		SELECT MONE_Codigo FROM Tasa_Cambio_Monedas WHERE EMPR_Codigo= @par_EMPR_Codigo 
		AND MONE_Codigo = @par_MONE_Codigo AND Fecha = @par_FECHA
		
	END
END  
GO