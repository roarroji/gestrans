﻿PRINT 'gsp_insertar_cajas'
GO
DROP PROCEDURE gsp_insertar_cajas
GO
CREATE PROCEDURE gsp_insertar_cajas
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo_Alterno VARCHAR(20), 
@par_Nombre VARCHAR(50), 
@par_PLUC_Codigo SMALLINT,
@par_OFIC_Codigo SMALLINT,
@par_Estado SMALLINT,
@par_USUA_Codigo_Crea SMALLINT
)
AS
BEGIN
INSERT INTO Cajas
(
EMPR_Codigo,
Codigo_Alterno,
Nombre,
PLUC_Codigo,
OFIC_Codigo,
Estado,
USUA_Codigo_Crea,
Fecha_Crea
)
VALUES
(
@par_EMPR_Codigo,
@par_Codigo_Alterno,
@par_Nombre,
@par_PLUC_Codigo,
@par_OFIC_Codigo,
@par_Estado,
@par_USUA_Codigo_Crea,
GETDATE()
)

SELECT Codigo = @@identity

END
GO