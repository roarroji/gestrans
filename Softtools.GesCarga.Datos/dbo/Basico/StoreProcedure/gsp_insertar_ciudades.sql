﻿PRINT 'gsp_insertar_ciudades'
GO
DROP PROCEDURE gsp_insertar_ciudades
GO
CREATE PROCEDURE gsp_insertar_ciudades
(
	@par_EMPR_Codigo SMALLINT,
	@par_DEPA_Codigo INT,
	@par_Nombre VARCHAR(50),
	@par_Codigo_Alterno VARCHAR(20),
	--@par_Tarifa_Impuesto1 NUMERIC(18,6),
	--@par_Tarifa_Impuesto2 NUMERIC(18,6),
	@par_Estado SMALLINT,
	@par_USUA_Codigo_Crea SMALLINT
)
AS
BEGIN
	INSERT INTO
		Ciudades
		(
			EMPR_Codigo,
			DEPA_Codigo,
			Nombre,
			Codigo_Alterno,
			--Tarifa_Impuesto1,
			--Tarifa_Impuesto2,
			Estado,
			USUA_Codigo_Crea,
			Fecha_Crea
		)
	VALUES
	(
		@par_EMPR_Codigo,
		@par_DEPA_Codigo,
		@par_Nombre,
		@par_Codigo_Alterno,
		--@par_Tarifa_Impuesto1,
		--@par_Tarifa_Impuesto2,
		@par_Estado,
		@par_USUA_Codigo_Crea,
		GETDATE()
	)
SELECT Codigo = @@identity
END
GO