﻿PRINT 'gsp_insertar_cuenta_bancarias'
GO
DROP PROCEDURE gsp_insertar_cuenta_bancarias
GO
CREATE PROCEDURE gsp_insertar_cuenta_bancarias
(
@par_EMPR_Codigo SMALLINT ,
@par_BANC_Codigo SMALLINT, 
@par_Numero_Cuenta VARCHAR(30),
@par_CATA_TICB_Codigo NUMERIC,
@par_Nombre VARCHAR(50),
@par_TERC_Codigo NUMERIC ,
@par_PLUC_Codigo NUMERIC ,
@par_Sobregiro_Autorizado MONEY,
@par_Saldo_Actual MONEY ,
@par_Estado SMALLINT ,
@par_USUA_Codigo_Crea SMALLINT 
)
AS
BEGIN
DECLARE @Codigo NUMERIC 
select @Codigo = ISNULL(MAX(Codigo),0)+1 from Cuenta_Bancarias WHERE EMPR_Codigo = @par_EMPR_Codigo 

INSERT INTO Cuenta_Bancarias
(
EMPR_Codigo,
Codigo,
BANC_Codigo,
Numero_Cuenta,
CATA_TICB_Codigo,
Nombre,
TERC_Codigo,
PLUC_Codigo,
Sobregiro_Autorizado,
Saldo_Actual,
Estado,
USUA_Codigo_Crea,
Fecha_Crea
) 
VALUES
(
@par_EMPR_Codigo,
@Codigo,
@par_BANC_Codigo,
@par_Numero_Cuenta,
@par_CATA_TICB_Codigo,
@par_Nombre,
@par_TERC_Codigo,
@par_PLUC_Codigo,
@par_Sobregiro_Autorizado,
@par_Saldo_Actual,
@par_Estado,
@par_USUA_Codigo_Crea,
GETDATE()
) 
SELECT @Codigo as Codigo

END
GO