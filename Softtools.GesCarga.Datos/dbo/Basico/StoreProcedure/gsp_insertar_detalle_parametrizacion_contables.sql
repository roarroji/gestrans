﻿PRINT 'gsp_insertar_detalle_parametrizacion_contables'
GO
DROP PROCEDURE gsp_insertar_detalle_parametrizacion_contables
GO
CREATE PROCEDURE gsp_insertar_detalle_parametrizacion_contables
(
@par_EMPR_Codigo SMALLINT,
@par_ENPC_Codigo NUMERIC,
@par_Orden NUMERIC,
@par_CATA_NACC_Codigo NUMERIC,
@par_PLUC_Codigo SMALLINT,
@par_Descripcion VARCHAR (250),
@par_CATA_TIPC_Codigo NUMERIC,
@par_CATA_VTPC_Codigo NUMERIC, 
@par_CATA_DCPC_Codigo NUMERIC,
@par_CATA_TEPC_Codigo NUMERIC,
@par_CATA_CCPC_Codigo NUMERIC, 
@par_CATA_SIPC_Codigo NUMERIC,
@par_CATA_TIDU_Codigo NUMERIC, 
@par_CATA_TICD_Codigo NUMERIC, 
@par_Prefijo VARCHAR(10),
@par_Codigo_Anexo  VARCHAR(10),
@par_Sufijo_Codigo_Anexo  VARCHAR(10),
@par_Campo_Auxiliar VARCHAR(10),
@par_CATA_TINA_Codigo NUMERIC, 
@par_Tercero_Filial SMALLINT
)
AS
BEGIN
INSERT INTO Detalle_Parametrizacion_Contables (
	EMPR_Codigo,
	ENPC_Codigo,
	Orden,
	CATA_NACC_Codigo,
	PLUC_Codigo,
	Descripcion,
	CATA_TIPC_Codigo,
	CATA_VTPC_Codigo,
	CATA_DCPC_Codigo,
	CATA_TEPC_Codigo,
	CATA_CCPC_Codigo,
	CATA_SIPC_Codigo,
	CATA_TIDU_Codigo,
	CATA_TICD_Codigo,
	Prefijo,
	Codigo_Anexo,
	Sufijo_Codigo_Anexo,
	Campo_Auxiliar,
	CATA_TINA_Codigo,
	Tercero_Filial
)VALUES (
	@par_EMPR_Codigo,
	@par_ENPC_Codigo,
	@par_Orden,
	@par_CATA_NACC_Codigo,
	@par_PLUC_Codigo,
	@par_Descripcion,
	@par_CATA_TIPC_Codigo,
	@par_CATA_VTPC_Codigo,
	@par_CATA_DCPC_Codigo,
	@par_CATA_TEPC_Codigo,
	@par_CATA_CCPC_Codigo,
	@par_CATA_SIPC_Codigo,
	@par_CATA_TIDU_Codigo,
	@par_CATA_TICD_Codigo,
	@par_Prefijo,
	@par_Codigo_Anexo,
	@par_Sufijo_Codigo_Anexo,
	@par_Campo_Auxiliar,
	@par_CATA_TINA_Codigo,
	@par_Tercero_Filial
)

SELECT @@IDENTITY AS Codigo

END
GO