﻿PRINT 'gsp_insertar_encabezado_concepto_contables'
GO
DROP PROCEDURE gsp_insertar_encabezado_concepto_contables
GO
CREATE PROCEDURE gsp_insertar_encabezado_concepto_contables
(
@par_EMPR_Codigo SMALLINT, 
@par_Codigo_Alterno VARCHAR(20),
@par_Nombre VARCHAR (50),
@par_TIDO_Codigo SMALLINT, 
@par_CATA_DOOR_Codigo NUMERIC,
@par_CATA_TICC_Codigo NUMERIC,
@par_CATA_TINA_Codigo NUMERIC,
@par_OFIC_Codigo_Aplica SMALLINT, 
@par_Observaciones  VARCHAR (250) = NULL,
@par_OFIC_Codigo SMALLINT, 
@par_Fuente VARCHAR (10) = NULL,
@par_Fuente_Anulacion VARCHAR (10) = NULL,
@par_Estado SMALLINT, 
@par_USUA_Codigo_Crea SMALLINT
)
AS
BEGIN

	INSERT INTO Encabezado_Concepto_Contables
	(
	EMPR_Codigo,
	Codigo_Alterno,
	Nombre,
	TIDO_Codigo,
	CATA_DOOR_Codigo,
	CATA_TICC_Codigo,
	CATA_TINA_Codigo,
	OFIC_Codigo_Aplica,
	Observaciones,
	OFIC_Codigo,
	Fuente,
	Fuente_Anulacion,
	Estado,
	USUA_Codigo_Crea,
	Fecha_Crea
	)
	VALUES
	(
	@par_EMPR_Codigo,
	@par_Codigo_Alterno,
	@par_Nombre,
	@par_TIDO_Codigo,
	@par_CATA_DOOR_Codigo,
	@par_CATA_TICC_Codigo,
	@par_CATA_TINA_Codigo,
	@par_OFIC_Codigo_Aplica,
	ISNULL(@par_Observaciones,''),
	@par_OFIC_Codigo,
	ISNULL(@par_Fuente,''),
	ISNULL(@par_Fuente_Anulacion,''),
	@par_Estado,
	@par_USUA_Codigo_Crea,
	GETDATE()
	)

	SELECT @@IDENTITY AS Codigo
END
GO