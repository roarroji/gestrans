﻿PRINT 'gsp_insertar_encabezado_parametrizacion_contables'
GO
DROP PROCEDURE gsp_insertar_encabezado_parametrizacion_contables
GO
CREATE PROCEDURE gsp_insertar_encabezado_parametrizacion_contables
(
	@par_EMPR_Codigo SMALLINT,
	@par_Codigo_Alterno VARCHAR (20),
	@par_Nombre VARCHAR (50),
	@par_CATA_TIDG_Codigo NUMERIC,
	@par_Fuente VARCHAR (10),
	@par_Fuente_Anulacion VARCHAR(10), 
	@par_CATA_TGDC_Codigo NUMERIC,
	@par_Observaciones VARCHAR (250),
	@par_Provision SMALLINT,
	@par_Estado SMALLINT,
	@par_USUA_Codigo_Crea SMALLINT
)
AS
BEGIN
INSERT INTO Encabezado_Parametrizacion_Contables (
	EMPR_Codigo,
	Codigo_Alterno,
	Nombre,
	CATA_TIDG_Codigo,
	Fuente,
	Fuente_Anulacion,
	CATA_TGDC_Codigo,
	Observaciones,
	Provision,
	Estado,
	USUA_Codigo_Crea,
	Fecha_Crea
)
VALUES (

	@par_EMPR_Codigo,
	@par_Codigo_Alterno,
	@par_Nombre,
	@par_CATA_TIDG_Codigo,
	@par_Fuente,
	@par_Fuente_Anulacion,
	@par_CATA_TGDC_Codigo,
	@par_Observaciones,
	@par_Provision,
	@par_Estado,
	@par_USUA_Codigo_Crea,
	GETDATE()
)


SELECT @@IDENTITY AS Codigo

END
GO