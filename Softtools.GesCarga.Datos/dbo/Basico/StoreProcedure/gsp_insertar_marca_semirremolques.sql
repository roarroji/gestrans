﻿print 'gsp_insertar_marca_semirremolques'
GO
DROP PROCEDURE gsp_insertar_marca_semirremolques
GO
CREATE PROCEDURE gsp_insertar_marca_semirremolques
(
@par_EMPR_Codigo SMALLINT ,  
@par_Codigo_Alterno VARCHAR (20),  
@par_Nombre VARCHAR (50),  
@par_Estado SMALLINT,  
@par_USUA_Codigo_Crea SMALLINT  
)
AS
BEGIN
INSERT INTO Marca_Semirremolques  
(  
EMPR_Codigo,  
Codigo_Alterno,  
Nombre,  
Estado,  
USUA_Codigo_Crea,  
Fecha_Crea  
)  
VALUES  
(  
@par_EMPR_Codigo,  
@par_Codigo_Alterno,  
@par_Nombre,  
@par_Estado,  
@par_USUA_Codigo_Crea,  
GETDATE()  
)  
SELECT Codigo = @@identity  
END
GO