﻿PRINT 'gsp_insertar_oficinas'
GO
DROP PROCEDURE gsp_insertar_oficinas
GO
CREATE PROCEDURE gsp_insertar_oficinas
(            
@par_EMPR_Codigo SMALLINT,            
@par_Codigo_Alterno VARCHAR(20),            
@par_Nombre VARCHAR(50),            
@par_CIUD_Codigo NUMERIC,            
@par_Direccion VARCHAR(150),            
@par_Telefono VARCHAR(20),            
@par_Email VARCHAR(150),           
@par_CATA_TIOF_Codigo NUMERIC = NULL,      
@par_REPA_Codigo  NUMERIC = NULL,           
@par_Resolucion_Facturacion VARCHAR(250) = NULL,  
@par_Aplica_Enturnamiento NUMERIC,       
@par_Estado SMALLINT,            
@par_USUA_Codigo_Crea SMALLINT,        
@par_ZOCI_Codigo NUMERIC        
)            
AS             
BEGIN             
            
DECLARE @Codigo NUMERIC             
SELECT @Codigo = ISNULL(MAX(Codigo),0)+1 from Oficinas WHERE EMPR_Codigo = @par_EMPR_Codigo             
            
 INSERT INTO Oficinas            
 (            
  EMPR_Codigo,            
  Codigo,            
  Codigo_Alterno,            
  Nombre,            
  CIUD_Codigo,            
  Direccion,            
  Telefono,            
  Email,            
  Resolucion_Facturacion,            
  CATA_TIOF_Codigo,        
  REPA_Codigo,     
  Aplica_Enturnamiento,
  Estado,  
  USUA_Codigo_Crea,            
  Fecha_Crea,        
  ZOCI_Codigo          
 )            
 VALUES            
 (            
  @par_EMPR_Codigo,            
  @Codigo,            
  @par_Codigo_Alterno,            
  @par_Nombre,            
  @par_CIUD_Codigo,            
  @par_Direccion,            
  @par_Telefono,            
  @par_Email,            
  ISNULL(@par_Resolucion_Facturacion,''),      
  @par_CATA_TIOF_Codigo,     
  ISNULL(@par_REPA_Codigo, 0),
  @par_Aplica_Enturnamiento,
  @par_Estado,
  @par_USUA_Codigo_Crea,            
  GETDATE(),        
  @par_ZOCI_Codigo           
 )            
 SELECT Codigo = @Codigo            
END            