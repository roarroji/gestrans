﻿PRINT 'gsp_modificar_chequera_cuenta_bancarias'
GO
DROP PROCEDURE gsp_modificar_chequera_cuenta_bancarias
GO
CREATE PROCEDURE gsp_modificar_chequera_cuenta_bancarias
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo NUMERIC,
@par_CUBA_Codigo SMALLINT,
@par_Cheque_Inicial NUMERIC,
@par_Cheque_Final NUMERIC,
@par_Cheque_Actual NUMERIC,
@par_Estado SMALLINT,
@par_USUA_Codigo_Modifica SMALLINT
)
AS
BEGIN
UPDATE Chequera_Cuenta_Bancarias
SET
CUBA_Codigo = @par_CUBA_Codigo,
Cheque_Inicial = @par_Cheque_Inicial,
Cheque_Final = @par_Cheque_Final,
Cheque_Actual = @par_Cheque_Actual,
Estado = @par_Estado,
USUA_Modifica = @par_USUA_Codigo_Modifica,
Fecha_Modifica = getdate()

WHERE
EMPR_Codigo = @par_EMPR_Codigo
AND Codigo = @par_Codigo

SELECT @par_Codigo AS Codigo

END 
GO