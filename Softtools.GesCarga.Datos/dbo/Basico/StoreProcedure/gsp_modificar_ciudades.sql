﻿PRINT 'gsp_modificar_ciudades'
GO
DROP PROCEDURE gsp_modificar_ciudades
GO
CREATE PROCEDURE gsp_modificar_ciudades
(
	@par_EMPR_Codigo SMALLINT,
	@par_Codigo INT,
	@par_DEPA_Codigo INT,
	@par_Nombre VARCHAR(50),
	@par_Codigo_Alterno VARCHAR(20),
	--@par_Tarifa_Impuesto1 NUMERIC(18,6),
	--@par_Tarifa_Impuesto2 NUMERIC(18,6),
	@par_Estado SMALLINT,
	@par_USUA_Codigo SMALLINT
)
AS
BEGIN
	UPDATE
		Ciudades
	SET
		DEPA_Codigo = @par_DEPA_Codigo,
		Nombre = @par_Nombre,
		Codigo_Alterno = @par_Codigo_Alterno,
		--Tarifa_Impuesto1 = @par_Tarifa_Impuesto1,
		--Tarifa_Impuesto2 = @par_Tarifa_Impuesto2,
		Estado = @par_Estado,
		USUA_Modifica = @par_USUA_Codigo,
		Fecha_Modifica = GETDATE()
	WHERE
		EMPR_Codigo = @par_EMPR_codigo AND
		Codigo = @par_Codigo

SELECT @par_Codigo AS Codigo

END
GO