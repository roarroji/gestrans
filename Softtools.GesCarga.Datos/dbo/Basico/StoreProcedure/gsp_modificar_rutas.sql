﻿PRINT 'gsp_modificar_rutas'
GO
DROP PROCEDURE gsp_modificar_rutas
GO
CREATE PROCEDURE gsp_modificar_rutas 
(  
@par_EMPR_Codigo SMALLINT,  
@par_Codigo NUMERIC,  
@par_Codigo_Alterno VARCHAR (20),  
@par_Nombre VARCHAR (100),  
@par_CIUD_Codigo_Origen NUMERIC,  
@par_CIUD_Codigo_Destino NUMERIC,  
@par_CATA_TIRU_Codigo NUMERIC = null,  
@par_Duracion_Horas NUMERIC(18,2) = NULL,
@par_Estado SMALLINT,  
@par_USUA_Codigo_Modifica SMALLINT  
)  
AS   
BEGIN  
 UPDATE Rutas  
 SET  
 Codigo_Alterno = @par_Codigo_Alterno,  
 Nombre = @par_Nombre,  
 CIUD_Codigo_Origen = @par_CIUD_Codigo_Origen,  
 CIUD_Codigo_Destino = @par_CIUD_Codigo_Destino,  
 CATA_TIRU_Codigo = ISNULL(@par_CATA_TIRU_Codigo,4400),  
 Duracion_Horas = ISNULL(@par_Duracion_Horas, 0),
 Estado = @par_Estado,  
 USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica,  
 Fecha_Modifica = GETDATE()  
 WHERE   
 EMPR_Codigo =  @par_EMPR_Codigo  
 AND Codigo = @par_Codigo  
  
SELECT @par_Codigo AS Codigo  
END  
GO	