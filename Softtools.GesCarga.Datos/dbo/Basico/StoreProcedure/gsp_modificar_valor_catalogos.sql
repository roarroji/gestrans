﻿PRINT 'gsp_modificar_valor_catalogos'
GO
DROP PROCEDURE gsp_modificar_valor_catalogos
GO 
CREATE PROCEDURE  gsp_modificar_valor_catalogos 
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo NUMERIC,
@par_Campo1 VARCHAR(50),
@par_Campo2 VARCHAR(50) = NULL,
@par_Campo3 VARCHAR(50) = NULL,
@par_Campo4 VARCHAR(50) = NULL,
@par_Campo5 VARCHAR(50) = NULL,
@par_USUA_Modifica SMALLINT
)
AS 
BEGIN 
	
	UPDATE Valor_Catalogos SET
	Campo1 = @par_Campo1,
	Campo2 = ISNULL(@par_Campo2,''),
	Campo3 = ISNULL(@par_Campo3,''),
	Campo4 = ISNULL(@par_Campo4,''),
	Campo5 = ISNULL(@par_Campo5,''),
	USUA_Modifica = @par_USUA_Modifica,
	Fecha_Modifica = GETDATE()
	WHERE 
	EMPR_Codigo = @par_EMPR_Codigo 
	AND Codigo = @par_Codigo

SELECT Codigo 
FROM
Valor_Catalogos
WHERE 
EMPR_Codigo = @par_EMPR_Codigo 
AND Codigo = @par_Codigo 

END
GO