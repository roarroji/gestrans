﻿PRINT 'gsp_obtener_cajas'
GO
DROP PROCEDURE gsp_obtener_cajas
GO
CREATE PROCEDURE gsp_obtener_cajas
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo SMALLINT
)
AS
BEGIN
SELECT 
1 AS Obtener,
CAJA.EMPR_Codigo,
CAJA.Codigo,
CAJA.Codigo_Alterno,
CAJA.Nombre,
CAJA.PLUC_Codigo,
CAJA.OFIC_Codigo,
CAJA.Estado,
PLUC.Nombre AS CuentaPUC,
OFIC.Nombre AS Oficina
FROM
Cajas CAJA,
Plan_Unico_Cuentas PLUC,
Oficinas OFIC

WHERE

CAJA.EMPR_Codigo = @par_EMPR_Codigo
AND CAJA.Codigo = @par_Codigo

AND CAJA.EMPR_Codigo = PLUC.EMPR_Codigo
AND CAJA.PLUC_Codigo = PLUC.Codigo

AND CAJA.EMPR_Codigo = OFIC.EMPR_Codigo 
AND CAJA.OFIC_Codigo = OFIC.Codigo

END
GO