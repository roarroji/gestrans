﻿PRINT 'gsp_obtener_color_vehiculos'
GO
DROP PROCEDURE gsp_obtener_color_vehiculos
GO
CREATE PROCEDURE gsp_obtener_color_vehiculos
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo NUMERIC
)
AS 
BEGIN

	SELECT
		1 AS Obtener,
		COVE.EMPR_Codigo,
		COVE.Codigo,
		COVE.Codigo_Alterno,
		COVE.Nombre,
		COVE.Estado,
		ROW_NUMBER() OVER(ORDER BY COVE.Nombre) AS RowNumber
	FROM
		Color_Vehiculos COVE
	WHERE
		COVE.EMPR_Codigo = @par_EMPR_Codigo
		AND COVE.Codigo = @par_Codigo
END
GO