﻿PRINT 'gsp_obtener_cuenta_bancarias'
GO
DROP PROCEDURE gsp_obtener_cuenta_bancarias
GO
CREATE PROCEDURE gsp_obtener_cuenta_bancarias
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo NUMERIC
)
AS
BEGIN
SELECT 
1 AS Obtener,
CUBA.EMPR_Codigo,
CUBA.Codigo,
CUBA.BANC_Codigo,
CUBA.Numero_Cuenta,
CUBA.CATA_TICB_Codigo,
CUBA.Nombre,
CUBA.TERC_Codigo,
CUBA.PLUC_Codigo,
CUBA.Sobregiro_Autorizado,
CUBA.Saldo_Actual,
CUBA.Estado,
BANC.Nombre AS NombreBanco,
TICB.Campo1 AS TipoCuentaBancaria,
PLUC.Nombre AS NombreCuentaPUC,
TERC.Numero_Identificacion,
TERC.Razon_Social + TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 AS NombreTercero

FROM 
Cuenta_Bancarias  CUBA,
Bancos BANC,
Valor_Catalogos TICB,
Plan_Unico_Cuentas PLUC,
Terceros TERC
WHERE 

CUBA.EMPR_Codigo = BANC.EMPR_Codigo 
AND CUBA.BANC_Codigo = BANC.Codigo

AND CUBA.EMPR_Codigo = TICB.EMPR_Codigo
AND CUBA.CATA_TICB_Codigo = TICB.Codigo 

AND CUBA.EMPR_Codigo = PLUC.EMPR_Codigo
AND CUBA.PLUC_Codigo = PLUC.Codigo

AND CUBA.EMPR_Codigo = TERC.EMPR_Codigo
AND CUBA.TERC_Codigo = TERC.Codigo

AND CUBA.EMPR_codigo = @par_EMPR_Codigo
AND CUBA.Codigo = @par_Codigo

END
GO