﻿print 'gsp_obtener_marca_semirremolques'
GO
DROP PROCEDURE gsp_obtener_marca_semirremolques
GO
CREATE PROCEDURE gsp_obtener_marca_semirremolques  
(
 @par_EMPR_Codigo SMALLINT,  
@par_Codigo NUMERIC   
)
AS
BEGIN
   SELECT  
  1 AS Obtener,  
  MASE.EMPR_Codigo,  
  MASE.Codigo,  
  MASE.Codigo_Alterno,  
  MASE.Nombre,  
  MASE.Estado,  
  ROW_NUMBER() OVER(ORDER BY MASE.Nombre) AS RowNumber  
 FROM  
  Marca_Semirremolques MASE
 WHERE  
  
  MASE.EMPR_Codigo = @par_EMPR_Codigo  
  AND MASE.Codigo = @par_Codigo  
END
GO