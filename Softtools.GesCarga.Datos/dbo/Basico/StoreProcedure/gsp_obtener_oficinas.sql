﻿Print 'gsp_obtener_oficinas'
GO
DROP PROCEDURE gsp_obtener_oficinas
GO
CREATE PROCEDURE gsp_obtener_oficinas   
(          
@par_EMPR_Codigo SMALLINT,          
@par_Codigo NUMERIC        
)          
AS           
BEGIN           
 SELECT           
  1 AS Obtener,          
  OFIC.EMPR_Codigo,          
  OFIC.Codigo,          
  OFIC.Codigo_Alterno,          
  OFIC.Nombre,          
  OFIC.CIUD_Codigo,          
  OFIC.Direccion,          
  OFIC.Telefono,          
  OFIC.Email,          
  OFIC.Resolucion_Facturacion,
  OFIC.Aplica_Enturnamiento,
  OFIC.Estado,         
  OFIC.CATA_TIOF_Codigo,       
  OFIC.REPA_Codigo,   
  CIUD.Nombre AS Ciudad,    
  ISNULL(ZONA.Codigo,0) AS Codigo_Zona,        
  ISNULL(ZONA.Nombre,'') AS Nombre_Zona    
  FROM          
   Oficinas OFIC    
  LEFT JOIN        
  Zona_Ciudades ZONA    ON       
  OFIC.EMPR_Codigo = ZONA.EMPR_Codigo        
  AND OFIC.ZOCI_Codigo = ZONA.Codigo,         
     
   Ciudades CIUD          
  WHERE          
   OFIC.EMPR_Codigo = CIUD.EMPR_Codigo          
   AND OFIC.CIUD_Codigo = CIUD.Codigo          
   AND OFIC.Codigo <> 0          
   AND OFIC.EMPR_Codigo = @par_EMPR_Codigo          
   AND OFIC.Codigo = @par_Codigo          
END     