﻿PRINT 'gsp_obtener_rutas'
GO
DROP PROCEDURE gsp_obtener_rutas
GO
CREATE PROCEDURE gsp_obtener_rutas 
(  
 @par_EMPR_Codigo SMALLINT,  
 @par_Codigo NUMERIC  
)  
AS   
BEGIN  
  
 SELECT  
  1 AS Obtener,  
  RUTA.EMPR_Codigo,  
  RUTA.Codigo,  
  RUTA.Codigo_Alterno,  
  RUTA.Nombre,  
  RUTA.Duracion_Horas,
  RUTA.Estado,  
  RUTA.CIUD_Codigo_Origen,  
  RUTA.CIUD_Codigo_Destino,  
  CIOR.Nombre AS CiudadOrigen,  
  CIDE.Nombre AS CiudadDestino,  
  ISNULL(RUTA.CATA_TIRU_Codigo,4400) AS CATA_TIRU_Codigo ,  
  ISNULL(TIRU.Campo1,'') as TipoRuta,  
  ROW_NUMBER() OVER(ORDER BY RUTA.Nombre) AS RowNumber  
 FROM  
  Rutas RUTA  
  LEFT JOIN Valor_Catalogos as TIRU  
  ON  RUTA.EMPR_Codigo = TIRU.EMPR_Codigo   
  AND RUTA.CATA_TIRU_Codigo = TIRU.Codigo,  
  Ciudades CIOR,  
  Ciudades CIDE  
 WHERE  
  RUTA.EMPR_Codigo = CIOR.EMPR_Codigo   
  AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo  
  
  AND RUTA.EMPR_Codigo = CIDE.EMPR_Codigo   
  AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo  
  
  AND RUTA.EMPR_Codigo = @par_EMPR_Codigo  
  AND RUTA.Codigo = @par_Codigo  
  
END  
GO	