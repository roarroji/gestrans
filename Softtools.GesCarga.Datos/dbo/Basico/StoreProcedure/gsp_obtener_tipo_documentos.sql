﻿PRINT 'gsp_consultar_tipo_documentos'
GO
DROP PROCEDURE gsp_consultar_tipo_documentos
GO    
CREATE PROCEDURE gsp_consultar_tipo_documentos 
(            
  @par_EMPR_Codigo SMALLINT  ,             
  @par_Codigo NUMERIC = NULL ,          
  @par_Nombre VARCHAR(100) = NULL, 
  @par_Cierre_Contable NUMERIC = NULL ,      
  @par_Estado NUMERIC = NULL ,      
  @par_NumeroPagina  NUMERIC = NULL,          
 @par_RegistrosPagina NUMERIC = NULL          
 )            
 AS            
 BEGIN            
  SET NOCOUNT ON;            
   DECLARE            
    @CantidadRegistros INT            
   SELECT @CantidadRegistros = (            
     SELECT DISTINCT             
      COUNT(1)             
      FROM Tipo_Documentos TIDO        
         
      WHERE  TIDO.EMPR_Codigo = @par_EMPR_Codigo          
      AND TIDO.Codigo = ISNULL(@par_Codigo,TIDO.Codigo)          
      AND TIDO.Nombre LIKE'%'+ ISNULL(@par_Nombre,TIDO.Nombre )+'%' 
	    AND TIDO.Cierre_Contable = ISNULL(@par_Cierre_Contable,TIDO.Cierre_Contable)     
        AND TIDO.Estado = ISNULL(@par_Estado,TIDO.Estado)      
        AND TIDO.Tipo_Documento_Sistema = 0  
         );                   
     WITH Pagina AS            
     (SELECT            
      TIDO.Codigo,      
      TIDO.EMPR_Codigo,        
      TIDO.Nombre,        
      TIDO.Consecutivo,      
      TIDO.CATA_TIGN_Codigo,  
	  TIDO.Cierre_Contable,      
      TIDO.Estado,        
      0 as Obtener,    
      ROW_NUMBER() OVER ( ORDER BY  TIDO.Codigo) AS RowNumber            
       FROM Tipo_Documentos TIDO        
         
      WHERE  TIDO.EMPR_Codigo = @par_EMPR_Codigo          
      AND TIDO.Codigo = ISNULL(@par_Codigo,TIDO.Codigo)          
      AND TIDO.Nombre LIKE'%'+ ISNULL(@par_Nombre,TIDO.Nombre )+'%'
	  AND TIDO.Cierre_Contable = ISNULL(@par_Cierre_Contable,TIDO.Cierre_Contable)    
      AND TIDO.Estado = ISNULL(@par_Estado,TIDO.Estado)      
      AND TIDO.Tipo_Documento_Sistema = 0  
  )           
   SELECT DISTINCT            
   Codigo,      
   EMPR_Codigo,        
   Nombre,        
   Consecutivo,      
   CATA_TIGN_Codigo,  
   Cierre_Contable,    
   Estado,        
   Obtener,    
    @CantidadRegistros AS TotalRegistros,            
    @par_NumeroPagina AS PaginaObtener,            
    @par_RegistrosPagina AS RegistrosPagina            
    FROM            
    Pagina            
    WHERE            
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)            
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)            
    order by Codigo            
   END   
GO     