﻿PRINT 'gsp_obtener_valor_catalogos' 
GO
DROP PROCEDURE gsp_obtener_valor_catalogos
GO
CREATE PROCEDURE gsp_obtener_valor_catalogos
(
@par_EMPR_Codigo SMALLINT,
@par_CATA_Codigo SMALLINT = NULL,
@par_Codigo NUMERIC = NULL
)
AS
BEGIN
	SELECT
		VACA.EMPR_Codigo,
		VACA.CATA_Codigo,
		VACA.Codigo,
		ISNULL(VACA.Campo1,'') AS Campo1,
		ISNULL(VACA.Campo2,'') AS Campo2,
		ISNULL(VACA.Campo3,'') AS Campo3,
		ISNULL(VACA.Campo4,'') AS Campo4,
		ISNULL(VACA.Campo5,'') AS Campo5,
		ISNULL(CATA.Nombre,'') AS Nombre
	FROM
		Valor_Catalogos AS VACA,
		Catalogos AS CATA
	WHERE
		VACA.EMPR_Codigo = @par_EMPR_Codigo

		AND VACA.EMPR_Codigo = CATA.EMPR_Codigo
		AND VACA.CATA_Codigo = CATA.Codigo
		AND VACA.Codigo = ISNULL(@par_Codigo, VACA.Codigo)
		AND VACA.CATA_Codigo = ISNULL(@par_CATA_Codigo, VACA.CATA_Codigo)
END
GO