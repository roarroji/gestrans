﻿Print 'Drop and create table Empresa_Factura_Electronica'
GO
DROP TABLE [dbo].[Empresa_Factura_Electronica]
GO
CREATE TABLE [dbo].[Empresa_Factura_Electronica](
	[EMPR_Codigo] [smallint] NOT NULL,
	[Proveedor] [varchar](50) NULL,
	[Usuario] [varchar](50) NULL,
	[Clave] [varchar](50) NULL,
	[Operador_Virtual] [varchar](50) NULL,
	[Clave_Tecnica_Factura] [varchar](50) NULL,
	[Clave_Externa_Factura] [varchar](50) NULL,
	[Prefijo_Nota_Credito] [varchar](50) NULL,
	[Clave_Externa_Nota_Credito] [varchar](50) NULL,
	[Prefijo_Factura] [varchar](50) NULL,
	[Dominio_Ftp] [varchar](max) NULL,
	[Ssh_Host_Key] [varchar](max) NULL,
	[Puerto_Ftp] [varchar](max) NULL,
 CONSTRAINT [PK_Empresa_Factura_Electronica_1] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

--Print 'Insert Info Empresa_Factura_Electronica'
--GO

--INSERT INTO [dbo].[Empresa_Factura_Electronica]
--           ([EMPR_Codigo]
--           ,[Proveedor]
--           ,[Usuario]
--           ,[Clave]
--           ,[Operador_Virtual]
--		     ,[Prefijo_Factura]
--           ,[Clave_Tecnica_Factura]
--           ,[Clave_Externa_Factura]
--           ,[Prefijo_Nota_Credito]
--           ,[Clave_Externa_Nota_Credito]
--           ,[Dominio_Ftp]
--           ,[Ssh_Host_Key]
--           ,[Puerto_Ftp])
--     VALUES
--           (2,
--		   'SAPHETY',
--		   'desarrollo@softtools.co',
--		   'Gestrans9)',
--		   'softtools',
--		   '',
--		   'Demo12345',
--		   'nE3LwUExeQ',
--		   '',
--		   'O6wSUmb1qZ',
--		   NULL,
--		   NULL,
--		   NULL)
--GO

