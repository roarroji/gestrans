﻿CREATE TABLE [dbo].[Consecutivo_Documento_Oficinas](
	[EMPR_Codigo] [smallint] NOT NULL,
	[Codigo] [smallint] IDENTITY(1,1) NOT NULL,
	[TIDO_Codigo] [numeric] NOT NULL,
	[OFIC_Codigo] [smallint] NOT NULL,
	[Consecutivo] [numeric](18, 0) NOT NULL,
	[Consecutivo_Hasta] [numeric](18, 0) NOT NULL,
	[Numero_Documentos_Faltantes_Aviso] [smallint] NOT NULL,
 CONSTRAINT [PK_Consecutivo_Documento_Oficinas] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Consecutivo_Documento_Oficinas]  WITH CHECK ADD  CONSTRAINT [FK_Consecutivo_Documento_Oficinas_Empresas] FOREIGN KEY([EMPR_Codigo])
REFERENCES [dbo].[Empresas] ([Codigo])
GO

ALTER TABLE [dbo].[Consecutivo_Documento_Oficinas] CHECK CONSTRAINT [FK_Consecutivo_Documento_Oficinas_Empresas]
GO

ALTER TABLE [dbo].[Consecutivo_Documento_Oficinas]  WITH CHECK ADD  CONSTRAINT [FK_Consecutivo_Documento_Oficinas_Oficinas] FOREIGN KEY([EMPR_Codigo], [OFIC_Codigo])
REFERENCES [dbo].[Oficinas] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Consecutivo_Documento_Oficinas] CHECK CONSTRAINT [FK_Consecutivo_Documento_Oficinas_Oficinas]
GO

ALTER TABLE [dbo].[Consecutivo_Documento_Oficinas]  WITH CHECK ADD  CONSTRAINT [FK_Consecutivo_Documento_Oficinas_Tipo_Documentos] FOREIGN KEY([EMPR_Codigo], [TIDO_Codigo])
REFERENCES [dbo].[Tipo_Documentos] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Consecutivo_Documento_Oficinas] CHECK CONSTRAINT [FK_Consecutivo_Documento_Oficinas_Tipo_Documentos]
GO