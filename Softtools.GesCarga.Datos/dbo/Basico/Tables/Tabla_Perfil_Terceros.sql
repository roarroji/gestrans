﻿DROP TABLE [Perfil_Terceros]
GO
CREATE TABLE [dbo].[Perfil_Terceros](
	[EMPR_Codigo] [smallint] NOT NULL,
	[TERC_Codigo] [numeric](18, 0) NOT NULL,
	[Codigo] [numeric](18, 0) NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Perfil_Terceros]  WITH CHECK ADD  CONSTRAINT [FK_Perfil_Terceros_Terceros] FOREIGN KEY([EMPR_Codigo], [TERC_Codigo])
REFERENCES [dbo].[Terceros] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Perfil_Terceros] CHECK CONSTRAINT [FK_Perfil_Terceros_Terceros]
GO
