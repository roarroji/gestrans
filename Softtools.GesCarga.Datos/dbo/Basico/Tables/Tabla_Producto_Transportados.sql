﻿PRINT 'Producto_Transportados'
GO
DROP TABLE [dbo].[Producto_Transportados]
GO
CREATE TABLE [dbo].[Producto_Transportados](
	[EMPR_Codigo] [numeric](18, 0) NOT NULL,
	[Codigo] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Codigo_Alterno] [varchar](20) NOT NULL,
	[Nombre] [varchar](40) NOT NULL,
	[Descripcion] [varchar](255) NOT NULL,
	[Estado] [SMALLINT] NOT NULL,
	[Fecha_Crea] [datetime] NOT NULL,
	[USUA_Codigo_Crea] [numeric](18, 0) NOT NULL,
	[Fecha_Modifica] [datetime] NULL,
	[USUA_Codigo_Modifica] [numeric](18, 0) NULL
 CONSTRAINT [PK_Producto_Cargas] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]
GO