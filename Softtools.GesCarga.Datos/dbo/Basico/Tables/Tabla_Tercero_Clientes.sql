﻿ALTER TABLE Tercero_Clientes ADD 
TERC_Codigo_Representante_Comercial NUMERIC,
Diaz_Plazo NUMERIC NULL,
Descuento_Documento_Retorno NUMERIC NULL,
Min_Kg_Cobrar NUMERIC NULL,
Porcentaje_Seguro NUMERIC NULL,
Referencias_Verificadas VARCHAR(200),
Cliente_BASC SMALLINT NULL,
Sufijo_Cuenta_Ingreso NUMERIC NULL
GO
ALTER TABLE dbo.Tercero_Clientes
	DROP CONSTRAINT PK_Tercero_Clientes
GO
ALTER TABLE dbo.Tercero_Clientes ADD CONSTRAINT
	PK_Tercero_Clientes_1 PRIMARY KEY CLUSTERED 
	(
	EMPR_Codigo,
	TERC_Codigo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Tercero_Clientes
	DROP COLUMN Codigo
GO