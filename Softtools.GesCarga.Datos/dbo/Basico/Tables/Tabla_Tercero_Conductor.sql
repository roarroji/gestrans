﻿PRINT 'Tercero_Conductores'
GO
DROP TABLE Tercero_Conductores
GO
CREATE TABLE Tercero_Conductores(
	EMPR_Codigo smallint NOT NULL ,
	TERC_Codigo numeric(18, 0) NOT NULL,
	CATA_TISA_Codigo numeric NOT NULL,
	CATA_CALC_Codigo numeric NOT NULL,
	Numero_Licencia varchar(20) NOT NULL,
	Fecha_Vencimiento_Licencia datetime NOT NULL,
	Conductor_Propio smallint NOT NULL,
	Fecha_Ultimo_Viaje datetime NOT NULL,
	CATA_TCCO_Codigo NUMERIC NOT NULL,
	 CONSTRAINT [PK_Tercero_Conductores] PRIMARY KEY CLUSTERED 
(
	EMPR_Codigo ASC,
	TERC_Codigo ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] 

GO

ALTER TABLE Tercero_Conductores  WITH CHECK ADD  CONSTRAINT FK_Tercero_Conductores_Empresas FOREIGN KEY(EMPR_Codigo)
REFERENCES Empresas (Codigo)
GO

ALTER TABLE Tercero_Conductores CHECK CONSTRAINT FK_Tercero_Conductores_Empresas
GO

ALTER TABLE Tercero_Conductores  WITH CHECK ADD  CONSTRAINT FK_Tercero_Conductores_Terceros FOREIGN KEY(EMPR_Codigo, TERC_Codigo)
REFERENCES Terceros (EMPR_Codigo, Codigo)
GO

ALTER TABLE Tercero_Conductores CHECK CONSTRAINT FK_Tercero_Conductores_Terceros
GO

ALTER TABLE Tercero_Conductores  WITH CHECK ADD  CONSTRAINT FK_Tercero_Conductores_Valor_Cat_Categoria_Licencia FOREIGN KEY(EMPR_Codigo, CATA_CALC_Codigo)
REFERENCES Valor_Catalogos (EMPR_Codigo, Codigo)
GO

ALTER TABLE Tercero_Conductores CHECK CONSTRAINT FK_Tercero_Conductores_Valor_Cat_Categoria_Licencia
GO

ALTER TABLE Tercero_Conductores  WITH CHECK ADD  CONSTRAINT FK_Tercero_Conductores_Valor_Catalogos_Tipo_Sangre FOREIGN KEY(EMPR_Codigo, CATA_TISA_Codigo)
REFERENCES Valor_Catalogos (EMPR_Codigo, Codigo)
GO

ALTER TABLE Tercero_Conductores CHECK CONSTRAINT FK_Tercero_Conductores_Valor_Catalogos_Tipo_Sangre
GO