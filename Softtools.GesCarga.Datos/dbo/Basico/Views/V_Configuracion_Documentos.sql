﻿Print 'V_Configuracion_Documentos'
GO
DROP VIEW V_Configuracion_Documentos
GO
CREATE VIEW V_Configuracion_Documentos 
AS  
SELECT 
CDGD.EMPR_Codigo,
CDGD.Codigo,
CDGD.EDGD_Codigo,
CDGD.DOGD_Codigo,
EDGD.Nombre AS Nombre_Entidad,
DOGD.Nombre AS Nombre_Documento,
ISNULL(CDGD.Aplica_Referencia,0) AS Aplica_Referencia ,
ISNULL(CDGD.Aplica_Emisor,0) AS Aplica_Emisor,
ISNULL(CDGD.Aplica_Fecha_Emision,0) AS Aplica_Fecha_Emision,
ISNULL(CDGD.Aplica_Fecha_Vencimiento,0) AS Aplica_Fecha_Vencimiento,
ISNULL(CDGD.Aplica_Archivo,0) AS Aplica_Archivo,
ISNULL(CDGD.Aplica_Eliminar,0) AS Aplica_Eliminar,
ISNULL(CDGD.Aplica_Descargar,0) AS Aplica_Descargar,
ISNULL(CDGD.Habilitado,0) AS Habilitado,
ISNULL(CDGD.Tamano,0) AS Tamano,
ISNULL(CDGD.Nombre_Tamano,'') AS Nombre_Tamano,
CDGD.CATA_TIAD_Codigo,
TIAD.Nombre AS Tipo_Archivo,
TIAD.Formato_Aplicacion,
CDGD.Estado

FROM Configuracion_Documento_Gestion_Documentos AS CDGD
LEFT JOIN Entidad_Documento_Gestion_Documentos AS EDGD 
ON CDGD.EMPR_Codigo = EDGD.EMPR_Codigo
AND CDGD.EDGD_Codigo = EDGD.Codigo

LEFT JOIN Documento_Gestion_Documentos AS DOGD
ON CDGD.EMPR_Codigo = DOGD.EMPR_Codigo
AND CDGD.DOGD_Codigo = DOGD.Codigo

LEFT JOIN V_Tipo_Archivo_Documento_Gestion_Documental AS TIAD
ON CDGD.EMPR_Codigo = TIAD.EMPR_Codigo
AND CDGD.CATA_TIAD_Codigo = TIAD.Codigo

GO
