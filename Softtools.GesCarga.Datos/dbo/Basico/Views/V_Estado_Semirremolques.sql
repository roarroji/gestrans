﻿Print 'V_Estado_Semirremolques'
GO
DROP VIEW V_Estado_Semirremolques
GO
CREATE VIEW V_Estado_Semirremolques 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 33
GO