﻿Print 'V_Formas_Pago_Clientes'
GO
DROP VIEW V_Formas_Pago_Clientes
GO
CREATE VIEW V_Formas_Pago_Clientes 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 15
GO