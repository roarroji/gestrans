﻿Print 'V_Regimen_Tributario'
GO
DROP VIEW V_Regimen_Tributario
GO
CREATE VIEW V_Regimen_Tributario 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 16
GO