﻿Print 'V_Sufijo_ICA_Parametrizacion_Contable'
GO
DROP VIEW V_Sufijo_ICA_Parametrizacion_Contable
GO
CREATE VIEW V_Sufijo_ICA_Parametrizacion_Contable 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre, Campo2,Campo3
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 40
GO