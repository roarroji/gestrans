﻿Print 'V_Tercero_Parametrizacion_Contable'
GO
DROP VIEW V_Tercero_Parametrizacion_Contable
GO
CREATE VIEW V_Tercero_Parametrizacion_Contable 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 31
GO