﻿Print 'V_Tipo_Archivo_Documento_Gestion_Documental'
GO
DROP VIEW V_Tipo_Archivo_Documento_Gestion_Documental
GO
CREATE VIEW V_Tipo_Archivo_Documento_Gestion_Documental 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre, Campo2 as Formato_Aplicacion
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 43
GO