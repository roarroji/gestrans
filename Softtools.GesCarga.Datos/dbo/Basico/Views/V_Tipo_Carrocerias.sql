﻿Print 'V_Tipo_Carrocerias'
GO
DROP VIEW V_Tipo_Carrocerias
GO
CREATE VIEW V_Tipo_Carrocerias 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 23
GO