﻿Print 'V_Tipo_Contratos_Empleados'
GO
DROP VIEW V_Tipo_Contratos_Empleados
GO
CREATE VIEW V_Tipo_Contratos_Empleados 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 12
GO