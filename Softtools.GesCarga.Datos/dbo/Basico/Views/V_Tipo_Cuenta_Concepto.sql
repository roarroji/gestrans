﻿Print 'V_Tipo_Cuenta_Concepto'
GO
DROP VIEW V_Tipo_Cuenta_Concepto
GO
CREATE VIEW V_Tipo_Cuenta_Concepto 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 30
GO