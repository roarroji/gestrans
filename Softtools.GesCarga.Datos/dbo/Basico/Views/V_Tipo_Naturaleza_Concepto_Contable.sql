﻿Print 'V_Tipo_Naturaleza_Concepto_Contable'
GO
DROP VIEW V_Tipo_Naturaleza_Concepto_Contable
GO
CREATE VIEW V_Tipo_Naturaleza_Concepto_Contable 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 29
GO