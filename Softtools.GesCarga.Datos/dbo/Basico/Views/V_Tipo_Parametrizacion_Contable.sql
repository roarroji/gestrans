﻿Print 'V_Tipo_Parametrizacion_Contable'
GO
DROP VIEW V_Tipo_Parametrizacion_Contable
GO
CREATE VIEW V_Tipo_Parametrizacion_Contable 
AS  
SELECT 
 EMPR_Codigo, CATA_Codigo, Codigo, Campo1 AS Nombre, Campo2,Campo3
FROM Valor_Catalogos 
WHERE 
CATA_Codigo = 38
GO