﻿Print 'gsp_consultar_tipo_tarifa_transporte_carga'
GO
DROP PROCEDURE gsp_consultar_tipo_tarifa_transporte_carga
GO
CREATE PROCEDURE gsp_consultar_tipo_tarifa_transporte_carga (
@par_EMPR_Codigo smallint,
@par_TATC_Codigo smallint,
@par_Codigo INT = NULL
)
AS
BEGIN
	
	SELECT	EMPR_Codigo,
			TATC_Codigo,
			Codigo,
			CATA_Codigo,
			Nombre,
			NombreTarifa
	FROM V_Tipo_Tarifa_Transporte_Carga
	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND TATC_Codigo = @par_TATC_Codigo
	AND Codigo = ISNULL(@par_Codigo, Codigo)

END
GO