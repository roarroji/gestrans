﻿PRINT 'gsp_insertar_encabezado_tarifario_compras'
GO
DROP PROCEDURE gsp_insertar_encabezado_tarifario_compras
GO
CREATE PROCEDURE gsp_insertar_encabezado_tarifario_compras
(
@par_EMPR_Codigo smallint
,@par_Numero numeric(18,0) = null
,@par_TIDO_Codigo numeric(18,0)
,@par_Nombre varchar(100) = null
,@par_Tarifario_Base smallint = null
,@par_Fecha_Vigencia_Inicio date = null
,@par_Fecha_Vigencia_Fin date = null
,@par_Estado smallint = null
,@par_USUA_Codigo_Crea smallint = null
)
AS BEGIN

EXEC gsp_generar_consecutivo  @par_EMPR_Codigo, @par_TIDO_Codigo, 0, @par_Numero OUTPUT  

INSERT INTO Encabezado_Tarifario_Carga_compras
           (EMPR_Codigo
           ,Numero
           ,TIDO_Codigo
           ,Nombre
           ,Tarifario_Base
           ,Fecha_Vigencia_Inicio
           ,Fecha_Vigencia_Fin
           ,Estado
           ,USUA_Codigo_Crea
           ,Fecha_Crea)
     VALUES
           (@par_EMPR_Codigo
           ,@par_Numero
           ,@par_TIDO_Codigo
           ,ISNULL(@par_Nombre,'')
           ,ISNULL(@par_Tarifario_Base,0)
           ,ISNULL(@par_Fecha_Vigencia_Inicio,'')
           ,ISNULL(@par_Fecha_Vigencia_Fin,'')
           ,ISNULL(@par_Estado,0)
           ,ISNULL(@par_USUA_Codigo_Crea,0)
           ,GETDATE())

SELECT @par_Numero AS Codigo

END
GO