﻿Print 'gsp_obtener_encabezado_tarifario_compras'
GO
DROP PROCEDURE gsp_obtener_encabezado_tarifario_compras
GO
CREATE PROCEDURE gsp_obtener_encabezado_tarifario_compras
(  
@par_EMPR_Codigo smallint,  
@par_Numero numeric = null,  
@par_Nombre varchar(100) = null  
)  
AS   
BEGIN  
  
	SELECT   
	1 AS Obtener,  
	EMPR_Codigo,  
	Numero,  
	Estado,  
	ISNULL(Nombre,'') AS Nombre,  
	ISNULL(Fecha_Vigencia_Inicio,'') AS Fecha_Vigencia_Inicio,  
	ISNULL(Fecha_Vigencia_Fin,'') AS Fecha_Vigencia_Fin,  
	Tarifario_Base  
  
	FROM   
	Encabezado_Tarifario_Carga_compras   
  
	WHERE EMPR_Codigo = @par_EMPR_Codigo  
	and Numero = ISNULL(@par_Numero,Numero)  
	and Nombre = ISNULL(@par_Nombre,Nombre)  
  
  
END  
GO