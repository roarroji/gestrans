﻿DROP PROCEDURE gsp_obtener_tarifario_proveedores
GO
CREATE PROCEDURE gsp_obtener_tarifario_proveedores                 
(                    
@par_EMPR_Codigo SMALLINT,                    
@par_TERC_Codigo NUMERIC,                    
@par_LNTC_Codigo SMALLINT,                    
@par_TLNC_Codigo SMALLINT,                    
@par_TATC_Codigo SMALLINT = null,                    
@par_RUTA_Codigo NUMERIC                    
)                    
AS                    
BEGIN                    
             
 DECLARE @Tarifas numeric = 0            
                
 SELECT                     
 0 As Obtener,                     
 TEPR.EMPR_Codigo,                     
 TEPR.TERC_Codigo,                     
 TEPR.ETCC_Numero,                     
 TEPR.Dias_Plazo_Cobro,                     
 TEPR.CATA_FOCO_Codigo,                    
 ISNULL(TERC.Razon_Social,'') + ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1,'') + ' ' + ISNULL(TERC.Apellido2,'') As NombreCompleto,                    
 TERC.Celulares,                     
 TERC.Telefonos,                     
 TERC.Numero_Identificacion,                     
 TERC.Codigo_Alterno,                    
 CONVERT(VARCHAR,ETCC.Numero) + ' - ' + ETCC.Nombre As NombreTarifario,                    
 DTCC.TATC_Codigo,                    
 DTCC.TTTC_Codigo,                    
 TATC.Nombre As NombreTarifaCarga,                    
 ETCC.Estado As EstadoTarifario,                    
 TTTC.Nombre As NombreTipoTarifaCarga,                    
 DTCC.Valor_Flete ,      
 TERC.CATA_TIAN_Codigo      
                 
                   
  FROM                 
  Tercero_Proveedores TEPR                
                
  LEFT JOIN Terceros TERC ON                
  TEPR.EMPR_Codigo = TERC.EMPR_Codigo                    
  AND TEPR.TERC_Codigo = TERC.Codigo                
                
  LEFT JOIN Encabezado_Tarifario_Carga_Compras ETCC  ON                
  ETCC.EMPR_Codigo = TERC.EMPR_Codigo                   
  AND TEPR.ETCC_Numero = ETCC.Numero        
                
  LEFT JOIN Detalle_Tarifario_Carga_Compras DTCC ON                
  ETCC.EMPR_Codigo = DTCC.EMPR_Codigo                    
  AND ETCC.Numero = DTCC.ETCC_Numero                
                   
  LEFT JOIN Tarifa_Transporte_Carga TATC ON                
  DTCC.EMPR_Codigo = TATC.EMPR_Codigo                    
  AND DTCC.TATC_Codigo = TATC.Codigo                  
                
  LEFT JOIN  V_Tipo_Tarifa_Transporte_Carga TTTC ON                
  DTCC.EMPR_Codigo = TTTC.EMPR_Codigo                    
  AND DTCC.TTTC_Codigo = TTTC.Codigo                     
  AND ISNULL(DTCC.TATC_Codigo,0) = ISNULL(TTTC.TATC_Codigo,0)                     
                      
  WHERE                     
  ETCC.EMPR_Codigo = @par_EMPR_Codigo                    
  AND TERC.Codigo = @par_TERC_Codigo                    
  AND DTCC.LNTC_Codigo = @par_LNTC_Codigo                    
  AND DTCC.TLNC_Codigo = @par_TLNC_Codigo                    
  AND( DTCC.TATC_Codigo = @par_TATC_Codigo or @par_TATC_Codigo is null)    
  AND DTCC.RUTA_Codigo = @par_RUTA_Codigo                    
              
  SELECT @Tarifas = @@ROWCOUNT            
 -- Si no encuentra ningún tarifario al tercero, se consulta el tarifario base                    
 IF @Tarifas = 0 BEGIN                    
                    
  SELECT                     
  0 As Obtener,                     
  ETCC.EMPR_Codigo,                     
  @par_TERC_Codigo As TERC_Codigo,                     
  ETCC.Numero As ETCC_Numero,                     
  0 As Dias_Plazo_Cobro,                     
  8802 As CATA_FOCO_Codigo,                    
  ISNULL(TERC.Razon_Social,'') + ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1,'') + ' ' + ISNULL(TERC.Apellido2,'') As NombreCompleto,                    
  TERC.Celulares,                     
  TERC.Telefonos,                     
  TERC.Numero_Identificacion,                     
  TERC.Codigo_Alterno,                    
  CONVERT(VARCHAR,ETCC.Numero) + ' - ' + ETCC.Nombre As NombreTarifario,                    
  DTCC.TATC_Codigo,      
  DTCC.TTTC_Codigo,                    
  TATC.Nombre As NombreTarifaCarga,                    
  ETCC.Estado As EstadoTarifario,                    
  TTTC.Nombre As NombreTipoTarifaCarga,                    
  DTCC.Valor_Flete                    
                    
  FROM Terceros TERC                 
                
  LEFT JOIN Encabezado_Tarifario_Carga_Compras ETCC  ON                
  ETCC.EMPR_Codigo = TERC.EMPR_Codigo                   
                
  LEFT JOIN Detalle_Tarifario_Carga_Compras DTCC ON                
  ETCC.EMPR_Codigo = DTCC.EMPR_Codigo                    
  AND ETCC.EMPR_Codigo = DTCC.ETCC_Numero                
                   
  LEFT JOIN Tarifa_Transporte_Carga TATC ON                
  DTCC.EMPR_Codigo = TATC.EMPR_Codigo                    
  AND DTCC.TATC_Codigo = TATC.Codigo                  
                
  LEFT JOIN  V_Tipo_Tarifa_Transporte_Carga TTTC ON                
 DTCC.EMPR_Codigo = TTTC.EMPR_Codigo                    
  AND DTCC.TTTC_Codigo = TTTC.Codigo                     
  AND DTCC.TATC_Codigo = TTTC.TATC_Codigo                     
                      
  WHERE                     
  ETCC.EMPR_Codigo = @par_EMPR_Codigo                    
  AND TERC.Codigo = @par_TERC_Codigo                    
  AND DTCC.LNTC_Codigo = @par_LNTC_Codigo                    
  AND DTCC.TLNC_Codigo = @par_TLNC_Codigo                    
  AND( DTCC.TATC_Codigo = @par_TATC_Codigo or @par_TATC_Codigo is null)    
  AND DTCC.RUTA_Codigo = @par_RUTA_Codigo                    
  AND ETCC.Tarifario_Base = 1                
                    
 END                      
END
GO