﻿PRINT 'gsp_consultar_detalle_tarifario_paqueteria_ventas'
GO
DROP PROCEDURE gsp_consultar_detalle_tarifario_paqueteria_ventas
GO
CREATE PROCEDURE gsp_consultar_detalle_tarifario_paqueteria_ventas  
(  
@par_EMPR_Codigo smallint,  
@par_ETCV_Numero numeric   
)  
as   
begin  
  
select   
EMPR_Codigo,  
ETCV_Numero,  
Minimo_Kilo_Cobro,
Maximo_Kilo_Cobro,
Valor_Kilo_Adicional,
Valor_Manejo,
Porcentaje_Manejo,
Minimo_Valor_Manejo,
Valor_Seguro,
Porcentaje_Seguro,
Minimo_Valor_Seguro,
Minimo_Valor_Unidad
from   
Tarifas_Paqueteria_Ventas   
  
where EMPR_Codigo = @par_EMPR_Codigo  
and ETCV_Numero = ISNULL(@par_ETCV_Numero,ETCV_Numero)  
  
  
end  
GO