﻿

PRINT 'gsp_insertar_detalle_tarifario_ventas'
GO
DROP PROCEDURE gsp_insertar_detalle_tarifario_ventas
GO
CREATE PROCEDURE gsp_insertar_detalle_tarifario_ventas    
(    
			@par_EMPR_Codigo smallint    
		   ,@par_Codigo numeric(18,0)   
           ,@par_ETCV_Numero numeric(18,0)    
           ,@par_LNTC_Codigo smallint    
           ,@par_TLNC_Codigo smallint    
           ,@par_RUTA_Codigo numeric(18,0) = NULL
		   ,@par_CIUD_Codigo_Origen numeric(18,0) = NULL
		   ,@par_CIUD_Codigo_Destino numeric(18,0) = NULL   
           ,@par_TATC_Codigo smallint    
           ,@par_TTTC_Codigo smallint    
           ,@par_Valor_Flete money    
           ,@par_Valor_Escolta money = null    
           --,@par_Valor_Kilo_Adicional money = null    
           ,@par_Valor_Otros1 money = null    
           ,@par_Valor_Otros2 money = null    
           ,@par_Valor_Otros3 money = null    
           ,@par_Valor_Otros4 money = null    
           ,@par_Fecha_Vigencia_Inicio date = null    
           ,@par_Fecha_Vigencia_Fin date = null    
           ,@par_Estado smallint    
           ,@par_USUA_Codigo_Crea smallint    
)    
AS BEGIN    
    
	IF @par_Codigo > 0
	BEGIN
	UPDATE Detalle_Tarifario_Carga_Ventas
	SET   
	Valor_Flete = ISNULL(@par_Valor_Flete,0),
	Valor_Escolta = ISNULL(@par_Valor_Escolta ,0),
	Valor_Otros1 =  ISNULL(@par_Valor_Otros1,0),
	Valor_Otros2 = ISNULL(@par_Valor_Otros2, 0),   
	Valor_Otros3 = ISNULL(@par_Valor_Otros3,0),    
	Valor_Otros4 = ISNULL(@par_Valor_Otros4,0),     
	Fecha_Vigencia_Inicio = ISNULL(@par_Fecha_Vigencia_Inicio,''),     
	Fecha_Vigencia_Fin =  ISNULL(@par_Fecha_Vigencia_Fin,''),   
	Estado =  @par_Estado,  
	USUA_Codigo_Modifica = @par_USUA_Codigo_Crea,
	Fecha_Modifica = GETDATE()
	
	WHERE
	EMPR_Codigo =  @par_EMPR_Codigo
	AND Codigo = @par_Codigo

	SELECT @par_Codigo AS Codigo   
	 
	END
	ELSE 
	BEGIN
		DECLARE @Codigo as numeric =(select ISNULL(max(Codigo),0)+1 from Detalle_Tarifario_Carga_Ventas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ETCV_Numero = @par_ETCV_Numero)    
    
		INSERT INTO Detalle_Tarifario_Carga_Ventas    
				   (    
					EMPR_Codigo    
				   ,ETCV_Numero    
				   ,Codigo    
				   ,LNTC_Codigo    
				   ,TLNC_Codigo    
				   ,RUTA_Codigo   
				   ,CIUD_Codigo_Origen
				   ,CIUD_Codigo_Destino
				   ,TATC_Codigo    
				   ,TTTC_Codigo    
				   ,Valor_Flete    
				   ,Valor_Escolta    
				   --,Valor_Kilo_Adicional    
				   ,Valor_Otros1    
				   ,Valor_Otros2    
				   ,Valor_Otros3    
				   ,Valor_Otros4    
				   ,Fecha_Vigencia_Inicio    
				   ,Fecha_Vigencia_Fin    
				   ,Estado    
				   ,USUA_Codigo_Crea    
				   ,Fecha_Crea    
			 )    
			 VALUES    
				   (@par_EMPR_Codigo    
				   ,@par_ETCV_Numero    
				   ,@Codigo    
				   ,@par_LNTC_Codigo    
				   ,@par_TLNC_Codigo    
				   ,@par_RUTA_Codigo 
				   ,@par_CIUD_Codigo_Origen
				   ,@par_CIUD_Codigo_Destino   
				   ,@par_TATC_Codigo    
				   ,@par_TTTC_Codigo    
				   ,@par_Valor_Flete    
				   ,isnull(@par_Valor_Escolta,0)    
				   --,isnull(@par_Valor_Kilo_Adicional,0)    
				   ,isnull(@par_Valor_Otros1,0)    
				   ,isnull(@par_Valor_Otros2,0)    
				   ,isnull(@par_Valor_Otros3,0)    
				   ,isnull(@par_Valor_Otros4,0)    
				   ,isnull(@par_Fecha_Vigencia_Inicio,'')    
				   ,isnull(@par_Fecha_Vigencia_Fin,'')    
				   ,@par_Estado    
				   ,@par_USUA_Codigo_Crea    
				   ,GETDATE()    
				   )    
		SELECT @Codigo AS Codigo    
    END
END    
GO