﻿PRINT 'gsp_insertar_encabezado_tarifario_ventas'
GO
DROP PROCEDURE gsp_insertar_encabezado_tarifario_ventas
GO
CREATE PROCEDURE gsp_insertar_encabezado_tarifario_ventas
(
@par_EMPR_Codigo smallint
,@par_Numero numeric(18,0) = null
,@par_TIDO_Codigo numeric(18,0)
,@par_Nombre varchar(100) = null
,@par_Tarifario_Base smallint = null
,@par_Fecha_Vigencia_Inicio date = null
,@par_Fecha_Vigencia_Fin date = null
,@par_Estado smallint = null
,@par_USUA_Codigo_Crea smallint = null
,@par_Aplica_Paqueteria	numeric= null
,@par_Minimo_Kilo_Cobro	numeric(18, 2)= null
,@par_Maximo_Kilo_Cobro	numeric(18, 2)= null
,@par_Valor_Kilo_Adicional	money= null
,@par_Valor_Manejo	money= null
,@par_Porcentaje_Manejo	numeric(18, 2)= null
,@par_Minimo_Valor_Manejo	money= null
,@par_Valor_Seguro	money= null
,@par_Porcentaje_Seguro	numeric(18, 2)= null
,@par_Minimo_Valor_Seguro	money= null
,@par_Minimo_Valor_Unidad	money= null
)
AS BEGIN

EXEC gsp_generar_consecutivo  @par_EMPR_Codigo, @par_TIDO_Codigo, 0, @par_Numero OUTPUT  

INSERT INTO Encabezado_Tarifario_Carga_Ventas
           (EMPR_Codigo
           ,Numero
           ,TIDO_Codigo
           ,Nombre
           ,Tarifario_Base
           ,Fecha_Vigencia_Inicio
           ,Fecha_Vigencia_Fin
           ,Estado
           ,USUA_Codigo_Crea
           ,Fecha_Crea)
     VALUES
           (@par_EMPR_Codigo
           ,@par_Numero
           ,@par_TIDO_Codigo
           ,ISNULL(@par_Nombre,'')
           ,ISNULL(@par_Tarifario_Base,0)
           ,ISNULL(@par_Fecha_Vigencia_Inicio,'')
           ,ISNULL(@par_Fecha_Vigencia_Fin,'')
           ,ISNULL(@par_Estado,0)
           ,ISNULL(@par_USUA_Codigo_Crea,0)
           ,GETDATE())

if		@par_Aplica_Paqueteria = 1 
BEGIN
INSERT INTO Tarifas_Paqueteria_Ventas
           (EMPR_Codigo
           ,ETCV_Numero
           ,Minimo_Kilo_Cobro
           ,Maximo_Kilo_Cobro
           ,Valor_Kilo_Adicional
           ,Valor_Manejo
           ,Porcentaje_Manejo
           ,Minimo_Valor_Manejo
           ,Valor_Seguro
           ,Porcentaje_Seguro
           ,Minimo_Valor_Seguro
           ,Minimo_Valor_Unidad
           ,USUA_Codigo_Crea
           ,Fecha_Crea
           )
     VALUES
           (@par_EMPR_Codigo
           ,@par_Numero
			,isnull(@par_Minimo_Kilo_Cobro,0)
			,isnull(@par_Maximo_Kilo_Cobro,0)
			,isnull(@par_Valor_Kilo_Adicional,0)
			,isnull(@par_Valor_Manejo,0)
			,isnull(@par_Porcentaje_Manejo,0)
			,isnull(@par_Minimo_Valor_Manejo,0)
			,isnull(@par_Valor_Seguro,0)
			,isnull(@par_Porcentaje_Seguro,0)
			,isnull(@par_Minimo_Valor_Seguro,0)
			,isnull(@par_Minimo_Valor_Unidad,0)
           ,isnull(@par_USUA_Codigo_Crea,0)
           ,GETDATE())
END



SELECT @par_Numero AS Codigo

END
GO