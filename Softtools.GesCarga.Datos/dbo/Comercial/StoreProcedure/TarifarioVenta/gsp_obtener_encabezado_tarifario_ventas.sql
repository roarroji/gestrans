﻿print 'gsp_obtener_encabezado_tarifario_ventas'
go
drop procedure gsp_obtener_encabezado_tarifario_ventas
go
create procedure gsp_obtener_encabezado_tarifario_ventas
(
@par_EMPR_Codigo smallint,
@par_Numero numeric = null,
@par_Nombre varchar(20) = null
)
as 
begin

select 
1 AS Obtener,
EMPR_Codigo,
Numero,
Estado,
ISNULL(Nombre,'') AS Nombre,
ISNULL(Fecha_Vigencia_Inicio,'') AS Fecha_Vigencia_Inicio,
ISNULL(Fecha_Vigencia_Fin,'') AS Fecha_Vigencia_Fin,
Tarifario_Base

from 
Encabezado_Tarifario_Carga_Ventas 

where EMPR_Codigo = @par_EMPR_Codigo
and Numero = ISNULL(@par_Numero,Numero)
and Nombre = ISNULL (@par_Nombre,Nombre)


end
GO