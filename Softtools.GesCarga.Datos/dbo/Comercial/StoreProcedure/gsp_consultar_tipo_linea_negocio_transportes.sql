﻿print 'gsp_consultar_tipo_linea_negocio_transportes'
go
drop procedure gsp_consultar_tipo_linea_negocio_transportes
go
create procedure gsp_consultar_tipo_linea_negocio_transportes
(
@par_EMPR_Codigo smallint,
@par_Estado smallint
)
as begin
select 
EMPR_Codigo,
Nombre,
Codigo,
Estado,
LNTC_Codigo
from Tipo_Linea_Negocio_Carga
where EMPR_Codigo = @par_EMPR_Codigo
and Estado = @par_Estado
end
go