﻿PRINT 'gps_Obtenter_Detalle_Facturas_SIESA'
GO
DROP PROCEDURE gps_Obtenter_Detalle_Facturas_SIESA
GO
CREATE PROCEDURE gps_Obtenter_Detalle_Facturas_SIESA
(
	@par_EMPR_Codigo smallint,
	@par_ENFA_Numero Numeric
)
AS
BEGIN
	SELECT 
	ENFA.EMPR_Codigo,
	ENFA.Numero,
	ENRE.Numero ENRE_Numero,
	ENRE.Numero_Documento,
	ENRE.VEHI_Codigo,
	VEHI.Placa,
	ENRE.Total_Flete_Cliente TotalFlete,
	ENRE.Cantidad_Cliente Cantidad,
	ENRE.Peso_Cliente Peso,
	ROW_NUMBER() OVER (ORDER BY ENRE.Numero DESC) AS NumeroRegistro

	FROM Encabezado_Facturas ENFA

	INNER JOIN Detalle_Remesas_Facturas DERF ON
	ENFA.EMPR_Codigo = DERF.EMPR_Codigo
	AND ENFA.Numero = DERF.ENFA_Numero

	LEFT JOIN Encabezado_Remesas ENRE ON
	DERF.EMPR_Codigo = ENRE.EMPR_Codigo
	AND DERF.ENRE_Numero = ENRE.Numero

	LEFT JOIN Vehiculos VEHI ON
	ENRE.EMPR_Codigo = VEHI.EMPR_Codigo
	AND ENRE.VEHI_Codigo = VEHI.Codigo

	WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo 
	AND ENFA.Numero = @par_ENFA_Numero
END
GO