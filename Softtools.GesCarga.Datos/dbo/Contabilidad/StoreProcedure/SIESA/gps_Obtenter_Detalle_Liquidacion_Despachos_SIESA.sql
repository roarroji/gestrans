﻿PRINT 'gps_Obtenter_Detalle_Liquidacion_Despachos_SIESA'
GO
DROP PROCEDURE gps_Obtenter_Detalle_Liquidacion_Despachos_SIESA
GO
CREATE PROCEDURE gps_Obtenter_Detalle_Liquidacion_Despachos_SIESA
(
	@par_EMPR_Codigo smallint,
	@par_ENCC_Numero Numeric
)
AS
BEGIN
	SELECT 
	DECC.EMPR_Codigo,
	DECC.ENCC_Numero,
	DECC.PLUC_Codigo, 
	PLUC.Codigo_Cuenta, 
	DECC.Valor_Base, 
	DECC.Valor_Credito,
	DECC.Valor_Debito, 
	DECC.Documento_Cruce, 
	DECC.Observaciones

	FROM Detalle_Comprobante_Contables AS DECC

	LEFT JOIN Plan_Unico_Cuentas AS PLUC ON
	DECC.EMPR_Codigo = PLUC.EMPR_Codigo
	AND DECC.PLUC_Codigo = PLUC.Codigo

	WHERE DECC.EMPR_Codigo = @par_EMPR_Codigo
	AND DECC.ENCC_Numero = @par_ENCC_Numero
	ORDER BY DECC.ID
END
GO