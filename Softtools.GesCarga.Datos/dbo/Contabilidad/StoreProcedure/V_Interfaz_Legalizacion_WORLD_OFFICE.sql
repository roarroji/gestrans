﻿CREATE VIEW V_Interfaz_Legalizacion_WORLD_OFFICE            
AS      
SELECT ELGC.EMPR_Codigo, EMPR.Nombre_Razon_Social, ELGC.Numero, ELGC.Fecha, ELGC.Numero_Documento, PLUC.Codigo_Cuenta, VEHI.Placa, 
/*SOLUCION TEMPORAL SIN PARAMETRIZAR MOVIMIENTOS CONTABLES*/ 
DLGC.Valor AS Valor_Debito, 
0 AS Valor_Credito, 
/* FIN SOLUCION TEMPORAL */ 
CLGC.Nombre AS Concepto, DLGC.Identificacion_Proveedor, BENE.Numero_Identificacion AS Identificacion_Beneficiario,  
BENE.Nombre + ' ' + BENE.Apellido1 + ' ' + BENE.Apellido2 AS Nombre_Beneficiario, RUTA.Nombre AS Ruta_Nombre,  
IIF(ELGC.Anulado <> 0,0,IIF(ELGC.Estado = 1, 1, 2)) AS Estado, 
(BENE.Nombre + ' ' + BENE.Apellido1 + ' ' + BENE.Apellido2) +'-'+ CONVERT(varchar(10),ELGC.Numero_Documento) +'-'+ RUTA.Nombre +'-'+ VEHI.Placa AS Concepto_Encabezado  ,
TEEX.Numero_Identificacion AS Tercero_Externo
FROM Encabezado_Legalizacion_Gastos_Conductor ELGC  

INNER JOIN Empresas EMPR 
ON ELGC.EMPR_Codigo = EMPR.Codigo 

INNER JOIN Detalle_Legalizacion_Gastos_Conductor DLGC ON 
ELGC.EMPR_Codigo = DLGC.EMPR_Codigo AND ELGC.Numero = DLGC.ELGC_Numero  

INNER JOIN Conceptos_Legalizacion_Gastos_Conductor CLGC ON 
DLGC.EMPR_Codigo = CLGC.EMPR_Codigo 
AND DLGC.CLGC_Codigo = CLGC.Codigo 

INNER JOIN Plan_Unico_Cuentas PLUC ON 
CLGC.EMPR_Codigo = PLUC.EMPR_Codigo 
AND CLGC.PLUC_Codigo = PLUC.Codigo  

INNER JOIN Encabezado_Planilla_Despachos ENPD ON 
DLGC.EMPR_Codigo = ENPD.EMPR_Codigo  
AND DLGC.ENPD_Numero = ENPD.Numero  

LEFT JOIN Vehiculos VEHI ON 
VEHI.EMPR_Codigo = ENPD.EMPR_Codigo  
AND VEHI.Codigo = ENPD.VEHI_Codigo  

LEFT JOIN Terceros BENE ON 
BENE.EMPR_Codigo = ENPD.EMPR_Codigo 
AND BENE.Codigo = ENPD.TERC_Codigo_Conductor  

LEFT JOIN Rutas RUTA ON  RUTA.EMPR_Codigo = ENPD.EMPR_Codigo  
AND RUTA.Codigo = ENPD.RUTA_Codigo  

/*Modificación AE 18/06/2020*/
LEFT JOIN Terceros TEEX ON
ELGC.EMPR_Codigo = TEEX.EMPR_Codigo AND
ELGC.USUA_Codigo_Crea = TEEX.Codigo
/*Fin Modificación*/

--WHERE ENPD.Numero = 62798 

UNION   

SELECT ELGC.EMPR_Codigo, EMPR.Nombre_Razon_Social,   ELGC.Numero, ELGC.Fecha, ELGC.Numero_Documento, PLUC.Codigo_Cuenta,
VEHI.Placa, 
/*SOLUCION TEMPORAL SIN PARAMETRIZAR MOVIMIENTOS CONTABLES*/ 
0 AS Valor_Debito, 
DLGC.Valor AS Valor_Credito, 
/* FIN SOLUCION TEMPORAL */
CLGC.Nombre AS Concepto, DLGC.Identificacion_Proveedor, BENE.Numero_Identificacion AS Identificacion_Beneficiario,  
BENE.Nombre + ' ' + BENE.Apellido1 + ' ' + BENE.Apellido2 AS Nombre_Beneficiario, RUTA.Nombre AS Ruta_Nombre,  
IIF(ELGC.Anulado <> 0,0,IIF(ELGC.Estado = 1, 1, 2)) AS Estado, (BENE.Nombre + ' ' + BENE.Apellido1 + ' ' + BENE.Apellido2) +'-'+ CONVERT(varchar(10),ELGC.Numero_Documento) +'-'+ RUTA.Nombre +'-'+ VEHI.Placa AS Concepto_Encabezado  ,
TEEX.Numero_Identificacion AS Tercero_Externo
FROM Encabezado_Legalizacion_Gastos_Conductor ELGC  

INNER JOIN Empresas EMPR ON 
ELGC.EMPR_Codigo = EMPR.Codigo  

INNER JOIN Detalle_Legalizacion_Gastos_Conductor DLGC ON
ELGC.EMPR_Codigo = DLGC.EMPR_Codigo 
AND ELGC.Numero = DLGC.ELGC_Numero   

INNER JOIN Conceptos_Legalizacion_Gastos_Conductor CLGC ON 
DLGC.EMPR_Codigo = CLGC.EMPR_Codigo 
AND DLGC.CLGC_Codigo = CLGC.Codigo  

INNER JOIN Plan_Unico_Cuentas PLUC ON 
CLGC.EMPR_Codigo = PLUC.EMPR_Codigo 
AND CLGC.PLUC_Codigo = PLUC.Codigo  

INNER JOIN Encabezado_Planilla_Despachos ENPD ON 
DLGC.EMPR_Codigo = ENPD.EMPR_Codigo  
AND DLGC.ENPD_Numero = ENPD.Numero  

LEFT JOIN Vehiculos VEHI ON  
VEHI.EMPR_Codigo = ENPD.EMPR_Codigo  
AND VEHI.Codigo = ENPD.VEHI_Codigo  

LEFT JOIN Terceros BENE ON 
BENE.EMPR_Codigo = ENPD.EMPR_Codigo 
AND BENE.Codigo = ENPD.TERC_Codigo_Conductor  

LEFT JOIN Rutas RUTA ON  
RUTA.EMPR_Codigo = ENPD.EMPR_Codigo  
AND RUTA.Codigo = ENPD.RUTA_Codigo  

/*Modificación AE 18/06/2020*/
LEFT JOIN Terceros TEEX ON
ELGC.EMPR_Codigo = TEEX.EMPR_Codigo AND
ELGC.USUA_Codigo_Crea = TEEX.Codigo
/*Fin Modificación*/

UNION  

SELECT DISTINCT ENDC.EMPR_Codigo, EMPR.Nombre_Razon_Social,   
AUX_ENC.Numero, AUX_ENC.Fecha, AUX_ENC.Numero_Documento, 
'133010' AS Codigo_Cuenta, VEHI.Placa, 
/*SOLUCION TEMPORAL SIN PARAMETRIZAR MOVIMIENTOS CONTABLES*/ 
0 AS Valor_Debito, 
ENDC.Valor_Pago_Recaudo_Total AS Valor_Credito, 
/* FIN SOLUCION TEMPORAL */ 
CASE WHEN ENDC.CATA_DOOR_Codigo = 2604 
THEN '103 Gast Cond - Valor Anticipo No: ' + CONVERT(varchar(10),AUX_ENC.Numero_Documento) + ' Manifiesto:' + ISNULL(CONVERT(VARCHAR(10),ENMC.Numero_Documento),'') 
WHEN ENDC.CATA_DOOR_Codigo = 2605 
THEN '103 Gast Cond - Valor Reanticipo No: ' + CONVERT(varchar(10),AUX_ENC.Numero_Documento) + ' Manifiesto:' + ISNULL(CONVERT(VARCHAR(10),ENMC.Numero_Documento),'') 
END AS Concepto, 
BENE.Numero_Identificacion AS Identificacion_Proveedor, 
BENE.Numero_Identificacion AS Identificacion_Beneficiario,  
BENE.Nombre + ' ' + BENE.Apellido1 + ' ' + BENE.Apellido2 AS Nombre_Beneficiario, 
RUTA.Nombre AS Ruta_Nombre,  
IIF(ENDC.Anulado <> 0,0,IIF(ENDC.Estado = 1, 1, 2)) AS Estado, 
(BENE.Nombre + ' ' + BENE.Apellido1 + ' ' + BENE.Apellido2) +','+ CONVERT(varchar(10),AUX_ENC.Numero_Documento) +'-'+ RUTA.Nombre +'-'+ VEHI.Placa AS Concepto_Encabezado ,
TEEX.Numero_Identificacion AS Tercero_Externo

FROM Encabezado_Documento_Comprobantes ENDC   

INNER JOIN Empresas EMPR ON 
ENDC.EMPR_Codigo = EMPR.Codigo  

INNER JOIN Encabezado_Planilla_Despachos ENPD ON 
ENPD.EMPR_Codigo = ENDC.EMPR_Codigo  
AND ENPD.Numero_Documento = ENDC.Documento_Origen  

INNER JOIN 
(SELECT DISTINCT EMPR_Codigo, ENPD_Numero, ELGC_Numero FROM Detalle_Legalizacion_Gastos_Conductor) AUX_DETA ON 
AUX_DETA.EMPR_Codigo = ENPD.EMPR_Codigo  
AND AUX_DETA.ENPD_Numero = ENPD.Numero  

INNER JOIN (SELECT DISTINCT EMPR_Codigo, Fecha, Numero, Numero_Documento FROM Encabezado_Legalizacion_Gastos_Conductor) AUX_ENC ON 
AUX_DETA.EMPR_Codigo = AUX_ENC.EMPR_Codigo 
AND AUX_DETA.ELGC_Numero = AUX_ENC.Numero  

LEFT JOIN Vehiculos VEHI ON  
VEHI.EMPR_Codigo = ENPD.EMPR_Codigo  
AND VEHI.Codigo = ENPD.VEHI_Codigo  

LEFT JOIN Terceros BENE ON 
BENE.EMPR_Codigo = ENPD.EMPR_Codigo 
AND BENE.Codigo = ENPD.TERC_Codigo_Conductor  

LEFT JOIN Rutas RUTA ON  
RUTA.EMPR_Codigo = ENPD.EMPR_Codigo  
AND RUTA.Codigo = ENPD.RUTA_Codigo  

LEFT JOIN Encabezado_Manifiesto_Carga ENMC ON 
ENMC.EMPR_Codigo = ENPD.EMPR_Codigo 

AND ENMC.Numero = ENPD.ENMC_Numero 

/*Modificación AE 18/06/2020*/
LEFT JOIN Encabezado_Legalizacion_Gastos_Conductor ELGC ON
ENDC.EMPR_Codigo = ELGC .EMPR_Codigo AND
ENDC.ELGC_Numero = ELGC.Numero

LEFT JOIN Terceros TEEX ON
ELGC.EMPR_Codigo = TEEX.EMPR_Codigo AND
ELGC.USUA_Codigo_Crea = TEEX.Codigo
/*Fin Modificación*/

WHERE ENDC.CATA_DOOR_Codigo IN (2604,2605) 
--AND ENPD.Numero = 62798 
GO