﻿PRINT 'gsp_generar_movimiento_contable'
GO
DROP PROCEDURE gsp_generar_movimiento_contable
GO
CREATE PROCEDURE gsp_generar_movimiento_contable  
(             
	@par_EMPR_Codigo SMALLINT,            
	@par_Numero NUMERIC,            
	@par_TIDO_Codigo NUMERIC            
)            
AS            
BEGIN            
           
	DELETE Detalle_Comprobante_Contables WHERE ENCC_Numero IN (SELECT Numero from Encabezado_Comprobante_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo_Documento = @par_Numero AND TIDO_Documento = @par_TIDO_Codigo)          
	DELETE Encabezado_Comprobante_Contables WHERE Numero IN (SELECT Numero from Encabezado_Comprobante_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo_Documento = @par_Numero AND TIDO_Documento = @par_TIDO_Codigo)          
          
	--Constantes            
	DECLARE             
	@OFIC_Codigo NUMERIC = 0,            
	@Cliente_Codigo NUMERIC = 0,            
	@ENPC_Codigo NUMERIC = 0,            
	@Tipo_Documento_Comprobante_Contable NUMERIC = 45,            
	@Valor_Documento NUMERIC = 3802,            
	@Valor_Concepto NUMERIC = 3803,            
	@Valor_Impuesto NUMERIC = 3801,            
	@Estado_Comprobante_Contable_Pendiente NUMERIC =  12001,            
	@ENCC_Numero NUMERIC = 0,             
	@Numero_Documento NUMERIC = 0,            
	@Naturaleza_Movimiento_Credito NUMERIC = 2902,            
	@Naturaleza_Movimiento_Debito NUMERIC = 2901,            
            
	--Liquidación            
	@Tipo_Documento_Liquidacion NUMERIC = 160,  
	@Tipo_Documento_Genera_Liquidacon NUMERIC = 3606,            
	@Documento_Origen_Liquidacion NUMERIC = 2602,            
	@Documento_Valor_Flete NUMERIC = 4202,            
	@Documento_Valor_Pagar_Transportador NUMERIC = 4213,            
            
	--Facturación            
	@Tipo_Documento_Factura NUMERIC = 170,  
	@Tipo_Documento_Genera_Factura NUMERIC = 3601,  
	@Documento_Origen_Factura NUMERIC = 2601,  
	@Documento_Valor_Factura NUMERIC = 4209,  
	@Documento_Subtotal NUMERIC = 4208,  
  
	--Notas Credito            
	@Tipo_Documento_Nota_Credito NUMERIC = 190,  
	@Tipo_Documento_Genera_Nota_Credito NUMERIC = 3608,  
	@Documento_Origen_Nota_Credito NUMERIC = 2610,  
	@Documento_Valor_Nota NUMERIC = 4218,  
  
	--Notas Debito  
	@Tipo_Documento_Nota_Debito NUMERIC = 195,  
	@Tipo_Documento_Genera_Nota_Debito NUMERIC = 3609,  
	@Documento_Origen_Nota_Debito NUMERIC = 2611,  
          
	--Comprobantes egreso Ingreso        
	@Tipo_Documento_Comprobante_Egreso NUMERIC = 30,            
	@Tipo_Documento_Comprobante_Ingreso NUMERIC = 40,           
	@Documento_Origen_Comprobante_Egreso NUMERIC = 2606,            
	@Documento_Origen_Comprobante_Ingreso NUMERIC = 2607        
     
	CREATE TABLE #TBL_Detalles_Comprobantes_Parametrizacion_Contable            
	(            
		Codigo NUMERIC(18,0) ,            
		Valor MONEY DEFAULT 0            
	)            
	----------------LIQUIDACION---------------  
	IF @par_TIDO_Codigo = @Tipo_Documento_Liquidacion  
		BEGIN  
		--Constantes            
		--Se consultan las parametrizaciones contables para la liquidación  
		SELECT @ENPC_Codigo = Codigo from Encabezado_Parametrizacion_Contables where EMPR_Codigo = @par_EMPR_Codigo AND  CATA_TIDG_Codigo = @Tipo_Documento_Genera_Liquidacon  
		IF (@ENPC_Codigo) > 0            
		BEGIN  
			SET @Numero_Documento =( SELECT Numero_Documento FROM Encabezado_Liquidacion_Planilla_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_Numero)            
            
			INSERT INTO Encabezado_Comprobante_Contables  
			(            
			EMPR_Codigo,  
			TIDO_Codigo,  
			Fecha,  
			TIDO_Documento,  
			Codigo_Documento,  
			Numero_Documento,  
			Fecha_Documento,  
			TERC_Documento,  
			OFIC_Documento,  
			CATA_DOOR_Codigo,  
			Año,  
			Periodo,  
			Observaciones,  
			Valor_Comprobante,  
			OFIC_Codigo,  
			CATA_ESIC_Codigo,  
			Anulado  
			)            
			SELECT            
			@par_EMPR_Codigo,  
			@Tipo_Documento_Comprobante_Contable,  
			GETDATE(),  
			@Tipo_Documento_Liquidacion,  
			ELPD.Numero,  
			ELPD.Numero_Documento,  
			ELPD.Fecha,  
			ENPD.TERC_Codigo_Tenedor,  
			ELPD.OFIC_Codigo,  
			@Documento_Origen_Liquidacion,  
			YEAR(GETDATE()),  
			MONTH(GETDATE()),  
			'MOVIMIENTO CONTRABLE GENERADO PARA LA LIQUIDACIÓN N° '+CONVERT(VARCHAR(10), ELPD.Numero_Documento),  
			0,  
			ELPD.OFIC_Codigo,  
			@Estado_Comprobante_Contable_Pendiente,  
			0  
			FROM Encabezado_Liquidacion_Planilla_Despachos AS ELPD    
              
			INNER JOIN Encabezado_Planilla_Despachos AS ENPD             
			ON ELPD.EMPR_Codigo = ENPD.EMPR_Codigo            
			AND ELPD.ENPD_Numero = ENPD.Numero  
  
			WHERE ELPD.EMPR_Codigo = @par_EMPR_Codigo AND ELPD.Numero = @par_Numero  
  
			SET @ENCC_Numero = @@IDENTITY            
      
			SELECT @OFIC_Codigo =  ELPD.OFIC_Codigo  
			FROM Encabezado_Liquidacion_Planilla_Despachos AS ELPD  
                
			INNER JOIN Encabezado_Planilla_Despachos AS ENPD             
			ON ELPD.EMPR_Codigo = ENPD.EMPR_Codigo            
			AND ELPD.ENPD_Numero = ENPD.Numero   
              
			WHERE ELPD.EMPR_Codigo = @par_EMPR_Codigo AND ELPD.Numero = @par_Numero  
			---------VALOR DOCUMENTO----------            
			--VALOR FLETE            
			IF EXISTS (SELECT * FROM Detalle_Parametrizacion_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_TIPC_Codigo = @Valor_Documento AND CATA_VTPC_Codigo = @Documento_Valor_Flete AND ENPC_Codigo = @ENPC_Codigo )            
			BEGIN  
				INSERT INTO #TBL_Detalles_Comprobantes_Parametrizacion_Contable            
				SELECT             
				DEPC.Codigo,            
				ELPD.Valor_Flete_Transportador            
				FROM  
				Detalle_Parametrizacion_Contables AS DEPC             
				INNER JOIN Encabezado_Liquidacion_Planilla_Despachos AS ELPD            
				ON DEPC.EMPR_Codigo = ELPD.EMPR_Codigo            
				where             
				DEPC.EMPR_Codigo = @par_EMPR_Codigo             
				AND CATA_TIPC_Codigo = @Valor_Documento            
				AND CATA_VTPC_Codigo = @Documento_Valor_Flete            
				AND ENPC_Codigo = @ENPC_Codigo             
				AND ELPD.Numero = @par_Numero            
			END  
			--VALOR PAGAR TRANSPORTADOR            
			IF EXISTS (SELECT * FROM Detalle_Parametrizacion_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_TIPC_Codigo = @Valor_Documento AND CATA_VTPC_Codigo = @Documento_Valor_Pagar_Transportador AND ENPC_Codigo = @ENPC_Codigo )            
			BEGIN  
				INSERT INTO #TBL_Detalles_Comprobantes_Parametrizacion_Contable            
				SELECT             
				DEPC.Codigo,            
				ELPD.Valor_Pagar            
				FROM             
              
				Detalle_Parametrizacion_Contables AS DEPC             
				INNER JOIN Encabezado_Liquidacion_Planilla_Despachos AS ELPD            
				ON DEPC.EMPR_Codigo = ELPD.EMPR_Codigo            
				where             
				DEPC.EMPR_Codigo = @par_EMPR_Codigo             
				AND CATA_TIPC_Codigo = @Valor_Documento            
				AND CATA_VTPC_Codigo = @Documento_Valor_Pagar_Transportador            
				AND ENPC_Codigo = @ENPC_Codigo             
				AND ELPD.Numero = @par_Numero            
			END  
			---------VALOR CONCEPTOS-----------            
			IF EXISTS (SELECT * FROM Detalle_Parametrizacion_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_TIPC_Codigo = @Valor_Concepto AND ENPC_Codigo = @ENPC_Codigo )            
				BEGIN  
				INSERT INTO #TBL_Detalles_Comprobantes_Parametrizacion_Contable            
				SELECT             
				DEPC.Codigo,            
				DLPD.Valor            
				FROM  
				Detalle_Parametrizacion_Contables AS DEPC             
				INNER JOIN Detalle_Liquidacion_Planilla_Despachos AS DLPD            
				ON DEPC.EMPR_Codigo = DLPD.EMPR_Codigo            
				AND DEPC.CATA_VTPC_Codigo = DLPD.CLPD_Codigo            
				where             
				DEPC.EMPR_Codigo = @par_EMPR_Codigo             
				AND CATA_TIPC_Codigo = @Valor_Concepto            
				AND ENPC_Codigo = @ENPC_Codigo             
				AND DLPD.ELPD_Numero = @par_Numero            
				AND DLPD.Valor > 0            
			END            
			---------Valor Impuesto-----------            
			IF EXISTS (SELECT * FROM Detalle_Parametrizacion_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_TIPC_Codigo = @Valor_Impuesto AND ENPC_Codigo = @ENPC_Codigo )            
			BEGIN  
				INSERT INTO #TBL_Detalles_Comprobantes_Parametrizacion_Contable            
				SELECT             
				DEPC.Codigo,            
				DILP.Valor_Impuesto            
				FROM  
				Detalle_Parametrizacion_Contables AS DEPC             
				INNER JOIN Detalle_Impuestos_Liquidacion_Planilla_Despachos AS DILP            
				ON DEPC.EMPR_Codigo = DILP.EMPR_Codigo            
				AND DEPC.CATA_VTPC_Codigo = DILP.ENIM_Codigo            
				where             
				DEPC.EMPR_Codigo = @par_EMPR_Codigo             
				AND CATA_TIPC_Codigo = @Valor_Impuesto            
				AND ENPC_Codigo = @ENPC_Codigo             
				AND DILP.ELPD_Numero = @par_Numero            
				AND DILP.Valor_Impuesto > 0            
			END  
  
			INSERT INTO Detalle_Comprobante_Contables  
			(            
			EMPR_Codigo,  ENCC_Numero,  PLUC_Codigo,            
			Valor_Debito,              
			Valor_Credito,              
			Valor_Base,            
			Observaciones,              
			Documento_Cruce,              
			Documento_Cruce_Alterno,            
			TERC_Codigo,              
			Centro_Costo,              
			Prefijo,            
			Codigo_Anexo,              
			Sufijo_Codigo_Anexo,              
			Campo_Auxiliar            
			)            
			SELECT             
			@par_EMPR_Codigo,  
			@ENCC_Numero,  
			CASE WHEN DEPC.CATA_SIPC_Codigo = 4001 then       
			isnull(  
			(  
			SELECT Codigo from Plan_Unico_Cuentas   
			where empr_codigo = @par_empr_codigo   
			and Codigo_Cuenta = (Select Codigo_Cuenta from  Plan_Unico_Cuentas where empr_codigo = @par_empr_codigo and Codigo = DEPC.PLUC_Codigo)+(Select Codigo_Alterno FROM Oficinas where empr_codigo = @par_empr_codigo and codigo = @OFIC_Codigo )  
			),0  
			)      
			ELSE DEPC.PLUC_Codigo       
			END,         
			CASE WHEN DEPC.CATA_NACC_Codigo = @Naturaleza_Movimiento_Debito THEN TDCP.Valor ELSE 0 END,            
			CASE WHEN DEPC.CATA_NACC_Codigo = @Naturaleza_Movimiento_Credito THEN TDCP.Valor ELSE 0 END,            
			TDCP.Valor,            
			DEPC.Descripcion,            
			@Numero_Documento,            
			@Numero_Documento,            
			(SELECT ENPD.TERC_Codigo_Tenedor FROM Encabezado_Liquidacion_Planilla_Despachos AS ELPD             
			INNER JOIN Encabezado_Planilla_Despachos AS ENPD             
			ON ELPD.EMPR_Codigo = ENPD.EMPR_Codigo            
			AND ELPD.ENPD_Numero = ENPD.Numero            
			WHERE ELPD.EMPR_Codigo = @par_EMPR_Codigo AND ELPD.Numero = @par_Numero),            
			1,1,1,1,1            
            
			FROM             
              
			Detalle_Parametrizacion_Contables AS DEPC             
			INNER JOIN #TBL_Detalles_Comprobantes_Parametrizacion_Contable AS TDCP            
			ON DEPC.Codigo = TDCP.Codigo            
  
			where             
			DEPC.EMPR_Codigo = @par_EMPR_Codigo             
			AND ENPC_Codigo = @ENPC_Codigo             
		END  
	END   
	----------------LIQUIDACION---------------  
	----------------FACTURA---------------  
	IF @par_TIDO_Codigo = @Tipo_Documento_Factura  
	BEGIN  
		--Constantes            
		--Facturación            
		--Se consultan las parametrizaciones contables para la liquidación             
		SELECT @ENPC_Codigo = Codigo from Encabezado_Parametrizacion_Contables where EMPR_Codigo = @par_EMPR_Codigo AND  CATA_TIDG_Codigo = @Tipo_Documento_Genera_Factura  
		IF (@ENPC_Codigo) > 0  
		BEGIN  
			SELECT @Numero_Documento = Numero_Documento FROM Encabezado_Facturas WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_Numero            
			INSERT INTO Encabezado_Comprobante_Contables  
			(            
			EMPR_Codigo,  
			TIDO_Codigo,  
			Fecha,  
			TIDO_Documento,  
			Codigo_Documento,  
			Numero_Documento,  
			Fecha_Documento,  
			TERC_Documento,  
			OFIC_Documento,  
			CATA_DOOR_Codigo,  
			Año,  
			Periodo,  
			Observaciones,  
			Valor_Comprobante,  
			OFIC_Codigo,  
			CATA_ESIC_Codigo,  
			Anulado  
			)            
			SELECT            
			@par_EMPR_Codigo,  
			@Tipo_Documento_Comprobante_Contable,  
			GETDATE(),  
			@Tipo_Documento_Factura,  
			ENFA.Numero,  
			ENFA.Numero_Documento,  
			ENFA.Fecha,  
			ENFA.TERC_Cliente,  
			ENFA.OFIC_Factura,  
			@Documento_Origen_Factura,  
			YEAR(GETDATE()),  
			MONTH(GETDATE()),  
			'MOVIMIENTO CONTRABLE GENERADO PARA LA FACTURA N° '+ CONVERT(VARCHAR(50), ENFA.Numero_Documento),  
			0,  
			ENFA.OFIC_Factura,            
			@Estado_Comprobante_Contable_Pendiente,  
			0            
			FROM Encabezado_Facturas AS ENFA  
			WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo AND ENFA.Numero = @par_Numero            
            
			SELECT @ENCC_Numero = @@IDENTITY  
       
			SELECT  @OFIC_Codigo = ENFA.OFIC_Factura, @Cliente_Codigo =  ENFA.TERC_Cliente           
			FROM Encabezado_Facturas AS ENFA  
			WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo AND ENFA.Numero = @par_Numero            
			---------VALOR DOCUMENTO----------            
			--VALOR FACTURA            
			IF EXISTS (SELECT * FROM Detalle_Parametrizacion_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_TIPC_Codigo = @Valor_Documento AND CATA_VTPC_Codigo = @Documento_Valor_Factura AND ENPC_Codigo = @ENPC_Codigo )  
			BEGIN  
				INSERT INTO #TBL_Detalles_Comprobantes_Parametrizacion_Contable            
				SELECT             
				DEPC.Codigo,            
				ENFA.Valor_Factura            
				FROM             
              
				Detalle_Parametrizacion_Contables AS DEPC             
				INNER JOIN Encabezado_Facturas AS ENFA            
				ON DEPC.EMPR_Codigo = ENFA.EMPR_Codigo            
				where             
				DEPC.EMPR_Codigo = @par_EMPR_Codigo             
				AND CATA_TIPC_Codigo = @Valor_Documento            
				AND CATA_VTPC_Codigo = @Documento_Valor_Factura            
				AND ENPC_Codigo = @ENPC_Codigo             
				AND ENFA.Numero = @par_Numero            
			END            
			--SUBTOTAL            
			IF EXISTS (SELECT * FROM Detalle_Parametrizacion_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_TIPC_Codigo = @Valor_Documento AND CATA_VTPC_Codigo = @Documento_Subtotal AND ENPC_Codigo = @ENPC_Codigo )            
				BEGIN  
				INSERT INTO #TBL_Detalles_Comprobantes_Parametrizacion_Contable            
				SELECT             
				DEPC.Codigo,            
				(ENFA.Subtotal)            
				FROM             
              
				Detalle_Parametrizacion_Contables AS DEPC             
				INNER JOIN Encabezado_Facturas AS ENFA            
				ON DEPC.EMPR_Codigo = ENFA.EMPR_Codigo            
				where             
				DEPC.EMPR_Codigo = @par_EMPR_Codigo             
				AND CATA_TIPC_Codigo = @Valor_Documento            
				AND CATA_VTPC_Codigo = @Documento_Subtotal            
				AND ENPC_Codigo = @ENPC_Codigo             
				AND ENFA.Numero = @par_Numero            
			END                 
			---------VALOR CONCEPTOS-----------            
			IF EXISTS (SELECT * FROM Detalle_Parametrizacion_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_TIPC_Codigo = @Valor_Concepto AND ENPC_Codigo = @ENPC_Codigo )            
				BEGIN  
				INSERT INTO #TBL_Detalles_Comprobantes_Parametrizacion_Contable            
				SELECT             
				DEPC.Codigo,            
				SUM(DCFA.Valor_Concepto)            
				FROM  
				Detalle_Parametrizacion_Contables AS DEPC             
				INNER JOIN Detalle_Conceptos_Facturas AS DCFA            
				ON DEPC.EMPR_Codigo = DCFA.EMPR_Codigo            
				AND DEPC.CATA_VTPC_Codigo = DCFA.COVE_Codigo            
				where             
				DEPC.EMPR_Codigo = @par_EMPR_Codigo             
				AND CATA_TIPC_Codigo = @Valor_Concepto            
				AND ENPC_Codigo = @ENPC_Codigo             
				AND DCFA.ENFA_Numero = @par_Numero            
				AND DCFA.Valor_Concepto > 0  
				GROUP BY DEPC.Codigo, DCFA.COVE_Codigo            
			END            
			---------Valor Impuesto-----------            
			IF EXISTS (SELECT * FROM Detalle_Parametrizacion_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_TIPC_Codigo = @Valor_Impuesto AND ENPC_Codigo = @ENPC_Codigo )  
			BEGIN            
				INSERT INTO #TBL_Detalles_Comprobantes_Parametrizacion_Contable            
				SELECT             
				DEPC.Codigo,            
				SUM(DIFA.Valor_Impuesto)            
				FROM  
				Detalle_Parametrizacion_Contables AS DEPC             
				INNER JOIN Detalle_Impuestos_Facturas AS DIFA            
				ON DEPC.EMPR_Codigo = DIFA.EMPR_Codigo            
				AND DEPC.CATA_VTPC_Codigo = DIFA.ENIM_Codigo            
				where             
				DEPC.EMPR_Codigo = @par_EMPR_Codigo             
				AND CATA_TIPC_Codigo = @Valor_Impuesto            
				AND ENPC_Codigo = @ENPC_Codigo             
				AND DIFA.ENFA_Numero = @par_Numero            
				AND DIFA.Valor_Impuesto > 0  
				GROUP BY DEPC.Codigo, DIFA.ENIM_Codigo  
			END            
            
			INSERT INTO Detalle_Comprobante_Contables  
			(            
			EMPR_Codigo,  ENCC_Numero,  PLUC_Codigo,            
			Valor_Debito,              
			Valor_Credito,              
			Valor_Base,            
			Observaciones,              
			Documento_Cruce,              
			Documento_Cruce_Alterno,            
			TERC_Codigo,              
			Centro_Costo,              
			Prefijo,            
			Codigo_Anexo,              
			Sufijo_Codigo_Anexo,              
			Campo_Auxiliar            
			)            
			SELECT             
			@par_EMPR_Codigo,  
			@ENCC_Numero,  
			CASE WHEN DEPC.CATA_SIPC_Codigo = 4001 then       
			isnull(  
			(  
			SELECT Codigo from Plan_Unico_Cuentas  
			where empr_codigo = @par_empr_codigo   
			and Codigo_Cuenta = (Select Codigo_Cuenta from Plan_Unico_Cuentas where empr_codigo = @par_empr_codigo and Codigo = DEPC.PLUC_Codigo)+(Select Codigo_Alterno FROM Oficinas where empr_codigo = @par_empr_codigo and codigo = @OFIC_Codigo )  
			),0  
			)      
			WHEN DEPC.CATA_SIPC_Codigo = 4002 then       
			isnull(  
			(  
			SELECT Codigo from Plan_Unico_Cuentas  
			where empr_codigo = @par_empr_codigo   
			and Codigo_Cuenta = (Select Codigo_Cuenta from  Plan_Unico_Cuentas where empr_codigo = @par_empr_codigo and Codigo = DEPC.PLUC_Codigo)+(Select Codigo_Alterno FROM Terceros where empr_codigo = @par_empr_codigo and codigo = @Cliente_Codigo )  
			),0  
			)      
			ELSE DEPC.PLUC_Codigo       
			END,         
			CASE WHEN DEPC.CATA_NACC_Codigo = @Naturaleza_Movimiento_Debito THEN TDCP.Valor ELSE 0 END,            
			CASE WHEN DEPC.CATA_NACC_Codigo = @Naturaleza_Movimiento_Credito THEN TDCP.Valor ELSE 0 END,            
			TDCP.Valor,            
			DEPC.Descripcion,            
			@Numero_Documento,            
			@Numero_Documento,            
			(SELECT ENFA.TERC_Cliente FROM Encabezado_Facturas AS ENFA             
			WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo AND ENFA.Numero = @par_Numero),            
			1,1,1,1,1  
			FROM                 
			Detalle_Parametrizacion_Contables AS DEPC             
			INNER JOIN #TBL_Detalles_Comprobantes_Parametrizacion_Contable AS TDCP            
			ON DEPC.Codigo = TDCP.Codigo  
			where             
			DEPC.EMPR_Codigo = @par_EMPR_Codigo             
			AND ENPC_Codigo = @ENPC_Codigo  
		END  
	END           
	----------------FACTURA---------------  
	----------------NOTAS CREDITO---------------  
	IF @par_TIDO_Codigo = @Tipo_Documento_Nota_Credito  
	BEGIN  
		--Se consultan las parametrizaciones contables para la Nota Credito  
		SELECT @ENPC_Codigo = Codigo from Encabezado_Parametrizacion_Contables where EMPR_Codigo = @par_EMPR_Codigo AND  CATA_TIDG_Codigo = @Tipo_Documento_Genera_Nota_Credito  
		IF (@ENPC_Codigo) > 0  
		BEGIN  
			SELECT @Numero_Documento = Numero_Documento FROM Encabezado_Notas_Facturas WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_Numero   
			INSERT INTO Encabezado_Comprobante_Contables  
			(            
			EMPR_Codigo,  
			TIDO_Codigo,  
			Fecha,  
			TIDO_Documento,  
			Codigo_Documento,  
			Numero_Documento,  
			Fecha_Documento,  
			TERC_Documento,  
			OFIC_Documento,  
			CATA_DOOR_Codigo,  
			Año,  
			Periodo,  
			Observaciones,  
			Valor_Comprobante,  
			OFIC_Codigo,  
			CATA_ESIC_Codigo,  
			Anulado  
			)            
			SELECT            
			@par_EMPR_Codigo,  
			@Tipo_Documento_Comprobante_Contable,  
			GETDATE(),  
			@Tipo_Documento_Nota_Credito,  
			ENNF.Numero,  
			ENNF.Numero_Documento,  
			ENNF.Fecha,  
			ENNF.TERC_Cliente,  
			ENNF.OFIC_Nota,  
			@Documento_Origen_Nota_Credito,  
			YEAR(GETDATE()),  
			MONTH(GETDATE()),  
			'MOVIMIENTO CONTRABLE GENERADO PARA LA NOTA CRÉDITO N° '+ CONVERT(VARCHAR(50), ENNF.Numero_Documento),  
			0,  
			ENNF.OFIC_Nota,            
			@Estado_Comprobante_Contable_Pendiente,  
			0            
			FROM Encabezado_Notas_Facturas AS ENNF  
			WHERE ENNF.EMPR_Codigo = @par_EMPR_Codigo AND ENNF.Numero = @par_Numero  
  
			SELECT @ENCC_Numero = @@IDENTITY  
  
			SELECT  @OFIC_Codigo = ENNF.OFIC_Nota, @Cliente_Codigo =  ENNF.TERC_Cliente           
			FROM Encabezado_Notas_Facturas AS ENNF  
			WHERE ENNF.EMPR_Codigo = @par_EMPR_Codigo AND ENNF.Numero = @par_Numero   
			---------VALOR NOTA----------   
			IF EXISTS (SELECT * FROM Detalle_Parametrizacion_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_TIPC_Codigo = @Valor_Documento AND CATA_VTPC_Codigo = @Documento_Valor_Nota AND ENPC_Codigo = @ENPC_Codigo)  
			BEGIN  
				INSERT INTO #TBL_Detalles_Comprobantes_Parametrizacion_Contable            
				SELECT             
				DEPC.Codigo,            
				ENNF.Valor_Nota            
				FROM             
				Detalle_Parametrizacion_Contables AS DEPC             
				INNER JOIN Encabezado_Notas_Facturas AS ENNF            
				ON DEPC.EMPR_Codigo = ENNF.EMPR_Codigo            
				where             
				DEPC.EMPR_Codigo = @par_EMPR_Codigo             
				AND CATA_TIPC_Codigo = @Valor_Documento            
				AND CATA_VTPC_Codigo = @Documento_Valor_Nota            
				AND ENPC_Codigo = @ENPC_Codigo             
				AND ENNF.Numero = @par_Numero   
			END  
			---------VALOR NOTA----------   
			INSERT INTO Detalle_Comprobante_Contables  
			(            
			EMPR_Codigo,    
			ENCC_Numero,   
			PLUC_Codigo,            
			Valor_Debito,              
			Valor_Credito,              
			Valor_Base,            
			Observaciones,              
			Documento_Cruce,              
			Documento_Cruce_Alterno,            
			TERC_Codigo,              
			Centro_Costo,              
			Prefijo,            
			Codigo_Anexo,              
			Sufijo_Codigo_Anexo,              
			Campo_Auxiliar            
			)            
			SELECT             
			@par_EMPR_Codigo,  
			@ENCC_Numero,  
			CASE WHEN DEPC.CATA_SIPC_Codigo = 4001 then       
			isnull(  
			(  
			SELECT Codigo from Plan_Unico_Cuentas  
			where empr_codigo = @par_empr_codigo   
			and Codigo_Cuenta = (Select Codigo_Cuenta from Plan_Unico_Cuentas where empr_codigo = @par_empr_codigo and Codigo = DEPC.PLUC_Codigo)+(Select Codigo_Alterno FROM Oficinas where empr_codigo = @par_empr_codigo and codigo = @OFIC_Codigo )  
			),0  
			)      
			WHEN DEPC.CATA_SIPC_Codigo = 4002 then       
			isnull(  
			(  
			SELECT Codigo from Plan_Unico_Cuentas  
			where empr_codigo = @par_empr_codigo   
			and Codigo_Cuenta = (Select Codigo_Cuenta from  Plan_Unico_Cuentas where empr_codigo = @par_empr_codigo and Codigo = DEPC.PLUC_Codigo)+(Select Codigo_Alterno FROM Terceros where empr_codigo = @par_empr_codigo and codigo = @Cliente_Codigo )  
			),0  
			)      
			ELSE DEPC.PLUC_Codigo       
			END,         
			CASE WHEN DEPC.CATA_NACC_Codigo = @Naturaleza_Movimiento_Debito THEN TDCP.Valor ELSE 0 END,            
			CASE WHEN DEPC.CATA_NACC_Codigo = @Naturaleza_Movimiento_Credito THEN TDCP.Valor ELSE 0 END,            
			TDCP.Valor,            
			DEPC.Descripcion,            
			@Numero_Documento,            
			@Numero_Documento,            
			(SELECT ENNF.TERC_Cliente FROM Encabezado_Notas_Facturas AS ENNF             
			WHERE ENNF.EMPR_Codigo = @par_EMPR_Codigo AND ENNF.Numero = @par_Numero),            
			1,1,1,1,1  
			FROM                 
			Detalle_Parametrizacion_Contables AS DEPC             
			INNER JOIN #TBL_Detalles_Comprobantes_Parametrizacion_Contable AS TDCP            
			ON DEPC.Codigo = TDCP.Codigo  
			where             
			DEPC.EMPR_Codigo = @par_EMPR_Codigo             
			AND ENPC_Codigo = @ENPC_Codigo  
		END  
	END  
	----------------NOTAS CREDITO---------------  
	----------------NOTAS DEBITO---------------  
	IF @par_TIDO_Codigo = @Tipo_Documento_Nota_Debito  
	BEGIN  
		--Se consultan las parametrizaciones contables para la Nota Debito  
		SELECT @ENPC_Codigo = Codigo from Encabezado_Parametrizacion_Contables where EMPR_Codigo = @par_EMPR_Codigo AND  CATA_TIDG_Codigo = @Tipo_Documento_Genera_Nota_Debito  
		IF (@ENPC_Codigo) > 0  
		BEGIN  
			SELECT @Numero_Documento = Numero_Documento FROM Encabezado_Notas_Facturas WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_Numero   
			INSERT INTO Encabezado_Comprobante_Contables  
			(            
			EMPR_Codigo,  
			TIDO_Codigo,  
			Fecha,  
			TIDO_Documento,  
			Codigo_Documento,  
			Numero_Documento,  
			Fecha_Documento,  
			TERC_Documento,  
			OFIC_Documento,  
			CATA_DOOR_Codigo,  
			Año,  
			Periodo,  
			Observaciones,  
			Valor_Comprobante,  
			OFIC_Codigo,  
			CATA_ESIC_Codigo,  
			Anulado  
			)            
			SELECT            
			@par_EMPR_Codigo,  
			@Tipo_Documento_Comprobante_Contable,  
			GETDATE(),  
			@Tipo_Documento_Nota_Debito,  
			ENNF.Numero,  
			ENNF.Numero_Documento,  
			ENNF.Fecha,  
			ENNF.TERC_Cliente,  
			ENNF.OFIC_Nota,  
			@Documento_Origen_Nota_Debito,  
			YEAR(GETDATE()),  
			MONTH(GETDATE()),  
			'MOVIMIENTO CONTRABLE GENERADO PARA LA NOTA DÉBITO N° '+ CONVERT(VARCHAR(50), ENNF.Numero_Documento),  
			0,  
			ENNF.OFIC_Nota,            
			@Estado_Comprobante_Contable_Pendiente,  
			0            
			FROM Encabezado_Notas_Facturas AS ENNF  
			WHERE ENNF.EMPR_Codigo = @par_EMPR_Codigo AND ENNF.Numero = @par_Numero  
  
			SELECT @ENCC_Numero = @@IDENTITY  
  
			SELECT  @OFIC_Codigo = ENNF.OFIC_Nota, @Cliente_Codigo =  ENNF.TERC_Cliente           
			FROM Encabezado_Notas_Facturas AS ENNF  
			WHERE ENNF.EMPR_Codigo = @par_EMPR_Codigo AND ENNF.Numero = @par_Numero   
			---------VALOR NOTA----------   
			IF EXISTS (SELECT * FROM Detalle_Parametrizacion_Contables WHERE EMPR_Codigo = @par_EMPR_Codigo AND CATA_TIPC_Codigo = @Valor_Documento AND CATA_VTPC_Codigo = @Documento_Valor_Nota AND ENPC_Codigo = @ENPC_Codigo)  
			BEGIN  
				INSERT INTO #TBL_Detalles_Comprobantes_Parametrizacion_Contable            
				SELECT             
				DEPC.Codigo,            
				ENNF.Valor_Nota            
				FROM             
				Detalle_Parametrizacion_Contables AS DEPC             
				INNER JOIN Encabezado_Notas_Facturas AS ENNF            
				ON DEPC.EMPR_Codigo = ENNF.EMPR_Codigo            
				where             
				DEPC.EMPR_Codigo = @par_EMPR_Codigo             
				AND CATA_TIPC_Codigo = @Valor_Documento            
				AND CATA_VTPC_Codigo = @Documento_Valor_Nota            
				AND ENPC_Codigo = @ENPC_Codigo             
				AND ENNF.Numero = @par_Numero   
			END  
			---------VALOR NOTA----------   
			INSERT INTO Detalle_Comprobante_Contables  
			(            
			EMPR_Codigo,    
			ENCC_Numero,   
			PLUC_Codigo,            
			Valor_Debito,              
			Valor_Credito,              
			Valor_Base,            
			Observaciones,              
			Documento_Cruce,              
			Documento_Cruce_Alterno,            
			TERC_Codigo,              
			Centro_Costo,              
			Prefijo,            
			Codigo_Anexo,              
			Sufijo_Codigo_Anexo,              
			Campo_Auxiliar            
			)            
			SELECT             
			@par_EMPR_Codigo,  
			@ENCC_Numero,  
			CASE WHEN DEPC.CATA_SIPC_Codigo = 4001 then       
			isnull(  
			(  
			SELECT Codigo from Plan_Unico_Cuentas  
			where empr_codigo = @par_empr_codigo   
			and Codigo_Cuenta = (Select Codigo_Cuenta from Plan_Unico_Cuentas where empr_codigo = @par_empr_codigo and Codigo = DEPC.PLUC_Codigo)+(Select Codigo_Alterno FROM Oficinas where empr_codigo = @par_empr_codigo and codigo = @OFIC_Codigo )  
			),0  
			)      
			WHEN DEPC.CATA_SIPC_Codigo = 4002 then       
			isnull(  
			(  
			SELECT Codigo from Plan_Unico_Cuentas  
			where empr_codigo = @par_empr_codigo   
			and Codigo_Cuenta = (Select Codigo_Cuenta from  Plan_Unico_Cuentas where empr_codigo = @par_empr_codigo and Codigo = DEPC.PLUC_Codigo)+(Select Codigo_Alterno FROM Terceros where empr_codigo = @par_empr_codigo and codigo = @Cliente_Codigo )  
			),0  
			)      
			ELSE DEPC.PLUC_Codigo       
			END,         
			CASE WHEN DEPC.CATA_NACC_Codigo = @Naturaleza_Movimiento_Debito THEN TDCP.Valor ELSE 0 END,            
			CASE WHEN DEPC.CATA_NACC_Codigo = @Naturaleza_Movimiento_Credito THEN TDCP.Valor ELSE 0 END,            
			TDCP.Valor,            
			DEPC.Descripcion,            
			@Numero_Documento,            
			@Numero_Documento,            
			(SELECT ENNF.TERC_Cliente FROM Encabezado_Notas_Facturas AS ENNF             
			WHERE ENNF.EMPR_Codigo = @par_EMPR_Codigo AND ENNF.Numero = @par_Numero),            
			1,1,1,1,1  
			FROM                 
			Detalle_Parametrizacion_Contables AS DEPC             
			INNER JOIN #TBL_Detalles_Comprobantes_Parametrizacion_Contable AS TDCP            
			ON DEPC.Codigo = TDCP.Codigo  
			where             
			DEPC.EMPR_Codigo = @par_EMPR_Codigo             
			AND ENPC_Codigo = @ENPC_Codigo  
		END  
	END  
	----------------NOTAS DEBITO---------------  
	----------------COMPROBANTE INGRESO - EGRESO------------------------  
	IF @par_TIDO_Codigo IN (@Tipo_Documento_Comprobante_Egreso,@Tipo_Documento_Comprobante_Ingreso)        
	BEGIN  
		INSERT INTO Encabezado_Comprobante_Contables  
		(            
		EMPR_Codigo,  
		TIDO_Codigo,  
		Fecha,  
		TIDO_Documento,  
		Codigo_Documento,  
		Numero_Documento,  
		Fecha_Documento,  
		TERC_Documento,  
		OFIC_Documento,  
		CATA_DOOR_Codigo,  
		Año,  
		Periodo,  
		Observaciones,  
		Valor_Comprobante,  
		OFIC_Codigo,  
		CATA_ESIC_Codigo,  
		Anulado  
		)            
		SELECT            
		@par_EMPR_Codigo,  
		@Tipo_Documento_Comprobante_Contable,  
		GETDATE(),  
		@par_TIDO_Codigo,  
		ENDC.Codigo,  
		ENDC.Numero,  
		ENDC.Fecha,  
		ENDC.TERC_Codigo_Titular,  
		ENDC.OFIC_Codigo_Creacion,  
		CASE WHEN @par_TIDO_Codigo = 30 THEN @Documento_Origen_Comprobante_Egreso ELSE @Documento_Origen_Comprobante_Ingreso END ,  
		YEAR(GETDATE()),  
		MONTH(GETDATE()),            
		'MOVIMIENTO CONTRABLE GENERADO PARA EL COMPROBANTE DE ' + CASE WHEN @par_TIDO_Codigo = 30 THEN 'EGRESO' ELSE 'INGRESO' END+' N° ' + CONVERT(VARCHAR(50), ENDC.Numero),  
		ENDC.Valor_Pago_Recaudo_Total,  
		ENDC.OFIC_Codigo_Creacion,            
		@Estado_Comprobante_Contable_Pendiente,  
		0  
		FROM Encabezado_Documento_Comprobantes AS ENDC  
		WHERE ENDC.EMPR_Codigo = @par_EMPR_Codigo AND ENDC.Codigo = @par_Numero  
		SET @ENCC_Numero = @@IDENTITY  
		INSERT INTO Detalle_Comprobante_Contables (  
		EMPR_Codigo,        
		ENCC_Numero,        
		PLUC_Codigo,        
		Valor_Debito,        
		Valor_Credito,        
		Valor_Base,        
		Observaciones,        
		Documento_Cruce,        
		Documento_Cruce_Alterno,        
		TERC_Codigo,        
		Centro_Costo,        
		Prefijo,        
		Codigo_Anexo,        
		Sufijo_Codigo_Anexo,        
		Campo_Auxiliar        
		)        
		SELECT  DEDC.EMPR_Codigo,        
		@ENCC_Numero,        
		DEDC.PLUC_Codigo,        
		DEDC.Valor_Debito,        
		DEDC.Valor_Credito,        
		DEDC.Valor_Base,        
		PLUC.Nombre,        
		ENDC.Numero,        
		ENDC.Numero,        
		DEDC.TERC_Codigo,        
		1,        
		1,        
		1,        
		1,        
		1        
		FROM Detalle_Documento_Comprobantes AS DEDC INNER JOIN         
		Encabezado_Documento_Comprobantes AS ENDC ON         
		DEDC.EMPR_Codigo = ENDC.EMPR_Codigo         
		AND DEDC.EDCO_Codigo = ENDC.Codigo        
		LEFT JOIN Plan_Unico_Cuentas AS PLUC ON        
		DEDC.EMPR_Codigo = PLUC.EMPR_Codigo         
		AND DEDC.PLUC_Codigo = PLUC.Codigo        
		WHERE ENDC.EMPR_Codigo = @par_EMPR_Codigo AND ENDC.Codigo = @par_Numero  
	END  
	----------------COMPROBANTE INGRESO - EGRESO------------------------  
	Select @ENCC_Numero as Numero  
END
GO