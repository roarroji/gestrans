﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 09/01/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

PRINT 'gsp_anular_encabezado_inspeccion_preoperacional'
GO
DROP PROCEDURE gsp_anular_encabezado_inspeccion_preoperacional
GO
CREATE PROCEDURE gsp_anular_encabezado_inspeccion_preoperacional 
(  
@par_EMPR_Codigo SMALLINT,  
@par_Numero NUMERIC,  
@par_Causa_Anulacion VARCHAR(150),  
@par_USUA_Codigo_Anula SMALLINT  
)  
AS  
BEGIN  

 UPDATE Encabezado_Inspecciones
 SET   
 Causa_Anula = @par_Causa_Anulacion,  
 USUA_Codigo_Anula = @par_USUA_Codigo_Anula,  
 Fecha_Anula = GETDATE(),  
 Anulado = 1  
 WHERE  
 EMPR_Codigo = @par_EMPR_Codigo  
 AND Numero = @par_Numero  
  
SELECT @@ROWCOUNT AS Numero  
  
END 
GO 