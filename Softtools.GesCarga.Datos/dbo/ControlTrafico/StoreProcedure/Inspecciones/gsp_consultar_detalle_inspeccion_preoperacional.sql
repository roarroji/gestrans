﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 09/01/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

PRINT 'gsp_consultar_detalle_inspeccion_preoperacional'
GO
DROP PROCEDURE gsp_consultar_detalle_inspeccion_preoperacional
GO
CREATE PROCEDURE gsp_consultar_detalle_inspeccion_preoperacional 
(  
@par_EMPR_Codigo SMALLINT,  
@par_Numero NUMERIC  
)  
AS  
BEGIN  
SELECT   
EMPR_Codigo,  
ENIN_Numero,  
ID,  
DSFI_Codigo,  
DIFI_Codigo,  
Nombre,  
Orden_Seccion,  
Relevante,  
Cumple,  
Orden_Item,  
Tamaño,  
CATA_TCON_Codigo,  
Valor  
FROM   
Detalle_Inspecciones   
WHERE   
EMPR_Codigo = @par_EMPR_Codigo  
AND ENIN_Numero = @par_Numero   
  
END  
GO