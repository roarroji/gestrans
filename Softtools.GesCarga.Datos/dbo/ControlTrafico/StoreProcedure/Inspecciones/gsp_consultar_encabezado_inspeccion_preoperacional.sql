﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 09/01/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_consultar_encabezado_inspeccion_preoperacional'
GO
DROP PROCEDURE gsp_consultar_encabezado_inspeccion_preoperacional
GO
CREATE PROCEDURE gsp_consultar_encabezado_inspeccion_preoperacional 
(              
 @par_EMPR_Codigo SMALLINT,              
 @par_Numero NUMERIC = NULL,  
 @par_Vehiculo VARCHAR(6) = NULL,          
 @par_Fecha_Inicial DATETIME =NULL,           
 @par_Fecha_Final DATETIME =NULL,   
 @par_Codigo_Alterno VARCHAR(20) = NULL,   
 @par_Codigo_Conductor NUMERIC = NULL,         
 @par_Codigo_Transportador NUMERIC = NULL,   
 @par_Codigo_Cliente NUMERIC = NULL,    
 @par_Nombre_Conductor VARCHAR(100) = NULL,             
 @par_Nombre_Transportador VARCHAR(100) = NULL,   
 @par_Nombre_Cliente VARCHAR(100) = NULL,                     
 @par_Formato_Inspeccion VARCHAR(100) = NULL,              
 @par_Estado SMALLINT = NULL,          
 @par_Anulado SMALLINT = NULL,                  
 @par_NumeroPagina INT = NULL,          
 @par_RegistrosPagina INT = NULL            
)              
AS              
BEGIN              
        
set @par_Fecha_Inicial = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Inicial)                       
set @par_Fecha_Inicial = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Inicial)),@par_Fecha_Inicial)                              
set @par_Fecha_Inicial = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Inicial)),@par_Fecha_Inicial)                                
set @par_Fecha_Final = dbo.Func_Fecha_Quita_Segundos(@par_Fecha_Final)                            
set @par_Fecha_Final = DATEADD(MINUTE, -(DATEPART(MINUTE,@par_Fecha_Final)),@par_Fecha_Final)                              
set @par_Fecha_Final = DATEADD(HOUR, -(DATEPART(HOUR,@par_Fecha_Final)),@par_Fecha_Final)                              
set @par_Fecha_Final = DATEADD(MILLISECOND, 998,@par_Fecha_Final)                              
set @par_Fecha_Final = DATEADD(SECOND,59,@par_Fecha_Final)                              
set @par_Fecha_Final = DATEADD(MINUTE,59,@par_Fecha_Final)                              
set @par_Fecha_Final = DATEADD(HOUR,23,@par_Fecha_Final)               
        
 SET NOCOUNT ON;              
  DECLARE              
   @CantidadRegistros INT              
  SELECT @CantidadRegistros = (              
    SELECT DISTINCT               
     COUNT(1)               
   FROM          
   Encabezado_Inspecciones ENIN    
     
   LEFT JOIN Vehiculos VEHI ON          
   ENIN.EMPR_Codigo = VEHI.EMPR_Codigo          
   AND ENIN.VEHI_Codigo = VEHI.Codigo          
        
   LEFT JOIN Encabezado_Formularios_Inspecciones EFIN ON           
   ENIN.EMPR_Codigo = EFIN.EMPR_Codigo          
   AND ENIN.EFIN_Codigo = EFIN.Codigo                  
          
   LEFT JOIN Semirremolques SEMI ON          
   ENIN.EMPR_Codigo = SEMI.EMPR_Codigo          
   AND ENIN.SEMI_Codigo = SEMI.Codigo          
          
   LEFT JOIN Terceros CLIE ON          
   ENIN.EMPR_Codigo = CLIE.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Cliente = CLIE.Codigo          
          
   LEFT JOIN Terceros COND ON          
   ENIN.EMPR_Codigo = COND.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Conductor = COND.Codigo          
          
   LEFT JOIN Terceros TRAP ON          
   ENIN.EMPR_Codigo = TRAP.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Transportador = TRAP.Codigo          
                       
   LEFT JOIN Sitios_Cargue_Descargue SICA ON          
   ENIN.EMPR_Codigo = SICA.EMPR_Codigo          
   AND ENIN.SICD_Codigo_Cargue = SICA.Codigo          
          
   LEFT JOIN Sitios_Cargue_Descargue SIDE ON          
   ENIN.EMPR_Codigo = SIDE.EMPR_Codigo          
   AND ENIN.SICD_Codigo_Descargue = SIDE.Codigo          
          
   LEFT JOIN Terceros RESP ON          
   ENIN.EMPR_Codigo = RESP.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Responsable = RESP.Codigo       
     
   LEFT JOIN Terceros AUZA ON          
   ENIN.EMPR_Codigo = AUZA.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Autoriza = AUZA.Codigo       
                 
  WHERE              
   ENIN.EMPR_Codigo = @par_EMPR_Codigo        
   AND ENIN.Numero = ISNULL(@par_Numero,ENIN.Numero)                              
   AND ((VEHI.Placa LIKE '%' + @par_Vehiculo + '%') OR (@par_Vehiculo IS NULL))          
   AND ((EFIN.Nombre LIKE '%' + @par_Formato_Inspeccion + '%') OR (@par_Formato_Inspeccion IS NULL))                 
   AND ENIN.Fecha_Inspeccion >= ISNULL(@par_Fecha_Inicial,ENIN.Fecha_Inspeccion)              
   AND ENIN.Fecha_Inspeccion <= ISNULL(@par_Fecha_Final,ENIN.Fecha_Inspeccion)                     
   AND ((ENIN.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))                 
   AND ENIN.TERC_Codigo_Conductor = ISNULL(@par_Codigo_Cliente, ENIN.TERC_Codigo_Conductor)        
   AND ENIN.TERC_Codigo_Transportador = ISNULL(@par_Codigo_Transportador, ENIN.TERC_Codigo_Transportador)        
   AND ENIN.TERC_Codigo_Cliente = ISNULL(@par_Codigo_Cliente, ENIN.TERC_Codigo_Cliente)        
   AND ((CONCAT(isnull(TRAP.Razon_Social,''),isnull(TRAP.Nombre,''),' ',TRAP.Apellido1,' ',TRAP.Apellido2) LIKE '%' + @par_Nombre_Transportador + '%') OR (TRAP.Numero_Identificacion = @par_Nombre_Transportador) OR( @par_Nombre_Transportador IS NULL))     
  
   AND ((CONCAT(isnull(CLIE.Razon_Social,''),isnull(CLIE.Nombre,''),' ',CLIE.Apellido1,' ',CLIE.Apellido2) LIKE '%' + @par_Nombre_Cliente + '%') OR (CLIE.Numero_Identificacion = @par_Nombre_Cliente) OR( @par_Nombre_Cliente IS NULL))          
   AND ((CONCAT(isnull(COND.Razon_Social,''),isnull(COND.Nombre,''),' ',COND.Apellido1,' ',COND.Apellido2) LIKE '%' + @par_Nombre_Conductor + '%') OR (COND.Numero_Identificacion = @par_Nombre_Conductor) OR( @par_Nombre_Conductor IS NULL))          
   AND ENIN.Estado = ISNULL(@par_Estado,ENIN.Estado)              
   AND ENIN.Anulado = ISNULL(@par_Anulado,ENIN.Anulado)      
  );                     
    WITH Pagina AS              
    (              
              
  SELECT           
   ENIN.EMPR_Codigo,          
   ENIN.Numero,          
   ENIN.TIDO_Codigo,              
   ENIN.Fecha_Inspeccion,          
   ENIN.Tiempo_Vigencia,          
   ENIN.Codigo_Alterno,      
   ENIN.EFIN_Codigo,              
   EFIN.Nombre AS FormularioInspeccion,   
   ENIN.VEHI_Codigo,         
   VEHI.Placa AS Vehiculo,   
   ISNULL(ENIN.SEMI_Codigo, 0) AS SEMI_Codigo,         
   ISNULL(SEMI.Placa, '') AS Semirremolque,    
   ENIN.TERC_Codigo_Cliente,        
   ISNULL(CLIE.Nombre,'') +' '+ ISNULL(CLIE.Apellido1,'')+ ' '+ISNULL(CLIE.Apellido2, '') +' '+ISNULL(CLIE.Razon_Social,'') AS NombreCliente,    
   ENIN.TERC_Codigo_Conductor,        
   ISNULL(COND.Nombre,'') +' '+ ISNULL(COND.Apellido1,'')+ ' '+ISNULL(COND.Apellido2, '') +' '+ISNULL(COND.Razon_Social,'') AS NombreConductor,  
   ENIN.TERC_Codigo_Transportador,          
   ISNULL(TRAP.Nombre,'') +' '+ ISNULL(TRAP.Apellido1,'')+ ' '+ISNULL(TRAP.Apellido2, '') +' '+ISNULL(TRAP.Razon_Social,'') AS NombreTransportador,     
   ENIN.TERC_Codigo_Responsable,       
   ISNULL(RESP.Nombre,'') +' '+ ISNULL(RESP.Apellido1,'')+ ' '+ISNULL(RESP.Apellido2, '') +' '+ISNULL(RESP.Razon_Social,'') AS NombreResponsable,   
   ISNULL(ENIN.TERC_Codigo_Autoriza, 0) AS TERC_Codigo_Autoriza,       
   ISNULL(AUZA.Nombre,'') +' '+ ISNULL(AUZA.Apellido1,'')+ ' '+ISNULL(AUZA.Apellido2, '') +' '+ISNULL(AUZA.Razon_Social,'') AS NombreAutoriza,   
   ISNULL(ENIN.Fecha_Autoriza,'') AS Fecha_Autoriza,  
   ENIN.Fecha_Hora_Inicio_Inspeccion,  
   ENIN.Fecha_Hora_Fin_Inspeccion,  
   ENIN.SICD_Codigo_Cargue,         
   ISNULL(SICA.Nombre, '') AS SitioCargue,   
   ENIN.SICD_Codigo_Descargue,           
   ISNULL(SIDE.Nombre, '') AS SitioDescargue,    
   ENIN.Observaciones,        
   ISNULL(ENIN.Firma, '') AS Firma,   
   ENIN.Estado,     
   ENIN.Anulado,           
   ROW_NUMBER() OVER(ORDER BY ENIN.Numero) AS RowNumber          
          
   FROM          
   Encabezado_Inspecciones ENIN    
     
   LEFT JOIN Vehiculos VEHI ON          
   ENIN.EMPR_Codigo = VEHI.EMPR_Codigo          
   AND ENIN.VEHI_Codigo = VEHI.Codigo          
        
   LEFT JOIN Encabezado_Formularios_Inspecciones EFIN ON           
   ENIN.EMPR_Codigo = EFIN.EMPR_Codigo          
   AND ENIN.EFIN_Codigo = EFIN.Codigo                  
          
   LEFT JOIN Semirremolques SEMI ON          
   ENIN.EMPR_Codigo = SEMI.EMPR_Codigo          
   AND ENIN.SEMI_Codigo = SEMI.Codigo          
          
   LEFT JOIN Terceros CLIE ON          
   ENIN.EMPR_Codigo = CLIE.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Cliente = CLIE.Codigo          
          
   LEFT JOIN Terceros COND ON          
   ENIN.EMPR_Codigo = COND.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Conductor = COND.Codigo          
          
   LEFT JOIN Terceros TRAP ON          
   ENIN.EMPR_Codigo = TRAP.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Transportador = TRAP.Codigo          
                       
   LEFT JOIN Sitios_Cargue_Descargue SICA ON          
   ENIN.EMPR_Codigo = SICA.EMPR_Codigo          
   AND ENIN.SICD_Codigo_Cargue = SICA.Codigo          
          
   LEFT JOIN Sitios_Cargue_Descargue SIDE ON          
   ENIN.EMPR_Codigo = SIDE.EMPR_Codigo          
   AND ENIN.SICD_Codigo_Descargue = SIDE.Codigo          
          
   LEFT JOIN Terceros RESP ON          
   ENIN.EMPR_Codigo = RESP.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Responsable = RESP.Codigo       
     
   LEFT JOIN Terceros AUZA ON          
   ENIN.EMPR_Codigo = AUZA.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Autoriza = AUZA.Codigo          
                 
  WHERE              
    ENIN.EMPR_Codigo = @par_EMPR_Codigo        
   AND ENIN.Numero = ISNULL(@par_Numero,ENIN.Numero)                         
   AND ((VEHI.Placa LIKE '%' + @par_Vehiculo + '%') OR (@par_Vehiculo IS NULL))          
   AND ((EFIN.Nombre LIKE '%' + @par_Formato_Inspeccion + '%') OR (@par_Formato_Inspeccion IS NULL))                 
   AND ENIN.Fecha_Inspeccion >= ISNULL(@par_Fecha_Inicial,ENIN.Fecha_Inspeccion)              
   AND ENIN.Fecha_Inspeccion <= ISNULL(@par_Fecha_Final,ENIN.Fecha_Inspeccion)                     
   AND ((ENIN.Codigo_Alterno LIKE '%' + @par_Codigo_Alterno + '%') OR (@par_Codigo_Alterno IS NULL))                 
   AND ENIN.TERC_Codigo_Conductor = ISNULL(@par_Codigo_Cliente, ENIN.TERC_Codigo_Conductor)        
   AND ENIN.TERC_Codigo_Transportador = ISNULL(@par_Codigo_Transportador, ENIN.TERC_Codigo_Transportador)        
   AND ENIN.TERC_Codigo_Cliente = ISNULL(@par_Codigo_Cliente, ENIN.TERC_Codigo_Cliente)        
   AND ((CONCAT(isnull(TRAP.Razon_Social,''),isnull(TRAP.Nombre,''),' ',TRAP.Apellido1,' ',TRAP.Apellido2) LIKE '%' + @par_Nombre_Transportador + '%') OR (TRAP.Numero_Identificacion = @par_Nombre_Transportador) OR( @par_Nombre_Transportador IS NULL))     
  
   AND ((CONCAT(isnull(CLIE.Razon_Social,''),isnull(CLIE.Nombre,''),' ',CLIE.Apellido1,' ',CLIE.Apellido2) LIKE '%' + @par_Nombre_Cliente + '%') OR (CLIE.Numero_Identificacion = @par_Nombre_Cliente) OR( @par_Nombre_Cliente IS NULL))          
   AND ((CONCAT(isnull(COND.Razon_Social,''),isnull(COND.Nombre,''),' ',COND.Apellido1,' ',COND.Apellido2) LIKE '%' + @par_Nombre_Conductor + '%') OR (COND.Numero_Identificacion = @par_Nombre_Conductor) OR( @par_Nombre_Conductor IS NULL))          
   AND ENIN.Estado = ISNULL(@par_Estado,ENIN.Estado)              
   AND ENIN.Anulado = ISNULL(@par_Anulado,ENIN.Anulado)    
  )               
  SELECT               
    0 As Obtener,            
 EMPR_Codigo,          
 Numero,          
 TIDO_Codigo,           
 Fecha_Inspeccion,          
 Tiempo_Vigencia,          
 Codigo_Alterno,                     
 Observaciones,        
 EFIN_Codigo,         
 FormularioInspeccion,      
 VEHI_Codigo,    
 Vehiculo,    
 SEMI_Codigo,          
 Semirremolque,    
 TERC_Codigo_Cliente,        
 NombreCliente,   
 TERC_Codigo_Conductor,        
 NombreConductor,      
 TERC_Codigo_Transportador,       
 NombreTransportador,   
 Fecha_Hora_Inicio_Inspeccion,  
 Fecha_Hora_Fin_Inspeccion,  
 TERC_Codigo_Responsable,         
 NombreResponsable,  
 TERC_Codigo_Autoriza,         
 NombreAutoriza,  
 Fecha_Autoriza,  
 SICD_Codigo_Cargue,          
 SitioCargue,   
 SICD_Codigo_Descargue,         
 SitioDescargue,  
 Observaciones,        
 Firma,   
 Estado,     
 Anulado,          
   @CantidadRegistros AS TotalRegistros,              
   @par_NumeroPagina AS PaginaObtener,              
   @par_RegistrosPagina AS RegistrosPagina              
  FROM              
   Pagina              
  WHERE              
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, 1000)            
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, 1000)            
  ORDER BY Numero ASC              
END              
GO