﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 09/01/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_obtener_encabezado_inspeccion_preoperacional'
GO
DROP PROCEDURE gsp_obtener_encabezado_inspeccion_preoperacional
GO
CREATE PROCEDURE gsp_obtener_encabezado_inspeccion_preoperacional 
(        
@par_EMPR_Codigo  SMALLINT,        
@par_Numero NUMERIC = NULL,                
@par_Codigo_Formato_Inspeccion NUMERIC = NULL    
)        
AS        
BEGIN        
        
SELECT         
 1 AS Obtener,        
 ENIN.EMPR_Codigo,        
 ENIN.Numero,        
 ENIN.TIDO_Codigo,            
 ENIN.EFIN_Codigo,        
 ENIN.Fecha_Inspeccion,        
 ENIN.Tiempo_Vigencia,        
 ENIN.Codigo_Alterno,        
 ENIN.VEHI_Codigo,        
 ISNULL(ENIN.SEMI_Codigo, 0) AS SEMI_Codigo,        
 ENIN.TERC_Codigo_Cliente,        
 ENIN.TERC_Codigo_Conductor,        
 ENIN.TERC_Codigo_Transportador,        
 ISNULL(ENIN.TERC_Codigo_Autoriza, 0) AS TERC_Codigo_Autoriza,    
 ISNULL(ENIN.Fecha_Hora_Inicio_Inspeccion,'') AS Fecha_Hora_Inicio_Inspeccion,        
 ISNULL(ENIN.Fecha_Hora_Fin_Inspeccion,'') AS Fecha_Hora_Fin_Inspeccion,        
 ENIN.SICD_Codigo_Cargue,        
 ENIN.SICD_Codigo_Descargue,        
 ENIN.Observaciones,        
 ENIN.TERC_Codigo_Responsable,        
 ISNULL(ENIN.Firma,'') AS Firma,              
 ENIN.Estado,        
 ENIN.Anulado,              
 EFIN.Nombre AS FormularioInspeccion,        
 VEHI.Placa AS Vehiculo,        
 ISNULL(SEMI.Placa, '' ) AS Semirremolque,        
 ISNULL(CLIE.Nombre,'') +' '+ ISNULL(CLIE.Apellido1,'')+ ' '+ISNULL(CLIE.Apellido2, '') +' '+ISNULL(CLIE.Razon_Social,'') AS NombreCliente,        
 ISNULL(COND.Nombre,'') +' '+ ISNULL(COND.Apellido1,'')+ ' '+ISNULL(COND.Apellido2, '') +' '+ISNULL(COND.Razon_Social,'') AS NombreConductor,        
 ISNULL(TRAP.Nombre,'') +' '+ ISNULL(TRAP.Apellido1,'')+ ' '+ISNULL(TRAP.Apellido2, '') +' '+ISNULL(TRAP.Razon_Social,'') AS NombreTransportador,     
 ISNULL(AUZA.Nombre,'') +' '+ ISNULL(AUZA.Apellido1,'')+ ' '+ISNULL(AUZA.Apellido2, '') +' '+ISNULL(AUZA.Razon_Social,'') AS NombreAutoriza,       
 ISNULL(RESP.Nombre,'') +' '+ ISNULL(RESP.Apellido1,'')+ ' '+ISNULL(RESP.Apellido2, '') +' '+ISNULL(RESP.Razon_Social,'') AS NombreResponsable,    
 ISNULL(ENIN.Fecha_Autoriza,'') AS Fecha_Autoriza,        
 ISNULL(SICA.Nombre,'') AS SitioCargue,        
 ISNULL(SIDE.Nombre,'') AS SitioDescargue       
        
 FROM          
   Encabezado_Inspecciones ENIN    
     
   LEFT JOIN Vehiculos VEHI ON          
   ENIN.EMPR_Codigo = VEHI.EMPR_Codigo          
   AND ENIN.VEHI_Codigo = VEHI.Codigo          
        
   LEFT JOIN Encabezado_Formularios_Inspecciones EFIN ON           
   ENIN.EMPR_Codigo = EFIN.EMPR_Codigo          
   AND ENIN.EFIN_Codigo = EFIN.Codigo                  
          
   LEFT JOIN Semirremolques SEMI ON          
   ENIN.EMPR_Codigo = SEMI.EMPR_Codigo          
   AND ENIN.SEMI_Codigo = SEMI.Codigo          
          
   LEFT JOIN Terceros CLIE ON          
   ENIN.EMPR_Codigo = CLIE.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Cliente = CLIE.Codigo          
          
   LEFT JOIN Terceros COND ON          
   ENIN.EMPR_Codigo = COND.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Conductor = COND.Codigo          
          
   LEFT JOIN Terceros TRAP ON          
   ENIN.EMPR_Codigo = TRAP.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Transportador = TRAP.Codigo          
                       
   LEFT JOIN Sitios_Cargue_Descargue SICA ON          
   ENIN.EMPR_Codigo = SICA.EMPR_Codigo          
   AND ENIN.SICD_Codigo_Cargue = SICA.Codigo          
          
   LEFT JOIN Sitios_Cargue_Descargue SIDE ON          
   ENIN.EMPR_Codigo = SIDE.EMPR_Codigo          
   AND ENIN.SICD_Codigo_Descargue = SIDE.Codigo          
          
   LEFT JOIN Terceros RESP ON          
   ENIN.EMPR_Codigo = RESP.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Responsable = RESP.Codigo    
  
   LEFT JOIN Terceros AUZA ON          
   ENIN.EMPR_Codigo = AUZA.EMPR_Codigo          
   AND ENIN.TERC_Codigo_Autoriza = AUZA.Codigo    
        
WHERE         
 ENIN.EMPR_Codigo = @par_EMPR_Codigo        
 AND ENIN.Numero = ISNULL(@par_Numero,ENIN.Numero)           
 AND ENIN.EFIN_Codigo = ISNULL(@par_Codigo_Formato_Inspeccion,ENIN.EFIN_Codigo)             
END   
GO