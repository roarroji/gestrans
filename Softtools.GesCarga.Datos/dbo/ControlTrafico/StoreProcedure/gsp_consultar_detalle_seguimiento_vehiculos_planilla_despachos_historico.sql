﻿
CREATE PROCEDURE gsp_consultar_detalle_seguimiento_vehiculos_planilla_despachos_historico                                 
(                                                                  
@par_EMPR_Codigo SMALLINT,            
@par_Fecha_Inicial DATETIME = NULL,                                              
@par_Fecha_Final DATETIME = NULL,           
@par_ENPD_Numero NUMERIC = NULL,                                                        
@par_Numero_Manifiesto NUMERIC = NULL,                                                              
--@par_Puesto_Control NUMERIC = NULL,                                                                  
@par_Vehiculo VARCHAR(10) = NULL,                                                                       
@par_Conductor VARCHAR(100) = NULL,                                                                               
@par_Ruta NUMERIC = NULL,                                                      
@par_CATA_TOSV_Codigo NUMERIC = NULL,                                                                        
@par_CATA_SRSV_Codigo NUMERIC = NULL,                                                                 
@par_Mostrar NUMERIC = NULL,                                                                  
@par_Estado NUMERIC = NULL,                                                                  
@par_Anulado NUMERIC = NULL,                                   
@par_Codigos_Oficinas VARCHAR(150) = null,                                                                 
@par_TERC_Codigo_Conductor NUMERIC = NULL,                                                                  
@par_TERC_Codigo_Cliente NUMERIC = NULL,                                                                  
--@par_CATA_TIDU_Codigo NUMERIC = NULL,                                                                  
--@par_CIUD_Codigo_Origen NUMERIC = NULL,                                                                  
--@par_CIUD_Codigo_Destino NUMERIC = NULL,                                                                  
@par_PRTR_Codigo NUMERIC = NULL,                                                                  
@par_Reportar_Cliente SMALLINT = NULL,                     
@par_NumeroPagina INT = NULL,                                                                      
@par_RegistrosPagina INT = NULL                                                                      
)                                                                  
AS                                                                  
BEGIN                               
        
--Se cargan los valores de tiempo de desfase del segumineto segun la empresa por medio del catalogo 204        
        
DECLARE @TIEMPO1 NUMERIC,@TIEMPO2 NUMERIC,@TIEMPO3 NUMERIC,@TIEMPO4 NUMERIC        
SELECT @TIEMPO1 = Campo3 FROM Valor_Catalogos where Codigo = 20401 and EMPR_Codigo = @par_EMPR_Codigo        
SELECT @TIEMPO2 = Campo3 FROM Valor_Catalogos where Codigo = 20402 and EMPR_Codigo = @par_EMPR_Codigo        
SELECT @TIEMPO3 = Campo3 FROM Valor_Catalogos where Codigo = 20403 and EMPR_Codigo = @par_EMPR_Codigo        
SELECT @TIEMPO4 = Campo3 FROM Valor_Catalogos where Codigo = 20404 and EMPR_Codigo = @par_EMPR_Codigo        
                      
SELECT EMPR_Codigo,ROW_NUMBER() OVER(PARTITION BY EMPR_Codigo, ENPD_Numero ORDER BY Fecha_Reporte) AS Orden, ENPD_Numero,ID INTO #TempOrden FROM dbo.Detalle_Seguimiento_Vehiculos                      
                      
update DSVE SET DSVE.Orden =  temp.ORDEN from Detalle_Seguimiento_Vehiculos as DSVE inner join #TempOrden as temp on                      
DSVE.EMPR_Codigo = temp.EMPR_Codigo                      
and DSVE.ID = temp.ID                      
                      
DROP TABLE #TempOrden                      
                      
DECLARE @IDMAX NUMERIC  = 0                                                                  
                                                                  
SET NOCOUNT ON;                     
  DECLARE                                                                         @CantidadRegistros INT                                                                        
  SELECT @CantidadRegistros = (                                                                        
    SELECT DISTINCT                                                                         
  COUNT(1)                                                                         
         FROM                                                                        
   Detalle_Seguimiento_Vehiculos DESV                                                                  
                                                                
   INNER JOIN Encabezado_Planilla_Despachos AS ENPD                                                                
   ON DESV.EMPR_Codigo = ENPD.EMPR_Codigo                                                                  
   AND DESV.ENPD_Numero = ENPD.Numero                                                             
          
   LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC                                                                
 ON ENPD.EMPR_Codigo = ENMC.EMPR_Codigo                                                                  
   AND ENPD.Numero = ENMC.ENPD_Numero                                                            
                                          
   INNER JOIN (SELECT EMPR_Codigo, ISNULL(MAX(Orden),0) AS Orden, ENPD_Numero FROM Detalle_Seguimiento_Vehiculos GROUP BY EMPR_Codigo, ENPD_Numero) AS DESE                                            
   ON DESV.EMPR_Codigo = DESE.EMPR_Codigo                                                                   
   AND DESV.ENPD_Numero = DESE.ENPD_Numero                                                                   
   AND DESV.Orden > (DESE.Orden - ISNULL(@par_Mostrar, DESE.Orden))                                                                       
                                                                  
   LEFT JOIN Valor_Catalogos AS TOSV                                                                  
   ON DESV.EMPR_Codigo = TOSV.EMPR_Codigo                                                                  
   AND DESV.CATA_TOSV_Codigo  = TOSV.Codigo                                                                  
                                                                  
   LEFT JOIN Valor_Catalogos AS SRSV                                                                  
   ON DESV.EMPR_Codigo = SRSV.EMPR_Codigo                                      
   AND DESV.CATA_SRSV_Codigo  = SRSV.Codigo                                                                  
                                                                  
   LEFT JOIN Valor_Catalogos AS NOSV                                                                  
   ON DESV.EMPR_Codigo = NOSV.EMPR_Codigo                                                                  
   AND DESV.CATA_NOSV_Codigo  = NOSV.Codigo                                                                  
                                                                
   LEFT JOIN Puesto_Controles AS PUCO                                                                  
   ON DESV.EMPR_Codigo = PUCO.EMPR_Codigo                                                                  
   AND DESV.PUCO_Codigo = PUCO.Codigo                                                      
                                                    
   LEFT JOIN Terceros AS COND                                     
   ON (ENPD.EMPR_Codigo = COND.EMPR_Codigo                                                                  
   AND ENPD.TERC_Codigo_Conductor  = COND.Codigo)                                                                   
                                                                    
   LEFT JOIN Terceros AS TENE                                                                  
   ON (ENPD.EMPR_Codigo = TENE.EMPR_Codigo                                                                  
   AND ENPD.TERC_Codigo_Tenedor  = TENE.Codigo)                                                                  
                                                            
   LEFT JOIN Vehiculos AS VEHI                                                                  
   ON (ENPD.EMPR_Codigo = VEHI.EMPR_Codigo                                                          
   AND ENPD.VEHI_Codigo  = VEHI.Codigo)                                                                   
                                              
   LEFT JOIN Terceros AS PGPS                         
   ON VEHI.EMPR_Codigo = PGPS.EMPR_Codigo                                                              
   AND VEHI.TERC_Codigo_Proveedor_GPS  = PGPS.Codigo                                           
                                                                  
   LEFT JOIN Semirremolques AS SEMI                                                        
   ON (ENPD.EMPR_Codigo = SEMI.EMPR_Codigo                                                                  
   AND ENPD.SEMI_Codigo  = SEMI.Codigo)                                                         
                                                       
   LEFT JOIN Rutas AS RUTA                                                                  
   ON (ENPD.EMPR_Codigo = RUTA.EMPR_Codigo                                                                  
   AND ENPD.RUTA_Codigo  = RUTA.Codigo)                                      
                         
   LEFT JOIN Usuarios AS USUA                                                 
   ON (DESV.EMPR_Codigo = USUA.EMPR_Codigo                                                                  
   AND DESV.USUA_Codigo_Crea  = USUA.Codigo)                               
                                      
   --LEFT JOIN Encabezado_Cumplido_Planilla_Despachos AS ECPD                                                                  
   --ON (ENPD.EMPR_Codigo = ECPD.EMPR_Codigo                                                                  
   --AND ENPD.Numero  = ECPD.ENPD_Numero)                                                          
                    
     WHERE                                                                  
   DESV.EMPR_Codigo = @par_EMPR_Codigo                                                    
   AND CONVERT(date, DESV.Fecha_Reporte) BETWEEN ISNULL(@par_Fecha_Inicial, CONVERT(date, DESV.Fecha_Reporte)) AND ISNULL(@par_Fecha_Final, CONVERT(date, DESV.Fecha_Reporte))                
   AND (ENPD.Numero_Documento = @par_ENPD_Numero or @par_ENPD_Numero is null)                                                    
   AND (ENMC.Numero_Documento = @par_Numero_Manifiesto or @par_Numero_Manifiesto is null)                                                     
  -- AND (DESV.PUCO_Codigo = @par_Puesto_Control or @par_Puesto_Control is null)                                                  
   AND (VEHI.Placa = @par_Vehiculo or @par_Vehiculo is null)                                                               
   AND(ISNULL(COND.Nombre, '') + ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') + ' ' + ISNULL(COND.Razon_Social, '')  LIKE                                                     
   CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Conductor, ISNULL(COND.Nombre, '') + ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') + ' ' + ISNULL(COND.Razon_Social, '')))), '%'))                                                       
   AND (RUTA.Codigo = @par_Ruta or @par_Ruta is null)                                                  
   AND (TOSV.Codigo = @par_CATA_TOSV_Codigo or @par_CATA_TOSV_Codigo is null)                                                  
   AND (SRSV.Codigo = @par_CATA_SRSV_Codigo or @par_CATA_SRSV_Codigo is null)                                                    
   AND DESV.Anulado = ISNULL(@par_Anulado, 0)                                                        
AND ENPD.Anulado = 0                                                     
   --AND NOT (ENPD.ECPD_Numero IS NULL OR ENPD.ECPD_Numero = 0)                      
   --AND DESV.Orden > (DESE.Orden - ISNULL(@par_Mostrar, DESE.Orden))                           
   --AND (ENPD.OFIC_Codigo IN (SELECT * FROM dbo.Func_Dividir_String(@par_Codigos_Oficinas,','))   or @par_Codigos_Oficinas is null)                          
   AND ISNULL(DESV.Fin_Viaje, 0) > 0                                   
                               
  AND (ENPD.TERC_Codigo_Conductor = @par_TERC_Codigo_Conductor OR @par_TERC_Codigo_Conductor IS NULL)                            
   AND (DESV.TERC_Cliente = @par_TERC_Codigo_Cliente OR @par_TERC_Codigo_Cliente IS NULL)                            
   --AND (VEHI.CATA_TIDV_Codigo = @par_CATA_TIDU_Codigo OR @par_CATA_TIDU_Codigo IS NULL)                            
   --AND (RUTA.CIUD_Codigo_Origen = @par_CIUD_Codigo_Origen OR @par_CIUD_Codigo_Origen IS NULL)                            
  -- AND (RUTA.CIUD_Codigo_Destino = @par_CIUD_Codigo_Destino OR @par_CIUD_Codigo_Destino IS NULL)                            
   AND (DESV.PRTR_Codigo = @par_PRTR_Codigo OR @par_PRTR_Codigo IS NULL)                 
   AND ENPD.Fecha_Hora_Salida <= CASE WHEN @par_EMPR_Codigo = 2 THEN GETDATE() ELSE ENPD.Fecha_Hora_Salida END        
   AND (DESV.Reportar_Cliente = @par_Reportar_Cliente OR @par_Reportar_Cliente IS NULL)    
        );                                                                               
    WITH Pagina AS                                                                        
    (                                                                        
                                                                  
 SELECT                                                                  
  DESV.EMPR_Codigo,                                                                    
  DESV.ENPD_Numero,                                                    
  DESV.ENOC_Numero,                     
  ISNULL(ENMC.Numero,0) as ENMC_Numero,                                     
  ISNULL(ENMC.Numero_Documento,0) as ENMC_Numero_Documento,                                                                  
  DESV.ID,                                                         
  DESV.Fecha_Reporte,                                                          
  DESV.Ubicacion,                                                                  
  DESV.Longitud,                                                                  
  DESV.Latitud,                                                           
  DESV.PUCO_Codigo,                                            
  ISNULL(DESV.CATA_TOSV_Codigo, 0) as CATA_TOSV_Codigo,                                                                
  DESV.CATA_SRSV_Codigo,                                                                  
  DESV.CATA_NOSV_Codigo,                                           
  DESV.Observaciones,                                                                  
  DESV.Kilometros_Vehiculo,                                                                  
  DESV.Reportar_Cliente,                                        
  ISNULL(DESV.Prioritario,0) AS Prioritario,                          
  DESV.Envio_Reporte_Cliente,                                                                  
  DESV.Fecha_Reporte_Cliente,                                                    
  DESV.Anulado,                                                                               
  ENPD.TERC_Codigo_Conductor,                                                      
ENPD.TERC_Codigo_Tenedor,                                                                   
  ENPD.VEHI_Codigo,                                                                  
  ISNULL(ENPD.SEMI_Codigo, 0) AS SEMI_Codigo,                                                                  
  ENPD.RUTA_Codigo,                                                                                       
  ISNULL(DESV.Fecha_Crea,'')AS Fecha_Ultimo_Reporte,                                                                        
  ISNULL(COND.Razon_Social, '')+ISNULL(COND.Nombre, '') + ' '+ ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') + ' (' + ISNULL(COND.Celulares, '') + ')' AS Nombre_Conductor,                                                           
  ISNULL(TENE.Razon_Social, '')+ISNULL(TENE.Nombre, '') + ' '+ ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2, '')  AS Nombre_Tenedor,                                             
  CASE WHEN ISNULL(VEHI.TERC_Codigo_Proveedor_GPS, 0) > 0 THEN                                        
  ISNULL(PGPS.Razon_Social, '')+ISNULL(PGPS.Nombre, '') + ' '+ ISNULL(PGPS.Apellido1,'') + ' ' + ISNULL(PGPS.Apellido2, '')                                        
  ELSE '' END AS Nombre_Proveedor_GPS,                                               
  ISNULL(VEHI.Placa, '') AS Vehiculo,                                                                    
  ISNULL(SEMI.Placa, '') AS Semirremolque,                                                      
  ISNULL(RUTA.Nombre, '') AS Ruta,                                                     
  ISNULL(PUCO.Nombre, ' ') AS Puesto_Control,                                                                
  ISNULL(TOSV.Campo1, ' ') AS Tipo_Origen_Seguimiento,                                          
  ISNULL(SRSV.Campo1, ' ') AS Sitio_Reporte_Seguimiento,                                                                  
  ISNULL(NOSV.Campo1, ' ') AS Novedad_Seguimiento,                                                                      
  DESE.Orden,                                                        
  ENPD.Numero_Documento AS Numero_Documento_Planilla,                                                       
  0 AS Numero_Documento_Orden,                                                     
  ISNULL(        
        
  CASE WHEN (CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, ENPD.Ultimo_Seguimiento_Vehicular, GETDATE()))) < @TIEMPO1 THEN 1                                                      
  WHEN (CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, ENPD.Ultimo_Seguimiento_Vehicular, GETDATE()))) between (@TIEMPO1 -1) and @TIEMPO2 THEN 2         
  WHEN (CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, ENPD.Ultimo_Seguimiento_Vehicular, GETDATE()))) between (@TIEMPO2 -1) and @TIEMPO3 THEN 3        
  WHEN (CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, ENPD.Ultimo_Seguimiento_Vehicular, GETDATE()))) between (@TIEMPO3 -1) and @TIEMPO4 THEN 4         
  WHEN (CONVERT(NUMERIC(18,2), DATEDIFF(MINUTE, ENPD.Ultimo_Seguimiento_Vehicular, GETDATE()))) >= @TIEMPO4 THEN 5 END        
          
  , 1) AS Desfase_Seguimiento,                          
  CIOR.Nombre AS CiudadOrigen,                        
  CIDE.Nombre AS CiudadDestino,                        
  TIDV.Campo1 AS TipoDueno,                        
  CLIE.Codigo AS CodigoCliente,                        
  ISNULL(CLIE.Razon_Social, '')+ISNULL(CLIE.Nombre, '') + ' '+ ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '')  AS Cliente,                           
  PRTR.Nombre as Producto,                        
  USUA.Nombre AS UsuarioCrea,                      
  ROW_NUMBER() OVER(ORDER BY ISNULL(DESV.Prioritario,0) DESC, DESV.Fecha_Reporte ASC) AS RowNumber                                                                        
           FROM                                                                        
   Detalle_Seguimiento_Vehiculos DESV                                                              
                                                                
   INNER JOIN Encabezado_Planilla_Despachos AS ENPD                                                                
   ON DESV.EMPR_Codigo = ENPD.EMPR_Codigo                                                                  
   AND DESV.ENPD_Numero = ENPD.Numero                                         
                           
                        
   LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC                                                                
   ON ENPD.EMPR_Codigo = ENMC.EMPR_Codigo                                                                  
   AND ENPD.Numero = ENMC.ENPD_Numero                                                                                  
                                              
   INNER JOIN (SELECT EMPR_Codigo, ISNULL(MAX(Orden),0) AS Orden, ENPD_Numero FROM Detalle_Seguimiento_Vehiculos GROUP BY EMPR_Codigo, ENPD_Numero ) AS DESE                                                                  
   ON DESV.EMPR_Codigo = DESE.EMPR_Codigo                                                                   
   AND DESV.ENPD_Numero = DESE.ENPD_Numero                                                                   
   AND DESV.Orden > (DESE.Orden - ISNULL(@par_Mostrar, DESE.Orden))                                                         
                                                         
   LEFT JOIN Valor_Catalogos AS TOSV                                                                  
   ON DESV.EMPR_Codigo = TOSV.EMPR_Codigo                                                                  
   AND DESV.CATA_TOSV_Codigo  = TOSV.Codigo                                                                  
                                                                  
   LEFT JOIN Valor_Catalogos AS SRSV                                                                  
   ON DESV.EMPR_Codigo = SRSV.EMPR_Codigo                                                                  
   AND DESV.CATA_SRSV_Codigo  = SRSV.Codigo                                                                  
                                   
  LEFT JOIN Valor_Catalogos AS NOSV                                                                  
   ON DESV.EMPR_Codigo = NOSV.EMPR_Codigo                                                                  
   AND DESV.CATA_NOSV_Codigo  = NOSV.Codigo                                                       
                                                       
   LEFT JOIN Puesto_Controles AS PUCO             
   ON DESV.EMPR_Codigo = PUCO.EMPR_Codigo                                                                  
   AND DESV.PUCO_Codigo = PUCO.Codigo                                                                
                                                                
   LEFT JOIN Terceros AS COND                                               
   ON (ENPD.EMPR_Codigo = COND.EMPR_Codigo                                                                  
   AND ENPD.TERC_Codigo_Conductor  = COND.Codigo)                                                                 
                                                                    
   LEFT JOIN Terceros AS TENE                                                                  
   ON (ENPD.EMPR_Codigo = TENE.EMPR_Codigo                                                                  
   AND ENPD.TERC_Codigo_Tenedor  = TENE.Codigo)                                                                  
                                                                  
   LEFT JOIN Vehiculos AS VEHI                                                                  
   ON (ENPD.EMPR_Codigo = VEHI.EMPR_Codigo                                                                  
   AND ENPD.VEHI_Codigo  = VEHI.Codigo)                                                                   
                           
                           
   LEFT JOIN Terceros AS PGPS                                                              
   ON VEHI.EMPR_Codigo = PGPS.EMPR_Codigo                                                              
   AND VEHI.TERC_Codigo_Proveedor_GPS  = PGPS.Codigo                                           
                                                                 
   LEFT JOIN Semirremolques AS SEMI                                                                  
   ON (ENPD.EMPR_Codigo = SEMI.EMPR_Codigo                                                                  
   AND ENPD.SEMI_Codigo  = SEMI.Codigo)                                                         
                                                       
   LEFT JOIN Rutas AS RUTA                                                                  
   ON (ENPD.EMPR_Codigo = RUTA.EMPR_Codigo                                                                  
   AND ENPD.RUTA_Codigo  = RUTA.Codigo)               
                        
   --LEFT JOIN Encabezado_Cumplido_Planilla_Despachos AS ECPD                                                                  
   --ON (ENPD.EMPR_Codigo = ECPD.EMPR_Codigo                                                                  
   --AND ENPD.Numero  = ECPD.ENPD_Numero)                                                                
               LEFT JOIN Producto_Transportados as PRTR                        
   ON DESV.EMPR_Codigo = PRTR.EMPR_Codigo                                                                  
   AND DESV.PRTR_Codigo = PRTR.Codigo                          
                      
      LEFT JOIN Ciudades AS CIOR                        
   ON  (RUTA.EMPR_Codigo = CIOR.EMPR_Codigo                                                                  
   AND RUTA.CIUD_Codigo_Origen  = CIOR.Codigo)                         
                        
   LEFT JOIN Ciudades AS CIDE                        
   ON  (RUTA.EMPR_Codigo = CIDE.EMPR_Codigo                                                                  
   AND RUTA.CIUD_Codigo_Destino  = CIDE.Codigo)                             
                        
   LEFT JOIN Terceros AS CLIE                        
   ON DESV.EMPR_Codigo = CLIE.EMPR_Codigo                                                                  
   AND DESV.TERC_Cliente = CLIE.Codigo                          
                           
                           
LEFT JOIN Valor_Catalogos AS TIDV                                                                  
   ON VEHI.EMPR_Codigo = TIDV.EMPR_Codigo                                                                  
   AND VEHI.CATA_TIDV_Codigo  = TIDV.Codigo                           
                        
    LEFT JOIN Usuarios AS USUA                                                                  
   ON (DESV.EMPR_Codigo = USUA.EMPR_Codigo                                                                  
   AND DESV.USUA_Codigo_Crea  = USUA.Codigo)                        
                      
     WHERE                                       
   DESV.EMPR_Codigo = @par_EMPR_Codigo                                                     
   AND CONVERT(date, DESV.Fecha_Reporte) BETWEEN ISNULL(@par_Fecha_Inicial, CONVERT(date, DESV.Fecha_Reporte)) AND ISNULL(@par_Fecha_Final, CONVERT(date, DESV.Fecha_Reporte))                
   AND (ENPD.Numero_Documento = @par_ENPD_Numero or @par_ENPD_Numero is null)                                                    
   AND (ENMC.Numero_Documento = @par_Numero_Manifiesto or @par_Numero_Manifiesto is null)                                                     
  -- AND (DESV.PUCO_Codigo = @par_Puesto_Control or @par_Puesto_Control is null)                                                  
   AND (VEHI.Placa = @par_Vehiculo or @par_Vehiculo is null)                    
   AND(ISNULL(COND.Nombre, '') + ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') + ' ' + ISNULL(COND.Razon_Social, '')  LIKE                                                     
   CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Conductor, ISNULL(COND.Nombre, '') + ' ' + ISNULL(COND.Apellido1, '') + ' ' + ISNULL(COND.Apellido2, '') + ' ' + ISNULL(COND.Razon_Social, '')))), '%'))                                                           
   AND (RUTA.Codigo = @par_Ruta or @par_Ruta is null)                                               
   AND (TOSV.Codigo = @par_CATA_TOSV_Codigo or @par_CATA_TOSV_Codigo is null)                                                  
AND (SRSV.Codigo = @par_CATA_SRSV_Codigo or @par_CATA_SRSV_Codigo is null)                                                    
   AND DESV.Anulado = ISNULL(@par_Anulado, 0)                                                        
   AND ENPD.Anulado = 0                                               
   --AND NOT (ENPD.ECPD_Numero IS NULL OR ENPD.ECPD_Numero = 0)                      
   --AND DESV.Orden > (DESE.Orden - ISNULL(@par_Mostrar, DESE.Orden))                                                              
   --AND (ENPD.OFIC_Codigo IN (SELECT * FROM dbo.Func_Dividir_String(@par_Codigos_Oficinas,','))   or @par_Codigos_Oficinas is null)                          
   AND ISNULL(DESV.Fin_Viaje, 0) > 0                                          
                               
   AND (ENPD.TERC_Codigo_Conductor = @par_TERC_Codigo_Conductor OR @par_TERC_Codigo_Conductor IS NULL)                            
   AND (DESV.TERC_Cliente = @par_TERC_Codigo_Cliente OR @par_TERC_Codigo_Cliente IS NULL)                            
  -- AND (VEHI.CATA_TIDV_Codigo = @par_CATA_TIDU_Codigo OR @par_CATA_TIDU_Codigo IS NULL)                            
  -- AND (RUTA.CIUD_Codigo_Origen = @par_CIUD_Codigo_Origen OR @par_CIUD_Codigo_Origen IS NULL)                            
  -- AND (RUTA.CIUD_Codigo_Destino = @par_CIUD_Codigo_Destino OR @par_CIUD_Codigo_Destino IS NULL)                            
   AND (DESV.PRTR_Codigo = @par_PRTR_Codigo OR @par_PRTR_Codigo IS NULL)                  
   AND ENPD.Fecha_Hora_Salida <= CASE WHEN @par_EMPR_Codigo = 2 THEN GETDATE() ELSE ENPD.Fecha_Hora_Salida END        
   AND (DESV.Reportar_Cliente = @par_Reportar_Cliente OR @par_Reportar_Cliente IS NULL)    
   )                                                    
  SELECT                                                                         
  0 As Obtener,                                                    
  1 AS Tipo_Consulta,                                                       
  EMPR_Codigo,                                                                
  ENPD_Numero,                                                     
  ENOC_Numero,                                    
  ENMC_Numero,                                     
  ENMC_Numero_Documento,                                                                  
  ID,                                                                  
  Fecha_Reporte,                             
  Ubicacion,                                                                  
  Longitud,                                                                  
  Latitud,                                                                  
  PUCO_Codigo,                                                           
  CATA_TOSV_Codigo,                                                             
  CATA_SRSV_Codigo,                                                                  
  CATA_NOSV_Codigo,                                           
  Observaciones,                                                           
  Kilometros_Vehiculo,                                                                  
  Reportar_Cliente,               
  Prioritario,                                                                
  Envio_Reporte_Cliente,                                                                  
  Fecha_Reporte_Cliente,                                                    
  Anulado,                                                                               
  TERC_Codigo_Conductor,                                                                  
  TERC_Codigo_Tenedor,                                                                   
  VEHI_Codigo,                                                                  
  SEMI_Codigo,                                                                  
  RUTA_Codigo,                                                                   
  Fecha_Ultimo_Reporte,                                                                        
  Nombre_Conductor,                                                                  
  Nombre_Tenedor,                                          
  Nombre_Proveedor_GPS,                                                                   
  Vehiculo,                                                                    
  Semirremolque,                                                      
  Ruta,                                                     
  Puesto_Control,                                                                
  Tipo_Origen_Seguimiento,                                                                  
  Sitio_Reporte_Seguimiento,                                            
  Novedad_Seguimiento,                                                         
  Orden,                                                       
  Numero_Documento_Planilla,                                                     
  Numero_Documento_Orden,                              
  CiudadOrigen,                        
  CiudadDestino,                        
  TipoDueno,                        
  Cliente,                        
  Producto,                        
  UsuarioCrea,                    
  CodigoCliente,              
  ISNULL(Desfase_Seguimiento, 1) AS Desfase_Seguimiento,                           
  @CantidadRegistros AS TotalRegistros,                                                                        
  @par_NumeroPagina AS PaginaObtener,                        
  @par_RegistrosPagina AS RegistrosPagina                                                                  
   FROM                                                                       
    Pagina                                                                    
   WHERE                                                         
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                                      
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                                      
 ORDER BY Prioritario DESC ,Fecha_Reporte ASC                  
END     
GO                          