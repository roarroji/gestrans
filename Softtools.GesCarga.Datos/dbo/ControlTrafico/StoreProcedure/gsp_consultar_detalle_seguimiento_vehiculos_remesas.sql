﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 30/03/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_consultar_detalle_seguimiento_vehiculos_remesas'
GO
DROP PROCEDURE gsp_consultar_detalle_seguimiento_vehiculos_remesas
GO
CREATE PROCEDURE gsp_consultar_detalle_seguimiento_vehiculos_remesas   
(                            
@par_EMPR_Codigo SMALLINT,                            
@par_ENPD_Numero NUMERIC,            
@par_Orden NUMERIC = NULL                             
)                            
AS                            
BEGIN                                                  
        
     
IF ISNULL(@par_Orden, 0) > 0    
                     
 BEGIN           
 SELECT   
 DESR.id,
  3 AS Tipo_Consulta,                                                            
  ENRE.Numero AS ENRE_Numero,           
  ENRE.Numero_Documento AS Numero_Documento_Remesa,                                
  ISNULL(CLIE.Razon_Social, '')+ISNULL(CLIE.Nombre, '') + ' '+ ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2, '')  AS Nombre_Cliente,     
  ENRE.Cantidad_Cliente,  
  ENRE.Peso_Cliente,   
  ISNULL(DESR.CATA_SRSV_Codigo, 8200) AS CATA_SRSV_Codigo,    
  ISNULL(DESR.CATA_NOSV_Codigo, 8100) AS CATA_NOSV_Codigo,   
  DESR.Reportar_Cliente                                  
                                        
   FROM   
                                      
   Encabezado_Planilla_Despachos AS ENPD               
     
   LEFT JOIN Detalle_Planilla_Despachos AS DEPD                          
   ON ENPD.EMPR_Codigo = DEPD.EMPR_Codigo                            
   AND ENPD.Numero = DEPD.ENPD_Numero   
  
   LEFT JOIN Encabezado_Remesas AS ENRE                          
   ON ENRE.EMPR_Codigo = DEPD.EMPR_Codigo                   
   AND ENRE.Numero = DEPD.ENRE_Numero      
     
   LEFT JOIN Detalle_Seguimiento_Remesas AS DESR                          
   ON ENRE.EMPR_Codigo = DESR.EMPR_Codigo                            
   AND ENRE.Numero = DESR.ENRE_Numero  
   AND ENRE.ENPD_Numero = DESR.ENPD_Numero           
  
   LEFT JOIN Terceros AS CLIE                            
   ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                            
   AND ENRE.TERC_Codigo_Cliente  = CLIE.Codigo                                           
                                            
   WHERE                            
   ENPD.EMPR_Codigo = @par_EMPR_Codigo           
   AND (ENPD.Numero = @par_ENPD_Numero OR @par_ENPD_Numero IS NULL)     
   AND (DESR.Orden = @par_Orden OR @par_Orden IS NULL)         
   ORDER BY ENRE.Numero DESC     
   END          
                   
               ELSE  
  
  BEGIN  
  SELECT     
  3 AS Tipo_Consulta,                                                            
  ENRE.Numero AS ENRE_Numero,           
  ENRE.Numero_Documento AS Numero_Documento_Remesa,                                
  ISNULL(CLIE.Razon_Social, '')+ISNULL(CLIE.Nombre, '') + ' '+ ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2, '')  AS Nombre_Cliente,     
  ENRE.Cantidad_Cliente,  
  ENRE.Peso_Cliente,    
  8200 AS CATA_SRSV_Codigo,    
  8100 AS CATA_NOSV_Codigo,   
  0 AS Reportar_Cliente                                  
                                        
   FROM   
                                      
   Encabezado_Planilla_Despachos AS ENPD               
     
   LEFT JOIN Detalle_Planilla_Despachos AS DEPD                          
   ON ENPD.EMPR_Codigo = DEPD.EMPR_Codigo                            
   AND ENPD.Numero = DEPD.ENPD_Numero   
  
   LEFT JOIN Encabezado_Remesas AS ENRE                          
   ON DEPD.EMPR_Codigo = ENRE.EMPR_Codigo                            
   AND DEPD.ENRE_Numero = ENRE.Numero         
  
   LEFT JOIN Terceros AS CLIE                            
   ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                            
   AND ENRE.TERC_Codigo_Cliente  = CLIE.Codigo                                           
                                            
   WHERE                            
   ENPD.EMPR_Codigo = @par_EMPR_Codigo           
   AND (ENPD.Numero = @par_ENPD_Numero OR @par_ENPD_Numero IS NULL)   
   ORDER BY ENRE.Numero DESC     
   END       
END      
GO