﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 30/03/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_consultar_novedades_sitio_reporte'
GO
DROP PROCEDURE gsp_consultar_novedades_sitio_reporte
GO
CREATE PROCEDURE gsp_consultar_novedades_sitio_reporte 
(                            
@par_EMPR_Codigo SMALLINT,                            
@par_CATA_SRSV_Codigo NUMERIC
)                            
AS                            
BEGIN                                                  
        
 SELECT
 	  
  5 AS Tipo_Consulta,    
  NOSV.Campo1 AS Novedad_Seguimiento,
  SRNS.CATA_NOSV_Codigo                                
                                        
   FROM   
                                      
   Sitio_Reporte_Novedades_Seguimiento_Vehicular AS SRNS         
  
   LEFT JOIN Valor_Catalogos AS NOSV                            
   ON SRNS.EMPR_Codigo = NOSV.EMPR_Codigo                            
   AND SRNS.CATA_NOSV_Codigo  = NOSV.Codigo                                           
                                            
   WHERE                            
   SRNS.EMPR_Codigo = @par_EMPR_Codigo           
   AND SRNS.CATA_SRSV_Codigo = @par_CATA_SRSV_Codigo
    
   ORDER BY NOSV.Campo1 DESC     
      
END      
GO