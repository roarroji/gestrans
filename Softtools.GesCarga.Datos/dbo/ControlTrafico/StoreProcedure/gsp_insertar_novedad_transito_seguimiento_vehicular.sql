﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 30/03/2019
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_insertar_novedad_transito_seguimiento_vehicular'
GO
DROP PROCEDURE gsp_insertar_novedad_transito_seguimiento_vehicular
GO        
CREATE procedure gsp_insertar_novedad_transito_seguimiento_vehicular 
(
 @par_EMPR_codigo SMALLINT =  null ,        
 @par_ENPD_numero NUMERIC(18,0) = null,        
 @par_ENMC_numero NUMERIC (18,0) = null,        
 @par_CATA_NOSV_Codigo NUMERIC (18,0) = null,        
 @par_CATA_SRSV_Codigo NUMERIC (18,0) = null,        
 @par_Ubicacion varchar(100) = null,   
 @par_Longitud NUMERIC (18,10)  = NULL,              
 @par_Latitud NUMERIC (18,10)  = NULL,       
 @par_Observaciones VARCHAR(200) = null, 
 @par_Aplica_Seguimiento NUMERIC = null,            
 @par_Usuario_Crea NUMERIC (18,0) = null 
 )        
AS         
BEGIN  

DECLARE @sma_Orden SMALLINT = 1     
SELECT  TOP 1  @sma_Orden = ISNULL(Orden,0) + 1 FROM Detalle_Seguimiento_Vehiculos             
WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENPD_Numero = @par_ENPD_Numero  ORDER BY Orden DESC           
        
INSERT INTO Novedades_Transito_Seguimiento_Vehicular
(        
 EMPR_Codigo ,        
 ENPD_Numero ,      
 ENMC_Numero,        
 CATA_NOSV_Codigo ,        
 CATA_SRSV_Codigo ,    
 Ubicacion, 
 Longitud,
 Latitud,   
 Observaciones ,        
 usua_codigo_crea,        
 Fecha_Crea
 )         
 values         
 (
   @par_EMPR_codigo,        
   @par_ENPD_numero,        
   @par_ENMC_numero,        
   @par_CATA_NOSV_Codigo,        
   @par_CATA_SRSV_Codigo,     
   ISNULL(@par_Ubicacion, ''),              
   ISNULL(@par_Longitud, 0),              
   ISNULL(@par_Latitud, 0),       
   ISNULL(@par_Observaciones,''),        
   @par_Usuario_Crea,        
   GETDATE()
 )        
 
SELECT @@IDENTITY as Numero     
  
IF @par_Aplica_Seguimiento = 1
BEGIN
    
INSERT INTO Detalle_Seguimiento_Vehiculos 
(    
 EMPR_Codigo    
,ENPD_Numero    
,ENOC_Numero  
,ENMC_Numero    
,Fecha_Reporte    
,CATA_TOSV_Codigo    
,CATA_SRSV_Codigo    
,CATA_NOSV_Codigo    
,Observaciones
,Ubicacion
,Longitud
,Latitud  
,PUCO_Codigo
,Kilometros_Vehiculo    
,Reportar_Cliente    
,Envio_Reporte_Cliente    
,Fecha_Reporte_Cliente    
,USUA_Codigo_Crea    
,Fecha_Crea    
,Anulado    
,Orden    
)    
VALUES    
(    
 @par_EMPR_codigo       
,@par_ENPD_numero
,0        
,@par_ENMC_numero
,GETDATE()    
,8003 
,@par_CATA_SRSV_Codigo    
,@par_CATA_NOSV_Codigo    
,ISNULL(@par_Observaciones,'')   
,ISNULL(@par_Ubicacion, '')              
,ISNULL(@par_Longitud, 0)             
,ISNULL(@par_Latitud, 0)  
,0
,0    
,0    
,0    
,GETDATE()    
,@par_Usuario_Crea   
,GETDATE()    
,0    
,@sma_Orden   
)    
            
INSERT INTO Detalle_Seguimiento_Remesas 
(              
EMPR_Codigo,              
ENPD_Numero,      
ENRE_Numero,            
Fecha_Reporte,              
CATA_TOSV_Codigo,              
Ubicacion,              
Longitud,              
Latitud,              
PUCO_Codigo,              
CATA_SRSV_Codigo,              
CATA_NOSV_Codigo,              
Observaciones,                     
Reportar_Cliente,              
Envio_Reporte_Cliente,              
Fecha_Reporte_Cliente,              
USUA_Codigo_Crea,              
Fecha_Crea,              
Anulado,              
Orden          
)              
SELECT            
@par_EMPR_Codigo,              
ISNULL(@par_ENPD_Numero, 0),              
Numero,                  
GETDATE(),              
8003,              
ISNULL(@par_Ubicacion, ''),              
ISNULL(@par_Longitud, 0),              
ISNULL(@par_Latitud, 0),              
0,              
ISNULL(@par_CATA_SRSV_Codigo, 8200),              
ISNULL(@par_CATA_NOSV_Codigo, 8100),              
ISNULL(@par_Observaciones,''),                    
0,              
0,              
GETDATE(),                
@par_Usuario_Crea,              
GETDATE(),              
0,              
@sma_Orden              
FROM Encabezado_Remesas 
WHERE EMPR_Codigo = @par_EMPR_codigo 
AND ENPD_Numero = @par_ENPD_numero            

END                 
END    
 
GO 