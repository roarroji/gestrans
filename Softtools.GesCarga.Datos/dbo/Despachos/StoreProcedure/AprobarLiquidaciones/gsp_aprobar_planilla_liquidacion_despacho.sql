﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 01/10/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'gsp_aprobar_planilla_liquidacion_despacho'
GO
DROP PROCEDURE gsp_aprobar_planilla_liquidacion_despacho
GO   
CREATE PROCEDURE gsp_aprobar_planilla_liquidacion_despacho 
(  
 @par_EMPR_Codigo SMALLINT,  
 @par_Numero NUMERIC,  
 @par_USUA_Codigo_Aprueba SMALLINT  
)  
AS  
BEGIN  
 UPDATE  
  Encabezado_Liquidacion_Planilla_Despachos  
 SET  
  Aprobar = 1,
  USUA_Codigo_Aprueba = @par_USUA_Codigo_Aprueba,
  Fecha_Aprueba = GETDATE()
 WHERE  
  EMPR_Codigo = @par_EMPR_Codigo  
  AND Numero = @par_Numero   
  
 SELECT @par_Numero AS Numero  
END  
GO