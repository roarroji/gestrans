﻿	/*Crea:Geferson Latorre
	Fecha_Crea: 30/09/2018
	Descripción_Crea:
	Modifica:
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
PRINT 'sp_consultar_detalle_comprobante_contables'
GO
DROP PROCEDURE sp_consultar_detalle_comprobante_contables
GO  
CREATE PROCEDURE sp_consultar_detalle_comprobante_contables 
(  
 @par_EMPR_Codigo SMALLINT,  
 @par_ENCC_Numero NUMERIC = NULL    
)  
AS   
BEGIN  
  
SELECT DECC.ID  
      ,DECC.PLUC_Codigo
      ,PLUC.Codigo_Cuenta As NumeroCuenta  
      ,DECC.ENCC_Numero 
      ,DECC.Valor_Debito  
      ,DECC.Valor_Credito  
      ,DECC.Valor_Base  
      ,DECC.Observaciones  
      ,DECC.Documento_Cruce  
      ,DECC.Documento_Cruce_Alterno    
      ,DECC.TERC_Codigo  
      ,(TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 + ' ' + TERC.Razon_Social)  As NombreTercero  
      ,DECC.Centro_Costo  
      ,DECC.Prefijo  
      ,DECC.Codigo_Anexo  
      ,DECC.Sufijo_Codigo_Anexo  
      ,DECC.Campo_Auxiliar  
  
  FROM 

 Detalle_Comprobante_Contables AS DECC  
  
 LEFT JOIN Plan_Unico_Cuentas AS PLUC  
 ON DECC.EMPR_Codigo = PLUC.EMPR_Codigo  
 AND DECC.PLUC_Codigo = PLUC.Codigo  

 LEFT JOIN Terceros AS TERC  
 ON DECC.EMPR_Codigo = TERC.EMPR_Codigo  
 AND DECC.TERC_Codigo = TERC.Codigo 
  
  WHERE 
 DECC.EMPR_Codigo = @par_EMPR_Codigo
 AND DECC.ENCC_Numero = ISNULL(@par_ENCC_Numero, DECC.ENCC_Numero)   
  
END  
GO
