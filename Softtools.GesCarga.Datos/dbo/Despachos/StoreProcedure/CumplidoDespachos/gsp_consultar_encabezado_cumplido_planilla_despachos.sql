﻿PRINT 'gsp_consultar_encabezado_cumplido_planilla_despachos'
GO
DROP PROCEDURE gsp_consultar_encabezado_cumplido_planilla_despachos
GO 
CREATE PROCEDURE gsp_consultar_encabezado_cumplido_planilla_despachos
(                        
 @par_EMPR_Codigo SMALLINT,                        
 @par_Numero NUMERIC = NULL,                        
 @par_Fecha_Inicial DATETIME = NULL,                        
 @par_Fecha_Final DATETIME = NULL,                        
 @par_ENPD_Numero NUMERIC = NULL,                        
 @par_ENMC_Numero NUMERIC = NULL,                        
 @par_VEHI_Placa VARCHAR(20) = NULL,                        
 @par_TENE_Codigo NUMERIC = NULL,                        
 @par_COND_Codigo NUMERIC = NULL,                      
 @par_Conductor VARCHAR(50)  = NULL,                               
 @par_Tenedor VARCHAR(50)  = NULL,                   
 @par_Estado INT = NULL,                        
 @par_Anulado SMALLINT = NULL ,                                  
 @par_OFIC_Codigo INT = NULL,                        
 @par_NumeroPagina INT = NULL,                        
 @par_RegistrosPagina INT = NULL                        
 ,@par_Usuario_Consulta INT  = NULL                                   
)                        
AS                        
BEGIN                        
                      
 SET @par_Fecha_Inicial = CONVERT(DATE,@par_Fecha_Inicial,101)                            
 SET @par_Fecha_Final = DATEADD(HOUR , 23 , @par_Fecha_Final)                            
 SET @par_Fecha_Final = DATEADD(MINUTE , 59 , @par_Fecha_Final)                            
 SET @par_Fecha_Final = DATEADD(SECOND , 59 , @par_Fecha_Final)                      
                      
 SET NOCOUNT ON;                         
 DECLARE @CantidadRegistros INT                        
 SELECT @CantidadRegistros =                        
 (                        
 SELECT DISTINCT                        
 COUNT(1)                        
 FROM                        
 Encabezado_Cumplido_Planilla_Despachos ECPD                        
                        
 LEFT JOIN Encabezado_Planilla_Despachos ENPD                        
 ON ECPD.EMPR_Codigo = ENPD.EMPR_Codigo AND ECPD.ENPD_Numero = ENPD.Numero                        
                        
 LEFT JOIN Encabezado_Manifiesto_Carga ENMC                        
 ON ECPD.EMPR_Codigo = ENMC.EMPR_Codigo AND ECPD.ENMC_Numero = ENMC.Numero                        
                           
 LEFT JOIN Vehiculos VEHI                           
 ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo AND ENPD.VEHI_Codigo = VEHI.Codigo                        
                        
 LEFT JOIN Terceros TENE ON                        
 ENPD.EMPR_Codigo = TENE.EMPR_Codigo AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo                        
                        
 LEFT JOIN Terceros COND ON                        
 ENPD.EMPR_Codigo = COND.EMPR_Codigo AND ENPD.TERC_Codigo_Conductor = COND.Codigo                        
                        
 LEFT JOIN Oficinas OFIC ON                        
 ECPD.EMPR_Codigo = OFIC.EMPR_Codigo AND ECPD.OFIC_Codigo = OFIC.Codigo                        
                        
 LEFT JOIN Rutas RUTA                        
 ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo AND ENPD.RUTA_Codigo = RUTA.Codigo             
           
 LEFT JOIN (SELECT ENPD_Numero,COUNT(*) AS Cantidad FROM Detalle_Novedades_Despacho WHERE EMPR_Codigo = @par_EMPR_Codigo GROUP BY ENPD_Numero) AS NVDE                
 ON NVDE.ENPD_Numero = ENPD.Numero                          
 WHERE                        
 ECPD.EMPR_Codigo = @par_EMPR_Codigo                        
 AND ECPD.Numero_Documento = ISNULL(@par_Numero,ECPD.Numero_Documento)                        
 AND ECPD.Fecha >= ISNULL(@par_Fecha_Inicial, ECPD.Fecha)                            
 AND ECPD.Fecha <= ISNULL(@par_Fecha_Final, ECPD.Fecha)                         
 AND ECPD.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ECPD.OFIC_Codigo)     
  AND (ECPD.OFIC_Codigo IN (                
   SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE EMPR_Codigo = @par_EMPR_Codigo AND USOF.USUA_Codigo = @par_Usuario_Consulta                
  ) OR @par_Usuario_Consulta IS NULL)    
 AND ECPD.Estado=ISNULL(@par_Estado,ECPD.Estado)                                  
 AND ECPD.Anulado = ISNULL (@par_Anulado,ECPD.Anulado )                         
 AND ENPD.Numero_Documento = ISNULL(@par_ENPD_Numero,ENPD.Numero_Documento)                        
 AND( ENMC.Numero_Documento = ISNULL(@par_ENMC_Numero,ENMC.Numero_Documento)   OR @par_ENMC_Numero IS NULL )                    
 AND VEHI.Placa = ISNULL(@par_VEHI_Placa, VEHI.Placa)                        
 AND TENE.Codigo = ISNULL(@par_TENE_Codigo,TENE.Codigo)         
 AND COND.Codigo = ISNULL(@par_COND_Codigo,COND.Codigo)                          
 AND ((COND.Nombre + ' ' + COND.Apellido1 + ' ' + COND.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Conductor)) + '%' OR COND.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Conductor)) + '%') OR (@par_Conductor IS NULL))                    
 AND ((TENE.Nombre + ' ' + TENE.Apellido1 + ' ' + TENE.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Tenedor)) + '%' OR TENE.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Tenedor)) + '%') OR (@par_Tenedor IS NULL))                    
 );                        
 WITH Pagina                        
 AS                        
 (SELECT                        
 ECPD.EMPR_Codigo,                        
 ECPD.Numero,                        
 ECPD.TIDO_Codigo,                        
 ECPD.Numero_Documento,                        
 ECPD.Fecha,                        
 ECPD.ENPD_Numero,                        
 ISNULL(ENPD.Numero_Documento,0) AS NumeroPlanilla,                        
 ECPD.ENMC_Numero,                        
 ISNULL(ENMC.Numero_Documento,0) AS NumeroManifiesto,                        
 ECPD.Estado,                        
 ECPD.Anulado,                        
 ENPD.TERC_Codigo_Tenedor,                        
 ISNULL(CONCAT(TENE.Razon_Social, ' ', TENE.Nombre,' ',TENE.Apellido1,' ',TENE.Apellido2),'') AS NombreTenedor,                        
 ISNULL(ENPD.TERC_Codigo_Conductor,0) AS TERC_Codigo_Conductor,                        
 ISNULL(CONCAT(COND.Razon_Social,' ', COND.Nombre,' ',COND.Apellido1,' ',COND.Apellido2),'') AS NombreConductor,                        
 ECPD.OFIC_Codigo,                        
 OFIC.Nombre AS NombreOficina,                        
 ISNULL(VEHI.Placa,'') AS PlacaVehiculo,                        
 ISNULL(RUTA.Nombre,'') AS NombreRuta,                        
 NVDE.Cantidad AS Novedades,            
 ROW_NUMBER() OVER (ORDER BY ECPD.Numero DESC) AS RowNumber                        
 FROM                        
 Encabezado_Cumplido_Planilla_Despachos ECPD                        
                        
 LEFT JOIN Encabezado_Planilla_Despachos ENPD                        
 ON ECPD.EMPR_Codigo = ENPD.EMPR_Codigo AND ECPD.ENPD_Numero = ENPD.Numero                        
                        
  LEFT JOIN Encabezado_Manifiesto_Carga ENMC                        
 ON ECPD.EMPR_Codigo = ENMC.EMPR_Codigo AND ECPD.ENMC_Numero = ENMC.Numero                        
                           
 LEFT JOIN Vehiculos VEHI                           
 ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo AND ENPD.VEHI_Codigo = VEHI.Codigo                        
                        
 LEFT JOIN Terceros TENE ON                        
 ENPD.EMPR_Codigo = TENE.EMPR_Codigo AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo                        
                        
 LEFT JOIN Terceros COND ON                        
 ENPD.EMPR_Codigo = COND.EMPR_Codigo AND ENPD.TERC_Codigo_Conductor = COND.Codigo                        
                        
 LEFT JOIN Oficinas OFIC ON                        
 ECPD.EMPR_Codigo = OFIC.EMPR_Codigo AND ECPD.OFIC_Codigo = OFIC.Codigo                        
                        
 LEFT JOIN Rutas RUTA                        
 ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo AND ENPD.RUTA_Codigo = RUTA.Codigo            
           
 LEFT JOIN (SELECT ENPD_Numero,COUNT(*) AS Cantidad FROM Detalle_Novedades_Despacho WHERE EMPR_Codigo = @par_EMPR_Codigo GROUP BY ENPD_Numero) AS NVDE                
 ON NVDE.ENPD_Numero = ENPD.Numero                         
 WHERE                        
 ECPD.EMPR_Codigo = @par_EMPR_Codigo                        
 AND ECPD.Numero_Documento = ISNULL(@par_Numero,ECPD.Numero_Documento)                        
 AND ECPD.Fecha >= ISNULL(@par_Fecha_Inicial, ECPD.Fecha)                            
 AND ECPD.Fecha <= ISNULL(@par_Fecha_Final, ECPD.Fecha)                         
 AND ECPD.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ECPD.OFIC_Codigo)        
 AND (ECPD.OFIC_Codigo IN (                
   SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE EMPR_Codigo = @par_EMPR_Codigo AND USOF.USUA_Codigo = @par_Usuario_Consulta                
 ) OR @par_Usuario_Consulta IS NULL)    
    
 AND ECPD.Estado = ISNULL(@par_Estado, ECPD.Estado)                  
 AND ECPD.Anulado = ISNULL (@par_Anulado,ECPD.Anulado )                 
 AND ENPD.Numero_Documento = ISNULL(@par_ENPD_Numero,ENPD.Numero_Documento)                        
 AND( ENMC.Numero_Documento = ISNULL(@par_ENMC_Numero,ENMC.Numero_Documento)   OR @par_ENMC_Numero IS NULL )                    
 AND VEHI.Placa = ISNULL(@par_VEHI_Placa, VEHI.Placa)                        
 AND TENE.Codigo = ISNULL(@par_TENE_Codigo,TENE.Codigo)                        
 AND COND.Codigo = ISNULL(@par_COND_Codigo,COND.Codigo)                  
 AND ((COND.Nombre + ' ' + COND.Apellido1 + ' ' + COND.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Conductor)) + '%' OR COND.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Conductor)) + '%') OR (@par_Conductor IS NULL))                    
 AND ((TENE.Nombre + ' ' + TENE.Apellido1 + ' ' + TENE.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Tenedor)) + '%' OR TENE.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Tenedor)) + '%') OR (@par_Tenedor IS NULL))                    
 )                        
 SELECT DISTINCT            
 0 AS ConsultaTiempos,                      
 0 AS Obtener,                        
 EMPR_Codigo,                        
 Numero,                        
 TIDO_Codigo,                        
 Numero_Documento,                        
 Fecha,                        
 ENPD_Numero,                        
 NumeroPlanilla,                        
 ENMC_Numero,                        
 NumeroManifiesto,                        
 Estado,                        
 Anulado,                        
 TERC_Codigo_Tenedor,                        
 NombreTenedor,                        
 TERC_Codigo_Conductor,                        
 NombreConductor,                        
 OFIC_Codigo,                        
 NombreOficina,                        
 PlacaVehiculo,                        
 NombreRuta,          
 Novedades,        
 @CantidadRegistros AS TotalRegistros,                        
 @par_NumeroPagina AS PaginaObtener,                        
 @par_RegistrosPagina AS RegistrosPagina                        
 FROM Pagina                        
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                        
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                        
END
GO