﻿PRINT 'gsp_obtener_encabezado_cumplido_planilla_despacho'
GO
DROP PROCEDURE gsp_obtener_encabezado_cumplido_planilla_despacho
GO
CREATE PROCEDURE gsp_obtener_encabezado_cumplido_planilla_despacho
(                  
	@par_EMPR_Codigo NUMERIC,                  
	@par_Numero NUMERIC                  
)                  
AS                  
BEGIN
	SELECT             
	0 AS ConsultaTiempos,                  
	1 AS Obtener,                  
	ECPD.EMPR_Codigo,                  
	ECPD.Numero,                  
	ECPD.TIDO_Codigo,                  
	ECPD.Numero_Documento,                  
	ECPD.Fecha,                  
	ECPD.ENPD_Numero,                  
	ENPD.Numero_Documento AS NumeroPlanilla,                  
	ECPD.ENMC_Numero,                  
	ISNULL(ENMC.Numero_Documento,0) AS NumeroManifiesto,                  
	ENPD.VEHI_Codigo,                  
	VEHI.Placa AS PlacaVehiculo,                  
	ENPD.TERC_Codigo_Tenedor,                  
	CONCAT(TENE.Razon_Social, ' ', TENE.Nombre,' ',TENE.Apellido1,' ',TENE.Apellido2) AS NombreTenedor,                  
	ENPD.TERC_Codigo_Conductor,                  
	CONCAT(COND.Razon_Social, ' ', COND.Nombre,' ',COND.Apellido1,' ',COND.Apellido2) AS NombreConductor,                  
	ECPD.Fecha_Entrega,                  
	ECPD.Fecha_Inicio_Cargue,                  
	ECPD.Fecha_Fin_Cargue,                  
	ECPD.Fecha_Inicio_Descargue,                  
	ECPD.Fecha_Fin_Descargue,                  
	ECPD.Valor_Multa_Extemporaneo,                  
	ECPD.Nombre_Entrego_Cumplido,                  
	ECPD.Observaciones,                  
	ECPD.OFIC_Codigo,                  
	OFIC.Nombre AS NombreOficina,                  
	ECPD.Estado,              
	ECPD.Fecha_Llegada_Cargue,              
	ECPD.Fecha_Llegada_Descargue,        
	ECPD.VEHI_Codigo_Transbordo,        
	ECPD.TERC_Codigo_Tenedor_Transbordo,        
	ECPD.TERC_Codigo_Propietario_Transbordo,        
	ECPD.TERC_Codigo_Condcutor_Transbordo,        
	ECPD.SEMI_Codigo_Transbordo,        
	ECPD.CATA_TICM_Codigo,        
	ECPD.CATA_MOSM_Codigo,        
	ECPD.CATA_COSM_Codigo,        
	SEMI.Placa as PlacaSemirremolque,      
	ENMCt.Numero_Documento as ManifiestoTransbordo        
	FROM                  
	Encabezado_Cumplido_Planilla_Despachos ECPD        
        
	LEFT JOIN Encabezado_Manifiesto_Carga ENMC                  
	ON ECPD.EMPR_Codigo = ENMC.EMPR_Codigo AND ECPD.ENMC_Numero = ENMC.Numero                  
                    
	LEFT JOIN Encabezado_Planilla_Despachos ENPD
	ON ECPD.EMPR_Codigo = ENPD.EMPR_Codigo AND ECPD.ENPD_Numero = ENPD.Numero
                  
	--LEFT JOIN Terceros TENE
	--ON ENMC.EMPR_Codigo = TENE.EMPR_Codigo AND ENMC.TERC_Codigo_Tenedor = TENE.Codigo
                  
	--LEFT JOIN Terceros COND
	--ON ENMC.EMPR_Codigo = COND.EMPR_Codigo AND ENMC.TERC_Codigo_Conductor = COND.Codigo
                  
	--LEFT JOIN Vehiculos VEHI
	--ON ENMC.EMPR_Codigo = VEHI.EMPR_Codigo AND ENMC.VEHI_Codigo = VEHI.Codigo
        
	--LEFT JOIN Semirremolques SEMI
	--ON ENMC.EMPR_Codigo = SEMI.EMPR_Codigo AND ENMC.SEMI_Codigo = SEMI.Codigo

	LEFT JOIN Terceros TENE
	ON ENPD.EMPR_Codigo = TENE.EMPR_Codigo AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo
                  
	LEFT JOIN Terceros COND
	ON ENPD.EMPR_Codigo = COND.EMPR_Codigo AND ENPD.TERC_Codigo_Conductor = COND.Codigo
                  
	LEFT JOIN Vehiculos VEHI
	ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo AND ENPD.VEHI_Codigo = VEHI.Codigo
        
	LEFT JOIN Semirremolques SEMI
	ON ENPD.EMPR_Codigo = SEMI.EMPR_Codigo AND ENPD.SEMI_Codigo = SEMI.Codigo
         
	LEFT JOIN Oficinas OFIC
	ON ECPD.EMPR_Codigo = OFIC.EMPR_Codigo AND ECPD.OFIC_Codigo = OFIC.Codigo
          
	LEFT JOIN Encabezado_Manifiesto_Carga ENMCt                  
	ON ENMCt.EMPR_Codigo = ENMC.EMPR_Codigo AND ENMCt.ENMC_Numero_Transbordo = ENMC.Numero           
        
	WHERE                  
	ECPD.EMPR_Codigo = @par_EMPR_Codigo                  
	AND ECPD.Numero = @par_Numero                  
END
GO