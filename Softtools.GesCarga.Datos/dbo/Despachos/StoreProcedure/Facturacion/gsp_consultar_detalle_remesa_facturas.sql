﻿  
Print 'gsp_consultar_detalle_remesa_facturas'
GO
DROP PROCEDURE gsp_consultar_detalle_remesa_facturas
GO
CREATE PROCEDURE gsp_consultar_detalle_remesa_facturas (      
	@par_EMPR_Codigo smallint,      
	@par_ENFA_Numero   numeric      
)      
AS      
BEGIN      
        
 SELECT DERF.EMPR_Codigo,        
 DERF.ENFA_Numero,        
 DERF.ENRE_Numero,        
 ISNULL(ENOS.Numero_Documento, 0) AS ENOS_Numero_Documento,    
 ISNULL(ENPD.Numero_Documento, 0) AS ENPD_Numero_Documento,    
 ISNULL(ENMC.Numero_Documento, 0) AS ENMC_Numero_Documento,    
 ENRE.Fecha,        
 ENRE.Numero_Documento As ENRE_Numero_Documento,      
 ISNULL(ENRE.MBL_Contenedor, 'MBL') AS MBL_Contenedor,        
 RUTA.Nombre As NombreRuta,        
 ENRE.Documento_Cliente As Numero_Documento_Cliente,        
 ENRE.Valor_Flete_Cliente,        
 PROD.Nombre As NombreProducto,        
 ENRE.TIDO_Codigo AS ENRE_TIDO_Codigo,
 ENRE.Cantidad_Cliente,       
 ENRE.Peso_Cliente,        
 ENRE.Total_Flete_Cliente,        
   VEHI.Placa,  
  CIOR.Nombre as CiudadRemitente,      
  CIDE.Nombre  as CiudadDestinatario,      
  TIDE.Campo1 AS TipoDespacho,  
  ENOS.CATA_TDOS_Codigo,  
 ISNULL(ENRE.Observaciones,'') As Observaciones        
        
 FROM Detalle_Remesas_Facturas DERF        
        
 INNER JOIN        
 Encabezado_Remesas AS ENRE ON        
 DERF.EMPR_Codigo = ENRE.EMPR_Codigo        
 AND DERF.ENRE_Numero = ENRE.Numero        
        
 INNER JOIN        
 Rutas AS RUTA ON        
 ENRE.EMPR_Codigo = RUTA.EMPR_Codigo        
 AND ENRE.RUTA_Codigo = RUTA.Codigo        
        
 INNER JOIN        
 Producto_Transportados AS PROD ON        
 ENRE.EMPR_Codigo = PROD.EMPR_Codigo        
 AND ENRE.PRTR_Codigo = PROD.Codigo    
     
 LEFT JOIN        
 Encabezado_Planilla_Despachos AS ENPD ON        
 ENRE.EMPR_Codigo = ENPD.EMPR_Codigo        
 AND ENRE.ENPD_Numero = ENPD.Numero        
    
 LEFT JOIN        
 Encabezado_Solicitud_Orden_Servicios AS ENOS ON        
 ENRE.EMPR_Codigo = ENOS.EMPR_Codigo        
 AND ENRE.ESOS_Numero = ENOS.Numero    
    
 LEFT JOIN        
 Encabezado_Manifiesto_Carga AS ENMC ON        
 ENRE.EMPR_Codigo = ENMC.EMPR_Codigo        
 AND ENRE.ENMC_Numero = ENMC.Numero    
  
  LEFT JOIN Vehiculos AS VEHI    
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo    
  AND ENRE.VEHI_Codigo = VEHI.Codigo    
  
  LEFT JOIN Ciudades as CIOR  
  ON CIOR.EMPR_Codigo = RUTA.EMPR_Codigo  
  AND CIOR.Codigo = RUTA.CIUD_Codigo_Origen  
  
  LEFT JOIN Ciudades as CIDE  
  ON CIDE.EMPR_Codigo = RUTA.EMPR_Codigo  
  AND CIDE.Codigo = RUTA.CIUD_Codigo_Destino  
  
  LEFT JOIN Valor_Catalogos AS TIDE  
  ON ENOS.EMPR_Codigo = TIDE.EMPR_Codigo  
  AND ENOS.CATA_TDOS_Codigo = TIDE.Codigo  
        
 WHERE DERF.EMPR_Codigo = @par_EMPR_Codigo        
 AND DERF.ENFA_Numero = @par_ENFA_Numero        
        
END
GO