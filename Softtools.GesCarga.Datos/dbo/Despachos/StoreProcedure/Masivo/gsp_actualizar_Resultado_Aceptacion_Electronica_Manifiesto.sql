﻿DROP PROCEDURE gsp_actualizar_Resultado_Aceptacion_Electronica_Manifiesto
GO
CREATE PROCEDURE gsp_actualizar_Resultado_Aceptacion_Electronica_Manifiesto        
(          
@par_EMPR_Codigo SMALLINT,                  
@par_ENMC_Numero NUMERIC = NULL,                 
@par_Resultado_Aceptacion_Electronica NUMERIC = NULL,                 
@par_Mensaje_Aceptacion_Electronica VARCHAR(MAX) = NULL,          
@par_Fecha_Aceptacion_Electronica VARCHAR(19) = NULL         
)                  
AS                  
BEGIN                  
 UPDATE Encabezado_Manifiesto_Carga SET          
 Numero_Aceptacion_Electronica = @par_Resultado_Aceptacion_Electronica,         
 Mensaje_Aceptacion_Electronica = @par_Mensaje_Aceptacion_Electronica,    
 Fecha_Aceptacion_Electronica =  CONVERT(Datetime, @par_Fecha_Aceptacion_Electronica,103) 
          
 WHERE EMPR_Codigo = @par_EMPR_Codigo          
 AND Numero_Manifiesto_Electronico = @par_ENMC_Numero          
          
 SELECT  Numero_Manifiesto_Electronico FROM Encabezado_Manifiesto_Carga          
 WHERE EMPR_Codigo = @par_EMPR_Codigo          
 AND Numero_Manifiesto_Electronico = @par_ENMC_Numero          
          
END 
GO