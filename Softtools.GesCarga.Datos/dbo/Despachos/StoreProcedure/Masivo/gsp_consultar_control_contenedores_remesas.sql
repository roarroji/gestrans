﻿
CREATE PROCEDURE gsp_consultar_control_contenedores_remesas    
(                                                    
 @par_EMPR_Codigo SMALLINT,                   
 @par_Numero NUMERIC = NULL,                                                    
 @par_Fecha_Inicial DATE = NULL,                                                    
 @par_Fecha_Final DATE = NULL,                                                    
 @par_ESOS_Numero INT = NULL,                 
 @par_Cliente VARCHAR(50) = NULL,                                                    
 @par_Documento_Cliente VARCHAR(30) = NULL,                                                    
 @par_Numero_Contenedor VARCHAR(30) = NULL,                                                    
 @par_Placa VARCHAR(20) = NULL,                 
 @par_OFIC_Codigo INT = NULL,                                                    
 @par_Estado SMALLINT = NULL,                    
 @par_ENPD_Numero_Documento NUMERIC = NULL,                                                   
 @par_ESOS_Numero_Documento NUMERIC = NULL,                                                   
 @par_ENPD_Numero NUMERIC = NULL,      
 @par_NumeroPagina INT = NULL,                                                    
 @par_RegistrosPagina INT = NULL, 
 @par_Anulado SMALLINT = NULL,  
 /* Modificación: AE 27/05/2020*/
 @par_Tarifa_Transporte INT = NULL   

 /*Fin Modificación*/
)                                                    
AS                                                    
BEGIN                                                    
 SET NOCOUNT ON;                                                     
 DECLARE @CantidadRegistros INT                                                    
 SELECT @CantidadRegistros =                                                    
 (                                                    
  SELECT DISTINCT                                                    
  COUNT(1)                                                    
  FROM                                                    
  Encabezado_Remesas ENRE                   
                     
                                         
                                                    
  LEFT JOIN Terceros CLIE                                                    
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                               
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                    
                                            
                                                    
  LEFT JOIN Terceros COND                                       
  ON ENRE.EMPR_Codigo = COND.EMPR_Codigo                               
  AND ENRE.TERC_Codigo_Conductor = COND.Codigo                                 
                                                   
                                                    
  LEFT JOIN Oficinas OFIC                                                    
  ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                               
  AND ENRE.OFIC_Codigo = OFIC.Codigo                                                    
                                            
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS                                                    
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo                               
  AND ENRE.ESOS_Numero = ESOS.Numero                                                    
                                               
                                               
  LEFT JOIN Vehiculos VEHI                                                    
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                               
  AND ENRE.VEHI_Codigo = VEHI.Codigo                                                    

                                                    
  LEFT JOIN Encabezado_Planilla_Despachos ENPD                                                    
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                   
  AND ENRE.ENPD_Numero = ENPD.Numero                   
                                                    
  LEFT JOIN Encabezado_Manifiesto_Carga ENMC                                                    
  ON ENRE.EMPR_Codigo = ENMC.EMPR_Codigo                   
  AND ENRE.ENMC_Numero = ENMC.Numero                                                    
                                                    

  /* Modificación: AE 27/05/2020*/
    LEFT JOIN Ciudades As CDCO                                                    
  ON ENRE.EMPR_Codigo = CDCO.EMPR_Codigo                               
  AND ENRE.CIUD_Codigo_Devolucion_Contenedor = CDCO.Codigo 


  LEFT JOIN Detalle_Novedades_Despacho DEND ON
  ENRE.EMPR_Codigo = DEND.EMPR_Codigo AND
  ENRE.ENPD_Numero = DEND.ENPD_Numero

  LEFT JOIN Detalle_Tiempos_Planilla_Despachos DTPD ON
  ENRE.EMPR_Codigo = DTPD.EMPR_Codigo AND
  ENRE.ENPD_Numero = DTPD.ENPD_Numero AND
  DTPD.CATA_SRSV_Codigo = 8208 --Fin Cargue

  /*Fin Modificación Joins*/
                        
  WHERE                            
  ENRE.EMPR_Codigo = @par_EMPR_Codigo                                                    
  AND ENRE.TIDO_Codigo = 100                                           
  AND ENRE.Anulado = ISNULL (@par_Anulado,ENRE.Anulado)    
  AND ((CLIE.Nombre + ' ' + CLIE.Apellido1 + ' ' + CLIE.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Cliente)) + '%' OR CLIE.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Cliente)) + '%') OR (@par_Cliente IS NULL)) 
  AND ENRE.Numero_Documento = ISNULL(@par_Numero, ENRE.Numero_Documento)                                       
  AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha) 
  AND  ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)                                
  AND ENRE.ESOS_Numero = ISNULL(@par_ESOS_Numero,ENRE.ESOS_Numero)
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                               
  AND ENRE.Estado = 1    
  AND ENRE.Anulado = 0             
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero) 
  AND (ENRE.Documento_Cliente  LIKE'%'+@par_Documento_Cliente+'%' OR @par_Documento_Cliente IS NULL)                      
  AND (ENRE.Numero_Contenedor = @par_Numero_Contenedor or @par_Numero_Contenedor is null)                                
  AND (ESOS.Numero_Documento = @par_ESOS_Numero_Documento OR @par_ESOS_Numero_Documento IS NULL)                                   
  AND (ENPD.Numero_Documento = @par_ENPD_Numero_Documento OR @par_ENPD_Numero_Documento IS NULL)                     
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                                                            
  AND VEHI.Placa = ISNULL(@par_Placa, VEHI.Placa)                              
  AND  CLIE.Numero_Identificacion = ISNULL(@par_Documento_Cliente,CLIE.Numero_Identificacion)               
    

  /* Modificación: AE 27/05/2020*/    
  AND ESOS.LNTC_Codigo = ISNULL(@par_Tarifa_Transporte,ESOS.LNTC_Codigo)  
 
  AND ((ENRE.Fecha_Devolcuion IS NULL AND @par_Estado = 1) 
  OR (ENRE.Fecha_Devolcuion IS NOT NULL AND @par_Estado = 0) 
  OR  @par_Estado IS null)
 );                                                    
 WITH Pagina                                             
 AS                                                    
 (            
  SELECT DISTINCT                               
  ENRE.EMPR_Codigo,                                                    
  ENRE.Numero,                                     
  ENRE.Numero_Documento,   
  ISNULL(ENRE.Documento_Cliente,'') AS Documento_Cliente,                                                
  ENRE.Fecha,                                        
  ENRE.TERC_Codigo_Cliente,                                                    
  CONCAT(ISNULL(CLIE.Razon_Social,''),CLIE.Nombre,' ',CLIE.Apellido1,' ',CLIE.Apellido2) AS NombreCliente,                                                    
  CLIE.Numero_Identificacion AS IdentificacionCliente,                                                       
  ENRE.Observaciones,                                                    
  ENRE.Cantidad_Cliente,                                                    
  ENRE.Peso_Cliente,                                                    
  ENRE.Anulado,                                                    
  ENRE.Estado,                                                    
  ENRE.Fecha_Crea, 
  ENRE.Fecha_Anula,                                                   
  ENRE.OFIC_Codigo,                    
  OFIC.Nombre AS NombreOficina,                                                  
  ENRE.ESOS_Numero,                                                    
  ESOS.Numero_Documento AS OrdenServicio,                                          
  ENRE.ENPD_Numero AS NumeroPlanilla,                                                    
  ISNULL(ENPD.Numero_Documento,0) AS NumeroDocumentoPlanilla,                                                    
  ENRE.ENMC_Numero AS NumeroManifiesto,                                                    
  ISNULL(ENMC.Numero_Documento,0) AS NumeroDocumentoManifiesto,                     
  ENRE.VEHI_Codigo,                                                    
  VEHI.Placa,                                                    
  ENRE.TERC_Codigo_Conductor,                                                    
  CONCAT(COND.Nombre,' ',COND.Apellido1,' ',COND.Apellido2) AS NombreConductor,                                                    
  COND.Numero_Identificacion AS IdentificacionConductor, 

  /* Modificación: AE 27/05/2020*/
  CDCO.Nombre AS NombreCiudadDevolucion,     
  ENRE.Patio_Devolcuion As Patio_Devolcuion,  
  DEND.ID AS Numero_Novedad,   
  ENRE.Fecha_Devolucion_Contenedor AS FechaDevolucion,
  DATEDIFF(DAY,ENRE.Fecha_Devolucion_Contenedor,ENRE.Fecha_Devolcuion) As DiasDemora,
  ENRE.Numero_Contenedor,
  ENRE.MBL_Contenedor,                
 ENRE.HBL_Contenedor,
 ENRE.Fecha_Devolcuion,
 DTPD.Fecha_Hora AS Fecha_Descargue_Mercancia,
 ESOS.Numero AS CodigoOrdenServicio,
  CASE WHEN ENRE.Fecha_Devolcuion > '1970-01-01 00:00:00.000' THEN 'SI' ELSE 'NO' END AS Devuelto,
  /*Fin Modificación*/
 /*Fin Modificación*/
                
  ROW_NUMBER() OVER (ORDER BY ENRE.Numero DESC) AS RowNumber                                                    
  FROM                                                    
  Encabezado_Remesas ENRE

                            
                                                    
  LEFT JOIN Terceros CLIE                                                    
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                               
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                                    
                                                  
                                                    
  LEFT JOIN Terceros COND                                       
  ON ENRE.EMPR_Codigo = COND.EMPR_Codigo                               
  AND ENRE.TERC_Codigo_Conductor = COND.Codigo                                 
                                             
                                                    
  LEFT JOIN Oficinas OFIC                                                    
  ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                               
  AND ENRE.OFIC_Codigo = OFIC.Codigo                                                    
                                            
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS                                                    
  ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo                               
  AND ENRE.ESOS_Numero = ESOS.Numero                                                    

  LEFT JOIN Vehiculos VEHI                                                    
  ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                               
  AND ENRE.VEHI_Codigo = VEHI.Codigo                                                    
                                       
                                                    
  LEFT JOIN Encabezado_Planilla_Despachos ENPD                                                    
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                                   
  AND ENRE.ENPD_Numero = ENPD.Numero                   
                                                    
  LEFT JOIN Encabezado_Manifiesto_Carga ENMC                                                    
  ON ENRE.EMPR_Codigo = ENMC.EMPR_Codigo                   
  AND ENRE.ENMC_Numero = ENMC.Numero                                                    
                                                    

  
  /* Modificación: AE 27/05/2020*/
    LEFT JOIN Ciudades As CDCO                                                    
  ON ENRE.EMPR_Codigo = CDCO.EMPR_Codigo                               
  AND ENRE.CIUD_Codigo_Devolucion_Contenedor = CDCO.Codigo 


  LEFT JOIN Detalle_Novedades_Despacho DEND ON
  ENRE.EMPR_Codigo = DEND.EMPR_Codigo AND
  ENRE.ENPD_Numero = DEND.ENPD_Numero

  

  LEFT JOIN Detalle_Tiempos_Planilla_Despachos DTPD ON
  ENRE.EMPR_Codigo = DTPD.EMPR_Codigo AND
  ENRE.ENPD_Numero = DTPD.ENPD_Numero AND
  DTPD.CATA_SRSV_Codigo = 8208 --Fin Cargue

  /*Fin Modificación Joins*/
   WHERE                            
  ENRE.EMPR_Codigo = @par_EMPR_Codigo                                                    
  AND ENRE.TIDO_Codigo = 100                                           
  AND ENRE.Anulado = ISNULL (@par_Anulado,ENRE.Anulado)    
  AND ((CLIE.Nombre + ' ' + CLIE.Apellido1 + ' ' + CLIE.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Cliente)) + '%' OR CLIE.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Cliente)) + '%') OR (@par_Cliente IS NULL))                          
  AND ENRE.Numero_Documento = ISNULL(@par_Numero, ENRE.Numero_Documento)                                       
   AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha) 
  AND  ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)                                 
  AND ENRE.ESOS_Numero = ISNULL(@par_ESOS_Numero,ENRE.ESOS_Numero)                                       
  AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                               
  AND ENRE.Estado = 1       
    AND ENRE.Anulado = 0                                               
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                  
  AND (ENRE.Documento_Cliente  LIKE'%'+@par_Documento_Cliente+'%' OR @par_Documento_Cliente IS NULL)                      
  AND (ENRE.Numero_Contenedor = @par_Numero_Contenedor or @par_Numero_Contenedor is null)                                
  AND (ESOS.Numero_Documento = @par_ESOS_Numero_Documento OR @par_ESOS_Numero_Documento IS NULL)                                   
  AND (ENPD.Numero_Documento = @par_ENPD_Numero_Documento OR @par_ENPD_Numero_Documento IS NULL)                     
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                      
  AND VEHI.Placa = ISNULL(@par_Placa, VEHI.Placa)                                   
  AND  CLIE.Numero_Identificacion = ISNULL(@par_Documento_Cliente,CLIE.Numero_Identificacion)    

  /* Modificación: AE 27/05/2020*/    
  AND ESOS.LNTC_Codigo = ISNULL(@par_Tarifa_Transporte,ESOS.LNTC_Codigo)

  /*Fin Modificación*/
  AND ((ENRE.Fecha_Devolcuion IS NULL AND @par_Estado = 1) 
  OR (ENRE.Fecha_Devolcuion IS NOT NULL AND @par_Estado = 0) 
  OR  @par_Estado IS null)   
 )                                                    
 SELECT DISTINCT                                    
 0 AS Obtener,                                                    
 EMPR_Codigo,                                                    
 Numero,                                        
 Numero_Documento,                                                 
 Documento_Cliente,                                                    
 Fecha,                                                   
 TERC_Codigo_Cliente,                                                    
 NombreCliente,                                                    
 IdentificacionCliente,                                                     
 Observaciones,                                                   
 Anulado,                                                    
 Estado,                                                    
 Fecha_Crea,                                                    
 OFIC_Codigo,                                                    
 NombreOficina,                                                    
 ESOS_Numero,                                                    
 OrdenServicio,                                                    
 NumeroPlanilla,                        
 NumeroDocumentoPlanilla,                                                    
 NumeroManifiesto,                                                    
 NumeroDocumentoManifiesto,                                                   
 VEHI_Codigo,         
 Placa,                                        
 TERC_Codigo_Conductor,                                      
 NombreConductor,                                                    
 IdentificacionConductor,                 
 @CantidadRegistros AS TotalRegistros,                                                    
 @par_NumeroPagina AS PaginaObtener,                                         
 @par_RegistrosPagina AS RegistrosPagina,  
 '(NO APLICA)' As Naviera             ,
 NombreCiudadDevolucion,
 Patio_Devolcuion,
 DiasDemora,
 Numero_Novedad,
 FechaDevolucion   ,
 Numero_Contenedor    ,
 MBL_Contenedor,
 HBL_Contenedor   ,
 Fecha_Devolcuion      ,
 Fecha_Descargue_Mercancia       ,
 CodigoOrdenServicio    ,
 Devuelto
 FROM Pagina                         WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                    
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                    
 ORDER BY Numero                                                    
END    
GO
