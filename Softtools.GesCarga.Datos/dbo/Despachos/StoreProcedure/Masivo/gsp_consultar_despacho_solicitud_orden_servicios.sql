﻿Print 'gsp_consultar_despacho_solicitud_orden_servicios'
GO
DROP PROCEDURE gsp_consultar_despacho_solicitud_orden_servicios
GO
CREATE PROCEDURE gsp_consultar_despacho_solicitud_orden_servicios      
(      
@par_EMPR_Codigo SMALLINT,      
@par_Numero numeric = NULL,
@par_Numero_Documento NUMERIC = NULL,      
@par_TIDO_Codigo   INT = NULL,
@par_Fecha_Inicial DATETIME = NULL,      
@par_Fecha_Final DATETIME = NULL,      
@par_OFIC_Despacha INT = NULL,      
@par_NumeroPagina INT = NULL,        
@par_RegistrosPagina INT = NULL,
@par_EstadoSolicitud INT = NULL,
@par_CLIE_Codigo	numeric = NULL

)      
AS      
BEGIN      
      
SET @par_Fecha_Inicial = CONVERT(DATE,@par_Fecha_Inicial,101)      
SET @par_Fecha_Final = DATEADD(HOUR , 23 , @par_Fecha_Final)      
SET @par_Fecha_Final = DATEADD(MINUTE , 59 , @par_Fecha_Final)      
SET @par_Fecha_Final = DATEADD(SECOND , 59 , @par_Fecha_Final)      
      
 SET NOCOUNT ON;          
  DECLARE          
   @CantidadRegistros INT          
  SELECT @CantidadRegistros = (          
    SELECT            
     COUNT(1)
    FROM          
     Encabezado_Solicitud_Orden_Servicios AS ESOS       
        
    INNER JOIN       
   Terceros CLIE ON       
   ESOS.EMPR_Codigo = CLIE.EMPR_Codigo       
   AND  ESOS.TERC_Codigo_Cliente = CLIE.Codigo      
       
   INNER JOIN       
   Oficinas OFIC ON       
   ESOS.EMPR_Codigo = OFIC.EMPR_Codigo       
   AND  ESOS.OFIC_Codigo_Despacha = OFIC.Codigo      
      
   INNER JOIN      
   Usuarios USUA ON      
   ESOS.EMPR_Codigo = USUA.EMPR_Codigo      
   AND ESOS.USUA_Codigo_Crea = USUA.Codigo      
   
   INNER JOIN 
	Linea_Negocio_Transporte_Carga LNTC ON
	ESOS.EMPR_Codigo = LNTC.EMPR_Codigo
	AND ESOS.LNTC_Codigo = LNTC.Codigo

	   
  -- JO 13-JUN-2018 - La solicitud no siempre tiene un detalle de despacho, si lo tiene, el detalle deberá cruzar de aquí en adelante    
  LEFT JOIN    
  Detalle_Despacho_Orden_Servicios AS DDOS ON    
  ESOS.EMPR_Codigo = DDOS.EMPR_Codigo    
  AND ESOS.Numero = DDOS.ESOS_Numero    

  INNER JOIN
  Tipo_Linea_Negocio_Carga TLNC ON
  DDOS.EMPR_Codigo = TLNC.EMPR_Codigo
  AND DDOS.TLNC_Codigo = TLNC.Codigo
      
  INNER JOIN    
  Producto_Transportados As PRTR ON    
  ESOS.EMPR_Codigo = PRTR.EMPR_Codigo    
  AND DDOS.PRTR_Codigo = PRTR.Codigo    
    
  INNER JOIN    
  Terceros As DEST ON    
  DDOS.EMPR_Codigo = DEST.EMPR_Codigo    
  AND DDOS.TERC_Codigo_Destinatario = DEST.Codigo    
    
  INNER JOIN    
  V_Tipo_Tarifa_Transporte_Carga AS TATC ON    
  DDOS.EMPR_Codigo = TATC.EMPR_Codigo    
  AND DDOS.TATC_Codigo_Venta = TATC.TATC_Codigo    
  AND DDOS.TTTC_Codigo_Venta = TATC.Codigo    
    
  INNER JOIN    
  Rutas As RUTA ON    
  DDOS.EMPR_Codigo = RUTA.EMPR_Codigo    
  AND DDOS.RUTA_Codigo = RUTA.Codigo    
    
  INNER JOIN
  Ciudades As CICA ON
  DDOS.EMPR_Codigo = CICA.EMPR_Codigo
  AND DDOS.CIUD_Codigo_Cargue  = CICA.Codigo

  LEFT JOIN
  Vehiculos	As VEHI ON
  DDOS.EMPR_Codigo = VEHI.EMPR_Codigo
  AND DDOS.VEHI_Codigo = VEHI.Codigo      

 WHERE          
   ESOS.EMPR_Codigo = @par_EMPR_Codigo      
   AND ESOS.Numero = ISNULL(@par_Numero, ESOS.Numero)
   AND ESOS.Numero_Documento = ISNULL(@par_Numero_Documento, ESOS.Numero_Documento)       
   AND ESOS.Fecha >= ISNULL(@par_Fecha_Inicial, ESOS.Fecha)      
   AND ESOS.Fecha <= ISNULL(@par_Fecha_Final, ESOS.Fecha)     
   AND ESOS.OFIC_Codigo_Despacha = ISNULL(@par_OFIC_Despacha, ESOS.OFIC_Codigo_Despacha) 
   AND ESOS.CATA_ESSO_Codigo = ISNULL(@par_EstadoSolicitud, ESOS.CATA_ESSO_Codigo)
  AND ESOS.Estado = 1 --> Activo  
  AND ESOS.Anulado = 0  
  AND ESOS.TERC_Codigo_Cliente = ISNULL(@par_CLIE_Codigo, ESOS.TERC_Codigo_Cliente)
     );                 
    WITH Pagina AS          
    (          
    SELECT          
  ESOS.EMPR_Codigo,      
  ESOS.Numero,      
  ESOS.TIDO_Codigo,      
  ESOS.Numero_Documento,      
  ESOS.Fecha,      
  ESOS.TERC_Codigo_Cliente,      
  ESOS.Observaciones,      
  ESOS.Estado,      
  ESOS.Anulado,      
  ESOS.USUA_Codigo_Crea,      
  ESOS.OFIC_Codigo_Despacha,    
  ISNULL(ESOS.CATA_ESSO_Codigo,0) As CATA_ESSO_Codigo,    
  USUA.Nombre As USUA_Crea_Nombre,      
    
  ISNULL(CLIE.Razon_Social ,'') + ISNULL(CLIE.Nombre ,'') + ' ' + ISNULL(CLIE.Apellido1 ,'')+ ' ' + ISNULL(CLIE.Apellido2 ,'') AS NombreCliente,      
  OFIC.Nombre AS Oficina,      
    
  DDOS.ID As ID_Registro_Detalle,    
  DDOS.RUTA_Codigo,    
  RUTA.Nombre As NombreRuta,    
  DDOS.ETCV_Numero,    
  DDOS.TATC_Codigo_Venta,    
  TATC.NombreTarifa As NombreTarifaVenta,    
  DDOS.TTTC_Codigo_Venta,    
  TATC.Nombre As NombreTipoTarifaVenta,    
  DDOS.PRTR_Codigo,    
  PRTR.Nombre As NombreProducto,    
  DDOS.Cantidad,    
  DDOS.Peso,    
  DDOS.Documento_Cliente,    
  DDOS.Direccion_Cargue,    
  DDOS.TERC_Codigo_Destinatario,    
  ISNULL(DEST.Razon_Social ,'') + ISNULL(DEST.Nombre ,'') + ' ' + ISNULL(DEST.Apellido1 ,'')+ ' ' + ISNULL(DEST.Apellido2 ,'') AS NombreDestinatario,      
  DDOS.ENOC_Numero,    
  DDOS.ENRE_Numero,    
  DDOS.ENPD_Numero,    
  DDOS.ENOC_Numero_Documento,    
  DDOS.ENRE_Numero_Documento,    
  DDOS.ENPD_Numero_Documento,    
  DDOS.ENMC_Numero,    
  DDOS.ENMC_Numero_Documento,    
  ESOS.LNTC_Codigo,
  ESOS.TLNC_Codigo,
  LNTC.Nombre As NombreLineaNegocio,
  TLNC.Nombre As NombreTipoLineaNegocio,
  CICA.Nombre As CiudadCargue,
  ISNULL(VEHI.Placa,'') As Placa,

        
     ROW_NUMBER() OVER(ORDER BY ESOS.Numero) AS RowNumber       
    FROM          
     Encabezado_Solicitud_Orden_Servicios AS ESOS       
    
   INNER JOIN       
   Terceros CLIE ON       
   ESOS.EMPR_Codigo = CLIE.EMPR_Codigo       
   AND  ESOS.TERC_Codigo_Cliente = CLIE.Codigo      
       
   INNER JOIN       
   Oficinas OFIC ON       
   ESOS.EMPR_Codigo = OFIC.EMPR_Codigo       
   AND  ESOS.OFIC_Codigo_Despacha = OFIC.Codigo      
      
   INNER JOIN      
   Usuarios USUA ON      
   ESOS.EMPR_Codigo = USUA.EMPR_Codigo      
   AND ESOS.USUA_Codigo_Crea = USUA.Codigo      
     
	INNER JOIN 
	Linea_Negocio_Transporte_Carga LNTC ON
	ESOS.EMPR_Codigo = LNTC.EMPR_Codigo
	AND ESOS.LNTC_Codigo = LNTC.Codigo

	  
  -- JO 13-JUN-2018 - La solicitud no siempre tiene un detalle de despacho, si lo tiene, el detalle deberá cruzar de aquí en adelante    
  LEFT JOIN    
  Detalle_Despacho_Orden_Servicios AS DDOS ON    
  ESOS.EMPR_Codigo = DDOS.EMPR_Codigo    
  AND ESOS.Numero = DDOS.ESOS_Numero    
    
  INNER JOIN
  Tipo_Linea_Negocio_Carga TLNC ON
  DDOS.EMPR_Codigo = TLNC.EMPR_Codigo
  AND DDOS.TLNC_Codigo = TLNC.Codigo

  INNER JOIN    
  Producto_Transportados As PRTR ON    
  ESOS.EMPR_Codigo = PRTR.EMPR_Codigo    
  AND DDOS.PRTR_Codigo = PRTR.Codigo    
    
  INNER JOIN    
  Terceros As DEST ON    
  DDOS.EMPR_Codigo = DEST.EMPR_Codigo    
  AND DDOS.TERC_Codigo_Destinatario = DEST.Codigo    
    
  INNER JOIN    
  V_Tipo_Tarifa_Transporte_Carga AS TATC ON    
  DDOS.EMPR_Codigo = TATC.EMPR_Codigo    
  AND DDOS.TATC_Codigo_Venta = TATC.TATC_Codigo    
  AND DDOS.TTTC_Codigo_Venta = TATC.Codigo    
    
  INNER JOIN    
  Rutas As RUTA ON    
  DDOS.EMPR_Codigo = RUTA.EMPR_Codigo    
  AND DDOS.RUTA_Codigo = RUTA.Codigo    
    
  INNER JOIN
  Ciudades As CICA ON
  DDOS.EMPR_Codigo = CICA.EMPR_Codigo
  AND DDOS.CIUD_Codigo_Cargue  = CICA.Codigo

  LEFT JOIN
  Vehiculos	As VEHI ON
  DDOS.EMPR_Codigo = VEHI.EMPR_Codigo
  AND DDOS.VEHI_Codigo = VEHI.Codigo

  WHERE          
	
  ESOS.EMPR_Codigo = @par_EMPR_Codigo      
  AND ESOS.Numero = ISNULL(@par_Numero, ESOS.Numero)
  AND ESOS.Numero_Documento = ISNULL(@par_Numero_Documento, ESOS.Numero_Documento)       
  AND ESOS.Fecha >= ISNULL(@par_Fecha_Inicial, ESOS.Fecha)      
  AND ESOS.Fecha <= ISNULL(@par_Fecha_Final, ESOS.Fecha)  
  AND ESOS.OFIC_Codigo_Despacha = ISNULL(@par_OFIC_Despacha, ESOS.OFIC_Codigo_Despacha) 
  AND ESOS.CATA_ESSO_Codigo = ISNULL(@par_EstadoSolicitud, ESOS.CATA_ESSO_Codigo)
  AND ESOS.TERC_Codigo_Cliente = ISNULL(@par_CLIE_Codigo, ESOS.TERC_Codigo_Cliente)
  AND ESOS.Estado = 1 --> Activo  
  AND ESOS.Anulado = 0     
      
  )           
  SELECT DISTINCT          
        0 As Obtener,      
  EMPR_Codigo,      
     
  Numero,      
  TIDO_Codigo,      
  Numero_Documento,      
  Fecha,      
    
  TERC_Codigo_Cliente,      
  Observaciones,      
  Estado,      
  Anulado,      
  USUA_Codigo_Crea,      
  OFIC_Codigo_Despacha,    
  CATA_ESSO_Codigo,    
  USUA_Crea_Nombre,      
  NombreCliente,      
  Oficina,      
  ID_Registro_Detalle,    
  RUTA_Codigo,    
  NombreRuta,    
  ETCV_Numero,    
  TATC_Codigo_Venta,    
  NombreTarifaVenta,    
  TTTC_Codigo_Venta,    
  NombreTipoTarifaVenta,    
  PRTR_Codigo,    
  NombreProducto,    
  Cantidad,    
  Peso,    
  Documento_Cliente,    
  Direccion_Cargue,    
  TERC_Codigo_Destinatario,    
  NombreDestinatario,      
  ENOC_Numero,    
  ENRE_Numero,    
  ENPD_Numero,    
  ENOC_Numero_Documento,    
  ENRE_Numero_Documento,    
  ENPD_Numero_Documento,     
  ENMC_Numero,    
  ENMC_Numero_Documento,    
  LNTC_Codigo,
  TLNC_Codigo,
  NombreLineaNegocio,
  NombreTipoLineaNegocio,
  CiudadCargue,
  Placa,

  @CantidadRegistros AS TotalRegistros,          
  @par_RegistrosPagina AS RegistrosPagina          
  FROM          
   Pagina          
  WHERE          
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
  ORDER BY Numero ASC          
      
END      
GOPrint 'gsp_consultar_despacho_solicitud_orden_servicios'
GO
