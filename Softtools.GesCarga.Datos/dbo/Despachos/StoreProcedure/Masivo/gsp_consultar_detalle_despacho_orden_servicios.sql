﻿Print 'gsp_consultar_detalle_despacho_orden_servicios'
GO
DROP PROCEDURE gsp_consultar_detalle_despacho_orden_servicios
GO
CREATE PROCEDURE gsp_consultar_detalle_despacho_orden_servicios(
@par_EMPR_Codigo Smallint,
@par_ESOS_Numero numeric,

@par_ID_Codigo numeric =NULL,
@par_NumeroPagina INT = NULL,  
@par_RegistrosPagina INT = NULL  
)
As
BEGIN

 DECLARE @CantidadRegistros INT 
 SELECT @CantidadRegistros = (
					SELECT DISTINCT COUNT(DDOS.EMPR_Codigo)
 					 
					FROM Detalle_Despacho_Orden_Servicios DDOS
					
					INNER JOIN
					Terceros DEST ON
					DDOS.EMPR_Codigo = DEST.EMPR_Codigo
					AND DDOS.TERC_Codigo_Destinatario = DEST.Codigo

					INNER JOIN
					Rutas RUTA ON
					DDOS.EMPR_Codigo = RUTA.EMPR_Codigo
					AND DDOS.RUTA_Codigo = RUTA.Codigo

					INNER JOIN
					Producto_Transportados PRTR ON
					DDOS.EMPR_Codigo = PRTR.EMPR_Codigo
					AND DDOS.PRTR_Codigo = PRTR.Codigo

					LEFT JOIN
					V_Tipo_Tarifa_Transporte_Carga TTCC ON
					DDOS.EMPR_Codigo = TTCC.EMPR_Codigo
					AND DDOS.TTTC_Codigo_Compra = TTCC.Codigo

					INNER JOIN
					V_Tipo_Tarifa_Transporte_Carga TTCV ON
					DDOS.EMPR_Codigo = TTCV.EMPR_Codigo
					AND DDOS.TTTC_Codigo_Venta = TTCV.Codigo

					INNER JOIN
					Encabezado_Tarifario_Carga_Ventas ETCV ON
					DDOS.EMPR_Codigo = ETCV.EMPR_Codigo
					AND DDOS.ETCV_Numero = ETCV.Numero

					INNER JOIN
					Linea_Negocio_Transporte_Carga LNTC ON
					DDOS.EMPR_Codigo = LNTC.EMPR_Codigo
					AND DDOS.LNTC_Codigo = LNTC.Codigo

					INNER JOIN
					Tipo_Linea_Negocio_Carga TLNC ON
					DDOS.EMPR_Codigo = TLNC.EMPR_Codigo
					AND DDOS.TLNC_Codigo = TLNC.Codigo

					WHERE 				
					DDOS.EMPR_Codigo = @par_EMPR_Codigo
					AND DDOS.ESOS_Numero = @par_ESOS_Numero
					AND DDOS.ID = ISNULL(@par_ID_Codigo, DDOS.ID)

					);

					WITH Paginas As (

					SELECT DDOS.EMPR_Codigo,
					DDOS.ESOS_Numero,
					DDOS.ID,
					DDOS.RUTA_Codigo,
					DDOS.ETCV_Numero,
					DDOS.ETCC_Numero,
					ISNULL(DDOS.TATC_Codigo_Compra, 0) As TATC_Codigo_Compra,
					ISNULL(DDOS.TATC_Codigo_Venta, 0) As TATC_Codigo_Venta,
					ISNULL(DDOS.TTTC_Codigo_Compra, 0) As TTTC_Codigo_Compra,
					ISNULL(DDOS.TTTC_Codigo_Venta, 0) As TTTC_Codigo_Venta,
					DDOS.PRTR_Codigo,
					DDOS.Cantidad,
					DDOS.Peso,
					DDOS.Valor,
					ISNULL(DDOS.Documento_Cliente,'') As Documento_Cliente,
					DDOS.CIUD_Codigo_Cargue,
					DDOS.Direccion_Cargue,
					ISNULL(DDOS.Telefono_Cargue,'') As Telefono_Cargue,
					ISNULL(DDOS.Contacto_Cargue,'') As Contacto_Cargue,
					DDOS.TERC_Codigo_Destinatario,
					DDOS.CIUD_Codigo_Destino,
					ISNULL(DDOS.Direccion_Destino,'') As Direccion_Destino,
					ISNULL(DDOS.Telefono_Destino,'') As Telefono_Destino,
					ISNULL(DDOS.Contacto_Destino,'') As Contacto_Destino,
					ISNULL(DDOS.Numero_Contenedor,'') As Numero_Contenedor,
					ISNULL(DDOS.MBL_Contenedor,'') As MBL_Contenedor,
					ISNULL(DDOS.HBL_Contenedor,'') As HBL_Contenedor,
					ISNULL(DDOS.DO_Contenedor,'') As DO_Contenedor,
					ISNULL(DDOS.Valor_FOB,0) As Valor_FOB,
					ISNULL(DDOS.SICD_Codigo_Devolucion_Contenedor,0) As SICD_Codigo_Devolucion_Contenedor,
					ISNULL(Fecha_Devolucion_Contenedor,'01/01/1900') As Fecha_Devolucion_Contenedor,
					ISNULL(DDOS.CATA_MOTR_Codigo,0) As CATA_MOTR_Codigo,
					ISNULL(DDOS.VEHI_Codigo,0) As VEHI_Codigo,
					ISNULL(DDOS.TERC_Codigo_Conductor,0) As TERC_Codigo_Conductor,
					DDOS.Flete_Cliente,
					DDOS.Valor_Flete_Cliente,
					DDOS.Flete_Transportador,
					DDOS.Valor_Flete_Transportador,
					DDOS.Valor_Anticipo,
					DDOS.Aplica_Escolta,
					ISNULL(DDOS.TERC_Codigo_Empresa_Escolta,0) As TERC_Codigo_Empresa_Escolta,
					DDOS.Valor_Escolta,
					DDOS.Fecha_Crea,
					DDOS.USUA_Codigo_Crea,
					DDOS.Fecha_Modifica,
					DDOS.USUA_Codigo_Modifica,
					ISNULL(DDOS.ENOC_Numero,0) As ENOC_Numero,
					ISNULL(DDOS.ENRE_Numero,0) As ENRE_Numero,
					ISNULL(DDOS.ENPD_Numero,0) As ENPD_Numero,
					ISNULL(DDOS.ENMC_Numero,0) As ENMC_Numero,
					DDOS.ENOC_Numero_Documento,
					DDOS.ENRE_Numero_Documento,
					DDOS.ENPD_Numero_Documento,
					DDOS.ENMC_Numero_Documento,
					CASE RUTA.CATA_TIRU_Codigo WHEN 4402 THEN 8812 ELSE 8811 END As Codigo_Tipo_Remesa, --> Si es URBANA o NACIONAL SEGUN TIPO RUTA
					CASE RUTA.CATA_TIRU_Codigo WHEN 4402 THEN 'REMESA URBANA' ELSE 'REMESA NACIONAL' END As Nombre_Tipo_Remesa, --> Si es URBANA o NACIONAL SEGUN TIPO RUTA
					ISNULL(DEST.Razon_Social,'') + ISNULL(DEST.Nombre,'') + ' ' + ISNULL(DEST.Apellido1,'') + ' ' + ISNULL(DEST.Apellido2,'') As NombreDestinatario,
					RUTA.Nombre As NombreRuta,
					RUTA.CIUD_Codigo_Origen As CIUD_Codigo_Origen_Ruta,
					RUTA.CIUD_Codigo_Destino As CIUD_Codigo_Destino_Ruta,
					PRTR.Nombre As NombreProducto,
					ISNULL(TTCC.NombreTarifa,'') As TarifaTransporteCargaCompra,
					ISNULL(TTCC.Nombre,'') As TipoTarifaTransporteCargaCompra,
					ISNULL(TTCV.NombreTarifa,'') As TarifaTransporteCargaVenta,
					ISNULL(TTCV.Nombre,'') As TipoTarifaTransporteCargaVenta,
					ISNULL(ETCV.Nombre,'') As  NombreTarifarioVenta,

					DDOS.LNTC_Codigo,
					DDOS.TLNC_Codigo,
					ISNULL(LNTC.Nombre,'') As NombreLineaNegocio,
					ISNULL(TLNC.Nombre,'') As NombreTipoLineaNegocio,

					ROW_NUMBER() OVER(ORDER BY DDOS.ID) AS RowNumber 				
					
					FROM Detalle_Despacho_Orden_Servicios DDOS
					
					INNER JOIN
					Terceros DEST ON
					DDOS.EMPR_Codigo = DEST.EMPR_Codigo
					AND DDOS.TERC_Codigo_Destinatario = DEST.Codigo

					INNER JOIN
					Rutas RUTA ON
					DDOS.EMPR_Codigo = RUTA.EMPR_Codigo
					AND DDOS.RUTA_Codigo = RUTA.Codigo

					INNER JOIN
					Producto_Transportados PRTR ON
					DDOS.EMPR_Codigo = PRTR.EMPR_Codigo
					AND DDOS.PRTR_Codigo = PRTR.Codigo

					INNER JOIN
					V_Tipo_Tarifa_Transporte_Carga TTCC ON
					DDOS.EMPR_Codigo = TTCC.EMPR_Codigo
					AND DDOS.TTTC_Codigo_Venta = TTCC.Codigo

					INNER JOIN
					V_Tipo_Tarifa_Transporte_Carga TTCV ON
					DDOS.EMPR_Codigo = TTCV.EMPR_Codigo
					AND DDOS.TTTC_Codigo_Venta = TTCV.Codigo
					
					INNER JOIN
					Encabezado_Tarifario_Carga_Ventas ETCV ON
					DDOS.EMPR_Codigo = ETCV.EMPR_Codigo
					AND DDOS.ETCV_Numero = ETCV.Numero

					INNER JOIN
					Linea_Negocio_Transporte_Carga LNTC ON
					DDOS.EMPR_Codigo = LNTC.EMPR_Codigo
					AND DDOS.LNTC_Codigo = LNTC.Codigo

					INNER JOIN
					Tipo_Linea_Negocio_Carga TLNC ON
					DDOS.EMPR_Codigo = TLNC.EMPR_Codigo
					AND DDOS.TLNC_Codigo = TLNC.Codigo
					
					WHERE 				
					DDOS.EMPR_Codigo = @par_EMPR_Codigo
					AND DDOS.ESOS_Numero = @par_ESOS_Numero
					AND DDOS.ID = ISNULL(@par_ID_Codigo, DDOS.ID)
					)

		SELECT DISTINCT
		
				EMPR_Codigo,
				ESOS_Numero,
				ID,
				RUTA_Codigo,
				PRTR_Codigo,
				ETCV_Numero,
				ETCC_Numero,
				TATC_Codigo_Compra,
				TATC_Codigo_Venta,
				TTTC_Codigo_Compra,
				TTTC_Codigo_Venta,
				Cantidad,
				Peso,
				Valor,
				Documento_Cliente,
				CIUD_Codigo_Cargue,
				Direccion_Cargue,
				Telefono_Cargue,
				Contacto_Cargue,
				TERC_Codigo_Destinatario,
				CIUD_Codigo_Destino,
				Direccion_Destino,
				Telefono_Destino,
				Contacto_Destino,
				Numero_Contenedor,
				MBL_Contenedor,
				HBL_Contenedor,
				DO_Contenedor,
				Valor_FOB,
				SICD_Codigo_Devolucion_Contenedor,
				Fecha_Devolucion_Contenedor,
				CATA_MOTR_Codigo,
				VEHI_Codigo,
				TERC_Codigo_Conductor,
				Flete_Cliente,
				Valor_Flete_Cliente,
				Flete_Transportador,
				Valor_Flete_Transportador,
				Valor_Anticipo,
				Aplica_Escolta,
				TERC_Codigo_Empresa_Escolta,
				Valor_Escolta,
				Fecha_Crea,
				USUA_Codigo_Crea,
				Fecha_Modifica,
				USUA_Codigo_Modifica,
				ENOC_Numero,
				ENRE_Numero,
				ENPD_Numero,
				ENMC_Numero,
				ENOC_Numero_Documento,
				ENRE_Numero_Documento,
				ENPD_Numero_Documento,
				ENMC_Numero_Documento,
				Codigo_Tipo_Remesa,
				Nombre_Tipo_Remesa,
				NombreDestinatario,
				NombreRuta,
				CIUD_Codigo_Origen_Ruta,
				CIUD_Codigo_Destino_Ruta,
				NombreProducto,
				TarifaTransporteCargaCompra,
				TipoTarifaTransporteCargaCompra,
				TarifaTransporteCargaVenta,
				TipoTarifaTransporteCargaVenta,
				NombreTarifarioVenta,

				LNTC_Codigo,
				TLNC_Codigo,
				NombreLineaNegocio,
				NombreTipoLineaNegocio,

			@CantidadRegistros AS TotalRegistros,    
			@par_RegistrosPagina AS RegistrosPagina  
			FROM    
			Paginas    
			WHERE    
		  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
		  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
		  ORDER BY ID ASC    

END
GO