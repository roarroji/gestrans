﻿PRINT 'gsp_consultar_orden_cargue'
GO
DROP PROCEDURE gsp_consultar_orden_cargue
GO
CREATE PROCEDURE gsp_consultar_orden_cargue
(                
	@par_EMPR_Codigo SMALLINT,                
	@par_Numero NUMERIC= NULL,                  
	@par_Numero_Manifiesto NUMERIC= NULL,                  
	@par_Fecha_Inicial DATE= NULL,               
	@par_Fecha_Final DATE= NULL,                 
	@par_Placa VARCHAR(6) = NULL,               
	@par_Estado SMALLINT = NULL,              
	@par_Nombre_Cliente VARCHAR(50) = NULL,
	@par_Usuario_Consulta INT,              
	@par_NumeroPagina INT = NULL,                
	@par_RegistrosPagina INT = NULL ,              
	@par_Anulado SMALLINT = NULL                    
)                
AS                
BEGIN                
	SET NOCOUNT ON;                
	DECLARE                
	@CantidadRegistros INT                
	SELECT @CantidadRegistros = (                
		SELECT DISTINCT                 
		COUNT(1)                 
		FROM                
		Encabezado_Orden_Cargues AS ENOC                 

		LEFT JOIN Vehiculos AS VEHI                                 
		ON ENOC.EMPR_Codigo = VEHI.EMPR_Codigo                                  
		AND ENOC.VEHI_Codigo = VEHI.Codigo                
              
		LEFT JOIN Terceros AS TECL                                 
		ON ENOC.EMPR_Codigo = TECL.EMPR_Codigo                                  
		AND ENOC.TERC_Codigo_Cliente = TECL.Codigo                
              
		LEFT JOIN Terceros AS TECO                                 
		ON ENOC.EMPR_Codigo = TECO.EMPR_Codigo                                  
		AND ENOC.TERC_Codigo_Conductor = TECO.Codigo             
          
		LEFT JOIN Rutas AS RUTA                                 
		ON ENOC.EMPR_Codigo = RUTA.EMPR_Codigo                                  
		AND ENOC.RUTA_Codigo = RUTA.Codigo           
                   
		LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC                                 
		ON ENOC.EMPR_Codigo = ENMC.EMPR_Codigo                                  
		AND ENOC.ENMC_Numero = ENMC.Numero       
      
		WHERE                   
		ENOC.EMPR_Codigo = @par_EMPR_Codigo               
		AND ENOC.Numero_Documento = ISNULL(@par_Numero,ENOC.Numero_Documento)           
		AND (ENMC.Numero_Documento = ISNULL(@par_Numero_Manifiesto,ENMC.Numero_Documento) or  @par_Numero_Manifiesto is null)          
		AND ENOC.FECHA >= ISNULL(@par_Fecha_Inicial ,ENOC.FECHA)              
		AND ENOC.FECHA <=ISNULL(@par_Fecha_Final,ENOC.FECHA )              
		AND ((VEHI.PLACA LIKE '%'+ @par_Placa + '%') OR (@par_Placa IS NULL))              
		AND ENOC.Estado=ISNULL(@par_Estado,ENOC.Estado)              
		AND ENOC.Anulado = ISNULL (@par_Anulado,ENOC.Anulado )
		AND ENOC.OFIC_Codigo IN (
			SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE USOF.USUA_Codigo = @par_Usuario_Consulta
		)
		AND CONCAT(TECL.Razon_Social,' ',TECL.Nombre,' ',TECL.Apellido1,' ',TECL.Apellido2) LIKE '%'+ ISNULL(@par_Nombre_Cliente, CONCAT(TECL.Razon_Social,' ',TECL.Nombre,' ',TECL.Apellido1,' ',TECL.Apellido2))+'%'
	);                         
	WITH Pagina AS                
	(                  
		SELECT                
		0 AS Obtener,                
		ENOC.EMPR_Codigo,              
		ENOC.Numero ,              
		ENOC.Numero_Documento ,              
		ENOC.Fecha,              
		ENOC.TERC_Codigo_Cliente,              
		ENOC.VEHI_Codigo,              
		ISNULL(TECL.Razon_Social,'')+' '+ISNULL(TECL.Nombre,'')+' '+ISNULL(TECL.Apellido1,'')+' '+ISNULL(TECL.Apellido2,'') AS NombreCliente,              
		ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')+' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS NombreConductor,              
		ENOC.TERC_Codigo_Conductor,              
		TECO.Telefonos AS TelefonoConductor,              
		ISNULL(VEHI.Placa,'') as PlacaVehiculo,               
		RUTA.Codigo AS RUTA_Codigo,              
		RUTA.Nombre AS NombreRuta,              
		ENOC.Estado,              
		ENOC.Anulado,
		ENOC.OFIC_Codigo,               
		ROW_NUMBER() OVER(ORDER BY ENOC.Numero DESC) AS RowNumber                
		FROM                
		Encabezado_Orden_Cargues AS ENOC                 

		LEFT JOIN Vehiculos AS VEHI                                 
		ON ENOC.EMPR_Codigo = VEHI.EMPR_Codigo                    
		AND ENOC.VEHI_Codigo = VEHI.Codigo                
              
		LEFT JOIN Terceros AS TECL                                 
		ON ENOC.EMPR_Codigo = TECL.EMPR_Codigo                                  
		AND ENOC.TERC_Codigo_Cliente = TECL.Codigo                
              
		LEFT JOIN Terceros AS TECO                                 
		ON ENOC.EMPR_Codigo = TECO.EMPR_Codigo                                  
		AND ENOC.TERC_Codigo_Conductor = TECO.Codigo             
          
		LEFT JOIN Rutas AS RUTA                                 
		ON ENOC.EMPR_Codigo = RUTA.EMPR_Codigo                                  
		AND ENOC.RUTA_Codigo = RUTA.Codigo           
      
              
		LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC                                 
		ON ENOC.EMPR_Codigo = ENMC.EMPR_Codigo                                  
		AND ENOC.ENMC_Numero = ENMC.Numero        
                   
		WHERE                   
		ENOC.EMPR_Codigo = @par_EMPR_Codigo               
		AND ENOC.Numero_Documento = ISNULL(@par_Numero,ENOC.Numero_Documento)           
		AND (ENMC.Numero_Documento = ISNULL(@par_Numero_Manifiesto,ENMC.Numero_Documento) or  @par_Numero_Manifiesto is null)          
      
		AND ENOC.FECHA >= ISNULL(@par_Fecha_Inicial ,ENOC.FECHA)              
		AND ENOC.FECHA <=ISNULL(@par_Fecha_Final,ENOC.FECHA )              
		AND ((VEHI.PLACA LIKE '%'+ @par_Placa + '%') OR (@par_Placa IS NULL))              
		AND ENOC.Estado=ISNULL(@par_Estado,ENOC.Estado)              
		AND ENOC.Anulado = ISNULL (@par_Anulado,ENOC.Anulado ) 
		AND ENOC.OFIC_Codigo IN (
			SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE USOF.USUA_Codigo = @par_Usuario_Consulta
		)
		AND CONCAT(TECL.Razon_Social,' ',TECL.Nombre,' ',TECL.Apellido1,' ',TECL.Apellido2) LIKE '%'+ ISNULL(@par_Nombre_Cliente, CONCAT(TECL.Razon_Social,' ',TECL.Nombre,' ',TECL.Apellido1,' ',TECL.Apellido2))+'%'             
	)                
	SELECT DISTINCT              
	Obtener,             
	EMPR_Codigo,              
	Numero ,               
	Numero_Documento,              
	Fecha,              
	PlacaVehiculo,              
	VEHI_Codigo,              
	TERC_Codigo_Cliente,              
	NombreCliente,              
	NombreConductor,              
	TERC_Codigo_Conductor,              
	RUTA_Codigo,              
	NombreRuta,              
	Estado,              
	Anulado,
	OFIC_Codigo,             
	Obtener,              
	TelefonoConductor,              
	@CantidadRegistros AS TotalRegistros,                
	@par_NumeroPagina AS PaginaObtener,                
	@par_RegistrosPagina AS RegistrosPagina                
	FROM                
	Pagina                
	WHERE                
	RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                
	AND RowNumber<= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                
	ORDER BY Numero  DESC              
END
GO