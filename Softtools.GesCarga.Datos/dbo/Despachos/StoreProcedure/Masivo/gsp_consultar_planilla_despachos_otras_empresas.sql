﻿CREATE PROCEDURE gsp_consultar_planilla_despachos_otras_empresas                        
(                                               
 @par_EMPR_Codigo SMALLINT,                                                      
 @par_Numero NUMERIC= NULL,     
 @par_Numero_Documento_Transporte Varchar(20) = NULL,                                             
 @par_Fecha_Inicial DATE= NULL,                                                     
 @par_Fecha_Final DATE= NULL,                                                      
 @par_Estado SMALLINT = NULL,                                                    
 @par_Anulado SMALLINT = NULL ,                                                    
 @par_VEHI_Placa Varchar(20) = NULL,                                                     
 @par_Codigo_Cliente Varchar(20) = NULL,      
 @par_RegistrosPagina INT = NULL,      
 @par_NumeroPagina INT = NULL                                             
                                           
)                                                      
AS                                                      
BEGIN                                                      
 SET NOCOUNT ON;                                                      
 DECLARE                                                      
 @CantidadRegistros INT                                                      
 SELECT @CantidadRegistros = (                                                      
  SELECT DISTINCT                                                       
  COUNT(1)           
                                                    
  FROM Planillas_Despachos_Otras_Empresas  PDOE  
  
   LEFT JOIN Encabezado_Remesas ENRE  
	ON ENRE.EMPR_Codigo = PDOE.EMPR_Codigo  
	AND ENRE.Numero = PDOE.ENRE_Numero  
  
	LEFT JOIN Detalle_Remesas_Facturas DERF  
	ON DERF.EMPR_Codigo = ENRE.EMPR_Codigo  
	AND DERF.ENRE_Numero = ENRE.Numero  
  
	LEFT JOIN Encabezado_Facturas ENFA  
	ON ENFA.EMPR_Codigo = DERF.EMPR_Codigo  
	AND ENFA.Numero = DERF.ENFA_Numero  
                                                    
  left join Vehiculos VEHI on                                            
  PDOE.EMPR_Codigo = VEHI.EMPR_Codigo                                                     
  AND PDOE.VEHI_Codigo = VEHI.Codigo                                                    
                                                
  left join Terceros TECO  on                                            
  PDOE.EMPR_Codigo = TECO.EMPR_Codigo                                                    
  AND PDOE.TERC_Codigo_Conductor = TECO.Codigo          
        
  left join Producto_Transportados PRTR ON      
  PDOE.EMPR_Codigo = PRTR.EMPR_Codigo      
  AND PDOE.PRTR_Codigo = PRTR.Codigo         
      
  left join Rutas RUTA ON      
  PDOE.EMPR_Codigo = RUTA.EMPR_Codigo      
  AND PDOE.RUTA_Codigo = RUTA.Codigo       
      
    left join Terceros TECL  on                                            
  PDOE.EMPR_Codigo = TECL.EMPR_Codigo                                                    
  AND PDOE.TERC_Codigo_Cliente = TECL.Codigo                
                                                   
  WHERE                                                      
                                                  
  PDOE.EMPR_Codigo = @par_EMPR_Codigo                                                     
  AND PDOE.Numero_Planilla = ISNULL(@par_Numero, PDOE.Numero_Planilla)                                                    
             
      
  AND ((PDOE.Numero_Documento_Transporte LIKE '%'+ @par_Numero_Documento_Transporte + '%') OR (@par_Numero_Documento_Transporte IS NULL))    
  AND PDOE.Fecha_Planilla >= ISNULL(@par_Fecha_Inicial ,PDOE.Fecha_Planilla)                                                    
  AND PDOE.Fecha_Planilla <=ISNULL(@par_Fecha_Final,PDOE.Fecha_Planilla )         
  AND ((VEHI.PLACA LIKE '%'+ @par_VEHI_Placa + '%') OR (@par_VEHI_Placa IS NULL))       
  AND CONCAT(TECL.Razon_Social,' ',TECL.Nombre,' ',TECL.Apellido1,' ',TECL.Apellido2) LIKE '%'+ ISNULL(@par_Codigo_Cliente, CONCAT(TECL.Razon_Social,' ',TECL.Nombre,' ',TECL.Apellido1,' ',TECL.Apellido2))+'%'                                               
  
    
  AND PDOE.Estado=ISNULL(@par_Estado,PDOE.Estado)                                                    
  AND PDOE.Anulado = ISNULL (@par_Anulado,PDOE.Anulado )          
             
 );                                                             
 WITH Pagina AS                     
 (                                                      
  SELECT                                                     
  PDOE.EMPR_Codigo,                                                   
  PDOE.Numero ,                                                    
  PDOE.Numero_Planilla ,                                                    
  PDOE.Fecha_Planilla,                                                    
  ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')                                                      
  +' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS Conductor,                              
  VEHI.Placa as PlacaVehiculo,                                                     
  VEHI.Codigo as VEHI_Codigo,                     
  PDOE.Estado,        
  PDOE.Anulado,                                
  PDOE.Valor_Total_Flete,                            
  PRTR.Nombre As Producto,      
  RUTA.Nombre As Ruta,    
  TECL.Razon_Social As PerfilCliente,    
  PDOE.Numero_Documento_Transporte As Numero_Documento_Transporte,    
  PDOE.Cantidad,    
  PDOE.Peso,    
  PDOE.Valor_Pagar,     
  Obtener=0,               
                     
  (SELECT SUM(Valor_Pagar) FROM Planillas_Despachos_Otras_Empresas            
  WHERE EMPR_Codigo = PDOE.EMPR_Codigo  and Estado = 1 and Anulado = @par_Anulado) as ValorReanticipos,    
CASE WHEN ENFA.Numero IS NULL THEN ''  
ELSE CONVERT(varchar(10),ENFA.Numero_Documento) END AS Facturado , 
        
  ROW_NUMBER() OVER ( ORDER BY   PDOE.NUMERO DESC) AS RowNumber                                
                                
  FROM Planillas_Despachos_Otras_Empresas  PDOE 
  
  LEFT JOIN Encabezado_Remesas ENRE  
	ON ENRE.EMPR_Codigo = PDOE.EMPR_Codigo  
	AND ENRE.Numero = PDOE.ENRE_Numero  
  
	LEFT JOIN Detalle_Remesas_Facturas DERF  
	ON DERF.EMPR_Codigo = ENRE.EMPR_Codigo  
	AND DERF.ENRE_Numero = ENRE.Numero  
  
	LEFT JOIN Encabezado_Facturas ENFA  
	ON ENFA.EMPR_Codigo = DERF.EMPR_Codigo  
	AND ENFA.Numero = DERF.ENFA_Numero  
                                                    
  left join Vehiculos VEHI on                                            
  PDOE.EMPR_Codigo = VEHI.EMPR_Codigo                                                     
  AND PDOE.VEHI_Codigo = VEHI.Codigo                                                    
                                                
  left join Terceros TECO  on                                            
  PDOE.EMPR_Codigo = TECO.EMPR_Codigo                                                    
  AND PDOE.TERC_Codigo_Conductor = TECO.Codigo        
         
  left join Producto_Transportados PRTR ON      
  PDOE.EMPR_Codigo = PRTR.EMPR_Codigo      
  AND PDOE.PRTR_Codigo = PRTR.Codigo           
    
  left join Rutas RUTA ON      
  PDOE.EMPR_Codigo = RUTA.EMPR_Codigo      
  AND PDOE.RUTA_Codigo = RUTA.Codigo      
    
  left join Terceros TECL  on                                            
  PDOE.EMPR_Codigo = TECL.EMPR_Codigo                                                    
  AND PDOE.TERC_Codigo_Cliente = TECL.Codigo    
                                                             
  WHERE                                                      
                                                  
  PDOE.EMPR_Codigo = @par_EMPR_Codigo                                                     
  AND PDOE.Numero_Planilla = ISNULL(@par_Numero, PDOE.Numero_Planilla)                                                    
                                                     
                                                 
  AND PDOE.Fecha_Planilla >= ISNULL(@par_Fecha_Inicial ,PDOE.Fecha_Planilla)                                                    
  AND PDOE.Fecha_Planilla <=ISNULL(@par_Fecha_Final,PDOE.Fecha_Planilla )       
  AND ((VEHI.PLACA LIKE '%'+ @par_VEHI_Placa + '%') OR (@par_VEHI_Placa IS NULL))                                
  AND ((PDOE.Numero_Documento_Transporte LIKE '%'+ @par_Numero_Documento_Transporte + '%') OR (@par_Numero_Documento_Transporte IS NULL))                                       
  AND CONCAT(TECL.Razon_Social,' ',TECL.Nombre,' ',TECL.Apellido1,' ',TECL.Apellido2) LIKE '%'+ ISNULL(@par_Codigo_Cliente, CONCAT(TECL.Razon_Social,' ',TECL.Nombre,' ',TECL.Apellido1,' ',TECL.Apellido2))+'%'    
  AND PDOE.Estado=ISNULL(@par_Estado,PDOE.Estado)                              
  AND PDOE.Anulado = ISNULL (@par_Anulado,PDOE.Anulado )                     
             
 )                                                     
 SELECT DISTINCT     
 EMPR_Codigo,                                        
 Numero_Planilla,                                                    
 Fecha_Planilla,                                                    
 PlacaVehiculo,                              
 VEHI_Codigo,                                            
 Conductor,    
 PerfilCliente,    
 Ruta,      
 Producto,                
 Estado,                                                    
 Valor_Total_Flete,                                    
 Anulado,             
 Numero_Documento_Transporte,        
 Cantidad,    
 Peso,    
 Valor_Pagar,    
 Numero,                                       
 Facturado,                             
 0 as Obtener,                     
                    
 ValorReanticipos,                  
          
 @CantidadRegistros AS TotalRegistros,                                                      
 @par_NumeroPagina AS PaginaObtener,                                                      
 @par_RegistrosPagina AS RegistrosPagina                                                      
 FROM                                                      
 Pagina                                                  
 WHERE                                                      
 RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                      
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                                      
 order by Numero_Planilla  DESC                                                    
END   
GO
