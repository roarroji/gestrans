﻿PRINT 'gsp_consultar_remesas'
GO
DROP PROCEDURE gsp_consultar_remesas
GO
CREATE PROCEDURE gsp_consultar_remesas
(                                        
	@par_EMPR_Codigo SMALLINT,                                        
	@par_TIDO_Codigo SMALLINT = NULL,                                        
	@par_Numero NUMERIC = NULL,                                        
	@par_Fecha_Inicial DATETIME = NULL,                                        
	@par_Fecha_Final DATETIME = NULL,                                        
	@par_ESOS_Numero INT = NULL,                                        
	@par_ENMC_Numero INT = NULL,                                        
	@par_Cliente VARCHAR(50) = NULL,                                        
	@par_Documento_Cliente VARCHAR(30) = NULL,                                        
	@par_Numero_Contenedor VARCHAR(30) = NULL,                                        
	@par_Placa VARCHAR(20) = NULL,                                        
	@par_Conductor VARCHAR(50) = NULL,                                        
	@par_Tenedor VARCHAR(50) = NULL,                                        
	@par_CATA_TIRE_Codigo SMALLINT = NULL,                                        
	@par_OFIC_Codigo INT = NULL,                                        
	@par_Estado SMALLINT = NULL,                                        
	@par_Cumplido SMALLINT = NULL,                                        
	@par_ENPD_Numero_Documento NUMERIC = NULL,                                       
	@par_ESOS_Numero_Documento NUMERIC = NULL,                                       
	@par_ENPD_Numero NUMERIC = NULL,                                        
	@par_CLIE_Codigo NUMERIC = NULL,                                      
	@par_CIUD_Codigo_Destino NUMERIC = NULL,                                      
	@par_CIUD_Codigo_origen NUMERIC = NULL,                                      
	@par_NumeroPagina INT = NULL,                                        
	@par_RegistrosPagina INT = NULL,                                       
	@par_PendienteFacturadas  SMALLINT = NULL,    
	@par_TEDI_Codigo   INT =  NULL,
	@par_Anulado SMALLINT = NULL,
	@par_Usuario_Consulta INT
)                                        
AS                                        
BEGIN                                        
	SET NOCOUNT ON;                                         
	DECLARE @CantidadRegistros INT                                        
	SELECT @CantidadRegistros =                                        
	(                                        
		SELECT DISTINCT                                        
		COUNT(1)                                        
		FROM                                        
		Encabezado_Remesas ENRE       
         
		LEFT JOIN Tipo_Documentos TIDO                                        
		ON ENRE.EMPR_Codigo = TIDO.EMPR_Codigo                   
		AND ENRE.TIDO_Codigo = TIDO.Codigo                                           
                                           
		LEFT JOIN V_Tipo_Remesas TIRE                                        
		ON ENRE.EMPR_Codigo = TIRE.EMPR_Codigo                   
		AND ENRE.CATA_TIRE_Codigo = TIRE.Codigo                                        
                                        
		LEFT JOIN Rutas RUTA                                        
		ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                   
		AND ENRE.RUTA_Codigo = RUTA.Codigo                                        
                                        
		LEFT JOIN Producto_Transportados PRTR                                        
		ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                   
		AND ENRE.PRTR_Codigo = PRTR.Codigo                                        
                                        
		LEFT JOIN Terceros CLIE                                        
		ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                   
		AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                        
                                        
		LEFT JOIN Terceros REMI                                        
		ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                   
		AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                        
                                        
		LEFT JOIN Terceros COND                           
		ON ENRE.EMPR_Codigo = COND.EMPR_Codigo                   
		AND ENRE.TERC_Codigo_Conductor = COND.Codigo                     
                  
		LEFT JOIN Ciudades CIUD                                        
		ON ENRE.EMPR_Codigo = CIUD.EMPR_Codigo                   
		AND ENRE.CIUD_Codigo_Remitente = CIUD.Codigo                                        
                                        
		--LEFT JOIN Detalle_Tarifario_Carga_Ventas DTCV                                        
		--ON ENRE.EMPR_Codigo = DTCV.EMPR_Codigo AND ENRE.DTCV_Codigo = DTCV.Codigo                                        
                                        
		LEFT JOIN Ciudades CIDE                                        
		ON ENRE.EMPR_Codigo = CIDE.EMPR_Codigo                   
		AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo                                        
                                        
		LEFT JOIN Oficinas OFIC                                        
		ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                   
		AND ENRE.OFIC_Codigo = OFIC.Codigo                                        
                                
		LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS                                        
		ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo                   
		AND ENRE.ESOS_Numero = ESOS.Numero                                        
                              
		LEFT JOIN Encabezado_Facturas ENFA                                        
		ON ENRE.EMPR_Codigo = ENFA.EMPR_Codigo                   
		AND ENRE.ENFA_Numero = ENFA.Numero                                        
                                   
		LEFT JOIN Vehiculos VEHI                                        
		ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                   
		AND ENRE.VEHI_Codigo = VEHI.Codigo                                        
                                     
		LEFT JOIN Terceros TENE                                        
		ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo                   
		AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo                                        
                                 
		LEFT JOIN Semirremolques SEMI                                        
		ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo                   
		AND ENRE.SEMI_Codigo = SEMI.Codigo                                   
                                        
		LEFT JOIN Encabezado_Planilla_Despachos ENPD                                        
		ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                       
		AND ENRE.ENPD_Numero = ENPD.Numero                                        
                                        
		LEFT JOIN Encabezado_Manifiesto_Carga ENMC                                        
		ON ENRE.EMPR_Codigo = ENMC.EMPR_Codigo       
		AND ENRE.ENMC_Numero = ENMC.Numero                                        
                                        
		LEFT JOIN Encabezado_Cumplido_Planilla_Despachos ECPD                                        
		ON ENRE.EMPR_Codigo = ECPD.EMPR_Codigo       
		AND ENRE.ECPD_Numero = ECPD.Numero                   
           
		LEFT JOIN Detalle_Despacho_Orden_Servicios AS DDOS       
		ON ENRE.EMPR_Codigo = DDOS.EMPR_Codigo        
		AND ENRE.Numero = DDOS.ENRE_Numero        
        
		LEFT JOIN Linea_Negocio_Transporte_Carga AS LNTC      
		ON DDOS.EMPR_Codigo = LNTC.EMPR_Codigo        
		AND DDOS.LNTC_Codigo = LNTC.Codigo        
       
		LEFT JOIN Tercero_Direcciones As TEDI     
		ON ENRE.EMPR_Codigo = TEDI.EMPR_Codigo    
		AND ENRE.TEDI_Codigo = TEDI.Codigo    
            
		WHERE                                        
		ENRE.EMPR_Codigo = @par_EMPR_Codigo                                        
		AND ENRE.TIDO_Codigo = 100                               
		AND ENRE.Anulado = ISNULL (@par_Anulado,ENRE.Anulado)
		AND(CONCAT('%', RTRIM(LTRIM(ISNULL(@par_Cliente,ISNULL(CLIE.Razon_Social, '') + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '')))), '%') LIKE                     
		CONCAT('%', RTRIM(LTRIM(ISNULL(@par_Cliente,ISNULL(CLIE.Razon_Social, '') + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '')))), '%'))                              
		AND (TENE.Razon_Social + ' ' +TENE.Apellido1 + ' ' + TENE.Apellido2 + ' ' + TENE.Nombre LIKE '%'+@par_Tenedor+'%' or @par_Tenedor is null)                      
		AND (COND.Nombre + ' ' +COND.Apellido1 + ' ' + COND.Apellido2 + ' ' + COND.Razon_Social LIKE '%'+@par_Conductor+'%' or @par_Conductor is null)                           
		AND ENRE.Numero_Documento = ISNULL(@par_Numero, ENRE.Numero_Documento)                           
		AND ENRE.Fecha BETWEEN ISNULL(@par_Fecha_Inicial, ENRE.Fecha) AND ISNULL(@par_Fecha_Final, ENRE.Fecha)                    
		AND ENRE.ESOS_Numero = ISNULL(@par_ESOS_Numero,ENRE.ESOS_Numero)                               
		AND ENRE.CATA_TIRE_Codigo = ISNULL(@par_CATA_TIRE_Codigo,ENRE.CATA_TIRE_Codigo)                            
		AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                   
		AND ENRE.Estado = ISNULL(@par_Estado,ENRE.Estado)                                        
		AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                        
		AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                             
		AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_CLIE_Codigo, ENRE.TERC_Codigo_Cliente)                       
		AND (ENRE.Documento_Cliente  LIKE'%'+@par_Documento_Cliente+'%' OR @par_Documento_Cliente IS NULL)          
		AND (ENRE.Numero_Contenedor = @par_Numero_Contenedor or @par_Numero_Contenedor is null)                    
		AND (ESOS.Numero_Documento = @par_ESOS_Numero_Documento OR @par_ESOS_Numero_Documento IS NULL)                       
		AND (ENPD.Numero_Documento = @par_ENPD_Numero_Documento OR @par_ENPD_Numero_Documento IS NULL)                      
		AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                    
		AND (ISNULL(ENRE.ENFA_Numero,0) = @par_PendienteFacturadas OR @par_PendienteFacturadas IS NULL  )                         
		AND ENRE.CIUD_Codigo_Destinatario = ISNULL(@par_CIUD_Codigo_Destino, ENRE.CIUD_Codigo_Destinatario)                       
		AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_CIUD_Codigo_origen, ENRE.CIUD_Codigo_Remitente)                                                   
		AND ( ENMC.Numero_Documento = @par_ENMC_Numero OR @par_ENMC_Numero IS NULL  )                                    
		AND TENE.Numero_Identificacion = ISNULL(@par_Tenedor,TENE.Numero_Identificacion)                             
		AND VEHI.Placa = ISNULL(@par_Placa, VEHI.Placa)                      
		AND COND.Numero_Identificacion = ISNULL(@par_Conductor,COND.Numero_Identificacion)                        
		AND  CLIE.Numero_Identificacion = ISNULL(@par_Cliente,CLIE.Numero_Identificacion)      
		AND (ENRE.TEDI_Codigo = @par_TEDI_Codigo OR @par_TEDI_Codigo IS NULL)  
		AND ENRE.OFIC_Codigo IN (
			SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE USOF.USUA_Codigo = @par_Usuario_Consulta
		)
	);                                        
	WITH Pagina                                        
	AS                                        
	(
		SELECT                           
		ENRE.EMPR_Codigo,                                        
		ENRE.Numero,                                        
		ENRE.TIDO_Codigo,                                        
		TIDO.Nombre AS TipoDocumento,                                        
		ENRE.Numero_Documento,                                        
		ENRE.CATA_TIRE_Codigo,                                        
		TIRE.Nombre AS TipoRemesa,                                        
		ISNULL(ENRE.Documento_Cliente,'') AS Documento_Cliente,                                        
		ISNULL(ENRE.Fecha_Documento_Cliente,'') AS Fecha_Documento_Cliente,                                        
		ENRE.Fecha,                                        
		ENRE.RUTA_Codigo,                                        
		RUTA.Nombre AS NombreRuta,                                        
		ENRE.PRTR_Codigo,                                        
		PRTR.Nombre AS NombreProducto,                                        
		ENRE.CATA_FOPR_Codigo,                                        
		ENRE.TERC_Codigo_Cliente,                                        
		CONCAT(ISNULL(REMI.Razon_Social,''),CLIE.Nombre,' ',CLIE.Apellido1,' ',CLIE.Apellido2) AS NombreCliente,                                        
		CLIE.Numero_Identificacion AS IdentificacionCliente,                                        
		ENRE.TERC_Codigo_Remitente,                                        
		CONCAT(ISNULL(REMI.Razon_Social,''),REMI.Nombre,' ',REMI.Apellido1,' ',REMI.Apellido2) AS Remitente,                                        
		REMI.Numero_Identificacion AS IdentificacionRemitente,                                        
		ENRE.CIUD_Codigo_Remitente,                                        
		CIUD.Nombre AS CiudadRemitente,                                        
		ENRE.Direccion_Remitente,                                        
		ENRE.Telefonos_Remitente,                                        
		ENRE.Observaciones,                                        
		ENRE.Cantidad_Cliente,                                        
		ENRE.Peso_Cliente,                                        
		ENRE.Peso_Volumetrico_Cliente,                                        
		0 as DTCV_Codigo,                                  
		0 AS TarifarioVenta,                                        
		ENRE.Valor_Flete_Cliente,                                        
		ENRE.Valor_Manejo_Cliente,                                        
		ENRE.Valor_Seguro_Cliente,                                        
		ENRE.Valor_Descuento_Cliente,                                        
		ENRE.Total_Flete_Cliente,                                        
		ENRE.Valor_Comercial_Cliente,                                        
		ENRE.Cantidad_Transportador,                                        
		ENRE.Peso_Transportador,                                        
		ENRE.Valor_Flete_Transportador,                                        
		ENRE.Total_Flete_Transportador,                                        
		ENRE.TERC_Codigo_Destinatario,                                        
		ENRE.CIUD_Codigo_Destinatario,                                        
		CIDE.Nombre AS CiudadDestinatario,                                        
		ENRE.Direccion_Destinatario,                                        
		ENRE.Telefonos_Destinatario,                                        
		ENRE.Anulado,                                        
		ENRE.Estado,                                        
		ENRE.Fecha_Crea,                                        
		ENRE.USUA_Codigo_Crea,                                        
		ENRE.Fecha_Modifica,                                        
		ENRE.USUA_Codigo_Modifica,                          
		ENRE.Fecha_Anula,                                        
		ENRE.USUA_Codigo_Anula,                                        
		ENRE.Causa_Anula,                                        
		ENRE.OFIC_Codigo,        
		OFIC.Nombre AS NombreOficina,                                        
		ENRE.ETCC_Numero,                                        
		ENRE.ETCV_Numero,                                        
		ENRE.ESOS_Numero,                                        
		ESOS.Numero_Documento AS OrdenServicio,                              
		ENRE.ENPD_Numero AS NumeroPlanilla,                                        
		ISNULL(ENPD.Numero_Documento,0) AS NumeroDocumentoPlanilla,                                        
		ENRE.ENMC_Numero AS NumeroManifiesto,                                        
		ISNULL(ENMC.Numero_Documento,0) AS NumeroDocumentoManifiesto,                                        
		ISNULL(ENRE.ECPD_Numero,0) AS ECPD_Numero,                                        
		ISNULL(ECPD.Numero_Documento,0) AS NumeroCumplido,                                        
		ISNULL(ENRE.ENFA_Numero,0) AS ENFA_Numero,                                        
		ISNULL(ENFA.Numero_Documento,0) AS NumeroFactura,          
		ENRE.VEHI_Codigo,                                        
		VEHI.Placa,                                        
		ENRE.TERC_Codigo_Conductor,                                        
		CONCAT(COND.Nombre,' ',COND.Apellido1,' ',COND.Apellido2) AS NombreConductor,                                        
		COND.Numero_Identificacion AS IdentificacionConductor,                                        
		COND.Telefonos AS TelefonoConductor,                                        
		VEHI.TERC_Codigo_Tenedor,                                        
		CONCAT(TENE.Nombre,' ',TENE.Apellido1,' ',TENE.Apellido2) AS NombreTenedor,                                        
		TENE.Numero_Identificacion AS IdentificacionTenedor,                                    
		ENRE.SEMI_Codigo,                                  
		ISNULL(SEMI.Placa,'') AS PlacaSemirremolque,                               
		ISNULL(PRTR.UEPT_Codigo, 0) AS Unidad_Empaque,                             
		ISNULL(ENRE.Distribucion,0) AS Distribucion,                    
		DDOS.LNTC_Codigo,        
		LNTC.Nombre as LineaNegocio,        
		TEDI.Nombre As NombreSede,    
    
		ROW_NUMBER() OVER (ORDER BY ENRE.Numero DESC) AS RowNumber                                        
		FROM                                        
		Encabezado_Remesas ENRE LEFT JOIN Tipo_Documentos TIDO                                        
		ON ENRE.EMPR_Codigo = TIDO.EMPR_Codigo                   
		AND ENRE.TIDO_Codigo = TIDO.Codigo                                           
                                           
		LEFT JOIN V_Tipo_Remesas TIRE                                        
		ON ENRE.EMPR_Codigo = TIRE.EMPR_Codigo                   
		AND ENRE.CATA_TIRE_Codigo = TIRE.Codigo                   
                                        
		LEFT JOIN Rutas RUTA                                        
		ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                   
		AND ENRE.RUTA_Codigo = RUTA.Codigo                                        
                                        
		LEFT JOIN Producto_Transportados PRTR                                        
		ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                   
		AND ENRE.PRTR_Codigo = PRTR.Codigo                                        
                                        
		LEFT JOIN Terceros CLIE                                        
		ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                   
		AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                        
                                        
		LEFT JOIN Terceros REMI                                        
		ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                   
		AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                        
                                        
		LEFT JOIN Ciudades CIUD                                        
		ON ENRE.EMPR_Codigo = CIUD.EMPR_Codigo                   
		AND ENRE.CIUD_Codigo_Remitente = CIUD.Codigo                                   
                   
		--LEFT JOIN Detalle_Tarifario_Carga_Ventas DTCV                                        
		--ON ENRE.EMPR_Codigo = DTCV.EMPR_Codigo AND ENRE.DTCV_Codigo = DTCV.Codigo                                        
                                        
		LEFT JOIN Ciudades CIDE                                        
		ON ENRE.EMPR_Codigo = CIDE.EMPR_Codigo                   
		AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo                                        
                                        
		LEFT JOIN Oficinas OFIC                                        
		ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                   
		AND ENRE.OFIC_Codigo = OFIC.Codigo                                   
                                
		LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS                                        
		ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo                   
		AND ENRE.ESOS_Numero = ESOS.Numero                                        
                     
		LEFT JOIN Terceros COND                                        
		ON ENRE.EMPR_Codigo = COND.EMPR_Codigo                   
		AND ENRE.TERC_Codigo_Conductor = COND.Codigo                            
                                  
		LEFT JOIN Encabezado_Facturas ENFA                                        
		ON ENRE.EMPR_Codigo = ENFA.EMPR_Codigo                   
		AND ENRE.ENFA_Numero = ENFA.Numero                                        
                                
		LEFT JOIN Vehiculos VEHI                                        
		ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                   
		AND ENRE.VEHI_Codigo = VEHI.Codigo                                        
                                       
                                 
		LEFT JOIN Semirremolques SEMI                                        
		ON ENRE.EMPR_Codigo = SEMI.EMPR_Codigo                   
		AND ENRE.SEMI_Codigo = SEMI.Codigo                                   
                                        
		LEFT JOIN Terceros TENE                                        
		ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo                   
		AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo                                        
                     
                  
		LEFT JOIN Encabezado_Planilla_Despachos ENPD                                        
		ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                       
		AND ENRE.ENPD_Numero = ENPD.Numero                                        
                                        
		LEFT JOIN Encabezado_Manifiesto_Carga ENMC                                        
		ON ENRE.EMPR_Codigo = ENMC.EMPR_Codigo AND ENRE.ENMC_Numero = ENMC.Numero                                        
                                        
		LEFT JOIN Encabezado_Cumplido_Planilla_Despachos ECPD                                        
		ON ENRE.EMPR_Codigo = ECPD.EMPR_Codigo AND ENRE.ECPD_Numero = ECPD.Numero          
        
		LEFT JOIN Detalle_Despacho_Orden_Servicios AS DDOS ON        
		ENRE.EMPR_Codigo = DDOS.EMPR_Codigo        
		AND ENRE.Numero = DDOS.ENRE_Numero        
        
		LEFT JOIN Linea_Negocio_Transporte_Carga AS LNTC ON        
		DDOS.EMPR_Codigo = LNTC.EMPR_Codigo        
		AND DDOS.LNTC_Codigo = LNTC.Codigo        
    
		LEFT JOIN Tercero_Direcciones As TEDI     
		ON ENRE.EMPR_Codigo = TEDI.EMPR_Codigo    
		AND ENRE.TEDI_Codigo = TEDI.Codigo    
    
		WHERE                                        
		ENRE.EMPR_Codigo = @par_EMPR_Codigo                                        
		AND ENRE.TIDO_Codigo = 100                            
		AND(CONCAT('%', RTRIM(LTRIM(ISNULL(@par_Cliente,ISNULL(CLIE.Razon_Social, '') + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '')))), '%') LIKE                     
		CONCAT('%', RTRIM(LTRIM(ISNULL(@par_Cliente,ISNULL(CLIE.Razon_Social, '') + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '')))), '%'))       
		AND (TENE.Nombre + ' ' +TENE.Apellido1 + ' ' + TENE.Apellido2 + ' ' + TENE.Razon_Social LIKE '%'+@par_Tenedor+'%' or @par_Tenedor is null)                      
		AND (COND.Nombre + ' ' +COND.Apellido1 + ' ' + COND.Apellido2 + ' ' + COND.Razon_Social LIKE '%'+@par_Conductor+'%' or @par_Conductor is null)                           
		AND ENRE.Numero_Documento = ISNULL(@par_Numero, ENRE.Numero_Documento)                                                           
		AND ENRE.Fecha BETWEEN ISNULL(@par_Fecha_Inicial, ENRE.Fecha) AND ISNULL(@par_Fecha_Final, ENRE.Fecha)                    
		AND ENRE.ESOS_Numero = ISNULL(@par_ESOS_Numero,ENRE.ESOS_Numero)                                        
		AND ENRE.CATA_TIRE_Codigo = ISNULL(@par_CATA_TIRE_Codigo,ENRE.CATA_TIRE_Codigo)                            
		AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)                                   
		AND ENRE.Estado = ISNULL(@par_Estado,ENRE.Estado)                                        
		AND ENRE.Cumplido = ISNULL(@par_Cumplido, ENRE.Cumplido)                                        
		AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                        
		AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_CLIE_Codigo, ENRE.TERC_Codigo_Cliente)                       
		AND (ENRE.Documento_Cliente  LIKE'%'+@par_Documento_Cliente+'%' OR @par_Documento_Cliente IS NULL)          
		AND (ENRE.Numero_Contenedor = @par_Numero_Contenedor or @par_Numero_Contenedor is null)                    
		AND (ESOS.Numero_Documento = @par_ESOS_Numero_Documento OR @par_ESOS_Numero_Documento IS NULL)                       
		AND (ENPD.Numero_Documento = @par_ENPD_Numero_Documento OR @par_ENPD_Numero_Documento IS NULL)                                             
		AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)                                    
		--AND ENPD.Numero_Documento = ISNULL(@par_ENPD_Numero, ENPD.Numero_Documento)                                    
		AND (ISNULL(ENRE.ENFA_Numero,0) = @par_PendienteFacturadas OR @par_PendienteFacturadas IS NULL  )                         
		AND ENRE.CIUD_Codigo_Destinatario = ISNULL(@par_CIUD_Codigo_Destino, ENRE.CIUD_Codigo_Destinatario)                       
		AND ENRE.CIUD_Codigo_Remitente = ISNULL(@par_CIUD_Codigo_origen, ENRE.CIUD_Codigo_Remitente)                   
		AND ( ENMC.Numero_Documento = @par_ENMC_Numero OR @par_ENMC_Numero IS NULL  )                                    
		AND TENE.Numero_Identificacion = ISNULL(@par_Tenedor,TENE.Numero_Identificacion)                             
		AND VEHI.Placa = ISNULL(@par_Placa, VEHI.Placa)                      
		AND COND.Numero_Identificacion = ISNULL(@par_Conductor,COND.Numero_Identificacion)                        
		AND  CLIE.Numero_Identificacion = ISNULL(@par_Cliente,CLIE.Numero_Identificacion)                          
		AND (ENRE.TEDI_Codigo = @par_TEDI_Codigo OR @par_TEDI_Codigo IS NULL)
		AND ENRE.Anulado = ISNULL (@par_Anulado,ENRE.Anulado)
		AND ENRE.OFIC_Codigo IN (
			SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE USOF.USUA_Codigo = @par_Usuario_Consulta
		)
	)                                        
	SELECT DISTINCT                                        
	0 AS Obtener,                                        
	EMPR_Codigo,                                        
	Numero,                                        
	TIDO_Codigo,                                        
	TipoDocumento,                              
	Numero_Documento,                                        
	CATA_TIRE_Codigo,                                        
	TipoRemesa,                                        
	Documento_Cliente,                                        
	Fecha_Documento_Cliente,                                        
	Fecha,                                        
	RUTA_Codigo,                                        
	NombreRuta,              
	PRTR_Codigo,                                        
	NombreProducto,                    
	CATA_FOPR_Codigo,                                        
	TERC_Codigo_Cliente,                                        
	NombreCliente,                                        
	IdentificacionCliente,                                        
	TERC_Codigo_Remitente,                                        
	Remitente,                                        
	IdentificacionRemitente,                                        
	CIUD_Codigo_Remitente,                                        
	CiudadRemitente,                                        
	Direccion_Remitente,                                        
	Telefonos_Remitente,                                        
	Observaciones,                                        
	Cantidad_Cliente,                                        
	Peso_Cliente,             
	Peso_Volumetrico_Cliente,                                        
	DTCV_Codigo,                                        
	TarifarioVenta,                                        
	Valor_Flete_Cliente,                                        
	Valor_Manejo_Cliente,                                        
	Valor_Seguro_Cliente,                                        
	Valor_Descuento_Cliente,                                        
	Total_Flete_Cliente,                                        
	Valor_Comercial_Cliente,                                        
	Cantidad_Transportador,                                        
	Peso_Transportador,                                        
	Valor_Flete_Transportador,                                        
	Total_Flete_Transportador,                                        
	TERC_Codigo_Destinatario,                                        
	CIUD_Codigo_Destinatario,                                        
	CiudadDestinatario,                             
	Direccion_Destinatario,                                        
	Telefonos_Destinatario,                                        
	Anulado,                                        
	Estado,                                        
	Fecha_Crea,                                        
	USUA_Codigo_Crea,                                        
	Fecha_Modifica,                                        
	USUA_Codigo_Modifica,                                        
	Fecha_Anula,                                        
	USUA_Codigo_Anula,                                        
	Causa_Anula,                                        
	OFIC_Codigo,                                        
	NombreOficina,                                        
	ETCC_Numero,                                        
	ETCV_Numero,                                        
	ESOS_Numero,                                        
	OrdenServicio,                                        
	NumeroPlanilla,            
	NumeroDocumentoPlanilla,                                        
	NumeroManifiesto,                                        
	NumeroDocumentoManifiesto,                                        
	ECPD_Numero,                                        
	NumeroCumplido,                                        
	ENFA_Numero,                                        
	NumeroFactura,                                        
	VEHI_Codigo,                                        
	Placa,                                        
	SEMI_Codigo,                              
	PlacaSemirremolque,                              
	TERC_Codigo_Conductor,                          
	NombreConductor,                                        
	IdentificacionConductor,                                        
	TelefonoConductor,                                        
	TERC_Codigo_Tenedor,                                        
	NombreTenedor,                                        
	IdentificacionTenedor,                              
	Unidad_Empaque,                             
	Distribucion,                                   
	LNTC_Codigo,        
	LineaNegocio,       
	NombreSede,     
	@CantidadRegistros AS TotalRegistros,                                        
	@par_NumeroPagina AS PaginaObtener,                             
	@par_RegistrosPagina AS RegistrosPagina                                        
	FROM Pagina                         WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                        
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                        
	ORDER BY Numero                                        
END
GO