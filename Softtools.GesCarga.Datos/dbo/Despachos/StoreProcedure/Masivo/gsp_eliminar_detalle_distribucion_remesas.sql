﻿PRINT 'gsp_eliminar_detalle_distribucion_remesas'
GO
DROP PROCEDURE gsp_eliminar_detalle_distribucion_remesas
GO
CREATE PROCEDURE gsp_eliminar_detalle_distribucion_remesas 
(      
  @par_EMPR_Codigo SMALLINT,      
  @par_Codigo NUMERIC,
  @par_ENRE_Numero NUMERIC      
)      
AS      
BEGIN      
      
  DELETE Detalle_Distribucion_Remesas       
  WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_Codigo      
      
    SELECT @@ROWCOUNT AS Codigo 
	
	DECLARE @CantidadDistricuciones INT

SELECT @CantidadDistricuciones = (   
SELECT COUNT(*) FROM Detalle_Distribucion_Remesas
WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_ENRE_Numero        
)
SELECT @CantidadDistricuciones
IF 	@CantidadDistricuciones = 0	
UPDATE Encabezado_Remesas SET Distribucion = 0 WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_ENRE_Numero   
END       
     
GO