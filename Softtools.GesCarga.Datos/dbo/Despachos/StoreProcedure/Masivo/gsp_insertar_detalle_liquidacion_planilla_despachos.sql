﻿PRINT 'gsp_insertar_detalle_liquidacion_planilla_despachos'
GO

DROP PROCEDURE gsp_insertar_detalle_liquidacion_planilla_despachos
GO

CREATE PROCEDURE gsp_insertar_detalle_liquidacion_planilla_despachos
(
	@par_EMPR_Codigo SMALLINT,
	@par_ELPD_Numero NUMERIC,
	@par_CLPD_Codigo NUMERIC,
	@par_Observaciones VARCHAR(500),
	@par_Valor MONEY
)
AS
BEGIN
	INSERT INTO
		Detalle_Liquidacion_Planilla_Despachos
		(
			EMPR_Codigo,
			ELPD_Numero,
			CLPD_Codigo,
			Observaciones,
			Valor
		)
	VALUES
	(
		@par_EMPR_Codigo,
		@par_ELPD_Numero,
		@par_CLPD_Codigo,
		@par_Observaciones,
		@par_Valor
	)

	SELECT ID = @@identity
END
GO