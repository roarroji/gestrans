﻿PRINT 'gsp_insertar_encabezado_documento_cuentas'
GO

DROP PROCEDURE gsp_insertar_encabezado_documento_cuentas
GO

CREATE PROCEDURE gsp_insertar_encabezado_documento_cuentas
(
	@par_EMPR_Codigo NUMERIC,
	@par_TIDO_Codigo NUMERIC,
	@par_Codigo_Alterno VARCHAR(20),
	@par_Fecha DATE,
	@par_TERC_Codigo NUMERIC,
	@par_CATA_DOOR_Codigo NUMERIC,
	@par_Codigo_Documento_Origen NUMERIC,
	@par_Documento_Origen NUMERIC,
	@par_Fecha_Documento_Origen DATE,
	@par_Fecha_Vence_Documento_Origen DATE,
	@par_Numeracion VARCHAR(20),
	@par_Observaciones VARCHAR(250),
	@par_PLUC_Codigo SMALLINT,
	@par_Valor_Total MONEY,
	@par_Abono MONEY,
	@par_Saldo MONEY,
	@par_Fecha_Cancelacion_Pago DATE,
	@par_Aprobado SMALLINT,
	@par_USUA_Codigo_Aprobo SMALLINT,
	@par_Fecha_Aprobo DATETIME,
	@par_OFIC_Codigo SMALLINT,
	@par_VEHI_Codigo NUMERIC,
	@par_USUA_Codigo_Crea NUMERIC
)
AS
BEGIN
	
	DECLARE @NumeroGenerado NUMERIC
	EXEC gsp_generar_consecutivo @par_EMPR_Codigo,30,@par_OFIC_Codigo,@NumeroGenerado OUTPUT

	INSERT INTO
		Encabezado_Documento_Cuentas
		(
			EMPR_Codigo,
			Numero,
			TIDO_Codigo,
			Codigo_Alterno,
			Fecha,
			TERC_Codigo,
			CATA_DOOR_Codigo,
			Codigo_Documento_Origen,
			Documento_Origen,
			Fecha_Documento_Origen,
			Fecha_Vence_Documento_Origen,
			Numeracion,
			Observaciones,
			PLUC_Codigo,
			Valor_Total,
			Abono,
			Saldo,
			Fecha_Cancelacion_Pago,
			Aprobado,
			USUA_Codigo_Aprobo,
			Fecha_Aprobo,
			OFIC_Codigo,
			VEHI_Codigo,
			Anulado,
			Estado,
			USUA_Codigo_Crea,
			Fecha_Crea
		)
	VALUES
	(
		@par_EMPR_Codigo,
		@NumeroGenerado,
		@par_TIDO_Codigo,
		@par_Codigo_Alterno,
		@par_Fecha,
		@par_TERC_Codigo,
		@par_CATA_DOOR_Codigo,
		@par_Codigo_Documento_Origen,
		@par_Documento_Origen,
		@par_Fecha_Documento_Origen,
		@par_Fecha_Vence_Documento_Origen,
		@par_Numeracion,
		@par_Observaciones,
		@par_PLUC_Codigo,
		@par_Valor_Total,
		@par_Abono,
		@par_Saldo,
		@par_Fecha_Cancelacion_Pago,
		@par_Aprobado,
		@par_USUA_Codigo_Aprobo,
		@par_Fecha_Aprobo,
		@par_OFIC_Codigo,
		@par_VEHI_Codigo,
		0,--Anulado
		1,--Estado
		@par_USUA_Codigo_Crea,
		GETDATE()
	)

	SELECT Codigo = @@identity

END
GO