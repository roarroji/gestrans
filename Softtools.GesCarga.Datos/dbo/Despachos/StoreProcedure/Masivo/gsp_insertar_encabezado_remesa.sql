﻿Print 'gsp_insertar_encabezado_remesas'
GO
DROP PROCEDURE gsp_insertar_encabezado_remesas
GO
CREATE PROCEDURE gsp_insertar_encabezado_remesas (
@par_EMPR_Codigo					smallint,
@par_TIDO_Codigo					numeric,

@par_CATA_TIRE_Codigo				int,
@par_Documento_Cliente				varchar(30),
@par_Fecha_Documento_Cliente		date,
@par_Fecha							date,
@par_RUTA_Codigo					numeric,
@par_PRTR_Codigo					numeric,
@par_CATA_FOPR_Codigo				numeric,
@par_TERC_Codigo_Cliente			numeric,
@par_TERC_Codigo_Remitente			numeric,
@par_CIUD_Codigo_Remitente			numeric,
@par_Direccion_Remitente			varchar(150),
@par_Telefonos_Remitente			varchar(100),
@par_Observaciones					varchar(100),
@par_Cantidad_Cliente				numeric (18, 2),
@par_Peso_Cliente					numeric (18, 2),
@par_Peso_Volumetrico_Cliente		numeric (18, 2),
@par_DTCV_Codigo					numeric,
@par_Valor_Flete_Cliente			money,
@par_Valor_Manejo_Cliente			money,
@par_Valor_Seguro_Cliente			money,
@par_Valor_Descuento_Cliente		money,
@par_Total_Flete_Cliente			money,
@par_Valor_Comercial_Cliente		money,
@par_Cantidad_Transportador			numeric (18, 2),
@par_Peso_Transportador				numeric (18, 2),
@par_Valor_Flete_Transportador		money,
@par_Total_Flete_Transportador		money,
@par_TERC_Codigo_Destinatario		numeric,
@par_CIUD_Codigo_Destinatario		numeric,
@par_Direccion_Destinatario			varchar(150),
@par_Telefonos_Destinatario			varchar(100),
@par_Estado							smallint,
@par_USUA_Codigo_Crea				smallint,
@par_OFIC_Codigo					smallint,
@par_Numeracion						varchar(50),
@par_ETCC_Numero					numeric,
@par_ETCV_Numero					numeric,
@par_ESOS_Numero					numeric,
@par_VEHI_Codigo					numeric,
@par_TERC_Codigo_Conductor			numeric
)
AS
BEGIN

	DECLARE @numNumeroDocumento NUMERIC = 0
	DECLARE @Numero NUMERIC = 0

	SELECT @Numero = MAX(ISNULL(Numero,0) + 1) FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo
	
	EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_TIDO_Codigo, @par_OFIC_Codigo, @numNumeroDocumento OUTPUT

		INSERT INTO Encabezado_Remesas (
		EMPR_Codigo,
		Numero,
		TIDO_Codigo,
		Numero_Documento,
		CATA_TIRE_Codigo,
		Documento_Cliente,
		--5

		Fecha_Documento_Cliente,
		Fecha,
		RUTA_Codigo,
		PRTR_Codigo,
		CATA_FOPR_Codigo,
		--10

		TERC_Codigo_Cliente,
		TERC_Codigo_Remitente,
		CIUD_Codigo_Remitente,
		Direccion_Remitente,
		Telefonos_Remitente,
		--15

		Observaciones,
		Cantidad_Cliente,
		Peso_Cliente,
		Peso_Volumetrico_Cliente,
		DTCV_Codigo,
		--20

		Valor_Flete_Cliente,
		Valor_Manejo_Cliente,
		Valor_Seguro_Cliente,
		Valor_Descuento_Cliente,
		Total_Flete_Cliente,
		--25

		Valor_Comercial_Cliente,
		Cantidad_Transportador,
		Peso_Transportador,
		Valor_Flete_Transportador,
		Total_Flete_Transportador,
		--30

		TERC_Codigo_Destinatario,
		CIUD_Codigo_Destinatario,
		Direccion_Destinatario,
		Telefonos_Destinatario,
		Anulado,
		--35

		Estado,
		Fecha_Crea,
		USUA_Codigo_Crea,
		Fecha_Modifica,
		USUA_Codigo_Modifica,
		--40

		Fecha_Anula,
		USUA_Codigo_Anula,
		Causa_Anula,
		OFIC_Codigo,
		Numeracion,
		--45

		ETCC_Numero,
		ETCV_Numero,
		ESOS_Numero,
		ENPD_Numero,
		ENMC_Numero,
		--50

		ENCU_Numero,
		ENFA_Numero,
		VEHI_Codigo,
		TERC_Codigo_Conductor
		--54
		)

		VALUES (
		@par_EMPR_Codigo,
		@Numero,
		@par_TIDO_Codigo,
		@numNumeroDocumento,
		@par_CATA_TIRE_Codigo,
		@par_Documento_Cliente,
		--5

		@par_Fecha_Documento_Cliente,
		@par_Fecha,
		@par_RUTA_Codigo,
		@par_PRTR_Codigo,
		@par_CATA_FOPR_Codigo,
		--10

		@par_TERC_Codigo_Cliente,
		@par_TERC_Codigo_Remitente,
		@par_CIUD_Codigo_Remitente,
		@par_Direccion_Remitente,
		@par_Telefonos_Remitente,
		--15

		@par_Observaciones,
		@par_Cantidad_Cliente,
		@par_Peso_Cliente,
		@par_Peso_Volumetrico_Cliente,
		@par_DTCV_Codigo,
		--20

		@par_Valor_Flete_Cliente,
		@par_Valor_Manejo_Cliente,
		@par_Valor_Seguro_Cliente,
		@par_Valor_Descuento_Cliente,
		@par_Total_Flete_Cliente,
		--25

		@par_Valor_Comercial_Cliente,
		@par_Cantidad_Transportador,
		@par_Peso_Transportador,
		@par_Valor_Flete_Transportador,
		@par_Total_Flete_Transportador,
		--30

		@par_TERC_Codigo_Destinatario,
		@par_CIUD_Codigo_Destinatario,
		@par_Direccion_Destinatario,
		@par_Telefonos_Destinatario,
		0,
		--35

		@par_Estado,
		GETDATE(),
		@par_USUA_Codigo_Crea,
		NULL,
		NULL,
		--40

		NULL,
		NULL,
		'',
		@par_OFIC_Codigo,
		@par_Numeracion,
		--45

		@par_ETCC_Numero,
		@par_ETCV_Numero,
		@par_ESOS_Numero,
		0,
		0,
		--50

		0,
		0,
		@par_VEHI_Codigo,
		@par_TERC_Codigo_Conductor
		--54

		)

		SELECT Numero, Numero_Documento FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo
		AND Numero_Documento = @numNumeroDocumento
		AND TIDO_Codigo = @par_TIDO_Codigo

END
GO