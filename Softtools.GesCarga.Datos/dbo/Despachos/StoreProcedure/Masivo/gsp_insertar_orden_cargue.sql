﻿PRINT 'gsp_insertar_orden_cargue'
GO
DROP PROCEDURE gsp_insertar_orden_cargue
GO
---------  70 % del desarrollo  --------------  
CREATE PROCEDURE gsp_insertar_orden_cargue 
(                  
@par_EMPR_Codigo                  SMALLINT,               
@par_TIDO_Codigo                  NUMERIC ,              
@par_ESOS_Numero                  NUMERIC,              
@par_Fecha                        DATE ,              
@par_TERC_Codigo_Cliente            NUMERIC ,              
@par_TERC_Codigo_Remitente          NUMERIC,               
@par_VEHI_Codigo                  NUMERIC ,              
@par_TERC_Codigo_Conductor          NUMERIC,              
@par_CIUD_Codigo_Cargue           NUMERIC ,              
@par_Direccion_Cargue             VARCHAR (150) ,              
@par_Telefonos_Cargue             VARCHAR (100) ,              
@par_Contacto_Cargue                VARCHAR (50) ,              
@par_Observaciones                VARCHAR (250) ,              
@par_RUTA_Codigo                  NUMERIC,              
@par_PRTR_Codigo                  NUMERIC,              
@par_Cantidad_Cliente             NUMERIC,              
@par_Peso_Cliente                 NUMERIC(18,2),              
@par_Valor_Anticipo               MONEY,              
@par_CIUD_Codigo_Descargue        NUMERIC,              
@par_Direccion_Descargue          VARCHAR (150) ,              
@par_Telefono_Descargue           VARCHAR (100) ,              
@par_Contacto_Descargue           VARCHAR (50) ,              
@par_Estado                       SMALLINT ,              
@par_USUA_Codigo_Crea             SMALLINT ,                
@par_OFIC_Codigo                  SMALLINT ,              
@par_Numeracion                   VARCHAR (50) ,            
@par_SEMI_Codigo NUMERIC  = NULL          
)                 
AS BEGIN                 
                
  DECLARE @numNumeroDocumento NUMERIC = 0              
  DECLARE @numNumero NUMERIC = 0              
              
  EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_TIDO_Codigo, @par_OFIC_Codigo, @numNumeroDocumento OUTPUT              
                
  INSERT INTO Encabezado_Orden_Cargues(              
       EMPR_Codigo              
       ,TIDO_Codigo              
       ,Numero_Documento              
       ,ESOS_Numero                
        ,Fecha                
        ,TERC_Codigo_Cliente                
        ,TERC_Codigo_Remitente                
        ,VEHI_Codigo                
        ,TERC_Codigo_Conductor                
        ,CIUD_Codigo_Cargue                
        ,Direccion_Cargue                
        ,Telefonos_Cargue                
        ,Contacto_Cargue                
        ,Observaciones                
        ,RUTA_Codigo                
        ,PRTR_Codigo                
        ,Cantidad_Cliente                
        ,Peso_Cliente                
        ,Valor_Anticipo                
        ,CIUD_Codigo_Descargue                
        ,Direccion_Descargue                
        ,Telefono_Descargue                
        ,Contacto_Descargue                
                 
        ,Estado                
        ,Fecha_Crea                
        ,USUA_Codigo_Crea                
                 
        ,OFIC_Codigo                
        ,Numeracion                
        ,ENMC_Numero                
        ,ENPD_Numero            
       ,Anulado                
    ,SEMI_Codigo            
         )                  
         VALUES(                
        @par_EMPR_Codigo              
       ,@par_TIDO_Codigo                 
       ,@numNumeroDocumento                 
       ,@par_ESOS_Numero                 
       ,@par_Fecha                 
       ,@par_TERC_Codigo_Cliente                 
       ,@par_TERC_Codigo_Remitente                 
       ,@par_VEHI_Codigo                 
       ,@par_TERC_Codigo_Conductor                 
       ,@par_CIUD_Codigo_Cargue                 
       ,@par_Direccion_Cargue                 
       ,@par_Telefonos_Cargue                
       ,@par_Contacto_Cargue                 
       ,@par_Observaciones                 
       ,@par_RUTA_Codigo                 
       ,@par_PRTR_Codigo                 
       ,@par_Cantidad_Cliente                 
       ,@par_Peso_Cliente                 
       ,@par_Valor_Anticipo                 
       ,@par_CIUD_Codigo_Descargue                 
       ,@par_Direccion_Descargue                  
       ,@par_Telefono_Descargue                  
       ,@par_Contacto_Descargue                  
       ,@par_Estado                 
 ,GETDATE()                
       ,@par_USUA_Codigo_Crea                 
                 
       ,@par_OFIC_Codigo                 
       ,@par_Numeracion                  
       ,0                 
       ,0              
       ,0            
    ,@par_SEMI_Codigo            
         )              
  SET @numNumero = @@IDENTITY              
        
  INSERT INTO Detalle_Seguimiento_Vehiculos (        
 EMPR_Codigo,        
 ENPD_Numero,        
 ENOC_Numero,        
 ENMC_Numero,        
 Fecha_Reporte,        
 CATA_TOSV_Codigo,        
 Ubicacion,        
 Longitud,        
 Latitud,        
 PUCO_Codigo,        
 CATA_SRSV_Codigo,        
 CATA_NOSV_Codigo,        
 Observaciones,        
 Kilometros_Vehiculo,        
 Reportar_Cliente,        
 Envio_Reporte_Cliente,        
 Fecha_Reporte_Cliente,        
 USUA_Codigo_Crea,        
 Fecha_Crea,        
 Anulado,        
 Orden)        
 VALUES         
 (        
 @par_EMPR_Codigo,        
 0,        
 @numNumero,        
 0,        
 getdate(),        
 8001,        
 '',        
 0,        
 0,        
 0,        
 8201,        
 8100,        
 'ORDEN DE CARGUE',        
 0,        
 0,        
 0,        
 '',        
 @par_USUA_Codigo_Crea,        
 GETDATE(),        
 0,        
 1        
    
    
 )        
     
 UPDATE Vehiculos SET Fecha_Ultimo_Cargue = GETDATE()     
 WHERE EMPR_Codigo = @par_EMPR_Codigo     
 AND Codigo = @par_VEHI_Codigo      
    
    
  SELECT Numero, Numero_Documento FROM Encabezado_Orden_Cargues WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @numNumero              
END  
  
GO  