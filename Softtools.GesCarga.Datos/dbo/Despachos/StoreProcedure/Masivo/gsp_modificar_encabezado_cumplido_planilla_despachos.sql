﻿PRINT 'gsp_modificar_encabezado_cumplido_planilla_despachos'
GO

DROP PROCEDURE gsp_modificar_encabezado_cumplido_planilla_despachos
GO

CREATE PROCEDURE gsp_modificar_encabezado_cumplido_planilla_despachos
(
	@par_EMPR_Codigo NUMERIC,
	@par_Numero NUMERIC,
	@par_Fecha DATE,
	@par_Fecha_Entrega DATETIME,
	@par_Fecha_Inicio_Cargue DATETIME,
	@par_Fecha_Fin_Cargue DATETIME,
	@par_Fecha_Inicio_Descargue DATETIME,
	@par_Fecha_Fin_Descargue DATETIME,
	@par_Valor_Multa_Extemporaneo MONEY,
	@par_Nombre_Entrego_Cumplido VARCHAR(50),
	@par_Observaciones VARCHAR(500),
	@par_Estado SMALLINT,
	@par_USUA_Codigo_Modifica SMALLINT,
	@par_OFIC_Codigo SMALLINT
)
AS
BEGIN
	UPDATE
		Encabezado_Cumplido_Planilla_Despachos
	SET
		Fecha						=	@par_Fecha,
		Fecha_Entrega				=	@par_Fecha_Entrega,
		Fecha_Inicio_Cargue			=	@par_Fecha_Inicio_Cargue,
		Fecha_Fin_Cargue			=	@par_Fecha_Fin_Cargue,
		Fecha_Inicio_Descargue		=	@par_Fecha_Inicio_Descargue,
		Fecha_Fin_Descargue			=	@par_Fecha_Fin_Descargue,
		Valor_Multa_Extemporaneo	=	@par_Valor_Multa_Extemporaneo,
		Nombre_Entrego_Cumplido		=	@par_Nombre_Entrego_Cumplido,
		Observaciones				=	@par_Observaciones,
		Estado						=	@par_Estado,
		Fecha_Modifica				=	GETDATE(),
		USUA_Codigo_Modifica		=	@par_USUA_Codigo_Modifica,
		OFIC_Codigo					=	@par_OFIC_Codigo
	WHERE
		EMPR_Codigo = @par_EMPR_Codigo
		AND Numero = @par_Numero
END
GO