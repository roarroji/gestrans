﻿
Print 'gsp_modificar_encabezado_remesa'
GO
DROP PROCEDURE gsp_modificar_encabezado_remesa
GO
CREATE PROCEDURE gsp_modificar_encabezado_remesa  
(  
@par_EMPR_Codigo    smallint,  
@par_Numero      numeric,  
@par_CATA_TIRE_Codigo   numeric = NULL,  
@par_Documento_Cliente   varchar(30) = NULL,  
@par_Fecha_Documento_Cliente date = NULL,  
@par_Fecha      date = NULL,  
@par_RUTA_Codigo    numeric = NULL,  
@par_PRTR_Codigo    numeric = NULL,  
@par_CATA_FOPR_Codigo   numeric = NULL,  
@par_TERC_Codigo_Cliente  numeric = NULL,  
@par_TERC_Codigo_Remitente  numeric = NULL,  
@par_CIUD_Codigo_Remitente  numeric = NULL,  
@par_Direccion_Remitente  varchar(150) = NULL,  
@par_Telefonos_Remitente  varchar(100) = NULL,  
@par_Observaciones    varchar(200) = NULL,  
@par_Cantidad_Cliente   numeric = NULL,  
@par_Peso_Cliente    numeric = NULL,  
@par_Peso_Volumetrico_Cliente numeric = NULL,  
@par_DTCV_Codigo    numeric = NULL,  
@par_Valor_Flete_Cliente  money = NULL,  
@par_Valor_Manejo_Cliente  money = NULL,  
@par_Valor_Seguro_Cliente  money = NULL,  
@par_Valor_Descuento_Cliente money = NULL,  
@par_Total_Flete_Cliente  money = NULL,  
@par_Valor_Comercial_Cliente money = NULL,  
@par_Cantidad_Transportador  numeric = NULL,  
@par_Peso_Transportador   numeric = NULL,  
@par_Valor_Flete_Transportador money = NULL,  
@par_Total_Flete_Transportador money = NULL,  
@par_TERC_Codigo_Destinatario numeric = NULL,  
@par_CIUD_Codigo_Destinatario numeric = NULL,  
@par_Direccion_Destinatario  varchar (150) = NULL,  
@par_Telefonos_Destinatario  varchar (100) = NULL,  
@par_Estado      smallint = NULL,  
@par_USUA_Codigo_Modifica  smallint = NULL,  
  
@par_ETCC_Numero    numeric = NULL,  
@par_ETCV_Numero    numeric = NULL,  
@par_ENOS_Numero    numeric = NULL,  
@par_ENPD_Numero    numeric = NULL,  
@par_ENMA_Numero    numeric = NULL,  
@par_ENCU_Numero    numeric = NULL,  
@par_ENFA_Numero    numeric = NULL,  
@par_VEHI_Codigo    numeric = NULL  
  
)  
as   
BEGIN  
  
UPDATE Encabezado_Remesas  
           SET   
          CATA_TIRE_Codigo = ISNULL(@par_CATA_TIRE_Codigo,0),  
   Documento_Cliente = ISNULL(@par_Documento_Cliente,''),  
   Fecha_Documento_Cliente = @par_Fecha_Documento_Cliente,  
   Fecha = @par_Fecha,  
   RUTA_Codigo = ISNULL(@par_RUTA_Codigo,0),  
   PRTR_Codigo = ISNULL(@par_PRTR_Codigo,0),  
   CATA_FOPR_Codigo = ISNULL(@par_CATA_FOPR_Codigo,0),  
   TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente,0),  
   TERC_Codigo_Remitente = ISNULL(@par_TERC_Codigo_Remitente,0),  
   CIUD_Codigo_Remitente = ISNULL(@par_CIUD_Codigo_Remitente,0),  
   Direccion_Remitente = ISNULL(@par_Direccion_Remitente,''),  
   Telefonos_Remitente = ISNULL(@par_Telefonos_Remitente,''),  
   Observaciones = ISNULL(@par_Observaciones,''),  
   Cantidad_Cliente = ISNULL(@par_Cantidad_Cliente,0),  
   Peso_Cliente = ISNULL(@par_Peso_Cliente,0),  
   Peso_Volumetrico_Cliente = ISNULL(@par_Peso_Volumetrico_Cliente,0),  
   DTCV_Codigo = ISNULL(@par_DTCV_Codigo,0),  
   Valor_Flete_Cliente = ISNULL(@par_Valor_Flete_Cliente,0),  
   Valor_Manejo_Cliente = ISNULL(@par_Valor_Manejo_Cliente,0),  
   Valor_Seguro_Cliente = ISNULL(@par_Valor_Seguro_Cliente,0),  
   Valor_Descuento_Cliente = ISNULL(@par_Valor_Descuento_Cliente,0),  
   Total_Flete_Cliente = ISNULL(@par_Total_Flete_Cliente,0),  
   Valor_Comercial_Cliente = ISNULL(@par_Valor_Comercial_Cliente,0),  
   Cantidad_Transportador = ISNULL(@par_Cantidad_Transportador,0),  
   Peso_Transportador = ISNULL(@par_Peso_Transportador,0),  
   Valor_Flete_Transportador = ISNULL(@par_Valor_Flete_Transportador,0),  
   Total_Flete_Transportador = ISNULL(@par_Total_Flete_Transportador,0),  
   TERC_Codigo_Destinatario = ISNULL(@par_TERC_Codigo_Destinatario,0),  
   CIUD_Codigo_Destinatario = ISNULL(@par_CIUD_Codigo_Destinatario,0),  
   Direccion_Destinatario = ISNULL(@par_Direccion_Destinatario,0),  
   Telefonos_Destinatario = ISNULL(@par_Telefonos_Destinatario,0),  
   Estado = ISNULL(@par_Estado,0),  
   USUA_Codigo_Modifica = ISNULL(@par_USUA_Codigo_Modifica,0),  
   ETCC_Numero = ISNULL(@par_ETCC_Numero,0),  
   ETCV_Numero = ISNULL(@par_ETCV_Numero,0),  
   ESOS_Numero = ISNULL(@par_ENOS_Numero,0),  
   ENPD_Numero = ISNULL(@par_ENPD_Numero,0),  
   ENMC_Numero = ISNULL(@par_ENMA_Numero,0),  
   ENCU_Numero = ISNULL(@par_ENCU_Numero,0),  
   ENFA_Numero = ISNULL(@par_ENFA_Numero,0),  
   VEHI_Codigo = ISNULL(@par_VEHI_Codigo,0),  
   Fecha_Modifica = GETDATE()  
  
     WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_Numero  
  
SELECT Numero, Numero_Documento FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_Numero   
END  
GO