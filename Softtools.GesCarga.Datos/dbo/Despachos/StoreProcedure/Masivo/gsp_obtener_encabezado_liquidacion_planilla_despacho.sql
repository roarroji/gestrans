﻿PRINT 'gsp_obtener_encabezado_liquidacion_planilla_despacho'
GO

DROP PROCEDURE gsp_obtener_encabezado_liquidacion_planilla_despacho
GO

CREATE PROCEDURE gsp_obtener_encabezado_liquidacion_planilla_despacho
(
	@par_EMPR_Codigo NUMERIC,
	@par_Numero NUMERIC
)
AS
BEGIN
	SELECT
		1 AS Obtener,
		ELPD.EMPR_Codigo,
		ELPD.Numero,
		ELPD.TIDO_Codigo,
		ELPD.Numero_Documento,
		ELPD.Fecha,
		ELPD.ENPD_Numero,
		ISNULL(ENPD.Numero_Documento,0) AS NumeroPlanilla,
		ISNULL(ENMC.Numero_Documento,0) AS NumeroManifiesto,
		ISNULL(ECPD.Numero_Documento,0) AS NumeroCumplido,
		ENPD.VEHI_Codigo,
		ISNULL(VEHI.Placa,'') AS PlacaVehiculo,
		ISNULL(ENPD.TERC_Codigo_Tenedor,0) AS TERC_Codigo_Tenedor,
		ISNULL(CONCAT(TENE.Nombre,' ',TENE.Apellido1,' ',TENE.Apellido2),'') AS NombreTenedor,
		ISNULL(ENPD.TERC_Codigo_Conductor,0) AS TERC_Codigo_Conductor,
		ISNULL(CONCAT(COND.Nombre,' ',COND.Apellido1,' ',COND.Apellido2),'') AS NombreConductor,
		ELPD.Fecha_Entrega,
		ELPD.Observaciones,
		ELPD.Valor_Flete_Transportador,
		ELPD.Valor_Conceptos_Liquidacion,
		ELPD.Valor_Base_Impuestos,
		ELPD.Valor_Impuestos,
		ELPD.Valor_Pagar,
		ELPD.OFIC_Codigo,
		OFIC.Nombre AS NombreOficina,
		ELPD.Estado
	FROM
		Encabezado_Liquidacion_Planilla_Despachos ELPD

		LEFT JOIN Encabezado_Planilla_Despachos ENPD
		ON ELPD.EMPR_Codigo = ENPD.EMPR_Codigo AND ELPD.ENPD_Numero = ENPD.Numero

		LEFT JOIN Terceros TENE
		ON ENPD.EMPR_Codigo = TENE.EMPR_Codigo AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo

		LEFT JOIN Terceros COND
		ON ENPD.EMPR_Codigo = COND.EMPR_Codigo AND ENPD.TERC_Codigo_Conductor = COND.Codigo

		LEFT JOIN Vehiculos VEHI
		ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo AND ENPD.VEHI_Codigo = VEHI.Codigo

		LEFT JOIN Encabezado_Cumplido_Planilla_Despachos ECPD
		ON ENPD.EMPR_Codigo = ECPD.EMPR_Codigo AND ENPD.ECPD_Numero = ECPD.Numero

		LEFT JOIN Encabezado_Manifiesto_Carga ENMC
		ON ENPD.EMPR_Codigo = ENMC.EMPR_Codigo AND ENPD.ENMC_Numero =  ENMC.Numero

		INNER JOIN Oficinas OFIC
		ON ELPD.EMPR_Codigo = OFIC.EMPR_Codigo AND ELPD.OFIC_Codigo = OFIC.Codigo

	WHERE
		ELPD.EMPR_Codigo = @par_EMPR_Codigo
		AND ELPD.Numero = @par_Numero
END
GO