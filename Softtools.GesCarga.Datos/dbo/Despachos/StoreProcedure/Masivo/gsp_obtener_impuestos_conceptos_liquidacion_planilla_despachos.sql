﻿PRINT 'gsp_obtener_impuestos_conceptos_liquidacion_planilla_Despachos'
GO

DROP PROCEDURE gsp_obtener_impuestos_conceptos_liquidacion_planilla_Despachos
GO

CREATE PROCEDURE gsp_obtener_impuestos_conceptos_liquidacion_planilla_Despachos
(
	@par_EMPR_Codigo SMALLINT
)
AS
BEGIN
	SELECT
		ENIM.EMPR_Codigo,
		ENIM.Codigo,
		ENIM.Nombre,
		ENIM.Operacion,
		ENIM.Estado,
		ENIM.Valor_Base,
		ENIM.Valor_Tarifa,
		ENIM.PLUC_Codigo
	FROM
		Impuestos_Conceptos_Liquidacion_Planilla_Despachos ICLP,
		Conceptos_Liquidacion_Planilla_Despachos CLPD,
		Encabezado_Impuestos ENIM,
		Impuestos_Tipo_Documentos IMTD
	WHERE
		ICLP.EMPR_Codigo = CLPD.EMPR_Codigo
		AND ICLP.CLPD_Codigo = CLPD.Codigo
		AND ICLP.EMPR_Codigo = ENIM.EMPR_Codigo
		AND ICLP.ENIM_Codigo = ENIM.Codigo
		AND ENIM.EMPR_Codigo = IMTD.EMPR_Codigo
		AND ENIM.Codigo = IMTD.ENIM_Codigo
		AND IMTD.TIDO_Codigo = 160 --Liquidacion Planilla Despacho Masivo
		AND ICLP.EMPR_Codigo = @par_EMPR_Codigo
END
GO