﻿PRINT 'gsp_obtener_orden_cargue'
GO
DROP PROCEDURE gsp_obtener_orden_cargue
GO   
CREATE PROCEDURE gsp_obtener_orden_cargue ( 
@par_EMPR_Codigo NUMERIC,            
@par_Numero NUMERIC)              
AS         
        
BEGIN             
SELECT           
        
   ENOC.EMPR_Codigo          
  ,ENOC.Numero          
  ,ENOC.TIDO_Codigo          
  ,ISNULL(ENOC.Numeracion,'') AS Numeracion          
  ,ENOC.Numero_Documento          
    ,ENOC.Fecha          
  ,TECL.Codigo TERC_Codigo_Cliente          
  , ISNULL(TECL.Razon_Social,'')+' '+ISNULL(TECL.Nombre,'')            
  +' '+ISNULL(TECL.Apellido1,'')+' '+ISNULL(TECL.Apellido2,'') AS NombreCliente          
  ,TERE.Codigo TERC_Codigo_Remitente          
  , ISNULL(TERE.Razon_Social,'')+' '+ISNULL(TERE.Nombre,'')            
  +' '+ISNULL(TERE.Apellido1,'')+' '+ISNULL(TERE.Apellido2,'') AS NombreRemitente          
  ,ISNULL(VEHI.Codigo,0) AS VEHI_Codigo          
  ,ISNULL(VEHI.Placa,'') AS PlacaVehiculo          
  ,ISNULL(SEMI.Codigo,0) AS SEMI_Codigo          
  ,ISNULL(SEMI.Placa,'') AS PlacaSemirremolque          
  ,TECO.Codigo TERC_Codigo_Conductor          
  ,ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')            
  +' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS NombreConductor          
  ,TECO.Telefonos AS TelefonoConductor          
  ,ENOC.Anulado          
  ,ENOC.Estado          
  ,OFIC.Nombre AS NombreOficina          
   ,ISNULL(ENMC.Numero_Documento,0) ENMC_Numero,      
   ISNULL(ENPD.Numero_Documento,0) ENPD_Numero,       
   ENOC.ESOS_Numero        
        
  --CARGUE          
  ,CIOC.Codigo CIUD_Codigo_Cargue          
  ,CIOC.Nombre AS NombreCiudadCargue           
  ,ENOC.Direccion_Cargue          
  ,ENOC.Telefonos_Cargue          
  ,ENOC.Contacto_Cargue          
           
  --DESCARGUE          
  ,CIDC.Codigo AS CIUD_Codigo_Descargue          
  ,CIDC.Nombre AS NombreCiudadDescargue           
  ,ENOC.Direccion_Descargue          
  ,ENOC.Telefono_Descargue          
  ,ENOC.Contacto_Descargue          
            
   --Detalle          
   ,RUTA.Codigo AS RUTA_Codigo          
  , RUTA.Nombre AS NombreRuta           
  ,PRTR.Codigo AS PRTR_Codigo          
  , PRTR.Nombre ProductoTransportado          
  ,ENOC.Cantidad_Cliente          
  ,ENOC.Peso_Cliente          
  ,ENOC.Valor_Anticipo          
  ,ENOC.Observaciones          
  ,ENOC.OFIC_Codigo          
  ,1 AS Obtener          
  ,ENOC.Fecha_Crea          
  ,ENOC.Fecha_Modifica          
  ,ENOC.Fecha_Anula          
  ,ENOC.USUA_Codigo_Crea          
  ,ENOC.USUA_Codigo_Anula          
  ,ENOC.USUA_Codigo_Modifica          
  ,ENOC.Causa_Anula          
        
  FROM Encabezado_Orden_Cargues ENOC         
        
  LEFT JOIN Terceros TECL  ON        
  ENOC.EMPR_Codigo  = TECL.EMPR_Codigo          
  AND ENOC.TERC_Codigo_Cliente = TECL.Codigo        
        
  LEFT JOIN Ciudades CIOC ON         
  ENOC.EMPR_Codigo = CIOC.EMPR_Codigo          
  AND ENOC.CIUD_Codigo_Cargue = CIOC.Codigo        
        
  LEFT JOIN Ciudades CICL  ON        
  TECL.EMPR_Codigo  = CICL.EMPR_Codigo          
  AND TECL.CIUD_Codigo = CICL.Codigo          
        
  LEFT JOIN Ciudades CIDC  ON        
  ENOC.EMPR_Codigo = CIDC.EMPR_Codigo          
  AND ENOC.CIUD_Codigo_Descargue = CIDC.Codigo          
        
  LEFT JOIN        
  Terceros TECO  ON        
  ENOC.EMPR_Codigo = TECO.EMPR_Codigo          
  AND ENOC.TERC_Codigo_Conductor = TECO.Codigo          
        
  LEFT JOIN Terceros TERE  ON        
  ENOC.EMPR_Codigo = TERE.EMPR_Codigo          
  AND ENOC.TERC_Codigo_Remitente = TERE.Codigo          
        
  LEFT JOIN Rutas RUTA  ON        
  ENOC.EMPR_Codigo = RUTA.EMPR_Codigo           
  AND ENOC.RUTA_Codigo = RUTA.Codigo          
        
  LEFT JOIN Vehiculos VEHI  ON        
  ENOC.EMPR_Codigo = VEHI.EMPR_Codigo          
  AND ENOC.VEHI_Codigo = VEHI.Codigo          
        
  LEFT JOIN Color_Vehiculos COVE ON        
  VEHI.EMPR_Codigo = COVE.EMPR_Codigo           
  AND VEHI.COVE_Codigo = COVE.Codigo          
         
  LEFT JOIN Valor_Catalogos TIVE  ON        
  VEHI.EMPR_Codigo = TIVE.EMPR_Codigo          
  AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo          
        
  LEFT JOIN        
  Semirremolques  SEMI  ON        
  ENOC.EMPR_Codigo =  SEMI.EMPR_Codigo        
  AND ENOC.SEMI_Codigo = SEMI.Codigo           
        
  LEFT JOIN Producto_Transportados PRTR  ON        
  ENOC.EMPR_Codigo = PRTR.EMPR_Codigo          
  AND ENOC.PRTR_Codigo = PRTR.Codigo        
        
  LEFT JOIN Unidad_Empaque_Producto_Transportados UEPT ON        
  PRTR.EMPR_Codigo = UEPT.EMPR_Codigo          
  AND PRTR.UEPT_Codigo = UEPT.Codigo          
        
  LEFT JOIN Oficinas OFIC   ON        
  ENOC.EMPR_Codigo = OFIC.EMPR_Codigo          
  AND ENOC.OFIC_Codigo = OFIC.Codigo         
         
  LEFT JOIN ENCABEZADO_MANIFIESTO_CARGA ENMC       
  ON  ENOC.EMPR_Codigo = ENMC.EMPR_Codigo      
  AND ENOC.ENMC_Numero = ENMC.Numero      
      
  LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS      
  ON ENOC.EMPR_Codigo = ESOS.EMPR_Codigo      
  AND ENOC.ESOS_Numero = ESOS.Numero      
      
  LEFT JOIN Encabezado_Planilla_Despachos ENPD      
  ON ENOC.EMPR_Codigo = ENPD.EMPR_Codigo      
  AND ENOC.ENPD_Numero = ENPD.Numero       
  AND ENPD.TIDO_Codigo = 150      
      
 WHERE        
          
 ENOC.EMPR_Codigo = @par_EMPR_Codigo          
 AND ENOC.Numero = @par_Numero           
      
END           
GO