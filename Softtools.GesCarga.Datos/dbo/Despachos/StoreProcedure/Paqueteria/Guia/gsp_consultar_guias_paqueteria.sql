﻿PRINT 'gsp_consultar_guias_paqueteria'
GO
DROP PROCEDURE gsp_consultar_guias_paqueteria
GO
CREATE PROCEDURE gsp_consultar_guias_paqueteria      
(      
 @par_EMPR_Codigo SMALLINT,      
     @par_Numero numeric = null,
     @par_NumeroInicial numeric = null,
     @par_NumeroFinal numeric = null,
     @par_Fecha Date = null,
     @par_FechaInicial Date = null,
     @par_FechaFinal Date = null,
	 @par_NumeroPlanillaInicial numeric = null,
     @par_NumeroPlanillaFinal numeric = null,
     @par_FechaPlanillaInicial Datetime = null,
     @par_FechaPlanillaFinal Datetime = null,
     @par_Documento_Cliente numeric = null,
     @par_Cliente varchar (150) = null,
     @par_TERC_Codigo_Cliente numeric = null,
     @par_PRTR_Codigo numeric = null,
     @par_CATA_TERP_Codigo numeric = null,
	 @par_CATA_TTRP_Codigo	numeric= NULL ,
		@par_CATA_TDRP_Codigo	numeric= NULL ,
		@par_CATA_TSRP_Codigo	numeric= NULL ,
		@par_RUTA_Codigo	numeric= NULL ,
		@par_VEHI_Codigo	numeric= NULL ,
		@par_Anulado	numeric= NULL ,
		@par_ENPD_Numero numeric= NULL ,
		@par_CIUD_Destino_Codigo numeric = null,
     @par_Remitente varchar (150) = null,
     @par_Destinatario varchar (150) = null,
     @par_Ruta varchar (150) = null,
     @par_Estado numeric = null,
     @par_Cumplido numeric = null,
 @par_NumeroPagina INT = NULL,    
 @par_RegistrosPagina INT = NULL    
)      
AS      
BEGIN      
 SET NOCOUNT ON;      
 DECLARE      
 @CantidadRegistros INT      
 SELECT @CantidadRegistros = (      
 SELECT DISTINCT       
  COUNT(1)       
 FROM      
 Encabezado_Paqueteria_Remesas as ENPR

 LEFT JOIN Encabezado_Remesas AS ENRE
 ON ENPR.EMPR_Codigo = ENRE.EMPR_Codigo
 AND ENPR.ENRE_Numero = ENRE.Numero

 LEFT JOIN Detalle_Remesas AS DERE
 ON DERE.EMPR_Codigo = ENRE.EMPR_Codigo
 AND DERE.ENRE_Numero = ENRE.Numero

 LEFT JOIN Terceros AS CLIE
 ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo
 
 LEFT JOIN Terceros AS DEST
 ON DERE.EMPR_Codigo = DEST.EMPR_Codigo
 AND DERE.TERC_Codigo_Destinatario = DEST.Codigo
 
 LEFT JOIN Terceros AS REMI
 ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo

 LEFT JOIN Rutas AS RUTA
 ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo
 AND ENRE.RUTA_Codigo = RUTA.Codigo

 LEFT JOIN Producto_Transportados AS PRTR
 ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo
 AND ENRE.PRTR_Codigo = PRTR.Codigo
 
 LEFT JOIN Detalle_Planilla_Despachos AS DEDP
 ON ENRE.EMPR_Codigo = DEDP.EMPR_Codigo
 AND ENRE.Numero = DEDP.ENRE_Numero
 
 LEFT JOIN Encabezado_Planilla_Despachos AS ENDP
 ON DEDP.EMPR_Codigo = ENDP.EMPR_Codigo
 AND DEDP.ENPD_Numero = ENDP.Numero

 WHERE      
  ENPR.EMPR_Codigo = @par_EMPR_Codigo      
  AND ENRE.TIDO_Codigo_Documento = 110      
  AND ENRE.Numero_Documento = ISNULL(@par_Numero,ENRE.Numero_Documento)      
  AND ENRE.Fecha = ISNULL(@par_Fecha,ENRE.Fecha)      
  AND ENRE.Documento_Cliente = ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)      
  AND ((CONCAT(isnull(CLIE.Razon_Social,''),isnull(CLIE.Nombre,''),' ',CLIE.Apellido1,' ',CLIE.Apellido2) LIKE '%' + @par_Cliente + '%') OR (@par_Cliente IS NULL))  
  AND ((CONCAT(isnull(DEST.Razon_Social,''),isnull(DEST.Nombre,''),' ',DEST.Apellido1,' ',DEST.Apellido2) LIKE '%' + @par_Remitente + '%') OR (@par_Remitente IS NULL))  
  AND ((CONCAT(isnull(REMI.Razon_Social,''),isnull(REMI.Nombre,''),' ',REMI.Apellido1,' ',CLIE.Apellido2) LIKE '%' + @par_Destinatario + '%') OR (@par_Destinatario IS NULL))  
  AND RUTA.Nombre LIKE '%'+ ISNULL(@par_Ruta, RUTA.Nombre )    +'%'  
  AND ENPR.CATA_ESRP_Codigo = ISNULL(@par_Estado,ENPR.CATA_ESRP_Codigo )      
  AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial,ENRE.Numero_Documento)      
  AND ENRE.Numero_Documento <= ISNULL(@par_NumeroFinal,ENRE.Numero_Documento)      
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial,ENRE.Fecha)      
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal,ENRE.Fecha)      
  
  AND (ENDP.Numero >= ISNULL(@par_NumeroPlanillaInicial,ENDP.Numero) OR (@par_NumeroPlanillaInicial IS NULL)  )   
  AND (ENDP.Numero <= ISNULL(@par_NumeroPlanillaFinal,ENDP.Numero)     OR (@par_NumeroPlanillaFinal IS NULL) )
  AND (ENDP.Fecha_Hora_Salida >= ISNULL(@par_FechaPlanillaInicial,ENDP.Fecha_Hora_Salida)    OR (@par_FechaPlanillaInicial IS NULL)  )
  AND (ENDP.Fecha_Hora_Salida <= ISNULL(@par_FechaPlanillaFinal,ENDP.Fecha_Hora_Salida) OR (@par_FechaPlanillaFinal IS NULL))

  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente,ENRE.TERC_Codigo_Cliente)      
  AND ENRE.PRTR_Codigo = ISNULL(@par_PRTR_Codigo,ENRE.PRTR_Codigo)
  AND ENPR.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo,ENPR.CATA_TERP_Codigo)
  AND ENPR.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo,ENPR.CATA_TTRP_Codigo)
  AND ENPR.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo,ENPR.CATA_TDRP_Codigo)
  AND ENPR.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo,ENPR.CATA_TSRP_Codigo)
  AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo,ENRE.RUTA_Codigo)
  AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo,ENRE.VEHI_Codigo)
  AND (ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo,ENRE.VEHI_Codigo) or ENRE.VEHI_Codigo = 0) 
  AND ENRE.Anulado = isnull(@par_Anulado, ENRE.Anulado )
  AND (ENRE.ENPD_Numero = isnull(@par_ENPD_Numero, ENRE.ENPD_Numero ) or ENRE.ENPD_Numero = 0)
  AND RUTA.CIUD_Codigo_Destino = isnull(@par_CIUD_Destino_Codigo,  RUTA.CIUD_Codigo_Destino)
  AND DERE.Cumplido = ISNULL(@par_Cumplido,DERE.Cumplido)
  );             
 WITH Pagina AS      
 (      
      
 SELECT      
  ENPR.EMPR_Codigo,      
  ENPR.ENRE_Numero as Codigo,      
  ENRE.Numero_Documento,  
  ENRE.Fecha,
  ISNULL(ENRE.ENPD_Numero,0) AS ENPD_Numero,
  ISNULL(ENRE.VEHI_Codigo,0) AS VEHI_Codigo,
  ISNULL(CLIE.Razon_Social,'') + ' ' + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,'') As NombreCliente,  
  ENRE.TERC_Codigo_Cliente,
  ENRE.CATA_FPRP_Codigo,
  ENRE.RUTA_Codigo,
  RUTA.CIUD_Codigo_Destino,
  ENPR.CATA_ESRP_Codigo,
  ENRE.Observaciones,
  ENRE.Valor_Flete_Cliente,
  ENRE.Total_Flete_Cliente,
  ENRE.Valor_Seguro_Cliente,
  ENRE.Cantidad_Cliente,
  ENRE.Peso_Cliente,
  PRTR.Nombre AS NombreProducto,
  ENRE.Documento_Cliente,
  RUTA.Nombre AS NombreRuta,
  ROW_NUMBER() OVER(ORDER BY ENPR.ENRE_Numero) AS RowNumber      
  FROM      
 Encabezado_Paqueteria_Remesas as ENPR

 LEFT JOIN Encabezado_Remesas AS ENRE
 ON ENPR.EMPR_Codigo = ENRE.EMPR_Codigo
 AND ENPR.ENRE_Numero = ENRE.Numero

 LEFT JOIN Detalle_Remesas AS DERE
 ON DERE.EMPR_Codigo = ENRE.EMPR_Codigo
 AND DERE.ENRE_Numero = ENRE.Numero

 LEFT JOIN Terceros AS CLIE
 ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo
 
 LEFT JOIN Terceros AS DEST
 ON DERE.EMPR_Codigo = DEST.EMPR_Codigo
 AND DERE.TERC_Codigo_Destinatario = DEST.Codigo
 
 LEFT JOIN Terceros AS REMI
 ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo

 LEFT JOIN Rutas AS RUTA
 ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo
 AND ENRE.RUTA_Codigo = RUTA.Codigo

 LEFT JOIN Producto_Transportados AS PRTR
 ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo
 AND ENRE.PRTR_Codigo = PRTR.Codigo
 
 LEFT JOIN Detalle_Planilla_Despachos AS DEDP
 ON ENRE.EMPR_Codigo = DEDP.EMPR_Codigo
 AND ENRE.Numero = DEDP.ENRE_Numero
 
 LEFT JOIN Encabezado_Planilla_Despachos AS ENDP
 ON DEDP.EMPR_Codigo = ENDP.EMPR_Codigo
 AND DEDP.ENPD_Numero = ENDP.Numero

 WHERE      
   ENPR.EMPR_Codigo = @par_EMPR_Codigo 
  AND ENRE.TIDO_Codigo_Documento = 110      
  AND ENRE.Numero_Documento = ISNULL(@par_Numero,ENRE.Numero_Documento)      
  AND ENRE.Fecha = ISNULL(@par_Fecha,ENRE.Fecha)      
  AND ENRE.Documento_Cliente = ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)      
  AND ((CONCAT(isnull(CLIE.Razon_Social,''),isnull(CLIE.Nombre,''),' ',CLIE.Apellido1,' ',CLIE.Apellido2) LIKE '%' + @par_Cliente + '%') OR (@par_Cliente IS NULL))  
  AND ((CONCAT(isnull(DEST.Razon_Social,''),isnull(DEST.Nombre,''),' ',DEST.Apellido1,' ',DEST.Apellido2) LIKE '%' + @par_Remitente + '%') OR (@par_Remitente IS NULL))  
  AND ((CONCAT(isnull(REMI.Razon_Social,''),isnull(REMI.Nombre,''),' ',REMI.Apellido1,' ',CLIE.Apellido2) LIKE '%' + @par_Destinatario + '%') OR (@par_Destinatario IS NULL))  
  AND RUTA.Nombre LIKE '%'+ ISNULL(@par_Ruta, RUTA.Nombre )    +'%'  
  AND ENPR.CATA_ESRP_Codigo = ISNULL(@par_Estado,ENPR.CATA_ESRP_Codigo )      
  AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial,ENRE.Numero_Documento)      
  AND ENRE.Numero_Documento <= ISNULL(@par_NumeroFinal,ENRE.Numero_Documento)      
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial,ENRE.Fecha)      
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal,ENRE.Fecha)      
  
  AND (ENDP.Numero >= ISNULL(@par_NumeroPlanillaInicial,ENDP.Numero) OR (@par_NumeroPlanillaInicial IS NULL)  )   
  AND (ENDP.Numero <= ISNULL(@par_NumeroPlanillaFinal,ENDP.Numero)     OR (@par_NumeroPlanillaFinal IS NULL) )
  AND (ENDP.Fecha_Hora_Salida >= ISNULL(@par_FechaPlanillaInicial,ENDP.Fecha_Hora_Salida)    OR (@par_FechaPlanillaInicial IS NULL)  )
  AND (ENDP.Fecha_Hora_Salida <= ISNULL(@par_FechaPlanillaFinal,ENDP.Fecha_Hora_Salida) OR (@par_FechaPlanillaFinal IS NULL))

  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente,ENRE.TERC_Codigo_Cliente)      
  AND ENRE.PRTR_Codigo = ISNULL(@par_PRTR_Codigo,ENRE.PRTR_Codigo)
  AND ENPR.CATA_TERP_Codigo = ISNULL(@par_CATA_TERP_Codigo,ENPR.CATA_TERP_Codigo)
  AND ENPR.CATA_TTRP_Codigo = ISNULL(@par_CATA_TTRP_Codigo,ENPR.CATA_TTRP_Codigo)
  AND ENPR.CATA_TDRP_Codigo = ISNULL(@par_CATA_TDRP_Codigo,ENPR.CATA_TDRP_Codigo)
  AND ENPR.CATA_TSRP_Codigo = ISNULL(@par_CATA_TSRP_Codigo,ENPR.CATA_TSRP_Codigo)
  AND ENRE.RUTA_Codigo = ISNULL(@par_RUTA_Codigo,ENRE.RUTA_Codigo)
  AND ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo,ENRE.VEHI_Codigo)
  AND (ENRE.VEHI_Codigo = ISNULL(@par_VEHI_Codigo,ENRE.VEHI_Codigo) or ENRE.VEHI_Codigo = 0) 
  AND ENRE.Anulado = isnull(@par_Anulado, ENRE.Anulado )
  AND (ENRE.ENPD_Numero = isnull(@par_ENPD_Numero, ENRE.ENPD_Numero ) or ENRE.ENPD_Numero = 0)
  AND RUTA.CIUD_Codigo_Destino = isnull(@par_CIUD_Destino_Codigo,  RUTA.CIUD_Codigo_Destino)
  AND DERE.Cumplido = ISNULL(@par_Cumplido,DERE.Cumplido)
     
  )     
 SELECT DISTINCT      
0 As Obtener,  
EMPR_Codigo,      
Codigo,      
Numero_Documento,  
Fecha,
ENPD_Numero,
VEHI_Codigo,
NombreCliente,  
TERC_Codigo_Cliente,
CATA_FPRP_Codigo,
RUTA_Codigo,
CIUD_Codigo_Destino,
CATA_ESRP_Codigo,
Observaciones,
Valor_Flete_Cliente,
Total_Flete_Cliente,
Valor_Seguro_Cliente,
Cantidad_Cliente,
Peso_Cliente,
NombreProducto,
Documento_Cliente, 
NombreRuta,
@CantidadRegistros AS TotalRegistros,      
@par_NumeroPagina AS PaginaObtener,      
@par_RegistrosPagina AS RegistrosPagina      
 FROM      
 Pagina      
 WHERE      
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
 ORDER BY Codigo asc      
      
END      
GO