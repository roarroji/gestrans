﻿PRINT 'gsp_modificar_encabezado_remesa_paqueteria'
go
DROP PROCEDURE gsp_modificar_encabezado_remesa_paqueteria
GO
CREATE procedure gsp_modificar_encabezado_remesa_paqueteria
(
@par_EMPR_Codigo smallint ,
@par_ENRE_Numero numeric(18,0),
@par_TERC_Codigo_Transportador_Externo numeric = NULL,
@par_Numero_Guia_Externa varchar(50) = NULL,
@par_CATA_TTRP_Codigo numeric,
@par_CATA_TDRP_Codigo numeric,
@par_CATA_TPRP_Codigo numeric,
@par_CATA_TSRP_Codigo numeric,
@par_CATA_TERP_Codigo numeric,
@par_CATA_TIRP_Codigo numeric,
@par_Fecha_Interfaz_Tracking datetime = NULL,
@par_Fecha_Interfaz_WMS datetime = NULL
		   )
as
begin

SET @par_Fecha_Interfaz_Tracking = CONVERT(DATE,@par_Fecha_Interfaz_Tracking,101)
SET @par_Fecha_Interfaz_WMS = CONVERT(DATE,@par_Fecha_Interfaz_WMS,101)

update Remesas_Paqueteria 
          
           SET 
         
				TERC_Codigo_Transportador_Externo = ISNULL(@par_TERC_Codigo_Transportador_Externo,0),
				Numero_Guia_Externa = ISNULL(@par_Numero_Guia_Externa,''),
				CATA_TTRP_Codigo = @par_CATA_TTRP_Codigo,
				CATA_TDRP_Codigo = @par_CATA_TDRP_Codigo,
				CATA_TPRP_Codigo = @par_CATA_TPRP_Codigo,
				CATA_TSRP_Codigo = @par_CATA_TSRP_Codigo,
				CATA_TERP_Codigo = @par_CATA_TERP_Codigo,
				CATA_TIRP_Codigo = @par_CATA_TIRP_Codigo,
				Fecha_Interfaz_Tracking = ISNULL(@par_Fecha_Interfaz_Tracking, '01/01/1900'),
				Fecha_Interfaz_WMS = ISNULL(@par_Fecha_Interfaz_WMS, '01/01/1900')
		  where 

		    EMPR_Codigo = @par_EMPR_Codigo
           AND ENRE_Numero = @par_ENRE_Numero

select @par_ENRE_Numero as Numero

END
GO