﻿PRINT 'gsp_obtener_guia_paqueteria'
GO
DROP PROCEDURE gsp_obtener_remesa_paqueteria
GO
CREATE PROCEDURE gsp_obtener_remesa_paqueteria    
(    
@par_EMPR_Codigo smallint,    
@par_Numero Numeric   
)    
AS     
BEGIN    
select   
1 as Obtener,
ENRE.EMPR_Codigo,  
ENRE.Fecha,  
ENRE.Numero,  
ENRE.Numero_Documento,  
ENRE.Documento_Cliente,  
ENRE.Observaciones,  
ENRE.Fecha_Documento_Cliente,  
ENRE.CATA_FOPR_Codigo,  
ENRE.TERC_Codigo_Cliente,  
ENRE.DTCV_Codigo,  
ENRE.PRTR_Codigo,  
ENRE.Peso_Cliente,  
ENRE.Cantidad_Cliente,  
ENRE.Valor_Comercial_Cliente,  
ENRE.Peso_Volumetrico_Cliente,  
ENPR.CATA_TERP_Codigo,  
ENPR.CATA_TTRP_Codigo,  
ENPR.CATA_TDRP_Codigo,  
ENPR.CATA_TIRP_Codigo,  
ENPR.CATA_TPRP_Codigo,  
ENPR.CATA_TSRP_Codigo,  

ENRE.TERC_Codigo_Remitente,  
ENRE.CIUD_Codigo_Remitente,  
ENRE.Direccion_Remitente,  
ENRE.Telefonos_Remitente,  
ENRE.TERC_Codigo_Destinatario,  
ENRE.CIUD_Codigo_Destinatario,  
ENRE.Direccion_Destinatario,  
ENRE.Telefonos_Destinatario,  
ENRE.Valor_Flete_Cliente,  
ENRE.Valor_Manejo_Cliente,  
ENRE.Valor_Seguro_Cliente,  
ENRE.Valor_Descuento_Cliente,  
ENRE.Total_Flete_Cliente,  
ENRE.Total_Flete_Transportador,  
ENRE.Estado,
ISNULL(DTCV.ETCV_Numero,0) As ETCV_Numero,
ISNULL(DTCV.LNTC_Codigo,0) As LNTC_Codigo,  
ISNULL(DTCV.TLNC_Codigo,0) As TLNC_Codigo,
ISNULL(DTCV.RUTA_Codigo,0) As RUTA_Codigo,
ISNULL(DTCV.TATC_Codigo,0) As TATC_Codigo,
ISNULL(DTCV.TTTC_Codigo,0) As TTTC_Codigo,
ENPR.Fecha_Interfaz_Tracking,
ENPR.Fecha_Interfaz_WMS,
isnull(ENRE.VEHI_Codigo,0) as VEHI_Codigo
  
from   
Remesas_Paqueteria as ENPR  
  
INNER JOIN Encabezado_Remesas AS ENRE  
ON ENPR.EMPR_Codigo = ENRE.EMPR_Codigo  
AND ENPR.ENRE_Numero = ENRE.Numero  
  
LEFT JOIN Detalle_Tarifario_Carga_Ventas AS DTCV  
ON ENPR.EMPR_Codigo = DTCV.EMPR_Codigo  
AND ENRE.DTCV_Codigo = DTCV.Codigo  
AND ENRE.EMPR_Codigo = DTCV.EMPR_Codigo  
--AND ENRE.ETCV_Numero = DTCV.ETCV_Numero  


WHERE

	ENRE.EMPR_Codigo = @par_EMPR_Codigo
	AND ENRE.Numero = @par_Numero
	AND ENRE.TIDO_Codigo_Documento = 110 --> Guía
END  
GO