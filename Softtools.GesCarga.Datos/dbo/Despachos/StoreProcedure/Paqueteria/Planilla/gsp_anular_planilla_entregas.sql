﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_anular_planilla_entregas'
DROP PROCEDURE gsp_anular_planilla_entregas 
GO
CREATE PROCEDURE gsp_anular_planilla_entregas(
@par_EMPR_Codigo NUMERIC,
@par_Codigo_Usuario NUMERIC,
@par_Numero_Planilla NUMERIC,
@par_observaciones varchar(200)
)
AS BEGIN 

UPDATE Encabezado_Planilla_Entregas 
SET Anulado = 1,
USUA_Codigo_Anula  = @par_Codigo_Usuario,
Causa_Anula = @par_observaciones,
Fecha_Anula = GETDATE()
WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero=@par_Numero_Planilla

UPDATE Encabezado_Remesas 
SET ENPE_Numero = 0 
FROM Detalle_Planilla_Entregas DPEN INNER JOIN Encabezado_Remesas ENRE
ON DPEN.EMPR_Codigo = ENRE.EMPR_Codigo 
AND DPEN.ENRE_Numero = ENRE.Numero
AND DPEN.ENPE_Numero = @par_Numero_Planilla

SELECT Numero_Documento FROM Encabezado_Planilla_Entregas WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero=@par_Numero_Planilla
END 
GO 