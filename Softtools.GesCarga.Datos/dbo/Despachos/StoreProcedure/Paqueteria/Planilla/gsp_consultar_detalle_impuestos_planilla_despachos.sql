﻿Print 'gsp_consultar_detalle_impuestos_planilla_despachos'
GO
DROP PROCEDURE gsp_consultar_detalle_impuestos_planilla_despachos
GO
CREATE PROCEDURE gsp_consultar_detalle_impuestos_planilla_despachos (
@par_EMPR_Codigo	smallint,
@par_ENPD_Numero	numeric
)
AS
BEGIN
	SELECT	EMPR_Codigo,
			ENPD_Numero,
			ENIM_Codigo,
			Valor_Tarifa,
			Valor_Base,
			Valor_Impuesto

	FROM Detalle_Impuestos_Planilla_Despachos 
	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND ENPD_Numero = @par_ENPD_Numero

END
GO