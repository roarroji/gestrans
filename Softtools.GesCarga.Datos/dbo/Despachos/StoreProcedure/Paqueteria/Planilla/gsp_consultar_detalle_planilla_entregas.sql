﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_consultar_detalle_planilla_entregas'
DROP PROCEDURE gsp_consultar_detalle_planilla_entregas 
GO

CREATE PROCEDURE gsp_consultar_detalle_planilla_entregas (  
     @par_EMPR_Codigo SMALLINT,        
     @par_Numero NUMERIC = null,  
	 @par_NumeroPagina  NUMERIC = null,
     @par_RegistrosPagina NUMERIC = null 
 )  
 AS  
 BEGIN  
  SET NOCOUNT ON;  

   DECLARE  
    @CantidadRegistros INT  
   SELECT @CantidadRegistros = (  
     SELECT DISTINCT   
      COUNT(1)   
      FROM Detalle_Planilla_Entregas DEPE
	  ,Encabezado_Planilla_Entregas ENPE
	  ,Encabezado_Remesas ENRE
      WHERE  
	  DEPE.EMPR_Codigo = ENPE.EMPR_Codigo
	  AND DEPE.ENPE_Numero = ENPE.Numero

      AND DEPE.EMPR_Codigo = ENRE.EMPR_Codigo
	  AND DEPE.ENRE_Numero = ENRE.Numero
	  AND ENRE.TIDO_Codigo = 110
	  AND ENRE.Anulado <> 1
	  AND ENRE.Estado = 1
	  AND ENPE.Numero = @par_Numero
       );         
     WITH Pagina AS  
     (  
      SELECT 
	  ENRE.EMPR_Codigo,
	  ENRE.Numero,
	  ENRE.Numero_Documento,
	  ENRE.Documento_Cliente,
	  ISNULL( ENRE.Numeracion, 0) AS Numeracion,
	  ENRE.Fecha,
	  ENRE.Valor_Flete_Cliente,
	  ENRE.Cantidad_Cliente,
	  ENRE.Peso_Cliente,

	   ROW_NUMBER() OVER ( ORDER BY   ENPE.NUMERO) AS RowNumber  
	   FROM Detalle_Planilla_Entregas DEPE
	  ,Encabezado_Planilla_Entregas ENPE
	  ,Encabezado_Remesas ENRE
      WHERE  
	    ENPE.Numero = @par_Numero
	 AND DEPE.EMPR_Codigo = ENPE.EMPR_Codigo
	  AND DEPE.ENPE_Numero = ENPE.Numero
	  AND DEPE.EMPR_Codigo = ENRE.EMPR_Codigo
	  AND DEPE.ENRE_Numero = ENRE.Numero
	  AND ENRE.TIDO_Codigo = 110
	  AND ENRE.Anulado <> 1
	  AND ENRE.Estado = 1
	 
	      ) 
   SELECT DISTINCT  
   EMPR_Codigo,
	  Numero,
	  Numero_Documento,
	  Documento_Cliente,
	  Numeracion, 
	  Fecha,
	  Valor_Flete_Cliente,
	  Cantidad_Cliente,
	  Peso_Cliente,
	@CantidadRegistros AS TotalRegistros,  
    @par_NumeroPagina AS PaginaObtener,  
    @par_RegistrosPagina AS RegistrosPagina  
    FROM  
    Pagina  
    WHERE  
    RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
    AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
    order by  Numero_Documento
   END   
GO