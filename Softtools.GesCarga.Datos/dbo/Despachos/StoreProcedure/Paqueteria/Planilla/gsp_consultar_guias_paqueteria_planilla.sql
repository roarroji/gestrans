﻿Print 'gsp_consultar_guias_paqueteria_planilla'
GO
DROP PROCEDURE gsp_consultar_guias_paqueteria_planilla
GO
CREATE PROCEDURE gsp_consultar_guias_paqueteria_planilla     
(        
 @par_EMPR_Codigo SMALLINT,        
 @par_ENPD_Numero numeric= NULL ,  
 @par_NumeroPagina INT = NULL,      
 @par_RegistrosPagina INT = NULL      
)        
AS        
BEGIN        
 SET NOCOUNT ON;        
 DECLARE        
 @CantidadRegistros INT        
 SELECT @CantidadRegistros = (        
 SELECT DISTINCT         
  COUNT(1)         
 FROM        
 Remesas_Paqueteria as ENPR  
  
 LEFT JOIN Encabezado_Remesas AS ENRE  
 ON ENPR.EMPR_Codigo = ENRE.EMPR_Codigo  
 AND ENPR.ENRE_Numero = ENRE.Numero  
  
 LEFT JOIN Terceros AS CLIE  
 ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo  
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo  
   
 LEFT JOIN Terceros AS DEST  
 ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo  
 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo  
   
 LEFT JOIN Terceros AS REMI  
 ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo  
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo  
  
 LEFT JOIN Rutas AS RUTA  
 ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo  
 AND ENRE.RUTA_Codigo = RUTA.Codigo  
  
 LEFT JOIN Producto_Transportados AS PRTR  
 ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo  
 AND ENRE.PRTR_Codigo = PRTR.Codigo  
   
 WHERE        
  ENPR.EMPR_Codigo = @par_EMPR_Codigo        
  AND ENRE.ENPD_Numero = isnull(@par_ENPD_Numero, ENRE.ENPD_Numero )  
  AND ENRE.TIDO_Codigo = 110  
  );               
 WITH Pagina AS        
 (        
        
 SELECT        
  ENPR.EMPR_Codigo,        
  ENPR.ENRE_Numero as Numero,        
  ENRE.Numero_Documento,    
  ENRE.Fecha,  
  ISNULL(ENRE.Observaciones,'') As Observaciones,  
  ISNULL(ENRE.Documento_Cliente,'') As Documento_Cliente,  
  ENRE.Estado,  
  ISNULL(ENRE.CATA_FOPR_Codigo,0) As CATA_FOPR_Codigo,  
  ISNULL(ENRE.Total_Flete_Cliente,0) As Total_Flete_Cliente,  
  ISNULL(ENRE.Valor_Flete_Cliente,0) As Valor_Flete_Cliente,  
  ISNULL(ENRE.Valor_Seguro_Cliente,0) As Valor_Seguro_Cliente,  
  ISNULL(ENRE.Cantidad_Cliente,0) As Cantidad_Cliente,  
  ISNULL(ENRE.Peso_Cliente,0) As Peso_Cliente,  
  ISNULL(ENRE.ENPD_Numero,0) AS ENPD_Numero,  
  ISNULL(PRTR.Nombre,'') AS NombreProducto,  
  ISNULL(ENRE.TERC_Codigo_Cliente,0) As TERC_Codigo_Cliente,  
  ISNULL(CLIE.Razon_Social,'') + ' ' + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,'') As NombreCliente,    
  ISNULL(ENRE.RUTA_Codigo,0) As RUTA_Codigo,  
  ISNULL(RUTA.Nombre,'') AS NombreRuta,  
  ISNULL(RUTA.CIUD_Codigo_Destino,0) As CIUD_Codigo_Destino,  
  ISNULL(ENRE.VEHI_Codigo,0) AS VEHI_Codigo,  
     
  ROW_NUMBER() OVER(ORDER BY ENPR.ENRE_Numero) AS RowNumber        
 FROM        
  Remesas_Paqueteria as ENPR  
  
  
 LEFT JOIN Encabezado_Remesas AS ENRE  
 ON ENPR.EMPR_Codigo = ENRE.EMPR_Codigo  
 AND ENPR.ENRE_Numero = ENRE.Numero  
  
 LEFT JOIN Terceros AS CLIE  
 ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo  
 AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo  
   
 LEFT JOIN Terceros AS DEST  
 ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo  
 AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo  
   
 LEFT JOIN Terceros AS REMI  
 ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo  
 AND ENRE.TERC_Codigo_Remitente = REMI.Codigo  
  
 LEFT JOIN Rutas AS RUTA  
 ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo  
 AND ENRE.RUTA_Codigo = RUTA.Codigo  
  
  LEFT JOIN Producto_Transportados AS PRTR  
 ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo  
 AND ENRE.PRTR_Codigo = PRTR.Codigo  
  
 WHERE        
  ENPR.EMPR_Codigo = @par_EMPR_Codigo        
  AND ENRE.ENPD_Numero = isnull(@par_ENPD_Numero, ENRE.ENPD_Numero)  
  AND ENRE.TIDO_Codigo = 110  
  )       
 SELECT DISTINCT        
0 As Obtener,    
EMPR_Codigo,        
Numero,        
Numero_Documento,    
Fecha,  
Observaciones,  
Documento_Cliente,  
Estado,  
CATA_FOPR_Codigo,  
Total_Flete_Cliente,  
Valor_Flete_Cliente,  
Valor_Seguro_Cliente,  
Cantidad_Cliente,  
Peso_Cliente,  
ENPD_Numero,  
NombreProducto,  
TERC_Codigo_Cliente,  
NombreCliente,    
RUTA_Codigo,  
NombreRuta,  
CIUD_Codigo_Destino,  
VEHI_Codigo,  
  
  
@CantidadRegistros AS TotalRegistros,        
@par_NumeroPagina AS PaginaObtener,        
@par_RegistrosPagina AS RegistrosPagina        
 FROM        
 Pagina        
 WHERE        
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)      
 ORDER BY Numero asc        
        
END        
GO