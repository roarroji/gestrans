﻿PRINT 'gsp_consultar_planilla_guias_paqueteria'
GO
DROP PROCEDURE gsp_consultar_planilla_guias_paqueteria
GO
CREATE PROCEDURE gsp_consultar_planilla_guias_paqueteria      
(      
 @par_EMPR_Codigo SMALLINT,      
     @par_Numero numeric = null,
     @par_Fecha Date = null,
     @par_Ruta varchar (150) = null,
     @par_Vehiculo varchar (50) = null,
     @par_Conductor varchar (150) = null,
     @par_Tenedor varchar (150) = null,
     @par_Estado numeric = null,
     @par_Numero_Documento_Remesa numeric = null,
 @par_NumeroPagina INT = NULL,    
 @par_RegistrosPagina INT = NULL    
)      
AS      
BEGIN      
 SET NOCOUNT ON;      
 DECLARE      
 @CantidadRegistros INT  
 IF	isnull(@par_Numero_Documento_Remesa	,0)>0
 BEGIN
 SELECT @CantidadRegistros = (      
 SELECT DISTINCT       
  COUNT(1)       
 FROM      
Encabezado_Planilla_Despachos as ENPD

LEFT JOIN Vehiculos AS VEHI
ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo
AND ENPD.VEHI_Codigo = VEHI.Codigo

LEFT JOIN Terceros AS COND
ON ENPD.EMPR_Codigo = COND.EMPR_Codigo
AND ENPD.TERC_Codigo_Conductor = COND.Codigo

LEFT JOIN Terceros AS TENE
ON ENPD.EMPR_Codigo = TENE.EMPR_Codigo
AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo

LEFT JOIN Rutas AS RUTA
ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo
AND ENPD.RUTA_Codigo = RUTA.Codigo

LEFT JOIN Detalle_Planilla_Despachos AS DEPD
ON ENPD.EMPR_Codigo = DEPD.EMPR_Codigo
AND ENPD.Numero = DEPD.ENPD_Numero

 WHERE      
  ENPD.EMPR_Codigo = @par_EMPR_Codigo      
	  AND ENPD.Numero = isnull(@par_Numero ,ENPD.Numero)
  AND ENPD.Fecha = ISNULL(@par_Fecha,ENPD.Fecha)      
  AND (VEHI.Placa = ISNULL(@par_Vehiculo,VEHI.Placa) or VEHI.Codigo_Alterno = ISNULL(@par_Vehiculo,VEHI.Codigo_Alterno))
  AND ((CONCAT(isnull(COND.Razon_Social,''),isnull(COND.Nombre,''),' ',COND.Apellido1,' ',COND.Apellido2) LIKE '%' + @par_Conductor + '%') OR (@par_Conductor IS NULL))  
  AND ((CONCAT(isnull(TENE.Razon_Social,''),isnull(TENE.Nombre,''),' ',TENE.Apellido1,' ',TENE.Apellido2) LIKE '%' + @par_Tenedor + '%') OR (@par_Tenedor IS NULL))  
  AND RUTA.Nombre LIKE '%'+ ISNULL(@par_Ruta, RUTA.Nombre )    +'%'  
  AND DEPD.Numero_Documento_Remesa = ISNULL(@par_Numero_Documento_Remesa,DEPD.Numero_Documento_Remesa ) 
  AND ENPD.Estado = ISNULL(@par_Estado,ENPD.Estado ) 
  );      
  WITH Pagina AS      
 (      
      
 SELECT DISTINCT     
  ENPD.EMPR_Codigo,      
  ENPD.Numero,      
  ENPD.Fecha,
  ISNULL(COND.Razon_Social,'') + ' ' + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') As NombreConductor,  
  ISNULL(TENE.Razon_Social,'') + ' ' + ISNULL(TENE.Nombre,'') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2,'') As NombreTenedor,  
  VEHI.Placa,
  RUTA.Nombre AS NombreRuta,
  ENPD.Estado,
  ROW_NUMBER() OVER(ORDER BY ENPD.Numero) AS RowNumber      
 FROM      
	Encabezado_Planilla_Despachos as ENPD

	LEFT JOIN Vehiculos AS VEHI
	ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo
	AND ENPD.VEHI_Codigo = VEHI.Codigo

	LEFT JOIN Terceros AS COND
	ON ENPD.EMPR_Codigo = COND.EMPR_Codigo
	AND ENPD.TERC_Codigo_Conductor = COND.Codigo

	LEFT JOIN Terceros AS TENE
	ON ENPD.EMPR_Codigo = TENE.EMPR_Codigo
	AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo

	LEFT JOIN Rutas AS RUTA
	ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo
	AND ENPD.RUTA_Codigo = RUTA.Codigo

	LEFT JOIN Detalle_Planilla_Despachos AS DEPD
	ON ENPD.EMPR_Codigo = DEPD.EMPR_Codigo
	AND ENPD.Numero = DEPD.ENPD_Numero

	 WHERE      
	  ENPD.EMPR_Codigo = @par_EMPR_Codigo      
	  AND ENPD.Numero = isnull(@par_Numero ,ENPD.Numero)
	  AND ENPD.Fecha = ISNULL(@par_Fecha,ENPD.Fecha)      
	  AND (VEHI.Placa = ISNULL(@par_Vehiculo,VEHI.Placa) or VEHI.Codigo_Alterno = ISNULL(@par_Vehiculo,VEHI.Codigo_Alterno))
	  AND ((CONCAT(isnull(COND.Razon_Social,''),isnull(COND.Nombre,''),' ',COND.Apellido1,' ',COND.Apellido2) LIKE '%' + @par_Conductor + '%') OR (@par_Conductor IS NULL))  
	  AND ((CONCAT(isnull(TENE.Razon_Social,''),isnull(TENE.Nombre,''),' ',TENE.Apellido1,' ',TENE.Apellido2) LIKE '%' + @par_Tenedor + '%') OR (@par_Tenedor IS NULL))  
	  AND RUTA.Nombre LIKE '%'+ ISNULL(@par_Ruta, RUTA.Nombre )    +'%'  
	  AND DEPD.Numero_Documento_Remesa = ISNULL(@par_Numero_Documento_Remesa,DEPD.Numero_Documento_Remesa ) 
	  AND ENPD.Estado = ISNULL(@par_Estado,ENPD.Estado ) 
     
  )     
 SELECT DISTINCT      
0 As Obtener,  
EMPR_Codigo,      
Numero,      
Fecha,      
NombreConductor,  
NombreTenedor,
Placa,  
NombreRuta,
Estado,
@CantidadRegistros AS TotalRegistros,      
@par_NumeroPagina AS PaginaObtener,      
@par_RegistrosPagina AS RegistrosPagina      
 FROM      
 Pagina      
 WHERE      
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
 ORDER BY Numero asc      
 END       
 ELSE
 BEGIN 
 SELECT @CantidadRegistros = (      
 SELECT DISTINCT       
  COUNT(1)       
 FROM      
Encabezado_Planilla_Despachos as ENPD

LEFT JOIN Vehiculos AS VEHI
ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo
AND ENPD.VEHI_Codigo = VEHI.Codigo

LEFT JOIN Terceros AS COND
ON ENPD.EMPR_Codigo = COND.EMPR_Codigo
AND ENPD.TERC_Codigo_Conductor = COND.Codigo

LEFT JOIN Terceros AS TENE
ON ENPD.EMPR_Codigo = TENE.EMPR_Codigo
AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo

LEFT JOIN Rutas AS RUTA
ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo
AND ENPD.RUTA_Codigo = RUTA.Codigo

 WHERE      
  ENPD.EMPR_Codigo = @par_EMPR_Codigo      
  AND ENPD.Numero = isnull(@par_Numero ,ENPD.Numero)
  AND ENPD.Fecha = ISNULL(@par_Fecha,ENPD.Fecha)      
  AND (VEHI.Placa = ISNULL(@par_Vehiculo,VEHI.Placa) or VEHI.Codigo_Alterno = ISNULL(@par_Vehiculo,VEHI.Codigo_Alterno))
  AND ((CONCAT(isnull(COND.Razon_Social,''),isnull(COND.Nombre,''),' ',COND.Apellido1,' ',COND.Apellido2) LIKE '%' + @par_Conductor + '%') OR (@par_Conductor IS NULL))  
  AND ((CONCAT(isnull(TENE.Razon_Social,''),isnull(TENE.Nombre,''),' ',TENE.Apellido1,' ',TENE.Apellido2) LIKE '%' + @par_Tenedor + '%') OR (@par_Tenedor IS NULL))  
  AND RUTA.Nombre LIKE '%'+ ISNULL(@par_Ruta, RUTA.Nombre )    +'%'  
  AND ENPD.Estado = ISNULL(@par_Estado,ENPD.Estado ) 
  );    
  WITH Pagina AS      
 (      
      
 SELECT DISTINCT     
  ENPD.EMPR_Codigo,      
  ENPD.Numero,      
  ENPD.Fecha,
  ISNULL(COND.Razon_Social,'') + ' ' + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') As NombreConductor,  
  ISNULL(TENE.Razon_Social,'') + ' ' + ISNULL(TENE.Nombre,'') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2,'') As NombreTenedor,  
  VEHI.Placa,
  RUTA.Nombre AS NombreRuta,
  ENPD.Estado,
  ROW_NUMBER() OVER(ORDER BY ENPD.Numero) AS RowNumber      
 FROM      
	Encabezado_Planilla_Despachos as ENPD

	LEFT JOIN Vehiculos AS VEHI
	ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo
	AND ENPD.VEHI_Codigo = VEHI.Codigo

	LEFT JOIN Terceros AS COND
	ON ENPD.EMPR_Codigo = COND.EMPR_Codigo
	AND ENPD.TERC_Codigo_Conductor = COND.Codigo

	LEFT JOIN Terceros AS TENE
	ON ENPD.EMPR_Codigo = TENE.EMPR_Codigo
	AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo

	LEFT JOIN Rutas AS RUTA
	ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo
	AND ENPD.RUTA_Codigo = RUTA.Codigo

	 WHERE      
	  ENPD.EMPR_Codigo = @par_EMPR_Codigo      
	  AND ENPD.Numero = isnull(@par_Numero ,ENPD.Numero)
	  AND ENPD.Fecha = ISNULL(@par_Fecha,ENPD.Fecha)      
	  AND (VEHI.Placa = ISNULL(@par_Vehiculo,VEHI.Placa) or VEHI.Codigo_Alterno = ISNULL(@par_Vehiculo,VEHI.Codigo_Alterno))
	  AND ((CONCAT(isnull(COND.Razon_Social,''),isnull(COND.Nombre,''),' ',COND.Apellido1,' ',COND.Apellido2) LIKE '%' + @par_Conductor + '%') OR (@par_Conductor IS NULL))  
	  AND ((CONCAT(isnull(TENE.Razon_Social,''),isnull(TENE.Nombre,''),' ',TENE.Apellido1,' ',TENE.Apellido2) LIKE '%' + @par_Tenedor + '%') OR (@par_Tenedor IS NULL))  
	  AND RUTA.Nombre LIKE '%'+ ISNULL(@par_Ruta, RUTA.Nombre )    +'%'  
	  AND ENPD.Estado = ISNULL(@par_Estado,ENPD.Estado ) 
     
  )     
 SELECT DISTINCT      
0 As Obtener,  
EMPR_Codigo,      
Numero,      
Fecha,      
NombreConductor,  
NombreTenedor,
Placa,  
NombreRuta,
Estado,
@CantidadRegistros AS TotalRegistros,      
@par_NumeroPagina AS PaginaObtener,      
@par_RegistrosPagina AS RegistrosPagina      
 FROM      
 Pagina      
 WHERE      
  RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)  
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)    
 ORDER BY Numero asc     
 END

END      
GO