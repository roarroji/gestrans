﻿PRINT 'gsp_insertar_detalle_auxiliares_planilla_despachos'
GO
DROP PROCEDURE gsp_insertar_detalle_auxiliares_planilla_despachos
GO
CREATE PROCEDURE gsp_insertar_detalle_auxiliares_planilla_despachos
(@par_EMPR_Codigo smallint 
,@par_ENPD_Numero numeric(18,0) 
,@par_TERC_Codigo_Funcionario numeric(18,0) 
,@par_Numero_Horas_Trabajadas numeric(18,2) )
as
begin

INSERT INTO Detalle_Auxiliares_Planilla_Despachos
           (EMPR_Codigo
           ,ENPD_Numero
           ,TERC_Codigo_Funcionario
           ,Numero_Horas_Trabajadas)
     VALUES
           (@par_EMPR_Codigo
           ,@par_ENPD_Numero 
           ,@par_TERC_Codigo_Funcionario 
           ,@par_Numero_Horas_Trabajadas )
SELECT @par_ENPD_Numero AS Numero
end
GO