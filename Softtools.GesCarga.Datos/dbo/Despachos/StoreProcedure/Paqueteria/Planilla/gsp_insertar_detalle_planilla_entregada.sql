﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	Print 'gsp_insertar_detalle_planilla_entregada'
GO

DROP PROCEDURE gsp_insertar_detalle_planilla_entregada
GO

CREATE PROCEDURE gsp_insertar_detalle_planilla_entregada (      
@par_EMPR_Codigo NUMERIC,
@par_Numero_Planilla NUMERIC,
@par_Numero_Guia NUMERIC,
@par_Oficina NUMERIC
)     
AS BEGIN     
    
  DECLARE @CodZona NUMERIC = 0  

   DECLARE @CodCiudad NUMERIC = 0  
  select @CodCiudad = (select OFIC.CIUD_Codigo from Oficinas OFIC where OFIC.Codigo = @par_Oficina )
    
  INSERT INTO Detalle_Planilla_Entregas(  
         EMPR_Codigo,
		 ENPE_Numero,
		 ENRE_Numero,
		 CIUD_Codigo,
		 ZOCI_Codigo
         )      
         VALUES(    
        @par_EMPR_Codigo,
		@par_Numero_Planilla,
		@par_Numero_Guia, 
		@CodCiudad,
		1
         )  
  
  UPDATE Encabezado_Remesas SET ENPE_Numero = @par_Numero_Planilla WHERE Numero = @par_Numero_Guia AND TIDO_Codigo = 110
  
  SELECT @@ROWCOUNT As CantidadRegistros
     
END  
GO