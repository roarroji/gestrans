﻿		/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	Print 'gsp_insertar_encabezado_planilla_entregada'
GO
DROP PROCEDURE gsp_insertar_encabezado_planilla_entregada
GO
CREATE PROCEDURE gsp_insertar_encabezado_planilla_entregada (      
@par_EMPR_Codigo NUMERIC,
@par_TIDO_Codigo NUMERIC,
@par_Fecha DATE,
@par_VEHI_Codigo NUMERIC,
@par_TERC_Codigo_Conductor NUMERIC,
@par_Cantidad_Total NUMERIC,
@par_Peso_Total NUMERIC,
@par_Estado NUMERIC,
@par_Observaciones VARCHAR(250),
@par_USUA_Codigo_Crea NUMERIC,
@par_OFIC_Codigo NUMERIC,
@par_Numeracion NUMERIC, 
@par_Guias_Seleccionadas VARCHAR(500)
)     
AS BEGIN     
    
DECLARE @tblGuias table (Numero numeric)    
INSERT INTO @tblGuias    
SELECT * FROM dbo.Func_Dividir_String (@par_Guias_Seleccionadas, ',')    

    
-- De las guias traidos sacar los posibles planillados    
DECLARE @tblGuiasPlanilladas table (Numero numeric)    
INSERT INTO @tblGuiasPlanilladas     
SELECT Numero FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo    
AND Numero IN (SELECT Numero FROM @tblGuias)    
AND TIDO_Codigo = 110  
AND ENPE_Numero <> 0
    
-- De las guias traidos sacar los posibles anulados    
DECLARE @tblGuiasAnulados table (Numero numeric)    
INSERT INTO @tblGuiasAnulados     
SELECT Numero FROM Encabezado_Remesas WHERE EMPR_Codigo = @par_EMPR_Codigo   
AND Numero in (SELECT Numero FROM @tblGuias)    
AND Anulado = 1    
AND TIDO_Codigo = 110  
-- Guardar el listado de tiquetes planillados en un varchar para armar mensaje    
DECLARE @varGuiasPlanillados VARCHAR(500) = ''    
SELECT DISTINCT @varGuiasPlanillados = ISNULL(CAST((SELECT DISTINCT CONVERT(VARCHAR,Numero) + ', '     
FROM @tblGuiasPlanilladas FOR XML PATH(''))as varchar(max)), '')    
FROM @tblGuiasPlanilladas     
    
-- Guardar el listado de tiquetes anulados en un varchar para armar mensaje    
DECLARE @varGuiasAnulados VARCHAR(500) = ''    
SELECT DISTINCT @varGuiasAnulados = ISNULL(CAST((SELECT DISTINCT CONVERT(VARCHAR,Numero) + ', '     
FROM @tblGuiasAnulados FOR XML PATH(''))as varchar(max)), '')    
FROM @tblGuiasAnulados     
    
IF ISNULL(@varGuiasPlanillados,'') <> '' OR ISNULL(@varGuiasAnulados,'') <> '' BEGIN    
 DECLARE @varMensajeProceso VARCHAR(MAX) = ''    
 SET @varMensajeProceso = 'La planilla no se puede generar debido a que las guias presentan las siguientes inconsistencias: ' + char(10) + char(10)    
     
 IF ISNULL(@varGuiasPlanillados,'') <> '' BEGIN    
  SET @varMensajeProceso += '-> Guia No. ' + @varGuiasPlanillados + ' planillado(s).' + char(10)    
 END    
 IF ISNULL(@varGuiasAnulados,'') <> '' BEGIN    
  SET @varMensajeProceso += '-> Guia No. ' + @varGuiasAnulados + ' anulado(s).'    
 END    
    
 SELECT 0 As Numero,0 AS Numero_Documento, @varMensajeProceso As Mensaje    
  
END    
ELSE    
BEGIN    


  DECLARE @numNumeroDocumento NUMERIC = 0  
  DECLARE @numNumero NUMERIC = 0  
  
  EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_TIDO_Codigo, @par_OFIC_Codigo, @numNumeroDocumento OUTPUT  
    
  INSERT INTO Encabezado_Planilla_Entregas(  
  EMPR_Codigo ,
	
	TIDO_Codigo ,
	Numero_Documento ,
	Fecha ,
	VEHI_Codigo ,
	TERC_Codigo_Conductor ,
	Cantidad_Total ,
	Peso_Total ,
	Estado ,
	Fecha_Crea ,
	USUA_Codigo_Crea ,
	OFIC_Codigo ,
	Numeracion,
	Anulado , 
	Observaciones
         )      
         VALUES(    
        @par_EMPR_Codigo ,
	
		@par_TIDO_Codigo ,
		@numNumeroDocumento ,
		@par_Fecha ,
		@par_VEHI_Codigo ,
		@par_TERC_Codigo_Conductor ,
		@par_Cantidad_Total ,
		@par_Peso_Total ,
		@par_Estado ,
		GETDATE() ,
		@par_USUA_Codigo_Crea ,
		@par_OFIC_Codigo ,
		@par_Numeracion,
		0,
		@par_Observaciones
         )  
  SET @numNumero = @@IDENTITY  
  
  SELECT Numero, Numero_Documento FROM Encabezado_Planilla_Entregas WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @numNumero  
     END
END  
GO