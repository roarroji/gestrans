﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	
print 'gsp_liberar_guias_anuladas'
DROP PROCEDURE gsp_liberar_guias_anuladas
GO
CREATE PROCEDURE gsp_liberar_guias_anuladas (
@par_EMPR_Codigo NUMERIC, 
@par_ENPE_Numero NUMERIC,

@par_OFIC_Codigo NUMERIC
)
AS BEGIN 


UPDATE Encabezado_Remesas SET ENPE_Numero = 0 

FROM Detalle_Planilla_Entregas DPEN 
INNER JOIN Encabezado_Remesas ENRE on
DPEN.EMPR_Codigo =ENRE.EMPR_Codigo
AND DPEN.ENRE_Numero = ENRE.Numero
WHERE DPEN.ENPE_Numero = @par_ENPE_Numero
AND ENRE.TIDO_Codigo = 110
AND OFIC_Codigo = @par_OFIC_Codigo

END 

GO