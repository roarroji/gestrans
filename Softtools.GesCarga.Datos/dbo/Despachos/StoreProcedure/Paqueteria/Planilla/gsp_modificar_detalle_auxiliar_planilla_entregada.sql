﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

	PRINT 'gsp_modificar_detalle_auxiliar_planilla_entregada'

DROP PROCEDURE gsp_modificar_detalle_auxiliar_planilla_entregada
GO

CREATE PROCEDURE gsp_modificar_detalle_auxiliar_planilla_entregada(  
@par_EMPR_Codigo NUMERIC,  
@par_Numero_Planilla NUMERIC,   
@par_Terc_Funcionario NUMERIC,   
@par_Observaciones VARCHAR(200), 
@par_Horas_trabajadas NUMERIC, 
@par_Valor MONEY)  
  
AS BEGIN   

DECLARE @Exite_Registro NUMERIC

SET @Exite_Registro = (SELECT COUNT (*) FROM Detalle_Auxiliares_Planilla_Entregas WHERE EMPR_Codigo= @par_EMPR_Codigo 
AND ENPE_Numero = @par_Numero_Planilla AND TERC_Codigo_Funcionario = @par_Terc_Funcionario )

	IF @Exite_Registro > 0 BEGIN 
		
		UPDATE Detalle_Auxiliares_Planilla_Entregas SET    
		Numero_Horas_Trabajadas = @par_Horas_trabajadas,
		Observaciones = @par_Observaciones,
		valor = @par_Valor
		WHERE EMPR_Codigo= @par_EMPR_Codigo 
		AND ENPE_Numero = @par_Numero_Planilla 
		AND TERC_Codigo_Funcionario = @par_Terc_Funcionario   
		
		SELECT @@ROWCOUNT as ENPE_Numero

	END

	IF @Exite_Registro = 0 BEGIN 
		
		 INSERT INTO Detalle_Auxiliares_Planilla_Entregas(  
         EMPR_Codigo,
		 ENPE_Numero,
		 TERC_Codigo_Funcionario,
		 Numero_Horas_Trabajadas,
		 Valor,
		 Observaciones
		 )      
         VALUES(    
        @par_EMPR_Codigo,
		@par_Numero_Planilla,
		@par_Terc_Funcionario,
		@par_Horas_trabajadas, 
	    @par_Valor, 
		ISNULL(@par_Observaciones,'')
		)  
  
		 SELECT @@ROWCOUNT As ENPE_Numero
		
	END
END  
GO