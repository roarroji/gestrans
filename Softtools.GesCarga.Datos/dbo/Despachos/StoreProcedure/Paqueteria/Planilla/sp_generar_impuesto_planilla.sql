﻿Print 'sp_generar_impuesto_planilla'
GO
DROP PROCEDURE sp_generar_impuesto_planilla
GO
CREATE PROCEDURE sp_generar_impuesto_planilla (
@par_EMPR_Codigo	smallint,
@par_ENPD_Numero	numeric,
@par_TIDO_Codigo	numeric,
@par_RUTA_Codigo	numeric,
@par_Valor			money

)
AS
BEGIN


DECLARE @tblImpuesto As TABLE (EMPR_Codigo SMALLINT,
ENPD_Numero NUMERIC,
TIDO_Codigo NUMERIC,
ENIM_Codigo	NUMERIC,

Valor_Tarifa NUMERIC(18, 6),
Valor_Base	MONEY,
Valor MONEY,
Valor_Impuesto MONEY,
CIUD_Impuesto NUMERIC,
CATA_TRAI_Codigo NUMERIC,
Label VARCHAR(50),
Operacion SMALLINT
)

DECLARE @numCiudadDestino NUMERIC = 0
DECLARE @numDepartamento NUMERIC = 0

SELECT @numCiudadDestino = ISNULL(CIUD_Codigo_Destino,0) FROM Rutas
WHERE EMPR_Codigo = @par_EMPR_Codigo 
AND Codigo = @par_RUTA_Codigo 

SELECT @numDepartamento = ISNULL(DEPA_Codigo,0) FROM Ciudades
WHERE EMPR_Codigo = @par_EMPR_Codigo 
AND Codigo = @numCiudadDestino 

		-- Impuesto Nacional ---------
		INSERT INTO @tblImpuesto 
		SELECT ENIM.EMPR_Codigo, 
		@par_ENPD_Numero, 
		IMTD.TIDO_Codigo,
		ENIM.Codigo,
		ENIM.Valor_Tarifa,
		ENIM.Valor_Base,
		@par_Valor,
		CASE WHEN ENIM.Valor_Base < @par_Valor THEN @par_Valor * ENIM.Valor_Tarifa ELSE @par_Valor END,
		0,
		ENIM.CATA_TRAI_Codigo,
		ENIM.Label,
		CASE ENIM.Operacion WHEN '+' THEN 1 WHEN '-' THEN 2 ELSE 0 END

		FROM Encabezado_Impuestos ENIM, Impuestos_Tipo_Documentos  IMTD
		WHERE ENIM.EMPR_Codigo = IMTD.EMPR_Codigo 
		AND ENIM.Codigo = IMTD.ENIM_Codigo 
		AND IMTD.TIDO_Codigo = @par_TIDO_Codigo 
		AND ENIM.CATA_TRAI_Codigo = 801 --> PAIS


		-- Impuesto Ciudad ---------------
		INSERT INTO @tblImpuesto 
		SELECT ENIM.EMPR_Codigo, 
		@par_ENPD_Numero, 
		IMTD.TIDO_Codigo,
		ENIM.Codigo,
		CIIM.Valor_Tarifa,
		CIIM.Valor_Base,
		@par_Valor,
		CASE WHEN CIIM.Valor_Base < @par_Valor THEN @par_Valor * CIIM.Valor_Tarifa ELSE @par_Valor END,
		0,
		ENIM.CATA_TRAI_Codigo,
		ENIM.Label,
		CASE ENIM.Operacion WHEN '+' THEN 1 WHEN '-' THEN 2 ELSE 0 END

		FROM Encabezado_Impuestos ENIM, Impuestos_Tipo_Documentos  IMTD,
		Ciudad_Detalle_Impuestos CIIM
		WHERE ENIM.EMPR_Codigo = IMTD.EMPR_Codigo 
		AND ENIM.Codigo = IMTD.ENIM_Codigo 
		AND ENIM.EMPR_Codigo = CIIM.EMPR_Codigo 
		AND ENIM.Codigo = CIIM.ENIM_Codigo 

		AND CIIM.CIUD_Codigo = @numCiudadDestino 
		AND IMTD.TIDO_Codigo = @par_TIDO_Codigo 
		AND ENIM.CATA_TRAI_Codigo = 803 --> CIUDAD

		-- Impuesto Departamental -------------
		INSERT INTO @tblImpuesto 
		SELECT ENIM.EMPR_Codigo, 
		@par_ENPD_Numero, 
		IMTD.TIDO_Codigo,
		ENIM.Codigo,
		DEIM.Valor_Tarifa,
		DEIM.Valor_Base,
		@par_Valor,
		CASE WHEN DEIM.Valor_Base < @par_Valor THEN @par_Valor * DEIM.Valor_Tarifa ELSE @par_Valor END,
		0,
		ENIM.CATA_TRAI_Codigo,
		ENIM.Label,
		CASE ENIM.Operacion WHEN '+' THEN 1 WHEN '-' THEN 2 ELSE 0 END

		FROM Encabezado_Impuestos ENIM, Impuestos_Tipo_Documentos  IMTD,
		Departamento_Detalle_Impuestos DEIM
		WHERE ENIM.EMPR_Codigo = IMTD.EMPR_Codigo 
		AND ENIM.Codigo = IMTD.ENIM_Codigo 
		AND ENIM.EMPR_Codigo = DEIM.EMPR_Codigo 
		AND ENIM.Codigo = DEIM.ENIM_Codigo 

		AND DEIM.DEPA_Codigo = @numDepartamento 
		AND IMTD.TIDO_Codigo = @par_TIDO_Codigo 
		AND ENIM.CATA_TRAI_Codigo = 802 --> DEPARTAMENTO


SELECT * FROM @tblImpuesto 
		
	
END
GO