﻿CREATE PROCEDURE gsp_consultar_manifiesto_aceptacion_electronica_rndc       
(      
 @par_EMPR_Codigo  smallint,          
 @par_ENRE_NumeroDocumento NUMERIC = NULL,          
 @par_ENMC_NumeroDocumento NUMERIC = NULL,          
 @par_Fecha_Inicial date = NULL,          
 @par_Fecha_Final date = NULL,           
 @par_OFIC_Codigo NUMERIC = NULL,        
 @par_NumeroPagina INT = NULL,                  
 @par_RegistrosPagina INT = NULL                  
)                  
AS                  
BEGIN                  
          
 DECLARE @CantidadRegistros NUMERIC            
 SELECT @CantidadRegistros = (                  
  SELECT DISTINCT COUNT(1)                   
  FROM Encabezado_Manifiesto_Carga AS ENMC            
            
  WHERE ENMC.EMPR_Codigo = @par_EMPR_Codigo        
  AND ENMC.Estado = 1-- Estado Remesa Definitivo          
  AND ENMC.Anulado = 0  -- Remesa No Anulada 

  AND ENMC.Aceptacion_Electronica = 1
  AND (ENMC.Numero_Aceptacion_Electronica = 0  OR ENMC.Numero_Aceptacion_Electronica IS NULL)
  AND ENMC.Fecha_Aceptacion_Electronica IS NULL
 );                  
 WITH Pagina                  
 AS                  
 ( SELECT          
  0 As Obtener,          
  1 AS OpcionReporte,          
  ENMC.EMPR_Codigo,          
  ENMC.Numero_Manifiesto_Electronico AS Numero,          
  ENMC.Numero_Documento AS NumeroDocumento,              
  ENMC.Fecha,          
  ISNULL(ENMC.VEHI_Codigo, 0) AS VEHI_Codigo,               
  ISNULL(ENMC.SEMI_Codigo, 0) AS SEMI_Codigo,           
  ISNULL(ENMC.RUTA_Codigo,0) AS RUTA_Codigo,          
  ENMC.Numero_Documento AS NumeroDocumentoManifiesto,          
  ISNULL(ENMC.Mensaje_Manifiesto_Electronico, '') AS Mensaje,          
  ENMC.Numero_Manifiesto_Electronico AS NumeroConfirmacionMinisterio,
  ROW_NUMBER() OVER (ORDER BY ENMC.Numero) AS RowNumber                  
          
  FROM Encabezado_Manifiesto_Carga AS ENMC            
            
  WHERE ENMC.EMPR_Codigo = @par_EMPR_Codigo        
  AND ENMC.Estado = 1-- Estado Remesa Definitivo          
  AND ENMC.Anulado = 0  -- Remesa No Anulada 

  AND ENMC.Aceptacion_Electronica = 1
  AND (ENMC.Numero_Aceptacion_Electronica = 0  OR ENMC.Numero_Aceptacion_Electronica IS NULL)
  AND ENMC.Fecha_Aceptacion_Electronica IS NULL
 )                  
          
 SELECT          
 Obtener,          
 OpcionReporte,          
 EMPR_Codigo,          
 Numero,          
 NumeroDocumento,          
 Fecha,          
 VEHI_Codigo,               
 SEMI_Codigo,                
 RUTA_Codigo,           
 NumeroDocumentoManifiesto,          
 Mensaje,      
 NumeroConfirmacionMinisterio,
 @CantidadRegistros AS TotalRegistros,          
 @par_NumeroPagina AS PaginaObtener,          
 @par_RegistrosPagina AS RegistrosPagina          
          
 FROM Pagina          
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)           
END    
GO