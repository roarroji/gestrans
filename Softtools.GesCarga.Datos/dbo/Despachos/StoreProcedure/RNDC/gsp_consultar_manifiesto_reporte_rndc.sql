﻿PRINT 'gsp_consultar_manifiesto_reporte_rndc'
GO
DROP PROCEDURE gsp_consultar_manifiesto_reporte_rndc
GO
CREATE PROCEDURE gsp_consultar_manifiesto_reporte_rndc
(      
	@par_EMPR_Codigo  smallint,          
	@par_ENRE_NumeroDocumento NUMERIC = NULL,          
	@par_ENMC_NumeroDocumento NUMERIC = NULL,          
	@par_Fecha_Inicial date = NULL,          
	@par_Fecha_Final date = NULL,           
	@par_OFIC_Codigo NUMERIC = NULL,        
	@par_NumeroPagina INT = NULL,                  
	@par_RegistrosPagina INT = NULL                  
)                  
AS                  
BEGIN                  
          
	DECLARE @CantidadRegistros NUMERIC            
	SELECT @CantidadRegistros = (                  
		SELECT DISTINCT COUNT(1)                   
		FROM Encabezado_Manifiesto_Carga AS ENMC            
              
		LEFT JOIN Vehiculos AS VEHI             
		ON ENMC.EMPR_Codigo = VEHI.EMPR_Codigo            
		AND ENMC.VEHI_Codigo = VEHI.Codigo          
          
		LEFT JOIN Semirremolques AS SEMI          
		ON ENMC.EMPR_Codigo = SEMI.EMPR_Codigo            
		AND ENMC.SEMI_Codigo = SEMI.Codigo            
              
		LEFT JOIN Rutas AS RUTA             
		ON ENMC.EMPR_Codigo = RUTA.EMPR_Codigo            
		AND ENMC.RUTA_Codigo = RUTA.Codigo            
              
            
		WHERE ENMC.EMPR_Codigo = @par_EMPR_Codigo        
		AND ENMC.Estado = 1-- Estado Remesa Definitivo          
		AND ENMC.Anulado = 0  -- Remesa No Anulada          
		AND ENMC.Estado = 1 -- Manifiesto en Definitivo        
		AND (ENMC.Numero_Manifiesto_Electronico = 0  OR ENMC.Numero_Manifiesto_Electronico IS NULL)--Remesa Sin Reportar          
		AND (ENMC.Numero_Cumplido_Electronico = 0 OR ENMC.Numero_Cumplido_Electronico IS NULL)--Cumplido de remesa sin reportar           
		AND (CONVERT (DATE, ENMC.Fecha) >= @par_Fecha_Inicial OR @par_Fecha_Inicial IS NULL)
		AND (CONVERT (DATE, ENMC.Fecha) <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)
  
        
		AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)          
		AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)          
		AND RUTA.CIUD_Codigo_Origen <> RUTA.CIUD_Codigo_Destino       
		AND (ENMC.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)  
	);                  
	WITH Pagina                  
	AS                  
	( 
		SELECT DISTINCT
		0 As Obtener,          
		1 AS OpcionReporte,          
		ENMC.EMPR_Codigo,          
		ENMC.Numero,          
		ENMC.Numero_Documento AS NumeroDocumento,          
		ENMC.Fecha,          
		ISNULL(ENMC.VEHI_Codigo, 0) AS VEHI_Codigo,          
		ISNULL(VEHI.Placa,0) AS PlacaVehiculo,          
		ISNULL(ENMC.SEMI_Codigo, 0) AS SEMI_Codigo,          
		ISNULL(SEMI.Placa, 0) AS PlacaSemirremolque,          
		ISNULL(ENMC.RUTA_Codigo,0) AS RUTA_Codigo,          
		ISNULL(RUTA.Nombre, 0) AS NombreRuta,          
		ENMC.Numero_Documento AS NumeroDocumentoManifiesto,          
		ISNULL(ENMC.Mensaje_Manifiesto_Electronico, '') AS Mensaje,          
            
		ROW_NUMBER() OVER (ORDER BY ENMC.Numero) AS RowNumber                  
          
		FROM Encabezado_Manifiesto_Carga AS ENMC            
              
		LEFT JOIN Vehiculos AS VEHI             
		ON ENMC.EMPR_Codigo = VEHI.EMPR_Codigo            
		AND ENMC.VEHI_Codigo = VEHI.Codigo          
          
		LEFT JOIN Semirremolques AS SEMI          
		ON ENMC.EMPR_Codigo = SEMI.EMPR_Codigo            
		AND ENMC.SEMI_Codigo = SEMI.Codigo            
              
		LEFT JOIN Rutas AS RUTA             
		ON ENMC.EMPR_Codigo = RUTA.EMPR_Codigo            
		AND ENMC.RUTA_Codigo = RUTA.Codigo            
              
            
		WHERE ENMC.EMPR_Codigo = @par_EMPR_Codigo        
		AND ENMC.Estado = 1-- Estado Remesa Definitivo          
		AND ENMC.Anulado = 0  -- Remesa No Anulada          
		AND ENMC.Estado = 1 -- Manifiesto en Definitivo        
		AND (ENMC.Numero_Manifiesto_Electronico = 0  OR ENMC.Numero_Manifiesto_Electronico IS NULL)--Remesa Sin Reportar          
		AND (ENMC.Numero_Cumplido_Electronico = 0 OR ENMC.Numero_Cumplido_Electronico IS NULL)--Cumplido de remesa sin reportar          
		AND (CONVERT (DATE, ENMC.Fecha) >= @par_Fecha_Inicial OR @par_Fecha_Inicial IS NULL)
		AND (CONVERT (DATE, ENMC.Fecha) <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)         
		AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)          
		AND ENMC.Numero_Documento = ISNULL(@par_ENMC_NumeroDocumento,ENMC.Numero_Documento)          
		AND RUTA.CIUD_Codigo_Origen <> RUTA.CIUD_Codigo_Destino        
		AND (ENMC.OFIC_Codigo = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)  
	)                  
          
	SELECT
	Obtener,          
	OpcionReporte,          
	EMPR_Codigo,          
	Numero,          
	NumeroDocumento,          
	Fecha,          
	VEHI_Codigo,          
	PlacaVehiculo,          
	SEMI_Codigo,          
	PlacaSemirremolque,          
	RUTA_Codigo,          
	NombreRuta,          
	NumeroDocumentoManifiesto,          
	Mensaje,          
           
	@CantidadRegistros AS TotalRegistros,          
	@par_NumeroPagina AS PaginaObtener,          
	@par_RegistrosPagina AS RegistrosPagina          
          
	FROM Pagina          
	WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)           
END
GO