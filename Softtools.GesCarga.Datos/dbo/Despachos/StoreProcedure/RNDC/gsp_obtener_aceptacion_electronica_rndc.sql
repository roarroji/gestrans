﻿
CREATE PROCEDURE  gsp_obtener_aceptacion_electronica_rndc                        
(@par_EMPR_Codigo smallint,                          
@par_ENMC_Numero Numeric)                          
AS                          
BEGIN                          

SELECT DISTINCT                          
CONCAT (EMPR.Numero_Identificacion,  IIF(EMPR.Digito_Chequeo IS NULL OR  EMPR.Digito_Chequeo = 0, '', EMPR.Digito_Chequeo) ) AS NUMNITEMPRESATRANSPORTE,                           
ENMA.Numero_Manifiesto_Electronico AS INGRESOIDMANIFIESTO                        
                          
FROM Encabezado_Manifiesto_Carga AS ENMA                           
                          
LEFT JOIN Empresas AS EMPR                          
ON ENMA.EMPR_Codigo = EMPR.Codigo                                             
                    
WHERE ENMA.EMPR_Codigo = @par_EMPR_Codigo                          
AND ENMA.Numero_Manifiesto_Electronico = @par_ENMC_Numero                         
                          
END  
GO