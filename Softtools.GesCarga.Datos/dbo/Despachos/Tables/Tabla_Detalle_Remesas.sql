﻿PRINT 'Detalle_Remesas'
GO
ALTER TABLE [dbo].[Detalle_Remesas] DROP CONSTRAINT [FK_Detalle_Remesas_Ciudades_Destinatario]
GO
DROP TABLE [dbo].[Detalle_Remesas]
GO
CREATE TABLE [dbo].[Detalle_Remesas](
	[EMPR_Codigo] [numeric](18, 0) NOT NULL,
	[ENRE_TIDO_Codigo] [smallint] NOT NULL,
	[ENRE_Codigo] [numeric](18, 0) NOT NULL,
	[ID] [numeric](18, 0) NOT NULL,
	[PRTR_Codigo] [numeric](18, 0) NOT NULL,
	[Cantidad] [numeric](18, 2) NOT NULL,
	[Peso] [numeric](18, 2) NOT NULL,
	[Nombre_Destinatario] [varchar](50) NOT NULL,
	[CIUD_Codigo_Destinatario] [numeric](18, 0) NOT NULL,
	[Direccion_Destinatario] [varchar](80) NOT NULL,
	[Telefono_Destinatario] [varchar](60) NULL,
 CONSTRAINT [PK_Detalle_Remesas] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[ENRE_Codigo] ASC,
	[ID] ASC,
	[ENRE_TIDO_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Detalle_Remesas]  WITH CHECK ADD  CONSTRAINT [FK_Detalle_Remesas_Ciudades_Destinatario] FOREIGN KEY([EMPR_Codigo], [CIUD_Codigo_Destinatario])
REFERENCES [dbo].[Ciudades] ([EMPR_Codigo], [Codigo])
GO
ALTER TABLE [dbo].[Detalle_Remesas] CHECK CONSTRAINT [FK_Detalle_Remesas_Ciudades_Destinatario]
GO