﻿PRINT 'TABLA Encabezado_Remesas'
GO
DROP TABLE [dbo].[Encabezado_Remesas]
GO
CREATE TABLE [dbo].[Encabezado_Remesas](
	[EMPR_Codigo] [numeric](18, 0) NOT NULL,
	[Codigo] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TIDO_Codigo] [numeric](18, 0) NOT NULL,
	[Numero_Documento] [numeric](18, 0) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[RUTA_Codigo] [numeric](18, 0) NOT NULL,
	[VEHI_Codigo] [numeric](18, 0) NULL,
	[SEMI_Codigo] [numeric](18, 0) NULL,
	[TERC_Codigo_Conductor] [numeric](18, 0) NULL,
	[TERC_Codigo_Cliente] [numeric](18, 0) NULL,
	[Observaciones] [varchar](400) NULL,	
	[Cantidad_Cliente] [numeric](18, 2) NOT NULL,
	[Peso_Cliente] [numeric](18, 2) NOT NULL,
	[Anulado] [smallint] NOT NULL,
	[Estado] [smallint] NOT NULL,
	[Fecha_Crea] [datetime] NOT NULL,
	[USUA_Codigo_Crea] [numeric](18, 0)  NOT NULL,
	[Fecha_Modifica] [datetime] NULL,
	[USUA_Codigo_Modifica] [numeric](18, 0) NULL,
	[Fecha_Anula] [datetime] NULL,
	[USUA_Codigo_Anula] [numeric](18, 0) NULL,
	[Causa_Anula] [varchar](150) NULL,
	[OFIC_Codigo] [numeric](18, 0) NOT NULL,
	[CIUD_Codigo_Origen] [numeric](18, 0) NULL,
	[CIUD_Codigo_Destino] [numeric](18, 0) NULL
 CONSTRAINT [PK_Encabezado_Remesas] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] 

GO