﻿Print 'V_Estado_Remesa_Paqueteria'
GO
DROP VIEW V_Estado_Remesa_Paqueteria
GO
CREATE VIEW V_Estado_Remesa_Paqueteria
AS
	SELECT EMPR_Codigo, Codigo, CATA_Codigo, Campo1 As Nombre
	FROM Valor_Catalogos
	WHERE CATA_Codigo = 60
GO