﻿PRINT 'V_Tipo_Remesas'
GO

DROP VIEW V_Tipo_Remesas
GO

CREATE VIEW V_Tipo_Remesas
AS
	SELECT
		EMPR_Codigo,
		Codigo,
		CATA_Codigo,
		Campo1 AS Nombre,
		USUA_Codigo_Crea,
		Fecha_Crea,
		USUA_Modifica,
		Fecha_Modifica
	FROM
		Valor_Catalogos
	WHERE
		CATA_Codigo = 91
GO