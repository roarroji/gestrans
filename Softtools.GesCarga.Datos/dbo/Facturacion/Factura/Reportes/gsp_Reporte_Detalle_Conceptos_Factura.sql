﻿--------------------------------------------------------- v1 MO

--Print 'CREATE PROCEDURE gsp_Reporte_Detalle_Conceptos_Factura'
--GO

--DROP PROCEDURE gsp_Reporte_Detalle_Conceptos_Factura    
--GO
--CREATE PROCEDURE gsp_Reporte_Detalle_Conceptos_Factura      
--(         
--@par_Empr_Codigo NUMERIC              
--, @par_Numero_Factura  NUMERIC             
--)        
--AS BEGIN   
  
--SELECT  
--COVE.Nombre AS NombreConcepto,  
--ENIM.Nombre As NombreImpuesto,   
--MIN(DICF.Valor_Tarifa) As Valor_Tarifa,   
--SUM(DICF.Valor_Base) As Valor_Base,   
--SUM(DICF.Valor_Impuesto)  As Valor_Impuesto  
  
--FROM Detalle_Impuestos_Conceptos_Facturas DICF  
  
--INNER JOIN Detalle_Conceptos_Facturas DECF ON  
--DICF.EMPR_Codigo = DECF.EMPR_Codigo  
--AND DICF.ENFA_Numero = DECF.ENFA_Numero  
--AND DICF.DECF_Codigo = DECF.Codigo  
  
--LEFT JOIN Conceptos_Ventas COVE ON  
--DECF.EMPR_Codigo = COVE.EMPR_Codigo  
--AND DECF.COVE_Codigo = COVE.Codigo  
  
--LEFT JOIN Encabezado_Impuestos ENIM ON  
--DICF.EMPR_Codigo = ENIM.EMPR_Codigo  
--AND DICF.ENIM_Codigo = ENIM.Codigo  
  
--WHERE DECF.EMPR_Codigo = @par_Empr_Codigo        
--AND DECF.ENFA_Numero = @par_Numero_Factura  
--AND DICF.Valor_Impuesto > 0  
  
--GROUP BY COVE.Nombre, ENIM.Nombre  
--ORDER BY COVE.Nombre ASC  
--END   
--GO
PRINT 'gsp_Reporte_Detalle_Conceptos_Factura'
GO
DROP PROCEDURE gsp_Reporte_Detalle_Conceptos_Factura
GO
CREATE PROCEDURE gsp_Reporte_Detalle_Conceptos_Factura
(             
 @par_Empr_Codigo NUMERIC                  
 , @par_Numero_Factura  NUMERIC                 
)            
AS   
BEGIN       
	SELECT      
	COVE.Nombre AS NombreConcepto,  
	COVE.Operacion AS OperacionConcepto,  
	1 AS Cantidad,  
	SUM(DECF.Valor_Concepto) As Valor_Concepto,
	MAX(DECF.Descripcion) AS Descripcion
       
	FROM Detalle_Conceptos_Facturas DECF      
    
	LEFT JOIN Conceptos_Ventas COVE ON      
	DECF.EMPR_Codigo = COVE.EMPR_Codigo      
	AND DECF.COVE_Codigo = COVE.Codigo      
      
	WHERE DECF.EMPR_Codigo = @par_Empr_Codigo            
	AND DECF.ENFA_Numero = @par_Numero_Factura      
      
	GROUP BY COVE.Nombre, COVE.Operacion  
	ORDER BY COVE.Nombre ASC      
END
GO