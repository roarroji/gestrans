﻿Print 'CREATE PROCEDURE gsp_reporte_impuestos_factura_remesas'
GO
DROP PROCEDURE gsp_reporte_impuestos_factura_remesas
GO
  
CREATE PROCEDURE gsp_reporte_impuestos_factura_remesas (    
@par_EMPR_Codigo SMALLINT,    
@par_Numero   NUMERIC    
)    
AS    
BEGIN  
SELECT 
ENIM.Nombre AS NombreImpuesto,
MIN(ENIM.Codigo) AS CodigoImpuesto,
MIN(DEIF.Valor_Tarifa) AS Valor_Tarifa, 
MIN(DEIF.Valor_Base) AS Valor_Base, 
SUM(DEIF.Valor_Impuesto) AS Valor_Impuesto

FROM Detalle_Impuestos_Facturas AS DEIF

LEFT JOIN Encabezado_Impuestos AS ENIM ON
DEIF.EMPR_Codigo = ENIM.EMPR_Codigo
AND DEIF.ENIM_Codigo = ENIM.Codigo

LEFT JOIN Valor_Catalogos AS CATA ON
DEIF.EMPR_Codigo = CATA.EMPR_Codigo
AND DEIF.CATA_TVFI_Codigo = CATA.Codigo

WHERE DEIF.EMPR_Codigo = @par_EMPR_Codigo
AND DEIF.ENFA_Numero = @par_Numero

GROUP BY ENIM.Nombre

END    
GO