﻿
Print 'V_Tipo_Naturaleza_Terceros'
GO
DROP VIEW V_Tipo_Naturaleza_Terceros
GO
CREATE VIEW V_Tipo_Naturaleza_Terceros
AS
	SELECT EMPR_Codigo, Codigo, CATA_Codigo, Campo1 As Nombre FROM
	Valor_Catalogos
	WHERE CATA_Codigo = 5
GO

--------------------------------------------------------------

Print 'V_Tipo_Documento_Terceros'
GO
DROP VIEW V_Tipo_Documento_Terceros
GO
CREATE VIEW V_Tipo_Documento_Terceros
AS
	SELECT EMPR_Codigo, Codigo, CATA_Codigo, Campo1 As Nombre FROM
	Valor_Catalogos
	WHERE CATA_Codigo = 1
GO


--------------------------------------------------------------

Print 'V_Tipo_Documento_Empresas'
GO
DROP VIEW V_Tipo_Documento_Empresas
GO
CREATE VIEW V_Tipo_Documento_Empresas
AS
	SELECT EMPR_Codigo, Codigo, CATA_Codigo, Campo1 As Nombre FROM
	Valor_Catalogos
	WHERE CATA_Codigo = 1
GO

--------------------------------------------------------------JO

Print 'CREATE PROCEDURE gsp_consultar_encabezado_facturas'
GO
DROP PROCEDURE gsp_consultar_encabezado_facturas
GO

CREATE PROCEDURE gsp_consultar_encabezado_facturas (        
        
 @par_EMPR_Codigo  smallint,        
 @par_Numero    numeric = NULL,        
 @par_Numero_Documento numeric = NULL,
 @par_CATA_TIFA_Codigo     numeric = NULL,  
 @par_Fecha_Inicial  date = NULL,        
 @par_Fecha_Final  date = NULL,        
 @par_TERC_Cliente  numeric = NULL,        
 @par_TERC_Facturar  numeric = NULL,        
 @par_Estado    smallint = NULL,        
 @par_Anulado   smallint = NULL,        
 @par_OFIC_Facturar  smallint = NULL,        
 @par_NumeroPagina INT = NULL,        
 @par_RegistrosPagina INT = NULL        
)        
AS        
BEGIN        
         
 DECLARE @CantidadRegistros NUMERIC  
 
 SELECT @CantidadRegistros = (        
        
  SELECT DISTINCT COUNT(1)         
          
 FROM Encabezado_Facturas ENFA     
    
 LEFT JOIN    
 Factura_Electronica FAEL ON    
 ENFA.EMPR_Codigo = FAEL.EMPR_Codigo    
 AND ENFA.Numero = FAEL.ENFA_Numero    
     
 LEFT JOIN    
 Empresa_Factura_Electronica EMFE ON    
 ENFA.EMPR_Codigo = EMFE.EMPR_Codigo    
         
 INNER JOIN        
 Oficinas OFIC ON        
 ENFA.EMPR_Codigo = OFIC.EMPR_Codigo        
 AND ENFA.OFIC_Factura = OFIC.Codigo        
        
 INNER JOIN        
 Terceros CLIE ON        
 ENFA.EMPR_Codigo = CLIE.EMPR_Codigo        
 AND ENFA.TERC_Cliente = CLIE.Codigo        
        
 INNER JOIN        
 Terceros TEFA ON        
 ENFA.EMPR_Codigo = TEFA.EMPR_Codigo        
 AND ENFA.TERC_Facturar = TEFA.Codigo        
        
 INNER JOIN        
 V_Forma_Pago_Ventas FPVE ON        
 ENFA.EMPR_Codigo = FPVE.EMPR_Codigo        
 AND ENFA.CATA_FPVE_Codigo = FPVE.Codigo        
        
 WHERE        
 ENFA.EMPR_Codigo = @par_EMPR_Codigo 
 AND (ENFA.CATA_TIFA_Codigo = @par_CATA_TIFA_Codigo OR @par_CATA_TIFA_Codigo IS NULL)
 AND (ENFA.Numero = @par_Numero OR @par_Numero IS NULL)        
 AND (ENFA.Numero_Documento = @par_Numero_Documento OR @par_Numero_Documento IS NULL)
 AND (ENFA.Fecha >= @par_Fecha_Inicial OR @par_Fecha_Inicial IS NULL)        
 AND (ENFA.Fecha <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)        
 AND (ENFA.TERC_Cliente = @par_TERC_Cliente OR @par_TERC_Cliente IS NULL)        
 AND (ENFA.TERC_Facturar = @par_TERC_Facturar OR @par_TERC_Facturar IS NULL)        
 AND (ENFA.OFIC_Factura = @par_OFIC_Facturar OR @par_OFIC_Facturar IS NULL)        
 AND (ENFA.Estado = @par_Estado OR @par_Estado IS NULL)  
 AND (ENFA.Anulado = @par_Anulado OR @par_Anulado IS NULL)
        
 );        
 WITH Pagina        
        
 AS        
 (        
 SELECT         
        
 0 As Obtener,        
 ENFA.EMPR_Codigo,        
 ENFA.Numero,        
 ENFA.TIDO_Codigo,        
 ENFA.Numero_Documento,        
 ENFA.Numero_Preimpreso,  
 ENFA.CATA_TIFA_Codigo, 
 ENFA.Fecha,        
 ENFA.OFIC_Factura,        
 OFIC.Nombre As OficinaFactura,        
     
 ENFA.TERC_Cliente,        
 ISNULL(CLIE.Razon_Social,'') + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,'') As NombreCliente,        
 ENFA.TERC_Facturar,        
 ISNULL(TEFA.Razon_Social,'') + ISNULL(TEFA.Nombre,'') + ' ' + ISNULL(TEFA.Apellido1,'') + ' ' + ISNULL(TEFA.Apellido2,'') As NombreFacturarA,        
 ENFA.CATA_FPVE_Codigo,        
 FPVE.Nombre As FormaPagoVenta,        
        
 ENFA.Valor_Factura,        
 ENFA.Valor_Remesas,     
 ISNULL(ENFA.Valor_Otros_Conceptos,0) As Valor_Otros_Conceptos,      
 ISNULL(ENFA.Valor_Impuestos,0) As Valor_Impuestos,    
 ISNULL(ENFA.Factura_Electronica,0) As Estado_FAEL,         
 ENFA.Estado,        
 ENFA.Anulado,        
 ENFA.Fecha_Crea,        
 ENFA.USUA_Codigo_Crea,        
 ENFA.Fecha_Modifica,        
 ENFA.USUA_Codigo_Modifica,    
 0 As ObtFacEle,     
 ISNULL(FAEL.ID_Numero_Documento_Proveedor, '') AS ID_Num_Doc_Pro,          
 ROW_NUMBER() OVER (ORDER BY ENFA.Numero) AS RowNumber        
        
 FROM Encabezado_Facturas ENFA        
     
 LEFT JOIN    
 Factura_Electronica FAEL ON    
 ENFA.EMPR_Codigo = FAEL.EMPR_Codigo    
 AND ENFA.Numero = FAEL.ENFA_Numero    
     
 INNER JOIN        
 Oficinas OFIC ON        
 ENFA.EMPR_Codigo = OFIC.EMPR_Codigo        
 AND ENFA.OFIC_Factura = OFIC.Codigo        
        
 INNER JOIN        
 Terceros CLIE ON        
 ENFA.EMPR_Codigo = CLIE.EMPR_Codigo        
 AND ENFA.TERC_Cliente = CLIE.Codigo        
        
 INNER JOIN        
 Terceros TEFA ON        
 ENFA.EMPR_Codigo = TEFA.EMPR_Codigo        
 AND ENFA.TERC_Facturar = TEFA.Codigo        
        
 INNER JOIN        
 V_Forma_Pago_Ventas FPVE ON        
 ENFA.EMPR_Codigo = FPVE.EMPR_Codigo        
 AND ENFA.CATA_FPVE_Codigo = FPVE.Codigo        
        
 WHERE        
 ENFA.EMPR_Codigo = @par_EMPR_Codigo        
 AND (ENFA.CATA_TIFA_Codigo = @par_CATA_TIFA_Codigo OR @par_CATA_TIFA_Codigo IS NULL)
 AND (ENFA.Numero = @par_Numero OR @par_Numero IS NULL)        
 AND (ENFA.Numero_Documento = @par_Numero_Documento OR @par_Numero_Documento IS NULL)
 AND (ENFA.Fecha >= @par_Fecha_Inicial OR @par_Fecha_Inicial IS NULL)        
 AND (ENFA.Fecha <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)        
 AND (ENFA.TERC_Cliente = @par_TERC_Cliente OR @par_TERC_Cliente IS NULL)        
 AND (ENFA.TERC_Facturar = @par_TERC_Facturar OR @par_TERC_Facturar IS NULL)        
 AND (ENFA.OFIC_Factura = @par_OFIC_Facturar OR @par_OFIC_Facturar IS NULL)        
 AND (ENFA.Estado = @par_Estado OR @par_Estado IS NULL)        
 AND (ENFA.Anulado = @par_Anulado OR @par_Anulado IS NULL)
 )        
        
 SELECT         
 Obtener,        
 EMPR_Codigo,        
 Numero,        
 TIDO_Codigo,        
 Numero_Documento,        
 Numero_Preimpreso,
 CATA_TIFA_Codigo, 
 Fecha,        
 OFIC_Factura,        
 OficinaFactura,        
        
 TERC_Cliente,        
 NombreCliente,        
 TERC_Facturar,        
 NombreFacturarA,        
 CATA_FPVE_Codigo,        
 FormaPagoVenta,        
        
 Valor_Factura,        
 Valor_Remesas,    
 Valor_Otros_Conceptos,    
 Valor_Impuestos ,    
 Estado_FAEL,        
 Estado,        
 Anulado,        
 Fecha_Crea,        
 USUA_Codigo_Crea,        
 Fecha_Modifica,        
 USUA_Codigo_Modifica,        
 ObtFacEle,      
 ID_Num_Doc_Pro,     
 @CantidadRegistros AS TotalRegistros,        
 @par_NumeroPagina AS PaginaObtener,        
 @par_RegistrosPagina AS RegistrosPagina        
        
 FROM Pagina        
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)        
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)        
 ORDER BY Numero        
        
END 
GO

--------------------------------------------------------------JO 
  
Print 'CREATE PROCEDURE gsp_obtener_encabezado_facturas'
GO
DROP PROCEDURE gsp_obtener_encabezado_facturas
GO
  
CREATE PROCEDURE gsp_obtener_encabezado_facturas (    
@par_EMPR_Codigo SMALLINT,    
@par_Numero NUMERIC = NULL,
@par_NumeroDocumento NUMERIC = NULL,
@par_TERC_Cliente NUMERIC = NULL,
@par_Estado NUMERIC = NULL,
@par_Anulado NUMERIC = NULL
)    
AS    
BEGIN    
    
 SELECT     
  1 As Obtener,    
  ENFA.EMPR_Codigo,    
  ENFA.Numero,    
  ENFA.TIDO_Codigo,    
  ENFA.Numero_Documento,    
  ENFA.Fecha,    
  ENFA.OFIC_Factura,    
  ENFA.TERC_Cliente,    
  ENFA.TERC_Facturar,
  ENFA.CATA_TIFA_Codigo,
  ENFA.CATA_FPVE_Codigo,    
  ISNULL(ENFA.Dias_Plazo,0) As Dias_Plazo,    
  ISNULL(ENFA.Observaciones,'') As Observaciones,    
  ISNULL(ENFA.Valor_Remesas,0) As Valor_Remesas,    
  ISNULL(ENFA.Valor_Otros_Conceptos,0) As Valor_Otros_Conceptos,    
  ISNULL(ENFA.Valor_Descuentos,0) As Valor_Descuentos,    
  ISNULL(ENFA.Subtotal,0) As Subtotal,    
  ISNULL(ENFA.Valor_Impuestos,0) As Valor_Impuestos,    
  ISNULL(ENFA.Valor_Factura,0) As Valor_Factura,    
  ISNULL(ENFA.Valor_TRM,0) As Valor_TRM,    
  ISNULL(ENFA.MONE_Codigo_Alterna,0) As MONE_Codigo_Alterna,    
  ISNULL(ENFA.Valor_Moneda_Alterna,0) As Valor_Moneda_Alterna,    
  ISNULL(ENFA.Valor_Anticipo,0) As Valor_Anticipo,    
  ISNULL(ENFA.Factura_Electronica,0) As Estado_FAEL,    
  ISNULL(ENFA.Estado,0) As Estado,    
    
  ISNULL(ENFA.Anulado,0) As Anulado,    
  ISNULL(ENFA.Fecha_Anula,'01/01/1900') As Fecha_Anula,    
  ISNULL(ENFA.USUA_Codigo_Anula,0) As USUA_Codigo_Anula,    
  ISNULL(ENFA.Causa_Anula,'') As Causa_Anula,  
  0 AS ObtFacEle  
    
  FROM Encabezado_Facturas ENFA    
  WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo    
  AND ENFA.Numero = ISNULL(@par_Numero, ENFA.Numero) 
  AND ENFA.Numero_Documento = ISNULL (@par_NumeroDocumento,ENFA.Numero_Documento)
  AND ENFA.TERC_Cliente = ISNULL (@par_TERC_Cliente,ENFA.TERC_Cliente)
  AND ENFA.Estado = ISNULL (@par_Estado,ENFA.Estado)
  AND ENFA.Anulado = ISNULL (@par_Anulado,ENFA.Anulado)
    
 END    
GO

--------------------------------------------------------------

Print 'CREATE PROCEDURE gsp_insertar_encabezado_facturas'
GO
DROP PROCEDURE gsp_insertar_encabezado_facturas
GO

CREATE PROCEDURE gsp_insertar_encabezado_facturas (  

@par_EMPR_Codigo      smallint,  
@par_TIDO_Codigo      numeric,  
@par_Numero_Preimpreso     varchar(20),  
@par_CATA_TIFA_Codigo     numeric,  
@par_Fecha        date,  
@par_OFIC_Factura      numeric,  
@par_TERC_Cliente      numeric,  
@par_TERC_Facturar      numeric,  
@par_CATA_FPVE_Codigo     numeric,  
@par_Dias_Plazo       smallint,  
@par_Observaciones      varchar(500),  
@par_Valor_Remesas      money,  
@par_Valor_Otros_Conceptos    money,  
@par_Valor_Descuentos     money,  
@par_Subtotal       money,  
@par_Valor_Impuestos     money,  
@par_Valor_Factura      money,  
@par_Valor_TRM       money,  
@par_MONE_Codigo_Alterna    numeric,  
@par_Valor_Moneda_Alterna    money,  
@par_Valor_Anticipo      money,  
@par_Resolucion_Facturacion    varchar(500),  
@par_Control_Impresion     smallint,  
@par_Estado        smallint,  
@par_USUA_Codigo_Crea     smallint,  
@par_Anulado       smallint  


)  

AS  
BEGIN  

DECLARE @numNumeroFactura NUMERIC = 0  

--Consumo Prefactura  
IF ISNULL(@par_Estado,0) = 0 BEGIN  
SET @par_TIDO_Codigo = 175 --> Prefactura  
END  

EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_TIDO_Codigo, @par_OFIC_Factura, @numNumeroFactura OUTPUT  


INSERT INTO Encabezado_Facturas (  
EMPR_Codigo,  
TIDO_Codigo,  
Numero_Documento,  
Numero_Preimpreso,  
CATA_TIFA_Codigo, 
Fecha,  
OFIC_Factura,  
TERC_Cliente,  
TERC_Facturar,  
CATA_FPVE_Codigo,  
Dias_Plazo,  
Observaciones,  
Valor_Remesas,  
Valor_Otros_Conceptos,  
Valor_Descuentos,  
Subtotal,  
Valor_Impuestos,  
Valor_Factura,  
Valor_TRM,  
MONE_Codigo_Alterna,  
Valor_Moneda_Alterna,  
Valor_Anticipo,  
Resolucion_Facturacion,  
Control_Impresion,  
Estado,  
Fecha_Crea,  
USUA_Codigo_Crea,  
Anulado,  
Factura_Electronica  

)  

VALUES(  
@par_EMPR_Codigo,  
@par_TIDO_Codigo,  
@numNumeroFactura,  
@par_Numero_Preimpreso, 
@par_CATA_TIFA_Codigo,
@par_Fecha,  
@par_OFIC_Factura,  
@par_TERC_Cliente,  
@par_TERC_Facturar,  
@par_CATA_FPVE_Codigo,  
@par_Dias_Plazo,  
@par_Observaciones,  
@par_Valor_Remesas,  
@par_Valor_Otros_Conceptos,  
@par_Valor_Descuentos,  
@par_Subtotal,  
@par_Valor_Impuestos,  
@par_Valor_Factura,  
@par_Valor_TRM,  
@par_MONE_Codigo_Alterna,  
@par_Valor_Moneda_Alterna,  
@par_Valor_Anticipo,  
@par_Resolucion_Facturacion,  
@par_Control_Impresion,  
@par_Estado,  
GETDATE(),  
@par_USUA_Codigo_Crea,  
@par_Anulado,  
0  
)  

SELECT Numero, Numero_Documento FROM Encabezado_Facturas   
WHERE EMPR_Codigo = @par_EMPR_Codigo  
AND Numero_Documento = @numNumeroFactura  
AND TIDO_Codigo = @par_TIDO_Codigo  
AND CATA_TIFA_Codigo = @par_CATA_TIFA_Codigo

END  
GO

--------------------------------------------------------------

Print 'CREATE PROCEDURE gsp_obtener_encabezado_factura_electronica_saphety'
GO
DROP PROCEDURE gsp_obtener_encabezado_factura_electronica_saphety
GO

CREATE PROCEDURE gsp_obtener_encabezado_factura_electronica_saphety (
@par_EMPR_Codigo smallint,
@par_Numero		 NUMERIC
)
AS
BEGIN

	SELECT 
	ENFA.EMPR_Codigo,
	ENFA.Numero, 
	EMPR.Numero_Identificacion AS IDEmpresaEmisora,
	CASE TIDE.Codigo 
	WHEN 101 THEN 'IdentityCard' 
	WHEN 102 THEN 'NIT' 
	WHEN 103 THEN 'ForeignerIdentification' 
	ELSE TIDE.Nombre END  As TipoIDEmpresaEmisora,
	ENFA.Numero_Documento,
	ENFA.Valor_Otros_Conceptos AS Valor_Otros_Conceptos,
	ENFA.Valor_Remesas AS Valor_Remesas,
	ENFA.Subtotal AS TotalBruto,
	ENFA.Valor_Impuestos AS Valor_Impuestos,
	ENFA.Valor_Factura,
	CLIE.Numero_Identificacion,
	CLIE.Direccion AS DireccionCliente,
	CLIE.Codigo_Postal AS CodigoPostalCliente,
	CLIE.Emails As CorreoCliente,
	ENFA.Fecha AS FechaCreacion,  
	DATEADD(DAY, 1,ENFA.Fecha) AS FechaVence,
	CASE TINT.Codigo
	WHEN 501 THEN 'Natural' 
	WHEN 502 THEN 'Legal' 
	ELSE TINT.Nombre END  As NombreTipoNaturalezaCliente,
	CASE TIDT.Codigo 
	WHEN 101 THEN 'IdentityCard' 
	WHEN 102 THEN 'NIT' 
	WHEN 103 THEN 'ForeignerIdentification' 
	ELSE TIDT.Nombre END  As NombreTipoDocumentoCliente,   
	ISNULL(CLIE.Razon_Social,'') + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,'') As NombreCompletoCliente,
	CIUD.Nombre AS CiudadCliente,
	DEPA.Nombre AS DistritoCliente,
	ISNULL(ENFA.Factura_Electronica,0) As Estado_FAEL
	
	FROM Encabezado_Facturas ENFA
	
	INNER JOIN 
	Empresas AS EMPR ON
	ENFA.EMPR_Codigo = EMPR.Codigo
	
	INNER JOIN
	V_Tipo_Documento_Empresas AS TIDE ON
	EMPR.Codigo = TIDE.EMPR_Codigo
	AND EMPR.CATA_TIID_Codigo = TIDE.Codigo

	INNER JOIN 
	Terceros AS CLIE ON
	ENFA.EMPR_Codigo = CLIE.EMPR_Codigo
	AND ENFA.TERC_Cliente = CLIE.Codigo

	INNER JOIN
	V_Tipo_Naturaleza_Terceros AS TINT ON
	CLIE.EMPR_Codigo = TINT.EMPR_Codigo
	AND CLIE.CATA_TINT_Codigo = TINT.Codigo

	INNER JOIN
	V_Tipo_Documento_Terceros AS TIDT ON
	CLIE.EMPR_Codigo = TIDT.EMPR_Codigo
	AND CLIE.CATA_TIID_Codigo = TIDT.Codigo

	INNER JOIN
	Ciudades AS CIUD ON
	CLIE.EMPR_Codigo = CIUD.EMPR_Codigo
	AND CLIE.CIUD_Codigo = CIUD.Codigo

	INNER JOIN
	Departamentos AS DEPA ON
	CIUD.EMPR_Codigo = DEPA.EMPR_Codigo
	AND CIUD.DEPA_Codigo = DEPA.Codigo
	
	WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo
	AND ENFA.Numero = @par_Numero

END
GO

--------------------------------------------------------------

Print 'CREATE PROCEDURE gsp_obtener_tipo_factura_electronica'
GO
DROP PROCEDURE gsp_obtener_tipo_factura_electronica
GO

CREATE PROCEDURE gsp_obtener_tipo_factura_electronica (
@par_EMPR_Codigo smallint,
@par_Numero		 NUMERIC
)
AS
BEGIN

--------- CONSULTAR FACTURA ELECTRONICA ------------

	DECLARE @varCUFE						  VARCHAR(80) = ''
	DECLARE @varID_Numero_Documento_Proveedor VARCHAR(50) = ''
	DECLARE @varQR_code						  numeric  = 0

	SELECT 
	@varCUFE = ISNULL(CUFE, ''), 
	@varID_Numero_Documento_Proveedor = ISNULL(ID_Numero_Documento_Proveedor, ''), 
	@varQR_code = IIF(QR_code = NULL, 0, 1)

	FROM  Factura_Electronica

	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND  ENFA_Numero = @par_Numero
	
------- Crear variable tabla para retornar datos de la factura

	DECLARE @tblFactura TABLE (
		EMPR_Codigo			  smallint,
		Numero				  numeric,
		IDEmpresaEmisora	  numeric,
		TipoIDEmpresaEmisora  varchar(100) DEFAULT '',
		Numero_Documento	  numeric,
		Valor_Otros_Conceptos MONEY,
		Valor_Remesas 		  MONEY,
		Subtotal			  MONEY,
		Valor_Impuestos		  MONEY,
		Valor_Factura		  MONEY,
		Numero_Identificacion numeric,
		DireccionCliente	  varchar(100) DEFAULT '',
		CodigoPostalCliente   varchar(100) DEFAULT '',
		CorreoCliente		  varchar(100) DEFAULT '',
		FechaCreacion		  varchar(100) DEFAULT '',
		FechaVence			  varchar(100) DEFAULT '',
		NombreTipoNaturaleza  varchar(100) DEFAULT '',
		NombreTipoDocumento   varchar(100) DEFAULT '',
		RazonSocialCliente	  varchar(100) DEFAULT '',
		CiudadCliente		  varchar(100) DEFAULT '',
		DistritoCliente		  varchar(100) DEFAULT '',
		Estado_FAEL			  numeric,
		ObtFacEle		      numeric,
		CUFE				  VARCHAR(80) DEFAULT '',
		ID_Num_Doc_Pro		  VARCHAR(80) DEFAULT '',
		QR_code				  numeric
	)
------- Inserta en la tabla datos de la factura
	INSERT INTO @tblFactura(
							EMPR_Codigo, 
							Numero, 
							IDEmpresaEmisora, 
							TipoIDEmpresaEmisora, 
							Numero_Documento, 
							Valor_Otros_Conceptos, 
							Valor_Remesas,
							Subtotal, 
							Valor_Impuestos,
							Valor_Factura, 
							Numero_Identificacion, 
							DireccionCliente, 
							CodigoPostalCliente, 
							CorreoCliente, 
							FechaCreacion, 
							FechaVence, 
							NombreTipoNaturaleza, 
							NombreTipoDocumento, 
							RazonSocialCliente, 
							CiudadCliente, 
							DistritoCliente,
							Estado_FAEL
							)
	
	EXEC gsp_obtener_encabezado_factura_electronica_saphety @par_EMPR_Codigo, @par_Numero
	UPDATE @tblFactura 
	SET ObtFacEle = 1,
	CUFE = @varCUFE,	 
	ID_Num_Doc_Pro	= @varID_Numero_Documento_Proveedor,
	QR_code	= @varQR_code
	SELECT * FROM @tblFactura
END
GO

--------------------------------------------------------------

Print 'CREATE PROCEDURE gsp_insertar_Factura_Electronica'
GO
DROP PROCEDURE gsp_insertar_Factura_Electronica
GO
  
CREATE PROCEDURE gsp_insertar_Factura_Electronica (  
@par_EMPR_Codigo  smallint,  
@par_ENFA_Numero  NUMERIC,  
@par_Nume_Docu   NUMERIC,  
@par_CUFE    VARCHAR(50) = '',  
@par_Fecha_Envio  DATETIME,  
@par_QRcode    VARBINARY(MAX)  
)  
AS  
BEGIN
	IF EXISTS   
	(SELECT ENFA_Numero   
	FROM Factura_Electronica   
	WHERE EMPR_Codigo = @par_EMPR_Codigo   
	AND ENFA_Numero=@par_ENFA_Numero) BEGIN  
		DELETE Factura_Electronica WHERE  EMPR_Codigo = @par_EMPR_Codigo  AND ENFA_Numero=@par_ENFA_Numero
	END  
	
	INSERT INTO Factura_Electronica  
	(EMPR_Codigo, ENFA_Numero, Numero_Documento, CUFE, Fecha_Envio, QR_code)  
	VALUES  
	(@par_EMPR_Codigo, @par_ENFA_Numero, @par_Nume_Docu, @par_CUFE, @par_Fecha_Envio, @par_QRcode)  
	
	SELECT ENFA_Numero AS ENFA_Numero  
	FROM Factura_Electronica  
	WHERE EMPR_Codigo = @par_EMPR_Codigo   
	AND ENFA_Numero=@par_ENFA_Numero  
	
END  

--------------------------------------------------------------

Print 'CREATE PROCEDURE gsp_actualizar_Factura_Electronica'
GO
DROP PROCEDURE gsp_actualizar_Factura_Electronica
GO

CREATE PROCEDURE gsp_actualizar_Factura_Electronica (
@par_EMPR_Codigo	   smallint,
@par_ENFA_Numero	   NUMERIC,
@par_ID_NUM_DOC_PRO	   VARCHAR(MAX) = ''
)
AS
BEGIN
	UPDATE Factura_Electronica SET 
	ID_Numero_Documento_Proveedor = @par_ID_NUM_DOC_PRO
	
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND ENFA_Numero = @par_ENFA_Numero
	
	UPDATE Encabezado_Facturas SET 
	Factura_Electronica = 1
	
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND Numero = @par_ENFA_Numero
	
	SELECT ENFA_Numero AS ENFA_Numero
	FROM Factura_Electronica
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND ENFA_Numero=@par_ENFA_Numero
END
GO

--------------------------------------------------------------GT

Print 'CREATE PROCEDURE gsp_consultar_novedades_planilla_despachos'
GO
DROP PROCEDURE gsp_consultar_novedades_planilla_despachos
GO

CREATE PROCEDURE gsp_consultar_novedades_planilla_despachos     
(            
	@par_EMPR_Codigo SMALLINT,       
	@par_ENPD_Numero NUMERIC = NULL,
	@par_ENRE_Numero NUMERIC = NULL
)            
AS            
BEGIN                
	SELECT DISTINCT           
	DEND.EMPR_Codigo,            
	DEND.ID,            
	DEND.ENPD_Numero,      
	ENPD.Numero_Documento AS ENPD_Numero_Documento,    
	ISNULL(DEND.ENRE_Numero, 0) AS ENRE_Numero,    
	ISNULL(ENRE.Numero_Documento, 0) AS ENRE_Numero_Documento,
	ISNULL(COVE.Codigo, 0) AS COVE_Codigo,
	ISNULL(COVE.Nombre, '') AS COVE_Nombre,
	DEND.NODE_Codigo,    
	ISNULL(NDES.Nombre, '') As Nombre_Novedad,      
	DEND.Observaciones,       
	ISNULL(DEND.Valor_Compra, 0) AS Valor_Compra,    
	ISNULL(DEND.Valor_Venta, 0) AS Valor_Venta,     
	ISNULL(DEND.TERC_Codigo_Proveedor, 0) AS TERC_Codigo_Proveedor,     
	ISNULL(PROV.Razon_Social,'') + ' ' + ISNULL(PROV.Nombre,'') + ' ' + ISNULL(PROV.Apellido1,'') + ' ' + ISNULL(PROV.Apellido2,'') As Nombre_Proveedor,     
	ISNULL(DEND.USUA_Codigo_Crea, 0) AS USUA_Codigo_Crea,     
	ISNULL(USUA.Codigo_Usuario,'') As Nombre_Usuario_Crea,    
	ISNULL(DEND.Fecha_Crea,'') As Fecha_Crea,    
	DEND.Anulado,  
	ISNULL(CLPD.Codigo,0) as CLPD_Codigo,  
	ISNULL( CLPD.Valor_Fijo  , 0) as Valor_Fijo,  
	ISNULL( CLPD.Operacion  , 0) as Operacion  
	
	FROM            
	Detalle_Novedades_Despacho as DEND      
	  
	LEFT JOIN Encabezado_Planilla_Despachos AS ENPD      
	ON DEND.EMPR_Codigo = ENPD.EMPR_Codigo      
	AND DEND.ENPD_Numero = ENPD.Numero      

	LEFT JOIN Encabezado_Remesas AS ENRE      
	ON DEND.EMPR_Codigo = ENRE.EMPR_Codigo      
	AND DEND.ENRE_Numero = ENRE.Numero      

	LEFT JOIN Novedades_Despacho AS NDES      
	ON DEND.EMPR_Codigo = NDES.EMPR_Codigo      
	AND DEND.NODE_Codigo = NDES.Codigo      

	LEFT JOIN Novedades_Conceptos AS NOCO     
	ON NDES.EMPR_Codigo = NOCO.EMPR_Codigo      
	AND NDES.Codigo = NOCO.NODE_Codigo 
	
	LEFT JOIN Conceptos_Ventas AS COVE     
	ON NOCO.EMPR_Codigo = COVE.EMPR_Codigo      
	AND NOCO.COVE_Codigo = COVE.Codigo

	LEFT JOIN Conceptos_Liquidacion_Planilla_Despachos AS CLPD     
	ON NOCO.EMPR_Codigo = CLPD.EMPR_Codigo      
	AND NOCO.CLPD_Codigo = CLPD.Codigo  
	   

	LEFT JOIN Terceros AS PROV      
	ON DEND.EMPR_Codigo = PROV.EMPR_Codigo      
	AND DEND.TERC_Codigo_Proveedor = PROV.Codigo      

	LEFT JOIN Usuarios AS USUA      
	ON DEND.EMPR_Codigo = USUA.EMPR_Codigo      
	AND DEND.USUA_Codigo_Crea = USUA.Codigo      
	  
	WHERE            
	DEND.EMPR_Codigo = @par_EMPR_Codigo            
	AND DEND.ENPD_Numero = ISNULL(@par_ENPD_Numero, DEND.ENPD_Numero) 
	AND DEND.ENRE_Numero = ISNULL(@par_ENRE_Numero, DEND.ENRE_Numero)
END      
GO


--------------------------------------------------------------GT

Print 'CREATE PROCEDURE gsp_insertar_lista_factura_empresa'
GO
DROP PROCEDURE gsp_insertar_lista_factura_empresa
GO

CREATE PROCEDURE gsp_insertar_lista_factura_empresa   
(@par_EMPR_Codigo SMALLINT,        
@par_Proveedor VARCHAR(50) = NULL,       
@par_Usuario VARCHAR(50) = NULL,       
@par_Clave VARCHAR(50) = NULL,        
@par_Operador_Virtual VARCHAR(50) = NULL,      
@par_Clave_Tecnica VARCHAR(50) = NULL,      
@par_Clave_Externa VARCHAR(50) = NULL      
)        
AS        
BEGIN        
        
INSERT INTO [dbo].Empresa_Factura_Electronica        
           (EMPR_Codigo      
           ,Proveedor        
           ,Usuario      
           ,Clave      
           ,Operador_Virtual      
           ,Clave_Tecnica
		   ,Clave_Externa        
         )        
     VALUES        
           (@par_EMPR_Codigo       
            ,@par_Proveedor  
            ,@par_Usuario  
            ,@par_Clave  
            ,@par_Operador_Virtual  
            ,@par_Clave_Tecnica
			,@par_Clave_Externa)  
      
END
GO       

--------------------------------------------------------------GT

Print 'CREATE PROCEDURE gsp_consultar_lista_factura_empresa'
GO
DROP PROCEDURE gsp_consultar_lista_factura_empresa
GO

  
CREATE PROCEDURE gsp_consultar_lista_factura_empresa       
(@par_EMPR_Codigo SMALLINT)          
          
AS          
BEGIN          
SELECT            
EMPR_Codigo AS Codigo_Empresa          
,ISNULL(Proveedor, '') AS Proveedor           
,ISNULL(Usuario, '') AS Usuario          
,ISNULL(Clave, '') AS Clave          
,ISNULL(Operador_Virtual, '') AS Operador_Virtual          
,ISNULL(Clave_Tecnica, '') AS Clave_Tecnica          
,ISNULL(Clave_Externa, '') AS Clave_Externa  
          
FROM Empresa_Factura_Electronica       
          
WHERE           
EMPR_Codigo = @par_EMPR_Codigo             
          
END 
GO

--------------------------------------------------------------


Print 'CREATE PROCEDURE gsp_insertar_detalle_Conceptos_facturas'
GO
DROP PROCEDURE gsp_insertar_detalle_Conceptos_facturas
GO

CREATE PROCEDURE gsp_insertar_detalle_Conceptos_facturas (  
@par_EMPR_Codigo	 smallint,  
@par_ENFA_Numero	 numeric, 
@par_DEND_ID 	 	 numeric, 
@par_COVE_Codigo 	 numeric,  
@par_Descripcion 	 VARCHAR(250), 
@par_Valor_Concepto  MONEY,  
@par_Valor_Impuestos MONEY,  
@par_ENRE_Numero 	 numeric,  
@par_ENOS_Numero 	 numeric,
@par_EMOE_Numero 	 numeric
)  
AS  
BEGIN  
  
 INSERT INTO Detalle_Conceptos_Facturas (  
 EMPR_Codigo,  
 ENFA_Numero,  
 DEND_ID, 
 COVE_Codigo,  
 Descripcion,
 Valor_Concepto,
 Valor_Impuestos,
 ENRE_Numero,
 ENOS_Numero,
 EMOE_Numero
 )  
 VALUES(  
 @par_EMPR_Codigo,  
 @par_ENFA_Numero, 
 @par_DEND_ID,
 @par_COVE_Codigo,
 @par_Descripcion,
 @par_Valor_Concepto,
 @par_Valor_Impuestos,
 @par_ENRE_Numero,
 @par_ENOS_Numero,
 @par_EMOE_Numero 
 )
 
 SELECT ENFA_Numero, Codigo
 FROM Detalle_Conceptos_Facturas
 WHERE EMPR_Codigo = @par_EMPR_Codigo  
 AND ENFA_Numero = @par_ENFA_Numero 
 AND Codigo = @@identity
 
END  
GO

--------------------------------------------------------------

Print 'CREATE PROCEDURE gsp_insertar_detalle_Impuestos_Conceptos_facturas'
GO
DROP PROCEDURE gsp_insertar_detalle_Impuestos_Conceptos_facturas
GO

CREATE PROCEDURE gsp_insertar_detalle_Impuestos_Conceptos_facturas (  
@par_EMPR_Codigo	 smallint,  
@par_ENFA_Numero	 numeric, 
@par_DECF_Codigo 	 numeric, 
@par_ENIM_Codigo 	 numeric,  
@par_Valor_tarifa 	 MONEY, 
@par_Valor_base      MONEY,  
@par_Valor_impuesto  MONEY
)  
AS  
BEGIN  
  
 INSERT INTO Detalle_Impuestos_Conceptos_Facturas (  
 EMPR_Codigo,  
 ENFA_Numero,  
 DECF_Codigo, 
 ENIM_Codigo,  
 Valor_Tarifa,
 Valor_Base,
 Valor_Impuesto
 )  
 VALUES(  
 @par_EMPR_Codigo,  
 @par_ENFA_Numero, 
 @par_DECF_Codigo,
 @par_ENIM_Codigo,
 @par_Valor_tarifa,
 @par_Valor_base,
 @par_Valor_impuesto
 )
 
 SELECT ENFA_Numero
 FROM Detalle_Impuestos_Conceptos_Facturas
 WHERE EMPR_Codigo = @par_EMPR_Codigo  
 AND ENFA_Numero = @par_ENFA_Numero 
 AND DECF_Codigo = @par_DECF_Codigo
 AND ENIM_Codigo = @par_ENIM_Codigo
 
END  
GO

--------------------------------------------------------------

Print 'CREATE PROCEDURE gsp_insertar_detalle_Impuestos_facturas'
GO
DROP PROCEDURE gsp_insertar_detalle_Impuestos_facturas
GO

CREATE PROCEDURE gsp_insertar_detalle_Impuestos_facturas (  
@par_EMPR_Codigo	  smallint,  
@par_ENFA_Numero	  numeric, 
@par_ENIM_Codigo 	  numeric, 
@par_CATA_TVFI_Codigo numeric,  
@par_Valor_tarifa 	  MONEY, 
@par_Valor_base       MONEY,  
@par_Valor_impuesto   MONEY
)  
AS  
BEGIN  
  
 INSERT INTO Detalle_Impuestos_Facturas (  
 EMPR_Codigo,  
 ENFA_Numero,  
 ENIM_Codigo, 
 CATA_TVFI_Codigo,  
 Valor_Tarifa,
 Valor_Base,
 Valor_Impuesto
 )  
 VALUES(  
 @par_EMPR_Codigo,  
 @par_ENFA_Numero, 
 @par_ENIM_Codigo,
 @par_CATA_TVFI_Codigo,
 @par_Valor_tarifa,
 @par_Valor_base,
 @par_Valor_impuesto
 )
 
 SELECT ENFA_Numero
 FROM Detalle_Impuestos_Facturas
 WHERE EMPR_Codigo = @par_EMPR_Codigo  
 AND ENFA_Numero = @par_ENFA_Numero 
 AND CATA_TVFI_Codigo = @par_CATA_TVFI_Codigo
 
END  
GO

--------------------------------------------------------------

Print 'CREATE PROCEDURE gsp_consultar_detalle_conceptos_facturas'
GO
DROP PROCEDURE gsp_consultar_detalle_conceptos_facturas
GO

CREATE PROCEDURE gsp_consultar_detalle_conceptos_facturas (  
@par_EMPR_Codigo smallint,  
@par_ENFA_Numero   numeric  
)  
AS  
BEGIN  
  
 SELECT
 DECF.EMPR_Codigo,  
 DECF.ENFA_Numero,
 DECF.Codigo,
 ISNULL(DECF.DEND_ID, 0) As DEND_ID, 
 ISNULL(DECF.COVE_Codigo, 0) As COVE_Codigo, 
 ISNULL(COVE.Nombre,'') As COVE_Nombre,
 ISNULL(DECF.Descripcion,'') As Descripcion,
 DECF.Valor_Concepto,
 DECF.Valor_Impuestos,
 DECF.ENRE_Numero, 
 ISNULL(ENRE.Numero_Documento, 0) As ENRE_Numero_Documento, 
 DECF.ENOS_Numero,
 DECF.EMOE_Numero
  
 FROM Detalle_Conceptos_Facturas DECF  

 LEFT JOIN  
 Encabezado_Remesas ENRE ON  
 DECF.EMPR_Codigo = ENRE.EMPR_Codigo 
 AND DECF.ENRE_Numero = ENRE.Numero  
  
 INNER JOIN  
 Conceptos_Ventas COVE ON  
 DECF.EMPR_Codigo = COVE.EMPR_Codigo  
 AND DECF.COVE_Codigo = COVE.Codigo 
  
 WHERE DECF.EMPR_Codigo = @par_EMPR_Codigo  
 AND DECF.ENFA_Numero = @par_ENFA_Numero  
  
END  
GO

--------------------------------------------------------------

Print 'CREATE PROCEDURE gsp_consultar_detalle_impuestos_facturas'
GO
DROP PROCEDURE gsp_consultar_detalle_impuestos_facturas
GO

CREATE PROCEDURE gsp_consultar_detalle_impuestos_facturas (  
@par_EMPR_Codigo smallint,  
@par_ENFA_Numero   numeric  
)  
AS  
BEGIN  
  
 SELECT
 DEIF.EMPR_Codigo,  
 DEIF.ENFA_Numero,
 DEIF.ENIM_Codigo,
 ENIM.Nombre as ENIM_Nombre,
 DEIF.CATA_TVFI_Codigo,
 DEIF.Valor_Tarifa,
 DEIF.Valor_Base,
 DEIF.Valor_Impuesto
 
 FROM Detalle_Impuestos_Facturas AS DEIF
 
 INNER JOIN Encabezado_Impuestos AS ENIM ON
 ENIM.EMPR_Codigo = DEIF.EMPR_Codigo
 AND ENIM.CODIGO = DEIF.ENIM_Codigo
 
 WHERE DEIF.EMPR_Codigo = @par_EMPR_Codigo  
 AND DEIF.ENFA_Numero = @par_ENFA_Numero  
  
END  
GO


--------------------------------------------------------------JO

Print 'CREATE PROCEDURE gsp_modificar_encabezado_facturas'
GO
DROP PROCEDURE gsp_modificar_encabezado_facturas
GO

CREATE PROCEDURE gsp_modificar_encabezado_facturas (  
  
 @par_EMPR_Codigo smallint,  
 @par_Numero numeric,  
  
 @par_Fecha      datetime,  
 @par_OFIC_Factura    smallint,  
 @par_TERC_Cliente    numeric,  
 @par_TERC_Facturar    numeric,  
 @par_CATA_FPVE_Codigo   numeric,  
 @par_Dias_Plazo     int = NULL,  
  
 @par_Observaciones    varchar(500) = NULL,  
 @par_Valor_Remesas    money = NULL,  
 @par_Valor_Otros_Conceptos  money = NULL,  
 @par_Valor_Descuentos   money = NULL,  
 @par_Subtotal     money = NULL,  
  
 @par_Valor_Impuestos   money = NULL,  
 @par_Valor_Factura    money = NULL,  
 @par_Valor_TRM     money =  NULL,  
 @par_MONE_Codigo_Alterna  money = NULL,  
 @par_Valor_Moneda_Alterna  money = NULL,  
  
 @par_Valor_Anticipo    money = NULL,  
 @par_Estado      money = NULL  
   
)  
  
AS  
BEGIN  
--Variable Tipo Factura
Declare @par_FactDefinitivo numeric
set @par_FactDefinitivo = 170
-- Limpia el detalle  
UPDATE Encabezado_Remesas SET ENFA_Numero = 0 WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENFA_Numero = @par_Numero  
DELETE Detalle_Remesas_Facturas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENFA_Numero = @par_Numero 
DELETE Detalle_Impuestos_Conceptos_Facturas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENFA_Numero = @par_Numero 
DELETE Detalle_Conceptos_Facturas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENFA_Numero = @par_Numero 
DELETE Detalle_Impuestos_Facturas WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENFA_Numero = @par_Numero 


--Actuviliza el encabezado  
UPDATE Encabezado_Facturas SET  

Fecha = @par_Fecha,  
OFIC_Factura = @par_OFIC_Factura,  
TERC_Cliente = @par_TERC_Cliente,  
TERC_Facturar = @par_TERC_Facturar,  
CATA_FPVE_Codigo = @par_CATA_FPVE_Codigo,  
Dias_Plazo  = ISNULL(@par_Dias_Plazo,0),  
Observaciones  = ISNULL(@par_Observaciones,''),  
Valor_Remesas = ISNULL(@par_Valor_Remesas,0),  
Valor_Otros_Conceptos = ISNULL(@par_Valor_Otros_Conceptos,0),  
Valor_Descuentos = ISNULL(@par_Valor_Descuentos,0),  
Subtotal = ISNULL(@par_Subtotal,0),  
Valor_Impuestos = ISNULL(@par_Valor_Impuestos,0),  
Valor_Factura = ISNULL(@par_Valor_Factura,0),  
Valor_TRM = ISNULL(@par_Valor_TRM,0),  
MONE_Codigo_Alterna = ISNULL(@par_MONE_Codigo_Alterna,0),  
Valor_Moneda_Alterna = ISNULL(@par_Valor_Moneda_Alterna,0),  
Valor_Anticipo = ISNULL(@par_Valor_Anticipo,0),  
Estado = ISNULL(@par_Estado,0)  

FROM Encabezado_Facturas ENFA  
WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo  
AND ENFA.NUmero = @par_Numero  

--- Consumo de factura definitiva  
IF ISNULL(@par_Estado,0) = 1 BEGIN  
DECLARE @numNumeroFactura NUMERIC = 0  
EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_FactDefinitivo, @par_OFIC_Factura, @numNumeroFactura OUTPUT  

UPDATE Encabezado_Facturas SET Numero_Documento = @numNumeroFactura, TIDO_Codigo =  @par_FactDefinitivo  
WHERE EMPR_Codigo = @par_EMPR_Codigo  
AND Numero = @par_Numero  
END  

--- Retorna la modificación  
SELECT Numero, Numero_Documento FROM Encabezado_Facturas ENFA  
WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo  
AND ENFA.NUmero = @par_Numero
AND ENFA.TIDO_Codigo = @par_FactDefinitivo
  
END  
GO


----------------------------------------------------------

Print 'CREATE PROCEDURE gsp_consultar_impuestos_conceptos_facturacion'
GO
DROP PROCEDURE gsp_consultar_impuestos_conceptos_facturacion
GO

CREATE PROCEDURE gsp_consultar_impuestos_conceptos_facturacion (          
  @par_EMPR_Codigo SMALLINT  ,           
  @par_Codigo NUMERIC = NULL ,
  @par_CIUD_Codigo NUMERIC = NULL,        
  @par_NumeroPagina  NUMERIC = NULL,        
  @par_RegistrosPagina NUMERIC = NULL      
 )          
AS          
BEGIN          
	SET NOCOUNT ON;          
	DECLARE          
	@CantidadRegistros INT          
	SELECT @CantidadRegistros = (          
		SELECT DISTINCT           
		COUNT(1)           

		FROM Impuestos_Conceptos_Ventas AS ICLP 

		INNER JOIN       
		Encabezado_Impuestos AS ENIM      
		ON ICLP.EMPR_Codigo = ENIM.EMPR_Codigo       
		AND ICLP.ENIM_Codigo = ENIM.Codigo   
		   
		WHERE  ICLP.EMPR_Codigo = @par_EMPR_Codigo        
		AND ICLP.COVE_Codigo = ISNULL(@par_Codigo,ICLP.COVE_Codigo)        
		AND ENIM.Estado = 1      
	);                 
	WITH Pagina AS          
	(SELECT          
	ENIM.Codigo,      
	ENIM.Nombre,      
	ENIM.Operacion,
	ISNULL(CASE WHEN ENIM.CATA_TRAI_Codigo = 803 THEN 
	(SELECT Valor_Base
	 FROM Detalle_Ciudad_Impuestos AS DECI
	 WHERE DECI.EMPR_Codigo =  ENIM.EMPR_Codigo 
	 AND  DECI.ENIM_Codigo = ENIM.Codigo 
	 AND CIUD_Codigo = @par_CIUD_Codigo ) 
	 ELSE ENIM.Valor_Base END, ENIM.Valor_Base) AS Valor_Base,
	ISNULL(CASE WHEN ENIM.CATA_TRAI_Codigo = 803 THEN 
	(SELECT Valor_Tarifa 
	FROM Detalle_Ciudad_Impuestos AS DECI
	WHERE DECI.EMPR_Codigo =  ENIM.EMPR_Codigo 
	AND  DECI.ENIM_Codigo = ENIM.Codigo 
	AND CIUD_Codigo = @par_CIUD_Codigo ) 
	ELSE ENIM.Valor_Tarifa END, ENIM.Valor_Tarifa) AS Valor_Tarifa,
	ISNULL(PUC.Codigo_Cuenta,0) PLUC_Codigo,      
	ENIM.Estado,      
	ROW_NUMBER() OVER ( ORDER BY  ENIM.Codigo) AS RowNumber          
	FROM Impuestos_Conceptos_Ventas ICLP INNER JOIN       
	Encabezado_Impuestos ENIM      
	ON ICLP.EMPR_Codigo = ENIM.EMPR_Codigo       
	AND ICLP.ENIM_Codigo = ENIM.Codigo      
	LEFT JOIN Plan_Unico_Cuentas PUC      
	ON ENIM.EMPR_Codigo = PUC.EMPR_Codigo      
	AND ENIM.PLUC_Codigo = PUC.Codigo      
	WHERE  ICLP.EMPR_Codigo = @par_EMPR_Codigo        
	AND ICLP.COVE_Codigo = ISNULL(@par_Codigo,ICLP.COVE_Codigo)        
	AND ENIM.Estado = 1
    )         
   SELECT DISTINCT          
	Codigo,        
	Nombre,      
	Operacion,       
	Estado,      
	Valor_Base,      
	Valor_Tarifa,      
	PLUC_Codigo,      
	@CantidadRegistros AS TotalRegistros,          
	@par_NumeroPagina AS PaginaObtener,          
	@par_RegistrosPagina AS RegistrosPagina          
	FROM          
	Pagina          
	WHERE          
	RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                  
END 
GO	

---------------------------------------------------------- SC

Print 'CREATE PROCEDURE gsp_consultar_impuestos_Tipo_documento'
GO
DROP PROCEDURE gsp_consultar_impuestos_Tipo_documento
GO

 CREATE PROCEDURE gsp_consultar_impuestos_Tipo_documento  
(      
  @par_EMPR_Codigo SMALLINT,      
  @par_Codigo NUMERIC = NULL,    
  @par_CodigoAlterno VARCHAR(20) = NULL,     
  @par_Nombre VARCHAR(100)= NULL,       
  @par_CATA_TRAI_Codigo NUMERIC= NULL,    
  @par_CATA_TIIM_Codigo NUMERIC= NULL,    
  @par_Estado SMALLINT = NULL,    
  @par_NumeroPagina INT = NULL,      
  @par_RegistrosPagina INT = NULL  ,    
  @par_TIDO_Codigo NUMERIC = NULL,    
  @par_CIUD_Codigo NUMERIC = NULL   
 )      
 AS      
 BEGIN      
   
      SELECT       
      ENIM.EMPR_Codigo,    
   ENIM.Codigo_Alterno,      
   ENIM.Codigo,     
   ENIM.Nombre,    
   ENIM.Label,     
   ENIM.CATA_TIIM_Codigo,     
   TIIM.Campo1 Tipo_Impuesto,    
   ENIM.CATA_TRAI_Codigo,     
   TRAI.Campo1 Tipo_Recaudo,    
   CASE WHEN ENIM.CATA_TRAI_Codigo = 803 THEN ISNULL((SELECT Valor_Tarifa FROM Detalle_Ciudad_Impuestos WHERE EMPR_Codigo =  ENIM.EMPR_Codigo AND  ENIM_Codigo = ENIM.Codigo AND CIUD_Codigo = @par_CIUD_Codigo ),ENIM.Valor_Tarifa)  ELSE ENIM.Valor_Tarifa END AS Valor_Tarifa,         
   CASE WHEN ENIM.CATA_TRAI_Codigo = 803 THEN ISNULL((SELECT Valor_Base FROM Detalle_Ciudad_Impuestos WHERE EMPR_Codigo =  ENIM.EMPR_Codigo AND  ENIM_Codigo = ENIM.Codigo AND CIUD_Codigo = @par_CIUD_Codigo ),ENIM.Valor_Base) ELSE ENIM.Valor_Base END AS Valor_Base,
   ENIM.Operacion,    
   ENIM.Estado,    
   USCR.Codigo USUA_Codigo_Crea ,    
   ENIM.Fecha_Crea ,    
   USMO.Codigo USUA_Codigo_Modifica ,    
   ENIM.Fecha_Modifica,    
   ENIM.PLUC_Codigo,    
   Obtener=0,  
        0 AS TotalRegistros,      
   0 AS PaginaObtener,      
   0 AS RegistrosPagina   
      FROM      
     Encabezado_Impuestos  ENIM    
    LEFT JOIN Impuestos_Tipo_Documentos AS ITDO  
  ON ENIM.EMPR_Codigo = ITDO.EMPR_Codigo  
  AND ENIM.Codigo = ITDO.ENIM_Codigo
    
  
      , usuarios USCR     
      , usuarios USMO     
   , Valor_Catalogos TIIM    
   , Valor_Catalogos TRAI    
      WHERE      
       ENIM.EMPR_Codigo = @par_EMPR_Codigo       
      AND ENIM.Codigo > 0      
   AND ENIM.EMPR_Codigo = USCR.EMPR_Codigo    
      AND ENIM.USUA_Codigo_Crea = USCR.Codigo    
   AND ENIM.EMPR_Codigo = USMO.EMPR_Codigo    
      AND ENIM.USUA_Codigo_Crea = USMO.Codigo    
   AND ENIM.EMPR_Codigo = TIIM.EMPR_Codigo    
   AND ENIM.CATA_TIIM_Codigo = TIIM.Codigo    
   AND ENIM.EMPR_Codigo = TRAI.EMPR_Codigo    
   AND ENIM.CATA_TRAI_Codigo = TRAI.Codigo    
   AND ITDO.TIDO_Codigo = @par_TIDO_Codigo
   AND ENIM.ESTADO  = ISNULL(@par_Estado, ENIM.ESTADO)
   AND ITDO.ESTADO  = ISNULL(@par_Estado, ITDO.ESTADO)
   END       
GO

-----------------------------------------------------------

Print 'CREATE PROCEDURE gsp_consultar_impuestos_concepto_venta'
GO
DROP PROCEDURE gsp_consultar_impuestos_concepto_venta
GO


CREATE PROCEDURE gsp_consultar_impuestos_concepto_venta (  
@par_EMPR_Codigo smallint,
@par_COVE_Codigo smallint
)  
AS  
BEGIN  

--Variable Estado Impuesto
Declare @par_estado_impuesto numeric
set @par_estado_impuesto = 1 

SELECT ENIM.EMPR_Codigo, ENIM.Codigo, ENIM.Operacion, ENIM.Valor_Tarifa, ENIM.Valor_Base
FROM Impuestos_Conceptos_Ventas  IMCV

INNER JOIN
Encabezado_Impuestos ENIM ON
IMCV.EMPR_Codigo = ENIM.EMPR_Codigo 
AND IMCV.ENIM_Codigo = ENIM.Codigo 

INNER JOIN Conceptos_Ventas COVE ON 
IMCV.EMPR_Codigo = COVE.EMPR_Codigo
AND IMCV.COVE_Codigo = COVE.Codigo

WHERE 
IMCV.EMPR_Codigo = @par_EMPR_Codigo
AND ENIM.Estado = @par_estado_impuesto
AND COVE.Codigo = @par_COVE_Codigo
  
END  
GO


-----------------------------------------------------------JO


  
Print 'CREATE PROCEDURE gsp_consultar_detalle_remesa_facturas'
GO
DROP PROCEDURE gsp_consultar_detalle_remesa_facturas
GO

CREATE PROCEDURE gsp_consultar_detalle_remesa_facturas (  
@par_EMPR_Codigo smallint,  
@par_ENFA_Numero   numeric  
)  
AS  
BEGIN  
    
 SELECT DERF.EMPR_Codigo,    
 DERF.ENFA_Numero,    
 DERF.ENRE_Numero,    
 ISNULL(ENOS.Numero_Documento, 0) AS ENOS_Numero_Documento,
 ISNULL(ENPD.Numero_Documento, 0) AS ENPD_Numero_Documento,
 ISNULL(ENMC.Numero_Documento, 0) AS ENMC_Numero_Documento,
 ENRE.Fecha,    
 ENRE.Numero_Documento As ENRE_Numero_Documento,  
 ISNULL(ENRE.MBL_Contenedor, 'MBL') AS MBL_Contenedor,    
 RUTA.Nombre As NombreRuta,    
 ENRE.Documento_Cliente As Numero_Documento_Cliente,    
 ENRE.Valor_Flete_Cliente,    
 PROD.Nombre As NombreProducto,    
 ENRE.Cantidad_Cliente,    
 ENRE.Peso_Cliente,    
 ENRE.Total_Flete_Cliente,    
 ISNULL(ENRE.Observaciones,'') As Observaciones    
    
 FROM Detalle_Remesas_Facturas DERF    
    
 INNER JOIN    
 Encabezado_Remesas AS ENRE ON    
 DERF.EMPR_Codigo = ENRE.EMPR_Codigo    
 AND DERF.ENRE_Numero = ENRE.Numero    
    
 INNER JOIN    
 Rutas AS RUTA ON    
 ENRE.EMPR_Codigo = RUTA.EMPR_Codigo    
 AND ENRE.RUTA_Codigo = RUTA.Codigo    
    
 INNER JOIN    
 Producto_Transportados AS PROD ON    
 ENRE.EMPR_Codigo = PROD.EMPR_Codigo    
 AND ENRE.PRTR_Codigo = PROD.Codigo
 
 LEFT JOIN    
 Encabezado_Planilla_Despachos AS ENPD ON    
 ENRE.EMPR_Codigo = ENPD.EMPR_Codigo    
 AND ENRE.ENPD_Numero = ENPD.Numero    

 LEFT JOIN    
 Encabezado_Solicitud_Orden_Servicios AS ENOS ON    
 ENRE.EMPR_Codigo = ENOS.EMPR_Codigo    
 AND ENRE.ESOS_Numero = ENOS.Numero

 LEFT JOIN    
 Encabezado_Manifiesto_Carga AS ENMC ON    
 ENRE.EMPR_Codigo = ENMC.EMPR_Codigo    
 AND ENRE.ENMC_Numero = ENMC.Numero
    
 WHERE DERF.EMPR_Codigo = @par_EMPR_Codigo    
 AND DERF.ENFA_Numero = @par_ENFA_Numero    
    
END 

-------------------------------BD Documental

Print 'CREATE TABLE DOCUMENTOS_FACTURA'
GO
DROP TABLE Documentos_Factura
GO

CREATE table Documentos_Factura
(
	EMPR_codigo smallint not null,
	ENFA_Numero numeric(18,0) not null,
	Codigo numeric(18,0)  Primary Key identity(1,1) not null,
	Fecha_Crea datetime null,
	Tipo_documento VARCHAR(50) null,
	Archivo VARCHAR(MAX) null,
	Descripcion VARCHAR(100) null,
);
GO

-------------------------------BD Documental

Print 'CREATE PROCEDURE gsp_insertar_Documento_Factura'
GO
DROP PROCEDURE gsp_insertar_Documento_Factura
GO

CREATE PROCEDURE gsp_insertar_Documento_Factura (
@par_EMPR_Codigo  smallint,
@par_ENFA_Numero  NUMERIC,
@par_Tipo_Docu	  VARCHAR(50) = '',
@par_Archivo	  VARCHAR(MAX),
@par_Descrip	  VARCHAR(100)
)
AS
BEGIN
	declare @fechaCrea datetime = null
	set @fechaCrea = GETDATE()
	
	IF EXISTS   
	(SELECT ENFA_Numero   
	FROM Documentos_Factura   
	WHERE EMPR_Codigo = @par_EMPR_Codigo   
	AND ENFA_Numero=@par_ENFA_Numero
	AND Tipo_documento=@par_Tipo_Docu) BEGIN  
		 DELETE Documentos_Factura WHERE  EMPR_Codigo = @par_EMPR_Codigo  AND ENFA_Numero=@par_ENFA_Numero AND Tipo_documento=@par_Tipo_Docu
	END  
	
	INSERT INTO Documentos_Factura
	(EMPR_Codigo, ENFA_Numero, Tipo_documento, Fecha_Crea, Archivo, Descripcion)
	VALUES
	(@par_EMPR_Codigo, @par_ENFA_Numero, @par_Tipo_Docu, @fechaCrea, @par_Archivo, @par_Descrip) 
	
	SELECT ENFA_Numero AS ENFA_Numero
	FROM Documentos_Factura
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND ENFA_Numero=@par_ENFA_Numero
	AND Tipo_documento=@par_Tipo_Docu
END
GO
-------------------------------BD Documental

Print 'CREATE PROCEDURE gsp_obtener_documento_factura'
GO
DROP PROCEDURE gsp_obtener_documento_factura
GO

CREATE PROCEDURE gsp_obtener_documento_factura (
@par_EMPR_Codigo  smallint,
@par_Numero		 NUMERIC
)
AS
BEGIN
	
	SELECT EMPR_codigo, 
	ENFA_Numero as Numero, 
	Archivo,
	2 AS ObtFacEle
	FROM Documentos_Factura 
	WHERE EMPR_codigo = @par_EMPR_Codigo 
	AND ENFA_Numero = @par_Numero
	
END
GO
