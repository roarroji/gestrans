﻿
GO
PRINT 'Opción Menú Facturacion-otros-conceptos'
GO
DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 900102		
GO
DELETE Menu_Aplicaciones WHERE Codigo = 900102
GO
INSERT INTO Menu_Aplicaciones( 
EMPR_Codigo,
Codigo,
MOAP_Codigo,
Nombre,
Padre_Menu,
Orden_Menu,
Mostrar_Menu,
Aplica_Habilitar,
Aplica_Consultar,
Aplica_Actualizar,
Aplica_Eliminar_Anular,
Aplica_Imprimir,
Aplica_Contabilidad,
Opcion_Permiso,
Opcion_Listado,
Aplica_Ayuda,
Url_Pagina,
Url_Ayuda) 
VALUES(
1
,900102
,90
,'Factura Otros Conceptos'
,9001
,20
,1
,1
,1
,1
,1
,1
,1
,0
,0
,0
,'#!ConsultarFacturaOtrosConceptos'
,'#!'
)
GO
INSERT INTO Permiso_Grupo_Usuarios(
EMPR_Codigo,
MEAP_Codigo,
GRUS_Codigo,
Habilitar,
Consultar,
Actualizar,
Eliminar_Anular,
Imprimir,
Permiso,
Contabilidad
) 
VALUES (
1,
900102,
1,
1,
1,
1,
1,
1,
0,
0
) 