﻿
Print 'CREATE PROCEDURE gsp_insertar_Factura_Electronica'
GO
DROP PROCEDURE gsp_insertar_Factura_Electronica
GO
  
CREATE PROCEDURE gsp_insertar_Factura_Electronica (  
@par_EMPR_Codigo  smallint,  
@par_ENFA_Numero  NUMERIC,  
@par_Nume_Docu   NUMERIC,  
@par_CUFE    VARCHAR(50) = '',  
@par_Fecha_Envio  DATETIME,  
@par_QRcode    VARBINARY(MAX)  
)  
AS  
BEGIN
	IF EXISTS   
	(SELECT ENFA_Numero   
	FROM Factura_Electronica   
	WHERE EMPR_Codigo = @par_EMPR_Codigo   
	AND ENFA_Numero=@par_ENFA_Numero) BEGIN  
		DELETE Factura_Electronica WHERE  EMPR_Codigo = @par_EMPR_Codigo  AND ENFA_Numero=@par_ENFA_Numero
	END  
	
	INSERT INTO Factura_Electronica  
	(EMPR_Codigo, ENFA_Numero, Numero_Documento, CUFE, Fecha_Envio, QR_code)  
	VALUES  
	(@par_EMPR_Codigo, @par_ENFA_Numero, @par_Nume_Docu, @par_CUFE, @par_Fecha_Envio, @par_QRcode)  
	
	SELECT ENFA_Numero AS ENFA_Numero  
	FROM Factura_Electronica  
	WHERE EMPR_Codigo = @par_EMPR_Codigo   
	AND ENFA_Numero=@par_ENFA_Numero  
	
END  