﻿
Print 'CREATE PROCEDURE gsp_obtener_encabezado_factura_electronica_saphety'
GO
DROP PROCEDURE gsp_obtener_encabezado_factura_electronica_saphety
GO

CREATE PROCEDURE gsp_obtener_encabezado_factura_electronica_saphety (
@par_EMPR_Codigo smallint,
@par_Numero		 NUMERIC
)
AS
BEGIN

	SELECT 
	ENFA.EMPR_Codigo,
	ENFA.Numero, 
	EMPR.Numero_Identificacion AS IDEmpresaEmisora,
	CASE TIDE.Codigo 
	WHEN 101 THEN 'IdentityCard' 
	WHEN 102 THEN 'NIT' 
	WHEN 103 THEN 'ForeignerIdentification' 
	ELSE TIDE.Nombre END  As TipoIDEmpresaEmisora,
	ENFA.Numero_Documento,
	ENFA.Valor_Otros_Conceptos AS Valor_Otros_Conceptos,
	ENFA.Valor_Remesas AS Valor_Remesas,
	ENFA.Subtotal AS TotalBruto,
	ENFA.Valor_Impuestos AS Valor_Impuestos,
	ENFA.Valor_Factura,
	CLIE.Numero_Identificacion,
	CLIE.Direccion AS DireccionCliente,
	CLIE.Codigo_Postal AS CodigoPostalCliente,
	CLIE.Emails As CorreoCliente,
	ENFA.Fecha AS FechaCreacion,  
	DATEADD(DAY, 1,ENFA.Fecha) AS FechaVence,
	CASE TINT.Codigo
	WHEN 501 THEN 'Natural' 
	WHEN 502 THEN 'Legal' 
	ELSE TINT.Nombre END  As NombreTipoNaturalezaCliente,
	CASE TIDT.Codigo 
	WHEN 101 THEN 'IdentityCard' 
	WHEN 102 THEN 'NIT' 
	WHEN 103 THEN 'ForeignerIdentification' 
	ELSE TIDT.Nombre END  As NombreTipoDocumentoCliente,   
	ISNULL(CLIE.Razon_Social,'') + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,'') As NombreCompletoCliente,
	CIUD.Nombre AS CiudadCliente,
	DEPA.Nombre AS DistritoCliente,
	ISNULL(ENFA.Factura_Electronica,0) As Estado_FAEL
	
	FROM Encabezado_Facturas ENFA
	
	INNER JOIN 
	Empresas AS EMPR ON
	ENFA.EMPR_Codigo = EMPR.Codigo
	
	INNER JOIN
	V_Tipo_Documento_Empresas AS TIDE ON
	EMPR.Codigo = TIDE.EMPR_Codigo
	AND EMPR.CATA_TIID_Codigo = TIDE.Codigo

	INNER JOIN 
	Terceros AS CLIE ON
	ENFA.EMPR_Codigo = CLIE.EMPR_Codigo
	AND ENFA.TERC_Cliente = CLIE.Codigo

	INNER JOIN
	V_Tipo_Naturaleza_Terceros AS TINT ON
	CLIE.EMPR_Codigo = TINT.EMPR_Codigo
	AND CLIE.CATA_TINT_Codigo = TINT.Codigo

	INNER JOIN
	V_Tipo_Documento_Terceros AS TIDT ON
	CLIE.EMPR_Codigo = TIDT.EMPR_Codigo
	AND CLIE.CATA_TIID_Codigo = TIDT.Codigo

	INNER JOIN
	Ciudades AS CIUD ON
	CLIE.EMPR_Codigo = CIUD.EMPR_Codigo
	AND CLIE.CIUD_Codigo = CIUD.Codigo

	INNER JOIN
	Departamentos AS DEPA ON
	CIUD.EMPR_Codigo = DEPA.EMPR_Codigo
	AND CIUD.DEPA_Codigo = DEPA.Codigo
	
	WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo
	AND ENFA.Numero = @par_Numero

END
GO