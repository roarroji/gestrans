﻿
Print 'CREATE PROCEDURE gsp_obtener_tipo_factura_electronica'
GO
DROP PROCEDURE gsp_obtener_tipo_factura_electronica
GO

CREATE PROCEDURE gsp_obtener_tipo_factura_electronica (
@par_EMPR_Codigo smallint,
@par_Numero		 NUMERIC
)
AS
BEGIN

--------- CONSULTAR FACTURA ELECTRONICA ------------

	DECLARE @varCUFE						  VARCHAR(80) = ''
	DECLARE @varID_Numero_Documento_Proveedor VARCHAR(50) = ''
	DECLARE @varQR_code						  numeric  = 0

	SELECT 
	@varCUFE = ISNULL(CUFE, ''), 
	@varID_Numero_Documento_Proveedor = ISNULL(ID_Numero_Documento_Proveedor, ''), 
	@varQR_code = IIF(QR_code = NULL, 0, 1)

	FROM  Factura_Electronica

	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND  ENFA_Numero = @par_Numero
	
------- Crear variable tabla para retornar datos de la factura

	DECLARE @tblFactura TABLE (
		EMPR_Codigo			  smallint,
		Numero				  numeric,
		IDEmpresaEmisora	  numeric,
		TipoIDEmpresaEmisora  varchar(100) DEFAULT '',
		Numero_Documento	  numeric,
		Valor_Otros_Conceptos MONEY,
		Valor_Remesas 		  MONEY,
		Subtotal			  MONEY,
		Valor_Impuestos		  MONEY,
		Valor_Factura		  MONEY,
		Numero_Identificacion numeric,
		DireccionCliente	  varchar(100) DEFAULT '',
		CodigoPostalCliente   varchar(100) DEFAULT '',
		CorreoCliente		  varchar(100) DEFAULT '',
		FechaCreacion		  varchar(100) DEFAULT '',
		FechaVence			  varchar(100) DEFAULT '',
		NombreTipoNaturaleza  varchar(100) DEFAULT '',
		NombreTipoDocumento   varchar(100) DEFAULT '',
		RazonSocialCliente	  varchar(100) DEFAULT '',
		CiudadCliente		  varchar(100) DEFAULT '',
		DistritoCliente		  varchar(100) DEFAULT '',
		Estado_FAEL			  numeric,
		ObtFacEle		      numeric,
		CUFE				  VARCHAR(80) DEFAULT '',
		ID_Num_Doc_Pro		  VARCHAR(80) DEFAULT '',
		QR_code				  numeric
	)
------- Inserta en la tabla datos de la factura
	INSERT INTO @tblFactura(
							EMPR_Codigo, 
							Numero, 
							IDEmpresaEmisora, 
							TipoIDEmpresaEmisora, 
							Numero_Documento, 
							Valor_Otros_Conceptos, 
							Valor_Remesas,
							Subtotal, 
							Valor_Impuestos,
							Valor_Factura, 
							Numero_Identificacion, 
							DireccionCliente, 
							CodigoPostalCliente, 
							CorreoCliente, 
							FechaCreacion, 
							FechaVence, 
							NombreTipoNaturaleza, 
							NombreTipoDocumento, 
							RazonSocialCliente, 
							CiudadCliente, 
							DistritoCliente,
							Estado_FAEL
							)
	
	EXEC gsp_obtener_encabezado_factura_electronica_saphety @par_EMPR_Codigo, @par_Numero
	UPDATE @tblFactura 
	SET ObtFacEle = 1,
	CUFE = @varCUFE,	 
	ID_Num_Doc_Pro	= @varID_Numero_Documento_Proveedor,
	QR_code	= @varQR_code
	SELECT * FROM @tblFactura
END
GO