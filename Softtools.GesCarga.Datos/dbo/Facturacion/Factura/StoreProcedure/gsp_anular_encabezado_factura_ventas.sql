﻿PRINT 'gsp_anular_encabezado_factura_ventas'
GO
DROP PROCEDURE gsp_anular_encabezado_factura_ventas
GO
CREATE PROCEDURE gsp_anular_encabezado_factura_ventas
(        
	@par_EMPR_Codigo       smallint,        
	@par_ENFA_Numero       NUMERIC,        
	@par_USUA_Codigo_Anula smallint,        
	@par_Causa_Anula    VARCHAR(150)        
)        
AS        
BEGIN        
	Declare @EstadoAnulado int = 1; -- Valor Anulacion        
	Declare @SinFacturaAsociada int = 0; --  quitar asociación factura - remesas        
	Declare @CATA_DOOR_Codigo_factura int = 2601; -- Cata Documento factura en Encabezado_Documento_Cuentas      
	Declare @AbonoCuentaCobro money = 0;        
	Declare @FechaAnulacion Date = Getdate();
	Declare @CATA_TIDO_Codigo_Remesa int = 110; -- TIDO_Codigo Remesa Paqueteria
	
	--Verifica si es una factura creada desde Remesas Paqueteria
	if exists(
		Select Numero, ENFA_Numero from Encabezado_Remesas where  ENFA_Numero = @par_ENFA_Numero AND TIDO_Codigo = @CATA_TIDO_Codigo_Remesa
	)
	BEGIN
		--Caso para Anulacion factura creada desde Paqueteria
		-- Anula Factura      
		UPDATE  Encabezado_Facturas  SET          
		Anulado = @EstadoAnulado,          
		USUA_Codigo_Anula = @par_USUA_Codigo_Anula,           
		Causa_Anula = @par_Causa_Anula,
		Fecha_Anula = GETDATE()       
		WHERE EMPR_Codigo = @par_EMPR_Codigo         
		AND Numero = @par_ENFA_Numero

		-- Anula Remesa
		Update Encabezado_Remesas SET
		Anulado = @EstadoAnulado,
		USUA_Codigo_Anula = @par_USUA_Codigo_Anula,           
		Causa_Anula = 'Anulación Factura',
		Fecha_Anula = GETDATE()
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND ENFA_Numero = @par_ENFA_Numero
		AND TIDO_Codigo = @CATA_TIDO_Codigo_Remesa

		SELECT Numero from Encabezado_Facturas        
		WHERE EMPR_Codigo = @par_EMPR_Codigo        
		AND Numero = @par_ENFA_Numero  

	END
	ELSE
	BEGIN
		-- Caso para Anulacion factura
		-- Verifica si la Cuenta por cobrar tiene pagos        
		SELECT @AbonoCuentaCobro = Abono FROM encabezado_documento_cuentas        
		WHERE EMPR_Codigo = @par_EMPR_Codigo          
		AND Codigo_Documento_Origen = @par_ENFA_Numero          
		AND CATA_DOOR_Codigo = @CATA_DOOR_Codigo_factura      
      
		IF ISNULL(@AbonoCuentaCobro, 0) = 0        
		BEGIN      
		   -- Anula Factura      
		  UPDATE  Encabezado_Facturas  SET          
		  Anulado = @EstadoAnulado,          
		  USUA_Codigo_Anula = @par_USUA_Codigo_Anula,           
		  Causa_Anula = @par_Causa_Anula,
		  Fecha_Anula = GETDATE()
		  WHERE EMPR_Codigo = @par_EMPR_Codigo         
		  AND Numero = @par_ENFA_Numero        
      
		   -- Quita Asociacion Factura - remesa        
		  UPDATE Encabezado_Remesas SET        
		  ENFA_Numero = @SinFacturaAsociada        
		  WHERE EMPR_Codigo = @par_EMPR_Codigo        
		  AND ENFA_Numero = @par_ENFA_Numero        
      
		  -- Anula Cuenta por cobrar de la factura      
		  UPDATE Encabezado_Documento_Cuentas SET        
		  Anulado = @EstadoAnulado        
		  WHERE EMPR_Codigo = @par_EMPR_Codigo        
		  AND Codigo_Documento_Origen = @par_ENFA_Numero        
		  AND CATA_DOOR_Codigo = @CATA_DOOR_Codigo_factura        
      
		  SELECT Numero from Encabezado_Facturas        
		  WHERE EMPR_Codigo = @par_EMPR_Codigo        
		  AND Numero = @par_ENFA_Numero        
  
		  EXEC gsp_anular_movimiento_contable @par_EMPR_Codigo,170,@par_ENFA_Numero  
  
		END      
		ELSE      
		BEGIN      
			SELECT 0 AS Numero        
		END      
	END
END
GO