﻿
Print 'CREATE PROCEDURE gsp_consultar_detalle_conceptos_facturas'
GO
DROP PROCEDURE gsp_consultar_detalle_conceptos_facturas
GO

CREATE PROCEDURE gsp_consultar_detalle_conceptos_facturas (  
@par_EMPR_Codigo smallint,  
@par_ENFA_Numero   numeric  
)  
AS  
BEGIN  
  
 SELECT
 DECF.EMPR_Codigo,  
 DECF.ENFA_Numero,
 DECF.Codigo,
 ISNULL(DECF.DEND_ID, 0) As DEND_ID, 
 ISNULL(DECF.COVE_Codigo, 0) As COVE_Codigo, 
 ISNULL(COVE.Nombre,'') As COVE_Nombre,
 ISNULL(DECF.Descripcion,'') As Descripcion,
 DECF.Valor_Concepto,
 DECF.Valor_Impuestos,
 DECF.ENRE_Numero, 
 ISNULL(ENRE.Numero_Documento, 0) As ENRE_Numero_Documento, 
 DECF.ENOS_Numero,
 DECF.EMOE_Numero
  
 FROM Detalle_Conceptos_Facturas DECF  

 LEFT JOIN  
 Encabezado_Remesas ENRE ON  
 DECF.EMPR_Codigo = ENRE.EMPR_Codigo 
 AND DECF.ENRE_Numero = ENRE.Numero  
  
 INNER JOIN  
 Conceptos_Ventas COVE ON  
 DECF.EMPR_Codigo = COVE.EMPR_Codigo  
 AND DECF.COVE_Codigo = COVE.Codigo 
  
 WHERE DECF.EMPR_Codigo = @par_EMPR_Codigo  
 AND DECF.ENFA_Numero = @par_ENFA_Numero  
  
END  
GO