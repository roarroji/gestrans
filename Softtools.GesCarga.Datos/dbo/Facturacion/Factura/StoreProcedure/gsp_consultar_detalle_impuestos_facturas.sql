﻿
Print 'CREATE PROCEDURE gsp_consultar_detalle_impuestos_facturas'
GO
DROP PROCEDURE gsp_consultar_detalle_impuestos_facturas
GO

CREATE PROCEDURE gsp_consultar_detalle_impuestos_facturas (  
@par_EMPR_Codigo smallint,  
@par_ENFA_Numero   numeric  
)  
AS  
BEGIN  
  
 SELECT
 DEIF.EMPR_Codigo,  
 DEIF.ENFA_Numero,
 DEIF.ENIM_Codigo,
 ENIM.Nombre as ENIM_Nombre,
 DEIF.CATA_TVFI_Codigo,
 DEIF.Valor_Tarifa,
 DEIF.Valor_Base,
 DEIF.Valor_Impuesto
 
 FROM Detalle_Impuestos_Facturas AS DEIF
 
 INNER JOIN Encabezado_Impuestos AS ENIM ON
 ENIM.EMPR_Codigo = DEIF.EMPR_Codigo
 AND ENIM.CODIGO = DEIF.ENIM_Codigo
 
 WHERE DEIF.EMPR_Codigo = @par_EMPR_Codigo  
 AND DEIF.ENFA_Numero = @par_ENFA_Numero  
  
END  
GO

