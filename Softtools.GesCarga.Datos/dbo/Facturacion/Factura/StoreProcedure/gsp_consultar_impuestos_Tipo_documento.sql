﻿
Print 'CREATE PROCEDURE gsp_consultar_impuestos_Tipo_documento'
GO
DROP PROCEDURE gsp_consultar_impuestos_Tipo_documento
GO

 CREATE PROCEDURE gsp_consultar_impuestos_Tipo_documento  
(      
  @par_EMPR_Codigo SMALLINT,      
  @par_Codigo NUMERIC = NULL,    
  @par_CodigoAlterno VARCHAR(20) = NULL,     
  @par_Nombre VARCHAR(100)= NULL,       
  @par_CATA_TRAI_Codigo NUMERIC= NULL,    
  @par_CATA_TIIM_Codigo NUMERIC= NULL,    
  @par_Estado SMALLINT = NULL,    
  @par_NumeroPagina INT = NULL,      
  @par_RegistrosPagina INT = NULL  ,    
  @par_TIDO_Codigo NUMERIC = NULL,    
  @par_CIUD_Codigo NUMERIC = NULL   
 )      
 AS      
 BEGIN      
   
      SELECT       
      ENIM.EMPR_Codigo,    
   ENIM.Codigo_Alterno,      
   ENIM.Codigo,     
   ENIM.Nombre,    
   ENIM.Label,     
   ENIM.CATA_TIIM_Codigo,     
   TIIM.Campo1 Tipo_Impuesto,    
   ENIM.CATA_TRAI_Codigo,     
   TRAI.Campo1 Tipo_Recaudo,    
   CASE WHEN ENIM.CATA_TRAI_Codigo = 803 THEN ISNULL((SELECT Valor_Tarifa FROM Detalle_Ciudad_Impuestos WHERE EMPR_Codigo =  ENIM.EMPR_Codigo AND  ENIM_Codigo = ENIM.Codigo AND CIUD_Codigo = @par_CIUD_Codigo ),ENIM.Valor_Tarifa)  ELSE ENIM.Valor_Tarifa END AS Valor_Tarifa,         
   CASE WHEN ENIM.CATA_TRAI_Codigo = 803 THEN ISNULL((SELECT Valor_Base FROM Detalle_Ciudad_Impuestos WHERE EMPR_Codigo =  ENIM.EMPR_Codigo AND  ENIM_Codigo = ENIM.Codigo AND CIUD_Codigo = @par_CIUD_Codigo ),ENIM.Valor_Base) ELSE ENIM.Valor_Base END AS Valor_Base,
   ENIM.Operacion,    
   ENIM.Estado,    
   USCR.Codigo USUA_Codigo_Crea ,    
   ENIM.Fecha_Crea ,    
   USMO.Codigo USUA_Codigo_Modifica ,    
   ENIM.Fecha_Modifica,    
   ENIM.PLUC_Codigo,    
   Obtener=0,  
        0 AS TotalRegistros,      
   0 AS PaginaObtener,      
   0 AS RegistrosPagina   
      FROM      
     Encabezado_Impuestos  ENIM    
    LEFT JOIN Impuestos_Tipo_Documentos AS ITDO  
  ON ENIM.EMPR_Codigo = ITDO.EMPR_Codigo  
  AND ENIM.Codigo = ITDO.ENIM_Codigo
    
  
      , usuarios USCR     
      , usuarios USMO     
   , Valor_Catalogos TIIM    
   , Valor_Catalogos TRAI    
      WHERE      
       ENIM.EMPR_Codigo = @par_EMPR_Codigo       
      AND ENIM.Codigo > 0      
   AND ENIM.EMPR_Codigo = USCR.EMPR_Codigo    
      AND ENIM.USUA_Codigo_Crea = USCR.Codigo    
   AND ENIM.EMPR_Codigo = USMO.EMPR_Codigo    
      AND ENIM.USUA_Codigo_Crea = USMO.Codigo    
   AND ENIM.EMPR_Codigo = TIIM.EMPR_Codigo    
   AND ENIM.CATA_TIIM_Codigo = TIIM.Codigo    
   AND ENIM.EMPR_Codigo = TRAI.EMPR_Codigo    
   AND ENIM.CATA_TRAI_Codigo = TRAI.Codigo    
   AND ITDO.TIDO_Codigo = @par_TIDO_Codigo
   AND ENIM.ESTADO  = ISNULL(@par_Estado, ENIM.ESTADO)
   AND ITDO.ESTADO  = ISNULL(@par_Estado, ITDO.ESTADO)
   END       
GO
