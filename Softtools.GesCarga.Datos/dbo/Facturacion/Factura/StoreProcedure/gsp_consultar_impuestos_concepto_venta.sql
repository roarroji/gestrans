﻿
Print 'CREATE PROCEDURE gsp_consultar_impuestos_concepto_venta'
GO
DROP PROCEDURE gsp_consultar_impuestos_concepto_venta
GO


CREATE PROCEDURE gsp_consultar_impuestos_concepto_venta (  
@par_EMPR_Codigo smallint,
@par_COVE_Codigo smallint
)  
AS  
BEGIN  

--Variable Estado Impuesto
Declare @par_estado_impuesto numeric
set @par_estado_impuesto = 1 

SELECT ENIM.EMPR_Codigo, ENIM.Codigo, ENIM.Operacion, ENIM.Valor_Tarifa, ENIM.Valor_Base
FROM Impuestos_Conceptos_Ventas  IMCV

INNER JOIN
Encabezado_Impuestos ENIM ON
IMCV.EMPR_Codigo = ENIM.EMPR_Codigo 
AND IMCV.ENIM_Codigo = ENIM.Codigo 

INNER JOIN Conceptos_Ventas COVE ON 
IMCV.EMPR_Codigo = COVE.EMPR_Codigo
AND IMCV.COVE_Codigo = COVE.Codigo

WHERE 
IMCV.EMPR_Codigo = @par_EMPR_Codigo
AND ENIM.Estado = @par_estado_impuesto
AND COVE.Codigo = @par_COVE_Codigo
  
END  
GO