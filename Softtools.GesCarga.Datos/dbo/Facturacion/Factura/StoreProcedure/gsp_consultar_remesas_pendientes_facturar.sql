﻿PRINT 'gsp_consultar_remesas_pendientes_facturar'
GO
DROP PROCEDURE gsp_consultar_remesas_pendientes_facturar
GO
CREATE PROCEDURE gsp_consultar_remesas_pendientes_facturar
(                                                  
	@par_EMPR_Codigo SMALLINT,          
	@par_Numero_Documento NUMERIC = NULL,          
	@par_Fecha_Inicial DATETIME = NULL,          
	@par_Fecha_Final DATETIME = NULL,          
	@par_CLIE_Codigo NUMERIC = NULL,          
	@par_Documento_Cliente VARCHAR(30) = NULL,  
	@par_ENPD_Numero_Documento NUMERIC = NULL,          
	@par_ESOS_Numero_Documento NUMERIC = NULL,  
	@par_TEDI_Codigo   INT =  NULL,          
	@par_NumeroPagina INT = NULL,          
	@par_RegistrosPagina INT = NULL          
)                                                  
AS                                                  
BEGIN  
	Declare @Remesa Numeric = 100;    
	Declare @RemesaPaqueteria Numeric = 110;   
	Declare @RemesaOtrasEmpresas Numeric = 118;   
  
	SET NOCOUNT ON;                                                   
	DECLARE @CantidadRegistros INT                                                  
	SELECT @CantidadRegistros =          
	(                                                  
	SELECT DISTINCT                                                  
	COUNT(1)                                                  
	FROM                                                  
	Encabezado_Remesas ENRE          
          
	LEFT JOIN Rutas RUTA          
	ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo          
	AND ENRE.RUTA_Codigo = RUTA.Codigo          
          
	LEFT JOIN Ciudades as CIOR        
	ON CIOR.EMPR_Codigo = RUTA.EMPR_Codigo        
	AND CIOR.Codigo = RUTA.CIUD_Codigo_Origen        
        
	LEFT JOIN Ciudades as CIDE        
	ON CIDE.EMPR_Codigo = RUTA.EMPR_Codigo        
	AND CIDE.Codigo = RUTA.CIUD_Codigo_Destino        
        
	LEFT JOIN Producto_Transportados PRTR          
	ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo          
	AND ENRE.PRTR_Codigo = PRTR.Codigo          
          
	LEFT JOIN Terceros CLIE          
	ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo          
	AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo          
          
	LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS          
	ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo          
	AND ENRE.ESOS_Numero = ESOS.Numero          
          
	LEFT JOIN Encabezado_Facturas ENFA          
	ON ENRE.EMPR_Codigo = ENFA.EMPR_Codigo          
	AND ENRE.ENFA_Numero = ENFA.Numero          
                                                    
	LEFT JOIN Encabezado_Planilla_Despachos ENPD          
	ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo          
	AND ENRE.ENPD_Numero = ENPD.Numero          
          
	LEFT JOIN Encabezado_Manifiesto_Carga ENMC          
	ON ENRE.EMPR_Codigo = ENMC.EMPR_Codigo          
	AND ENRE.ENMC_Numero = ENMC.Numero          
          
	LEFT JOIN Encabezado_Cumplido_Planilla_Despachos ECPD          
	ON ENRE.EMPR_Codigo = ECPD.EMPR_Codigo          
	AND ENRE.ECPD_Numero = ECPD.Numero          
          
	LEFT JOIN Detalle_Despacho_Orden_Servicios AS DDOS          
	ON ENRE.EMPR_Codigo = DDOS.EMPR_Codigo          
	AND ENRE.Numero = DDOS.ENRE_Numero          
          
	LEFT JOIN Tercero_Direcciones As TEDI          
	ON ENRE.EMPR_Codigo = TEDI.EMPR_Codigo          
	AND ENRE.TEDI_Codigo = TEDI.Codigo          
        
            
	LEFT JOIN Vehiculos AS VEHI          
	ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo          
	AND ENRE.VEHI_Codigo = VEHI.Codigo  
           
	WHERE                   
	ENRE.EMPR_Codigo = @par_EMPR_Codigo          
	AND ENRE.TIDO_Codigo IN (100, 118)  
	AND ENRE.Anulado = 0          
	AND ENRE.Numero_Documento = ISNULL(@par_Numero_Documento, ENRE.Numero_Documento)          
	AND ENRE.Fecha BETWEEN ISNULL(@par_Fecha_Inicial, ENRE.Fecha) AND ISNULL(@par_Fecha_Final, ENRE.Fecha)          
	AND (ESOS.Numero_Documento = @par_ESOS_Numero_Documento OR @par_ESOS_Numero_Documento IS NULL)          
	AND (ENPD.Numero_Documento = @par_ENPD_Numero_Documento OR @par_ENPD_Numero_Documento IS NULL)          
	AND ((ISNULL(ENRE.Cumplido, 0) = 1) OR (ESOS.Facturar_Despachar = 1 ))          
	AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_CLIE_Codigo, ENRE.TERC_Codigo_Cliente)          
	AND (ENRE.Documento_Cliente  LIKE'%'+@par_Documento_Cliente+'%' OR @par_Documento_Cliente IS NULL)          
	AND (ENRE.ENFA_Numero = 0 OR ENRE.ENFA_Numero IS NULL)  
	AND  CLIE.Numero_Identificacion = ISNULL(@par_Documento_Cliente,CLIE.Numero_Identificacion)          
	AND (ENRE.TEDI_Codigo = @par_TEDI_Codigo OR @par_TEDI_Codigo IS NULL)     
	AND (ENRE.Remesa_Cortesia <> 1 or ENRE.Remesa_Cortesia IS NULL)  
	);                 
	WITH Pagina                                                  
	AS                                                  
	(          
	SELECT          
	ENRE.EMPR_Codigo,          
	ENRE.Numero,          
	ENRE.Numero_Documento,  
	ENRE.TIDO_Codigo,  
	ENRE.TERC_Codigo_Cliente,          
	ISNULL(ENRE.Documento_Cliente,'') AS Documento_Cliente,
	ISNULL(CLIE.Razon_Social,'') + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,'') As NombreCliente,      
	ENRE.Fecha,          
	ENRE.RUTA_Codigo,          
	RUTA.Nombre AS NombreRuta,          
	ENRE.PRTR_Codigo,          
	PRTR.Nombre AS NombreProducto,          
	ENRE.CATA_FOPR_Codigo,          
	ENRE.TERC_Codigo_Remitente,          
	ENRE.Observaciones,          
	ENRE.Cantidad_Cliente,          
	ENRE.Peso_Cliente,          
	ENRE.Peso_Volumetrico_Cliente,          
	ENRE.Valor_Flete_Cliente,          
	ENRE.Valor_Manejo_Cliente,          
	ENRE.Valor_Seguro_Cliente,          
	ENRE.Valor_Descuento_Cliente,          
	ENRE.Total_Flete_Cliente,          
	ENRE.Valor_Comercial_Cliente,          
	ENRE.Cantidad_Transportador,          
	ENRE.Peso_Transportador,          
	ENRE.Valor_Flete_Transportador,          
	ENRE.Total_Flete_Transportador,          
	ENRE.Anulado,          
	ENRE.Estado,          
	ENRE.Fecha_Crea,          
	ENRE.USUA_Codigo_Crea,          
	ENRE.Fecha_Modifica,          
	ENRE.USUA_Codigo_Modifica,          
	ENRE.Fecha_Anula,          
	ENRE.USUA_Codigo_Anula,          
	ENRE.Causa_Anula,          
	ENRE.ETCC_Numero,          
	ENRE.ETCV_Numero,          
	ENRE.ESOS_Numero,          
	ESOS.Numero_Documento AS OrdenServicio,          
	ISNULL(ESOS.Facturar_Despachar, 0) AS Facturar_Despachar,          
	ENRE.ENPD_Numero AS NumeroPlanilla,          
	ISNULL(ENPD.Numero_Documento,0) AS NumeroDocumentoPlanilla,          
	ENRE.ENMC_Numero AS NumeroManifiesto,          
	ISNULL(ENMC.Numero_Documento,0) AS NumeroDocumentoManifiesto,          
	ISNULL(ENRE.ECPD_Numero,0) AS ECPD_Numero,          
	ISNULL(ECPD.Numero_Documento,0) AS NumeroCumplido,          
	ENRE.Cumplido AS Cumplido,           
	ISNULL(ENRE.ENFA_Numero,0) AS ENFA_Numero,          
	ISNULL(ENFA.Numero_Documento,0) AS NumeroFactura,          
	ENRE.VEHI_Codigo,          
	ISNULL(PRTR.UEPT_Codigo, 0) AS Unidad_Empaque,          
	ISNULL(ENRE.Distribucion,0) AS Distribucion,          
	ISNULL(TEDI.Nombre, '') As NombreSede,              
	VEHI.Placa,        
	CIOR.Nombre as CiudadRemitente,            
	CIDE.Nombre  as CiudadDestinatario,  
	CASE ENRE.TIDO_Codigo  
	When @Remesa Then TIDE.Campo1  
	When @RemesaPaqueteria Then 'Paquetería'  
	When @RemesaOtrasEmpresas Then 'Manifiesto Otras Empresas'  
	END AS TipoDespacho,  
	--TIDE.Campo1 AS TipoDespacho,        
	ESOS.CATA_TDOS_Codigo,        
	ROW_NUMBER() OVER (ORDER BY ENRE.Numero DESC) AS RowNumber                                                  
	FROM                                                  
	Encabezado_Remesas ENRE          
          
	LEFT JOIN Rutas RUTA          
	ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo          
	AND ENRE.RUTA_Codigo = RUTA.Codigo          
  
	LEFT JOIN Producto_Transportados PRTR          
	ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo          
	AND ENRE.PRTR_Codigo = PRTR.Codigo          
          
	LEFT JOIN Terceros CLIE          
	ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo          
	AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo          
          
	LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS          
	ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo          
	AND ENRE.ESOS_Numero = ESOS.Numero          
          
	LEFT JOIN Encabezado_Facturas ENFA          
	ON ENRE.EMPR_Codigo = ENFA.EMPR_Codigo          
	AND ENRE.ENFA_Numero = ENFA.Numero          
                                                    
	LEFT JOIN Encabezado_Planilla_Despachos ENPD          
	ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo          
	AND ENRE.ENPD_Numero = ENPD.Numero          
          
	LEFT JOIN Encabezado_Manifiesto_Carga ENMC          
	ON ENRE.EMPR_Codigo = ENMC.EMPR_Codigo          
	AND ENRE.ENMC_Numero = ENMC.Numero          
          
	LEFT JOIN Encabezado_Cumplido_Planilla_Despachos ECPD          
	ON ENRE.EMPR_Codigo = ECPD.EMPR_Codigo       
	AND ENRE.ECPD_Numero = ECPD.Numero          
          
	LEFT JOIN Detalle_Despacho_Orden_Servicios AS DDOS          
	ON ENRE.EMPR_Codigo = DDOS.EMPR_Codigo          
	AND ENRE.Numero = DDOS.ENRE_Numero          
          
	LEFT JOIN Tercero_Direcciones As TEDI          
	ON ENRE.EMPR_Codigo = TEDI.EMPR_Codigo          
	AND ENRE.TEDI_Codigo = TEDI.Codigo          
        
	LEFT JOIN Vehiculos AS VEHI          
	ON ENRE.EMPR_Codigo = VEHI.EMPR_Codigo          
	AND ENRE.VEHI_Codigo = VEHI.Codigo          
        
	LEFT JOIN Ciudades as CIOR        
	ON CIOR.EMPR_Codigo = RUTA.EMPR_Codigo        
	AND CIOR.Codigo = RUTA.CIUD_Codigo_Origen        
        
	LEFT JOIN Ciudades as CIDE        
	ON CIDE.EMPR_Codigo = RUTA.EMPR_Codigo        
	AND CIDE.Codigo = RUTA.CIUD_Codigo_Destino        
        
	LEFT JOIN Valor_Catalogos AS TIDE        
	ON ESOS.EMPR_Codigo = TIDE.EMPR_Codigo        
	AND ESOS.CATA_TDOS_Codigo = TIDE.Codigo        
        
	WHERE                   
	ENRE.EMPR_Codigo = @par_EMPR_Codigo          
	AND ENRE.TIDO_Codigo IN (100, 118)  
	AND ENRE.Anulado = 0          
	AND ENRE.Numero_Documento = ISNULL(@par_Numero_Documento, ENRE.Numero_Documento)          
	AND ENRE.Fecha BETWEEN ISNULL(@par_Fecha_Inicial, ENRE.Fecha) AND ISNULL(@par_Fecha_Final, ENRE.Fecha)          
	AND (ESOS.Numero_Documento = @par_ESOS_Numero_Documento OR @par_ESOS_Numero_Documento IS NULL)          
	AND (ENPD.Numero_Documento = @par_ENPD_Numero_Documento OR @par_ENPD_Numero_Documento IS NULL)          
	AND ((ISNULL(ENRE.Cumplido, 0) = 1) OR (ESOS.Facturar_Despachar = 1 ))       
	AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_CLIE_Codigo, ENRE.TERC_Codigo_Cliente)          
	AND (ENRE.Documento_Cliente  LIKE'%'+@par_Documento_Cliente+'%' OR @par_Documento_Cliente IS NULL)          
	AND (ENRE.ENFA_Numero = 0 OR ENRE.ENFA_Numero IS NULL)  
	AND  CLIE.Numero_Identificacion = ISNULL(@par_Documento_Cliente,CLIE.Numero_Identificacion)          
	AND (ENRE.TEDI_Codigo = @par_TEDI_Codigo OR @par_TEDI_Codigo IS NULL)     
	AND (ENRE.Remesa_Cortesia <> 1 or ENRE.Remesa_Cortesia IS NULL)  
	)                                                  
	SELECT DISTINCT                                                  
	0 AS Obtener,          
	EMPR_Codigo,          
	Numero,  
	Numero_Documento,          
	TIDO_Codigo,  
	Documento_Cliente,
	NombreCliente,          
	Fecha,          
	RUTA_Codigo,          
	NombreRuta,          
	PRTR_Codigo,          
	NombreProducto,          
	CATA_FOPR_Codigo,          
	TERC_Codigo_Cliente,          
	TERC_Codigo_Remitente,          
	Observaciones,          
	Cantidad_Cliente,          
	Peso_Cliente,          
	Peso_Volumetrico_Cliente,          
	Valor_Flete_Cliente,          
	Valor_Manejo_Cliente,          
	Valor_Seguro_Cliente,          
	Valor_Descuento_Cliente,          
	Total_Flete_Cliente,          
	Valor_Comercial_Cliente,          
	Cantidad_Transportador,          
	Peso_Transportador,          
	Valor_Flete_Transportador,          
	Total_Flete_Transportador,          
	Anulado,          
	Estado,          
	Fecha_Crea,          
	USUA_Codigo_Crea,          
	Fecha_Modifica,          
	USUA_Codigo_Modifica,          
	Fecha_Anula,          
	USUA_Codigo_Anula,          
	Causa_Anula,          
	ETCC_Numero,          
	ETCV_Numero,          
	ESOS_Numero,          
	OrdenServicio,          
	Facturar_Despachar,          
	NumeroPlanilla,          
	NumeroDocumentoPlanilla,          
	NumeroManifiesto,          
	NumeroDocumentoManifiesto,          
	ECPD_Numero,          
	NumeroCumplido,          
	Cumplido,           ENFA_Numero,          
	NumeroFactura,          
	VEHI_Codigo,          
	Unidad_Empaque,          
	Distribucion,          
	NombreSede,          
	Placa,        
	CiudadRemitente,        
	CiudadDestinatario,        
	TipoDespacho,        
	CATA_TDOS_Codigo,        
	@CantidadRegistros AS TotalRegistros,          
	@par_NumeroPagina AS PaginaObtener,          
	@par_RegistrosPagina AS RegistrosPagina          
	FROM Pagina WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
	ORDER BY Numero          
END
GO