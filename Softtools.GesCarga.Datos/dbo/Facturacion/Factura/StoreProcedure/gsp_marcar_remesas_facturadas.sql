﻿


PRINT 'gsp_marcar_remesas_facturadas'
GO
DROP PROCEDURE gsp_marcar_remesas_facturadas
GO
CREATE PROCEDURE gsp_marcar_remesas_facturadas 
(        
@par_EMPR_Codigo SMALLINT,        
@par_Numero_Remesas VARCHAR(1000)  
)        
AS            
 BEGIN          
  UPDATE Encabezado_Remesas SET ENFA_Numero = -1      
  
  WHERE EMPR_Codigo = @par_EMPR_Codigo        
  AND Numero IN (SELECT * FROM dbo.Func_Dividir_String(@par_Numero_Remesas,',')) 
      
  SELECT @@ROWCOUNT AS RemesasMarcadas  
          
END        
GO