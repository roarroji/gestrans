﻿PRINT 'V_Detalle_Documento_Cuenta_Comprobantes_Facturas'
GO
DROP VIEW V_Detalle_Documento_Cuenta_Comprobantes_Facturas
GO
CREATE VIEW V_Detalle_Documento_Cuenta_Comprobantes_Facturas 
AS
SELECT 
EDCO.EMPR_Codigo, 
EDCO.Fecha, 
DDCO.EDCO_Codigo EDCO_Codigo, 
DDCO.ENDC_Codigo ENDC_Codigo, 
DDCO.Valor_Pago_Recaudo
FROM Encabezado_Documento_Comprobantes EDCO

LEFT JOIN Detalle_Documento_Cuenta_Comprobantes DDCO ON
EDCO.EMPR_Codigo = DDCO.EMPR_Codigo
AND EDCO.Codigo = DDCO.EDCO_Codigo

WHERE EDCO.CATA_DOOR_Codigo = 2601 --Documento Origen Factura
and EDCO.TIDO_Codigo = 40 --Comprobante Ingreso
AND EDCO.Estado = 1
AND EDCO.Anulado = 0
GO