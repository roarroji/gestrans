﻿Print 'gsp_Reporte_Encabezado_Nota_Credito'
GO
DROP PROCEDURE gsp_Reporte_Encabezado_Nota_Credito    
GO
CREATE PROCEDURE gsp_Reporte_Encabezado_Nota_Credito  
(                   
	@par_Empr_Codigo NUMERIC,                     
	@par_Numero_Nota  NUMERIC                       
)                  
AS   
BEGIN          
          
	SELECT           
	ENNF.EMPR_Codigo,        
	ENNF.TIDO_Codigo,        
	ENNF.Numero_Documento,          
	ENNF.Fecha,          
	ENNF.Observaciones,          
	ENNF.Valor_Nota,        
	ENNF.CUDE,            
	ENFA.Numero_Documento AS ENFA_Numero_Documento,          
	ENFA.Valor_Factura AS ENFA_Valor_Factura,          
	dbo.funcCantidadConLetra(Valor_Nota) AS valorLetra,  
	CONF.Nombre AS CONF_Nombre,        
          
	ISNULL(CLIE.Razon_Social,'') + ISNULL(CLIE.Nombre,'') +' '+ ISNULL(CLIE.Apellido1,'')+' '+ISNULL(CLIE.Apellido2,'') AS NombreCliente,          
	CLIE.Numero_Identificacion AS CLIE_Numero_Identificacion,          
	ISNULL(CIUD.Nombre,'') AS CLIE_Ciudad,          
	ISNULL(CLIE.Direccion, '') AS CLIE_Direccion,          
	ISNULL(CLIE.Telefonos, '') AS CLIE_Telefonos,          
          
	EMPR.Nombre_Razon_Social,  
	CONCAT(EMPR.Numero_Identificacion,'-',EMPR.Digito_Chequeo) IdentificacionEmpresa,  
	EMPR.Direccion,  
	EMPR.Telefonos,  
	EMPR.Actividad_Economica,  
	EMFE.Firma_Digital,  
	iif(ENNF.TIDO_Codigo = 190, EMFE.Prefijo_Nota_Credito, EMFE.Prefijo_Nota_Debito) AS Prefijo,  
	CIOF.Nombre CiudadOficina,  
	OFRE.Resolucion_Facturacion,  
  
	USUA.Nombre AS USUA_Nombre,      
	ENNF.Estado,      
	ENNF.Anulado        
          
	FROM Encabezado_Notas_Facturas AS ENNF          
          
	INNER JOIN Encabezado_Facturas AS ENFA ON          
	ENNF.EMPR_Codigo = ENFA.EMPR_Codigo          
	AND ENNF.ENFA_Numero = ENFA.Numero   
   
	LEFT JOIN Empresa_Factura_Electronica AS EMFE ON                      
	ENFA.EMPR_Codigo = EMFE.EMPR_Codigo         
          
	LEFT JOIN Terceros AS CLIE ON          
	ENNF.EMPR_Codigo = CLIE.EMPR_Codigo          
	AND ENNF.TERC_Cliente = CLIE.Codigo          
          
	LEFT JOIN Ciudades AS CIUD ON          
	CLIE.EMPR_Codigo = CIUD.EMPR_Codigo          
	AND CLIE.CIUD_Codigo = CIUD.Codigo          
          
	LEFT JOIN Conceptos_Notas_Facturas AS CONF ON          
	ENNF.EMPR_Codigo = CONF.EMPR_Codigo          
	AND ENNF.CONF_Codigo = CONF.Codigo          
          
	INNER JOIN Empresas AS EMPR ON          
	ENNF.EMPR_Codigo = EMPR.Codigo          
  
	LEFT JOIN Oficinas OFRE on  
	ENFA.EMPR_Codigo = OFRE.EMPR_Codigo  
	AND ENFA.OFIC_Factura = OFRE.Codigo  
  
	LEFT JOIN  Oficinas OFIC on  
	ENNF.EMPR_Codigo = OFIC.EMPR_Codigo  
	AND ENNF.OFIC_Nota = OFIC.Codigo  
                    
	LEFT JOIN  Ciudades CIOF on                     
	OFIC.EMPR_Codigo = CIOF.EMPR_Codigo                        
	AND OFIC.CIUD_Codigo = CIOF.Codigo          
          
	INNER JOIN Usuarios AS USUA ON          
	ENNF.EMPR_Codigo = USUA.EMPR_Codigo          
	AND ENNF.USUA_Codigo_Crea = USUA.Codigo          
          
	WHERE ENNF.EMPR_Codigo = @par_Empr_Codigo          
	AND ENNF.Numero = @par_Numero_Nota          
          
END
GO