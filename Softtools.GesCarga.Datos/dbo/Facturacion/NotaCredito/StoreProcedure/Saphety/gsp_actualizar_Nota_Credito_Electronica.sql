﻿
Print 'CREATE PROCEDURE gsp_actualizar_Nota_Credito_Electronica'
GO
DROP PROCEDURE gsp_actualizar_Nota_Credito_Electronica
GO

CREATE PROCEDURE gsp_actualizar_Nota_Credito_Electronica (
@par_EMPR_Codigo	   smallint,
@par_ENNF_Numero	   NUMERIC,
@par_ID_NUM_DOC_PRO	   VARCHAR(MAX) = ''
)
AS
BEGIN
	UPDATE Encabezado_Notas_Facturas SET 
	ID_Numero_Documento_Proveedor_Electronico = @par_ID_NUM_DOC_PRO,
	Nota_Electronica = 1,
	Fecha_Envio_Electronico = GETDATE()

	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND Numero = @par_ENNF_Numero
	
	SELECT Numero, Numero_Documento 
	FROM Encabezado_Notas_Facturas
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND Numero = @par_ENNF_Numero
END
GO