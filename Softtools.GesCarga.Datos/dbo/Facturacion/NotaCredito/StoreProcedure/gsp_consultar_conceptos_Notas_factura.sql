﻿
Print 'CREATE PROCEDURE gsp_consultar_conceptos_Notas_factura'
GO
DROP PROCEDURE gsp_consultar_conceptos_Notas_factura
GO

CREATE PROCEDURE gsp_consultar_conceptos_Notas_factura(    
	@par_EMPR_Codigo smallint,
	@par_Codigo numeric = NULL,
	@par_ConceptoSistema numeric = NULL,
	@par_Nombre VARCHAR = NULL,
	@par_Estado smallint = NULL,
	@par_NumeroPagina INT = NULL,      
	@par_RegistrosPagina INT = NULL      
)          
AS          
BEGIN          
	SET NOCOUNT ON;          
	DECLARE          
	@CantidadRegistros INT          
	SELECT @CantidadRegistros = (          
		SELECT DISTINCT           
		COUNT(1)           
		FROM Conceptos_Notas_Facturas CONF
		WHERE CONF.EMPR_Codigo = @par_EMPR_Codigo
		AND CONF.Codigo = ISNULL(@par_Codigo,CONF.Codigo)
		AND CONF.Nombre LIKE '%'+ ISNULL(@par_Nombre,CONF.Nombre)+'%'
		AND CONF.Concepto_Sistema = ISNULL(@par_ConceptoSistema,CONF.Concepto_Sistema)
		AND CONF.Estado = ISNULL (@par_Estado,CONF.Estado)
	);                 
	WITH Pagina AS          
	(          
	SELECT
	CONF.EMPR_Codigo,
	CONF.Codigo,
	-- CONF.Concepto_Sistema,
	CONF.Nombre,
	CONF.Estado,
	0 as Obtener,
	ROW_NUMBER() OVER ( ORDER BY  CONF.Codigo) AS RowNumber          
	FROM Conceptos_Notas_Facturas CONF
	WHERE CONF.EMPR_Codigo = @par_EMPR_Codigo
	AND CONF.Codigo = ISNULL(@par_Codigo,CONF.Codigo)
	AND CONF.Nombre LIKE '%'+ ISNULL(@par_Nombre,CONF.Nombre)+'%'
	AND CONF.Concepto_Sistema = ISNULL(@par_ConceptoSistema,CONF.Concepto_Sistema)
	AND CONF.Estado = ISNULL (@par_Estado,CONF.Estado)     
	)         
	SELECT DISTINCT
	EMPR_Codigo,
	Codigo,
	Concepto_Sistema,
	Nombre,
	Estado,
	Obtener,
	
	@CantidadRegistros AS TotalRegistros,          
	@par_NumeroPagina AS PaginaObtener,          
	@par_RegistrosPagina AS RegistrosPagina          
	FROM          
	Pagina          
	WHERE          
	RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
END 
