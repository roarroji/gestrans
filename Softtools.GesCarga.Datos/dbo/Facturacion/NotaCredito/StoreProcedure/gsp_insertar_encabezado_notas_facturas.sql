﻿
Print 'CREATE PROCEDURE gsp_insertar_encabezado_notas_facturas'
GO
DROP PROCEDURE gsp_insertar_encabezado_notas_facturas
GO

CREATE PROCEDURE gsp_insertar_encabezado_notas_facturas (
@par_EMPR_Codigo 	   smallint,
@par_TIDO_Codigo 	   numeric,
@par_ENFA_Numero 	   numeric,
@par_Fecha 			   date,
@par_OFIC_Nota 		   numeric,
@par_TERC_Cliente	   numeric,
@par_TERC_Facturar 	   numeric,
@par_CONF_Codigo 	   numeric,
@par_Observaciones 	   varchar(500),
@par_Valor_Nota 	   money,
@par_Estado 		   smallint,
@par_USUA_Codigo_Crea  smallint,
@par_Anulado 		   smallint
)
AS
BEGIN

DECLARE @numNumeroNota NUMERIC = 0  
EXEC gsp_generar_consecutivo @par_EMPR_Codigo, @par_TIDO_Codigo, @par_OFIC_Nota, @numNumeroNota OUTPUT

INSERT INTO Encabezado_Notas_Facturas
(
EMPR_Codigo,
ENFA_Numero,
TIDO_Codigo,
Numero_Documento,
Numero_Preimpreso,
Fecha,
OFIC_Nota,
TERC_Cliente,
TERC_Facturar,
CONF_Codigo,
Observaciones,
Valor_Nota,
Nota_Electronica,
Estado,
Fecha_Crea,
USUA_Codigo_Crea,
Anulado
)
values
(
@par_EMPR_Codigo,
@par_ENFA_Numero,
@par_TIDO_Codigo,
@numNumeroNota,
@numNumeroNota,
@par_Fecha,
@par_OFIC_Nota,
@par_TERC_Cliente,
@par_TERC_Facturar,
@par_CONF_Codigo,
@par_Observaciones,
@par_Valor_Nota,
0,
@par_Estado,
GETDATE(),
@par_USUA_Codigo_Crea,
@par_Anulado
)

SELECT Numero, Numero_Documento FROM Encabezado_Notas_Facturas
WHERE EMPR_Codigo = @par_EMPR_Codigo
AND Numero_Documento = @numNumeroNota
AND TIDO_Codigo = @par_TIDO_Codigo

END
GO