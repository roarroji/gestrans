﻿
Print 'CREATE PROCEDURE gsp_modificar_encabezado_notas_facturas'
GO
DROP PROCEDURE gsp_modificar_encabezado_notas_facturas
GO

CREATE PROCEDURE gsp_modificar_encabezado_notas_facturas (
@par_EMPR_Codigo		  smallint,
@par_ENNF_Numero		  smallint,
@par_ENFA_Numero		  numeric,
@par_Fecha				  date,
@par_OFIC_Nota			  numeric,
@par_TERC_Cliente		  numeric,
@par_TERC_Facturar		  numeric,
@par_CONF_Codigo		  numeric,
@par_Observaciones		  varchar(500),
@par_Valor_Nota			  money,
@par_Estado				  smallint,
@par_USUA_Codigo_Modifica smallint
)
AS
BEGIN

UPDATE Encabezado_Notas_Facturas SET

Fecha = @par_Fecha,
OFIC_Nota = @par_OFIC_Nota,
TERC_Cliente = @par_TERC_Cliente,
TERC_Facturar = @par_TERC_Facturar,
CONF_Codigo = ISNULL(@par_CONF_Codigo,0),
Observaciones  = ISNULL(@par_Observaciones,''),
Valor_Nota = ISNULL(@par_Valor_Nota,0),
Estado = ISNULL(@par_Estado,0),
USUA_Codigo_Modifica = ISNULL(@par_USUA_Codigo_Modifica, 0)

WHERE EMPR_Codigo = @par_EMPR_Codigo
AND Numero = @par_ENNF_Numero

--- Retorna la modificación    
SELECT Numero, Numero_Documento FROM Encabezado_Notas_Facturas    
WHERE EMPR_Codigo = @par_EMPR_Codigo    
AND Numero = @par_ENNF_Numero  

END  
GO