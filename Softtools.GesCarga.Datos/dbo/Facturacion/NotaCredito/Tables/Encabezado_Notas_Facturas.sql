﻿
CREATE TABLE [dbo].[Encabezado_Notas_Facturas](
	[EMPR_Codigo] [smallint] NOT NULL,
	[Numero] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ENFA_Numero] [numeric](18, 0) NOT NULL,
	[TIDO_Codigo] [numeric](18, 0) NOT NULL,
	[Numero_Documento] [numeric](18, 0) NOT NULL,
	[Numero_Preimpreso] [varchar](20) NULL,
	[Fecha] [date] NOT NULL,
	[OFIC_Nota] [numeric](18, 0) NOT NULL,
	[TERC_Cliente] [numeric](18, 0) NOT NULL,
	[TERC_Facturar] [numeric](18, 0) NOT NULL,
	[CONF_Codigo] [numeric](18, 0) NOT NULL,
	[Observaciones] [varchar](500) NOT NULL,
	[Valor_Nota] [money] NOT NULL,
	[Nota_Electronica] [smallint] NOT NULL,
	[Fecha_Envio_Electronico] [datetime] NULL,
	[ID_Numero_Documento_Proveedor_Electronico] [varchar](50) NULL,
	[Estado] [smallint] NOT NULL,
	[Fecha_Crea] [datetime] NOT NULL,
	[USUA_Codigo_Crea] [smallint] NOT NULL,
	[Fecha_Modifica] [datetime] NULL,
	[USUA_Codigo_Modifica] [smallint] NULL,
	[Anulado] [smallint] NOT NULL,
	[Fecha_Anula] [datetime] NULL,
	[USUA_Codigo_Anula] [smallint] NULL,
	[Causa_Anula] [varchar](150) NULL,
 CONSTRAINT [PK_Encabezado_Notas_Facturas] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[Numero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Encabezado_Notas_Facturas]  WITH CHECK ADD  CONSTRAINT [FK_Encabezado_Notas_Facturas_Conceptos_Notas_Facturas] FOREIGN KEY([EMPR_Codigo], [CONF_Codigo])
REFERENCES [dbo].[Conceptos_Notas_Facturas] ([EMPR_Codigo], [Codigo])
GO

ALTER TABLE [dbo].[Encabezado_Notas_Facturas] CHECK CONSTRAINT [FK_Encabezado_Notas_Facturas_Conceptos_Notas_Facturas]
GO

