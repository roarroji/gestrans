﻿CREATE PROCEDURE gsp_consultar_encabezado_Nota_Facturas  
(                  
 @par_EMPR_Codigo  smallint,      
 @par_TIDO_Codigo  Numeric = NULL,      
 @par_Numero    numeric = NULL,      
 @par_Numero_Documento numeric = NULL,          
 @par_Fecha_Inicial  date = NULL,                  
 @par_Fecha_Final  date = NULL,                  
 @par_TERC_Cliente  numeric = NULL,            
 @par_TERC_Facturar  numeric = NULL,                 
 @par_Estado    smallint = NULL,                  
 @par_Anulado   smallint = NULL,                  
 @par_OFIC_Nota  smallint = NULL,                  
 @par_NumeroPagina INT = NULL,                  
 @par_RegistrosPagina INT = NULL,      
 @par_Usuario_Consulta INT  = NULL                                       
)                  
AS                  
BEGIN      
  DECLARE @CantidadRegistros NUMERIC            
  SELECT @CantidadRegistros = (                  
  SELECT DISTINCT COUNT(1)                   
  FROM Encabezado_Notas_Facturas ENNF               
          
  INNER JOIN                  
  Oficinas OFIC ON                  
  ENNF.EMPR_Codigo = OFIC.EMPR_Codigo                  
  AND ENNF.OFIC_Nota = OFIC.Codigo          
          
  INNER JOIN                  
  Terceros CLIE ON                  
  ENNF.EMPR_Codigo = CLIE.EMPR_Codigo                  
  AND ENNF.TERC_Cliente = CLIE.Codigo      
          
  INNER JOIN                  
  Terceros TEFA ON                  
  ENNF.EMPR_Codigo = TEFA.EMPR_Codigo                  
  AND ENNF.TERC_Facturar = TEFA.Codigo          
      
  INNER JOIN          
  Encabezado_Facturas AS ENFA ON          
  ENNF.EMPR_Codigo = ENFA.EMPR_Codigo          
  AND ENNF.ENFA_Numero = ENFA.Numero    
      
  INNER JOIN          
  Tipo_Documentos AS TIDO ON          
  ENNF.EMPR_Codigo = TIDO.EMPR_Codigo          
  AND ENNF.TIDO_Codigo = TIDO.Codigo          
            
  WHERE          
  ENNF.EMPR_Codigo = @par_EMPR_Codigo      
  AND ENNF.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENNF.TIDO_Codigo)      
  AND (ENNF.Numero = @par_Numero OR @par_Numero IS NULL)          
  AND (ENNF.Numero_Documento = @par_Numero_Documento OR @par_Numero_Documento IS NULL)          
  AND (ENNF.Fecha >= @par_Fecha_Inicial OR @par_Fecha_Inicial IS NULL)          
  AND (ENNF.Fecha <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)          
  AND (ENNF.TERC_Cliente = @par_TERC_Cliente OR @par_TERC_Cliente IS NULL)          
  AND (ENNF.TERC_Facturar = @par_TERC_Facturar OR @par_TERC_Facturar IS NULL)          
  AND (ENNF.OFIC_Nota = @par_OFIC_Nota OR @par_OFIC_Nota IS NULL)             
  AND (ENNF.OFIC_Nota IN (                    
  SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE EMPR_Codigo = @par_EMPR_Codigo AND USOF.USUA_Codigo = @par_Usuario_Consulta                    
  ) OR @par_Usuario_Consulta IS NULL)        
  AND (ENNF.Estado = @par_Estado OR @par_Estado IS NULL)            
  AND (ENNF.Anulado = @par_Anulado OR @par_Anulado IS NULL)          
  );                  
  WITH Pagina                  
  AS                  
  (              
  SELECT      
  ENNF.EMPR_Codigo,                  
  ENNF.Numero,          
  ENNF.Numero_Documento AS ENNF_NumeroDocumento,          
  ENNF.Numero_Preimpreso AS ENNF_NumeroPreimpreso,          
  ENNF.TIDO_Codigo,    
  TIDO.Nombre TIDO_Nombre,                
  ENNF.Fecha AS ENNF_Fecha,       
  ENFA.Numero_Documento AS ENFA_NumeroDocumento,      
  OFIC.Nombre As OFIC_NombreNota,      
  ENNF.TERC_Cliente,             
  LTRIM(RTRIM(ISNULL(CLIE.Razon_Social,'') + ISNULL(CLIE.Nombre,'') + ' ' + ISNULL(CLIE.Apellido1,'') + ' ' + ISNULL(CLIE.Apellido2,'')))  As CLIE_NombreCompleto,          
  ENNF.TERC_Facturar,          
  LTRIM(RTRIM(ISNULL(TEFA.Razon_Social,'') + ISNULL(TEFA.Nombre,'') + ' ' + ISNULL(TEFA.Apellido1,'') + ' ' + ISNULL(TEFA.Apellido2,'')))  As TEFA_NombreCompleto,           
  ENNF.Nota_Electronica AS ENNF_Nota_Electronica,          
  ENNF.Estado AS ENNF_Estado,          
  ENNF.Anulado AS ENNF_Anulado,      
  ENNF.Fecha_Crea,                  
  ENNF.USUA_Codigo_Crea,                  
  ENNF.Fecha_Modifica,                  
  ENNF.USUA_Codigo_Modifica,      
  ENNF.Valor_Nota,
  ENFA.Fecha AS ENFA_Fecha,
  ENFA.Valor_Factura,

      
  ROW_NUMBER() OVER (ORDER BY ENNF.Numero) AS RowNumber                  
          
  FROM Encabezado_Notas_Facturas ENNF       
          INNER JOIN                  
  Oficinas OFIC ON                  
  ENNF.EMPR_Codigo = OFIC.EMPR_Codigo                  
  AND ENNF.OFIC_Nota = OFIC.Codigo          
          
  INNER JOIN                  
  Terceros CLIE ON                  
  ENNF.EMPR_Codigo = CLIE.EMPR_Codigo                  
  AND ENNF.TERC_Cliente = CLIE.Codigo            
          
  INNER JOIN                  
  Terceros TEFA ON                  
  ENNF.EMPR_Codigo = TEFA.EMPR_Codigo                  
  AND ENNF.TERC_Facturar = TEFA.Codigo          
      
  INNER JOIN          
  Encabezado_Facturas AS ENFA ON          
  ENNF.EMPR_Codigo = ENFA.EMPR_Codigo          
  AND ENNF.ENFA_Numero = ENFA.Numero    
      
  INNER JOIN          
  Tipo_Documentos AS TIDO ON          
  ENNF.EMPR_Codigo = TIDO.EMPR_Codigo          
  AND ENNF.TIDO_Codigo = TIDO.Codigo          
            
  WHERE          
  ENNF.EMPR_Codigo = @par_EMPR_Codigo      
  AND ENNF.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENNF.TIDO_Codigo)      
  AND (ENNF.Numero = @par_Numero OR @par_Numero IS NULL)          
  AND (ENNF.Numero_Documento = @par_Numero_Documento OR @par_Numero_Documento IS NULL)          
  AND (ENNF.Fecha >= @par_Fecha_Inicial OR @par_Fecha_Inicial IS NULL)          
  AND (ENNF.Fecha <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)          
  AND (ENNF.TERC_Cliente = @par_TERC_Cliente OR @par_TERC_Cliente IS NULL)          
  AND (ENNF.TERC_Facturar = @par_TERC_Facturar OR @par_TERC_Facturar IS NULL)          
  AND (ENNF.OFIC_Nota = @par_OFIC_Nota OR @par_OFIC_Nota IS NULL)          
  AND (ENNF.OFIC_Nota IN (                    
  SELECT USOF.OFIC_Codigo FROM Usuario_Oficinas USOF WHERE EMPR_Codigo = @par_EMPR_Codigo AND USOF.USUA_Codigo = @par_Usuario_Consulta                    
  ) OR @par_Usuario_Consulta IS NULL)        
  AND (ENNF.Estado = @par_Estado OR @par_Estado IS NULL)          
  AND (ENNF.Anulado = @par_Anulado OR @par_Anulado IS NULL)            
  )                  
          
  SELECT      
  EMPR_Codigo,          
  Numero,          
  ENNF_NumeroDocumento,          
  ENNF_NumeroPreimpreso,          
  TIDO_Codigo,    
  TIDO_Nombre,        
  ENNF_Fecha,          
  ENFA_NumeroDocumento,             
  OFIC_NombreNota,          
  TERC_Cliente,          
  CLIE_NombreCompleto,          
  TERC_Facturar,          
  TEFA_NombreCompleto,  
  ENNF_Nota_Electronica,         
  ENNF_Estado,          
  ENNF_Anulado,          
  Fecha_Crea,          
  USUA_Codigo_Crea,          
  Fecha_Modifica,          
  USUA_Codigo_Modifica,     
  Valor_Nota AS ENNF_Valor_Nota,  
  ENFA_Fecha,
  Valor_Factura,
  @CantidadRegistros AS TotalRegistros,          
  @par_NumeroPagina AS PaginaObtener,          
  @par_RegistrosPagina AS RegistrosPagina          
          
  FROM Pagina          
  WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)          
  AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)           
END  
GO