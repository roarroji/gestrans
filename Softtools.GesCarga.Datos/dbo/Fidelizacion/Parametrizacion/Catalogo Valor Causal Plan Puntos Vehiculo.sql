﻿PRINT 'Catalogo Causal Plan Puntos Vehiculo - 189 - CPPV'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 189
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 189
GO
DELETE Catalogos WHERE Codigo = 189
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos (EMPR_Codigo, Codigo, Nombre_Corto, Nombre, Actualizable, Numero_Columnas, Numero_Campos_Llave)
SELECT Codigo, 189, 'CPPV','Catalogo Causal Plan Puntos Vehiculos', 1, 2, 1 FROM Empresas
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 189, 18901, 'Apoyo Urbano', '10', '', '', 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 189, 18902, 'Apoyo Exportaciones', '20', '', '', 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 189, 18903, 'Apoyo Destino Poco Frecuentes', '20', '', '', 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 189, 18904, 'No tomar la orden de cargue', '-2', '', '', 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 189, 18905, 'irrespeto con algun funcionario', '-12', '', '', 1, GETDATE() FROM Empresas
GO
-----------------------------------------CONFIGURACION CATALOGOS--------------------------------------------------
INSERT INTO Configuracion_Catalogos (EMPR_Codigo, CATA_Codigo, Secuencia, Nombre, Obligatorio, Parte_Llave, Tipo, Longitud_Campo, Numero_Decimales,
Utilizar_Combo, Codigo_Catalogo_Combo, Campo_Codigo_Combo, Campo_Nombre_Combo, Visible, Sugerir_Codigo)
SELECT Codigo,189,1,'Código',1,1,1,1,0,0,0,0,0,1,0 FROM Empresas
GO
INSERT INTO Configuracion_Catalogos (EMPR_Codigo, CATA_Codigo, Secuencia, Nombre, Obligatorio, Parte_Llave, Tipo, Longitud_Campo, Numero_Decimales,
Utilizar_Combo, Codigo_Catalogo_Combo, Campo_Codigo_Combo, Campo_Nombre_Combo, Visible, Sugerir_Codigo)
SELECT Codigo,189,2,'Nombre',1,1,1,1,0,0,0,0,0,1,0 FROM Empresas
GO
INSERT INTO Configuracion_Catalogos (EMPR_Codigo, CATA_Codigo, Secuencia, Nombre, Obligatorio, Parte_Llave, Tipo, Longitud_Campo, Numero_Decimales,
Utilizar_Combo, Codigo_Catalogo_Combo, Campo_Codigo_Combo, Campo_Nombre_Combo, Visible, Sugerir_Codigo)
SELECT Codigo,189,3,'Puntos',1,1,1,1,0,0,0,0,0,1,0 FROM Empresas
GO