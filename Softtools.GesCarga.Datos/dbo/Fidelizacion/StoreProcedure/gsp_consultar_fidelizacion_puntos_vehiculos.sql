﻿Print 'gsp_consultar_fidelizacion_puntos_vehiculos'
GO
DROP PROCEDURE gsp_consultar_fidelizacion_puntos_vehiculos
GO
CREATE PROCEDURE gsp_consultar_fidelizacion_puntos_vehiculos
(
	@par_EMPR_Codigo smallint,
	@par_Vehi_Codigo numeric = NULL,
	@par_TENE_Codigo numeric = NULL,
	@par_COND_Codigo numeric = NULL,
	@par_Cata_CFTR numeric = NULL,  
	@par_Estado smallint = NULL,
	@par_NumeroPagina INT = NULL,                    
	@par_RegistrosPagina INT   = NULL  
)
AS                                 
BEGIN                        
	SET NOCOUNT ON;                              
	DECLARE @CantidadRegistros INT                        
	SELECT                        
	@CantidadRegistros = (SELECT COUNT(1)

	FROM Plan_Puntos_Vehiculos PPVE

	INNER JOIN Vehiculos AS VEHI
	ON PPVE.EMPR_Codigo = VEHI.EMPR_Codigo
	AND PPVE.VEHI_Codigo = VEHI.Codigo

	LEFT JOIN Terceros AS TENE
	ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo

	LEFT JOIN Terceros AS COND
	ON VEHI.EMPR_Codigo = COND.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = COND.Codigo

	LEFT JOIN Valor_Catalogos AS CFTR
	ON PPVE.EMPR_Codigo = CFTR.EMPR_Codigo
	AND PPVE.CATA_CFTR_Codigo = CFTR.Codigo

	WHERE PPVE.EMPR_Codigo = @par_EMPR_Codigo
	AND PPVE.VEHI_Codigo = ISNULL(@par_Vehi_Codigo, PPVE.VEHI_Codigo)
	AND TENE.Codigo = ISNULL(@par_TENE_Codigo, TENE.Codigo)
	AND COND.Codigo = ISNULL(@par_COND_Codigo, COND.Codigo)
	AND PPVE.CATA_CFTR_Codigo = ISNULL(@par_Cata_CFTR, PPVE.CATA_CFTR_Codigo)
	AND PPVE.Estado = ISNULL(@par_Estado, PPVE.Estado)

);                        
WITH Pagina
AS
(
	SELECT
	PPVE.EMPR_Codigo,
	VEHI.Codigo AS VEHI_Codigo,
	VEHI.Placa AS VEHI_Placa,
	ISNULL(TENE.Codigo, 0) AS TENE_Codigo,
	IIF(TENE.Nombre is NULL OR TENE.Nombre = '', TENE.Razon_Social, CONCAT(TENE.Nombre,' ', TENE.Apellido1)) AS TENE_Nombre,
	--ISNULL(TENE.Nombre, '') AS TENE_Nombre,
	ISNULL(COND.Codigo, 0) AS COND_Codigo,
	IIF(COND.Nombre is NULL OR COND.Nombre = '', COND.Razon_Social, CONCAT(COND.Nombre,' ', COND.Apellido1)) AS COND_Nombre,
	--ISNULL(COND.Nombre, '') AS COND_Nombre,
	ISNULL(CFTR.Codigo, 0) AS CFTR_Codigo,
	ISNULL(CFTR.Campo1, '') AS CFTR_Campo1,
	ISNULL(PPVE.Fecha_Actualiza_Puntos,'') AS PPVE_FechaAct,
	ISNULL(PPVE.Total_Puntos, 0) AS Total_Puntos,
	PPVE.Estado,
	ROW_NUMBER() OVER (ORDER BY PPVE.VEHI_Codigo) AS RowNumber
	        
	FROM Plan_Puntos_Vehiculos PPVE

	INNER JOIN Vehiculos AS VEHI
	ON PPVE.EMPR_Codigo = VEHI.EMPR_Codigo
	AND PPVE.VEHI_Codigo = VEHI.Codigo

	LEFT JOIN Terceros AS TENE
	ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo

	LEFT JOIN Terceros AS COND
	ON VEHI.EMPR_Codigo = COND.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = COND.Codigo

	LEFT JOIN Valor_Catalogos AS CFTR
	ON PPVE.EMPR_Codigo = CFTR.EMPR_Codigo
	AND PPVE.CATA_CFTR_Codigo = CFTR.Codigo

	WHERE PPVE.EMPR_Codigo = @par_EMPR_Codigo
	AND PPVE.VEHI_Codigo = ISNULL(@par_Vehi_Codigo, PPVE.VEHI_Codigo)
	AND TENE.Codigo = ISNULL(@par_TENE_Codigo, TENE.Codigo)
	AND COND.Codigo = ISNULL(@par_COND_Codigo, COND.Codigo)
	AND PPVE.CATA_CFTR_Codigo = ISNULL(@par_Cata_CFTR, PPVE.CATA_CFTR_Codigo)
	AND PPVE.Estado = ISNULL(@par_Estado, PPVE.Estado)
)                        
	SELECT                     
	EMPR_Codigo,
	VEHI_Codigo,
	VEHI_Placa,
	TENE_Codigo,
	TENE_Nombre,
	COND_Codigo,
	COND_Nombre,
	CFTR_Codigo,
	CFTR_Campo1,
	PPVE_FechaAct,
	Total_Puntos,
	Estado,
	@CantidadRegistros AS TotalRegistros,
	@par_NumeroPagina AS PaginaObtener,
	@par_RegistrosPagina AS RegistrosPagina

	FROM Pagina                        
	WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                        
	AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                        
	ORDER BY Total_Puntos DESC                        
END