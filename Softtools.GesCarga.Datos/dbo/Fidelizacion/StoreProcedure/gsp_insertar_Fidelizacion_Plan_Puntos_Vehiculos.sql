﻿PRINT 'gsp_insertar_Fidelizacion_Plan_Puntos_Vehiculos'
GO
DROP PROCEDURE gsp_insertar_Fidelizacion_Plan_Puntos_Vehiculos 
GO
CREATE PROCEDURE gsp_insertar_Fidelizacion_Plan_Puntos_Vehiculos 
(
	@par_EMPR_Codigo smallint,
	@par_Vehi_Codigo numeric,
	@par_Cata_CFTR numeric, 
	@par_TotalPuntos numeric, 
	@par_Fecha varchar(20),
	@par_Estado smallint,
	@par_USUA_Codigo_Crea numeric
)
AS
BEGIN

	INSERT INTO Plan_Puntos_Vehiculos
	(
	EMPR_Codigo,
	VEHI_Codigo,
	CATA_CFTR_Codigo,
	Total_Puntos,
	Estado,
	Fecha,
	USUA_Codigo_Crea,
	Fecha_Crea
	)
	VALUES(
	@par_EMPR_Codigo,
	@par_Vehi_Codigo,
	@par_Cata_CFTR,
	@par_TotalPuntos,
	@par_Estado,
	@par_Fecha,
	@par_USUA_Codigo_Crea,
	GETDATE()
	)

	SELECT VEHI_Codigo FROM Plan_Puntos_Vehiculos
	WHERE EMPR_Codigo = @par_EMPR_Codigo      
	AND VEHI_Codigo = @par_Vehi_Codigo

END 
GO
