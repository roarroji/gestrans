﻿PRINT 'gsp_insertar_detalle_causal_fidelizacion_plan_puntos_vehiculos'
GO
DROP PROCEDURE gsp_insertar_detalle_causal_fidelizacion_plan_puntos_vehiculos 
GO
CREATE PROCEDURE gsp_insertar_detalle_causal_fidelizacion_plan_puntos_vehiculos 
(
	@par_EMPR_Codigo smallint,
	@par_Vehi_Codigo numeric,
	@par_Cata_CPPV numeric, 
	@par_Puntos numeric, 
	@par_Fecha_Inicio_Vigencia varchar(20),
	@par_Fecha_Fin_Vigencia varchar(20),
	@par_Estado smallint,
	@par_Anulado smallint,
	@par_USUA_Codigo_Crea numeric
)
AS
BEGIN

	INSERT INTO Detalle_Causales_Plan_Puntos_Vehiculos
	(
	EMPR_Codigo,
	VEHI_Codigo,
	CATA_CPPV_Codigo,
	Puntos,
	Fecha_Inicio,
	Fecha_Vence,
	Estado,
	USUA_Codigo_Crea,
	Fecha_Crea,
	Anulado
	)
	VALUES(
	@par_EMPR_Codigo,
	@par_Vehi_Codigo,
	@par_Cata_CPPV,
	@par_Puntos,
	@par_Fecha_Inicio_Vigencia,
	@par_Fecha_Fin_Vigencia,
	@par_Estado,
	@par_USUA_Codigo_Crea,
	GETDATE(),
	@par_Anulado
	)

	SELECT ID as Codigo FROM Detalle_Causales_Plan_Puntos_Vehiculos
	WHERE EMPR_Codigo = @par_EMPR_Codigo      
	AND VEHI_Codigo = @par_Vehi_Codigo
	AND ID = @@identity
END 
GO
