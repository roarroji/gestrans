﻿Print 'gsp_listado_informe_fidelizaciones'
GO
DROP PROCEDURE gsp_listado_informe_fidelizaciones
GO
CREATE PROCEDURE gsp_listado_informe_fidelizaciones (
@par_EMPR_Codigo	SMALLINT,
@par_VEHI_Codigo	NUMERIC = NULL,
@par_Fecha_Inicial	DATETIME = NULL,
@par_Fecha_Final	DATETIME = NULL,
@par_Estado			NUMERIC = NULL
)
AS
BEGIN

SET @par_Fecha_Inicial	= CONVERT(DATE , @par_Fecha_Inicial)
SET @par_Fecha_Final = DATEADD(HOUR , 23 , @par_Fecha_Final)
SET @par_Fecha_Final = DATEADD(MINUTE , 59 , @par_Fecha_Final)
SET @par_Fecha_Final = DATEADD(SECOND , 59 , @par_Fecha_Final)


	SELECT 
	PPVE.EMPR_Codigo,
	PPVE.VEHI_Codigo,
	VEHI.Codigo_Alterno As NumeroVehiculo,
	ISNULL(COND.Numero_Identificacion,'') As IdenConductor,
	ISNULL(COND.Razon_Social,'') + ISNULL(COND.Nombre,'') + ' ' + ISNULL(COND.Apellido1,'') + ' ' + ISNULL(COND.Apellido2,'') As NombreConductor,
	ISNULL(TENE.Numero_Identificacion,'') As IdenTenedor,
	ISNULL(TENE.Razon_Social,'') + ISNULL(TENE.Nombre,'') + ' ' + ISNULL(TENE.Apellido1,'') + ' ' + ISNULL(TENE.Apellido2,'') As NombreTenedor,
	VEHI.Placa,
	PPVE.Fecha, 
	PPVE.Fecha_Actualiza_Puntos, 
	PPVE.Total_Puntos, 
	PPVE.USUA_Codigo_Crea, 
	PPVE.USUA_Codigo_Modifica, 
	PPVE.Estado,

	CASE PPVE.Estado WHEN 1 THEN 'ACTIVO' ELSE 'INACTIVO' END As NombreEstado,
	EMPR.Nombre_Razon_Social
	FROM 
	Plan_Puntos_Vehiculos PPVE,
	Vehiculos VEHI,
	Empresas EMPR,
	Terceros TENE,
	Terceros COND
	WHERE PPVE.EMPR_Codigo = VEHI.EMPR_Codigo
	AND PPVE.VEHI_Codigo = VEHI.Codigo
	AND PPVE.EMPR_Codigo = EMPR.Codigo
	AND VEHI.EMPR_Codigo = COND.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = COND.Codigo
	AND VEHI.EMPR_Codigo = TENE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo

	AND PPVE.EMPR_Codigo = @par_EMPR_Codigo
	AND VEHI.Codigo = ISNULL(@par_VEHI_Codigo, VEHI.Codigo)
	AND PPVE.Fecha >= ISNULL(@par_Fecha_Inicial, PPVE.Fecha)
	AND PPVE.Fecha <= ISNULL(@par_Fecha_Final, PPVE.Fecha)
	AND PPVE.Estado = ISNULL(@par_Estado, PPVE.Estado)
	

END
GO