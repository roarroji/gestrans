﻿PRINT 'gsp_modifica_Fidelizacion_Plan_Puntos_Vehiculos'
GO
DROP PROCEDURE gsp_modifica_Fidelizacion_Plan_Puntos_Vehiculos 
GO
CREATE PROCEDURE gsp_modifica_Fidelizacion_Plan_Puntos_Vehiculos 
(
	@par_EMPR_Codigo smallint,
	@par_Vehi_Codigo numeric,
	@par_Cata_CFTR numeric, 
	@par_Estado smallint,
	@par_Fecha varchar(20),
	@par_USUA_Codigo_Modifica numeric
)
AS
BEGIN

	Update Plan_Puntos_Vehiculos SET
	CATA_CFTR_Codigo = @par_Cata_CFTR,
	Estado = @par_Estado,
	Fecha = @par_Fecha,
	USUA_Codigo_Modifica = @par_USUA_Codigo_Modifica,
	Fecha_Modifica = GETDATE()

	WHERE EMPR_Codigo = @par_EMPR_Codigo
	AND VEHI_Codigo = @par_Vehi_Codigo
	
	SELECT VEHI_Codigo FROM Plan_Puntos_Vehiculos
	WHERE EMPR_Codigo = @par_EMPR_Codigo      
	AND VEHI_Codigo = @par_Vehi_Codigo
END 
GO
