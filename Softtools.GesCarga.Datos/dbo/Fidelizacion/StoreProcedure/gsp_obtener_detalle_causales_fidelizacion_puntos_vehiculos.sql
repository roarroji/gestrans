﻿Print 'gsp_obtener_detalle_causales_fidelizacion_puntos_vehiculos'
GO
DROP PROCEDURE gsp_obtener_detalle_causales_fidelizacion_puntos_vehiculos
GO
CREATE PROCEDURE gsp_obtener_detalle_causales_fidelizacion_puntos_vehiculos
(
	@par_EMPR_Codigo smallint,
	@par_Codigo numeric
)
AS                                 
BEGIN                        
	SELECT
	DECA.EMPR_Codigo,
	DECA.ID AS Codigo,
	VEHI.Codigo AS VEHI_Codigo,
	VEHI.Placa AS VEHI_Placa,
	ISNULL(TENE.Codigo, 0) AS TENE_Codigo,
	IIF(TENE.Nombre is NULL OR TENE.Nombre = '', TENE.Razon_Social, CONCAT(TENE.Nombre,' ', TENE.Apellido1)) AS TENE_Nombre,
	ISNULL(COND.Codigo, 0) AS COND_Codigo,
	IIF(COND.Nombre is NULL OR COND.Nombre = '', COND.Razon_Social, CONCAT(COND.Nombre,' ', COND.Apellido1)) AS COND_Nombre,
	ISNULL(CPPV.Codigo, 0) AS CPPV_Codigo,
	ISNULL(CPPV.Campo1, '') AS CPPV_Campo1,
	ISNULL(DECA.Fecha_Inicio,'') AS DECA_FechaInicio,
	ISNULL(DECA.Fecha_Vence,'') AS DECA_FechaFin,
	ISNULL(DECA.Puntos, 0) AS Puntos,
	DECA.Estado,
	DECA.Anulado,
	USUA.Codigo_Usuario as USUA_CodigoUsuario,
	DECA.Fecha_Crea	
	        
	FROM Detalle_Causales_Plan_Puntos_Vehiculos DECA

	INNER JOIN Vehiculos AS VEHI
	ON DECA.EMPR_Codigo = VEHI.EMPR_Codigo
	AND DECA.VEHI_Codigo = VEHI.Codigo

	LEFT JOIN Terceros AS TENE
	ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo

	LEFT JOIN Terceros AS COND
	ON VEHI.EMPR_Codigo = COND.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = COND.Codigo

	LEFT JOIN Valor_Catalogos AS CPPV
	ON DECA.EMPR_Codigo = CPPV.EMPR_Codigo
	AND DECA.CATA_CPPV_Codigo = CPPV.Codigo

	LEFT JOIN Usuarios AS USUA
	ON DECA.EMPR_Codigo = USUA.EMPR_Codigo
	AND DECA.USUA_Codigo_Crea = USUA.Codigo

	WHERE DECA.EMPR_Codigo = @par_EMPR_Codigo
	AND DECA.ID = @par_Codigo
END
GO 