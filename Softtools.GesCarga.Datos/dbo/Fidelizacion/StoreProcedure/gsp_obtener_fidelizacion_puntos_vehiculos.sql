﻿Print 'gsp_obtener_fidelizacion_puntos_vehiculos'
GO
DROP PROCEDURE gsp_obtener_fidelizacion_puntos_vehiculos
GO
CREATE PROCEDURE gsp_obtener_fidelizacion_puntos_vehiculos
(
	@par_EMPR_Codigo smallint,
	@par_Vehi_Codigo numeric
)
AS                                 
BEGIN                        
	SELECT
	PPVE.EMPR_Codigo,
	VEHI.Codigo AS VEHI_Codigo,
	VEHI.Placa AS VEHI_Placa,
	ISNULL(TENE.Codigo, 0) AS TENE_Codigo,
	IIF(TENE.Nombre is NULL OR TENE.Nombre = '', TENE.Razon_Social, CONCAT(TENE.Nombre,' ', TENE.Apellido1)) AS TENE_Nombre,
	ISNULL(COND.Codigo, 0) AS COND_Codigo,
	IIF(COND.Nombre is NULL OR COND.Nombre = '', COND.Razon_Social, CONCAT(COND.Nombre,' ', COND.Apellido1)) AS COND_Nombre,
	ISNULL(CFTR.Codigo, 0) AS CFTR_Codigo,
	ISNULL(CFTR.Campo1, '') AS CFTR_Campo1,
	ISNULL(PPVE.Fecha_Actualiza_Puntos,'') AS PPVE_FechaAct,
	ISNULL(PPVE.Total_Puntos, 0) AS Total_Puntos,
	ISNULL(PPVE.Fecha,'') AS Fecha,
	PPVE.Estado
	        
	FROM Plan_Puntos_Vehiculos PPVE

	INNER JOIN Vehiculos AS VEHI
	ON PPVE.EMPR_Codigo = VEHI.EMPR_Codigo
	AND PPVE.VEHI_Codigo = VEHI.Codigo

	LEFT JOIN Terceros AS TENE
	ON VEHI.EMPR_Codigo = TENE.EMPR_Codigo
	AND VEHI.TERC_Codigo_Tenedor = TENE.Codigo

	LEFT JOIN Terceros AS COND
	ON VEHI.EMPR_Codigo = COND.EMPR_Codigo
	AND VEHI.TERC_Codigo_Conductor = COND.Codigo

	LEFT JOIN Valor_Catalogos AS CFTR
	ON PPVE.EMPR_Codigo = CFTR.EMPR_Codigo
	AND PPVE.CATA_CFTR_Codigo = CFTR.Codigo

	WHERE PPVE.EMPR_Codigo = @par_EMPR_Codigo
	AND PPVE.VEHI_Codigo = ISNULL(@par_Vehi_Codigo, PPVE.VEHI_Codigo)

END
GO 