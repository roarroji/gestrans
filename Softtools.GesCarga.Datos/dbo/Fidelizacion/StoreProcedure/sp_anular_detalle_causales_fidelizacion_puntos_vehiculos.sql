﻿PRINT 'sp_anular_detalle_causales_fidelizacion_puntos_vehiculos'
GO
DROP PROCEDURE [dbo].sp_anular_detalle_causales_fidelizacion_puntos_vehiculos
GO
CREATE PROCEDURE [dbo].sp_anular_detalle_causales_fidelizacion_puntos_vehiculos(
  @par_EMPR_Codigo SMALLINT,
  @par_Codigo NUMERIC,
  @par_Vehiculo NUMERIC,
  @par_USUA_Codigo_Anula NUMERIC,
  @par_Causa_Anulacion VARCHAR (150)
  )
AS
BEGIN
	UPDATE Detalle_Causales_Plan_Puntos_Vehiculos  SET
	Fecha_Anula = GETDATE(),
	Anulado = 1,
	USUA_Codigo_Anula = @par_USUA_Codigo_Anula,
	Causa_Anula = @par_Causa_Anulacion

	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND ID = @par_Codigo
	AND VEHI_Codigo = @par_Vehiculo

	SELECT @@ROWCOUNT AS Codigo
END
GO