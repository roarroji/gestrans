﻿Print 'V_Causal_Fidelizacion_Anuladas'
GO
DROP VIEW V_Causal_Fidelizacion_Anuladas
GO
CREATE VIEW V_Causal_Fidelizacion_Anuladas
AS
SELECT EMPR_Codigo, Codigo, CATA_Codigo, Campo1 As Nombre, CONVERT(NUMERIC(18,2),Campo2) As Puntos 
FROM Valor_Catalogos
WHERE CATA_Codigo = 189
GO