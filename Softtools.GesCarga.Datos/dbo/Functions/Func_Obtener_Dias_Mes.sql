﻿Print 'Func_Obtener_Dias_Mes'
GO
DROP FUNCTION Func_Obtener_Dias_Mes
GO
CREATE FUNCTION Func_Obtener_Dias_Mes
(
	@par_Fecha datetime
)
RETURNS NUMERIC
AS
BEGIN

declare @fechastr varchar(20), @fechadat datetime;
select @fechastr = convert(varchar, DATEPART(year, @par_Fecha)) +'/'+ convert(varchar,DATEPART(MONTH, @par_Fecha)) +'/' + '1';
select @fechadat = convert(datetime, @fechastr);
select @fechadat = DATEADD(MONTH,1,@fechadat);
select @fechadat = DATEADD(DAY,-1,@fechadat);
return DATEPART(DAY, @fechadat);


	
	--RETURN CASE WHEN MONTH(@par_Fecha) IN (1, 3, 5, 7, 8, 10, 12) THEN 31
 --               WHEN MONTH(@par_Fecha) IN (4, 6, 9, 11) THEN 30
 --               ELSE CASE WHEN (YEAR(@par_Fecha) % 4    = 0 AND
 --                               YEAR(@par_Fecha) % 100 != 0) OR
 --                              (YEAR(@par_Fecha) % 400  = 0)
 --                         THEN 29
 --                         ELSE 28
 --                    END
 --          END

END
GO