﻿Print 'Func_Prorratear_Flete_Transportador_Planilla_Especial'
GO
DROP FUNCTION Func_Prorratear_Flete_Transportador_Planilla_Especial
GO
CREATE FUNCTION Func_Prorratear_Flete_Transportador_Planilla_Especial
( 
	@par_EMPR_Codigo numeric,
	@par_EPEP_Numero numeric,
	@par_ID_Detalle numeric
  
)
RETURNS numeric
	
    BEGIN 

   DECLARE @inCantidadDetalle int = 0
			DECLARE @monValorPorDetalle money = 0
			DECLARE @intEsUltimoDetalle smallint = 0
			DECLARE @monValorFlete money = 0


			SELECT @inCantidadDetalle = ISNULL(COUNT(EMPR_Codigo),0) FROM Detalle_Recorrido_Planilla_Especial_Pasajeros
			WHERE EMPR_Codigo = @par_EMPR_Codigo
			AND EPEP_Numero = @par_EPEP_Numero

			SELECT @monValorPorDetalle = ROUND(ISNULL(Valor_Tarifa_Transportador/@inCantidadDetalle, 0),0), @monValorFlete = ISNULL(Valor_Tarifa_Transportador, 0)
			FROM Encabezado_Planilla_Especial_Pasajeros
			WHERE EMPR_Codigo = @par_EMPR_Codigo
			AND Numero = @par_EPEP_Numero

			SELECT @intEsUltimoDetalle = ISNULL(EMPR_Codigo,0) FROM Detalle_Recorrido_Planilla_Especial_Pasajeros
			WHERE EMPR_Codigo = @par_EMPR_Codigo
			AND EPEP_Numero = @par_EPEP_Numero
			AND Codigo = @par_ID_Detalle 
			AND Codigo in (SELECT MAX(Codigo) FROM Detalle_Recorrido_Planilla_Especial_Pasajeros
			WHERE EMPR_Codigo = @par_EMPR_Codigo
			AND EPEP_Numero = @par_EPEP_Numero)

			DECLARE @monResultado MONEY = 0

			IF ISNULL(@intEsUltimoDetalle, 0) > 0 BEGIN

				SET  @monResultado = (@monValorPorDetalle * @inCantidadDetalle)
				SET  @monResultado = @monResultado - @monValorFlete
				set  @monValorPorDetalle = @monValorPorDetalle - @monResultado
			END


        RETURN @monValorPorDetalle
    END
GO