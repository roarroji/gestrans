﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 15/10/2021 
-- Módulo: Gesphone

PRINT 'gsp_consultar_detalle_intento_entrega_guia'
GO
DROP PROCEDURE gsp_consultar_detalle_intento_entrega_guia
GO
CREATE PROCEDURE dbo.gsp_consultar_detalle_intento_entrega_guia      
(                      
  @par_EMPR_Codigo SMALLINT,
  @par_Numero INTEGER
)                      
AS                       
BEGIN
 
  SELECT EMPR_Codigo, ENRE_Numero, ID, Observaciones, USUA_Codigo_Crea, Fecha_Crea, Novedad_Intento_Entrega 
  FROM Detalle_Intentos_Entrega_Guias 
  WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_Numero  
  ORDER BY ID DESC

END    
GO 