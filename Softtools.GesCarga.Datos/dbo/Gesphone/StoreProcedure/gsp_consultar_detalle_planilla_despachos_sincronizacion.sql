﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 17/04/2021 
-- Módulo: Gesphone

PRINT 'gsp_consultar_detalle_planilla_despachos_sincronizacion'
GO
DROP PROCEDURE gsp_consultar_detalle_planilla_despachos_sincronizacion
GO
CREATE PROCEDURE dbo.gsp_consultar_detalle_planilla_despachos_sincronizacion      
(                      
  @par_EMPR_Codigo SMALLINT,
  @par_ENPD_Numero INTEGER
)                      
AS                       
BEGIN
  
   -- GE: Guias Entregas, GR: Guias Recogidas
  SELECT    
    --- Información General
    @par_EMPR_Codigo AS CodEmp,
    CONVERT(INTEGER, DEPD.ENPD_Numero) AS NumeroPlanilla, -- Numero Planilla
    CONVERT(INTEGER, DEPD.TIDO_Codigo) AS TipDoc,  -- Tipo Documento Remesa
    CONVERT(INTEGER, ENRE.Numero) AS NumeroRemesa, -- Numero Remesa
    CONVERT(INTEGER, ENRE.Numero_Documento) AS NumeroDocumentoRemesa, -- NumeroDocumentoRemesa
    ENRE.Fecha as Fec, -- Fecha Remesa
    CONVERT(INTEGER, ISNULL(ENRP.CATA_LNPA_Codigo, 21601)) as LinNeg, -- Codigo Linea Negocio
    CONVERT(INTEGER, ENRE.CATA_FOPR_Codigo) AS ForPag, -- Codigo Forma de Pago

    -- Cliente
    CONVERT(INTEGER, ENRE.TERC_Codigo_Cliente) AS CodCli, -- Codigo Tercero Cliente
     CLIE.Numero_Identificacion AS NumIdeCli, -- Número Identificación Cliente
    (CLIE.Razon_Social + '' + CLIE.Nombre) AS NomCli, -- Nombre o Razon Social Cliente
    (CLIE.Apellido1 + ' ' + CLIE.Apellido2) AS ApeCli, -- Apellidos Cliente

    -- Remitente
    CONVERT(INTEGER,ENRE.TERC_Codigo_Remitente) AS CodRem, -- Codigo Remitente
    CONVERT(INTEGER,ENRE.CIUD_Codigo_Remitente) AS CodCiuRem, -- Codigo Ciudad Remitente
    CIRE.Nombre AS NomCiuRem, -- Nombre Ciudad Remitente
    ENRE.Direccion_Remitente AS DirRem, -- Dirección Remitente    
    CONVERT(INTEGER,REMI.CATA_TIID_Codigo) AS TipIdeRem, -- Tipo Idetificación Remitente
    REMI.Numero_Identificacion AS NumIdeRem, -- Número Identificación Remitente
    (REMI.Razon_Social + '' + REMI.Nombre) AS NomRem, -- Nombre Remitente
    (REMI.Apellido1 + ' ' + REMI.Apellido2) AS ApeRem, -- Apellidos Remitente
    ENRE.Barrio_Remitente AS BarRem, -- Barrio Remitente
    ISNULL(ZOCI.Nombre, 'N/A') AS ZonRem, -- Zona Remitente
    REMI.Telefonos AS TelRem, -- Telefono Remitente

    -- Destinatario
    CONVERT(INTEGER,ENRE.TERC_Codigo_Destinatario) AS CodDes, -- Codigo Destinatario
    CONVERT(INTEGER,ENRE.CIUD_Codigo_Destinatario) AS CodCiuDest, -- Codigo Ciudad Destinatario
    CIDE.Nombre AS NomCiuDest, -- Nombre Ciudad Destinatario
    ENRE.Direccion_Destinatario AS DirDest, -- Dirección Destinario    
    CONVERT(INTEGER,DEST.CATA_TIID_Codigo) AS TipIdeDest, -- Tipo Idetificación Destinatario
    DEST.Numero_Identificacion AS NumIdeDest, -- Número Identificación Destinarario
    (DEST.Razon_Social + '' + DEST.Nombre) AS NomDest, -- Nombre Destinatario
    (DEST.Apellido1 + ' ' + DEST.Apellido2) AS ApeDest, -- Apellidos Destinatario
    ENRE.Barrio_Destinatario AS BarDest, -- Barrio Destinatario
    ISNULL(ZOCI.Nombre, 'N/A') AS ZonDest, -- Zona Destinatario
    DEST.Telefonos AS TelDest, -- Telefono Destinatario

    -- Información Recolección (No APLICA) 
    NULL AS FecRec, -- Fecha Recolección
    '' AS Cont, -- Contacto
    '' AS Bar, -- Barrio
    '' AS Dir, -- Dirección
    '' AS Tel, -- Teléfono
    0 AS CiuInfRec, -- Ciudad Contacto Recolección
    '' AS NomCiuCont, -- Nombre Ciudad Contacto

    -- Tarifas
    CONVERT(INTEGER,ISNULL(DTCV.TATC_Codigo, 0)) As CodTar, -- Código Tarifa                                              
  	ISNULL(TTTC.NombreTarifa,'') As NomTar, -- Nombre Tarifa                   
  	CONVERT(INTEGER,ISNULL(DTCV.TTTC_Codigo, 0)) As CodTipTar, -- Código Tipo Tarifa                                                
  	ISNULL(TTTC.Nombre,'') As NomTipTar, -- Nombre Tipo Tarifa
    CONVERT(INTEGER,ENRE.PRTR_Codigo) as CodPro, -- Código Producto
    PRTR.Nombre as NomPro, -- Nombre Producto
    ENRP.Descripcion_Mercancia AS Merca, -- Mercancia
    0 AS CodUniEmp, -- Codígo Unidad de Empaque
    '' AS NomUniEmp, -- Nombre Unidad de Empaque
    
    -- Totales
    ISNULL(ENRE.Cantidad_Cliente,0) AS Cant, -- Cantidad Cliente                    
    ISNULL(ENRE.Peso_Cliente, 0) AS Pes, -- Peso Cliente   
    ISNULL(ENRP.Largo, 0) AS Lar, -- Largo      
    ISNULL(ENRP.Ancho, 0) AS Anc, -- Ancho        
    ISNULL(ENRP.Alto, 0) AS Alt, -- Alto       
    ISNULL(ENRP.Peso_Volumetrico, 0) AS PeVo, -- Peso Volumetrico
    ISNULL(ENRP.Peso_A_Cobrar, 0) AS PeCo, -- Peso Cobrar        
    ISNULL(ENRE.Valor_Comercial_Cliente, 0) AS ValCom, -- Valor Comercial        
    ISNULL(ENRE.Valor_Manejo_Cliente, 0) AS ValMan, -- Valor Manejo        
    ISNULL(ENRE.Valor_Seguro_Cliente, 0) AS ValSeg, -- Valor Seguro       
    ISNULL(ENRE.Valor_Reexpedicion, 0) AS ValRex, -- Valor Reexpedicion       
    ISNULL(ENRE.Valor_Flete_Cliente, 0) AS ValFle, -- Valor Flete       
    ISNULL(ENRE.Total_Flete_Cliente, 0) AS TotFle, -- Total Flete       	
    ISNULL(ENRE.Valor_Cargue, 0) AS ValAcaLoc,
    ISNULL(ENRE.Valor_Descargue, 0) AS ValAcaDes,

    -- Complementaria  
    CONVERT(INTEGER, ENRP.CATA_ESRP_Codigo) AS Est, -- Estado Remesa
    ENRP.Guia_Creada_Ruta_Conductor AS EsGuiaRecogida, -- Es Guía Recogida
    (SELECT Count(0) From Remesa_Gestion_Documentos GEDO WITH (NOLOCK) WHERE GEDO.EMPR_Codigo = @par_EMPR_Codigo AND GEDO.ENRE_Numero = ENRE.Numero) AS TieneDoc, --Tiene Gestión de Documentos
    CONVERT(DATE,ENRP.Fecha_Entrega) as FecEnt, -- Fecha Entrega
    CONVERT(INTEGER,ENRP.CATA_HERP_Codigo) AS HorEnt, -- Horario Entrega
    ENRE.Observaciones AS Obs,
    CONVERT(INTEGER,ENRE.Cumplido) As Cum,

    -- Entrega/Devolución
    CONVERT(DATE,ENRP.Fecha_Recibe) AS FecReci, 
    ENRP.Numero_Identificacion_Recibe AS IdeRec, 
    ENRP.Nombre_Recibe AS NomRec, 
    ENRP.Telefonos_Recibe AS TelRec, 
    ISNULL(ENRP.Cantidad_Recibe,0) AS CanRec, 
    ISNULL(ENRP.Peso_Recibe,0) AS PesRec, 
    ENRP.Observaciones_Recibe As ObsRec, 
    CONVERT(INTEGER,ENRP.CATA_NERP_Codigo) AS NovRec

  FROM Encabezado_Remesas ENRE WITH (NOLOCK) INNER JOIN Remesas_Paqueteria ENRP ON ENRE.Numero = ENRP.ENRE_Numero 
  INNER JOIN Detalle_Planilla_Despachos DEPD  WITH (NOLOCK) ON ENRE.Numero = DEPD.ENRE_Numero  
  INNER JOIN Terceros CLIE  WITH (NOLOCK) ON ENRE.TERC_Codigo_Cliente = CLIE.Codigo 
  INNER JOIN Terceros REMI  WITH (NOLOCK) ON ENRE.TERC_Codigo_Remitente = REMI.Codigo 
  INNER JOIN Ciudades CIRE  WITH (NOLOCK) ON ENRE.CIUD_Codigo_Remitente = CIRE.Codigo
  INNER JOIN Terceros DEST  WITH (NOLOCK) ON ENRE.TERC_Codigo_Destinatario = DEST.Codigo 
  INNER JOIN Ciudades CIDE  WITH (NOLOCK) ON ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo
      
  LEFT JOIN Zona_Ciudades As ZOCI ON                          
  ENRP.EMPR_Codigo = ZOCI.EMPR_Codigo                          
  AND ENRP.ZOCI_Codigo_Entrega = ZOCI.Codigo  
  
  LEFT JOIN Detalle_Tarifario_Carga_Ventas AS DTCV                                                  
  ON ENRE.EMPR_Codigo = DTCV.EMPR_Codigo                                                  
  AND ENRE.DTCV_Codigo = DTCV.Codigo  
  
  LEFT JOIN V_Tipo_Tarifa_Transporte_Carga As TTTC ON                                            
  ISNULL(DTCV.EMPR_Codigo, 0) = TTTC.EMPR_Codigo                                            
  AND ISNULL(DTCV.TATC_Codigo, 0) = TTTC.TATC_Codigo                                            
  AND ISNULL(DTCV.TTTC_Codigo, 0) = TTTC.Codigo     
  
  LEFT JOIN Producto_Transportados AS PRTR ON                    
  ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                    
  AND ENRE.PRTR_Codigo = PRTR.Codigo 
  
  --  LEFT JOIN Unidad_Empaque_Producto_Transportados AS UEPT ON                    
  --	ENRE.EMPR_Codigo = UEPT.EMPR_Codigo                    
  --	AND ENRE.UEPT_Codigo = UEPT.Codigo    
  
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo AND DEPD.ENPD_Numero = @par_ENPD_Numero    

  UNION ALL
  -- RE: Recolecciones
  SELECT 
    
    @par_EMPR_Codigo AS CodEmp,
    CONVERT(INTEGER, ENPD.Numero) AS NumeroPlanilla, 
    CONVERT(INTEGER, ENRE.TIDO_Codigo) AS TipDoc, 
    CONVERT(INTEGER, ENRE.Numero) AS NumeroRemesa, 
    CONVERT(INTEGER, ENRE.Numero_Documento) AS NumeroDocumentoRemesa, 
    ENRE.Fecha as Fec,
    CONVERT(INTEGER, ISNULL(ENRE.CATA_LNPA_Codigo, 21601)) as LinNeg,
    CONVERT(INTEGER,ENRE.FPVE_Codigo) AS ForPag,

    -- Cliente
    CONVERT(INTEGER, ENRE.TERC_Codigo_Cliente) AS CodCli, -- Codigo Tercero Cliente
     CLIE.Numero_Identificacion AS NumIdeCli, -- Número Identificación Cliente
    (CLIE.Razon_Social + '' + CLIE.Nombre) AS NomCli, -- Nombre o Razon Social Cliente
    (CLIE.Apellido1 + ' ' + CLIE.Apellido2) AS ApeCli, -- Apellidos Cliente

    -- Remitente
    CONVERT(INTEGER,ENRE.TERC_Codigo_Remitente) AS CodRem, -- Codigo Remitente
    CONVERT(INTEGER,ENRE.CIUD_Codigo_Remitente) AS CodCiuRem, -- Codigo Ciudad Remitente
    CIRE.Nombre AS NomCiuRem, -- Nombre Ciudad Remitente
    ENRE.Direccion_Remitente AS DirRem, -- Dirección Remitente    
    CONVERT(INTEGER,REMI.CATA_TIID_Codigo) AS TipIdeRem, -- Tipo Idetificación Remitente
    REMI.Numero_Identificacion AS NumIdeRem, -- Número Identificación Remitente
    (REMI.Razon_Social + '' + REMI.Nombre) AS NomRem, -- Nombre Remitente
    (REMI.Apellido1 + ' ' + REMI.Apellido2) AS ApeRem, -- Apellidos Remitente
    ENRE.Barrio_Remitente AS BarRem, -- Barrio Remitente
    ISNULL(ZOCI.Nombre, 'N/A') AS ZonRem, -- Zona Remitente
    REMI.Telefonos AS TelRem, -- Telefono Remitente

    -- Destinatario
    CONVERT(INTEGER,ENRE.TERC_Codigo_Destinatario) AS CodDes, -- Codigo Destinatario
    CONVERT(INTEGER,ENRE.CIUD_Codigo_Destinatario) AS CodCiuDest, -- Codigo Ciudad Destinatario
    CIDE.Nombre AS NomCiuDest, -- Nombre Ciudad Destinatario
    ENRE.Direccion_Destinatario AS DirDest, -- Dirección Destinario    
    CONVERT(INTEGER,DEST.CATA_TIID_Codigo) AS TipIdeDest, -- Tipo Idetificación Destinatario
    DEST.Numero_Identificacion AS NumIdeDest, -- Número Identificación Destinarario
    (DEST.Razon_Social + '' + DEST.Nombre) AS NomDest, -- Nombre Destinatario
    (DEST.Apellido1 + ' ' + DEST.Apellido2) AS ApeDest, -- Apellidos Destinatario
    ENRE.Barrio_Destinatario AS BarDest, -- Barrio Destinatario
    ISNULL(ZOCI.Nombre, 'N/A') AS ZonDest, -- Zona Destinatario
    DEST.Telefonos AS TelDest, -- Telefono Destinatario

    -- Información Recolección (No APLICA) 
    ENRE.Fecha_Recoleccion AS FecRec, -- Fecha Recolección
    ENRE.Nombre_Contacto AS Cont, -- Contacto
    ENRE.Barrio AS Bar, -- Barrio
    ENRE.Direccion AS Dir, -- Dirección
    ENRE.Telefonos AS Tel, -- Teléfono
    CONVERT(INTEGER,ENRE.CIUD_Codigo) AS CiuInfRec, -- Ciudad Contacto Recolección
    CCON.Nombre AS NomCiuCont, -- Nombre Ciudad Contacto

    -- Tarifas
    CONVERT(INTEGER,ISNULL(DTCV.TATC_Codigo, 0)) As CodTar, -- Código Tarifa                                              
  	ISNULL(TTTC.NombreTarifa,'') As NomTar, -- Nombre Tarifa                   
  	CONVERT(INTEGER,ISNULL(DTCV.TTTC_Codigo, 0)) As CodTipTar, -- Código Tipo Tarifa                                                
  	ISNULL(TTTC.Nombre,'') As NomTipTar, -- Nombre Tipo Tarifa
    CONVERT(INTEGER,ENRE.PRTR_Codigo) as CodPro, -- Código Producto
    PRTR.Nombre as NomPro, -- Nombre Producto
    ENRE.Mercancia AS Merca, -- Mercancia
    CONVERT(INTEGER,UEPT.Codigo) AS CodUniEmp, -- Codígo Unidad de Empaque
    UEPT.Descripcion AS NomUniEmp, -- Nombre Unidad de Empaque
    
    -- Totales
    ISNULL(ENRE.Cantidad,0) AS Cant, -- Cantidad Cliente                    
    ISNULL(ENRE.Peso, 0) AS Pes, -- Peso Cliente   
    ISNULL(ENRE.Largo, 0) AS Lar, -- Largo      
    ISNULL(ENRE.Ancho, 0) AS Anc, -- Ancho        
    ISNULL(ENRE.Alto, 0) AS Alt, -- Alto       
    ISNULL(ENRE.Peso_Volumetrico, 0) AS PeVo, -- Peso Volumetrico
    ISNULL(ENRE.Peso_A_Cobrar, 0) AS PeCo, -- Peso Cobrar        
    ISNULL(ENRE.Valor_Comercial_Cliente, 0) AS ValCom, -- Valor Comercial        
    ISNULL(ENRE.Valor_Manejo_Cliente, 0) AS ValMan, -- Valor Manejo        
    ISNULL(ENRE.Valor_Seguro_Cliente, 0) AS ValSeg, -- Valor Seguro       
    0 AS ValRex, -- Valor Reexpedicion       
    ISNULL(ENRE.Valor_Flete_Cliente, 0) AS ValFle, -- Valor Flete       
    ISNULL(ENRE.Total_Flete_Cliente, 0) AS TotFle, -- Total Flete  
    0 AS ValAcaLoc,
    0 AS ValAcaDes,

    -- Complementaria  
    CONVERT(INTEGER, ENRE.CATA_EREP_Codigo) AS Est, -- Estado Remesa
    CONVERT(SMALLINT, 0) AS EsGuiaRecogida, -- Es Guía Recogida
    0 AS TieneDoc, --Tiene Gestión de Documentos
    CONVERT(DATE,ENRE.Fecha_Entrega) as FecEnt, -- Fecha Entrega
    CONVERT(INTEGER,ENRE.CATA_HERP_Codigo) AS HorEnt, -- Horario Entrega
    ENRE.Observaciones AS Obs,    
    (SELECT Count(0) From Cumplido_Remesas CURE WITH (NOLOCK) WHERE CURE.EMPR_Codigo = @par_EMPR_Codigo AND CURE.ENRE_Numero = ENRE.Numero) AS Cum, -- Remesa Cumplida

     -- Entrega/Devolución
    null AS FecReci, 
    '' AS IdeRec, 
    '' AS NomRec, 
    '' AS TelRec, 
    0 AS CanRec, 
    0 AS PesRec, 
    '' As ObsRec, 
    0 AS NovRec
    
  FROM Encabezado_Recolecciones ENRE WITH (NOLOCK) INNER JOIN Encabezado_Planilla_Despachos ENPD  WITH (NOLOCK) ON ENRE.ENPR_Numero = ENPD.Numero
  INNER JOIN Terceros CLIE  WITH (NOLOCK) ON ENRE.TERC_Codigo_Cliente = CLIE.Codigo 
  INNER JOIN Terceros REMI  WITH (NOLOCK) ON ENRE.TERC_Codigo_Remitente = REMI.Codigo 
  INNER JOIN Ciudades CIRE  WITH (NOLOCK) ON ENRE.CIUD_Codigo_Remitente = CIRE.Codigo
  INNER JOIN Terceros DEST  WITH (NOLOCK) ON ENRE.TERC_Codigo_Destinatario = DEST.Codigo 
  INNER JOIN Ciudades CIDE  WITH (NOLOCK) ON ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo
  LEFT JOIN Ciudades CCON  WITH (NOLOCK) ON ENRE.CIUD_Codigo = CCON.Codigo

  LEFT JOIN Zona_Ciudades As ZOCI ON                          
	ENRE.EMPR_Codigo = ZOCI.EMPR_Codigo                          
	AND ENRE.ZOCI_Codigo = ZOCI.Codigo  

  LEFT JOIN Detalle_Tarifario_Carga_Ventas AS DTCV                                                  
  ON ENRE.EMPR_Codigo = DTCV.EMPR_Codigo                                                  
  AND ENRE.DTCV_Codigo = DTCV.Codigo  
  
  LEFT JOIN V_Tipo_Tarifa_Transporte_Carga As TTTC ON                                            
  ISNULL(DTCV.EMPR_Codigo, 0) = TTTC.EMPR_Codigo                                            
  AND ISNULL(DTCV.TATC_Codigo, 0) = TTTC.TATC_Codigo                                            
  AND ISNULL(DTCV.TTTC_Codigo, 0) = TTTC.Codigo     
  
  LEFT JOIN Producto_Transportados AS PRTR ON                    
  ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                    
  AND ENRE.PRTR_Codigo = PRTR.Codigo 
  
  LEFT JOIN Unidad_Empaque_Producto_Transportados AS UEPT ON                    
  ENRE.EMPR_Codigo = UEPT.EMPR_Codigo                    
  AND ENRE.UEPT_Codigo = UEPT.Codigo 

  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo AND ENPD.Numero = @par_ENPD_Numero
                  
END    
GO