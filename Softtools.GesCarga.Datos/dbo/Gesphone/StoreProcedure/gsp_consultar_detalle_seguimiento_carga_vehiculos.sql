﻿PRINT 'gsp_consultar_detalle_seguimiento_carga_vehiculos'
GO
DROP PROCEDURE gsp_consultar_detalle_seguimiento_carga_vehiculos
GO
CREATE PROCEDURE gsp_consultar_detalle_seguimiento_carga_vehiculos
(
	@par_EMPR_Codigo SMALLINT,
	@par_Numero NUMERIC = NULL,
	@par_Numero_Documento VARCHAR(20) = NULL,
	@par_Numero_Documento_Soporte NUMERIC = NULL,
	@par_Numero_Remesa NUMERIC = NULL,
	@par_Estado SMALLINT = NULL,
	@par_Nombre_Cliente VARCHAR(50) = NULL,
	@par_NumeroPagina INT = NULL,
	@par_RegistrosPagina INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
		DECLARE
			@CantidadRegistros	INT
		SELECT @CantidadRegistros = (
				SELECT DISTINCT 
					COUNT(1) 
				FROM
					Detalle_Seguimiento_Carga_Vehiculos DECV,
					Tipo_Documentos TIDO,
					V_Novedad_Seguimiento_Carga NOSC,
					V_Tipo_Reporte_Seguimiento TIRS,
					Terceros TERC,
					Ciudades CIUD
				WHERE

					DECV.EMPR_Codigo = TIDO.EMPR_Codigo 
					AND DECV.TIDO_Soporte = TIDO.Codigo

					AND DECV.EMPR_Codigo = NOSC.EMPR_Codigo 
					AND DECV.CATA_NOSC_Codigo = NOSC.Codigo

					AND DECV.EMPR_Codigo = TIRS.EMPR_Codigo 
					AND DECV.CATA_TIRS_Codigo = TIRS.Codigo

					AND DECV.EMPR_Codigo = TERC.EMPR_Codigo 
					AND DECV.TERC_Codigo_Cliente = TERC.Codigo

					AND DECV.EMPR_Codigo = CIUD.EMPR_Codigo 
					AND DECV.CIUD_Recibido_Cliente = CIUD.Codigo

					AND DECV.EMPR_Codigo = @par_EMPR_Codigo
					AND DECV.Estado = ISNULL(@par_Estado, DECV.Estado)
					AND ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%') OR (@par_Nombre_Cliente IS NULL))
					AND ((DECV.Numero_Documento LIKE '%' + @par_Numero_Documento + '%') OR (@par_Numero_Documento IS NULL))
					AND DECV.Numero_Documento_Soporte = ISNULL(@par_Numero_Documento_Soporte, DECV.Numero_Documento_Soporte)
					AND DECV.ID = ISNULL(@par_Numero, DECV.ID)
					AND DECV.Numero_Remesa = ISNULL(@par_Numero_Remesa, DECV.Numero_Remesa)
					);
					       
				WITH Pagina AS
				(

				SELECT
					DECV.EMPR_Codigo,
					DECV.ID,
					DECV.Numero_Documento,
					DECV.ENSV_Codigo,
					DECV.TIDO_Soporte,
					DECV.Numero_Documento_Soporte,
					DECV.Numero_Remesa,
					DECV.Fecha_Hora_Reporte,
					DECV.CATA_TIRS_Codigo,
					DECV.TERC_Codigo_Cliente,
					DECV.CATA_NOSC_Codigo,
					DECV.Observaciones,
					DECV.Identificacion_Recibido_Cliente,
					DECV.Nombre_Recibido_Cliente,
					DECV.CIUD_Recibido_Cliente,
					DECV.Direccion_Recibido_Cliente,
					DECV.Telefono_Recibido_Cliente,
					DECV.Cumplir,
					DECV.Reportar_Cliente,
					DECV.Estado,
					DECV.Anulado,
					TIDO.Nombre AS TipoDocumento,
					NOSC.Nombre AS NovedadSeguimiento,
					TIRS.Nombre AS TipoSeguimiento,
					CASE WHEN TERC.CATA_TINT_Codigo = 1202 THEN TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 ELSE TERC.Razon_Social END As NombreCliente,
					CIUD.Nombre AS Ciudad,
					ROW_NUMBER() OVER(ORDER BY DECV.ID) AS RowNumber
				FROM
					Detalle_Seguimiento_Carga_Vehiculos DECV,
					Tipo_Documentos TIDO,
					V_Novedad_Seguimiento_Carga NOSC,
					V_Tipo_Reporte_Seguimiento TIRS,
					Terceros TERC,
					Ciudades CIUD
				WHERE

					DECV.EMPR_Codigo = TIDO.EMPR_Codigo 
					AND DECV.TIDO_Soporte = TIDO.Codigo

					AND DECV.EMPR_Codigo = NOSC.EMPR_Codigo 
					AND DECV.CATA_NOSC_Codigo = NOSC.Codigo

					AND DECV.EMPR_Codigo = TIRS.EMPR_Codigo 
					AND DECV.CATA_TIRS_Codigo = TIRS.Codigo

					AND DECV.EMPR_Codigo = TERC.EMPR_Codigo 
					AND DECV.TERC_Codigo_Cliente = TERC.Codigo

					AND DECV.EMPR_Codigo = CIUD.EMPR_Codigo 
					AND DECV.CIUD_Recibido_Cliente = CIUD.Codigo

					AND DECV.EMPR_Codigo = @par_EMPR_Codigo
					AND DECV.Estado = ISNULL(@par_Estado, DECV.Estado)
					AND ((TERC.Nombre + ' ' + TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%' OR TERC.Razon_Social LIKE '%' + RTRIM(LTRIM(@par_Nombre_Cliente)) + '%') OR (@par_Nombre_Cliente IS NULL))
					AND ((DECV.Numero_Documento LIKE '%' + @par_Numero_Documento + '%') OR (@par_Numero_Documento IS NULL))
					AND DECV.Numero_Documento_Soporte = ISNULL(@par_Numero_Documento_Soporte, DECV.Numero_Documento_Soporte)
					AND DECV.ID = ISNULL(@par_Numero, DECV.ID)
					AND DECV.Numero_Remesa = ISNULL(@par_Numero_Remesa, DECV.Numero_Remesa)
		)
		SELECT DISTINCT
		0 AS Obtener,
		EMPR_Codigo,
		ID,
		Numero_Documento,
		ENSV_Codigo,
		TIDO_Soporte,
		Numero_Documento_Soporte,
		Numero_Remesa,
		Fecha_Hora_Reporte,
		CATA_TIRS_Codigo,
		TERC_Codigo_Cliente,
		CATA_NOSC_Codigo,
		Observaciones,
		Identificacion_Recibido_Cliente,
		Nombre_Recibido_Cliente,
		CIUD_Recibido_Cliente,
		Direccion_Recibido_Cliente,
		Telefono_Recibido_Cliente,
		Cumplir,
		Reportar_Cliente,
		Estado,
		Anulado,
		TipoDocumento,
		NovedadSeguimiento,
		TipoSeguimiento,
		NombreCliente,
		Ciudad,
		@CantidadRegistros AS TotalRegistros,
		@par_NumeroPagina AS PaginaObtener,
		@par_RegistrosPagina AS RegistrosPagina
		FROM
			Pagina
		WHERE
			RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
			AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)
		order by id

END
GO