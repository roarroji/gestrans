﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 18/10/2021 
-- Módulo: Gesphone

PRINT 'gsp_consultar_firma_remesa_paqueteria'
GO
DROP PROCEDURE gsp_consultar_firma_remesa_paqueteria
GO
CREATE PROCEDURE dbo.gsp_consultar_firma_remesa_paqueteria      
(                      
  @par_EMPR_Codigo SMALLINT,
  @par_Numero INTEGER
)                      
AS                       
BEGIN
 
  SELECT 1 AS EsFirma, Firma_Recibe AS Imagen
  FROM Remesas_Paqueteria    
  WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENRE_Numero = @par_Numero  
                  
END    
GO