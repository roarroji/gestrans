﻿PRINT 'gsp_consultar_seguimientos_abiertos'
GO
DROP PROCEDURE gsp_consultar_seguimientos_abiertos
GO
CREATE PROCEDURE gsp_consultar_seguimientos_abiertos
(
  @par_EMPR_Codigo SMALLINT,
  @par_Numero_Documento NUMERIC = NULL
)
AS
BEGIN
  
  SELECT TOP 1
	1 as Obtener,
	ENSV.EMPR_Codigo,
    ENSV.Codigo,
    ENRE.TIDO_Codigo, 
    ENRE.Codigo AS Codigo_Remesa, 
    ENSV.ENRE_Numero, 
    
    ENRE.Fecha as FechaDocumento, 
    ENRE.Cantidad_Cliente, 
    ENRE.Peso_Cliente, 
    ENRE.TERC_Codigo_Conductor, 
    ENRE.TERC_Codigo_Cliente, 
    CASE WHEN CLIE.CATA_TINT_Codigo = 1202 THEN CLIE.Nombre + ' ' + CLIE.Apellido1 + ' ' + CLIE.Apellido2 ELSE CLIE.Razon_Social END As NombreCliente,
    ENRE.CIUD_Codigo_Destino, 
    CIUD.Nombre AS NombreCiudad,
    DERE.PRTR_Codigo,
    PRTR.Nombre AS NombreProducto,
    ENSV.Estado,
    ENSV.Anulado
  FROM 
    Encabezado_Seguimiento_Vehiculos ENSV, 
    Encabezado_Remesas ENRE, 
    Ciudades CIUD,
    Terceros CLIE,    
    Detalle_Remesas DERE,
    Producto_Transportados PRTR

  WHERE ENSV.EMPR_Codigo = ENRE.EMPR_Codigo 
  AND ENRE.EMPR_Codigo =  CIUD.EMPR_Codigo
  AND ENSV.EMPR_Codigo = @par_EMPR_Codigo
  AND ENRE.CIUD_Codigo_Destino = CIUD.Codigo
  AND ENRE.Numero_Documento = ENSV.ENRE_Numero
  AND ((ENSV.ENRE_Numero = @par_Numero_Documento) OR (@par_Numero_Documento IS NULL)) 
  AND ENRE.EMPR_Codigo = CLIE.EMPR_Codigo
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo
  AND ENRE.EMPR_Codigo = DERE.EMPR_Codigo
  AND ENRE.Codigo = DERE.ENRE_Codigo
  AND DERE.EMPR_Codigo = PRTR.EMPR_Codigo
  AND DERE.PRTR_Codigo = PRTR.Codigo

END
GO

