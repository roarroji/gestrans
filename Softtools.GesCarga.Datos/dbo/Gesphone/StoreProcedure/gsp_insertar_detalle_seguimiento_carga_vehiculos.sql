﻿PRINT 'gsp_insertar_detalle_seguimiento_carga_vehiculos'
GO
DROP PROCEDURE gsp_insertar_detalle_seguimiento_carga_vehiculos
GO
CREATE PROCEDURE gsp_insertar_detalle_seguimiento_carga_vehiculos
(
@par_EMPR_Codigo SMALLINT,
@par_Numero_Documento VARCHAR(20),
@par_ENSV_Codigo NUMERIC,
@par_TIDO_Soporte NUMERIC,
@par_Numero_Documento_Soporte NUMERIC,
@par_Numero_Remesa NUMERIC,
@par_CATA_TIRS_Codigo NUMERIC,
@par_TERC_Codigo_Cliente NUMERIC,
@par_CATA_NOSC_Codigo NUMERIC,
@par_Observaciones VARCHAR (200) = NULL,
@par_Identificacion_Recibido_Cliente VARCHAR (15)= NULL,
@par_Nombre_Recibido_Cliente  VARCHAR (50)= NULL,
@par_CIUD_Recibido_Cliente NUMERIC,
@par_Direccion_Recibido_Cliente VARCHAR (100) = NULL,
@par_Telefono_Recibido_Cliente VARCHAR (20) = NULL,
@par_Cumplir SMALLINT = NULL,
@par_Reportar_Cliente SMALLINT = NULL,
@par_Estado SMALLINT,
@par_OFIC_Crea NUMERIC,
@par_USUA_Codigo_Crea NUMERIC,
@par_Firma IMAGE = NULL
)
AS
BEGIN 
	INSERT INTO Detalle_Seguimiento_Carga_Vehiculos
	(
		EMPR_Codigo,
		Numero_Documento,
		ENSV_Codigo,
		TIDO_Soporte,
		Numero_Documento_Soporte,
		Numero_Remesa,
		Fecha_Hora_Reporte,
		CATA_TIRS_Codigo,
		TERC_Codigo_Cliente,
		CATA_NOSC_Codigo,
		Observaciones,
		Identificacion_Recibido_Cliente,
		Nombre_Recibido_Cliente,
		CIUD_Recibido_Cliente,
		Direccion_Recibido_Cliente,
		Telefono_Recibido_Cliente,
		Cumplir,
		Reportar_Cliente,
		Estado,
		OFIC_Crea,
		USUA_Codigo_Crea,
		Fecha_Crea,
		Firma
	)
	VALUES
	(
		@par_EMPR_Codigo,
		@par_Numero_Documento,
		@par_ENSV_Codigo,
		@par_TIDO_Soporte,
		@par_Numero_Documento_Soporte,
		@par_Numero_Remesa,
		GETDATE(),
		@par_CATA_TIRS_Codigo,
		@par_TERC_Codigo_Cliente,
		@par_CATA_NOSC_Codigo,
		ISNULL(@par_Observaciones,''),
		ISNULL(@par_Identificacion_Recibido_Cliente,''),
		ISNULL(@par_Nombre_Recibido_Cliente,''),
		@par_CIUD_Recibido_Cliente,
		ISNULL(@par_Direccion_Recibido_Cliente,''),
		ISNULL(@par_Telefono_Recibido_Cliente,''),
		@par_Cumplir,
		@par_Reportar_Cliente,
		@par_Estado,
		@par_OFIC_Crea,
		@par_USUA_Codigo_Crea,
		GETDATE(),
		@par_Firma
	)

END
GO