﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 19/07/2021 
-- Módulo: Gesphone

PRINT 'gsp_modificar_estado_recolecciones'
GO
DROP PROCEDURE gsp_modificar_estado_recolecciones
GO
CREATE PROCEDURE dbo.gsp_modificar_estado_recolecciones      
(                      
  @par_EMPR_Codigo SMALLINT,
  @par_Numero INTEGER,
  @par_Estado INTEGER
)                      
AS                       
BEGIN
 
  UPDATE Encabezado_Recolecciones SET      
   CATA_EREP_Codigo = @par_Estado
  WHERE
    EMPR_Codigo = @par_EMPR_codigo                       
    AND Numero = @par_Numero                          
                  
 SELECT Numero, Numero_Documento FROM Encabezado_Recolecciones WHERE EMPR_Codigo = @par_EMPR_codigo AND Numero = @par_Numero   
                  
END    
GO