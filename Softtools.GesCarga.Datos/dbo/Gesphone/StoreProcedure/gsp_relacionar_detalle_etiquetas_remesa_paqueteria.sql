﻿-- Creado por: Anyelo Amado
-- Fecha Creación: 22/09/2021 
-- Módulo: Gesphone

PRINT 'gsp_relacionar_detalle_etiquetas_remesa_paqueteria'
GO
DROP PROCEDURE gsp_relacionar_detalle_etiquetas_remesa_paqueteria
GO
CREATE PROCEDURE dbo.gsp_relacionar_detalle_etiquetas_remesa_paqueteria      
(                      
  @par_EMPR_Codigo SMALLINT,
  @par_ENRE_Numero NUMERIC,
  @par_USUA_Modifica INTEGER,
  @par_IdEtiqueta NUMERIC
)                      
AS                       
BEGIN
  
    UPDATE Detalle_Asignacion_Etiquetas_Preimpresas
    SET ENRE_Numero = @par_ENRE_Numero, Fecha_Modifica = getdate(), USUA_Codigo_Modifica = @par_USUA_Modifica
    WHERE EMPR_Codigo = @par_EMPR_Codigo AND ID = @par_IdEtiqueta   
                  
END    
GO