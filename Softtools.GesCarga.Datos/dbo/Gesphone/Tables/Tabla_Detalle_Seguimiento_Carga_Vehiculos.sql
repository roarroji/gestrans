﻿PRINT 'Detalle_Seguimiento_Carga_Vehiculos'
GO
ALTER TABLE [dbo].[Detalle_Seguimiento_Carga_Vehiculos] DROP CONSTRAINT [FK_Detalle_Seguimiento_Carga_Vehiculos_Encabezado_Seguimiento_Vehiculos]
GO
ALTER TABLE [dbo].[Detalle_Seguimiento_Carga_Vehiculos] DROP CONSTRAINT [FK_Detalle_Seguimiento_Carga_Vehiculos_Valor_Catalogos]
GO
ALTER TABLE [dbo].[Detalle_Seguimiento_Carga_Vehiculos] DROP CONSTRAINT [FK_Detalle_Seguimiento_Carga_Vehiculos_Valor_Catalogos1]
GO
DROP TABLE [dbo].[Detalle_Seguimiento_Carga_Vehiculos]
GO
CREATE TABLE [dbo].[Detalle_Seguimiento_Carga_Vehiculos](
	[EMPR_Codigo] [smallint] NOT NULL,
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Numero_Documento] [numeric](18, 0) NOT NULL,
	[ENSV_Codigo] [numeric](18, 0) NOT NULL,
	[TIDO_Soporte] [numeric](18, 0) NOT NULL,
	[Numero_Documento_Soporte] [numeric](18, 0) NOT NULL,
	[Numero_Remesa] [numeric](18, 0) NOT NULL,
	[Fecha_Hora_Reporte] [datetime] NOT NULL,
	[CATA_TIRS_Codigo] [numeric](18, 0) NOT NULL,
	[TERC_Codigo_Cliente] [numeric](18, 0) NOT NULL,
	[CATA_NOSC_Codigo] [numeric](18, 0) NOT NULL,
	[Observaciones] [varchar](200) NOT NULL,
	[Identificacion_Recibido_Cliente] [varchar](15) NOT NULL,
	[Nombre_Recibido_Cliente] [varchar](50) NOT NULL,
	[CIUD_Recibido_Cliente] [numeric](18, 0) NOT NULL,
	[Direccion_Recibido_Cliente] [varchar](100) NULL,
	[Telefono_Recibido_Cliente] [varchar](20) NOT NULL,
	[Cumplir] [smallint] NULL,
	[Reportar_Cliente] [smallint] NULL,
	[Envio_Reporte_Cliente] [smallint] NULL,
	[Fecha_Envio_Reporte_Cliente] [datetime] NULL,
	[Longitud] [numeric](18, 10) NULL,
	[Latitud] [numeric](18, 10) NULL,
	[Estado] [smallint] NULL,
	[OFIC_Crea] [numeric](18, 0) NOT NULL,
	[USUA_Codigo_Crea] [numeric](18, 0) NOT NULL,
	[Fecha_Crea] [datetime] NOT NULL,
	[USUA_Codigo_Modifica] [numeric](18, 0) NULL,
	[Fecha_Modifica] [datetime] NULL,
	[Anulado] [smallint] NULL,
	[USUA_Codigo_Anula] [numeric](18, 0) NULL,
	[Fecha_Anula] [datetime] NULL,
	[Causa_Anula] [varchar](150) NULL,
	[Cantidad_Recibido_Cliente] [numeric](18, 0) NULL DEFAULT ((0)),
	[Peso_Recibido_Cliente] [numeric](18, 0) NULL DEFAULT ((0)),
	[Firma] [image] NULL,
 CONSTRAINT [PK_Detalle_Seguimiento_Carga_Vehiculos] PRIMARY KEY CLUSTERED 
(
	[EMPR_Codigo] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Detalle_Seguimiento_Carga_Vehiculos]  WITH NOCHECK ADD  CONSTRAINT [FK_Detalle_Seguimiento_Carga_Vehiculos_Encabezado_Seguimiento_Vehiculos] FOREIGN KEY([ENSV_Codigo])
REFERENCES [dbo].[Encabezado_Seguimiento_Vehiculos] (Codigo)
GO
ALTER TABLE [dbo].[Detalle_Seguimiento_Carga_Vehiculos] CHECK CONSTRAINT [FK_Detalle_Seguimiento_Carga_Vehiculos_Encabezado_Seguimiento_Vehiculos]
GO
ALTER TABLE [dbo].[Detalle_Seguimiento_Carga_Vehiculos]  WITH NOCHECK ADD  CONSTRAINT [FK_Detalle_Seguimiento_Carga_Vehiculos_Valor_Catalogos] FOREIGN KEY([CATA_TIRS_Codigo])
REFERENCES [dbo].[Valor_Catalogos] ([Codigo])
GO
ALTER TABLE [dbo].[Detalle_Seguimiento_Carga_Vehiculos] CHECK CONSTRAINT [FK_Detalle_Seguimiento_Carga_Vehiculos_Valor_Catalogos]
GO
ALTER TABLE [dbo].[Detalle_Seguimiento_Carga_Vehiculos]  WITH NOCHECK ADD  CONSTRAINT [FK_Detalle_Seguimiento_Carga_Vehiculos_Valor_Catalogos1] FOREIGN KEY([CATA_NOSC_Codigo])
REFERENCES [dbo].[Valor_Catalogos] ([Codigo])
GO
ALTER TABLE [dbo].[Detalle_Seguimiento_Carga_Vehiculos] CHECK CONSTRAINT [FK_Detalle_Seguimiento_Carga_Vehiculos_Valor_Catalogos1]
GO