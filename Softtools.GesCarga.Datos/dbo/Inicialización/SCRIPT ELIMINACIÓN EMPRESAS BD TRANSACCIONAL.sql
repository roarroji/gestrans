	-- Autor: JCB
	-- Fecha: 12-FEB-2021
	-- Observaciones: Este SCRIPT permite borrar la informaci�n de las empresas diferentes a la que se ingresa en el par�metro @EMPR_Codigo

	SELECT * FROM Empresas

	DECLARE @EMPR_Codigo SMALLINT = 13 --Se Ingresa el c�digo de la empresa a la cual no se le borrara la informaci�n

	-- Detalles Documentos
	DELETE Detalle_Legalizacion_Recaudo_Planillas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Planilla_Despachos_Retorna_Contado WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Conductores_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Impuestos_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Novedades_Despacho WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Legalizacion_Gastos_Conductor WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Planilla_Recolecciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Auxiliares_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Despachos_Legalizacion_Gastos_Conductor WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Tarifario_Carga_Ventas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Impuestos_Liquidacion_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Solicitud_Anticipos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Distribucion_Remesas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Combustible_Gastos_Conductor WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Legalizacion_Gastos_Varios_Conductores WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Impuestos_Liquidacion_Planilla_Proveedores WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Liquidacion_Planilla_Proveedores WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Planilla_Cargue_Descargue WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Detalle_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Conceptos_Solicitud_Orden_Servicios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Inspecciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Comprobante_Contables WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Secciones_Formularios_Inspecciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Items_Formularios_Inspecciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Seguimiento_Remesas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Seguimiento_Vehiculos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Manifiesto_Carga WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Solicitud_Orden_Servicios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Programacion_Orden_Servicios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Parametrizacion_Contables WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Impuestos_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Despacho_Orden_Servicios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Documento_Comprobantes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Despachos_Plan_Puntos_Vehiculos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Documento_Cuenta_Comprobantes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Legalizacion_Gastos_Conductor WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Integraciones_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Auxiliares_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Concepto_Contables WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Tiempos_Remesas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Impuestos_Conceptos_Liquidacion_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Novedades_Despacho WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Referencias_Estudio_Seguridad WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Impuestos_Conceptos_Facturas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Polizas_Solicitud_Orden_Servicios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Remesas_Facturas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Planilla_Recolecciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Tarifario_Carga_Ventas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Liquidacion_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Impuestos_Facturas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Asignacion_Precintos_Oficina WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Reexpedicion_Remesas_Paqueteria WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Conceptos_Facturas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Tiempos_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Entidades_Consultadas_Estudio_Seguridad WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Tarifario_Carga_Compras WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Orden_Compra_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Documento_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo

	-- Otras Tablas Detalle
	DELETE Cumplido_Remesas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Remesas_Paqueteria WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Planilla_Despachos_Proveedores WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Empresa_Factura_Electronica WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Factura_Electronica WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Estados_Factura_Electronica WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Remesa_Manifiesto_Carga WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Planillas_Despachos_Otras_Empresas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Novedades_Transito_Seguimiento_Vehicular WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Remesa_Gestion_Documentos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Importacion_Exportacion_Solicitud_Orden_Servicios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tarifas_Paqueteria_Ventas WHERE EMPR_Codigo  <> @EMPR_Codigo
	
	-- Encabezado Documentos
	DELETE Encabezado_Documento_Cuentas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Manifiesto_Carga WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Documento_Comprobantes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Orden_Cargues WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Liquidacion_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Facturas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Orden_Compra_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Planilla_Cargue_Descargue WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Encabezado_Liquidacion_Planilla_Proveedores WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Legalizacion_Gastos_Conductor WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Legalizacion_Recaudo_Planillas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Legalizacion_Recaudo_Planillas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Cumplido_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Tarifario_Carga_Ventas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Solicitud_Orden_Servicios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Comprobante_Contables WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Remesas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Solicitud_Anticipos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Documento_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Formularios_Inspecciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Inspecciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Cumplido_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Concepto_Contables WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Parametrizacion_Contables WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Recolecciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Notas_Facturas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Tarifario_Carga_Compras WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Asignacion_Precintos_Oficina WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Autorizaciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Estudio_Seguridad WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Programacion_Orden_Servicios WHERE EMPR_Codigo  <> @EMPR_Codigo
	
	-- Fidelizacion
	DELETE Detalle_Causales_Plan_Puntos_Vehiculos WHERE EMPR_Codigo  <> @EMPR_Codigo 
	DELETE Detalle_Despachos_Plan_Puntos_Vehiculos WHERE EMPR_Codigo  <> @EMPR_Codigo 
	DELETE Plan_Puntos_Vehiculos WHERE EMPR_Codigo  <> @EMPR_Codigo
	
	-- Mantenimiento
	DELETE Detalle_Tarea_Equipo_Mantenimientos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Equipo_Plan_Equipo_Mantenimientos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Equipo_Mantenimientos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Plan_Equipo_Mantenimientos WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Subsistema_Mantenimientos WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Sistema_Mantenimientos WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Linea_Mantenimientos WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Estado_Equipo_Mantenimientos WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Unidad_Frecuencia_Mantenimientos WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Marca_Equipo_Mantenimientos WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Tipo_Equipo_Mantenimientos WHERE EMPR_Codigo <> @EMPR_Codigo
		
	-- Almacen
	DELETE Referencia_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Unidad_Empaque_Referencia_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Almacen_Referencia_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo 
	DELETE Proveedor_Referencia_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Referencia_Almacen_Plan_Mantenimientos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Orden_Compra_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Documento_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Grupo_Referencia_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Almacenes WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Detalle_Ubicacion_Referencia_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Ubicacion_Referencia_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo
	
	-- Basico
	DELETE Impuestos_Tipo_Documentos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Impuestos_Conceptos_Liquidacion_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Cuenta_Bancaria_Oficinas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Departamento_Impuestos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Cuenta_Bancarias WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Bancos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Marca_Semirremolques WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Semirremolque_Novedades WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Semirremolques WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Enturnamiento_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Vehiculo_Propietarios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Vehiculo_Novedades WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Vehiculos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Lista_Negra_Vehiculos WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Sitios_Cargue_Descargue_Rutas WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Rendimiento_Galon_Combustible WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Peaje_Rutas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Rutas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Chequera_Cuenta_Bancarias WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Regiones_Paises WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tarifas_Paqueteria_Ventas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Plan_Unico_Cuentas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Cajas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Plantilla_Ruta_Legalizacion_Gastos_Conductor WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Ciudad_Impuestos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Seguimientos_Empresas_GPS WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Usuario_Oficinas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Oficinas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Ciudades WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Departamentos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Puertos_Paises WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Paises WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Sitios_Cargue_Descargue WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Unidad_Empaque_Producto_Transportados WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Novedades_Transito_Seguimiento_Vehicular WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Documento_Gestion_Documentos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tasa_Cambio_Monedas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Monedas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tarifa_Transporte_Carga WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Sitio_Reporte_Novedades_Seguimiento_Vehicular WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Entidad_Documento_Gestion_Documentos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Importacion_Exportacion_Solicitud_Orden_Servicios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Enturnamiento_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tipo_Linea_Negocio_Carga WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Linea_Negocio_Transporte_Carga WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Configuracion_Documento_Gestion_Documentos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tipo_Linea_Negocio_Carga WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Zona_Ciudades WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tipo_Tarifa_Transporte_Carga WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Validacion_Procesos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Conceptos_Notas_Facturas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Lista_Distribucion_Correos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tercero_Impuestos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Puesto_Controles WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Novedades_Conceptos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Cuenta_Bancarias WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Clientes_Lineas_Servicios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tipo_Documentos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Conceptos_Ventas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Productos_Ministerio_Transporte WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Consecutivo_Documento_Oficinas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Configuracion_Controles WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Detalle_Causales_Plan_Puntos_Vehiculos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Configuracion_Catalogos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Encabezado_Impuestos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Unidad_Medida_Referencia_Almacenes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Novedades_Despacho WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Conceptos_Legalizacion_Gastos_Conductor WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Evento_Correos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Historico_Bandeja_Salida_Correos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Cierre_Contable_Documentos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Codigo_DANE_Ciudades WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Ciudades_Oficina_Enturnamiento_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Impuestos_Conceptos_Ventas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tasa_Cambio_Monedas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Puesto_Control_Rutas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tipo_Linea_Tarifa_Transporte_Carga WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Conceptos_Liquidacion_Planilla_Despachos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Color_Vehiculos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Ciudades_Regiones_Paises WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Unidad_Medida_Producto_Transportados WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Producto_Transportados WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Marca_Vehiculos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Linea_Vehiculos WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Vehiculo_Propietarios WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE T_Documento_Vehiculos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Vehiculos WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Bandeja_Salida_Correos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Documentos_Interfaz_Contable_PSL WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Configuracion_Servidor_Correos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Bandeja_Notificaciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE T_Archivo WHERE EMPR_Codigo  <> @EMPR_Codigo
	
	-- Terceros
	DELETE Terceros_Sitos_Cargue_Descargue WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tercero_Proveedores WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tercero_Conductores WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tercero_Clientes_Gestion_Documentos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tercero_Clientes_Condiciones_Comerciales WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tercero_Novedades WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tercero_Empleados WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tercero_Clientes_Condiciones_Peso_Cumplido WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tercero_Listas_Correos WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Perfil_Terceros WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tercero_Clientes_Cupo_Sedes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tercero_Direcciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Tercero_Clientes WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Terceros WHERE EMPR_Codigo  <> @EMPR_Codigo

	-- Menu
	DELETE Menu_Aplicaciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Modulo_Aplicaciones WHERE EMPR_Codigo  <> @EMPR_Codigo
	
	-- Usuarios
	DELETE Grupo_Usuarios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Usuario_Grupo_Usuarios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Usuario_Oficinas WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Permiso_Grupo_Usuarios WHERE EMPR_Codigo  <> @EMPR_Codigo
	DELETE Usuarios WHERE EMPR_Codigo  <> @EMPR_Codigo
		
	-- Catalogos
	DELETE Valor_Catalogos WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Catalogos WHERE EMPR_Codigo <> @EMPR_Codigo

	DELETE Empresa_Manifiesto_Electronico WHERE EMPR_Codigo <> @EMPR_Codigo
	DELETE Empresas WHERE Codigo <> @EMPR_Codigo
		
	SELECT * FROM Empresas