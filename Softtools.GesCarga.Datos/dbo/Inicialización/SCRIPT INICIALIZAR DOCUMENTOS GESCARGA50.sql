DECLARE @Documentos TABLE  
( 
ENRE_Numero  NUMERIC(18,0) NULL ,  
ENRE_Numero_Documento  NUMERIC(18,0) NULL ,  
ENPD_Numero  NUMERIC(18,0) NULL ,  
ENDP_Numero_Documento  NUMERIC(18,0) NULL ,  
ESOS_Numero  NUMERIC(18,0) NULL ,  
ESOS_Numero_Documento  NUMERIC(18,0) NULL ,  
ENOC_Numero  NUMERIC(18,0) NULL ,  
ENOC_Numero_Documento  NUMERIC(18,0) NULL ,  
ENMA_Numero  NUMERIC(18,0) NULL ,  
ENMA_Numero_Documento  NUMERIC(18,0) NULL ,  
ELPD_Numero  NUMERIC(18,0) NULL ,  
ELPD_Numero_Documento  NUMERIC(18,0) NULL ,  
ENFA_Numero   NUMERIC(18,0) NULL ,  
ENFA_Numero_Documento  NUMERIC(18,0) NULL ,  
ECPD_Numero  NUMERIC(18,0) NULL ,  
ECPD_Numero_Documento  NUMERIC(18,0) NULL ,  
ENDC_Codigo_Factura  NUMERIC(18,0) NULL ,  
ENDC_Codigo_Liquidacion  NUMERIC(18,0) NULL ,  
ENDC_Codigo_Anticipo  NUMERIC(18,0) NULL ,  
ENDO_Codigo_Factura  NUMERIC(18,0) NULL ,  
ENDO_Codigo_Liquidacion  NUMERIC(18,0) NULL ,  
ENDO_Codigo_Anticipo  NUMERIC(18,0) NULL ,  
ENCC_Numero_Comprobantes NUMERIC(18,0) NULL ,  
ENCC_Numero_Liquidacion NUMERIC(18,0) NULL ,  
ENCC_Numero_Factura  NUMERIC(18,0) NULL

)  
INSERT INTO @Documentos
select ISNULL(ENRE.Numero,0) as ENRE_Numero,
ISNULL(ENRE.Numero_Documento,0) as ENRE_Numero_Documento,
ISNULL(DPDE.ENPD_Numero,0) as ENPD_Numero,
ISNULL(ENPD.Numero_Documento,0) AS ENDP_Numero_Documento,
ISNULL(DPOS.ESOS_Numero,0) AS ESOS_Numero,
ISNULL(ESOS.Numero_Documento,0) as ESOS_Numero_Documento,
ISNULL(ENOC.Numero,0) as ENOC_Numero,
ISNULL(ENOC.Numero_Documento,0) as ENOC_Numero_Documento,
ISNULL(ENMA.Numero,0) as ENMA_Numero,
ISNULL(ENMA.Numero_Documento,0) as ENMA_Numero_Documento,
ISNULL(ELPD.Numero,0) as ELPD_Numero,
ISNULL(ELPD.Numero_Documento,0) AS ELPD_Numero_Documento,
ISNULL(ENFA.Numero,0) as ENFA_Numero ,
ISNULL(ENFA.Numero_Documento,0) as ENFA_Numero_Documento,
ISNULL(ECPD.Numero,0) as ECPD_Numero,
ISNULL(ECPD.Numero_Documento,0) as ECPD_Numero_Documento,
ISNULL(ENDC.Codigo,0) as ENDC_Codigo_Factura,
ISNULL(ENDCL.Codigo,0) as ENDC_Codigo_Liquidacion,
ISNULL(ENDCA.Codigo,0) as ENDC_Codigo_Anticipo,
ISNULL(ENDCOF.Codigo,0) as ENDO_Codigo_Factura,
ISNULL(ENDCOL.Codigo,0) as ENDO_Codigo_Liquidacion,
ISNULL(ENDCOA.Codigo,0) as ENDO_Codigo_Anticipo,
ISNULL(ENCCC.Numero,0) as ENCC_Numero_Comprobantes,
ISNULL(ENCCL.Numero,0) as ENCC_Numero_Liquidacion,
ISNULL(ENCCF.Numero,0) as ENCC_Numero_Factura

from Encabezado_Remesas as ENRE
LEFT JOIN Detalle_Planilla_Despachos aS DPDE ON
ENRE.EMPR_Codigo = DPDE.EMPR_Codigo
AND ENRE.Numero = DPDE.ENRE_Numero

LEFT JOIN Encabezado_Planilla_Despachos AS ENPD ON 
DPDE.EMPR_Codigo = ENPD.EMPR_Codigo
and DPDE.ENPD_Numero = ENPD.Numero

LEFT JOIN Detalle_Despacho_Orden_Servicios AS DPOS ON 
ENRE.EMPR_Codigo = DPOS.EMPR_Codigo
AND ENRE.Numero = DPOS.ENRE_Numero

LEFT JOIN Encabezado_Solicitud_Orden_Servicios AS ESOS ON
DPOS.EMPR_Codigo = ESOS.EMPR_Codigo
and DPOS.ESOS_Numero = ESOS.Numero

LEFT JOIN Encabezado_Orden_Cargues AS ENOC ON
DPOS.EMPR_Codigo = ENOC.EMPR_Codigo
and DPOS.ENOC_Numero = ENOC.Numero

LEFT JOIN Encabezado_Manifiesto_Carga AS ENMA ON
ENPD.EMPR_Codigo = ENMA.EMPR_Codigo
and ENPD.Numero = ENMA.ENPD_Numero

LEFT JOIN Encabezado_Liquidacion_Planilla_Despachos AS ELPD ON
ENPD.EMPR_Codigo = ELPD.EMPR_Codigo
and ENPD.Numero = ELPD.ENPD_Numero

LEFT JOIN Detalle_Remesas_Facturas AS DERF ON
ENRE.EMPR_Codigo = DERF.EMPR_Codigo
and ENRE.Numero = DERF.ENRE_Numero

LEFT JOIN Encabezado_Facturas AS ENFA ON
DERF.EMPR_Codigo = ENFA.EMPR_Codigo
and DERF.ENFA_Numero = ENFA.Numero

LEFT JOIN Encabezado_Cumplido_Planilla_Despachos as ECPD ON
ENPD.EMPR_Codigo = ECPD.EMPR_Codigo
and ENPD.Numero = ECPD.ENPD_Numero

LEFT JOIN Encabezado_Documento_Cuentas as ENDC ON
ENFA.EMPR_Codigo = ENDC.EMPR_Codigo
and ENFA.Numero = ENDC.Codigo_Documento_Origen
and ENDC.CATA_DOOR_Codigo = 4601

LEFT JOIN Encabezado_Documento_Cuentas as ENDCL ON
ELPD.EMPR_Codigo = ENDCL.EMPR_Codigo
and ELPD.Numero = ENDCL.Codigo_Documento_Origen
and ENDCL.CATA_DOOR_Codigo = 4604

LEFT JOIN Encabezado_Documento_Cuentas as ENDCA ON
ENPD.EMPR_Codigo = ENDCA.EMPR_Codigo
and ENPD.Numero = ENDCA.Codigo_Documento_Origen
and ENDCA.CATA_DOOR_Codigo = 4604

LEFT JOIN Encabezado_Documento_Comprobantes as ENDCOF ON
ENFA.EMPR_Codigo = ENDCOF.EMPR_Codigo
and ENFA.Numero_Documento = ENDCOF.Documento_Origen
and ENDCOF.CATA_DOOR_Codigo = 4601

LEFT JOIN Encabezado_Documento_Comprobantes as ENDCOL ON
ELPD.EMPR_Codigo = ENDCOL.EMPR_Codigo
and ELPD.Numero_Documento = ENDCOL.Documento_Origen
and ENDCOL.CATA_DOOR_Codigo = 4604

LEFT JOIN Encabezado_Documento_Comprobantes as ENDCOA ON
ENPD.EMPR_Codigo = ENDCOA.EMPR_Codigo
and ENPD.Numero_Documento = ENDCOA.Documento_Origen
and ENDCOA.CATA_DOOR_Codigo = 4604

LEFT JOIN Encabezado_Comprobante_Contables as ENCCC ON
ENDCOF.EMPR_Codigo = ENCCC.EMPR_Codigo
and 
(
ENDCOF.Codigo = ENCCC.Codigo_Documento
or ENDCOL.Codigo = ENCCC.Codigo_Documento
or ENDCOA.Codigo = ENCCC.Codigo_Documento
)
and 
(ENCCC.TIDO_Documento = 30
or ENCCC.TIDO_Documento = 40
)


LEFT JOIN Encabezado_Comprobante_Contables as ENCCL ON
ELPD.EMPR_Codigo = ENCCL.EMPR_Codigo
and ELPD.Numero = ENCCL.Codigo_Documento
and ENCCL.TIDO_Documento = 160

LEFT JOIN Encabezado_Comprobante_Contables as ENCCF ON
ENFA.EMPR_Codigo = ENCCF.EMPR_Codigo
and ENFA.Numero = ENCCF.Codigo_Documento
and ENCCF.TIDO_Documento = 170


where 
ENRE.EMPR_Codigo  = 8
AND
ENRE.Numero_Documento in (
0
)


DELETE Detalle_Comprobante_Contables WHERE ENCC_Numero NOT IN (SELECT ENCC_Numero_Factura FROM @Documentos) AND  ENCC_Numero NOT IN (SELECT  ENCC_Numero_Liquidacion FROM @Documentos) AND  ENCC_Numero NOT IN (SELECT  ENCC_Numero_Comprobantes FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Encabezado_Comprobante_Contables WHERE Numero NOT IN (SELECT ENCC_Numero_Factura FROM @Documentos) AND  Numero NOT IN (SELECT  ENCC_Numero_Liquidacion FROM @Documentos) AND  Numero NOT IN (SELECT  ENCC_Numero_Comprobantes FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Documento_Cuenta_Comprobantes WHERE EDCO_Codigo NOT IN (SELECT ENDO_Codigo_Anticipo FROM @Documentos) AND  EDCO_Codigo NOT IN (SELECT  ENDO_Codigo_Liquidacion FROM @Documentos) AND  EDCO_Codigo NOT IN (SELECT  ENDO_Codigo_Factura FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Documento_Comprobantes WHERE EDCO_Codigo NOT IN (SELECT ENDO_Codigo_Anticipo FROM @Documentos) AND  EDCO_Codigo NOT IN (SELECT  ENDO_Codigo_Liquidacion FROM @Documentos) AND  EDCO_Codigo NOT IN (SELECT  ENDO_Codigo_Factura FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Encabezado_Documento_Comprobantes WHERE Codigo NOT IN (SELECT ENDO_Codigo_Anticipo FROM @Documentos) AND  Codigo NOT IN (SELECT  ENDO_Codigo_Liquidacion FROM @Documentos) AND  Codigo NOT IN (SELECT  ENDO_Codigo_Factura FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Encabezado_Documento_Cuentas WHERE Codigo NOT IN (SELECT ENDC_Codigo_Factura FROM @Documentos) AND  Codigo NOT IN (SELECT  ENDC_Codigo_Liquidacion FROM @Documentos) AND  Codigo NOT IN (SELECT  ENDC_Codigo_Anticipo FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Impuestos_Conceptos_Facturas WHERE ENFA_Numero NOT IN (SELECT ENFA_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Conceptos_Facturas WHERE ENFA_Numero NOT IN (SELECT ENFA_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Impuestos_Facturas WHERE ENFA_Numero NOT IN (SELECT ENFA_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Remesas_Facturas WHERE ENFA_Numero NOT IN (SELECT ENFA_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Factura_Electronica WHERE ENFA_Numero NOT IN (SELECT ENFA_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Encabezado_Facturas WHERE Numero NOT IN (SELECT ENFA_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Impuestos_Liquidacion_Planilla_Despachos WHERE ELPD_Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Liquidacion_Planilla_Despachos WHERE ELPD_Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Encabezado_Liquidacion_Planilla_Despachos WHERE Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Encabezado_Cumplido_Planilla_Despachos WHERE Numero NOT IN (SELECT ECPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Remesa_Manifiesto_Carga WHERE ENRE_Numero  NOT IN (SELECT ENRE_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Manifiesto_Carga WHERE ENMC_Numero NOT IN (SELECT ENMA_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Encabezado_Manifiesto_Carga WHERE Numero NOT IN (SELECT ENMA_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Auxiliares_Planilla_Despachos WHERE ENPD_Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Impuestos_Planilla_Despachos WHERE ENPD_Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Integraciones_Planilla_Despachos WHERE ENPD_Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Legalizacion_Gastos_Conductor WHERE ENPD_Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Novedades_Despacho WHERE ENPD_Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Seguimiento_Vehiculos WHERE ENPD_Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Planilla_Despachos WHERE ENPD_Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Planilla_Recolecciones WHERE ENPR_Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Novedades_Transito_Seguimiento_Vehicular WHERE ENPD_Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Conductores_Planilla_Despachos WHERE ENPD_Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Encabezado_Planilla_Despachos WHERE Numero NOT IN (SELECT ENPD_Numero FROM @Documentos) AND EMPR_Codigo  = 8 -- 1
DELETE Encabezado_Orden_Cargues WHERE Numero NOT IN (SELECT ENOC_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Conceptos_Solicitud_Orden_Servicios WHERE ESOS_Numero NOT IN (SELECT ESOS_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Despacho_Orden_Servicios WHERE ESOS_Numero NOT IN (SELECT ESOS_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Polizas_Solicitud_Orden_Servicios WHERE ESOS_Numero NOT IN (SELECT ESOS_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Despacho_Orden_Servicios WHERE ESOS_Numero NOT IN (SELECT ESOS_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Solicitud_Orden_Servicios WHERE ESOS_Numero NOT IN (SELECT ESOS_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Importacion_Exportacion_Solicitud_Orden_Servicios WHERE ESOS_Numero NOT IN (SELECT ESOS_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Encabezado_Solicitud_Orden_Servicios WHERE Numero NOT IN (SELECT ESOS_Numero FROM @Documentos) AND EMPR_Codigo  = 8 --2
DELETE Encabezado_Recolecciones WHERE EMPR_Codigo  = 8
DELETE Remesas_Paqueteria WHERE ENRE_Numero  NOT IN (SELECT ENRE_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Cumplido_Remesas WHERE ENRE_Numero  NOT IN (SELECT ENRE_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Detalle_Distribucion_Remesas WHERE ENRE_Numero  NOT IN (SELECT ENRE_Numero FROM @Documentos) AND EMPR_Codigo  = 8
--DELETE Detalle_Guias_Planilla_Recolecciones WHERE ENRE_Numero  NOT IN (SELECT ENRE_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Encabezado_Remesas WHERE Numero  NOT IN (SELECT ENRE_Numero FROM @Documentos) AND EMPR_Codigo  = 8
DELETE Bandeja_Salida_Correos WHERE EMPR_Codigo  = 8
DELETE Bandeja_Notificaciones WHERE EMPR_Codigo  = 8
DELETE Detalle_Entidades_Consultadas_Estudio_Seguridad WHERE EMPR_Codigo  = 8
DELETE Detalle_Referencias_Estudio_Seguridad WHERE EMPR_Codigo  = 8
DELETE Encabezado_Estudio_Seguridad WHERE EMPR_Codigo  = 8
DELETE Detalle_Despachos_Legalizacion_Gastos_Conductor  WHERE EMPR_Codigo  = 8
DELETE Detalle_Legalizacion_Gastos_Conductor  WHERE EMPR_Codigo  = 8

DELETE Detalle_Combustible_Gastos_Conductor WHERE EMPR_Codigo  = 8
DELETE Encabezado_Legalizacion_Gastos_Conductor  WHERE EMPR_Codigo  = 8
DELETE Encabezado_Autorizaciones  WHERE EMPR_Codigo  = 8
DELETE Detalle_Inspecciones  WHERE EMPR_Codigo  = 8
DELETE Encabezado_Inspecciones  WHERE EMPR_Codigo  = 8

DELETE Detalle_Programacion_Orden_Servicios WHERE EMPR_Codigo  = 8
DELETE Encabezado_Programacion_Orden_Servicios WHERE EMPR_Codigo  = 8
DELETE Enturnamiento_Despachos WHERE EMPR_Codigo  = 8
DELETE Detalle_Despachos_Plan_Puntos_Vehiculos WHERE EMPR_Codigo  = 8
DELETE Detalle_Causales_Plan_Puntos_Vehiculos WHERE EMPR_Codigo  = 8
--DELETE Plan_Puntos_Vehiculos WHERE EMPR_Codigo  = 8

--DELETE Vehiculos WHERE EMPR_Codigo  = 8
--DELETE Semirremolques WHERE EMPR_Codigo  = 8
GO

