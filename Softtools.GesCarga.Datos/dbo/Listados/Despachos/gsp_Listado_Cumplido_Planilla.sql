﻿Print 'gsp_Listado_Cumplido_Planilla'
GO
DROP PROCEDURE gsp_Listado_Cumplido_Planilla
GO
 CREATE PROCEDURE  gsp_Listado_Cumplido_Planilla (        
@par_EMPR_Codigo NUMERIC,            
@par_Numero_Documento_Inicial NUMERIC = null,            
@par_Numero_Documento_Final NUMERIC = null,            
@par_Fecha_Incial DATE = NULL,             
@par_Fecha_Final DATE = NULL,             
@par_Cliente VARCHAR(50) = NULL,             
@par_OFIC_Codigo NUMERIC = NULL ,            
@par_Estado NUMERIC = NULL ,            
@par_Ruta VARCHAR(50) = NULL)        
AS BEGIN      

	DECLARE @intAnulado SMALLINT = NULL
	SET @intAnulado = CASE @par_Estado WHEN NULL THEN NULL WHEN 2 THEN 1 ELSE 0 END
    SELECT @par_Estado = IIF (@par_Estado = 2,NULL,@par_Estado)
   
SELECT EMPR.Nombre_Razon_Social            
, ECPD.Numero_Documento Numero_Cumplido        
, ECPD.Fecha         
, ENPD.Numero_Documento Numero_Planilla         
, ENPD.Fecha InicioViaje        
, VEHI.Placa        
, ISNULL(TENE.Razon_Social,'')+' '+ISNULL(TENE.Nombre,'')              
+' '+ISNULL(TENE.Apellido1,'')+' '+ISNULL(TENE.Apellido2,'') AS NombreTenedor           
,TENE.Numero_Identificacion IdentificacionTenedor        
, ISNULL(COND.Razon_Social,'')+' '+ISNULL(COND.Nombre,'')              
+' '+ISNULL(COND.Apellido1,'')+' '+ISNULL(COND.Apellido2,'') AS NombreConductor            
, ISNULL(COND.Razon_Social,'')+' '+ISNULL(COND.Nombre,'')              
+' '+ISNULL(COND.Apellido1,'')+' '+ISNULL(COND.Apellido2,'') AS NombreEntrego           
, ECPD.Observaciones         
,        
CASE ECPD.Anulado WHEN 1 THEN 'A' ELSE         
  CASE ECPD.ESTADO WHEN 1 THEN 'D' ELSE 'B'        
    END         
END as Estado        
,OFIC.Nombre OFICINA         
,ISNULL(@par_Numero_Documento_Inicial,0) AS NumeroInicial            
,ISNULL(@par_Numero_Documento_Final,0) AS NumeroFinal            
,ISNULL(@par_Fecha_Incial,'01/01/0001') AS FechaInicial            
,ISNULL(@par_Fecha_Final,'01/01/0001') AS FechaFinal  
,ISNULL(RUTA.Nombre,'') As NombreRuta
FROM Encabezado_Cumplido_Planilla_Despachos ECPD        
 LEFT JOIN Empresas EMPR ON         
 ECPD.EMPR_Codigo = EMPR.Codigo         
        
LEFT JOIN Encabezado_Planilla_Despachos ENPD ON         
ECPD.EMPR_Codigo = ENPD.EMPR_Codigo         
AND ECPD.ENPD_Numero = ENPD.Numero         
        
 LEFT JOIN Vehiculos VEHI ON         
 ENPD.EMPR_Codigo = VEHI.EMPR_Codigo        
 AND  ENPD.VEHI_Codigo = VEHI.Codigo        
        
 LEFT JOIN Terceros TENE ON         
 ENPD.EMPR_Codigo = TENE.EMPR_Codigo         
 AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo        
         
 LEFT JOIN Terceros COND ON         
 ENPD.EMPR_Codigo = COND.EMPR_Codigo         
 AND ENPD.TERC_Codigo_Conductor = COND.Codigo        
        
 LEFT JOIN Rutas RUTA ON         
 ENPD.EMPR_Codigo = RUTA.EMPR_Codigo         
 AND ENPD.RUTA_Codigo = RUTA.Codigo         
        
 LEFT JOIN Oficinas OFIC ON         
 ECPD.EMPR_Codigo = OFIC.EMPR_Codigo         
 AND ECPD.OFIC_Codigo = OFIC.Codigo        
        
WHERE ECPD.TIDO_Codigo =155        
 AND ECPD.EMPR_Codigo = @par_EMPR_Codigo        
 AND ECPD.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ECPD.Numero_Documento)            
 AND ECPD.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ECPD.Numero_Documento)              
 AND ECPD.Fecha >= ISNULL( @par_Fecha_Incial,ECPD.Fecha)             
 AND ECPD.Fecha <= ISNULL( @par_Fecha_Final,ECPD.Fecha)             
 AND ECPD.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ECPD.OFIC_Codigo)
 AND (TENE.Nombre + ' ' +TENE.Apellido1 + ' ' + TENE.Apellido2 + ' ' + TENE.Razon_Social LIKE '%'+@par_Cliente+'%' or @par_Cliente is null)            
 AND ECPD.OFIC_Codigo = ISNULL(ECPD.OFIC_Codigo, @par_OFIC_Codigo)
 AND ECPD.Estado = ISNULL (@par_Estado,ECPD.Estado)            
 AND ECPD.Anulado = ISNULL(@intAnulado, ECPD.Anulado)
 AND (RUTA.Nombre LIKE '%'+@par_Ruta+'%' or @par_Ruta is null)      
END         
GO