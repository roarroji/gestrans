﻿Print 'gsp_Listado_Distriucion_Remesas'
GO
DROP PROCEDURE gsp_Listado_Distriucion_Remesas
GO
CREATE PROCEDURE gsp_Listado_Distriucion_Remesas     
(                  
@par_EMPR_Codigo NUMERIC,                  
@par_Numero_Documento_Inicial NUMERIC = null,                  
@par_Numero_Documento_Final NUMERIC = null,                  
@par_Fecha_Incial DATE = NULL,                   
@par_Fecha_Final DATE = NULL,                   
@par_Cliente VARCHAR(50) = NULL,                   
@par_OFIC_Codigo NUMERIC = NULL,
@par_Estado NUMERIC = NULL ,                  
@par_Ruta VARCHAR(50) = NULL     
)                  
AS BEGIN      
           
	DECLARE @intAnulado SMALLINT = NULL
	SET @intAnulado = CASE @par_Estado WHEN NULL THEN NULL WHEN 2 THEN 1 ELSE 0 END
    SELECT @par_Estado = IIF (@par_Estado = 2,NULL,@par_Estado)
	      
SELECT     
ENRE.Numero_Documento AS Numero,    
ENRE.Fecha,    
ENDP.Numero_Documento AS Planilla,    
ENRE.Documento_Cliente,    
ENRE.Cantidad_Cliente,    
ENRE.Peso_Cliente,    
CONCAT(CLIE.Razon_Social,CLIE.Nombre,' ',CLIE.Apellido1,' ',CLIE.Apellido2) AS Cliente,    
RUTA.Nombre AS Ruta,    
CIOR.Nombre AS Origen,    
CIDE.Nombre AS Destino,    
DDRE.Cantidad,    
DDRE.Peso,    
DDRE.Documento_Cliente AS Documento_Cliente_Distribucion,    
DDRE.Fecha_Documento_Cliente,    
DDRE.Numero_Identificacion_Destinatario,    
DDRE.Nombre_Destinatario,    
DDRE.Direccion_Destinatario,    
DDRE.Fecha_Recibe,    
DDRE.Numero_Identificacion_Recibe,    
DDRE.Nombre_Recibe,    
DDRE.Telefonos_Recibe,    
DDRE.Cantidad_Recibe,    
DDRE.Peso_Recibe,    
ISNULL(ENRE.OFIC_Codigo, '') AS Codigo_Oficina,    
ISNULL(OFIC.Nombre, '') AS Nombre_Oficina    
    
FROM     
    
Detalle_Distribucion_Remesas AS DDRE     
    
LEFT JOIN Encabezado_Remesas AS ENRE     
ON DDRE.EMPR_Codigo = ENRE.EMPR_Codigo    
AND DDRE.ENRE_Numero = ENRE.Numero    
    
LEFT JOIN Terceros AS CLIE     
ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo    
AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo    
    
LEFT JOIN Encabezado_Planilla_Despachos AS ENDP     
ON ENRE.EMPR_Codigo = ENDP.EMPR_Codigo    
AND ENRE.ENPD_Numero = ENDP.Numero    
    
LEFT JOIN Rutas AS RUTA     
ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo    
AND ENRE.RUTA_Codigo = RUTA.Codigo    
    
LEFT JOIN Ciudades AS CIOR     
ON ENRE.EMPR_Codigo = CIOR.EMPR_Codigo    
AND ENRE.CIUD_Codigo_Remitente = CIOR.Codigo    
    
LEFT JOIN Ciudades AS CIDE     
ON DDRE.EMPR_Codigo = CIDE.EMPR_Codigo    
AND DDRE.CIUD_Codigo_Destinatario = CIDE.Codigo    
    
LEFT JOIN Oficinas AS OFIC     
ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo    
AND ENRE.OFIC_Codigo = OFIC.Codigo    
    
WHERE     
            
 ENRE.TIDO_Codigo = 100                
 AND ENRE.Anulado = 0        
 AND ENRE.EMPR_Codigo =  @par_EMPR_Codigo               
 AND ENRE.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ENRE.Numero_Documento)                  
 AND ENRE.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ENRE.Numero_Documento)      
 AND ENRE.Fecha >= ISNULL( @par_Fecha_Incial,ENRE.Fecha)                   
 AND ENRE.Fecha <= ISNULL( @par_Fecha_Final,ENRE.Fecha)              
 AND (CLIE.Razon_Social + '' +  CLIE.Nombre + ' ' + CLIE.Apellido1 + ' ' + CLIE.Apellido2 LIKE '%'+@par_Cliente+'%' or @par_Cliente is null)       
 AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)
 AND ENRE.Anulado = ISNULL(@intAnulado, ENRE.Anulado)
 AND ENRE.Estado = ISNULL (@par_Estado,ENRE.Estado)                 
 AND (RUTA.Nombre LIKE '%'+@par_Ruta+'%' or @par_Ruta is null)        
    
ORDER BY ENRE.Numero                
    
END         
GO