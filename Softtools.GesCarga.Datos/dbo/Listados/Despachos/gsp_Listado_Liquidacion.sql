﻿Print 'gsp_Listado_Liquidacion'
GO
DROP PROCEDURE gsp_Listado_Liquidacion
GO              
CREATE PROCEDURE gsp_Listado_Liquidacion         
(
@par_EMPR_Codigo NUMERIC,            
@par_Numero_Documento_Inicial NUMERIC = null,            
@par_Numero_Documento_Final NUMERIC = null,            
@par_Fecha_Incial DATE = NULL,             
@par_Fecha_Final DATE = NULL,             
@par_Cliente VARCHAR(MAX) = NULL,             
@par_OFIC_Codigo NUMERIC = NULL ,            
@par_Estado NUMERIC = NULL ,            
@par_Ruta VARCHAR(50) = NULL
)        
AS BEGIN        

	DECLARE @intAnulado SMALLINT = NULL
	SET @intAnulado = CASE @par_Estado WHEN NULL THEN NULL WHEN 2 THEN 1 ELSE 0 END
    SELECT @par_Estado = IIF (@par_Estado = 2,NULL,@par_Estado)

SELECT DISTINCT        
EMPR.Nombre_Razon_Social        
,ELPD.Numero_Documento        
,ELPD.Fecha         
,ENPD.Numero_Documento NumeroPlanilla         
,ENPD.Fecha FechaPlanilla        
, VEHI.Placa        
, ISNULL(TENE.Razon_Social,'')+' '+ISNULL(TENE.Nombre,'')              
+' '+ISNULL(TENE.Apellido1,'')+' '+ISNULL(TENE.Apellido2,'') AS NombreTenedor           
, ISNULL(COND.Razon_Social,'')+' '+ISNULL(COND.Nombre,'')              
+' '+ISNULL(COND.Apellido1,'')+' '+ISNULL(COND.Apellido2,'') AS NombreConductor            
, ELPD.Valor_Flete_Transportador         
, ELPD.Valor_Conceptos_Liquidacion        
, ICA.Valor_Impuesto ICA        
, REFU.Valor_Impuesto ReteFuente        
, ELPD.Valor_Impuestos        
, ELPD.VALOR_PAGAR        
, ELPD.Valor_Flete_Transportador - ( ICA.Valor_Impuesto + REFU.Valor_Impuesto + ELPD.Valor_Conceptos_Liquidacion ) as ValorTotal         
, CASE ELPD.Anulado WHEN 1 THEN 'A' ELSE         
  CASE ELPD.ESTADO WHEN 1 THEN 'D' ELSE 'B'        
  END         
  END AS Estado        
  ,ISNULL(@par_Numero_Documento_Inicial,0) AS NumeroInicial            
,ISNULL(@par_Numero_Documento_Final,0) AS NumeroFinal            
,ISNULL(@par_Fecha_Incial,'01/01/0001') AS FechaInicial            
,ISNULL(@par_Fecha_Final,'01/01/0001') AS FechaFinal            
 ,OFIC.Nombre OFICINA         
 , '' AS TRANSPORTADOR        
 FROM Encabezado_Liquidacion_Planilla_Despachos ELPD        
        
 LEFT JOIN Empresas EMPR ON         
 ELPD.EMPR_Codigo = EMPR.Codigo         
        
LEFT JOIN  Encabezado_Planilla_Despachos ENPD ON          
 ELPD.EMPR_Codigo = ENPD.EMPR_Codigo         
 AND ELPD.ENPD_Numero = ENPD.Numero         
         
 LEFT join Detalle_Planilla_Despachos DEPD ON         
 ENPD.EMPR_Codigo = DEPD.EMPR_Codigo         
 AND ENPD.Numero = DEPD.ENPD_Numero        
        
 LEFT JOIN Vehiculos VEHI ON         
 ENPD.EMPR_Codigo = VEHI.EMPR_Codigo        
 AND  ENPD.VEHI_Codigo = VEHI.Codigo        
        
 LEFT JOIN Terceros TENE ON         
 ENPD.EMPR_Codigo = TENE.EMPR_Codigo         
 AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo        
        
  LEFT JOIN Terceros COND ON         
 ENPD.EMPR_Codigo = COND.EMPR_Codigo         
 AND ENPD.TERC_Codigo_Conductor = COND.Codigo        
        
 LEFT JOIN Detalle_Impuestos_Liquidacion_Planilla_Despachos ICA ON         
 ELPD.EMPR_Codigo = ICA.EMPR_Codigo         
 AND ELPD.Numero = ICA.ELPD_Numero        
 AND  ICA.ENIM_Codigo = 21        
 LEFT JOIN Detalle_Impuestos_Liquidacion_Planilla_Despachos REFU ON         
 ELPD.EMPR_Codigo = REFU.EMPR_Codigo         
 AND ELPD.Numero = REFU.ELPD_Numero        
  AND REFU.ENIM_Codigo = 40        
  LEFT JOIN Rutas RUTA ON         
 ENPD.EMPR_Codigo = RUTA.EMPR_Codigo         
 AND ENPD.RUTA_Codigo = RUTA.Codigo         
        
 LEFT JOIN Oficinas OFIC ON         
 ELPD.EMPR_Codigo = OFIC.EMPR_Codigo         
 AND ELPD.OFIC_Codigo = OFIC.Codigo        
        
 WHERE         
 ELPD.EMPR_Codigo = @par_EMPR_Codigo        
 and ELPD.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ELPD.Numero_Documento)            
 AND ELPD.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ELPD.Numero_Documento)              
 AND ELPD.Fecha >= ISNULL( @par_Fecha_Incial,ELPD.Fecha)             
 AND ELPD.Fecha <= ISNULL( @par_Fecha_Final,ELPD.Fecha)             
   
   AND (TENE.Nombre + ' ' +TENE.Apellido1 + ' ' + TENE.Apellido2 + ' ' + TENE.Razon_Social LIKE '%'+@par_Cliente+'%' or @par_Cliente is null)       
 AND ELPD.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ELPD.OFIC_Codigo)
 AND ELPD.Estado = ISNULL (@par_Estado,ELPD.Estado)            
 AND ELPD.Anulado = ISNULL(@intAnulado, ELPD.Anulado)
 AND (RUTA.Nombre LIKE '%'+@par_Ruta+'%' or @par_Ruta is null)      
        
 END         
GO