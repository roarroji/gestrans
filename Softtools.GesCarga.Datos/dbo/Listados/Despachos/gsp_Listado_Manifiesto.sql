﻿Print 'gsp_Listado_Manifiesto'  
GO
DROP PROCEDURE gsp_Listado_Manifiesto
GO
CREATE PROCEDURE gsp_Listado_Manifiesto         
(@par_EMPR_Codigo NUMERIC,            
@par_Numero_Documento_Inicial NUMERIC = null,            
@par_Numero_Documento_Final NUMERIC = null,            
@par_Fecha_Incial DATE = NULL,             
@par_Fecha_Final DATE = NULL,             
@par_Cliente VARCHAR(50) = NULL,             
@par_OFIC_Codigo NUMERIC = NULL ,            
@par_Estado NUMERIC = NULL ,            
@par_Ruta VARCHAR(20) = NULL )           
AS         
 BEGIN     
	
	DECLARE @intAnulado SMALLINT = NULL
	SET @intAnulado = CASE @par_Estado WHEN NULL THEN NULL WHEN 2 THEN 1 ELSE 0 END
    SELECT @par_Estado = IIF (@par_Estado = 2,NULL,@par_Estado)
	    
SELECT         
EMPR.Nombre_Razon_Social            
,ENMA.Numero_Documento        
,ENMA.Fecha         
,VEHI.Placa        
,TIVE.Campo1 TipoVehiculo        
, ISNULL(TENE.Razon_Social,'')+' '+ISNULL(TENE.Nombre,'')              
+' '+ISNULL(TENE.Apellido1,'')+' '+ISNULL(TENE.Apellido2,'') AS NombreTenedor           
,TENE.Numero_Identificacion IdentificacionTenedor        
, ISNULL(COND.Razon_Social,'')+' '+ISNULL(COND.Nombre,'')              
+' '+ISNULL(COND.Apellido1,'')+' '+ISNULL(COND.Apellido2,'') AS NombreConductor            
,COND.Numero_Identificacion IdentificacionConductor        
, ISNULL(AFIL.Razon_Social,'')+' '+ISNULL(AFIL.Nombre,'')              
+' '+ISNULL(AFIL.Apellido1,'')+' '+ISNULL(AFIL.Apellido2,'') AS NombreAfiliador          
,COND.Celulares         
,RUTA.Nombre ruta        
,ENMA.Valor_Flete        
,ENMA.Valor_ICA        
,ENMA.Valor_Retencion_Fuente         
,ENMA.Valor_Anticipo        
,ENMA.Valor_Pagar        
,OFIC.Nombre OFICINA         
,ISNULL(@par_Numero_Documento_Inicial,0) AS NumeroInicial            
,ISNULL(@par_Numero_Documento_Final,0) AS NumeroFinal            
,ISNULL(@par_Fecha_Incial,'01/01/0001') AS FechaInicial            
,ISNULL(@par_Fecha_Final,'01/01/0001') AS FechaFinal            
,CASE ENMA.Anulado WHEN 1 THEN 'A' ELSE         
  CASE ENMA.ESTADO WHEN 1 THEN 'D' ELSE 'B'        
    END         
END as Estado        
        
 FROM Encabezado_Manifiesto_Carga ENMA        
 INNER JOIN Empresas EMPR ON         
 ENMA.EMPR_Codigo = EMPR.Codigo         
        
 LEFT JOIN Vehiculos VEHI ON         
 ENMA.EMPR_Codigo = VEHI.EMPR_Codigo        
 AND  ENMA.VEHI_Codigo = VEHI.Codigo        
        
 LEFT JOIN Valor_Catalogos TIVE ON         
 VEHI.EMPR_Codigo = TIVE.EMPR_Codigo        
 AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo        
        
 LEFT JOIN Terceros TENE ON         
 ENMA.EMPR_Codigo = TENE.EMPR_Codigo         
 AND ENMA.TERC_Codigo_Tenedor = TENE.Codigo        
      
 LEFT JOIN Terceros CLIE ON        
 ENMA.EMPR_Codigo = CLIE.EMPR_Codigo         
 AND ENMA.TERC_Codigo_Afiliador = CLIE.Codigo        
  
 LEFT JOIN Terceros COND ON         
 ENMA.EMPR_Codigo = COND.EMPR_Codigo         
 AND ENMA.TERC_Codigo_Conductor = COND.Codigo        
        
  LEFT JOIN Terceros AFIL ON         
 ENMA.EMPR_Codigo = AFIL.EMPR_Codigo         
 AND ENMA.TERC_Codigo_Afiliador = AFIL.Codigo        
        
 LEFT JOIN Rutas RUTA ON         
 ENMA.EMPR_Codigo = RUTA.EMPR_Codigo         
 AND ENMA.RUTA_Codigo = RUTA.Codigo         
        
 LEFT JOIN Oficinas OFIC ON         
 ENMA.EMPR_Codigo = OFIC.EMPR_Codigo         
 AND ENMA.OFIC_Codigo = OFIC.Codigo        
        
        
 WHERE ENMA.EMPR_Codigo = @par_EMPR_Codigo        
  AND ENMA.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ENMA.Numero_Documento)            
 AND ENMA.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ENMA.Numero_Documento)              
 AND ENMA.Fecha >= ISNULL( @par_Fecha_Incial,ENMA.Fecha)             
 AND ENMA.Fecha <= ISNULL( @par_Fecha_Final,ENMA.Fecha)             
 AND (TENE.Nombre + ' ' +TENE.Apellido1 + ' ' + TENE.Apellido2 + ' ' + TENE.Razon_Social LIKE '%'+@par_Cliente+'%' or @par_Cliente is null)      
  AND ENMA.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENMA.OFIC_Codigo)
  AND ENMA.Anulado = ISNULL(@intAnulado, ENMA.Anulado)
 AND ENMA.Estado = ISNULL (@par_Estado,ENMA.Estado)            
 AND (RUTA.Nombre LIKE '%'+@par_Ruta+'%' or @par_Ruta is null)      
           
 END      
 
GO