﻿Print 'gsp_Listado_Novedades_Remesas'
GO
DROP PROCEDURE gsp_Listado_Novedades_Remesas
GO
CREATE PROCEDURE gsp_Listado_Novedades_Remesas     
(                  
@par_EMPR_Codigo NUMERIC,                  
@par_Numero_Documento_Inicial NUMERIC = null,                  
@par_Numero_Documento_Final NUMERIC = null,                  
@par_Fecha_Incial DATE = NULL,                   
@par_Fecha_Final DATE = NULL,                   
@par_Cliente VARCHAR(50) = NULL,                   
@par_OFIC_Codigo NUMERIC = NULL ,
@par_Estado NUMERIC = NULL ,                  
@par_Ruta VARCHAR(50) = NULL     
)                  
AS BEGIN      
           
	DECLARE @intAnulado SMALLINT = NULL
	SET @intAnulado = CASE @par_Estado WHEN NULL THEN NULL WHEN 2 THEN 1 ELSE 0 END
    SELECT @par_Estado = IIF (@par_Estado = 2,NULL,@par_Estado)
	      
SELECT     
ENRE.Fecha AS Fecha,      
ENRE.Numero_Documento as [Remesa],    
ESOS.Numero_Documento as [Orden_Servicio],    
ENPD.Numero_Documento AS [Planilla_Despacho],    
ISNULL(ENMC.Numero_Documento,0) AS [Manifiesto_Carga],    
CASE WHEN NODE.ID > 0 THEN 'SI' else 'NO' END,    
VEHI.Placa,    
TIVE.Nombre AS Tipo_Vehiculo,    
RUTA.Nombre AS Ruta,    
CONCAT(COND.Razon_Social,COND.Nombre,' ',COND.Apellido1,' ',COND.Apellido2) AS Conductor,    
COND.Numero_Identificacion AS [Identificacion_Conductor],    
ENPD.Valor_Anticipo,    
ISNULL(ENDC.Numero,0) AS Comprobante,    
CONCAT(TENE.Razon_Social,TENE.Nombre,' ',TENE.Apellido1,' ',TENE.Apellido2) AS Tenedor,    
ENPD.Valor_Flete_Transportador as [Total_Transportador],    
ENRE.Total_Flete_Cliente as [Total_Cliente],    
ISNULL(NOVE.Nombre,'') AS Novedad,    
ISNULL(NODE.[Valor_Compra],0) AS [Valor_Compra],    
ISNULL(NODE.[Valor_Venta],0) AS [Valor_Venta],    
ISNULL(NODE.[Valor_Costo],0) AS [Valor_Costo],    
CONCAT(CLIE.Razon_Social,CLIE.Nombre,' ',CLIE.Apellido1,' ',CLIE.Apellido2) AS Cliente,    
CONCAT(PROV.Razon_Social,PROV.Nombre,' ',PROV.Apellido1,' ',PROV.Apellido2) AS Proveedor,    
ISNULL(ENRE.OFIC_Codigo, '') AS Codigo_Oficina,    
ISNULL(OFIC.Nombre, '') AS Nombre_Oficina    
    
FROM    
    
Encabezado_Remesas AS ENRE     
    
LEFT JOIN Detalle_Novedades_Despacho AS NODE     
ON NODE.EMPR_Codigo = ENRE.EMPR_Codigo    
AND NODE.ENRE_Numero = ENRE.Numero    
AND NODE.Anulado = 0    
    
LEFT JOIN Encabezado_Solicitud_Orden_Servicios AS ESOS     
ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo    
and ENRE.ESOS_Numero = ESOS.Numero    
    
LEFT JOIN Encabezado_Planilla_Despachos AS ENPD     
ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo    
AND ENRE.ENPD_Numero = ENPD.Numero    
    
LEFT JOIN Encabezado_Manifiesto_Carga AS ENMC     
ON ENPD.EMPR_Codigo = ENMC.EMPR_Codigo    
AND ENPD.Numero = ENMC.ENPD_Numero    
    
LEFT JOIN Vehiculos AS VEHI     
ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo    
AND ENPD.VEHI_Codigo = VEHI.Codigo    
    
LEFT JOIN V_Tipo_vehiculos AS TIVE     
ON TIVE.EMPR_Codigo = VEHI.EMPR_Codigo    
AND TIVE.Codigo = VEHI.CATA_TIVE_Codigo    
    
LEFT JOIN Rutas AS RUTA     
ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo    
AND ENPD.RUTA_Codigo = RUTA.Codigo    
    
LEFT JOIN Terceros AS COND     
ON ENPD.EMPR_Codigo = COND.EMPR_Codigo    
AND ENPD.TERC_Codigo_Conductor = COND.Codigo    
    
LEFT JOIN Terceros AS TENE     
ON ENPD.EMPR_Codigo = TENE.EMPR_Codigo    
AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo    
    
LEFT JOIN Terceros as CLIE     
ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo    
AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo    
    
LEFT JOIN Terceros AS PROV     
ON NODE.EMPR_Codigo = PROV.EMPR_Codigo    
AND NODE.TERC_Codigo_Proveedor = PROV.Codigo    
    
LEFT JOIN Novedades_Despacho AS NOVE     
ON NODE.EMPR_Codigo = NOVE.EMPR_Codigo    
AND NODE.NODE_Codigo = NOVE.Codigo    
    
LEFT JOIN Oficinas AS OFIC     
ON ENRE.EMPR_Codigo = OFIC.EMPR_Codigo    
AND ENRE.OFIC_Codigo = OFIC.Codigo    
    
LEFT JOIN Encabezado_Documento_Comprobantes AS ENDC     
ON ENDC.EMPR_Codigo = ENPD.EMPR_Codigo    
AND ENDC.CATA_DOOR_Codigo = 2604    
AND ENDC.TIDO_Codigo = 30    
AND ENDC.Documento_Origen = ENPD.Numero_Documento    
    
WHERE     
            
 ENRE.TIDO_Codigo = 100                
 AND ENRE.Anulado = 0        
 AND ENRE.EMPR_Codigo =  @par_EMPR_Codigo               
 AND ENRE.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ENRE.Numero_Documento)                  
 AND ENRE.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ENRE.Numero_Documento)      
 AND ENRE.Fecha >= ISNULL( @par_Fecha_Incial,ENRE.Fecha)                   
 AND ENRE.Fecha <= ISNULL( @par_Fecha_Final,ENRE.Fecha)              
 AND (CLIE.Razon_Social + '' +  CLIE.Nombre + ' ' + CLIE.Apellido1 + ' ' + CLIE.Apellido2 LIKE '%'+@par_Cliente+'%' or @par_Cliente is null)       
 AND ENRE.OFIC_Codigo = ISNULL(@par_OFIC_Codigo, ENRE.OFIC_Codigo)
 AND ENRE.Anulado = ISNULL(@intAnulado, ENRE.Anulado)
 AND ENRE.Estado = ISNULL (@par_Estado,ENRE.Estado)                 
 AND (RUTA.Nombre LIKE '%'+@par_Ruta+'%' or @par_Ruta is null)        
    
ORDER BY ENRE.Numero                
    
END         
GO