﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


	DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 400210 --Permiso listado Remesas por Oficina
DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 400211 --Permiso listado Remesas por Cliente
GO

DELETE Menu_Aplicaciones WHERE Codigo = 400210 -- listado Remesas por Oficina
DELETE Menu_Aplicaciones WHERE Codigo = 400211 -- listado Remesas por Cliente
GO

INSERT INTO Menu_Aplicaciones
SELECT
	Codigo,400210,40,'Remesas por Oficina',4002,50,0,1,1,1,1,1,0,0,1,0,'#!listadoRemesasporOficina','#',null
FROM
	Empresas
GO

INSERT INTO Menu_Aplicaciones
SELECT
	Codigo,400211,40,'Remesas por Cliente',4002,60,0,1,1,1,1,1,0,0,1,0,'#!listadoRemesasporOficina','#',null
FROM
	Empresas
GO

INSERT INTO Permiso_Grupo_Usuarios

SELECT
	EMPR_Codigo,400210,Codigo,1,1,1,1,1,1,0
FROM
	Grupo_Usuarios
WHERE
	Codigo = 1 --Permiso listado Remesas por oficina
GO

INSERT INTO Permiso_Grupo_Usuarios
SELECT
	EMPR_Codigo,400210,Codigo,0,0,0,0,0,0,0
FROM
	Grupo_Usuarios
WHERE
	Codigo <> 1
 --Permiso listado Remesas por oficina
GO

INSERT INTO Permiso_Grupo_Usuarios

SELECT
	EMPR_Codigo,400211,Codigo,1,1,1,1,1,1,0
FROM
	Grupo_Usuarios
WHERE
	Codigo = 1 --Permiso listado Remesas por cliente
GO

INSERT INTO Permiso_Grupo_Usuarios
SELECT
	EMPR_Codigo,400211,Codigo,0,0,0,0,0,0,0
FROM
	Grupo_Usuarios
WHERE
	Codigo <> 1
 --Permiso listado Remesas por cliente
GO

PRINT 'gsp_Listado_Remesa'

DROP PROCEDURE gsp_Listado_Remesa
GO

CREATE PROCEDURE gsp_Listado_Remesa(
@par_EMPR_Codigo NUMERIC,
@par_Numero_Documento_Inicial NUMERIC = null,
@par_Numero_Documento_Final NUMERIC = null,
@par_Fecha_Incial DATE = NULL, 
@par_Fecha_Final DATE = NULL, 
@par_Cliente VARCHAR(510) = NULSL, 
@par_Codigo_Oficina NUMERIC = NULL ,
@par_Estado NUMERIC = NULL ,
@par_Ruta VARCHAR (100) = NULL )
AS BEGIN 
SELECT 
EMPR.Nombre_Razon_Social
,ENRE.Numero AS Remesa
, ENRE.FECHA 
, ENRE.ESOS_Numero AS OrdenServicio
, ENRE.Numero_Documento AS  Documento_Cliente
, ENFA.Numero_Documento AS NumeroFactura
, ISNULL(TECL.Razon_Social,'')+' '+ISNULL(TECL.Nombre,'')  
+' '+ISNULL(TECL.Apellido1,'')+' '+ISNULL(TECL.Apellido2,'') AS NombreCliente
, ETCV.Nombre AS TarifarioCompra
, RUTA.Nombre AS NombreRuta
, VEHI.Placa
, ENRE.Peso_Cliente
, ENRE.Cantidad_Cliente
, ENRE.Valor_Flete_Cliente 
,ENRE.Valor_Flete_Transportador  
,OFIC.Nombre AS OFICINA
,ISNULL(@par_Numero_Documento_Inicial,0) AS NumeroInicial
,ISNULL(@par_Numero_Documento_Final,0) AS NumeroFinal
,ISNULL(@par_Fecha_Incial,'01/01/0001') AS FechaInicial
,ISNULL(@par_Fecha_Final,'01/01/0001') AS FechaFinal
FROM Encabezado_Remesas   ENRE
 ,Encabezado_Facturas ENFA
, Terceros TECL
, Empresas EMPR
, Rutas RUTA 
, Vehiculos VEHI
, Encabezado_Tarifario_Carga_Ventas ETCV 
, Oficinas OFIC

WHERE
ENRE.TIDO_Codigo=100 

AND  ENRE.EMPR_Codigo = EMPR.Codigo 

AND ENRE.EMPR_Codigo = TECL.EMPR_Codigo
AND ENRE.TERC_Codigo_Cliente = TECL.Codigo

AND ENRE.EMPR_Codigo = RUTA.EMPR_Codigo
AND ENRE.RUTA_Codigo = RUTA.Codigo 

AND ENRE.EMPR_Codigo = VEHI.EMPR_Codigo
AND ENRE.VEHI_Codigo = VEHI.Codigo

AND ENRE.EMPR_Codigo = ETCV.EMPR_Codigo 
AND ENRE.ETCV_Numero = ETCV.Numero 


AND ENRE.EMPR_Codigo = OFIC.EMPR_Codigo
AND ENRE.OFIC_Codigo = OFIC.Codigo 
AND ENRE.EMPR_Codigo = ENFA.EMPR_Codigo
AND ENRE.ENFA_Numero = ENFA.Numero
AND ENRE.TIDO_Codigo=100

  AND ENRE.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ENRE.Numero_Documento)
	AND ENRE.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ENRE.Numero_Documento) 
	AND	ENRE.EMPR_Codigo =  @par_EMPR_Codigo 
	AND ENRE.Fecha >= ISNULL( @par_Fecha_Incial,ENRE.Fecha) 
	AND ENRE.Fecha <= ISNULL( @par_Fecha_Final,ENRE.Fecha) 
	AND(TECL.Nombre + ' ' +TECL.Apellido1 + ' ' + TECL.Apellido2 + ' ' + TECL.Razon_Social  
	LIKE CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Cliente,TECL.Nombre + ' ' +TECL.Apellido1 + ' ' + TECL.Apellido2 + ' ' + TECL.Razon_Social))), '%'))
	AND OFIC.Codigo = ISNULL(@par_Codigo_Oficina,OFIC.Codigo) 
	AND ENRE.Estado = ISNULL (@par_Estado,ENRE.Estado)
	AND RUTA.Nombre = ISNULL(@par_Ruta, RUTA.Nombre) 

END 
GO