﻿ DROP PROCEDURE gsp_Listado_Remesas_Generales_Por_Tipo        
 GO

CREATE PROCEDURE gsp_Listado_Remesas_Generales_Por_Tipo        
(        
 @par_EMPR_Codigo NUMERIC,                      
 @par_Numero_Documento_Inicial NUMERIC = null,                      
 @par_Numero_Documento_Final NUMERIC = null,                      
 @par_Fecha_Incial DATE = NULL,                       
 @par_Fecha_Final DATE = NULL,                       
 @par_Cliente VARCHAR(MAX) = NULL,                           
 @par_Codigo_Oficina NUMERIC = NULL ,        
 @par_Vehiculo NUMERIC = NULL ,        
 @par_Estado NUMERIC = NULL ,                          
 @par_Ruta VARCHAR(50) = NULL        
)                      
AS        
BEGIN         
        
 Declare @Estadolist NUMERIC = NULL;        
 Declare @Anuladolist NUMERIC = NULL;        
        
 IF (@par_Estado IS NOT NULL)        
 BEGIN        
  IF(@par_Estado IN (0,1))        
  BEGIN        
   SET @Estadolist = @par_Estado;        
  END        
  ELSE        
  BEGIN        
   SET @Anuladolist = 1;        
  END                     
 END        
                      
 SELECT                           
 ENRE.Numero AS NumeroRemesa,        
 ENRE.CATA_TIRE_Codigo AS CodigoTipoRemesa,        
 TIRE.Campo1 AS NombreTipoRemesa,        
 ENRE.Numero_Documento AS NumeroDocumentoRemesa,        
 ENRE.fecha,        
 ESOS.Numero_Documento AS NumeroDocumentoOrdenServicio,        
 ISNULL(ENFA.Numero_Documento, 0) AS NumeroDocumentoFactura,        
 ISNULL(ENMA.Numero_Documento, 0) AS NumeroDocumentoManifiesto,        
 ISNULL(TECL.Numero_Identificacion, '') AS DocumentoCliente,        
 ISNULL(TECL.Razon_Social,'')+' '+ISNULL(TECL.Nombre,'') +' '+ISNULL(TECL.Apellido1,'')+' '+ISNULL(TECL.Apellido2,'') AS NombreCliente,        
 ISNULL(TEND.Razon_Social,'')+' '+ISNULL(TEND.Nombre,'') +' '+ISNULL(TEND.Apellido1,'')+' '+ISNULL(TEND.Apellido2,'') AS NombreTenedor,        
 ISNULL(COND.Razon_Social,'')+' '+ISNULL(COND.Nombre,'') +' '+ISNULL(COND.Apellido1,'')+' '+ISNULL(COND.Apellido2,'') AS NombreConductor,        
 ISNULL(DCGC.Km_Ruta, 0) AS Km_Ruta,        
 ETCV.Nombre AS TarifarioCompra,        
 VEHI.Placa,        
 TIVE.Campo1 AS TipoVehiculo,        
 RUTA.Nombre AS RutaNombre,        
 ENRE.Peso_Cliente,        
 ENRE.Cantidad_Cliente,        
 ENRE.Valor_Manejo_Cliente,        
 ENRE.Valor_Flete_Cliente,        
 ENRE.Total_Flete_Cliente,  
 CASE WHEN (ENPD.Valor_Flete_Transportador IS NULL OR ENPD.Valor_Flete_Transportador = 0) THEN
 ENRE.Valor_Flete_Transportador
 ELSE ISNULL(ENPD.Valor_Flete_Transportador,0) END AS Valor_Flete_Transportador,  
 --ENRE.Valor_Flete_Transportador,        
 ENRE.Total_Flete_Transportador,        
 OFIC.Nombre AS OficinaNombre,        
 OFIC.Codigo AS OficinaCodigo,        
 ENRE.Observaciones,        
 CASE WHEN ENRE.Anulado = 1 THEN 'A'          
 WHEN ENRE.ESTADO = 1 THEN 'D' ELSE 'B'                       
 END AS Estado          
      
 ,CASE ENRE.Remesa_Cortesia       
 WHEN 1 THEN 'SI'       
 WHEN ISNULL(ENRE.Remesa_Cortesia,0) THEN 'NO'      
 ELSE 'NO'      
END AS Remesa_Cortesia,    
CASE   WHEN len(TERFA.Razon_Social) = 0 then TERFA.nombre + ' ' + TERFA.apellido1 + ' '+TERFA.Apellido2 ELSE TERFA.Razon_Social END AS Facturar_A    
                
 FROM Encabezado_Remesas ENRE       
   
 LEFT JOIN Encabezado_Planilla_Despachos ENPD  
 ON ENPD.EMPR_Codigo = ENRE.EMPR_Codigo   
 AND ENPD.Numero = ENRE.ENPD_Numero  
        
 LEFT JOIN Valor_Catalogos TIRE ON        
 ENRE.EMPR_Codigo = TIRE.EMPR_Codigo                      
 AND ENRE.CATA_TIRE_Codigo = TIRE.Codigo        
        
 LEFT JOIN Encabezado_Facturas ENFA ON          
 ENRE.EMPR_Codigo = ENFA.EMPR_Codigo                      
 AND ENRE.ENFA_Numero = ENFA.Numero                      
                    
 LEFT JOIN Terceros TECL   on                    
 ENRE.EMPR_Codigo = TECL.EMPR_Codigo                      
 AND ENRE.TERC_Codigo_Cliente = TECL.Codigo                     
        
 LEFT JOIN Rutas RUTA    on                    
 ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                      
 AND ENRE.RUTA_Codigo = RUTA.Codigo                       
                      
 LEFT JOIN Vehiculos VEHI   on                    
 ENRE.EMPR_Codigo = VEHI.EMPR_Codigo                      
 AND ENRE.VEHI_Codigo = VEHI.Codigo           
         
 LEFT JOIN Valor_Catalogos TIVE ON        
 VEHI.EMPR_Codigo = TIVE.EMPR_Codigo                      
 AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo        
        
 LEFT JOIN Terceros TEND ON                    
 VEHI.EMPR_Codigo = TEND.EMPR_Codigo                      
 AND VEHI.TERC_Codigo_Tenedor = TEND.Codigo        
        
 LEFT JOIN Terceros COND ON                    
 ENRE.EMPR_Codigo = COND.EMPR_Codigo                      
 AND ENRE.TERC_Codigo_Conductor = COND.Codigo        
                
 LEFT JOIN Encabezado_Tarifario_Carga_Ventas ETCV    on                    
 ENRE.EMPR_Codigo = ETCV.EMPR_Codigo                       
 AND ENRE.ETCV_Numero = ETCV.Numero                       
                
 LEFT JOIN Oficinas OFIC   on                    
 ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                      
 AND ENRE.OFIC_Codigo = OFIC.Codigo                       
                  
 LEFT JOIN Encabezado_Solicitud_Orden_Servicios ESOS                   
 ON ENRE.EMPR_Codigo = ESOS.EMPR_Codigo                   
 AND ENRE.ESOS_Numero = ESOS.Numero                  
                  
 LEFT JOIN Encabezado_Manifiesto_Carga ENMA ON                   
 ENRE.EMPR_Codigo = ENMA.EMPR_Codigo                  
 AND ENRE.ENMC_Numero = ENMA.Numero        
         
 LEFT JOIN Detalle_Combustible_Gastos_Conductor DCGC ON        
 ENRE.EMPR_Codigo = DCGC.EMPR_Codigo         
 AND ENRE.ENPD_Numero = DCGC.ENPD_Numero    
     
 LEFT JOIN Terceros TERFA ON     
 ENFA.EMPR_Codigo = TERFA.EMPR_Codigo    
 AND ENFA.TERC_Facturar = TERFA.Codigo    
        
 WHERE        
        
 ENRE.EMPR_Codigo = @par_EMPR_Codigo                      
 AND ENRE.TIDO_Codigo = 100                       
 AND ENRE.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ENRE.Numero_Documento)                      
 AND ENRE.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ENRE.Numero_Documento)                       
 AND ENRE.EMPR_Codigo =  @par_EMPR_Codigo                       
 AND ENRE.Fecha >= ISNULL( @par_Fecha_Incial,ENRE.Fecha)                       
 AND ENRE.Fecha <= ISNULL( @par_Fecha_Final,ENRE.Fecha)                       
 AND (TECL.Nombre + ' ' +TECL.Apellido1 + ' ' + TECL.Apellido2 + ' ' + TECL.Razon_Social LIKE '%'+@par_Cliente+'%' or @par_Cliente is null)                       
 AND ENRE.OFIC_Codigo = ISNULL(@par_Codigo_Oficina, ENRE.OFIC_Codigo)                      
 AND ENRE.Estado = ISNULL(@Estadolist,ENRE.Estado)        
 AND (ENRE.Anulado = @Anuladolist OR @Anuladolist IS NULL)                         
 AND (RUTA.Nombre LIKE '%'+@par_Ruta+'%' or @par_Ruta is null)        
 AND ENRE.VEHI_Codigo = ISNULL(@par_Vehiculo, ENRE.VEHI_Codigo)        
END   
GO