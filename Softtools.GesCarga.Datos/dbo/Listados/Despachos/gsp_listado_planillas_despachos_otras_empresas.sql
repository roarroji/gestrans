﻿DROP PROCEDURE gsp_listado_planillas_despachos_otras_empresas      
GO
CREATE PROCEDURE gsp_listado_planillas_despachos_otras_empresas    
(    
@par_EMPR_Codigo smallint,    
@par_Numero_Planilla numeric = NULL,    
@par_Fecha_Inicial Date = NULL,    
@par_Fecha_Final Date = NULL,    
@par_Estado smallint = NULL,    
@par_VEHI_Placa varchar(20) = NULL,    
@par_Nombre_Conductor varchar(40) = NULL    
)    
AS    
BEGIN    
SELECT    
PDOE.*,    
ISNULL(TERC.Nombre,'')+' '+ISNULL(TERC.Apellido1,'')+' '+ISNULL(TERC.Apellido2,'') As NombreConductor,    
VEHI.Placa As PlacaVehiculo,    
TECL.Razon_Social As NombreCliente,    
RUTA.Nombre As NombreRuta,    
SEMI.Placa As PlacaSemirremolque,    
PRTR.Nombre As NombreProducto,    
TATC.Nombre As NombreTarifa,    
EMPR.Nombre_Razon_Social As NombreEmpresa,    
TIVE.Campo1 As TipoVehiculo,    
TERC.Numero_Identificacion As DocumentoConductor,    
CASE PDOE.Anulado WHEN 1 THEN 'ANULADO' ELSE    
CASE PDOE.Estado WHEN 1 THEN 'DEFINITIVO' ELSE 'BORRADOR'    
END    
END AS EstadoPlanilla,  
CASE WHEN ENFA.Numero IS NULL THEN ''  
ELSE CONVERT(varchar(10),ENFA.Numero_Documento) END AS Facturado   
    
FROM Planillas_Despachos_Otras_Empresas PDOE   
  
LEFT JOIN Encabezado_Remesas ENRE  
ON ENRE.EMPR_Codigo = PDOE.EMPR_Codigo  
AND ENRE.Numero = PDOE.ENRE_Numero  
  
LEFT JOIN Detalle_Remesas_Facturas DERF  
ON DERF.EMPR_Codigo = ENRE.EMPR_Codigo  
AND DERF.ENRE_Numero = ENRE.Numero  
  
LEFT JOIN Encabezado_Facturas ENFA  
ON ENFA.EMPR_Codigo = DERF.EMPR_Codigo  
AND ENFA.Numero =DERF.ENFA_Numero  
    
LEFT JOIN Vehiculos VEHI ON    
PDOE.EMPR_Codigo = VEHI.EMPR_Codigo AND    
PDOE.VEHI_Codigo = VEHI.Codigo    
    
LEFT JOIN Terceros TECL ON    
PDOE.EMPR_Codigo = TECL.EMPR_Codigo AND    
PDOE.TERC_Codigo_Cliente = TECL.Codigo    
    
LEFT JOIN Terceros TERC ON    
PDOE.EMPR_Codigo = TERC.EMPR_Codigo AND    
PDOE.TERC_Codigo_Conductor = TERC.Codigo    
    
LEFT JOIN Rutas RUTA ON    
PDOE.EMPR_Codigo = RUTA.EMPR_Codigo AND    
PDOE.RUTA_Codigo = RUTA.Codigo    
    
LEFT JOIN Semirremolques SEMI ON    
PDOE.EMPR_Codigo = SEMI.EMPR_Codigo AND    
PDOE.SEMI_Codigo = SEMI.Codigo    
    
LEFT JOIN Producto_Transportados PRTR ON    
PDOE.EMPR_Codigo = PRTR.EMPR_Codigo AND    
PDOE.PRTR_Codigo = PRTR.Codigo    
    
LEFT JOIN Tarifa_Transporte_Carga TATC ON    
PDOE.EMPR_Codigo = TATC.EMPR_Codigo AND    
PDOE.TATC_Codigo = TATC.Codigo    
    
LEFT JOIN Empresas EMPR ON    
PDOE.EMPR_Codigo = EMPR.Codigo    
    
LEFT JOIN Valor_Catalogos TIVE ON    
PDOE.EMPR_Codigo = TIVE.EMPR_Codigo AND    
VEHI.CATA_TIVE_Codigo = TIVE.Codigo    
    
    
WHERE    
    
PDOE.EMPR_Codigo = @par_EMPR_Codigo    
AND PDOE.Numero_Planilla = ISNULL(@par_Numero_Planilla,PDOE.Numero_Planilla)    
AND PDOE.Fecha_Planilla >= ISNULL(@par_Fecha_Inicial,PDOE.Fecha_Planilla)    
AND PDOE.Fecha_Planilla <= ISNULL(@par_Fecha_Final,PDOE.Fecha_Planilla)    
AND ((VEHI.Placa LIKE '%'+@par_VEHI_Placa+'%') OR (@par_VEHI_Placa IS NULL))    
AND CONCAT(TERC.Nombre,' ',TERC.Apellido1,' ',TERC.Apellido2)LIKE '%'+ISNULL(@par_Nombre_Conductor,CONCAT(TERC.Nombre,' ',TERC.Apellido1,' ',TERC.Apellido2))+'%'    
AND PDOE.Estado = ISNULL(@par_Estado,PDOE.Estado)    
    
    
END    
GO