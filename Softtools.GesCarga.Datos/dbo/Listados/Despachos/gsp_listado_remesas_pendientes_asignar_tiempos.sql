﻿PRINT 'gsp_listado_remesas_pendientes_asignar_tiempos'
GO
DROP PROCEDURE gsp_listado_remesas_pendientes_asignar_tiempos
GO
CREATE PROCEDURE gsp_listado_remesas_pendientes_asignar_tiempos
(
	@par_EMPR_Codigo SMALLINT,
	@par_Fecha_Inicial DATE = NULL,
	@par_Fecha_Final DATE = NULL,
	@OFIC_Codigo SMALLINT = NULL
)
AS
BEGIN
	Declare @TiemposRemesas Int = 0;
	--Validacion cantidad tiempos para remesas
	SELECT @TiemposRemesas = Count(Codigo) FROM Valor_Catalogos 
	WHERE CATA_Codigo = 202 AND campo2 is not null and EMPR_Codigo = @par_EMPR_Codigo

	SELECT 
	EMPR.Nombre_Razon_Social,
	@par_Fecha_Inicial AS FechaInicial,
	@par_Fecha_Final AS FechaFinal,
	ENRE.Numero, 
	ENRE.Numero_Documento,
	ENRE.Fecha,
	ISNULL(CIOR.Nombre, '') AS CIUD_Origen_Nombre,
	ISNULL(CIDE.Nombre, '') AS CIUD_Destino_Nombre,
	OFIC.Nombre AS Oficina,
	USUA.Codigo_Usuario

	from Encabezado_Remesas as ENRE

	INNER JOIN Empresas EMPR ON
	ENRE.EMPR_Codigo = EMPR.Codigo

	LEFT JOIN Rutas RUTA ON
	ENRE.EMPR_Codigo = RUTA.EMPR_Codigo
	AND ENRE.RUTA_Codigo = RUTA.Codigo

	LEFT JOIN Ciudades CIOR ON
	RUTA.EMPR_Codigo = CIOR.EMPR_Codigo
	AND RUTA.CIUD_Codigo_Origen = CIOR.Codigo

	LEFT JOIN Ciudades CIDE ON
	RUTA.EMPR_Codigo = CIDE.EMPR_Codigo
	AND RUTA.CIUD_Codigo_Destino = CIDE.Codigo

	LEFT JOIN Oficinas OFIC ON
	ENRE.EMPR_Codigo = OFIC.EMPR_Codigo
	AND ENRE.OFIC_Codigo = OFIC.Codigo

	LEFT JOIN Usuarios USUA ON
	ENRE.USUA_Codigo_Crea = USUA.Codigo
	AND ENRE.USUA_Codigo_Crea = USUA.Codigo

	WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo
	AND
	(
		SELECT count(DETR.ENRE_Numero) from Detalle_Tiempos_Remesas DETR 
		where DETR.ENRE_Numero = ENRE.Numero
	) < @TiemposRemesas
	AND ENRE.Fecha >= ISNULL(@par_Fecha_Inicial, ENRE.Fecha)
	AND ENRE.Fecha <= ISNULL(@par_Fecha_Final, ENRE.Fecha)
	AND ENRE.OFIC_Codigo = ISNULL(@OFIC_Codigo, ENRE.OFIC_Codigo)
	AND ENRE.Estado = 1--Estado Definitivo

	ORDER by ENRE.Fecha ASC
END
GO