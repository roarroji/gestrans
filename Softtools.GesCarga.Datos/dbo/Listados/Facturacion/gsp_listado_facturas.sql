﻿PRINT 'gsp_listado_facturas'
GO
DROP PROCEDURE gsp_listado_facturas
GO         
CREATE PROCEDURE gsp_listado_facturas (
	@par_EMPR_Codigo NUMERIC,              
	@par_Numero_Documento_Inicial NUMERIC = null,              
	@par_Numero_Documento_Final NUMERIC = null,              
	@par_Fecha_Incial DATE = NULL,               
	@par_Fecha_Final DATE = NULL,               
	@par_Cliente NUMERIC = NULL,               
	@par_Codigo_Oficina VARCHAR(20) = NULL,              
	@par_Estado NUMERIC = NULL,
	@par_Anulado NUMERIC = NULL
)
AS 
BEGIN
	IF(@par_Anulado IS NULL AND @par_Estado IS NOT NULL)
	BEGIN
		SET @par_Anulado = 0
	END

	SELECT         
	EMPR.Nombre_Razon_Social          
	,ENFA.Numero_Documento        
	,ENFA.Fecha         
	,ENFA.Observaciones        
	,OFIC.Nombre Oficina        
	, ISNULL(CLIE.Razon_Social,'')+' '+ISNULL(CLIE.Nombre,'')                
	+' '+ISNULL(CLIE.Apellido1,'')+' '+ISNULL(CLIE.Apellido2,'') AS NombreCliente           
	, ISNULL(FACA.Razon_Social,'')+' '+ISNULL(FACA.Nombre,'')                
	+' '+ISNULL(FACA.Apellido1,'')+' '+ISNULL(FACA.Apellido2,'') AS NombreFacturar           
	,enfa.Observaciones         
	,ENFA.Valor_Remesas        
	,ENFA.Valor_Otros_Conceptos        
	,ENFA.Valor_Factura        
	,ENFA.Valor_Impuestos        
	,ENFA.Valor_Descuentos        
	,CASE ENFA.Anulado WHEN 1 THEN 'A' ELSE            
	CASE ENFA.ESTADO WHEN 1 THEN 'D' ELSE 'B'          
	END    
	END as Estado
	,ISNULL(@par_Numero_Documento_Inicial,0) AS NumeroInicial              
	,ISNULL(@par_Numero_Documento_Final,0) AS NumeroFinal              
	,ISNULL(@par_Fecha_Incial,'01/01/0001') AS FechaInicial              
	,ISNULL(@par_Fecha_Final,'01/01/0001') AS FechaFinal            
	FROM Encabezado_Facturas ENFA        
          
	LEFT JOIN Empresas EMPR   on            
	ENFA.EMPR_Codigo = EMPR.Codigo            
        
	LEFT JOIN Terceros CLIE         
	ON ENFA.EMPR_Codigo = CLIE.EMPR_Codigo         
	AND ENFA.TERC_Cliente = CLIE.Codigo         
        
	LEFT JOIN Terceros FACA         
	ON ENFA.EMPR_Codigo = FACA.EMPR_Codigo         
	AND ENFA.TERC_Cliente = FACA.Codigo         
        
	LEFT JOIN Oficinas OFIC         
	ON ENFA.EMPR_Codigo= OFIC.EMPR_Codigo        
	AND ENFA.OFIC_Factura = OFIC.Codigo         
        
	WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo
	AND ENFA.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ENFA.Numero_Documento)              
	AND ENFA.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ENFA.Numero_Documento)                
	AND ENFA.Fecha >= ISNULL( @par_Fecha_Incial,ENFA.Fecha)               
	AND ENFA.Fecha <= ISNULL( @par_Fecha_Final,ENFA.Fecha)               
	AND (OFIC.Nombre LIKE '%'+@par_Codigo_Oficina+'%' or @par_Codigo_Oficina is null)
	AND (ENFA.Estado  = @par_Estado OR @par_Estado IS NULL)  
	AND (ENFA.Anulado  = @par_Anulado OR @par_Anulado IS NULL)                     
END
GO