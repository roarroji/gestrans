﻿PRINT 'gsp_Listado_combustible_Chip'
GO
DROP PROCEDURE gsp_Listado_combustible_Chip
GO
CREATE PROCEDURE gsp_Listado_combustible_Chip
(        
	@par_EMPR_Codigo smallint,        
	@par_Fecha_Inicial DATE = NULL,        
	@par_Fecha_Final DATE = NULL,        
	@par_Numero_Documento NUMERIC = NULL,        
	@par_TERC_Codigo_Conductor NUMERIC = NULL,        
	@par_VEHI_Codigo NUMERIC = NULL,        
	@par_OFIC_Codigo NUMERIC = NULL,        
	@par_Estado NUMERIC = NULL,    
	@par_Anulado NUMERIC = NULL    
)        
AS     
BEGIN  
	IF(@par_Anulado IS NULL AND @par_Estado IS NOT NULL)  
	BEGIN  
	SET @par_Anulado = 0  
	END  
  
	SELECT    
	ELGC.EMPR_Codigo,    
	ELGC.Numero,    
	ELGC.Numero_Documento,     
	ELGC.Fecha,    
	DLGC.Numero_Factura,    
	DLGC.Fecha_Factura,    
	ISNULL(TERC.Razon_Social,'') + ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1,'') + ' ' + ISNULL(TERC.Apellido2,'') as NombreConductor,    
	VEHI.Placa,    
	DLGC.Valor,    
	DLGC.Valor_Galones,    
	DLGC.Cantidad_Galones,    
	DLGC.Nombre_Estacion,    
	OFIC.Nombre as NombreOficina,    
	CASE ELGC.Anulado WHEN 1 THEN 'A' ELSE  
	CASE ELGC.Estado WHEN 1 THEN 'D' ELSE 'B'   
	END   
	END AS Estado  
    
	FROM Encabezado_Legalizacion_Gastos_Conductor ELGC    
    
	INNER JOIN Detalle_Legalizacion_Gastos_Conductor DLGC ON    
	DLGC.EMPR_Codigo = ELGC.EMPR_Codigo    
	AND DLGC.ELGC_Numero = ELGC.Numero    
    
	INNER JOIN Conceptos_Legalizacion_Gastos_Conductor CLGC ON    
	CLGC.EMPR_Codigo = DLGC.EMPR_Codigo    
	AND CLGC.Codigo = DLGC.CLGC_Codigo    
    
	INNER JOIN Encabezado_Planilla_Despachos ENPD ON    
	ENPD.EMPR_Codigo = DLGC.EMPR_Codigo    
	AND ENPD.Numero = DLGC.ENPD_Numero    
    
	INNER JOIN Vehiculos VEHI ON    
	VEHI.EMPR_Codigo = ENPD.EMPR_Codigo    
	AND VEHI.Codigo = ENPD.VEHI_Codigo    
    
	LEFT JOIN Terceros TERC ON     
	TERC.EMPR_Codigo = ELGC.EMPR_Codigo    
	AND TERC.Codigo = ELGC.TERC_Codigo_Conductor    
    
	INNER JOIN Oficinas OFIC ON     
	OFIC.EMPR_Codigo = ELGC.EMPR_Codigo    
	AND OFIC.Codigo = ELGC.OFIC_Codigo    
    
	WHERE     
	ELGC.EMPR_Codigo = @par_EMPR_Codigo    
	AND (ELGC.Fecha >= @par_Fecha_Inicial or @par_Fecha_Inicial IS NULL)        
	AND (ELGC.Fecha <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)    
	AND (ELGC.Numero_Documento = @par_Numero_Documento OR @par_Numero_Documento IS NULL)    
	AND (ELGC.TERC_Codigo_Conductor = @par_TERC_Codigo_Conductor OR @par_TERC_Codigo_Conductor IS NULL)    
	AND (VEHI.Codigo  = @par_VEHI_Codigo OR @par_VEHI_Codigo IS NULL)  
	AND (ELGC.OFIC_Codigo  = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)   
	AND (ELGC.Estado  = @par_Estado OR @par_Estado IS NULL)    
	AND (ELGC.Anulado  = @par_Anulado OR @par_Anulado IS NULL)    
	AND (DLGC.CLGC_Codigo = 43 OR CLGC.Nombre like '%COMBUSTIBLE%')    
	AND DLGC.Tiene_Chip = 1    
	ORDER BY 1, 2    
END
GO