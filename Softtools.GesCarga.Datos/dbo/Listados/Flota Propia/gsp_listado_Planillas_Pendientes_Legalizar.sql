﻿PRINT 'gsp_listado_Planillas_Pendientes_Legalizar'
GO
DROP PROCEDURE gsp_listado_Planillas_Pendientes_Legalizar
GO
CREATE PROCEDURE gsp_listado_Planillas_Pendientes_Legalizar
(        
	@par_EMPR_Codigo smallint,        
	@par_Fecha_Inicial DATE = NULL,        
	@par_Fecha_Final DATE = NULL,        
	@par_Numero_Documento NUMERIC = NULL,        
	@par_TERC_Codigo_Conductor NUMERIC = NULL,        
	@par_VEHI_Codigo NUMERIC = NULL,        
	@par_OFIC_Codigo NUMERIC = NULL,        
	@par_Estado NUMERIC = NULL,  
	@par_Anulado NUMERIC = NULL  
)        
AS     
BEGIN  
	IF(@par_Anulado IS NULL AND @par_Estado IS NOT NULL)  
	BEGIN  
	SET @par_Anulado = 0  
	END  
  
	SELECT DISTINCT     
	ENPD.EMPR_Codigo,    
	ENPD.Numero_Documento as Planilla,     
	ISNULL(CONVERT(VARCHAR(10),ENMC.Numero_Documento),'') AS Manifiesto,     
	VEHI.Placa,     
	ISNULL(TERC.Razon_Social,'') + ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1,'') + ' ' + ISNULL(TERC.Apellido2,'') as NombreConductor,    
	RUTA.Nombre AS RutaNombre,     
	ENPD.Fecha,     
	OFIC.Nombre AS NombreOficina,    
	CASE WHEN DLGC.ENPD_Numero IS NULL THEN 'PENDIENTE'    
	ELSE 'LEGALIZADO' END AS Estado_Legalizacion,    
	CASE ENPD.anulado WHEN 1 THEN 'A' ELSE  
	CASE ENPD.Estado WHEN 1 THEN 'D' ELSE 'B'   
	END  
	END AS Estado_Planilla  
    
	FROM Encabezado_Planilla_Despachos ENPD    
    
	LEFT JOIN Detalle_Legalizacion_Gastos_Conductor DLGC ON    
	DLGC.ENPD_Numero = ENPD.Numero    
    
	INNER JOIN Vehiculos VEHI ON     
	VEHI.EMPR_Codigo = ENPD.EMPR_Codigo     
	AND VEHI.Codigo = ENPD.VEHI_Codigo     
    
	LEFT JOIN Encabezado_Manifiesto_Carga ENMC ON    
	ENMC.EMPR_Codigo = ENPD.EMPR_Codigo    
	AND ENMC.ENPD_Numero = ENPD.Numero    
    
	LEFT JOIN Terceros TERC ON     
	TERC.EMPR_Codigo = ENPD.EMPR_Codigo    
	AND TERC.Codigo = ENPD.TERC_Codigo_Conductor    
    
	LEFT JOIN Rutas RUTA ON     
	RUTA.EMPR_Codigo = ENPD.EMPR_Codigo     
	AND RUTA.Codigo = ENPD.RUTA_Codigo     
    
	LEFT JOIN Oficinas OFIC ON     
	ENPD.EMPR_Codigo = OFIC.EMPR_Codigo     
	AND ENPD.OFIC_Codigo = OFIC.Codigo     
    
	WHERE     
	ENPD.EMPR_Codigo = @par_EMPR_Codigo    
	AND (ENPD.Fecha >= @par_Fecha_Inicial or @par_Fecha_Inicial IS NULL)        
	AND (ENPD.Fecha <= @par_Fecha_Final OR @par_Fecha_Final IS NULL)    
	AND (ENPD.Numero_Documento = @par_Numero_Documento OR @par_Numero_Documento IS NULL)    
	AND (ENPD.TERC_Codigo_Conductor = @par_TERC_Codigo_Conductor OR @par_TERC_Codigo_Conductor IS NULL)    
	AND (VEHI.Codigo  = @par_VEHI_Codigo OR @par_VEHI_Codigo IS NULL)    
	AND (ENPD.OFIC_Codigo  = @par_OFIC_Codigo OR @par_OFIC_Codigo IS NULL)    
	AND (ENPD.Estado  = @par_Estado OR @par_Estado IS NULL)    
	AND (ENPD.Anulado  = @par_Anulado OR @par_Anulado IS NULL)    
	AND DLGC.ENPD_Numero IS NULL  
	AND VEHI.CATA_TIDV_Codigo = 2102  
	ORDER BY ENPD.Fecha DESC, 1 DESC    
END  
GO