﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */
	
USE GESCARGA50_DESARROLLO
GO
PRINT 'ELIMAR PERMISOS Y EL MENU'

DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 4002 --Permiso listado 
DELETE Menu_Aplicaciones WHERE Codigo = 4002 --listado 

INSERT INTO Menu_Aplicaciones
SELECT
	Codigo,4002,40,'Listados',40,20,1,1,1,1,1,1,1,0,0,0,'#!ConsultarListadoGuiaPaqueterias','#!',NULL
FROM
	Empresas
GO

INSERT INTO Permiso_Grupo_Usuarios

SELECT
	EMPR_Codigo,4002,Codigo,1,1,1,1,1,1,0
FROM
	Grupo_Usuarios
WHERE
	Codigo = 1 --Permiso listado Guias Paqueterias por oficina
GO


INSERT INTO Permiso_Grupo_Usuarios
SELECT
	EMPR_Codigo,4002,Codigo,0,0,0,0,0,0,0
FROM
	Grupo_Usuarios
WHERE
	Codigo <> 1
 --Permiso listado Guias Paqueterias por oficina
GO


DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 400207 --Permiso listado Guias Paqueterias por cliente 
DELETE Menu_Aplicaciones WHERE Codigo = 400207 --Permiso listado Guias Paqueterias por cliente 
GO

INSERT INTO Menu_Aplicaciones
SELECT
	Codigo,400207,40,'Guias por cliente',4002,30,0,1,1,1,1,1,0,0,1,0,'#!listadoGuiasporCliente','#',null
FROM
	Empresas
GO

INSERT INTO Permiso_Grupo_Usuarios
SELECT
	EMPR_Codigo,400207,Codigo,1,1,1,1,1,1,0
FROM
	Grupo_Usuarios
WHERE
	Codigo = 1 --Permiso listado Guias Paqueterias por cliente 
GO

INSERT INTO Permiso_Grupo_Usuarios
SELECT
	EMPR_Codigo,400207,Codigo,0,0,0,0,0,0,0
FROM
	Grupo_Usuarios
WHERE
	Codigo <> 1
 --Permiso listado Guias Paqueterias por cliente 
GO


DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 400206 --Permiso listado Guias Paqueterias por oficina
DELETE Menu_Aplicaciones WHERE Codigo = 400206 --listado Guias Paqueterias por oficina 
GO


INSERT INTO Menu_Aplicaciones
SELECT
	Codigo,400206,40,'Guias por Oficina',4002,20,0,1,1,1,1,1,0,0,1,0,'#!listadoGuiasporOficina','#',null
FROM
	Empresas
GO

INSERT INTO Permiso_Grupo_Usuarios

SELECT
	EMPR_Codigo,400206,Codigo,1,1,1,1,1,1,0
FROM
	Grupo_Usuarios
WHERE
	Codigo = 1 --Permiso listado Guias Paqueterias por oficina
GO

INSERT INTO Permiso_Grupo_Usuarios
SELECT
	EMPR_Codigo,400206,Codigo,0,0,0,0,0,0,0
FROM
	Grupo_Usuarios
WHERE
	Codigo <> 1
 --Permiso listado Guias Paqueterias por oficina
GO

PRINT 'gsp_listado_guias  '

DROP PROCEDURE gsp_listado_guias
GO

CREATE PROCEDURE gsp_listado_guias (
@par_EMPR_Codigo NUMERIC,
@par_Numero_Documento_Inicial NUMERIC = null,
@par_Numero_Documento_Final NUMERIC = null,
@par_Fecha_Incial DATE = NULL, 
@par_Fecha_Final DATE = NULL, 
@par_Cliente VARCHAR(150) = NULL, 
@par_Remitente VARCHAR(150) = NULL, 
@par_Destinario VARCHAR(150) = NULL, 
@par_Codigo_Oficina NUMERIC = NULL ,
@par_Estado NUMERIC = NULL ,
@par_Ruta VARCHAR (100) = NULL 
)
AS BEGIN 
SELECT 
EMPR.Nombre_Razon_Social,
ENRE.Numero Guia, 
ENRE.Numero_Documento N_Alterno,
ENRE.ETCV_Numero Contr_transportador,
ENRE.Fecha,
RUTA.Nombre RUTA,
ENRE.Cantidad_Cliente, 
ENRE.Peso_Cliente,
ENRE.Valor_Flete_Cliente,
ENRE.Valor_Flete_Transportador,
ISNULL(CLIE.Razon_Social,'')+' '+ISNULL(CLIE.Nombre,'')  
+' '+ISNULL(CLIE.Apellido1,'')+' '+ISNULL(CLIE.Apellido2,'') Cliente,
OFIC.Nombre OFICINA
,ISNULL(@par_Numero_Documento_Inicial,0) AS NumeroInicial
,ISNULL(@par_Numero_Documento_Final,0) AS NumeroFinal
,ISNULL(@par_Fecha_Incial,'01/01/0001') AS FechaInicial
,ISNULL(@par_Fecha_Final,'01/01/0001') AS FechaFinal


 FROM Encabezado_Remesas ENRE
 ,Empresas EMPR
 ,Rutas RUTA
 ,Oficinas OFIC
 ,Terceros CLIE
 ,Terceros REMI
 ,Terceros DEST

 WHERE 
     ENRE.EMPR_Codigo = EMPR.Codigo 
  	AND  ENRE.EMPR_Codigo = RUTA.EMPR_Codigo 
	 AND ENRE.RUTA_Codigo = RUTA.Codigo 
	AND  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo 
	 AND ENRE.OFIC_Codigo = OFIC.Codigo 
	 AND ENRE.EMPR_Codigo = CLIE.EMPR_Codigo 
	AND  ENRE.TERC_Codigo_Cliente = CLIE.Codigo 
	AND  ENRE.EMPR_Codigo = REMI.EMPR_Codigo 
	AND  ENRE.TERC_Codigo_Remitente = REMI.Codigo 
    AND  ENRE.EMPR_Codigo = DEST.EMPR_Codigo 
	AND  ENRE.TERC_Codigo_Destinatario = DEST.Codigo 
	 AND ENRE.TIDO_Codigo	= 110

	 AND ENRE.Numero_Documento >= ISNULL( @par_Numero_Documento_Inicial,ENRE.Numero_Documento)
	AND ENRE.Numero_Documento <= ISNULL( @par_Numero_Documento_Final,ENRE.Numero_Documento) 
	AND	 ENRE.EMPR_Codigo =  @par_EMPR_Codigo 
	AND ENRE.Fecha >= ISNULL( @par_Fecha_Incial,ENRE.Fecha) 
	AND ENRE.Fecha <= ISNULL( @par_Fecha_Final,ENRE.Fecha) 
	AND CLIE.Numero_Identificacion = ISNULL(@par_Cliente, CLIE.Numero_Identificacion) 
		AND(CLIE.Nombre + ' ' +CLIE.Apellido1 + ' ' + CLIE.Apellido2 + ' ' + CLIE.Razon_Social  
	LIKE CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Cliente,CLIE.Nombre + ' ' +CLIE.Apellido1 + ' ' + CLIE.Apellido2 + ' ' + CLIE.Razon_Social))), '%'))
	AND(REMI.Nombre + ' ' +REMI.Apellido1 + ' ' + REMI.Apellido2 + ' ' + REMI.Razon_Social  
	LIKE CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Remitente,REMI.Nombre + ' ' +REMI.Apellido1 + ' ' + REMI.Apellido2 + ' ' + REMI.Razon_Social))), '%'))
		AND(DEST.Nombre + ' ' +DEST.Apellido1 + ' ' + DEST.Apellido2 + ' ' + DEST.Razon_Social  
	LIKE CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Destinario,CLIE.Nombre + ' ' +DEST.Apellido1 + ' ' + DEST.Apellido2 + ' ' + DEST.Razon_Social))), '%'))
AND OFIC.Codigo = ISNULL(@par_Codigo_Oficina,OFIC.Codigo) 
	AND ENRE.Estado = ISNULL (@par_Estado,ENRE.Estado)
	AND RUTA.Nombre = ISNULL(@par_Ruta, RUTA.Nombre) 


END 
GO




