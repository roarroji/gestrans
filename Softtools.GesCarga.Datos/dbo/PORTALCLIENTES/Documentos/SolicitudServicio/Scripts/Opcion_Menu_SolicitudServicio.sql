﻿

GO
PRINT 'Opción Menú Portal-Documentos-SolicitudServicio'
GO
DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 2400101		
GO
DELETE Menu_Aplicaciones WHERE Codigo = 2400101
GO
INSERT INTO Menu_Aplicaciones( 
EMPR_Codigo,
Codigo,
MOAP_Codigo,
Nombre,
Padre_Menu,
Orden_Menu,
Mostrar_Menu,
Aplica_Habilitar,
Aplica_Consultar,
Aplica_Actualizar,
Aplica_Eliminar_Anular,
Aplica_Imprimir,
Aplica_Contabilidad,
Opcion_Permiso,
Opcion_Listado,
Aplica_Ayuda,
Url_Pagina,
Url_Ayuda) 
VALUES(
1
,2100101
,210
,'Solicitud Servicio'
,21001
,10
,1
,1
,1
,1
,1
,1
,1
,0
,0
,0
,'#!ConsultarSolicitudServicio'
,'#!'
)
GO
INSERT INTO Permiso_Grupo_Usuarios(
EMPR_Codigo,
MEAP_Codigo,
GRUS_Codigo,
Habilitar,
Consultar,
Actualizar,
Eliminar_Anular,
Imprimir,
Permiso,
Contabilidad
) 
VALUES (
1,
2100101,
1,
1,
1,
1,
1,
1,
0,
0
) 



