﻿PRINT 'gsp_consultar_lista_planilla_guias'
GO
DROP PROCEDURE gsp_consultar_lista_planilla_guias
GO
CREATE PROCEDURE gsp_consultar_lista_planilla_guias 
(@par_EMPR_Codigo SMALLINT,  
@par_ENPR_Codigo NUMERIC,
@par_NumeroPagina INT = 1,         
@par_RegistrosPagina INT = 10    )  
  
  AS        
BEGIN      
      
  SET NOCOUNT ON;        
  DECLARE @CantidadRegistros INT        
             
  BEGIN        
   SELECT @CantidadRegistros = (        
    SELECT DISTINCT         
    COUNT(1)         
    FROM         
  
     Detalle_Guias_Planilla_Recolecciones AS DGPR  
	 
	 LEFT JOIN Encabezado_Remesas AS ENRE                     
     ON DGPR.EMPR_Codigo = ENRE.EMPR_Codigo                      
     AND DGPR.ENRE_Numero = ENRE.Numero   
	 
	 LEFT JOIN Terceros AS TERC                     
     ON ENRE.EMPR_Codigo = TERC.EMPR_Codigo                      
     AND ENRE.TERC_Codigo_Cliente = TERC.Codigo     
	      
  WHERE   
DGPR.EMPR_Codigo = @par_EMPR_Codigo  
AND DGPR.ENPR_Numero = @par_ENPR_Codigo     
   );               
  WITH Pagina AS        
  (        
  SELECT DISTINCT   
  DGPR.EMPR_Codigo
  ,DGPR.ENPR_Numero
  ,DGPR.ENRE_Numero  
  ,ENRE.Numero_Documento        
  ,ENRE.Fecha         
  ,ISNULL(ENRE.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente   
  ,ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1, '')+ ' ' + ISNULL(TERC.Apellido2,'')+ ' ' + ISNULL(TERC.Razon_Social,'') AS Nombre_Cliente
  ,ISNULL(ENRE.Peso_Cliente,0) AS Peso_Cliente   
  ,ISNULL(ENRE.Cantidad_Cliente,0) AS Cantidad_Cliente  
  ,ISNULL(ENRE.Observaciones,0) AS Observaciones  
  ,ENRE.Estado
  ,ROW_NUMBER() OVER(ORDER BY ENRE.Numero) AS RowNumber        
               
  FROM         
  
     Detalle_Guias_Planilla_Recolecciones AS DGPR  
	 
	 LEFT JOIN Encabezado_Remesas AS ENRE                     
     ON DGPR.EMPR_Codigo = ENRE.EMPR_Codigo                      
     AND DGPR.ENRE_Numero = ENRE.Numero    
	 
	 LEFT JOIN Terceros AS TERC                     
     ON ENRE.EMPR_Codigo = TERC.EMPR_Codigo                      
     AND ENRE.TERC_Codigo_Cliente = TERC.Codigo    

	 LEFT JOIN Rutas AS RUTA                     
     ON ENRE.EMPR_Codigo = RUTA.EMPR_Codigo                      
     AND ENRE.RUTA_Codigo = RUTA.Codigo  
     
  WHERE   
DGPR.EMPR_Codigo = @par_EMPR_Codigo  
AND DGPR.ENPR_Numero = @par_ENPR_Codigo    
    )        
        
   SELECT 
   EMPR_Codigo
   ,ENPR_Numero
   ,ENRE_Numero       
   ,Numero_Documento        
   ,Fecha   
   ,TERC_Codigo_Cliente         
   ,Nombre_Cliente 
   ,Peso_Cliente  
   ,Cantidad_Cliente  
   ,Estado  
   ,Observaciones       
   ,@CantidadRegistros AS TotalRegistros        
   ,@par_NumeroPagina AS PaginaObtener        
   ,@par_RegistrosPagina AS RegistrosPagina        
   FROM        
    Pagina        
   WHERE        
    RowNumber > (ISNULL(@par_NumeroPagina,1) -1) * ISNULL(@par_RegistrosPagina,10000)        
    AND RowNumber <=ISNULL(@par_NumeroPagina,1) *ISNULL( @par_RegistrosPagina,10000)        
   ORDER BY ENRE_Numero ASC        
  END  

END  
GO