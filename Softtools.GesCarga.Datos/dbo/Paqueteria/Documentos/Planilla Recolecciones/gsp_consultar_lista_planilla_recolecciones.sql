﻿PRINT 'gsp_consultar_lista_planilla_recolecciones'
GO
DROP PROCEDURE gsp_consultar_lista_planilla_recolecciones
GO
CREATE PROCEDURE gsp_consultar_lista_planilla_recolecciones  
(@par_EMPR_Codigo SMALLINT,  
@par_ENPR_Codigo NUMERIC,
@par_NumeroPagina INT = 1,         
@par_RegistrosPagina INT = 10    )  
  
  AS        
BEGIN      
      
  SET NOCOUNT ON;        
  DECLARE @CantidadRegistros INT        
             
  BEGIN        
   SELECT @CantidadRegistros = (        
    SELECT DISTINCT         
    COUNT(1)         
    FROM         
  
     Detalle_Planilla_Recolecciones DPRE  
	 
	 LEFT JOIN Encabezado_Recolecciones AS EREC                     
     ON DPRE.EMPR_Codigo = EREC.EMPR_Codigo                      
     AND DPRE.EREC_Numero = EREC.Numero           
    
  LEFT JOIN Zona_Ciudades AS ZOCI                     
     ON EREC.EMPR_Codigo = ZOCI.EMPR_Codigo                      
     AND EREC.ZOCI_Codigo = ZOCI.Codigo    
    
  LEFT JOIN Oficinas AS OFIC                     
     ON EREC.EMPR_Codigo = OFIC.EMPR_Codigo                      
     AND EREC.OFIC_Codigo = OFIC.Codigo      
    
  LEFT JOIN Ciudades AS CIUD                     
     ON EREC.EMPR_Codigo = CIUD.EMPR_Codigo                      
     AND EREC.CIUD_Codigo = CIUD.Codigo    
  
  LEFT JOIN Terceros AS TERC                     
     ON EREC.EMPR_Codigo = TERC.EMPR_Codigo                      
     AND EREC.TERC_Codigo_Cliente = TERC.Codigo    
  
  LEFT JOIN Unidad_Empaque_Producto_Transportados AS UEPT                     
     ON EREC.EMPR_Codigo = UEPT.EMPR_Codigo                      
     AND EREC.UEPT_Codigo = UEPT.Codigo     
     
  WHERE   
DPRE.EMPR_Codigo = @par_EMPR_Codigo  
AND DPRE.ENPR_Numero = @par_ENPR_Codigo     
   );               
  WITH Pagina AS        
  (        
  SELECT DISTINCT   
  DPRE.EMPR_Codigo
  ,DPRE.ENPR_Numero
  ,DPRE.EREC_Numero  
  ,EREC.Numero_Documento        
  ,EREC.Fecha         
  ,ISNULL(EREC.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente   
  ,ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1, '')+ ' ' + ISNULL(TERC.Apellido2,'')+ ' ' + ISNULL(TERC.Razon_Social,'') AS Nombre_Cliente            ,ISNULL(EREC.Nombre_Contacto,'') AS Nombre_Contacto       
  ,ISNULL(ZOCI.Codigo, 0) AS Codigo_Zona      
  ,ISNULL(ZOCI.Nombre,'') AS Nombre_Zona     
  ,ISNULL(OFIC.CIUD_Codigo, 0) AS Codigo_Oficina  
  ,ISNULL(OFIC.Nombre,'') AS Nombre_Oficina  
  ,ISNULL(CIUD.Codigo, 0) AS Codigo_Ciudad  
  ,ISNULL(CIUD.Nombre,'') AS Nombre_Ciudad  
  ,ISNULL(EREC.Barrio, '') AS Barrio        
  ,ISNULL(EREC.Direccion, '') AS Direccion        
  ,ISNULL(EREC.Telefonos, 0) AS Telefonos    
  ,ISNULL(EREC.Mercancia, '') AS Mercancia      
  ,ISNULL(UEPT.Codigo, 0) AS UEPT_Codigo     
  ,ISNULL(UEPT.Descripcion, '') AS Unidad_Empaque        
  ,ISNULL(EREC.Cantidad, 0) AS Cantidad   
  ,ISNULL(EREC.Peso, 0) AS Peso        
  ,ISNULL(EREC.Observaciones, '') AS Observaciones
  ,CASE WHEN EREC.Estado = 1 AND EREC.Anulado = 0 AND EREC.ENPR_Numero = 0  THEN 1   
  WHEN EREC.ENPR_Numero > 0 THEN 2   
  WHEN EREC.Anulado = 1 THEN 3 END AS Estado               
  ,ROW_NUMBER() OVER(ORDER BY EREC.Numero) AS RowNumber        
               
  FROM         
  
      Detalle_Planilla_Recolecciones DPRE  
	 
	 LEFT JOIN Encabezado_Recolecciones AS EREC                     
     ON DPRE.EMPR_Codigo = EREC.EMPR_Codigo                      
     AND DPRE.EREC_Numero = EREC.Numero           
    
  LEFT JOIN Zona_Ciudades AS ZOCI                     
     ON EREC.EMPR_Codigo = ZOCI.EMPR_Codigo                      
     AND EREC.ZOCI_Codigo = ZOCI.Codigo    
    
  LEFT JOIN Oficinas AS OFIC                     
     ON EREC.EMPR_Codigo = OFIC.EMPR_Codigo                      
     AND EREC.OFIC_Codigo = OFIC.Codigo      
    
  LEFT JOIN Ciudades AS CIUD                     
     ON EREC.EMPR_Codigo = CIUD.EMPR_Codigo                      
     AND EREC.CIUD_Codigo = CIUD.Codigo    
  
  LEFT JOIN Terceros AS TERC                     
     ON EREC.EMPR_Codigo = TERC.EMPR_Codigo                      
     AND EREC.TERC_Codigo_Cliente = TERC.Codigo    
  
  LEFT JOIN Unidad_Empaque_Producto_Transportados AS UEPT                     
     ON EREC.EMPR_Codigo = UEPT.EMPR_Codigo                      
     AND EREC.UEPT_Codigo = UEPT.Codigo     
  
   WHERE   
DPRE.EMPR_Codigo = @par_EMPR_Codigo  
AND DPRE.ENPR_Numero = @par_ENPR_Codigo    
    )        
        
   SELECT 
   EMPR_Codigo
   ,ENPR_Numero
   ,EREC_Numero      
   ,Numero_Documento        
   ,Fecha   
   ,TERC_Codigo_Cliente         
   ,Nombre_Cliente        
   ,Nombre_Contacto   
   ,Codigo_Zona     
   ,Nombre_Zona   
   ,Codigo_Oficina   
   ,Nombre_Oficina  
   ,Codigo_Ciudad  
   ,Nombre_Ciudad  
   ,Barrio        
   ,Direccion        
   ,Telefonos    
   ,Mercancia      
   ,UEPT_Codigo    
   ,Unidad_Empaque        
   ,Cantidad   
   ,Peso        
   ,Observaciones
   ,Estado           
   ,@CantidadRegistros AS TotalRegistros        
   ,@par_NumeroPagina AS PaginaObtener        
   ,@par_RegistrosPagina AS RegistrosPagina        
   FROM        
    Pagina        
   WHERE        
    RowNumber > (ISNULL(@par_NumeroPagina,1) -1) * ISNULL(@par_RegistrosPagina,10000)        
    AND RowNumber <=ISNULL(@par_NumeroPagina,1) *ISNULL( @par_RegistrosPagina,10000)        
   ORDER BY EREC_Numero ASC        
  END  

END  
GO