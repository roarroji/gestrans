﻿DROP PROCEDURE gsp_consultar_remesas_pendientes_planillar_paqueteria
GO
CREATE PROCEDURE gsp_consultar_remesas_pendientes_planillar_paqueteria    
(                                        
 @par_EMPR_Codigo SMALLINT,    
 @par_NumeroInicial numeric = NULL,    
 @par_FechaInicial Date = NULL,    
 @par_FechaFinal Date = NULL,    
 @par_Documento_Cliente varchar(30) = NULL,                                        
 @par_NombreCliente varchar (150) = NULL,                                        
 @par_TERC_Codigo_Cliente numeric = NULL,    
 @par_Ciudad_Origen varchar (50) = NULL,                                        
 @par_Ciudad_Destino varchar (50) = NULL,    
 @par_NombreDestinatario varchar (150) = NULL,    
 @par_NombreRemitente varchar(150) = NULL,    
 @par_ENPD_Numero numeric = NULL,    
 @par_Anulado numeric= NULL,    
 @par_Estado numeric = NULL,    
 @par_TIDO_Codigo INT = NULL,    
 @par_NumeroPagina INT = NULL,                            
 @par_RegistrosPagina INT = NULL,                            
 @par_OFIC_Codigo_Actual_Usuario INT = NULL,    
 @par_Ciudad_Destino_Codigo INT = NULL,    
 @par_Reexpedicion INT = NULL    
)                                        
AS                                                 
BEGIN                                        
 IF @par_FechaInicial <> NULL BEGIN                                        
 SET @par_FechaInicial = CONVERT(DATE, @par_FechaInicial, 101)                                        
 END                                        
                                        
 IF @par_FechaFinal <> NULL BEGIN                                        
 SET @par_FechaFinal = CONVERT(DATE, @par_FechaFinal, 101)                                        
 END                                        
                                        
 SET NOCOUNT ON;                                              
 DECLARE @CantidadRegistros INT                                        
 SELECT                                        
 @CantidadRegistros = (SELECT DISTINCT                     
  COUNT(1)                                        
  FROM Remesas_Paqueteria ENRP                                        
                                        
  INNER JOIN Encabezado_Remesas AS ENRE                                  
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                        
  AND ENRP.ENRE_Numero = ENRE.Numero                                        
                                        
  INNER JOIN Oficinas AS OFIC ON                          
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                          
  AND ENRE.OFIC_Codigo = OFIC.Codigo                          
                        
  LEFT JOIN Terceros AS CLIE                 
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                        
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                        
                                
  LEFT JOIN Terceros AS REMI                                        
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                        
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                        
                                        
  LEFT JOIN Terceros AS DEST                                        
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                        
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                        
                                    
  LEFT JOIN Producto_Transportados AS PRTR                                        
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo                                        
  AND ENRE.PRTR_Codigo = PRTR.Codigo                                        
                                        
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                        
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                              
  AND ENRE.ENPD_Numero = ENPD.Numero                                        
  AND ENPD.TIDO_Codigo = 130 --Planilla Despacho Guias    
                                    
  LEFT JOIN Ciudades CIOR                               
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                        
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                        
                                        
  LEFT JOIN Ciudades CIDE                                        
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                        
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                  
                     
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                  
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                  
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                      
                         
  INNER JOIN Oficinas AS OFAC ON                          
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                          
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                                         
                                        
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo    
  AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)    
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)    
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)    
  AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                        
  AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                          
  OR (@par_NombreCliente IS NULL))                                        
  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)    
  AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'    
  AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                   
  AND ((CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                        
  OR (@par_NombreRemitente IS NULL))                                        
  AND ((CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                
  OR (@par_NombreDestinatario IS NULL))    
  AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                        
  AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                                                         
  AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)    
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)    
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)    
  AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)      
  AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)    
 );                                        
 WITH Pagina                                        
 AS                                        
 (      
  SELECT                                        
  ENRE.EMPR_Codigo                                        
  ,ENRE.Numero                                        
  ,ENRE.Numero_Documento                                        
  ,ENRE.Fecha                                        
  ,ISNULL(ENPD.Numero_Documento, 0) AS NumeroDocumentoPlanilla                                        
  ,ISNULL(ENRE.VEHI_Codigo, 0) AS VEHI_Codigo                                        
  ,ISNULL(CLIE.Razon_Social, '') + ' ' + ISNULL(CLIE.Nombre, '') + ' ' + ISNULL(CLIE.Apellido1, '') + ' ' + ISNULL(CLIE.Apellido2, '') AS NombreCliente                                    
  ,ENRE.TERC_Codigo_Cliente                                        
  ,ENRE.CATA_FOPR_Codigo             
  ,FOPR.Campo1 AS FormaPago                                        
  ,ENRE.RUTA_Codigo                                        
  ,ENRP.CIUD_Codigo_Origen                                        
  ,ENRP.CIUD_Codigo_Destino                          
  ,CIOR.Nombre AS CiudadOrigen                                        
  ,CIDE.Nombre AS CiudadDestino                  
  ,ISNULL(ENRP.SICD_Codigo_Descargue, 0) AS SICD_Codigo_Descargue                  
  ,ISNULL(SICD_DESC.Nombre, '') AS SitioDescargue                         
  ,ISNULL(ENRP.OFIC_Codigo_Origen, 0) AS OFIC_Codigo_Origen                        
  ,ISNULL(OFIC.Nombre, '') AS NombreOficinaOrigen                           
  ,ISNULL(OFAC.Nombre, '') AS NombreOficinaActual                           
  ,ENRP.CATA_ESRP_Codigo                      
  ,ENRE.Observaciones                                        
  --,(ENRE.Valor_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS  Valor_Flete_Cliente
  ,(ENRE.Valor_Flete_Cliente) AS  Valor_Flete_Cliente  
  ,(ENRE.Total_Flete_Cliente - ENRE.Valor_Seguro_Cliente) AS Total_Flete_Cliente                                        
  ,ENRE.Valor_Seguro_Cliente                                        
  ,ENRE.Cantidad_Cliente                                        
  ,ENRE.Peso_Cliente                                        
  ,ENRE.Estado                                        
  ,ENRE.Anulado                                        
  ,PRTR.Nombre AS NombreProducto                                        
  ,ENRE.Documento_Cliente                           
  ,ENRE.OFIC_Codigo                          
  ,OFIC.Nombre As NombreOficina                                       
  ,'' AS NombreRuta                          
  ,ENRE.Valor_Flete_Transportador             
  ,ENRE.Valor_Reexpedicion      
  ,ENRP.Reexpedicion      
  ,ISNULL(REMI.Razon_Social, '') + ' ' + ISNULL(REMI.Nombre, '') + ' ' + ISNULL(REMI.Apellido1, '') + ' ' + ISNULL(REMI.Apellido2, '') AS NombreRemitente                                        
  ,ISNULL(DEST.Razon_Social, '') + ' ' + ISNULL(DEST.Nombre, '') + ' ' + ISNULL(DEST.Apellido1, '') + ' ' + ISNULL(DEST.Apellido2, '') AS NombreDestinatario                                        
  ,ROW_NUMBER() OVER (ORDER BY ENRP.ENRE_Numero) AS RowNumber                                        
  FROM Remesas_Paqueteria AS ENRP                                        
                                        
  INNER JOIN Encabezado_Remesas AS ENRE                                        
  ON ENRP.EMPR_Codigo = ENRE.EMPR_Codigo                                        
  AND ENRP.ENRE_Numero = ENRE.Numero                                        
                                    
  INNER JOIN Oficinas AS OFIC ON                          
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo                          
  AND ENRE.OFIC_Codigo = OFIC.Codigo                         
                        
  LEFT JOIN Terceros AS CLIE                                        
  ON ENRE.EMPR_Codigo = CLIE.EMPR_Codigo                                        
  AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo                                        
                                        
  LEFT JOIN Terceros AS REMI                                        
  ON ENRE.EMPR_Codigo = REMI.EMPR_Codigo                                        
  AND ENRE.TERC_Codigo_Remitente = REMI.Codigo                                        
                                        
  LEFT JOIN Terceros AS DEST                                        
  ON ENRE.EMPR_Codigo = DEST.EMPR_Codigo                                        
  AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo                                        
                                        
  LEFT JOIN Producto_Transportados AS PRTR                                        
  ON ENRE.EMPR_Codigo = PRTR.EMPR_Codigo          
  AND ENRE.PRTR_Codigo = PRTR.Codigo                                        
                                        
  LEFT JOIN Encabezado_Planilla_Despachos AS ENPD                                        
  ON ENRE.EMPR_Codigo = ENPD.EMPR_Codigo                                        
  AND ENRE.ENPD_Numero = ENPD.Numero                                        
  AND ENPD.TIDO_Codigo = 130 --Planilla Despacho Guias    
                                      
  LEFT JOIN Ciudades CIOR                                        
  ON ENRP.EMPR_Codigo = CIOR.EMPR_Codigo                                 
  AND ENRP.CIUD_Codigo_Origen = CIOR.Codigo                                        
                              
  LEFT JOIN Ciudades CIDE                                        
  ON ENRP.EMPR_Codigo = CIDE.EMPR_Codigo                                        
  AND ENRP.CIUD_Codigo_Destino = CIDE.Codigo                  
                     
  LEFT JOIN Sitios_Cargue_Descargue SICD_DESC ON                  
  ENRP.EMPR_Codigo = SICD_DESC.EMPR_Codigo                  
  AND ENRP.SICD_Codigo_Descargue = SICD_DESC.Codigo                                                
                                        
  INNER JOIN Oficinas AS OFAC ON                          
  ENRP.EMPR_Codigo = OFAC.EMPR_Codigo                          
  AND ENRP.OFIC_Codigo_Actual = OFAC.Codigo                  
                      
  LEFT JOIN Valor_Catalogos AS FOPR ON                
  ENRE.EMPR_Codigo = FOPR.EMPR_Codigo                
  AND ENRE.CATA_FOPR_Codigo = FOPR.Codigo                
                
  WHERE ENRE.EMPR_Codigo = @par_EMPR_Codigo    
  AND ENRE.Numero_Documento >= ISNULL(@par_NumeroInicial, ENRE.Numero_Documento)    
  AND ENRE.Fecha >= ISNULL(@par_FechaInicial, ENRE.Fecha)    
  AND ENRE.Fecha <= ISNULL(@par_FechaFinal, ENRE.Fecha)    
  AND ENRE.Documento_Cliente >= ISNULL(@par_Documento_Cliente, ENRE.Documento_Cliente)                                        
  AND ((CONCAT(ISNULL(CLIE.Razon_Social, ''), ISNULL(CLIE.Nombre, ''), ' ', ISNULL(CLIE.Apellido1, ''), ' ', ISNULL(CLIE.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreCliente, '') + '%')                          
  OR (@par_NombreCliente IS NULL))                                        
  AND ENRE.TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente, ENRE.TERC_Codigo_Cliente)    
  AND CIOR.Nombre LIKE '%' + ISNULL(@par_Ciudad_Origen,  CIOR.Nombre) + '%'    
  AND CIDE.Nombre LIKE '%' + ISNULL(@par_Ciudad_Destino,  CIDE.Nombre) + '%'                                   
  AND ((CONCAT(ISNULL(REMI.Razon_Social, ''), ISNULL(REMI.Nombre, ''), ' ', ISNULL(REMI.Apellido1, ''), ' ', ISNULL(REMI.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreRemitente, '') + '%')                                        
  OR (@par_NombreRemitente IS NULL))                                        
  AND ((CONCAT(ISNULL(DEST.Razon_Social, ''), ISNULL(DEST.Nombre, ''), ' ', ISNULL(DEST.Apellido1, ''), ' ', ISNULL(DEST.Apellido2, '')) LIKE '%' + ISNULL(@par_NombreDestinatario, '') + '%')                                
  OR (@par_NombreDestinatario IS NULL))    
  AND ENRE.Anulado = ISNULL(@par_Anulado, ENRE.Anulado)                                        
  AND ENRE.Estado = ISNULL(@par_Estado, ENRE.Estado)                                                                         
  AND ENRE.TIDO_Codigo = ISNULL(@par_TIDO_Codigo, ENRE.TIDO_Codigo)    
  AND ENRE.ENPD_Numero = ISNULL(@par_ENPD_Numero, ENRE.ENPD_Numero)    
  AND ENRP.OFIC_Codigo_Actual = ISNULL(@par_OFIC_Codigo_Actual_Usuario, ENRP.OFIC_Codigo_Actual)    
  AND (ENRP.CIUD_Codigo_Destino = @par_Ciudad_Destino_Codigo OR @par_Ciudad_Destino_Codigo IS NULL)      
  AND (ENRP.Reexpedicion = @par_Reexpedicion OR @par_Reexpedicion IS NULL)    
 )                                        
 SELECT DISTINCT                                        
 0 AS Obtener    
 ,EMPR_Codigo                                        
 ,Numero                                        
 ,Numero_Documento                                        
 ,Fecha                                        
 ,NumeroDocumentoPlanilla                                        
 ,VEHI_Codigo                    
 ,NombreCliente                                        
 ,TERC_Codigo_Cliente                                        
 ,CATA_FOPR_Codigo                     
 ,FormaPago                
 ,RUTA_Codigo                                        
 ,CIUD_Codigo_Origen                                        
 ,CIUD_Codigo_Destino                                        
 ,CiudadOrigen                                        
 ,CiudadDestino              
 ,SICD_Codigo_Descargue                  
 ,SitioDescargue                       
 ,OFIC_Codigo_Origen                          
 ,NombreOficinaOrigen                                  
 ,Observaciones                                        
 ,Valor_Flete_Cliente                                        
 ,Total_Flete_Cliente                                        
 ,Valor_Seguro_Cliente                                        
 ,Cantidad_Cliente                                        
 ,Peso_Cliente                                        
 ,Estado                    
 ,Anulado            
 ,NombreProducto                               
 ,Documento_Cliente                         
 ,OFIC_Codigo                          
 ,NombreOficina                 
 ,NombreRuta                                        
 ,NombreOficinaActual                      
 ,CATA_ESRP_Codigo                       
 ,Valor_Flete_Transportador                
 ,NombreRemitente             
 ,Valor_Reexpedicion      
 ,Reexpedicion          
 , NombreDestinatario               
 ,@CantidadRegistros AS TotalRegistros                                        
 ,@par_NumeroPagina AS PaginaObtener                                        
 ,@par_RegistrosPagina AS RegistrosPagina                                        
 FROM Pagina                                        
 WHERE RowNumber > (ISNULL(@par_NumeroPagina, 1) - 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                        
 AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, @CantidadRegistros)                                        
 ORDER BY Numero ASC                                        
END 
GO