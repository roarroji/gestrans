﻿PRINT 'gsp_insertar_lista_auxiliares'
GO
DROP PROCEDURE gsp_insertar_lista_auxiliares
GO
CREATE PROCEDURE gsp_insertar_lista_auxiliares    
(@par_EMPR_Codigo SMALLINT,    
@par_ENPR_Codigo NUMERIC,   
@par_TERC_Codigo NUMERIC,   
@par_Horas_Trabajados NUMERIC(18,2),    
@par_Valor_Horas MONEY,  
@par_Observaciones VARCHAR(500) = NULL  
)    
AS    
BEGIN    
    
INSERT INTO [dbo].Detalle_Auxiliares_Planilla_Recolecciones    
           (EMPR_Codigo  
           ,ENPR_Numero    
           ,TERC_Codigo_Funcionario  
           ,Numero_Horas_Trabajadas  
           ,Valor  
           ,Observaciones    
         )    
     VALUES    
           (@par_EMPR_Codigo   
     ,@par_ENPR_Codigo    
           ,@par_TERC_Codigo    
           ,@par_Horas_Trabajados    
           ,@par_Valor_Horas  
     ,ISNULL(@par_Observaciones, ''))    
  
     SELECT ENPR_Numero FROM Detalle_Auxiliares_Planilla_Recolecciones  
     WHERE EMPR_Codigo = @par_EMPR_Codigo  
     AND TERC_Codigo_Funcionario = @par_TERC_Codigo  
  
END    
GO