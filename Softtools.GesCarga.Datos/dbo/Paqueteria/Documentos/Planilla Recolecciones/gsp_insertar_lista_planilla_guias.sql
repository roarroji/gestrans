﻿PRINT 'gsp_insertar_lista_planilla_guias'
GO
DROP PROCEDURE gsp_insertar_lista_planilla_guias
GO
CREATE PROCEDURE gsp_insertar_lista_planilla_guias 
(@par_EMPR_Codigo SMALLINT,    
@par_ENPR_Numero NUMERIC,   
@par_ENRE_Numero NUMERIC  
)    
AS    
BEGIN   
    
INSERT INTO Detalle_Guias_Planilla_Recolecciones  
           (EMPR_Codigo  
     ,ENPR_Numero    
           ,ENRE_Numero   
         )    
     VALUES    
           (@par_EMPR_Codigo   
     ,@par_ENPR_Numero    
           ,@par_ENRE_Numero)  
  
    UPDATE Encabezado_Remesas SET ENPR_Numero = @par_ENPR_Numero  
    WHERE EMPR_Codigo = @par_EMPR_Codigo AND  Numero = @par_ENRE_Numero  
	UPDATE Remesas_Paqueteria SET CATA_ESRP_Codigo = 6001  
    WHERE EMPR_Codigo = @par_EMPR_Codigo AND  ENRE_Numero = @par_ENRE_Numero 
  
END    
GO