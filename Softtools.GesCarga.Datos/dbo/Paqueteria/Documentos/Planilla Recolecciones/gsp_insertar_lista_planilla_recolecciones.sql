﻿PRINT 'gsp_insertar_lista_planilla_recolecciones'
GO
DROP PROCEDURE gsp_insertar_lista_planilla_recolecciones
GO
CREATE PROCEDURE gsp_insertar_lista_planilla_recolecciones  
(@par_EMPR_Codigo SMALLINT,  
@par_ENPR_Numero NUMERIC, 
@par_EREC_Numero NUMERIC
)  
AS  
BEGIN  
  
INSERT INTO Detalle_Planilla_Recolecciones
           (EMPR_Codigo
		   ,ENPR_Numero  
           ,EREC_Numero 
         )  
     VALUES  
           (@par_EMPR_Codigo 
		   ,@par_ENPR_Numero  
           ,@par_EREC_Numero)

  UPDATE Encabezado_Recolecciones SET ENPR_Numero = @par_ENPR_Numero
  WHERE EMPR_Codigo = @par_EMPR_Codigo AND  Numero = @par_EREC_Numero

END  
GO