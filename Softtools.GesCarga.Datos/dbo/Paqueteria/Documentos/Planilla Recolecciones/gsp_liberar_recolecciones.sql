﻿PRINT 'gsp_liberar_recolecciones'
GO
DROP PROCEDURE gsp_liberar_recolecciones
GO
CREATE PROCEDURE gsp_liberar_recolecciones (  
@par_EMPR_Codigo NUMERIC,   
@par_ENPR_Numero NUMERIC,  
@par_EREC_Numero NUMERIC
)  
AS BEGIN   
UPDATE Encabezado_Recolecciones SET ENPR_Numero = 0   
WHERE Numero = @par_EREC_Numero AND ENPR_Numero = @par_ENPR_Numero  
  
 SELECT @@ROWCOUNT As CantidadRegistros  
END
GO   