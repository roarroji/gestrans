﻿PRINT 'gsp_eliminar_recolecciones'
GO
DROP PROCEDURE gsp_eliminar_recolecciones
GO
CREATE PROCEDURE gsp_eliminar_recolecciones(    
  @par_EMPR_Codigo SMALLINT,    
  @par_Numero NUMERIC    
  )    
AS    
BEGIN    
      
  UPDATE Encabezado_Recolecciones SET Anulado = 1  
      
  WHERE EMPR_Codigo = @par_EMPR_Codigo     
    AND Numero = @par_Numero    
    
    SELECT @@ROWCOUNT AS Numero    
    
END    
GO