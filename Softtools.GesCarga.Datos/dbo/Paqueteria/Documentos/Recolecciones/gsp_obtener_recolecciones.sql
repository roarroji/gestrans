﻿PRINT 'gsp_obtener_recolecciones'	
GO
DROP PROCEDURE gsp_obtener_recolecciones
GO
CREATE PROCEDURE gsp_obtener_recolecciones    
(    
@par_EMPR_Codigo SMALLINT,    
@par_Numero SMALLINT = NULL
)    
AS    
BEGIN    
 SELECT   
  0 AS Obtener, 
  ENRE.EMPR_Codigo,    
  ENRE.Numero,    
  ENRE.Numero_Documento,       
  ISNULL(ENRE.Fecha,'') AS Fecha,
  ISNULL(ENRE.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente, 
  ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1, '')+ ' ' + ISNULL(TERC.Apellido2,'')+ ' ' + ISNULL(TERC.Razon_Social,'') AS Nombre_Cliente,       
  ISNULL(ENRE.Nombre_Contacto,'') AS Nombre_Contacto,
  ISNULL(ENRE.OFIC_Codigo, 0) AS Codigo_Oficina,     
  ISNULL(OFIC.Nombre, '') AS Nombre_Oficina,  
  ISNULL(ENRE.ZOCI_Codigo, 0) AS Codigo_Zona,
  ISNULL(ZOCI.Nombre, '') AS Nombre_Zona,  
  '' AS Nombre_Ciudad,
  '' AS Unidad_Empaque,
  ISNULL(ENRE.CIUD_Codigo,'') AS Codigo_Ciudad,
  ISNULL(ENRE.Barrio, 0) AS Barrio,       
  ISNULL(ENRE.Direccion,'') AS Direccion,
  ISNULL(ENRE.Telefonos, '') AS Telefonos,       
  ISNULL(ENRE.Mercancia,'') AS Mercancia,
  ISNULL(ENRE.UEPT_Codigo, 0) AS UEPT_Codigo,       
  ISNULL(ENRE.Cantidad,0) AS Cantidad,    
  ISNULL(ENRE.Peso,0) AS Peso,
  ISNULL(ENRE.Observaciones,'') AS Observaciones,   
  ISNULL(ENRE.Estado,'') AS Estado

 FROM   
  
  Encabezado_Recolecciones AS ENRE 
  
  LEFT JOIN Terceros AS TERC ON  
  ENRE.EMPR_Codigo = TERC.EMPR_Codigo  
  AND ENRE.TERC_Codigo_Cliente = TERC.Codigo  

  LEFT JOIN Ciudades AS CIUD ON  
  ENRE.EMPR_Codigo = CIUD.EMPR_Codigo  
  AND ENRE.CIUD_Codigo = CIUD.Codigo  

  LEFT JOIN Oficinas AS OFIC ON  
  ENRE.EMPR_Codigo = OFIC.EMPR_Codigo  
  AND ENRE.OFIC_Codigo = OFIC.Codigo  

  LEFT JOIN Zona_Ciudades AS ZOCI ON  
  ENRE.EMPR_Codigo = ZOCI.EMPR_Codigo  
  AND ENRE.ZOCI_Codigo = ZOCI.Codigo  
     
 WHERE    
  ENRE.EMPR_Codigo = @par_EMPR_Codigo  
  AND ENRE.Numero = @par_Numero 
END 
GO 