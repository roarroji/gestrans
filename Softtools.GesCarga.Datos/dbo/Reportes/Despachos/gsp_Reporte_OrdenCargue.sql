﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 400208 --Permiso listado Orden Cargue por Oficina
DELETE Permiso_Grupo_Usuarios WHERE MEAP_Codigo = 400209 --Permiso listado Orden Cargue por Cliente
GO

DELETE Menu_Aplicaciones WHERE Codigo = 400208 -- listado Orden Cargue por Oficina
DELETE Menu_Aplicaciones WHERE Codigo = 400209 -- listado Orden Cargue por Cliente
GO


INSERT INTO Menu_Aplicaciones
SELECT
	Codigo,400208,40,'Orden Cargue por Oficina',4002,30,0,1,1,1,1,1,0,0,1,0,'#!listadoOrdenCarguesporOficina','#',null
FROM
	Empresas
GO

INSERT INTO Menu_Aplicaciones
SELECT
	Codigo,400209,40,'Orden Cargue por Cliente',4002,40,0,1,1,1,1,1,0,0,1,0,'#!listadoOrdenCargueporOficina','#',null
FROM
	Empresas
GO

INSERT INTO Permiso_Grupo_Usuarios

SELECT
	EMPR_Codigo,400209,Codigo,1,1,1,1,1,1,0
FROM
	Grupo_Usuarios
WHERE
	Codigo = 1 
	GO

INSERT INTO Permiso_Grupo_Usuarios
SELECT
	EMPR_Codigo,400209,Codigo,0,0,0,0,0,0,0
FROM
	Grupo_Usuarios
WHERE
	Codigo <> 1

GO

INSERT INTO Permiso_Grupo_Usuarios

SELECT
	EMPR_Codigo,400208,Codigo,1,1,1,1,1,1,0
FROM
	Grupo_Usuarios
WHERE
	Codigo = 1 
GO

INSERT INTO Permiso_Grupo_Usuarios
SELECT
	EMPR_Codigo,400208,Codigo,0,0,0,0,0,0,0
FROM
	Grupo_Usuarios
WHERE
	Codigo <> 1
 
GO

print'gsp_Reporte_OrdenCargue'

DROP PROCEDURE gsp_Reporte_OrdenCargue
GO

CREATE PROCEDURE gsp_Reporte_OrdenCargue (
@par_EMPR_Codigo numeric 
, @par_Numero_Documento numeric 
)
AS BEGIN
SELECT DISTINCT ENOC.Numero_Documento
, EMPR.Nombre_Razon_Social  AS NombreEmpresa
, EMPR.Direccion  AS DireccionEmpresa
, EMPR.Telefonos AS TelefonoEmpresa
, EMPR.Numero_Identificacion AS NIT 
, CIOC.Nombre AS NombreCiudadCargue 
, ENOC.Fecha AS FechaCargue
, ISNULL(TECL.Razon_Social,'')+' '+ISNULL(TECL.Nombre,'')  
+' '+ISNULL(TECL.Apellido1,'')+' '+ISNULL(TECL.Apellido2,'') AS NombreCliente
, CICL.Nombre AS CiudadCliente 
, ENOC.Direccion_Cargue Direccion 
, TECL.Telefonos AS TelefonoCliente
, ENOC.Cantidad_Cliente 
--Empaque 
, ENOC.Peso_Cliente 
, RUTA.Nombre AS Ruta 
, ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')  
+ ' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS NombreConductor
, TECO.Numero_Identificacion AS CedulaConductor 
, TECO.Telefonos AS CelularConductr 
, TIVE.Campo1 AS TipoVehiculo
, VEHI.Placa 
, VEHI.Modelo 
, COVE.Nombre AS ColorVehiculo
, SEMI.Placa AS Semiremolque 
, VEHI.Peso_Bruto AS PesoVehiVacio 
, ENOC.Observaciones 
, ESOS.Numero_Documento OrdenServicio
, PRTR.Nombre ProductoTransportado
, UEPT.Nombre_Corto UnidadEmpaque

FROM Encabezado_Orden_Cargues ENOC
, Empresas EMPR
, Ciudades CIOC
, Ciudades CICL
, Terceros TECL
, Terceros TECO
, Rutas RUTA
, Vehiculos VEHI
, Color_Vehiculos COVE
, Valor_Catalogos TIVE
, Semirremolques  SEMI
, Producto_Transportados PRTR
, Unidad_Empaque_Producto_Transportados UEPT
, Encabezado_Solicitud_Orden_Servicios ESOS
WHERE ENOC.EMPR_Codigo = EMPR.Codigo

AND ENOC.EMPR_Codigo = CIOC.EMPR_Codigo
AND ENOC.CIUD_Codigo_Cargue = CIOC.Codigo

AND ENOC.EMPR_Codigo  = TECL.EMPR_Codigo
AND ENOC.TERC_Codigo_Cliente = TECL.Codigo

AND TECL.EMPR_Codigo  = CICL.EMPR_Codigo
AND TECL.CIUD_Codigo = CICL.Codigo

AND ENOC.EMPR_Codigo = RUTA.EMPR_Codigo 
AND ENOC.RUTA_Codigo = RUTA.Codigo

AND ENOC.EMPR_Codigo = TECO.EMPR_Codigo
AND ENOC.TERC_Codigo_Conductor = TECO.Codigo

AND ENOC.EMPR_Codigo = VEHI.EMPR_Codigo
AND ENOC.VEHI_Codigo = VEHI.Codigo

AND VEHI.EMPR_Codigo = COVE.EMPR_Codigo 
AND VEHI.COVE_Codigo = COVE.Codigo

AND VEHI.EMPR_Codigo =  SEMI.EMPR_Codigo 
AND VEHI.SEMI_Codigo = SEMI.Codigo 

AND ENOC.EMPR_Codigo = PRTR.EMPR_Codigo
AND ENOC.PRTR_Codigo = PRTR.Codigo

AND PRTR.EMPR_Codigo = UEPT.EMPR_Codigo
AND PRTR.UEPT_Codigo = UEPT.Codigo

AND VEHI.EMPR_Codigo = TIVE.EMPR_Codigo
AND VEHI.CATA_TIVE_Codigo = TIVE.Codigo

AND ENOC.EMPR_Codigo = ESOS.EMPR_Codigo
AND ENOC.ESOS_Numero = ESOS.Numero

AND ENOC.EMPR_Codigo = @par_EMPR_Codigo
AND ENOC.Numero = @par_Numero_Documento	
END 
GO