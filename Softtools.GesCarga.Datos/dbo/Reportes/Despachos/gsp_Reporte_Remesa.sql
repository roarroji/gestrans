﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


	PRINT 'gsp_Reporte_Remesa'

DROP PROCEDURE gsp_Reporte_Remesa
GO	

CREATE PROCEDURE gsp_Reporte_Remesa  (
@par_EMPR_Codigo numeric 
, @par_Numero_Documento numeric )
AS BEGIN
SELECT 
EMPR.Nombre_Razon_Social 
, EMPR.Numero_Identificacion NIT
, EMPR.Direccion 
, EMPR.Telefonos TelefonoEmpresa
, ENRE.Numero AS Remesa
, CIOR.Nombre AS CiudadOrigen
, CIDE.Nombre AS CiudadDestino
, ENRE.FECHA 
, ISNULL(TERE.Razon_Social,'')+' '+ISNULL(TERE.Nombre,'')  
+' '+ISNULL(TERE.Apellido1,'')+' '+ISNULL(TERE.Apellido2,'') AS NombreRemitente
, ENRE.Direccion_Destinatario
,  EMPR.Telefonos
, ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'')  
+' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS NombreConductor
, EMPR.Telefonos TelefonoConductor 
, TECO.Numero_Identificacion CedulaConductor 
, ENRE.Observaciones 
, VEHI.Placa
, ENRE.Cantidad_Cliente
--
,ENRE.Peso_Cliente 
,PRTR.Nombre AS NombreProducto 
, ISNULL(TEDE.Razon_Social,'')+' '+ISNULL(TEDE.Nombre,'')  
+' '+ISNULL(TEDE.Apellido1,'')+' '+ISNULL(TEDE.Apellido2,'') AS NombreDestinatario
,UEPT.Nombre_Corto UnidadEmpaque
,ENRE.Documento_Cliente Remision 
, ISNULL(TECL.Razon_Social,'')+' '+ISNULL(TECL.Nombre,'')  
+' '+ISNULL(TECL.Apellido1,'')+' '+ISNULL(TECL.Apellido2,'') AS NombreCliente
, TECL.Direccion DireccionCliente
,  TECL.Telefonos DireccionTelefono

FROM Encabezado_Remesas   ENRE

, Empresas EMPR
, Terceros TECO
, Terceros TERE
, Terceros TEDE
, Ciudades CIOR
, Ciudades CIDE
, Oficinas OFIC
, Vehiculos VEHI
, Producto_Transportados PRTR
, Unidad_Empaque_Producto_Transportados UEPT
, Terceros TECL

WHERE ENRE.EMPR_Codigo = EMPR.Codigo 

AND ENRE.EMPR_Codigo = TECO.EMPR_Codigo
AND ENRE.TERC_Codigo_Conductor = TECO.Codigo 

AND ENRE.EMPR_Codigo = CIOR.EMPR_Codigo
AND ENRE.CIUD_Codigo_Remitente = CIOR.Codigo

AND ENRE.EMPR_Codigo = CIDE.EMPR_Codigo
AND ENRE.CIUD_Codigo_Destinatario = CIDE.Codigo

AND ENRE.EMPR_Codigo = TERE.EMPR_Codigo
AND ENRE.TERC_Codigo_Remitente = TERE.Codigo 

AND ENRE.EMPR_Codigo = TEDE.EMPR_Codigo
AND ENRE.TERC_Codigo_Destinatario = TEDE.Codigo 

AND ENRE.EMPR_Codigo = TECL.EMPR_Codigo
AND ENRE.TERC_Codigo_Cliente = TECL.Codigo 

AND ENRE.EMPR_Codigo = VEHI.EMPR_Codigo
AND ENRE.VEHI_Codigo = VEHI.Codigo

AND ENRE.EMPR_Codigo = OFIC.EMPR_Codigo
AND ENRE.OFIC_Codigo = OFIC.Codigo 

AND ENRE.EMPR_Codigo = PRTR.EMPR_Codigo 
AND ENRE.PRTR_Codigo = PRTR.Codigo

AND PRTR.EMPR_Codigo = UEPT.EMPR_Codigo
AND PRTR.UEPT_Codigo = UEPT.Codigo

AND ENRE.EMPR_Codigo = @par_EMPR_Codigo
AND ENRE.Numero_Documento = @par_Numero_Documento

AND ENRE.TIDO_Codigo=100

END 
GO 
