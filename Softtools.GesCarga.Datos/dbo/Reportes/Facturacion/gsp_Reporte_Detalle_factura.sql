﻿	/*Crea:Edsson Pedreros
	Fecha_Crea:
	Descripción_Crea:
	Modifica:Edsson Pedreros
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */


	PRINT 'gsp_Reporte_Detalle_factura'
DROP PROCEDURE gsp_Reporte_Detalle_factura
GO

CREATE PROCEDURE gsp_Reporte_Detalle_factura (
@par_Empr_Codigo NUMERIC  
, @par_Numero_Factura  NUMERIC )
AS BEGIN
SELECT 
ENRE.Numero 
, ENRE.Documento_Cliente
, ENRE.ENMC_Numero MANIFIESTO 
, ENRE.FECHA
, RUTA.CATA_TIRU_Codigo RUTA  
, VEHI.Placa
, ENRE.Peso_Cliente
, ENRE.Valor_Flete_Cliente FleteTon
, ENRE.Valor_Comercial_Cliente
, ENRE.Documento_Cliente Remision 
 FROM 
Encabezado_Facturas ENFA
, Detalle_Remesas_Facturas	DRFA
, Encabezado_Remesas ENRE
, Empresas EMPR
, Rutas RUTA
, Vehiculos VEHI
WHERE ENFA.EMPR_Codigo = EMPR.Codigo

AND ENFA.EMPR_Codigo = DRFA.EMPR_Codigo 
AND ENFA.Numero = DRFA.ENFA_Numero 

AND DRFA.EMPR_Codigo = ENRE.EMPR_Codigo 
AND DRFA.ENRE_Numero = ENRE.Numero

AND ENRE.EMPR_Codigo = RUTA.EMPR_Codigo
AND ENRE.RUTA_Codigo = RUTA.Codigo

AND ENRE.EMPR_Codigo = VEHI.EMPR_Codigo
AND ENRE.VEHI_Codigo = VEHI.Codigo

AND ENFA.EMPR_Codigo = @par_Empr_Codigo
AND ENFA.Numero = @par_Numero_Factura
END 
GO