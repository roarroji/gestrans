﻿DROP PROCEDURE gps_reporte_encabezado_legalizacion_gastos
GO
CREATE PROCEDURE gps_reporte_encabezado_legalizacion_gastos      
(            
 @par_EMPR_Codigo NUMERIC,            
 @par_Numero NUMERIC,            
 @par_Usuario NUMERIC              
)            
AS             
BEGIN            
 SELECT DISTINCT            
 EMPR.Nombre_Razon_Social NombreEmpresa,                
 EMPR.Numero_Identificacion IdentificacionEmpresa,            
 EMPR.Direccion  AS DireccionEmpresa,            
 EMPR.Telefonos AS TelefonoEmpresa,            
 CIEM.Nombre AS CiudadEmpresa,        
        
 ELGC.Numero,        
 ELGC.Numero_Documento,        
 ELGC.Fecha,        
 ELGC.Valor_Gastos,        
 ELGC.Valor_Anticipos,        
 ELGC.Valor_Reanticipos,        
 ELGC.Saldo_Favor_Conductor,        
 ELGC.Saldo_Favor_Empresa,        
 ISNULL(ELGC.Total_Galones, 0) Total_Galones,        
 ISNULL(ELGC.Total_Galones_Autorizados, 0) Total_Galones_Autorizados,        
 ISNULL(ELGC.Total_Peajes, 0) Total_Peajes,      
 OFIC.Nombre NombreOficina,        
 ISNULL(ELGC.Observaciones, '') Observaciones,        
 ELGC.Estado,        
 ELGC.Anulado,        
        
 ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'') +' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS NombreConductor,            
 TECO.Numero_Identificacion AS IdentificacionConductor,            
 TECO.Direccion AS DireccionConductor,            
 TECO.Telefonos AS TelefonoConductor,             
            
 USUA.Codigo_Usuario Usuario,       
     
 (Select  Codigo_Usuario FROM Usuarios where EMPR_Codigo = @par_EMPR_Codigo and Codigo = @par_Usuario) as USUA_Imprime            
       ,ELGC.Valor_Total_Combustible    
   ,ELGC.Valor_Promedio_Galon       
   ,ENPD.Numero_Documento  
   ,VEHI.Placa  
   ,RUTA.Nombre AS RUTA ,
   SEMI.Placa AS Placa_Remolque

 from Encabezado_Legalizacion_Gastos_Conductor ELGC        
            
 LEFT JOIN Empresas EMPR ON            
 ELGC.EMPR_Codigo = EMPR.CODIGO            
         
 LEFT JOIN Usuarios USUA ON            
 ELGC.EMPR_Codigo = USUA.EMPR_Codigo            
 AND ELGC.USUA_Codigo_Crea = USUA.Codigo            
            
 LEFT JOIN Oficinas OFIC ON            
 ELGC.EMPR_Codigo = OFIC.EMPR_Codigo            
 AND ELGC.OFIC_Codigo = OFIC.Codigo            
            
 LEFT JOIN Terceros TECO ON            
 ELGC.EMPR_Codigo = TECO.EMPR_Codigo            
 AND ELGC.TERC_Codigo_Conductor = TECO.Codigo            
        
 LEFT JOIN  Ciudades CIEM ON            
 EMPR.Codigo = CIEM.EMPR_Codigo            
 AND EMPR.CIUD_Codigo = CIEM.Codigo         
   
 LEFT JOIN Encabezado_Planilla_Despachos AS ENPD  
 ON ELGC.EMPR_Codigo = ENPD.EMPR_Codigo  
 AND ELGC.ENPD_Numero = ENPD.Numero  
  
 LEFT JOIN Vehiculos AS VEHI  
 ON ENPD.EMPR_Codigo = VEHI.EMPR_Codigo  
 AND ENPD.VEHI_Codigo = VEHI.Codigo  
   
 LEFT JOIN Rutas AS RUTA  
 ON ENPD.EMPR_Codigo = RUTA.EMPR_Codigo  
 AND ENPD.RUTA_Codigo = RUTA.Codigo  

 LEFT JOIN Semirremolques SEMI ON
 ENPD.EMPR_Codigo = SEMI.EMPR_Codigo
 AND ENPD.SEMI_Codigo = SEMI.Codigo
             
 WHERE ELGC.EMPR_CODIGO = @par_EMPR_Codigo            
 AND ELGC.NUMERO = @par_Numero            
END        