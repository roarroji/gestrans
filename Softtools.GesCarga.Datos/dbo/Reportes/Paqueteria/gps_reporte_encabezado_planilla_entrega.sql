﻿PRINT 'gps_reporte_encabezado_planilla_entrega'
GO
DROP PROCEDURE gps_reporte_encabezado_planilla_entrega
GO 
CREATE PROCEDURE gps_reporte_encabezado_planilla_entrega
(
	@par_EMPR_Codigo NUMERIC,
	@par_Numero NUMERIC,
	@par_Usuario NUMERIC  
)
AS 
BEGIN
	SELECT DISTINCT
	EMPR.Nombre_Razon_Social NombreEmpresa,    
	EMPR.Numero_Identificacion IdentificacionEmpresa,
	EMPR.Direccion  AS DireccionEmpresa,
	EMPR.Telefonos AS TelefonoEmpresa,
	CIEM.Nombre AS CiudadEmpresa,
	ENPD.Numero_Documento,
	ENPD.Cantidad,
	ENPD.Fecha,
	VEHI.Placa VehiculoPlaca,
	VEHI.Peso_Bruto,
	MAVE.Nombre MarcaVehiculo,
	SEMI.Placa SemiPlaca,
	RUTA.NOMBRE,
	CIOR.NOMBRE CiudadOrigen,
	CIDE.NOMBRE CiudadDestino,
	ENPD.Peso,
	ISNULL(TEPR.Razon_Social,'')+' '+ISNULL(TEPR.Nombre,'') +' '+ISNULL(TEPR.Apellido1,'')+' '+ISNULL(TEPR.Apellido2,'') AS TitutlarPropietario,
	TEPR.Numero_Identificacion AS IdentificacionPropietario,
	TEPR.Direccion AS DireccionPropietario,
	TEPR.Telefonos AS TelefonoPropietario,
	CIPR.Nombre AS CiudadPropietario,
	ISNULL(TENE.Razon_Social,'')+' '+ISNULL(TENE.Nombre,'') +' '+ISNULL(TENE.Apellido1,'')+' '+ISNULL(TENE.Apellido2,'') AS TitutlarTenedor,
	TENE.Numero_Identificacion AS IdentificacionTenedor,
	TENE.Direccion AS DireccionTenedor, 
	TENE.Telefonos AS TelefonoTitular,
	CITE.Nombre AS CiudadTenedor,
	ISNULL(TECO.Razon_Social,'')+' '+ISNULL(TECO.Nombre,'') +' '+ISNULL(TECO.Apellido1,'')+' '+ISNULL(TECO.Apellido2,'') AS TitutlarConductor,
	TECO.Numero_Identificacion AS IdentificacionConductor,
	TECO.Direccion AS DireccionConductor,
	TECO.Telefonos AS TelefonoConductor, 
	ENPD.Valor_Flete_Transportador,
	ENPD.Valor_Anticipo,
	ENPD.Valor_Impuestos,
	ENPD.Valor_Pagar_Transportador,
	ENPD.Valor_Flete_Cliente,
	ENPD.Observaciones,
	ENPD.Anulado,
	ENPD.Estado,
	TLNC.Nombre TipoLineaNegocio,
	LNTC.Nombre LineaNegocio,
	TTTC.NombreTarifa Tarifa, 
	TTTC.EstadoTarifa,
	ETCC.Nombre Tarifario, 
	USUA.Nombre Usuario, 
	OFIC.Nombre Oficina, 
	CICO.Nombre AS CiudadConductor,
	ISNULL(TTTC.Nombre,'(NO APLICA)') AS TipoTarifa,
	(Select  Nombre FROM Usuarios where EMPR_Codigo = @par_EMPR_Codigo and Codigo = @par_Usuario) as USUA_Imprime

	from Encabezado_Planilla_Despachos ENPD

	LEFT JOIN Empresas EMPR ON
	ENPD.EMPR_Codigo = EMPR.CODIGO

	LEFT JOIN VEHICULOS VEHI ON
	ENPD.EMPR_CODIGO = VEHI.EMPR_CODIGO
	AND ENPD.VEHI_CODIGO = VEHI.CODIGO

	LEFT JOIN Marca_Vehiculos MAVE ON
	VEHI.EMPR_Codigo = MAVE.EMPR_Codigo
	AND VEHI.MAVE_Codigo = MAVE.Codigo

	LEFT JOIN Usuarios USUA ON
	ENPD.EMPR_Codigo = USUA.EMPR_Codigo
	AND ENPD.USUA_Codigo_Crea = USUA.Codigo

	LEFT JOIN Oficinas OFIC ON
	ENPD.EMPR_Codigo = OFIC.EMPR_Codigo
	AND ENPD.OFIC_Codigo = OFIC.Codigo

	LEFT JOIN Terceros TENE ON
	ENPD.EMPR_Codigo = TENE.EMPR_Codigo
	AND ENPD.TERC_Codigo_Tenedor = TENE.Codigo

	LEFT JOIN Terceros TECO ON
	ENPD.EMPR_Codigo = TECO.EMPR_Codigo
	AND ENPD.TERC_Codigo_Conductor = TECO.Codigo

	LEFT JOIN Terceros TEPR ON
	VEHI.EMPR_Codigo = TEPR.EMPR_Codigo
	AND VEHI.TERC_Codigo_Propietario = TEPR.Codigo

	LEFT JOIN Tipo_Linea_Negocio_Carga TLNC ON
	ENPD.EMPR_Codigo = TLNC.EMPR_Codigo
	AND ENPD.TLNC_Codigo_Compra = TLNC.Codigo

	LEFT JOIN Linea_Negocio_Transporte_Carga LNTC ON
	ENPD.EMPR_Codigo = LNTC.EMPR_Codigo
	and ENPD.LNTC_Codigo_Compra = LNTC.Codigo

	LEFT JOIN Encabezado_Tarifario_Carga_Compras ETCC ON
	ENPD.EMPR_Codigo = ETCC.EMPR_Codigo
	AND ENPD.ETCC_Numero = ETCC.Numero

	LEFT JOIN  V_Tipo_Tarifa_Transporte_Carga TTTC ON
	ENPD.EMPR_Codigo = TTTC.EMPR_Codigo
	AND ISNULL(ENPD.TTTC_Codigo_Compra,0) = ISNULL(TTTC.Codigo,0)
	AND ENPD.TATC_Codigo_Compra = TTTC.TATC_Codigo

	LEFT JOIN SEMIRREMOLQUES SEMI ON
	ENPD.EMPR_CODIGO = SEMI.EMPR_CODIGO
	AND ENPD.SEMI_CODIGO = SEMI.CODIGO

	LEFT JOIN RUTAS RUTA ON
	ENPD.EMPR_CODIGO = RUTA.EMPR_CODIGO
	AND ENPD.RUTA_CODIGO = RUTA.CODIGO

	LEFT JOIN CIUDADES CIOR ON
	RUTA.EMPR_CODIGO = CIOR.EMPR_CODIGO
	AND RUTA.CIUD_CODIGO_ORIGEN = CIOR.CODIGO

	LEFT JOIN CIUDADES CIDE ON
	RUTA.EMPR_CODIGO = CIDE.EMPR_CODIGO
	AND RUTA.CIUD_CODIGO_DESTINO = CIDE.CODIGO

	LEFT JOIN Ciudades CIPR ON
	TEPR.EMPR_Codigo = CIPR.EMPR_Codigo
	AND TEPR.CIUD_Codigo = CIPR.Codigo

	LEFT JOIN Ciudades CITE ON
	TENE.EMPR_Codigo = CITE.EMPR_Codigo
	AND TENE.CIUD_Codigo = CITE.Codigo

	LEFT JOIN  Ciudades CIEM ON
	EMPR.Codigo = CIEM.EMPR_Codigo
	AND EMPR.CIUD_Codigo = CIEM.Codigo

	LEFT JOIN Ciudades CICO ON
	TECO.EMPR_Codigo = CICO.EMPR_Codigo
	AND TECO.CIUD_Codigo = CICO.Codigo

	WHERE ENPD.EMPR_CODIGO = @PAR_EMPR_CODIGO
	AND ENPD.NUMERO = @PAR_NUMERO
END
GO