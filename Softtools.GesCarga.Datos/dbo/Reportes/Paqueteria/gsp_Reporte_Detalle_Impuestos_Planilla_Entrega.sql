﻿PRINT 'gsp_Reporte_Detalle_Impuestos_Planilla_Entrega'
GO
DROP PROCEDURE gsp_Reporte_Detalle_Impuestos_Planilla_Entrega
GO 
CREATE PROCEDURE gsp_Reporte_Detalle_Impuestos_Planilla_Entrega
(
	@par_EMPR_Codigo NUMERIC,
	@par_ENPD_Numero NUMERIC
)
AS 
BEGIN

	SELECT 
	ENIM.Nombre,
	DIPD.Valor_Tarifa,
	DIPD.Valor_Base,
	DIPD.Valor_Impuesto

	FROM Detalle_Impuestos_Planilla_Despachos DIPD 

	LEFT JOIN Encabezado_Impuestos ENIM ON
	DIPD.EMPR_Codigo = ENIM.EMPR_Codigo
	AND DIPD.ENIM_Codigo = ENIM.Codigo

	WHERE DIPD.EMPR_Codigo = @par_EMPR_Codigo
	AND DIPD.ENPD_Numero = @par_ENPD_Numero
	AND DIPD.Valor_Impuesto > 0

END
GO