﻿PRINT 'gsp_Reporte_Detalle_Planilla_Guias_Recolecciones'
GO
DROP PROCEDURE gsp_Reporte_Detalle_Planilla_Guias_Recolecciones
GO 
CREATE PROCEDURE gsp_Reporte_Detalle_Planilla_Guias_Recolecciones
(
	@par_EMPR_Codigo NUMERIC,
	@par_ENPD_Numero NUMERIC
)
AS 
BEGIN
	SELECT 
	ENRE.Numero_Documento,
	ENRE.Fecha,
	ISNULL(CLIE.Razon_Social,'')+' '+ISNULL(CLIE.Nombre,'') +' '+ISNULL(CLIE.Apellido1,'')+' '+ISNULL(CLIE.Apellido2,'') AS NombreCliente,
	ISNULL(REMI.Razon_Social,'')+' '+ISNULL(REMI.Nombre,'') +' '+ISNULL(REMI.Apellido1,'')+' '+ISNULL(REMI.Apellido2,'') AS NombreRemitente,
	ISNULL(DEST.Razon_Social,'')+' '+ISNULL(DEST.Nombre,'') +' '+ISNULL(DEST.Apellido1,'')+' '+ISNULL(DEST.Apellido2,'') AS NombreDestinatario,
	ISNULL(CDES.Nombre, '') AS CiudadDestino,
	ENRE.Barrio_Destinatario,
	ENRE.Direccion_Destinatario,
	ENRE.Telefonos_Destinatario,
	PRTR.Nombre,
	ENRE.Cantidad_Cliente,
	ENRE.Peso_Cliente,
	ENRE.Observaciones
	
	FROM Detalle_Planilla_Despachos DEPD, Encabezado_Remesas ENRE

	LEFT JOIN Terceros CLIE ON
	ENRE.EMPR_Codigo = CLIE.EMPR_Codigo
	AND ENRE.TERC_Codigo_Cliente = CLIE.Codigo

	LEFT JOIN Terceros REMI ON
	ENRE.EMPR_Codigo = REMI.EMPR_Codigo
	AND ENRE.TERC_Codigo_Remitente = REMI.Codigo

	LEFT JOIN Terceros DEST ON
	ENRE.EMPR_Codigo = DEST.EMPR_Codigo
	AND ENRE.TERC_Codigo_Destinatario = DEST.Codigo

	LEFT JOIN Ciudades CDES ON
	ENRE.EMPR_Codigo = CDES.EMPR_Codigo
	AND ENRE.CIUD_Codigo_Destinatario = CDES.Codigo

	LEFT JOIN Producto_Transportados AS PRTR ON
	ENRE.EMPR_Codigo = PRTR.EMPR_Codigo
	AND ENRE.PRTR_Codigo = PRTR.Codigo

	WHERE DEPD.EMPR_Codigo = @par_EMPR_Codigo
	and DEPD.ENPD_Numero = @par_ENPD_Numero
	AND DEPD.ENRE_Numero = ENRE.Numero
END
GO