﻿PRINT 'gsp_Reporte_Detalle_Recolecciones'
GO
DROP PROCEDURE gsp_Reporte_Detalle_Recolecciones
GO
CREATE PROCEDURE gsp_Reporte_Detalle_Recolecciones(  
@par_EMPR_Codigo NUMERIC,    
@par_Numero_Planilla  NUMERIC
) 
  
  AS        
  BEGIN  
  SELECT DISTINCT
     
  DPRE.EMPR_Codigo AS EMPR_Codigo_Recolecciones
  ,DPRE.ENPR_Numero AS ENPR_Numero_Recolecciones
  ,DPRE.EREC_Numero AS EREC_Numero_Recolecciones
  ,EREC.Numero_Documento AS Numero_Documento_Recolecciones     
  ,EREC.Fecha AS Fecha_Recolecciones     
  ,ISNULL(EREC.TERC_Codigo_Cliente, 0) AS TERC_Codigo_Cliente_Recolecciones   
  ,ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1, '')+ ' ' + ISNULL(TERC.Apellido2,'')+ ' ' + ISNULL(TERC.Razon_Social,'') AS Nombre_Cliente_Recolecciones            
  ,ISNULL(EREC.Nombre_Contacto,'') AS Nombre_Contacto_Recolecciones       
  ,ISNULL(ZOCI.Codigo, 0) AS Codigo_Zona_Recolecciones      
  ,ISNULL(ZOCI.Nombre,'') AS Nombre_Zona_Recolecciones
  ,ISNULL(CIUD.Codigo, 0) AS Codigo_Ciudad_Recolecciones  
  ,ISNULL(CIUD.Nombre,'') AS Nombre_Ciudad_Recolecciones  
  ,ISNULL(EREC.Barrio, '') AS Barrio_Recolecciones        
  ,ISNULL(EREC.Direccion, '') AS Direccion_Recolecciones        
  ,ISNULL(EREC.Telefonos, 0) AS Telefonos_Recolecciones    
  ,ISNULL(EREC.Mercancia, '') AS Mercancia_Recolecciones      
  ,ISNULL(UEPT.Codigo, 0) AS UEPT_Codigo_Recolecciones     
  ,ISNULL(UEPT.Descripcion, '') AS Unidad_Empaque_Recolecciones        
  ,ISNULL(EREC.Cantidad, 0) AS Cantidad_Recolecciones   
  ,ISNULL(EREC.Peso, 0) AS Peso_Recolecciones        
  ,ISNULL(EREC.Observaciones, '') AS Observaciones_Recolecciones     
               
  FROM         
    	  
      Detalle_Planilla_Recolecciones DPRE,
	  Encabezado_Recolecciones EREC,
	  Encabezado_Planilla_Recolecciones ENPR,
	  Zona_Ciudades ZOCI,
	  Ciudades CIUD,
	  Terceros TERC,
	  Unidad_Empaque_Producto_Transportados UEPT 
	 
  WHERE

     DPRE.EMPR_Codigo = EREC.EMPR_Codigo                      
     AND DPRE.EREC_Numero = EREC.Numero    
	                    
     AND EREC.EMPR_Codigo = ZOCI.EMPR_Codigo                      
     AND EREC.ZOCI_Codigo = ZOCI.Codigo    
                            
     AND EREC.EMPR_Codigo = CIUD.EMPR_Codigo                      
     AND EREC.CIUD_Codigo = CIUD.Codigo    
                     
     AND EREC.EMPR_Codigo = TERC.EMPR_Codigo                      
     AND EREC.TERC_Codigo_Cliente = TERC.Codigo    
                      
     AND EREC.EMPR_Codigo = UEPT.EMPR_Codigo                      
     AND EREC.UEPT_Codigo = UEPT.Codigo  
	 
	 AND DPRE.EMPR_Codigo = @par_EMPR_Codigo
	 AND DPRE.ENPR_Numero = @par_Numero_Planilla     
      
END  
GO