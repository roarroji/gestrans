﻿	/*Crea:JOhan Sebastian Borda Borda 
	Fecha_Crea:19/03/2021
	Descripción_Crea:
	Modifica:Johan Sebastian Borda Borda
	Fecha_Modifica: 
	Descripción_Modifica:
	Funciones_Repositorio: 
	Funciones_FrontEnd: */

PRINT 'gsp_reporte_detalle_legalizacion_recaudo_guias'
GO

DROP PROCEDURE gsp_reporte_detalle_legalizacion_recaudo_guias
GO

CREATE PROCEDURE gsp_reporte_detalle_legalizacion_recaudo_guias
(                
 @EMPR_Codigo smallint,        
 @ELRG_Numero numeric  
)                
AS                 
BEGIN                
 SELECT  
 DLRG.ENRE_Numero,            
 DLRG.Numero_Documento_Remesa,            
 DLRG.Fecha_Pago,  
 DLRG.CATA_FPVE_Codigo,  
 FPVE.Campo1 AS CATA_FPVE_Nombre,  
 DLRG.CATA_FPDC_Codigo,  
 FPDC.Campo1 AS CATA_FPDC_Nombre,  
 IIF(DLRG.Legalizo = 1, 'SI', 'NO') AS Legalizo,  
 DLRG.Valor,  
 DLRG.Documento_Pago,  
 DLRG.Observaciones,  
 DLRG.ENPD_Numero,  
 ENPD.Numero_Documento AS NumeroDocumentoPlanilla,  
 ELGU.Numero_Documento AS NumeroDocumentoLegalizarGuias  
  
 FROM Detalle_Legalizacion_Recaudo_Guias DLRG  
  
 LEFT JOIN Valor_Catalogos FPVE ON   
 DLRG.EMPR_Codigo = FPVE.EMPR_Codigo  
 AND DLRG.CATA_FPVE_Codigo = FPVE.Codigo  
  
 LEFT JOIN Valor_Catalogos FPDC ON   
 DLRG.EMPR_Codigo = FPDC.EMPR_Codigo  
 AND DLRG.CATA_FPDC_Codigo = FPDC.Codigo  
  
 LEFT JOIN Encabezado_Planilla_Despachos ENPD ON   
 DLRG.EMPR_Codigo = ENPD.EMPR_Codigo  
 AND DLRG.ENPD_Numero = ENPD.Numero  
  
 LEFT JOIN Encabezado_Legalizacion_Guias ELGU ON   
 DLRG.EMPR_Codigo = ELGU.EMPR_Codigo  
 AND DLRG.ELGU_Numero = ELGU.Numero  
  
 WHERE          
 DLRG.EMPR_CODIGO = @EMPR_Codigo                
 AND DLRG.ELRG_Numero = @ELRG_Numero  
  
END   
GO
