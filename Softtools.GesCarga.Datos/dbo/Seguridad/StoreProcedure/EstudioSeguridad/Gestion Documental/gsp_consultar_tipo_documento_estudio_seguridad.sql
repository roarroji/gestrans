﻿PRINT 'gsp_consultar_tipo_documento_estudio_seguridad'
GO
DROP PROCEDURE gsp_consultar_tipo_documento_estudio_seguridad
GO
CREATE PROCEDURE gsp_consultar_tipo_documento_estudio_seguridad 
(    
@par_EMPR_Codigo SMALLINT,    
@par_Codigo SMALLINT = NULL,  
@par_EOES_Codigo SMALLINT = NULL   
)    
AS    
BEGIN    
 SELECT    
  TDES.EMPR_Codigo,      
  TDES.Codigo,    
  TDES.EOES_Codigo,   
  ISNULL(TDES.Nombre, '') AS Nombre,   
  ISNULL(TDES.Aplica_Referencia, '') AS Aplica_Referencia,  
  ISNULL(TDES.Aplica_Emisor, '') AS Aplica_Emisor,  
  ISNULL(TDES.Aplica_Fecha_Emision, '') AS Aplica_Fecha_Emision,  
  ISNULL(TDES.Aplica_Fecha_Vence, '') AS Aplica_Fecha_Vence   
 FROM    
  Tipo_Documento_Estudio_Seguridad AS TDES  
 WHERE    
  TDES.EMPR_Codigo = @par_EMPR_Codigo      
  AND TDES.Codigo = ISNULL(@par_Codigo, TDES.Codigo)    
  AND TDES.EOES_Codigo = ISNULL(@par_EOES_Codigo, TDES.EOES_Codigo)    
END    
GO