﻿PRINT 'gsp_eliminar_t_estudio_seguridad_documentos'
GO
DROP PROCEDURE gsp_eliminar_t_estudio_seguridad_documentos
GO
CREATE PROCEDURE gsp_eliminar_t_estudio_seguridad_documentos 
(      
  @par_EMPR_Codigo SMALLINT,  
  @par_USUA_Codigo SMALLINT,              
  @par_TDES_Codigo NUMERIC  
)      
AS      
 BEGIN      
   DELETE T_Estudio_Seguridad_Documentos       
   WHERE       
   EMPR_Codigo =  @par_EMPR_Codigo      
   AND USUA_Codigo = @par_USUA_Codigo    
   AND TDES_Codigo = @par_TDES_Codigo  
                
END      
GO