﻿PRINT 'gsp_insertar_detalle_documentos_estudio_seguridad'
GO
DROP PROCEDURE gsp_insertar_detalle_documentos_estudio_seguridad
GO
CREATE PROCEDURE gsp_insertar_detalle_documentos_estudio_seguridad
(  
@par_EMPR_Codigo SMALLINT ,  
 @par_Numero NUMERIC,  
 @par_TDES_Codigo NUMERIC, 
 @par_EOES_Codigo NUMERIC, 
 @par_Referencia VARCHAR(50) = NULL,  
 @par_Emisor VARCHAR(50) =NULL,  
 @par_Fecha_Emision DATETIME =NULL,  
 @par_Fecha_Vence DATETIME= NULL, 
 @par_USUA_Codigo_Crea SMALLINT  
)  
as  
begin  
IF EXISTS(SELECT ENES_Numero FROM Estudio_Seguridad_Documentos WHERE EMPR_Codigo = @par_EMPR_Codigo AND TDES_Codigo = @par_TDES_Codigo AND ENES_Numero = @par_Numero )   
 BEGIN   
   UPDATE Estudio_Seguridad_Documentos  
     SET Referencia = @par_Referencia  
       ,Emisor = @par_Emisor  
       ,Fecha_Emision = @par_Fecha_Emision  
       ,Fecha_Vence = @par_Fecha_Vence  
       ,USUA_Modifica = @par_USUA_Codigo_Crea  
       ,Fecha_Modifica = GETDATE()  
       WHERE EMPR_Codigo = @par_EMPR_Codigo   
       AND TDES_Codigo = @par_TDES_Codigo   
       AND ENES_Numero = @par_Numero   
  END  
 ELSE  
  BEGIN  
   INSERT INTO Estudio_Seguridad_Documentos
   (  
        EMPR_Codigo
       ,ENES_Numero
	   ,TDES_Codigo
	   ,EOES_Codigo
       ,Referencia 
       ,Emisor 
       ,Fecha_Emision
       ,Fecha_Vence
       ,USUA_Crea
       ,Fecha_Crea
	   )
	   VALUES(
	   @par_EMPR_Codigo
	   ,@par_Numero
	   ,@par_TDES_Codigo
	   ,@par_EOES_Codigo
	   ,@par_Referencia  
       ,@par_Emisor  
       ,@par_Fecha_Emision  
       ,@par_Fecha_Vence  
       ,@par_USUA_Codigo_Crea  
       ,GETDATE()  
	   ) 
  END  
 END 

select @@ROWCOUNT as RegistrosAfectados
 
GO