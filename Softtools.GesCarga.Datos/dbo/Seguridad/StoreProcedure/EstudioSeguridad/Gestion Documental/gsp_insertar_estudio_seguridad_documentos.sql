﻿PRINT 'gsp_insertar_estudio_seguridad_documentos'
GO
DROP PROCEDURE gsp_insertar_estudio_seguridad_documentos
GO
CREATE PROCEDURE gsp_insertar_estudio_seguridad_documentos 
(        
  @par_USUA_Codigo SMALLINT,        
  @par_EMPR_Codigo SMALLINT,              
  @par_ENES_Numero NUMERIC = NULL,        
  @par_EOES_Codigo NUMERIC = NULL,         
  @par_TDES_Codigo NUMERIC = NULL,                  
  @par_Referencia VARCHAR(50) = NULL,       
  @par_Emisor VARCHAR(50) = NULL,        
  @par_Fecha_Emision DATE = NULL,       
  @par_Fecha_Vence DATE = NULL,       
  @par_Documento VARBINARY(MAX) = NULL,               
  @par_Nombre_Documento VARCHAR(50) = NULL,            
  @par_Extension_Documento CHAR(5) = NULL 
)          
AS          
BEGIN     
  
 DECLARE @ValorCondicionDefinitivo INT    
 SELECT @ValorCondicionDefinitivo = (    
    
 SELECT DISTINCT     
 COUNT(*)     
 FROM    
     
 Estudio_Seguridad_Documentos AS TESD   
    
 WHERE    
    
 TESD.EMPR_Codigo = @par_EMPR_Codigo                    
 AND TESD.ENES_Numero = @par_ENES_Numero    
 AND TESD.TDES_Codigo = @par_TDES_Codigo )   

 DECLARE @ValorCondicion INT    
 SELECT @ValorCondicion = (    
    
 SELECT DISTINCT     
 COUNT(*)     
 FROM    
     
 T_Estudio_Seguridad_Documentos AS TESD   
    
 WHERE    
    
 TESD.EMPR_Codigo = @par_EMPR_Codigo           
 AND TESD.USUA_Codigo = @par_USUA_Codigo    
 AND TESD.TDES_Codigo = @par_TDES_Codigo )   

	  IF (@ValorCondicionDefinitivo = 0)
		 BEGIN
      
		   IF (@ValorCondicion = 0)   
		           BEGIN    
    
					  INSERT INTO          
					  T_Estudio_Seguridad_Documentos       
					  (      
					  USUA_Codigo,        
					  EMPR_Codigo,     
					  ENES_Numero,      
					  EOES_Codigo,      
					  TDES_Codigo,       
					  Referencia,      
					  Emisor,      
					  Fecha_Emision,      
					  Fecha_Vence,      
					  Documento,      
					  Nombre_Documento,      
					  Extension_Documento    
					  )          
					  VALUES          
					  (      
					  @par_USUA_Codigo,       
					  @par_EMPR_Codigo,            
					  @par_ENES_Numero,        
					  @par_EOES_Codigo,      
					  @par_TDES_Codigo,            
					  ISNULL(@par_Referencia, ''),        
					  ISNULL(@par_Emisor, ''),             
					  ISNULL(@par_Fecha_Emision, ''),       
					  ISNULL(@par_Fecha_Vence, ''),            
					  @par_Documento,            
					  ISNULL(@par_Nombre_Documento, NULL),            
					  ISNULL(@par_Extension_Documento, NULL)       
					  )               
					  SELECT @@ROWCOUNT AS Codigo  
		      
                 END   
           ELSE IF (@ValorCondicion > 0)         
                   BEGIN    
        
					  IF (@par_Documento IS NULL)   
						  BEGIN   
  
								UPDATE          
								T_Estudio_Seguridad_Documentos       
								SET                       
								Referencia = ISNULL(@par_Referencia, ''),      
								Emisor = ISNULL(@par_Emisor, ''),      
								Fecha_Emision = ISNULL(@par_Fecha_Emision, ''),      
								Fecha_Vence = ISNULL(@par_Fecha_Vence, '')
								WHERE    
								EMPR_Codigo = @par_EMPR_Codigo           
								AND USUA_Codigo = @par_USUA_Codigo    
								AND TDES_Codigo = @par_TDES_Codigo       
								SELECT @@ROWCOUNT AS Codigo
			       
						   END
					  ELSE
						   BEGIN
			   					UPDATE          
				 				T_Estudio_Seguridad_Documentos       
				 				SET                 
								Referencia = ISNULL(@par_Referencia, ''),      
								Emisor = ISNULL(@par_Emisor, ''),      
								Fecha_Emision = ISNULL(@par_Fecha_Emision, ''),      
								Fecha_Vence = ISNULL(@par_Fecha_Vence, ''),      
								Documento = @par_Documento,      
								Nombre_Documento = ISNULL(@par_Nombre_Documento, NULL),      
								Extension_Documento = ISNULL(@par_Extension_Documento, NULL)  
								WHERE    
								EMPR_Codigo = @par_EMPR_Codigo           
								AND USUA_Codigo = @par_USUA_Codigo    
								AND TDES_Codigo = @par_TDES_Codigo
								SELECT @@ROWCOUNT AS Codigo
					       END			
			     
                   END
	      END
      ELSE
	     BEGIN

		 IF (@ValorCondicion > 0)         
                   BEGIN    
        
					  IF (@par_Documento IS NULL)   
						  BEGIN   
  
								UPDATE          
								T_Estudio_Seguridad_Documentos       
								SET                       
								Referencia = ISNULL(@par_Referencia, ''),      
								Emisor = ISNULL(@par_Emisor, ''),      
								Fecha_Emision = ISNULL(@par_Fecha_Emision, ''),      
								Fecha_Vence = ISNULL(@par_Fecha_Vence, '')
								WHERE    
								EMPR_Codigo = @par_EMPR_Codigo           
								AND USUA_Codigo = @par_USUA_Codigo    
								AND TDES_Codigo = @par_TDES_Codigo       
								SELECT @@ROWCOUNT AS Codigo
			       
						   END
					  ELSE
						   BEGIN
			   					UPDATE          
				 				T_Estudio_Seguridad_Documentos       
				 				SET                 
								Referencia = ISNULL(@par_Referencia, ''),      
								Emisor = ISNULL(@par_Emisor, ''),      
								Fecha_Emision = ISNULL(@par_Fecha_Emision, ''),      
								Fecha_Vence = ISNULL(@par_Fecha_Vence, ''),      
								Documento = @par_Documento,      
								Nombre_Documento = ISNULL(@par_Nombre_Documento, NULL),      
								Extension_Documento = ISNULL(@par_Extension_Documento, NULL)  
								WHERE    
								EMPR_Codigo = @par_EMPR_Codigo           
								AND USUA_Codigo = @par_USUA_Codigo    
								AND TDES_Codigo = @par_TDES_Codigo
								SELECT @@ROWCOUNT AS Codigo
					       END			
			     
                   END
				   ELSE
				   BEGIN

						 INSERT INTO T_Estudio_Seguridad_Documentos      
						(         
						EMPR_Codigo,     
						ENES_Numero,      
						EOES_Codigo,      
						TDES_Codigo,      
						Referencia,      
						Emisor,      
						Fecha_Emision,      
						Fecha_Vence,      
						Documento,      
						Nombre_Documento,      
						Extension_Documento,
						USUA_Codigo 
						)      
						SELECT        
						 EMPR_Codigo,     
						 @par_ENES_Numero,      
						 EOES_Codigo,      
						 TDES_Codigo,      
	 					 Referencia,      
						 Emisor,      
						 Fecha_Emision,      
						 Fecha_Vence,      
						 Documento,      
						 Nombre_Documento,      
	   					 Extension_Documento,    
 	 					 @par_USUA_Codigo 
						 FROM      
						 Estudio_Seguridad_Documentos       
						 WHERE       
						 EMPR_Codigo =  @par_EMPR_Codigo      
						 AND TDES_Codigo = @par_TDES_Codigo  
						 SELECT @@ROWCOUNT AS Codigo  
			    END
	     END
    END
GO  