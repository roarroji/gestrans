﻿PRINT 'gsp_transladar_documento_estudio_seguridad'
GO
DROP PROCEDURE gsp_transladar_documento_estudio_seguridad
GO
CREATE PROCEDURE gsp_transladar_documento_estudio_seguridad 
(        
@par_EMPR_Codigo SMALLINT,        
@par_Numero NUMERIC,        
@par_USUA_Codigo SMALLINT        
)        
AS        
BEGIN     
  
 DECLARE @ValorCondicionDefinitivo INT      
 SELECT @ValorCondicionDefinitivo = (   
      
 SELECT DISTINCT       
 COUNT(*)       
 FROM      
       
 Estudio_Seguridad_Documentos AS TESD     
      
 WHERE      
      
 TESD.EMPR_Codigo = @par_EMPR_Codigo                      
 AND TESD.ENES_Numero = @par_Numero )   
   
   IF (@ValorCondicionDefinitivo > 1)  
        BEGIN  
		   DELETE Estudio_Seguridad_Documentos  WHERE         
		   EMPR_Codigo = @par_EMPR_Codigo                     
		   AND ENES_Numero = @par_Numero    
		   AND TDES_Codigo IN (SELECT TDES_Codigo FROM T_Estudio_Seguridad_Documentos WHERE EMPR_Codigo =  @par_EMPR_Codigo AND USUA_Codigo = @par_USUA_Codigo)  
  
			   INSERT INTO Estudio_Seguridad_Documentos        
			  (           
			  EMPR_Codigo,       
			  ENES_Numero,        
			  EOES_Codigo,        
			  TDES_Codigo,        
			  Referencia,        
			  Emisor,        
			  Fecha_Emision,        
			  Fecha_Vence,        
			  Documento,        
			  Nombre_Documento,        
			  Extension_Documento,      
			  USUA_Crea,      
			  Fecha_Crea      
			  )        
				 SELECT          
				EMPR_Codigo,       
				@par_Numero,        
				EOES_Codigo,        
				TDES_Codigo,        
				Referencia,        
				Emisor,        
				Fecha_Emision,        
				Fecha_Vence,        
				Documento,        
				Nombre_Documento,        
				Extension_Documento,      
				@par_USUA_Codigo,      
				GETDATE()      
				FROM        
				T_Estudio_Seguridad_Documentos         
				WHERE         
				EMPR_Codigo = @par_EMPR_Codigo   
				AND USUA_Codigo = @par_USUA_Codigo    
				SELECT @@ROWCOUNT AS Codigo    
  
   END     

      ELSE
	    BEGIN
		  INSERT INTO Estudio_Seguridad_Documentos        
			  (           
			  EMPR_Codigo,       
			  ENES_Numero,        
			  EOES_Codigo,        
			  TDES_Codigo,        
			  Referencia,        
			  Emisor,        
			  Fecha_Emision,        
			  Fecha_Vence,        
			  Documento,        
			  Nombre_Documento,        
			  Extension_Documento,      
			  USUA_Crea,      
			  Fecha_Crea      
			  )        
				 SELECT          
				EMPR_Codigo,       
				@par_Numero,        
				EOES_Codigo,        
				TDES_Codigo,        
				Referencia,        
				Emisor,        
				Fecha_Emision,        
				Fecha_Vence,        
				Documento,        
				Nombre_Documento,        
				Extension_Documento,      
				@par_USUA_Codigo,      
				GETDATE()      
				FROM        
				T_Estudio_Seguridad_Documentos         
				WHERE         
				EMPR_Codigo = @par_EMPR_Codigo    
				AND USUA_Codigo = @par_USUA_Codigo    
				SELECT @@ROWCOUNT AS Codigo  
		END        
END  
GO  