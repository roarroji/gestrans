﻿PRINT 'gsp_consultar_listas_entidades_consultadas'
GO
DROP PROCEDURE gsp_consultar_listas_entidades_consultadas
GO
CREATE PROCEDURE gsp_consultar_listas_entidades_consultadas   
(@par_EMPR_Codigo SMALLINT,    
@par_Numero_Estudio NUMERIC)    
    
AS  
BEGIN  
SELECT      
ECES.EMPR_Codigo    
,ECES.ENES_Numero    
,ECES.CATA_ECES_Codigo    
,ECES.Observaciones   
,ECES.Estado 
,VACA.Campo1 AS Nombre_Entidad_Consultada    
    
FROM Detalle_Entidades_Consultadas_Estudio_Seguridad ECES     
    
LEFT JOIN Valor_Catalogos VACA ON    
ECES.EMPR_Codigo = VACA.EMPR_Codigo   
AND ECES.CATA_ECES_Codigo = VACA.Codigo   
    
WHERE     
ECES.EMPR_Codigo = @par_EMPR_Codigo    
AND ECES.ENES_Numero = @par_Numero_Estudio    
    
END    
GO