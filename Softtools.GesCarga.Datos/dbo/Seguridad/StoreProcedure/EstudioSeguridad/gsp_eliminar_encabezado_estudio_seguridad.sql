﻿PRINT 'gsp_eliminar_encabezado_estudio_seguridad'
GO
DROP PROCEDURE gsp_eliminar_encabezado_estudio_seguridad
GO
CREATE PROCEDURE gsp_eliminar_encabezado_estudio_seguridad( 
  @par_EMPR_Codigo SMALLINT,    
  @par_Numero NUMERIC    
  )    
AS    
BEGIN    
      
  UPDATE Encabezado_Estudio_Seguridad SET Anulado = 1  
      
  WHERE EMPR_Codigo = @par_EMPR_Codigo     
    AND Numero = @par_Numero    
    
    SELECT @@ROWCOUNT AS Numero    
    
END    
GO