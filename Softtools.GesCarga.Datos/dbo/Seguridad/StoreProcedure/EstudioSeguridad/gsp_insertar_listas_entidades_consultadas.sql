﻿PRINT 'gsp_insertar_listas_entidades_consultadas'
GO
DROP PROCEDURE gsp_insertar_listas_entidades_consultadas
GO
CREATE PROCEDURE gsp_insertar_listas_entidades_consultadas   
(@par_EMPR_Codigo SMALLINT,    
@par_Numero_Estudio NUMERIC,    
@par_Entidad_Consultada NUMERIC,  
@par_Estado SMALLINT,  
@par_Observaciones VARCHAR(1000) = NULL 
)    
AS   
BEGIN    
    
INSERT INTO [dbo].[Detalle_Entidades_Consultadas_Estudio_Seguridad]    
           ([EMPR_Codigo]    
           ,[ENES_Numero]
		   ,[CATA_ECES_Codigo]
		   ,[Estado]
		   ,[Observaciones]    
         )    
     VALUES    
           (@par_EMPR_Codigo    
           ,@par_Numero_Estudio    
           ,@par_Entidad_Consultada 
		   ,@par_Estado   
           ,@par_Observaciones)    
END  
GO