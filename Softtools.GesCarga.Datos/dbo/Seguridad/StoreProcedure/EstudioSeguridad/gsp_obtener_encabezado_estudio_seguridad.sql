﻿PRINT 'gsp_obtener_encabezado_estudio_seguridad'
GO
DROP PROCEDURE gsp_obtener_encabezado_estudio_seguridad
GO
CREATE PROCEDURE gsp_obtener_encabezado_estudio_seguridad 
(        
@par_EMPR_Codigo SMALLINT,        
@par_Numero NUMERIC = NULL    
)        
AS        
BEGIN        
 SELECT   
  0 AS Codigos_Terceros,    
  1 AS Obtener,     
  ENES.EMPR_Codigo,        
  ENES.Numero,        
  ENES.Numero_Documento,           
  ISNULL(ENES.Fecha,'') AS Fecha,    
  ISNULL(ENES.Placa, 0) AS Placa,     
  ENES.CATA_ESES_Codigo AS Estado_Solicitud,     
  ENES.Estado,     
  ENES.Numero_Autorizacion,     
  ISNULL(ENES.OFIC_Codigo, 0) AS OFIC_Codigo,         
  ISNULL(OFIC.Nombre, '') AS Nombre_Oficina,      
  ENES.CATA_CRES_Codigo,    
  ISNULL(ENES.Observaciones_Estudio_Seguridad,'') AS Observaciones_Estudio_Seguridad,    
  ISNULL(ENES.MAVE_Codigo, 0) AS MAVE_Codigo,      
  ISNULL(ENES.CATA_TIVE_Codigo, 0) AS CATA_TIVE_Codigo,    
  ISNULL(ENES.Modelo, 0) AS Modelo_Vehiculo,      
  ISNULL(ENES.Modelo_Repotenciado, 0) AS Modelo_Repotenciado,              
  ISNULL(ENES.COVE_Codigo, 0) AS COVE_Codigo,    
  ISNULL(ENES.TERC_Aseguradora_Seguro_Obligatorio, 0) AS TERC_Aseguradora_Seguro_Obligatorio,           
  ISNULL(ASEG.Nombre,'') AS Nombre_Aseguradora,    
  ISNULL(ENES.Numero_Seguro_Obligatorio, '') AS Numero_Seguro_Obligatorio,          
  ISNULL(ENES.Fecha_Emision_Seguro_Obligatorio, '') AS Fecha_Emision_Seguro_Obligatorio,    
  ISNULL(ENES.Fecha_Vence_Seguro_Obligatorio, '') AS Fecha_Vence_Seguro_Obligatorio,      
  ISNULL(ENES.Numero_Revision_Tecnomecanica, 0) AS Numero_Revision_Tecnomecanica,    
  ISNULL(ENES.Fecha_Emision_Revision_Tecnomecanica, '') AS Fecha_Emision_Revision_Tecnomecanica,           
  ISNULL(ENES.Fecha_Vence_Revision_Tecnomecanica, '') AS Fecha_Vence_Revision_Tecnomecanica,    
  ISNULL(ENES.Nombre_Empresa_GPS, '') AS Nombre_Empresa_GPS,           
  ISNULL(ENES.Telefono_Empresa_GPS,'') AS Telefono_Empresa_GPS,    
  ISNULL(ENES.Usuario_GPS, '') AS Usuario_GPS,           
  ISNULL(ENES.Clave_GPS, '') AS Clave_GPS,       
  ISNULL(ENES.Empresa_Afiliadora, '') AS Empresa_Afiliadora,    
  ISNULL(ENES.Telefono_Afiliadora, '') AS Telefono_Afiliadora,           
  ISNULL(ENES.Numero_Carnet_Afiliadora, '') AS Numero_Carnet_Afiliadora,    
  ISNULL(ENES.Fecha_Vence_Afiliadora, '') AS Fecha_Vence_Afiliadora, 
  
  ISNULL(ENES.Placa_Semirremolque, '') AS Placa_Semirremolque,
  ISNULL(ENES.MASE_Codigo, 0) AS MASE_Codigo,
  ISNULL(ENES.CATA_TISE_Codigo, 0) AS CATA_TISE_Codigo,
  ISNULL(ENES.CATA_TICA_Codigo, 0) AS CATA_TICA_Codigo,
  ISNULL(ENES.Modelo_Semirremolque, 0) AS Modelo_Semirremolque,
  ISNULL(ENES.Numero_Ejes, 0) AS Numero_Ejes, 
              
  ISNULL(ENES.CATA_TINT_Propietario, 0) AS CATA_TINT_Propietario,    
  ISNULL(ENES.CATA_TIID_Propietario, 0) AS CATA_TIID_Propietario,           
  ISNULL(ENES.Numero_Identificacion_Propietario, '') AS Numero_Identificacion_Propietario,
  ISNULL(ENES.Nombre_Propietario, '') AS Nombre_Propietario,       
  ISNULL(ENES.Apellido1_Propietario, '') AS Apellido1_Propietario,       
  ISNULL(ENES.Apellido2_Propietario, '') AS Apellido2_Propietario,   
  ISNULL(ENES.Razon_Social_Propietario, '') AS Razon_Social_Propietario,        
  ISNULL(ENES.Digito_Chequeo_Propietario,0) AS Digito_Chequeo_Propietario,       
  ISNULL(ENES.CIUD_Identificacion_Propietario, 0) AS CIUD_Identificacion_Propietario,    
  ISNULL(ENES.CIUD_Residencia_Propietario, 0) AS CIUD_Residencia_Propietario,           
  ISNULL(ENES.Direccion_Propietario, 0) AS Direccion_Propietario,       
  ISNULL(ENES.Telefono_Propietario, 0) AS Telefono_Propietario,        
  ISNULL(ENES.Celular_Propietario, 0) AS Celular_Propietario,    

  ISNULL(ENES.CATA_TINT_Propietario_Remolque, 0) AS CATA_TINT_Propietario_Remolque,    
  ISNULL(ENES.CATA_TIID_Propietario_Remolque, 0) AS CATA_TIID_Propietario_Remolque,           
  ISNULL(ENES.Numero_Identificacion_Propietario_Remolque, '') AS Numero_Identificacion_Propietario_Remolque,       
  ISNULL(ENES.Nombre_Propietario_Remolque, '') AS Nombre_Propietario_Remolque,    
  ISNULL(ENES.Apellido1_Propietario_Semirremolque, '') AS Apellido1_Propietario_Remolque,  
  ISNULL(ENES.Apellido2_Propietario_Semirremolque, '') AS Apellido2_Propietario_Remolque,  
  ISNULL(ENES.Razon_Social_Propietario_Semirremolque, '') AS Razon_Social_Propietario_Remolque,      
  ISNULL(ENES.Digito_Chequeo_Propietario_Remolque, 0) AS Digito_Chequeo_Propietario_Remolque,       
  ISNULL(ENES.CIUD_Identificacion_Propietario_Remolque, 0) AS CIUD_Identificacion_Propietario_Remolque,    
  ISNULL(ENES.CIUD_Residencia_Propietario_Remolque, 0) AS CIUD_Residencia_Propietario_Remolque,           
  ISNULL(ENES.Direccion_Propietario_Remolque, 0) AS Direccion_Propietario_Remolque,       
  ISNULL(ENES.Telefono_Propietario_Remolque, 0) AS Telefono_Propietario_Remolque,        
  ISNULL(ENES.Celular_Propietario_Remolque, 0) AS Celular_Propietario_Remolque, 
     
  ISNULL(ENES.CATA_TINT_Tenedor, 0) AS CATA_TINT_Tenedor,    
  ISNULL(ENES.CATA_TIID_Tenedor, 0) AS CATA_TIID_Tenedor,          
  ISNULL(ENES.Numero_Identificacion_Tenedor, '') AS Numero_Identificacion_Tenedor,       
  ISNULL(ENES.Nombre_Tenedor, '') AS Nombre_Tenedor,
  ISNULL(ENES.Apellido1_Tenedor, '') AS Apellido1_Tenedor,
  ISNULL(ENES.Apellido2_Tenedor, '') AS Apellido2_Tenedor,
  ISNULL(ENES.Razon_Social_Tenedor, '') AS Razon_Social_Tenedor,        
  ISNULL(ENES.Digito_Chequeo_Tenedor,0) AS Digito_Chequeo_Tenedor,       
  ISNULL(ENES.CIUD_Identificacion_Tenedor, 0) AS CIUD_Identificacion_Tenedor,    
  ISNULL(ENES.CIUD_Residencia_Tenedor, 0) AS CIUD_Residencia_Tenedor,           
  ISNULL(ENES.Direccion_Tenedor, 0) AS Direccion_Tenedor,       
  ISNULL(ENES.Telefono_Tenedor, 0) AS Telefono_Tenedor,        
  ISNULL(ENES.Celular_Tenedor, 0) AS Celular_Tenedor,  
     
  ISNULL(ENES.CATA_TINT_Conductor, 0) AS CATA_TINT_Conductor,    
  ISNULL(ENES.CATA_TIID_Conductor, 0) AS CATA_TIID_Conductor,           
  ISNULL(ENES.Numero_Identificacion_Conductor, '') AS Numero_Identificacion_Conductor,       
  ISNULL(ENES.Nombre_Conductor, '') AS Nombre_Conductor,  
  ISNULL(ENES.Apellido1_Conductor, '') AS Apellido1_Conductor,
  ISNULL(ENES.Apellido2_Conductor, '') AS Apellido2_Conductor,
  ISNULL(ENES.Razon_Social_Conductor, '') AS Razon_Social_Conductor,	      
  ISNULL(ENES.Digito_Chequeo_Conductor,0) AS Digito_Chequeo_Conductor,       
  ISNULL(ENES.CIUD_Identificacion_Conductor, 0) AS CIUD_Identificacion_Conductor,    
  ISNULL(ENES.CIUD_Residencia_Conductor, 0) AS CIUD_Residencia_Conductor,           
  ISNULL(ENES.Direccion_Conductor, 0) AS Direccion_Conductor,       
  ISNULL(ENES.Telefono_Conductor, 0) AS Telefono_Conductor,        
  ISNULL(ENES.Celular_Conductor, 0) AS Celular_Conductor, 
     
  ISNULL(ENES.Nombre_Cliente, '') AS Nombre_Cliente,    
  ISNULL(ENES.Nombre_Mercancia_Cliente, '') AS Nombre_Mercancia_Cliente,           
  ISNULL(ENES.RUTA_Codigo_Cliente, 0) AS RUTA_Codigo_Cliente,     
  '' AS Nombre_Ruta,      
  ISNULL(ENES.Valor_Mercancia_Cliente, 0) AS Valor_Mercancia_Cliente,        
  ISNULL(ENES.Observaciones_Despacho_Cliente, '') AS Observaciones_Despacho_Cliente     
    
 FROM       
      
  Encabezado_Estudio_Seguridad AS ENES     
      
  INNER JOIN Oficinas AS OFIC ON      
  ENES.EMPR_Codigo = OFIC.EMPR_Codigo      
  AND ENES.OFIC_Codigo = OFIC.Codigo     
      
  LEFT JOIN Terceros AS ASEG ON      
  ENES.EMPR_Codigo = ASEG.EMPR_Codigo      
  AND ENES.TERC_Aseguradora_Seguro_Obligatorio = ASEG.Codigo      
         
 WHERE        
  ENES.EMPR_Codigo = @par_EMPR_Codigo      
  AND ENES.Numero = @par_Numero    
  END     
GO