﻿
Print 'gsp_modificar_usuarios'
GO
DROP PROCEDURE gsp_modificar_usuarios
GO
CREATE PROCEDURE gsp_modificar_usuarios        
(        
@par_EMPR_Codigo SMALLINT,        
@par_Codigo SMALLINT,        
@par_Codigo_Usuario VARCHAR(10) = NULL,        
@par_Codigo_Alterno VARCHAR(20) = NULL,        
@par_Nombre VARCHAR(50)= NULL,        
@par_Descripcion VARCHAR(100) = NULL,        
@par_Habilitado SMALLINT,        
@par_Clave VARCHAR(10),        
@par_Dias_Cambio_Clave SMALLINT,        
@par_Manager SMALLINT = NULL,        
@par_Login SMALLINT,        
@par_CATA_APLI_Codigo NUMERIC,        
@par_TERC_Codigo_Empleado NUMERIC = null,        
@par_TERC_Codigo_Cliente NUMERIC = null,        
@par_TERC_Codigo_Conductor NUMERIC = null,       
@par_TERC_Codigo_Proveedor NUMERIC = null,       
@par_Intentos_Ingreso SMALLINT ,        
@par_Mensaje_Banner VARCHAR (250) = NULL        
)        
AS        
BEGIN        
UPDATE Usuarios SET         
    
Codigo_Usuario = ISNULL(@par_Codigo_Usuario,  Codigo_Usuario ),    
Codigo_Alterno = ISNULL(@par_Codigo_Alterno,''),        
Nombre = ISNULL(@par_Nombre,  Nombre  ),    
Descripcion =ISNULL( @par_Descripcion,''),            
Clave = @par_Clave,        
Dias_Cambio_Clave = @par_Dias_Cambio_Clave,        
Manager = ISNULL(@par_Manager,  Manager ),     
Login = @par_Login,           
TERC_Codigo_Empleado =ISNULL(@par_TERC_Codigo_Empleado ,  0 ),     
TERC_Codigo_Cliente = ISNULL(@par_TERC_Codigo_Cliente,    0),    
TERC_Codigo_Conductor = ISNULL(@par_TERC_Codigo_Conductor, 0  ) ,    
TERC_Codigo_Proveedor = ISNULL(@par_TERC_Codigo_Proveedor,  0),    
Intentos_Ingreso =@par_Intentos_Ingreso ,        
Mensaje_Banner = ISNULL(@par_Mensaje_Banner,'')  ,    
Fecha_Ultimo_Cambio_Clave = GETDATE()  ,  
Habilitado = ISNULL(@par_Habilitado,0),
CATA_APLI_Codigo = ISNULL(@par_CATA_APLI_Codigo, 0)
WHERE         
EMPR_Codigo = @par_EMPR_Codigo        
AND Codigo = @par_Codigo        
        
SELECT  @par_Codigo AS Codigo        
        
END
GO      