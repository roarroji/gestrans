﻿PRINT 'gsp_guardar_ingreso_usuario'
GO
DROP PROCEDURE gsp_guardar_ingreso_usuario
GO
CREATE PROCEDURE gsp_guardar_ingreso_usuario(
  @par_EMPR_Codigo SMALLINT,
  @par_USUA_Codigo SMALLINT 
  )
AS
BEGIN 
  IF @par_USUA_Codigo = 1
  BEGIN
    UPDATE Usuarios SET Login = 0, Fecha_Ultimo_Ingreso = GETDATE(), Intentos_Ingreso = 0
    WHERE 
	EMPR_Codigo = @par_EMPR_Codigo
    AND Codigo = @par_USUA_Codigo

    SELECT Login FROM Usuarios WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_USUA_Codigo
  END
  ELSE 
	BEGIN 
	UPDATE Usuarios SET Login = 1, Fecha_Ultimo_Ingreso = GETDATE(), Intentos_Ingreso = 0
    WHERE 
	EMPR_Codigo = @par_EMPR_Codigo
    AND Codigo = @par_USUA_Codigo

    SELECT Login FROM Usuarios WHERE EMPR_Codigo = @par_EMPR_Codigo AND Codigo = @par_USUA_Codigo
  END
END
GO