﻿PRINT 'Catalogo Estado Porgramación Orden Servicio - 186 - EPOS'
GO
DELETE Valor_Catalogos WHERE CATA_Codigo = 186
GO
DELETE Configuracion_Catalogos WHERE CATA_Codigo = 186
GO
DELETE Catalogos WHERE Codigo = 186
GO
------------------------------------------CATALOGOS------------------------------------------------------
INSERT INTO Catalogos (EMPR_Codigo, Codigo, Nombre_Corto, Nombre, Actualizable, Numero_Columnas, Numero_Campos_Llave)
SELECT Codigo, 186, 'EPOS','Estado Programación Orden Servicio', 0, 2, 1 FROM Empresas
GO
-----------------------------------------VALOR CATALOGOS--------------------------------------------------
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea)
SELECT Codigo, 186, 18601, 'Pendiente', '', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 186, 18602, 'Incompleta', '', '', '', 1, 1, GETDATE() FROM Empresas
GO
INSERT INTO valor_catalogos (EMPR_Codigo, CATA_Codigo, Codigo, Campo1,Campo2,Campo3,Campo4, Estado, USUA_Codigo_Crea,Fecha_Crea) 
SELECT Codigo, 186, 18603, 'Despachada', '', '', '', 1, 1, GETDATE() FROM Empresas
GO