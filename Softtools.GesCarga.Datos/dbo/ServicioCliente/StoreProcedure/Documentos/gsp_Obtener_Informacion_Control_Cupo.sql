﻿DROP PROCEDURE gsp_Obtener_Informacion_Control_Cupo
GO
CREATE PROCEDURE gsp_Obtener_Informacion_Control_Cupo  
(      
 @par_EMPR_Codigo SMALLINT,      
 @par_TERC_Codigo NUMERIC,      
 @par_SEDE_Codigo NUMERIC = NULL      
)      
AS      
BEGIN      
 Declare @CupoGeneral NUMERIC = 19202      
 Declare @CupoSede NUMERIC = 19203      
 Declare @TipoCupoCliente NUMERIC      
 Declare @NumeroDocumentoCliente NUMERIC      
 Declare @CodigoSede VARCHAR(10);      
      
 Declare @CupoCliente Money = 0      
 Declare @DisponibleERP Money = 0      
 Declare @PedidosPorDespachar Money = 0      
 Declare @RemesasPorFacturar Money = 0      
      
 SELECT @TipoCupoCliente = CATA_TIVC_Codigo, @NumeroDocumentoCliente = Numero_Identificacion       
 FROM Terceros       
 WHERE EMPR_Codigo = @par_EMPR_Codigo and Codigo = @par_TERC_Codigo      
      
 if(@TipoCupoCliente = @CupoGeneral)      
 BEGIN      
 --Codigo Para Produccion      
 --SET @CupoCliente = 0      
 --SET @DisponibleERP = 0      
 SELECT @CupoCliente = MCLCUPOASIG, @DisponibleERP = MCLCUPODISP      
 FROM SSF_IMPOCOMA..UN_MONECLIEN WHERE       
 MCLCOMPANIA='01'      
 and MCLTERCEGENER = @NumeroDocumentoCliente  --nit del cliente      
 and MCLCODIGO='00'  -- siempre código 00      
 END       
      
 if(@TipoCupoCliente = @CupoSede)      
 BEGIN      
 SELECT @CodigoSede = Codigo_Alterno       
 FROM Tercero_Direcciones       
 WHERE EMPR_Codigo = @par_EMPR_Codigo and TERC_Codigo = @par_TERC_Codigo AND Codigo = @par_SEDE_Codigo      
 --Codigo Para Produccion      
 --SET @CupoCliente = 0      
 --SET @DisponibleERP = 0      
 SELECT @CupoCliente = MCLCUPOASIG, @DisponibleERP = MCLCUPODISP      
 FROM SSF_IMPOCOMA..UN_MONECLIEN WHERE       
 MCLCOMPANIA='01'      
 and MCLTERCEGENER = @NumeroDocumentoCliente  --nit del cliente      
 and MCLCODIGO = @CodigoSede      
 END      
      
 --Ordenes Pendientes de despachar      
 --SELECT @PedidosPorDespachar = SUM (DPOS.Valor_Flete_Cliente)      
 --FROM Detalle_Programacion_Orden_Servicios DPOS      
      
 --LEFT JOIN Detalle_Despacho_Orden_Servicios DDOS ON      
 --DPOS.EMPR_Codigo = DDOS.EMPR_Codigo      
 --AND DPOS.ID = DDOS.DPOS_ID      
      
 --LEFT JOIN Encabezado_Programacion_Orden_Servicios EPOS ON      
 --DPOS.EMPR_Codigo = EPOS.EMPR_Codigo      
 --AND DPOS.EPOS_Numero = EPOS.Numero      
      
 --WHERE       
 --DPOS.EMPR_Codigo = @par_EMPR_Codigo      
 --and EPOS.TERC_Codigo_Cliente = @par_TERC_Codigo      
 --AND DDOS.DPOS_ID is null     
 select @PedidosPorDespachar = SUM(ESOSValorTotal) from V_CalculoValorOrdenServicio    
 where EMPR_Codigo = @par_EMPR_Codigo     
 and TERC_Codigo_Cliente = @par_TERC_Codigo      
 and CATA_ESSO_Codigo = 9201 --Pendientes  
 and Estado = 1    
 and Anulado = 0    
    
 --Remesas Pendientes Facturar      
 SELECT @RemesasPorFacturar = SUM(Total_Flete_Cliente)       
 from Encabezado_Remesas      
 where EMPR_Codigo = @par_EMPR_Codigo      
 AND TERC_Codigo_Cliente = @par_TERC_Codigo      
 AND (ENFA_Numero  = 0 or ENFA_Numero is NULL)      
      
 SELECT       
 @TipoCupoCliente AS TipoCupoCliente,      
 @NumeroDocumentoCliente AS Documento,      
 ISNULL(@CodigoSede, 0) AS CodigoSede,      
 ISNULL(@CupoCliente, 0) AS CupoCliente,      
 ISNULL(@DisponibleERP, 0) AS DisponibleERP,      
 ISNULL(@PedidosPorDespachar, 0) AS PedidosPorDespachar,      
 ISNULL(@RemesasPorFacturar, 0) AS RemesasPorFacturar      
END  
GO