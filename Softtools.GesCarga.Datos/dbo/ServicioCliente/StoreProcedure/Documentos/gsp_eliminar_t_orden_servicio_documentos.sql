﻿PRINT 'gsp_eliminar_t_orden_servicio_documentos'
GO
DROP PROCEDURE gsp_eliminar_t_orden_servicio_documentos
GO
CREATE PROCEDURE gsp_eliminar_t_orden_servicio_documentos 
(      
  @par_EMPR_Codigo SMALLINT,  
  @par_USUA_Codigo SMALLINT,  
  @par_TIDO_Codigo SMALLINT,              
  @par_CDGD_Codigo NUMERIC   
)      
AS      
 BEGIN      
   DELETE T_Gestion_Documental_Documentos       
   WHERE       
   EMPR_Codigo =  @par_EMPR_Codigo      
   AND USUA_Codigo = @par_USUA_Codigo   
   AND TIDO_Codigo = @par_TIDO_Codigo  
   AND CDGD_Codigo = @par_CDGD_Codigo  
                
END      
GO