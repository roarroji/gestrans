﻿PRINT 'gsp_insertar_detalle_orden_servicio_documentos'
GO
DROP PROCEDURE gsp_insertar_detalle_orden_servicio_documentos
GO
CREATE PROCEDURE gsp_insertar_detalle_orden_servicio_documentos 
(      
@par_EMPR_Codigo SMALLINT ,      
 @par_Numero NUMERIC,      
 @par_TIDO_Codigo NUMERIC,     
 @par_CDGD_Codigo NUMERIC,     
 @par_Referencia VARCHAR(50) = NULL,      
 @par_Emisor VARCHAR(50) =NULL,      
 @par_Fecha_Emision DATETIME =NULL,      
 @par_Fecha_Vence DATETIME= NULL,     
 @par_USUA_Codigo_Crea SMALLINT      
)      
AS
BEGIN    
IF EXISTS(SELECT Numero FROM Gestion_Documental_Documentos WHERE EMPR_Codigo = @par_EMPR_Codigo AND Numero = @par_Numero AND TIDO_Codigo = @par_TIDO_Codigo AND CDGD_Codigo = @par_CDGD_Codigo )       
 BEGIN       
   UPDATE Gestion_Documental_Documentos      
     SET Referencia = @par_Referencia      
       ,Emisor = @par_Emisor      
       ,Fecha_Emision = @par_Fecha_Emision      
       ,Fecha_Vence = @par_Fecha_Vence      
       ,USUA_Codigo_Crea = @par_USUA_Codigo_Crea      
       ,Fecha_Modifica = GETDATE()      
       WHERE EMPR_Codigo = @par_EMPR_Codigo       
       AND TIDO_Codigo = @par_TIDO_Codigo       
    AND CDGD_Codigo = @par_CDGD_Codigo    
       AND Numero = @par_Numero       
  END      
 ELSE      
  BEGIN      
   INSERT INTO Gestion_Documental_Documentos    
   (      
        EMPR_Codigo    
       ,Numero    
    ,TIDO_Codigo    
    ,CDGD_Codigo    
       ,Referencia     
       ,Emisor     
       ,Fecha_Emision    
       ,Fecha_Vence    
       ,USUA_Codigo_Crea    
       ,Fecha_Crea    
    )    
    VALUES(    
    @par_EMPR_Codigo    
    ,@par_Numero    
    ,@par_TIDO_Codigo    
    ,@par_CDGD_Codigo    
    ,@par_Referencia      
       ,@par_Emisor      
       ,@par_Fecha_Emision      
       ,@par_Fecha_Vence      
       ,@par_USUA_Codigo_Crea      
       ,GETDATE()      
    )     
  END      
 END     
    
select @@ROWCOUNT as RegistrosAfectados    
GO
