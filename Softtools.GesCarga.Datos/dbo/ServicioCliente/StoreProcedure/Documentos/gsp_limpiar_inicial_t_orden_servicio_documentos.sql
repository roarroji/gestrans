﻿PRINT 'gsp_limpiar_inicial_t_orden_servicio_documentos'
GO
DROP PROCEDURE gsp_limpiar_inicial_t_orden_servicio_documentos
GO
CREATE PROCEDURE gsp_limpiar_inicial_t_orden_servicio_documentos 
(      
@par_EMPR_Codigo SMALLINT,      
@par_USUA_Codigo SMALLINT        
)      
AS      
BEGIN      
 DELETE T_Gestion_Documental_Documentos       
 WHERE       
 EMPR_Codigo =  @par_EMPR_Codigo      
 AND USUA_Codigo = @par_USUA_Codigo    

 SELECT Codigo = @@ROWCOUNT      
   
END      
GO