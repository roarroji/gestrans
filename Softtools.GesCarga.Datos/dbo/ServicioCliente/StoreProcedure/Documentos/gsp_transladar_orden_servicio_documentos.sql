﻿PRINT 'gsp_transladar_orden_servicio_documentos'
GO
DROP PROCEDURE gsp_transladar_orden_servicio_documentos
GO
CREATE PROCEDURE gsp_transladar_orden_servicio_documentos 
(    
@par_EMPR_Codigo SMALLINT,    
@par_Numero NUMERIC,    
@par_USUA_Codigo SMALLINT    
)    
AS    
BEGIN    
     
 INSERT INTO Gestion_Documental_Documentos    
 (       
EMPR_Codigo,   
Numero,     
TIDO_Codigo,  
CDGD_Codigo,    
Referencia,    
Emisor,    
Fecha_Emision,    
Fecha_Vence,    
Archivo,    
Nombre_Documento,    
Extension_Documento,  
USUA_Codigo_Crea,  
Fecha_Crea  
 )    
SELECT      
 EMPR_Codigo,   
@par_Numero,  
TIDO_Codigo,    
CDGD_Codigo,    
Referencia,    
Emisor,    
Fecha_Emision,    
Fecha_Vence,    
Archivo,    
Nombre_Documento,    
Extension_Documento,  
@par_USUA_Codigo,  
GETDATE()  
 FROM    
 T_Gestion_Documental_Documentos     
 WHERE     
  EMPR_Codigo =  @par_EMPR_Codigo    
  AND USUA_Codigo = @par_USUA_Codigo    
    
SELECT @@ROWCOUNT AS Codigo  
    
END    
GO