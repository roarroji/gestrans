﻿PRINT 'gsp_actualizar_encabezado_documento_cuentas'
GO
DROP PROCEDURE gsp_actualizar_encabezado_documento_cuentas
GO
CREATE PROCEDURE gsp_actualizar_encabezado_documento_cuentas
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo NUMERIC,
@par_Valor_Abono MONEY,
@par_USUA_Codigo_Aprueba SMALLINT
)
AS 
BEGIN

UPDATE Encabezado_Documento_Cuentas
SET
Abono = @par_Valor_Abono + Abono,
Aprobado = 1,
Saldo = Saldo - @par_Valor_Abono,
USUA_Codigo_Aprobo = @par_USUA_Codigo_Aprueba,
Fecha_Aprobo = GETDATE()
WHERE
EMPR_Codigo = @par_EMPR_Codigo
AND Codigo=  @par_Codigo

SELECT @par_Codigo AS Codigo

END
GO