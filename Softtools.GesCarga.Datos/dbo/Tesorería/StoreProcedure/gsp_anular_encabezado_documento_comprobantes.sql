﻿PRINT 'gsp_anular_encabezado_documento_comprobantes'
GO
DROP PROCEDURE gsp_anular_encabezado_documento_comprobantes
GO
CREATE PROCEDURE gsp_anular_encabezado_documento_comprobantes
(
@par_EMPR_Codigo SMALLINT ,
@par_Codigo NUMERIC,
@par_Numero NUMERIC,
@par_TIDO_Codigo NUMERIC,
@par_USUA_Anula SMALLINT,
@par_Causa_Anulacion VARCHAR(150),
@par_OFIC_Anula SMALLINT
)
AS
BEGIN
UPDATE Encabezado_Documento_Comprobantes
SET 
Anulado = 1,
USUA_Codigo_Anula = @par_USUA_Anula,
Fecha_Anula = GETDATE(),
OFIC_Codigo_Anula = @par_OFIC_Anula,
Causa_Anulacion = @par_Causa_Anulacion

WHERE 
EMPR_Codigo = @par_EMPR_Codigo
AND Codigo = @par_Codigo
AND TIDO_Codigo = @par_TIDO_Codigo
AND Numero = @par_Numero


    SELECT @@ROWCOUNT AS Numero

END
GO