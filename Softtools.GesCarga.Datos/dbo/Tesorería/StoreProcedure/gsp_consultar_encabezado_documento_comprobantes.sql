﻿PRINT 'gsp_consultar_encabezado_documento_comprobantes'
GO
DROP PROCEDURE gsp_consultar_encabezado_documento_comprobantes
GO
CREATE PROCEDURE gsp_consultar_encabezado_documento_comprobantes
(    
 @par_EMPR_Codigo SMALLINT,    
 @par_TIDO_Codigo NUMERIC,    
 @par_Numero_Inicial NUMERIC = NULL,
 @par_Numero_Final NUMERIC = NULL,
 @par_Fecha_Inicial DATETIME = NULL,
 @par_Fecha_Final DATETIME = NULL,
 @par_Codigo_Documento NUMERIC = NULL,
 @par_Numero_Documento NUMERIC = NULL,
 @par_Oficina SMALLINT = NULL,
 @par_Tercero VARCHAR (150) = NULL,
 @par_Beneficiario VARCHAR (150) = NULL,
 @par_Estado SMALLINT = NULL,
 @par_Anulado SMALLINT = NULL,        
 @par_NumeroPagina INT = NULL,  
 @par_RegistrosPagina INT = NULL  
)    
AS    
BEGIN    
 SET NOCOUNT ON;    
  DECLARE @CantidadRegistros INT    
  DECLARE @intEstado INT 

  SELECT @CantidadRegistros = (    
    SELECT DISTINCT     
     COUNT(1)     
    FROM    
     Encabezado_Documento_Comprobantes EDCO INNER JOIN Terceros  TERC ON
	 EDCO.EMPR_Codigo = TERC.EMPR_Codigo
	 AND EDCO.TERC_Codigo_Titular = TERC.Codigo

	 LEFT JOIN 
	 Terceros  BENE ON 
	 EDCO.EMPR_Codigo = BENE.EMPR_Codigo
	 AND EDCO.TERC_Codigo_Beneficiario = BENE.Codigo

	 LEFT JOIN 
	 Oficinas  OFIC ON
	 EDCO.EMPR_Codigo = OFIC.EMPR_Codigo
	 AND EDCO.OFIC_Codigo_Destino = OFIC.Codigo

	 LEFT JOIN 
	 V_Documentos_Origen DOOR  ON
	 EDCO.EMPR_Codigo = DOOR.EMPR_Codigo
	 AND EDCO.CATA_DOOR_Codigo = DOOR.Codigo


    WHERE    
		EDCO.EMPR_Codigo = @par_EMPR_Codigo
		AND EDCO.TIDO_Codigo = @par_TIDO_Codigo
		AND ((EDCO.Numero >= @par_Numero_Inicial AND EDCO.Numero <= @par_Numero_Final) OR (@par_Numero_Inicial IS NULL))
		AND ((EDCO.Fecha BETWEEN @par_Fecha_Inicial AND @par_Fecha_Final) OR (@par_Fecha_Inicial IS NULL))
		AND EDCO.Estado = ISNULL(@par_Estado,EDCO.Estado)
		AND EDCO.Anulado = ISNULL(@par_Anulado,EDCO.Anulado)
		AND EDCO.CATA_DOOR_Codigo = ISNULL(@par_Codigo_Documento,EDCO.CATA_DOOR_Codigo)
		AND EDCO.Documento_Origen = ISNULL(@par_Numero_Documento,EDCO.Documento_Origen)
		AND(TERC.Nombre + ' ' +TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Tercero,TERC.Nombre + ' ' +TERC.Apellido1 + ' ' + TERC.Apellido2))), '%'))
		AND(BENE.Nombre + ' ' +BENE.Apellido1 + ' ' + BENE.Apellido2 LIKE CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Beneficiario,BENE.Nombre + ' ' +BENE.Apellido1 + ' ' + BENE.Apellido2))), '%'))
		AND OFIC.Codigo = ISNULL(@par_Oficina,OFIC.Codigo)
     );           
    WITH Pagina AS    
    (    
    
    SELECT    
	EDCO.EMPR_Codigo,
	EDCO.Codigo,
	EDCO.Numero,
	EDCO.TIDO_Codigo,
	EDCO.Codigo_Alterno,
	EDCO.Fecha,
	EDCO.TERC_Codigo_Titular,
	EDCO.TERC_Codigo_Beneficiario,
	EDCO.Observaciones,
	EDCO.CATA_DOOR_Codigo,
	EDCO.CATA_FPDC_Codigo,
	EDCO.CATA_DEIN_Codigo,
	EDCO.Documento_Origen,
	EDCO.Anulado,
	EDCO.Estado,
	EDCO.OFIC_Codigo_Destino,
	ISNULL(BENE.Razon_Social,'') + ' ' + ISNULL(BENE.Nombre,'') + ' ' + ISNULL(BENE.Apellido1,'') + ' ' + ISNULL(BENE.Apellido2,'') As NombreBeneficiario,
	ISNULL(TERC.Razon_Social,'') + ' ' + ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1,'') + ' ' + ISNULL(TERC.Apellido2,'') As NombreTercero,
	OFIC.Nombre AS Oficina,
	DOOR.Nombre AS DocumentoOrigen,
    ROW_NUMBER() OVER(ORDER BY Numero) AS RowNumber    
FROM    
     Encabezado_Documento_Comprobantes EDCO INNER JOIN Terceros  TERC ON
	 EDCO.EMPR_Codigo = TERC.EMPR_Codigo
	 AND EDCO.TERC_Codigo_Titular = TERC.Codigo

	 LEFT JOIN 
	 Terceros  BENE ON 
	 EDCO.EMPR_Codigo = BENE.EMPR_Codigo
	 AND EDCO.TERC_Codigo_Beneficiario = BENE.Codigo

	 LEFT JOIN 
	 Oficinas  OFIC ON
	 EDCO.EMPR_Codigo = OFIC.EMPR_Codigo
	 AND EDCO.OFIC_Codigo_Destino = OFIC.Codigo

	 LEFT JOIN 
	 V_Documentos_Origen DOOR  ON
	 EDCO.EMPR_Codigo = DOOR.EMPR_Codigo
	 AND EDCO.CATA_DOOR_Codigo = DOOR.Codigo


    WHERE    
		EDCO.EMPR_Codigo = @par_EMPR_Codigo
		AND EDCO.TIDO_Codigo = @par_TIDO_Codigo
		AND ((EDCO.Numero >= @par_Numero_Inicial AND EDCO.Numero <= @par_Numero_Final) OR (@par_Numero_Inicial IS NULL))
		AND ((EDCO.Fecha BETWEEN @par_Fecha_Inicial AND @par_Fecha_Final) OR (@par_Fecha_Inicial IS NULL))
		AND EDCO.Estado = ISNULL(@par_Estado,EDCO.Estado)
		AND EDCO.Anulado = ISNULL(@par_Anulado,EDCO.Anulado)
		AND EDCO.CATA_DOOR_Codigo = ISNULL(@par_Codigo_Documento,EDCO.CATA_DOOR_Codigo)
		AND EDCO.Documento_Origen = ISNULL(@par_Numero_Documento,EDCO.Documento_Origen)
		AND(TERC.Nombre + ' ' +TERC.Apellido1 + ' ' + TERC.Apellido2 LIKE CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Tercero,TERC.Nombre + ' ' +TERC.Apellido1 + ' ' + TERC.Apellido2))), '%'))
		AND(BENE.Nombre + ' ' +BENE.Apellido1 + ' ' + BENE.Apellido2 LIKE CONCAT('%', RTRIM(LTRIM  (ISNULL(@par_Beneficiario,BENE.Nombre + ' ' +BENE.Apellido1 + ' ' + BENE.Apellido2))), '%'))
		AND OFIC.Codigo = ISNULL(@par_Oficina,OFIC.Codigo)
  )     
  SELECT DISTINCT    
    0 As Obtener,  
	EMPR_Codigo,
	Codigo,
	Numero,
	TIDO_Codigo,
	Codigo_Alterno,
	Fecha,
	TERC_Codigo_Titular,
	TERC_Codigo_Beneficiario,
	Observaciones,
	CATA_DOOR_Codigo,
	CATA_FPDC_Codigo,
	CATA_DEIN_Codigo,
	Documento_Origen,
	Anulado,
	Estado,
	OFIC_Codigo_Destino,
	NombreBeneficiario, 
	NombreTercero, 
	Oficina,
	DocumentoOrigen,   
   @CantidadRegistros AS TotalRegistros,    
   @par_NumeroPagina AS PaginaObtener,    
   @par_RegistrosPagina AS RegistrosPagina    
  FROM    
   Pagina    
  WHERE    
   RowNumber > (ISNULL(@par_NumeroPagina, 1) -1) * ISNULL(@par_RegistrosPagina, 1000)  
   AND RowNumber <= ISNULL(@par_NumeroPagina, 1) * ISNULL(@par_RegistrosPagina, 1000)  
  ORDER BY Numero ASC    
    
END    
go