﻿PRINT 'gsp_insertar_detalle_documento_comprobantes'
GO
DROP PROCEDURE gsp_insertar_detalle_documento_comprobantes
GO
CREATE PROCEDURE gsp_insertar_detalle_documento_comprobantes
(
@par_EMPR_Codigo SMALLINT,
@par_EDCO_Codigo NUMERIC,
@par_PLUC_Codigo SMALLINT,
@par_TERC_Codigo NUMERIC,
@par_Valor_Base MONEY,
@par_Valor_Debito MONEY,
@par_Valor_Credito MONEY,
@par_Genera_Cuenta SMALLINT,
@par_Observaciones VARCHAR(100) = null,
@par_CATA_TEPC_Codigo NUMERIC,
@par_CATA_CCPC_Codigo NUMERIC,
@par_CATA_DOCR_Codigo NUMERIC
)
AS
BEGIN

INSERT INTO Detalle_Documento_Comprobantes
(
EMPR_Codigo,
EDCO_Codigo,
PLUC_Codigo,
TERC_Codigo,
Valor_Base,
Valor_Debito,
Valor_Credito,
Genera_Cuenta,
Observaciones,
CATA_TEPC_Codigo,
CATA_CCPC_Codigo,
CATA_DOCR_Codigo
)
VALUES (
@par_EMPR_Codigo,
@par_EDCO_Codigo,
@par_PLUC_Codigo,
@par_TERC_Codigo,
@par_Valor_Base,
@par_Valor_Debito,
@par_Valor_Credito,
@par_Genera_Cuenta,
ISNULL(@par_Observaciones,''),
@par_CATA_TEPC_Codigo,
@par_CATA_CCPC_Codigo,
@par_CATA_DOCR_Codigo
)

SELECT @@IDENTITY AS Codigo 
END
GO