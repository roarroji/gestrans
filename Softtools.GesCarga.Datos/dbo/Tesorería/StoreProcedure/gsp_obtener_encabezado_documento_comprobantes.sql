﻿PRINT 'gsp_obtener_encabezado_documento_comprobantes'
GO
DROP PROCEDURE gsp_obtener_encabezado_documento_comprobantes
GO
CREATE PROCEDURE gsp_obtener_encabezado_documento_comprobantes
(
@par_EMPR_Codigo SMALLINT,
@par_Codigo NUMERIC
)
AS
BEGIN
SELECT 
1 AS Obtener,
ENDC.EMPR_Codigo,
ENDC.Codigo,
ENDC.Numero,
ENDC.TIDO_Codigo,
ENDC.Codigo_Alterno,
ENDC.Fecha,
ENDC.TERC_Codigo_Titular,
ENDC.TERC_Codigo_Beneficiario,
ENDC.Observaciones,
ENDC.CATA_DOOR_Codigo,
ENDC.CATA_FPDC_Codigo,
ENDC.CATA_DEIN_Codigo,
ENDC.Documento_Origen,
ENDC.CUBA_Codigo,
ENDC.CAJA_Codigo,
ENDC.Numero_Pago_Recaudo,
ENDC.Fecha_Pago_Recaudo,
ENDC.Valor_Pago_Recaudo_Transferencia,
ENDC.Valor_Pago_Recaudo_Efectivo,
ENDC.Valor_Pago_Recaudo_Cheque,
ENDC.Valor_Pago_Recaudo_Total,
ENDC.Numeracion,
ENDC.Anulado,
ENDC.ECCO_Codigo,
ENDC.OFIC_Codigo_Creacion,
ENDC.OFIC_Codigo_Destino,
ENDC.Genero_Consignacion,
ENDC.Valor_Alterno,
ENDC.VEHI_Codigo,
ENDC.Estado,
ISNULL(BENE.Razon_Social,'') + ' ' + ISNULL(BENE.Nombre,'') + ' ' + ISNULL(BENE.Apellido1,'') + ' ' + ISNULL(BENE.Apellido2,'') As NombreBeneficiario,
ISNULL(TERC.Razon_Social,'') + ' ' + ISNULL(TERC.Nombre,'') + ' ' + ISNULL(TERC.Apellido1,'') + ' ' + ISNULL(TERC.Apellido2,'') As NombreTercero,
TERC.Numero_Identificacion AS IdentTercero,
BENE.Numero_Identificacion AS IdentBeneficiario

FROM
Encabezado_Documento_Comprobantes ENDC,
Terceros TERC,
Terceros BENE

WHERE

ENDC.EMPR_Codigo = @par_EMPR_Codigo
AND ENDC.Codigo = @par_Codigo

AND ENDC.EMPR_Codigo = TERC.EMPR_Codigo
AND ENDC.TERC_Codigo_Titular = TERC.Codigo

AND ENDC.EMPR_Codigo = BENE.EMPR_Codigo
AND ENDC.TERC_Codigo_Beneficiario = BENE.Codigo

END
GO