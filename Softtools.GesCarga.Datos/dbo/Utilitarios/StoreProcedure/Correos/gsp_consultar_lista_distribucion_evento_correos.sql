﻿PRINT 'gsp_consultar_lista_distribucion_evento_correos'
GO
DROP PROCEDURE gsp_consultar_lista_distribucion_evento_correos
GO
CREATE PROCEDURE gsp_consultar_lista_distribucion_evento_correos
(  
	@par_EMPR_Codigo smallint,  
	@par_EVCO_Codigo numeric,  
	@par_TERC_Codigo_Transportador NUMERIC = NULL,  
	@par_TERC_Codigo_Cliente NUMERIC = NULL,  
	@par_TERC_Codigo_Conductor NUMERIC = NULL,
	@par_TEDI_Codigo NUMERIC = NULL
)  
AS  
BEGIN
--Constantes  
Declare @ProcesoCorreoENOS Numeric = 10
Declare @Correos TABLE (  
 Correo VARCHAR(100) NOT NULL  
)  
--Inserta los correos principales de la distribución de correos  
INSERT INTO @Correos  
SELECT Email   
FROM Lista_Distribucion_Correos   
WHERE EMPR_Codigo = @par_EMPR_Codigo  
AND EVCO_Codigo =  @par_EVCO_Codigo  
AND Email != '@Transportador'  
AND Email != '@Cliente'  
AND Email != '@Conductor'
--Consulta e inserta los correos del transportador  
IF EXISTS(SELECT * FROM Lista_Distribucion_Correos WHERE EMPR_Codigo = @par_EMPR_Codigo AND EVCO_Codigo =  @par_EVCO_Codigo AND Email = '@Transportador')  
BEGIN
	IF @par_TEDI_Codigo IS NOT NULL BEGIN
		INSERT INTO @Correos  
		SELECT Emails FROM Terceros 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND Codigo = @par_TERC_Codigo_Transportador 
		AND NOT(Emails = '') 
		AND Emails IS NOT NULL

		INSERT INTO @Correos  
		SELECT Email FROM Tercero_Listas_Correos 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND EVCO_Codigo = @par_EVCO_Codigo 
		AND TERC_Codigo = @par_TERC_Codigo_Transportador 
		AND NOT(Email = '') 
		AND Email IS NOT NULL 
		AND TEDI_Codigo = ISNULL(@par_TEDI_Codigo, TEDI_Codigo)
		AND EXISTS(SELECT Codigo FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo AND PROC_Codigo = @ProcesoCorreoENOS)
	END
	ELSE BEGIN
		INSERT INTO @Correos  
		SELECT Emails FROM Terceros 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND Codigo = @par_TERC_Codigo_Transportador 
		AND NOT(Emails = '') 
		AND Emails IS NOT NULL  

		INSERT INTO @Correos  
		SELECT Email FROM Tercero_Listas_Correos 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND EVCO_Codigo = @par_EVCO_Codigo 
		AND TERC_Codigo = @par_TERC_Codigo_Transportador 
		AND NOT(Email = '') 
		AND Email IS NOT NULL  
	END
END
--Consulta e inserta los correos del Cliente  
IF EXISTS(SELECT * FROM Lista_Distribucion_Correos WHERE EMPR_Codigo = @par_EMPR_Codigo AND EVCO_Codigo =  @par_EVCO_Codigo AND Email = '@Cliente')  
BEGIN  
	IF @par_TEDI_Codigo IS NOT NULL BEGIN
		INSERT INTO @Correos  
		SELECT Emails FROM Terceros 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND Codigo = @par_TERC_Codigo_Cliente 
		AND NOT(Emails = '') AND Emails IS NOT NULL  

		INSERT INTO @Correos  
		SELECT Email FROM Tercero_Listas_Correos 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND EVCO_Codigo = @par_EVCO_Codigo 
		AND TERC_Codigo = @par_TERC_Codigo_Cliente 
		AND NOT(Email = '') 
		AND Email IS NOT NULL  
		AND TEDI_Codigo = ISNULL(@par_TEDI_Codigo, TEDI_Codigo)
		AND EXISTS(SELECT Codigo FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo AND PROC_Codigo = @ProcesoCorreoENOS)
	END
	ELSE BEGIN
		INSERT INTO @Correos  
		SELECT Emails FROM Terceros 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND Codigo = @par_TERC_Codigo_Cliente 
		AND NOT(Emails = '') AND Emails IS NOT NULL  

		INSERT INTO @Correos  
		SELECT Email FROM Tercero_Listas_Correos 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND EVCO_Codigo = @par_EVCO_Codigo 
		AND TERC_Codigo = @par_TERC_Codigo_Cliente 
		AND NOT(Email = '') 
		AND Email IS NOT NULL 
	END
END
--Consulta e inserta los correos del Conductor
IF EXISTS(SELECT * FROM Lista_Distribucion_Correos WHERE EMPR_Codigo = @par_EMPR_Codigo AND EVCO_Codigo =  @par_EVCO_Codigo AND Email = '@Conductor')
BEGIN
	IF @par_TEDI_Codigo IS NOT NULL BEGIN
		INSERT INTO @Correos  
		SELECT Emails FROM Terceros 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND Codigo = @par_TERC_Codigo_Conductor 
		AND NOT(Emails = '') 
		AND Emails IS NOT NULL  

		INSERT INTO @Correos  
		SELECT Email FROM Tercero_Listas_Correos 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND EVCO_Codigo = @par_EVCO_Codigo 
		AND TERC_Codigo = @par_TERC_Codigo_Conductor 
		AND NOT(Email = '') 
		AND Email IS NOT NULL
		AND TEDI_Codigo = ISNULL(@par_TEDI_Codigo, TEDI_Codigo)
		AND EXISTS(SELECT Codigo FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo AND PROC_Codigo = @ProcesoCorreoENOS)
	END
	ELSE BEGIN
		INSERT INTO @Correos  
		SELECT Emails FROM Terceros 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND Codigo = @par_TERC_Codigo_Conductor 
		AND NOT(Emails = '') 
		AND Emails IS NOT NULL  

		INSERT INTO @Correos  
		SELECT Email FROM Tercero_Listas_Correos 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND EVCO_Codigo = @par_EVCO_Codigo 
		AND TERC_Codigo = @par_TERC_Codigo_Conductor 
		AND NOT(Email = '') 
		AND Email IS NOT NULL  
	END
END
--Consulta los correos de la empresa
IF @par_TEDI_Codigo IS NOT NULL BEGIN
	INSERT INTO @Correos  
	SELECT Emails FROM Terceros 
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND Numero_Identificacion IN (SELECT Numero_Identificacion FROM Empresas where EMPR_Codigo = @par_EMPR_Codigo) 
	AND NOT(Emails = '') 
	AND Emails IS NOT NULL  
	
	INSERT INTO @Correos  
	SELECT Email FROM Tercero_Listas_Correos 
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND EVCO_Codigo = @par_EVCO_Codigo 
	AND TERC_Codigo IN 
	(
		SELECT Codigo FROM Terceros 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND Numero_Identificacion IN (SELECT Numero_Identificacion FROM Empresas where EMPR_Codigo = @par_EMPR_Codigo)
	) 
	AND NOT(Email = '') 
	AND Email IS NOT NULL 
	AND TEDI_Codigo = ISNULL(@par_TEDI_Codigo, TEDI_Codigo)
	AND EXISTS(SELECT Codigo FROM Validacion_Procesos WHERE EMPR_Codigo = @par_EMPR_Codigo AND PROC_Codigo = @ProcesoCorreoENOS)
END
ELSE BEGIN
	INSERT INTO @Correos  
	SELECT Emails FROM Terceros 
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND Numero_Identificacion IN (SELECT Numero_Identificacion FROM Empresas where EMPR_Codigo = @par_EMPR_Codigo) 
	AND NOT(Emails = '') 
	AND Emails IS NOT NULL  
	
	INSERT INTO @Correos  
	SELECT Email FROM Tercero_Listas_Correos 
	WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND EVCO_Codigo = @par_EVCO_Codigo 
	AND TERC_Codigo IN 
	(
		SELECT Codigo FROM Terceros 
		WHERE EMPR_Codigo = @par_EMPR_Codigo 
		AND Numero_Identificacion IN (SELECT Numero_Identificacion FROM Empresas where EMPR_Codigo = @par_EMPR_Codigo)
	) 
	AND NOT(Email = '') 
	AND Email IS NOT NULL 
END

SELECT DISTINCT Correo  
FROM @Correos  
  
END
GO
