﻿PRINT 'gsp_eliminar_lista_distribucion_correos'
GO
DROP PROCEDURE gsp_eliminar_lista_distribucion_correos
GO
CREATE PROCEDURE gsp_eliminar_lista_distribucion_correos  
(@par_EMPR_Codigo SMALLINT,  
@par_EVCO_Codigo NUMERIC)  
AS
BEGIN
DELETE Lista_Distribucion_Correos  
where   
EMPR_Codigo = @par_EMPR_Codigo  
AND EVCO_Codigo = @par_EVCO_Codigo  
END  
GO