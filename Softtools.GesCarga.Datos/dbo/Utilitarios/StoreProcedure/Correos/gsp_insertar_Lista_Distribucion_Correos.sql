﻿PRINT 'gsp_insertar_Lista_Distribucion_Correos'
GO
DROP PROCEDURE gsp_insertar_Lista_Distribucion_Correos
GO
CREATE PROCEDURE gsp_insertar_Lista_Distribucion_Correos  
(  
@par_EMPR_Codigo smallint,  
@par_EVCO_Codigo numeric,  
@par_Email varchar(200),  
@par_TERC_Codigo_Empleado numeric,  
@par_Estado smallint,  
@par_USUA_Codigo_Crea smallint  
  
  
)  
AS  
BEGIN  
INSERT INTO Lista_Distribucion_Correos  
(  
EMPR_Codigo,  
EVCO_Codigo,  
Email,  
TERC_Codigo_Empleado,  
Estado,  
USUA_Codigo_Crea,  
Fecha_Crea  
  
  
)  
VALUES   
(  
@par_EMPR_Codigo,  
@par_EVCO_Codigo,  
@par_Email,  
@par_TERC_Codigo_Empleado,  
@par_Estado,  
@par_USUA_Codigo_Crea,  
GETDATE()  
)  
  
SELECT @@IDENTITY AS Numero  
  
END  
GO