﻿PRINT 'gsp_obtener_lista_distribucion_correos'
GO
DROP PROCEDURE gsp_obtener_lista_distribucion_correos
GO
CREATE PROCEDURE gsp_obtener_lista_distribucion_correos  
(  
@par_EMPR_Codigo smallint,  
@par_EVCO_Codigo numeric,  
@par_Email varchar(200),  
@par_TERC_Codigo_Empleado numeric,  
@par_Estado smallint  
  
)  
AS  
BEGIN  
 SELECT  
  1 AS Obtener,  
  LDCO.EMPR_Codigo,   
  LDCO.EVCO_Codigo,  
  LDCO.Email,  
  LDCO.TERC_Codigo_Empleado,  
  LDCO.Estado,  
  ISNULL(TERC.Nombre,'')+' '+ISNULL(TERC.Apellido1,'')+' '+ISNULL(TERC.Apellido2,'')+' '+ISNULL(TERC.Razon_Social,'') AS NombreCompleto   
 FROM  
  Lista_Distribucion_Correos LDCO,  
  terceros TERC  
 WHERE  
 LDCO.EMPR_Codigo = TERC.EMPR_Codigo  
 AND LDCO.TERC_Codigo_Empleado = TERC.Codigo  
 AND LDCO.EMPR_Codigo = @par_EMPR_Codigo  
 AND LDCO.EVCO_Codigo = @par_EVCO_Codigo  
 AND LDCO.Email = @par_Email  
 AND LDCO.TERC_Codigo_Empleado = @par_TERC_Codigo_Empleado  
 AND LDCO.Estado = @par_Estado  
END  
GO