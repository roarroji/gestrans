﻿Print 'gsp_consultar_documento_cumplido_despachos'
GO
DROP PROCEDURE gsp_consultar_documento_cumplido_despachos
GO
CREATE PROCEDURE gsp_consultar_documento_cumplido_despachos (
@par_EMPR_Codigo SMALLINT,
@par_ENCU_Numero NUMERIC = NULL,
@par_USUA_Codigo NUMERIC
)
AS
BEGIN

	SET @par_ENCU_Numero = ISNULL(@par_ENCU_Numero,0)
	DECLARE @tblRes As TABLE (Resultado SMALLINT)
	INSERT INTO @tblRes
	EXEC gsp_trasladar_documento_cumplido_despachos 
	@par_EMPR_Codigo = @par_EMPR_Codigo,
	@par_ENCU_Numero = @par_ENCU_Numero,
	@par_USUA_Codigo = @par_USUA_Codigo,
	@par_A_Temporal	 = 1

	IF EXISTS(SELECT * FROM @tblRes) BEGIN

			SELECT EMPR_Codigo, 
			ID, 
			ENCU_Numero, 
			Nombre As Nombre_Asignado, 
			NombreArchivo,
			CASE WHEN Archivo IS NULL THEN 0 ELSE 1 END As ValorDocumento, 
			ExtensionArchivo,
			Tipo,
			Fecha_Crea,
			USUA_Codigo
			FROM T_Documento_Cumplido_Despachos

		WHERE EMPR_Codigo = @par_EMPR_Codigo
		AND ENCU_Numero = @par_ENCU_Numero
		AND USUA_Codigo = @par_USUA_Codigo

	END

END
GO