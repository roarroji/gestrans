﻿Print 'gsp_eliminar_t_documento_cumplido_despachos'
GO
DROP PROCEDURE gsp_eliminar_t_documento_cumplido_despachos
GO
CREATE PROCEDURE gsp_eliminar_t_documento_cumplido_despachos (

	@par_EMPR_Codigo SMALLINT,
	@par_USUA_Codigo INT,
	@par_ENCU_Numero NUMERIC = NULL,
	@par_ID			 INTEGER = NULL
)

AS
BEGIN
	
	IF EXISTS(SELECT EMPR_Codigo FROM T_Documento_Cumplido_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo 
	AND USUA_Codigo = @par_USUA_Codigo) BEGIN

			DELETE T_Documento_Cumplido_Despachos 
			WHERE EMPR_Codigo = @par_EMPR_Codigo 
			AND USUA_Codigo = @par_USUA_Codigo
			AND ENCU_Numero = ISNULL(@par_ENCU_Numero, ENCU_Numero)
			AND ID = ISNULL(@par_ID, ID)

			SELECT @@ROWCOUNT As RegistrosAfectados
	END ELSE
	BEGIN
		SELECT 1 As RegistrosAfectados
	END

END
GO