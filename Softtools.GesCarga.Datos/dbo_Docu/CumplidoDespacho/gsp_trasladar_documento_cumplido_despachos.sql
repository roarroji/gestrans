﻿Print 'gsp_trasladar_documento_cumplido_despachos'
GO
DROP PROCEDURE gsp_trasladar_documento_cumplido_despachos
GO
CREATE PROCEDURE gsp_trasladar_documento_cumplido_despachos (
@par_EMPR_Codigo SMALLINT,
@par_ENCU_Numero NUMERIC,
@par_USUA_Codigo NUMERIC,
@par_A_Temporal	 SMALLINT = NULL

)
AS
BEGIN

IF ISNULL(@par_A_Temporal,0) = 0 BEGIN
		
		DELETE Documento_Cumplido_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENCU_Numero = @par_ENCU_Numero

		INSERT INTO Documento_Cumplido_Despachos (EMPR_Codigo,
		ID,
		ENCU_Numero,
		Nombre,
		NombreArchivo,
		Archivo,
		ExtensionArchivo,
		Tipo,
		Fecha_Crea,
		USUA_Codigo_Crea)

		SELECT EMPR_Codigo,
		ID,
		@par_ENCU_Numero,
		Nombre,
		NombreArchivo,
		Archivo,
		ExtensionArchivo,
		Tipo,
		GETDATE(),
		@par_USUA_Codigo
		FROM T_Documento_Cumplido_Despachos
		WHERE EMPR_Codigo = @par_EMPR_Codigo
		AND USUA_Codigo = @par_USUA_Codigo


		IF EXISTS(SELECT EMPR_Codigo FROM Documento_Cumplido_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENCU_Numero = @par_ENCU_Numero) 
		BEGIN
			DELETE T_Documento_Cumplido_Despachos
			WHERE EMPR_Codigo = @par_EMPR_Codigo
			AND USUA_Codigo = @par_USUA_Codigo
			SELECT 1 As RegistrosAfectados
		END
		ELSE BEGIN
			SELECT 0 As RegistrosAfectados
		END
	END
	ELSE BEGIN

	--ELiminar de nuevo por problemas de hilos multiples
	IF EXISTS(SELECT EMPR_Codigo FROM T_Documento_Cumplido_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND USUA_Codigo = @par_USUA_Codigo) 
		BEGIN
			DELETE T_Documento_Cumplido_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND USUA_Codigo = @par_USUA_Codigo
		END
		

		INSERT INTO T_Documento_Cumplido_Despachos (

					EMPR_Codigo,
					ID,
					USUA_Codigo,
					ENCU_Numero,
					Nombre,
					NombreArchivo,
					Archivo,
					ExtensionArchivo,
					Tipo,
					Fecha_Crea
		)
		SELECT EMPR_Codigo,
					ID,
					@par_USUA_Codigo,
					ENCU_Numero,
					Nombre,
					NombreArchivo,
					Archivo,
					ExtensionArchivo,
					Tipo,
					Fecha_Crea
				FROM Documento_Cumplido_Despachos
				WHERE EMPR_Codigo = @par_EMPR_Codigo
				AND ENCU_Numero = @par_ENCU_Numero


		IF EXISTS(SELECT EMPR_Codigo FROM T_Documento_Cumplido_Despachos WHERE EMPR_Codigo = @par_EMPR_Codigo AND ENCU_Numero = @par_ENCU_Numero AND USUA_Codigo = @par_USUA_Codigo) 
		BEGIN
			SELECT 1 As RegistrosAfectados
		END
		ELSE BEGIN
			SELECT 0 As RegistrosAfectados
		END

	END


END
GO