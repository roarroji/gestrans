﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Almacen
'Imports Softtools.GesPasajeros.Entidades.Basico


Namespace Almacen

    ''' <summary>
    ''' Clase <see cref="DetalleDocumentoAlmacenes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleDocumentoAlmacenes
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleDocumentoAlmacenes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleDocumentoAlmacenes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            ReferenciaAlmacen = New ReferenciaAlmacenes With {.Codigo = Read(lector, "REAL_Codigo"), .NombreReferencia = Read(lector, "ReferenciaAlmacenes"), .Referencia = New Referencias With {.CodigoReferencia = Read(lector, "Codigo_Referencia")}}
            ReferenciaProveedor = New ReferenciaProveedores With {.Codigo = Read(lector, "PREA_Codigo"), .NombreReferencia = Read(lector, "ReferenciaProveedor")}
            ValorUnitario = Read(lector, "Valor_Unitario")
            Cantidad = Read(lector, "Cantidad")
            ValorAntesDescuento = Read(lector, "Valor_Total")
            ValorTotal = Read(lector, "Valor_Total")
            UnidadMedida = New UnidadMedidaReferencias With {.Codigo = Read(lector, "UMRA_Codigo"), .Nombre = Read(lector, "UnidadMedida"), .NombreCorto = Read(lector, "NombreCortoUM")}
            UnidadEmpaque = New UnidadEmpaqueReferencias With {.Codigo = Read(lector, "UERA_Codigo"), .Nombre = Read(lector, "UnidadEmpaque"), .NombreCorto = Read(lector, "NombreCortoUE")}
            'Referencia = Read(lector, "Referencia")
        End Sub
        ''' <summary>
        ''' Obtiene o establece la referencia almacen del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ReferenciaAlmacen As ReferenciaAlmacenes

        ''' <summary>
        ''' Obtiene o establece la referencia almacen del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ReferenciaProveedor As ReferenciaProveedores
        ''' <summary>
        ''' Obtiene o establece el valor unitario del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ValorUnitario As Double
        ''' <summary>
        ''' Obtiene o establece la cantidad del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property Cantidad As Integer
        ''' <summary>
        ''' Obtiene o establece  el valor total del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ValorTotal As Double
        ''' <summary>
        ''' Obtiene o establece  el valor total del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ValorAntesDescuento As Double

        <JsonProperty>
        Public Property Referencia As String

        <JsonProperty>
        Public Property Almacen As Almacenes

        <JsonProperty>
        Public Property UnidadMedida As UnidadMedidaReferencias

        <JsonProperty>
        Public Property UnidadEmpaque As UnidadEmpaqueReferencias

        <JsonProperty>
        Public Property NumeroOrdenCompra As Integer

    End Class
End Namespace
