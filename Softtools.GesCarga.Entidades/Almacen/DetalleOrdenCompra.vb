﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Almacen





Namespace Almacen

    ''' <summary>
    ''' Clase <see cref="DetalleOrdenCompra"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleOrdenCompra
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleOrdenCompra"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleOrdenCompra"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroOrden = Read(lector, "EOCA_Numero")
            ReferenciaAlmacen = New ReferenciaAlmacenes With {.Codigo = Read(lector, "REAL_Codigo"), .NombreReferencia = Read(lector, "ReferenciaAlmacenes"), .Referencia = New Referencias With {.CodigoReferencia = Read(lector, "Codigo_Referencia")}}
            ReferenciaProveedor = New ReferenciaProveedores With {.Codigo = Read(lector, "PREA_Codigo"), .NombreReferencia = Read(lector, "ReferenciaProveedor")}
            UnidadMedida = New UnidadMedidaReferencias With {.Codigo = Read(lector, "UMRA_Codigo"), .Nombre = Read(lector, "UnidadMedida"), .NombreCorto = Read(lector, "NombreCortoUM")}
            UnidadEmpaque = New UnidadEmpaqueReferencias With {.Codigo = Read(lector, "UNEM_Codigo"), .Nombre = Read(lector, "UnidadEmpaque"), .NombreCorto = Read(lector, "NombreCortoUE")}
            ValorUnitario = Read(lector, "Valor_Unitario")
            Cantidad = Read(lector, "Cantidad")
            'Dimension = Read(lector, "Dimension")
            ValorNeto = Read(lector, "Valor_Neto")
            PorcentajeDescuento = Read(lector, "Porcentaje_Descuento")
            ValorDescuento = Read(lector, "Valor_Descuento")
            ValorAntesDescuento = Read(lector, "Valor_Antes_Descuento")
            PorcentajeIva = Read(lector, "Porcentaje_IVA")
            ValorIva = Read(lector, "Valor_IVA")
            Subtotal = Read(lector, "Subtotal")
            ValorTotal = Read(lector, "Valor_Total")
            CantidadRecibida = Read(lector, "Cantidad_Recibida")
            CantidadFaltante = Read(lector, "Cantidad_Faltante")
            TipoDocumento = Read(lector, "TIDO_Codigo")
        End Sub

        ''' <summary>
        ''' Obtiene o establece id del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property Id As Integer

        ''' <summary>
        ''' Obtiene o establece numero de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property NumeroOrden As Integer
        ''' <summary>
        ''' Obtiene o establece la referencia almacen del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ReferenciaAlmacen As ReferenciaAlmacenes
        ''' <summary>
        ''' Obtiene o establece la referencia almacen del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ReferenciaProveedor As ReferenciaProveedores
        ''' <summary>
        ''' Obtiene o establece la referencia proveedor del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ValorAntesDescuento As Double
        ''' <summary>
        ''' Obtiene o establece el valor unitario del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ValorUnitario As Double
        ''' <summary>
        ''' Obtiene o establece la cantidad del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property Cantidad As Integer
        ''' <summary>
        ''' Obtiene o establece la dimension del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property Dimension As Integer
        ''' <summary>
        ''' Obtiene o establece el valor neto del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ValorNeto As Double
        ''' <summary>
        ''' Obtiene o establece el porcentaje descuento del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property PorcentajeDescuento As Integer
        ''' <summary>
        ''' Obtiene o establece el valor descuento del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ValorDescuento As Double
        ''' <summary>
        ''' Obtiene o establece el porcentaje iva del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property PorcentajeIva As Double
        ''' <summary>
        ''' Obtiene o establece el valor iva del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ValorIva As Double
        ''' <summary>
        ''' Obtiene o establece  el valor total del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ValorTotal As Double
        ''' <summary>
        ''' Obtiene o establece la cantidad recibida del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property CantidadRecibida As Integer
        ''' <summary>
        ''' Obtiene o establece la cantidad faltante del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property CantidadFaltante As Integer
        ''' <summary>
        ''' Obtiene o establece la cantidad faltante del detalle de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property Referencia As String

        <JsonProperty>
        Public Property UnidadMedida As UnidadMedidaReferencias
        <JsonProperty>
        Public Property UnidadEmpaque As UnidadEmpaqueReferencias
        <JsonProperty>
        Public Property Subtotal As Double

    End Class
End Namespace
