﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Almacen
Imports Softtools.GesCarga.Entidades.Basico.General


Namespace Almacen

    ''' <summary>
    ''' Clase <see cref="OrdenCompra"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class OrdenCompra
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="OrdenCompra"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="OrdenCompra"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            Fecha = Read(lector, "Fecha")
            Almacen = New Almacenes With {.Codigo = Read(lector, "ALMA_Codigo"), .Nombre = Read(lector, "NombreAlmacen")}
            Proveedor = New Tercero With {.Codigo = Read(lector, "TERC_Proveedor"), .Nombre = Read(lector, "NombreTercero")}
            Observaciones = Read(lector, "Observaciones")
            Estado = New ValorCatalogos With {.Codigo = Read(lector, "Estado"), .Nombre = Read(lector, "EstadoDocumento")}
            Anulado = Read(lector, "Anulado")
            TotalRegistros = Read(lector, "TotalRegistros")
            RegistrosPagina = Read(lector, "RegistrosPagina")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            ContactoRecibe = Read(lector, "Contacto_Recibe")
            ContactoEntrega = Read(lector, "Contacto_Entrega")
            FechaEntrega = Read(lector, "Fecha_Entrega")
            FormaPago = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FOCO_Codigo")}
            DiasCredito = Read(lector, "Dias_Credito")
            PorcentajeAnticipo = Read(lector, "Porcentaje_Anticipo")
            ValorAnticipo = Read(lector, "Valor_Anticipo")
            Subtotal = Read(lector, "Subtotal")
            ValorDescuento = Read(lector, "Valor_Descuento")
            ValorIva = Read(lector, "Valor_IVA")
            ValorTotal = Read(lector, "Valor_Total")
            ValorFlete = Read(lector, "Valor_Flete")
            ValorAntesDescuento = Read(lector, "Valor_Antes_Descuento")
            PorcentajeDescuento = Read(lector, "Porcentaje_Descuento")
            Cumplido = Read(lector, "Cumplido")
            Oficina = Read(lector, "OFIC_Codigo")


        End Sub

        ''' <summary>
        ''' Obtiene o establece 
        ''' </summary>
        <JsonProperty>
        Public Property Almacen As Almacenes

        ''' <summary>
        ''' Obtiene o establece 
        ''' </summary>
        <JsonProperty>
        Public Property ContactoRecibe As String
        ''' <summary>
        ''' Obtiene o establece el proveedor de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property Proveedor As Tercero
        ''' <summary>
        ''' Obtiene o establece 
        ''' </summary>
        <JsonProperty>
        Public Property ContactoEntrega As String

        <JsonProperty>
        Public Property ValorAntesDescuento As Double

        <JsonProperty>
        Public Property PorcentajeDescuento As Integer

        <JsonProperty>
        Public Property ValorTransporte As Double

        ''' <summary>
        ''' Obtiene o establece 
        ''' </summary>
        <JsonProperty>
        Public Property FechaEntrega As DateTime
        ''' <summary>
        ''' Obtiene o establece la forma de pago de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property FormaPago As ValorCatalogos
        ''' <summary>
        ''' Obtiene o establece 
        ''' </summary>
        <JsonProperty>
        Public Property DiasCredito As Integer
        ''' <summary>
        ''' Obtiene o establece 
        ''' </summary>
        <JsonProperty>
        Public Property PorcentajeAnticipo As Integer
        ''' <summary>
        ''' Obtiene o establece 
        ''' </summary>
        <JsonProperty>
        Public Property ValorAnticipo As Double
        ''' <summary>
        ''' Obtiene o establece 
        ''' </summary>
        <JsonProperty>
        Public Property Subtotal As Double
        ''' <summary>
        ''' Obtiene o establece 
        ''' </summary>
        <JsonProperty>
        Public Property ValorDescuento As Double
        ''' <summary>
        ''' Obtiene o establece iva de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ValorIva As Double
        ''' <summary>
        ''' Obtiene o establece valor total de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ValorTotal As Double
        ''' <summary>
        ''' Obtiene o establece valor flete de la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property ValorFlete As Double
        ''' <summary>
        ''' Obtiene o establece si se encuentra cumplida la orden de compra
        ''' </summary>
        <JsonProperty>
        Public Property Cumplido As Integer

        ''' <summary>
        ''' Obtiene o establece el detalle de la orden de compra
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty>
        Public Property Detalles As IEnumerable(Of DetalleOrdenCompra)

        <JsonProperty>
        Public Property NombreProveedor As String

        <JsonProperty>
        Public Property CodigoEstadoDocumento As String


        <JsonProperty>
        Public Property Estado As ValorCatalogos


    End Class
End Namespace
