﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

''' <summary>
''' Clase <see cref="Base"/>
''' </summary>
Public Class Base

#Region "Variables publicas"

    ''' <summary>
    ''' Obtiene o establece el código de la empresa a la que pertenece el usuario
    ''' </summary>
    <JsonProperty>
    Public Property CodigoEmpresa As Short

    ''' <summary>
    ''' Obtiene o establece el nombre de la empresa a la que pertenece el usuario
    ''' </summary>    
    <JsonProperty>
    Public Property NombreEmpresa As String

    ''' <summary>
    ''' Obtiene o establece si está consultando u obteniendo con el objetivo de no retornar la entidad completa
    ''' </summary>
    <JsonProperty>
    Public Property Obtener As Long

    ''' <summary>
    ''' Obtiene o establece si está consultando una lista
    ''' </summary>
    <JsonProperty>
    Public Property sLista As Long

    ''' <summary>
    ''' Obtiene o establece el número de la página que se desea visualizar.
    ''' </summary>
    <JsonProperty>
    Public Property Pagina As Integer

    ''' <summary>
    ''' Obtiene o establece el número máximo de registros mostrados por página.
    ''' </summary>
    <JsonProperty>
    Public Property RegistrosPagina As Integer

    ''' <summary>
    ''' Obtiene o establece el total páginas necesarias para mostrar la totalidad de los registros retornados por una consulta.
    ''' </summary>
    <JsonProperty>
    Public Property TotalPaginas As Integer

    ''' <summary>
    ''' Obtiene o establece el total de registros retornados por una consulta sin paginar.
    ''' </summary>
    <JsonProperty>
    Public Property TotalRegistros As Integer

    ''' <summary>
    ''' Obtiene o establece una condicion adicional para consultas SQL especificas.
    ''' </summary>
    <JsonProperty>
    Public Property CondicionSQLAux As String

    ''' <summary>
    ''' Obtiene o establece una condicion para cargar valores a un combo y no todas las columnas.
    ''' </summary>
    <JsonProperty>
    Public Property AplicaCombo As Boolean

    ''' <summary>
    ''' Obtiene o establece el Mensaje SQL
    ''' </summary>
    <JsonProperty>
    Public Property MensajeSQL As String

    <JsonProperty>
    Public Property Sync As Boolean
    <JsonProperty>
    Public Property ValorAutocomplete As String
#End Region
    <JsonProperty>
    Public Property UsuarioConsulta As Usuarios
#Region "Constantes Publicas"

    Public Const PROCESO_OBTENER As Byte = 1
    Public Const PROCESO_CONSULTAR As Byte = 1


#End Region
    Public Function Read(Lector As IDataReader, strCampo As String) As Object
        Dim Tabla As DataTable = Lector.GetSchemaTable
        Dim ExisteCampo As Boolean = False
        For Each Campo As DataRow In Tabla.Rows
            If Campo("ColumnName").ToString() = strCampo Then
                ExisteCampo = True
                Exit For
            End If
        Next
        If ExisteCampo Then
            If Not IsDBNull(Lector.Item(strCampo)) Then
                Return Lector.Item(strCampo)
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

End Class