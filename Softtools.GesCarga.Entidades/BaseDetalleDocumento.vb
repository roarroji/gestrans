﻿Imports Newtonsoft.Json

''' <summary>
''' Clase <see cref="BaseDetalleDocumento"/>
''' </summary>
Public Class BaseDetalleDocumento
    Inherits Base

    ''' <summary>
    ''' Obtiene o establece el identificador unico del registro
    ''' </summary>
    <JsonProperty>
    Public Property Id As Integer

    ''' <summary>
    ''' Obtiene o establece el número del encabezado
    ''' </summary>
    ''' <returns></returns>
    Public Property NumeroEncabezado As Long

    ''' <summary>
    ''' Obtiene o establece el código del usuario que ingresa el detalle
    ''' </summary>
    ''' <returns></returns>
    Public Property CodigoUsuarioCrea As Integer

    ''' <summary>
    ''' Obtiene o establece el código del usuario que anula el detalle
    ''' </summary>
    ''' <returns></returns>
    Public Property CodigoUsuarioAnula As Integer

    Public Function Read(Lector As IDataReader, strCampo As String) As Object
        Dim Tabla As DataTable = Lector.GetSchemaTable
        Dim ExisteCampo As Boolean = False
        For Each Campo As DataRow In Tabla.Rows
            If Campo("ColumnName").ToString() = strCampo Then
                ExisteCampo = True
                Exit For
            End If
        Next
        If ExisteCampo Then
            If Not IsDBNull(Lector.Item(strCampo)) Then
                Return Lector.Item(strCampo)
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function
End Class
