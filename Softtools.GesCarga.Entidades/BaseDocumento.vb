﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

''' <summary>
''' Clase <see cref="BaseDocumento"/>
''' </summary>
<JsonObject>
Public Class BaseDocumento
    Inherits Base

    ''' <summary>
    ''' Inicializa una nueva instancia de la clase <see cref="BaseDocumento"/>
    ''' </summary>
    Sub New()
    End Sub

    ''' <summary>
    ''' Inicializa una nueva instancia de la clase <see cref="BaseDocumento"/>, para las propiedades que se heredan
    ''' </summary>
    ''' <param name="lector"></param>
    Sub New(lector As IDataReader)
        CodigoEmpresa = Read(lector, "EMPR_Codigo")
        Fecha = Read(lector, "Fecha")
        Observaciones = Read(lector, "Observaciones")
    End Sub


    ''' <summary>
    ''' Obtiene o establece el número del documento
    ''' </summary>
    <JsonProperty>
    Public Property Numero As Long

    ''' <summary>
    ''' Obtiene o establece el código del documento
    ''' </summary>
    <JsonProperty>
    Public Property Codigo As Long


    ''' <summary>
    ''' Obtiene o establece el código alterno del documento
    ''' </summary>
    <JsonProperty>
    Public Property CodigoAlterno As String

    ''' <summary>
    ''' Obtiene o establece el código alterno del documento
    ''' </summary>
    <JsonProperty>
    Public Property NumeroDocumento As Integer

    ''' <summary>
    ''' Obtiene o establece el tipo de documento
    ''' </summary>
    <JsonProperty>
    Public Property TipoDocumento As Integer

    <JsonProperty>
    Public Property TipoDocumentoOrigen As Integer

    ''' <summary>
    ''' Obtiene o establece la fecha del documento
    ''' </summary>
    <JsonProperty>
    Public Property Fecha As DateTime

    ''' <summary>
    ''' Obtiene o establece las observaciones del documento
    ''' </summary>    
    <JsonProperty>
    Public Property Observaciones As String

    ''' <summary>
    ''' Obtiene o establece el usuario que crea el documento
    ''' </summary>
    <JsonProperty>
    Public Property UsuarioCrea As Usuarios

    ''' <summary>
    ''' Obtiene o establece el estado del documento
    ''' </summary>
    <JsonProperty>
    Public Property EstadoDocumento As ValorCatalogos

    ''' <summary>
    ''' Obtiene o establece la oficina del documento
    ''' </summary>
    <JsonProperty>
    Public Property Oficina As Oficinas

    ''' <summary>
    ''' Obtiene o establece la fecha de creación del documento
    ''' </summary>
    <JsonProperty>
    Public Property FechaCrea As DateTime

    ''' <summary>
    ''' Obtiene o establece el usuario que modifica el documento
    ''' </summary>
    <JsonProperty>
    Public Property UsuarioModifica As Usuarios

    ''' <summary>
    ''' Obtiene o establece la fecha de modificación del documento
    ''' </summary>
    <JsonProperty>
    Public Property FechaModifica As DateTime

    ''' <summary>
    ''' Obtiene o establece el estado
    ''' </summary>
    <JsonProperty>
    Public Property Estado As Integer
    <JsonProperty>
    Public Property NumeroPreImpreso As String
    <JsonProperty>
    Public Property ValorOtrosConceptos As Double
    <JsonProperty>
    Public Property ValorDescuentos As Double
    <JsonProperty>
    Public Property Subtotal As Double
    <JsonProperty>
    Public Property ValorImpuestos As Double
    <JsonProperty>
    Public Property ValorTRM As Double
    <JsonProperty>
    Public Property ValorMonedaAlterna As Double
    <JsonProperty>
    Public Property ValorAnticipo As Double
    <JsonProperty>
    Public Property ResolucionFacturacion As String
    <JsonProperty>
    Public Property ControlImpresion As Integer
    <JsonProperty>
    Public Property Moneda As Monedas

#Region "Auditoria de Anulación"

    ''' <summary>
    ''' Obtiene o establece si el documento fue anulado
    ''' </summary>
    <JsonProperty>
    Public Property Anulado As Short

    ''' <summary>
    ''' Obtiene o establece el usuario que anulo el documento
    ''' </summary>
    <JsonProperty>
    Public Property UsuarioAnula As Usuarios

    ''' <summary>
    ''' Obtiene o establece la causa de anulación del documento
    ''' </summary>
    <JsonProperty>
    Public Property CausaAnulacion As String

    ''' <summary>
    ''' Obtiene o establece la fecha de anulación del documento
    ''' </summary>
    <JsonProperty>
    Public Property FechaAnula As DateTime

    ''' <summary>
    ''' Obtiene o establece la oficina que anula el documento
    ''' </summary>
    <JsonProperty>
    Public Property OficinaAnula As Oficinas

#End Region

#Region "Filtros de Búsqueda"

    <JsonProperty>
    Public Property NumeroInicial As Long

    <JsonProperty>
    Public Property NumeroFinal As Long

    <JsonProperty>
    Public Property FechaInicial As Date

    <JsonProperty>
    Public Property FechaFinal As Date

#End Region
#Region "Tipo Documentos"
    ''' <summary>
    ''' Constante Tipo Documento Factura
    ''' </summary>
    <JsonProperty>
    Public Const TIPO_DOCUMENTO_FACTURA As Byte = 60

    ''' <summary>
    ''' Constante Tipo Documento Prefactura
    ''' </summary>
    <JsonProperty>
    Public Const TIPO_DOCUMENTO_PREFACTURA As Byte = 60
#End Region

    Public Function Read(Lector As IDataReader, strCampo As String) As Object
        Dim Tabla As DataTable = Lector.GetSchemaTable
        Dim ExisteCampo As Boolean = False
        For Each Campo As DataRow In Tabla.Rows
            If Campo("ColumnName").ToString() = strCampo Then
                ExisteCampo = True
                Exit For
            End If
        Next
        If ExisteCampo Then
            If Not IsDBNull(Lector.Item(strCampo)) Then
                If IsDate(Lector.Item(strCampo)) Then
                    If Lector.Item(strCampo) > Date.MinValue Then
                        Return Lector.Item(strCampo)
                    Else
                        Return Nothing
                    End If
                Else
                    Return Lector.Item(strCampo)
                End If
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

End Class

