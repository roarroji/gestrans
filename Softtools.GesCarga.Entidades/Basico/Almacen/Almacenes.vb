﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref=" Almacenes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Almacenes
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Almacenes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Almacenes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Nombre = Read(lector, "Nombre")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            CuentaPUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo"), .CodigoCuenta = Read(lector, "Codigo_Cuenta")}
            Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo"), .Nombre = Read(lector, "Ciudad")}
            Contacto = Read(lector, "Contacto")
            Direccion = Read(lector, "Direccion")
            Telefono = Read(lector, "Telefono")
            Celular = Read(lector, "Celular")
            Estado = Read(lector, "Estado")
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "Oficina")}
            Obtener = Read(lector, "Obtener")

            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            End If


        End Sub

        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Contacto As String
        <JsonProperty>
        Public Property Ciudad As Ciudades
        <JsonProperty>
        Public Property Direccion As String
        <JsonProperty>
        Public Property Telefono As String
        <JsonProperty>
        Public Property Celular As String
        <JsonProperty>
        Public Property NombreCiudad As String

    End Class
End Namespace
