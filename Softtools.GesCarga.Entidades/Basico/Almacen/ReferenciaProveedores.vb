﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref=" ReferenciaProveedores"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ReferenciaProveedores
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ReferenciaProveedores"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ReferenciaProveedores"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            ReferenciaProveedor = Read(lector, "Referencia_Proveedor")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")

            If Obtener = 0 Then
                Referencia = New Referencias With {.Codigo = Read(lector, "REAL_Codigo"), .CodigoReferencia = Read(lector, "Codigo_Referencia"), .Nombre = Read(lector, "Referencia")}
                Proveedor = New Terceros With {.Codigo = Read(lector, "TERC_Proveedor"), .Nombre = Read(lector, "NombreTercero")}
                TotalRegistros = Read(lector, "TotalRegistros")

            Else
                Referencia = New Referencias With {.Codigo = Read(lector, "REAL_Codigo")}
                Proveedor = New Terceros With {.Codigo = Read(lector, "TERC_Proveedor")}
            End If


        End Sub


        <JsonProperty>
        Public Property Referencia As Referencias
        <JsonProperty>
        Public Property Proveedor As Terceros
        <JsonProperty>
        Public Property ReferenciaProveedor As String


        <JsonProperty>
        Public Property NombreProveedor As String
        <JsonProperty>
        Public Property NombreReferencia As String
        <JsonProperty>
        Public Property CodigoRefencia As String

    End Class
End Namespace
