﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria


Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref=" Referencias"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Referencias
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Referencias"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Referencias"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            ReferenciaProveedor = New ReferenciaProveedores With {.Codigo = Read(lector, "CodigoReferenciaProveedor"), .NombreReferencia = Read(lector, "NombreReferenciaProveedor")}
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            CodigoReferencia = Read(lector, "Codigo_Referencia")
            Referencia = Read(lector, "Referencia")
            CuentaPUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo")}
            Marca = Read(lector, "Marca")
            Descripcion = Read(lector, "Descripcion")
            PorcentajeIva = Read(lector, "Porcentaje_Iva")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")

            If Obtener = 0 Then
                UnidadEmpaque = New UnidadEmpaqueReferencias With {.Codigo = Read(lector, "UERA_Codigo"), .Nombre = Read(lector, "UnidadEmpaque")}
                UnidadMedida = New UnidadMedidaReferencias With {.Codigo = Read(lector, "UMRA_Codigo"), .Nombre = Read(lector, "UnidadMedida")}
                GrupoReferencias = New GrupoReferencias With {.Codigo = Read(lector, "GRRA_Codigo"), .Nombre = Read(lector, "GrupoReferencia")}
                TotalRegistros = Read(lector, "TotalRegistros")

            Else
                UnidadEmpaque = New UnidadEmpaqueReferencias With {.Codigo = Read(lector, "UERA_Codigo"), .Nombre = Read(lector, "UnidadEmpaque"), .NombreCorto = Read(lector, "NombreCortoEM")}
                UnidadMedida = New UnidadMedidaReferencias With {.Codigo = Read(lector, "UMRA_Codigo"), .Nombre = Read(lector, "UnidadMedida"), .NombreCorto = Read(lector, "NombreCortoUM")}
                GrupoReferencias = New GrupoReferencias With {.Codigo = Read(lector, "GRRA_Codigo")}
            End If





        End Sub
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property CodigoReferencia As String
        <JsonProperty>
        Public Property UnidadMedida As UnidadMedidaReferencias
        <JsonProperty>
        Public Property UnidadEmpaque As UnidadEmpaqueReferencias
        <JsonProperty>
        Public Property GrupoReferencias As GrupoReferencias
        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas
        <JsonProperty>
        Public Property Marca As String
        <JsonProperty>
        Public Property Descripcion As String
        <JsonProperty>
        Public Property PorcentajeIva As Double
        <JsonProperty>
        Public Property Referencia As String

        <JsonProperty>
        Public Property ReferenciaProveedor As ReferenciaProveedores

    End Class
End Namespace
