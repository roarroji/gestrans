﻿Imports Newtonsoft.Json

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref=" ColorVehiculos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ColorVehiculos
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ColorVehiculos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ColorVehiculos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String
    End Class
End Namespace
