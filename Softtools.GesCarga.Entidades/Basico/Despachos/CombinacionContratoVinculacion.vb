﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.Despachos
    <JsonObject>
    Public NotInheritable Class CombinacionContratoVinculacion
        Inherits BaseBasico
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Cliente = New Terceros With {.Codigo = Read(lector, "CLIE_Codigo"), .NombreCompleto = Read(lector, "CLIE_Nombre")}
            Remitente = New Terceros With {.Codigo = Read(lector, "REMI_Codigo"), .NombreCompleto = Read(lector, "REMI_Nombre")}
            Tenedor = New Terceros With {.Codigo = Read(lector, "TENE_Codigo"), .NombreCompleto = Read(lector, "TENE_Nombre")}
            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "PRTR_Nombre")}
            TipoCobro = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TCOB_Codigo"), .Nombre = Read(lector, "CATA_TCOB_Nombre")}
            Valor = Read(lector, "Valor")
            Estado = Read(lector, "Estado")
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property Remitente As Terceros
        <JsonProperty>
        Public Property Tenedor As Terceros
        <JsonProperty>
        Public Property Producto As ProductoTransportados
        <JsonProperty>
        Public Property TipoCobro As ValorCatalogos
        <JsonProperty>
        Public Property Valor As Double
    End Class
End Namespace

