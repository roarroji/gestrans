﻿Imports Newtonsoft.Json

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref=" FotosVehiculos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class FotosVehiculos
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="FotosVehiculos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="FotosVehiculos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = lector.Item("EMPR_Codigo")
            Codigo = lector.Item("Codigo")
            CodigoVehiculo = lector.Item("VEHI_Codigo")
            NombreDocumento = lector.Item("Nombre_Documento")
            Documento = lector.Item("Documento")
            ExtencionDocumento = lector.Item("Extencion_Documento")
        End Sub


        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property CodigoVehiculo As Integer
        <JsonProperty>
        Public Property Documento As Byte()
        <JsonProperty>
        Public Property NombreDocumento As String
        <JsonProperty>
        Public Property ExtencionDocumento As String
        <JsonProperty>
        Public Property EliminarTemporal As Short

    End Class
End Namespace
