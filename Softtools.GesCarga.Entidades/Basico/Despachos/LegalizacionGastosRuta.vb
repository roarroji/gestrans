﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.FlotaPropia
Namespace Basico.Despachos

    <JsonObject>
    Public NotInheritable Class LegalizacionGastosRuta
        Inherits BaseBasico

        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "Nombre_RUTA")}
            TipoVehiculo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIVE_Codigo"), .Nombre = Read(lector, "CATA_TIVE_Nombre")}
            Valor = Read(lector, "Valor")
            Concepto = New ConceptoGastos With {.Codigo = Read(lector, "CLGC_Codigo"), .Nombre = Read(lector, "CLGC_Nombre")}
            AplicaAnticipo = Read(lector, "Aplica_Calculo_Anticipo")
        End Sub

        <JsonProperty>
        Public Property Ruta As Rutas

        <JsonProperty>
        Public Property AplicaAnticipo As String

        <JsonProperty>
        Public Property TipoVehiculo As ValorCatalogos
        <JsonProperty>
        Public Property Valor As Integer
        <JsonProperty>
        Public Property Concepto As ConceptoGastos
    End Class
End Namespace