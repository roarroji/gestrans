﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Facturacion

Namespace Basico.Despachos
    ''' <summary>
    ''' Clase <see cref="NovedadesConceptos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class NovedadesConceptos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="NovedadesConceptos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="NovedadesConceptos"/>
        ''' </summary>
        ''' <param name="lector">Objeto DataReader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NODECodigo = Read(lector, "NODE_Codigo")
            COVECodigo = Read(lector, "COVE_Codigo")
            CLPDCodigo = Read(lector, "CLPD_Codigo")

        End Sub

        <JsonProperty>
        Public Property NODECodigo As Integer
        <JsonProperty>
        Public Property COVECodigo As Integer
        <JsonProperty>
        Public Property CLPDCodigo As Integer
        <JsonProperty>
        Public Property ConceptoLiquidacion As ConceptoLiquidacionPlanillaDespacho
        <JsonProperty>
        Public Property ConceptoVenta As ConceptoFacturacion

    End Class

End Namespace