﻿Imports Newtonsoft.Json

Namespace Basico.Despachos
    ''' <summary>
    ''' Clase <see cref="NovedadesDespachos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class NovedadesDespachos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="NovedadesDespachos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="NovedadesDespachos"/>
        ''' </summary>
        ''' <param name="lector">Objeto DataReader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            NovedadSistema = Read(lector, "Novedad_Sistema")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Conceptos As NovedadesConceptos
        <JsonProperty>
        Public Property NovedadSistema As Integer

    End Class

End Namespace