﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.Despachos
    <JsonObject>
    Public NotInheritable Class Peajes
        Inherits BaseDocumento

        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Codigo_Alterno = Read(lector, "Codigo_Alterno")
            TipoPeaje = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIPE_Codigo"), .Nombre = Read(lector, "CATA_TIPE_Nombre")}
            Proveedor = New Tercero With {.Nombre = Read(lector, "PROV_Nombre"), .Codigo = Read(lector, "PROV_Codigo")}
            Contacto = Read(lector, "Contacto")
            Telefono = Read(lector, "Telefono")
            Celular = Read(lector, "Celular")
            Ubicacion = Read(lector, "Ubicacion")
            Longitud = Read(lector, "Longitud")
            Latitud = Read(lector, "Latitud")
            Estado = Read(lector, "Estado")
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub

        <JsonProperty>
        Public Property Peajes As Peajes
        <JsonProperty>
        Public Property Planitlla As Byte()
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Codigo_Alterno As String
        <JsonProperty>
        Public Property TipoPeaje As ValorCatalogos
        <JsonProperty>
        Public Property Proveedor As Tercero
        <JsonProperty>
        Public Property Contacto As String
        <JsonProperty>
        Public Property Telefono As String
        <JsonProperty>
        Public Property Celular As String
        <JsonProperty>
        Public Property Ubicacion As String
        <JsonProperty>
        Public Property Longitud As Double
        <JsonProperty>
        Public Property Latitud As Double
        <JsonProperty>
        Public Property ConsultarTarifasPeajes As Boolean
        <JsonProperty>
        Public Property TarifasPeajes As IEnumerable(Of TarifasPeajes)
    End Class
End Namespace
