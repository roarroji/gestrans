﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref=" PuestoControles"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class PuestoControles
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="PuestoControles"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="PuestoControles"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Estado = Read(lector, "Estado")
            Ubicacion = Read(lector, "Ubicacion")
            Longitud = Read(lector, "Longitud")
            Latitud = Read(lector, "Latitud")
            Obtener = Read(lector, "Obtener")
            TipoPuestoControl = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TPCO_Codigo"), .Nombre = Read(lector, "Tipo_Recaudo")}
            If Obtener = 0 Then
                Proveedor = New Tercero With {.Nombre = Read(lector, "NombreProveedor")}
                TotalRegistros = Read(lector, "TotalRegistros")
            ElseIf Obtener = 2 Then
                Orden = Read(lector, "Orden")
                TiempoEstimado = Read(lector, "Tiempo_Estimado_Arribo")
            Else


                Proveedor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Proveedor")}
                Telefono = Read(lector, "Telefono")
                Contacto = Read(lector, "Contacto")
                Celular = Read(lector, "Celular")
            End If
        End Sub
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Contacto As String
        <JsonProperty>
        Public Property Telefono As String
        <JsonProperty>
        Public Property Celular As String
        <JsonProperty>
        Public Property Ubicacion As String
        <JsonProperty>
        Public Property Proveedor As Tercero
        <JsonProperty>
        Public Property Orden As Integer
        <JsonProperty>
        Public Property TiempoEstimado As DateTime

        <JsonProperty>
        Public Property Longitud As Double
        <JsonProperty>
        Public Property Latitud As Double
        <JsonProperty>
        Public Property TipoPuestoControl As ValorCatalogos

    End Class
End Namespace
