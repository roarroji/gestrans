﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref=" Rutas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Rutas
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Rutas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Rutas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            CiudadOrigen = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Origen"), .Nombre = Read(lector, "CiudadOrigen")}
            CiudadDestino = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino"), .Nombre = Read(lector, "CiudadDestino")}
            TipoRuta = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIRU_Codigo"), .Nombre = Read(lector, "TipoRuta")}
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            DuracionHoras = Read(lector, "Duracion_Horas")
            Kilometros = Read(lector, "Kilometros")
            If Not IsDBNull(Read(lector, "Porcentaje_Anticipo")) Then
                PorcentajeAnticipo = Read(lector, "Porcentaje_Anticipo")
            End If
            PlanPuntos = Read(lector, "Puntos")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
        End Sub
        <JsonProperty>
        Public Property Trayectos As IEnumerable(Of TrayectoRuta)
        ''' <summary>
        ''' Obtiene o establece el nombre de la ruta
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String
        ''' <summary>
        ''' Obtiene o establece la ciudad origen de la ruta
        ''' </summary>
        <JsonProperty>
        Public Property CiudadOrigen As Ciudades
        ''' <summary>
        ''' Obtiene o establece la ciudad destino de la ruta
        ''' </summary>
        <JsonProperty>
        Public Property DuracionHoras As Double
        <JsonProperty>
        Public Property CiudadDestino As Ciudades
        <JsonProperty>
        Public Property NombreCiudadDestino As String
        <JsonProperty>
        Public Property NombreCiudadOrigen As String
        <JsonProperty>
        Public Property PuestosControl As IEnumerable(Of PuestoControles)
        <JsonProperty>
        Public Property ConsultaPuestosControl As Boolean
        <JsonProperty>
        Public Property LegalizacionGastosRuta As IEnumerable(Of LegalizacionGastosRuta)
        <JsonProperty>
        Public Property ConsultaLegalizacionGastosRuta As Boolean
        <JsonProperty>
        Public Property ConsultarPeajes As Boolean
        <JsonProperty>
        Public Property PeajeRutas As IEnumerable(Of PeajeRutas)
        <JsonProperty>
        Public Property TipoRuta As ValorCatalogos
        <JsonProperty>
        Public Property PorcentajeAnticipo As Double
        <JsonProperty>
        Public Property PlanPuntos As Double
        <JsonProperty>
        Public Property Kilometros As Double
        <JsonProperty>
        Public Property Planitlla As Byte()
    End Class
End Namespace
