﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Comercial.Documentos

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref=" Semirremolques"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Semirremolques
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Semirremolques"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Semirremolques"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Placa = Read(lector, "Placa")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Modelo = Read(lector, "Modelo")
            'EstadoSemirremolques = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESSE_Codigo"), .Nombre = Read(lector, "EstadoSemirremolque")}
            Estado = New Estado With {.Codigo = Read(lector, "Estado"), .Nombre = Read(lector, "EstadoSemirremolque")}
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                Marca = New MarcaSemirremolques With {.Nombre = Read(lector, "Marca")}
                Propietario = New Tercero With {.Nombre = Read(lector, "NombrePropietario")}
                TotalRegistros = Read(lector, "TotalRegistros")
                TipoSemirremolque = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TISE_Codigo"), .Nombre = Read(lector, "TipoSemirremolque")}
                JustificacionBloqueo = Read(lector, "Justificacion_Bloqueo")
            Else
                Propietario = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Propietario")}
                EquipoPropio = Read(lector, "Equipo_Propio")
                Marca = New MarcaSemirremolques With {.Codigo = Read(lector, "MASE_Codigo")}
                TipoSemirremolque = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TISE_Codigo"), .Nombre = Read(lector, "TipoSemirremolque")}
                TipoCarroceria = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICA_Codigo")}
                TipoEquipo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIEQ_Codigo")}
                NumeroEjes = Read(lector, "Numero_Ejes")
                LevantaEjes = Read(lector, "Levanta_Ejes")
                Ancho = Read(lector, "Ancho")
                Alto = Read(lector, "Alto")
                Largo = Read(lector, "Largo")
                PesoVacio = Read(lector, "Peso_Vacio")
                Capacidad = Read(lector, "Capacidad_Kilos")
                CapacidadGalones = Read(lector, "Capacidad_Galones")
                JustificacionBloqueo = Read(lector, "Justificacion_Bloqueo")

            End If
            JustificacionNovedad = Read(lector, "Justificacion_Novedad")
            Novedad = New ValorCatalogos With {.Nombre = Read(lector, "Novedad"), .Codigo = Read(lector, "CATA_NOSE_Codigo")}
            TipoSemirremolque = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TISE_Codigo"), .Nombre = Read(lector, "TipoSemirremolque")}
            JustificacionBloqueo = Read(lector, "Justificacion_Bloqueo")
        End Sub

        <JsonProperty>
        Public Property ListaNovedades As IEnumerable(Of NovedadTercero)
        <JsonProperty>
        Public Property Novedad As ValorCatalogos
        <JsonProperty>
        Public Property JustificacionNovedad As String
        <JsonProperty>
        Public Property Placa As String
        <JsonProperty>
        Public Property Propietario As Tercero
        <JsonProperty>
        Public Property EquipoPropio As Integer
        <JsonProperty>
        Public Property Marca As MarcaSemirremolques
        <JsonProperty>
        Public Property TipoSemirremolque As ValorCatalogos
        <JsonProperty>
        Public Property TipoCarroceria As ValorCatalogos
        <JsonProperty>
        Public Property TipoEquipo As ValorCatalogos
        <JsonProperty>
        Public Property Modelo As Integer
        <JsonProperty>
        Public Property NumeroEjes As Integer
        <JsonProperty>
        Public Property LevantaEjes As Integer
        <JsonProperty>
        Public Property Ancho As Double
        <JsonProperty>
        Public Property Largo As Double
        <JsonProperty>
        Public Property Alto As Double
        <JsonProperty>
        Public Property PesoVacio As Double
        <JsonProperty>
        Public Property Capacidad As Double
        <JsonProperty>
        Public Property CapacidadGalones As Double
        '<JsonProperty>
        'Public Property EstadoSemirremolques As ValorCatalogos
        <JsonProperty>
        Public Property Estado As Estado
        <JsonProperty>
        Public Property JustificacionBloqueo As String
        <JsonProperty>
        Public Property Documentos As IEnumerable(Of Documentos)
        <JsonProperty>
        Public Property Planitlla As Byte()
    End Class

    Public Class SemirremolquesDocumentos
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="VehiculosDocumentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="VehiculosDocumentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader con resultado de consulta</param>
        Sub New(lector As IDataReader)
            Placa = Read(lector, "Placa")
            Tercero = New Terceros With {
                .Codigo = Read(lector, "TERC_Codigo"),
                .NombreCompleto = Read(lector, "TERC_Nombre"),
                .NumeroIdentificacion = Read(lector, "TERC_Identificacion"),
                .Celular = Read(lector, "TERC_Celular"),
                .Correo = Read(lector, "TERC_Emails")
            }
            TipoPropietario = Read(lector, "Tipo_Propietario")
            Documento = Read(lector, "Documento")
            FechaVenceDocumento = Read(lector, "Fecha_Vence")
            CantidadDiasFaltantes = Read(lector, "Dias_por_Vencer")
            FechaEmision = Read(lector, "Fecha_Emision")
        End Sub

        <JsonProperty>
        Public Property Semirremolque As Semirremolques
        <JsonProperty>
        Public Property Tercero As Terceros
        <JsonProperty>
        Public Property Placa As String
        <JsonProperty>
        Public Property TipoPropietario As String
        <JsonProperty>
        Public Property Documento As String
        <JsonProperty>
        Public Property FechaEmision As DateTime
        <JsonProperty>
        Public Property FechaVenceDocumento As DateTime
        <JsonProperty>
        Public Property Fecha As DateTime
        <JsonProperty>
        Public Property CantidadDiasFaltantes As String
        <JsonProperty>
        Public Property CodigoDetalleEpos As Integer
        <JsonProperty>
        Public Property TipoAplicativo As Integer

    End Class
End Namespace
