﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref=" SitiosCargueDescargue"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class SitiosCargueDescargue
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SitiosCargueDescargue"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SitiosCargueDescargue"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            CodigoTipoSitio = Read(lector, "CATA_TSCD_Codigo")
            Direccion = Read(lector, "Direccion")
            Contacto = Read(lector, "Contacto")
            Estado = Read(lector, "Estado")
            Longitud = Read(lector, "Longitud")
            Latitud = Read(lector, "Latitud")
            Obtener = Read(lector, "Obtener")
            If Obtener = 1 Then
                Pais = New Paises With {.Codigo = Read(lector, "PAIS_Codigo")}
                Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo"), .Departamento = New Departamentos With {.Nombre = Read(lector, "NombreDepartamento")}}
                Zona = New Zonas With {.Codigo = Read(lector, "ZOCI_Codigo")}
            End If
            If Obtener = 0 Then
                Pais = New Paises With {.Codigo = Read(lector, "PAIS_Codigo"), .Nombre = Read(lector, "Nombre_Pais")}
                Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo"), .Nombre = Read(lector, "Nombre_Ciudad"), .Departamento = New Departamentos With {.Nombre = Read(lector, "NombreDepartamento")}}
                Zona = New Zonas With {.Codigo = Read(lector, "ZOCI_Codigo"), .Nombre = Read(lector, "Nombre_Zona")}
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
            Telefono = Read(lector, "Telefono")
            ValorCargue = Read(lector, "Valor_Cargue")
            ValorDescargue = Read(lector, "Valor_Descargue")
        End Sub


        <JsonProperty>
        Public Property ValorDescargue As Long
        <JsonProperty>
        Public Property ValorCargue As Long
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property CodigoTipoSitio As Integer
        <JsonProperty>
        Public Property Pais As Paises
        <JsonProperty>
        Public Property Ciudad As Ciudades
        <JsonProperty>
        Public Property Zona As Zonas
        <JsonProperty>
        Public Property Direccion As String
        <JsonProperty>
        Public Property Telefono As String
        <JsonProperty>
        Public Property Contacto As String
        <JsonProperty>
        Public Property Longitud As Double
        <JsonProperty>
        Public Property Latitud As Double
        <JsonProperty>
        Public Property InsertoPlantilla As Short
        <JsonProperty>
        Public Property Planitlla As Byte()
    End Class
End Namespace
