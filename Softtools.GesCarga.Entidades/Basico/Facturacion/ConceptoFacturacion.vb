﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.Facturacion

    ''' <summary>
    ''' Clase <see cref=" ConceptoFacturacion"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ConceptoFacturacion
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ConceptoFacturacion"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ConceptoFacturacion"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            ConceptoSistema = Read(lector, "Concepto_Sistema")
            AplicaImpuesto = Read(lector, "Impuesto")
            Operacion = Read(lector, "Operacion")
            FechaCrea = Read(lector, "Fecha_Crea")


            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")

            Else

            End If

        End Sub
        <JsonProperty>
        Public Property Placa As String

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property ConceptoSistema As Integer

        <JsonProperty>
        Public Property AplicaImpuesto As Integer

        <JsonProperty>
        Public Property Operacion As Integer

        <JsonProperty>
        Public Property Anular As Integer

        <JsonProperty>
        Public Property CodigoImpuesto As Integer
        <JsonProperty>
        Public Property Ciudad As Ciudades

        <JsonProperty>
        Public Property ListadoImpuestos As IEnumerable(Of ImpuestoConceptoFacturacion)


    End Class
End Namespace
