﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" CodigoDaneCiudades"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class CodigoDaneCiudades
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="CodigoDaneCiudades"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="CodigoDaneCiudades"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Departamento = New Departamentos With {.Codigo = Read(lector, "Codigo_Departamento"), .Nombre = Read(lector, "Departamento")}
            Ciudad = New Ciudades With {.Codigo = Read(lector, "Codigo_Ciudad"), .Nombre = Read(lector, "Ciudad")}
            TotalRegistros = Read(lector, "TotalRegistros")
            CodigoDivision = Read(lector, "Codigo_Division")


        End Sub

        <JsonProperty>
        Public Property Master As Long
        <JsonProperty>
        Public Property UsuarioCrea As Usuarios
        <JsonProperty>
        Public Property Obtenido As Long
        <JsonProperty>
        Public Property CodigoDivision As String
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Codigo As Integer
        ''' <summary>
        ''' Obtiene o establece si el catalogo es actualizable o no
        ''' </summary>
        <JsonProperty>
        Public Property Departamento As Departamentos

        ''' <summary>
        ''' Obtiene o establece el numero de columnas del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Ciudad As Ciudades


    End Class
End Namespace
