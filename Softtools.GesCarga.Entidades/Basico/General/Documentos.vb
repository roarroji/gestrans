﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" Documentos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Documentos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Documentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Documentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "CDGD_Codigo")
            Configuracion = New ConfiguracionGestionDocumentos With {.Codigo = Read(lector, "CDGD_Codigo")}
            If Not IsDBNull(Read(lector, "Emisor")) Then
                Emisor = Read(lector, "Emisor")
            End If
            If Not IsDBNull(Read(lector, "Referencia")) Then
                Referencia = Read(lector, "Referencia")
            End If
            If Not IsDBNull(Read(lector, "Fecha_Emision")) Then
                FechaEmision = Read(lector, "Fecha_Emision")
            End If
            If Not IsDBNull(Read(lector, "Fecha_Vence")) Then
                FechaVence = Read(lector, "Fecha_Vence")
            End If
            ValorDocumento = Read(lector, "ValorDocumento")
            If Not IsDBNull(Read(lector, "Archivo")) Then
                Archivo = Read(lector, "Archivo")
                If Not IsDBNull(Read(lector, "Nombre_Documento")) Then
                    Nombre = Read(lector, "Nombre_Documento")
                End If
                If Not IsDBNull(Read(lector, "Extension")) Then
                    Extension = Read(lector, "Extension")
                End If
                If Not IsDBNull(Read(lector, "Tipo")) Then
                    Tipo = Read(lector, "Tipo")
                End If
            End If
            Numero = Read(lector, "Numero")

            'Cumplido
            NombreDocumento = Read(lector, "NombreArchivo")
            ExtensionDocumento = Read(lector, "ExtensionArchivo")
            NombreAsignado = Read(lector, "Nombre_Asignado")
            Id = Read(lector, "ID")
            AplicaInactivacion = Read(lector, "Obligatorio_Inactivacion")
            Descripcion = Read(lector, "Descripcion")

        End Sub

        <JsonProperty>
        Public Property AplicaInactivacion As Integer
        <JsonProperty>
        Public Property Vehiculo As Boolean
        <JsonProperty>
        Public Property CodigoVehiculo As Integer
        <JsonProperty>
        Public Property Terceros As Boolean
        <JsonProperty>
        Public Property CodigoTerceros As Integer
        <JsonProperty>
        Public Property Semirremolques As Boolean
        <JsonProperty>
        Public Property CodigoSemirremolques As Integer
        <JsonProperty>
        Public Property CodigoTemporal As Integer
        <JsonProperty>
        Public Property Configuracion As ConfiguracionGestionDocumentos
        <JsonProperty>
        Public Property Referencia As String
        <JsonProperty>
        Public Property Emisor As String
        <JsonProperty>
        Public Property FechaEmision As DateTime
        <JsonProperty>
        Public Property FechaVence As DateTime
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Extension As String
        <JsonProperty>
        Public Property Archivo As Byte()
        <JsonProperty>
        Public Property Tipo As String
        <JsonProperty>
        Public Property Id As Integer
        <JsonProperty>
        Public Property EliminarTemporal As Integer
        <JsonProperty>
        Public Property Temporal As Boolean
        <JsonProperty>
        Public Property ValorDocumento As Integer
        <JsonProperty>
        Public Property EliminaDocumento As Integer
        <JsonProperty>
        Public Property NumeroCumplido As Long
        <JsonProperty>
        Public Property NombreAsignado As String
        <JsonProperty>
        Public Property Cumplido As Boolean
        <JsonProperty>
        Public Property HaciaTemporal As Byte
        <JsonProperty>
        Public Property NombreDocumento As String
        <JsonProperty>
        Public Property ExtensionDocumento As String
        <JsonProperty>
        Public Property CumplidoRemesa As Boolean
        <JsonProperty>
        Public Property Remesa As Boolean
        <JsonProperty>
        Public Property Descripcion As String

    End Class
End Namespace
