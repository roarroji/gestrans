﻿Imports Newtonsoft.Json
Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="EmpresaManifiestoElectronico"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EmpresaManifiestoElectronico
        Inherits BaseDocumento
        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="EmpresaManifiestoElectronico"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EmpresaManifiestoElectronico"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "Codigo_Empresa")
            CodigoRegionalMinisterio = Read(lector, "Codigo_Regional_Ministerio")
            CodigoEmpresaMinisterio = Read(lector, "Codigo_Empresa_Ministerio")
            UsuarioManifiestoElectronico = Read(lector, "Usuario_Manifiesto_Electronico")
            NumeroResolucion = Read(lector, "Numero_Resolucion_Habilitacion_Empresa")
            ClaveManifiestoElectronico = Read(lector, "Clave_Manifiesto_Electronico")
            EnlaceWSMinisterio = Read(lector, "Enlace_WS_Ministerio")

        End Sub

        <JsonProperty>
        Public Property CodigoRegionalMinisterio As String
        <JsonProperty>
        Public Property CodigoEmpresaMinisterio As Integer
        <JsonProperty>
        Public Property UsuarioManifiestoElectronico As String
        <JsonProperty>
        Public Property NumeroResolucion As Integer
        <JsonProperty>
        Public Property ClaveManifiestoElectronico As String
        <JsonProperty>
        Public Property EnlaceWSMinisterio As String

    End Class

End Namespace
