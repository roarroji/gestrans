﻿Imports Newtonsoft.Json
Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="Empresas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Empresas
        Inherits BaseDocumento
        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="Empresas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Empresas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            ObtenerValor = Read(lector, "Obtener")

            If ObtenerValor = 0 Then
                Try
                    Codigo = Read(lector, "Codigo")

                    CodigoAlterno = Read(lector, "Codigo_Alterno")
                    RazonSocial = Read(lector, "Nombre_Razon_Social")
                    NumeroIdentificacion = Read(lector, "Numero_Identificacion")
                    Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo"), .Nombre = Read(lector, "Nombre_Ciudad")}
                    Pais = New Paises With {.Codigo = Read(lector, "PAIS_Codigo")}
                    Telefono = Read(lector, "Telefonos")
                    Direccion = Read(lector, "Direccion")
                    CodigoPostal = Read(lector, "Codigo_Postal")
                    Email = Read(lector, "Emails")
                    PaginaWeb = Read(lector, "Pagina_Web")
                    MensajeBanner = Read(lector, "Mensaje_Banner")
                    GeneraManifiesto = Read(lector, "Genera_Manifiesto")
                    AprobarLiquidacion = Read(lector, "Aprobar_Liquidacion_Despachos")
                    Usuario = New SeguridadUsuarios.Usuarios With {.CodigoUsuario = Read(lector, "Codigo_Usuario"), .Correo = Read(lector, "Emails"), .CodigoCambioClave = Read(lector, "Codigo_Cambio_Clave"), .FechaVigenciaCambioClave = Read(lector, "Fecha_Vigencia_Cambio_Clave"), .Codigo = Read(lector, "Codigo"), .Clave = Read(lector, "Clave"), .ExisteUsuario = Read(lector, "ExisteUsuario"), .CorreoManager = Read(lector, "CorreoManager")}


                Catch ex As Exception

                End Try
            End If
            ReportaRNDC = Read(lector, "ReportaRNDC")
            If ObtenerValor = 1 Then
                Codigo = Read(lector, "Codigo")
                CodigoAlterno = Read(lector, "Codigo_Alterno")
                RazonSocial = Read(lector, "Nombre_Razon_Social")
                NombreCortoRazonSocial = Read(lector, "Nombre_Corto_Razon_Social")
                NumeroIdentificacion = Read(lector, "Numero_Identificacion")
                Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo"), .Nombre = Read(lector, "Nombre_Ciudad")}
                Telefono = Read(lector, "Telefonos")
            End If

            If ObtenerValor = 2 Then
                Codigo = Read(lector, "Codigo")
                CodigoAlterno = Read(lector, "Codigo_Alterno")
                RazonSocial = Read(lector, "Nombre_Razon_Social")
                NombreCortoRazonSocial = Read(lector, "Nombre_Corto_Razon_Social")
                CodigoTipoDocumento = Read(lector, "CATA_TIID_Codigo")
                NumeroIdentificacion = Read(lector, "Numero_Identificacion")
                DigitoChequeo = Read(lector, "Digito_Chequeo")
                Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo")}
                Telefono = Read(lector, "Telefonos")
                Direccion = Read(lector, "Direccion")
                Pais = New Paises With {.Codigo = Read(lector, "PAIS_Codigo")}
                CodigoPostal = Read(lector, "Codigo_Postal")
                Email = Read(lector, "Email")
                PaginaWeb = Read(lector, "Pagina_Web")
                Estado = Read(lector, "Estado")
                MensajeBanner = Read(lector, "Mensaje_Banner")
                AprobarLiquidacion = Read(lector, "Aprobar_Liquidacion_Despachos")


            End If
            Aseguradora = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Aseguradora")}
            NumeroPoliza = Read(lector, "Numero_Poliza")
            FechaVencimientoPoliza = Read(lector, "Fecha_Vencimiento_Poliza")
            DiasVigenciaPlanPuntos = Read(lector, "Dias_Vigencia_Plan_Puntos")
            ValorPoliza = Read(lector, "Valor_Poliza")
            ValorCombustible = Read(lector, "Valor_Galon_Combustilbe")
            CodigoUIAF = Read(lector, "Codigo_UIAF")
            AceptacionElectronica = Read(lector, "AceptacionElectronica")
            Prefijo = Read(lector, "Prefijo")
            ActividadEconomica = Read(lector, "Actividad_Economica")
        End Sub

        ''' Obtiene e establece la razon de la empresa
        ''' <summary>
        ''' </summary>
        <JsonProperty>
        Public Property RazonSocial As String
        ''' Obtiene e establece la razon de la empresa
        ''' <summary>
        ''' </summary>
        <JsonProperty>
        Public Property NombreCortoRazonSocial As String
        ''' <summary>
        ''' Obtiene e establece el codigo del tipo de documento de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property TipoNaturaleza As String
        ''' <summary>
        ''' Obtiene el tipo de naturaleza
        ''' </summary>
        <JsonProperty>
        Public Property CodigoTipoDocumento As Integer
        ''' <summary>
        ''' Obtiene e establece el numero de idetificacion de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property NumeroIdentificacion As String
        ''' <summary>
        ''' Obtiene e establece el numero de idetificacion de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property AprobarLiquidacion As Integer
        ''' <summary>
        ''' Obtiene e establece el digito de chequeo de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property TipoNumeroIdentificacion As String
        ''' <summary>
        ''' Obtiene e establece el valor string del tipo de identificacion
        ''' </summary>
        <JsonProperty>
        Public Property DigitoChequeo As Short
        ''' <summary>
        ''' Obtiene e establece la ciudad de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property Pais As Paises
        ''' <summary>
        ''' Obtiene e establece la ciudad de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property Ciudad As Ciudades
        ''' <summary>
        ''' Obtiene e establece el teléfono de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property Direccion As String
        ''' <summary>
        ''' Obtiene e establece el teléfono de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property Telefono As String
        ''' <summary>
        ''' Obtiene e establece el codigo del tipo de documento de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property CodigoPostal As String
        ''' <summary>
        ''' Obtiene e establece el Email de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property Email As String
        ''' <summary>
        ''' Obtiene e establece el PaginaWeb de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property PaginaWeb As String
        ''' <summary>
        ''' Obtiene e establece el MensajeBanner de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property MensajeBanner As String
        ''' <summary>
        ''' Obtiene e establece el MensajeBanner de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property Prefijo As String
        ''' <summary>
        ''' Obtiene e establece el codigo del tipo de documento de la empresa
        ''' </summary>
        <JsonProperty>
        Public Property ObtenerValor As Integer
        ''' <summary>
        ''' Obtiene y establece si genera manifiesto
        ''' </summary>
        <JsonProperty>
        Public Property GeneraManifiesto As Integer
        ''' <summary>
        ''' Obtiene y establece si reporta al RNDC
        ''' </summary>
        <JsonProperty>
        Public Property ReportaRNDC As Integer

        <JsonProperty>
        Public Property ListadoRNDC As EmpresaManifiestoElectronico
        <JsonProperty>
        Public Property EmpresaFacturaElectronica As EmpresaFacturaElectonica
        <JsonProperty>
        Public Property Aseguradora As Tercero
        <JsonProperty>
        Public Property NumeroPoliza As String
        <JsonProperty>
        Public Property FechaVencimientoPoliza As Date
        <JsonProperty>
        Public Property Validaciones As IEnumerable(Of EmpresaValidaciones)
        <JsonProperty>
        Public Property Controles As IEnumerable(Of EmpresaControles)
        <JsonProperty>
        Public Property DiasVigenciaPlanPuntos As Integer
        <JsonProperty>
        Public Property ValorPoliza As Double
        <JsonProperty>
        Public Property ValorCombustible As Double
        <JsonProperty>
        Public Property Usuario As SeguridadUsuarios.Usuarios
        <JsonProperty>
        Public Property CodigoUIAF As Integer
        <JsonProperty>
        Public Property AceptacionElectronica As Integer
        <JsonProperty>
        Public Property ActividadEconomica As String
    End Class

End Namespace
