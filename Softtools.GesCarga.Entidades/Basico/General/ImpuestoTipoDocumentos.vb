﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="ImpuestoTipoDocumentos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ImpuestoTipoDocumentos
        Inherits Base
        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="ImpuestoTipoDocumentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ImpuestoTipoDocumentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Obtener = Read(lector, "Obtener")
            If Obtener = 1 Then
                CodigoImpuesto = Read(lector, "ENIM_Codigo")
                Nombre = Read(lector, "Nombre")
                Estado = Read(lector, "Estado")
            Else
                Numero = Read(lector, "Numero")

                CodigoTipoDocumento = Read(lector, "TIDO_Codigo")
                CodigoImpuesto = Read(lector, "ENIM_Codigo")
                ValorTarifa = Read(lector, "Valor_Tarifa")
                ValorBase = Read(lector, "Valor_Base")
                Valor = Read(lector, "Valor")
                ValorImpuesto = Read(lector, "Valor_Impuesto")
                CodigoCiudadImpuesto = Read(lector, "CIUD_Impuesto")
                CodigoTipoRecaudoImpuesto = Read(lector, "CATA_TRAI_Codigo")
                Label = Read(lector, "Label")
                Operacion = Read(lector, "Operacion")
                CodigoRuta = Read(lector, "")
            End If


        End Sub

        <JsonProperty>
        Public Property CodigoTipoDocumento As Integer
        <JsonProperty>
        Public Property CodigoImpuesto As Integer
        <JsonProperty>
        Public Property Estado As Integer

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property CodigoRuta As Double
        <JsonProperty>
        Public Property Numero As Long
        <JsonProperty>
        Public Property ValorTarifa As Double
        <JsonProperty>
        Public Property ValorBase As Double
        <JsonProperty>
        Public Property Valor As Double
        <JsonProperty>
        Public Property ValorImpuesto As Double
        <JsonProperty>
        Public Property CodigoCiudadImpuesto As Integer
        <JsonProperty>
        Public Property CodigoTipoRecaudoImpuesto As Integer
        <JsonProperty>
        Public Property Label As String
        <JsonProperty>
        Public Property Operacion As Byte


    End Class

End Namespace
