﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" Impuestos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Impuestos
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Impuestos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Impuestos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            Label = Read(lector, "Label")
            Tipo_Recaudo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TRAI_Codigo"), .Nombre = Read(lector, "Tipo_Recaudo")}
            Tipo_Impuesto = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIIM_Codigo"), .Nombre = Read(lector, "Tipo_Impuesto")}
            Operacion = Read(lector, "Operacion")
            PUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo")}
            Valor_tarifa = Read(lector, "Valor_Tarifa")
            valor_base = Read(lector, "Valor_Base")
            Estado = Read(lector, "Estado")
            Codigo_USUA_Crea = Read(lector, "USUA_Codigo_Crea")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
            Try

                Codigo_USUA_Modi = Read(lector, "USUA_Codigo_Modifica")
            Catch ex As Exception

            End Try



        End Sub

        <JsonProperty>
        Public Property CodigoTercero As String

        <JsonProperty>
        Public Property CodigoAlterno As String


        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property Tipo_Recaudo As ValorCatalogos

        <JsonProperty>
        Public Property Tipo_Impuesto As ValorCatalogos

        <JsonProperty>
        Public Property Operacion As Integer

        <JsonProperty>
        Public Property Valor_tarifa As Double

        <JsonProperty>
        Public Property valor_base As Long

        <JsonProperty>
        Public Property PUC As PlanUnicoCuentas

        <JsonProperty>
        Public Property Codigo As Integer

        <JsonProperty>
        Public Property Codigo_USUA_Crea As Integer

        <JsonProperty>
        Public Property Codigo_USUA_Modi As Integer

        <JsonProperty>
        Public Property Estado As Integer

        <JsonProperty>
        Public Property Label As String


        <JsonProperty>
        Public Property PaginaObtener As Integer
        <JsonProperty>
        Public Property AplicaDetalle As Integer
        <JsonProperty>
        Public Property AplicaTipoDocumento As Integer
        <JsonProperty>
        Public Property CodigoTipoDocumento As Integer
        <JsonProperty>
        Public Property CodigoCiudad As Integer
        <JsonProperty>
        Public Property CodigoDepartamento As Integer

        <JsonProperty>
        Public Property ListadoCiudades As IEnumerable(Of impuestosCiudades)
        <JsonProperty>
        Public Property ListadoDepartamentos As IEnumerable(Of ImpuestosDepartamento)



    End Class
End Namespace
