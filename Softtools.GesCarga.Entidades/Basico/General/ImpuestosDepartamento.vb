﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico.General
    ''' <summary>
    ''' Clase <see cref="ImpuestosDepartamento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ImpuestosDepartamento
        Inherits Departamentos

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ImpuestosDepartamento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ImpuestosDepartamento"/>
        ''' </summary>
        ''' <param name="lector">Objeto DataReader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")

            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            ValorTarifa = Read(lector, "Valor_Tarifa")
            ValorBase = Read(lector, "Valor_Base")
            CuentaPUC = New PlanUnicoCuentas With {.CodigoNombreCuenta = Read(lector, "NombreCuenta"), .Codigo = Read(lector, "CodigoPuc")}




        End Sub

        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property ValorTarifa As Double
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property ValorBase As Double

        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas


    End Class

End Namespace