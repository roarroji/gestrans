﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" ImpuestosTerceros"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ImpuestosTerceros
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ImpuestosTerceros"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ImpuestosTerceros"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Tipo_Impuesto = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIIM_Codigo")}
            Estado = Read(lector, "Estado")

        End Sub

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property Tipo_Impuesto As ValorCatalogos

        <JsonProperty>
        Public Property CodigoTercero As Integer

        <JsonProperty>
        Public Property Codigo As Integer

        <JsonProperty>
        Public Property Codigo_USUA_Crea As Integer

        <JsonProperty>
        Public Property Codigo_USUA_Modi As Integer

        <JsonProperty>
        Public Property Estado As Integer

    End Class
End Namespace
