﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" Monedas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Monedas
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Monedas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Monedas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            NombreCorto = Read(lector, "Nombre_Corto")
            Simbolo = Read(lector, "Simbolo")
            Local = Read(lector, "Local")
            Estado = Read(lector, "Estado")
            Valor_Moneda_Local = Read(lector, "Valor_Moneda_Local")

            Try
                TotalRegistros = Read(lector, "TotalRegistros")
                PaginaObtener = Read(lector, "PaginaObtener")
                RegistrosPagina = Read(lector, "RegistrosPagina")
                Fecha = Read(lector, "Fecha")

            Catch ex As Exception

            End Try



        End Sub
        <JsonProperty>
        Public Property Codigo As Double

        <JsonProperty>
        Public Property NombreCorto As String

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property Simbolo As String

        <JsonProperty>
        Public Property Local As Short

        <JsonProperty>
        Public Property Valor_Moneda_Local As Double

        <JsonProperty>
        Public Property PaginaObtener As Integer
    End Class
End Namespace
