﻿Imports Newtonsoft.Json

Namespace Basico.General
    ''' <summary>
    ''' Clase <see cref="Paises"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Paises
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Paises"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Paises"/>
        ''' </summary>
        ''' <param name="lector">Objeto DataReader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            Moneda = New Monedas With {.Codigo = Read(lector, "MONE_Codigo")}
            Estado = Read(lector, "Estado")
            NombreCorto = Read(lector, "Nombre_Corto")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
        End Sub

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property NombreCorto As String

        <JsonProperty>
        Public Property Moneda As Monedas
    End Class

End Namespace