﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" ProductoTransportados"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ProductoTransportados
        Inherits BaseDocumento
        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ProductoTransportados"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ProductoTransportados"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            CodigoMinisterio = Read(lector, "Codigo_Ministerio")
            ProductoMinisterio = Read(lector, "Producto_Ministerio")
            Descripcion = Read(lector, "Descripcion")
            UnidadMedida = Read(lector, "Unidad_Medida")
            UnidadEmpaque = Read(lector, "Unidad_Empaque")
            LineaProducto = Read(lector, "Linea_Producto")
            NaturalezaProducto = Read(lector, "Naturaleza_Producto")
            Estado = Read(lector, "Estado")
            TotalRegistros = Read(lector, "TotalRegistros")
            AplicaTarifario = Read(lector, "Aplica_Tarifario")
            CodigoProductoMinisterio = Read(lector, "CodigoProductoMinisterio")
            Peso = Read(lector, "Peso")
            NivelRiesgo = Read(lector, "CATA_NRPR_Codigo")

        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property ProductoMinisterio As String
        <JsonProperty>
        Public Property Descripcion As String
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property CodigoMinisterio As Integer
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property UnidadMedida As Integer
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property UnidadEmpaque As Integer
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property NivelRiesgo As Integer
        <JsonProperty>
        Public Property LineaProducto As Integer
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property NaturalezaProducto As Integer
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property AplicaTarifario As Short

        <JsonProperty>
        Public Property NombreCompleto As String

        <JsonProperty>
        Public Property CodigoProductoMinisterio As Integer

        <JsonProperty>
        Public Property Plantilla As Byte()

        <JsonProperty>
        Public Property Peso As Double

    End Class
End Namespace
