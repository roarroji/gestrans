﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="ProductosMinisterioTransporte"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ProductosMinisterioTransporte
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ProductosMinisterioTransporte"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ProductosMinisterioTransporte"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Nombre")
            NombreCompleto = Read(lector, "NombreCompleto")
            Descripcion = Read(lector, "Descripcion")
            Estado = Read(lector, "Estado")
            TotalRegistros = Read(lector, "TotalRegistros")

        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Descripcion As String
        <JsonProperty>
        Public Property NombreCompleto As String

    End Class
End Namespace
