﻿Imports Newtonsoft.Json

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RegionesPaises"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class RegionesPaises
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="RegionesPaises"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="RegionesPaises"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            Obtener = Read(lector, "Obtener")

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Pais = New Paises With {.Codigo = Read(lector, "PAIS_Codigo")}
            Estado = Read(lector, "Estado")

        End Sub

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property Pais As Paises

    End Class
End Namespace

