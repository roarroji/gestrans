﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios


Namespace Basico.General


    <JsonObject>
    Public NotInheritable Class UnidadEmpaqueProductosTransportados
        Inherits Base
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Nombre = Read(lector, "Descripcion")
            Estado = Read(lector, "Estado")
            FechaCrea = Read(lector, "Fecha_Crea")
            FechaModifica = Read(lector, "Fecha_Moodifica")
            UsuarioModifica = Read(lector, "USUA_Codigo_Modifica")
            TotalRegistros = Read(lector, "TotalRegistros")
            NombreCorto = Read(lector, "Nombre_Corto")
            UsuarioCrea = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Crea")}

        End Sub

        <JsonProperty>
        Public Property Codigo As Long
        <JsonProperty>
        Public Property CodigoAlterno As String
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Estado As Short
        <JsonProperty>
        Public Property FechaCrea As DateTime
        <JsonProperty>
        Public Property UsuarioCrea As Usuarios
        <JsonProperty>
        Public Property FechaModifica As DateTime
        <JsonProperty>
        Public Property UsuarioModifica As Long
        <JsonProperty>
        Public Property NombreCorto As String
    End Class




End Namespace
