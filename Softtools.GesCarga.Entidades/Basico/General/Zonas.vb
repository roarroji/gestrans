﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Paqueteria

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="Zonas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Zonas
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Zonas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Zonas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo"), .Nombre = Read(lector, "Nombre_Ciudad"), .Departamento = New Departamentos With {.Nombre = Read(lector, "NombreDepartamento")}}
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            RemesaPaqueteria = New RemesaPaqueteria With {.Codigo = Read(lector, "Remesa"), .NumeroDocumento = Read(lector, "Numero_Documento")}


        End Sub

        <JsonProperty>
        Public Property RemesaPaqueteria As RemesaPaqueteria

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property CadenaCiudades As String
        <JsonProperty>
        Public Property Ciudad As Ciudades

        <JsonProperty>
        Public Property ListaZonas As IEnumerable(Of Zonas)

    End Class
End Namespace

