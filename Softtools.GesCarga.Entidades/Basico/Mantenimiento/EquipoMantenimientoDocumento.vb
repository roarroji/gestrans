﻿Imports Newtonsoft.Json

Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref="EquipoMantenimientoDocumento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EquipoMantenimientoDocumento
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EquipoMantenimientoDocumento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EquipoMantenimientoDocumento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Referencia = Read(lector, "Referencia")
            Documento = Read(lector, "Documento")
            ValorDocumento = Read(lector, "ValorDocumento")
            Extencion = Read(lector, "Extencion_Documento")
            ID_Documento = Read(lector, "ID")

        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Referencia As String
        <JsonProperty>
        Public Property NombreDocumento As String
        <JsonProperty>
        Public Property ExtensionDocumento As String

        <JsonProperty>
        Public Property Documento As Byte()
        <JsonProperty>
        Public Property ValorDocumento As Short
        <JsonProperty>
        Public Property ID_Documento As Integer
        <JsonProperty>
        Public Property ID_Documento_temporal As Integer
        <JsonProperty>
        Public Property ID_Documento_Modifica As Integer
        <JsonProperty>
        Public Property Extencion As String

        <JsonProperty>
        Public Property idRow As Integer

        <JsonProperty>
        Public Property temp As Integer
    End Class
End Namespace
