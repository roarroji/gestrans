﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref="EstadoEquipoMantenimiento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EstadoEquipoMantenimiento
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EstadoEquipoMantenimiento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EstadoEquipoMantenimiento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = New ValorCatalogos With {.Codigo = Read(lector, "Estado"), .Nombre = Read(lector, "NombreEstado")}

            Try
                TotalRegistros = Read(lector, "TotalRegistros")
            Catch
                TotalRegistros = 0
            End Try

        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property Estado As ValorCatalogos



        <JsonProperty>
        Public Property idRow As Integer






    End Class
End Namespace
