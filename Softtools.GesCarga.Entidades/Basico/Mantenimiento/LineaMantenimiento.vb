﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref=" LineaMantenimiento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class LineaMantenimiento
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaMantenimiento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaMantenimiento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = New ValorCatalogos With {.Codigo = Read(lector, "CodigoEstado"), .Nombre = Read(lector, "NombreEstado")}
            TotalRegistros = Read(lector, "TotalRegistros")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
        End Sub

        <JsonProperty>
        Public Property CodigoAlterno As String
        <JsonProperty>
        Public Property Codigo As Short
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Estado As ValorCatalogos
        <JsonProperty>
        Public Property UsuarioCrea As Usuarios
    End Class
End Namespace