﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Mantenimiento


    <JsonObject>
    Public NotInheritable Class MarcaEquipoMantenimiento
        Inherits BaseDocumento


        Sub New()
        End Sub


        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Estado = New ValorCatalogos With {.Codigo = Read(lector, "CodigoEstado"), .Nombre = Read(lector, "Estado")}
            Try
                TotalRegistros = Read(lector, "TotalRegistros")
            Catch
                TotalRegistros = 0
            End Try
        End Sub
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property CodigoAlterno As String
        <JsonProperty>
        Public Property Estado As ValorCatalogos
    End Class
End Namespace