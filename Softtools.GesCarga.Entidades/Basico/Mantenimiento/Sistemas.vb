﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico

    ''' <summary>
    ''' Clase <see cref=" Sistemas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Sistemas
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Sistemas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Sistemas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            LineaMantenimiento = New LineaMantenimiento With {.Codigo = Read(lector, "LIMA_Codigo"), .Nombre = Read(lector, "LineaMantenimiento")}
            Estado = New ValorCatalogos With {.Codigo = Read(lector, "CodigoEstado"), .Nombre = Read(lector, "Estado")}
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Try
                TotalRegistros = Read(lector, "TotalRegistros")
            Catch
            End Try

        End Sub

        <JsonProperty>
        Public Property CodigoAlterno As String
        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property LineaMantenimiento As LineaMantenimiento


        <JsonProperty>
        Public Property Estado As ValorCatalogos


#Region "Filtros de Búsqueda"


#End Region
    End Class
End Namespace
