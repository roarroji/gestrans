﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Namespace Basico

    ''' <summary>
    ''' Clase <see cref=" SubSistemas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class SubSistemas
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SubSistemas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="SubSistemas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            TipoSubsistema = New TipoSubsistemas With {.Codigo = Read(lector, "TISM_Codigo"), .Nombre = Read(lector, "TipoSubSistema")}
            SistemaMantenimiento = New Sistemas With {.Codigo = Read(lector, "SIMA_Codigo"), .Nombre = Read(lector, "SistemaMantenimiento")}
            Estado = New ValorCatalogos With {.Codigo = Read(lector, "CodigoEstado"), .Nombre = Read(lector, "Estado")}
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Try
                TotalRegistros = Read(lector, "TotalRegistros")
            Catch
            End Try

        End Sub
        <JsonProperty>
        Public Property CodigoAlterno As String


        <JsonProperty>
        Public Property Nombre As String


        <JsonProperty>
        Public Property SistemaMantenimiento As Sistemas


        <JsonProperty>
        Public Property TipoSubsistema As TipoSubsistemas



        <JsonProperty>
        Public Property Estado As ValorCatalogos


#Region "Filtros de Búsqueda"

#End Region
    End Class
End Namespace
