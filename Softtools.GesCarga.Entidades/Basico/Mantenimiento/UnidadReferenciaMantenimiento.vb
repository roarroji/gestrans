﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General


Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref="UnidadReferenciaMantenimiento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class UnidadReferenciaMantenimiento
        Inherits BaseDocumento


        Sub New()
        End Sub


        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            NombreCorto = Read(lector, "Nombre_Corto")
            Estado = New ValorCatalogos With {.Codigo = Read(lector, "Estado")}

            Try
                TotalRegistros = Read(lector, "TotalRegistros")
            Catch
                TotalRegistros = 0
            End Try
        End Sub

        <JsonProperty>
        Public Property Nombre As String


        <JsonProperty>
        Public Property NombreCorto As String


        <JsonProperty>
        Public Property Estado As ValorCatalogos
    End Class
End Namespace
