﻿Imports Newtonsoft.Json

Namespace Basico.ServicioCliente

    ''' <summary>
    ''' Clase <see cref="TarifaTransporteCarga"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TarifaTransporteCarga
        Inherits BaseBasico

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="TarifaTransporteCarga"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TarifaTransporteCarga"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            Obtener = Read(lector, "Obtener")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")

        End Sub

        <JsonProperty>
        Public Property Nombre As String

    End Class

End Namespace
