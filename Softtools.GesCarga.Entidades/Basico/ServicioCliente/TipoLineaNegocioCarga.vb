﻿Imports Newtonsoft.Json

Namespace Basico.ServicioCliente

    ''' <summary>
    ''' Clase <see cref="TipoLineaNegocioCarga"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TipoLineaNegocioCarga
        Inherits BaseBasico

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="TipoLineaNegocioCarga"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoLineaNegocioCarga"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            LineaNegocioTransporteCarga = New LineaNegocioTransporteCarga With {.Codigo = Read(lector, "LNTC_Codigo"), .Nombre = Read(lector, "LineaNegocio")}

        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property LineaNegocioTransporteCarga As LineaNegocioTransporteCarga

    End Class

End Namespace
