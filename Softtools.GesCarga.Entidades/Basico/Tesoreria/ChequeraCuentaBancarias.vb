﻿Imports Newtonsoft.Json

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="ChequeraCuentaBancarias"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ChequeraCuentaBancarias
        Inherits BaseBasico

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="ChequeraCuentaBancarias"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ChequeraCuentaBancarias"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CuentaBancaria = New CuentaBancarias With {.Codigo = Read(lector, "CUBA_Codigo"), .Nombre = Read(lector, "CuentaBancaria")}
            ChequeInicial = Read(lector, "Cheque_Inicial")
            ChequeFinal = Read(lector, "Cheque_Final")
            ChequeActual = Read(lector, "Cheque_Actual")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            Else

            End If
        End Sub

        <JsonProperty>
        Public Property CuentaBancaria As CuentaBancarias
        <JsonProperty>
        Public Property ChequeInicial As Integer
        <JsonProperty>
        Public Property ChequeFinal As Integer
        <JsonProperty>
        Public Property ChequeActual As Integer
        <JsonProperty>
        Public Property NombreCuentaBancaria As String

    End Class

End Namespace
