﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="DetalleConceptoContables"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleConceptoContables
        Inherits BaseBasico

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="DetalleConceptoContables"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleConceptoContables"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            CodigoConceptoComtable = Read(lector, "ECCO_Codigo")
            Codigo = Read(lector, "Codigo")
            CuentaPUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo"), .CodigoCuenta = Read(lector, "CodigoCuentaPuc"), .Nombre = Read(lector, "CuentaPUC"), .AplicarMedioPago = Read(lector, "Aplicar_Medio_Pago")}
            ValorBase = Read(lector, "Valor_Base")
            PorcentajeTarifa = Read(lector, "Porcentaje_Tarifa")
            GeneraCuenta = Read(lector, "Genera_Cuenta")
            Prefijo = Read(lector, "Prefijo")
            CodigoAnexo = Read(lector, "Codigo_Anexo")
            SufijoCodigoAnexo = Read(lector, "Sufijo_Codigo_Anexo")
            CampoAuxiliar = Read(lector, "Campo_Auxiliar")
            NaturalezaConceptoContable = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NACC_Codigo"), .Nombre = Read(lector, "NaturalezaConcepto")}
            TipoCuentaConcepto = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TCUC_Codigo"), .Nombre = Read(lector, "TipoCuenta")}
            DocumentoCruce = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOCR_Codigo"), .Nombre = Read(lector, "DocuentoCruce")}
            TerceroParametrizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TEPC_Codigo"), .Nombre = Read(lector, "TerceroParametrizable")}
            CentroCostoParametrizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CCPC_Codigo"), .Nombre = Read(lector, "CentroCostoParametrizable")}


        End Sub

        <JsonProperty>
        Public Property CodigoConceptoComtable As Integer
        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas
        <JsonProperty>
        Public Property ValorBase As Integer
        <JsonProperty>
        Public Property PorcentajeTarifa As Integer
        <JsonProperty>
        Public Property GeneraCuenta As String
        <JsonProperty>
        Public Property Prefijo As String
        <JsonProperty>
        Public Property CodigoAnexo As String
        <JsonProperty>
        Public Property SufijoCodigoAnexo As String
        <JsonProperty>
        Public Property CampoAuxiliar As String
        <JsonProperty>
        Public Property NaturalezaConceptoContable As ValorCatalogos
        <JsonProperty>
        Public Property TipoCuentaConcepto As ValorCatalogos
        <JsonProperty>
        Public Property DocumentoOrigen As ValorCatalogos
        <JsonProperty>
        Public Property DocumentoCruce As ValorCatalogos
        <JsonProperty>
        Public Property TerceroParametrizacion As ValorCatalogos
        <JsonProperty>
        Public Property CentroCostoParametrizacion As ValorCatalogos

    End Class

End Namespace
