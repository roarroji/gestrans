﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="DetalleParametrizacionContables"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleParametrizacionContables
        Inherits BaseBasico

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="DetalleParametrizacionContables"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleParametrizacionContables"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            CodigoParametrizacion = Read(lector, "ENPC_Codigo")
            Codigo = Read(lector, "Codigo")
            Orden = Read(lector, "Orden")
            CuentaPUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo"), .CodigoCuenta = Read(lector, "CodigoCuentaPuc"), .Nombre = Read(lector, "CuentaPUC")}
            NaturalezaMovimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NACC_Codigo"), .Nombre = Read(lector, "NaturalezaConcepto")}
            Descripcion = Read(lector, "Descripcion")
            TipoParametrizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIPC_Codigo"), .Nombre = Read(lector, "TipoParametrizacion")}
            ValorParametrizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_VTPC_Codigo")}
            DocumentoCruce = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DCPC_Codigo"), .Nombre = Read(lector, "DocumentoCruce")}
            TerceroParametrizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TEPC_Codigo"), .Nombre = Read(lector, "TerceroParametrizacion")}
            CentroCostoParametrizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CCPC_Codigo"), .Nombre = Read(lector, "CentroCostoParametrizacion")}
            SufijoICAParametrizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_SIPC_Codigo"), .Nombre = Read(lector, "SifijoICAParametrizacion")}
            TipoDueno = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIDU_Codigo"), .Nombre = Read(lector, "TipoDueño")}
            TipoConductor = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICD_Codigo"), .Nombre = Read(lector, "TipoConductor")}
            Prefijo = Read(lector, "Prefijo")
            CodigoAnexo = Read(lector, "Codigo_Anexo")
            SufijoCodigoAnexo = Read(lector, "Sufijo_Codigo_Anexo")
            CampoAuxiliar = Read(lector, "Campo_Auxiliar")
            TipoNaturaleza = Read(lector, "CATA_TINA_Codigo")
            Tercero_Filial = Read(lector, "Tercero_Filial")



        End Sub

        <JsonProperty>
        Public Property CodigoParametrizacion As Integer
        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas
        <JsonProperty>
        Public Property Orden As Integer
        <JsonProperty>
        Public Property Descripcion As String
        <JsonProperty>
        Public Property CodigoAnexo As String
        <JsonProperty>
        Public Property SufijoCodigoAnexo As String
        <JsonProperty>
        Public Property Prefijo As String
        <JsonProperty>
        Public Property CampoAuxiliar As String
        <JsonProperty>
        Public Property TipoNaturaleza As Integer
        <JsonProperty>
        Public Property Tercero_Filial As Integer
        <JsonProperty>
        Public Property NaturalezaMovimiento As ValorCatalogos
        <JsonProperty>
        Public Property TipoCuentaConcepto As ValorCatalogos
        <JsonProperty>
        Public Property TipoParametrizacion As ValorCatalogos
        <JsonProperty>
        Public Property ValorParametrizacion As ValorCatalogos
        <JsonProperty>
        Public Property TerceroParametrizacion As ValorCatalogos
        <JsonProperty>
        Public Property CentroCostoParametrizacion As ValorCatalogos
        <JsonProperty>
        Public Property SufijoICAParametrizacion As ValorCatalogos
        <JsonProperty>
        Public Property TipoDueno As ValorCatalogos
        <JsonProperty>
        Public Property DocumentoCruce As ValorCatalogos
        <JsonProperty>
        Public Property TipoConductor As ValorCatalogos

    End Class

End Namespace
