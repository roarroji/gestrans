﻿Imports Newtonsoft.Json

<JsonObject>
Public Class TercerosModificacionCARGAAPP


    Sub New()
    End Sub

    Public Function Read(Lector As IDataReader, strCampo As String) As Object
        Dim Tabla As DataTable = Lector.GetSchemaTable
        Dim ExisteCampo As Boolean = False
        For Each Campo As DataRow In Tabla.Rows
            If Campo("ColumnName").ToString() = strCampo Then
                ExisteCampo = True
                Exit For
            End If
        Next
        If ExisteCampo Then
            If Not IsDBNull(Lector.Item(strCampo)) Then
                If IsDate(Lector.Item(strCampo)) Then
                    If Lector.Item(strCampo) > Date.MinValue Then
                        Return Lector.Item(strCampo)
                    Else
                        Return Nothing
                    End If
                Else
                    Return Lector.Item(strCampo)
                End If
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Sub New(lector As IDataReader)
        identificacion = Read(lector, "identificacion")
        nombres = Read(lector, "nombres")
        apellidos = Read(lector, "apellidos")
        direccion = Read(lector, "direccion")
        ciudad = Read(lector, "ciudad")
        email = Read(lector, "email")
        celular = Read(lector, "celular")
        codigoNovedad = Read(lector, "codigoNovedad")
        activo = Read(lector, "activo")
    End Sub

    <JsonProperty>
    Public Property direccion As String
    <JsonProperty>
    Public Property ciudad As String
    <JsonProperty>
    Public Property identificacion As String
    <JsonProperty>
    Public Property email As String
    <JsonProperty>
    Public Property nombres As String
    <JsonProperty>
    Public Property apellidos As String

    <JsonProperty>
    Public Property celular As String
    <JsonProperty>
    Public Property codigoNovedad As Long
    <JsonProperty>
    Public Property activo As Boolean
End Class
