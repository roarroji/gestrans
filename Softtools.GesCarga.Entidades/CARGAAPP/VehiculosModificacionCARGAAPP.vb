﻿Imports Newtonsoft.Json

<JsonObject>
Public Class VehiculosModificacionCARGAAPP


    Sub New()
    End Sub

    Public Function Read(Lector As IDataReader, strCampo As String) As Object
        Dim Tabla As DataTable = Lector.GetSchemaTable
        Dim ExisteCampo As Boolean = False
        For Each Campo As DataRow In Tabla.Rows
            If Campo("ColumnName").ToString() = strCampo Then
                ExisteCampo = True
                Exit For
            End If
        Next
        If ExisteCampo Then
            If Not IsDBNull(Lector.Item(strCampo)) Then
                If IsDate(Lector.Item(strCampo)) Then
                    If Lector.Item(strCampo) > Date.MinValue Then
                        Return Lector.Item(strCampo)
                    Else
                        Return Nothing
                    End If
                Else
                    Return Lector.Item(strCampo)
                End If
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Sub New(lector As IDataReader)
        placa = Read(lector, "placa")
        placaTrailer = Read(lector, "placaTrailer")
        codigoTipoCombustible = Read(lector, "codigoTipoCombustible")
        codigoAlternoEmpresaGPS = Read(lector, "codigoAlternoEmpresaGPS")
        usuarioGPS = Read(lector, "usuarioGPS")
        passwordGPS = Read(lector, "passwordGPS")
        tipoVehiculo = Read(lector, "tipoVehiculo")
        modelo = Read(lector, "modelo")
        activo = Read(lector, "activo")
    End Sub

    <JsonProperty>
    Public Property placa As String
    <JsonProperty>
    Public Property placaTrailer As String
    <JsonProperty>
    Public Property codigoTipoCombustible As Long
    <JsonProperty>
    Public Property codigoAlternoEmpresaGPS As String
    <JsonProperty>
    Public Property usuarioGPS As String
    <JsonProperty>
    Public Property passwordGPS As String

    <JsonProperty>
    Public Property tipoVehiculo As Long
    <JsonProperty>
    Public Property modelo As Long
    <JsonProperty>
    Public Property activo As Boolean
End Class
