﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref=" DetalleTarifarioCompras"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleTarifarioCompras
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleTarifarioCompras"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleTarifarioCompras"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            NumeroTarifario = Read(lector, "ETCC_Numero")
            TipoLineaNegocioTransportes = New TipoLineaNegocioTransportes With {
                .LineaNegocioTransporte = New LineaNegocioTransportes With {.Codigo = Read(lector, "LNTC_Codigo"), .Nombre = Read(lector, "NombreLinea")},
                .Codigo = Read(lector, "TLNC_Codigo"), .Nombre = Read(lector, "NombreTipoLinea")
            }
            TipoTarifaTransportes = New TipoTarifaTransportes With {.Codigo = Read(lector, "TTTC_Codigo"), .Nombre = Read(lector, "NombreTipoTarifa"), .TarifaTransporte = New TarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo"), .Nombre = Read(lector, "NombreTarifa"), .TipoLineaNegocioTransporte = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo")}}}
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo")}
            ValorFlete = Read(lector, "Valor_Flete")
            ValorEscolta = Read(lector, "Valor_Escolta")
            'ValorKgAdicional = Read(lector, "Valor_Kilo_Adicional")
            ValorAdicional1 = Read(lector, "Valor_Otros1")
            ValorAdicional2 = Read(lector, "Valor_Otros2")
            FechaInicioVigencia = Read(lector, "Fecha_Vigencia_Inicio")
            FechaFinVigencia = Read(lector, "Fecha_Vigencia_Fin")
            Estado = New Estado With {.Codigo = Read(lector, "Estado")}

            NombreTipoTarifa = Read(lector, "NombreTipoTarifa")
            NombreTarifa = Read(lector, "NombreTarifa")
            PorcentajeAfiliado = Read(lector, "Porcentaje_Afiliado")
            CodigoEstadoTarifa = Read(lector, "EstadoTarifa")

        End Sub

        <JsonProperty>
        Public Property TipoLineaNegocioTransportes As TipoLineaNegocioTransportes
        <JsonProperty>
        Public Property TipoTarifaTransportes As TipoTarifaTransportes
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property NumeroTarifario As Integer
        <JsonProperty>
        Public Property ValorFlete As Double
        <JsonProperty>
        Public Property ValorEscolta As Double
        <JsonProperty>
        Public Property ValorKgAdicional As Double
        <JsonProperty>
        Public Property ValorAdicional1 As Double
        <JsonProperty>
        Public Property ValorAdicional2 As Double
        <JsonProperty>
        Public Property FechaInicioVigencia As Date
        <JsonProperty>
        Public Property FechaFinVigencia As Date
        <JsonProperty>
        Public Property Estado As Estado
        <JsonProperty>
        Public Property NombreTarifa As String
        <JsonProperty>
        Public Property CodigoEstadoTarifa As Integer
        <JsonProperty>
        Public Property NombreTipoTarifa As String
        <JsonProperty>
        Public Property PorcentajeAfiliado As Double
    End Class
End Namespace
