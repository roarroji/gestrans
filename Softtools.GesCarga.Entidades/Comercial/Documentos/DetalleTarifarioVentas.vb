﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref=" DetalleTarifarioVentas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleTarifarioVentas
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleTarifarioVentas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleTarifarioVentas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            NumeroTarifario = Read(lector, "ETCV_Numero")
            TipoLineaNegocioTransportes = New TipoLineaNegocioTransportes With {.LineaNegocioTransporte = New LineaNegocioTransportes With {.Codigo = Read(lector, "LNTC_Codigo")}, .Codigo = Read(lector, "TLNC_Codigo"), .Nombre = Read(lector, "Tipo_Linea_Negocio_Carga")}
            TipoTarifaTransportes = New TipoTarifaTransportes With {
                .Codigo = Read(lector, "TTTC_Codigo"),
                .Nombre = Read(lector, "TTTC_Nombre"),
                .TarifaTransporte = New TarifaTransportes With {
                    .Codigo = Read(lector, "TATC_Codigo"),
                    .Nombre = Read(lector, "Tarifa"),
                    .ValorMaximo = Read(lector, "TATC_Valor_Maximo"),
                    .TipoLineaNegocioTransporte = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo")}
                }
            }
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "Ruta")}
            CiudadOrigen = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Origen"), .Nombre = Read(lector, "CiudadOrigen")}
            CiudadDestino = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino"), .Nombre = Read(lector, "CiudadDestino")}
            FormaPagoVentas = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FPVE_Codigo"), .Nombre = Read(lector, "FormaPago")}
            ValorFlete = Read(lector, "Valor_Flete")
            ValorEscolta = Read(lector, "Valor_Escolta")
            'ValorKgAdicional = Read(lector, "Valor_Kilo_Adicional")
            ValorAdicional1 = Read(lector, "Valor_Otros1")
            ValorAdicional2 = Read(lector, "Valor_Otros2")
            ValorCargue = Read(lector, "Valor_Cargue")
            ValorDescargue = Read(lector, "Valor_Descargue")
            ValorFleteTransportador = Read(lector, "Valor_Flete_Transportador")
            FechaInicioVigencia = Read(lector, "Fecha_Vigencia_Inicio")
            FechaFinVigencia = Read(lector, "Fecha_Vigencia_Fin")
            PorcentajeAfiliado = Read(lector, "Porcentaje_Afiliado")
            PorcentajeSeguro = Read(lector, "Porcentaje_Seguro")
            LineaNegocioTransportes = New LineaNegocioTransportes With {.Codigo = Read(lector, "LNTC_Codigo"), .Nombre = Read(lector, "Linea_Negocio_Transporte_Carga")}
            TarifaTransportes = New TarifaTransportes With {.Nombre = Read(lector, "Tarifa")}
            Reexpedicion = Read(lector, "Reexpedicion")

            Me.Estado = New Estado With {.Codigo = Read(lector, "Estado")}

        End Sub



        <JsonProperty>
        Public Property Reexpedicion As Short

        <JsonProperty>
        Public Property TarifaTransportes As TarifaTransportes

        <JsonProperty>
        Public Property LineaNegocioTransportes As LineaNegocioTransportes
        <JsonProperty>
        Public Property TarifaTransporteCarga As ValorCatalogos
        <JsonProperty>
        Public Property TipoTarifaTransporteCarga As ValorCatalogos

        <JsonProperty>
        Public Property TipoLineaNegocioTransportes As TipoLineaNegocioTransportes
        <JsonProperty>
        Public Property TipoTarifaTransportes As TipoTarifaTransportes

        <JsonProperty>
        Public Property FormaPagoVentas As ValorCatalogos
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property CiudadOrigen As Ciudades
        <JsonProperty>
        Public Property CiudadDestino As Ciudades
        <JsonProperty>
        Public Property NumeroTarifario As Integer
        <JsonProperty>
        Public Property ValorFlete As Double
        <JsonProperty>
        Public Property ValorEscolta As Double
        <JsonProperty>
        Public Property ValorKgAdicional As Double
        <JsonProperty>
        Public Property ValorAdicional1 As Double
        <JsonProperty>
        Public Property ValorAdicional2 As Double
        <JsonProperty>
        Public Property ValorCargue As Double
        <JsonProperty>
        Public Property ValorDescargue As Double
        <JsonProperty>
        Public Property ValorFleteTransportador As Double
        <JsonProperty>
        Public Property FechaInicioVigencia As Date
        <JsonProperty>
        Public Property FechaFinVigencia As Date
        <JsonProperty>
        Public Overloads Property Estado As Estado
        <JsonProperty>
        Public Property PorcentajeAfiliado As Double
        <JsonProperty>
        Public Property PorcentajeSeguro As Double
    End Class
End Namespace
