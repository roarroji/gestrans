﻿Imports Newtonsoft.Json

Namespace Comercial.Documentos
    <JsonObject>
    Public NotInheritable Class Estado

        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Codigo As Integer
    End Class
End Namespace
