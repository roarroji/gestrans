﻿Imports Newtonsoft.Json

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref=" TarifaTransportes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TarifaTransportes
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TarifaTransportes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TarifaTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            'LineaNegocioTransporte = New LineaNegocioTransportes With {.Codigo = Read(lector, "LNTC_Codigo")}
            TipoLineaNegocioTransporte = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo")}
            ValorMaximo = Read(lector, "TATC_Valor_Maximo")
        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String
        '<JsonProperty>
        'Public Property LineaNegocioTransporte As LineaNegocioTransportes
        <JsonProperty>
        Public Property TipoLineaNegocioTransporte As TipoLineaNegocioTransportes
        <JsonProperty>
        Public Property OpcionPlantilla As Integer
        <JsonProperty>
        Public Property OPOtrasEmpresas As Integer
        <JsonProperty>
        Public Property ValorMaximo As Double
    End Class
End Namespace
