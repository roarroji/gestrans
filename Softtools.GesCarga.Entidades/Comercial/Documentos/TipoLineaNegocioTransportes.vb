﻿Imports Newtonsoft.Json

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref=" TipoLineaNegocioTransportes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class TipoLineaNegocioTransportes
        Inherits BaseBasico

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoLineaNegocioTransportes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="TipoLineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            LineaNegocioTransporte = New LineaNegocioTransportes With {.Codigo = Read(lector, "LNTC_Codigo")}
        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property LineaNegocioTransporte As LineaNegocioTransportes
        <JsonProperty>
        Public Property OpcionPlantilla As Integer
    End Class
End Namespace
