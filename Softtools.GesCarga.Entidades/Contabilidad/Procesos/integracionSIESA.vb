﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Contabilidad.Documentos

    ''' <summary>
    ''' Clase <see cref=" integracionSIESA"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class integracionSIESA
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="integracionSIESA"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="integracionSIESA"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")

            Obtener = Read(lector, "Obtener")

            If Obtener = 0 Then 'Consultar

                Fecha = Read(lector, "Fecha")
                DocumentoOrigen = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOOR_Codigo"), .Nombre = Read(lector, "NombreDocumentoOrigen")}
                NumeroDocumento = Read(lector, "Numero_Documento")
                Observaciones = Read(lector, "Observaciones")
                Ano = Read(lector, "Año")
                Periodo = Read(lector, "Periodo")
                EstadoInterfazContable = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESIC_Codigo"), .Nombre = Read(lector, "NombreEstadoInterfaz")}
                TotalRegistros = Read(lector, "TotalRegistros")
                Anulado = Read(lector, "Anulado")
                Estado = Read(lector, "estado")

            Else 'Obtener

                Fecha = Read(lector, "Fecha")
                DocumentoOrigen = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOOR_Codigo")}
                NumeroDocumento = Read(lector, "Numero_Documento")
                EstadoInterfazContable = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESIC_Codigo")}

            End If
            InterfazContable = Read(lector, "Interfase_Contable")
            FechaInterfazContable = Read(lector, "Fecha_Interfase_Contable")
            IntentosInterfazContable = Read(lector, "Intentos_Interfase_Contable")
            MensajeError = Read(lector, "Mensaje_Error")
            NombreTercero = Read(lector, "NombreTercero")

        End Sub


        <JsonProperty>
        Public Property TipoDocumentoOrigen As TipoDocumentos
        <JsonProperty>
        Public Property CodigoDocumento As Short
        <JsonProperty>
        Public Property FechaDocumento As DateTime
        <JsonProperty>
        Public Property TerceroDocumento As Tercero
        <JsonProperty>
        Public Property NombreTercero As String
        <JsonProperty>
        Public Property OficinaDocumento As Oficinas
        <JsonProperty>
        Public Property DocumentoOrigen As ValorCatalogos
        <JsonProperty>
        Public Property Ano As Short
        <JsonProperty>
        Public Property Periodo As Short
        <JsonProperty>
        Public Property ValorComprobante As Double
        <JsonProperty>
        Public Property Fuente As String
        <JsonProperty>
        Public Property FuenteAnulacion As String
        <JsonProperty>
        Public Property MensajeError As String
        <JsonProperty>
        Public Property EstadoInterfazContable As ValorCatalogos
        <JsonProperty>
        Public Property DetalleCuentas As IEnumerable(Of DetalleComprobantesContables)
        <JsonProperty>
        Public Property ListaComprobantes As IEnumerable(Of EncabezadoComprobantesContables)
        <JsonProperty>
        Public Property ListaDocumentos As IEnumerable(Of EncabezadoComprobantesContables)
        <JsonProperty>
        Public Property Masivo As Short
        <JsonProperty>
        Public Property InterfazContable As Integer
        <JsonProperty>
        Public Property FechaInterfazContable As DateTime
        <JsonProperty>
        Public Property IntentosInterfazContable As Integer
    End Class
End Namespace
