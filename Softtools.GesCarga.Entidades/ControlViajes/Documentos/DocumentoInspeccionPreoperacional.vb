﻿Imports Newtonsoft.Json

Namespace ControlViajes

    ''' <summary>
    ''' Clase <see cref="DocumentoInspeccionPreoperacional"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DocumentoInspeccionPreoperacional
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="DocumentoInspeccionPreoperacional"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DocumentoInspeccionPreoperacional"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = lector.Item("EMPR_Codigo")
            Codigo = lector.Item("Codigo")
            NumeroInspeccion = lector.Item("ENIS_Numero")
            NombreDocumento = lector.Item("Nombre_Documento")
            Documento = lector.Item("Documento")
            ExtencionDocumento = lector.Item("Extencion_Documento")

        End Sub

        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property NumeroInspeccion As Integer
        <JsonProperty>
        Public Property Documento As Byte()
        <JsonProperty>
        Public Property NombreDocumento As String
        <JsonProperty>
        Public Property ExtencionDocumento As String
        <JsonProperty>
        Public Property EliminarTemporal As Short

    End Class

End Namespace
