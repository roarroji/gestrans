﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Utilitarios
Imports Softtools.GesCarga.Entidades.Despachos.Masivo
Imports Softtools.GesCarga.Entidades.Despachos.OrdenCargue

Namespace ControlViajes

    ''' <summary>
    ''' Clase <see cref="InspeccionPreoperacional"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class InspeccionPreoperacional
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="InspeccionPreoperacional"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="InspeccionPreoperacional"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            FormatoInspeccion = New FormularioInspecciones With {.Codigo = Read(lector, "EFIN_Codigo"), .Nombre = Read(lector, "FormularioInspeccion"), .TipoInspeccion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIIN_Codigo")}}
            FechaInspeccion = Read(lector, "Fecha_Inspeccion")
            TiempoVigencia = Read(lector, "Tiempo_Vigencia")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "Vehiculo")}
            Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "Semirremolque")}
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .Nombre = Read(lector, "NombreCliente")}
            Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .Nombre = Read(lector, "NombreConductor")}
            Transportador = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Transportador"), .Nombre = Read(lector, "NombreTransportador")}
            FechaInicioHora = Read(lector, "Fecha_Hora_Inicio_Inspeccion")
            FechaFinHora = Read(lector, "Fecha_Hora_Fin_Inspeccion")
            SitioCargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Cargue"), .Nombre = Read(lector, "SitioCargue")}
            SitioDescargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Descargue"), .Nombre = Read(lector, "SitioDescargue")}
            Observaciones = Read(lector, "Observaciones")
            Responsable = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Responsable"), .Nombre = Read(lector, "NombreResponsable")}
            Autoriza = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Autoriza"), .Nombre = Read(lector, "NombreAutoriza")}
            FechaAutoriza = Read(lector, "Fecha_Autoriza")
            Firma = Read(lector, "Firma")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            Obtener = Read(lector, "Obtener")
            TotalRegistros = Read(lector, "TotalRegistros")
            OrdenCargue = New OrdenCargue With {.Numero = Read(lector, "ENOC_Numero"), .NumeroDocumento = Read(lector, "NumeroDocumentoOrdenCargue"), .CiudadCargue = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Cargue"), .Nombre = Read(lector, "Ciudad_Cargue")}}
            Planilla = New PlanillaDespachos With {.Numero = Read(lector, "ENPD_Numero"), .NumeroDocumento = Read(lector, "NumeroDocumentoPlanilla"), .Manifiesto = New Despachos.Manifiesto With {.Numero = Read(lector, "ENMC_Numero"), .NumeroDocumento = Read(lector, "NumeroDocumentoManifiesto")}, .CiudadOrigen = Read(lector, "CIUD_Codigo_Origen"), .CiudadDestino = Read(lector, "CIUD_Codigo_Destino")}
            Cumple = Read(lector, "Cumple")
        End Sub

        <JsonProperty>
        Public Property Cumple As Short
        <JsonProperty>
        Public Property OrdenCargue As OrdenCargue

        <JsonProperty>
        Public Property Planilla As PlanillaDespachos
        <JsonProperty>
        Public Property NumeroViaje As Integer
        <JsonProperty>
        Public Property NumeroPreenturne As Integer
        <JsonProperty>
        Public Property FormatoInspeccion As FormularioInspecciones
        <JsonProperty>
        Public Property FechaInspeccion As DateTime
        <JsonProperty>
        Public Property FechaAutoriza As DateTime
        <JsonProperty>
        Public Property TiempoVigencia As Integer
        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Semirremolque As Semirremolques
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property Conductor As Terceros
        <JsonProperty>
        Public Property Transportador As Terceros
        <JsonProperty>
        Public Property Autoriza As Terceros
        <JsonProperty>
        Public Property FechaInicioHora As DateTime
        <JsonProperty>
        Public Property FechaFinHora As DateTime
        <JsonProperty>
        Public Property SitioCargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property SitioDescargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property Responsable As Terceros
        <JsonProperty>
        Public Property Firma As Byte()
        <JsonProperty>
        Public Property Detalle As IEnumerable(Of DetalleInspeccionPreoperacional)
        <JsonProperty>
        Public Property ConsularDetalle As Short
        '<JsonProperty>
        'Public Property DetalleFotos As IEnumerable(Of DocumentoInspeccionPreoperacional)
        '<JsonProperty>
        'Public Property DetalleFotos1 As IEnumerable(Of DocumentoInspeccionPreoperacional)
        '<JsonProperty>
        'Public Property DetalleFotos2 As IEnumerable(Of DocumentoInspeccionPreoperacional)
        <JsonProperty>
        Public Property EventoCorreo As EventoCorreos
        <JsonProperty>
        Public Property NumeroCorreo As Short

    End Class

End Namespace
