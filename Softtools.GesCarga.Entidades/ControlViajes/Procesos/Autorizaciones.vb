﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Despachos.Masivo
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Entidades.Despachos.OrdenCargue
Imports Softtools.GesCarga.Entidades.Despachos


Namespace ControlViajes

    ''' <summary>
    ''' Clase <see cref="Autorizaciones"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Autorizaciones
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="Autorizaciones"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Autorizaciones"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            UsuarioSolicita = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Solicita"), .Nombre = Read(lector, "UsuarioSolicita")}
            FechaSolicita = Read(lector, "Fecha_Solicita")
            TipoAutorizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TSAO_Codigo"), .Nombre = Read(lector, "TipoSolicitud")}
            UsuarioGestion = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Autoriza"), .Nombre = Read(lector, "UsuarioAutoriza")}
            FechaAutoriza = Read(lector, "Fecha_Autoriza")
            Observaciones = Read(lector, "Observaciones_Autoriza")
            EstadoAutorizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESAO_Codigo"), .Nombre = Read(lector, "EstadoSolicitud")}
            TotalRegistros = Read(lector, "TotalRegistros")
            Vehiculo = New Vehiculos With {
                .Placa = Read(lector, "PlacaVehiculo"), .Codigo = Read(lector, "VEHI_Codigo"),
                .Conductor = New Tercero With {.NombreCompleto = Read(lector, "NombreConductor"), .Celulares = Read(lector, "TelefonoConductor")}
            }
            Valor = Read(lector, "Valor")
            OrdenCargue = New OrdenCargue With {.NumeroDocumento = Read(lector, "NumeroDocumentoOrdenCargue")}
            PlanillaDespacho = New PlanillaDespachos With {.NumeroDocumento = Read(lector, "Planilla"), .Manifiesto = New Manifiesto With {.NumeroDocumento = Read(lector, "NumeroDocumentoManifiesto")}}
            EstudioSeguridad = New EstudioSeguridad With {.NumeroDocumento = Read(lector, "EstudioSeguridad")}
            NumeroAutorizacion = Read(lector, "Numero_Autorización")
            ObservacionSolicita = Read(lector, "ObservacionSolicita")
            ValorDeclaradoAutorizacion = Read(lector, "Valor_Declarado_Autorizacion")
            NumeroDocumento = Read(lector, "Numero_Documento_Origen")
            TipoNotificacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_PRNO_Codigo")}
            Consultar = Read(lector, "Consultar")
            Autorizar = Read(lector, "Autorizar")
            ProgramacionOrdenServicio = Read(lector, "Numero_Programacion")
            Cliente = Read(lector, "Cliente")
            Producto = Read(lector, "Producto")
        End Sub
        <JsonProperty>
        Public Property OrdenCargue As OrdenCargue
        <JsonProperty>
        Public Property Cliente As String
        <JsonProperty>
        Public Property Producto As String
        <JsonProperty>
        Public Property ProgramacionOrdenServicio As Long
        <JsonProperty>
        Public Property Usuario As Usuarios
        <JsonProperty>
        Public Property Autorizar As Short
        <JsonProperty>
        Public Property Consultar As Short
        <JsonProperty>
        Public Property TipoNotificacion As ValorCatalogos
        <JsonProperty>
        Public Property DetalleConceptos As IEnumerable(Of ConceptoAutorizacionValorDeclarado)

        <JsonProperty>
        Public Property Documento As Documentos
        <JsonProperty>
        Public Property ValorDeclaradoAutorizacion As Double
        <JsonProperty>
        Public Property EstudioSeguridad As EstudioSeguridad
        <JsonProperty>
        Public Property PlanillaDespacho As PlanillaDespachos
        <JsonProperty>
        Public Property NumeroAutorizacion As Long
        <JsonProperty>
        Public Property TipoAutorizacion As ValorCatalogos
        <JsonProperty>
        Public Property EstadoAutorizacion As ValorCatalogos
        <JsonProperty>
        Public Property UsuarioSolicita As Usuarios
        <JsonProperty>
        Public Property FechaSolicita As DateTime
        <JsonProperty>
        Public Property UsuarioGestion As Usuarios
        <JsonProperty>
        Public Property FechaGestiona As DateTime
        <JsonProperty>
        Public Property CuentaPorPagar As EncabezadoDocumentoCuentas
        <JsonProperty>
        Public Property NombreUsuarioSolicita As String
        <JsonProperty>
        Public Property NombreUsuarioAutoriza As String
        <JsonProperty>
        Public Property FechaInicioAutoriza As DateTime
        <JsonProperty>
        Public Property FechaFinAutoriza As DateTime
        <JsonProperty>
        Public Property FechaAutoriza As DateTime
        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Valor As String
        <JsonProperty>
        Public Property ObservacionSolicita As String
        <JsonProperty>
        Public Property Notificacion As String
        <JsonProperty>
        Public Property ID_Enturne As Integer
        <JsonProperty>
        Public Property NumeroComprobante As Integer
        <JsonProperty>
        Public Property ValorFlete As Double
        <JsonProperty>
        Public Property ValorFleteReal As Double
    End Class

End Namespace
