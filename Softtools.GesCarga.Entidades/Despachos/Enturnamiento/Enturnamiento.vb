﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General


Namespace Despachos
    <JsonObject>
    Public NotInheritable Class Enturnamiento
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Enturnamiento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            Fecha = Read(lector, "Fecha")
            FechaDisponible = Read(lector, "Fecha_Hora_Disponible")
            Turno = Read(lector, "Turno")
            ID = Read(lector, "ID")
            Vehiculo = New Vehiculos With {
                .Codigo = Read(lector, "VEHI_Codigo"),
                .Placa = Read(lector, "Placa"),
                .FechaUltimoCargue = Read(lector, "Fecha_Ultimo_Cargue"),
                .Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "SEMI_Placa")}
            }
            Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .Nombre = Read(lector, "NombreConductor")}
            TipoVehiculo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIVE_Codigo"), .Nombre = Read(lector, "TipoVehiculo")}
            Modelo = Read(lector, "Modelo")
            PesoBruto = Read(lector, "Peso_Bruto")
            Capacidad = Read(lector, "Capacidad_Kilos")
            CapacidadM3 = Read(lector, "Capacidad_Volumen")
            Tenedor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Tenedor"), .Nombre = Read(lector, "NombreTenedor")}

            TipoCarroceria = New ValorCatalogos With {.Nombre = Read(lector, "TipoCarroceria")}
            EstadoTurno = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESED_Codigo")}
            CiudadEnturna = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Enturna"), .Nombre = Read(lector, "CiudadEnturne")}
            OficinaEnturna = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Enturna"), .Nombre = Read(lector, "OficinaEnturne")}
            'OficinaDestino = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Destino1"), .Nombre = Read(lector, "OficinaDestino1")}
            'OficinaDestino2 = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Destino2"), .Nombre = Read(lector, "OficinaDestino2")}
            Latitud = Read(lector, "Latitud")
            Longitud = Read(lector, "Longitud")
            CiudadDestino1 = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino1"), .Nombre = Read(lector, "CiudadDestino1")}
            CiudadDestino2 = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino2"), .Nombre = Read(lector, "CiudadDestino2")}
            RegionPaisDestino = New RegionesPaises With {.Codigo = Read(lector, "REPA_Codigo_Destino1")}
            RegionPaisDestino2 = New RegionesPaises With {.Codigo = Read(lector, "REPA_Codigo_Destino2")}

            FechaTurno = Read(lector, "Fecha_Turno")
        End Sub

        <JsonProperty>
        Public Property ID As Integer
        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property TipoVehiculo As ValorCatalogos
        <JsonProperty>
        Public Property Modelo As String
        <JsonProperty>
        Public Property PesoBruto As Double
        <JsonProperty>
        Public Property Capacidad As Double
        <JsonProperty>
        Public Property CapacidadM3 As Double

        <JsonProperty>
        Public Property TipoCarroceria As ValorCatalogos
        <JsonProperty>
        Public Property Conductor As Terceros
        <JsonProperty>
        Public Property Tenedor As Terceros
        <JsonProperty>
        Public Property Ubicacion As String
        <JsonProperty>
        Public Property Longitud As Double
        <JsonProperty>
        Public Property Latitud As Double
        <JsonProperty>
        Public Property CiudadEnturna As Ciudades
        <JsonProperty>
        Public Property OficinaEnturna As Oficinas
        <JsonProperty>
        Public Property RegionPaisDestino As RegionesPaises
        <JsonProperty>
        Public Property OficinaDestino As Oficinas
        <JsonProperty>
        Public Property RegionPaisDestino2 As RegionesPaises
        <JsonProperty>
        Public Property OficinaDestino2 As Oficinas
        <JsonProperty>
        Public Property FechaDisponible As DateTime
        <JsonProperty>
        Public Property Turno As Integer
        <JsonProperty>
        Public Property FechaTurno As DateTime
        <JsonProperty>
        Public Property Novedad As ValorCatalogos
        <JsonProperty>
        Public Property EstadoTurno As ValorCatalogos
        <JsonProperty>
        Public Property AsignarTurno As Short
        <JsonProperty>
        Public Property Desenturnar As Short
        <JsonProperty>
        Public Property CerrarTurno As Short
        <JsonProperty>
        Public Property CambioTurno As Short
        <JsonProperty>
        Public Property TurnoCambio As Integer
        <JsonProperty>
        Public Property CiudadDestino1 As Ciudades
        <JsonProperty>
        Public Property CiudadDestino2 As Ciudades
        <JsonProperty>
        Public Property Autorizacion As Short

    End Class
End Namespace