﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades.Tesoreria

Namespace Despachos
    <JsonObject>
    Public NotInheritable Class Liquidacion
        Inherits Base
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Liquidacion"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            If Read(lector, "Obtener").Equals(0) Then
                Me.NumeroDocumento = Read(lector, "Numero_Documento")
                Me.NumeroFinal = Read(lector, "Numero_Documento")
                Me.FechaInicial = Read(lector, "Fecha")
                Me.FechaFinal = Read(lector, "Fecha")
                Me.NumeroPlanilla = Read(lector, "NumeroPlanilla")
                Me.TotalRegistros = Read(lector, "TotalRegistros")
                Me.Anulado = Read(lector, "Anulado")
                Me.ValorPagar = Read(lector, "Valor_Pagar")
            Else
                Me.NumeroCumplido = Read(lector, "NumeroCumplido")
                Me.FechaEntrega = Read(lector, "Fecha_Entrega")
                Me.Observaciones = Read(lector, "Observaciones")
                Me.ValorFleteTranportador = Read(lector, "Valor_Flete_Transportador")
                Me.ValorConceptosLiquidacion = Read(lector, "Valor_Conceptos_Liquidacion")
                Me.ValorBaseImpuestos = Read(lector, "Valor_Base_Impuestos")
                Me.ValorImpuestos = Read(lector, "Valor_Impuestos")
                Me.ValorPagar = Read(lector, "Valor_Pagar")
                Me.PlacaSemirremolque = Read(lector, "PlacaSemirremolque")
            End If
            Me.NombreRuta = Read(lector, "NombreRuta")
            Me.CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Me.Fecha = Read(lector, "Fecha")
            Me.Tenedor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Tenedor"), .NombreCompleto = Read(lector, "NombreTenedor")}
            Me.Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .NombreCompleto = Read(lector, "NombreConductor")}
            Me.Numero = Read(lector, "Numero")
            Me.NumeroDocumento = Read(lector, "Numero_Documento")
            Me.Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "NombreOficina")}
            Me.Estado = Read(lector, "Estado")
            Me.NumeroPlanilla = Read(lector, "NumeroPlanilla")
            Me.NumeroManifiesto = Read(lector, "NumeroManifiesto")
            Me.PlacaVehiculo = Read(lector, "PlacaVehiculo")
            Me.TipoDocumento = New TipoDocumentos With {.Codigo = Read(lector, "TIDO_Codigo")}
            UsuarioCumplido = Read(lector, "UsuarioCumplido")
            CiudadOrigen = Read(lector, "CiudadOrigen")
            CiudadDestino = Read(lector, "CiudadDestino")
            FechaCumplido = Read(lector, "FechaCumplido")
            TipoDespacho = Read(lector, "TipoDespacho")
            FleteUnitario = Read(lector, "FleteUnitario")
            PesoCargue = Read(lector, "PesoCargue")
            NumeroTarifario = Read(lector, "NumeroTarifario")
            PesoCumplido = Read(lector, "PesoCumplido")
            PesoFaltante = Read(lector, "Peso_Faltante")
            Cantidad = Read(lector, "Cantidad")
            ValorCvt = Read(lector, "Valor_CVT")
        End Sub

#Region "Propiedades"

        <JsonProperty>
        Public Property Numero As Long

        <JsonProperty>
        Public Property TipoDocumento As TipoDocumentos

        <JsonProperty>
        Public Property NumeroDocumento As Long

        <JsonProperty>
        Public Property Fecha As Date

        <JsonProperty>
        Public Property FechaEntrega As Date

        <JsonProperty>
        Public Property NumeroPlanilla As Long

        <JsonProperty>
        Public Property NumeroManifiesto As Long

        <JsonProperty>
        Public Property NumeroCumplido As Long

        <JsonProperty>
        Public Property Observaciones As String

        <JsonProperty>
        Public Property ValorFleteTranportador As Double

        <JsonProperty>
        Public Property ValorConceptosLiquidacion As Double

        <JsonProperty>
        Public Property ValorBaseImpuestos As Double

        <JsonProperty>
        Public Property ValorImpuestos As Double

        <JsonProperty>
        Public Property ValorPagar As Double

        <JsonProperty>
        Public Property Aprobado As Integer

        <JsonProperty>
        Public Property Anulado As Integer

        <JsonProperty>
        Public Property Estado As Integer

        <JsonProperty>
        Public Property FechaCrea As BaseBasico

        <JsonProperty>
        Public Property UsuarioCrea As Usuarios

        <JsonProperty>
        Public Property CodigoUsuario As Integer

        <JsonProperty>
        Public Property CodigoRuta As Integer

        <JsonProperty>
        Public Property FechaModifica As BaseBasico

        <JsonProperty>
        Public Property UsuarioModifica As Usuarios

        <JsonProperty>
        Public Property FechaAnula As Date

        <JsonProperty>
        Public Property UsuarioAnula As Usuarios

        <JsonProperty>
        Public Property CausaAnula As String

        <JsonProperty>
        Public Property CausaRechazo As String

        <JsonProperty>
        Public Property NumeroInicial As Long

        <JsonProperty>
        Public Property NumeroFinal As Long

        <JsonProperty>
        Public Property FechaInicial As Date

        <JsonProperty>
        Public Property FechaFinal As Date

        <JsonProperty>
        Public Property PlacaVehiculo As String

        <JsonProperty>
        Public Property PlacaSemirremolque As String

        <JsonProperty>
        Public Property NombreRuta As String

        <JsonProperty>
        Public Property Tenedor As Terceros

        <JsonProperty>
        Public Property Conductor As Terceros

        <JsonProperty>
        Public Property Numeracion As String

        <JsonProperty>
        Public Property DetalleLiquidacion As DetalleLiquidacion

        <JsonProperty>
        Public Property ListaDetalleLiquidacion As IEnumerable(Of DetalleLiquidacion)

        <JsonProperty>
        Public Property Oficina As Oficinas

        <JsonProperty>
        Public Property ListaImpuestosConceptosLiquidacion As IEnumerable(Of ImpuestoConceptoLiquidacionPlanillaDespacho)

        <JsonProperty>
        Public Property CuentaPorPagar As EncabezadoDocumentoCuentas

        <JsonProperty>
        Public Property PesoCargue As Double

        <JsonProperty>
        Public Property UsuarioCumplido As String
        <JsonProperty>
        Public Property CiudadOrigen As String
        <JsonProperty>
        Public Property CiudadDestino As String
        <JsonProperty>
        Public Property FechaCumplido As Date

        <JsonProperty>
        Public Property FleteUnitario As Double
        <JsonProperty>
        Public Property TipoDespacho As String
        <JsonProperty>
        Public Property PesoCumplido As Double
        <JsonProperty>
        Public Property NumeroTarifario As Double
        <JsonProperty>
        Public Property PesoFaltante As Double
        <JsonProperty>
        Public Property Cantidad As Double
        <JsonProperty>
        Public Property ValorCvt As Double
#End Region

    End Class
End Namespace
