﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Despachos.Planilla
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades.Tesoreria

Namespace Despachos
    <JsonObject>
    Public NotInheritable Class Manifiesto
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Manifiesto"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            If Read(lector, "Obtener").Equals(0) Then
                NumeroDocumento = Read(lector, "Numero")
                FechaInicial = Read(lector, "Fecha")
                FechaFinal = Read(lector, "Fecha")
                TotalRegistros = Read(lector, "TotalRegistros")
                NombreEmpresa = Read(lector, "Empresa")
                NombreCiudadOrigen = Read(lector, "CiudadOrigen")
                NombreCiudadDestino = Read(lector, "CiudadDestino")
                NombreProductotrans = Read(lector, "NombreProducto")
                CodigoDepartOrigen = Read(lector, "CIOR_DEPA_Codigo")
                CodigoDepartDestino = Read(lector, "CIDE_DEPA_Codigo")
            End If

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            Fecha = Read(lector, "Fecha")
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "PlacaVehiculo")}
            Propietario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Propietario"), .NombreCompleto = Read(lector, "NombrePropietario"), .NumeroIdentificacion = Read(lector, "IdentificacionPropietario")}
            Tenedor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Tenedor"), .NombreCompleto = Read(lector, "NombreTenedor"), .NumeroIdentificacion = Read(lector, "IdentificacionTenedor")}
            Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .NombreCompleto = Read(lector, "NombreConductor"), .NumeroIdentificacion = Read(lector, "IdentificacionConductor"), .Telefonos = Read(lector, "TelefonoConductor")}
            Segundo_Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Segundo_Conductor"), .NombreCompleto = Read(lector, "NombreSegundoConductor"), .NumeroIdentificacion = Read(lector, "IdentificacionSegundoConductor")}
            Afiliador = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Afiliador"), .NombreCompleto = Read(lector, "NombreAfiliador"), .NumeroIdentificacion = Read(lector, "IdentificacionAfiliador")}
            Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "PlacaSemirremolque")}
            Tipo_Vehiculo = New ValorCatalogos With {.Codigo = Read(lector, "TIVE_Codigo"), .Nombre = Read(lector, "TipoVehiculo")}
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "NombreRuta")}
            Observaciones = Read(lector, "Observaciones")
            Fecha_Cumplimiento = Read(lector, "Fecha_Cumplimiento")
            Cantidad_Total = Read(lector, "Cantidad_Total")
            Peso_Total = Read(lector, "Peso_Total")
            Valor_Flete = Read(lector, "Valor_Flete")
            Valor_Retencion_Fuente = Read(lector, "Valor_Retencion_Fuente")
            Valor_ICA = Read(lector, "Valor_ICA")
            Valor_Otros_Descuentos = Read(lector, "Valor_Otros_Descuentos")
            Valor_Flete_Neto = Read(lector, "Valor_Flete_Neto")
            Valor_Anticipo = Read(lector, "Valor_Anticipo")
            Valor_Pagar = Read(lector, "Valor_Pagar")
            Aseguradora = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Aseguradora"), .NombreCompleto = Read(lector, "NombreAseguradora"), .NumeroIdentificacion = Read(lector, "IdentificacionAseguradora")}
            Numero_Poliza = Read(lector, "Numero_Poliza")
            Fecha_Vigencia_Poliza = Read(lector, "Fecha_Vigencia_Poliza")
            Numero_Liquidacion = Read(lector, "ELPD_Numero")
            Numero_Cumplido = Read(lector, "ECPD_Numero")
            Finalizo_Viaje = Read(lector, "Finalizo_Viaje")
            Anulado = Read(lector, "Anulado")
            Siniestro = Read(lector, "Siniestro")
            Estado = New BaseBasico With {.Estado = Read(lector, "Estado")}
            NombreEstado = Read(lector, "NombreEstado")
            Numeracion = Read(lector, "Numeracion")
            Tipo_Documento = New TipoDocumentos With {.Codigo = Read(lector, "TIDO_Codigo"), .Nombre = Read(lector, "TipoDocumento")}
            Usuario_Crea = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Crea")}
            Fecha_Crea = New BaseBasico With {.FechaCrea = Read(lector, "Fecha_Crea")}
            Usuario_Modifica = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Modifica")}
            Fecha_Modifica = New BaseBasico With {.FechaCrea = Read(lector, "Fecha_Modifica")}
            Fecha_Anula = Read(lector, "Fecha_Anula")
            Usuario_Anula = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Anula")}
            Causa_Anula = Read(lector, "Causa_Anula")
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "Oficina")}
            Tipo_Manifiesto = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIMA_Codigo"), .Nombre = Read(lector, "TipoManifiesto")}
            Numero_Manifiesto_Electronico = Read(lector, "Numero_Manifiesto_Electronico")
            Fecha_Reporte_Manifiesto_Electronico = Read(lector, "Fecha_Reporte_Manifiesto_Electronico")
            Mensaje_Manifiesto_Electronico = Read(lector, "Mensaje_Manifiesto_Electronico")
            Motivo_Anulacion_Manifiesto = New ValorCatalogos With {.Codigo = Read(lector, "CATA_MAMC_Codigo"), .Nombre = Read(lector, "MotivoAnulacion")}
            Planilla = New Planillas With {.NumeroDocumento = Read(lector, "NumeroDocumentoPlanilla"), .Numero = Read(lector, "NumeroPlanilla")}
            AceptacionElectronica = Read(lector, "Aceptacion_Electronica")
            CantidadViajesDia = Read(lector, "Cantidad_viajes_dia")
            'Producto = New ProductoTransportados With {.Nombre = Read(lector, "NombreProducto")}
        End Sub

#Region "Propiedades"

        <JsonProperty>
        Public Property CantidadViajesDia As Long

        <JsonProperty>
        Public Property Numero As Long

        <JsonProperty>
        Public Property NumeroDocumento As Long

        <JsonProperty>
        Public Property FechaInicial As Date

        <JsonProperty>
        Public Property FechaFinal As Date

        <JsonProperty>
        Public Property NumeroPlanilla As Long

        <JsonProperty>
        Public Property NumeroSolicitudServicio As Long

        <JsonProperty>
        Public Property IdDetalleSolicitudServicio As Integer

        <JsonProperty>
        Public Property Fecha As Date

        <JsonProperty>
        Public Property Vehiculo As Vehiculos

        <JsonProperty>
        Public Property Propietario As Terceros

        <JsonProperty>
        Public Property Tenedor As Terceros

        <JsonProperty>
        Public Property Conductor As Terceros

        <JsonProperty>
        Public Property Segundo_Conductor As Terceros

        <JsonProperty>
        Public Property Afiliador As Terceros

        <JsonProperty>
        Public Property Semirremolque As Semirremolques

        <JsonProperty>
        Public Property Tipo_Vehiculo As ValorCatalogos

        <JsonProperty>
        Public Property Ruta As Rutas

        <JsonProperty>
        Public Property Observaciones As String

        <JsonProperty>
        Public Property Fecha_Cumplimiento As Date

        <JsonProperty>
        Public Property Cantidad_Total As Integer

        <JsonProperty>
        Public Property Peso_Total As Integer

        <JsonProperty>
        Public Property Valor_Flete As Double

        <JsonProperty>
        Public Property Valor_Retencion_Fuente As Double

        <JsonProperty>
        Public Property Valor_ICA As Double

        <JsonProperty>
        Public Property Valor_Otros_Descuentos As Double

        <JsonProperty>
        Public Property Valor_Flete_Neto As Double

        <JsonProperty>
        Public Property Valor_Anticipo As Double

        <JsonProperty>
        Public Property Valor_Pagar As Double

        <JsonProperty>
        Public Property Aseguradora As Terceros

        <JsonProperty>
        Public Property Numero_Poliza As String

        <JsonProperty>
        Public Property Fecha_Vigencia_Poliza As Date

        <JsonProperty>
        Public Property Numero_Liquidacion As Integer

        <JsonProperty>
        Public Property Numero_Cumplido As Integer

        <JsonProperty>
        Public Property Finalizo_Viaje As Integer

        <JsonProperty>
        Public Property Anulado As Integer

        <JsonProperty>
        Public Property Siniestro As Integer

        <JsonProperty>
        Public Property Estado As BaseBasico

        <JsonProperty>
        Public Property NombreEstado As String

        <JsonProperty>
        Public Property Numeracion As String

        <JsonProperty>
        Public Property Tipo_Documento As TipoDocumentos

        <JsonProperty>
        Public Property Usuario_Crea As Usuarios

        <JsonProperty>
        Public Property Fecha_Crea As BaseBasico

        <JsonProperty>
        Public Property Usuario_Modifica As Usuarios

        <JsonProperty>
        Public Property Fecha_Modifica As BaseBasico

        <JsonProperty>
        Public Property Fecha_Anula As Date

        <JsonProperty>
        Public Property Usuario_Anula As Usuarios

        <JsonProperty>
        Public Property Causa_Anula As String

        <JsonProperty>
        Public Property Oficina As Oficinas

        <JsonProperty>
        Public Property Tipo_Manifiesto As ValorCatalogos

        <JsonProperty>
        Public Property Numero_Manifiesto_Electronico As Long

        <JsonProperty>
        Public Property Fecha_Reporte_Manifiesto_Electronico As Date

        <JsonProperty>
        Public Property Mensaje_Manifiesto_Electronico As String

        <JsonProperty>
        Public Property Motivo_Anulacion_Manifiesto As ValorCatalogos

        <JsonProperty>
        Public Property DetalleManifiesto As DetalleManifiesto

        <JsonProperty>
        Public Property ListaDetalleManifiesto As IEnumerable(Of DetalleManifiesto)

        <JsonProperty>
        Public Property CuentaPorPagar As EncabezadoDocumentoCuentas

        <JsonProperty>
        Public Property Producto As ProductoTransportados

        <JsonProperty>
        Public Property NommbreEmmpresa As String

        <JsonProperty>
        Public Property NombreCiudadOrigen As String

        <JsonProperty>
        Public Property CodigoDepartOrigen As Integer

        <JsonProperty>
        Public Property CodigoDepartDestino As Integer

        <JsonProperty>
        Public Property NombreCiudadDestino As String

        <JsonProperty>
        Public Property NombreProductotrans As String

        <JsonProperty>
        Public Property QR_Manifiesto As Byte()

        <JsonProperty>
        Public Property NumeroDocumentoPlanilla As Integer

        <JsonProperty>
        Public Property Planilla As Planillas

        <JsonProperty>
        Public Property DocumentosRelacionados As IEnumerable(Of EncabezadoDocumentos)

        <JsonProperty>
        Public Property UsuarioConsulta As Usuarios
        <JsonProperty>
        Public Property TipoCumplidoManifiesto As ValorCatalogos
        <JsonProperty>
        Public Property MotivoSuspensionManifiesto As ValorCatalogos
        <JsonProperty>
        Public Property ConsecuenciaSuspencionManifiesto As ValorCatalogos
        <JsonProperty>
        Public Property ManifiestoTransbordo As Integer
        <JsonProperty>
        Public Property VehiculoTransbordo As Vehiculos
        <JsonProperty>
        Public Property TenedorTransbordo As Tercero
        <JsonProperty>
        Public Property PropietarioTransbordo As Tercero
        <JsonProperty>
        Public Property ConductorTransbordo As Tercero
        <JsonProperty>
        Public Property AceptacionElectronica As Integer
#End Region

    End Class
End Namespace
