﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios


Namespace Despachos.OrdenCargue
    <JsonObject>
    Public Class OrdenCargue
        Inherits Base

        Sub New()
        End Sub
        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="OrdenCargue"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>

        Sub New(lector As IDataReader)



            NumeroFinal = Read(lector, "Numero")
            FechaInicial = Read(lector, "Fecha")
            FechaFinal = Read(lector, "Fecha")
            If Read(lector, "Obtener") = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
                Anulado = Read(lector, "Anulado")
            End If

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            ConceptoAnulacion = Read(lector, "CATA_CAOC_Codigo")
            Numero = Read(lector, "Numero")
            Fecha = Read(lector, "Fecha")
            TerceroCliente = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .Nombre = Read(lector, "NombreCliente")}
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "PlacaVehiculo")}


            NumeroDocumento = Read(lector, "Numero_Documento")
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "NombreRuta")}
            Estado = Read(lector, "Estado")
            TerceroConductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .Nombre = Read(lector, "NombreConductor"), .Celular = Read(lector, "TelefonoConductor")}

            Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "PlacaSemirremolque")}
            NumeroSolicitudServicio = Read(lector, "ESOS_Numero")

            TipoDocumento = New Documentos With {.Codigo = Read(lector, "TIDO_Codigo")}
            TerceroRemitente = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Remitente"), .Nombre = Read(lector, "NombreRemitente")}


            CiudadCargue = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Cargue"), .Nombre = Read(lector, "NombreCiudadCargue")}
            FechaCargue = Read(lector, "Fecha_Cargue")
            SitioCargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Cargue"), .Nombre = Read(lector, "SICD_Nombre_Cargue")}

            DireccionCargue = Read(lector, "Direccion_Cargue")
            TelefonosCargue = Read(lector, "Telefonos_Cargue")
            ContactoCargue = Read(lector, "Contacto_Cargue")
            Observaciones = Read(lector, "Observaciones")
            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "ProductoTransportado")}
            CantidadCliente = Read(lector, "Cantidad_Cliente")
            PesoCliente = Read(lector, "Peso_Cliente")
            ValorAnticipo = Read(lector, "Valor_Anticipo")

            CiudadDescargue = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Descargue"), .Nombre = Read(lector, "NombreCiudadDescargue")}
            FechaDescargue = Read(lector, "Fecha_Descargue")
            SitioDescargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Descargue"), .Nombre = Read(lector, "SICD_Nombre_Descargue")}

            DireccionDescargue = Read(lector, "Direccion_Descargue")
            TelefonoDescargue = Read(lector, "Telefono_Descargue")
            ContactoDescargue = Read(lector, "Contacto_Descargue")


            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "NombreOficina")}
            Numeracion = Read(lector, "Numeracion")
            EncabezadoManifiestoCarga = New Manifiesto With {.Numero = Read(lector, "ENMC_Numero")}
            EncabezadoPlanillaDespacho = Read(lector, "ENPD_Numero")

            Try
                FechaCrea = Read(lector, "Fecha_Crea")
                CodigoUsuarioCrea = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Crea")}
                FechaModifica = Read(lector, "Fecha_Modifica")
                CodigoUsuarioModifica = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Modifica"), .Nombre = Read(lector, "USUA_Nombre_Modifica")}
                FechaAnula = Read(lector, "Fecha_Anula")
                CodigoUsuarioAnula = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Anula")}
                CausaAnula = Read(lector, "Causa_Anula")
            Catch ex As Exception

            End Try


        End Sub


#Region "Propiedades"

        <JsonProperty>
        Public Property ListaPrecintos As IEnumerable(Of DetallePrecintos)

        <JsonProperty>
        Public Property NumeroInicial As Integer

        <JsonProperty>
        Public Property NumeroFinal As Integer

        <JsonProperty>
        Public Property FechaInicial As Date

        <JsonProperty>
        Public Property FechaFinal As Date

        <JsonProperty>
        Public Property Numero As Integer

        <JsonProperty>
        Public Property NumeroPlanilla As Integer

        <JsonProperty>
        Public Property NumeroManifiesto As Integer

        <JsonProperty>
        Public Property TipoDocumento As Documentos

        <JsonProperty>
        Public Property ConceptoAnulacion As Integer

        <JsonProperty>
        Public Property NumeroDocumento As Integer

        <JsonProperty>
        Public Property NumeroSolicitudServicio As Long

        <JsonProperty>
        Public Property Fecha As Date

        <JsonProperty>
        Public Property TerceroCliente As Tercero

        <JsonProperty>
        Public Property TerceroRemitente As Tercero

        <JsonProperty>
        Public Property Vehiculo As Vehiculos

        <JsonProperty>
        Public Property Semirremolque As Semirremolques

        <JsonProperty>
        Public Property TerceroConductor As Terceros

        '10
        <JsonProperty>
        Public Property CiudadCargue As Ciudades

        <JsonProperty>
        Public Property FechaCargue As DateTime

        <JsonProperty>
        Public Property SitioCargue As SitiosCargueDescargue

        <JsonProperty>
        Public Property DireccionCargue As String

        <JsonProperty>
        Public Property TelefonosCargue As String

        <JsonProperty>
        Public Property ContactoCargue As String

        <JsonProperty>
        Public Property Observaciones As String

        <JsonProperty>
        Public Property Ruta As Rutas

        <JsonProperty>
        Public Property Producto As ProductoTransportados

        <JsonProperty>
        Public Property CantidadCliente As Double

        <JsonProperty>
        Public Property PesoCliente As Double

        <JsonProperty>
        Public Property ValorAnticipo As Integer

        '20
        <JsonProperty>
        Public Property CiudadDescargue As Ciudades

        <JsonProperty>
        Public Property FechaDescargue As DateTime

        <JsonProperty>
        Public Property SitioDescargue As SitiosCargueDescargue

        <JsonProperty>
        Public Property DireccionDescargue As String

        <JsonProperty>
        Public Property TelefonoDescargue As String

        <JsonProperty>
        Public Property ContactoDescargue As String

        <JsonProperty>
        Public Property Anulado As Integer

        <JsonProperty>
        Public Property Estado As Integer

        <JsonProperty>
        Public Property FechaCrea As DateTime

        <JsonProperty>
        Public Property CodigoUsuarioCrea As Usuarios

        <JsonProperty>
        Public Property FechaModifica As DateTime

        <JsonProperty>
        Public Property CodigoUsuarioModifica As Usuarios

        '30
        <JsonProperty>
        Public Property FechaAnula As DateTime

        <JsonProperty>
        Public Property CodigoUsuarioAnula As Usuarios

        <JsonProperty>
        Public Property CausaAnula As String

        <JsonProperty>
        Public Property Oficina As Oficinas

        <JsonProperty>
        Public Property Numeracion As String

        <JsonProperty>
        Public Property EncabezadoManifiestoCarga As Manifiesto

        <JsonProperty>
        Public Property EncabezadoPlanillaDespacho As Long
        <JsonProperty>
        Public Property IDDetalleSolicitud As Long
        <JsonProperty>
        Public Property DesdeDespacharSolicitud As Boolean
        <JsonProperty>
        Public Property UsuarioConsulta As Usuarios
        '37
#End Region

    End Class
End Namespace