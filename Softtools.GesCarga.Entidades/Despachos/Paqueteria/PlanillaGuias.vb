﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Imports Softtools.GesCarga.Entidades.Despachos.Planilla
Imports Softtools.GesCarga.Entidades.Tesoreria

Namespace Paqueteria
    <JsonObject>
    Public Class PlanillaGuias
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="PlanillaGuias"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Planilla = New Planillas With {.Numero = Read(lector, "Numero")}
            Planilla.Fecha = Read(lector, "Fecha")
            Planilla.Anulado = Read(lector, "Anulado")
            If Read(lector, "Obtener") = 1 Then
                Planilla.NumeroDocumento = Read(lector, "Numero_Documento")
                Planilla.Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo")}
                Planilla.Vehiculo.Conductor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Conductor")}
                Planilla.Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo")}
                Planilla.FechaHoraSalida = Read(lector, "Fecha_Hora_Salida")
                Planilla.Estado = New Estado With {.Codigo = Read(lector, "Estado")}
                Planilla.TipoTarifaTransportes = New TipoTarifaTransportes With {.Codigo = Read(lector, "TTTC_Codigo_Compra"), .Nombre = Read(lector, "Tipo_Tarifa")}
                Planilla.TarifaTransportes = New TarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo_Compra"), .Nombre = Read(lector, "Tarifa")}
                Planilla.LineaNegocioTransporte = New LineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo_Compra")}
                Planilla.TipoLineaNegocioTransportes = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "LNTC_Codigo_Compra")}
                Planilla.Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "Ruta"), .TipoRuta = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIRU_Codigo")}}
                Planilla.TarifarioCompras = Read(lector, "ETCC_Numero")
                Planilla.Cantidad = Read(lector, "Cantidad")
                Planilla.ValorFleteTransportador = Read(lector, "Valor_Flete_Transportador")
                Planilla.ValorAnticipo = Read(lector, "Valor_Anticipo")
                Planilla.ValorImpuestos = Read(lector, "Valor_Impuestos")
                Planilla.ValorPagarTransportador = Read(lector, "Valor_Pagar_Transportador")
                Planilla.ValorFleteCliente = Read(lector, "Valor_Flete_Cliente")
                Planilla.ValorSeguroMercancia = Read(lector, "Valor_Seguro_Mercancia")
                Planilla.ValorOtrosCobros = Read(lector, "Valor_Otros_Cobros")
                Planilla.ValorTotalCredito = Read(lector, "Valor_Total_Credito")
                Planilla.ValorTotalContado = Read(lector, "Valor_Total_Contado")
                Planilla.ValorTotalAlcobro = Read(lector, "Valor_Total_Alcobro")
                Planilla.Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo")}
                TipoDocumento = Read(lector, "TIDO_Codigo")
                RecibidoOficina = Read(lector, "Recibido_Oficina")
            Else
                Planilla.Vehiculo = New Vehiculos With {.Placa = Read(lector, "Placa")}
                Planilla.Vehiculo.Conductor = New Tercero With {.Nombre = Read(lector, "NombreConductor")}
                Planilla.Vehiculo.Tenedor = New Tercero With {.Nombre = Read(lector, "NombreTenedor")}
                Planilla.Ruta = New Rutas With {.Nombre = Read(lector, "NombreRuta")}
                Planilla.Estado = New Estado With {.Codigo = Read(lector, "Estado")}
                Planilla.Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo")}
                Planilla.NumeroDocumento = Read(lector, "Numero_Documento")
                TotalRegistros = Read(lector, "TotalRegistros")
            End If
            Planilla.ValorSeguroPoliza = Read(lector, "Valor_Seguro_Poliza")
            Planilla.AnticipoPagadoA = Read(lector, "Anticipo_Pagado_A")
            Planilla.PagoAnticipo = Read(lector, "PagoAnticipo")
            Planilla.RemesaPadre = Read(lector, "ENRE_Numero_Documento_Masivo")
            Planilla.RemesaMasivo = Read(lector, "ENRE_Numero_Masivo")
            Planilla.Peso = Read(lector, "Peso")
            Planilla.NumeroCumplido = Read(lector, "ECPD_Numero")
            Planilla.NumeroLiquidacion = Read(lector, "ELPD_Numero")
            Planilla.NumeroManifiesto = Read(lector, "Manifiesto")

            Planilla.ValorBruto = Read(lector, "Valor_Bruto")
            Planilla.Porcentaje = Read(lector, "Porcentaje")
            Planilla.ValorDescuentos = Read(lector, "Valor_Descuentos")
            Planilla.FleteSugerido = Read(lector, "Flete_Sugerido")
            Planilla.ValorFondoAyuda = Read(lector, "Valor_Fondo_Ayuda_Mutua")
            Planilla.ValorNetoPagar = Read(lector, "Valor_Neto_Pagar")
            Planilla.ValorUtilidad = Read(lector, "Valor_Utilidad")
            Planilla.ValorEstampilla = Read(lector, "Valor_Estampilla")

            Planilla.GastosAgencia = Read(lector, "Gastos_Agencia")
            Planilla.ValorReexpedicion = Read(lector, "Valor_Reexpedicion")
            Planilla.ValorPlanillaAdicional = Read(lector, "Valor_Planilla_Adicional")
            Planilla.CalcularContraentregas = Read(lector, "Calcular_Contra_Entrega")
            Planilla.Observaciones = Read(lector, "Observaciones")

            NumeroLegalizacion = Read(lector, "ELGC_Numero")

        End Sub

        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Planilla As Planillas
        <JsonProperty>
        Public Property CuentaPorPagar As EncabezadoDocumentoCuentas
        <JsonProperty>
        Public Property ListadoPlanillaRecolecciones As IEnumerable(Of DetallePlanillaRecolecciones)
        <JsonProperty>
        Public Property ListadoPlanillaRecoleccionesNoCkeck As IEnumerable(Of DetallePlanillaRecolecciones)
        <JsonProperty>
        Public Property ConsultaPLanillasenRuta As Short
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property Conductores As IEnumerable(Of Terceros)
        <JsonProperty>
        Public Property NumeroLegalizacion As Integer
        <JsonProperty>
        Public Property RecibidoOficina As Integer
        <JsonProperty>
        Public Property Conductor As Terceros
        <JsonProperty>
        Public Property DocumentosRelacionados As IEnumerable(Of EncabezadoDocumentos)

    End Class
End Namespace
