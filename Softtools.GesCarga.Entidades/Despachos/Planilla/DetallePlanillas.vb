﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Entidades.Paqueteria

Namespace Despachos.Planilla
    <JsonObject>
    Public NotInheritable Class DetallePlanillas
        Inherits BaseDetalleDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            NumeroRemesa = Read(lector, "NumeroRemesa")
            NumeroInternoRemesa = Read(lector, "Numero")
            NombreCliente = Read(lector, "NombreCliente")
            CodigoCliente = Read(lector, "TERC_Codigo_Cliente")
            NombreProducto = Read(lector, "NombreProducto")
            CodigoProducto = Read(lector, "PRTR_Codigo")
            CantidadRemesa = Read(lector, "CantidadRemesa")
            PesoRemesa = Read(lector, "PesoRemesa")
            NombreDestinatario = Read(lector, "NombreDestinatario")
            Fecha = Read(lector, "Fecha")
            NombreRuta = Read(lector, "NombreRuta")
            DocumentoCliente = Read(lector, "Documento_Cliente")
            FleteCliente = Read(lector, "Valor_Flete_Cliente")
            TotalFleteTransportador = Read(lector, "Total_Flete_Transportador")
            CodigoCiudadDestinoRuta = Read(lector, "CIUD_Codigo_Destino")
            CiudadDestinatario = Read(lector, "CiudadDestinatario")
            CiudadRemitente = Read(lector, "CiudadRemitente")
            DireccionDestinatario = Read(lector, "DireccionDestinatario")
            NumeroOrdenServicio = Read(lector, "ESOS_Numero")
            Observaciones = Read(lector, "Observaciones")
            NumeroUnidad = Read(lector, "Numero_Unidad")
            NumeroDocumentoRemesa = Read(lector, "Numero_Documento_Remesa")
            TotalFleteCliente = Read(lector, "TotalFleteCliente")
        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        ''' 
        <JsonProperty>
        Public Property NumeroDocumentoRemesa As Long
        <JsonProperty>
        Public Property NumeroUnidad As Long
        <JsonProperty>
        Public Property ObservacionesVerificacion As String
        <JsonProperty>
        Public Property Remesa As Remesas
        <JsonProperty>
        Public Property RemesaPaqueteria As RemesaPaqueteria
        <JsonProperty>
        Public Property TotalFleteCliente As Integer
        <JsonProperty>
        Public Property NumeroPlanilla As Integer
        <JsonProperty>
        Public Property NumeroInternoRemesa As Long
        <JsonProperty>
        Public Property NumeroRemesa As Double
        <JsonProperty>
        Public Property CodigoCliente As Integer
        <JsonProperty>
        Public Property NombreCliente As String
        <JsonProperty>
        Public Property NombreProducto As String
        <JsonProperty>
        Public Property CodigoProducto As Integer
        <JsonProperty>
        Public Property CantidadRemesa As Double
        <JsonProperty>
        Public Property PesoRemesa As Double
        <JsonProperty>
        Public Property NombreDestinatario As String

        <JsonProperty>
        Public Property Fecha As DateTime
        <JsonProperty>
        Public Property NombreRuta As String

        <JsonProperty>
        Public Property DocumentoCliente As String

        <JsonProperty>
        Public Property FleteCliente As Double
        <JsonProperty>
        Public Property TotalFleteTransportador As Double

        <JsonProperty>
        Public Property CodigoCiudadDestinoRuta As Integer
        <JsonProperty>
        Public Property CiudadDestinatario As String
        Public Property CiudadRemitente As String

        <JsonProperty>
        Public Property DireccionDestinatario As String
        <JsonProperty>
        Public Property ListaReexpedicionOficinas As IEnumerable(Of ReexpedicionRemesasPaqueteria)
        <JsonProperty>
        Public Property NumeroOrdenServicio As Integer

        <JsonProperty>
        Public Property Observaciones As String


    End Class
End Namespace
