﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Entidades.Despachos.Planilla
Imports Softtools.GesCarga.Entidades.Tesoreria

Namespace Despachos.Masivo
    <JsonObject>
    Public Class ConductorTrayectoPlanillaDespacho
        Inherits Planillas
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Planilla = New PlanillaDespachos With {.Numero = Read(lector, "ENPD_Numero")}
            Conductor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Conductor")}
            RutaTrayecto = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo_Trayecto")}
            ValorFlete = Read(lector, "Valor_Flete")
            ValorAnticipo = Read(lector, "Valor_Anticipo")
            PorcentajeFlete = Read(lector, "Porcentaje_Flete_Trayecto")
            PorcentajeAnticipo = Read(lector, "Porcentaje_Anticipo_Trayecto")
            PorcentajeAnticipoRuta = Read(lector, "Porcentaje_Anticipo_Ruta")
            OrdenTrayecto = Read(lector, "Orden_Trayecto")

        End Sub

        <JsonProperty>
        Public Property CuentaPorPagar As EncabezadoDocumentoCuentas

        <JsonProperty>
        Public Property Planilla As PlanillaDespachos

        <JsonProperty>
        Public Property RutaTrayecto As Rutas

        <JsonProperty>
        Public Property ValorFlete As Double

        <JsonProperty>
        Public Property PorcentajeFlete As Double
        <JsonProperty>
        Public Property PorcentajeAnticipo As Double
        <JsonProperty>
        Public Property PorcentajeAnticipoRuta As Double
        <JsonProperty>
        Public Property OrdenTrayecto As Integer
    End Class
End Namespace
