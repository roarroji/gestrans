﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref=" DetalleImpuestosPlanilla"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleImpuestosPlanilla
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleImpuestosPlanilla"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleImpuestosPlanilla"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            Label = Read(lector, "Label")
            Valor_tarifa = Read(lector, "Valor_Tarifa")
            valor_base = Read(lector, "Valor_Base")
            ValorImpuesto = Read(lector, "Valor_Impuesto")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
                PaginaObtener = Read(lector, "PaginaObtener")
                RegistrosPagina = Read(lector, "RegistrosPagina")
            End If
            Try

                Codigo_USUA_Modi = Read(lector, "USUA_Codigo_Modifica")
            Catch ex As Exception

            End Try



        End Sub

        <JsonProperty>
        Public Property CodigoTercero As String

        <JsonProperty>
        Public Property CodigoAlterno As String


        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property Tipo_Recaudo As ValorCatalogos

        <JsonProperty>
        Public Property Tipo_Impuesto As ValorCatalogos

        <JsonProperty>
        Public Property Operacion As Integer

        <JsonProperty>
        Public Property Valor_tarifa As Double

        <JsonProperty>
        Public Property valor_base As Long

        <JsonProperty>
        Public Property PUC As PlanUnicoCuentas

        <JsonProperty>
        Public Property Codigo As Integer

        <JsonProperty>
        Public Property Codigo_USUA_Crea As Integer

        <JsonProperty>
        Public Property Codigo_USUA_Modi As Integer

        <JsonProperty>
        Public Property Estado As Integer

        <JsonProperty>
        Public Property Label As String

        <JsonProperty>
        Public Property ValorImpuesto As Double



        <JsonProperty>
        Public Property PaginaObtener As Integer




    End Class
End Namespace
