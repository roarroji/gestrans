﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Despachos.Planilla
    <JsonObject>
    Public Class DetalleTiemposPlanillaDespachos
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleTiemposPlanillaDespachos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroPlanilla = Read(lector, "ENPD_Numero")
            NumeroRemesa = Read(lector, "ENRE_Numero")
            NumeroDocumentoRemesa = Read(lector, "NumeroRemesa")
            SitioReporteSeguimientoVehicular = New ValorCatalogos With {.Codigo = Read(lector, "CATA_SRSV_Codigo")}
            FechaHora = Read(lector, "Fecha_Hora")

        End Sub


        <JsonProperty>
        Public Property SitioReporteSeguimientoVehicular As ValorCatalogos

        <JsonProperty>
        Public Property NumeroPlanilla As Integer

        <JsonProperty>
        Public Property NumeroRemesa As Integer
        <JsonProperty>
        Public Property NumeroDocumentoRemesa As Integer
        <JsonProperty>
        Public Property FechaHora As DateTime


    End Class
End Namespace
