﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades.Despachos.Planilla
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Entidades.Paqueteria

Namespace Despachos.Remesa
    <JsonObject>
    Public NotInheritable Class Remesas
        Inherits Base
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Remesas"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            Naviera = Read(lector, "Naviera")
            NumeroDocumento = Read(lector, "Numero_Documento")
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            TipoDocumento = New TipoDocumentos With {.Codigo = Read(lector, "TIDO_Codigo")}
            FechaInicial = Read(lector, "Fecha")
            FechaFinal = Read(lector, "Fecha")
            TotalRegistros = Read(lector, "TotalRegistros")
            NumeroOrdenServicio = Read(lector, "ESOS_Numero")
            NumeroSemillaOrdenServicio = Read(lector, "ESOS_Numero")
            Anulado = Read(lector, "Anulado")


            TipoDocumento = New TipoDocumentos With {.Codigo = Read(lector, "TIDO_Codigo"), .Nombre = Read(lector, "TipoDocumento")}
            FechaDocumentoCliente = Read(lector, "Fecha_Documento_Cliente")
            'FormaPago
            Remitente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Remitente"), .NombreCompleto = Read(lector, "NombreRemitente")}
            Remitente.Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Remitente"), .Nombre = Read(lector, "CiudadRemitente")}
            DireccionRemitente = Read(lector, "Direccion_Remitente")
            TelefonoRemitente = Read(lector, "Telefonos_Remitente")
            Observaciones = Read(lector, "Observaciones")
            Numeracion = Read(lector, "Numeracion")
            PesoCliente = Read(lector, "Peso_Cliente")
            PesoVolumetricoCliente = Read(lector, "Peso_Volumetrico_Cliente")
            DetalleTarifaVenta = New DetalleTarifarioVentas With {.Codigo = Read(lector, "DTCV_Codigo"), .NumeroTarifario = Read(lector, "NumeroTarifarioVenta")}

            ValorManejoCliente = Read(lector, "Valor_Manejo_Cliente")
            ValorSeguroCliente = Read(lector, "Valor_Seguro_Cliente")
            ValorDescuentoCliente = Read(lector, "Valor_Descuento_Cliente")
            ValorComercialCliente = Read(lector, "Valor_Comercial_Cliente")
            CantidadTransportador = Read(lector, "Cantidad_Transportador")
            PesoTransportador = Read(lector, "Peso_Transportador")
            ValorFleteTransportador = Read(lector, "Valor_Flete_Transportador")
            Destinatario = New Terceros With {.NumeroIdentificacion = Read(lector, "Numero_Identificacion"), .Codigo = Read(lector, "TERC_Codigo_Destinatario"),
                .NombreCompleto = Read(lector, "NombreDestinatario"), .TipoIdentificacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIID_Codigo")},
                .Telefonos = Read(lector, "Telefonos")
            }
            Destinatario.Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destinatario"), .Nombre = Read(lector, "CiudadDestinatario")}
            DireccionDestinatario = Read(lector, "Direccion_Destinatario")
            TelefonoDestinatario = Read(lector, "Telefonos_Destinatario")
            Anulado = Read(lector, "Anulado")
            FechaCrea = Read(lector, "Fecha_Crea")
            UsuarioCrea = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Crea"), .CodigoUsuario = Read(lector, "UsuarioCrea")}
            FechaModifica = Read(lector, "Fecha_Modifica")
            FechaAnula = Read(lector, "Fecha_Anula")
            CausaAnula = Read(lector, "Causa_Anula")

            TarifarioCompra = New TarifarioCompras With {.Numero = Read(lector, "NumeroTarifarioCompra")}
            OrdenCargue = New OrdenCargue.OrdenCargue With {.Numero = Read(lector, "NumeroOrdenCargue"), .NumeroDocumento = Read(lector, "NumeroDocumentoOrdenCargue")}
            TarifaCliente = New TarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo"), .Nombre = Read(lector, "TipoTarifa")}
            'TarifaTransportador = New TarifaTransportes With {.Codigo = Read(lector, "")}
            TipoTarifaCliente = New TipoTarifaTransportes With {.Codigo = Read(lector, "CodigoValorTipoTarifa"), .Nombre = Read(lector, "NombreValorTipoTarifa")}
            'TipoTarifaTransportador = New TipoTarifaTransportes With {.Codigo = Read(lector, "")}
            TipoVehiculo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIVE_Codigo"), .Nombre = Read(lector, "TipoVehiculo")}
            NumeroConfirmacionMinisterio = Read(lector, "Numero_Remesa_Electronico")
            NumeroContenedor = Read(lector, "Numero_Contenedor")
            MBLContenedor = Read(lector, "MBL_Contenedor")
            HBLContenedor = Read(lector, "HBL_Contenedor")
            DOContenedor = Read(lector, "DO_Contenedor")
            ValorFOB = Read(lector, "Valor_FOB")
            UsuarioModifica = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Modifica")}
            FechaRecibe = Read(lector, "Fecha_Recibe")
            CantidadRecibe = Read(lector, "Cantidad_Recibe")
            PesoRecibe = Read(lector, "Peso_Recibe")
            CodigoTipoIdentificacionRecibe = Read(lector, "CATA_TIID_Codigo_Recibe")
            NumeroIdentificacionRecibe = Read(lector, "Numero_Identificacion_Recibe")
            NombreRecibe = Read(lector, "Nombre_Recibe")
            TelefonoRecibe = Read(lector, "Telefonos_Recibe")
            Firma = Read(lector, "Firma_Recibe")
            ObservacionesRecibe = Read(lector, "Observaciones_Recibe")

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            ValorFleteCliente = Read(lector, "Valor_Flete_Cliente")
            CantidadCliente = Read(lector, "Cantidad_Cliente")
            TotalFleteCliente = Read(lector, "Total_Flete_Cliente")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            Fecha = Read(lector, "Fecha")
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "NombreRuta")}
            NumeroCumplido = Read(lector, "NumeroCumplido")
            Factura = New Facturas With {.Numero = Read(lector, "NumeroFactura"), .NumeroDocumento = Read(lector, "ENFA_Numero_Documento"), .Estado = Read(lector, "ENFA_Estado")}
            NumeroOrdenServicio = Read(lector, "OrdenServicio")
            Manifiesto = New Manifiesto With {.Numero = Read(lector, "NumeroManifiesto"), .NumeroDocumento = Read(lector, "NumeroDocumentoManifiesto")}
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .NombreCompleto = Read(lector, "NombreCliente")}
            NumeroDocumentoCliente = Read(lector, "Documento_Cliente")
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "Placa")}
            Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "PlacaSemirremolque")}
            Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .NombreCompleto = Read(lector, "NombreConductor"), .Telefonos = Read(lector, "TelefonoConductor")}
            Tenedor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Tenedor"), .NombreCompleto = Read(lector, "NombreTenedor")}
            TipoRemesa = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIRE_Codigo"), .Nombre = Read(lector, "TipoRemesa")}
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "NombreOficina")}
            Estado = Read(lector, "Estado")
            Planilla = New Planillas With {.Numero = Read(lector, "NumeroPlanilla"), .NumeroDocumento = Read(lector, "NumeroDocumentoPlanilla")}
            ProductoTransportado = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "NombreProducto")}
            NumeroConfirmacionMinisterioRemesa = Read(lector, "Numero_Remesa_Electronico")
            LineaNegocio = New LineaNegocioTransportes With {.Codigo = Read(lector, "LNTC_Codigo"), .Nombre = Read(lector, "LineaNegocio")}
            NombreSede = Read(lector, "NombreSede")
            SitioDescargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Descargue"), .Nombre = Read(lector, "SitioDescargue")}
            TotalFleteTransportador = Read(lector, "Total_Flete_Transportador")
            TipoDespacho = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TDOS_Codigo"), .Nombre = Read(lector, "TipoDespacho")}
            Try
                Observaciones = Read(lector, "Observaciones")
                Distribucion = Read(lector, "Distribucion")
            Catch ex As Exception

            End Try
            CiudadDevolucion = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Devolucion_Contenedor"), .Nombre = Read(lector, "NombreCiudadDevolucion")}
            PatioDevolucion = Read(lector, "Patio_Devolcuion")
            FechaDevolucion = Read(lector, "Fecha_Devolcuion")
            RemesaCortesia = Read(lector, "Remesa_Cortesia")
            PermisoInvias = Read(lector, "Permiso_Invias")
            NovedadDespacho = Read(lector, "Numero_Novedad")
            DSOS = New ServicioCliente.DetalleSolicitudOrdenServicios With {.FechaDevolucionContenedor = Read(lector, "FechaDevolucion")}
            DiasDemora = Read(lector, "DiasDemora")
            FechaDescargueMercancia = Read(lector, "Fecha_Descargue_Mercancia")
            CodigoOrdenServicio = Read(lector, "CodigoOrdenServicio")
            FechaDevolucionContenedor = Read(lector, "Fecha_Devolucion_Contenedor")
            SICDDevolucionContenedor = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Devolucion_Contenedor")}
            Devuelto = Read(lector, "Devuelto")
            FacturarPesoCumplido = Read(lector, "Facturar_Cantidad_Peso_Cumplido")
            CumplidoRemesa = New CumplidoRemesa With {.PesoDescargue = Read(lector, "Peso_Descargue")}
            ValorDeclarado = Read(lector, "Valor_Declarado")
            DT = Read(lector, "DT_SAP")
            Entrega = Read(lector, "Entrega_SAP")
            TerceroFacturarA = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Facturara"), .NombreCompleto = Read(lector, "NombreFacturarA")}
        End Sub

        <JsonProperty>
        Public Property TerceroFacturarA As Terceros
        <JsonProperty>
        Public Property CodigoClientePagador As String
        <JsonProperty>
        Public Property DT As String
        <JsonProperty>
        Public Property Entrega As String
        <JsonProperty>
        Public Property Remesas As IEnumerable(Of Remesas)

        <JsonProperty>
        Public Property ValorDeclarado As Long

        <JsonProperty>
        Public Property Devuelto As String

        <JsonProperty>
        Public Property SICDDevolucionContenedor As SitiosCargueDescargue

        <JsonProperty>
        Public Property FechaDevolucionContenedor As Date

        <JsonProperty>
        Public Property TarifaTransporte As Integer
        <JsonProperty>
        Public Property CodigoOrdenServicio As Long
        <JsonProperty>
        Public Property FechaDescargueMercancia As Date
        <JsonProperty>
        Public Property DiasDemora As Integer
        <JsonProperty>
        Public Property DSOS As ServicioCliente.DetalleSolicitudOrdenServicios
        <JsonProperty>
        Public Property NovedadDespacho As Long
        <JsonProperty>
        Public Property FechaInicial As Date

        <JsonProperty>
        Public Property Naviera As String

        <JsonProperty>
        Public Property FechaFinal As Date

        <JsonProperty>
        Public Property FiltroCliente As String

        <JsonProperty>
        Public Property FiltroConductor As String

        <JsonProperty>
        Public Property FiltroTenedor As String

        <JsonProperty>
        Public Property Numero As Integer

        <JsonProperty>
        Public Property TipoDocumento As TipoDocumentos

        <JsonProperty>
        Public Property TipoRemesaDocumento As TipoDocumentos

        <JsonProperty>
        Public Property NumeroDocumento As Long

        <JsonProperty>
        Public Property TipoRemesa As ValorCatalogos

        <JsonProperty>
        Public Property NumeroDocumentoCliente As String

        <JsonProperty>
        Public Property FechaDocumentoCliente As Date

        <JsonProperty>
        Public Property Fecha As Date

        <JsonProperty>
        Public Property Ruta As Rutas

        <JsonProperty>
        Public Property ProductoTransportado As ProductoTransportados

        <JsonProperty>
        Public Property FormaPago As ValorCatalogos

        <JsonProperty>
        Public Property Cliente As Terceros

        <JsonProperty>
        Public Property Remitente As Terceros

        <JsonProperty>
        Public Property CiudadRemitente As Ciudades

        <JsonProperty>
        Public Property DireccionRemitente As String

        <JsonProperty>
        Public Property TelefonoRemitente As String

        <JsonProperty>
        Public Property Observaciones As String

        <JsonProperty>
        Public Property CantidadCliente As Double

        <JsonProperty>
        Public Property CantidadPrecintos As Double

        <JsonProperty>
        Public Property NumeroConfirmacionMinisterioRemesa As String

        <JsonProperty>
        Public Property PesoCliente As Double

        <JsonProperty>
        Public Property PesoVolumetricoCliente As Double

        <JsonProperty>
        Public Property DetalleTarifaVenta As DetalleTarifarioVentas

        <JsonProperty>
        Public Property TarifarioCompra As TarifarioCompras

        <JsonProperty>
        Public Property ValorManejoCliente As Double

        <JsonProperty>
        Public Property ValorFleteCliente As Double

        <JsonProperty>
        Public Property ValorSeguroCliente As Double

        <JsonProperty>
        Public Property Cumplida As Double

        <JsonProperty>
        Public Property ValorDescuentoCliente As Double

        <JsonProperty>
        Public Property TotalFleteCliente As Double

        <JsonProperty>
        Public Property ValorComercialCliente As Double

        <JsonProperty>
        Public Property CantidadTransportador As Double

        <JsonProperty>
        Public Property PesoTransportador As Double

        <JsonProperty>
        Public Property ValorFleteTransportador As Double

        <JsonProperty>
        Public Property TotalFleteTransportador As Double

        <JsonProperty>
        Public Property Destinatario As Terceros

        <JsonProperty>
        Public Property CiudadDestinatario As Ciudades

        <JsonProperty>
        Public Property DireccionDestinatario As String

        <JsonProperty>
        Public Property TelefonoDestinatario As String

        <JsonProperty>
        Public Property Anulado As Integer

        <JsonProperty>
        Public Property Estado As Integer

        <JsonProperty>
        Public Property FechaCrea As Date

        <JsonProperty>
        Public Property UsuarioCrea As Usuarios

        <JsonProperty>
        Public Property FechaModifica As Date

        <JsonProperty>
        Public Property UsuarioModifica As Usuarios

        <JsonProperty>
        Public Property FechaAnula As Date

        <JsonProperty>
        Public Property UsuarioAnula As Usuarios

        <JsonProperty>
        Public Property CausaAnula As String

        <JsonProperty>
        Public Property Oficina As Oficinas

        <JsonProperty>
        Public Property Numeracion As String

        <JsonProperty>
        Public Property TarifarioVenta As TarifarioVentas

        <JsonProperty>
        Public Property NumeroOrdenServicio As Long 'Numero Documento Orden De Servicio
        <JsonProperty>
        Public Property NumeroDocumentoOrdenServicio As Long
        <JsonProperty>
        Public Property NumeroSemillaOrdenServicio As Long 'Numero Orden Servicio

        <JsonProperty>
        Public Property OrdenCargue As OrdenCargue.OrdenCargue

        <JsonProperty>
        Public Property Planilla As Planillas

        <JsonProperty>
        Public Property Manifiesto As Manifiesto

        <JsonProperty>
        Public Property NumeroCumplido As Long

        <JsonProperty>
        Public Property Factura As Facturas

        <JsonProperty>
        Public Property Vehiculo As Vehiculos

        <JsonProperty>
        Public Property Conductor As Terceros

        <JsonProperty>
        Public Property Tenedor As Terceros

        <JsonProperty>
        Public Property TarifaCliente As TarifaTransportes

        <JsonProperty>
        Public Property TarifaTransportador As TarifaTransportes

        <JsonProperty>
        Public Property TipoTarifaCliente As TipoTarifaTransportes

        <JsonProperty>
        Public Property TipoTarifaTransportador As TipoTarifaTransportes

        <JsonProperty>
        Public Property TipoVehiculo As ValorCatalogos

        <JsonProperty>
        Public Property DesdeDespacharSolicitud As Boolean

        <JsonProperty>
        Public Property IDDetalleSolicitud As Long
        <JsonProperty>
        Public Property RemesaCortesia As Short


        <JsonProperty>
        Public Property Semirremolque As Semirremolques

        <JsonProperty>
        Public Property NumeroConfirmacionMinisterio As String

        <JsonProperty>
        Public Property Numeroplanillarecoleccion As Integer

        <JsonProperty>
        Public Property Numeroplanillaentrega As Integer

        <JsonProperty>
        Public Property NumeroplanillaDespacho As Integer

        <JsonProperty>
        Public Property NumeroDocumentoPlanillaDespacho As Integer

        <JsonProperty>
        Public Property Numerocumplidoplanilladespacho As Long

        <JsonProperty>
        Public Property Numerocumplidoplanillaentrega As Long

        <JsonProperty>
        Public Property Numerocumplidoplanillarecoleccion As Long
        <JsonProperty>
        Public Property Cumplido As Integer
        <JsonProperty>
        Public Property CumplidoRemesas As IEnumerable(Of CumplidoRemesa)
        <JsonProperty>
        Public Property CumplidoRemesa As CumplidoRemesa
        <JsonProperty>
        Public Property BarrioRemitente As String
        <JsonProperty>
        Public Property CodigoPostalRemitente As String
        <JsonProperty>
        Public Property BarrioDestinatario As String
        <JsonProperty>
        Public Property CodigoPostalDestinatario As String
        <JsonProperty>
        Public Property ObservacionesRemitente As String
        <JsonProperty>
        Public Property ObservacionesDestinatario As String
        <JsonProperty>
        Public Property PendienteFacturadas As Boolean


        <JsonProperty>
        Public Property NumeroPlanilla As Integer
        <JsonProperty>
        Public Property Distribucion As Integer
        <JsonProperty>
        Public Property DocumentosRelacionados As IEnumerable(Of EncabezadoDocumentos)

        <JsonProperty>
        Public Property NumeroContenedor As String
        <JsonProperty>
        Public Property MBLContenedor As String
        <JsonProperty>
        Public Property HBLContenedor As String
        <JsonProperty>
        Public Property DOContenedor As String
        <JsonProperty>
        Public Property ValorFOB As Double

        <JsonProperty>
        Public Property TipoConsulta As Short
        <JsonProperty>
        Public Property DevolucionRemesa As Short
        <JsonProperty>
        Public Property Entregado As Short
        <JsonProperty>
        Public Property FechaRecibe As DateTime
        <JsonProperty>
        Public Property CantidadRecibe As Double
        <JsonProperty>
        Public Property PesoRecibe As Double
        <JsonProperty>
        Public Property CodigoTipoIdentificacionRecibe As Integer
        <JsonProperty>
        Public Property NumeroIdentificacionRecibe As Long
        <JsonProperty>
        Public Property NombreRecibe As String
        <JsonProperty>
        Public Property TelefonoRecibe As String
        <JsonProperty>
        Public Property ObservacionesRecibe As String
        <JsonProperty>
        Public Property NumerosRemesas As String
        <JsonProperty>
        Public Property Fotografia As FotoDetalleDistribucionRemesas
        <JsonProperty>
        Public Property Fotografias As IEnumerable(Of FotoDetalleDistribucionRemesas)
        <JsonProperty>
        Public Property Firma As Byte()
        <JsonProperty>
        Public Property ListaPrecintos As IEnumerable(Of DetallePrecintos)
        <JsonProperty>
        Public Property LineaNegocio As LineaNegocioTransportes
        <JsonProperty>
        Public Property CodigoSede As Integer
        <JsonProperty>
        Public Property NombreSede As String
        <JsonProperty>
        Public Property TipoPrecinto As ValorCatalogos
        <JsonProperty>
        Public Property NovedadEntrega As ValorCatalogos
        <JsonProperty>
        Public Property UsuarioConsulta As Usuarios
        <JsonProperty>
        Public Property SitioDescargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property TipoDespacho As ValorCatalogos
        <JsonProperty>
        Public Property CiudadDevolucion As Ciudades
        <JsonProperty>
        Public Property PatioDevolucion As String
        <JsonProperty>
        Public Property FechaDevolucion As Date
        <JsonProperty>
        Public Property PermisoInvias As String
        <JsonProperty>
        Public Property FacturarA As Integer
        <JsonProperty>
        Public Property ValorComisionOficina As Double
        <JsonProperty>
        Public Property ValorComisionAgencista As Double
        <JsonProperty>
        Public Property ValorOtros As Double
        <JsonProperty>
        Public Property ValorCargue As Double
        <JsonProperty>
        Public Property ValorDescargue As Double
        <JsonProperty>
        Public Property FacturarPesoCumplido As Integer
        <JsonProperty>
        Public Property GestionDocumentosRemesa As IEnumerable(Of GestionDocumentosRemesa)
    End Class
End Namespace