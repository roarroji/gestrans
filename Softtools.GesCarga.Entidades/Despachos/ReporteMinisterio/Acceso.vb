﻿Imports System.Xml
Namespace Despachos.Procesos
    Public Class Acceso

#Region "Variables"
        Public username As String
        Public password As String
        Public simulacion As String
#End Region

#Region "Constantes"
        Public Const NOMBRE_TAG As String = "acceso"

        Public Const AMBIENTE_WS_MINISTERIO_R = "R"
        Public Const AMBIENTE_WS_MINISTERIO_S = "S"
#End Region

#Region "Constructor"
        Sub New(ByVal username As String, ByVal password As String, ByVal simulacion As String)
            Me.username = username
            Me.password = password
            Me.simulacion = simulacion
        End Sub

        Public Sub New()
            username = ""
            password = ""
            simulacion = ""
        End Sub
#End Region

#Region "Funciones Publicas"
        Public Sub Crear_XML_Acceso(ByRef XmlDocumento As XmlDocument)
            Try
                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                xmlElementoRoot = XmlDocumento.DocumentElement

                NuevoElemento = XmlDocumento.CreateElement("username")
                Dim txtNodo As XmlText = XmlDocumento.CreateTextNode(Me.username)
                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                NuevoElemento = XmlDocumento.CreateElement("password")
                txtNodo = XmlDocumento.CreateTextNode(Me.password)
                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                NuevoElemento = XmlDocumento.CreateElement("ambiente")
                txtNodo = XmlDocumento.CreateTextNode(Me.simulacion)
                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
            Catch ex As Exception

            End Try
        End Sub
#End Region
    End Class
End Namespace

