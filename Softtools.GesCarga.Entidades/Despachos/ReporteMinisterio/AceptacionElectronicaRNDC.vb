﻿Imports Newtonsoft.Json
Namespace Despachos.Procesos
    <JsonObject>
    Public NotInheritable Class AceptacionElectronicaRNDC
        Inherits BaseDocumento
        Sub New()
        End Sub
        Sub New(lector As IDataReader)
            OpcionReporte = Read(lector, "OpcionReporte")
            NUMNITEMPRESATRANSPORTE = Read(lector, "NUMNITEMPRESATRANSPORTE")

            INGRESOIDMANIFIESTO = Read(lector, "INGRESOIDMANIFIESTO")
        End Sub

        <JsonProperty>
        Public Property NUMNITEMPRESATRANSPORTE As String
        <JsonProperty>
        Public Property INGRESOIDMANIFIESTO As String
        <JsonProperty>
        Public Property OpcionReporte As Integer

    End Class
End Namespace
