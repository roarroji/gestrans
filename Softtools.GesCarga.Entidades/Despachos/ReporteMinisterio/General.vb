﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Configuration
Imports System.Text.RegularExpressions
Imports System.Xml
Imports System.Text
Namespace Despachos.Procesos
    Public Class General

#Region "Declaracion Enums"

        Public Enum Resultado_Proceso_Reporte As Integer
            EXITOSO = 1
            ERROR_REPORTE = 2
            ERROR_ACTUALIZA_CONFIRMACION = 3
        End Enum

        Public Enum Tipos_Procesos As Integer
            FORMATO_INGRESO = 1
            FORTMATO_CONSULTA = 2
            FORTMATO_CONSULTA_PROCESOS = 3
            FORTMATO_CONSULTA_SICETAC = 6
        End Enum
        Public Enum Tipos_Roles As Integer
            REMITENTE = 1
            DESTINATARIO = 2
            CONDUCTOR = 3
            TENEDOR = 4
            PROPIETARIO = 5
        End Enum
        Public Enum Tipos_Documentos As Integer
            SOLICITUD_WS_MINISTERIO_PROCESO_INFORMACION_CARGA = 1
            SOLICITUD_WS_MINISTERIO_PROCESO_INFORMACION_VIAJE = 2
            SOLICITUD_WS_MINISTERIO_PROCESO_REMESA = 3
            SOLICITUD_WS_MINISTERIO_PROCESO_MANIFIESTO = 4
            SOLICITUD_WS_MINISTERIO_PROCESO_CUMPLIR_REMESA = 5
            SOLICITUD_WS_MINISTERIO_PROCESO_CUMPLIR_INICIAL_REMESA = 45
            SOLICITUD_WS_MINISTERIO_PROCESO_CUMPLIR_MANIFIESTO = 6
            SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_INFO_CARGA = 7
            SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_VIAJE = 8
            SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_REMESA = 9
            SOLICITUD_WS_MINISTERIO_PROCESO_TERCERO = 11
            SOLICITUD_WS_MINISTERIO_PROCESO_VEHICULO = 12
            SOLICITUD_WS_MINISTERIO_PROCESO_CONSULTA_SICETAC = 26
            SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_MANIFIESTO = 32
            SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_CUMPLIDO_MANIFIESTO = 29
            SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_CUMPLIDO_REMESA = 28
            SOLICITUD_WS_MINISTERIO_PROCESO_CONSULTA_ACEPTACION_ELECTRONICA = 73

        End Enum
        Public Enum Ambiantes_Desarrollos As Integer
            GESTRANS20 = 1
            GESTRANS45 = 2
            LOCAL = 3
        End Enum
        Public Enum Tipos_Configuracion_Vehiculo

            CAMIONRIGIDODE2EJES = 50
            CAMIONRIGIDODE3EJES = 51
            CAMIONRIGIDODE4EJES = 52
            CAMIONRIGIDODEMASDE4EJES = 56

            TRACTOCAMIONDE2EJES = 53
            TRACTOCAMIONDE3EJES = 54
            TRACTOCAMIONDEMASDE3EJES = 55


            SEMIREMOLQUEDE1EJE = 61
            SEMIREMOLQUEDE2EJE = 62
            SEMIREMOLQUEDE3EJE = 63
            SEMIREMOLQUEDEMAS4EJE = 64

            REMOLQUEDE2EJES = 71
            REMOLQUEDE3EJES = 72
            REMOLQUEDE4EJES = 73
            REMOLQUEDEMAS4EJES = 74

            REMOLQUEBALANCEADODE1EJE = 81
            REMOLQUEBALANCEADODE2EJE = 82
            REMOLQUEBALANCEADODE3EJE = 83
            REMOLQUEBALANCEADODE4EJE = 84
            REMOLQUEBALANCEADODEMASDE4EJE = 85

        End Enum

        Public Enum OPCIONES_REPORTE_RNDC
            REMESA = 1
            REMESA_CUMPLIDA = 2
            REMESA_ANULADA = 3
            REMESA_CUMLIDA_ANULADA = 4
            MANIFIESTO = 5
            MANIFIESTO_CUMPLIDO = 6
            MANIFIESTO_ANULADO = 7
            MANIFIESTO_CUMPLIDO_ANULADO = 8
            REMESA_CUMPLIDO_INICIAL = 9
            ACEPTACION_ELECTRONICA = 10
        End Enum

#End Region

#Region "Declaracion Variables"

        ' Objetos de base de datos
        Private strCadenaDeConexionSQL As String
        Private strCadenaDeConexionDocumentalSQL As String
        Private sqlConexion As SqlConnection
        Private sqlConexionDocumental As SqlConnection
        Private sqlTransaccion As SqlTransaction
        Private sqlComando As SqlCommand
        Private sqlExcepcionSQL As SqlException
        Private strNombreConexion As String
        Public strSmtpCorreo As String
        Public strUsuarioCorreo As String
        Public strContrasenaCorreo As String
        Public intRequiereAutenticacionSmtp As Byte
        Public strSmtpPuerto As String
        Public intAutenticacionSSL As Byte
        'Parametro que define los días de vigencia que tiene un estudio de seguridad para ser usado al crear un tercero
        Public intDiasVigenciaEstudioSeguridad As Integer

        ' Parametros de Consultas
        Public intDiasRangConsMast As Integer
        Public intDocuRangConsMast As Integer
        Public intTreiDiasRangConsMast As Integer
        Public intRangoConsultaListados As Integer

        ' Parametro Log Menu usuarios
        Public intLogMenuUsuarios As Integer

        ' Parametro Ip Servidor Telmex
        Public strIpServidor As String
        ' Parametro Número Máximo de Puesto Control para el Impreso
        Public intMaxNumPueCon As Integer
        ' Imprimir Active X
        Public intImprimirActiveX As Integer

        Public strUsuarioAvantel As String
        Public strClaveAvantel As String
        Public strRutaCarpetaDocumentosRemesas As String

#End Region

#Region "Constantes"

        Public Const CREAR_CARTERA As Byte = 1

        Public Const DESFASE_ENCRIPTACION As Byte = 10

        'Constantes Login
        Public Const CLAVE_ENCRIPTADO_BD As String = "$$SOFTTOOLS$$"

        ' Tipos de naturalezas
        Public Const TINA_JURIDICA As String = "JURÍDICA"
        Public Const TINA_NATURAL As String = "NATURAL"
        Public Const CODIGO_TINA_JURIDICA As Short = 1
        Public Const CODIGO_TINA_NATURAL As Short = 2

        Public Const ITEM_VACIO As String = " "
        Public Const SELECIONE_UN_ITEM As String = "Seleccione un Item"

        ' Tipos de identificación
        Public Const TIID_NIT As String = "NIT"
        Public Const TIID_CEDULA As String = "CC"
        Public Const TIID_CEDULA_EXTRANJERIA As String = "CE"

        ' FECHA NULA DE SQL SERVER
        Public Const FECHA_NULA As String = "01/01/1900"

        ' Errores que se atrapan
        Public Const ERROR_LLAVE_DUPLICADA As String = "Violation of PRIMARY KEY constraint "
        Public Const CODIGO_ERROR_LLAVE_DUPLICADA As Short = 2627
        Public Const CODIGO_ERROR_INDICE_UNICO_REPETIDO As Short = 2601
        Public Const ERROR_INDICE_DUPLICADO As String = "Cannot insert duplicate key row in object " 'TABLE REFERENCE. The statement has been terminated."
        Public Const ERROR_INFRACCION_INTEGRIDAD_REFERENCIAL As String = "DELETE statement conflicted with TABLE REFERENCE constraint "
        Public Const CODIGO_ERROR_INFRACCION_INTEGRIDAD_REFERENCIAL As Short = 547
        Public Const CODIGO_ERROR_FECHA_FUERA_DE_RANGO As Short = 242
        Public Const ERROR_INDICE_UNICO_REPETIDO As String = "Se produjo un error al guardar, debido a que el índice único se encuentra repetido"
        Public Const ERROR_LLAVE_DUPLICADA_ As String = "Se produjo un error al guardar debido a que la llave se encuentra duplicada"
        Public Const ERROR_LLAVE_FORANEA_DESDE_OTRA_TABLA As String = "The DELETE statement conflicted with the REFERENCE constraint"

        ' Constante de estados de documentos
        Public Const ESTADO_BORRADOR As Byte = 0
        Public Const ESTADO_DEFINITIVO As Byte = 1
        Public Const ESTADO_LEGALIZADO As Byte = 2
        Public Const CODIGO_ANULADO As Byte = 2
        Public Const ESTADO_ANULADO As Byte = 1
        Public Const ESTADO_INACTIVO As Byte = 0
        Public Const CODIGO_INACTIVO As Byte = 2
        Public Const ESTADO_BLOQUEADO As Byte = 1
        Public Const CODIGO_BLOQUEADO As Byte = 2

        Public Const ESTADO_ACTIVO As Byte = 1
        Public Const ESTADO_CONFIRMADO As Byte = 4
        Public Const ESTADO_PLANIFICADO As Byte = 1
        ' Constantes de manejo de maestro Detalle
        Public Const HACIA_DEFINITIVA As Byte = 0
        Public Const HACIA_TEMPORAL As Byte = 1
        Public Const HACIA_TEMPORAL_INICIAL As Byte = 2
        Public Const HACIA_SUBTEMPORAL As Byte = 3
        Public Const DESDE_DEFINITIVA As Boolean = False
        Public Const DESDE_TEMPORAL As Boolean = True
        Public Const DESDE_SUBTEMPORAL As Byte = 3

        ' Constantes usadas en calculos
        Public Const FACTOR_CONVERSION_KILO_TONELADAS As Short = 1000
        Public Const FACTOR_CONVERSION_PORCENTAJE As Short = 100
        Public Const FACTOR_CONVERSION_X_MIL As Short = 1000
        Public Const FACTOR_BYTES_KILOBYTES As Short = 1024

        ' Constantes de Formateo para la presentacion
        'Public Const FORMATO_NUMERO_2_DECIMALES As String = "#,###.##"
        Public Const FORMATO_NUMERO_3_DECIMALES As String = "#,##0.000"
        Public Const FORMATO_NUMERO_4_DECIMALES As String = "#,##0.0000"
        Public Const FORMATO_NUMERO_2_DECIMALES As String = "#,##0.00"
        'Public Const FORMATO_NUMERO_0_DECIMALES As String = "#,000"
        Public Const FORMATO_NUMERO_0_DECIMALES As String = "#,##0"
        Public Const FORMATO_NUMERO_6_DECIMALES As String = "#,##0.000000"
        Public Const FORMATO_NUMERO_5_DECIMALES As String = "#,##0.00000"

        Public Const FORMATO_FECHA_DIA_MES_ANO As String = "dd/MM/yyyy"
        Public Const FORMATO_FECHA_ANO_MES_DIA As String = "yyyy/MM/dd"
        Public Const FORMATO_FECHA_MES_DIA_AÑO As String = "MM/dd/yyyy"
        Public Const FORMATO_HORA_MINUTO_SEGUNDO As String = "HH:mm:ss"
        Public Const FORMATO_HORA_MINUTO As String = "HH:mm"
        Public Const FORMATO_DOCE_HORAS As String = "hh:mm tt"
        Public Const MEDIA_HORA As Double = 0.5
        Public Const FORMATO_NUMERO_DOS_DIGITOS As String = "#0#"

        'Formatos máscara
        Public Const MASCARA_NUMERO_TELEFONICO As String = "9999999"
        Public Const MASCARA_NUMERO_CELULAR As String = "9999999999"

        ' Constantes del Message Box
        Public Const MOSTRAR_BOTON_ACEPTAR As Boolean = True
        Public Const OCULTAR_BOTON_ACEPTAR As Boolean = False
        Public Const MOSTRAR_BOTONES_SI_NO As Boolean = True
        Public Const OCULTAR_BOTONES_SI_NO As Boolean = False
        Public Const SELECCIONO_SI As Byte = 1
        Public Const SELECCIONO_NO As Byte = 2

        ' Constante de no aplica
        Public Const NO_APLICA As Byte = 0
        Public Const VALOR_DEFECTO_ENVIO As Byte = 1
        Public Const VALOR_MARCA_DEFECTO_ENVIO As Byte = 1
        Public Const PESO_VALOR_DEFECTO As Long = 2000
        Public Const PESO_VALOR_DEFECTO_CABEZOTE As Long = 16000

        ' Constantes numéricas
        Public Const CERO As Byte = 0
        Public Const UNO As Byte = 1
        Public Const DOS As Byte = 2
        Public Const MENOS_DOS As Integer = -2

        Public Const TRES As Byte = 3
        Public Const CUATRO As Byte = 4
        Public Const CINCO As Byte = 5
        Public Const SEIS As Byte = 6
        Public Const SIETE As Byte = 7
        Public Const OCHO As Byte = 8
        Public Const NUEVE As Byte = 9

        Public Const DIEZ As Byte = 10
        Public Const DOCE As Byte = 12
        Public Const TRECE As Byte = 13
        Public Const ONCE As Short = 11
        Public Const SESENTA As Short = 60
        Public Const VALOR_MONETARIO_POR_DEFECTO As Decimal = 0.0

        Public Const SEPARA_CORREO As Char = ","
        Public Const CERO_STRING As Char = "0"
        'Constante Etiqueta Numero documento
        Public Const ETIQUETA_DOCUMENTO_REMESA As String = "Remesa No."
        Public Const ETIQUETA_DOCUMENTO_MANIFIESTO As String = "Manifiesto No."
        Public Const ETIQUETA_DOCUMENTO_ORDEN_CARGUE As String = "Orden de Cargue No."

        ' Constantes de Equipos Propios
        Public Const TERCERO As Byte = 0
        Public Const PROPIO As Byte = 1
        Public Const SOCIO As Byte = 2

        ' Constante de Grupo Administrador
        Public Const GRUPO_ADMINISTRADOR As String = "ADMIN"
        Public Const GRUPO_SUPER_ADMINISTRADOR As String = "SADMIN"
        Public Const USUARIO_ADMINISTRADOR As String = "ADMIN"

        ' Constante de Grupo Facturación Bogotá
        Public Const GRUPO_FACTURACION As String = "AFACTURACION"

        ' Constante de Grupo Despacho Bogotá
        Public Const GRUPO_DESPACHO As String = "DESAPACHOS"
        Public Const GRUPO_DESPACHO_MULTIGLOBAL As String = "DESPA"
        Public Const GRUPO_TESORERIA_MULTIGLOBAL As String = "TESOR"

        ' Constante de Grupo Seguridad Bogotá
        Public Const GRUPO_SEGURIDAD As String = "SEGURIDAD"

        ' Constante de Grupo Movil
        Public Const GRUPO_MOVIL As String = "MOVIL"

        ' Constantes de Conexión BD
        Public Const NOMBRE_CONEXION As String = "GESTRANS"
        Public Const NOMBRE_CONEXION_DOCUMENTAL As String = "GESTRANS_DOCUMENTAL"

        Public Const LINEA_NOMBRE_CONEXION As Byte = 2
        Public Const NOMBRE_PARAMETRO_CONEXION As String = "Nombre Conexión"
        Public Const NOMBRE_USUARIO_CONEXION As String = "sa"
        Public Const PASSWORD_USUARIO_CONEXION As String = "admin"

        ' Constante del archivo de parametros
        Public Const RUTA_ARCHIVO_PARAMETROS As String = "\Parametros.txt"

        'Constantes de limitación de rangos filtro páginas Master
        Public Const DIFERENCIAS_FECHAS_MASTERS As Byte = 31
        Public Const DIFERENCIAS_FECHAS_MASTERS_UNMES As Byte = 31
        Public Const DIFERENCIAS_NUMERO_DOCUMENTOS_MASTERS As Byte = 60

        'TIPO MANIFIFESTO
        Public Const TIPO_MANIFIESTO_OTRA_EMPRESAS As Byte = 2
        Public Const TIPO_LINEA_NEGOCIO_NACIONAL_GUIAS As Integer = 20

        'TIPO Tarifa_Ruta_Contrato_Clientes
        Public Const TIPO_TARIFA_CONTENEDOR As Byte = 1

        'Codigo Faltantes en liquidacion mani
        Public Const CODIGO_FALTANTES As Byte = 9

        'Estado aprobación Contratos
        Public Const CONTRATO_POR_APROBAR As Byte = 0
        Public Const CONTRATO_APROBADO As Byte = 1
        Public Const CONTRATO_RECHAZADO As Byte = 2

        'Estado aprobación remesas
        Public Const REMESA_POR_APROBAR As Byte = 0
        Public Const REMESA_APROBADA As Byte = 1
        Public Const REMESA_RECHAZADA As Byte = 2

        ' Constantes de tipos de campos en BD
        Public Const CAMPO_NUMERICO As Byte = 1
        Public Const CAMPO_ALFANUMERICO As Byte = 2

        Public Const OPCION_NO As Byte = 0
        Public Const OPCION_SI As Byte = 1
        Public Const NO As String = "No"
        Public Const SI As String = "Si"

        'Estado Reportar Destino Seguro/Satrack
        Public Const REPORTAR As Byte = 1
        Public Const NO_REPORTAR As Byte = 3
        Public Const REPORTADO As Byte = 2

        'Terceros
        Public Const CODIGO_TERCERO_UNO As Short = 1
        Public Const CODIGO_TERCERO_SISTEMA As Short = 0

        Public Const LINK_NUEVO As String = "Nuevo "
        Public Const LINK_NUEVA As String = "Nueva "

        Public Const ANULACION As Boolean = True
        Public Const LONGITUD_PLACA_VEHICULO As Integer = 6
        Public Const LONGITUD_PLACA_REMOLQUE As Integer = 6
        Public Const LONGITUD_PLACA_TEMPORAL As Integer = 5
        Public Const LONGITUD_NUMERO_CELULAR As Integer = 10

        'Constantes para enviar correos
        Public Const IP_SOFTTOOLS_COM_CO As String = "smtp.softtools.com.co"
        Public Const PUERTO_SALIENTE_DE_CORREOS As Integer = 25

        Public Const CARGA_COMPLETA As Boolean = True
        Public Const CARGA_PARCIAL As Boolean = False

        'Constante cantidad carácteres de búsqueda
        Public Const CARACTERES_BUSQUEDA_NOMBRE As Integer = 5
        Public Const CARACTERES_CRITERIO_BUSQUEDA_POPUP As Integer = 3
        Public Const MINIMO_TRES_CARACTERES_INGRESO_NOMBRE As Byte = 3
        Public Const MINIMO_CUATRO_CARACTERES_INGRESO_NOMBRE As Byte = 4
        Public Const MINIMO_CARACTERES_CODIGO_ALTERNO As Byte = 3
        Public Const MINIMO_CARACTERES_NOMBRE_PRODUCTO As Byte = 3
        Public Const MINIMO_CARACTERES_UNIDADES_MEDIDA_EMPAQUE As Byte = 2
        Public Const MINIMO_CARACTERES_INGRESO_MARCAS As Byte = 3
        Public Const MAXIMO_ESPACIO_CARACTERES As Short = 1000
        Public Const ASCII_A As Byte = 65
        Public Const ASCII_Z As Byte = 90
        Public Const CARACTERES_MAXIMO_DIRECCION_RNDC_TERCERO As Integer = 50
        Public Const MINIMO_MESES_CERRE_OPERATIVO As Integer = 12

        'Generales
        Public Const VACIO As String = ""
        Public Const NUMERO_CARACTERES_EXTENSION_ARCHIVOS As Short = 3
        Public Const CODIGO_RUTA_DEFECTO As Integer = 999
        Public Const CODIGO_CIUDAD_DEFECTO As Integer = 999

        ' Constante de dias del mes
        Public Const TREINTA_DIAS As Integer = 30

        'Constante de sitio cargue/descargue

        Public Const CARGUE As Integer = 1
        Public Const DESCARGUE As Integer = 1

        'Configuración correo del Web.config
        Public Const MENSAJE_CORREO_NO_CONFIGURADO As String = "No se ha configurado una cuenta de correo para enviar avisos"

        'Constante de facturación de contrato
        Public Const CONTRATO_NO_FACTURADO As Integer = 0
        Public Const CONTRATO_FACTURADO As Integer = 1

        Public Const CARACTER_SALTO_LINEA As String = Chr(10)
        Public Const CARACTER_ENTER As String = Chr(13)
        Public Const CAMPO_CEROS_DERECHA As Integer = 5
        Public Const CAMPO_CEROS_IZQUIERDA As Integer = 6
        Public Const CAMPO_DECIMAL As Integer = 3
        Public Const CAMPO_FECHA As Integer = 4
        Public Const CAMPO_IDENTIFICACION_DIGITO_CHEQUEO As Integer = 7
        Public Const MAXIMO_TRANSPORTE_CONTENEDORES_20 As Byte = 2
        Public Const MAXIMO_TRANSPORTE_CONTENEDORES_40 As Byte = 1

        Public Const FILTRAR_X_FECHA As Short = 1
        Public Const NO_FILTRAR_X_FECHA As Short = 0

        ' Constantes de menu aplicaciones
        Public Const OPCION_LISTADO As Byte = 1

        ' Constantes de impresión
        Public Const IMPRESION_DIRECTA As Byte = 1
        Public Const IMPRESION_PDF As Byte = 2

        'Constante de Hora por defecto ministerio de transporte
        Public Const HORA_DEFECTO As String = "00:00"
        Public Const HORA_DEFECTO_30_MIN As String = "00:30"
        Public Const HORA_DEFECTO_60_MIN As String = "01:00"
        Public Const HORA_DEFECTO_90_MIN As String = "01:30"

        'Impuestas a los cuales se les aplica prorrateo
        Public Const PRORRATEO_RETE_ICA As Byte = 1
        Public Const PRORRATEO_RETE_FUENTE As Byte = 2
        Public Const PRORRATEO_RETE_CREE As Byte = 3

        'SUFIJOS CONCEPTOS PRORRATEO
        Public Const RETE_ICA As String = "ICA"
        Public Const RETE_FUENTE As String = "FUENTE"
        Public Const RETE_CREE As String = "CREE"

        Public Const REMESA As String = "REMESA"
        Public Const GUIA As String = "GUIA"

#End Region

#Region "Constantes Numero Errores"
        Public Const NUMERO_ERROR_INTEGRIDAD_REFERENCIAL As Byte = 5
#End Region

#Region "Constructor"

        Public Sub New()

        End Sub

#End Region

#Region "Propiedades"

        Public Property NombreConexion() As String
            Get
                Return Me.strNombreConexion
            End Get
            Set(ByVal value As String)
                Me.strNombreConexion = value
            End Set
        End Property

        Public Property CadenaDeConexionSQL() As String
            Get
                Me.strCadenaDeConexionSQL = ConfigurationManager.ConnectionStrings(General.NOMBRE_CONEXION).ConnectionString
                Return Me.strCadenaDeConexionSQL
            End Get
            Set(ByVal value As String)
                Me.strCadenaDeConexionSQL = value
            End Set
        End Property

        Public Property CadenaDeConexionDocumentalSQL() As String
            Get
                Me.strCadenaDeConexionDocumentalSQL = ConfigurationManager.ConnectionStrings(General.NOMBRE_CONEXION_DOCUMENTAL).ConnectionString
                Return Me.strCadenaDeConexionDocumentalSQL
            End Get
            Set(ByVal value As String)
                Me.strCadenaDeConexionDocumentalSQL = value
            End Set
        End Property

        Public Property ConexionSQL() As SqlConnection
            Get
                If IsNothing(Me.sqlConexion) Then
                    Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
                End If
                Return Me.sqlConexion
            End Get
            Set(ByVal value As SqlConnection)
                If IsNothing(Me.sqlConexion) Then
                    Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
                End If
                Me.sqlConexion = value
            End Set
        End Property

        Public Property ConexionDocumentalSQL() As SqlConnection
            Get
                If IsNothing(Me.sqlConexionDocumental) Then
                    Me.sqlConexionDocumental = New SqlConnection(Me.strCadenaDeConexionDocumentalSQL)
                End If
                Return Me.sqlConexionDocumental
            End Get
            Set(ByVal value As SqlConnection)
                If IsNothing(Me.sqlConexionDocumental) Then
                    Me.sqlConexionDocumental = New SqlConnection(Me.strCadenaDeConexionDocumentalSQL)
                End If
                Me.sqlConexionDocumental = value
            End Set
        End Property

        Public Property TransaccionSQL() As SqlTransaction
            Get
                Return Me.sqlTransaccion
            End Get
            Set(ByVal value As SqlTransaction)
                Me.sqlTransaccion = value
            End Set
        End Property

        Public Property ComandoSQL() As SqlCommand
            Get
                Return Me.sqlComando
            End Get
            Set(ByVal value As SqlCommand)
                Me.sqlComando = value
            End Set
        End Property

        Public Property ExcepcionSQL() As SqlException
            Get
                Return Me.sqlExcepcionSQL
            End Get
            Set(ByVal value As SqlException)
                Me.sqlExcepcionSQL = value
            End Set
        End Property

#End Region

#Region "Funciones Publicas"

        Public Function Encriptar_Clave_Usuario(ByVal Clave As String) As String
            Dim strAuxiliar As String = String.Empty
            Try
                For Each Carater In Clave
                    strAuxiliar += Chr(Asc(Carater) + DESFASE_ENCRIPTACION)
                Next
            Catch ex As Exception
                strAuxiliar = ex.Message
            End Try
            Return strAuxiliar
        End Function

        Public Function Desencriptar_Clave_Usuario(ByVal Clave As String) As String
            Dim strAuxiliar As String = String.Empty
            Try
                For Each Carater In Clave
                    strAuxiliar += Chr(Asc(Carater) - DESFASE_ENCRIPTACION)
                Next
            Catch ex As Exception
                strAuxiliar = ex.Message
            End Try
            Return strAuxiliar
        End Function

        Public Function Formatear_Fecha_SQL(ByVal strFecha As String) As String

            If Len(strFecha) = 0 Then
                Formatear_Fecha_SQL = "01/01/1900"
            Else
                Formatear_Fecha_SQL = Mid$(strFecha, 4, 2) & "/" & Mid$(strFecha, 1, 2) & "/" & Mid$(strFecha, 7, 4)
            End If

        End Function

        Public Function Formatear_Fecha_Hora_SQL(ByVal dteFecha As Date) As String
            Dim strFecha As String

            strFecha = dteFecha.Month & "/" & dteFecha.Day & "/" & dteFecha.Year & " "
            strFecha += Mid(dteFecha.ToString, 11)
            Formatear_Fecha_Hora_SQL = strFecha

        End Function

        Public Function Leer_Fecha_SQL(ByVal strFecha As String) As String

            If Len(strFecha) = 0 Then
                Leer_Fecha_SQL = ""
            ElseIf Mid$(strFecha, 1, 10) = "01/01/1900" Then
                Leer_Fecha_SQL = ""
            Else
                Leer_Fecha_SQL = Mid$(strFecha, 1, 10)
            End If

        End Function

        Public Function Fecha_Valida(ByVal strFecha As String) As Boolean
            Dim strTemp As String

            strTemp = strFecha

            If IsDate(strFecha) Then
                ' Validar el dia
                If Val(Mid$(strTemp, 1, 2)) >= 1 And Val(Mid$(strTemp, 1, 2)) <= 31 Then
                    ' Validar el mes
                    If Val(Mid$(strTemp, 4, 2)) >= 1 And Val(Mid$(strTemp, 4, 2)) <= 12 Then
                        ' Validar el año
                        If Val(Mid$(strTemp, 7, 4)) >= 1980 And Val(Mid$(strTemp, 7, 4)) <= 2030 Then
                            Fecha_Valida = True
                        Else
                            Fecha_Valida = False
                            Exit Function
                        End If
                    Else
                        Fecha_Valida = False
                        Exit Function
                    End If
                Else
                    Fecha_Valida = False
                    Exit Function
                End If
            Else
                Fecha_Valida = False
            End If

        End Function

        Public Function Separar_Correos(ByVal Correo As String, ByRef arrCorreos() As String) As Boolean
            Try
                Separar_Correos = True
                Dim intPosicion As Short
                intPosicion = 0
                Dim intCantidadCorreos As Short
                intCantidadCorreos = 0
                Dim intPosInicial As Short
                intPosInicial = 0
                Dim intIndice As Short
                intIndice = 0

                For intPosicion = 0 To Len(Correo) - 1 Step 1
                    If Correo.Chars(intPosicion) = SEPARA_CORREO Then
                        intCantidadCorreos += 1
                    End If
                Next

                ReDim arrCorreos(intCantidadCorreos)

                For intPosicion = 0 To Len(Correo) - 1 Step 1

                    If Correo.Chars(intPosicion) = SEPARA_CORREO Then
                        arrCorreos(intIndice) = LTrim(Correo.Substring(intPosInicial, intPosicion - intPosInicial))
                        intPosInicial = intPosicion + 1
                        intIndice += 1
                    End If

                    If intPosicion = Len(Correo) - 1 Then
                        arrCorreos(intIndice) = LTrim(Correo.Substring(intPosInicial, (intPosicion - intPosInicial) + General.UNO))
                    End If

                Next

            Catch ex As Exception
                Separar_Correos = False
            End Try
        End Function

        Public Function Email_Valido(ByVal Email As String) As Boolean
            Try
                'Valida Email por medio de expresiones regulares - código cambiado JAO - 24/05/2012

                Dim rgxPatronCorreo As Regex = New Regex("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
                Dim mthEvaluaCorreo As Match = rgxPatronCorreo.Match(Email)

                If mthEvaluaCorreo.Success Then
                    Email_Valido = True
                Else
                    Email_Valido = False
                End If
            Catch ex As Exception
                Email_Valido = False
            End Try
        End Function

        Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
            Dim daDataAdapter As SqlDataAdapter
            Dim dsDataSet As DataSet = New DataSet

            Try
                Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                    daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                    daDataAdapter.Fill(dsDataSet)
                    ConexionSQL.Close()
                End Using

            Catch ex As Exception
                strError = ex.Message.ToString()
            Finally
                If ConexionSQL.State() = ConnectionState.Open Then
                    ConexionSQL.Close()
                End If
            End Try

            Retorna_Dataset = dsDataSet

            Try
                dsDataSet.Dispose()
            Catch ex As Exception
                strError = ex.Message.ToString()
            End Try

        End Function

        Function Retorna_Dataset_Documental(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
            Dim daDataAdapter As SqlDataAdapter
            Dim dsDataSet As DataSet = New DataSet

            Try
                Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionDocumentalSQL)
                    daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                    daDataAdapter.Fill(dsDataSet)
                    ConexionSQL.Close()
                End Using

            Catch ex As Exception
                strError = ex.Message.ToString()
            Finally
                If ConexionSQL.State() = ConnectionState.Open Then
                    ConexionSQL.Close()
                End If
            End Try

            Retorna_Dataset_Documental = dsDataSet

            Try
                dsDataSet.Dispose()
            Catch ex As Exception
                strError = ex.Message.ToString()
            End Try

        End Function

        Function Retorna_Campo_BD(ByVal CodigoEmpresa As Integer, ByVal Tabla As String, ByVal CampoConsulta As String, ByVal CampoLlave As String, ByVal TipoLlave As Integer, ByVal ValorLlave As String, ByRef Resultado As String, Optional ByVal Condicion As String = "", Optional ByRef strError As String = "", Optional ByVal SobreNombreCampoConsulta As String = "") As Boolean
            Try
                Dim strSQL As String

                strSQL = "SELECT TOP 1 " & CampoConsulta & Chr(13)
                strSQL += " FROM " & Tabla & " WITH (NOLOCK) " & Chr(13)
                strSQL += " WHERE EMPR_Codigo = " & CodigoEmpresa & Chr(13)
                If TipoLlave = CAMPO_NUMERICO Then
                    strSQL += " AND " & CampoLlave & " = " & ValorLlave & Chr(13)
                ElseIf TipoLlave = CAMPO_ALFANUMERICO Then
                    strSQL += " AND " & CampoLlave & " = '" & ValorLlave & "'" & Chr(13)
                End If
                If Len(Trim(Condicion)) > 0 Then
                    strSQL += " AND " & Condicion
                End If

                If SobreNombreCampoConsulta = "" Then
                    SobreNombreCampoConsulta = CampoConsulta
                End If

                ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                Dim ComandoSQL As SqlCommand
                ComandoSQL = New SqlCommand(strSQL, ConexionSQL)
                ConexionSQL.Open()
                Dim sdrConsulta As SqlDataReader = ComandoSQL.ExecuteReader

                If sdrConsulta.Read Then
                    Resultado = sdrConsulta(SobreNombreCampoConsulta).ToString()
                    Retorna_Campo_BD = True
                Else
                    Resultado = General.CERO
                    Retorna_Campo_BD = False
                End If
                sdrConsulta.Close()
                sdrConsulta.Dispose()

                strError = ""

            Catch ex As Exception
                Retorna_Campo_BD = False
                Resultado = ""
                strError = ex.Message.ToString()
            Finally
                If ConexionSQL.State() = ConnectionState.Open Then
                    ConexionSQL.Close()
                End If
            End Try


        End Function

        Function Retorna_Campo_BD_Documental(ByVal CodigoEmpresa As Integer, ByVal Tabla As String, ByVal CampoConsulta As String, ByVal CampoLlave As String, ByVal TipoLlave As Integer, ByVal ValorLlave As String, ByRef Resultado As String, Optional ByVal Condicion As String = "", Optional ByRef strError As String = "", Optional ByVal SobreNombreCampoConsulta As String = "") As Boolean
            Try
                Dim strSQL As String

                strSQL = "SELECT TOP 1 " & CampoConsulta & Chr(13)
                strSQL += " FROM " & Tabla & " WITH (NOLOCK) " & Chr(13)
                strSQL += " WHERE EMPR_Codigo = " & CodigoEmpresa & Chr(13)
                If TipoLlave = CAMPO_NUMERICO Then
                    strSQL += " AND " & CampoLlave & " = " & ValorLlave & Chr(13)
                ElseIf TipoLlave = CAMPO_ALFANUMERICO Then
                    strSQL += " AND " & CampoLlave & " = '" & ValorLlave & "'" & Chr(13)
                End If
                If Len(Trim(Condicion)) > 0 Then
                    strSQL += " AND " & Condicion
                End If

                If SobreNombreCampoConsulta = "" Then
                    SobreNombreCampoConsulta = CampoConsulta
                End If

                ConexionSQL = New SqlConnection(Me.strCadenaDeConexionDocumentalSQL)
                Dim ComandoSQL As SqlCommand
                ComandoSQL = New SqlCommand(strSQL, ConexionDocumentalSQL)
                ConexionSQL.Open()
                Dim sdrConsulta As SqlDataReader = ComandoSQL.ExecuteReader

                If sdrConsulta.Read Then
                    Resultado = sdrConsulta(SobreNombreCampoConsulta).ToString()
                    Retorna_Campo_BD_Documental = True
                Else
                    Resultado = General.CERO
                    Retorna_Campo_BD_Documental = False
                End If
                sdrConsulta.Close()
                sdrConsulta.Dispose()

                strError = ""

            Catch ex As Exception
                Retorna_Campo_BD_Documental = False
                Resultado = ""
                strError = ex.Message.ToString()
            Finally
                If ConexionSQL.State() = ConnectionState.Open Then
                    ConexionSQL.Close()
                End If
            End Try
        End Function

        Function Retorna_Campo_Empresa(ByVal CodigoEmpresa As Integer, ByVal CampoConsulta As String, ByRef Resultado As String, Optional ByRef strError As String = "") As Boolean
            Try
                Dim strSQL As String

                strSQL = "SELECT " & CampoConsulta
                strSQL += " FROM Empresas"
                strSQL += " WHERE Codigo = " & CodigoEmpresa

                ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                Dim ComandoSQL As SqlCommand
                ComandoSQL = New SqlCommand(strSQL, ConexionSQL)
                ConexionSQL.Open()
                Dim sdrConsulta As SqlDataReader = ComandoSQL.ExecuteReader

                If sdrConsulta.Read Then
                    Resultado = sdrConsulta(CampoConsulta).ToString()
                    Retorna_Campo_Empresa = True
                Else
                    Resultado = ""
                    Retorna_Campo_Empresa = False
                End If
                sdrConsulta.Close()
                sdrConsulta.Dispose()

            Catch ex As Exception
                Retorna_Campo_Empresa = False
                Resultado = ""
                strError = ex.Message.ToString()
            Finally
                If ConexionSQL.State() = ConnectionState.Open Then
                    ConexionSQL.Close()
                End If
            End Try


        End Function

        Public Function Retorna_Campos(ByVal intCodigoEmpresa As Integer, ByVal strNombreTabla As String, ByVal arrCampos As String(), ByRef arrValoresCampos As String(), ByVal intNumeroCampos As Integer, ByVal intTipoLlave As Integer, ByVal strCampoLlave As String, ByVal strValorLlave As String, ByVal strCondicion As String, Optional ByRef strError As String = "") As Boolean

            Try
                Dim strSQL As String, intCont As Integer = 0

                strSQL = "SELECT "
                For intCont = 0 To intNumeroCampos - 1
                    If intCont = 0 Then
                        strSQL = strSQL & arrCampos(intCont)
                    Else
                        strSQL = strSQL & ", " & arrCampos(intCont)
                    End If
                Next
                strSQL = strSQL & Chr(10) & " FROM " & strNombreTabla
                strSQL = strSQL & Chr(10) & " WHERE EMPR_Codigo = " & intCodigoEmpresa

                If intTipoLlave = CAMPO_NUMERICO Then
                    strSQL += " AND " & strCampoLlave & " = " & strValorLlave & Chr(13)
                ElseIf intTipoLlave = CAMPO_ALFANUMERICO Then
                    strSQL += " AND " & strCampoLlave & " = '" & strValorLlave & "'" & Chr(13)
                End If
                If strCondicion <> "" Then
                    strSQL = strSQL & Chr(10) & " AND " & strCondicion
                End If

                ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                Dim ComandoSQL As SqlCommand
                ComandoSQL = New SqlCommand(strSQL, ConexionSQL)
                ConexionSQL.Open()

                Dim sdrConsulta As SqlDataReader = ComandoSQL.ExecuteReader

                If sdrConsulta.Read Then
                    ReDim arrValoresCampos(intNumeroCampos - 1)

                    For intCont = 0 To intNumeroCampos - 1
                        If IsDBNull(sdrConsulta(arrCampos(intCont).ToString)) Then
                            arrValoresCampos(intCont) = String.Empty
                        Else
                            arrValoresCampos(intCont) = sdrConsulta(arrCampos(intCont).ToString)
                        End If
                    Next
                    Retorna_Campos = True
                Else
                    Retorna_Campos = False
                End If
                sdrConsulta.Close()
                sdrConsulta.Dispose()

            Catch ex As Exception
                Retorna_Campos = False
                strError = ex.Message.ToString()
            Finally
                If ConexionSQL.State() = ConnectionState.Open Then
                    ConexionSQL.Close()
                End If
            End Try
        End Function


        Public Function Retorna_Campos_Documental(ByVal intCodigoEmpresa As Integer, ByVal strNombreTabla As String, ByVal arrCampos As String(), ByRef arrValoresCampos As String(), ByVal intNumeroCampos As Integer, ByVal intTipoLlave As Integer, ByVal strCampoLlave As String, ByVal strValorLlave As String, ByVal strCondicion As String, Optional ByRef strError As String = "") As Boolean

            Try
                Dim strSQL As String, intCont As Integer = 0

                strSQL = "SELECT "
                For intCont = 0 To intNumeroCampos - 1
                    If intCont = 0 Then
                        strSQL = strSQL & arrCampos(intCont)
                    Else
                        strSQL = strSQL & ", " & arrCampos(intCont)
                    End If
                Next
                strSQL = strSQL & Chr(10) & " FROM " & strNombreTabla
                strSQL = strSQL & Chr(10) & " WHERE EMPR_Codigo = " & intCodigoEmpresa

                If intTipoLlave = CAMPO_NUMERICO Then
                    strSQL += " AND " & strCampoLlave & " = " & strValorLlave & Chr(13)
                ElseIf intTipoLlave = CAMPO_ALFANUMERICO Then
                    strSQL += " AND " & strCampoLlave & " = '" & strValorLlave & "'" & Chr(13)
                End If
                If strCondicion <> "" Then
                    strSQL = strSQL & Chr(10) & " AND " & strCondicion
                End If

                ConexionSQL = New SqlConnection(Me.strCadenaDeConexionDocumentalSQL)
                Dim ComandoSQL As SqlCommand
                ComandoSQL = New SqlCommand(strSQL, ConexionSQL)
                ConexionSQL.Open()

                Dim sdrConsulta As SqlDataReader = ComandoSQL.ExecuteReader

                If sdrConsulta.Read Then
                    ReDim arrValoresCampos(intNumeroCampos - 1)

                    For intCont = 0 To intNumeroCampos - 1
                        If IsDBNull(sdrConsulta(arrCampos(intCont).ToString)) Then
                            arrValoresCampos(intCont) = String.Empty
                        Else
                            arrValoresCampos(intCont) = sdrConsulta(arrCampos(intCont).ToString)
                        End If
                    Next
                    Retorna_Campos_Documental = True
                Else
                    Retorna_Campos_Documental = False
                End If
                sdrConsulta.Close()
                sdrConsulta.Dispose()

            Catch ex As Exception
                Retorna_Campos_Documental = False
                strError = ex.Message.ToString()
            Finally
                If ConexionSQL.State() = ConnectionState.Open Then
                    ConexionSQL.Close()
                End If
            End Try
        End Function

        Public Function Retorna_Campos_Misma_Transaccion(ByVal intCodigoEmpresa As Integer, ByVal strNombreTabla As String, ByVal arrCampos As String(), ByRef arrValoresCampos As String(), ByVal intNumeroCampos As Integer, ByVal intTipoLlave As Integer, ByVal strCampoLlave As String, ByVal strValorLlave As String, ByVal strCondicion As String, Optional ByRef strError As String = "") As Boolean

            Try
                Dim strSQL As String, intCont As Integer = 0

                strSQL = "SELECT "
                For intCont = 0 To intNumeroCampos - 1
                    If intCont = 0 Then
                        strSQL = strSQL & arrCampos(intCont)
                    Else
                        strSQL = strSQL & ", " & arrCampos(intCont)
                    End If
                Next
                strSQL = strSQL & Chr(10) & " FROM " & strNombreTabla
                strSQL = strSQL & Chr(10) & " WHERE EMPR_Codigo = " & intCodigoEmpresa

                If intTipoLlave = CAMPO_NUMERICO Then
                    strSQL += " AND " & strCampoLlave & " = " & strValorLlave & Chr(13)
                ElseIf intTipoLlave = CAMPO_ALFANUMERICO Then
                    strSQL += " AND " & strCampoLlave & " = '" & strValorLlave & "'" & Chr(13)
                End If
                If strCondicion <> "" Then
                    strSQL = strSQL & Chr(10) & " AND " & strCondicion
                End If

                ComandoSQL.CommandType = CommandType.Text
                ComandoSQL.CommandText = strSQL

                Dim sdrConsulta As SqlDataReader = ComandoSQL.ExecuteReader

                If sdrConsulta.Read Then
                    ReDim arrValoresCampos(intNumeroCampos - 1)

                    For intCont = 0 To intNumeroCampos - 1
                        If IsDBNull(sdrConsulta(arrCampos(intCont).ToString)) Then
                            arrValoresCampos(intCont) = String.Empty
                        Else
                            arrValoresCampos(intCont) = sdrConsulta(arrCampos(intCont).ToString)
                        End If
                    Next
                    Retorna_Campos_Misma_Transaccion = True
                Else
                    Retorna_Campos_Misma_Transaccion = False
                End If
                sdrConsulta.Close()
                sdrConsulta.Dispose()

            Catch ex As Exception
                Retorna_Campos_Misma_Transaccion = False
                strError = ex.Message.ToString()
            End Try
        End Function


        Function Retorna_COUNT_SQL(ByVal strSQL As String, ByRef lonNumeRegi As Long, Optional strError As String = "") As Boolean
            Dim bolResultado As Boolean

            Try
                ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                Dim ComandoSQL As SqlCommand
                ComandoSQL = New SqlCommand(strSQL, ConexionSQL)
                ConexionSQL.Open()
                Dim sdrConsulta As SqlDataReader = ComandoSQL.ExecuteReader

                If sdrConsulta.Read Then
                    lonNumeRegi = sdrConsulta(0).ToString()
                    bolResultado = True
                Else
                    lonNumeRegi = 0
                    bolResultado = False
                End If
                sdrConsulta.Close()
                sdrConsulta.Dispose()

                strError = ""

            Catch ex As Exception
                bolResultado = False
                strError = ex.Message.ToString()
            Finally
                If ConexionSQL.State() = ConnectionState.Open Then
                    ConexionSQL.Close()
                End If
            End Try

            Return bolResultado

        End Function

        Public Function Valida_Digito_Chequeo(ByVal strNIT As String, ByRef intDigitoChequeo As Integer) As Boolean
            Try
                Dim intDigiCheq, intSumaDigi, j As Integer
                Dim intPesoEsta() As Integer = {3, 7, 13, 17, 19, 23, 29, 37, 41, 43, 47, 53, 59, 67, 71}

                intSumaDigi = 0

                If Len(strNIT) <= 15 Then
                    For i As Integer = Len(strNIT) To 1 Step -1
                        intSumaDigi = intSumaDigi + ((Val(Mid(strNIT, i, 1))) * intPesoEsta(j))
                        j = j + 1
                    Next i

                    intDigiCheq = intSumaDigi Mod 11

                    If intDigiCheq > 1 Then
                        intDigiCheq = 11 - intDigiCheq
                    End If

                    '   intDigitoChequeo = intDigiCheq

                    If intDigiCheq = intDigitoChequeo Then
                        Return True
                    Else
                        Return False
                    End If

                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try

        End Function

        Public Function Traducir_Error(ByVal Descripcion As String) As String

            Dim strError As String, strErrorNativo As String
            Dim intCont As Short

            For intCont = 1 To Len(Descripcion)
                If Mid(Descripcion, intCont, 1) = "'" Then
                    Exit For
                End If
            Next

            strErrorNativo = Mid(Descripcion, 1, intCont - 1)

            Select Case strErrorNativo
                Case General.ERROR_INDICE_DUPLICADO
                    strError = ERROR_INDICE_UNICO_REPETIDO
                Case General.ERROR_LLAVE_DUPLICADA
                    strError = "Se produjo un error al guardar debido a que la llave se encuentra duplicada"
                Case General.ERROR_INFRACCION_INTEGRIDAD_REFERENCIAL
                    strError = "Se produjo un error de integridad referencial al borrar el registro en la base de datos"
                Case ERROR_LLAVE_FORANEA_DESDE_OTRA_TABLA
                    strError = "No se puede eliminar el registro actual porque se encuentra relacionado con otra tabla"
                Case Else
                    strError = Descripcion
            End Select

            Traducir_Error = strError
        End Function

        Public Function Traducir_Error(ByVal Codigo As Integer) As String
            Dim strError As String

            Select Case Codigo
                Case General.CODIGO_ERROR_LLAVE_DUPLICADA
                    strError = "Se produjo un error al guardar debido a que el indice único se encuentra repetido"
                Case General.CODIGO_ERROR_LLAVE_DUPLICADA
                    strError = "Se produjo un error al guardar debido a que la llave se encuentra duplicada"
                Case General.CODIGO_ERROR_INFRACCION_INTEGRIDAD_REFERENCIAL
                    strError = "Se produjo un error de infracción de integridad referencial"
                Case General.CODIGO_ERROR_FECHA_FUERA_DE_RANGO
                    strError = "Se produjo al intentar ingresar una fecha fuera de rango"
                Case General.CODIGO_ERROR_INDICE_UNICO_REPETIDO
                    strError = "Se produjo un error al guardar debido a que el indice único se encuentra repetido"
                Case Else
                    strError = Codigo & " " & Me.ExcepcionSQL.Message
            End Select

            Traducir_Error = strError
        End Function

        Public Function Ejecutar_SQL(ByVal strSQL As String, Optional ByRef strError As String = "") As Boolean
            Dim ComandoSQL As New SqlCommand
            Dim lonRegiAfec As Long = 0

            Try
                Me.ConexionSQL.Open()

                ComandoSQL.CommandType = CommandType.Text
                ComandoSQL.CommandText = strSQL
                ComandoSQL.Connection = ConexionSQL

                lonRegiAfec = Val(ComandoSQL.ExecuteNonQuery().ToString)
                Ejecutar_SQL = True
            Catch ex As Exception
                strError = ex.Message
                Ejecutar_SQL = False
            Finally
                If ConexionSQL.State() = ConnectionState.Open Then
                    ConexionSQL.Close()
                End If
            End Try

            Return lonRegiAfec

        End Function

        Public Function Ejecutar_SQL_Documental(ByVal strSQL As String, Optional ByRef strError As String = "") As Boolean
            Dim ComandoSQL As New SqlCommand
            Dim lonRegiAfec As Long = 0

            Try
                Me.ConexionDocumentalSQL.Open()

                ComandoSQL.CommandType = CommandType.Text
                ComandoSQL.CommandText = strSQL
                ComandoSQL.Connection = ConexionDocumentalSQL

                lonRegiAfec = Val(ComandoSQL.ExecuteNonQuery().ToString)
                Ejecutar_SQL_Documental = True
            Catch ex As Exception
                strError = ex.Message
                Ejecutar_SQL_Documental = False
            Finally
                If ConexionDocumentalSQL.State() = ConnectionState.Open Then
                    ConexionDocumentalSQL.Close()
                End If
            End Try

            Return lonRegiAfec

        End Function

        Public Function Ejecutar_SQL_Misma_Transaccion(ByVal strSQL As String, Optional ByRef strError As String = "") As Boolean

            Dim lonRegiAfec As Long = 0

            Try
                'Me.ConexionSQL.Open()

                ComandoSQL.CommandType = CommandType.Text
                ComandoSQL.CommandText = strSQL

                lonRegiAfec = Val(ComandoSQL.ExecuteNonQuery().ToString)
                Ejecutar_SQL_Misma_Transaccion = True

            Catch ex As Exception
                strError = ex.Message
                Ejecutar_SQL_Misma_Transaccion = False

            Finally
                'If ConexionSQL.State() = ConnectionState.Open Then
                '    ConexionSQL.Close()
                'End If
            End Try

            Return lonRegiAfec

        End Function

        Public Function Formatear_Numero(ByVal Numero As String, ByVal Decimales As Integer) As String

            Dim strNumeroFormateado As String
            Dim intCont As Integer
            Dim intPosiPunt As Integer

            strNumeroFormateado = ""
            For intCont = 1 To Numero.Length

                If Mid(Numero, intCont, 1) = "." Then
                    strNumeroFormateado += Mid(Numero, 1, intCont - 1)
                    intPosiPunt = intCont
                    Exit For
                End If
            Next

            If Decimales > 0 Then
                strNumeroFormateado += Mid(Numero, intPosiPunt, Decimales + 1)
            End If

            Return strNumeroFormateado

        End Function

        Public Function Numero_A_Letras(ByVal value As Double) As String
            Dim lonNumero As Long = Math.Abs(value)
            value = lonNumero

            Select Case value
                Case 0 : Numero_A_Letras = "CERO"
                Case 1 : Numero_A_Letras = "UN"
                Case 2 : Numero_A_Letras = "DOS"
                Case 3 : Numero_A_Letras = "TRES"
                Case 4 : Numero_A_Letras = "CUATRO"
                Case 5 : Numero_A_Letras = "CINCO"
                Case 6 : Numero_A_Letras = "SEIS"
                Case 7 : Numero_A_Letras = "SIETE"
                Case 8 : Numero_A_Letras = "OCHO"
                Case 9 : Numero_A_Letras = "NUEVE"
                Case 10 : Numero_A_Letras = "DIEZ"
                Case 11 : Numero_A_Letras = "ONCE"
                Case 12 : Numero_A_Letras = "DOCE"
                Case 13 : Numero_A_Letras = "TRECE"
                Case 14 : Numero_A_Letras = "CATORCE"
                Case 15 : Numero_A_Letras = "QUINCE"

                Case Is < 20 : Numero_A_Letras = "DIECI" & Numero_A_Letras(value - 10)
                Case 20 : Numero_A_Letras = "VEINTE"
                Case Is < 30 : Numero_A_Letras = "VEINTI" & Numero_A_Letras(value - 20)

                Case 30 : Numero_A_Letras = "TREINTA"
                Case 40 : Numero_A_Letras = "CUARENTA"
                Case 50 : Numero_A_Letras = "CINCUENTA"
                Case 60 : Numero_A_Letras = "SESENTA"
                Case 70 : Numero_A_Letras = "SETENTA"
                Case 80 : Numero_A_Letras = "OCHENTA"
                Case 90 : Numero_A_Letras = "NOVENTA"

                Case Is < 100 : Numero_A_Letras = Numero_A_Letras(Int(value \ 10) * 10) & " Y " & Numero_A_Letras(value Mod 10)
                Case 100 : Numero_A_Letras = "CIEN"
                Case Is < 200 : Numero_A_Letras = "CIENTO " & Numero_A_Letras(value - 100)
                Case 200, 300, 400, 600, 800 : Numero_A_Letras = Numero_A_Letras(Int(value \ 100)) & "CIENTOS"
                Case 500 : Numero_A_Letras = "QUINIENTOS"
                Case 700 : Numero_A_Letras = "SETECIENTOS"
                Case 900 : Numero_A_Letras = "NOVECIENTOS"

                Case Is < 1000 : Numero_A_Letras = Numero_A_Letras(Int(value \ 100) * 100) & " " & Numero_A_Letras(value Mod 100)
                Case 1000 : Numero_A_Letras = "MIL"
                Case Is < 2000 : Numero_A_Letras = "MIL " & Numero_A_Letras(value Mod 1000)

                Case Is < 1000000 : Numero_A_Letras = Numero_A_Letras(Int(value \ 1000)) & " MIL"
                    If value Mod 1000 Then Numero_A_Letras = Numero_A_Letras & " " & Numero_A_Letras(value Mod 1000)
                Case 1000000 : Numero_A_Letras = "UN MILLON"

                Case Is < 2000000 : Numero_A_Letras = "UN MILLON " & Numero_A_Letras(value Mod 1000000)
                Case Is < 1000000000000.0# : Numero_A_Letras = Numero_A_Letras(Int(value / 1000000)) & " MILLONES "
                    If (value - Int(value / 1000000) * 1000000) Then Numero_A_Letras = Numero_A_Letras & " " & Numero_A_Letras(value - Int(value / 1000000) * 1000000)

                Case 1000000000000.0# : Numero_A_Letras = "UN BILLON"
                Case Is < 2000000000000.0# : Numero_A_Letras = "UN BILLON " & Numero_A_Letras(value - Int(value / 1000000000000.0#) * 1000000000000.0#)

                Case Else : Numero_A_Letras = Numero_A_Letras(Int(value / 1000000000000.0#)) & " BILLONES"
                    If (value - Int(value / 1000000000000.0#) * 1000000000000.0#) Then Numero_A_Letras = Numero_A_Letras & " " & Numero_A_Letras(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
            End Select
        End Function

        Public Function Redondear_X_Exceso(ByVal Valor As Double) As Integer
            Dim intAuxiliar As Integer
            Dim dblAuxiliar As Double

            intAuxiliar = CInt(Valor)
            Double.TryParse(intAuxiliar.ToString, dblAuxiliar)

            If Valor > dblAuxiliar Then
                Redondear_X_Exceso = intAuxiliar + 1
            Else
                Redondear_X_Exceso = intAuxiliar
            End If

        End Function

        Public Function Calcular_Peso_Flete(ByVal PesoOriginal As Double) As Long

            Dim lonAuxiliar As Long

            lonAuxiliar = Redondear_X_Exceso(PesoOriginal / General.FACTOR_CONVERSION_KILO_TONELADAS)
            lonAuxiliar = lonAuxiliar * General.FACTOR_CONVERSION_KILO_TONELADAS
            Calcular_Peso_Flete = lonAuxiliar

        End Function

        Public Function Leer_Linea_Archivo_Plano(ByVal strRutaArchivo As String, ByVal intLinea As Integer) As String
            Dim sreLector As StreamReader
            Dim strLinea As String
            Dim intCont As Integer

            Try
                sreLector = New StreamReader(strRutaArchivo)
                intCont = 0
                strLinea = ""
                Do
                    strLinea = sreLector.ReadLine()
                    intCont += 1
                Loop Until intCont = intLinea
                sreLector.Close()

                Leer_Linea_Archivo_Plano = strLinea

            Catch ex As Exception
                Leer_Linea_Archivo_Plano = "No se pudo leer el archivo Error: " & ex.Message
            End Try

        End Function

        Public Function Leer_Parametro_Archivo_Plano(ByVal strRutaArchivo As String, ByVal strParametro As String) As String
            Dim sreLector As StreamReader
            Dim strLinea As String
            Dim intLinea As Integer
            Dim intCont As Integer

            Try
                sreLector = New StreamReader(strRutaArchivo)
                intCont = 0
                strLinea = ""
                intLinea = 50
                Do
                    strLinea = sreLector.ReadLine()
                    intCont += 1
                    If strParametro = Mid(strParametro, 1, Len(strLinea) - 1) Then
                        intLinea = intCont + 1
                    End If
                Loop Until intCont = intLinea
                sreLector.Close()
                Leer_Parametro_Archivo_Plano = strLinea

            Catch ex As Exception
                Leer_Parametro_Archivo_Plano = "No se pudo leer el archivo Error: " & ex.Message
            End Try

        End Function

        Public Function Retorna_Numero_Sin_Formato(ByVal NumeroFormateado As String) As Double
            Dim strAuxiliar As String = ""

            strAuxiliar = NumeroFormateado
            strAuxiliar = Replace(strAuxiliar, ",", "")
            Retorna_Numero_Sin_Formato = Val(strAuxiliar)

        End Function

        Public Function FormCampMiniTran(ByVal strValor As String, ByVal intLongitud As Integer, ByVal intTipoCampo As Integer, Optional ByVal intPresicion As Integer = 0) As String
            Dim intCon%, strFormato$, intPos%, strValorEntero$, strValorDecimal$, intLon%, strSignoNegativo$, intLongSignoNegativo%
            FormCampMiniTran = String.Empty

            strValor = strValor.Replace(CARACTER_SALTO_LINEA, "")
            strValor = strValor.Replace(CARACTER_ENTER, "")

            strFormato = ""
            strSignoNegativo = ""
            strValor = Trim$(strValor)

            If intTipoCampo = CAMPO_CEROS_DERECHA Then
                strValor = Trim$(strValor)
                intLon = Len(strValor)

                For intCon = intLon To intLongitud - 1
                    strValor = strValor & "0"
                Next
                FormCampMiniTran = strValor

            ElseIf intTipoCampo = CAMPO_CEROS_IZQUIERDA Then
                strValor = Trim$(strValor)
                intLon = Len(strValor)

                For intCon = intLon To intLongitud - 1
                    strValor = "0" & strValor
                Next
                FormCampMiniTran = strValor

            ElseIf intTipoCampo = CAMPO_NUMERICO Then

                ' Si es vacio
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    FormCampMiniTran = strFormato

                Else
                    ' Buscar signo negativo
                    intPos = InStr(strValor, "-")
                    If intPos > 0 Then
                        strSignoNegativo = "-"
                        intLongSignoNegativo = 1
                        strValor = Mid$(strValor, 2)
                    End If

                    For intCon = 1 To intLongitud - intLongSignoNegativo
                        strFormato = strFormato & "0"
                    Next

                    FormCampMiniTran = strSignoNegativo & Format$(Val(strValor), strFormato)

                End If
            ElseIf intTipoCampo = CAMPO_IDENTIFICACION_DIGITO_CHEQUEO Then
                strValor = Replace(strValor, ".", "")
                strValor = Replace(strValor, "-", "")
                intLon = Len(strValor)

                For intCon = intLon To intLongitud - 1
                    strValor = "0" & strValor
                Next
                FormCampMiniTran = strValor

            ElseIf intTipoCampo = CAMPO_ALFANUMERICO Then

                If Len(strValor) > intLongitud Then
                    strValor = Mid$(strValor, 1, intLongitud)
                End If
                FormCampMiniTran = strValor & Space$(intLongitud - Len(strValor))


            ElseIf intTipoCampo = CAMPO_DECIMAL Then

                ' Si es vacio
                If strValor = "" Then
                    For intCon = 1 To intLongitud
                        strFormato = strFormato & "0"
                    Next
                    FormCampMiniTran = strFormato

                Else

                    ' Buscar signo negativo
                    intPos = InStr(strValor, "-")
                    If intPos > 0 Then
                        strSignoNegativo = "-"
                        intLongSignoNegativo = 1
                        strValor = Mid$(strValor, 2)
                    End If

                    ' Buscar signo decimal
                    intPos = InStr(strValor, ".")
                    If intPos > 0 Then
                        strValorEntero = Mid$(strValor, 1, intPos - 1)
                        strValorDecimal = Mid$(strValor, intPos + 1, intPresicion)
                    Else
                        strValorEntero = strValor
                        strValorDecimal = ""
                    End If
                    strValorEntero = Trim$(strValorEntero)
                    strValorDecimal = Trim$(strValorDecimal)

                    ' Formatear Entero
                    For intCon = 1 To intLongitud - intPresicion - intLongSignoNegativo
                        strFormato = strFormato & "0"
                    Next

                    If strValorEntero = "" Then
                        strValorEntero = strFormato
                    Else
                        strValorEntero = Format$(Val(strValorEntero), strFormato)
                    End If

                    ' Formatear Decimal
                    strFormato = ""
                    intLon = Len(strValorDecimal)
                    If intLon < intPresicion Then
                        ' Si la longitud del valor decimal es menor a la precisión se añaden ceros a la derecha hasta cumplir el tamaño de la precision
                        For intCon = intLon To intPresicion - 1
                            strValorDecimal = strValorDecimal & "0"
                        Next

                    ElseIf intLon = 0 Then
                        ' Si la longitud es 0 se llena el valor decimal con ceros
                        For intCon = 0 To intPresicion - 1
                            strValorDecimal = strValorDecimal & "0"
                        Next

                    End If

                    FormCampMiniTran = strSignoNegativo & strValorEntero & "." & strValorDecimal

                End If

            ElseIf intTipoCampo = CAMPO_FECHA Then
                FormCampMiniTran = Format$(CDate(strValor), "yyyy/MM/dd")
                FormCampMiniTran = Replace(FormCampMiniTran, "/", "")
            End If

            Return FormCampMiniTran

        End Function

        Public Function Obtener_Mensaje_XML(ByVal strMensaje As String) As String
            Obtener_Mensaje_XML = String.Empty

            Dim xmlRespuesta As New XmlDocument
            xmlRespuesta.LoadXml(strMensaje)
            Dim xmlElementoAuxiliar As XmlNodeList = xmlRespuesta.GetElementsByTagName("ErrorMSG")
            If Not IsNothing(xmlElementoAuxiliar(CERO)) Then
                Obtener_Mensaje_XML = xmlElementoAuxiliar(CERO).InnerText
            Else
                xmlElementoAuxiliar = xmlRespuesta.GetElementsByTagName("ingresoid")
                If Not IsNothing(xmlElementoAuxiliar(CERO)) Then
                    Obtener_Mensaje_XML = xmlElementoAuxiliar(CERO).InnerText
                End If
            End If

            Return Obtener_Mensaje_XML

        End Function

        Public Function Retorna_Xml_Auditoria_Desde_Parametros_SP(ByVal arrParametros As SqlParameterCollection, ByVal NombreTabla As String, ByRef MensajeError As String) As String

            Try
                Dim objGeneral As New General
                Dim xmlDocumento As New XmlDocument
                Dim xmlElementoRoot As XmlElement

                Dim xmlElemento As XmlElement
                Dim txtNodo As XmlText

                xmlElementoRoot = xmlDocumento.CreateElement(NombreTabla)
                xmlDocumento.AppendChild(xmlElementoRoot)

                For Each Item As SqlParameter In arrParametros
                    xmlElemento = xmlDocumento.CreateElement(Replace(Item.ParameterName.Trim, "@par_", ""))
                    If IsDBNull(Item.Value) Or IsNothing(Item.Value) Then
                        Item.Value = ""
                    End If
                    txtNodo = xmlDocumento.CreateTextNode(Item.Value)

                    xmlElemento.AppendChild(txtNodo)
                    xmlElementoRoot.AppendChild(xmlElemento)
                Next

                Return objGeneral.Formatear_XML(xmlDocumento.InnerXml)

            Catch ex As Exception
                Retorna_Xml_Auditoria_Desde_Parametros_SP = ""
                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

                Try
                    Me.ExcepcionSQL = ex
                    MensajeError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.Traducir_Error(Me.ExcepcionSQL.Number)
                Catch Exc As Exception
                    MensajeError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.Traducir_Error(ex.Message)
                End Try

            End Try
        End Function

        Public Function Formatear_XML(Texto As String) As String

            Dim xmlDocumento As New Xml.XmlDocument
            xmlDocumento.LoadXml(Texto)

            Dim objConstructorString As New StringBuilder
            Dim objEscritorString As New StringWriter(objConstructorString)

            Dim objTextoEscritoXml As Xml.XmlTextWriter = Nothing

            Try

                objTextoEscritoXml = New Xml.XmlTextWriter(objEscritorString)
                objTextoEscritoXml.Formatting = Xml.Formatting.Indented
                xmlDocumento.WriteTo(objTextoEscritoXml)

            Finally

                If objEscritorString IsNot Nothing Then objEscritorString.Close()

            End Try

            Return objConstructorString.ToString()

        End Function

        'Public Function Mostrar_Mensaje(ByVal Mensaje As String, ByVal tmTipoMensa As Tipo_Mensaje, ByVal Titulo As String, Optional ByVal DirrecionURL As String = Nothing, Optional ByVal strControles As String = Nothing, Optional ByVal VentanaNueva As Modo_Ventana = 0) As String
        '    Dim strUrl As String = String.Empty
        '    If Not IsNothing(DirrecionURL) Then
        '        strUrl = DirrecionURL
        '    End If
        '    Dim strCont As String = String.Empty
        '    If Not IsNothing(strControles) Then
        '        strCont = strControles
        '    End If

        '    Return "Mostrar_mensaje('" & Mensaje & "'," & Val(tmTipoMensa) & ",'" & Titulo & "','" & DirrecionURL & "','" & strCont & "','" & VentanaNueva & "');"
        'End Function

#End Region

    End Class
End Namespace