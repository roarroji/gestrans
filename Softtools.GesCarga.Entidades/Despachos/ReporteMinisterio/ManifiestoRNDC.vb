﻿Imports Newtonsoft.Json
Namespace Despachos.Procesos
    <JsonObject>
    Public NotInheritable Class ManifiestoRNDC
        Inherits BaseDocumento
        Sub New()
        End Sub
        Sub New(lector As IDataReader)
            OpcionReporte = Read(lector, "OpcionReporte")
            NUMNITEMPRESATRANSPORTE = Read(lector, "NUMNITEMPRESATRANSPORTE")
            Tenedor = New TerceroRNDC With {
                        .NUMNITEMPRESATRANSPORTE = Read(lector, "NUMNITEMPRESATRANSPORTE"),
                        .CODTIPOIDTERCERO = Read(lector, "TENE_CODTIPOIDTERCERO"),
                        .DIGITOCHEQUEO = Read(lector, "REMI_DIGITOCHEQUEO"),
                        .NUMIDTERCERO = Read(lector, "TENE_NUMIDTERCERO"),
                        .NOMIDTERCERO = Read(lector, "TENE_NOMIDTERCERO"),
                        .PRIMERAPELLIDOIDTERCERO = Read(lector, "TENE_PRIMERAPELLIDOIDTERCERO"),
                        .SEGUNDOAPELLIDOIDTERCERO = Read(lector, "TENE_SEGUNDOAPELLIDOIDTERCERO"),
                        .CODSEDETERCERO = Read(lector, "TENE_CODSEDETERCERO"),
                        .NOMSEDETERCERO = Read(lector, "TENE_NOMSEDETERCERO"),
                        .NUMTELEFONOCONTACTO = Read(lector, "TENE_NUMTELEFONOCONTACTO"),
                        .NUMCELULARPERSONA = Read(lector, "TENE_NUMCELULARPERSONA"),
                        .NOMENCLATURADIRECCION = Read(lector, "TENE_NOMENCLATURADIRECCION"),
                        .CODMUNICIPIORNDC = Read(lector, "TENE_CODSEDETERCERO"),
                        .Reportar_RNDC = Read(lector, "TENE_Reportar_RNDC"),
                        .Codigo = Read(lector, "TENE_Codigo")
                    }
            Propietario = New TerceroRNDC With {
              .NUMNITEMPRESATRANSPORTE = Read(lector, "NUMNITEMPRESATRANSPORTE"),
              .CODTIPOIDTERCERO = Read(lector, "PROP_CODTIPOIDTERCERO"),
              .DIGITOCHEQUEO = Read(lector, "REMI_DIGITOCHEQUEO"),
              .NUMIDTERCERO = Read(lector, "PROP_NUMIDTERCERO"),
              .NOMIDTERCERO = Read(lector, "PROP_NOMIDTERCERO"),
              .PRIMERAPELLIDOIDTERCERO = Read(lector, "PROP_PRIMERAPELLIDOIDTERCERO"),
              .SEGUNDOAPELLIDOIDTERCERO = Read(lector, "PROP_SEGUNDOAPELLIDOIDTERCERO"),
              .CODSEDETERCERO = Read(lector, "PROP_CODSEDETERCERO"),
              .NOMSEDETERCERO = Read(lector, "PROP_NOMSEDETERCERO"),
              .NUMTELEFONOCONTACTO = Read(lector, "PROP_NUMTELEFONOCONTACTO"),
              .NUMCELULARPERSONA = Read(lector, "PROP_NUMCELULARPERSONA"),
              .NOMENCLATURADIRECCION = Read(lector, "PROP_NOMENCLATURADIRECCION"),
              .CODMUNICIPIORNDC = Read(lector, "PROP_CODSEDETERCERO"),
                        .Reportar_RNDC = Read(lector, "PROP_Reportar_RNDC"),
                        .Codigo = Read(lector, "PROP_Codigo")
          }
            Conductor = New TerceroRNDC With {
             .NUMNITEMPRESATRANSPORTE = Read(lector, "NUMNITEMPRESATRANSPORTE"),
             .CODTIPOIDTERCERO = Read(lector, "COND_CODTIPOIDTERCERO"),
             .DIGITOCHEQUEO = Read(lector, "REMI_DIGITOCHEQUEO"),
             .NUMIDTERCERO = Read(lector, "COND_NUMIDTERCERO"),
             .NOMIDTERCERO = Read(lector, "COND_NOMIDTERCERO"),
             .PRIMERAPELLIDOIDTERCERO = Read(lector, "COND_PRIMERAPELLIDOIDTERCERO"),
             .SEGUNDOAPELLIDOIDTERCERO = Read(lector, "COND_SEGUNDOAPELLIDOIDTERCERO"),
             .CODSEDETERCERO = Read(lector, "COND_CODSEDETERCERO"),
             .NOMSEDETERCERO = Read(lector, "COND_NOMSEDETERCERO"),
             .NUMTELEFONOCONTACTO = Read(lector, "COND_NUMTELEFONOCONTACTO"),
             .NUMCELULARPERSONA = Read(lector, "COND_NUMCELULARPERSONA"),
             .NOMENCLATURADIRECCION = Read(lector, "COND_NOMENCLATURADIRECCION"),
             .CODMUNICIPIORNDC = Read(lector, "COND_CODSEDETERCERO"),
             .NUMLICENCIACONDUCCION = Read(lector, "COND_NUMLICENCIACONDUCCION"),
             .CODCATEGORIALICENCIACONDUCCION = Read(lector, "COND_CODCATEGORIALICENCIACONDUCCION"),
             .FECHAVENCIMIENTOLICENCIA = Read(lector, "FECHAVENCIMIENTOLICENCIA"),
                        .Reportar_RNDC = Read(lector, "COND_Reportar_RNDC"),
                        .Codigo = Read(lector, "COND_Codigo")
         }
            Vehiculo = New VehiculoRNDC With {
             .NUMNITEMPRESATRANSPORTE = Read(lector, "NUMNITEMPRESATRANSPORTE"),
             .NUMPLACA = Read(lector, "NUMPLACA"),
             .CODCONFIGURACIONUNIDADCARGA = Read(lector, "CODCONFIGURACIONUNIDADCARGA"),
             .CODMARCAVEHICULOCARGA = Read(lector, "CODMARCAVEHICULOCARGA"),
             .CODLINEAVEHICULOCARGA = Read(lector, "CODLINEAVEHICULOCARGA"),
             .ANOFABRICACIONVEHICULOCARGA = Read(lector, "ANOFABRICACIONVEHICULOCARGA"),
             .CODTIPOIDPROPIETARIO = Read(lector, "PROP_CODTIPOIDTERCERO"),
             .NUMIDPROPIETARIO = Read(lector, "PROP_NUMIDTERCERO"),
             .CODTIPOIDTENEDOR = Read(lector, "TENE_CODTIPOIDTERCERO"),
             .NUMIDTENEDOR = Read(lector, "TENE_NUMIDTERCERO"),
             .PESOVEHICULOVACIO = Read(lector, "PESOVEHICULOVACIO"),
             .UNIDADMEDIDACAPACIDAD = Read(lector, "UNIDADMEDIDACAPACIDAD"),
             .CODCOLORVEHICULOCARGA = Read(lector, "CODCOLORVEHICULOCARGA"),
             .CODTIPOCARROCERIA = Read(lector, "CODTIPOCARROCERIA"),
             .NUMCHASIS = Read(lector, "NUMCHASIS"),
             .CAPACIDADUNIDADCARGA = Read(lector, "CAPACIDADUNIDADCARGA"),
             .NUMSEGUROSOAT = Read(lector, "NUMSEGUROSOAT"),
             .FECHAVENCIMIENTOSOAT = Read(lector, "FECHAVENCIMIENTOSOAT"),
             .NUMNITASEGURADORASOAT = Read(lector, "NUMNITASEGURADORASOAT"),
                        .Reportar_RNDC = Read(lector, "VEHI_Reportar_RNDC"),
                        .Codigo = Read(lector, "VEHI_Codigo")
         }
            Semirremolque = New VehiculoRNDC With {
             .NUMNITEMPRESATRANSPORTE = Read(lector, "NUMNITEMPRESATRANSPORTE"),
             .NUMPLACA = Read(lector, "NUMPLACAREMOLQUE"),
             .CODCONFIGURACIONUNIDADCARGA = Read(lector, "CODCONFIGURACIONUNIDADCARGA"),
             .CODMARCAVEHICULOCARGA = Read(lector, "CODMARCAVEHICULOCARGA"),
             .CODLINEAVEHICULOCARGA = Read(lector, "CODLINEAVEHICULOCARGA"),
             .ANOFABRICACIONVEHICULOCARGA = Read(lector, "ANOFABRICACIONVEHICULOCARGA"),
             .CODTIPOIDPROPIETARIO = Read(lector, "PROP_CODTIPOIDTERCERO"),
             .NUMIDPROPIETARIO = Read(lector, "PROP_NUMIDTERCERO"),
             .CODTIPOIDTENEDOR = Read(lector, "TENE_CODTIPOIDTERCERO"),
             .NUMIDTENEDOR = Read(lector, "TENE_NUMIDTERCERO"),
             .PESOVEHICULOVACIO = Read(lector, "PESOSEMIRREMOLQUEVACIO"),
             .UNIDADMEDIDACAPACIDAD = Read(lector, "UNIDADMEDIDACAPACIDAD"),
             .CODCOLORVEHICULOCARGA = Read(lector, "CODCOLORVEHICULOCARGA"),
             .CODTIPOCARROCERIA = Read(lector, "CODTIPOCARROCERIA"),
             .NUMCHASIS = Read(lector, "NUMCHASIS"),
             .CAPACIDADUNIDADCARGA = Read(lector, "CAPACIDADUNIDADCARGA"),
             .NUMSEGUROSOAT = Read(lector, "NUMSEGUROSOAT"),
             .FECHAVENCIMIENTOSOAT = Read(lector, "FECHAVENCIMIENTOSOAT"),
             .NUMNITASEGURADORASOAT = Read(lector, "NUMNITASEGURADORASOAT"),
             .Reportar_RNDC = Read(lector, "SEMI_Reportar_RNDC"),
             .Codigo = Read(lector, "SEMI_Codigo")
         }
            NUMMANIFIESTOCARGA = Read(lector, "NUMMANIFIESTOCARGA")
            CONSECUTIVOINFORMACIONVIAJE = Read(lector, "CONSECUTIVOINFORMACIONVIAJE")
            MANNROMANIFIESTOTRANSBORDO = Read(lector, "MANNROMANIFIESTOTRANSBORDO")
            NUMMANIFIESTOCARGATRANSBORDO = Read(lector, "NUMMANIFIESTOCARGATRANSBORDO")
            CODOPERACIONTRANSPORTE = Read(lector, "CODOPERACIONTRANSPORTE")
            VIAJESDIA = Read(lector, "VIAJESDIA")
            FECHAEXPEDICIONMANIFIESTO = Read(lector, "FECHAEXPEDICIONMANIFIESTO")
            CODMUNICIPIOORIGENMANIFIESTO = Read(lector, "CODMUNICIPIOORIGENMANIFIESTO")
            CODMUNICIPIODESTINOMANIFIESTO = Read(lector, "CODMUNICIPIODESTINOMANIFIESTO")
            ACEPTACIONELECTRONICA = Read(lector, "ACEPTACIONELECTRONICA")

            CODIDTITULARMANIFIESTO = Read(lector, "TENE_CODTIPOIDTERCERO")
            NUMIDTITULARMANIFIESTO = Read(lector, "TENE_NUMIDTERCERO")
            NUMPLACA = Read(lector, "NUMPLACA")
            NUMPLACAREMOLQUE = Read(lector, "NUMPLACAREMOLQUE")
            CODIDCONDUCTOR = Read(lector, "COND_CODTIPOIDTERCERO")
            NUMIDCONDUCTOR = Read(lector, "COND_NUMIDTERCERO")
            CODIDCONDUCTOR2 = Read(lector, "COND_CODTIPOIDTERCERO2")
            NUMIDCONDUCTOR2 = Read(lector, "COND_NUMIDTERCERO2")
            VALORFLETEPACTADOVIAJE = Read(lector, "VALORFLETEPACTADOVIAJE")
            RETENCIONFUENTEMANIFIESTO = Read(lector, "RETENCIONFUENTEMANIFIESTO")
            RETENCIONICAMANIFIESTOCARGA = Read(lector, "RETENCIONICAMANIFIESTOCARGA")
            VALORANTICIPOMANIFIESTO = Read(lector, "VALORANTICIPOMANIFIESTO")
            CODMUNICIPIOPAGOSALDO = Read(lector, "CODMUNICIPIOPAGOSALDO")
            FECHAPAGOSALDOMANIFIESTO = Read(lector, "FECHAPAGOSALDOMANIFIESTO")
            CODRESPONSABLEPAGOCARGUE = Read(lector, "CODRESPONSABLEPAGOCARGUE")
            CODRESPONSABLEPAGODESCARGUE = Read(lector, "CODRESPONSABLEPAGODESCARGUE")
            OBSERVACIONES = Read(lector, "OBSERVACIONES")
            FECHAENTREGADOCUMENTOS = Read(lector, "FECHAENTREGADOCUMENTOS")
            VALORADICIONALHORASCARGUE = Read(lector, "VALORADICIONALHORASCARGUE")
            VALORADICIONALHORASDESCARGUE = Read(lector, "VALORADICIONALHORASDESCARGUE")
            'SUSPENSION VIAJE
            TIPOCUMPLIDOMANIFIESTO = Read(lector, "TIPOCUMPLIDOMANIFIESTO")
            MOTIVOSUSPENSIONMANIFIESTO = Read(lector, "MOTIVOSUSPENSIONMANIFIESTO")
            CONSECUENCIASUSPENSIONMANIFIESTO = Read(lector, "CONSECUENCIASUSPENSIONMANIFIESTO")
            VALORDESCUENTOFLETE = Read(lector, "VALORDESCUENTOFLETE")
            MOTIVOVALORDESCUENTOMANIFIESTO = Read(lector, "MOTIVOVALORDESCUENTOMANIFIESTO")
        End Sub

        <JsonProperty>
        Public Property NUMNITEMPRESATRANSPORTE As String
        <JsonProperty>
        Public Property NUMMANIFIESTOCARGA As String
        <JsonProperty>
        Public Property CONSECUTIVOINFORMACIONVIAJE As String
        <JsonProperty>
        Public Property MANNROMANIFIESTOTRANSBORDO As String
        <JsonProperty>
        Public Property NUMMANIFIESTOCARGATRANSBORDO As String
        <JsonProperty>
        Public Property CODOPERACIONTRANSPORTE As String
        <JsonProperty>
        Public Property VIAJESDIA As String
        <JsonProperty>
        Public Property FECHAEXPEDICIONMANIFIESTO As String
        <JsonProperty>
        Public Property CODMUNICIPIOORIGENMANIFIESTO As String
        <JsonProperty>
        Public Property CODMUNICIPIODESTINOMANIFIESTO As String
        <JsonProperty>
        Public Property CODIDTITULARMANIFIESTO As String
        <JsonProperty>
        Public Property NUMIDTITULARMANIFIESTO As String
        <JsonProperty>
        Public Property NUMPLACA As String
        <JsonProperty>
        Public Property NUMPLACAREMOLQUE As String
        <JsonProperty>
        Public Property CODIDCONDUCTOR As String
        <JsonProperty>
        Public Property NUMIDCONDUCTOR As String
        <JsonProperty>
        Public Property CODIDCONDUCTOR2 As String
        <JsonProperty>
        Public Property NUMIDCONDUCTOR2 As String
        <JsonProperty>
        Public Property VALORFLETEPACTADOVIAJE As String
        <JsonProperty>
        Public Property RETENCIONFUENTEMANIFIESTO As String
        <JsonProperty>
        Public Property RETENCIONICAMANIFIESTOCARGA As String
        <JsonProperty>
        Public Property VALORANTICIPOMANIFIESTO As String
        <JsonProperty>
        Public Property CODMUNICIPIOPAGOSALDO As String
        <JsonProperty>
        Public Property FECHAPAGOSALDOMANIFIESTO As String
        <JsonProperty>
        Public Property CODRESPONSABLEPAGOCARGUE As String
        <JsonProperty>
        Public Property CODRESPONSABLEPAGODESCARGUE As String
        <JsonProperty>
        Public Property OBSERVACIONES As String
        <JsonProperty>
        Public Property CONSECUTIVOREMESA As String()
        <JsonProperty>
        Public Property ACEPTACIONELECTRONICA As String

        ''CAMPOS CUMPLIDO
        <JsonProperty>
        Public Property TIPOCUMPLIDOMANIFIESTO As String
        <JsonProperty>
        Public Property MOTIVOSUSPENSIONMANIFIESTO As String
        <JsonProperty>
        Public Property CONSECUENCIASUSPENSIONMANIFIESTO As String
        <JsonProperty>
        Public Property VALORADICIONALHORASCARGUE As String
        <JsonProperty>
        Public Property VALORADICIONALHORASDESCARGUE As String
        <JsonProperty>
        Public Property VALORADICIONALFLETE As String
        <JsonProperty>
        Public Property MOTIVOVALORADICIONAL As String
        <JsonProperty>
        Public Property VALORDESCUENTOFLETE As String
        <JsonProperty>
        Public Property MOTIVOVALORDESCUENTOMANIFIESTO As String
        <JsonProperty>
        Public Property VALORSOBREANTICIPO As String
        <JsonProperty>
        Public Property FECHAENTREGADOCUMENTOS As String
        <JsonProperty>
        Public Property CODMOTIVOANULACIONCUMPLIDO As String

        ''campos anulaci
        <JsonProperty>
        Public Property MOTIVOANULACIONMANIFIESTO As String

        <JsonProperty>
        Public Property Tenedor As TerceroRNDC
        <JsonProperty>
        Public Property Propietario As TerceroRNDC
        <JsonProperty>
        Public Property Conductor As TerceroRNDC
        <JsonProperty>
        Public Property Vehiculo As VehiculoRNDC
        <JsonProperty>
        Public Property Semirremolque As VehiculoRNDC
        <JsonProperty>
        Public Property OpcionReporte As Integer


        <JsonProperty>
        Public Property Remesas As IEnumerable(Of RemesasRNDC)
        Const NOMBRE_TAG As String = "variables"
        Const REMESASMAN As String = "REMESASMAN"
    End Class
End Namespace
