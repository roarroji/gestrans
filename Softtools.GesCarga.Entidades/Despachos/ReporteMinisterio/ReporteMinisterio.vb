﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Despachos.Procesos.General

Namespace Despachos.Procesos
    <JsonObject>
    Public NotInheritable Class ReporteMinisterio
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ReporteMinisterio"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            SeguimientoRemesaManifiesto = Read(lector, "SeguimientoRemesaManifiesto")
            If SeguimientoRemesaManifiesto = 1 Then
                Remesa = New Remesas With {.Numero = Read(lector, "Numero"),
                                             .NumeroDocumento = Read(lector, "NumeroDocumento"),
                                             .Fecha = Read(lector, "Fecha"),
                                             .Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "PlacaVehiculo")},
                                             .Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "PlacaSemirremolque")},
                                             .Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "NombreRuta")}
                 }
                Mensaje = Read(lector, "Mensaje")
                TotalRegistros = Read(lector, "TotalRegistros")
            Else
                OpcionReporte = Read(lector, "OpcionReporte")
                Select Case OpcionReporte
                    Case OPCIONES_REPORTE_RNDC.REMESA
                        Remesa = New Remesas With {.Numero = Read(lector, "Numero"),
                                                    .NumeroDocumento = Read(lector, "NumeroDocumento"),
                                                    .Fecha = Read(lector, "Fecha"),
                                                    .Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .NombreCompleto = Read(lector, "NombreCliente")},
                                                    .Remitente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Remitente"), .NombreCompleto = Read(lector, "NombreRemitente")},
                                                    .Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "PlacaVehiculo")},
                                                    .Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "PlacaSemirremolque")},
                                                    .Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "NombreRuta")},
                                                    .Manifiesto = New Manifiesto With {.Numero = Read(lector, "ENMC_Numero"), .NumeroDocumento = Read(lector, "NumeroDocumentoManifiesto")},
                                                    .ProductoTransportado = New ProductoTransportados With {.Nombre = Read(lector, "PRTR_Nombre")},
                                                    .NumeroSemillaOrdenServicio = Read(lector, "ESOS_Numero"),
                                                    .NumeroOrdenServicio = Read(lector, "NumeroDocumentoOrdenservicio"),
                                                    .PermisoInvias = Read(lector, "Permiso_Invias")
                        }
                        Mensaje = Read(lector, "Mensaje")
                End Select
                If Read(lector, "Obtener") = 0 Then
                    TotalRegistros = Read(lector, "TotalRegistros")
                End If
            End If

            'CodigoEmpresa = Read(lector, "EMPR_Codigo")
            'AplicaRemesa = Read(lector, "AplicaRemesa")
            'NumeroDocumento = Read(lector, "Numero_Documento")

            'PlacaVehiculo = Read(lector, "PlacaVehiculo")
            'NombreRuta = Read(lector, "NombreRuta")
            'Fecha = Read(lector, "Fecha")
            'If AplicaRemesa = 1 Then
            '    NumeroDocumentoOrdenservicio = Read(lector, "NumeroDocumentoOrdenservicio")
            '    NumeroDocumentoManifiesto = Read(lector, "NumeroDocumentoManifiesto")
            '    Mensaje = Read(lector, "Mensaje_Remesa_Electronico")
            '    NombreCliente = Read(lector, "NombreCliente")

            'Else
            '    NombreConductor = Read(lector, "NombreConductor")
            '    PlacaSemiremolque = Read(lector, "PlacaSemiremolque")
            '    NumeroCumplido = Read(lector, "NumeroCumplido")
            '    NombreOficina = Read(lector, "NombreOficina")
            '    Mensaje = Read(lector, "Mensaje_Manifiesto_Electronico")
            'End If
        End Sub

        <JsonProperty>
        Public Property SeguimientoRemesaManifiesto As Short

        <JsonProperty>
        Public Property OpcionReporte As Integer

        <JsonProperty>
        Public Property Remesa As Remesas
        <JsonProperty>
        Public Property DetalleReporteRemesas As IEnumerable(Of Remesas)

        <JsonProperty>
        Public Property Manifiesto As Manifiesto

        <JsonProperty>
        Public Property DetalleReporteManifiesto As IEnumerable(Of Manifiesto)

        <JsonProperty>
        Public Property Mensaje As String

        <JsonProperty>
        Public Property CodigosOficinas As String
        <JsonProperty>
        Public Property ConsultaServicioAutomatico As Short
        '<JsonProperty>
        'Public Property PlacaSemiremolque As String

        '<JsonProperty>
        'Public Property Fecha As DateTime

        '<JsonProperty>
        'Public Property PlacaVehiculo As String

        '<JsonProperty>
        'Public Property NombreCliente As String

        '<JsonProperty>
        'Public Property Mensaje As String

        '<JsonProperty>
        'Public Property NombreConductor As String

        '<JsonProperty>
        'Public Property NombreOficina As String

        '<JsonProperty>
        'Public Property NombreRuta As String

        '<JsonProperty>
        'Public Property NumeroDocumento As Integer

        '<JsonProperty>
        'Public Property NumeroDocumentoOrdenservicio As Integer

        '<JsonProperty>
        'Public Property NumeroDocumentoManifiesto As Integer

        '<JsonProperty>
        'Public Property NumeroCumplido As Integer

        '<JsonProperty>
        'Public Property Usuario As Usuarios

        '<JsonProperty>
        'Public Property AplicaManifiesto As Integer

        '<JsonProperty>
        'Public Property AplicaRemesa As Integer

        '<JsonProperty>
        'Public Property ListadoBase As IEnumerable(Of BaseDocumento)


    End Class
End Namespace
