﻿Imports System.Xml
Imports Softtools.GesCarga.Entidades.Despachos.Procesos.General
Namespace Despachos.Procesos
    Public Class Solicitud

#Region "Propiedades"
        Public tipo As String
        Public Procesoid As String
#End Region

#Region "Constantes"
        Const NOMBRE_TAG As String = "solicitud"

        Public Const SOLICITUD_WS_MINISTERIO_TIPO_INSERTAR = 1
        Public Const SOLICITUD_WS_MINISTERIO_TIPO_CONSULTAR = 3

        Public Const SOLICITUD_WS_MINISTERIO_PROCESO_INFORMACION_CARGA = 1
        Public Const SOLICITUD_WS_MINISTERIO_PROCESO_INFORMACION_VIAJE = 2
        Public Const SOLICITUD_WS_MINISTERIO_PROCESO_REMESA = 3
        Public Const SOLICITUD_WS_MINISTERIO_PROCESO_MANIFIESTO = 4
        Public Const SOLICITUD_WS_MINISTERIO_PROCESO_CUMPLIR_REMESA = 5
        Public Const SOLICITUD_WS_MINISTERIO_PROCESO_CUMPLIR_MANIFIESTO = 6
        Public Const SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_INFO_CARGA = 7
        Public Const SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_VIAJE = 8
        Public Const SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_REMESA = 9
        Public Const SOLICITUD_WS_MINISTERIO_PROCESO_TERCERO = 11
        Public Const SOLICITUD_WS_MINISTERIO_PROCESO_VEHICULO = 12
        Public Const SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_MANIFIESTO = 32
#End Region

#Region "Contructor"
        Sub New(ByVal strtipo As String, ByVal strProcesoid As String)
            Me.tipo = strtipo
            Me.Procesoid = strProcesoid
        End Sub

        Public Sub New()
            Me.tipo = ""
            Me.Procesoid = ""
        End Sub
#End Region

#Region "Funciones Privadas"

#End Region

#Region "Funciones Publicas"
        Public Sub Crear_XML_Solicitud(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByVal Tipos_Documentos As Tipos_Documentos)
            Try
                Select Case TipoFormato
                    Case Tipos_Procesos.FORMATO_INGRESO
                        Me.tipo = TipoFormato
                    Case Tipos_Procesos.FORTMATO_CONSULTA
                        Me.tipo = TipoFormato
                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
                        Me.tipo = TipoFormato
                    Case Tipos_Procesos.FORTMATO_CONSULTA_SICETAC
                        Me.tipo = TipoFormato
                End Select
                Me.Procesoid = Tipos_Documentos

                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
                xmlElementoRoot.AppendChild(NuevoElemento)

                xmlElementoRoot = XmlDocumento.DocumentElement

                NuevoElemento = XmlDocumento.CreateElement("tipo")
                Dim txtNodo As XmlText = XmlDocumento.CreateTextNode(Me.tipo)
                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

                NuevoElemento = XmlDocumento.CreateElement("procesoid")
                txtNodo = XmlDocumento.CreateTextNode(Me.Procesoid)
                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

            Catch ex As Exception

            End Try
        End Sub

#End Region

    End Class
End Namespace

