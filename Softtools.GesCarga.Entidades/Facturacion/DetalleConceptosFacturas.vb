﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Facturacion
Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Namespace Facturacion
    <JsonObject>
    Public NotInheritable Class DetalleConceptosFacturas
        Inherits BaseDetalleDocumento
        Sub New()
        End Sub
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroEncabezado = Read(lector, "ENFA_Numero")
            DetalleNODE = New DetalleNovedadesDespachos With {.Codigo = Read(lector, "DEND_ID")}
            DECF_Codigo = Read(lector, "Codigo")
            ConceptosVentas = New ConceptoFacturacion With {
                .Codigo = Read(lector, "COVE_Codigo"),
                .Nombre = Read(lector, "COVE_Nombre"),
                .Operacion = Read(lector, "COVE_Operacion")
            }
            Descripcion = Read(lector, "Descripcion")
            Valor_Concepto = Read(lector, "Valor_Concepto")
            Valor_Impuestos = Read(lector, "Valor_Impuestos")
            Remesas = New Remesas With {.Numero = Read(lector, "ENRE_Numero"), .NumeroDocumento = Read(lector, "ENRE_Numero_Documento")}
            NumeroOrdenServicio = Read(lector, "ENOS_Numero")
            Emoe_numero = Read(lector, "EMOE_Numero")
        End Sub
        <JsonProperty>
        Public Property DECF_Codigo As Integer
        <JsonProperty>
        Public Property DetalleNODE As DetalleNovedadesDespachos
        <JsonProperty>
        Public Property Remesas As Remesas
        <JsonProperty>
        Public Property ConceptosVentas As ConceptoFacturacion
        <JsonProperty>
        Public Property Descripcion As String
        <JsonProperty>
        Public Property Valor_Concepto As Integer
        <JsonProperty>
        Public Property Valor_Impuestos As Integer
        <JsonProperty>
        Public Property NumeroOrdenServicio As Long
        <JsonProperty>
        Public Property Emoe_numero As Long
        <JsonProperty>
        Public Property DetalleImpuestoConceptosFacturas As IEnumerable(Of DetalleImpuestosConceptosFacturas)
    End Class
End Namespace
