﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Facturacion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Tesoreria

Namespace Facturacion
    <JsonObject>
    Public NotInheritable Class Facturas
        Inherits BaseDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Empresas = New Empresas With {
                .NumeroIdentificacion = Read(lector, "EMPR_Numero_Identificacion"),
                .DigitoChequeo = Read(lector, "EMPR_Digito_Chequeo"),
                .RazonSocial = Read(lector, "Nombre_Razon_Social"),
                .Telefono = Read(lector, "Telefonos"),
                .Email = Read(lector, "Emails"),
                .TipoNumeroIdentificacion = Read(lector, "EMPR_Nombre_TipoIdentificacion")
            }

            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            NombreTipoFactura = Read(lector, "CATA_TIFA_Nombre")
            CataTipoFactura = Read(lector, "CATA_TIFA_Codigo")
            NumeroOrdenCompra = Read(lector, "Numero_Orden_Compra")
            Fecha = Read(lector, "Fecha")
            ValorFactura = Read(lector, "Valor_Factura")
            ValorRemesas = Read(lector, "Valor_Remesas")
            ValorOtrosConceptos = Read(lector, "Valor_Otros_Conceptos")
            Subtotal = Read(lector, "Subtotal")
            ValorRemesas = Read(lector, "Valor_Remesas")
            ValorImpuestos = Read(lector, "Valor_Impuestos")
            ValorFactura = Read(lector, "Valor_Factura")

            Cliente = New Terceros With {
                .Codigo = Read(lector, "TERC_Cliente"),
                .NumeroIdentificacion = Read(lector, "CLIE_Numero_Identificacion"),
                .DigitoChequeo = Read(lector, "CLIE_Digito_Chequeo"),
                .Telefonos = Read(lector, "CLIE_Telefonos"),
                .Direccion = Read(lector, "CLIE_Direccion"),
                .Correo = Read(lector, "CLIE_Emails"),
                .TipoNaturaleza = New ValorCatalogos With {.Nombre = Read(lector, "CLIE_Nombre_TipoNaturaleza")},
                .TipoIdentificacion = New ValorCatalogos With {.Nombre = Read(lector, "CLIE_Nombre_TipoIdentificacion")},
                .Nombre = Read(lector, "NombreTERCCliente"),
                .PrimeroApellido = Read(lector, "Apellido1TERCCliente"),
                .SegundoApellido = Read(lector, "Apellido2TERCCliente"),
                .NombreCompleto = Read(lector, "NombreCliente"),
                .Ciudad = New Ciudades With {
                    .CodigoAlterno = Read(lector, "CLIE_Codigo_Ciudad"),
                    .Nombre = Read(lector, "CLIE_Nombre_Ciudad "),
                    .CodigoPostal = Read(lector, "CLIE_Codigo_Postal_Ciudad")
                },
                .Departamentos = New Departamentos With {.CodigoAlterno = Read(lector, "CLIE_Codigo_Departamento"), .Nombre = Read(lector, "CLIE_Nombre_Departamento")}
            }
            FacturarA = New Terceros With {
                .Codigo = Read(lector, "TERC_Facturar"),
                .NumeroIdentificacion = Read(lector, "FACTA_Numero_Identificacion"),
                .DigitoChequeo = Read(lector, "FACTA_Digito_Chequeo"),
                .Telefonos = Read(lector, "FACTA_Telefonos"),
                .Direccion = Read(lector, "FACTA_Direccion"),
                .Correo = Read(lector, "FACTA_Emails"),
                .TipoNaturaleza = New ValorCatalogos With {.Nombre = Read(lector, "FACTA_Nombre_TipoNaturaleza")},
                .TipoIdentificacion = New ValorCatalogos With {.Nombre = Read(lector, "FACTA_Nombre_TipoIdentificacion")},
                .Nombre = Read(lector, "NombreTERCFacturarA"),
                .PrimeroApellido = Read(lector, "Apellido1TERCFacturarA"),
                .SegundoApellido = Read(lector, "Apellido2TERCFacturarA"),
                .NombreCompleto = Read(lector, "NombreFacturarA"),
                .Ciudad = New Ciudades With {
                    .CodigoAlterno = Read(lector, "FACTA_Codigo_Ciudad"),
                    .Nombre = Read(lector, "FACTA_Nombre_Ciudad "),
                    .CodigoPostal = Read(lector, "FACTA_Codigo_Postal_Ciudad")
                },
                .Departamentos = New Departamentos With {.CodigoAlterno = Read(lector, "FACTA_Codigo_Departamento"), .Nombre = Read(lector, "FACTA_Nombre_Departamento")}
            }
            OficinaFactura = New Oficinas With {
                .Codigo = Read(lector, "OFIC_Factura"),
                .Nombre = Read(lector, "OficinaFactura"),
                .ClaveExternaFactura = Read(lector, "Clave_Externa_Factura"),
                .ClaveTecnicaFactura = Read(lector, "Clave_Tecnica_Factura")
            }

            ValorTotalIva = Read(lector, "Valor_Total_Iva")
            FechaCreacion = Read(lector, "FechaCreacion")
            FechaVence = Read(lector, "FechaVence")

            FacturaElectronica = New FacturaElectronica With {
                .CUFE = Read(lector, "CUFE"),
                .ID_Pruebas = Read(lector, "ID_Pruebas"),
                .ID_Habilitacion = Read(lector, "ID_Habilitacion"),
                .ID_Emision = Read(lector, "ID_Emision"),
                .QRcode = Read(lector, "QR_code"),
                .Archivo = Read(lector, "Archivo")
            }
            DiasPlazo = Read(lector, "Dias_Plazo")
            FormaPago = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FPVE_Codigo"), .Nombre = Read(lector, "CATA_FPVE_Nombre")}
            SedeFacturacion = New TerceroClienteCupoSedes With {.Codigo = Read(lector, "TEDI_Codigo"), .Nombre = Read(lector, "TEDI_Nombre")}
            Observaciones = Read(lector, "Observaciones")
            Estado = Read(lector, "Estado")
            EstadoFAEL = Read(lector, "Estado_FAEL")
            Anulado = Read(lector, "Anulado")
            TotalRegistros = Read(lector, "TotalRegistros")
            RemesasPaqueteria = Read(lector, "RemesasPaqueteria")
            CausaAnulacion = Read(lector, "Causa_Anula")
            Prefijo = Read(lector, "Prefijo")

            InterfazContable = Read(lector, "Interfaz_Contable_Factura")
            FeachaInterfazContable = Read(lector, "Fecha_Interfase_Contable")
            IntentosInterfazContable = Read(lector, "Intentos_Interfase_Contable")
            MensajeErrorInterfazContable = Read(lector, "Mensaje_Error")


        End Sub

        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property FacturarA As Terceros
        <JsonProperty>
        Public Property SedeFacturacion As TerceroClienteCupoSedes
        <JsonProperty>
        Public Property FormaPago As ValorCatalogos
        <JsonProperty>
        Public Property DiasPlazo As Short
        <JsonProperty>
        Public Property ValorRemesas As Double
        <JsonProperty>
        Public Property NombreTipoFactura As String
        <JsonProperty>
        Public Property CataTipoFactura As Double
        <JsonProperty>
        Public Property ValorFactura As Double
        <JsonProperty>
        Public Property Remesas As IEnumerable(Of DetalleFacturas)
        <JsonProperty>
        Public Property RemesasPaqueteria As Integer
        <JsonProperty>
        Public Property OtrosConceptos As IEnumerable(Of DetalleConceptosFacturas)
        <JsonProperty>
        Public Property OficinaFactura As Oficinas
        <JsonProperty>
        Public Property NombreTipoNaturaleza As String
        <JsonProperty>
        Public Property NumeroOrdenCompra As String
        <JsonProperty>
        Public Property NombreTipoDocumento As String
        <JsonProperty>
        Public Property RazonSocial As String
        <JsonProperty>
        Public Property Ciudad As String
        <JsonProperty>
        Public Property Prefijo As String
        <JsonProperty>
        Public Property CuentaPorCobrar As EncabezadoDocumentoCuentas
        <JsonProperty>
        Public Property ImpuestoFacturas As IEnumerable(Of ImpuestoFacturas)
        <JsonProperty>
        Public Property DetalleImpuestosFacturas As IEnumerable(Of DetalleImpuestosFacturas)

#Region "Informacion Factura Electronica"
        <JsonProperty>
        Public Property FechaCreacion As String
        <JsonProperty>
        Public Property FechaVence As String
        <JsonProperty>
        Public Property Empresas As Empresas
        <JsonProperty>
        Public Property ValorTotalIva As Double
        <JsonProperty>
        Public Property EstadoFAEL As Integer
        <JsonProperty>
        Public Property FacturaElectronica As FacturaElectronica
#End Region

#Region "Interfaz Contable"
        <JsonProperty>
        Public Property InterfazContable As String
        <JsonProperty>
        Public Property FeachaInterfazContable As DateTime
        <JsonProperty>
        Public Property IntentosInterfazContable As Integer
        <JsonProperty>
        Public Property MensajeErrorInterfazContable As String
#End Region
    End Class
End Namespace
