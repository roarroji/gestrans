﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Namespace Facturacion
    <JsonObject>
    Public NotInheritable Class ImpuestoFacturas
        Inherits BaseDocumento
        Sub New()
        End Sub
        Sub New(lector As IDataReader)
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Operacion = Read(lector, "Operacion")
            Estado = Read(lector, "Estado")
            Valor_tarifa = Read(lector, "Valor_Tarifa")
            Valor_base = Read(lector, "Valor_Base")
        End Sub
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Operacion As Integer
        <JsonProperty>
        Public Property Valor_tarifa As Double
        <JsonProperty>
        Public Property Valor_base As Long
        <JsonProperty>
        Public Property Ciudad As Ciudades
        <JsonProperty>
        Public Property CodigoTipoDocumento As Integer
    End Class
End Namespace

