﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.Facturacion

Namespace Facturacion
    <JsonObject>
    Public NotInheritable Class NotasFacturas
        Inherits BaseDocumento
        Sub New()
        End Sub
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Empresas = New Empresas With {
                .NumeroIdentificacion = Read(lector, "EMPR_Numero_Identificacion"),
                .DigitoChequeo = Read(lector, "EMPR_Digito_Chequeo"),
                .RazonSocial = Read(lector, "Nombre_Razon_Social"),
                .Telefono = Read(lector, "Telefonos"),
                .Email = Read(lector, "Emails"),
                .TipoNumeroIdentificacion = Read(lector, "EMPR_Nombre_TipoIdentificacion")
            }

            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "ENNF_NumeroDocumento")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            NombreTipoDocumento = Read(lector, "TIDO_Nombre")
            Fecha = Read(lector, "ENNF_Fecha")
            ValorNota = Read(lector, "ENNF_Valor_Nota")
            Prefijo = Read(lector, "Prefijo_Nota")

            ConceptoNotas = New ConceptoNotasFacturas With {.Codigo = Read(lector, "CONF_Codigo"), .Nombre = Read(lector, "CONF_Nombre")}

            Factura = New Facturas With {
                .Numero = Read(lector, "ENFA_Numero"),
                .ValorFactura = Read(lector, "Valor_Factura"),
                .NumeroDocumento = Read(lector, "ENFA_NumeroDocumento"),
                .Fecha = Read(lector, "ENFA_Fecha"),
                .FechaCreacion = Read(lector, "FechaCreacion"),
                .Prefijo = Read(lector, "Prefijo_Factura"),
                .FacturaElectronica = New FacturaElectronica With {.CUFE = Read(lector, "CUFE"), .FechaCreacion = Read(lector, "FAEL_FechaEnvio")}
            }
            OficinaNota = New Oficinas With {.Codigo = Read(lector, "OFIC_Nota"), .Nombre = Read(lector, "OFIC_NombreNota")}
            Observaciones = Read(lector, "ENNF_Observaciones")

            Estado = Read(lector, "ENNF_Estado")
            NotaElectronica = Read(lector, "ENNF_Nota_Electronica")
            Anulado = Read(lector, "ENNF_Anulado")

            Clientes = New Terceros With {
                .Codigo = Read(lector, "TERC_Cliente"),
                .NumeroIdentificacion = Read(lector, "CLIE_Numero_Identificacion"),
                .DigitoChequeo = Read(lector, "CLIE_Digito_Chequeo"),
                .Telefonos = Read(lector, "CLIE_Telefonos"),
                .Nombre = Read(lector, "NombreTERCCliente"),
                .PrimeroApellido = Read(lector, "Apellido1TERCCliente"),
                .SegundoApellido = Read(lector, "Apellido2TERCCliente"),
                .NombreCompleto = Read(lector, "CLIE_NombreCompleto"),
                .Direccion = Read(lector, "CLIE_Direccion"),
                .Correo = Read(lector, "CLIE_Emails"),
                .TipoNaturaleza = New ValorCatalogos With {.Nombre = Read(lector, "CLIE_Nombre_TipoNaturaleza")},
                .TipoIdentificacion = New ValorCatalogos With {.Nombre = Read(lector, "CLIE_Nombre_TipoIdentificacion")},
                .Ciudad = New Ciudades With {
                    .CodigoAlterno = Read(lector, "CLIE_Codigo_Ciudad"),
                    .Nombre = Read(lector, "CLIE_Nombre_Ciudad "),
                    .CodigoPostal = Read(lector, "CLIE_Codigo_Postal_Ciudad")
                },
                .Departamentos = New Departamentos With {.CodigoAlterno = Read(lector, "CLIE_Codigo_Departamento"), .Nombre = Read(lector, "CLIE_Nombre_Departamento")}
            }
            FacturarA = New Terceros With {
                .Codigo = Read(lector, "TERC_Facturar"),
                .NumeroIdentificacion = Read(lector, "FACTA_Numero_Identificacion"),
                .DigitoChequeo = Read(lector, "FACTA_Digito_Chequeo"),
                .Telefonos = Read(lector, "FACTA_Telefonos"),
                .Nombre = Read(lector, "NombreTERCFacturarA"),
                .PrimeroApellido = Read(lector, "Apellido1TERCFacturarA"),
                .SegundoApellido = Read(lector, "Apellido2TERCFacturarA"),
                .NombreCompleto = Read(lector, "TEFA_NombreCompleto"),
                .Direccion = Read(lector, "FACTA_Direccion"),
                .Correo = Read(lector, "FACTA_Emails"),
                .TipoNaturaleza = New ValorCatalogos With {.Nombre = Read(lector, "FACTA_Nombre_TipoNaturaleza")},
                .TipoIdentificacion = New ValorCatalogos With {.Nombre = Read(lector, "FACTA_Nombre_TipoIdentificacion")},
                .Ciudad = New Ciudades With {
                    .CodigoAlterno = Read(lector, "FACTA_Codigo_Ciudad"),
                    .Nombre = Read(lector, "FACTA_Nombre_Ciudad "),
                    .CodigoPostal = Read(lector, "FACTA_Codigo_Postal_Ciudad")
                },
                .Departamentos = New Departamentos With {.CodigoAlterno = Read(lector, "FACTA_Codigo_Departamento"), .Nombre = Read(lector, "FACTA_Nombre_Departamento")}
            }

            FechaCreacion = Read(lector, "FechaCreacion")
            FechaVence = Read(lector, "FechaVence")

            CUDE = Read(lector, "CUDE")
            ID_Pruebas = Read(lector, "ID_Pruebas")
            ID_Habilitacion = Read(lector, "ID_Habilitacion")
            ID_Emision = Read(lector, "ID_Emision")
            Archivo = Read(lector, "Archivo")

            MensajeSQL = Read(lector, "MensajeSQL")

            TotalRegistros = Read(lector, "TotalRegistros")

        End Sub
        <JsonProperty>
        Public Property Factura As Facturas
        <JsonProperty>
        Public Property OficinaNota As Oficinas
        <JsonProperty>
        Public Property Clientes As Terceros
        <JsonProperty>
        Public Property FacturarA As Terceros
        <JsonProperty>
        Public Property ConceptoNotas As ConceptoNotasFacturas
        <JsonProperty>
        Public Property ValorNota As Double
        <JsonProperty>
        Public Property NombreTipoDocumento As String
        <JsonProperty>
        Public Property CodigoError As String
        <JsonProperty>
        Public Property MensajeError As String
        <JsonProperty>
        Public Property Prefijo As String
#Region "Informacion Factura Electronica"
        <JsonProperty>
        Public Property CUDE As String
        <JsonProperty>
        Public Property FechaCreacion As String
        <JsonProperty>
        Public Property FechaVence As String
        <JsonProperty>
        Public Property Empresas As Empresas
        <JsonProperty>
        Public Property NotaElectronica As Integer
        <JsonProperty>
        Public Property FechaEnvioElectronico As Date
        <JsonProperty>
        Public Property ID_Pruebas As String
        <JsonProperty>
        Public Property ID_Habilitacion As String
        <JsonProperty>
        Public Property ID_Emision As String
        <JsonProperty>
        Public Property DocumentoUBL As String
        <JsonProperty>
        Public Property TipoDocumentoUBL As String
        <JsonProperty>
        Public Property Archivo As String
        <JsonProperty>
        Public Property TipoArchivo As String
        <JsonProperty>
        Public Property DecripcionArchivo As String
#End Region
    End Class
End Namespace

