﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Namespace Fidelizacion.Documentos
    <JsonObject>
    Public NotInheritable Class DetalleCausalesPuntosVehiculos
        'Inherits BaseBasico
        Inherits BaseDocumento

        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "VEHI_Placa")}
            Tenedor = New Tercero With {.Codigo = Read(lector, "TENE_Codigo"), .NombreCompleto = Read(lector, "TENE_Nombre")}
            Conductor = New Tercero With {.Codigo = Read(lector, "COND_Codigo"), .NombreCompleto = Read(lector, "COND_Nombre")}
            CATA_CPPV = New ValorCatalogos With {.Codigo = Read(lector, "CPPV_Codigo"), .Nombre = Read(lector, "CPPV_Campo1")}

            Puntos = Read(lector, "Puntos")
            FechaInicioVigencia = Read(lector, "DECA_FechaInicio")
            FechaFinVigencia = Read(lector, "DECA_FechaFin")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            UsuarioCrea = New Usuarios With {.CodigoUsuario = Read(lector, "USUA_CodigoUsuario")}
            FechaCrea = Read(lector, "Fecha_Crea")
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub

        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Tenedor As Tercero
        <JsonProperty>
        Public Property Conductor As Tercero
        <JsonProperty>
        Public Property CATA_CPPV As ValorCatalogos 'Causal Plan Puntos Vehiculo
        <JsonProperty>
        Public Property Puntos As Long
        <JsonProperty>
        Public Property FechaInicioVigencia As Date
        <JsonProperty>
        Public Property FechaFinVigencia As Date
        '''Filtros Fechas
        <JsonProperty>
        Public Property FechaInicioVigenciaDesde As Date
        <JsonProperty>
        Public Property FechaInicioVigenciaHasta As Date
        <JsonProperty>
        Public Property FechaFinVigenciaDesde As Date
        <JsonProperty>
        Public Property FechaFinVigenciaHasta As Date

    End Class
End Namespace

