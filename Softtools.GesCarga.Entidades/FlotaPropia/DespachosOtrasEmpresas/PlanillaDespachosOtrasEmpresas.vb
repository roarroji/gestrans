﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

Namespace FlotaPropia

    <JsonObject>
    Public Class PlanillaDespachosOtrasEmpresas
        Inherits BaseDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Planilla")
            Fecha = Read(lector, "Fecha_Planilla")
            NumeroDocumentoTransporte = Read(lector, "Numero_Documento_Transporte")
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .NombreCompleto = Read(lector, "PerfilCliente")}
            DocumentoCliente = Read(lector, "Documento_Cliente")
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "Ruta")}
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "PlacaVehiculo")}
            Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "Semirremolque")}
            Conductor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .Nombre = Read(lector, "Conductor"), .NombreCompleto = Read(lector, "Conductor")}
            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "Producto")}
            Cantidad = Read(lector, "Cantidad")
            Peso = Read(lector, "Peso")
            TipoTarifa = New TipoTarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo"), .Nombre = Read(lector, "TipoTarifa")}
            FleteTransportador = Read(lector, "Valor_Flete_Transportador")
            TotalFlete = Read(lector, "Valor_Total_Flete")
            RetencionFuente = Read(lector, "Valor_Retencion_Fuente")
            RetencionICA = Read(lector, "Valor_Retencion_ICA")
            AnticipoCliente = Read(lector, "Valor_Anticipo_Cliente")
            ValorPagar = Read(lector, "Valor_Pagar")
            AnticipoPropio = Read(lector, "Valor_Anticipo_Propio")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            FechaCrea = Read(lector, "Fecha_Crea")
            CodigoUsuarioCrea = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Crea")}
            FechaModifica = Read(lector, "Fecha_Modifica")
            UsuarioModifica = Read(lector, "USUA_Codigo_Modifica")
            FechaAnula = Read(lector, "Fecha_Anula")
            UsuarioAnula = Read(lector, "USUA_Codigo_Anula")
            CausaAnulacion = Read(lector, "Causa_Anula")
            TotalRegistros = Read(lector, "TotalRegistros")
            Observaciones = Read(lector, "Observaciones")
            Consecutivo = Read(lector, "Numero_Documento")
            EncabezadoFactura = Read(lector, "EncabezadoFactura")
            Facturado = Read(lector, "Facturado")
        End Sub
#Region "Propiedades"
        <JsonProperty>
        Public Property Consecutivo As Double
        <JsonProperty>
        Public Property NumeroDocumentoTransporte As Long
        <JsonProperty>
        Public Overloads Property NumeroDocumento As Long
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property DocumentoCliente As Long
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Semirremolque As Semirremolques
        <JsonProperty>
        Public Property Conductor As Tercero
        <JsonProperty>
        Public Property Producto As ProductoTransportados
        <JsonProperty>
        Public Property Cantidad As Double
        <JsonProperty>
        Public Property Peso As Double
        <JsonProperty>
        Public Property TipoTarifa As TipoTarifaTransportes
        <JsonProperty>
        Public Property FleteTransportador As Double
        <JsonProperty>
        Public Property TotalFlete As Double
        <JsonProperty>
        Public Property RetencionFuente As Double
        <JsonProperty>
        Public Property RetencionICA As Double
        <JsonProperty>
        Public Property AnticipoCliente As Double
        <JsonProperty>
        Public Property ValorPagar As Double
        <JsonProperty>
        Public Property AnticipoPropio As Double
        <JsonProperty>
        Public Property CodigoUsuarioCrea As Usuarios
        <JsonProperty>
        Public Property NombreConductor As String
        <JsonProperty>
        Public Property PlacaVehiculo As String
        <JsonProperty>
        Public Property EncabezadoFactura As Long
        <JsonProperty>
        Public Property Facturado As String
#End Region
    End Class

End Namespace

