﻿Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Newtonsoft.Json

Namespace FlotaPropia
    Public Class DetalleSolicitudAnticipo
        Inherits BaseDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            TipoSolicitud = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TSAN_Codigo")}
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo")}
            Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo")}
            Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor")}
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo")}
            Planilla = Read(lector, "ENPD_Numero")
            SitioCargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Cargue")}
            SitioDescargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Descargue")}
            ValorSugerido = Read(lector, "Valor_Sugerido")
            ValorRegistrado = Read(lector, "Valor_Registrado")
            Observaciones = Read(lector, "Observaciones")
            Solicitud = New EncabezadoSolicitudAnticipo With {.Numero = Read(lector, "ENSA_Numero")}

        End Sub
        <JsonProperty>
        Public Property EstadoEncabezado As Short
        <JsonProperty>
        Public Property EstadoERP As Short
        <JsonProperty>
        Public Property Solicitud As EncabezadoSolicitudAnticipo

        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Semirremolque As Semirremolques

        <JsonProperty>
        Public Property TipoSolicitud As ValorCatalogos

        <JsonProperty>
        Public Property Planilla As Long
        <JsonProperty>
        Public Property Conductor As Terceros
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property SitioCargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property SitioDescargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property ValorSugerido As Double
        <JsonProperty>
        Public Property ValorRegistrado As Double


    End Class
End Namespace