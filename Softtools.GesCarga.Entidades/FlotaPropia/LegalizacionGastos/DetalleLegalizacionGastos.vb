﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.FlotaPropia
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Despachos.Masivo
Imports Softtools.GesCarga.Entidades.Tesoreria

Namespace FlotaPropia
    <JsonObject>
    Public NotInheritable Class DetalleLegalizacionGastos
        Inherits Base
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LegalizacionGastos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Concepto = New ConceptoGastos With {.Codigo = Read(lector, "CLGC_Codigo"), .Nombre = Read(lector, "CLGC_Nombre")}
            Planilla = New PlanillaDespachos With {.Codigo = Read(lector, "ENPD_Numero"), .Numero = Read(lector, "ENPD_Numero"), .ValorAnticipo = Read(lector, "Valor_Anticipo"), .ValorReanticipos = Read(lector, "ValorReanticipos"), .Ruta = New Rutas With {.Nombre = Read(lector, "Ruta")}, .Vehiculo = New Vehiculos With {.Placa = Read(lector, "Placa")}, .NumeroDocumento = Read(lector, "Numero_Documento"), .Manifiesto = New Despachos.Manifiesto With {.Codigo = Read(lector, "NumeroManifiesto"), .NumeroDocumento = Read(lector, "NumeroDocumentoManifiesto")}}
            Observaciones = Read(lector, "Observaciones")
            Valor = Read(lector, "Valor")
            Proveedor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Proveedor")}
            NumeroFactura = Read(lector, "Numero_Factura")
            FechaFactura = Read(lector, "Fecha_Factura")
            CantidadGalones = Read(lector, "Cantidad_Galones")
            If Read(lector, "Tiene_Chip") > 0 Then
                TieneChip = True
            Else
                TieneChip = False
            End If
            Km_Ruta = Read(lector, "Km_Ruta")
            Km_Galon = Read(lector, "Km_Galon_Vehiculo")
            Gal_Estimados = Read(lector, "Galones_Estimado_Vehiculo")
            Km_Inicial = Read(lector, "Km_Inicial")
            Km_Final = Read(lector, "Km_Final")
            Total_Km = Read(lector, "Total_Km")
            Horas_Inicio = Read(lector, "Horas_Inicial_Termo")
            Horas_Fin = Read(lector, "Horas_Final_Termo")
            Horas_Termo = Read(lector, "Horas_Termo")
            Horas_Galon_Termo = Read(lector, "Horas_Galon_Termo")
            Galon_Estimado_Termo = Read(lector, "Galones_Estimado_Termo")
            Galones_Autorizados = Read(lector, "Galones_Autorizados")
            Total_Galones = Read(lector, "Total_Galones")
            ValorGalones = Read(lector, "Valor_Galones")
            NombreEstacion = Read(lector, "Nombre_Estacion")

            Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor")}
            ValorGastos = Read(lector, "Valor_Gastos")
            ValorAnticipos = Read(lector, "Valor_Anticipos")
            ValorReanticipos = Read(lector, "Valor_Reanticipos")
            SaldoConductor = Read(lector, "Saldo_Favor_Conductor")
            SaldoEmpresa = Read(lector, "Saldo_Favor_Empresa")
            DocumentoCuenta = Read(lector, "ENDC_Codigo")
        End Sub

#Region "Propiedades"

        <JsonProperty>
        Public Property CuentaPorPagar As EncabezadoDocumentoCuentas

        <JsonProperty>
        Public Property CuentaPorCobrar As EncabezadoDocumentoCuentas

        <JsonProperty>
        Public Property DocumentoCuenta As Long

        <JsonProperty>
        Public Property Concepto As ConceptoGastos
        <JsonProperty>
        Public Property Planilla As PlanillaDespachos
        <JsonProperty>
        Public Property Observaciones As String

        <JsonProperty>
        Public Property Valor As Double

        <JsonProperty>
        Public Property Proveedor As Terceros

        <JsonProperty>
        Public Property NumeroFactura As Integer

        <JsonProperty>
        Public Property FechaFactura As Date

        <JsonProperty>
        Public Property CantidadGalones As Double

        <JsonProperty>
        Public Property TieneChip As Boolean
        <JsonProperty>
        Public Property ValorGalones As Double
        <JsonProperty>
        Public Property NombreEstacion As String
        <JsonProperty>
        Public Property Km_Ruta As Double
        <JsonProperty>
        Public Property Km_Galon As Double
        <JsonProperty>
        Public Property Gal_Estimados As Double
        <JsonProperty>
        Public Property Km_Inicial As Double
        <JsonProperty>
        Public Property Km_Final As Double
        <JsonProperty>
        Public Property Total_Km As Double
        <JsonProperty>
        Public Property Galones_Autorizados As Double
        <JsonProperty>
        Public Property Horas_Inicio As Double
        <JsonProperty>
        Public Property Horas_Fin As Double
        <JsonProperty>
        Public Property Horas_Termo As Double
        <JsonProperty>
        Public Property Horas_Galon_Termo As Double
        <JsonProperty>
        Public Property Galon_Estimado_Termo As Double
        <JsonProperty>
        Public Property Total_Galones As Double
        <JsonProperty>
        Public Property Km_Ruta_Base As Double
        <JsonProperty>
        Public Property Conductor As Terceros
        <JsonProperty>
        Public Property ValorGastos As Double
        <JsonProperty>
        Public Property ValorAnticipos As Double
        <JsonProperty>
        Public Property ValorReanticipos As Double
        <JsonProperty>
        Public Property SaldoConductor As Double
        <JsonProperty>
        Public Property SaldoEmpresa As Double
#End Region

    End Class
End Namespace
