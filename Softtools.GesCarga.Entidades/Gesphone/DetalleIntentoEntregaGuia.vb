﻿Imports Newtonsoft.Json

Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="DetalleIntentoEntregaGuia"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleIntentoEntregaGuia

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleIntentoEntregaGuia"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleIntentoEntregaGuia"/>.
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = New BaseDocumento().Read(lector, "EMPR_Codigo")
            NumeroRemesa = New BaseDocumento().Read(lector, "ENRE_Numero")
            NumeroDocumentoRemesa = New BaseDocumento().Read(lector, "ENRE_Numero_Documento")
            ID = New BaseDocumento().Read(lector, "ID")
            Observaciones = New BaseDocumento().Read(lector, "Observaciones")
            CodigoUsuarioCrea = New BaseDocumento().Read(lector, "USUA_Codigo_Crea")
            FechaCrea = New BaseDocumento().Read(lector, "Fecha_Crea")
            NovedadIntentoEntrega = New BaseDocumento().Read(lector, "Novedad_Intento_Entrega")
        End Sub

        <JsonProperty>
        Public Property CodigoEmpresa As Short

        <JsonProperty>
        Public Property NumeroRemesa As Integer

        <JsonProperty>
        Public Property NumeroDocumentoRemesa As Integer

        <JsonProperty>
        Public Property ID As Integer

        <JsonProperty>
        Public Property Observaciones As String

        <JsonProperty>
        Public Property CodigoUsuarioCrea As Integer

        <JsonProperty>
        Public Property FechaCrea As DateTime

        <JsonProperty>
        Public Property NovedadIntentoEntrega As String

    End Class

End Namespace