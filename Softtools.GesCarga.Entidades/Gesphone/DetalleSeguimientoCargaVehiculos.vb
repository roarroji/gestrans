﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="DetalleSeguimientoCargaVehiculos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleSeguimientoCargaVehiculos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleSeguimientoCargaVehiculos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleSeguimientoCargaVehiculos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "ID")
            NumeroDocumento = Read(lector, "Numero_Documento")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            FechaReporte = Read(lector, "Fecha_Hora_Reporte")
            TipoSeguimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIRS_Codigo"), .Nombre = Read(lector, "TipoSeguimiento")}
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .NombreCompleto = Read(lector, "NombreCliente")}
            NovedadSeguimiento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NOSC_Codigo"), .Nombre = Read(lector, "NovedadSeguimiento")}
            Observaciones = Read(lector, "Observaciones")
            IdentificacionRecibido = Read(lector, "Identificacion_Recibido_Cliente")
            NombreRecibido = Read(lector, "Nombre_Recibido_Cliente")
            CiudadRecibido = New Ciudades With {.Codigo = Read(lector, "CIUD_Recibido_Cliente"), .Nombre = Read(lector, "Ciudad")}
            DireccionRecibido = Read(lector, "Direccion_Recibido_Cliente")
            TelefonoRecibido = Read(lector, "Telefono_Recibido_Cliente")
            Cumplir = Read(lector, "Cumplir")
            ReportarCliente = Read(lector, "Reportar_Cliente")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            Else
                NumeroSeguimiento = Read(lector, "ENSV_Numero")
                CantidadResividoCliente = Read(lector, "Cantidad_Recibido_Cliente")
                PesoResividoCliente = Read(lector, "Peso_Recibido_Cliente")
                Firma = Read(lector, "Firma")
            End If
        End Sub
        <JsonProperty>
        Public Property ID As Integer
        <JsonProperty>
        Public Property NumeroSeguimiento As Integer
        <JsonProperty>
        Public Property EnvioReporte As Integer
        <JsonProperty>
        Public Property FechaReporte As DateTime
        <JsonProperty>
        Public Property TipoSeguimiento As ValorCatalogos
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property NovedadSeguimiento As ValorCatalogos
        <JsonProperty>
        Public Property IdentificacionRecibido As String
        <JsonProperty>
        Public Property NombreRecibido As String
        <JsonProperty>
        Public Property CiudadRecibido As Ciudades
        <JsonProperty>
        Public Property DireccionRecibido As String
        <JsonProperty>
        Public Property TelefonoRecibido As String
        <JsonProperty>
        Public Property Cumplir As Short
        <JsonProperty>
        Public Property ReportarCliente As Short
        <JsonProperty>
        Public Property FechaEnvioReporte As DateTime
        <JsonProperty>
        Public Property Longitud As Integer
        <JsonProperty>
        Public Property Latitud As Integer
        <JsonProperty>
        Public Property OficinaCrea As Integer
        <JsonProperty>
        Public Property Firma As Byte()
        <JsonProperty>
        Public Property NombreCliente As String
        <JsonProperty>
        Public Property CantidadResividoCliente As String
        <JsonProperty>
        Public Property PesoResividoCliente As String

    End Class
End Namespace
