﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.Despachos


Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="EncabezadoCheckListVehiculos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EncabezadoCheckListVehiculos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoCheckListVehiculos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoCheckListVehiculos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            Fecha = Read(lector, "Fecha")
            FechaVigencia = Read(lector, "Fecha_Vigencia")
            NumeroDocumento = Read(lector, "Numero_Documento")
            KilometrosVehiculo = Read(lector, "Kilometros_Vehiculo")
            Responsable = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Responsable")}
            Observaciones = Read(lector, "Observaciones")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
                Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "PlacaSemiremolque")}
                Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .NombreCompleto = Read(lector, "NombreConductor")}
                Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "PlacaVehiculo")}
            Else
                Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo")}
                Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor")}
                Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo")}
            End If

        End Sub
        <JsonProperty>
        Public Property FechaVigencia As DateTime
        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property KilometrosVehiculo As Integer
        <JsonProperty>
        Public Property Semirremolque As Semirremolques
        <JsonProperty>
        Public Property Conductor As Terceros
        <JsonProperty>
        Public Property Responsable As Terceros
        <JsonProperty>
        Public Property Detalle As IEnumerable(Of DetalleCheckListVehiculos)
        <JsonProperty>
        Public Property Placa As String

    End Class
End Namespace
