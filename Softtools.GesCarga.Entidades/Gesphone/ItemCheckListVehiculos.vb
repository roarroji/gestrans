﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General


Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="ItemCheckListVehiculos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ItemCheckListVehiculos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ItemCheckListVehiculos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ItemCheckListVehiculos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            CodigoChecklist = Read(lector, "CHLV_Codigo")
            Codigo = Read(lector, "ID")
            Nombre = Read(lector, "Nombre")
            OrdenFormulario = Read(lector, "Orden_Formulario")
            TipoPregunta = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TPCL_Codigo")}
            TipoControl = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TCON_Codigo")}
            Estado = Read(lector, "Estado")

        End Sub
        <JsonProperty>
        Public Property CodigoChecklist As Integer
        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property OrdenFormulario As Integer
        <JsonProperty>
        Public Property TipoPregunta As ValorCatalogos
        <JsonProperty>
        Public Property TipoControl As ValorCatalogos

    End Class
End Namespace
