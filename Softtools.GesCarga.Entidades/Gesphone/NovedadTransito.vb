﻿
Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="NovedadTransito"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class NovedadTransito

        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="NovedadTransito"/>
        ''' </summary>
        Sub New()
        End Sub
        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="CheckListCargueDescargues"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Cliente = New Cliente With {.Codigo = Read(lector, "TERC_Codigo_Cliente")}
            Novedad = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NOSV_Codigo")}
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")

            If Obtener = 0 Then

                TotalRegistros = Read(lector, "TotalRegistros")
            End If
        End Sub

        <JsonProperty>
        Public Property AplicaSeguimiento As Integer
        <JsonProperty>
        Public Property Nombre As Integer
        <JsonProperty>
        Public Property Novedad As ValorCatalogos
        <JsonProperty>
        Public Property sitio As ValorCatalogos
        <JsonProperty>
        Public Property Cliente As Cliente
        <JsonProperty>
        Public Property Valor As Double
        <JsonProperty>
        Public Property ValorTransportador As Double
        <JsonProperty>
        Public Property ValorCliente As Double
        <JsonProperty>
        Public Property NumeroManifiesto As Double
        <JsonProperty>
        Public Property NumeroPlanilla As Double
        <JsonProperty>
        Public Property Ubicacion As String
        <JsonProperty>
        Public Property Longitud As Double
        <JsonProperty>
        Public Property Latitud As Double

    End Class
End Namespace


