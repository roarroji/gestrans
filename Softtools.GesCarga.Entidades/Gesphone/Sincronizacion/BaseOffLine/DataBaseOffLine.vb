﻿Imports Newtonsoft.Json

Namespace Gesphone

    ''' <summary>
    ''' Clase que tiene la informacion general de una base de datos OffLine
    ''' </summary>
    Public Class DataBaseOffLine

        ''' <summary>
        ''' Obtiene o establece el nombre de la base de datos.
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty("database")>
        Public Property Database As String

        ''' <summary>
        ''' Obtiene o establece la version de la base de datos.
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty("version")>
        Public Property Version As Integer

        ''' <summary>
        ''' Obtiene o establece si la base de datos estara encriptada.
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty("encrypted")>
        Public Property Encrypted As Boolean

        ''' <summary>
        ''' Obtiene o establece el modo de la base de datos.
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty("mode")>
        Public Property Mode As String

        ''' <summary>
        ''' Obtiene o establece la lista de tablas de la base de datos.
        ''' </summary>
        ''' <returns></returns>
        <JsonProperty("tables")>
        Public Property Tables As List(Of Table)

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DataBaseOffLine"/>
        ''' </summary>
        Sub New()
            Database = "gesphone-offline-db"
            Version = 1
            Encrypted = False
            Mode = "full"
        End Sub

    End Class
End Namespace


