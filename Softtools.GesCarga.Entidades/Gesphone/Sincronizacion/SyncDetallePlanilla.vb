﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Paqueteria

Namespace Gesphone
    ''' <summary>
    ''' Clase que tiene la informacion correspondiente al encabezado de un tarifario venta
    ''' </summary>
    <JsonObject>
    Public Class SyncDetallePlanilla

        <JsonProperty>
        Public Property CodEmp As Integer

        <JsonProperty>
        Public Property NumeroPlanilla As Integer

        <JsonProperty>
        <NotMapped>
        Public Property NumeroDocumentoPlanilla As Integer

        <JsonProperty>
        Public Property TipDoc As Integer

        <JsonProperty>
        Public Property NumeroRemesa As Integer

        <JsonProperty>
        Public Property NumeroDocumentoRemesa As Integer

        <JsonProperty>
        Public Property Fec As DateTime

        <JsonProperty>
        Public Property LinNeg As Integer

        <JsonProperty>
        Public Property ForPag As Integer

        <JsonProperty>
        <NotMapped>
        Public Property Tipo As Integer

        <JsonProperty>
        Public Property CodCli As Integer

        <JsonProperty>
        Public Property NumIdeCli As String

        <JsonProperty>
        Public Property NomCli As String

        <JsonProperty>
        Public Property ApeCli As String

        <JsonProperty>
        Public Property CodRem As Integer

        <JsonProperty>
        Public Property CodCiuRem As Integer

        <JsonProperty>
        Public Property NomCiuRem As String

        <JsonProperty>
        Public Property DirRem As String

        <JsonProperty>
        Public Property TipIdeRem As Integer

        <JsonProperty>
        Public Property NumIdeRem As String

        <JsonProperty>
        Public Property NomRem As String

        <JsonProperty>
        Public Property ApeRem As String

        <JsonProperty>
        Public Property BarRem As String

        <JsonProperty>
        Public Property ZonRem As String

        <JsonProperty>
        Public Property TelRem As String

        <JsonProperty>
        Public Property CodDes As Integer

        <JsonProperty>
        Public Property CodCiuDest As Integer

        <JsonProperty>
        Public Property NomCiuDest As String

        <JsonProperty>
        Public Property DirDest As String

        <JsonProperty>
        Public Property TipIdeDest As Integer

        <JsonProperty>
        Public Property NumIdeDest As String

        <JsonProperty>
        Public Property NomDest As String

        <JsonProperty>
        Public Property ApeDest As String

        <JsonProperty>
        Public Property BarDest As String

        <JsonProperty>
        Public Property ZonDest As String

        <JsonProperty>
        Public Property TelDest As String

        <JsonProperty>
        Public Property FecRec As Date

        <JsonProperty>
        Public Property Cont As String

        <JsonProperty>
        Public Property Bar As String

        <JsonProperty>
        Public Property Dir As String

        <JsonProperty>
        Public Property Tel As String

        <JsonProperty>
        Public Property CiuInfRec As Integer

        <JsonProperty>
        Public Property NomCiuCont As String

        <JsonProperty>
        Public Property CodTar As Integer

        <JsonProperty>
        Public Property NomTar As String

        <JsonProperty>
        Public Property CodTipTar As Integer

        <JsonProperty>
        Public Property NomTipTar As String

        <JsonProperty>
        Public Property CodPro As Integer

        <JsonProperty>
        Public Property NomPro As String

        <JsonProperty>
        Public Property Merca As String

        <JsonProperty>
        Public Property CodUniEmp As Integer

        <JsonProperty>
        Public Property NomUniEmp As String

        <JsonProperty>
        Public Property Cant As Decimal

        <JsonProperty>
        Public Property Pes As Decimal

        <JsonProperty>
        Public Property Lar As Decimal

        <JsonProperty>
        Public Property Anc As Decimal

        <JsonProperty>
        Public Property Alt As Decimal

        <JsonProperty>
        Public Property PeVo As Decimal

        <JsonProperty>
        Public Property PeCo As Decimal

        <JsonProperty>
        Public Property ValCom As Decimal

        <JsonProperty>
        Public Property ValMan As Decimal

        <JsonProperty>
        Public Property ValSeg As Decimal

        <JsonProperty>
        Public Property ValRex As Decimal

        <JsonProperty>
        Public Property ValFle As Decimal

        <JsonProperty>
        Public Property TotFle As Decimal

        <JsonProperty>
        Public Property Est As Integer

        <JsonProperty>
        Public Property EsGuiaRecogida As Short

        <JsonProperty>
        Public Property TieneDoc As Integer

        <JsonProperty>
        Public Property FecEnt As Date

        <JsonProperty>
        Public Property HorEnt As Integer

        <JsonProperty>
        Public Property Obs As String

        <JsonProperty>
        Public Property Cum As Integer

        <JsonProperty>
        Public Property FecReci As Date

        <JsonProperty>
        Public Property IdeRec As String

        <JsonProperty>
        Public Property NomRec As String

        <JsonProperty>
        Public Property TelRec As String

        <JsonProperty>
        Public Property CanRec As Decimal

        <JsonProperty>
        Public Property PesRec As Decimal

        <JsonProperty>
        Public Property ObsRec As String

        <JsonProperty>
        Public Property NovRec As Integer

        <JsonProperty>
        <NotMapped>
        Public Property Off As Byte

        <JsonProperty>
        Public Property ValAcaLoc As Decimal
        <JsonProperty>
        Public Property ValAcaDes As Decimal
        <JsonProperty>
        <NotMapped>
        Public Property Etiquetas As List(Of Integer)

        <JsonProperty>
        <NotMapped>
        Public Property GesDoc As List(Of SyncGestionDocumentos)

        <JsonProperty>
        <NotMapped>
        Public Property Intentos As List(Of SyncIntentoEntrega)
    End Class

    <JsonObject>
    Public Class SyncDetallePlanillaImagenes

        <JsonProperty>
        Public Property EsFirma As Integer

        <JsonProperty>
        Public Property Imagen As Byte()

    End Class

    <JsonObject>
    Public Class RemesaGesphone

        <JsonProperty>
        Public Property Remesa As RemesaPaqueteria

        <JsonProperty>
        Public Property NumeroRecoleccion As Integer

    End Class

    <JsonObject>
    Public Class RecoleccionEstado

        <JsonProperty>
        Public Property CodigoEmpresa As Integer

        <JsonProperty>
        Public Property NumeroRecoleccion As Integer

        <JsonProperty>
        Public Property Estado As Integer

    End Class

End Namespace
