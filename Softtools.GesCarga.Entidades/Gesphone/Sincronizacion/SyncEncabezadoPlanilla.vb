﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Paqueteria

Namespace Gesphone
    ''' <summary>
    ''' Clase que tiene la informacion correspondiente al encabezado de una planilla.
    ''' </summary>
    <JsonObject>
    Public Class SyncEncabezadoPlanilla

        Sub New()
        End Sub

        Sub New(record As PlanillaGuias, tipoDocumento As Integer)
            CodigoEmpresa = record.CodigoEmpresa
            Numero = record.Planilla.Numero
            NumeroDocumento = record.Planilla.NumeroDocumento
            Me.TipoDocumento = tipoDocumento
            Estado = record.Planilla.Estado.Codigo
            Me.DetallePlanilla = New List(Of SyncDetallePlanilla)
        End Sub

        <JsonProperty>
        Public Property CodigoEmpresa As Integer

        <JsonProperty>
        Public Property Numero As Integer

        <JsonProperty>
        Public Property NumeroDocumento As Integer

        <JsonProperty>
        Public Property TipoDocumento As Integer

        <JsonProperty>
        Public Property Estado As Integer

        <JsonProperty>
        Public Property CodigoAforador As Integer

        <JsonProperty>
        Public Property NombreAforador As String

        <NotMapped>
        <JsonProperty>
        Public Property DetallePlanilla As List(Of SyncDetallePlanilla)

        <NotMapped>
        <JsonProperty>
        Public Property DetalleAsignacionEtiquetas As List(Of SyncAsignacionEtiquetas)

    End Class


    <JsonObject>
    Public Class SyncAsignacionEtiquetas

        Sub New(registro As DetalleEtiquetaPreimpresa)
            'Id = registro.IdPreci
            'Ofi = registro.Oficina.Codigo
            Ne = registro.Numero_Precinto
            ''Re = registro.Responsable.Codigo
            'Nr = registro.ENRE_Numero
            'Es = registro.Estado
        End Sub

        Sub New(lector As IDataReader)
            'Id = Read(lector, "ID")
            'Ofi = Read(lector, "OFIC_Codigo")
            Ne = Read(lector, "Numero_Etiqueta")
            ''Re = Read(lector, "TERC_Codigo_Responsable")
            'Nr = Read(lector, "ENRE_Numero")
            'Es = Read(lector, "Estado")
        End Sub

        Public Function Read(Lector As IDataReader, strCampo As String) As Object
            Dim Tabla As DataTable = Lector.GetSchemaTable
            Dim ExisteCampo As Boolean = False
            For Each Campo As DataRow In Tabla.Rows
                If Campo("ColumnName").ToString() = strCampo Then
                    ExisteCampo = True
                    Exit For
                End If
            Next
            If ExisteCampo Then
                If Not IsDBNull(Lector.Item(strCampo)) Then
                    Return Lector.Item(strCampo)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        End Function

        '<JsonProperty>
        'Public Property Id As Integer

        '<JsonProperty>
        'Public Property Ofi As Integer

        <JsonProperty>
        Public Property Ne As Integer

        '<JsonProperty>
        'Public Property Re As Integer

        '<JsonProperty>
        'Public Property Nr As Integer

        '<JsonProperty>
        'Public Property Es As Integer

    End Class

End Namespace

