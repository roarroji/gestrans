﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Comercial.Documentos

Namespace Gesphone

    ''' <summary>
    ''' Clase que tiene la informacion de las fotos asociadas a una remesa.
    ''' </summary>
    <JsonObject>
    Public Class SyncFotosRemesa

        <JsonProperty>
        <SchemaOffLine("Id", "INTEGER PRIMARY KEY NOT NULL")>
        Public Property Id As Integer

        <JsonProperty>
        <SchemaOffLine("Numero", "INTEGER")>
        Public Property Numero As Integer

        <JsonProperty>
        <SchemaOffLine("Foto", "BLOB")>
        Public Property Foto As String

        <JsonProperty>
        <SchemaOffLine("EsFirma", "INTEGER")>
        Public Property EsFirma As Integer

    End Class

    ''' <summary>
    ''' Clase que tiene la informacion correspondiente al encabezado de un tarifario venta.
    ''' </summary>
    <JsonObject>
    Public Class SyncEncabezadoTarifarioVenta

        <JsonProperty>
        Public Property CodigoEmpresa As Integer

        <JsonProperty>
        Public Property Numero As Integer

        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property Estado As Integer

        <JsonProperty>
        Public Property GestionDocumentos As Integer

        <NotMapped>
        <JsonProperty>
        Public Property DetalleTarifario As List(Of Object)

        <NotMapped>
        <JsonProperty>
        Public Property CondicionesComerciales As IEnumerable(Of CondicionComercialTarifaTercero)

        <NotMapped>
        <JsonProperty>
        Public Property Paqueteria As DetallePaquteriaVentas

    End Class

End Namespace
