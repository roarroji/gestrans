﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Gesphone

    ''' <summary>
    ''' Clase que tiene la informacion de productos transportados a sincronizar.
    ''' </summary>
    <JsonObject>
    Public Class SyncProductosTransportados

        Sub New(registro As ProductoTransportados)
            Id = registro.Codigo
            Nom = registro.Nombre
            Um = registro.UnidadMedida
            Ue = registro.UnidadEmpaque
            'Lp = registro.LineaProducto
            'Np = registro.NaturalezaProducto
            At = registro.AplicaTarifario
            Ps = registro.Peso
        End Sub

        Public Property Id As Integer

        Public Property Nom As String

        Public Property Um As Integer

        Public Property Ue As Integer

        'Public Property Lp As Integer

        'Public Property Np As Integer

        Public Property At As Integer

        Public Property Ps As Integer
    End Class

End Namespace
