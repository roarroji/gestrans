﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Gesphone

    ''' <summary>
    ''' Clase que tiene la informacion de sitios cargue descargue a sincronizar.
    ''' </summary>
    <JsonObject>
    Public Class SyncSitiosCargueDescargue

        Sub New(registro As SitiosCargueDescargue)
            Id = registro.Codigo
            Nom = registro.Nombre
            Tscd = registro.CodigoTipoSitio
            Ci = registro.Ciudad.Codigo
            Dir = registro.Direccion
            Tel = registro.Telefono
            Vc = registro.ValorCargue
            Vd = registro.ValorDescargue
        End Sub

        Public Property Id As Integer

        Public Property Nom As String

        Public Property Tscd As Integer

        Public Property Ci As Integer

        Public Property Dir As String

        Public Property Tel As String

        Public Property Vc As Long

        Public Property Vd As Long

    End Class

End Namespace

