﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Gesphone

    ''' <summary>
    ''' Clase que tiene la informacion de terceros a sincronizar.
    ''' </summary>
    <JsonObject>
    Public Class SyncTerceros

        Sub New(tercero As Terceros)
            Id = tercero.Codigo
            TIde = tercero.TipoIdentificacion.Codigo
            NumIde = tercero.NumeroIdentificacion
            Nom = IIf(String.IsNullOrEmpty(tercero.Nombre), tercero.RazonSocial, tercero.Nombre)
            Ape = $"{tercero.PrimeroApellido} {tercero.SegundoApellido}".Trim()
            Ci = IIf(tercero.Ciudad Is Nothing, 0, tercero.Ciudad.Codigo)
            Dir = tercero.Direccion
            Bar = tercero.Barrio
            Tel = tercero.Telefonos
            Eml = IIf(tercero.Correo Is Nothing, String.Empty, tercero.Correo)
        End Sub

        Public Property Id As Integer

        Public Property TIde As Integer

        Public Property NumIde As String

        Public Property Nom As String

        Public Property Ape As String

        Public Property Ci As Integer

        Public Property Dir As String

        Public Property Bar As String

        Public Property Tel As String

        Public Property Eml As String

        Public Property GesDoc As List(Of SyncGestionDocumentos)

        Public Property Fp As List(Of Integer)

    End Class

End Namespace
