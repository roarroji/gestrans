﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico
Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref="DetalleOrdenTrabajoMantenimiento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleOrdenTrabajoMantenimiento
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleOrdenTrabajoMantenimiento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleOrdenTrabajoMantenimiento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            id = Read(lector, "ID")
            SistemaMantenimiento = New Sistemas With {.Nombre = Read(lector, "NombreSistema"), .Codigo = Read(lector, "SIMA_Codigo")}
            SubsistemaMantenimiento = New SubSistemas With {.Nombre = Read(lector, "NombreSubsistema"), .Codigo = Read(lector, "SUMA_Codigo")}
            UnidadReferenciaMantenimiento = New UnidadReferenciaMantenimiento With {.Nombre = Read(lector, "NombreUnidadFrecuencia"), .Codigo = Read(lector, "UNFM_Codigo"), .NombreCorto = Read(lector, "NombreCortoUnidadFrecuencia")}
            Cantidad = Read(lector, "Cantidad")
            Descripcion = Read(lector, "Descripcion")
        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property NumeroOrden As Integer
        <JsonProperty>
        Public Property id As Integer
        <JsonProperty>
        Public Property SistemaMantenimiento As Sistemas
        <JsonProperty>
        Public Property SubsistemaMantenimiento As SubSistemas
        <JsonProperty>
        Public Property UnidadReferenciaMantenimiento As UnidadReferenciaMantenimiento
        <JsonProperty>
        Public Property Cantidad As Integer
        <JsonProperty>
        Public Property Descripcion As String

    End Class
End Namespace
