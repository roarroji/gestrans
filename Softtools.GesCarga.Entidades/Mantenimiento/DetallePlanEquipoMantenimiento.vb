﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico
Imports Softtools.GesCarga.Entidades.Basico.Almacen
Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref=" DetallePlanEquipoMantenimiento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetallePlanEquipoMantenimiento
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetallePlanEquipoMantenimiento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetallePlanEquipoMantenimiento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "EPEM_Codigo")
            SistemaMantenimiento = New Sistemas With {.Codigo = Read(lector, "SIMA_Codigo"), .Nombre = Read(lector, "SistemaMantenimiento")}
            SubsistemaMantenimiento = New SubSistemas With {.Codigo = Read(lector, "SUMA_Codigo"), .Nombre = Read(lector, "SubsistemaMantenimiento"), .TipoSubsistema = New TipoSubsistemas With {.Codigo = Read(lector, "TISM_Codigo")}}
            UnidadReferenciaMantenimiento = New UnidadReferenciaMantenimiento With {.Codigo = Read(lector, "UNFM_Codigo"), .NombreCorto = Read(lector, "UnidadReferencia")}
            FrecuenciaUso = Read(lector, "Frecuencia_Uso")
            Descripcion = Read(lector, "Descripcion")
            ValorManoObra = Read(lector, "Valor_Mano_Obra")
            ValorRepuestos = Read(lector, "Valor_Repuestos")
            Cantidad = Read(lector, "Cantidad")
        End Sub

        ''' <summary>
        ''' Obtiene o establece el sistema mantenimineto del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property SistemaMantenimiento As Sistemas

        ''' <summary>
        ''' Obtiene o establece subsistema mantenimiento del detalle
        ''' </summary>
        <JsonProperty>
        Public Property SubsistemaMantenimiento As SubSistemas

        ''' <summary>
        ''' Obtiene o establece el tipo mantenimineto del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property UnidadReferenciaMantenimiento As UnidadReferenciaMantenimiento

        ''' <summary>
        ''' Obtiene o establece la descripcion del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property Descripcion As String

        ''' <summary>
        ''' Obtiene o establece el valor iva del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property ValorRepuestos As Double

        ''' <summary>
        ''' Obtiene o establece el valor total del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property ValorManoObra As Double

        ''' <summary>
        ''' Obtiene o establece el cantidad del detalle 
        ''' </summary>
        <JsonProperty>
        Public Property Cantidad As Integer
        <JsonProperty>
        Public Property FrecuenciaUso As Integer
        <JsonProperty>
        Public Property Referencias As IEnumerable(Of ReferenciaAlmacenes)
        <JsonProperty>
        Public Property CantidadAsociacion As Double

    End Class
End Namespace
