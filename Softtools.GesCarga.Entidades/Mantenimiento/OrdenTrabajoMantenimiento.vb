﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref="OrdenTrabajoMantenimiento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class OrdenTrabajoMantenimiento
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="OrdenTrabajoMantenimiento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="OrdenTrabajoMantenimiento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader, Optional obtener As Boolean = False)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            Fecha = Read(lector, "Fecha")
            Equipo = New EquipoMantenimiento With {.Codigo = Read(lector, "EQMA_Codigo"), .Nombre = Read(lector, "EquipoMantenimiento")}
            Proveedor = New Tercero With {.Codigo = Read(lector, "TERC_Proveedor"), .Nombre = Read(lector, "NombreProveedor")}
            Estado = Read(lector, "Estado")
            TotalRegistros = Read(lector, "TotalRegistros")
            If obtener Then
                ContactoEntrega = Read(lector, "Contacto_Entrega")
                FechaEntrega = Read(lector, "Fecha_Entrega")
                Observaciones = Read(lector, "Observaciones")
            End If

        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del almacen
        ''' </summary>
        <JsonProperty>
        Public Property Equipo As EquipoMantenimiento
        <JsonProperty>
        Public Property Proveedor As Tercero
        <JsonProperty>
        Public Property ContactoEntrega As String
        <JsonProperty>
        Public Property FechaEntrega As DateTime
        <JsonProperty>
        Public Property Detalles As IEnumerable(Of DetalleOrdenTrabajoMantenimiento)

    End Class
End Namespace
