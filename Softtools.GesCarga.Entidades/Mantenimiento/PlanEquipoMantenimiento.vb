﻿Imports Newtonsoft.Json

Namespace Mantenimiento

    ''' <summary>
    ''' Clase <see cref="PlanEquipoMantenimiento"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class PlanEquipoMantenimiento
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="PlanEquipoMantenimiento"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="PlanEquipoMantenimiento"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            TotalRegistros = Read(lector, "TotalRegistros")

        End Sub

        ''' <summary>
        ''' Obtiene o establece el nombre del plan de mantenimiento
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String
        ''' <summary>
        ''' Obtiene o establece la descripcion del plan de mantenimiento
        ''' </summary>
        <JsonProperty>
        Public Property Descripcion As String

        ''' <summary>
        ''' Obtiene o establece el detalle del plan de mantenimiento
        ''' </summary>
        <JsonProperty>
        Public Property Detalles As IEnumerable(Of DetallePlanEquipoMantenimiento)
        ''' <summary>
        ''' Obtiene o establece el detalle del plan de mantenimiento
        ''' </summary>
        <JsonProperty>
        Public Property DetalleEquipos As IEnumerable(Of EquipoPlanEquipoMantenimiento)


#Region "Filtros de Búsqueda"

        <JsonProperty>
        Public Property CodigoInicial As Integer

        <JsonProperty>
        Public Property CodigoFinal As Integer

#End Region



    End Class
End Namespace
