﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Paqueteria
    <JsonObject>
    Public NotInheritable Class DetalleDocumentosLegalizarRemesas
        Inherits BaseBasico
        Sub New()

        End Sub
        Sub New(lector As IDataReader)
            TipoDocumento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TDGC_Codigo"), .Nombre = Read(lector, "CATA_TDGC_Nombre")}
        End Sub
        <JsonProperty>
        Public Property NumeroRemesa As Integer
        <JsonProperty>
        Public Property NumeroLegalizarRemesa As Integer
        <JsonProperty>
        Public Property TipoDocumento As ValorCatalogos
    End Class
End Namespace
