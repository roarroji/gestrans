﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades.Despachos.OrdenCargue
Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Entidades.Despachos.Masivo
Imports Softtools.GesCarga.Entidades.Despachos
Namespace Basico.Despachos
    ''' <summary>
    ''' Clase <see cref=" DetallePrecintos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleEtiquetaPreimpresa
        Inherits BaseBasico
        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetallePrecintos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetallePrecintos"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>

        Sub New(lector As IDataReader)
            IdPreci = Read(lector, "IDPrecinto")
            CodigoEmpresa = Read(lector, "Empr_Codigo")
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "NombreOficina")}
            Numero_Precinto = Read(lector, "Numero_Etiqueta")
            EAOP_Numero = Read(lector, "EAOP_Numero")
            ENPD_Numero = Read(lector, "ENPD_Numero")
            ENMD_Numero = Read(lector, "ENMD_Numero")
            ENRE_Numero = Read(lector, "ENRE_Numero")
            Anulado = Read(lector, "Anulado")
            Estado = Read(lector, "Estado")
            TotalRegistros = Read(lector, "TotalRegistros")
            OficinaOrigen = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Origen"), .Nombre = Read(lector, "NombreOficinaOrigen")}
            TipoPrecinto = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TAPR_Codigo"), .Nombre = Read(lector, "TipoPrecinto")}
            OrdenCargue = New OrdenCargue With {.Numero = Read(lector, "ENOC_Numero"), .NumeroDocumento = Read(lector, "OrdenCargue")}
            Remesa = New Remesas With {.Numero = Read(lector, "ENRE_Numero"), .NumeroDocumento = Read(lector, "NumeroRemesa")}
            Planilla = New PlanillaDespachos With {.Numero = Read(lector, "ENPD_Numero"), .NumeroDocumento = Read(lector, "NumeroPlanillaDespacho"), .Manifiesto = New Manifiesto With {.Numero = Read(lector, "ENMC_Numero"), .NumeroDocumento = Read(lector, "NumeroMafiesto")}}
            Responsable = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Responsable"), .Nombre = Read(lector, "NombreResponsable")}
        End Sub
        <JsonProperty>
        Public Property Responsable As Tercero
        <JsonProperty>
        Public Property Planilla As PlanillaDespachos
        <JsonProperty>
        Public Property Remesa As Remesas
        <JsonProperty>
        Public Property OrdenCargue As OrdenCargue

        <JsonProperty>
        Public Property NumeroInicial As Double
        <JsonProperty>
        Public Property NumeroFinal As Double
        <JsonProperty>
        Public Property IdPreci As Double
        <JsonProperty>
        Public Property Oficina As Oficinas
        <JsonProperty>
        Public Property Numero_Precinto As Double
        <JsonProperty>
        Public Property EAOP_Numero As Double
        <JsonProperty>
        Public Property ENPD_Numero As Long
        <JsonProperty>
        Public Property ENMD_Numero As Long
        <JsonProperty>
        Public Property ENRE_Numero As Long
        <JsonProperty>
        Public Property OficinaOrigen As Oficinas
        <JsonProperty>
        Public Property Anulado As Double
        <JsonProperty>
        Public Property Usuario_anula As Usuarios
        <JsonProperty>
        Public Property Causa_anula As String
        <JsonProperty>
        Public Property Fecha_Anula As DateTime

        <JsonProperty>
        Public Property TipoPrecinto As ValorCatalogos





    End Class
End Namespace
