﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Paqueteria
    <JsonObject>
    Public Class DetalleLegalizarRecaudoRemesas
        Inherits BaseDocumento
        Sub New()
        End Sub
        Sub New(lector As IDataReader)
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            NumeroLegalizarRecaudo = Read(lector, "ELRG_Numero")
            FormaPago = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FPVE_Codigo"), .Nombre = Read(lector, "CATA_FPVE_Nombre")}
            Remitente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Remitente"), .NombreCompleto = Read(lector, "NombreRemitente")}
            Destinatario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Destinatario"), .NombreCompleto = Read(lector, "NombreDestinatario")}
            MetodoPago = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FPDC_Codigo")}
            FechaPago = Read(lector, "Fecha_Pago")
            DocumentoPago = Read(lector, "Documento_Pago")
            Valor = Read(lector, "Valor")
            Legalizo = Read(lector, "Legalizo")
            Observaciones = Read(lector, "Observaciones")
            NumeroDocumentoPlanilla = Read(lector, "ENDP_NumeroDocumento")
            NumeroDocumentoLegalizarRemesas = Read(lector, "ELGU_NumeroDocumento")
            ResponsableLegalizarRecaudo = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Responsable"), .NombreCompleto = Read(lector, "TERC_Nombre_Responsable")}
        End Sub
        <JsonProperty>
        Public Property NumeroLegalizarRecaudo As Integer
        <JsonProperty>
        Public Property NumeroDocumentoPlanilla As Integer
        <JsonProperty>
        Public Property NumeroDocumentoLegalizarRemesas As Integer
        <JsonProperty>
        Public Property FormaPago As ValorCatalogos
        <JsonProperty>
        Public Property Remitente As Terceros
        <JsonProperty>
        Public Property Destinatario As Terceros
        <JsonProperty>
        Public Property MetodoPago As ValorCatalogos
        <JsonProperty>
        Public Property FechaPago As DateTime
        <JsonProperty>
        Public Property DocumentoPago As String
        <JsonProperty>
        Public Property Valor As Double
        <JsonProperty>
        Public Property Legalizo As Integer
        <JsonProperty>
        Public Property ResponsableLegalizarRecaudo As Terceros
    End Class
End Namespace
