﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Despachos.Remesa

Namespace Paqueteria
    <JsonObject>
    Public NotInheritable Class DetalleLegalizarRemesasPaqueteria
        Inherits BaseBasico
        Sub New()

        End Sub
        Sub New(lector As IDataReader)
            Remesa = New RemesaPaqueteria With {
                .Remesa = New Remesas With {
                    .Numero = Read(lector, "Numero"),
                    .NumeroDocumento = Read(lector, "Numero_Documento"),
                    .Fecha = Read(lector, "Fecha"),
                    .Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .Nombre = Read(lector, "NombreCliente")},
                    .Remitente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Remitente"), .Nombre = Read(lector, "NombreRemitente")},
                    .CiudadRemitente = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Origen"), .Nombre = Read(lector, "CiudadOrigen")},
                    .CiudadDestinatario = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino"), .Nombre = Read(lector, "CiudadDestino")},
                    .CantidadCliente = Read(lector, "Cantidad_Cliente"),
                    .PesoCliente = Read(lector, "Peso_Cliente"),
                    .FormaPago = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FOPR_Codigo"), .Nombre = Read(lector, "FormaPago")},
                    .ValorFleteCliente = Read(lector, "Valor_Flete_Cliente"),
                    .TotalFleteCliente = Read(lector, "Total_Flete_Cliente"),
                    .Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "VEHI_Placa")}
                },
                .EstadoGuia = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESRP_Codigo"), .Nombre = Read(lector, "NombreEstadoRemesa")},
                .OficinaActual = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Actual"), .Nombre = Read(lector, "NombreOficinaActual")},
                .NumeroPlanilla = Read(lector, "NumeroDocumentoPlanilla"),
                .FechaPlanillaInicial = Read(lector, "FechaPlanilla"),
                .NumeroDocumentoLegalizarRecaudo = Read(lector, "ELRG_NumeroDocumento"),
                .GuiaCreadaRutaConductor = Read(lector, "Guia_Creada_Ruta_Conductor")
            }
            NovedadLegalizarRemesas = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NLGU_Codigo"), .Nombre = Read(lector, "CATA_NLGU_Nombre")}
            GestionDocumentos = Read(lector, "Gestion_Documentos")
            EntregaRecaudo = Read(lector, "Entrega_Recaudo")
            EntregaArchivo = Read(lector, "Entrega_Archivo")
            EntregaCartera = Read(lector, "Entrega_Cartera")
            Observaciones = Read(lector, "Observaciones")
            ValorRecaudoTercero = Read(lector, "Valor_Recaudo_Tercero")
            Legalizo = Read(lector, "Legalizo")
        End Sub
        <JsonProperty>
        Public Property NumeroLegalizarRemesas As Integer
        <JsonProperty>
        Public Property Remesa As RemesaPaqueteria
        <JsonProperty>
        Public Property NovedadLegalizarRemesas As ValorCatalogos
        <JsonProperty>
        Public Property GestionDocumentos As Integer
        <JsonProperty>
        Public Property EntregaRecaudo As Integer
        <JsonProperty>
        Public Property EntregaArchivo As Integer
        <JsonProperty>
        Public Property EntregaCartera As Integer
        <JsonProperty>
        Public Property Observaciones As String
        <JsonProperty>
        Public Property ValorRecaudoTercero As Double
        <JsonProperty>
        Public Property Legalizo As Integer
        '<JsonProperty>
        'Public Property DetalleDocumentosLegalizarRemesas As IEnumerable(Of DetalleDocumentosLegalizarRemesas)
    End Class
End Namespace

