﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Comercial.Documentos

Namespace Despachos.Paqueteria
    <JsonObject>
    Public Class DetallePlanillaEntregadas
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            Numero = Read(lector, "Numero")
            DocumentoCliente = Read(lector, "Numero_Documento")
            Cliente = Read(lector, "NombreCliente")
            CiudadDestinatario = Read(lector, "Ciudad_Destinatario")
            CiudadRemitente = Read(lector, "Ciudad_Remitente")
            Fecha = Read(lector, "Fecha")
            ValorCliente = Read(lector, "Valor_Flete_Cliente")
            CantidadCliente = Read(lector, "Cantidad_Cliente")
            PesoCliente = Read(lector, "Peso_Cliente")
            Try
                CodigoEmpresa = Read(lector, "EMPR_Codigo")
                CodigoPlanillaEntregada = Read(lector, "ENPE_Numero")
                COdigoRemesa = Read(lector, "ENRE_Numero")
                CodigoCiudad = Read(lector, "CIUD_Codigo")
                CodigoZonaCiudad = Read(lector, "ZOCI_Codigo")
                CodGuia = Read(lector, "ENRE_Numero")
                CodOficina = Read(lector, "OFIC_Codigo")
            Catch ex As Exception

            End Try

        End Sub


        <JsonProperty>
        Public Property CodigoPlanillaEntregada As Integer

        <JsonProperty>
        Public Property COdigoRemesa As Integer

        <JsonProperty>
        Public Property CodigoCiudad As Integer

        <JsonProperty>
        Public Property CodigoZonaCiudad As Integer

        <JsonProperty>
        Public Property NumeroPagina As Integer

        <JsonProperty>
        Public Property DocumentoCliente As Integer

        <JsonProperty>
        Public Property Numeracion As Integer

        <JsonProperty>
        Public Property CodGuia As Integer

        <JsonProperty>
        Public Property CodOficina As Integer

        <JsonProperty>
        Public Property ValorCliente As Integer

        <JsonProperty>
        Public Property CantidadCliente As Integer

        <JsonProperty>
        Public Property PesoCliente As Integer
        <JsonProperty>
        Public Property Cliente As String

        <JsonProperty>
        Public Property CiudadRemitente As String
        <JsonProperty>
        Public Property CiudadDestinatario As String



    End Class
End Namespace
