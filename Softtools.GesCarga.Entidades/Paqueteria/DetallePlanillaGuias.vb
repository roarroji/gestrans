﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Despachos.Remesa

Namespace Despachos.Paqueteria
    <JsonObject>
    Public Class DetallePlanillaGuias
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetallePlanillaGuias"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroPlanilla = Read(lector, "ENPR_Numero")
            Numero = Read(lector, "ENRE_Numero")
            Remesa = New Remesas With {.NumeroDocumento = Read(lector, "Numero_Documento"),
                                       .Fecha = Read(lector, "Fecha"),
                                       .Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .NombreCompleto = Read(lector, "Nombre_Cliente")},
                                       .PesoCliente = Read(lector, "Peso_Cliente"),
                                       .CantidadCliente = Read(lector, "Cantidad_Cliente"),
                                       .Observaciones = Read(lector, "Observaciones")
            }
            Estado = Read(lector, "Estado")

        End Sub

        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property NumeroPlanilla As Integer
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property Remesa As Remesas
        <JsonProperty>
        Public Property PesoCliente As Integer
        <JsonProperty>
        Public Property CantidadCliente As Integer

    End Class

End Namespace
