﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Despachos.Paqueteria
    <JsonObject>
    Public Class DetallePlanillaRecolecciones
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetallePlanillaRecolecciones"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroPlanilla = Read(lector, "ENPR_Numero")
            Numero = Read(lector, "EREC_Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            Fecha = Read(lector, "Fecha")
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .Nombre = Read(lector, "Nombre_Cliente")}
            Remitente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Remitente"), .Nombre = Read(lector, "TERC_Nombre_Remitente")}
            Zonas = New Zonas With {.Codigo = Read(lector, "Codigo_Zona"), .Nombre = Read(lector, "Nombre_Zona")}
            NombreContacto = Read(lector, "Nombre_Contacto")
            Ciudad = New Ciudades With {.Codigo = Read(lector, "Codigo_Ciudad"), .Nombre = Read(lector, "Nombre_Ciudad")}
            Barrio = Read(lector, "Barrio")
            Direccion = Read(lector, "Direccion")
            Telefonos = Read(lector, "Telefonos")
            Mercancia = Read(lector, "Mercancia")
            UnidadEmpaque = Read(lector, "Unidad_Empaque")
            Cantidad = Read(lector, "Cantidad")
            Peso = Read(lector, "Peso")
            Observaciones = Read(lector, "Observaciones")
            Estado = Read(lector, "Estado")
            Oficinas = New Oficinas With {.Codigo = Read(lector, "Codigo_Oficina"), .Nombre = Read(lector, "Nombre_Oficina")}

        End Sub

        <JsonProperty>
        Public Property Tercero As Terceros
        <JsonProperty>
        Public Property NumeroPlanilla As Integer
        <JsonProperty>
        Public Property NumeroEREC As Integer
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property Remitente As Terceros
        <JsonProperty>
        Public Property Zonas As Zonas
        <JsonProperty>
        Public Property NombreContacto As String
        <JsonProperty>
        Public Property Ciudad As Ciudades
        <JsonProperty>
        Public Property Barrio As String
        <JsonProperty>
        Public Property Direccion As String
        <JsonProperty>
        Public Property Telefonos As String
        <JsonProperty>
        Public Property Mercancia As String
        <JsonProperty>
        Public Property UnidadEmpaque As String
        <JsonProperty>
        Public Property Cantidad As Integer
        <JsonProperty>
        Public Property Peso As Integer
        <JsonProperty>
        Public Property Oficinas As Oficinas

    End Class

End Namespace
