﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Paqueteria
    <JsonObject>
    Public NotInheritable Class DetalleUnidades
        Inherits Base
        Sub New()

        End Sub
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            ID = Read(lector, "ID")
            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "PRTR_Nombre")}
            Referencia = Read(lector, "Referencia")
            Peso = Read(lector, "Peso_Cliente")
            Largo = Read(lector, "Largo")
            Alto = Read(lector, "Alto")
            Ancho = Read(lector, "Ancho")
            PesoVolumetrico = Read(lector, "Peso_Volumetrico")
            ValorComercial = Read(lector, "Valor_Comercial")
            DescripcionProducto = Read(lector, "Descripcion_Mercancia")
            NumeroUnidad = Read(lector, "Numero_Unidad")
            ValorFlete = Read(lector, "Valor_Flete")
        End Sub
        <JsonProperty>
        Public Property ID As Integer
        <JsonProperty>
        Public Property NumeroDocumentoRemesa As Integer
        <JsonProperty>
        Public Property NumeroUnidad As Integer
        <JsonProperty>
        Public Property NumeroRemesa As Integer
        <JsonProperty>
        Public Property Producto As ProductoTransportados
        <JsonProperty>
        Public Property Referencia As String
        <JsonProperty>
        Public Property Peso As Double
        <JsonProperty>
        Public Property Largo As Double
        <JsonProperty>
        Public Property Alto As Double
        <JsonProperty>
        Public Property Ancho As Double
        <JsonProperty>
        Public Property PesoVolumetrico As Double
        <JsonProperty>
        Public Property ValorComercial As Double
        <JsonProperty>
        Public Property DescripcionProducto As String
        <JsonProperty>
        Public Property ValorFlete As Double
    End Class
End Namespace

