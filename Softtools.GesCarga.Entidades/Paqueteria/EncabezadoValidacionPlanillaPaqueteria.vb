﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Imports Softtools.GesCarga.Entidades.Despachos.Planilla
Imports Softtools.GesCarga.Entidades.Tesoreria

Namespace Paqueteria
    <JsonObject>
    Public Class EncabezadoValidacionPlanillaPaqueteria
        Inherits BaseDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            Fecha = Read(lector, "Fecha")
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo")}
            Observaciones = Read(lector, "Observaciones")
            RecibidoOficina = Read(lector, "Recibido_Oficina")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            NumeroPlanilla = Read(lector, "Planilla_Despacho")
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "Vehiculo")}
            Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "Placa")}
            Conductor = New Terceros With {.NombreCompleto = Read(lector, "Conductor"), .Codigo = Read(lector, "TERC_Codigo_Conductor")}
            Tenedor = New Terceros With {.NombreCompleto = Read(lector, "Tenedor")}
            TotalRegistros = Read(lector, "TotalRegistros")
            Cantidad = Read(lector, "Cantidad")
            Peso = Read(lector, "Peso")
            Observaciones = Read(lector, "Observaciones")
            TipoPlanilla = Read(lector, "Tipo_Planilla")
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub
        <JsonProperty>
        Public Property Tenedor As Terceros
        <JsonProperty>
        Public Property Conductor As Terceros
        <JsonProperty>
        Public Property NumeroPlanilla As Long
        <JsonProperty>
        Public Property TipoPlanilla As String
        <JsonProperty>
        Public Property Cantidad As Long
        <JsonProperty>
        Public Property Peso As Double
        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Semirremolque As Semirremolques
        <JsonProperty>
        Public Overloads Property Numero As Long
        <JsonProperty>
        Public Property ListadoGuias As IEnumerable(Of DetallePlanillas)
        <JsonProperty>
        Public Property TipoVerificacion As ValorCatalogos
        <JsonProperty>
        Public Property Planilla As Planillas
        <JsonProperty>
        Public Property CuentaPorPagar As EncabezadoDocumentoCuentas
        <JsonProperty>
        Public Property ListadoPlanillaRecolecciones As IEnumerable(Of DetallePlanillaRecolecciones)
        <JsonProperty>
        Public Property ConsultaPLanillasenRuta As Short
        <JsonProperty>
        Public Property Conductores As IEnumerable(Of Terceros)
        <JsonProperty>
        Public Property NumeroLegalizacion As Integer
        <JsonProperty>
        Public Property RecibidoOficina As Integer
        <JsonProperty>
        Public Property DocumentosRelacionados As IEnumerable(Of EncabezadoDocumentos)
    End Class
End Namespace

