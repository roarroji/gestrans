﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Comercial.Documentos

Namespace Despachos.Paqueteria
    <JsonObject>
    Public Class PlanillaEntregadas
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="LineaNegocioTransportes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            FechaInicial = Read(lector, "Fecha")
            FechaFinal = Read(lector, "Fecha")
            Fecha = Read(lector, "Fecha")
            Obtener = Read(lector, "Obtener")

            If Obtener = 0 Then
                TotalRegistros = Read(lector, "TotalRegistros")
                PaginaObtener = Read(lector, "PaginaObtener")
                RegistrosPagina = Read(lector, "RegistrosPagina")
            End If
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "Placa")}
            Conductor = New Tercero With {.NombreCompleto = Read(lector, "NombreCondutor"), .Codigo = Read(lector, "TERC_Codigo_Conductor")}
            CantidadTotal = Read(lector, "Cantidad_Total")
            PesoTotal = Read(lector, "Peso_Total")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")

            CodigoOficina = Read(lector, "CodigoOficina")

            If Obtener <> 0 Then


                Try
                    Observaciones = Read(lector, "Observaciones")
                    Numeracion = Read(lector, "Numeracion")
                    Funcionario = New Tercero With {.NombreCompleto = Read(lector, "NombreFuncionario"), .Codigo = Read(lector, "TERC_Codigo_Funcionario")}

                Catch ex As Exception

                End Try



            End If


        End Sub
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Vehiculo As Vehiculos

        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property CodigoOficina As Integer
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Ruta As Rutas

        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Conductor As Tercero
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property Funcionario As Tercero
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property CantidadTotal As Integer
        ''' <summary>
        ''' Obtiene o establece el nombre del catalogo
        ''' </summary>
        <JsonProperty>
        Public Property PesoTotal As Integer

        <JsonProperty>
        Public Property PaginaObtener As Integer

        <JsonProperty>
        Public Property Numeracion As Integer

        <JsonProperty>
        Public Property ListadoPlanillaEntregada As IEnumerable(Of DetallePlanillaEntregadas)

        <JsonProperty>
        Public Property ListadoGuiasNoSeleccionadas As IEnumerable(Of DetallePlanillaEntregadas)

        <JsonProperty>
        Public Property ListadoAuxiliarPlanillaEntregada As IEnumerable(Of DetalleAuxiliaresPlanillaRecolecciones)

        <JsonProperty>
        Public Property cadenaguias As String


    End Class
End Namespace
