﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Entidades.Despachos
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Imports Softtools.GesCarga.Entidades.Despachos.Planilla
Imports Softtools.GesCarga.Entidades.Tesoreria

Namespace Paqueteria
    <JsonObject>
    Public Class PlanillaPaqueteria
        Inherits BaseDocumento
        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            Planilla = New Planillas With {
                .Numero = Read(lector, "Numero"),
                .NumeroDocumento = Read(lector, "Numero_Documento"),
                .Fecha = Read(lector, "Fecha"),
                .Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo"), .Placa = Read(lector, "VEHI_Placa")},
                .Conductor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .NombreCompleto = Read(lector, "NombreConductor")},
                .Tenedor = New Tercero With {.Codigo = Read(lector, "TERC_Codigo_Tenedor"), .NombreCompleto = Read(lector, "NombreTenedor")},
                .Semirremolque = New Semirremolques With {.Codigo = Read(lector, "SEMI_Codigo"), .Placa = Read(lector, "SEMI_Placa")},
                .FechaHoraSalida = Read(lector, "Fecha_Hora_Salida"),
                .LineaNegocioTransporte = New LineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo")},
                .TipoLineaNegocioTransportes = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "LNTC_Codigo")},
                .TarifaTransportes = New TarifaTransportes With {.Codigo = Read(lector, "TATC_Codigo"), .Nombre = Read(lector, "TATC_Nombre")},
                .TipoTarifaTransportes = New TipoTarifaTransportes With {.Codigo = Read(lector, "TTTC_Codigo"), .Nombre = Read(lector, "TTTC_Nombre")},
                .Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "RUTA_Nombre"), .TipoRuta = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIRU_Codigo")}},
                .TarifarioCompras = Read(lector, "ETCC_Numero"),
                .Cantidad = Read(lector, "Cantidad"),
                .Peso = Read(lector, "Peso"),
                .ValorFleteTransportador = Read(lector, "Valor_Flete_Transportador"),
                .ValorAnticipo = Read(lector, "Valor_Anticipo"),
                .ValorImpuestos = Read(lector, "Valor_Impuestos"),
                .ValorPagarTransportador = Read(lector, "Valor_Pagar_Transportador"),
                .ValorFleteCliente = Read(lector, "Valor_Flete_Cliente"),
                .AnticipoPagadoA = Read(lector, "Anticipo_Pagado_A"),
                .PagoAnticipo = Read(lector, "PagoAnticipo"),
                .RemesaPadre = Read(lector, "ENRE_Numero_Documento_Masivo"),
                .RemesaMasivo = Read(lector, "ENRE_Numero_Masivo"),
                .NumeroCumplido = Read(lector, "ECPD_Numero"),
                .NumeroManifiesto = Read(lector, "Manifiesto"),
                .NumeroLiquidacion = Read(lector, "ELPD_NumeroDocumento"),
                .NumeroLegalizacionGastos = Read(lector, "ELGC_NumeroDocumento"),
                .NumeroLegalizarRecaudo = Read(lector, "ELGC_NumeroDocumento")
            }
            Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo")}
            Observaciones = Read(lector, "Observaciones")
            RecibidoOficina = Read(lector, "Recibido_Oficina")
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")
            NumeroPrecintoPaqueteria = Read(lector, "Precinto_Paqueteria")

            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub

        <JsonProperty>
        Public Property TipoConsulta As Integer
        <JsonProperty>
        Public Property Planilla As Planillas
        <JsonProperty>
        Public Property CuentaPorPagar As EncabezadoDocumentoCuentas
        <JsonProperty>
        Public Property ListadoPlanillaRecolecciones As IEnumerable(Of DetallePlanillaRecolecciones)
        <JsonProperty>
        Public Property ConsultaPLanillasenRuta As Short
        <JsonProperty>
        Public Property Conductores As IEnumerable(Of Terceros)
        <JsonProperty>
        Public Property NumeroLegalizacion As Integer
        <JsonProperty>
        Public Property RecibidoOficina As Integer
        <JsonProperty>
        Public Property DocumentosRelacionados As IEnumerable(Of EncabezadoDocumentos)
        <JsonProperty>
        Public Property Manifiesto As Manifiesto
        <JsonProperty>
        Public Property NumeroPrecintoPaqueteria As Integer
    End Class
End Namespace

