﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Despachos.Paqueteria
    <JsonObject>
    Public Class PlanillaRecolecciones
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="PlanillaRecolecciones"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            FechaPlanilla = Read(lector, "Fecha_Planilla")
            PlacaVehiculo = Read(lector, "Placa_Vehiculo")
            CodigoVehiculo = Read(lector, "Codigo_Vehiculo")
            Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .Nombre = Read(lector, "Nombre_Conductor")}
            Oficinas = New Oficinas With {.Codigo = Read(lector, "Codigo_Oficina"), .Nombre = Read(lector, "Nombre_Oficina")}
            CantidadTotal = Read(lector, "Cantidad_Total")
            PesoTotal = Read(lector, "Peso_Total")
            Estado = Read(lector, "Estado")
            Observaciones = Read(lector, "Observaciones")

            Obtener = Read(lector, "Obtener")
            If Obtener = 1 Then
                TotalRegistros = Read(lector, "TotalRegistros")
            End If

        End Sub

        <JsonProperty>
        Public Property CodigoVehiculo As Integer
        <JsonProperty>
        Public Property Oficinas As Oficinas
        <JsonProperty>
        Public Property Conductor As Terceros
        <JsonProperty>
        Public Property FechaInicio As Date
        <JsonProperty>
        Public Property FechaFin As Date
        <JsonProperty>
        Public Property CantidadTotal As Integer
        <JsonProperty>
        Public Property PesoTotal As Integer
        <JsonProperty>
        Public Property NombreConductor As String
        <JsonProperty>
        Public Property FechaPlanilla As Date
        <JsonProperty>
        Public Property PlacaVehiculo As String
        <JsonProperty>
        Public Property ListadoAuxiliares As IEnumerable(Of DetalleAuxiliaresPlanillaRecolecciones)
        <JsonProperty>
        Public Property ListadoPlanillaRecolecciones As IEnumerable(Of DetallePlanillaRecolecciones)
        <JsonProperty>
        Public Property ListadoPlanillaGuias As IEnumerable(Of DetallePlanillaGuias)
        <JsonProperty>
        Public Property ListadoPlanillaRecoleccionesNoCkeck As IEnumerable(Of DetallePlanillaRecolecciones)
        <JsonProperty>
        Public Property ListadoPlanillaGuiasNoCkeck As IEnumerable(Of DetallePlanillaGuias)
        <JsonProperty>
        Public Property CadenaRecoleccion As String
        <JsonProperty>
        Public Property CadenaGuia As String

    End Class
End Namespace
