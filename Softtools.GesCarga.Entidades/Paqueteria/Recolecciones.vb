﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Entidades.Despachos.Remesa

Namespace Despachos.Paqueteria
    <JsonObject>
    Public Class Recolecciones
        Inherits BaseDocumento
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Recolecciones"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            Fecha = Read(lector, "Fecha")
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .Nombre = Read(lector, "Nombre_Cliente")}
            Remitente = New Terceros With {
                .Codigo = Read(lector, "TERC_Codigo_Remitente"),
                .Nombre = Read(lector, "Nombre_Remitente"),
                .NumeroIdentificacion = Read(lector, "Identificacion_Remitente"),
                .Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Remitente"), .Nombre = Read(lector, "CIUD_Nombre_Remitente")},
                .TipoIdentificacion = New ValorCatalogos With {.Codigo = Read(lector, "Tipo_Identificacion_Remitente")},
                .Telefonos = Read(lector, "Telefonos_Remitente"),
                .Direccion = Read(lector, "Direccion_Remitente"),
                .CorreoFacturacion = Read(lector, "Correo_Remitente")
            }
            Destinatario = New Terceros With {
                .Codigo = Read(lector, "TERC_Codigo_Destinatario"),
                .Nombre = Read(lector, "Nombre_Destinatario"),
                .NumeroIdentificacion = Read(lector, "Identificacion_Destinatario"),
                .Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destinatario"), .Nombre = Read(lector, "CIUD_Nombreo_Destinatario")},
                .TipoIdentificacion = New ValorCatalogos With {.Codigo = Read(lector, "Tipo_Identificacion_Destinatario")},
                .Telefonos = Read(lector, "Telefonos_Destinatario"),
                .Barrio = Read(lector, "Barrio_Destinatario"),
                .CodigoPostal = Read(lector, "Codigo_Postal_Destinatario"),
                .Direccion = Read(lector, "Direccion_Destinatario")
            }
            FechaEntrega = Read(lector, "Fecha_Entrega")
            HorarioEntrega = New ValorCatalogos With {.Codigo = Read(lector, "CATA_HERP_Codigo")}
            FechaRecoleccion = Read(lector, "Fecha_Recoleccion")
            NombreContacto = Read(lector, "Nombre_Contacto")
            Ciudad = New Ciudades With {.Codigo = Read(lector, "Codigo_Ciudad"), .Nombre = Read(lector, "Nombre_Ciudad")}
            Zonas = New Zonas With {.Codigo = Read(lector, "Codigo_Zona"), .Nombre = Read(lector, "Nombre_Zona")}
            Direccion = Read(lector, "Direccion")
            Telefonos = Read(lector, "Telefonos")
            OficinaGestiona = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Gestiona"), .Nombre = Read(lector, "OficinaGestiona")}
            Barrio = Read(lector, "Barrio")
            Latitud = Read(lector, "Latitud")
            Longitud = Read(lector, "Longitud")
            FormaPago = New ValorCatalogos With {.Codigo = Read(lector, "FPVE_Codigo"), .Nombre = Read(lector, "FPVE_Nombre")}
            DetalleTarifaVenta = New DetalleTarifarioVentas With {.Codigo = Read(lector, "DTCV_Codigo")}
            DetalleTarifaVenta.TipoLineaNegocioTransportes = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo")}
            ProductoTransportado = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "PRTR_Nombre")}
            Mercancia = Read(lector, "Mercancia")
            CodigoUnidadEmpaque = Read(lector, "UEPT_Codigo")
            UnidadEmpaque = Read(lector, "Unidad_Empaque")
            Cantidad = Read(lector, "Cantidad")
            Peso = Read(lector, "Peso")
            Largo = Read(lector, "Largo")
            Ancho = Read(lector, "Ancho")
            Alto = Read(lector, "Alto")
            PesoVolumetrico = Read(lector, "Peso_Volumetrico")
            PesoCobrar = Read(lector, "Peso_A_Cobrar")
            ValorComercial = Read(lector, "Valor_Comercial_Cliente")
            ValorFleteCliente = Read(lector, "Valor_Flete_Cliente")
            ValorManejoCliente = Read(lector, "Valor_Manejo_Cliente")
            ValorSeguroCliente = Read(lector, "Valor_Seguro_Cliente")
            TotalFleteCliente = Read(lector, "Total_Flete_Cliente")
            Observaciones = Read(lector, "Observaciones")
            Oficinas = New Oficinas With {.Codigo = Read(lector, "Codigo_Oficina"), .Nombre = Read(lector, "Nombre_Oficina")}
            Estado = Read(lector, "Estado")
            Anulado = Read(lector, "Anulado")

            EstadoRecoleccion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_EREP_Codigo"), .Nombre = Read(lector, "CATA_EREP_Nombre")}
            Secuencia = Read(lector, "Secuencia")
            NovedadRecoleccion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NORP_Codigo")}
            Firma = Read(lector, "Firma_Recibe")
            NombreZona = Read(lector, "Nombre_Zona")
            NombreOficina = Read(lector, "Nombre_Oficina")
            NumeroPlanilla = Read(lector, "Numero_Planilla")
            FechaPlanilla = Read(lector, "Fecha_Planilla")
            PlacaVehiculo = Read(lector, "Placa_Vehiculo")

            CodigoTipoTarifaTransporte = Read(lector, "TTTC_Codigo")
            NombreTipoTarifa = Read(lector, "NombreTipoTarifa")
            CodigoTarifaTransporte = Read(lector, "TATC_Codigo")
            NombreTarifa = Read(lector, "NombreTarifa")

            TotalRegistros = Read(lector, "TotalRegistros")
            IdentificacionContacto = Read(lector, "Identificacion_Contacto")

        End Sub

        <JsonProperty>
        Public Property Plantilla As Byte()
        <JsonProperty>
        Public Property IdentificacionContacto As Long
        <JsonProperty>
        Public Property Version1 As Short
        <JsonProperty>
        Public Property OficinaGestiona As Oficinas
        <JsonProperty>
        Public Property Ciudad As Ciudades
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property FechaInicio As Date
        <JsonProperty>
        Public Property FechaRecoleccion As DateTime
        <JsonProperty>
        Public Property FechaEntrega As DateTime
        <JsonProperty>
        Public Property HorarioEntrega As ValorCatalogos
        <JsonProperty>
        Public Property FechaFin As Date
        <JsonProperty>
        Public Property Oficinas As Oficinas
        <JsonProperty>
        Public Property Zonas As Zonas
        <JsonProperty>
        Public Property NombreContacto As String
        <JsonProperty>
        Public Property NombreOficina As String
        <JsonProperty>
        Public Property NombreZona As String
        <JsonProperty>
        Public Property Barrio As String
        <JsonProperty>
        Public Property Direccion As String
        <JsonProperty>
        Public Property Telefonos As String
        <JsonProperty>
        Public Property Mercancia As String
        <JsonProperty>
        Public Property CodigoUnidadEmpaque As Integer
        <JsonProperty>
        Public Property UnidadEmpaque As String
        <JsonProperty>
        Public Property Cantidad As Double
        <JsonProperty>
        Public Property Peso As Double
        <JsonProperty>
        Public Property NumeroPlanilla As Integer
        <JsonProperty>
        Public Property FechaPlanilla As Date
        <JsonProperty>
        Public Property PlacaVehiculo As String
        <JsonProperty>
        Public Property NombreCliente As String
        <JsonProperty>
        Public Property NombreRemitente As String
        <JsonProperty>
        Public Property NombreCiudad As String
        <JsonProperty>
        Public Property CodigoCiudadPlanillaRecoleccion As Integer
        <JsonProperty>
        Public Property CodigoZonaPlanillaRecoleccion As Integer
        <JsonProperty>
        Public Property CodigoOficinaPlanillaRecoleccion As Integer

        <JsonProperty>
        Public Property Recolecciones As IEnumerable(Of Recolecciones)

        <JsonProperty>
        Public Property AsociarRecoleccionPlanilla As Integer
        <JsonProperty>
        Public Property Latitud As Double
        <JsonProperty>
        Public Property Longitud As Double

        <JsonProperty>
        Public Property Remitente As Terceros
        <JsonProperty>
        Public Property Destinatario As Terceros

        <JsonProperty>
        Public Property Planilla As Long
        <JsonProperty>
        Public Property EstadoRecoleccion As ValorCatalogos
        <JsonProperty>
        Public Property Secuencia As Long

        <JsonProperty>
        Public Property NovedadRecoleccion As ValorCatalogos

        <JsonProperty>
        Public Property Firma As Byte()
        <JsonProperty>
        Public Property Fotografias As IEnumerable(Of FotoDetalleDistribucionRemesas)
        <JsonProperty>
        Public Property DevolucionRemesa As Short

        <JsonProperty>
        Public Property FormaPago As ValorCatalogos
        <JsonProperty>
        Public Property DetalleTarifaVenta As DetalleTarifarioVentas
        <JsonProperty>
        Public Property ProductoTransportado As ProductoTransportados
        <JsonProperty>
        Public Property Largo As Double
        <JsonProperty>
        Public Property Alto As Double
        <JsonProperty>
        Public Property Ancho As Double
        <JsonProperty>
        Public Property PesoVolumetrico As Double
        <JsonProperty>
        Public Property PesoCobrar As Double
        <JsonProperty>
        Public Property ValorComercial As Double
        <JsonProperty>
        Public Property ValorFleteCliente As Double
        <JsonProperty>
        Public Property ValorManejoCliente As Double
        <JsonProperty>
        Public Property ValorSeguroCliente As Double
        <JsonProperty>
        Public Property TotalFleteCliente As Double

        <JsonProperty>
        Public Property CodigoTipoTarifaTransporte As Integer
        <JsonProperty>
        Public Property NombreTipoTarifa As String
        <JsonProperty>
        Public Property CodigoTarifaTransporte As Integer
        <JsonProperty>
        Public Property NombreTarifa As String
        <JsonProperty>
        Public Property LineaNegocioPaqueteria As ValorCatalogos

    End Class
End Namespace
