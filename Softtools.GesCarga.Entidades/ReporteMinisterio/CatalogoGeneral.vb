﻿
'Imports System.Data
'Imports System.Data.SqlClient
'Imports System

'Namespace Entidades.ReporteMinisterio

'    Public Class CatalogoGeneral
'#Region "Declaracion Variables"

'        Public strError As String

'        Private objGeneral As General
'        Private objEmpresa As Empresas
'        Private strCodigo As String
'        Private strNombre As String
'        Private intActualizable As Integer
'        Private intNumeroColumnas As Integer
'        Private intCamposLlave As Integer

'#End Region

'#Region "Constructor"

'        Sub New()
'            Try
'                Me.objGeneral = New General
'                Me.objEmpresa = New Empresas(0)
'                Me.strCodigo = ""
'                Me.strNombre = ""
'                Me.intActualizable = 0
'                Me.intNumeroColumnas = 0
'                Me.intCamposLlave = 0
'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la página " & Me.ToString & " en la función New: " & ex.Message.ToString()
'            End Try

'        End Sub

'        Sub New(ByRef Empresa As Empresas)
'            Try
'                Me.objGeneral = New General
'                Me.objEmpresa = Empresa
'                Me.strCodigo = ""
'                Me.strNombre = ""
'                Me.intActualizable = 0
'                Me.intNumeroColumnas = 0
'                Me.intCamposLlave = 0
'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la página " & Me.ToString & " en la función New: " & ex.Message.ToString()
'            End Try

'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property Actualizable() As Integer
'            Get
'                Return Me.intActualizable
'            End Get
'            Set(ByVal value As Integer)
'                Me.intActualizable = value
'            End Set
'        End Property

'        Public Property NumeroColumnas() As Integer
'            Get
'                Return Me.intNumeroColumnas
'            End Get
'            Set(ByVal value As Integer)
'                Me.intNumeroColumnas = value
'            End Set
'        End Property

'        Public Property CamposLlave() As Integer
'            Get
'                Return Me.intCamposLlave
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCamposLlave = value
'            End Set
'        End Property

'        Public Property Empresa() As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(ByVal value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property

'        Public Property General() As General
'            Get
'                Return Me.objGeneral
'            End Get
'            Set(ByVal value As General)
'                Me.objGeneral = value
'            End Set
'        End Property

'        Public Property Codigo() As String
'            Get
'                Return Me.strCodigo
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigo = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean
'            Try
'                Consultar = True
'                Dim strSQL As String
'                Dim sdrCatalogoGeneral As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre, Actualizable, Numero_Columnas, Campos_Llave FROM Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = '" & Me.strCodigo & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogoGeneral = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogoGeneral.Read() Then
'                    Me.strNombre = sdrCatalogoGeneral("Nombre").ToString()
'                    Me.intActualizable = Val(sdrCatalogoGeneral("Actualizable").ToString())
'                    Me.intNumeroColumnas = Val(sdrCatalogoGeneral("Numero_Columnas").ToString())
'                    Me.intCamposLlave = Val(sdrCatalogoGeneral("Campos_Llave").ToString())
'                    Consultar = True
'                Else
'                    Me.strCodigo = ""
'                    Consultar = False
'                End If

'                sdrCatalogoGeneral.Close()
'                sdrCatalogoGeneral.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCodigo = ""

'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar = False

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Codigo As String) As Boolean
'            Try
'                Consultar = True
'                Me.Empresa = Empresa
'                Me.strCodigo = Codigo

'                Dim strSQL As String
'                Dim sdrCatalogoGeneral As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre, Actualizable, Numero_Columnas, Campos_Llave FROM Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = '" & Me.strCodigo & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
'                Me.objGeneral.ConexionSQL.Open()
'                sdrCatalogoGeneral = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrCatalogoGeneral.Read() Then

'                    Me.strNombre = sdrCatalogoGeneral("Nombre").ToString()
'                    Me.intActualizable = Val(sdrCatalogoGeneral("Actualizable").ToString())
'                    Me.intNumeroColumnas = Val(sdrCatalogoGeneral("Numero_Columnas").ToString())
'                    Me.intCamposLlave = Val(sdrCatalogoGeneral("Campos_Llave").ToString())

'                    Consultar = True
'                Else
'                    Me.strCodigo = ""
'                    Consultar = False
'                End If

'                sdrCatalogoGeneral.Close()
'                sdrCatalogoGeneral.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCodigo = ""

'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'                Consultar = False

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'#End Region

'    End Class


'End Namespace

