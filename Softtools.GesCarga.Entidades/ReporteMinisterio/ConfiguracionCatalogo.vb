﻿'Imports System.Data
'Imports System.Data.SqlClient
'Imports System

'Namespace Entidades.ReporteMinisterio
'    Public Class ConfiguracionCatalogo

'#Region "Declaracion Variables"

'        Public strError As String

'        Private objGeneral As General
'        Private objEmpresa As Empresas
'        Private objCatalogoGeneral As CatalogoGeneral
'        Private intSecuencia As Integer
'        Private strNombre As String
'        Private intTipo As Integer
'        Private intLongitudCampo As Integer
'        Private intObligatorio As Integer
'        Private intNumeroDecimales As Integer
'        Private intLongitudColumna As Integer

'#End Region

'#Region "Constructor"

'        Sub New()
'            Try

'                Me.objGeneral = New General
'                Me.objEmpresa = New Empresas
'                Me.objCatalogoGeneral = New CatalogoGeneral(Me.objEmpresa)
'                Me.intSecuencia = 0
'                Me.strNombre = ""
'                Me.intTipo = 0
'                Me.intLongitudCampo = 0
'                Me.intObligatorio = 0
'                Me.intNumeroDecimales = 0
'                Me.intLongitudColumna = 0

'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la página " & Me.ToString & " en la función New: " & ex.Message.ToString()
'            End Try

'        End Sub

'        Sub New(ByRef Empresa As Empresas)
'            Try
'                Me.objEmpresa = Empresa
'                Me.objGeneral = New General
'                Me.objCatalogoGeneral = New CatalogoGeneral(Me.objEmpresa)
'                Me.intSecuencia = 0
'                Me.strNombre = ""
'                Me.intTipo = 0
'                Me.intLongitudCampo = 0
'                Me.intObligatorio = 0
'                Me.intNumeroDecimales = 0
'                Me.intLongitudColumna = 0
'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la página " & Me.ToString & " en la función New: " & ex.Message.ToString()
'            End Try

'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property LongitudCampo() As Integer
'            Get
'                Return Me.intLongitudCampo
'            End Get
'            Set(ByVal value As Integer)
'                Me.intLongitudCampo = value
'            End Set
'        End Property

'        Public Property LongitudColumna() As Integer
'            Get
'                Return Me.intLongitudColumna
'            End Get
'            Set(ByVal value As Integer)
'                Me.intLongitudColumna = value
'            End Set
'        End Property

'        Public Property NumeroDecimales() As Integer
'            Get
'                Return Me.intNumeroDecimales
'            End Get
'            Set(ByVal value As Integer)
'                Me.intNumeroDecimales = value
'            End Set
'        End Property

'        Public Property Obligatorio() As Integer
'            Get
'                Return Me.intObligatorio
'            End Get
'            Set(ByVal value As Integer)
'                Me.intObligatorio = value
'            End Set
'        End Property

'        Public Property Secuencia() As Integer
'            Get
'                Return Me.intSecuencia
'            End Get
'            Set(ByVal value As Integer)
'                Me.intSecuencia = value
'            End Set
'        End Property

'        Public Property Tipo() As Integer
'            Get
'                Return Me.intTipo
'            End Get
'            Set(ByVal value As Integer)
'                Me.intTipo = value
'            End Set
'        End Property

'        Public Property CatalogoGeneral() As CatalogoGeneral
'            Get
'                Return Me.objCatalogoGeneral
'            End Get
'            Set(ByVal value As CatalogoGeneral)
'                Me.objCatalogoGeneral = value
'            End Set
'        End Property

'        Public Property Empresa() As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(ByVal value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property

'        Public Property General() As General
'            Get
'                Return Me.objGeneral
'            End Get
'            Set(ByVal value As General)
'                Me.objGeneral = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean

'            Consultar = True
'            Dim strSQL As String
'            Dim sdrConfiguracionCatalogo As SqlDataReader
'            Dim ComandoSQL As SqlCommand

'            strSQL = "SELECT * FROM Configuracion_Catalogos"
'            strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'            strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'            strSQL += " AND Secuencia = " & Me.intSecuencia

'            ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'            Try
'                Me.objGeneral.ConexionSQL.Open()
'                sdrConfiguracionCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConfiguracionCatalogo.Read() Then

'                    Me.strNombre = sdrConfiguracionCatalogo("Nombre").ToString()
'                    Me.intTipo = Val(sdrConfiguracionCatalogo("Tipo").ToString())
'                    Me.intLongitudCampo = sdrConfiguracionCatalogo("Longitud_Campo").ToString()
'                    Me.intObligatorio = Val(sdrConfiguracionCatalogo("Obligatorio").ToString())
'                    Me.intNumeroDecimales = Val(sdrConfiguracionCatalogo("Numero_Decimales").ToString())

'                    Me.intLongitudColumna = Val(sdrConfiguracionCatalogo("Longitud_Grid").ToString())

'                End If

'                sdrConfiguracionCatalogo.Close()
'                sdrConfiguracionCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la página " & Me.ToString & " en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal CatalogoGeneral As CatalogoGeneral, ByVal Columna As Integer) As Boolean
'            Try
'                Consultar = True
'                Me.objEmpresa = Empresa
'                Me.objCatalogoGeneral = CatalogoGeneral
'                Me.intSecuencia = Columna

'                Dim strSQL As String
'                Dim sdrConfiguracionCatalogo As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT * FROM Configuracion_Catalogos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND CATA_Codigo = '" & Me.objCatalogoGeneral.Codigo & "'"
'                strSQL += " AND Secuencia = " & Me.intSecuencia

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)
'                Me.objGeneral.ConexionSQL.Open()
'                sdrConfiguracionCatalogo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrConfiguracionCatalogo.Read() Then

'                    Me.strNombre = sdrConfiguracionCatalogo("Nombre").ToString()
'                    Me.intTipo = Val(sdrConfiguracionCatalogo("Tipo").ToString())
'                    Me.intLongitudCampo = sdrConfiguracionCatalogo("Longitud_Campo").ToString()
'                    Me.intObligatorio = Val(sdrConfiguracionCatalogo("Obligatorio").ToString())
'                    Me.intNumeroDecimales = Val(sdrConfiguracionCatalogo("Numero_Decimales").ToString())

'                    Me.intLongitudColumna = Val(sdrConfiguracionCatalogo("Longitud_Grid").ToString())

'                End If

'                sdrConfiguracionCatalogo.Close()
'                sdrConfiguracionCatalogo.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strError = "Sucedió un error en la página " & Me.ToString & " en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'#End Region

'    End Class

'End Namespace
