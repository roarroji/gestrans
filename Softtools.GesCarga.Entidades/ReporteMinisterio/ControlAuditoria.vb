﻿'Imports System.Data
'Imports System.Data.SqlClient
'Imports System.IO
'Imports System.Xml
'Imports System.Xml.Serialization

'Namespace Entidades.ReporteMinisterio
'    Public Class ControlAuditoria
'#Region "Declaracion Variables"

'        Private intCodigoEmpresa As Integer
'        Private strCodigoUsuario As String
'        Private strNombreTabla As String
'        Private strNumeroDocumentoEncabezado As String
'        Private strNumeroDocumentoDetalle As String
'        Private strXMLAuditoria As String
'        Private lonCodigoMenuAplicacion As Long
'        Private intTipoTransaccion As Short
'        Private strError As String
'        Private objGeneral As General

'#End Region


'#Region "Declaracion Constantes"

'        'Constantes de control auditoria
'        Public Const TRANSACCION_INSERTAR As Short = 1
'        Public Const TRANSACCION_MODIFICAR As Short = 2
'        Public Const TRANSACCION_ELIMINAR As Short = 3

'#End Region

'#Region "Constructor"

'        Sub New()

'            Me.intCodigoEmpresa = 0
'            Me.strCodigoUsuario = ""
'            Me.strNombreTabla = ""
'            Me.strNumeroDocumentoEncabezado = ""
'            Me.strNumeroDocumentoDetalle = General.CERO_STRING
'            Me.strXMLAuditoria = ""
'            Me.lonCodigoMenuAplicacion = 0
'            Me.intTipoTransaccion = 0
'            Me.objGeneral = New General

'        End Sub

'#End Region

'#Region "Atributos"

'        Public Property CodigoEmpresa() As Integer
'            Get
'                Return Me.intCodigoEmpresa
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCodigoEmpresa = value
'            End Set
'        End Property
'        Public Property CodigoUsuario() As String
'            Get
'                Return Me.strCodigoUsuario
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigoUsuario = value
'            End Set
'        End Property
'        Public Property NombreTabla() As String
'            Get
'                Return Me.strNombreTabla
'            End Get
'            Set(ByVal value As String)
'                Me.strNombreTabla = value
'            End Set
'        End Property
'        Public Property NumeroDocumentoEncabezado() As String
'            Get
'                Return Me.strNumeroDocumentoEncabezado
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroDocumentoEncabezado = value
'            End Set
'        End Property
'        Public Property NumeroDocumentoDetalle() As String
'            Get
'                Return Me.strNumeroDocumentoDetalle
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroDocumentoDetalle = value
'            End Set
'        End Property
'        Public Property CodigoMenuAplicacion() As Long
'            Get
'                Return Me.lonCodigoMenuAplicacion
'            End Get
'            Set(ByVal value As Long)
'                Me.lonCodigoMenuAplicacion = value
'            End Set
'        End Property

'        Public Property TipoTransaccion() As Short
'            Get
'                Return Me.intTipoTransaccion
'            End Get
'            Set(ByVal value As Short)
'                Me.intTipoTransaccion = value
'            End Set
'        End Property

'        Public Property XmlAuditoria As String
'            Get
'                Return Me.strXMLAuditoria
'            End Get
'            Set(value As String)
'                Me.strXMLAuditoria = value
'            End Set
'        End Property

'        Public Property MensajeError As String
'            Get
'                Return Me.strError
'            End Get
'            Set(value As String)
'                Me.strError = value
'            End Set
'        End Property


'#End Region

'#Region "Funciones Publicas"

'        Public Function Insertar_Control_Auditoria_SQL(ByVal arrParametros As SqlParameterCollection, ByVal NombreTabla As String, ByVal TipoTransaccion As Short, ByRef Mensaje As String) As Boolean

'            Try
'                Dim lonNumeRegi As Long

'                Me.XmlAuditoria = Me.objGeneral.Retorna_Xml_Auditoria_Desde_Parametros_SP(arrParametros, NombreTabla, Mensaje)
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_control_auditorias", Me.objGeneral.ConexionSQL)
'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.CodigoEmpresa
'                ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.CodigoUsuario
'                ComandoSQL.Parameters.Add("@par_TTAU_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_TTAU_Codigo").Value = TipoTransaccion
'                ComandoSQL.Parameters.Add("@par_MEAP_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_MEAP_Codigo").Value = Me.lonCodigoMenuAplicacion
'                ComandoSQL.Parameters.Add("@par_Nombre_Tabla", SqlDbType.VarChar, 150) : ComandoSQL.Parameters("@par_Nombre_Tabla").Value = NombreTabla
'                ComandoSQL.Parameters.Add("@par_Numero_Documento_Encabezado", SqlDbType.VarChar, 15) : ComandoSQL.Parameters("@par_Numero_Documento_Encabezado").Value = Me.strNumeroDocumentoEncabezado
'                ComandoSQL.Parameters.Add("@par_Numero_Documento_Detalle", SqlDbType.VarChar, 15) : ComandoSQL.Parameters("@par_Numero_Documento_Detalle").Value = Me.strNumeroDocumentoDetalle

'                ComandoSQL.Parameters.Add("@par_InformacionXML", SqlDbType.Xml) : ComandoSQL.Parameters("@par_InformacionXML").Value = Me.strXMLAuditoria


'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_Control_Auditoria_SQL = True
'                Else
'                    Insertar_Control_Auditoria_SQL = False
'                End If

'            Catch ex As Exception
'                Insertar_Control_Auditoria_SQL = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'#End Region
'    End Class
'End Namespace