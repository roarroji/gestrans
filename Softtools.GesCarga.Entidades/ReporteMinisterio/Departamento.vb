﻿
'Imports System.Data
'Imports System.Data.SqlClient

'Namespace Entidades.ReporteMinisterio

'    Public Class Departamento
'#Region "Declaracion Variables"

'        Dim strSQL As String

'        Public strError As String
'        Public bolModificar As Boolean
'        Private intCodigo As Integer
'        Private strNombre As String

'        Private objEmpresa As Empresas
'        Private objCataRegion As Catalogo
'        Private strCodigoDane As String
'        Private objGeneral As General
'        Private objPais As Pais

'#End Region

'#Region "Constructor"

'        Sub New()
'            Me.objEmpresa = New Empresas(0)
'            Me.intCodigo = 0
'            Me.objCataRegion = New Catalogo(Me.objEmpresa, "REGE")
'            Me.strCodigoDane = ""
'            Me.objGeneral = New General
'            Me.strNombre = ""
'            Me.objPais = New Pais
'        End Sub

'        Sub New(ByRef Empresa As Empresas)
'            Me.objEmpresa = Empresa
'            Me.intCodigo = 0
'            Me.objCataRegion = New Catalogo(Me.objEmpresa, "REGE")
'            Me.strCodigoDane = ""
'            Me.objGeneral = New General
'            Me.strNombre = ""
'            Me.objPais = New Pais
'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property Codigo() As Integer
'            Get
'                Return Me.intCodigo
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCodigo = value
'            End Set
'        End Property

'        Public Property CataRegion() As Catalogo
'            Get
'                Return Me.objCataRegion
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataRegion = value
'            End Set
'        End Property

'        Public Property Empresa() As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(ByVal value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property

'        Public Property CodigoDane() As String
'            Get
'                Return Me.strCodigoDane
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigoDane = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'        Public Property Pais() As Pais
'            Get
'                Return objPais
'            End Get
'            Set(ByVal value As Pais)
'                Me.objPais = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean

'            Try

'                Me.objEmpresa = Empresa
'                Me.intCodigo = Codigo
'                Dim sdrDepartamento As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre, REGE_Codigo, Codigo_DANE, PAIS_Codigo FROM Departamentos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrDepartamento = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrDepartamento.Read() Then
'                    Me.strNombre = sdrDepartamento("Nombre").ToString()
'                    Me.objCataRegion.Campo1 = (sdrDepartamento("REGE_Codigo").ToString)
'                    Me.strCodigoDane = sdrDepartamento("Codigo_DANE").ToString
'                    Me.objPais.Codigo = sdrDepartamento("PAIS_Codigo").ToString
'                    Consultar = True
'                Else
'                    Me.intCodigo = 0
'                    Consultar = False
'                End If

'                sdrDepartamento.Close()
'                sdrDepartamento.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Consultar(ByVal Empresa As Empresas) As Boolean
'            Try

'                Me.objEmpresa = Empresa
'                Me.intCodigo = Codigo
'                Dim sdrDepartamento As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre, REGE_Codigo, Codigo_DANE, PAIS_Codigo FROM Departamentos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrDepartamento = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrDepartamento.Read() Then
'                    Me.strNombre = sdrDepartamento("Nombre").ToString()
'                    Me.objCataRegion.Campo1 = sdrDepartamento("REGE_Codigo").ToString
'                    Me.strCodigoDane = sdrDepartamento("Codigo_DANE").ToString
'                    Me.objPais.Codigo = sdrDepartamento("PAIS_Codigo").ToString

'                    Consultar = True
'                Else
'                    Me.intCodigo = 0
'                    Consultar = False
'                End If

'                sdrDepartamento.Close()
'                sdrDepartamento.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Codigo As Integer) As Boolean
'            Try

'                Me.objEmpresa = Empresa
'                Me.intCodigo = Codigo
'                Dim sdrDepartamento As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre, REGE_Codigo, Codigo_DANE, PAIS_Codigo FROM Departamentos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrDepartamento = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrDepartamento.Read() Then
'                    Me.strNombre = sdrDepartamento("Nombre").ToString()
'                    Me.objCataRegion.Campo1 = sdrDepartamento("REGE_Codigo").ToString
'                    Me.strCodigoDane = sdrDepartamento("Codigo_DANE").ToString
'                    Me.objPais.Codigo = sdrDepartamento("PAIS_Codigo").ToString

'                    Consultar = True
'                Else
'                    Me.intCodigo = 0
'                    Consultar = False
'                End If

'                sdrDepartamento.Close()
'                sdrDepartamento.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro() As Boolean
'            Try
'                Dim sdrDepartamento As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo FROM Departamentos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()
'                sdrDepartamento = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrDepartamento.Read() Then
'                    Existe_Registro = True
'                Else
'                    Me.intCodigo = 0
'                    Existe_Registro = False
'                End If
'                sdrDepartamento.Close()
'                sdrDepartamento.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Existe_Registro = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas) As Boolean
'            Try
'                Me.objEmpresa = Empresa
'                Me.intCodigo = Codigo

'                Dim sdrDepartamento As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Codigo, Nombre FROM Paises"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True

'                Me.objGeneral.ConexionSQL.Open()
'                sdrDepartamento = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrDepartamento.Read() Then
'                    Me.strNombre = sdrDepartamento("Nombre").ToString()
'                    Me.intCodigo = Val(sdrDepartamento("Codigo").ToString())
'                    Existe_Registro = True
'                Else
'                    Me.intCodigo = 0
'                    Existe_Registro = False
'                End If

'                sdrDepartamento.Close()
'                sdrDepartamento.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Existe_Registro = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Codigo As Integer) As Boolean
'            Try
'                Me.objEmpresa = Empresa
'                Me.intCodigo = Codigo

'                Dim sdrDepartamento As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Codigo, Nombre FROM Paises"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True

'                Me.objGeneral.ConexionSQL.Open()
'                sdrDepartamento = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrDepartamento.Read() Then
'                    Me.strNombre = sdrDepartamento("Nombre").ToString()
'                    Me.intCodigo = Val(sdrDepartamento("Codigo").ToString())
'                    Existe_Registro = True
'                Else
'                    Me.intCodigo = 0
'                    Existe_Registro = False
'                End If

'                sdrDepartamento.Close()
'                sdrDepartamento.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Existe_Registro = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Guardar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                If Datos_Requeridos(Mensaje) Then
'                    If Not bolModificar Then
'                        Guardar = Insertar_SQL()
'                    Else
'                        Guardar = Modificar_SQL()
'                    End If
'                Else
'                    Guardar = False
'                End If
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Guardar = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                Guardar = False
'            End Try

'        End Function

'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Eliminar = Borrar_SQL()
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                Eliminar = False
'            End Try
'        End Function

'#End Region

'#Region "Funciones Privadas"

'        Private Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Me.strError = ""
'                Dim intCont As Integer = 0
'                Datos_Requeridos = True

'                If Me.strNombre = "" Then
'                    intCont += 1
'                    strError += intCont & ". Debe digitar un nombre para el departamento"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If
'                If Me.intCodigo = 0 Then
'                    intCont += 1
'                    strError += intCont & ". Debe digitar un código válido para el departamento"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.strNombre <> "" And Len(Me.strNombre) < General.MINIMO_CUATRO_CARACTERES_INGRESO_NOMBRE Then
'                    intCont += 1
'                    strError += intCont & ". El nombre del Departamento no puede tener menos de cuatro (4) carácteres"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If


'            Catch ex As Exception
'                Me.intCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & Me.objGeneral.Traducir_Error(ex.Message)
'                Datos_Requeridos = False
'            End Try

'        End Function

'        Private Function Borrar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_departamentos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.intCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                Else
'                    Borrar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Borrar_SQL = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Insertar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_departamentos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.intCodigo
'                ComandoSQL.Parameters.Add("@par_REGE_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_REGE_Codigo").Value = Me.objCataRegion.Campo1
'                ComandoSQL.Parameters.Add("@par_PAIS_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_PAIS_Codigo").Value = Me.objPais.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo_DANE", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Codigo_DANE").Value = Me.strCodigoDane

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_SQL = True
'                Else
'                    Insertar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Insertar_SQL = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Modificar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_departamentos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.intCodigo
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_REGE_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_REGE_Codigo").Value = Me.objCataRegion.Campo1
'                ComandoSQL.Parameters.Add("@par_PAIS_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_PAIS_Codigo").Value = Me.objPais.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo_DANE", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Codigo_DANE").Value = Me.strCodigoDane

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Modificar_SQL = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'#End Region
'    End Class

'End Namespace