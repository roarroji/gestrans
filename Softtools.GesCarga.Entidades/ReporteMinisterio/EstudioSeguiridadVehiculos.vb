﻿'Imports System.Data
'Imports System.Data.SqlClient


'Namespace Entidades.ReporteMinisterio
'    Public Class EstudioSeguiridadVehiculos

'#Region "Declaracion Variables"

'        Dim strSQL As String
'        Dim objGeneral As General
'        Public strError As String
'        Public bolModificar As Boolean
'        Public intAprobar As Integer

'        Private objVehiculo As Vehiculos
'        Private objEmpresa As Empresas
'        Private objCataTipoIdentificacion As Catalogo


'        Private lonNumero As Long
'        Private dteFecha As Date
'        Private strPlaca As String

'        Private lonMarca As Long
'        Private intModelo As Integer
'        Private lonColor As Long
'        Private strClase As String
'        Private strSemirremolque As String

'        Private intModeloRepotenciado As Integer
'        Private strEmpresaAfiliadora As String
'        Private strTelefonoEmpresaAfiliadora As String
'        Private strIdentificacionPropietario As String
'        Private strNombrePropietario As String

'        Private objCiudadPropietario As Ciudad
'        Private strTelefonoPropietario As String
'        Private strIdentificacionTenedor As String
'        Private strNombreTenedor As String
'        Private objCiudadTenedor As Ciudad

'        'Propietario Remolque
'        Private objCiudadPropietarioRemolque As Ciudad
'        Private strTelefonoPropietarioRemolque As String
'        Private strIdentificacionPropietarioRemolque As String
'        Private strNombrePropietarioRemolque As String
'        Private strConfirmoPropietarioRemolque As String
'        Private objCiudadResidenciaPropietarioRemolque As Ciudad
'        Private strCelularPropietarioRemolque As String
'        Private strDireccionPropietarioRemolque As String


'        Private strTelefonoTenedor As String
'        Private strIdentificacionConductor As String
'        Private strNombreConductor As String
'        Private objCiudadConductor As Ciudad
'        Private strTelefonoConductor As String

'        Private strNombreCliente As String
'        Private strMercancia As String
'        Private objRuta As Ruta
'        Private dblValor As Double
'        Private strReferenciasEmpresas As String

'        Private strObservaciones As String
'        Private intAutorizoDijinConductor As Integer
'        Private intAutorizoDijinVehiculo As Integer
'        Private intAutorizoDijinTenedor As Integer
'        Private intAutorizoColfecarConductor As Integer

'        Private intAutorizoColfecarVehiculo As Integer
'        Private intAutorizoAsecargaVehiculo As Integer
'        Private intAutorizoAsecargaTenedor As Integer
'        Private intAutorizoAsecargaConductor As Integer
'        Private intAutorizoSimit As Integer

'        Private intAutorizoLicenciaConduccion As Integer
'        Private intTieneGPS As Integer
'        Private strNombreEmpresaGPS As String
'        Private strClaveGPS As String
'        Private strTelefonoGPS As String

'        Private intAutorizoColfecarTenedor As Integer
'        Private objCataRiesgoEstudio As Catalogo
'        Private intAutorizadoSeguridad As Integer
'        Private intAutorizadoDireccion As Integer
'        Private intNumeroAutorizacion As Integer

'        Private intAnulado As Integer
'        Private objCataEstado As Catalogo
'        Private objCataEstadoEstudioSeguridad As Catalogo
'        Private intNumeracion As Integer
'        Private objDocumento As TipoDocumentos

'        Private objDocumentoAutorizacion As TipoDocumentos
'        Private objUsuario As Usuario
'        Private objOficina As Oficina
'        Private objUsuarioAnulo As Usuario
'        Private strCausaAnula As String

'        Private dteFechaAnula As Date
'        Private strObservacionesEstudio As String
'        Private strCelularPropietario As String
'        Private strConfirmoPropietario As String
'        Private strCelularTenedor As String

'        Private strConfirmoTenedor As String
'        Private strCelularConductor As String
'        Private strConfirmoConductor As String
'        Private strReferenciasPersonales As String
'        Private dteRevisionTecnoMecanica As Date

'        Private strSOAT As String
'        Private dteFechaSOAT As Date
'        Private bytFoto As Byte()
'        Public bolFoto As Boolean
'        Private objValidacion As Validacion

'        Private strNombreAutorizoOtro As String
'        Private intAutorizoOtro As Integer
'        Private intDefenCarga As Integer
'        Private intProcuraduria As Integer
'        Private intDestinoSeguro As Integer

'        Private strUsuarioGPS As String
'        Private strNumeroCarnetAfiliadora As String
'        Private dteFechaVencimientoAfiliadora As Date
'        Private objAseguradora As Tercero
'        Private objCataTipoVehiculo As Catalogo

'        Private strNumeroRevisionTecnomecanica As String
'        Private strIdentificacionAfiliadora As String
'        Private strDireccionPropietario As String
'        Private objCiudadResidenciaPropietario As Ciudad
'        Private strDireccionTenedor As String

'        Private dteFechaAprueba As Date

'        Private objCiudadResidenciaTenedor As Ciudad
'        Private strDireccionConductor As String
'        Private objCiudadResidenciaConductor As Ciudad
'        Private bytHuellaDigital As Byte()
'        Public bolHuellaDigital As Boolean
'        Public intPropietarioRemolque As Boolean
'        Public objCataTipoGps As Catalogo
'        Private intCodigo As Integer
'        Public bolAvisoActualizar As Boolean
'        Public bolAvisoEliminar As Boolean

'        Private IntTipoNaturalezaPropietario As Integer
'        Private StrTipoIdentificacionPropietario As String
'        Private IntTipoNaturalezaTenedor As Integer
'        Private StrTipoIdentificacionTenedor As String
'        Private IntTipoNaturalezaConductor As Integer
'        Private StrTipoIdentificacionConductor As String
'        Private intCodigoChequeoPropietario As Integer
'        Private intCodigoChequeoConductor As Integer
'        Private intCodigoChequeoTenedor As Integer

'        'Variables para la Gestión Documental
'        Private intEntidadOrigen As Byte
'        Private intAplica_Referencia As Byte
'        Private intAplica_Emisor As Byte
'        Private intAplica_Fecha_Emision As Byte
'        Private intAplica_Fecha_Vence As Byte
'        Private intTipoDocumentoDigitalizacion As Byte
'        Private strReferencia As String
'        Private strEmisor As String
'        Private dteFechaEmision As Date
'        Private dteFechaVence As Date
'        Private bytDocumentoAdjunto As Byte()
'        Private strNombreDocumento As String
'        Private strExtensDocumento As String

'#End Region

'#Region "Constantes"

'        Public Const DIAS_VIGENCIA_ESTUDIO As Long = 5
'        Public Const BYTES_IMAGEN_DEFECTO As Integer = 0
'        Public Const NOMBRE_CLASE As String = "clsEstudioSeguridadVehiculos"

'        Public Const ESTADO_SOLICITUD As String = "0"
'        Public Const ESTADO_APROBADO As String = "1"
'        Public Const ESTADO_RECHAZADO As String = "2"

'        ' Validaciones

'        Const VALIDA_COLOR_VEHICULO As String = "clsEstuSeguVali1"
'        Const VALIDA_EMPRESA_AFILIADORA As String = "clsEstuSeguVali2"
'        Const VALIDA_TELEFONO_EMPRESA_AFILIADORA As String = "clsEstuSeguVali3"
'        Const VALIDA_SOAT As String = "clsEstuSeguVali4"
'        Const VALIDA_FECHA_SOAT As String = "clsEstuSeguVali5"

'        Const VALIDA_TELEFONO_PROPIETARIO As String = "clsEstuSeguVali6"
'        Const VALIDA_CELULAR_PROPIETARIO As String = "clsEstuSeguVali7"
'        Const VALIDA_TELEFONO_TENEDOR As String = "clsEstuSeguVali8"
'        Const VALIDA_CELULAR_TENEDOR As String = "clsEstuSeguVali9"
'        Const VALIDA_TELEFONO_CONDUCTOR As String = "clsEstuSeguVali10"

'        Const VALIDA_CELULAR_CONDUCTOR As String = "clsEstuSeguVali11"
'        Const VALIDA_NOMBRE_CLIENTE As String = "clsEstuSeguVali12"
'        Const VALIDA_MERCANCIA As String = "clsEstuSeguVali13"
'        Const VALIDA_RUTA As String = "clsEstuSeguVali14"
'        Const VALIDA_REFERENCIA_EMPRESA As String = "clsEstuSeguVali15"

'        Const VALIDA_REFERENCIA_PERSONAL As String = "clsEstuSeguVali16"
'        Const VALIDA_OBSERVACIONES As String = "clsEstuSeguVali17"
'        Const VALIDA_CONFIRMADO_PROPIETARIO As String = "clsEstuSeguVali18"
'        Const VALIDA_CONFIRMADO_TENEDOR As String = "clsEstuSeguVali19"
'        Const VALIDA_CONFIRMADO_CONDUCTOR As String = "clsEstuSeguVali20"

'        'Const VALIDA_REFERENCIAS_OTRAS_EMPRESAS As String = "clsEstuSeguVali21"
'        'Const VALIDA_REFERENCIAS_PERSONALES As String = "clsEstuSeguVali22"
'        Const VALIDA_MODELO_REPOTENCIADO As String = "clsEstuSeguVali23"
'        Const VALIDA_VALOR_CLIENTE As String = "clsEstuSeguVali24"
'        Const VALIDA_CLASE_VEHICULO As String = "clsEstuSeguVali25"
'        Const VALIDA_REVISION_TECNOMECANICA As String = "clsEstuSeguVali26"

'        Const VALIDA_NUMERO_CARNET_AFILIACION As String = "clsEstuSeguVali27"
'        Const VALIDA_FECHA_VENCIMIENTO_AFILACION As String = "clsEstuSeguVali28"
'        Const EXIGE_CLASE_VEHICULO As String = "clsEstuSeguVali29"

'        Const VALIDA_SEMI_PLACA_POR_TIPO_VEHICULO As String = "clsEstuSeguVali30"
'        Const VALIDA_INGRESO_DIRECCION_CIUDAD_RESIDENCIA_TERCEROS As String = "clsEstuSeguVali31"
'        Const VALIDA_INGRESO_IMAGEN_HUELLA As String = "clsEstuSeguVali32"

'        Public Const PERMISO_GESTION_DOCUMENTAL As String = "clsEstuSeguVali33"

'        Const BYTE_VACIO As Integer = 0

'#End Region

'#Region "Constructor"

'        Sub New()

'            Try

'                Me.intAprobar = 0
'                Me.objEmpresa = New Empresas(0)
'                Me.lonNumero = 0
'                Me.dteFecha = Date.MinValue
'                Me.dteFechaAprueba = Date.MinValue
'                Me.strPlaca = ""

'                Me.lonMarca = 0
'                Me.intModelo = 0
'                Me.lonColor = 0
'                Me.strClase = ""
'                Me.strSemirremolque = ""

'                Me.intModeloRepotenciado = 0
'                Me.strEmpresaAfiliadora = ""
'                Me.strTelefonoEmpresaAfiliadora = ""
'                Me.strIdentificacionPropietario = ""
'                Me.strNombrePropietario = ""

'                Me.objCiudadPropietario = New Ciudad(Me.objEmpresa)
'                Me.strTelefonoPropietario = ""
'                Me.strIdentificacionTenedor = ""
'                Me.strNombreTenedor = ""
'                Me.objCiudadTenedor = New Ciudad(Me.objEmpresa)

'                Me.strTelefonoTenedor = ""
'                Me.strIdentificacionConductor = ""
'                Me.strNombreConductor = ""
'                Me.objCiudadConductor = New Ciudad(Me.objEmpresa)
'                Me.strTelefonoConductor = ""

'                Me.strNombreCliente = ""
'                Me.strMercancia = ""
'                Me.objRuta = New Ruta()
'                Me.dblValor = 0
'                Me.strReferenciasEmpresas = ""

'                Me.strObservaciones = ""
'                Me.intAutorizoDijinConductor = 0
'                Me.intAutorizoDijinVehiculo = 0
'                Me.intAutorizoDijinTenedor = 0
'                Me.intAutorizoColfecarConductor = 0

'                Me.intAutorizoColfecarVehiculo = 0
'                Me.intAutorizoAsecargaVehiculo = 0
'                Me.intAutorizoAsecargaTenedor = 0
'                Me.intAutorizoAsecargaConductor = 0
'                Me.intAutorizoSimit = 0

'                Me.intAutorizoLicenciaConduccion = 0
'                Me.intTieneGPS = 0
'                Me.strNombreEmpresaGPS = ""
'                Me.strClaveGPS = ""
'                Me.strTelefonoGPS = ""

'                Me.intAutorizoColfecarTenedor = 0
'                Me.objCataRiesgoEstudio = New Catalogo(Me.objEmpresa, "CRES")
'                Me.objCataTipoIdentificacion = New Catalogo(Me.objEmpresa, "TIID")

'                Me.intAutorizadoSeguridad = 0
'                Me.intAutorizadoDireccion = 0
'                Me.intNumeroAutorizacion = 0

'                Me.intAnulado = 0
'                Me.objCataEstado = New Catalogo(Me.objEmpresa, "ESDO")
'                Me.objCataEstadoEstudioSeguridad = New Catalogo(Me.objEmpresa, "ESES")
'                Me.intNumeracion = 0
'                Me.objDocumento = New TipoDocumentos(Me.objEmpresa)

'                Me.objDocumento.Codigo = TipoDocumentos.TIDO_ESTUDIO_SEGURIDAD
'                Me.objDocumentoAutorizacion = New TipoDocumentos(Me.objEmpresa)
'                Me.objDocumentoAutorizacion.Codigo = TipoDocumentos.TIDO_ESTUDIO_SEGURIDAD_AUTORIZACION
'                Me.objUsuario = New Usuario(Me.objEmpresa)
'                Me.objOficina = New Oficina(Me.objEmpresa)

'                Me.objUsuarioAnulo = New Usuario(Me.objEmpresa)
'                Me.strCausaAnula = ""
'                Me.strObservacionesEstudio = ""
'                Me.dteFechaAnula = Date.MinValue
'                Me.objGeneral = New General()

'                Me.objVehiculo = New Vehiculos()
'                Me.strCelularConductor = ""
'                Me.strCelularPropietario = ""
'                Me.strCelularTenedor = ""
'                Me.strConfirmoConductor = ""

'                Me.strConfirmoPropietario = ""
'                Me.strConfirmoTenedor = ""
'                Me.strReferenciasPersonales = ""
'                Me.dteRevisionTecnoMecanica = Date.MinValue
'                Me.strSOAT = ""

'                Me.dteFechaSOAT = Date.MinValue
'                Me.objValidacion = New Validacion(Me.objEmpresa)
'                Me.strNombreAutorizoOtro = ""
'                Me.intAutorizoOtro = 0
'                Me.intDefenCarga = 0

'                Me.intProcuraduria = 0
'                Me.intDestinoSeguro = 0
'                Me.strUsuarioGPS = String.Empty
'                Me.strNumeroCarnetAfiliadora = ""
'                Me.dteFechaVencimientoAfiliadora = Date.MinValue

'                Me.objAseguradora = New Tercero(Me.objEmpresa)
'                Me.objCataTipoVehiculo = New Catalogo(Me.objEmpresa, "TIVE")
'                Me.strNumeroRevisionTecnomecanica = ""
'                Me.strDireccionPropietario = ""
'                Me.objCiudadResidenciaPropietario = New Ciudad(Me.objEmpresa)

'                Me.strDireccionTenedor = ""
'                Me.objCiudadResidenciaTenedor = New Ciudad(Me.objEmpresa)
'                Me.strDireccionConductor = ""
'                Me.objCiudadResidenciaConductor = New Ciudad(Me.objEmpresa)
'                Me.objCataTipoGps = New Catalogo(Me.objEmpresa, "TGPS")


'                Me.strIdentificacionPropietarioRemolque = ""
'                Me.strNombrePropietarioRemolque = ""
'                Me.objCiudadPropietarioRemolque = New Ciudad(Me.objEmpresa)
'                Me.strTelefonoPropietarioRemolque = ""
'                Me.strConfirmoPropietarioRemolque = ""
'                Me.objCiudadResidenciaPropietarioRemolque = New Ciudad(Me.objEmpresa)
'                Me.strCelularPropietarioRemolque = ""
'                Me.DireccionPropietarioRemolque = ""

'                Me.intCodigo = 0

'                Me.IntTipoNaturalezaPropietario = 0
'                Me.StrTipoIdentificacionPropietario = ""
'                Me.IntTipoNaturalezaTenedor = 0
'                Me.StrTipoIdentificacionTenedor = ""

'                Me.IntTipoNaturalezaConductor = 0
'                Me.StrTipoIdentificacionConductor = ""
'                Me.intCodigoChequeoPropietario = 0
'                Me.intCodigoChequeoConductor = 0
'                Me.intCodigoChequeoTenedor = 0

'                Me.intAplica_Referencia = 0
'                Me.intAplica_Emisor = 0
'                Me.intAplica_Fecha_Emision = 0
'                Me.intAplica_Fecha_Vence = 0
'                Me.intEntidadOrigen = 0
'                Me.intTipoDocumentoDigitalizacion = 0
'                Me.strReferencia = String.Empty
'                Me.strEmisor = String.Empty
'                Me.dteFechaEmision = Date.MinValue
'                Me.dteFechaVence = Date.MinValue

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Me.strError = Me.objGeneral.Traducir_Error(ex.Message)
'            End Try

'        End Sub

'        Sub New(ByRef Empresa As Empresas)

'            Try
'                Me.intAprobar = 0
'                Me.objEmpresa = Empresa
'                Me.lonNumero = 0
'                Me.dteFecha = Date.MinValue
'                Me.dteFechaAprueba = Date.MinValue
'                Me.strPlaca = ""

'                Me.lonMarca = 0
'                Me.intModelo = 0
'                Me.lonColor = 0
'                Me.strClase = ""
'                Me.strSemirremolque = ""

'                Me.intModeloRepotenciado = 0
'                Me.strEmpresaAfiliadora = ""
'                Me.strTelefonoEmpresaAfiliadora = ""
'                Me.strIdentificacionPropietario = ""
'                Me.strNombrePropietario = ""

'                Me.objCiudadPropietario = New Ciudad(Me.objEmpresa)
'                Me.strTelefonoPropietario = ""
'                Me.strIdentificacionTenedor = ""
'                Me.strNombreTenedor = ""
'                Me.objCiudadTenedor = New Ciudad(Me.objEmpresa)

'                Me.strTelefonoTenedor = ""
'                Me.strIdentificacionConductor = ""
'                Me.strNombreConductor = ""
'                Me.objCiudadConductor = New Ciudad(Me.objEmpresa)
'                Me.strTelefonoConductor = ""

'                Me.strNombreCliente = ""
'                Me.strMercancia = ""
'                Me.objRuta = New Ruta()
'                Me.dblValor = 0
'                Me.strReferenciasEmpresas = ""

'                Me.strObservaciones = ""
'                Me.intAutorizoDijinConductor = 0
'                Me.intAutorizoDijinVehiculo = 0
'                Me.intAutorizoDijinTenedor = 0
'                Me.intAutorizoColfecarConductor = 0

'                Me.intAutorizoColfecarVehiculo = 0
'                Me.intAutorizoAsecargaVehiculo = 0
'                Me.intAutorizoAsecargaTenedor = 0
'                Me.intAutorizoAsecargaConductor = 0
'                Me.intAutorizoSimit = 0

'                Me.intAutorizoLicenciaConduccion = 0
'                Me.intTieneGPS = 0
'                Me.strNombreEmpresaGPS = ""
'                Me.strClaveGPS = ""
'                Me.strTelefonoGPS = ""

'                Me.intAutorizoColfecarTenedor = 0
'                Me.objCataRiesgoEstudio = New Catalogo(Me.objEmpresa, "CRES")
'                Me.objCataTipoIdentificacion = New Catalogo(Me.objEmpresa, "TIID")
'                Me.intAutorizadoSeguridad = 0
'                Me.intAutorizadoDireccion = 0
'                Me.intNumeroAutorizacion = 0

'                Me.intAnulado = 0
'                Me.objCataEstado = New Catalogo(Me.objEmpresa, "ESDO")
'                Me.objCataEstadoEstudioSeguridad = New Catalogo(Me.objEmpresa, "ESES")
'                Me.intNumeracion = 0
'                Me.objDocumento = New TipoDocumentos(Me.objEmpresa)

'                Me.objDocumento.Codigo = TipoDocumentos.TIDO_ESTUDIO_SEGURIDAD
'                Me.objDocumentoAutorizacion = New TipoDocumentos(Me.objEmpresa)
'                Me.objDocumentoAutorizacion.Codigo = TipoDocumentos.TIDO_ESTUDIO_SEGURIDAD_AUTORIZACION
'                Me.objUsuario = New Usuario(Me.objEmpresa)
'                Me.objOficina = New Oficina(Me.objEmpresa)

'                Me.objUsuarioAnulo = New Usuario(Me.objEmpresa)
'                Me.strCausaAnula = ""
'                Me.strObservacionesEstudio = ""
'                Me.dteFechaAnula = Date.MinValue
'                Me.objGeneral = New General()

'                Me.objVehiculo = New Vehiculos()
'                Me.strCelularConductor = ""
'                Me.strCelularPropietario = ""
'                Me.strCelularTenedor = ""
'                Me.strConfirmoConductor = ""

'                Me.strConfirmoPropietario = ""
'                Me.strConfirmoTenedor = ""
'                Me.strReferenciasPersonales = ""
'                Me.dteRevisionTecnoMecanica = Date.MinValue
'                Me.strSOAT = ""

'                Me.dteFechaSOAT = Date.MinValue
'                Me.objValidacion = New Validacion(Me.objEmpresa)
'                Me.strNombreAutorizoOtro = ""
'                Me.intAutorizoOtro = 0
'                Me.intDefenCarga = 0

'                Me.intProcuraduria = 0
'                Me.intDestinoSeguro = 0
'                Me.strUsuarioGPS = String.Empty
'                Me.strNumeroCarnetAfiliadora = ""
'                Me.dteFechaVencimientoAfiliadora = Date.MinValue

'                Me.objAseguradora = New Tercero(Me.objEmpresa)
'                Me.objCataTipoVehiculo = New Catalogo(Me.objEmpresa, "TIVE")
'                Me.strNumeroRevisionTecnomecanica = ""
'                Me.strDireccionPropietario = ""
'                Me.objCiudadResidenciaPropietario = New Ciudad(Me.objEmpresa)

'                Me.strDireccionTenedor = ""
'                Me.objCiudadResidenciaTenedor = New Ciudad(Me.objEmpresa)
'                Me.strDireccionConductor = ""
'                Me.objCiudadResidenciaConductor = New Ciudad(Me.objEmpresa)
'                Me.objCataTipoGps = New Catalogo(Me.objEmpresa, "TGPS")

'                Me.intCodigo = 0


'                Me.strIdentificacionPropietarioRemolque = ""
'                Me.strNombrePropietarioRemolque = ""
'                Me.objCiudadPropietarioRemolque = New Ciudad(Me.objEmpresa)
'                Me.strTelefonoPropietarioRemolque = ""
'                Me.strConfirmoPropietarioRemolque = ""
'                Me.objCiudadResidenciaPropietarioRemolque = New Ciudad(Me.objEmpresa)
'                Me.strCelularPropietarioRemolque = ""
'                Me.DireccionPropietarioRemolque = ""

'                Me.IntTipoNaturalezaPropietario = ""
'                Me.StrTipoIdentificacionPropietario = ""
'                Me.IntTipoNaturalezaTenedor = ""
'                Me.StrTipoIdentificacionTenedor = ""

'                Me.IntTipoNaturalezaConductor = ""
'                Me.StrTipoIdentificacionConductor = ""
'                Me.intCodigoChequeoPropietario = 0
'                Me.intCodigoChequeoConductor = 0
'                Me.intCodigoChequeoTenedor = 0

'                Me.intAplica_Referencia = 0
'                Me.intAplica_Emisor = 0
'                Me.intAplica_Fecha_Emision = 0
'                Me.intAplica_Fecha_Vence = 0
'                Me.intEntidadOrigen = 0
'                Me.intTipoDocumentoDigitalizacion = 0
'                Me.strReferencia = String.Empty
'                Me.strEmisor = String.Empty
'                Me.dteFechaEmision = Date.MinValue
'                Me.dteFechaVence = Date.MinValue

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Me.strError = Me.objGeneral.Traducir_Error(ex.Message)
'            End Try

'        End Sub

'        Sub New(ByVal Empresa As Empresas, ByVal Numero As Integer)

'            Me.intAprobar = 0

'            Me.objEmpresa = Empresa
'            Me.lonNumero = Numero
'            Me.dteFecha = Date.MinValue
'            Me.dteFechaAprueba = Date.MinValue
'            Me.strPlaca = ""
'            Me.lonMarca = 0

'            Me.intModelo = 0
'            Me.lonColor = 0
'            Me.strClase = ""
'            Me.strSemirremolque = ""
'            Me.intModeloRepotenciado = 0

'            Me.strEmpresaAfiliadora = ""
'            Me.strTelefonoEmpresaAfiliadora = ""
'            Me.strIdentificacionPropietario = ""
'            Me.strNombrePropietario = ""
'            Me.objCiudadPropietario = New Ciudad(Me.objEmpresa)

'            Me.strTelefonoPropietario = ""
'            Me.strIdentificacionTenedor = ""
'            Me.strNombreTenedor = ""
'            Me.objCiudadTenedor = New Ciudad(Me.objEmpresa)
'            Me.strTelefonoTenedor = ""

'            Me.strIdentificacionConductor = ""
'            Me.strNombreConductor = ""
'            Me.objCiudadConductor = New Ciudad(Me.objEmpresa)
'            Me.strTelefonoConductor = ""
'            Me.strNombreCliente = ""

'            Me.strMercancia = ""
'            Me.objRuta = New Ruta()
'            Me.dblValor = 0
'            Me.strReferenciasEmpresas = ""
'            Me.strObservaciones = ""

'            Me.intAutorizoDijinConductor = 0
'            Me.intAutorizoDijinVehiculo = 0
'            Me.intAutorizoDijinTenedor = 0
'            Me.intAutorizoColfecarConductor = 0
'            Me.intAutorizoColfecarVehiculo = 0

'            Me.intAutorizoAsecargaVehiculo = 0
'            Me.intAutorizoAsecargaTenedor = 0
'            Me.intAutorizoAsecargaConductor = 0
'            Me.intAutorizoSimit = 0
'            Me.intAutorizoLicenciaConduccion = 0

'            Me.intTieneGPS = 0
'            Me.strNombreEmpresaGPS = ""
'            Me.strClaveGPS = ""
'            Me.strTelefonoGPS = ""

'            Me.intAutorizoColfecarTenedor = 0
'            Me.objCataRiesgoEstudio = New Catalogo(Me.objEmpresa, "CRES")
'            Me.intAutorizadoSeguridad = 0
'            Me.intAutorizadoDireccion = 0
'            Me.intNumeroAutorizacion = 0

'            Me.intAnulado = 0
'            Me.objCataEstado = New Catalogo(Me.objEmpresa, "ESDO")
'            Me.objCataEstadoEstudioSeguridad = New Catalogo(Me.objEmpresa, "ESES")
'            Me.intNumeracion = 0
'            Me.objDocumento = New TipoDocumentos(Me.objEmpresa)
'            Me.objDocumento.Codigo = TipoDocumentos.TIDO_ESTUDIO_SEGURIDAD

'            Me.objUsuario = New Usuario(Me.objEmpresa)
'            Me.objOficina = New Oficina(Me.objEmpresa)
'            Me.objUsuarioAnulo = New Usuario(Me.objEmpresa)
'            Me.strCausaAnula = ""
'            Me.strObservacionesEstudio = ""

'            Me.dteFechaAnula = Date.MinValue
'            Me.objGeneral = New General()
'            Me.objVehiculo = New Vehiculos()

'            Me.strCelularConductor = ""
'            Me.strCelularPropietario = ""
'            Me.strCelularTenedor = ""
'            Me.strConfirmoConductor = ""
'            Me.strConfirmoPropietario = ""

'            Me.strConfirmoTenedor = ""
'            Me.strReferenciasPersonales = ""
'            Me.dteRevisionTecnoMecanica = Date.MinValue
'            Me.strSOAT = ""
'            Me.dteFechaSOAT = Date.MinValue

'            Me.objValidacion = New Validacion(Me.objEmpresa)
'            Me.strNombreAutorizoOtro = ""
'            Me.intAutorizoOtro = 0
'            Me.intDefenCarga = 0
'            Me.intProcuraduria = 0

'            Me.intDestinoSeguro = 0
'            Me.strUsuarioGPS = String.Empty
'            Me.strNumeroCarnetAfiliadora = ""
'            Me.dteFechaVencimientoAfiliadora = Date.MinValue
'            Me.objAseguradora = New Tercero(Me.objEmpresa)

'            Me.objCataTipoVehiculo = New Catalogo(Me.objEmpresa, "TIVE")
'            Me.strNumeroRevisionTecnomecanica = ""
'            Me.strDireccionPropietario = ""
'            Me.objCiudadResidenciaPropietario = New Ciudad(Me.objEmpresa)

'            Me.strDireccionTenedor = ""
'            Me.objCiudadResidenciaTenedor = New Ciudad(Me.objEmpresa)
'            Me.strDireccionConductor = ""
'            Me.objCiudadResidenciaConductor = New Ciudad(Me.objEmpresa)
'            Me.objCataTipoGps = New Catalogo(Me.objEmpresa, "TGPS")

'            Me.strIdentificacionPropietarioRemolque = ""
'            Me.strNombrePropietarioRemolque = ""
'            Me.objCiudadPropietarioRemolque = New Ciudad(Me.objEmpresa)
'            Me.strTelefonoPropietarioRemolque = ""
'            Me.strConfirmoPropietarioRemolque = ""
'            Me.objCiudadResidenciaPropietarioRemolque = New Ciudad(Me.objEmpresa)
'            Me.strCelularPropietarioRemolque = ""
'            Me.DireccionPropietarioRemolque = ""

'            Me.IntTipoNaturalezaPropietario = ""
'            Me.StrTipoIdentificacionPropietario = ""
'            Me.IntTipoNaturalezaTenedor = ""
'            Me.StrTipoIdentificacionTenedor = ""

'            Me.IntTipoNaturalezaConductor = ""
'            Me.StrTipoIdentificacionConductor = ""
'            Me.intCodigoChequeoPropietario = 0
'            Me.intCodigoChequeoConductor = 0
'            Me.intCodigoChequeoTenedor = 0

'            Me.intAplica_Referencia = 0
'            Me.intAplica_Emisor = 0
'            Me.intAplica_Fecha_Emision = 0
'            Me.intAplica_Fecha_Vence = 0
'            Me.intEntidadOrigen = 0
'            Me.intTipoDocumentoDigitalizacion = 0
'            Me.strReferencia = String.Empty
'            Me.strEmisor = String.Empty
'            Me.dteFechaEmision = Date.MinValue
'            Me.dteFechaVence = Date.MinValue

'        End Sub

'#End Region

'#Region "Atributos"

'        Public Property CataTipoVehiculo() As Catalogo
'            Get
'                Return Me.objCataTipoVehiculo
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoVehiculo = value
'            End Set
'        End Property

'        Public Property NumeroRevisionTecnomecanica() As String
'            Get
'                Return Me.strNumeroRevisionTecnomecanica
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroRevisionTecnomecanica = value
'            End Set
'        End Property

'        Public Property NumeroCarnetAfiliadora() As String
'            Get
'                Return Me.strNumeroCarnetAfiliadora
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroCarnetAfiliadora = value
'            End Set
'        End Property

'        Public Property FechaVencimientoAfiliadora() As Date
'            Get
'                Return Me.dteFechaVencimientoAfiliadora
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaVencimientoAfiliadora = value
'            End Set
'        End Property

'        Public Property DefenCarga() As Integer
'            Get
'                Return intDefenCarga
'            End Get
'            Set(ByVal value As Integer)
'                intDefenCarga = value
'            End Set
'        End Property

'        Public Property AutorizoOtro() As Integer
'            Get
'                Return intAutorizoOtro
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizoOtro = value
'            End Set
'        End Property

'        Public Property NombreAutorizoOtro() As String
'            Get
'                Return strNombreAutorizoOtro
'            End Get
'            Set(ByVal value As String)
'                strNombreAutorizoOtro = value
'            End Set
'        End Property

'        Public Property Foto() As Byte()
'            Get
'                Return Me.bytFoto
'            End Get
'            Set(ByVal value As Byte())
'                Me.bytFoto = value
'            End Set
'        End Property

'        Public Property Fecha() As Date
'            Get
'                Return dteFecha
'            End Get
'            Set(ByVal value As Date)
'                dteFecha = value
'            End Set
'        End Property

'        Public Property FechaAprueba() As Date
'            Get
'                Return dteFechaAprueba
'            End Get
'            Set(ByVal value As Date)
'                dteFechaAprueba = value
'            End Set
'        End Property

'        Public Property Placa() As String
'            Get
'                Return Me.strPlaca
'            End Get
'            Set(ByVal value As String)
'                Me.strPlaca = value
'            End Set
'        End Property

'        Public Property Marca() As Long
'            Get
'                Return Me.lonMarca
'            End Get
'            Set(ByVal value As Long)
'                Me.lonMarca = value
'            End Set
'        End Property

'        Public Property Modelo() As Integer
'            Get
'                Return Me.intModelo
'            End Get
'            Set(ByVal value As Integer)
'                Me.intModelo = value
'            End Set
'        End Property

'        Public Property Color() As Long
'            Get
'                Return Me.lonColor
'            End Get
'            Set(ByVal value As Long)
'                Me.lonColor = value
'            End Set
'        End Property

'        Public Property Clase() As String
'            Get
'                Return Me.strClase
'            End Get
'            Set(ByVal value As String)
'                Me.strClase = value
'            End Set
'        End Property

'        Public Property Semirremolque() As String
'            Get
'                Return Me.strSemirremolque
'            End Get
'            Set(ByVal value As String)
'                Me.strSemirremolque = value
'            End Set
'        End Property

'        Public Property ModeloRepontenciado() As Integer
'            Get
'                Return Me.intModeloRepotenciado
'            End Get
'            Set(ByVal value As Integer)
'                Me.intModeloRepotenciado = value
'            End Set
'        End Property

'        Public Property EmpresaAfiliadora() As String
'            Get
'                Return strEmpresaAfiliadora
'            End Get
'            Set(ByVal value As String)
'                strEmpresaAfiliadora = value
'            End Set
'        End Property

'        Public Property TelefonoEmpresaAfiliadora() As String
'            Get
'                Return strTelefonoEmpresaAfiliadora
'            End Get
'            Set(ByVal value As String)
'                strTelefonoEmpresaAfiliadora = value
'            End Set
'        End Property

'        Public Property IdentificacionPropietario() As String
'            Get
'                Return strIdentificacionPropietario
'            End Get
'            Set(ByVal value As String)
'                strIdentificacionPropietario = value
'            End Set
'        End Property

'        Public Property NombrePropietario() As String
'            Get
'                Return strNombrePropietario
'            End Get
'            Set(ByVal value As String)
'                strNombrePropietario = value
'            End Set
'        End Property

'        Public Property CiudadPropietario() As Ciudad
'            Get
'                Return objCiudadPropietario
'            End Get
'            Set(ByVal value As Ciudad)
'                objCiudadPropietario = value
'            End Set
'        End Property

'        Public Property TelefonoPropietario() As String
'            Get
'                Return strTelefonoPropietario
'            End Get
'            Set(ByVal value As String)
'                strTelefonoPropietario = value
'            End Set
'        End Property

'        Public Property IdentificacionTenedor() As String
'            Get
'                Return strIdentificacionTenedor
'            End Get
'            Set(ByVal value As String)
'                strIdentificacionTenedor = value
'            End Set
'        End Property

'        Public Property NombreTenedor() As String
'            Get
'                Return strNombreTenedor
'            End Get
'            Set(ByVal value As String)
'                strNombreTenedor = value
'            End Set
'        End Property

'        Public Property CiudadTenedor() As Ciudad
'            Get
'                Return objCiudadTenedor
'            End Get
'            Set(ByVal value As Ciudad)
'                objCiudadTenedor = value
'            End Set
'        End Property

'        Public Property TelefonoTenedor() As String
'            Get
'                Return strTelefonoTenedor
'            End Get
'            Set(ByVal value As String)
'                strTelefonoTenedor = value
'            End Set
'        End Property

'        Public Property IdentificacionConductor() As String
'            Get
'                Return strIdentificacionConductor
'            End Get
'            Set(ByVal value As String)
'                strIdentificacionConductor = value
'            End Set
'        End Property

'        Public Property NombreConductor() As String
'            Get
'                Return strNombreConductor
'            End Get
'            Set(ByVal value As String)
'                strNombreConductor = value
'            End Set
'        End Property

'        Public Property CiudadConductor() As Ciudad
'            Get
'                Return objCiudadConductor
'            End Get
'            Set(ByVal value As Ciudad)
'                objCiudadConductor = value
'            End Set
'        End Property

'        Public Property TelefonoConductor() As String
'            Get
'                Return strTelefonoConductor
'            End Get
'            Set(ByVal value As String)
'                strTelefonoConductor = value
'            End Set
'        End Property

'        Public Property NombreCliente() As String
'            Get
'                Return strNombreCliente
'            End Get
'            Set(ByVal value As String)
'                strNombreCliente = value
'            End Set
'        End Property

'        Public Property Mercancia() As String
'            Get
'                Return strMercancia
'            End Get
'            Set(ByVal value As String)
'                strMercancia = value
'            End Set
'        End Property

'        Public Property Ruta() As Ruta
'            Get
'                Return Me.objRuta
'            End Get
'            Set(ByVal value As Ruta)
'                Me.objRuta = value
'            End Set
'        End Property

'        Public Property Valor() As Double
'            Get
'                Return dblValor
'            End Get
'            Set(ByVal value As Double)
'                dblValor = value
'            End Set
'        End Property

'        Public Property Referencias() As String
'            Get
'                Return strReferenciasEmpresas
'            End Get
'            Set(ByVal value As String)
'                strReferenciasEmpresas = value
'            End Set
'        End Property

'        Public Property Observaciones() As String
'            Get
'                Return strObservaciones
'            End Get
'            Set(ByVal value As String)
'                strObservaciones = value
'            End Set
'        End Property

'        Public Property ObservacionesEstudio() As String
'            Get
'                Return strObservacionesEstudio
'            End Get
'            Set(ByVal value As String)
'                strObservacionesEstudio = value
'            End Set
'        End Property

'        Public Property AutorizoDijinConductor() As Integer
'            Get
'                Return intAutorizoDijinConductor
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizoDijinConductor = value
'            End Set
'        End Property

'        Public Property AutorizoDijinVehiculo() As Integer
'            Get
'                Return intAutorizoDijinVehiculo
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizoDijinVehiculo = value
'            End Set
'        End Property

'        Public Property AutorizoDijinTenedor() As Integer
'            Get
'                Return intAutorizoDijinTenedor
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizoDijinTenedor = value
'            End Set
'        End Property

'        Public Property AutorizoColfecarConductor() As Integer
'            Get
'                Return intAutorizoColfecarConductor
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizoColfecarConductor = value
'            End Set
'        End Property

'        Public Property AutorizoColfecarVehiculo() As Integer
'            Get
'                Return intAutorizoColfecarVehiculo
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizoColfecarVehiculo = value
'            End Set
'        End Property

'        Public Property AutorizoColfecarTenedor() As Integer
'            Get
'                Return intAutorizoColfecarTenedor
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizoColfecarTenedor = value
'            End Set
'        End Property

'        Public Property AutorizoAsecargaVehiculo() As Integer
'            Get
'                Return intAutorizoAsecargaVehiculo
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizoAsecargaVehiculo = value
'            End Set
'        End Property

'        Public Property AutorizoAsecargaTenedor() As Integer
'            Get
'                Return intAutorizoAsecargaTenedor
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizoAsecargaTenedor = value
'            End Set
'        End Property

'        Public Property AutorizoAsecargaConductor() As Integer
'            Get
'                Return intAutorizoAsecargaConductor
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizoAsecargaConductor = value
'            End Set
'        End Property

'        Public Property AutorizoSimit() As Integer
'            Get
'                Return intAutorizoSimit
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizoSimit = value
'            End Set
'        End Property

'        Public Property AutorizoLicenciaConduccion() As Integer
'            Get
'                Return intAutorizoLicenciaConduccion
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizoLicenciaConduccion = value
'            End Set
'        End Property

'        Public Property TieneGPS() As Integer
'            Get
'                Return intTieneGPS
'            End Get
'            Set(ByVal value As Integer)
'                intTieneGPS = value
'            End Set
'        End Property

'        Public Property NombreEmpresaGPS() As String
'            Get
'                Return strNombreEmpresaGPS
'            End Get
'            Set(ByVal value As String)
'                strNombreEmpresaGPS = value
'            End Set
'        End Property

'        Public Property ClaveGPS() As String
'            Get
'                Return strClaveGPS
'            End Get
'            Set(ByVal value As String)
'                strClaveGPS = value
'            End Set
'        End Property

'        Public Property TelefonoGPS() As String
'            Get
'                Return strTelefonoGPS
'            End Get
'            Set(ByVal value As String)
'                strTelefonoGPS = value
'            End Set
'        End Property

'        Public Property CataRiesgoSeguridad() As Catalogo
'            Get
'                Return Me.objCataRiesgoEstudio
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataRiesgoEstudio = value
'            End Set
'        End Property

'        Public Property AutorizadoSeguridad() As Integer
'            Get
'                Return intAutorizadoSeguridad
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizadoSeguridad = value
'            End Set
'        End Property

'        Public Property AutorizadoDireccion() As Integer
'            Get
'                Return intAutorizadoDireccion
'            End Get
'            Set(ByVal value As Integer)
'                intAutorizadoDireccion = value
'            End Set
'        End Property

'        Public Property NumeroAutorizacion() As Integer
'            Get
'                Return intNumeroAutorizacion
'            End Get
'            Set(ByVal value As Integer)
'                intNumeroAutorizacion = value
'            End Set
'        End Property

'        Public Property Anulado() As Integer
'            Get
'                Return intAnulado
'            End Get
'            Set(ByVal value As Integer)
'                intAnulado = value
'            End Set
'        End Property

'        Public Property CataEstado() As Catalogo
'            Get
'                Return Me.objCataEstado
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataEstado = value
'            End Set
'        End Property

'        Public Property CataEstadoSeguridadVehiculo() As Catalogo
'            Get
'                Return Me.objCataEstadoEstudioSeguridad
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataEstadoEstudioSeguridad = value
'            End Set
'        End Property

'        Public Property Numeracion() As Integer
'            Get
'                Return intNumeracion
'            End Get
'            Set(ByVal value As Integer)
'                intNumeracion = value
'            End Set
'        End Property

'        Public Property Documento() As TipoDocumentos
'            Get
'                Return Me.objDocumento
'            End Get
'            Set(ByVal value As TipoDocumentos)
'                objDocumento = value
'            End Set
'        End Property

'        Public Property Usuario() As Usuario
'            Get
'                Return objUsuario
'            End Get
'            Set(ByVal value As Usuario)
'                objUsuario = value
'            End Set
'        End Property

'        Public Property Numero() As Long
'            Get
'                Return lonNumero
'            End Get
'            Set(ByVal value As Long)
'                lonNumero = value
'            End Set
'        End Property

'        Public Property Oficina() As Oficina
'            Get
'                Return objOficina
'            End Get
'            Set(ByVal value As Oficina)
'                objOficina = value
'            End Set
'        End Property

'        Public Property UsuarioAnulo() As Usuario
'            Get
'                Return Me.objUsuarioAnulo
'            End Get
'            Set(ByVal value As Usuario)
'                Me.objUsuarioAnulo = value
'            End Set
'        End Property

'        Public Property FechaAnulo() As Date
'            Get
'                Return Me.dteFechaAnula
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaAnula = value
'            End Set
'        End Property

'        Public Property CausaAnula() As String
'            Get
'                Return Me.strCausaAnula
'            End Get
'            Set(ByVal value As String)
'                Me.strCausaAnula = value
'            End Set
'        End Property

'        Public Property Empresa() As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(ByVal value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property

'        Public Property Aprobar() As Integer
'            Get
'                Return Me.intAprobar
'            End Get
'            Set(ByVal value As Integer)
'                Me.intAprobar = value
'            End Set
'        End Property

'        Public Property ConfirmoPropietario() As String
'            Get
'                Return Me.strConfirmoPropietario
'            End Get
'            Set(ByVal value As String)
'                Me.strConfirmoPropietario = value
'            End Set
'        End Property

'        Public Property ConfirmoConductor() As String
'            Get
'                Return Me.strConfirmoConductor
'            End Get
'            Set(ByVal value As String)
'                Me.strConfirmoConductor = value
'            End Set
'        End Property

'        Public Property ConfirmoTenedor() As String
'            Get
'                Return Me.strConfirmoTenedor
'            End Get
'            Set(ByVal value As String)
'                Me.strConfirmoTenedor = value
'            End Set
'        End Property

'        Public Property CelularConductor() As String
'            Get
'                Return Me.strCelularConductor
'            End Get
'            Set(ByVal value As String)
'                Me.strCelularConductor = value
'            End Set
'        End Property

'        Public Property CelularPropietario() As String
'            Get
'                Return Me.strCelularPropietario
'            End Get
'            Set(ByVal value As String)
'                Me.strCelularPropietario = value
'            End Set
'        End Property

'        Public Property CelularTenedor() As String
'            Get
'                Return Me.strCelularTenedor
'            End Get
'            Set(ByVal value As String)
'                Me.strCelularTenedor = value
'            End Set
'        End Property

'        Public Property SOAT() As String
'            Get
'                Return Me.strSOAT
'            End Get
'            Set(ByVal value As String)
'                Me.strSOAT = value
'            End Set
'        End Property

'        Public Property FechaSOAT() As Date
'            Get
'                Return Me.dteFechaSOAT
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaSOAT = value
'            End Set
'        End Property

'        Public Property RevisionTecnoMecanica() As Date
'            Get
'                Return Me.dteRevisionTecnoMecanica
'            End Get
'            Set(ByVal value As Date)
'                Me.dteRevisionTecnoMecanica = value
'            End Set
'        End Property

'        Public Property ReferenciasPersonales() As String
'            Get
'                Return Me.strReferenciasPersonales
'            End Get
'            Set(ByVal value As String)
'                Me.strReferenciasPersonales = value
'            End Set
'        End Property

'        Public Property Procuraduria() As Integer
'            Get
'                Return Me.intProcuraduria
'            End Get
'            Set(ByVal value As Integer)
'                Me.intProcuraduria = value
'            End Set
'        End Property

'        Public Property DestinoSeguro() As Integer
'            Get
'                Return Me.intDestinoSeguro
'            End Get
'            Set(ByVal value As Integer)
'                Me.intDestinoSeguro = value
'            End Set
'        End Property

'        Public Property UsuarioGPS() As String
'            Get
'                Return Me.strUsuarioGPS
'            End Get
'            Set(ByVal value As String)
'                Me.strUsuarioGPS = value
'            End Set
'        End Property

'        Public Property Aseguradora() As Tercero
'            Get
'                Return Me.objAseguradora
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objAseguradora = value
'            End Set
'        End Property

'        Public Property IdentificacionAfiliadora() As String
'            Get
'                Return Me.strIdentificacionAfiliadora
'            End Get
'            Set(ByVal value As String)
'                Me.strIdentificacionAfiliadora = value
'            End Set
'        End Property


'        Public Property DireccionPropietario() As String
'            Get
'                Return Me.strDireccionPropietario
'            End Get
'            Set(ByVal value As String)
'                Me.strDireccionPropietario = value
'            End Set
'        End Property

'        Public Property CiudadResidenciaPropietario() As Ciudad
'            Get
'                Return Me.objCiudadResidenciaPropietario
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudadResidenciaPropietario = value
'            End Set
'        End Property

'        Public Property DireccionConductor() As String
'            Get
'                Return Me.strDireccionConductor
'            End Get
'            Set(ByVal value As String)
'                Me.strDireccionConductor = value
'            End Set
'        End Property

'        Public Property CiudadResidenciaConductor() As Ciudad
'            Get
'                Return Me.objCiudadResidenciaConductor
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudadResidenciaConductor = value
'            End Set
'        End Property

'        Public Property DireccionTenedor() As String
'            Get
'                Return Me.strDireccionTenedor
'            End Get
'            Set(ByVal value As String)
'                Me.strDireccionTenedor = value
'            End Set
'        End Property

'        Public Property CiudadResidenciaTenedor() As Ciudad
'            Get
'                Return Me.objCiudadResidenciaTenedor
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudadResidenciaTenedor = value
'            End Set
'        End Property

'        Public Property HuellaDigital() As Byte()
'            Get
'                Return Me.bytHuellaDigital
'            End Get
'            Set(ByVal value As Byte())
'                Me.bytHuellaDigital = value
'            End Set
'        End Property

'        Public Property CataTipoGPS() As Catalogo
'            Get
'                Return Me.objCataTipoGps
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoGps = value
'            End Set
'        End Property
'        ' Propiedades para Propietario de remolque 
'        Public Property NombrePropietarioRemolque As String
'            Get
'                Return strNombrePropietarioRemolque
'            End Get
'            Set(value As String)
'                strNombrePropietarioRemolque = value
'            End Set
'        End Property
'        Public Property IdentificacionPropietarioRemolque() As String
'            Get
'                Return strIdentificacionPropietarioRemolque
'            End Get
'            Set(ByVal value As String)
'                strIdentificacionPropietarioRemolque = value
'            End Set
'        End Property
'        Public Property TelefonoPropietarioRemolque() As String
'            Get
'                Return strTelefonoPropietarioRemolque
'            End Get
'            Set(ByVal value As String)
'                strTelefonoPropietarioRemolque = value
'            End Set
'        End Property
'        Public Property CiudadPropietarioRemolque() As Ciudad
'            Get
'                Return Me.objCiudadPropietarioRemolque
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudadPropietarioRemolque = value
'            End Set
'        End Property
'        Public Property ConfirmoPropietarioRemolque() As String
'            Get
'                Return strConfirmoPropietarioRemolque
'            End Get
'            Set(ByVal value As String)
'                strConfirmoPropietarioRemolque = value
'            End Set
'        End Property
'        Public Property CiudadResidenciaPropietarioRemolque() As Ciudad
'            Get
'                Return Me.objCiudadResidenciaPropietarioRemolque
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudadResidenciaPropietarioRemolque = value
'            End Set
'        End Property
'        Public Property CelularPropietarioRemolque() As String
'            Get
'                Return Me.strCelularPropietarioRemolque
'            End Get
'            Set(ByVal value As String)
'                Me.strCelularPropietarioRemolque = value
'            End Set
'        End Property
'        Public Property DireccionPropietarioRemolque() As String
'            Get
'                Return Me.strDireccionPropietarioRemolque
'            End Get
'            Set(ByVal value As String)
'                Me.strDireccionPropietarioRemolque = value
'            End Set
'        End Property

'        Public Property TipoNaturalezaPropietario() As Integer
'            Get
'                Return Me.IntTipoNaturalezaPropietario
'            End Get
'            Set(ByVal value As Integer)
'                Me.IntTipoNaturalezaPropietario = value
'            End Set
'        End Property

'        Public Property TipoIdentificacionPropietario() As String
'            Get
'                Return Me.StrTipoIdentificacionPropietario
'            End Get
'            Set(ByVal value As String)
'                Me.StrTipoIdentificacionPropietario = value
'            End Set
'        End Property


'        Public Property PropietarioRemolque As Integer
'            Get
'                Return Me.intPropietarioRemolque
'            End Get
'            Set(value As Integer)
'                Me.intPropietarioRemolque = value
'            End Set
'        End Property


'        Public Property TipoNaturalezaTenedor() As Integer
'            Get
'                Return Me.IntTipoNaturalezaTenedor
'            End Get
'            Set(ByVal value As Integer)
'                Me.IntTipoNaturalezaTenedor = value
'            End Set
'        End Property

'        Public Property TipoNaturalezaConductor() As Integer
'            Get
'                Return Me.IntTipoNaturalezaConductor
'            End Get
'            Set(ByVal value As Integer)
'                Me.IntTipoNaturalezaConductor = value
'            End Set
'        End Property

'        Public Property TipoIdentificacionTenedor() As String
'            Get
'                Return Me.StrTipoIdentificacionTenedor
'            End Get
'            Set(ByVal value As String)
'                Me.StrTipoIdentificacionTenedor = value
'            End Set
'        End Property


'        Public Property TipoIdentificacionConductor() As String
'            Get
'                Return Me.StrTipoIdentificacionConductor
'            End Get
'            Set(ByVal value As String)
'                Me.StrTipoIdentificacionConductor = value
'            End Set
'        End Property


'        Public Property CodigoChequeoPropietario() As Integer
'            Get
'                Return Me.intCodigoChequeoPropietario
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCodigoChequeoPropietario = value
'            End Set
'        End Property

'        Public Property CodigoChequeoTenedor() As Integer
'            Get
'                Return Me.intCodigoChequeoTenedor
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCodigoChequeoTenedor = value
'            End Set
'        End Property

'        Public Property CodigoChequeoConductor() As Integer
'            Get
'                Return Me.intCodigoChequeoConductor
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCodigoChequeoConductor = value
'            End Set
'        End Property

'        Public Property AplicaReferencia() As Integer
'            Get
'                Return Me.intAplica_Referencia
'            End Get
'            Set(ByVal value As Integer)
'                Me.intAplica_Referencia = value
'            End Set
'        End Property

'        Public Property AplicaEmisor() As Integer
'            Get
'                Return Me.intAplica_Emisor
'            End Get
'            Set(ByVal value As Integer)
'                Me.intAplica_Emisor = value
'            End Set
'        End Property

'        Public Property AplicaFechaEmision() As Integer
'            Get
'                Return Me.intAplica_Fecha_Emision
'            End Get
'            Set(ByVal value As Integer)
'                Me.intAplica_Fecha_Emision = value
'            End Set
'        End Property

'        Public Property AplicaFechaVence() As Integer
'            Get
'                Return Me.intAplica_Fecha_Vence
'            End Get
'            Set(ByVal value As Integer)
'                Me.intAplica_Fecha_Vence = value
'            End Set
'        End Property

'        Public Property EntidadOrigen() As Integer
'            Get
'                Return Me.intEntidadOrigen
'            End Get
'            Set(ByVal value As Integer)
'                Me.intEntidadOrigen = value
'            End Set
'        End Property

'        Public Property TipoDocumentoDigitalizacion() As Integer
'            Get
'                Return Me.intTipoDocumentoDigitalizacion
'            End Get
'            Set(ByVal value As Integer)
'                Me.intTipoDocumentoDigitalizacion = value
'            End Set
'        End Property

'        Public Property ReferenciaDocumental() As String
'            Get
'                Return Me.strReferencia
'            End Get
'            Set(ByVal value As String)
'                Me.strReferencia = value
'            End Set
'        End Property

'        Public Property EmisorDocumental() As String
'            Get
'                Return Me.strEmisor
'            End Get
'            Set(ByVal value As String)
'                Me.strEmisor = value
'            End Set
'        End Property

'        Public Property FechaEmisionDocumental() As Date
'            Get
'                Return Me.dteFechaEmision
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaEmision = value
'            End Set
'        End Property

'        Public Property FechaVenceDocumental() As Date
'            Get
'                Return Me.dteFechaVence
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaVence = value
'            End Set
'        End Property

'        Public Property DocumentoAdjunto As Byte()
'            Get
'                Return Me.bytDocumentoAdjunto
'            End Get
'            Set(ByVal value As Byte())
'                Me.bytDocumentoAdjunto = value
'            End Set
'        End Property

'        Public Property NombreDocumento As String
'            Get
'                Return Me.strNombreDocumento
'            End Get
'            Set(ByVal value As String)
'                Me.strNombreDocumento = value
'            End Set
'        End Property

'        Public Property ExtensionDocumento As String
'            Get
'                Return Me.strExtensDocumento
'            End Get
'            Set(ByVal value As String)
'                Me.strExtensDocumento = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar(Optional ByVal CargaCompleta As Boolean = True) As Boolean
'            Try

'                Dim sdrEstSegVeh As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Numero, Fecha, Placa, MARC_Codigo, "
'                strSQL += "Modelo, COLO_Codigo, Clase, Placa_Semiremolque, Modelo_Repotenciado, "
'                strSQL += "Empresa_Afiliadora, Telefono_Afiliadora, Identificacion_Propietario, Nombre_Propietario, CIUD_Propietario, "
'                strSQL += "Telefono_Propietario, Identificacion_Tenedor, Nombre_Tenedor, CIUD_Tenedor, Telefono_Tenedor, "
'                strSQL += "Identificacion_Conductor, Nombre_Conductor, CIUD_Conductor, Telefono_Conductor, Nombre_Cliente, "
'                strSQL += "Nombre_Mercancia, RUTA_Codigo, Valor, Referencias_Empresas, Observaciones, "
'                strSQL += "Autorizo_Dijin_Conductor, Autorizo_Dijin_Vehiculo, Autorizo_Dijin_Tenedor, Autorizo_Colfecar_Conductor, Autorizo_Colfecar_Vehiculo, "
'                strSQL += "Autorizo_Colfecar_Tenedor, Clase_Riesgo, Autorizo_Asecarga_Vehiculo, Autorizo_Asecarga_Tenedor, Autorizo_Asecarga_Conductor, "
'                strSQL += "Autorizo_Simit, Autorizo_Licencia_Conduccion, Tiene_GPS, Clave_GPS, Nombre_Empresa_GPS, "
'                strSQL += "Telefono_GPS, Autorizado_Seguridad, Autorizado_Direccion, Observaciones_Estudio, Numero_Autorizacion, "
'                strSQL += "Anulado, Estado, Estado_Estudio, Celular_Propietario, Celular_Tenedor, Celular_Conductor,Celular_Propietario_Remolque, "
'                strSQL += "Confirmo_Propietario, Confirmo_Tenedor, Confirmo_Conductor, Referencias_Personales,Confirmo_Propietario_Remolque, "
'                strSQL += "Revision_TecnoMecanica, SOAT, Fecha_SOAT, Numeracion, TIDO_Codigo, "
'                strSQL += "Fecha_Crea, USUA_Crea, Fecha_Modifica, USUA_Modifica, Fecha_Anula, "
'                strSQL += "USUA_Anula, Causa_Anula, Fecha_Aprueba, USUA_Aprueba,  OFIC_Codigo, "
'                strSQL += "Autorizacion_Otro, Nombre_Autorizo_Otro, DefenCarga, Procuraduria, Destino_Seguro, "
'                strSQL += "Identificacion_Propietario_Remolque, Nombre_Propietario_Remolque, CIUD_Propietario_Remolque, Telefono_Propietario_Remolque, "
'                strSQL += "Usuario_GPS, Numero_Carnet_Afiliadora, Fecha_Vencimiento_Afiliadora, TERC_Aseguradora, TIVE_Codigo, "
'                strSQL += "Numero_Revision_Tecnomecanica, "
'                strSQL += "'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END, Identificacion_Afiliadora, "
'                strSQL += "Direccion_Propietario, CIUD_Residencia_Propietario, Direccion_Tenedor, CIUD_Residencia_Tenedor, Direccion_Conductor, CIUD_Residencia_Conductor,Direccion_Propietario_Remolque,CIUD_Residencia_Propietario_Remolque,TGPS_Codigo,Propietario_Remolque,"
'                strSQL += "TINA_Propietario,TINA_Conductor,TINA_Tenedor,TIID_Propietario,TIID_Conductor,TIID_Tenedor,"
'                strSQL += "Digito_Chequeo_Propietario, Digito_Chequeo_Conductor, Digito_Chequeo_Tenedor,"
'                strSQL += "'HuellaDigital' = CASE WHEN Huella_Digital IS NULL THEN NULL ELSE 1 END "
'                strSQL += "FROM Estudio_Seguridad_Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero = " & Me.lonNumero

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrEstSegVeh = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEstSegVeh.Read() Then

'                    Date.TryParse(sdrEstSegVeh("Fecha").ToString(), Me.dteFecha)
'                    Date.TryParse(sdrEstSegVeh("Fecha_Aprueba").ToString(), Me.dteFechaAprueba)
'                    Me.strPlaca = sdrEstSegVeh("Placa").ToString()
'                    Me.lonMarca = Val(sdrEstSegVeh("MARC_Codigo").ToString())
'                    Me.intModelo = Val(sdrEstSegVeh("Modelo").ToString())
'                    Me.lonColor = Val(sdrEstSegVeh("COLO_Codigo").ToString())

'                    Me.strClase = sdrEstSegVeh("Clase").ToString()
'                    Me.strSemirremolque = sdrEstSegVeh("Placa_Semiremolque").ToString()
'                    Me.intModeloRepotenciado = Val(sdrEstSegVeh("Modelo_Repotenciado").ToString())

'                    Me.strEmpresaAfiliadora = sdrEstSegVeh("Empresa_Afiliadora").ToString()
'                    Me.strTelefonoEmpresaAfiliadora = sdrEstSegVeh("Telefono_Afiliadora").ToString()
'                    Me.strIdentificacionPropietario = sdrEstSegVeh("Identificacion_Propietario").ToString()
'                    Me.strNombrePropietario = sdrEstSegVeh("Nombre_Propietario").ToString()

'                    Me.StrTipoIdentificacionPropietario = sdrEstSegVeh("TIID_Propietario").ToString()
'                    Me.IntTipoNaturalezaPropietario = sdrEstSegVeh("TINA_Propietario").ToString()


'                    Me.intCodigoChequeoPropietario = sdrEstSegVeh("Digito_Chequeo_Propietario").ToString()
'                    Me.intCodigoChequeoConductor = sdrEstSegVeh("Digito_Chequeo_Conductor").ToString()
'                    Me.intCodigoChequeoTenedor = sdrEstSegVeh("Digito_Chequeo_Tenedor").ToString()

'                    Me.strTelefonoPropietario = sdrEstSegVeh("Telefono_Propietario").ToString()
'                    Me.strIdentificacionTenedor = sdrEstSegVeh("Identificacion_Tenedor").ToString()
'                    Me.strNombreTenedor = sdrEstSegVeh("Nombre_Tenedor").ToString()
'                    Me.strTelefonoTenedor = sdrEstSegVeh("Telefono_Tenedor").ToString()
'                    ' datos propietario Remolque 
'                    Me.strIdentificacionPropietarioRemolque = sdrEstSegVeh("Identificacion_Propietario_Remolque").ToString()
'                    Me.strNombrePropietarioRemolque = sdrEstSegVeh("Nombre_Propietario_Remolque").ToString()
'                    Me.strTelefonoPropietarioRemolque = sdrEstSegVeh("Telefono_Propietario_Remolque").ToString()


'                    Me.strIdentificacionConductor = sdrEstSegVeh("Identificacion_Conductor").ToString()
'                    Me.strNombreConductor = sdrEstSegVeh("Nombre_Conductor").ToString()
'                    Me.strTelefonoConductor = sdrEstSegVeh("Telefono_Conductor").ToString()
'                    Me.strNombreCliente = sdrEstSegVeh("Nombre_Cliente").ToString()

'                    Me.StrTipoIdentificacionConductor = sdrEstSegVeh("TIID_Conductor").ToString()
'                    Me.IntTipoNaturalezaConductor = sdrEstSegVeh("TINA_Conductor").ToString()


'                    Me.StrTipoIdentificacionTenedor = sdrEstSegVeh("TIID_Tenedor").ToString()
'                    Me.IntTipoNaturalezaTenedor = sdrEstSegVeh("TINA_Tenedor").ToString()


'                    Me.strMercancia = sdrEstSegVeh("Nombre_Mercancia").ToString()
'                    Me.dblValor = Val(sdrEstSegVeh("Valor").ToString())
'                    Me.strReferenciasEmpresas = sdrEstSegVeh("Referencias_Empresas").ToString()
'                    Me.strObservaciones = sdrEstSegVeh("Observaciones").ToString()

'                    Me.intAutorizoDijinConductor = Val(sdrEstSegVeh("Autorizo_Dijin_Conductor").ToString())
'                    Me.intAutorizoDijinVehiculo = Val(sdrEstSegVeh("Autorizo_Dijin_Vehiculo").ToString())
'                    Me.intAutorizoDijinTenedor = Val(sdrEstSegVeh("Autorizo_Dijin_Tenedor").ToString())
'                    Me.intAutorizoColfecarConductor = Val(sdrEstSegVeh("Autorizo_Colfecar_Conductor").ToString())
'                    Me.intAutorizoColfecarVehiculo = Val(sdrEstSegVeh("Autorizo_Colfecar_Vehiculo").ToString())

'                    Me.intAutorizoColfecarTenedor = Val(sdrEstSegVeh("Autorizo_Colfecar_Tenedor").ToString())
'                    Me.objCataRiesgoEstudio.Campo1 = Val(sdrEstSegVeh("Clase_Riesgo").ToString())
'                    Me.intAutorizadoSeguridad = Val(sdrEstSegVeh("Autorizado_Seguridad").ToString())
'                    Me.intAutorizadoDireccion = Val(sdrEstSegVeh("Autorizado_Direccion").ToString())
'                    Me.intNumeroAutorizacion = Val(sdrEstSegVeh("Numero_Autorizacion").ToString())
'                    Me.intPropietarioRemolque = Val(sdrEstSegVeh("Propietario_Remolque").ToString())

'                    Me.intAnulado = Val(sdrEstSegVeh("Anulado").ToString())
'                    Me.objCataEstado.Campo1 = Val(sdrEstSegVeh("Estado").ToString())

'                    If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Estado_Documentos ", "Nombre", "Codigo", General.CAMPO_NUMERICO, Me.objCataEstado.Campo1, Me.objCataEstado.Campo2, , strError) Then
'                        Me.objCataEstado.Campo2 = Me.objCataEstado.Campo2
'                    End If

'                    Me.objCataEstadoEstudioSeguridad.Campo1 = Val(sdrEstSegVeh("Estado_Estudio").ToString())
'                    Me.intNumeracion = Val(sdrEstSegVeh("Numeracion").ToString())
'                    Me.strCausaAnula = sdrEstSegVeh("Causa_Anula").ToString()

'                    Date.TryParse(sdrEstSegVeh("Fecha_Anula").ToString(), dteFechaAnula)
'                    Me.strObservacionesEstudio = sdrEstSegVeh("Observaciones_Estudio").ToString()
'                    Me.intAutorizoAsecargaVehiculo = Val(sdrEstSegVeh("Autorizo_Asecarga_Vehiculo").ToString())
'                    Me.intAutorizoAsecargaTenedor = Val(sdrEstSegVeh("Autorizo_Asecarga_Tenedor").ToString())
'                    Me.intAutorizoAsecargaConductor = Val(sdrEstSegVeh("Autorizo_Asecarga_Conductor").ToString())
'                    Me.intAutorizoSimit = Val(sdrEstSegVeh("Autorizo_Simit").ToString())

'                    Me.intAutorizoLicenciaConduccion = Val(sdrEstSegVeh("Autorizo_Licencia_Conduccion").ToString())
'                    Me.intTieneGPS = Val(sdrEstSegVeh("Tiene_GPS").ToString())
'                    Me.strNombreEmpresaGPS = sdrEstSegVeh("Nombre_Empresa_GPS").ToString()
'                    Me.strClaveGPS = sdrEstSegVeh("Clave_GPS").ToString()
'                    Me.strTelefonoGPS = sdrEstSegVeh("Telefono_GPS").ToString()

'                    Me.strCelularPropietario = sdrEstSegVeh("Celular_Propietario")
'                    Me.strCelularPropietarioRemolque = sdrEstSegVeh("Celular_Propietario_Remolque")
'                    Me.strCelularTenedor = sdrEstSegVeh("Celular_Tenedor")
'                    Me.strCelularConductor = sdrEstSegVeh("Celular_Conductor")
'                    Me.strConfirmoPropietario = sdrEstSegVeh("Confirmo_Propietario")
'                    Me.strConfirmoTenedor = sdrEstSegVeh("Confirmo_Tenedor")
'                    Me.strConfirmoPropietarioRemolque = sdrEstSegVeh("Confirmo_Propietario_Remolque")

'                    Me.strConfirmoConductor = sdrEstSegVeh("Confirmo_Conductor")
'                    Me.strReferenciasPersonales = sdrEstSegVeh("Referencias_Personales")
'                    Date.TryParse(sdrEstSegVeh("Revision_TecnoMecanica"), Me.dteRevisionTecnoMecanica)
'                    Me.strSOAT = sdrEstSegVeh("SOAT")
'                    Date.TryParse(sdrEstSegVeh("Fecha_SOAT"), Me.dteFechaSOAT)

'                    If sdrEstSegVeh("Foto") IsNot DBNull.Value Then
'                        bolFoto = True
'                    Else
'                        bolFoto = False
'                    End If
'                    Me.strNombreAutorizoOtro = sdrEstSegVeh("Nombre_Autorizo_Otro")
'                    Me.intAutorizoOtro = Val(sdrEstSegVeh("Autorizacion_Otro"))
'                    Me.intDefenCarga = Val(sdrEstSegVeh("DefenCarga"))
'                    Me.intProcuraduria = Val(sdrEstSegVeh("Procuraduria").ToString())

'                    Me.intDestinoSeguro = Val(sdrEstSegVeh("Destino_Seguro").ToString())
'                    Me.strUsuarioGPS = sdrEstSegVeh("Usuario_GPS").ToString()
'                    Me.strNumeroCarnetAfiliadora = sdrEstSegVeh("Numero_Carnet_Afiliadora").ToString()
'                    Date.TryParse(sdrEstSegVeh("Fecha_Vencimiento_Afiliadora"), Me.dteFechaVencimientoAfiliadora)
'                    Me.objCataTipoVehiculo.Campo1 = Val(sdrEstSegVeh("TIVE_Codigo").ToString())
'                    Me.strNumeroRevisionTecnomecanica = sdrEstSegVeh("Numero_Revision_Tecnomecanica").ToString()

'                    Me.strIdentificacionAfiliadora = sdrEstSegVeh("Identificacion_Afiliadora").ToString()
'                    Me.strDireccionPropietario = sdrEstSegVeh("Direccion_Propietario").ToString()
'                    Me.strDireccionPropietarioRemolque = sdrEstSegVeh("Direccion_Propietario_Remolque").ToString()
'                    Me.strDireccionTenedor = sdrEstSegVeh("Direccion_Tenedor").ToString()
'                    Me.strDireccionConductor = sdrEstSegVeh("Direccion_Conductor").ToString()

'                    If sdrEstSegVeh("HuellaDigital") IsNot DBNull.Value Then
'                        bolHuellaDigital = True
'                    Else
'                        bolHuellaDigital = False
'                    End If

'                    Me.objCataTipoGps.Campo1 = sdrEstSegVeh("TGPS_Codigo").ToString()

'                    If CargaCompleta Then

'                        Me.objCiudadPropietario.Codigo = Val(sdrEstSegVeh("CIUD_Propietario").ToString())
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Ciudades", "Nombre", "Codigo", General.CAMPO_NUMERICO, Me.objCiudadPropietario.Codigo, Me.objCiudadPropietario.Nombre, , strError) Then
'                            Me.objCiudadPropietario.Nombre = Me.objCiudadPropietario.Nombre
'                        End If


'                        Me.objCiudadPropietarioRemolque.Codigo = Val(sdrEstSegVeh("CIUD_Propietario_Remolque").ToString())
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Ciudades", "Nombre", "Codigo", General.CAMPO_NUMERICO, Me.objCiudadPropietarioRemolque.Codigo, Me.objCiudadPropietarioRemolque.Nombre, , strError) Then
'                            Me.objCiudadPropietarioRemolque.Nombre = Me.objCiudadPropietarioRemolque.Nombre
'                        End If



'                        Me.objCiudadTenedor.Codigo = Val(sdrEstSegVeh("CIUD_Tenedor").ToString())
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Ciudades", "Nombre", "Codigo", General.CAMPO_NUMERICO, Me.objCiudadTenedor.Codigo, Me.objCiudadTenedor.Nombre, , strError) Then
'                            Me.objCiudadTenedor.Nombre = Me.objCiudadTenedor.Nombre
'                        End If

'                        Me.objCiudadConductor.Codigo = Val(sdrEstSegVeh("CIUD_Conductor").ToString())
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Ciudades", "Nombre", "Codigo", General.CAMPO_NUMERICO, Me.objCiudadConductor.Codigo, Me.objCiudadConductor.Nombre, , strError) Then
'                            Me.objCiudadConductor.Nombre = Me.objCiudadConductor.Nombre
'                        End If

'                        Me.objRuta.Codigo = Val(sdrEstSegVeh("Ruta_Codigo").ToString())
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Rutas", "NombreRuta", "Codigo", General.CAMPO_NUMERICO, Me.objRuta.Codigo, Me.objRuta.Nombre, , strError) Then
'                            Me.objRuta.Nombre = Me.objRuta.Nombre
'                        End If

'                        Me.objDocumento.Codigo = Val(sdrEstSegVeh("TIDO_Codigo").ToString())

'                        Me.objUsuario.Codigo = sdrEstSegVeh("USUA_Crea").ToString()
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Usuarios", "Nombre", "Codigo", General.CAMPO_NUMERICO, "'" & Me.objUsuario.Codigo & "'", Me.objUsuario.Nombre, , strError) Then
'                            Me.objUsuario.Nombre = Me.objUsuario.Nombre
'                        End If

'                        Me.objOficina.Codigo = Val(sdrEstSegVeh("OFIC_Codigo").ToString())
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Oficinas", "NombreOficina", "Codigo", General.CAMPO_NUMERICO, Me.objOficina.Codigo, Me.objOficina.Nombre, , strError) Then
'                            Me.objOficina.Nombre = Me.objOficina.Nombre
'                        End If

'                        Me.objUsuarioAnulo.Codigo = sdrEstSegVeh("USUA_Anula").ToString()
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Usuarios", "Nombre", "Codigo", General.CAMPO_NUMERICO, "'" & Me.objUsuarioAnulo.Codigo & "'", Me.objUsuarioAnulo.Nombre, , strError) Then
'                            Me.objUsuarioAnulo.Nombre = Me.objUsuarioAnulo.Nombre
'                        End If
'                        Me.objAseguradora.Codigo = Val(sdrEstSegVeh("TERC_Aseguradora").ToString())
'                        Me.objCiudadResidenciaPropietario.Existe_Registro(Me.objEmpresa, sdrEstSegVeh("CIUD_Residencia_Propietario").ToString())
'                        Me.objCiudadResidenciaPropietarioRemolque.Existe_Registro(Me.objEmpresa, sdrEstSegVeh("CIUD_Residencia_Propietario_Remolque").ToString())
'                        Me.objCiudadResidenciaTenedor.Existe_Registro(Me.objEmpresa, sdrEstSegVeh("CIUD_Residencia_Tenedor").ToString())
'                        Me.objCiudadResidenciaConductor.Existe_Registro(Me.objEmpresa, sdrEstSegVeh("CIUD_Residencia_Conductor").ToString())
'                    Else
'                        Me.objCiudadPropietario.Codigo = Val(sdrEstSegVeh("CIUD_Propietario").ToString())
'                        Me.objCiudadPropietarioRemolque.Codigo = Val(sdrEstSegVeh("CIUD_Propietario_Remolque").ToString())
'                        Me.objCiudadTenedor.Codigo = Val(sdrEstSegVeh("CIUD_Tenedor").ToString())
'                        Me.objCiudadConductor.Codigo = Val(sdrEstSegVeh("CIUD_Conductor").ToString())
'                        Me.objRuta.Codigo = Val(sdrEstSegVeh("Ruta_Codigo").ToString())
'                        Me.objDocumento.Codigo = Val(sdrEstSegVeh("TIDO_Codigo").ToString())
'                        Me.objUsuario.Codigo = sdrEstSegVeh("USUA_Crea").ToString()
'                        Me.objOficina.Codigo = Val(sdrEstSegVeh("OFIC_Codigo").ToString())
'                        Me.objUsuarioAnulo.Codigo = sdrEstSegVeh("USUA_Anula").ToString()
'                        Me.objAseguradora.Codigo = Val(sdrEstSegVeh("TERC_Aseguradora").ToString())
'                        Me.objCiudadResidenciaPropietario.Codigo = sdrEstSegVeh("CIUD_Residencia_Propietario").ToString()
'                        Me.objCiudadResidenciaPropietarioRemolque.Codigo = sdrEstSegVeh("CIUD_Residencia_Propietario_Remolque").ToString()

'                        Me.objCiudadResidenciaTenedor.Codigo = sdrEstSegVeh("CIUD_Residencia_Tenedor").ToString()
'                        Me.objCiudadResidenciaConductor.Codigo = sdrEstSegVeh("CIUD_Residencia_Conductor").ToString()
'                    End If

'                    Consultar = True
'                Else
'                    Me.lonNumero = 0
'                    Consultar = False
'                End If

'                sdrEstSegVeh.Close()

'            Catch ex As Exception
'                Me.lonNumero = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Consultar = False
'                End Try
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Numero As Long, Optional ByVal CargaCompleta As Boolean = True) As Boolean

'            Try

'                Me.objEmpresa = Empresa
'                Me.lonNumero = Numero

'                Dim sdrEstSegVeh As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Numero, Fecha, Placa, MARC_Codigo, "
'                strSQL += "Modelo, COLO_Codigo, Clase, Placa_Semiremolque, Modelo_Repotenciado, "
'                strSQL += "Empresa_Afiliadora, Telefono_Afiliadora, Identificacion_Propietario, Nombre_Propietario, CIUD_Propietario, "
'                strSQL += "Telefono_Propietario, Identificacion_Tenedor, Nombre_Tenedor, CIUD_Tenedor, Telefono_Tenedor, "
'                strSQL += "Identificacion_Conductor, Nombre_Conductor, CIUD_Conductor, Telefono_Conductor, Nombre_Cliente, "
'                strSQL += "Nombre_Mercancia, RUTA_Codigo, Valor, Referencias_Empresas, Observaciones, "
'                strSQL += "Autorizo_Dijin_Conductor, Autorizo_Dijin_Vehiculo, Autorizo_Dijin_Tenedor, Autorizo_Colfecar_Conductor, Autorizo_Colfecar_Vehiculo, "
'                strSQL += "Autorizo_Colfecar_Tenedor, Clase_Riesgo, Autorizo_Asecarga_Vehiculo, Autorizo_Asecarga_Tenedor, Autorizo_Asecarga_Conductor, "
'                strSQL += "Autorizo_Simit, Autorizo_Licencia_Conduccion, Tiene_GPS, Clave_GPS, Nombre_Empresa_GPS, "
'                strSQL += "Telefono_GPS, Autorizado_Seguridad, Autorizado_Direccion, Observaciones_Estudio, Numero_Autorizacion, "
'                strSQL += "Anulado, Estado, Estado_Estudio, Celular_Propietario, Celular_Propietario_Remolque,Celular_Tenedor, Celular_Conductor, "
'                strSQL += "Confirmo_Propietario, Confirmo_Tenedor, Confirmo_Conductor, Confirmo_Propietario_Remolque,Referencias_Personales, "
'                strSQL += "Revision_TecnoMecanica, SOAT, Fecha_SOAT, Numeracion, TIDO_Codigo, "
'                strSQL += "Fecha_Crea, USUA_Crea, Fecha_Modifica, USUA_Modifica, Fecha_Anula, "
'                strSQL += "USUA_Anula, Causa_Anula, Fecha_Aprueba, USUA_Aprueba,  OFIC_Codigo, "
'                strSQL += "Identificacion_Propietario_Remolque, Nombre_Propietario_Remolque, CIUD_Propietario_Remolque, Telefono_Propietario_Remolque, "
'                strSQL += "Autorizacion_Otro, Nombre_Autorizo_Otro, DefenCarga, Procuraduria, Destino_Seguro, "
'                strSQL += "Usuario_GPS, Numero_Carnet_Afiliadora, Fecha_Vencimiento_Afiliadora, TERC_Aseguradora, TIVE_Codigo, "
'                strSQL += "Numero_Revision_Tecnomecanica, "
'                strSQL += "'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END, Identificacion_Afiliadora, "
'                strSQL += "Direccion_Propietario, CIUD_Residencia_Propietario, Direccion_Tenedor, CIUD_Residencia_Tenedor, Direccion_Conductor, CIUD_Residencia_Conductor, Direccion_Propietario_Remolque,CIUD_Residencia_Propietario_Remolque, TGPS_Codigo,Propietario_Remolque,"
'                strSQL += "TINA_Propietario,TINA_Conductor,TINA_Tenedor,TIID_Propietario,TIID_Conductor,TIID_Tenedor,"
'                strSQL += "Digito_Chequeo_Propietario, Digito_Chequeo_Conductor, Digito_Chequeo_Tenedor,"
'                strSQL += "'HuellaDigital' = CASE WHEN Huella_Digital IS NULL THEN NULL ELSE 1 END "
'                strSQL += "FROM Estudio_Seguridad_Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero = " & Me.lonNumero

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrEstSegVeh = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEstSegVeh.Read() Then

'                    Date.TryParse(sdrEstSegVeh("Fecha").ToString(), Me.dteFecha)
'                    Date.TryParse(sdrEstSegVeh("Fecha_Aprueba").ToString(), Me.dteFechaAprueba)
'                    Me.strPlaca = sdrEstSegVeh("Placa").ToString()
'                    Me.lonMarca = Val(sdrEstSegVeh("MARC_Codigo").ToString())

'                    Me.intModelo = Val(sdrEstSegVeh("Modelo").ToString())
'                    Me.lonColor = Val(sdrEstSegVeh("COLO_Codigo").ToString())

'                    Me.strClase = sdrEstSegVeh("Clase").ToString()
'                    Me.strSemirremolque = sdrEstSegVeh("Placa_Semiremolque").ToString()
'                    Me.intModeloRepotenciado = Val(sdrEstSegVeh("Modelo_Repotenciado").ToString())
'                    Me.intPropietarioRemolque = Val(sdrEstSegVeh("Propietario_Remolque").ToString())
'                    Me.strEmpresaAfiliadora = sdrEstSegVeh("Empresa_Afiliadora").ToString()
'                    Me.strTelefonoEmpresaAfiliadora = sdrEstSegVeh("Telefono_Afiliadora").ToString()
'                    Me.strIdentificacionPropietario = sdrEstSegVeh("Identificacion_Propietario").ToString()
'                    Me.strNombrePropietario = sdrEstSegVeh("Nombre_Propietario").ToString()

'                    Me.StrTipoIdentificacionPropietario = sdrEstSegVeh("TIID_Propietario").ToString()
'                    Me.IntTipoNaturalezaPropietario = sdrEstSegVeh("TINA_Propietario").ToString()





'                    Me.strTelefonoPropietario = sdrEstSegVeh("Telefono_Propietario").ToString()
'                    Me.strIdentificacionTenedor = sdrEstSegVeh("Identificacion_Tenedor").ToString()
'                    Me.strNombreTenedor = sdrEstSegVeh("Nombre_Tenedor").ToString()
'                    Me.strTelefonoTenedor = sdrEstSegVeh("Telefono_Tenedor").ToString()
'                    ' datos propietario Remolque 
'                    Me.strIdentificacionPropietarioRemolque = sdrEstSegVeh("Identificacion_Propietario_Remolque").ToString()
'                    Me.strNombrePropietarioRemolque = sdrEstSegVeh("Nombre_Propietario_Remolque").ToString()
'                    Me.strTelefonoPropietarioRemolque = sdrEstSegVeh("Telefono_Propietario_Remolque").ToString()


'                    Me.strIdentificacionConductor = sdrEstSegVeh("Identificacion_Conductor").ToString()
'                    Me.strNombreConductor = sdrEstSegVeh("Nombre_Conductor").ToString()
'                    Me.strTelefonoConductor = sdrEstSegVeh("Telefono_Conductor").ToString()
'                    Me.strNombreCliente = sdrEstSegVeh("Nombre_Cliente").ToString()

'                    Me.StrTipoIdentificacionConductor = sdrEstSegVeh("TIID_Conductor").ToString()
'                    Me.IntTipoNaturalezaConductor = sdrEstSegVeh("TINA_Conductor").ToString()


'                    Me.StrTipoIdentificacionTenedor = sdrEstSegVeh("TIID_Tenedor").ToString()
'                    Me.IntTipoNaturalezaTenedor = sdrEstSegVeh("TINA_Tenedor").ToString()


'                    Me.intCodigoChequeoPropietario = sdrEstSegVeh("Digito_Chequeo_Propietario").ToString()
'                    Me.intCodigoChequeoConductor = sdrEstSegVeh("Digito_Chequeo_Conductor").ToString()
'                    Me.intCodigoChequeoTenedor = sdrEstSegVeh("Digito_Chequeo_Tenedor").ToString()

'                    Me.strMercancia = sdrEstSegVeh("Nombre_Mercancia").ToString()
'                    Me.dblValor = Val(sdrEstSegVeh("Valor").ToString())
'                    Me.strReferenciasEmpresas = sdrEstSegVeh("Referencias_Empresas").ToString()
'                    Me.strObservaciones = sdrEstSegVeh("Observaciones").ToString()

'                    Me.intAutorizoDijinConductor = Val(sdrEstSegVeh("Autorizo_Dijin_Conductor").ToString())
'                    Me.intAutorizoDijinVehiculo = Val(sdrEstSegVeh("Autorizo_Dijin_Vehiculo").ToString())
'                    Me.intAutorizoDijinTenedor = Val(sdrEstSegVeh("Autorizo_Dijin_Tenedor").ToString())
'                    Me.intAutorizoColfecarConductor = Val(sdrEstSegVeh("Autorizo_Colfecar_Conductor").ToString())
'                    Me.intAutorizoColfecarVehiculo = Val(sdrEstSegVeh("Autorizo_Colfecar_Vehiculo").ToString())
'                    Me.intPropietarioRemolque = Val(sdrEstSegVeh("Propietario_Remolque").ToString())

'                    Me.intAutorizoColfecarTenedor = Val(sdrEstSegVeh("Autorizo_Colfecar_Tenedor").ToString())
'                    Me.objCataRiesgoEstudio.intCampoCodigo = Val(sdrEstSegVeh("Clase_Riesgo").ToString())
'                    Me.intAutorizadoSeguridad = Val(sdrEstSegVeh("Autorizado_Seguridad").ToString())
'                    Me.intAutorizadoDireccion = Val(sdrEstSegVeh("Autorizado_Direccion").ToString())
'                    Me.intNumeroAutorizacion = Val(sdrEstSegVeh("Numero_Autorizacion").ToString())

'                    Me.intAnulado = Val(sdrEstSegVeh("Anulado").ToString())
'                    Me.objCataEstado.Campo1 = Val(sdrEstSegVeh("Estado").ToString())
'                    Me.objCataEstadoEstudioSeguridad.Campo1 = Val(sdrEstSegVeh("Estado_Estudio").ToString())
'                    Me.intNumeracion = Val(sdrEstSegVeh("Numeracion").ToString())
'                    Me.strCausaAnula = sdrEstSegVeh("Causa_Anula").ToString()

'                    Date.TryParse(sdrEstSegVeh("Fecha_Anula").ToString(), dteFechaAnula)
'                    Me.strObservacionesEstudio = sdrEstSegVeh("Observaciones_Estudio").ToString()
'                    Me.intAutorizoAsecargaVehiculo = Val(sdrEstSegVeh("Autorizo_Asecarga_Vehiculo").ToString())
'                    Me.intAutorizoAsecargaTenedor = Val(sdrEstSegVeh("Autorizo_Asecarga_Tenedor").ToString())
'                    Me.intAutorizoAsecargaConductor = Val(sdrEstSegVeh("Autorizo_Asecarga_Conductor").ToString())
'                    Me.intAutorizoSimit = Val(sdrEstSegVeh("Autorizo_Simit").ToString())

'                    Me.intAutorizoLicenciaConduccion = Val(sdrEstSegVeh("Autorizo_Licencia_Conduccion").ToString())
'                    Me.intTieneGPS = Val(sdrEstSegVeh("Tiene_GPS").ToString())
'                    Me.strNombreEmpresaGPS = sdrEstSegVeh("Nombre_Empresa_GPS").ToString()
'                    Me.strClaveGPS = sdrEstSegVeh("Clave_GPS").ToString()
'                    Me.strTelefonoGPS = sdrEstSegVeh("Telefono_GPS").ToString()

'                    Me.strCelularPropietario = sdrEstSegVeh("Celular_Propietario")
'                    Me.strCelularPropietarioRemolque = sdrEstSegVeh("Celular_Propietario_Remolque")

'                    Me.strCelularTenedor = sdrEstSegVeh("Celular_Tenedor")
'                    Me.strCelularConductor = sdrEstSegVeh("Celular_Conductor")
'                    Me.strConfirmoPropietario = sdrEstSegVeh("Confirmo_Propietario")
'                    Me.strConfirmoPropietarioRemolque = sdrEstSegVeh("Confirmo_Propietario_Remolque")

'                    Me.strConfirmoTenedor = sdrEstSegVeh("Confirmo_Tenedor")

'                    Me.strConfirmoConductor = sdrEstSegVeh("Confirmo_Conductor")
'                    Me.strReferenciasPersonales = sdrEstSegVeh("Referencias_Personales")
'                    Date.TryParse(sdrEstSegVeh("Revision_TecnoMecanica"), Me.dteRevisionTecnoMecanica)
'                    Me.strSOAT = sdrEstSegVeh("SOAT")
'                    Date.TryParse(sdrEstSegVeh("Fecha_SOAT"), Me.dteFechaSOAT)

'                    If sdrEstSegVeh("Foto") IsNot DBNull.Value Then
'                        bolFoto = True
'                    Else
'                        bolFoto = False
'                    End If
'                    Me.strNombreAutorizoOtro = sdrEstSegVeh("Nombre_Autorizo_Otro")
'                    Me.intAutorizoOtro = Val(sdrEstSegVeh("Autorizacion_Otro"))
'                    Me.intDefenCarga = Val(sdrEstSegVeh("DefenCarga"))
'                    Me.intProcuraduria = Val(sdrEstSegVeh("Procuraduria").ToString())

'                    Me.intDestinoSeguro = Val(sdrEstSegVeh("Destino_Seguro").ToString())
'                    Me.strUsuarioGPS = sdrEstSegVeh("Usuario_GPS").ToString()
'                    Me.strNumeroCarnetAfiliadora = sdrEstSegVeh("Numero_Carnet_Afiliadora").ToString()
'                    Date.TryParse(sdrEstSegVeh("Fecha_Vencimiento_Afiliadora"), Me.dteFechaVencimientoAfiliadora)
'                    Me.objCataTipoVehiculo.Campo1 = Val(sdrEstSegVeh("TIVE_Codigo").ToString())

'                    Me.strNumeroRevisionTecnomecanica = sdrEstSegVeh("Numero_Revision_Tecnomecanica").ToString()

'                    Me.strIdentificacionAfiliadora = sdrEstSegVeh("Identificacion_Afiliadora").ToString()
'                    Me.strDireccionPropietario = sdrEstSegVeh("Direccion_Propietario").ToString()
'                    Me.strDireccionPropietarioRemolque = sdrEstSegVeh("Direccion_Propietario_Remolque").ToString()

'                    Me.strDireccionTenedor = sdrEstSegVeh("Direccion_Tenedor").ToString()
'                    Me.strDireccionConductor = sdrEstSegVeh("Direccion_Conductor").ToString()

'                    If sdrEstSegVeh("HuellaDigital") IsNot DBNull.Value Then
'                        bolHuellaDigital = True
'                    Else
'                        bolHuellaDigital = False
'                    End If

'                    Me.objCataTipoGps.Campo1 = sdrEstSegVeh("TGPS_Codigo").ToString()

'                    If CargaCompleta Then

'                        Me.objCiudadPropietario.Codigo = Val(sdrEstSegVeh("CIUD_Propietario").ToString())
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Ciudades", "Nombre", "Codigo", General.CAMPO_NUMERICO, Me.objCiudadPropietario.Codigo, Me.objCiudadPropietario.Nombre, , strError) Then
'                            Me.objCiudadPropietario.Nombre = Me.objCiudadPropietario.Nombre
'                        End If

'                        Me.objCiudadPropietarioRemolque.Codigo = Val(sdrEstSegVeh("CIUD_Propietario_Remolque").ToString())
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Ciudades", "Nombre", "Codigo", General.CAMPO_NUMERICO, Me.objCiudadPropietario.Codigo, Me.objCiudadPropietario.Nombre, , strError) Then
'                            Me.objCiudadPropietarioRemolque.Nombre = Me.objCiudadPropietarioRemolque.Nombre
'                        End If


'                        Me.objCiudadTenedor.Codigo = Val(sdrEstSegVeh("CIUD_Tenedor").ToString())
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Ciudades", "Nombre", "Codigo", General.CAMPO_NUMERICO, Me.objCiudadTenedor.Codigo, Me.objCiudadTenedor.Nombre, , strError) Then
'                            Me.objCiudadTenedor.Nombre = Me.objCiudadTenedor.Nombre
'                        End If

'                        Me.objCiudadConductor.Codigo = Val(sdrEstSegVeh("CIUD_Conductor").ToString())
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Ciudades", "Nombre", "Codigo", General.CAMPO_NUMERICO, Me.objCiudadConductor.Codigo, Me.objCiudadConductor.Nombre, , strError) Then
'                            Me.objCiudadConductor.Nombre = Me.objCiudadConductor.Nombre
'                        End If

'                        Me.objRuta.Codigo = Val(sdrEstSegVeh("Ruta_Codigo").ToString())
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Rutas", "NombreRuta", "Codigo", General.CAMPO_NUMERICO, Me.objRuta.Codigo, Me.objRuta.Nombre, , strError) Then
'                            Me.objRuta.Nombre = Me.objRuta.Nombre
'                        End If

'                        Me.objDocumento.Codigo = Val(sdrEstSegVeh("TIDO_Codigo").ToString())

'                        Me.objUsuario.Codigo = sdrEstSegVeh("USUA_Crea").ToString()
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Usuarios", "Nombre", "Codigo", General.CAMPO_NUMERICO, "'" & Me.objUsuario.Codigo & "'", Me.objUsuario.Nombre, , strError) Then
'                            Me.objUsuario.Nombre = Me.objUsuario.Nombre
'                        End If

'                        Me.objOficina.Codigo = Val(sdrEstSegVeh("OFIC_Codigo").ToString())
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Oficinas", "NombreOficina", "Codigo", General.CAMPO_NUMERICO, Me.objOficina.Codigo, Me.objOficina.Nombre, , strError) Then
'                            Me.objOficina.Nombre = Me.objOficina.Nombre
'                        End If

'                        Me.objUsuarioAnulo.Codigo = sdrEstSegVeh("USUA_Anula").ToString()
'                        If Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "V_Usuarios", "Nombre", "Codigo", General.CAMPO_NUMERICO, "'" & Me.objUsuarioAnulo.Codigo & "'", Me.objUsuarioAnulo.Nombre, , strError) Then
'                            Me.objUsuarioAnulo.Nombre = Me.objUsuarioAnulo.Nombre
'                        End If


'                        Me.objAseguradora.Codigo = Val(sdrEstSegVeh("TERC_Aseguradora").ToString())
'                        Me.objCiudadResidenciaPropietario.Existe_Registro(Me.objEmpresa, sdrEstSegVeh("CIUD_Residencia_Propietario").ToString())
'                        Me.objCiudadResidenciaPropietarioRemolque.Existe_Registro(Me.objEmpresa, sdrEstSegVeh("CIUD_Residencia_Propietario_Remolque").ToString())

'                        Me.objCiudadResidenciaTenedor.Existe_Registro(Me.objEmpresa, sdrEstSegVeh("CIUD_Residencia_Tenedor").ToString())
'                        Me.objCiudadResidenciaConductor.Existe_Registro(Me.objEmpresa, sdrEstSegVeh("CIUD_Residencia_Conductor").ToString())


'                    Else
'                        Me.objCiudadPropietario.Codigo = Val(sdrEstSegVeh("CIUD_Propietario").ToString())
'                        Me.objCiudadPropietarioRemolque.Codigo = Val(sdrEstSegVeh("CIUD_Propietario_Remolque").ToString())
'                        Me.objCiudadTenedor.Codigo = Val(sdrEstSegVeh("CIUD_Tenedor").ToString())
'                        Me.objCiudadConductor.Codigo = Val(sdrEstSegVeh("CIUD_Conductor").ToString())
'                        Me.objRuta.Codigo = Val(sdrEstSegVeh("Ruta_Codigo").ToString())
'                        Me.objDocumento.Codigo = Val(sdrEstSegVeh("TIDO_Codigo").ToString())
'                        Me.objUsuario.Codigo = sdrEstSegVeh("USUA_Crea").ToString()
'                        Me.objOficina.Codigo = Val(sdrEstSegVeh("OFIC_Codigo").ToString())
'                        Me.objUsuarioAnulo.Codigo = sdrEstSegVeh("USUA_Anula").ToString()
'                        Me.objAseguradora.Codigo = Val(sdrEstSegVeh("TERC_Aseguradora").ToString())
'                        Me.objCiudadResidenciaPropietario.Codigo = sdrEstSegVeh("CIUD_Residencia_Propietario").ToString()
'                        Me.objCiudadResidenciaPropietarioRemolque.Codigo = sdrEstSegVeh("CIUD_Residencia_Propietario_Remolque").ToString()


'                        Me.objCiudadResidenciaTenedor.Codigo = sdrEstSegVeh("CIUD_Residencia_Tenedor").ToString()
'                        Me.objCiudadResidenciaConductor.Codigo = sdrEstSegVeh("CIUD_Residencia_Conductor").ToString()

'                    End If

'                    Consultar = True
'                Else
'                    Me.lonNumero = 0
'                    Consultar = False
'                End If

'                sdrEstSegVeh.Close()

'            Catch ex As Exception
'                Me.lonNumero = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Consultar = False
'                End Try
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Cargar_Estudio_Seguridad_Terceros(ByRef Empresa As Empresas, ByVal Numero As Long) As Boolean

'            Try

'                Cargar_Estudio_Seguridad_Terceros = True
'                Me.objEmpresa = Empresa
'                Me.lonNumero = Numero

'                Dim sdrEstSegVeh As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Numero, Fecha, Identificacion_Propietario, Nombre_Propietario,  "
'                strSQL += " CIUD_Propietario, Telefono_Propietario, Identificacion_Tenedor, Nombre_Tenedor, CIUD_Tenedor,"
'                strSQL += " Identificacion_Propietario_Remolque, Telefono_Propietario_Remolque, Nombre_Propietario_Remolque,"
'                strSQL += " Telefono_Tenedor, Identificacion_Conductor, Nombre_Conductor, CIUD_Conductor, Telefono_Conductor, "
'                strSQL += " Anulado, Estado, Estado_Estudio, Celular_Propietario, Celular_Tenedor,Celular_Propietario_Remolque, CIUD_Propietario_Remolque,"
'                strSQL += " Celular_Conductor, CiudadConductor, CiudadPropietario, CiudadPropietarioRemolque, CiudadTenedor, CIUD_Residencia_Propietario, CIUD_Residencia_Propietario_Remolque, "
'                strSQL += " CIUD_Residencia_Tenedor, CIUD_Residencia_Conductor,CiudadResidenciaPropietario,CiudadResidenciaPropietarioRemolque, CiudadResidenciaConductor, "
'                strSQL += " CiudadResidenciaTenedor, Direccion_Propietario, Direccion_Tenedor, Direccion_Conductor,Direccion_Propietario_Remolque, "
'                strSQL += "TINA_Propietario,TINA_Conductor,TINA_Tenedor,TIID_Propietario,TIID_Conductor,TIID_Tenedor,"
'                strSQL += "Digito_Chequeo_Propietario, Digito_Chequeo_Conductor, Digito_Chequeo_Tenedor"

'                strSQL += " FROM V_Estudio_Seguridad_Vehiculo_Terceros"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero = " & Me.lonNumero

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrEstSegVeh = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEstSegVeh.Read() Then

'                    Date.TryParse(sdrEstSegVeh("Fecha").ToString(), Me.dteFecha)
'                    Me.strIdentificacionPropietario = sdrEstSegVeh("Identificacion_Propietario").ToString()
'                    Me.strNombrePropietario = sdrEstSegVeh("Nombre_Propietario").ToString()
'                    Me.strTelefonoPropietario = sdrEstSegVeh("Telefono_Propietario").ToString()
'                    Me.strIdentificacionTenedor = sdrEstSegVeh("Identificacion_Tenedor").ToString()

'                    Me.strIdentificacionPropietarioRemolque = sdrEstSegVeh("Identificacion_Propietario_Remolque").ToString()
'                    Me.strNombrePropietarioRemolque = sdrEstSegVeh("Nombre_Propietario_Remolque").ToString()
'                    Me.strTelefonoPropietarioRemolque = sdrEstSegVeh("Telefono_Propietario_Remolque").ToString()





'                    Me.strNombreTenedor = sdrEstSegVeh("Nombre_Tenedor").ToString()
'                    Me.strTelefonoTenedor = sdrEstSegVeh("Telefono_Tenedor").ToString()
'                    Me.strIdentificacionConductor = sdrEstSegVeh("Identificacion_Conductor").ToString()
'                    Me.strNombreConductor = sdrEstSegVeh("Nombre_Conductor").ToString()
'                    Me.strTelefonoConductor = sdrEstSegVeh("Telefono_Conductor").ToString()

'                    Me.intAnulado = Val(sdrEstSegVeh("Anulado").ToString())
'                    Me.objCataEstado.Campo1 = Val(sdrEstSegVeh("Estado").ToString())
'                    Me.objCataEstadoEstudioSeguridad.Campo1 = Val(sdrEstSegVeh("Estado_Estudio").ToString())
'                    Me.strCelularPropietario = sdrEstSegVeh("Celular_Propietario")
'                    Me.strCelularTenedor = sdrEstSegVeh("Celular_Tenedor")
'                    Me.strCelularPropietarioRemolque = sdrEstSegVeh("Celular_Propietario_Remolque")

'                    Me.strCelularConductor = sdrEstSegVeh("Celular_Conductor")
'                    Me.objCiudadPropietario.Codigo = Val(sdrEstSegVeh("CIUD_Propietario").ToString())
'                    Me.objCiudadPropietario.Nombre = sdrEstSegVeh("CiudadPropietario").ToString()

'                    Me.objCiudadPropietarioRemolque.Codigo = Val(sdrEstSegVeh("CIUD_Propietario_Remolque").ToString())
'                    Me.objCiudadPropietarioRemolque.Nombre = sdrEstSegVeh("CiudadPropietarioRemolque").ToString()



'                    Me.objCiudadTenedor.Codigo = Val(sdrEstSegVeh("CIUD_Tenedor").ToString())
'                    Me.objCiudadTenedor.Nombre = sdrEstSegVeh("CiudadTenedor").ToString()

'                    Me.objCiudadConductor.Codigo = Val(sdrEstSegVeh("CIUD_Conductor").ToString())
'                    Me.objCiudadConductor.Nombre = sdrEstSegVeh("CiudadConductor").ToString()
'                    Me.objCiudadResidenciaPropietario.Codigo = Val(sdrEstSegVeh("CIUD_Residencia_Propietario").ToString())
'                    Me.objCiudadResidenciaPropietario.Nombre = sdrEstSegVeh("CiudadResidenciaPropietario").ToString()
'                    Me.objCiudadResidenciaTenedor.Codigo = Val(sdrEstSegVeh("CIUD_Residencia_Tenedor").ToString())
'                    Me.objCiudadResidenciaPropietarioRemolque.Codigo = Val(sdrEstSegVeh("CIUD_Residencia_Propietario_Remolque").ToString())
'                    Me.objCiudadResidenciaPropietarioRemolque.Nombre = sdrEstSegVeh("CiudadResidenciaPropietarioRemolque").ToString()


'                    Me.objCiudadResidenciaTenedor.Nombre = sdrEstSegVeh("CiudadResidenciaTenedor").ToString()
'                    Me.objCiudadResidenciaConductor.Codigo = Val(sdrEstSegVeh("CIUD_Residencia_Conductor").ToString())
'                    Me.objCiudadResidenciaConductor.Nombre = sdrEstSegVeh("CiudadResidenciaConductor").ToString()
'                    Me.strDireccionPropietario = sdrEstSegVeh("Direccion_Propietario").ToString()
'                    Me.strDireccionTenedor = sdrEstSegVeh("Direccion_Tenedor").ToString()
'                    Me.strDireccionPropietarioRemolque = sdrEstSegVeh("Direccion_Propietario_Remolque").ToString()
'                    Me.strDireccionConductor = sdrEstSegVeh("Direccion_Conductor").ToString()

'                    Me.strIdentificacionPropietario = sdrEstSegVeh("Identificacion_Propietario").ToString()
'                    Me.strNombrePropietario = sdrEstSegVeh("Nombre_Propietario").ToString()

'                    Me.StrTipoIdentificacionPropietario = sdrEstSegVeh("TIID_Propietario").ToString()
'                    Me.IntTipoNaturalezaPropietario = sdrEstSegVeh("TINA_Propietario").ToString()

'                    Me.StrTipoIdentificacionConductor = sdrEstSegVeh("TIID_Conductor").ToString()
'                    Me.IntTipoNaturalezaConductor = sdrEstSegVeh("TINA_Conductor").ToString()


'                    Me.StrTipoIdentificacionTenedor = sdrEstSegVeh("TIID_Tenedor").ToString()
'                    Me.IntTipoNaturalezaTenedor = sdrEstSegVeh("TINA_Tenedor").ToString()


'                    Me.intCodigoChequeoPropietario = sdrEstSegVeh("Digito_Chequeo_Propietario").ToString()
'                    Me.intCodigoChequeoConductor = sdrEstSegVeh("Digito_Chequeo_Conductor").ToString()
'                    Me.intCodigoChequeoTenedor = sdrEstSegVeh("Digito_Chequeo_Tenedor").ToString()

'                    Cargar_Estudio_Seguridad_Terceros = True
'                Else
'                    Me.lonNumero = 0
'                    Cargar_Estudio_Seguridad_Terceros = False
'                End If

'                sdrEstSegVeh.Close()

'            Catch ex As Exception
'                Me.lonNumero = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Estudio_Seguridad_Terceros: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Cargar_Estudio_Seguridad_Terceros = False
'                End Try
'                Cargar_Estudio_Seguridad_Terceros = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Guardar(ByRef Mensaje As String) As Boolean

'            Try
'                Dim bolGuardo As Boolean
'                If Datos_Requeridos(Mensaje) Then

'                    Me.objGeneral.ConexionSQL.Open()
'                    Me.objGeneral.ComandoSQL = Me.objGeneral.ConexionSQL.CreateCommand()
'                    Me.objGeneral.ComandoSQL.Connection = Me.objGeneral.ConexionSQL()

'                    Me.objGeneral.TransaccionSQL = Me.objGeneral.ConexionSQL.BeginTransaction()
'                    Me.objGeneral.ComandoSQL.Transaction = Me.objGeneral.TransaccionSQL

'                    If Not bolModificar Then
'                        bolGuardo = Insertar_SQL()
'                    Else
'                        bolGuardo = Modificar_SQL()
'                    End If

'                    If bolGuardo Then
'                        Actualizar_Datos_Terceros()
'                    End If

'                    If bolGuardo Then
'                        Me.objGeneral.TransaccionSQL.Commit()
'                    Else
'                        Me.objGeneral.TransaccionSQL.Rollback()
'                    End If

'                    If bolGuardo And objValidacion.Consultar(Me.objEmpresa, PERMISO_GESTION_DOCUMENTAL) Then
'                        Trasladar_T_Documentos(General.HACIA_DEFINITIVA)
'                    End If

'                End If
'                Mensaje = Me.strError
'                Return bolGuardo

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                Guardar = False
'            End Try

'        End Function

'        Public Function Guardar_T_Documentos(ByRef Mensaje As String) As Boolean
'            Try
'                Dim bolGuardo As Boolean
'                If Datos_Requeridos_Documentos(Mensaje) Then

'                    Me.objGeneral.ConexionDocumentalSQL.Open()
'                    Me.objGeneral.ComandoSQL = Me.objGeneral.ConexionDocumentalSQL.CreateCommand()
'                    Me.objGeneral.ComandoSQL.Connection = Me.objGeneral.ConexionDocumentalSQL()

'                    bolGuardo = Insertar_T_Documentos_SQL(Mensaje)
'                End If
'                'Mensaje = Me.strError
'                Return bolGuardo

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                Guardar_T_Documentos = False
'            End Try

'        End Function

'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Eliminar = Anular_SQL()
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                Eliminar = False
'            End Try
'        End Function

'        Public Function Eliminar_Foto(ByVal Numero As Long) As Boolean
'            Try
'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_eliminar_foto_estudio_seguridad_vehiculos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Numero", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Numero").Value = Numero

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Eliminar_Foto = True
'                Else
'                    Eliminar_Foto = False
'                End If

'            Catch ex As Exception
'                Eliminar_Foto = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_Foto : " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Eliminar_Foto = False
'                End Try

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Eliminar_Huella_Digital(ByVal Numero As Long) As Boolean

'            Try
'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_eliminar_huella_estudio_seguridad_vehiculos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Numero", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Numero").Value = Numero

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Eliminar_Huella_Digital = True
'                Else
'                    Eliminar_Huella_Digital = False
'                End If

'            Catch ex As Exception
'                Eliminar_Huella_Digital = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_Huella_Digital: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Eliminar_Huella_Digital = False
'                End Try

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro() As Boolean

'            Try
'                Dim sdrEstudioSeguridad As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Fecha, Fecha_Aprueba FROM Estudio_Seguridad_Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero = " & Me.lonNumero


'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()
'                sdrEstudioSeguridad = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEstudioSeguridad.Read() Then
'                    Existe_Registro = True
'                    Date.TryParse(sdrEstudioSeguridad("Fecha").ToString(), Me.dteFecha)
'                    Date.TryParse(sdrEstudioSeguridad("Fecha_Aprueba").ToString(), Me.dteFechaAprueba)
'                Else
'                    Me.lonNumero = 0
'                    Existe_Registro = False
'                End If

'                sdrEstudioSeguridad.Close()

'            Catch ex As Exception

'                Me.lonNumero = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(ex.Message)
'                Existe_Registro = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If

'            End Try

'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Numero As Long) As Boolean

'            Try
'                Dim sdrEstudioSeguridad As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                Me.objEmpresa = Empresa
'                Me.lonNumero = Numero

'                strSQL = "SELECT EMPR_Codigo, Fecha, Fecha_Aprueba FROM Estudio_Seguridad_Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero = " & Me.lonNumero

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()
'                sdrEstudioSeguridad = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEstudioSeguridad.Read() Then
'                    Existe_Registro = True
'                    Date.TryParse(sdrEstudioSeguridad("Fecha").ToString(), Me.dteFecha)
'                    Date.TryParse(sdrEstudioSeguridad("Fecha_Aprueba").ToString(), Me.dteFechaAprueba)
'                Else
'                    Me.lonNumero = 0
'                    Existe_Registro = False
'                End If

'                sdrEstudioSeguridad.Close()

'            Catch ex As Exception

'                Me.lonNumero = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(ex.Message)
'                Existe_Registro = False
'            Finally

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If

'            End Try

'        End Function
'#End Region

'#Region "Funciones Privadas"

'        Private Function Insertar_SQL() As Boolean
'            Dim lonNumeRegi As Long

'            Try

'                Me.objGeneral.ComandoSQL.CommandText = "sp_insertar_estudio_seguridad_vehiculos"
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                Me.objGeneral.ComandoSQL.Parameters.Clear()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero").Direction = ParameterDirection.Output
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha").Value = Me.dteFecha
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Placa", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Placa").Value = Me.strPlaca
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_MARC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_MARC_Codigo").Value = Me.lonMarca

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Modelo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Modelo").Value = Me.intModelo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_COLO_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_COLO_Codigo").Value = Me.lonColor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Clase", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Clase").Value = Me.strClase
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Placa_Semiremolque", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Placa_Semiremolque").Value = Me.strSemirremolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Modelo_Repotenciado", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Modelo_Repotenciado").Value = Me.intModeloRepotenciado

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Empresa_Afiliadora", SqlDbType.VarChar, 40) : Me.objGeneral.ComandoSQL.Parameters("@par_Empresa_Afiliadora").Value = Me.strEmpresaAfiliadora
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_Afiliadora", SqlDbType.VarChar, 40) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_Afiliadora").Value = Me.strTelefonoEmpresaAfiliadora
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Identificacion_Propietario", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Identificacion_Propietario").Value = Me.strIdentificacionPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Propietario", SqlDbType.VarChar, 35) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Propietario").Value = Me.strNombrePropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Propietario", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Propietario").Value = Me.objCiudadPropietario.Codigo

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_Propietario", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_Propietario").Value = Me.strTelefonoPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Identificacion_Tenedor", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Identificacion_Tenedor").Value = Me.strIdentificacionTenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Tenedor", SqlDbType.VarChar, 35) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Tenedor").Value = Me.strNombreTenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Tenedor", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Tenedor").Value = Me.objCiudadTenedor.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_Tenedor", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_Tenedor").Value = Me.strTelefonoTenedor

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Identificacion_Conductor", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Identificacion_Conductor").Value = Me.strIdentificacionConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Conductor", SqlDbType.VarChar, 35) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Conductor").Value = Me.strNombreConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Conductor", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Conductor").Value = Me.objCiudadConductor.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_Conductor", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_Conductor").Value = Me.strTelefonoConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Cliente", SqlDbType.VarChar, 35) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Cliente").Value = Me.strNombreCliente

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Mercancia", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Mercancia").Value = Me.strMercancia
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_RUTA_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_RUTA_Codigo").Value = Me.objRuta.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Valor", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Valor").Value = Me.dblValor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Referencias_Empresas", SqlDbType.VarChar, 250) : Me.objGeneral.ComandoSQL.Parameters("@par_Referencias_Empresas").Value = Me.strReferenciasEmpresas
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Observaciones", SqlDbType.VarChar, 250) : Me.objGeneral.ComandoSQL.Parameters("@par_Observaciones").Value = Me.strObservaciones

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Dijin_Conductor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Dijin_Conductor").Value = Me.intAutorizoDijinConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Dijin_Vehiculo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Dijin_Vehiculo").Value = Me.intAutorizoDijinVehiculo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Dijin_Tenedor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Dijin_Tenedor").Value = Me.intAutorizoDijinTenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Colfecar_Conductor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Colfecar_Conductor").Value = Me.intAutorizoColfecarConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Colfecar_Vehiculo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Colfecar_Vehiculo").Value = Me.intAutorizoColfecarVehiculo

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Colfecar_Tenedor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Colfecar_Tenedor").Value = Me.intAutorizoColfecarTenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Clase_Riesgo", SqlDbType.Char, 1) : Me.objGeneral.ComandoSQL.Parameters("@par_Clase_Riesgo").Value = Me.objCataRiesgoEstudio.Retorna_Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizado_Seguridad", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizado_Seguridad").Value = Me.intAutorizadoSeguridad
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizado_Direccion", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizado_Direccion").Value = Me.intAutorizadoDireccion

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Anulado", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Anulado").Value = Me.intAnulado
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Estado").Value = Val(Me.objCataEstado.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Estado_Estudio", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Estado_Estudio").Value = Val(Me.objCataEstadoEstudioSeguridad.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numeracion", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Numeracion").Value = Me.intNumeracion.ToString
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIDO_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TIDO_Codigo").Value = Me.objDocumento.Codigo

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_USUA_Crea", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_USUA_Crea").Value = Me.objUsuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_OFIC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_OFIC_Codigo").Value = Me.objOficina.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Observaciones_Estudio", SqlDbType.VarChar, 250) : Me.objGeneral.ComandoSQL.Parameters("@par_Observaciones_Estudio").Value = Me.strObservacionesEstudio
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Asecarga_Vehiculo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Asecarga_Vehiculo").Value = Me.intAutorizoAsecargaVehiculo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Asecarga_Tenedor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Asecarga_Tenedor").Value = Me.intAutorizoAsecargaTenedor

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Asecarga_Conductor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Asecarga_Conductor").Value = Me.intAutorizoAsecargaConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Simit", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Simit").Value = Me.intAutorizoSimit
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Licencia_Conduccion", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Licencia_Conduccion").Value = Me.intAutorizoLicenciaConduccion
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Tiene_GPS", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Tiene_GPS").Value = Me.intTieneGPS
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Empresa_GPS", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Empresa_GPS").Value = Me.strNombreEmpresaGPS

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Clave_GPS", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Clave_GPS").Value = Me.strClaveGPS
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_GPS", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_GPS").Value = Me.strTelefonoGPS
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Celular_Propietario", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Celular_Propietario").Value = Me.strCelularPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Celular_Tenedor", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Celular_Tenedor").Value = Me.strCelularTenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Celular_Conductor", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Celular_Conductor").Value = Me.strCelularConductor

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Confirmo_Propietario", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Confirmo_Propietario").Value = Me.strConfirmoPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Confirmo_Tenedor", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Confirmo_Tenedor").Value = Me.strConfirmoTenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Confirmo_Conductor", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Confirmo_Conductor").Value = Me.strConfirmoConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Referencias_Personales", SqlDbType.VarChar, 250) : Me.objGeneral.ComandoSQL.Parameters("@par_Referencias_Personales").Value = Me.strReferenciasPersonales
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Revision_TecnoMecanica", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Revision_TecnoMecanica").Value = Me.dteRevisionTecnoMecanica

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_SOAT", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_SOAT").Value = Me.strSOAT
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_SOAT", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_SOAT").Value = Me.dteFechaSOAT

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Autorizo_Otro", SqlDbType.VarChar, 150) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Autorizo_Otro").Value = Me.strNombreAutorizoOtro
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizacion_Otro", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizacion_Otro").Value = Me.intAutorizoOtro
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_DefenCarga", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_DefenCarga").Value = Me.intDefenCarga
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Procuraduria", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Procuraduria").Value = Me.intProcuraduria

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Destino_Seguro", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Destino_Seguro").Value = Me.intDestinoSeguro
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Usuario_GPS", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Usuario_GPS").Value = Me.strUsuarioGPS
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Carnet_Afiliadora", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Carnet_Afiliadora").Value = Me.strNumeroCarnetAfiliadora
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Vencimiento_Afiliadora", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Vencimiento_Afiliadora").Value = Me.dteFechaVencimientoAfiliadora
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Aseguradora", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Aseguradora").Value = Me.objAseguradora.Codigo

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIVE_Codigo", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_TIVE_Codigo").Value = Me.CataTipoVehiculo.Retorna_Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Revision_Tecnomecanica", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Revision_Tecnomecanica").Value = Me.strNumeroRevisionTecnomecanica
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Identificacion_Afiliadora", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Identificacion_Afiliadora").Value = Me.strIdentificacionAfiliadora
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Direccion_Propietario", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Direccion_Propietario").Value = Me.strDireccionPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Direccion_Tenedor", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Direccion_Tenedor").Value = Me.strDireccionTenedor

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Direccion_Conductor", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Direccion_Conductor").Value = Me.strDireccionConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Residencia_Propietario", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Residencia_Propietario").Value = Me.objCiudadResidenciaPropietario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Residencia_Tenedor", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Residencia_Tenedor").Value = Me.objCiudadResidenciaTenedor.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Residencia_Conductor", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Residencia_Conductor").Value = Me.objCiudadResidenciaConductor.Codigo

'                'If Me.bytHuellaDigital(0) = BYTES_IMAGEN_DEFECTO Then
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Huella_Digital", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Huella_Digital").Value = DBNull.Value
'                'Else
'                'Me.objGeneral.ComandoSQL.Parameters.Add("@par_Huella_Digital", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Huella_Digital").Value = Me.bytHuellaDigital
'                'End If
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TGPS_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_TGPS_Codigo").Value = Me.objCataTipoGps.Retorna_Codigo


'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Identificacion_Propietario_Remolque", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Identificacion_Propietario_Remolque").Value = Me.strIdentificacionPropietarioRemolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Propietario_Remolque", SqlDbType.VarChar, 35) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Propietario_Remolque").Value = Me.strNombrePropietarioRemolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Propietario_Remolque", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Propietario_Remolque").Value = Me.objCiudadPropietarioRemolque.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_Propietario_Remolque", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_Propietario_Remolque").Value = Me.strTelefonoPropietarioRemolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Confirmo_Propietario_Remolque", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Confirmo_Propietario_Remolque").Value = Me.strConfirmoPropietarioRemolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Residencia_Propietario_Remolque", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Residencia_Propietario_Remolque").Value = Me.objCiudadResidenciaPropietarioRemolque.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Celular_Propietario_Remolque", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Celular_Propietario_Remolque").Value = Me.strCelularPropietarioRemolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Direccion_Propietario_Remolque", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Direccion_Propietario_Remolque").Value = Me.strDireccionPropietarioRemolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Propietario_Remolque", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Propietario_Remolque").Value = Me.intPropietarioRemolque

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TINA_Propietario", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_TINA_Propietario").Value = Me.IntTipoNaturalezaPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TINA_Conductor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_TINA_Conductor").Value = Me.IntTipoNaturalezaConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TINA_Tenedor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_TINA_Tenedor").Value = Me.IntTipoNaturalezaTenedor

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIID_Propietario", SqlDbType.Char, (3)) : Me.objGeneral.ComandoSQL.Parameters("@par_TIID_Propietario").Value = Me.StrTipoIdentificacionPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIID_Conductor", SqlDbType.Char, (3)) : Me.objGeneral.ComandoSQL.Parameters("@par_TIID_Conductor").Value = Me.StrTipoIdentificacionConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIID_Tenedor", SqlDbType.Char, (3)) : Me.objGeneral.ComandoSQL.Parameters("@par_TIID_Tenedor").Value = Me.StrTipoIdentificacionTenedor

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Digito_Chequeo_Propietario", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Digito_Chequeo_Propietario").Value = Me.intCodigoChequeoPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Digito_Chequeo_Conductor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Digito_Chequeo_Conductor").Value = Me.intCodigoChequeoConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Digito_Chequeo_Tenedor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Digito_Chequeo_Tenedor").Value = Me.intCodigoChequeoTenedor


'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_SQL = True
'                    lonNumero = Me.objGeneral.ComandoSQL.Parameters("@par_Numero").Value
'                Else
'                    Insertar_SQL = False
'                End If

'            Catch ex As Exception
'                Insertar_SQL = False
'                Me.lonNumero = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Insertar_SQL = False
'                End Try
'            Finally

'            End Try
'        End Function

'        Private Function Insertar_T_Documentos_SQL(ByRef Mensaje As String) As Boolean
'            Dim lonNumeRegi As Long
'            Try
'                Me.objGeneral.ComandoSQL.CommandText = "sp_insertar_t_estudio_seguridad_vehiculo_documentos"
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                Me.objGeneral.ComandoSQL.Parameters.Clear()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.objUsuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_ESVE_Numero", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_ESVE_Numero").Value = Me.lonNumero
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EOES_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_EOES_Codigo").Value = Me.intEntidadOrigen
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIDD_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_TIDD_Codigo").Value = Me.intTipoDocumentoDigitalizacion

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Referencia", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Referencia").Value = Me.strReferencia
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Emisor", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Emisor").Value = Me.strEmisor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Emision", SqlDbType.Date) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Emision").Value = Me.dteFechaEmision
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Vence", SqlDbType.Date) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Vence").Value = Me.dteFechaVence
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Documento", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Documento").Value = Me.bytDocumentoAdjunto
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Documento", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Documento").Value = Me.strNombreDocumento
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Extension_Documento", SqlDbType.Char, 5) : Me.objGeneral.ComandoSQL.Parameters("@par_Extension_Documento").Value = Me.strExtensDocumento
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Mensaje_Error", SqlDbType.VarChar, 52) : Me.objGeneral.ComandoSQL.Parameters("@par_Mensaje_Error").Direction = ParameterDirection.Output

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_T_Documentos_SQL = True
'                Else
'                    Insertar_T_Documentos_SQL = False
'                    Mensaje = Me.objGeneral.ComandoSQL.Parameters("@par_Mensaje_Error").Value
'                End If

'            Catch ex As Exception
'                Insertar_T_Documentos_SQL = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_Documentos_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Insertar_T_Documentos_SQL = False
'                End Try
'            End Try
'        End Function

'        Public Function Eliminar_T_Detalle_Documentos() As Boolean
'            Dim lonNumeRegi As Long
'            Try
'                Me.objGeneral.ComandoSQL = New SqlCommand("sp_eliminar_t_estudio_seguridad_vehiculo_documentos", Me.objGeneral.ConexionDocumentalSQL)
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.objUsuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ConexionDocumentalSQL.Open()

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Eliminar_T_Detalle_Documentos = True
'                Else
'                    Eliminar_T_Detalle_Documentos = False
'                End If

'            Catch ex As Exception
'                Eliminar_T_Detalle_Documentos = False
'                Me.lonNumero = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_T_Detalle_Documentos: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Eliminar_T_Detalle_Documentos = False
'                End Try
'            Finally
'                If Me.objGeneral.ConexionDocumentalSQL.State = ConnectionState.Open Then
'                    Me.objGeneral.ConexionDocumentalSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Eliminar_Documento_T_Detalle_Documentos(ByVal Codigo As Integer) As Boolean
'            Dim lonNumeRegi As Long
'            Try
'                Me.objGeneral.ComandoSQL = New SqlCommand("sp_eliminar_documento_t_estudio_seguridad_vehiculo_documentos", Me.objGeneral.ConexionDocumentalSQL)
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.objUsuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo").Value = Codigo

'                Me.objGeneral.ConexionDocumentalSQL.Open()

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Eliminar_Documento_T_Detalle_Documentos = True
'                Else
'                    Eliminar_Documento_T_Detalle_Documentos = False
'                End If

'            Catch ex As Exception
'                Eliminar_Documento_T_Detalle_Documentos = False
'                Me.lonNumero = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_T_Detalle_Documentos: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Eliminar_Documento_T_Detalle_Documentos = False
'                End Try
'            Finally
'                If Me.objGeneral.ConexionDocumentalSQL.State = ConnectionState.Open Then
'                    Me.objGeneral.ConexionDocumentalSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Trasladar_T_Documentos(ByVal A_Temporal As Byte) As Boolean
'            Dim lonNumeRegi As Long
'            Try
'                Me.objGeneral.ComandoSQL = New SqlCommand("sp_trasladar_t_estudio_seguridad_vehiculo_documentos", Me.objGeneral.ConexionDocumentalSQL)
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                Me.objGeneral.ComandoSQL.Parameters.Clear()
'                Me.objGeneral.ConexionDocumentalSQL.Open()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_A_Temporal", SqlDbType.Bit) : Me.objGeneral.ComandoSQL.Parameters("@par_A_Temporal").Value = A_Temporal
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.objUsuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_ESVE_Numero", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_ESVE_Numero").Value = Me.lonNumero

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Trasladar_T_Documentos = True
'                Else
'                    Trasladar_T_Documentos = False
'                End If

'            Catch ex As Exception
'                Trasladar_T_Documentos = False
'                Me.lonNumero = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_Documentos_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Trasladar_T_Documentos = False
'                End Try
'            Finally
'                If Me.objGeneral.ConexionDocumentalSQL.State = ConnectionState.Open Then
'                    Me.objGeneral.ConexionDocumentalSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Modificar_SQL() As Boolean
'            Dim lonNumeRegi As Long

'            Try

'                Me.objGeneral.ComandoSQL.CommandText = "sp_modificar_estudio_seguridad_vehiculos"
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                Me.objGeneral.ComandoSQL.Parameters.Clear()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero").Value = Me.lonNumero
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha").Value = Me.dteFecha
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Placa", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Placa").Value = Me.strPlaca
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_MARC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_MARC_Codigo").Value = Me.lonMarca

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Modelo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Modelo").Value = Me.intModelo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_COLO_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_COLO_Codigo").Value = Me.lonColor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Clase", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Clase").Value = Me.strClase
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Placa_Semiremolque", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Placa_Semiremolque").Value = Me.strSemirremolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Modelo_Repotenciado", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Modelo_Repotenciado").Value = Me.intModeloRepotenciado

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Empresa_Afiliadora", SqlDbType.VarChar, 40) : Me.objGeneral.ComandoSQL.Parameters("@par_Empresa_Afiliadora").Value = Me.strEmpresaAfiliadora
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_Afiliadora", SqlDbType.VarChar, 40) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_Afiliadora").Value = Me.strTelefonoEmpresaAfiliadora
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Identificacion_Propietario", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Identificacion_Propietario").Value = Me.strIdentificacionPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Propietario", SqlDbType.VarChar, 35) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Propietario").Value = Me.strNombrePropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Propietario", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Propietario").Value = Me.objCiudadPropietario.Codigo

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_Propietario", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_Propietario").Value = Me.strTelefonoPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Identificacion_Tenedor", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Identificacion_Tenedor").Value = Me.strIdentificacionTenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Tenedor", SqlDbType.VarChar, 35) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Tenedor").Value = Me.strNombreTenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Tenedor", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Tenedor").Value = Me.objCiudadTenedor.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_Tenedor", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_Tenedor").Value = Me.strTelefonoTenedor

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Identificacion_Conductor", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Identificacion_Conductor").Value = Me.strIdentificacionConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Conductor", SqlDbType.VarChar, 35) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Conductor").Value = Me.strNombreConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Conductor", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Conductor").Value = Me.objCiudadConductor.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_Conductor", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_Conductor").Value = Me.strTelefonoConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Cliente", SqlDbType.VarChar, 35) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Cliente").Value = Me.strNombreCliente

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Mercancia", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Mercancia").Value = Me.strMercancia
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_RUTA_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_RUTA_Codigo").Value = Me.objRuta.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Valor", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Valor").Value = Me.dblValor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Referencias_Empresas", SqlDbType.VarChar, 250) : Me.objGeneral.ComandoSQL.Parameters("@par_Referencias_Empresas").Value = Me.strReferenciasEmpresas
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Observaciones", SqlDbType.VarChar, 250) : Me.objGeneral.ComandoSQL.Parameters("@par_Observaciones").Value = Me.strObservaciones

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Dijin_Conductor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Dijin_Conductor").Value = Me.intAutorizoDijinConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Dijin_Vehiculo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Dijin_Vehiculo").Value = Me.intAutorizoDijinVehiculo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Dijin_Tenedor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Dijin_Tenedor").Value = Me.intAutorizoDijinTenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Colfecar_Conductor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Colfecar_Conductor").Value = Me.intAutorizoColfecarConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Colfecar_Vehiculo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Colfecar_Vehiculo").Value = Me.intAutorizoColfecarVehiculo

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Colfecar_Tenedor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Colfecar_Tenedor").Value = Me.intAutorizoColfecarTenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Clase_Riesgo", SqlDbType.Char, 1) : Me.objGeneral.ComandoSQL.Parameters("@par_Clase_Riesgo").Value = Me.objCataRiesgoEstudio.Retorna_Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizado_Seguridad", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizado_Seguridad").Value = Me.intAutorizadoSeguridad
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizado_Direccion", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizado_Direccion").Value = Me.intAutorizadoDireccion
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Autorizacion", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Autorizacion").Direction = ParameterDirection.Output

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Anulado", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Anulado").Value = Me.intAnulado
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Estado").Value = Val(Me.objCataEstado.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Estado_Estudio", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Estado_Estudio").Value = Val(Me.objCataEstadoEstudioSeguridad.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numeracion", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Numeracion").Value = Me.intNumeracion
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIDO_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TIDO_Codigo").Value = Me.objDocumento.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIDO_Autorizacion_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TIDO_Autorizacion_Codigo").Value = Me.objDocumentoAutorizacion.Codigo

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_USUA_Modifica", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_USUA_Modifica").Value = Me.objUsuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_OFIC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_OFIC_Codigo").Value = Me.objOficina.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Aprobar", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Aprobar").Value = Me.intAprobar
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Observaciones_Estudio", SqlDbType.VarChar, 250) : Me.objGeneral.ComandoSQL.Parameters("@par_Observaciones_Estudio").Value = Me.strObservacionesEstudio
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Asecarga_Vehiculo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Asecarga_Vehiculo").Value = Me.intAutorizoAsecargaVehiculo

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Asecarga_Tenedor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Asecarga_Tenedor").Value = Me.intAutorizoAsecargaTenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Asecarga_Conductor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Asecarga_Conductor").Value = Me.intAutorizoAsecargaConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Simit", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Simit").Value = Me.intAutorizoSimit
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizo_Licencia_Conduccion", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizo_Licencia_Conduccion").Value = Me.intAutorizoLicenciaConduccion
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Tiene_GPS", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Tiene_GPS").Value = Me.intTieneGPS

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Empresa_GPS", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Empresa_GPS").Value = Me.strNombreEmpresaGPS
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Clave_GPS", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Clave_GPS").Value = Me.strClaveGPS
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_GPS", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_GPS").Value = Me.strTelefonoGPS
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Celular_Propietario", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Celular_Propietario").Value = Me.strCelularPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Celular_Tenedor", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Celular_Tenedor").Value = Me.strCelularTenedor

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Celular_Conductor", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Celular_Conductor").Value = Me.strCelularConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Confirmo_Propietario", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Confirmo_Propietario").Value = Me.strConfirmoPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Confirmo_Tenedor", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Confirmo_Tenedor").Value = Me.strConfirmoTenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Confirmo_Conductor", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Confirmo_Conductor").Value = Me.strConfirmoConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Referencias_Personales", SqlDbType.VarChar, 250) : Me.objGeneral.ComandoSQL.Parameters("@par_Referencias_Personales").Value = Me.strReferenciasPersonales

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Revision_TecnoMecanica", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Revision_TecnoMecanica").Value = Me.dteRevisionTecnoMecanica
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_SOAT", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_SOAT").Value = Me.strSOAT
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_SOAT", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_SOAT").Value = Me.dteFechaSOAT

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Autorizo_Otro", SqlDbType.VarChar, 150) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Autorizo_Otro").Value = Me.strNombreAutorizoOtro
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autorizacion_Otro", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Autorizacion_Otro").Value = Me.intAutorizoOtro
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_DefenCarga", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_DefenCarga").Value = Me.intDefenCarga
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Procuraduria", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Procuraduria").Value = Me.intProcuraduria

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Destino_Seguro", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Destino_Seguro").Value = Me.intDestinoSeguro
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Usuario_GPS", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Usuario_GPS").Value = Me.strUsuarioGPS
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Carnet_Afiliadora", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Carnet_Afiliadora").Value = Me.strNumeroCarnetAfiliadora
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Vencimiento_Afiliadora", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Vencimiento_Afiliadora").Value = Me.dteFechaVencimientoAfiliadora
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Aseguradora", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Aseguradora").Value = Me.objAseguradora.Codigo

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIVE_Codigo", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_TIVE_Codigo").Value = Me.CataTipoVehiculo.Retorna_Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Revision_Tecnomecanica", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Revision_Tecnomecanica").Value = Me.strNumeroRevisionTecnomecanica
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Identificacion_Afiliadora", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Identificacion_Afiliadora").Value = Me.strIdentificacionAfiliadora
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Direccion_Propietario", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Direccion_Propietario").Value = Me.strDireccionPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Direccion_Tenedor", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Direccion_Tenedor").Value = Me.strDireccionTenedor

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Direccion_Conductor", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Direccion_Conductor").Value = Me.strDireccionConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Residencia_Propietario", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Residencia_Propietario").Value = Me.objCiudadResidenciaPropietario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Residencia_Tenedor", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Residencia_Tenedor").Value = Me.objCiudadResidenciaTenedor.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Residencia_Conductor", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Residencia_Conductor").Value = Me.objCiudadResidenciaConductor.Codigo

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Huella_Digital", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Huella_Digital").Value = DBNull.Value

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TGPS_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_TGPS_Codigo").Value = Me.objCataTipoGps.Retorna_Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Identificacion_Propietario_Remolque", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Identificacion_Propietario_Remolque").Value = Me.strIdentificacionPropietarioRemolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Propietario_Remolque", SqlDbType.VarChar, 35) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Propietario_Remolque").Value = Me.strNombrePropietarioRemolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Propietario_Remolque", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Propietario_Remolque").Value = Me.objCiudadPropietarioRemolque.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_Propietario_Remolque", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_Propietario_Remolque").Value = Me.strTelefonoPropietarioRemolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Confirmo_Propietario_Remolque", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Confirmo_Propietario_Remolque").Value = Me.strConfirmoPropietarioRemolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Residencia_Propietario_Remolque", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Residencia_Propietario_Remolque").Value = Me.objCiudadResidenciaPropietarioRemolque.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Celular_Propietario_Remolque", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Celular_Propietario_Remolque").Value = Me.strCelularPropietarioRemolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Direccion_Propietario_Remolque", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Direccion_Propietario_Remolque").Value = Me.strDireccionPropietarioRemolque
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Propietario_Remolque", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Propietario_Remolque").Value = Me.intPropietarioRemolque







'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TINA_Propietario", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_TINA_Propietario").Value = Me.IntTipoNaturalezaPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TINA_Conductor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_TINA_Conductor").Value = Me.IntTipoNaturalezaConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TINA_Tenedor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_TINA_Tenedor").Value = Me.IntTipoNaturalezaTenedor

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIID_Propietario", SqlDbType.Char, (3)) : Me.objGeneral.ComandoSQL.Parameters("@par_TIID_Propietario").Value = Me.StrTipoIdentificacionPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIID_Conductor", SqlDbType.Char, (3)) : Me.objGeneral.ComandoSQL.Parameters("@par_TIID_Conductor").Value = Me.StrTipoIdentificacionConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIID_Tenedor", SqlDbType.Char, (3)) : Me.objGeneral.ComandoSQL.Parameters("@par_TIID_Tenedor").Value = Me.StrTipoIdentificacionTenedor

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Digito_Chequeo_Propietario", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Digito_Chequeo_Propietario").Value = Me.intCodigoChequeoPropietario
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Digito_Chequeo_Conductor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Digito_Chequeo_Conductor").Value = Me.intCodigoChequeoConductor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Digito_Chequeo_Tenedor", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Digito_Chequeo_Tenedor").Value = Me.intCodigoChequeoTenedor




'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If

'            Catch ex As Exception
'                Modificar_SQL = False
'                Me.lonNumero = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Modificar_SQL = False
'                End Try
'            Finally

'            End Try
'        End Function

'        Private Function Anular_SQL() As Boolean
'            Dim lonNumeRegi As Long

'            Me.objGeneral.ConexionSQL.Open()
'            Try
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_anular_estudio_seguridad_vehiculos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_TIDO_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TIDO_Codigo").Value = Me.objDocumento.Codigo
'                ComandoSQL.Parameters.Add("@par_Numero", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Numero").Value = Me.lonNumero
'                ComandoSQL.Parameters.Add("@par_USUA_Anula", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Anula").Value = Me.Usuario.Codigo
'                ComandoSQL.Parameters.Add("@par_Causa_Anula", SqlDbType.VarChar, 150) : ComandoSQL.Parameters("@par_Causa_Anula").Value = Me.strCausaAnula

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Anular_SQL = True
'                Else
'                    Anular_SQL = False
'                    Me.strError = "El estudio de Seguridad esta ya aprobado y por lo tanto no se puede anular"
'                End If

'            Catch ex As Exception
'                Anular_SQL = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Anular_SQL: " & Me.objGeneral.Traducir_Error(ex.Message)
'                    Anular_SQL = False
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function



'        Private Function Datos_Requeridos(ByRef Mensaje As String) As Boolean

'            Try
'                Me.strError = ""

'                Dim intCont As Short
'                intCont = 0
'                Datos_Requeridos = True
'                Dim bolValidaIngresoDireccionCiudadResidenciaTerceros As Boolean = False

'                If Not Me.dteFecha = Date.MinValue Then
'                    If Not Me.objGeneral.Fecha_Valida(Me.dteFecha) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Fecha del Estudio de Seguridad invalida, digite dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                Else
'                    Me.dteFecha = General.FECHA_NULA
'                End If

'                If Len(Trim(Me.strPlaca)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar la placa del vehículo."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                Else
'                    If Vehiculo_Lista_Negra() Then
'                        intCont += 1
'                        Me.strError = intCont & ". La placa del vehículo se encuentra en la lista negra de vehículos, no es posible crear el mismo."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If

'                End If

'                If Me.intModelo = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el modelo del vehículo."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_MODELO_REPOTENCIADO) Then
'                    If intModeloRepotenciado = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, EXIGE_CLASE_VEHICULO) Then
'                    If Trim(Me.strClase) = "" Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la clase del vehículo."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_COLOR_VEHICULO) Then
'                    If lonColor = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_EMPRESA_AFILIADORA) Then
'                    If Trim(Me.strIdentificacionAfiliadora) = String.Empty Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_TELEFONO_EMPRESA_AFILIADORA) Then
'                    If Len(Trim(Me.strTelefonoEmpresaAfiliadora)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_NUMERO_CARNET_AFILIACION) Then
'                    If Trim(Me.NumeroCarnetAfiliadora) = String.Empty Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_FECHA_VENCIMIENTO_AFILACION) Then
'                    If Me.dteFechaVencimientoAfiliadora = Date.MinValue Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                ElseIf Me.dteFechaVencimientoAfiliadora = Date.MinValue Then
'                    Me.dteFechaVencimientoAfiliadora = General.FECHA_NULA
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_SOAT) Then
'                    If Me.objValidacion.Valida Then
'                        If Len(Trim(Me.strSOAT)) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If
'                End If

'                If Me.dteFechaSOAT = Date.MinValue Then
'                    Me.dteFechaSOAT = General.FECHA_NULA
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_FECHA_SOAT) Then
'                    If Not Me.dteFechaSOAT = Date.MinValue Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFechaSOAT) Then
'                            intCont += 1
'                            Me.strError += intCont & "Digite una fecha válida SOAT. "
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                        If Me.dteFechaSOAT = General.FECHA_NULA Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    Else
'                        Me.dteFechaSOAT = General.FECHA_NULA
'                    End If
'                End If

'                If Me.objAseguradora.Codigo = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe seleccionar una aseguradora."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.dteRevisionTecnoMecanica = Date.MinValue Then
'                    Me.dteRevisionTecnoMecanica = General.FECHA_NULA
'                End If
'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_SEMI_PLACA_POR_TIPO_VEHICULO) Then
'                    If Me.objCataTipoVehiculo.Retorna_Nombre <> "" And Me.strSemirremolque = String.Empty And (Val(Me.objCataTipoVehiculo.Campo5) = Vehiculos.TIPO_VEHICULO_CODIGO_MINISTERIO_53 Or
'                           Val(Me.objCataTipoVehiculo.Campo5) = Vehiculos.TIPO_VEHICULO_CODIGO_MINISTERIO_54) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la placa del semirremolque para este tipo de vehículo."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_REVISION_TECNOMECANICA) Then
'                    If Not Me.dteRevisionTecnoMecanica = Date.MinValue Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteRevisionTecnoMecanica) Then
'                            intCont += 1
'                            Me.strError += intCont & "Ingrese una fecha valida revisión tecnomecanica."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                        If Me.dteRevisionTecnoMecanica = General.FECHA_NULA Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    Else
'                        Me.dteRevisionTecnoMecanica = General.FECHA_NULA
'                    End If
'                End If

'                If Me.intPropietarioRemolque = True Then

'                    If Trim(Me.strIdentificacionPropietarioRemolque) = String.Empty Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la identificación del Propietario de Remolque."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If

'                    If Trim(Me.strNombrePropietarioRemolque) = String.Empty Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar el nombre del Propietario de Remolque."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If

'                    If Me.objCiudadPropietarioRemolque.Codigo = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar una ciudad válida para el Propietario de Remolque."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_INGRESO_DIRECCION_CIUDAD_RESIDENCIA_TERCEROS) Then

'                        bolValidaIngresoDireccionCiudadResidenciaTerceros = True
'                        If RTrim(Me.strDireccionPropietarioRemolque) = "" Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar la dirección del Propietario de Remolque. "
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                        If Me.objCiudadResidenciaPropietarioRemolque.Codigo = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar la ciudad de residencia del Propietario de Remolque. "
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If

'                    End If

'                Else
'                    If Trim(Me.strIdentificacionPropietario) = String.Empty Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la identificación del Propietario."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Not IsNumeric(Me.strIdentificacionPropietario) Then
'                        intCont += 1
'                        Me.strError += intCont & ".La identificacion del propietario solo debe contener caracteres numericos"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Trim(Me.strNombrePropietario) = String.Empty Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar el nombre del Propietario."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If

'                    If Me.objCiudadPropietario.Codigo = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & " Debe ingresar una ciudad válida para el Propietario"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If

'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_INGRESO_DIRECCION_CIUDAD_RESIDENCIA_TERCEROS) Then

'                        bolValidaIngresoDireccionCiudadResidenciaTerceros = True
'                        If RTrim(Me.strDireccionPropietario) = "" Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar la dirección del Propietario. "
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                        If Me.objCiudadResidenciaPropietario.Codigo = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar la ciudad de residencia del Propietario. "
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If

'                    End If



'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_TELEFONO_PROPIETARIO) Then
'                    If Len(Trim(Me.strTelefonoPropietario)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CELULAR_PROPIETARIO) Then
'                    If Len(Trim(Me.strCelularPropietario)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If



'                If Trim(Me.strIdentificacionTenedor) = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar la identificación del Tenedor."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If
'                If Not IsNumeric(Me.strIdentificacionTenedor) Then
'                    intCont += 1
'                    Me.strError += intCont & ".La identificacion del tenedor solo debe contener caracteres numericos"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If
'                If Trim(Me.strNombreTenedor) = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el nombre del Tenedor."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objCiudadTenedor.Codigo = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar una ciudad válida para el Tenedor."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_TELEFONO_TENEDOR) Then
'                    If Len(Trim(Me.strTelefonoTenedor)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CELULAR_TENEDOR) Then
'                    If Len(Trim(Me.strCelularTenedor)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If



'                If bolValidaIngresoDireccionCiudadResidenciaTerceros Then
'                    If RTrim(Me.strDireccionTenedor) = "" Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la dirección del Tenedor. "
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Me.objCiudadResidenciaTenedor.Codigo = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la ciudad de residencia del Tenedor. "
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If


'                If Trim(Me.strIdentificacionConductor) = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar la identificación del Conductor."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If
'                If Not IsNumeric(Me.strIdentificacionConductor) Then
'                    intCont += 1
'                    Me.strError += intCont & ".La identificacion del conductor solo debe contener caracteres numericos"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If
'                If Trim(Me.strNombreConductor) = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el nombre del Conductor."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objCiudadConductor.Codigo = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar una ciudad válida para el Conductor."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_TELEFONO_CONDUCTOR) Then
'                    If Len(Trim(Me.strTelefonoConductor)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CELULAR_CONDUCTOR) Then
'                    If Len(Trim(Me.strCelularConductor)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If bolValidaIngresoDireccionCiudadResidenciaTerceros Then
'                    If RTrim(Me.strDireccionConductor) = "" Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la dirección del Conductor. "
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                    If Me.objCiudadResidenciaConductor.Codigo = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la ciudad de residencia del Conductor. "
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                'Dato requerido quitado por solicitud de Suramericana de Tranportes

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_NOMBRE_CLIENTE) Then
'                    If Trim(Me.strNombreCliente) = String.Empty Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_MERCANCIA) Then
'                    If Trim(Me.strMercancia) = String.Empty Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_RUTA) Then
'                    If Me.objRuta.Codigo = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If



'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_VALOR_CLIENTE) Then
'                    If dblValor = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_REFERENCIA_EMPRESA) Then
'                    If Me.strReferenciasEmpresas = "" Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_REFERENCIA_PERSONAL) Then
'                    If Me.strReferenciasPersonales = "" Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_OBSERVACIONES) Then
'                    If Me.strObservaciones = "" Then
'                        intCont += 1
'                        Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.objCataEstado.Retorna_Codigo = Val(General.ESTADO_DEFINITIVO) And (Me.objCataEstadoEstudioSeguridad.Retorna_Codigo = ESTADO_RECHAZADO Or
'            Me.objCataEstadoEstudioSeguridad.Retorna_Codigo = ESTADO_APROBADO) Then
'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CONFIRMADO_TENEDOR) Then
'                        If strConfirmoTenedor = String.Empty Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If

'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CONFIRMADO_PROPIETARIO) Then
'                        If Len(Trim(Me.strConfirmoPropietario)) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If

'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CONFIRMADO_CONDUCTOR) Then
'                        If Len(Trim(Me.strConfirmoConductor)) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If
'                End If

'                If Me.dteFechaSOAT = Date.MinValue Then
'                    Me.dteFechaSOAT = General.FECHA_NULA
'                End If

'                If Me.dteRevisionTecnoMecanica = Date.MinValue Then
'                    Me.dteRevisionTecnoMecanica = General.FECHA_NULA
'                End If

'                'validacion digito chequeo propietario'

'                If Me.StrTipoIdentificacionPropietario = General.TIID_NIT Then
'                    If Not Me.objGeneral.Valida_Digito_Chequeo(Me.strIdentificacionPropietario, Me.intCodigoChequeoPropietario) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Digito de chequeo de Propietario inválido."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.StrTipoIdentificacionConductor = General.TIID_NIT Then
'                    If Not Me.objGeneral.Valida_Digito_Chequeo(Me.strIdentificacionConductor, Me.intCodigoChequeoConductor) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Digito de chequeo de Conductor inválido."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Me.StrTipoIdentificacionTenedor = General.TIID_NIT Then
'                    If Not Me.objGeneral.Valida_Digito_Chequeo(Me.strIdentificacionTenedor, Me.intCodigoChequeoTenedor) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Digito de chequeo de Tenedor inválido."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If










'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_INGRESO_IMAGEN_HUELLA) Then
'                    If Me.CataEstado.Campo1 = General.ESTADO_DEFINITIVO Then
'                        If Me.bolHuellaDigital = False Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar la foto de la huella digital del Conductor. "
'                            Me.strError += " <Br />"
'                            Datos_Requeridos = False
'                        End If
'                    End If
'                End If


'            Catch ex As Exception
'                Datos_Requeridos = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Function

'        Private Function Datos_Requeridos_Documentos(ByRef Mensaje As String) As Boolean
'            Try
'                Me.strError = ""
'                Dim intCont As Short
'                intCont = 0
'                Datos_Requeridos_Documentos = True

'                Dim arrCampos(3) As String
'                Dim arrValoresCampos(3) As String

'                arrCampos(0) = "Aplica_Referencia"
'                arrCampos(1) = "Aplica_Emisor"
'                arrCampos(2) = "Aplica_Fecha_Emision"
'                arrCampos(3) = "Aplica_Fecha_Vence"
'                Me.objGeneral.Retorna_Campos_Documental(Me.objEmpresa.Codigo, "Tipo_Documento_Digitalizacion", arrCampos, arrValoresCampos, 4, General.CAMPO_NUMERICO, "Codigo", TipoDocumentoDigitalizacion, "", "")

'                If TipoDocumentoDigitalizacion = General.CERO Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe seleccionar el tipo de documento"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Documentos = False
'                End If

'                If Val(arrValoresCampos(0)) = General.UNO Then
'                    If ReferenciaDocumental = String.Empty Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la referencia"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Documentos = False
'                    End If
'                End If

'                If Val(arrValoresCampos(1)) = General.UNO Then
'                    If EmisorDocumental = String.Empty Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar el emisor"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Documentos = False
'                    End If
'                End If

'                If Val(arrValoresCampos(2)) = General.UNO Then
'                    If FechaEmisionDocumental = Date.MinValue Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la fecha de emisión"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Documentos = False
'                    End If
'                Else
'                    If FechaEmisionDocumental = Date.MinValue Then
'                        FechaEmisionDocumental = General.FECHA_NULA
'                    End If
'                End If

'                If Val(arrValoresCampos(3)) = General.UNO Then
'                    If FechaVenceDocumental = Date.MinValue Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la fecha de vencimiento"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Documentos = False
'                    End If
'                Else
'                    If FechaVenceDocumental = Date.MinValue Then
'                        FechaVenceDocumental = General.FECHA_NULA
'                    End If
'                End If

'                If DocumentoAdjunto(0) = BYTE_VACIO Or DocumentoAdjunto Is Nothing Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe seleccionar el documento a digitalizar"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Documentos = False
'                End If

'                Mensaje = Me.strError

'            Catch ex As Exception
'                Datos_Requeridos_Documentos = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos_Documentos: " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Function

'        Private Function Actualizar_Datos_Terceros() As Boolean

'            Dim bolContinuar As Boolean = True
'            Dim strNombreAuxiliar As String = ""
'            Dim strPrimerApellidoAuxiliar As String = ""
'            Dim strSegundoApellidoAuxiliar As String = ""

'            Try

'                Obtener_Nombres_Y_Apellidos(Me.NombrePropietario, strNombreAuxiliar, strPrimerApellidoAuxiliar, strSegundoApellidoAuxiliar)

'                Me.strSQL = "UPDATE Terceros SET Nombre = '" & strNombreAuxiliar & "', Apellido1='" & strPrimerApellidoAuxiliar & "', Apellido2='" & strSegundoApellidoAuxiliar & "',"
'                Me.strSQL += " Celular= '" & Me.CelularPropietario & "', Telefono1='" & Me.strTelefonoPropietario & "', CIUD_Codigo = " & Me.objCiudadResidenciaPropietario.Codigo
'                Me.strSQL += ", Direccion = '" & Me.strDireccionPropietario & "' "
'                Me.strSQL += " WHERE EMPR_Codigo  = " & Me.objEmpresa.Codigo
'                Me.strSQL += " AND Numero_Identificacion  = '" & Me.strIdentificacionPropietario & "' "

'                Me.objGeneral.Ejecutar_SQL_Misma_Transaccion(Me.strSQL, Me.strError)

'                strNombreAuxiliar = ""
'                strPrimerApellidoAuxiliar = ""
'                strSegundoApellidoAuxiliar = ""
'                Obtener_Nombres_Y_Apellidos(Me.NombreTenedor, strNombreAuxiliar, strPrimerApellidoAuxiliar, strSegundoApellidoAuxiliar)

'                Me.strSQL = "UPDATE Terceros SET Nombre = '" & strNombreAuxiliar & "', Apellido1='" & strPrimerApellidoAuxiliar & "', Apellido2='" & strSegundoApellidoAuxiliar & "',"
'                Me.strSQL += " Celular= '" & Me.CelularTenedor & "', Telefono1='" & Me.strTelefonoTenedor & "', CIUD_Codigo = " & Me.objCiudadResidenciaTenedor.Codigo
'                Me.strSQL += ", Direccion = '" & Me.strDireccionTenedor & "' "
'                Me.strSQL += " WHERE EMPR_Codigo  = " & Me.objEmpresa.Codigo
'                Me.strSQL += " AND Numero_Identificacion  = '" & Me.strIdentificacionTenedor & "' "

'                Me.objGeneral.Ejecutar_SQL_Misma_Transaccion(Me.strSQL, Me.strError)

'                strNombreAuxiliar = ""
'                strPrimerApellidoAuxiliar = ""
'                strSegundoApellidoAuxiliar = ""
'                Obtener_Nombres_Y_Apellidos(Me.NombreConductor, strNombreAuxiliar, strPrimerApellidoAuxiliar, strSegundoApellidoAuxiliar)

'                Me.strSQL = "UPDATE Terceros SET Nombre = '" & strNombreAuxiliar & "', Apellido1='" & strPrimerApellidoAuxiliar & "', Apellido2='" & strSegundoApellidoAuxiliar & "',"
'                Me.strSQL += " Celular= '" & Me.CelularConductor & "', Telefono1='" & Me.strTelefonoConductor & "', CIUD_Codigo = " & Me.objCiudadResidenciaConductor.Codigo
'                Me.strSQL += ", Direccion = '" & Me.strDireccionConductor & "' "
'                Me.strSQL += " WHERE EMPR_Codigo  = " & Me.objEmpresa.Codigo
'                Me.strSQL += " AND Numero_Identificacion  = '" & Me.strIdentificacionConductor & "' "

'                Me.objGeneral.Ejecutar_SQL_Misma_Transaccion(Me.strSQL, Me.strError)
'                Actualizar_Datos_Terceros = True
'            Catch ex As Exception
'                Actualizar_Datos_Terceros = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Actualizar_Datos_Terceros : " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try

'        End Function

'        Private Function Obtener_Nombres_Y_Apellidos(ByVal NombreCompleto As String, ByRef Nombre As String, ByRef PrimerApellido As String, ByRef SegundoApellido As String) As Boolean

'            Try
'                Dim arrNombreCompletoTercero() As String
'                Dim intContador As Integer

'                arrNombreCompletoTercero = Split(Replace(NombreCompleto, "  ", " "), " ")

'                If arrNombreCompletoTercero.Length > 2 Then
'                    For intContador = 0 To arrNombreCompletoTercero.Length - 3
'                        Nombre = Nombre & " " & arrNombreCompletoTercero(intContador)
'                    Next
'                    PrimerApellido = arrNombreCompletoTercero(arrNombreCompletoTercero.Length - 2)
'                    SegundoApellido = arrNombreCompletoTercero(arrNombreCompletoTercero.Length - 1)
'                ElseIf arrNombreCompletoTercero.Length > 1 Then
'                    Nombre = arrNombreCompletoTercero(0)
'                    PrimerApellido = arrNombreCompletoTercero(1)
'                Else
'                    Nombre = arrNombreCompletoTercero(0)
'                End If
'                Obtener_Nombres_Y_Apellidos = True
'            Catch ex As Exception
'                Obtener_Nombres_Y_Apellidos = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Obtener_Nombres_Y_Apellidos: " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try
'        End Function

'        Public Function Vehiculo_Lista_Negra() As Boolean
'            Try
'                Dim dtsConsulta As New DataSet

'                Vehiculo_Lista_Negra = True

'                strSQL = "SELECT Codigo"
'                strSQL += " FROM Lista_Negra_Vehiculos "
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += "   AND VEHI_Placa = '" & Me.strPlaca & "'"
'                strSQL += "   AND Estado = " & General.ESTADO_ACTIVO

'                dtsConsulta = Me.objGeneral.Retorna_Dataset(strSQL, strError)

'                If Not IsNothing(dtsConsulta) Then
'                    If dtsConsulta.Tables(0).Rows.Count > General.CERO Then
'                        For i = 0 To (dtsConsulta.Tables(0).Rows.Count - 1)
'                            If Val(dtsConsulta.Tables(0).Rows(i).Item("Codigo").ToString) > General.CERO Then
'                                Vehiculo_Lista_Negra = True
'                            Else
'                                Vehiculo_Lista_Negra = False
'                            End If
'                        Next
'                    Else
'                        Vehiculo_Lista_Negra = False
'                    End If

'                End If

'            Catch ex As Exception
'                Vehiculo_Lista_Negra = False
'                Me.strError = "En la clase Vehiculos se presentó un error en la función Validar_Lista_Negra: " & Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'#End Region
'    End Class

'End Namespace