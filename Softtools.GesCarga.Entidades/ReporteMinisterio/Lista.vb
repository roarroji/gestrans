﻿'Imports Microsoft.VisualBasic
'Imports System.Data
'Imports System.Data.SqlClient
'Imports System.Configuration
'Imports System
'Namespace Entidades.ReporteMinisterio
'    Public Class Lista


'        Public Const ITEM_TODAS As String = "(TODAS)"
'        Public Const ITEM_TODOS As String = "(TODOS)"
'        Public Const ITEM_VACIO As String = "       "
'        Public Const SELECCIONO_NO As Integer = 0
'        Public Const SELECCIONO_SI As Integer = 1
'        Public Const NO_APLICA As String = "(NO APLICA)"
'        Public Const SIN_ITEM As String = " "

'        Public Const PRIMERA_TABLA As Integer = 0
'        Public Const PRIMERA_COLUMNA As Integer = 0
'        Public Const PRIMER_REGISTRO As Integer = 0
'        Public Const PRIMERA_POSICION_CADENA As Integer = 1
'        Public Const COLUMNA_CODIGO As Byte = 0

'        Dim strSQL As String, strMensajeError As String
'        Private objGeneral As New General

'        Public Sub Retorna_Llaves(ByVal strLlaveCompuesta As String, ByVal intComponentes As Short, ByRef Llave1 As String, Optional ByRef Llave2 As String = "", Optional ByRef Llave3 As String = "")
'            Dim intCont As Integer, intLlave As Integer
'            Dim intPosiInic As Integer, intLongitud As Integer
'            Dim strAuxiliar As String
'            Dim chrCaracter As Char

'            intLlave = 0
'            intPosiInic = 1
'            intLongitud = 0
'            strAuxiliar = strLlaveCompuesta
'            For intCont = 1 To strLlaveCompuesta.Length
'                chrCaracter = Mid(strLlaveCompuesta, intCont, 1)
'                If chrCaracter = "¬" Then
'                    intLlave += 1

'                    strAuxiliar = Mid(strLlaveCompuesta, intPosiInic, intLongitud)
'                    Select Case intLlave
'                        Case 1
'                            Llave1 = strAuxiliar
'                        Case 2
'                            Llave2 = strAuxiliar
'                        Case 3
'                            Llave3 = strAuxiliar
'                    End Select
'                    intLongitud = -1
'                    intPosiInic = intCont + 1

'                End If
'                intLongitud += 1
'            Next
'        End Sub

'        ''' <summary>
'        ''' Retorna la lista de elementos que conforman un catalogo dado su código
'        ''' </summary>
'        ''' <param name="CodigoEmpresa">codigo identificador de la empresa</param>
'        ''' <param name="Codigo_Catalogo">codigo del catalago</param>
'        ''' <param name="ResultadoCatalogo"> arreglo de objetos de la clase catalogo</param>
'        ''' <param name="MensajeError"></param>
'        ''' <returns></returns>
'        ''' <remarks></remarks>
'        Public Function Retornar_Listado_Catalogo(ByVal CodigoEmpresa As Long, ByVal Codigo_Catalogo As String, ByRef ResultadoCatalogo As Catalogo(), ByRef MensajeError As String) As Boolean

'            Dim ComandoSQL As SqlCommand
'            Dim strSQL As String = ""
'            Dim dsDataSet As New DataSet
'            Dim resultado As Boolean



'            strSQL = "SELECT VACA.EMPR_Codigo, VACA.Campo1 AS Codigo, VACA.Campo2 AS Nombre, " &
'                 "ISNULL(VACA.Campo3,'') AS NombreSecundario, ISNULL(VACA.Campo4,'') AS Campo4, ISNULL(VACA.Campo5,'') AS Campo5, " &
'                 "ISNULL(VACA.Campo6,'') AS Campo6, ISNULL(VACA.Campo7,'') AS Campo7, ISNULL(VACA.Campo8,'') AS Campo8 " &
'                 "FROM Valor_Catalogos VACA " &
'                 "WHERE VACA.EMPR_Codigo = " & CodigoEmpresa &
'                 " AND VACA.CATA_CODIGO = '" & Codigo_Catalogo & "'"


'            ComandoSQL = New SqlCommand(strSQL, objGeneral.ConexionSQL)

'            Try
'                objGeneral.ConexionSQL.Open()

'                dsDataSet = objGeneral.Retorna_Dataset(strSQL, MensajeError)

'                If dsDataSet.Tables(0).Rows.Count > 0 Then

'                    ReDim ResultadoCatalogo(dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows.Count - 1)
'                    Dim intContador As Integer = 0
'                    For intContador = 0 To dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows.Count - 1
'                        ResultadoCatalogo(intContador) = New Catalogo
'                        ResultadoCatalogo(intContador).Campo1 = dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows(intContador).Item("Codigo")
'                        ResultadoCatalogo(intContador).Campo2 = dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows(intContador).Item("Nombre")
'                        ResultadoCatalogo(intContador).Campo3 = dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows(intContador).Item("NombreSecundario")
'                        ResultadoCatalogo(intContador).Campo4 = dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows(intContador).Item("Campo4")
'                        ResultadoCatalogo(intContador).Campo5 = dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows(intContador).Item("Campo5")
'                        ResultadoCatalogo(intContador).Campo6 = dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows(intContador).Item("Campo6")
'                        ResultadoCatalogo(intContador).Campo7 = dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows(intContador).Item("Campo7")
'                        ResultadoCatalogo(intContador).Campo8 = dsDataSet.Tables(Lista.PRIMERA_TABLA).Rows(intContador).Item("Campo8")
'                    Next

'                Else
'                    MensajeError = "No existen elementos con el código de catálogo ingresado"
'                End If

'            Catch ex As Exception
'            Catch ex As Exception

'                Try
'                    objGeneral.ExcepcionSQL = ex
'                    MensajeError = objGeneral.Traducir_Error(objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    MensajeError = objGeneral.Traducir_Error(ex.Message)
'                    resultado = False
'                End Try
'            Finally
'                If objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    objGeneral.ConexionSQL.Close()
'                Else
'                    resultado = True
'                End If
'                dsDataSet.Dispose()

'            End Try

'            Return resultado

'        End Function

'    End Class
'End Namespace