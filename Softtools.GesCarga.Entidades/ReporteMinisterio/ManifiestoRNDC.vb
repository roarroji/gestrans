﻿'Imports Microsoft.VisualBasic
'Imports System.Xml
'Imports System.Data.SqlClient
'Imports System.Data
'Imports Softtools.GesCarga.Entidades.ManifiestoElectronico
'Namespace Entidades.ReporteMinisterio
'    Public Class ManifiestoRNDC
'#Region "Variables"
'        Public NUMNITEMPRESATRANSPORTE As String
'        Public NUMMANIFIESTOCARGA As String
'        Public CONSECUTIVOINFORMACIONVIAJE As String
'        Public MANNROMANIFIESTOTRANSBORDO As String
'        Public NUMMANIFIESTOCARGATRANSBORDO As String
'        Public CODOPERACIONTRANSPORTE As String
'        Public FECHAEXPEDICIONMANIFIESTO As String
'        Public CODMUNICIPIOORIGENMANIFIESTO As String
'        Public CODMUNICIPIODESTINOMANIFIESTO As String
'        Public CODIDTITULARMANIFIESTO As String
'        Public NUMIDTITULARMANIFIESTO As String
'        Public NUMPLACA As String
'        Public NUMPLACAREMOLQUE As String
'        Public CODIDCONDUCTOR As String
'        Public NUMIDCONDUCTOR As String
'        Public VALORFLETEPACTADOVIAJE As String
'        Public RETENCIONFUENTEMANIFIESTO As String
'        Public RETENCIONICAMANIFIESTOCARGA As String
'        Public VALORANTICIPOMANIFIESTO As String
'        Public CODMUNICIPIOPAGOSALDO As String
'        Public FECHAPAGOSALDOMANIFIESTO As String
'        Public CODRESPONSABLEPAGOCARGUE As String
'        Public CODRESPONSABLEPAGODESCARGUE As String
'        Public OBSERVACIONES As String
'        Public CONSECUTIVOREMESA() As String

'        ''CAMPOS CUMPLIDO
'        Public TIPOCUMPLIDOMANIFIESTO As String
'        Public MOTIVOSUSPENSIONMANIFIESTO As String
'        Public CONSECUENCIASUSPENSIONMANIFIESTO As String
'        Public VALORADICIONALHORASCARGUE As String
'        Public VALORADICIONALHORASDESCARGUE As String
'        Public VALORADICIONALFLETE As String
'        Public MOTIVOVALORADICIONAL As String
'        Public VALORDESCUENTOFLETE As String
'        Public MOTIVOVALORDESCUENTOMANIFIESTO As String
'        Public VALORSOBREANTICIPO As String
'        Public FECHAENTREGADOCUMENTOS As String
'        Public CODMOTIVOANULACIONCUMPLIDO As String

'        ''campos anulación
'        Public MOTIVOANULACIONMANIFIESTO As String

'        Const NOMBRE_TAG As String = "variables"
'        Const REMESASMAN As String = "REMESASMAN"
'        Const NOMBRE_PROCESO_INGRESO_DETALLE_MANIFIESTO As String = "procesoid"
'        Const VALOR_PROCESO_INGRESO_DETALLE_MANIFIESTO As String = "43"

'        Public Const MOTIVO_ANULACION_MANIFIESTO_ERROR_DIGITACION As String = "D"
'        Public Const MOTIVO_ANULACION_MANIFIESTO_CANCELACION_SERVICIO As String = "S"

'        'Campos anulación cumplido

'        Public OBSERVACIONES_ANULACION_CUMPLIDO As String

'        'SICETAC
'        'Consulta

'        Public FECHAING As String
'        Public CONFIGURACION As String
'        Public VALOR As String
'        Public VALORTONELADA As String
'        Public VALORHORA As String
'        Public DISTANCIA As String

'        'Ingreso
'        Public PERIODO As String
'        Public MODELO As String
'        Public ORIGEN As String
'        Public DESTINO As String

'        Public bolVehiculoPropio As Boolean

'        Dim objGeneral As General
'        Dim solicitud As Solicitud
'        Dim wsManifiestoElectronico As ManifiestoElectronico.IBPMServicesservice
'        Dim objInterfaz As InterfazMinisterioTransporte
'        Dim acceso As Acceso
'        Dim strError As String

'#End Region

'#Region "Constantes"
'        Public Const NOMBRE_TAG_CONSULTA As String = "documento"
'        Public Const ERROR_MANIFIESTO_LICENCIA_CONDUCTOR_VENCIDA As String = "MAN245: La licencia de conducción del Conductor se encuentra vencida"
'        Public Const ERROR_MANIFIESTO_IDEN_CONDUCTOR_INEXISTENTE_O_LICENCIA_O_FECHA_ERRONEA As String = "MAN240: El número de identificación del conductor no existe o no tiene reportada una licencia de conducción"
'#End Region

'#Region "Propiedades"
'        Public Property EnlaceMinisterio As String
'            Get
'                Return Me.wsManifiestoElectronico.Url
'            End Get
'            Set(value As String)
'                Me.wsManifiestoElectronico.Url = value
'            End Set
'        End Property
'#End Region

'#Region "Constructor"

'        Sub New(ByVal acceso As Acceso, ByVal solicitud As Solicitud)
'            Me.objGeneral = New General
'            Me.acceso = New Acceso
'            Me.solicitud = New Solicitud
'            Me.wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice

'            Me.objInterfaz = New InterfazMinisterioTransporte(General.UNO)

'            Me.acceso = acceso
'            Me.solicitud = solicitud
'        End Sub

'        Sub New()
'            Me.objGeneral = New General
'            Me.acceso = New Acceso
'            Me.solicitud = New Solicitud
'            Me.wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
'            Me.objInterfaz = New InterfazMinisterioTransporte(General.UNO)
'        End Sub

'#End Region

'#Region "Funciones Privadas"

'#End Region

'#Region "Funciones Publicas"
'        Public Sub Crear_XML_Manifiesto(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByRef lstVariablesDestinatario As List(Of String))
'            Try

'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)

'                Select Case TipoFormato
'                    Case Tipos_Procesos.FORMATO_INGRESO
'                        Dim txtNodo As XmlText
'                        If Me.NUMNITEMPRESATRANSPORTE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.NUMMANIFIESTOCARGA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMMANIFIESTOCARGA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMMANIFIESTOCARGA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CONSECUTIVOINFORMACIONVIAJE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOINFORMACIONVIAJE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CONSECUTIVOINFORMACIONVIAJE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.MANNROMANIFIESTOTRANSBORDO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("MANNROMANIFIESTOTRANSBORDO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.MANNROMANIFIESTOTRANSBORDO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODOPERACIONTRANSPORTE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODOPERACIONTRANSPORTE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODOPERACIONTRANSPORTE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHAEXPEDICIONMANIFIESTO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHAEXPEDICIONMANIFIESTO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHAEXPEDICIONMANIFIESTO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODMUNICIPIOORIGENMANIFIESTO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODMUNICIPIOORIGENMANIFIESTO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODMUNICIPIOORIGENMANIFIESTO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODMUNICIPIODESTINOMANIFIESTO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODMUNICIPIODESTINOMANIFIESTO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODMUNICIPIODESTINOMANIFIESTO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODIDTITULARMANIFIESTO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODIDTITULARMANIFIESTO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODIDTITULARMANIFIESTO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.NUMIDTITULARMANIFIESTO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMIDTITULARMANIFIESTO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMIDTITULARMANIFIESTO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If


'                        If Me.NUMPLACA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMPLACA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMPLACA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.NUMPLACAREMOLQUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMPLACAREMOLQUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMPLACAREMOLQUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODIDCONDUCTOR <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODIDCONDUCTOR")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODIDCONDUCTOR)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.NUMIDCONDUCTOR <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMIDCONDUCTOR")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMIDCONDUCTOR)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Not bolVehiculoPropio Then
'                            If Me.VALORFLETEPACTADOVIAJE <> String.Empty Then
'                                NuevoElemento = XmlDocumento.CreateElement("VALORFLETEPACTADOVIAJE")
'                                Me.VALORFLETEPACTADOVIAJE = objInterfaz.Formatear_Campo(Me.VALORFLETEPACTADOVIAJE, 11, InterfazMinisterioTransporte.CAMPO_DECIMAL, 0)
'                                txtNodo = XmlDocumento.CreateTextNode(Me.VALORFLETEPACTADOVIAJE)
'                                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                            End If

'                            If Me.RETENCIONICAMANIFIESTOCARGA <> String.Empty Then
'                                NuevoElemento = XmlDocumento.CreateElement("RETENCIONICAMANIFIESTOCARGA")
'                                Me.RETENCIONICAMANIFIESTOCARGA = objInterfaz.Formatear_Campo(Me.RETENCIONICAMANIFIESTOCARGA, 4, InterfazMinisterioTransporte.CAMPO_DECIMAL, 0)
'                                txtNodo = XmlDocumento.CreateTextNode(Me.RETENCIONICAMANIFIESTOCARGA)
'                                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                            End If

'                            If Me.VALORANTICIPOMANIFIESTO <> String.Empty Then
'                                NuevoElemento = XmlDocumento.CreateElement("VALORANTICIPOMANIFIESTO")
'                                Me.VALORANTICIPOMANIFIESTO = objInterfaz.Formatear_Campo(Me.VALORANTICIPOMANIFIESTO, 11, InterfazMinisterioTransporte.CAMPO_DECIMAL, 0)
'                                txtNodo = XmlDocumento.CreateTextNode(Me.VALORANTICIPOMANIFIESTO)
'                                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                            End If
'                        End If



'                        If Me.RETENCIONFUENTEMANIFIESTO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("RETENCIONFUENTEMANIFIESTO")
'                            Me.RETENCIONFUENTEMANIFIESTO = objInterfaz.Formatear_Campo(Me.RETENCIONFUENTEMANIFIESTO, 11, InterfazMinisterioTransporte.CAMPO_DECIMAL, 0)
'                            txtNodo = XmlDocumento.CreateTextNode(Me.RETENCIONFUENTEMANIFIESTO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If



'                        If Me.CODMUNICIPIOPAGOSALDO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODMUNICIPIOPAGOSALDO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODMUNICIPIOPAGOSALDO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODMUNICIPIOPAGOSALDO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODMUNICIPIOPAGOSALDO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODMUNICIPIOPAGOSALDO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODRESPONSABLEPAGOCARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODRESPONSABLEPAGOCARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODRESPONSABLEPAGOCARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODRESPONSABLEPAGODESCARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODRESPONSABLEPAGODESCARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODRESPONSABLEPAGODESCARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHAPAGOSALDOMANIFIESTO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHAPAGOSALDOMANIFIESTO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHAPAGOSALDOMANIFIESTO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.OBSERVACIONES <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("OBSERVACIONES")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.OBSERVACIONES)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        NuevoElemento = XmlDocumento.CreateElement(REMESASMAN)
'                        Dim xmlAtributo As XmlAttribute = XmlDocumento.CreateAttribute(NOMBRE_PROCESO_INGRESO_DETALLE_MANIFIESTO)
'                        xmlAtributo.Value = VALOR_PROCESO_INGRESO_DETALLE_MANIFIESTO
'                        NuevoElemento.Attributes.Append(xmlAtributo)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)

'                        Dim cont As Short = 0
'                        While cont <> CONSECUTIVOREMESA.Length
'                            NuevoElemento = XmlDocumento.CreateElement("REMESA")
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(NuevoElemento)

'                            NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CONSECUTIVOREMESA(cont))
'                            xmlElementoRoot.LastChild.LastChild.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.LastChild.LastChild.AppendChild(txtNodo)
'                            cont = cont + 1
'                        End While

'                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
'                        Dim strAxuliarCampos As String = String.Empty
'                        For Each Items As String In lstVariablesDestinatario
'                            strAxuliarCampos &= Items
'                        Next
'                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("ingresoid" & strAxuliarCampos)
'                        xmlElementoRoot.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
'                        xmlElementoRoot.AppendChild(NuevoElemento)
'                        xmlElementoRoot = XmlDocumento.DocumentElement

'                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        NuevoElemento = XmlDocumento.CreateElement("NUMMANIFIESTOCARGA")
'                        txtNodo = XmlDocumento.CreateTextNode("'" & Me.NUMMANIFIESTOCARGA & "'")
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                End Select

'            Catch ex As Exception

'            End Try
'        End Sub

'        Public Sub Crear_XML_Cumplido_Manifiesto(ByRef XmlDocumento As XmlDocument, ByVal TipoProceso As Tipos_Procesos)
'            Try
'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)
'                Dim txtNodo As XmlText

'                Select Case TipoProceso
'                    Case Tipos_Procesos.FORMATO_INGRESO
'                        If Me.NUMNITEMPRESATRANSPORTE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.NUMMANIFIESTOCARGA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMMANIFIESTOCARGA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMMANIFIESTOCARGA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.TIPOCUMPLIDOMANIFIESTO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("TIPOCUMPLIDOMANIFIESTO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.TIPOCUMPLIDOMANIFIESTO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.MOTIVOSUSPENSIONMANIFIESTO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("MOTIVOSUSPENSIONMANIFIESTO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.MOTIVOSUSPENSIONMANIFIESTO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CONSECUENCIASUSPENSIONMANIFIESTO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CONSECUENCIASUSPENSIONMANIFIESTO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CONSECUENCIASUSPENSIONMANIFIESTO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHAENTREGADOCUMENTOS <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHAENTREGADOCUMENTOS")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHAENTREGADOCUMENTOS)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.VALORADICIONALHORASCARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("VALORADICIONALHORASCARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.VALORADICIONALHORASCARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.VALORADICIONALHORASDESCARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("VALORADICIONALHORASDESCARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.VALORADICIONALHORASDESCARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.VALORADICIONALFLETE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("VALORADICIONALFLETE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.VALORADICIONALFLETE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.MOTIVOVALORADICIONAL <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("MOTIVOVALORADICIONAL")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.MOTIVOVALORADICIONAL)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.VALORDESCUENTOFLETE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("VALORDESCUENTOFLETE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.VALORDESCUENTOFLETE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.MOTIVOVALORDESCUENTOMANIFIESTO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("MOTIVOVALORDESCUENTOMANIFIESTO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.MOTIVOVALORDESCUENTOMANIFIESTO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.VALORSOBREANTICIPO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("VALORSOBREANTICIPO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.VALORSOBREANTICIPO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.VALORSOBREANTICIPO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("VALORSOBREANTICIPO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.VALORSOBREANTICIPO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.OBSERVACIONES <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("OBSERVACIONES")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.OBSERVACIONES)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If
'                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        txtNodo = XmlDocumento.CreateTextNode("ingresoid")
'                        xmlElementoRoot.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
'                        xmlElementoRoot.AppendChild(NuevoElemento)
'                        xmlElementoRoot = XmlDocumento.DocumentElement

'                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        NuevoElemento = XmlDocumento.CreateElement("NUMMANIFIESTOCARGA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMMANIFIESTOCARGA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End Select

'            Catch ex As Exception

'            End Try
'        End Sub

'        Public Sub Crear_XML_Anular_Manifiesto(ByRef XmlDocumento As XmlDocument)
'            Try
'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)
'                Dim txtNodo As XmlText

'                If Me.NUMNITEMPRESATRANSPORTE <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                    txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'                If Me.NUMMANIFIESTOCARGA <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("NUMMANIFIESTOCARGA")
'                    txtNodo = XmlDocumento.CreateTextNode(Me.NUMMANIFIESTOCARGA)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'                If Me.MOTIVOANULACIONMANIFIESTO <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("MOTIVOANULACIONMANIFIESTO")
'                    txtNodo = XmlDocumento.CreateTextNode(Me.MOTIVOANULACIONMANIFIESTO)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'                If Me.OBSERVACIONES <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("OBSERVACIONES")
'                    txtNodo = XmlDocumento.CreateTextNode(Me.OBSERVACIONES)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If
'            Catch ex As Exception

'            End Try
'        End Sub

'        Public Sub Crear_XML_Anular_Informacion_Viaje(ByRef XmlDocumento As XmlDocument)
'            Try
'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)
'                Dim txtNodo As XmlText

'                If Me.NUMNITEMPRESATRANSPORTE <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                    txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'                If Me.NUMMANIFIESTOCARGA <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOINFORMACIONVIAJE")
'                    txtNodo = XmlDocumento.CreateTextNode(Me.NUMMANIFIESTOCARGA)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'                If Me.MOTIVOANULACIONMANIFIESTO <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("MOTIVOANULACIONINFOVIAJE")
'                    txtNodo = XmlDocumento.CreateTextNode("S")
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If
'                If Me.OBSERVACIONES <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("OBSERVACIONES")
'                    txtNodo = XmlDocumento.CreateTextNode(Me.OBSERVACIONES)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If
'            Catch ex As Exception
'            End Try
'        End Sub

'        Public Sub Crear_XML_Anular_Informacion_Cumplido(ByRef XmlDocumento As XmlDocument)
'            Try
'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)
'                Dim txtNodo As XmlText

'                If Me.NUMNITEMPRESATRANSPORTE <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                    txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'                If Me.NUMMANIFIESTOCARGA <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("NUMMANIFIESTOCARGA")
'                    txtNodo = XmlDocumento.CreateTextNode(Me.NUMMANIFIESTOCARGA)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If
'                If Me.CODMOTIVOANULACIONCUMPLIDO <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("CODMOTIVOANULACIONCUMPLIDO")
'                    txtNodo = XmlDocumento.CreateTextNode(Me.CODMOTIVOANULACIONCUMPLIDO)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If
'                If Me.OBSERVACIONES_ANULACION_CUMPLIDO <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("OBSERVACIONES")
'                    txtNodo = XmlDocumento.CreateTextNode(Me.OBSERVACIONES_ANULACION_CUMPLIDO)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If
'            Catch ex As Exception
'            End Try
'        End Sub

'        Public Function Reportar_Manifiesto(ByRef TextoXmlManifiesto As String, ByVal TipoFormato As Tipos_Procesos, ByRef strMensajeErrorRemesa As String, ByRef lstVariablesDestinatario As List(Of String)) As Double
'            Try
'                strMensajeErrorRemesa = String.Empty

'                solicitud.tipo = Solicitud.SOLICITUD_WS_MINISTERIO_TIPO_INSERTAR
'                solicitud.Procesoid = Solicitud.SOLICITUD_WS_MINISTERIO_PROCESO_MANIFIESTO
'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)

'                acceso.Crear_XML_Acceso(xmlDoc)

'                solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_MANIFIESTO)
'                Me.Crear_XML_Manifiesto(xmlDoc, TipoFormato, lstVariablesDestinatario)

'                TextoXmlManifiesto = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Reportar_Manifiesto = Obtener_Mensaje_XML(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, strMensajeErrorRemesa, lstVariablesDestinatario)

'            Catch ex As Exception
'                strMensajeErrorRemesa = ex.Message
'                Reportar_Manifiesto = General.CERO
'            End Try
'        End Function
'        Public Function Obtener_Mensaje_XML(ByVal strMensaje As String, ByVal TipoDocumento As Tipos_Procesos, ByRef strErrores As String, ByRef lstVariables As List(Of String)) As Double
'            Try
'                Obtener_Mensaje_XML = General.CERO

'                Dim xmlRespuesta As New XmlDocument
'                xmlRespuesta.LoadXml(strMensaje)
'                Select Case TipoDocumento
'                    Case Tipos_Procesos.FORMATO_INGRESO
'                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
'                            Obtener_Mensaje_XML = General.CERO
'                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
'                        End If
'                        If xmlRespuesta.GetElementsByTagName("ingresoid").Count > 0 Then
'                            Obtener_Mensaje_XML = Val(xmlRespuesta.GetElementsByTagName("ingresoid")(0).InnerText)
'                            strErrores = String.Empty
'                        End If
'                    Case Tipos_Procesos.FORTMATO_CONSULTA

'                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
'                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
'                            Obtener_Mensaje_XML = General.CERO
'                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
'                        End If
'                        If xmlRespuesta.GetElementsByTagName("ingresoid").Count > 0 Then
'                            Obtener_Mensaje_XML = Val(xmlRespuesta.GetElementsByTagName("ingresoid")(0).InnerText)
'                            strErrores = String.Empty
'                            For i As Integer = 0 To lstVariables.Count - 1
'                                If xmlRespuesta.GetElementsByTagName(lstVariables(i)).Count > 0 Then
'                                    lstVariables(i) = xmlRespuesta.GetElementsByTagName(lstVariables(i))(0).InnerText
'                                Else
'                                    lstVariables(i) = String.Empty
'                                End If
'                            Next
'                        End If
'                    Case Tipos_Procesos.FORTMATO_CONSULTA_SICETAC
'                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
'                            Obtener_Mensaje_XML = General.CERO
'                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
'                        End If
'                        If xmlRespuesta.GetElementsByTagName("valor").Count > 0 Then
'                            Obtener_Mensaje_XML = Val(xmlRespuesta.GetElementsByTagName("valor")(0).InnerText)
'                            strErrores = String.Empty
'                            For i As Integer = 0 To lstVariables.Count - 1
'                                If xmlRespuesta.GetElementsByTagName(lstVariables(i)).Count > 0 Then
'                                    lstVariables(i) = xmlRespuesta.GetElementsByTagName(lstVariables(i))(0).InnerText
'                                Else
'                                    lstVariables(i) = String.Empty
'                                End If
'                            Next
'                        End If
'                End Select
'            Catch ex As Exception
'                strErrores = ex.Message
'            End Try
'        End Function

'        Public Function Reportar_Cumplido_Manifiesto(ByRef TextoXmlCumplidoManifiesto As String, ByVal TipoProceso As Tipos_Procesos, ByRef StrError As String) As Double
'            Try
'                solicitud.tipo = Solicitud.SOLICITUD_WS_MINISTERIO_TIPO_INSERTAR
'                solicitud.Procesoid = Solicitud.SOLICITUD_WS_MINISTERIO_PROCESO_CUMPLIR_MANIFIESTO
'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)
'                acceso.Crear_XML_Acceso(xmlDoc)
'                solicitud.Crear_XML_Solicitud(xmlDoc, TipoProceso, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_CUMPLIR_MANIFIESTO)
'                Me.Crear_XML_Cumplido_Manifiesto(xmlDoc, TipoProceso)

'                TextoXmlCumplidoManifiesto = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Reportar_Cumplido_Manifiesto = Obtener_Mensaje_XML(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), StrError)

'            Catch ex As Exception
'                Reportar_Cumplido_Manifiesto = General.CERO
'                StrError = ex.Message
'            End Try
'        End Function
'        Public Function Reportar_Anulacion_Manifiesto(ByRef TextoXmlManifiestoAnulado As String, ByRef strMensaje As String) As Double
'            Try
'                solicitud.tipo = Solicitud.SOLICITUD_WS_MINISTERIO_TIPO_INSERTAR
'                solicitud.Procesoid = Solicitud.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_MANIFIESTO
'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)

'                acceso.Crear_XML_Acceso(xmlDoc)
'                solicitud.Crear_XML_Solicitud(xmlDoc, Tipos_Procesos.FORMATO_INGRESO, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_MANIFIESTO)
'                Me.Crear_XML_Anular_Manifiesto(xmlDoc)
'                TextoXmlManifiestoAnulado = objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Reportar_Anulacion_Manifiesto = Obtener_Mensaje_XML(wsManifiestoElectronico.AtenderMensajeBPM(TextoXmlManifiestoAnulado), strMensaje)

'            Catch ex As Exception
'                strMensaje = ex.Message
'                Reportar_Anulacion_Manifiesto = General.CERO
'            End Try
'        End Function

'        Public Function Reportar_Anulacion_Informacion_Viaje(ByRef TextoXmlAnulacionViaje As String, ByRef StrMensaje As String) As Double
'            Try
'                solicitud.tipo = Solicitud.SOLICITUD_WS_MINISTERIO_TIPO_INSERTAR
'                solicitud.Procesoid = Solicitud.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_VIAJE
'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)

'                acceso.Crear_XML_Acceso(xmlDoc)
'                solicitud.Crear_XML_Solicitud(xmlDoc, Tipos_Procesos.FORMATO_INGRESO, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_VIAJE)
'                Me.Crear_XML_Anular_Informacion_Viaje(xmlDoc)
'                TextoXmlAnulacionViaje = objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Reportar_Anulacion_Informacion_Viaje = Obtener_Mensaje_XML(wsManifiestoElectronico.AtenderMensajeBPM(TextoXmlAnulacionViaje), StrMensaje)

'            Catch ex As Exception
'                StrMensaje = ex.Message
'                Reportar_Anulacion_Informacion_Viaje = General.CERO
'            End Try
'        End Function

'        Public Function Reportar_Anulacion_Cumplido(ByRef TextoXmlAnulacionCumplido As String, ByRef StrMensaje As String) As Double
'            Try
'                solicitud.tipo = Solicitud.SOLICITUD_WS_MINISTERIO_TIPO_INSERTAR
'                solicitud.Procesoid = Solicitud.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_VIAJE
'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)

'                acceso.Crear_XML_Acceso(xmlDoc)
'                solicitud.Crear_XML_Solicitud(xmlDoc, Tipos_Procesos.FORMATO_INGRESO, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_CUMPLIDO)
'                Me.Crear_XML_Anular_Informacion_Cumplido(xmlDoc)
'                TextoXmlAnulacionCumplido = objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Reportar_Anulacion_Cumplido = Obtener_Mensaje_XML(wsManifiestoElectronico.AtenderMensajeBPM(TextoXmlAnulacionCumplido), StrMensaje)

'            Catch ex As Exception
'                StrMensaje = ex.Message
'                Reportar_Anulacion_Cumplido = General.CERO
'            End Try
'        End Function
'        Public Function Obtener_Mensaje_XML(ByVal strMensaje As String, ByRef strError As String) As Double
'            Try
'                Obtener_Mensaje_XML = General.CERO
'                Dim xmlRespuesta As New XmlDocument
'                xmlRespuesta.LoadXml(strMensaje)
'                Dim xmlElementoAuxiliar As XmlNodeList = xmlRespuesta.GetElementsByTagName("ErrorMSG")
'                If Not IsNothing(xmlElementoAuxiliar(General.CERO)) Then
'                    strError = xmlElementoAuxiliar(General.CERO).InnerText
'                    Obtener_Mensaje_XML = General.CERO
'                Else
'                    xmlElementoAuxiliar = xmlRespuesta.GetElementsByTagName("ingresoid")
'                    If Not IsNothing(xmlElementoAuxiliar(General.CERO)) Then
'                        strError = String.Empty
'                        Obtener_Mensaje_XML = Val(xmlElementoAuxiliar(General.CERO).InnerText)
'                    End If
'                End If
'            Catch ex As Exception
'                strError = ex.Message
'            End Try
'        End Function

'        Public Function Consultar_Flete_Minimo_Sicetac(ByRef TextoXmlSicetac As String, ByVal TipoFormato As Tipos_Procesos, ByRef strErrores As String, ByRef lstVariables As List(Of String), ByVal Tipos_Rol As Tipos_Roles) As Double
'            Try
'                Consultar_Flete_Minimo_Sicetac = General.CERO
'                strErrores = String.Empty

'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)

'                Me.acceso.Crear_XML_Acceso(xmlDoc)
'                Me.solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_CONSULTA_SICETAC)
'                Me.Crear_XML_Consulta_SICETAC(xmlDoc)
'                TextoXmlSicetac = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Consultar_Flete_Minimo_Sicetac = Obtener_Mensaje_XML(Me.wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, strErrores, lstVariables)

'            Catch ex As Exception
'                Consultar_Flete_Minimo_Sicetac = 0
'                strErrores = ex.Message
'            End Try
'        End Function

'        Public Sub Crear_XML_Consulta_SICETAC(ByRef XmlDocumento As XmlDocument)
'            Try
'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)

'                xmlElementoRoot = XmlDocumento.DocumentElement

'                Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("FECHAING,CONFIGURACION,VALOR,VALORTONELADA,VALORHORA,DISTANCIA")
'                xmlElementoRoot.LastChild.AppendChild(txtNodo)

'                xmlElementoRoot = XmlDocumento.DocumentElement
'                NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
'                xmlElementoRoot.AppendChild(NuevoElemento)

'                'NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                'txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                'xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                'xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                NuevoElemento = XmlDocumento.CreateElement("PERIODO")
'                txtNodo = XmlDocumento.CreateTextNode(Me.PERIODO)
'                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                NuevoElemento = XmlDocumento.CreateElement("MODELO")
'                txtNodo = XmlDocumento.CreateTextNode(Me.MODELO)
'                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                NuevoElemento = XmlDocumento.CreateElement("ORIGEN")
'                txtNodo = XmlDocumento.CreateTextNode(Me.ORIGEN)
'                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                NuevoElemento = XmlDocumento.CreateElement("DESTINO")
'                txtNodo = XmlDocumento.CreateTextNode(Me.DESTINO)
'                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                NuevoElemento = XmlDocumento.CreateElement("CONFIGURACION")
'                txtNodo = XmlDocumento.CreateTextNode(Me.CONFIGURACION)
'                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'            Catch ex As Exception

'            End Try
'        End Sub
'#End Region
'    End Class

'End Namespace
