﻿'Imports System.Data
'Imports System.Data.SqlClient
'Namespace Entidades.ReporteMinisterio

'    Public Class Pais
'#Region "Declaracion Variables"

'        Dim strSQL As String

'        Public strError As String
'        Public bolModificar As Boolean
'        Private intCodigo As Integer
'        Private strNombre As String
'        Private strCodigoAlterno As String
'        Private objEmpresa As Empresas
'        Private objGeneral As General

'#End Region

'#Region "Declaracion Constantes"
'        Public Const COLOMBIA As Byte = 1
'#End Region

'#Region "Constructor"

'        Sub New()
'            Me.objGeneral = New General
'            Me.objEmpresa = New Empresas(0)
'            Me.intCodigo = 0
'            Me.strNombre = ""
'            Me.strCodigoAlterno = ""
'        End Sub

'        Sub New(ByRef Empresa As Empresas)
'            Me.objGeneral = New General
'            Me.objEmpresa = Empresa
'            Me.intCodigo = 0
'            Me.strNombre = ""
'            Me.strCodigoAlterno = ""
'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property Codigo() As Integer
'            Get
'                Return Me.intCodigo
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCodigo = value
'            End Set
'        End Property

'        Public Property CodigoAlterno() As String
'            Get
'                Return Me.strCodigoAlterno
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigoAlterno = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean
'            Try

'                Dim sdrPais As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT * FROM Paises"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrPais = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrPais.Read() Then
'                    Me.strNombre = sdrPais("Nombre").ToString()
'                    Me.strCodigoAlterno = sdrPais("Codigo_Alterno").ToString()
'                    Me.intCodigo = Val(sdrPais("Codigo").ToString())
'                    Consultar = True
'                Else
'                    Me.intCodigo = 0
'                    Consultar = False
'                End If

'                sdrPais.Close()
'                sdrPais.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Codigo As Integer) As Boolean
'            Try
'                Me.objEmpresa = Empresa
'                Dim sdrPais As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT * FROM Paises"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrPais = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrPais.Read() Then
'                    Me.strNombre = sdrPais("Nombre").ToString()
'                    Me.strCodigoAlterno = sdrPais("Codigo_Alterno").ToString()
'                    Me.intCodigo = Val(sdrPais("Codigo").ToString())
'                    Consultar = True
'                Else
'                    Me.intCodigo = 0
'                    Consultar = False
'                End If

'                sdrPais.Close()
'                sdrPais.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Existe_Registro() As Boolean
'            Try
'                Dim sdrPais As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo FROM Paises"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()
'                sdrPais = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrPais.Read() Then
'                    Existe_Registro = True
'                Else
'                    Me.intCodigo = 0
'                    Existe_Registro = False
'                End If
'                sdrPais.Close()
'                sdrPais.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Existe_Registro = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Codigo As Long) As Boolean
'            Try
'                Me.objEmpresa = Empresa
'                Me.intCodigo = Codigo

'                Dim sdrPais As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Codigo, Nombre FROM Paises"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.intCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True

'                Me.objGeneral.ConexionSQL.Open()
'                sdrPais = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrPais.Read() Then
'                    Me.intCodigo = Val(sdrPais("Codigo").ToString())
'                    Existe_Registro = True
'                Else
'                    Existe_Registro = False
'                End If

'                sdrPais.Close()
'                sdrPais.Dispose()

'            Catch ex As Exception
'                Me.intCodigo = 0
'                Existe_Registro = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Guardar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                If Datos_Requeridos(Mensaje) Then
'                    If Not bolModificar Then
'                        Guardar = Insertar_SQL()
'                    Else
'                        Guardar = Modificar_SQL()
'                    End If
'                Else
'                    Guardar = False
'                End If
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Guardar = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar: " & ex.Message.ToString()
'            End Try
'        End Function

'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Eliminar = Borrar_SQL()
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar: " & ex.Message.ToString()
'                Eliminar = False
'            End Try
'        End Function

'#End Region

'#Region "Funciones Privadas"

'        Private Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Me.strError = ""
'                Dim intCont As Integer = 0
'                Datos_Requeridos = True

'                If Me.strNombre = "" Then
'                    intCont += 1
'                    strError += intCont & ". Digite el nombre del país"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If


'                If Me.intCodigo = 0 Then
'                    intCont += 1
'                    strError += intCont & ". Digite un código válido para el país"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.intCodigo <> 0 And Existe_Registro(Me.objEmpresa, Me.intCodigo) Then

'                    If Me.bolModificar = False Then
'                        intCont += 1
'                        strError += intCont & ". El código digitado para el país ya se encuentra registrado"
'                        strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If

'                End If

'                If Me.strNombre <> "" And Len(Me.strNombre) < General.MINIMO_CUATRO_CARACTERES_INGRESO_NOMBRE Then

'                    intCont += 1
'                    strError += intCont & ". El nombre del País no puede tener menos de cuatro (4) carácteres"
'                    strError += " <Br />"
'                    Datos_Requeridos = False

'                End If

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & ex.Message.ToString()
'                Datos_Requeridos = False
'            End Try

'        End Function

'        Private Function Borrar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_paises", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.intCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                Else
'                    Borrar_SQL = False
'                End If

'            Catch ex As Exception

'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)

'                Borrar_SQL = False

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Insertar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_paises", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.intCodigo
'                ComandoSQL.Parameters.Add("@par_Codigo_Alterno", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Codigo_Alterno").Value = Me.strCodigoAlterno
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre


'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_SQL = True
'                Else
'                    Insertar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Insertar_SQL = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Modificar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_paises", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.intCodigo
'                ComandoSQL.Parameters.Add("@par_Codigo_Alterno", SqlDbType.VarChar, 10) : ComandoSQL.Parameters("@par_Codigo_Alterno").Value = Me.strCodigoAlterno
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre


'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Modificar_SQL = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'#End Region
'    End Class

'End Namespace

