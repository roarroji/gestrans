﻿'Imports System.Data
'Imports System.Data.SqlClient
'Namespace Entidades.ReporteMinisterio
'    Public Class PlanUnicoCuenta
'#Region "Declaracion Variables"

'        Dim strSQL As String
'        Dim intCont As Short

'        Public intTipo As Integer
'        Private dblTarifa As Double
'        Public strError As String
'        Public bolModificar As Boolean
'        Private objEmpresa As Empresas

'        Private strCodigo As String
'        Private strNombre As String
'        Private dblValorBase As Double
'        Private intExigeCentroCosto As Integer
'        Private intExigeValorBase As Integer

'        Private intExigeTercero As Integer
'        Private intAplicaCertificadoReteFuente As Integer
'        Private intAplicaCertificadoReteIca As Integer
'        Private objGeneral As General

'        Private intExigeDocumentoCruce As Integer
'        Private objCataPlanoInterfaseContable As Catalogo

'        Private objUsuario As Usuario
'        Private intCuentaBanco As Integer
'        Private objControlAuditoria As ControlAuditoria

'        <System.Xml.Serialization.XmlIgnore()>
'        Public bolAuditar As Boolean

'#End Region

'#Region "Declaracion de Constantes"

'        Public Const EXIGE As Integer = 1
'        Public Const NO_EXIGE As Integer = 0
'        Public Const ES_CUENTA_DE_BANCO As Integer = 1
'        Public Const CUENTA_PUC_VALOR_NEGATIVO_FACTURA As String = "2805050102"
'        Public Const SUFIJO_CUENTA_PUC_VALOR_FACTURA_PARA_CLIENTE_EXTERIOR_ALCOMEX As String = "1001"

'#End Region

'#Region "Constructores"

'        Sub New()

'            Me.objEmpresa = New Empresas(0)
'            Me.dblTarifa = 0
'            Me.strCodigo = ""
'            Me.strNombre = ""
'            Me.intExigeCentroCosto = 0

'            Me.intExigeValorBase = 0
'            Me.intExigeTercero = 0
'            Me.intAplicaCertificadoReteFuente = 0
'            Me.intAplicaCertificadoReteIca = 0
'            Me.objGeneral = New General

'            Me.dblValorBase = 0
'            Me.objUsuario = New Usuario(Me.objEmpresa)
'            Me.objCataPlanoInterfaseContable = New Catalogo(Me.objEmpresa, "PICO")
'            Me.intCuentaBanco = 0
'            Me.objControlAuditoria = New ControlAuditoria

'        End Sub

'        Sub New(ByRef Empresa As Empresas)

'            Me.objEmpresa = Empresa
'            Me.dblTarifa = 0
'            Me.strCodigo = ""
'            Me.strNombre = ""
'            Me.intExigeCentroCosto = 0

'            Me.intExigeValorBase = 0
'            Me.intExigeTercero = 0
'            Me.intAplicaCertificadoReteFuente = 0
'            Me.intAplicaCertificadoReteIca = 0
'            Me.objGeneral = New General

'            Me.dblValorBase = 0
'            Me.objUsuario = New Usuario(Me.objEmpresa)
'            Me.objCataPlanoInterfaseContable = New Catalogo(Me.objEmpresa, "PICO")
'            Me.intCuentaBanco = 0
'            Me.objControlAuditoria = New ControlAuditoria

'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property Codigo() As String
'            Get
'                Return Me.strCodigo
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigo = value
'            End Set
'        End Property

'        <System.Xml.Serialization.XmlIgnore()>
'        Public Property CataPlanoInterfaseContable() As Catalogo
'            Get
'                Return Me.objCataPlanoInterfaseContable
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataPlanoInterfaseContable = value
'            End Set
'        End Property

'        Public Property ExigeDocumentoCruce() As Integer
'            Get
'                Return Me.intExigeDocumentoCruce
'            End Get
'            Set(ByVal value As Integer)
'                If value < 0 Then
'                    value = value * (-1)
'                End If
'                Me.intExigeDocumentoCruce = value
'            End Set
'        End Property

'        Public Property ExigeCentroCosto() As Integer
'            Get
'                Return Me.intExigeCentroCosto
'            End Get
'            Set(ByVal value As Integer)
'                If value < 0 Then
'                    value = value * (-1)
'                End If
'                Me.intExigeCentroCosto = value
'            End Set
'        End Property

'        Public Property ExigeValorBase() As Integer
'            Get
'                Return Me.intExigeValorBase
'            End Get
'            Set(ByVal value As Integer)
'                If value < 0 Then
'                    value = value * (-1)
'                End If

'                Me.intExigeValorBase = value
'            End Set
'        End Property

'        Public Property ExigeTercero() As Integer
'            Get
'                Return Me.intExigeTercero
'            End Get
'            Set(ByVal value As Integer)
'                If value < 0 Then
'                    value = value * (-1)
'                End If

'                Me.intExigeTercero = value
'            End Set
'        End Property

'        Public Property AplicaReteFuente() As Integer
'            Get
'                Return Me.intAplicaCertificadoReteFuente
'            End Get
'            Set(ByVal value As Integer)
'                If value < 0 Then
'                    value = value * (-1)
'                End If

'                Me.intAplicaCertificadoReteFuente = value
'            End Set
'        End Property

'        Public Property AplicaReteIca() As Integer
'            Get
'                Return Me.intAplicaCertificadoReteIca
'            End Get
'            Set(ByVal value As Integer)
'                If value < 0 Then
'                    value = value * (-1)
'                End If

'                Me.intAplicaCertificadoReteIca = value
'            End Set
'        End Property

'        Public Property modificar() As Boolean
'            Get
'                Return Me.bolModificar
'            End Get
'            Set(ByVal value As Boolean)
'                Me.bolModificar = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'        Public Property Tarifa() As Double
'            Get
'                Return Me.dblTarifa
'            End Get
'            Set(ByVal value As Double)
'                Me.dblTarifa = value
'            End Set
'        End Property

'        Public Property ValorBase() As Double
'            Get
'                Return Me.dblValorBase
'            End Get
'            Set(ByVal value As Double)
'                Me.dblValorBase = value
'            End Set
'        End Property

'        <System.Xml.Serialization.XmlIgnore()>
'        Public Property Usuario() As Usuario
'            Get
'                Return Me.objUsuario
'            End Get
'            Set(ByVal value As Usuario)
'                Me.objUsuario = value
'            End Set
'        End Property

'        Public Property CuentaBanco() As Integer
'            Get
'                Return Me.intCuentaBanco
'            End Get
'            Set(ByVal value As Integer)
'                If value < 0 Then
'                    value = value * (-1)
'                End If
'                Me.intCuentaBanco = value
'            End Set
'        End Property

'        <System.Xml.Serialization.XmlIgnore()>
'        Public Property ControlAuditoria() As ControlAuditoria
'            Get
'                Return Me.objControlAuditoria
'            End Get
'            Set(ByVal value As ControlAuditoria)
'                Me.objControlAuditoria = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean

'            Try

'                Dim sdrPlanUnicoCuenta As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Codigo, Nombre, Exige_Centro_Costo, Exige_Valor_Base, Exige_Tercero,"
'                strSQL += " Aplica_Certificado_Rete_Fuente, Aplica_Certificado_Rete_Ica, Tarifa, Valor_Base,"
'                strSQL += " USUA_Crea, Exige_Documento_Cruce, PICO_Codigo, Cuenta_Banco FROM Plan_Unico_Cuentas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = '" & Me.strCodigo & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrPlanUnicoCuenta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrPlanUnicoCuenta.Read() Then

'                    Me.strCodigo = sdrPlanUnicoCuenta("Codigo")
'                    Me.strNombre = sdrPlanUnicoCuenta("Nombre").ToString()

'                    If sdrPlanUnicoCuenta("Exige_Centro_Costo") IsNot DBNull.Value Then
'                        Me.intExigeCentroCosto = sdrPlanUnicoCuenta("Exige_Centro_Costo")
'                    End If

'                    If sdrPlanUnicoCuenta("Exige_Valor_Base") IsNot DBNull.Value Then
'                        Me.intExigeValorBase = sdrPlanUnicoCuenta("Exige_Valor_Base")
'                    End If

'                    If sdrPlanUnicoCuenta("Exige_Tercero") IsNot DBNull.Value Then
'                        Me.intExigeTercero = sdrPlanUnicoCuenta("Exige_Tercero")
'                    End If

'                    Integer.TryParse(sdrPlanUnicoCuenta("Exige_Centro_Costo").ToString(), Me.intExigeCentroCosto)
'                    Integer.TryParse(sdrPlanUnicoCuenta("Exige_Valor_Base").ToString(), Me.intExigeValorBase)
'                    Integer.TryParse(sdrPlanUnicoCuenta("Exige_Tercero").ToString(), Me.intExigeTercero)
'                    Integer.TryParse(sdrPlanUnicoCuenta("Aplica_Certificado_Rete_Fuente").ToString(), Me.intAplicaCertificadoReteFuente)
'                    Integer.TryParse(sdrPlanUnicoCuenta("Aplica_Certificado_Rete_Ica").ToString(), Me.intAplicaCertificadoReteIca)

'                    Me.dblTarifa = Val(sdrPlanUnicoCuenta("Tarifa").ToString())
'                    Me.dblValorBase = Val(sdrPlanUnicoCuenta("Valor_Base").ToString())
'                    Me.objUsuario.Codigo = sdrPlanUnicoCuenta("USUA_Crea").ToString()

'                    Integer.TryParse(sdrPlanUnicoCuenta("Exige_Documento_Cruce").ToString(), Me.intExigeDocumentoCruce)

'                    Me.objCataPlanoInterfaseContable.Campo1 = Val(sdrPlanUnicoCuenta("PICO_Codigo").ToString)

'                    If sdrPlanUnicoCuenta("Cuenta_Banco") IsNot DBNull.Value Then
'                        Me.intCuentaBanco = sdrPlanUnicoCuenta("Cuenta_Banco")
'                    End If

'                    Consultar = True

'                Else
'                    Me.strCodigo = 0
'                    Consultar = False
'                End If

'                sdrPlanUnicoCuenta.Close()
'                sdrPlanUnicoCuenta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.strCodigo = 0
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Codigo As String) As Boolean

'            Try

'                Me.objEmpresa = Empresa
'                Me.strCodigo = Codigo

'                Dim sdrPlanUnicoCuenta As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Codigo, Nombre, Exige_Centro_Costo, Exige_Valor_Base, Exige_Tercero,"
'                strSQL += " Aplica_Certificado_Rete_Fuente, Aplica_Certificado_Rete_Ica, Tarifa, Valor_Base,"
'                strSQL += " Exige_Documento_Cruce, PICO_Codigo, Cuenta_Banco FROM Plan_Unico_Cuentas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = '" & Me.strCodigo & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrPlanUnicoCuenta = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrPlanUnicoCuenta.Read() Then
'                    Me.strCodigo = sdrPlanUnicoCuenta("Codigo")
'                    Me.strNombre = sdrPlanUnicoCuenta("Nombre").ToString()

'                    If sdrPlanUnicoCuenta("Exige_Centro_Costo") IsNot DBNull.Value Then
'                        Me.intExigeCentroCosto = sdrPlanUnicoCuenta("Exige_Centro_Costo")
'                    End If
'                    If sdrPlanUnicoCuenta("Exige_Valor_Base") IsNot DBNull.Value Then
'                        Me.intExigeValorBase = sdrPlanUnicoCuenta("Exige_Valor_Base")
'                    End If
'                    If sdrPlanUnicoCuenta("Exige_Tercero") IsNot DBNull.Value Then
'                        Me.intExigeTercero = sdrPlanUnicoCuenta("Exige_Tercero")
'                    End If

'                    Integer.TryParse(sdrPlanUnicoCuenta("Exige_Centro_Costo").ToString(), Me.intExigeCentroCosto)
'                    Integer.TryParse(sdrPlanUnicoCuenta("Exige_Valor_Base").ToString(), Me.intExigeValorBase)
'                    Integer.TryParse(sdrPlanUnicoCuenta("Exige_Tercero").ToString(), Me.intExigeTercero)
'                    Integer.TryParse(sdrPlanUnicoCuenta("Aplica_Certificado_Rete_Fuente").ToString(), Me.intAplicaCertificadoReteFuente)
'                    Integer.TryParse(sdrPlanUnicoCuenta("Aplica_Certificado_Rete_Ica").ToString(), Me.intAplicaCertificadoReteIca)

'                    Me.dblTarifa = Val(sdrPlanUnicoCuenta("Tarifa").ToString())
'                    Me.dblValorBase = Val(sdrPlanUnicoCuenta("Valor_Base").ToString())

'                    Integer.TryParse(sdrPlanUnicoCuenta("Exige_Documento_Cruce").ToString(), Me.intExigeDocumentoCruce)
'                    Me.objCataPlanoInterfaseContable.Campo1 = Val(sdrPlanUnicoCuenta("PICO_Codigo").ToString)

'                    If sdrPlanUnicoCuenta("Cuenta_Banco") IsNot DBNull.Value Then
'                        Me.intCuentaBanco = sdrPlanUnicoCuenta("Cuenta_Banco")
'                    End If

'                    Consultar = True
'                Else
'                    Consultar = False
'                End If

'                sdrPlanUnicoCuenta.Close()
'                sdrPlanUnicoCuenta.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro() As Boolean

'            Try
'                Dim sdrPlanUnicCuent As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Nombre FROM Plan_Unico_Cuentas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = '" & Me.strCodigo & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()

'                sdrPlanUnicCuent = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrPlanUnicCuent.Read() Then
'                    Existe_Registro = True
'                    Me.strCodigo = sdrPlanUnicCuent("Codigo").ToString()
'                    Me.strNombre = sdrPlanUnicCuent("Nombre").ToString()
'                Else
'                    Me.strCodigo = ""
'                    Existe_Registro = False
'                End If

'                sdrPlanUnicCuent.Close()
'                sdrPlanUnicCuent.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception

'                Me.strCodigo = ""
'                Existe_Registro = False
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)

'            Finally

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If

'            End Try

'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Codigo As String) As Boolean

'            Try

'                Dim sdrPlanUnicCuent As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                Me.objEmpresa = Empresa
'                Me.strCodigo = Codigo


'                strSQL = "SELECT EMPR_Codigo, Codigo, Nombre FROM Plan_Unico_Cuentas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = '" & Me.strCodigo & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()

'                sdrPlanUnicCuent = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrPlanUnicCuent.Read() Then
'                    Me.strCodigo = sdrPlanUnicCuent("Codigo").ToString()
'                    Me.strNombre = sdrPlanUnicCuent("Nombre").ToString()
'                    Existe_Registro = True
'                Else
'                    Me.strCodigo = ""
'                    Existe_Registro = False
'                End If

'                sdrPlanUnicCuent.Close()
'                sdrPlanUnicCuent.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception

'                Me.strCodigo = ""
'                Existe_Registro = False
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)

'            Finally

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If

'            End Try

'        End Function

'        Public Function Guardar(Optional ByRef Mensaje As String = "") As Boolean

'            Try

'                Guardar = False

'                If Datos_Requeridos(Mensaje) Then
'                    Guardar = True
'                    If Not modificar Then
'                        Guardar = Insertar_SQL()
'                    Else
'                        Guardar = Modificar_SQL()
'                    End If
'                End If

'                Mensaje = Me.strError

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Guardar = False
'            End Try

'        End Function

'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean

'            Try
'                Eliminar = Borrar_SQL()
'            Catch ex As Exception
'                Eliminar = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'            End Try
'            Mensaje = Me.strError
'        End Function

'        Public Function Convierte_Datos(Optional ByRef val As Integer = 0) As Integer

'            Try
'                If val = -1 Then
'                    val = val * -1
'                End If

'                Return Convierte_Datos

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Convierte_Datos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Convierte_Datos = 0
'            End Try

'        End Function

'#End Region

'#Region "Funciones Privadas"

'        Private Function Insertar_SQL() As Boolean

'            Try

'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_plan_unico_cuentas", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Codigo").Value = Me.strCodigo
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 70) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_Exige_Centro_Costo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Exige_Centro_Costo").Value = Me.intExigeCentroCosto
'                ComandoSQL.Parameters.Add("@par_Exige_Valor_Base", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Exige_Valor_Base").Value = Me.intExigeValorBase

'                ComandoSQL.Parameters.Add("@par_Valor_Base", SqlDbType.Money) : ComandoSQL.Parameters("@par_Valor_Base").Value = Me.dblValorBase
'                ComandoSQL.Parameters.Add("@par_Exige_Tercero", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Exige_Tercero").Value = Me.intExigeTercero
'                ComandoSQL.Parameters.Add("@par_Aplica_Certificado_Rete_Fuente", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Aplica_Certificado_Rete_Fuente").Value = Me.intAplicaCertificadoReteFuente
'                ComandoSQL.Parameters.Add("@par_Aplica_Certificado_Rete_Ica", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Aplica_Certificado_Rete_Ica").Value = Me.intAplicaCertificadoReteIca
'                ComandoSQL.Parameters.Add("@par_Tarifa", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Tarifa").Value = Me.dblTarifa

'                ComandoSQL.Parameters.Add("@par_USUA_Crea", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Crea").Value = Me.objUsuario.Codigo
'                ComandoSQL.Parameters.Add("@par_Exige_Documento_Cruce", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Exige_Documento_Cruce").Value = Me.intExigeDocumentoCruce
'                ComandoSQL.Parameters.Add("@par_PICO_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_PICO_Codigo").Value = Val(Me.objCataPlanoInterfaseContable.Campo1)
'                ComandoSQL.Parameters.Add("@par_Cuenta_Banco", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Cuenta_Banco").Value = Me.intCuentaBanco

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_SQL = True
'                    If Me.bolAuditar Then
'                        Me.objControlAuditoria.Insertar_Control_Auditoria_SQL(ComandoSQL.Parameters, "Plan_Unico_Cuentas", ControlAuditoria.TRANSACCION_INSERTAR, Me.strError)
'                    End If
'                Else
'                    Insertar_SQL = False
'                End If

'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Insertar_SQL = False
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Private Function Modificar_SQL() As Boolean

'            Try
'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_plan_unico_cuentas", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Codigo").Value = Me.strCodigo
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 70) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_Exige_Centro_Costo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Exige_Centro_Costo").Value = Me.intExigeCentroCosto
'                ComandoSQL.Parameters.Add("@par_Exige_Valor_Base", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Exige_Valor_Base").Value = Me.intExigeValorBase

'                ComandoSQL.Parameters.Add("@par_Valor_Base", SqlDbType.Money) : ComandoSQL.Parameters("@par_Valor_Base").Value = Me.dblValorBase
'                ComandoSQL.Parameters.Add("@par_Exige_Tercero", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Exige_Tercero").Value = Me.intExigeTercero
'                ComandoSQL.Parameters.Add("@par_Aplica_Certificado_Rete_Fuente", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Aplica_Certificado_Rete_Fuente").Value = Me.intAplicaCertificadoReteFuente
'                ComandoSQL.Parameters.Add("@par_Aplica_Certificado_Rete_Ica", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Aplica_Certificado_Rete_Ica").Value = Me.intAplicaCertificadoReteIca
'                ComandoSQL.Parameters.Add("@par_Tarifa", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Tarifa").Value = Me.dblTarifa

'                ComandoSQL.Parameters.Add("@par_USUA_Modifica", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Modifica").Value = Me.objUsuario.Codigo
'                ComandoSQL.Parameters.Add("@par_Exige_Documento_Cruce", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Exige_Documento_Cruce").Value = Me.intExigeDocumentoCruce
'                ComandoSQL.Parameters.Add("@par_PICO_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_PICO_Codigo").Value = Val(Me.objCataPlanoInterfaseContable.Campo1)
'                ComandoSQL.Parameters.Add("@par_Cuenta_Banco", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Cuenta_Banco").Value = Me.intCuentaBanco

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                    If Me.bolAuditar Then
'                        Me.objControlAuditoria.Insertar_Control_Auditoria_SQL(ComandoSQL.Parameters, "Plan_Unico_Cuentas", ControlAuditoria.TRANSACCION_MODIFICAR, Me.strError)
'                    End If
'                Else
'                    Modificar_SQL = False
'                End If

'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Modificar_SQL = False
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Private Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean

'            Try

'                Me.strError = ""
'                Datos_Requeridos = True
'                intCont = 1

'                If strCodigo = "0" Or strCodigo = "" Then
'                    Me.strError = intCont & ". Debe digitar el código de la cuenta."
'                    intCont += 1
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If IsNumeric(strCodigo) = False And strCodigo <> "" Then
'                    Me.strError += intCont & ". El código de la cuenta debe ser numérico."
'                    intCont += 1
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(strCodigo) > 20 Then
'                    Me.strError = intCont & ". El código de la cuenta debe tener una longitud máxima de dieciséis (16) carácteres"
'                    intCont += 1
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(strNombre) <= 0 Then
'                    Me.strError += intCont & ". Debe digitar el nombre de la cuenta " & strCodigo
'                    intCont += 1
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(strNombre) > 40 Then
'                    Me.strError += intCont & ". La longitud del nombre debe ser menor a cuarenta (40) carácteres."
'                    intCont += 1
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If


'                If Len(strNombre) < 4 Then
'                    Me.strError += intCont & ". La longitud del Nombre no puede tener menos de cuatro (4) carácteres."
'                    intCont += 1
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Not IsNumeric(dblTarifa) Then
'                    Me.strError += intCont & ". La tarifa tiene que ser un valor numérico."
'                    intCont += 1
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If IsNumeric(dblTarifa) And CDbl(Val(dblTarifa)) < 0 Then
'                    Me.strError += intCont & ". La tarifa no puede ser negativa."
'                    intCont += 1
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If IsNumeric(dblTarifa) And CDbl(Val(dblTarifa)) > 1 Then
'                    Me.strError += intCont & ". La tarifa no puede ser mayor a 1."
'                    intCont += 1
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If intExigeValorBase = EXIGE Then
'                    If dblValorBase = 0 Then
'                        Me.strError += intCont & ". Debe digitar el Valor Base."
'                        intCont += 1
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la página " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Datos_Requeridos = False
'            End Try

'        End Function

'        Private Function Borrar_SQL() As Boolean


'            Try
'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_plan_unico_cuentas", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Codigo").Value = Me.strCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                    If Me.bolAuditar Then
'                        Me.objControlAuditoria.Insertar_Control_Auditoria_SQL(ComandoSQL.Parameters, "Plan_Unico_Cuentas", ControlAuditoria.TRANSACCION_ELIMINAR, Me.strError)
'                    End If
'                Else
'                    Borrar_SQL = False
'                End If
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la página " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Borrar_SQL = False
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'#End Region
'    End Class
'End Namespace
