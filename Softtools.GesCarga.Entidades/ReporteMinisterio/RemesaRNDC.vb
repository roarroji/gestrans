﻿'Imports Microsoft.VisualBasic
'Imports System.Xml
'Imports Softtools.GesCarga.Entidades.ManifiestoElectronico

'Namespace Entidades.ReporteMinisterio
'    Public Class RemesaRNDC
'#Region "variables"
'        Public NUMNITEMPRESATRANSPORTE As String
'        Public CONSECUTIVOREMESA As String
'        Public CONSECUTIVOINFORMACIONCARGA As String
'        Public CONSECUTIVOCARGADIVIDIDA As String
'        Public CODOPERACIONTRANSPORTE As String
'        Public CODTIPOEMPAQUE As String
'        Public CODNATURALEZACARGA As String
'        Public DESCRIPCIONCORTAPRODUCTO As String
'        Public MERCANCIAREMESA As String
'        Public CANTIDADINFORMACIONCARGA As String
'        Public CANTIDADCARGADA As String
'        Public UNIDADMEDIDACAPACIDAD As String
'        Public PESOCONTENEDORVACIO As String
'        Public CODTIPOIDREMITENTE As String
'        Public NUMIDREMITENTE As String
'        Public CODSEDEREMITENTE As String
'        Public CODTIPOIDDESTINATARIO As String
'        Public NUMIDDESTINATARIO As String
'        Public CODSEDEDESTINATARIO As String
'        Public CODTIPOIDPROPIETARIO As String
'        Public NUMIDPROPIETARIO As String
'        Public CODSEDEPROPIETARIO As String
'        Public DUENOPOLIZA As String
'        Public NUMPOLIZATRANSPORTE As String
'        Public FECHAVENCIMIENTOPOLIZACARGA As String
'        Public COMPANIASEGURO As String
'        Public FECHACITAPACTADACARGUE As String
'        Public HORACITAPACTADACARGUE As String
'        Public FECHALLEGADACARGUE As String
'        Public HORALLEGADACARGUEREMESA As String
'        Public FECHAENTRADACARGUE As String
'        Public HORAENTRADACARGUEREMESA As String
'        Public FECHASALIDACARGUE As String
'        Public HORASALIDACARGUEREMESA As String
'        Public HORASPACTODESCARGUE As String
'        Public MINUTOSPACTODESCARGUE As String
'        Public FECHACITAPACTADADESCARGUE As String
'        Public HORACITAPACTADADESCARGUEREMESA As String
'        Public OBSERVACIONES As String

'        Public HORASPACTOCARGA As String
'        Public MINUTOSPACTOCARGA As String

'        ''CAMPOS CUMPLIDO
'        Public TIPOCUMPLIDOREMESA As String
'        Public MOTIVOSUSPENSIONREMESA As String
'        Public CANTIDADENTREGADA As String
'        Public HORACITAREALPACTADACARGUE As String
'        Public FECHALLEGADADESCARGUE As String
'        Public HORALLEGADADESCARGUECUMPLIDO As String
'        Public FECHAENTRADADESCARGUE As String
'        Public HORAENTRADADESCARGUECUMPLIDO As String
'        Public FECHASALIDADESCARGUE As String
'        Public HORASALIDADESCARGUECUMPLIDO As String
'        Public VALORREMESAFACTURA As String

'        'CAMPOS ANULACION
'        Public MOTIVOREVERSAREMESA As String
'        Public NUMMANIFIESTOCARGA As String
'        Public CODMUNICIPIOTRANSBORDO As String
'        Public MOTIVOANULACIONREMESA As String
'        Public MOTIVOTRANSBORDOREMESA As String
'        Public Const TIPO_REVERSA_REMESA_ANULACION As String = "A"
'        Public Const TIPO_REVERSA_REMESA_LIBERAR_MANIFIESTO As String = "L"

'        Public REMCIUDAD_DESTI_TRANSBORDO As String
'        Public REMMOTIVOTRANSBORDO As String
'        Public REMREMITENTE As String

'        Const NOMBRE_TAG As String = "variables"

'        Dim solicitud As Solicitud
'        Dim objGeneral As General
'        Dim acceso As Acceso
'        Dim wsManifiestoElectronico As ManifiestoElectronico.IBPMServicesservice
'        Dim strRespuesta As String
'#End Region

'#Region "Propiedades"
'        Public Property EnlaceMinisterio As String
'            Get
'                Return Me.wsManifiestoElectronico.Url
'            End Get
'            Set(value As String)
'                Me.wsManifiestoElectronico.Url = value
'            End Set
'        End Property
'#End Region

'#Region "Constantes"
'        Public Const NOMBRE_TAG_CONSULTA As String = "documento"
'        Public Const ERROR_CIUDAD_DESTINATARIO_IGUAL_CIUDAD_REMITENTE As String = "La ciudad del remitente no puede ser igual a la ciudad del destinatario. Solamente pueden ser iguales cuando la naturaleza"
'#End Region

'#Region "Constructor"
'        Sub New(ByVal acceso As Acceso, ByVal solicitud As Solicitud)
'            Me.objGeneral = New General
'            Me.solicitud = New Solicitud
'            Me.acceso = New Acceso
'            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
'            Me.solicitud = solicitud
'            Me.acceso = acceso
'        End Sub

'        Sub New()
'            Me.solicitud = New Solicitud
'            Me.acceso = New Acceso
'            Me.objGeneral = New General
'            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
'        End Sub
'#End Region

'#Region "Funciones Privadas"

'#End Region

'#Region "Funciones Publicas"
'        Public Sub Crear_XML_Remesa(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByRef lstVariablesDestinatario As List(Of String))
'            Try

'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)

'                Select Case TipoFormato
'                    Case Tipos_Procesos.FORMATO_INGRESO
'                        Dim txtNodo As XmlText
'                        If Me.NUMNITEMPRESATRANSPORTE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CONSECUTIVOREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CONSECUTIVOREMESA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CONSECUTIVOINFORMACIONCARGA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOINFORMACIONCARGA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CONSECUTIVOINFORMACIONCARGA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CONSECUTIVOCARGADIVIDIDA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOCARGADIVIDIDA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CONSECUTIVOCARGADIVIDIDA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODOPERACIONTRANSPORTE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODOPERACIONTRANSPORTE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODOPERACIONTRANSPORTE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODNATURALEZACARGA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODNATURALEZACARGA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODNATURALEZACARGA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CANTIDADCARGADA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CANTIDADCARGADA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CANTIDADCARGADA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.UNIDADMEDIDACAPACIDAD <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("UNIDADMEDIDACAPACIDAD")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.UNIDADMEDIDACAPACIDAD)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODTIPOEMPAQUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODTIPOEMPAQUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODTIPOEMPAQUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.PESOCONTENEDORVACIO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("PESOCONTENEDORVACIO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.PESOCONTENEDORVACIO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.MERCANCIAREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("MERCANCIAREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.MERCANCIAREMESA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.DESCRIPCIONCORTAPRODUCTO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("DESCRIPCIONCORTAPRODUCTO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.DESCRIPCIONCORTAPRODUCTO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODTIPOIDREMITENTE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODTIPOIDREMITENTE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODTIPOIDREMITENTE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.NUMIDREMITENTE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMIDREMITENTE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMIDREMITENTE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODSEDEREMITENTE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODSEDEREMITENTE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODSEDEREMITENTE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODTIPOIDDESTINATARIO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODTIPOIDDESTINATARIO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODTIPOIDDESTINATARIO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.NUMIDDESTINATARIO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMIDDESTINATARIO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMIDDESTINATARIO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODSEDEDESTINATARIO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODSEDEDESTINATARIO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODSEDEDESTINATARIO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.DUENOPOLIZA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("DUENOPOLIZA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.DUENOPOLIZA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.NUMPOLIZATRANSPORTE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMPOLIZATRANSPORTE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMPOLIZATRANSPORTE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.COMPANIASEGURO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("COMPANIASEGURO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.COMPANIASEGURO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHAVENCIMIENTOPOLIZACARGA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHAVENCIMIENTOPOLIZACARGA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHAVENCIMIENTOPOLIZACARGA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORASPACTOCARGA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORASPACTOCARGA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORASPACTOCARGA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.MINUTOSPACTOCARGA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("MINUTOSPACTOCARGA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.MINUTOSPACTOCARGA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORASPACTODESCARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORASPACTODESCARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORASPACTODESCARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.MINUTOSPACTODESCARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("MINUTOSPACTODESCARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.MINUTOSPACTODESCARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHALLEGADACARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHALLEGADACARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHALLEGADACARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORALLEGADACARGUEREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORALLEGADACARGUEREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORALLEGADACARGUEREMESA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHAENTRADACARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHAENTRADACARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHAENTRADACARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORAENTRADACARGUEREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORAENTRADACARGUEREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORAENTRADACARGUEREMESA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHASALIDACARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHASALIDACARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHASALIDACARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORASALIDACARGUEREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORASALIDACARGUEREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORASALIDACARGUEREMESA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODTIPOIDPROPIETARIO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODTIPOIDPROPIETARIO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODTIPOIDPROPIETARIO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.NUMIDPROPIETARIO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMIDPROPIETARIO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMIDPROPIETARIO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CODSEDEPROPIETARIO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CODSEDEPROPIETARIO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CODSEDEPROPIETARIO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        'If Me.NUMIDDESTINATARIO <> String.Empty Then
'                        '    NuevoElemento = XmlDocumento.CreateElement("NUMIDDESTINATARIO")
'                        '    txtNodo = XmlDocumento.CreateTextNode(Me.NUMIDDESTINATARIO)
'                        '    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        '    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        'End If

'                        If Me.FECHACITAPACTADACARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHACITAPACTADACARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHACITAPACTADACARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORACITAPACTADACARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORACITAPACTADACARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORACITAPACTADACARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHACITAPACTADADESCARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHACITAPACTADADESCARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHACITAPACTADADESCARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORACITAPACTADADESCARGUEREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORACITAPACTADADESCARGUEREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORACITAPACTADADESCARGUEREMESA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If
'                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
'                        Dim strAxuliarCampos As String = String.Empty
'                        For Each Items As String In lstVariablesDestinatario
'                            strAxuliarCampos &= Items
'                        Next
'                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("ingresoid" & strAxuliarCampos)
'                        xmlElementoRoot.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
'                        xmlElementoRoot.AppendChild(NuevoElemento)
'                        xmlElementoRoot = XmlDocumento.DocumentElement


'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOREMESA")
'                        txtNodo = XmlDocumento.CreateTextNode("'" & Me.CONSECUTIVOREMESA & "'")
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                End Select
'            Catch ex As Exception

'            End Try
'        End Sub

'        Public Sub Crear_XML_Cumplido_Remesa(ByRef XmlDocumento As XmlDocument, ByVal TipoProceso As Tipos_Procesos)
'            Try
'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)
'                Dim txtNodo As XmlText
'                Select Case TipoProceso
'                    Case Tipos_Procesos.FORMATO_INGRESO
'                        If Me.NUMNITEMPRESATRANSPORTE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CONSECUTIVOREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CONSECUTIVOREMESA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.NUMMANIFIESTOCARGA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMMANIFIESTOCARGA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMMANIFIESTOCARGA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.TIPOCUMPLIDOREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("TIPOCUMPLIDOREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.TIPOCUMPLIDOREMESA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.MOTIVOSUSPENSIONREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("MOTIVOSUSPENSIONREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.MOTIVOSUSPENSIONREMESA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CANTIDADCARGADA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CANTIDADCARGADA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CANTIDADCARGADA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.CANTIDADENTREGADA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CANTIDADENTREGADA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CANTIDADENTREGADA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.UNIDADMEDIDACAPACIDAD <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("UNIDADMEDIDACAPACIDAD")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.UNIDADMEDIDACAPACIDAD)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHALLEGADACARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHALLEGADACARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHALLEGADACARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORALLEGADACARGUEREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORALLEGADACARGUEREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORALLEGADACARGUEREMESA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHAENTRADACARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHAENTRADACARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHAENTRADACARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORAENTRADACARGUEREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORAENTRADACARGUEREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORAENTRADACARGUEREMESA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHASALIDACARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHASALIDACARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHASALIDACARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORASALIDACARGUEREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORASALIDACARGUEREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORASALIDACARGUEREMESA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHALLEGADADESCARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHALLEGADADESCARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHALLEGADADESCARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORALLEGADADESCARGUECUMPLIDO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORALLEGADADESCARGUECUMPLIDO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORALLEGADADESCARGUECUMPLIDO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHAENTRADADESCARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHAENTRADADESCARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHAENTRADADESCARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORAENTRADADESCARGUECUMPLIDO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORAENTRADADESCARGUECUMPLIDO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORAENTRADADESCARGUECUMPLIDO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.FECHASALIDADESCARGUE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("FECHASALIDADESCARGUE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.FECHASALIDADESCARGUE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.HORASALIDADESCARGUECUMPLIDO <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("HORASALIDADESCARGUECUMPLIDO")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.HORASALIDADESCARGUECUMPLIDO)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                        If Me.OBSERVACIONES <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("OBSERVACIONES")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.OBSERVACIONES)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If
'                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        txtNodo = XmlDocumento.CreateTextNode("INGRESOID")
'                        xmlElementoRoot.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
'                        xmlElementoRoot.AppendChild(NuevoElemento)
'                        xmlElementoRoot = XmlDocumento.DocumentElement


'                        If Me.NUMNITEMPRESATRANSPORTE <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If
'                        If Me.CONSECUTIVOREMESA <> String.Empty Then
'                            NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOREMESA")
'                            txtNodo = XmlDocumento.CreateTextNode("'" & Me.CONSECUTIVOREMESA & "'")
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If

'                End Select
'            Catch ex As Exception

'            End Try
'        End Sub

'        Public Sub Crear_XML_Anular_Remesa(ByRef XmlDocumento As XmlDocument)
'            Try
'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)

'                If Me.NUMNITEMPRESATRANSPORTE <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                    Dim txtNodo As XmlText = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'                If Me.CONSECUTIVOREMESA <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOREMESA")
'                    Dim txtNodo As XmlText = XmlDocumento.CreateTextNode(Me.CONSECUTIVOREMESA)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'                If Me.MOTIVOANULACIONREMESA <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("MOTIVOANULACIONREMESA")
'                    Dim txtNodo As XmlText = XmlDocumento.CreateTextNode(Me.MOTIVOANULACIONREMESA)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'                If Me.MOTIVOANULACIONREMESA <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("MOTIVOREVERSAREMESA")
'                    Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("A")
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'                If Me.OBSERVACIONES <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("OBSERVACIONES")
'                    Dim txtNodo As XmlText = XmlDocumento.CreateTextNode(Me.OBSERVACIONES)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'            Catch ex As Exception

'            End Try
'        End Sub
'        Public Sub Crear_XML_Anular_Informacion_Carga(ByRef XmlDocumento As XmlDocument)
'            Try
'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)

'                If Me.NUMNITEMPRESATRANSPORTE <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                    Dim txtNodo As XmlText = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'                If Me.CONSECUTIVOREMESA <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("CONSECUTIVOINFORMACIONCARGA")
'                    Dim txtNodo As XmlText = XmlDocumento.CreateTextNode(Me.CONSECUTIVOREMESA)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'                NuevoElemento = XmlDocumento.CreateElement("MOTIVOANULACIONINFOCARGA")
'                Dim txtNodos As XmlText = XmlDocumento.CreateTextNode("D")
'                xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodos)


'                If Me.OBSERVACIONES <> String.Empty Then
'                    NuevoElemento = XmlDocumento.CreateElement("OBSERVACIONES")
'                    Dim txtNodo As XmlText = XmlDocumento.CreateTextNode(Me.OBSERVACIONES)
'                    xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                    xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                End If

'            Catch ex As Exception

'            End Try
'        End Sub
'        Public Function Reportar_Remesa(ByRef TextoXmlRemesa As String, ByVal TipoFormato As Tipos_Procesos, ByRef strMensajeErrorRemesa As String, ByRef lstVariablesDestinatario As List(Of String)) As Double
'            Try

'                Reportar_Remesa = General.CERO
'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)

'                acceso.Crear_XML_Acceso(xmlDoc)
'                solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_REMESA)
'                Me.Crear_XML_Remesa(xmlDoc, TipoFormato, lstVariablesDestinatario)
'                TextoXmlRemesa = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Reportar_Remesa = Obtener_Mensaje_XML(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, strMensajeErrorRemesa, lstVariablesDestinatario)

'            Catch ex As Exception
'                Reportar_Remesa = General.CERO
'                strMensajeErrorRemesa = ex.Message
'            End Try
'        End Function

'        Public Function Obtener_Mensaje_XML(ByVal strMensaje As String, ByVal TipoFormato As Tipos_Procesos, ByRef strErrores As String, ByRef lstVariables As List(Of String)) As Double
'            Try
'                Obtener_Mensaje_XML = General.CERO
'                Dim xmlRespuesta As New XmlDocument
'                xmlRespuesta.LoadXml(strMensaje)
'                Dim lstxmlObjetos As New List(Of XmlElement)
'                For Each Item As XmlNode In xmlRespuesta.DocumentElement
'                    lstxmlObjetos.Add(Item)
'                Next

'                Select Case TipoFormato
'                    Case Tipos_Procesos.FORMATO_INGRESO
'                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
'                            Obtener_Mensaje_XML = General.CERO
'                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
'                        End If
'                        If xmlRespuesta.GetElementsByTagName("ingresoid").Count > 0 Then
'                            Obtener_Mensaje_XML = Val(xmlRespuesta.GetElementsByTagName("ingresoid")(0).InnerText)
'                            strErrores = String.Empty
'                        End If
'                    Case Tipos_Procesos.FORTMATO_CONSULTA_PROCESOS
'                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
'                            Obtener_Mensaje_XML = General.CERO
'                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
'                        End If
'                        If xmlRespuesta.GetElementsByTagName("ingresoid").Count > 0 Then
'                            Obtener_Mensaje_XML = Val(xmlRespuesta.GetElementsByTagName("ingresoid")(0).InnerText)
'                            strErrores = String.Empty
'                            For i As Integer = 0 To lstVariables.Count - 1
'                                If xmlRespuesta.GetElementsByTagName(lstVariables(i)).Count > 0 Then
'                                    lstVariables(i) = xmlRespuesta.GetElementsByTagName(lstVariables(i))(0).InnerText
'                                Else
'                                    lstVariables(i) = String.Empty
'                                End If
'                            Next
'                        End If
'                End Select

'            Catch ex As Exception
'                strErrores = ex.Message
'            End Try
'        End Function

'        Public Function Reportar_Cumplido_Remesa(ByRef TextoXmlCumplidoRemesa As String, ByRef strError As String, ByVal TipoProceso As Tipos_Procesos) As Double
'            Try
'                solicitud.tipo = Solicitud.SOLICITUD_WS_MINISTERIO_TIPO_INSERTAR
'                solicitud.Procesoid = Solicitud.SOLICITUD_WS_MINISTERIO_PROCESO_CUMPLIR_REMESA

'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)
'                acceso.Crear_XML_Acceso(xmlDoc)
'                solicitud.Crear_XML_Solicitud(xmlDoc, TipoProceso, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_CUMPLIR_REMESA)
'                Me.Crear_XML_Cumplido_Remesa(xmlDoc, TipoProceso)
'                TextoXmlCumplidoRemesa = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Reportar_Cumplido_Remesa = Obtener_Mensaje_XML(wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), strError)

'            Catch ex As Exception
'                Reportar_Cumplido_Remesa = General.CERO
'                strError = ex.Message
'            End Try
'        End Function
'        Public Function Obtener_Mensaje_XML(ByVal strMensaje As String, ByRef strError As String) As Double
'            Try
'                Obtener_Mensaje_XML = General.CERO
'                Dim xmlRespuesta As New XmlDocument
'                xmlRespuesta.LoadXml(strMensaje)
'                Dim xmlElementoAuxiliar As XmlNodeList = xmlRespuesta.GetElementsByTagName("ErrorMSG")
'                If Not IsNothing(xmlElementoAuxiliar(General.CERO)) Then
'                    strError = xmlElementoAuxiliar(General.CERO).InnerText
'                    Obtener_Mensaje_XML = General.CERO
'                Else
'                    xmlElementoAuxiliar = xmlRespuesta.GetElementsByTagName("ingresoid")
'                    If Not IsNothing(xmlElementoAuxiliar(General.CERO)) Then
'                        strError = String.Empty
'                        Obtener_Mensaje_XML = Val(xmlElementoAuxiliar(General.CERO).InnerText)
'                    End If
'                End If

'            Catch ex As Exception
'                strError = ex.Message
'            End Try
'        End Function
'        Public Function Reportar_Anulacion_Remesa(ByRef TextoXmlRemesaAnulada As String, ByRef strMensaje As String) As Double
'            Try
'                solicitud.tipo = Solicitud.SOLICITUD_WS_MINISTERIO_TIPO_INSERTAR
'                solicitud.Procesoid = Solicitud.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_REMESA

'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)

'                acceso.Crear_XML_Acceso(xmlDoc)
'                solicitud.Crear_XML_Solicitud(xmlDoc, Tipos_Procesos.FORMATO_INGRESO, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_REMESA)
'                Me.Crear_XML_Anular_Remesa(xmlDoc)
'                TextoXmlRemesaAnulada = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Reportar_Anulacion_Remesa = Obtener_Mensaje_XML(wsManifiestoElectronico.AtenderMensajeBPM(TextoXmlRemesaAnulada), strMensaje)

'            Catch ex As Exception
'                strMensaje = ex.Message
'                Reportar_Anulacion_Remesa = General.CERO
'            End Try
'        End Function

'        Public Function Reportar_Anulacion_Informacion_Carga(ByRef TextoXmlAnulacionCarga As String, ByRef strMensaje As String) As Double
'            Try
'                solicitud.tipo = Solicitud.SOLICITUD_WS_MINISTERIO_TIPO_INSERTAR
'                solicitud.Procesoid = Solicitud.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_REMESA

'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)

'                acceso.Crear_XML_Acceso(xmlDoc)
'                solicitud.Crear_XML_Solicitud(xmlDoc, Tipos_Procesos.FORMATO_INGRESO, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_ANULAR_INFO_CARGA)
'                Me.Crear_XML_Anular_Informacion_Carga(xmlDoc)
'                TextoXmlAnulacionCarga = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Reportar_Anulacion_Informacion_Carga = Obtener_Mensaje_XML(wsManifiestoElectronico.AtenderMensajeBPM(TextoXmlAnulacionCarga), strMensaje)
'            Catch ex As Exception
'                strMensaje = ex.Message
'                Reportar_Anulacion_Informacion_Carga = General.CERO
'            End Try
'        End Function

'#End Region
'    End Class
'End Namespace