﻿'Imports System.Data
'Imports System.Data.SqlClient

'Namespace Entidades.ReporteMinisterio
'    Public Class SitioCargueDescargue

'#Region "Declaracion Variables"

'        Dim strSQL As String
'        Dim objGeneral As General
'        Public strError As String
'        Public bolModificar As Boolean
'        Private objUsuario As Usuario

'        Private lonCodiPais As Long
'        Private objEmpresa As Empresas
'        Private lonCodigo As Long
'        Private objCataTipoIdentificacion As Catalogo
'        Private objCataTipoSitio As Catalogo
'        Private objCataHorarioAtencion As Catalogo

'        Private strNumeroIdentificacion As String
'        Private strNombre As String
'        Private strNombreContacto As String
'        Private strCargoContacto As String
'        Private objCiudad As Ciudad

'        Private strDireccion As String
'        Private strTelefono As String
'        Private strFax As String
'        Private strEmail As String
'        Private strCelular As String

'        Private intCargue As Integer
'        Private intDescargue As Integer
'        Private strObservaciones As String
'        Private intEstado As Integer
'        Private intCantidadOrdenesServicio As Integer
'#End Region

'#Region "Declaracion Constantes"
'        Public Const COLOMBIA As Byte = 1
'        Public Const SITIO_TIPO_CLIENTE As Byte = 1
'#End Region

'#Region "Constructor"

'        Sub New()

'            Me.objEmpresa = New Empresas(0)
'            Me.objCataTipoIdentificacion = New Catalogo(Me.objEmpresa)
'            Me.objCataTipoSitio = New Catalogo(Me.objEmpresa)
'            Me.lonCodiPais = 0
'            Me.intEstado = 0

'            Me.strNumeroIdentificacion = ""
'            Me.strNombre = ""
'            Me.strNombreContacto = ""
'            Me.strCargoContacto = ""
'            Me.objCiudad = New Ciudad(Me.objEmpresa)

'            Me.strDireccion = ""
'            Me.strTelefono = ""
'            Me.strFax = ""
'            Me.strEmail = ""
'            Me.strCelular = ""

'            Me.objCataHorarioAtencion = New Catalogo(Me.objEmpresa)
'            Me.intCargue = 0
'            Me.intDescargue = 0
'            Me.strObservaciones = ""

'            Me.objGeneral = New General()
'            Me.objUsuario = New Usuario(Me.objEmpresa)
'            Me.intCantidadOrdenesServicio = General.CERO
'        End Sub

'        Sub New(ByRef Empresa As Empresas)

'            Me.objEmpresa = Empresa
'            Me.objCataTipoIdentificacion = New Catalogo(Me.objEmpresa)
'            Me.objCataTipoSitio = New Catalogo(Me.objEmpresa)
'            Me.lonCodiPais = 0
'            Me.intEstado = 0

'            Me.strNumeroIdentificacion = ""
'            Me.strNombre = ""
'            Me.strNombreContacto = ""
'            Me.strCargoContacto = ""
'            Me.objCiudad = New Ciudad(Me.objEmpresa)

'            Me.strDireccion = ""
'            Me.strTelefono = ""
'            Me.strFax = ""
'            Me.strEmail = ""
'            Me.strCelular = ""

'            Me.objCataHorarioAtencion = New Catalogo(Me.objEmpresa)
'            Me.intCargue = 0
'            Me.intDescargue = 0
'            Me.strObservaciones = ""
'            Me.objGeneral = New General()

'            Me.objUsuario = New Usuario(Me.objEmpresa)
'            Me.intCantidadOrdenesServicio = General.CERO
'        End Sub

'        Sub New(ByRef Empresa As Empresas, ByVal Codigo As Integer)

'            Me.objEmpresa = Empresa
'            Me.objCataTipoIdentificacion = New Catalogo(Me.objEmpresa)
'            Me.objCataTipoSitio = New Catalogo(Me.objEmpresa)
'            Me.lonCodiPais = 0
'            Me.intEstado = 0

'            Me.strNumeroIdentificacion = ""
'            Me.strNombre = ""
'            Me.strNombreContacto = ""
'            Me.strCargoContacto = ""
'            Me.objCiudad = New Ciudad(Me.objEmpresa)

'            Me.strDireccion = ""
'            Me.strTelefono = ""
'            Me.strFax = ""
'            Me.strEmail = ""
'            Me.strCelular = ""

'            Me.objCataHorarioAtencion = New Catalogo(Me.objEmpresa)
'            Me.intCargue = 0
'            Me.intDescargue = 0
'            Me.strObservaciones = ""
'            Me.objGeneral = New General()

'            Me.objUsuario = New Usuario(Me.objEmpresa)
'            Me.intCantidadOrdenesServicio = General.CERO
'        End Sub

'#End Region

'#Region "Atributos"
'        Public Property Cargue() As Integer
'            Get
'                Return Me.intCargue
'            End Get
'            Set(ByVal value As Integer)
'                If value < 0 Then value = value * (-1)
'                Me.intCargue = value
'            End Set
'        End Property

'        Public Property Descargue() As Integer
'            Get
'                Return Me.intDescargue
'            End Get
'            Set(ByVal value As Integer)
'                If value < 0 Then value = value * (-1)
'                Me.intDescargue = value
'            End Set
'        End Property

'        Public Property CataHorarioAtencion() As Catalogo
'            Get
'                Return Me.objCataHorarioAtencion
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataHorarioAtencion = value
'            End Set
'        End Property

'        Public Property CataTipoIdentificacion() As Catalogo
'            Get
'                Return Me.objCataTipoIdentificacion
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoIdentificacion = value
'            End Set
'        End Property

'        Public Property CataTipoSitio() As Catalogo
'            Get
'                Return Me.objCataTipoSitio
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoSitio = value
'            End Set
'        End Property
'        Public Property Ciudad() As Ciudad
'            Get
'                Return Me.objCiudad
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudad = value
'            End Set
'        End Property

'        Public Property Usuario() As Usuario
'            Get
'                Return Me.objUsuario
'            End Get
'            Set(ByVal value As Usuario)
'                Me.objUsuario = value
'            End Set
'        End Property

'        Public Property Empresa() As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(ByVal value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property

'        Public Property CargoContacto() As String
'            Get
'                Return Me.strCargoContacto
'            End Get
'            Set(ByVal value As String)
'                Me.strCargoContacto = value
'            End Set
'        End Property

'        Public Property CodigoPais() As Long
'            Get
'                Return Me.lonCodiPais
'            End Get
'            Set(ByVal value As Long)
'                Me.lonCodiPais = value
'            End Set
'        End Property

'        Public Property Celular() As String
'            Get
'                Return Me.strCelular
'            End Get
'            Set(ByVal value As String)
'                Me.strCelular = value
'            End Set
'        End Property

'        Public Property Email() As String
'            Get
'                Return Me.strEmail
'            End Get
'            Set(ByVal value As String)
'                Me.strEmail = value
'            End Set
'        End Property

'        Public Property Fax() As String
'            Get
'                Return Me.strFax
'            End Get
'            Set(ByVal value As String)
'                Me.strFax = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'        Public Property NombreContacto() As String
'            Get
'                Return Me.strNombreContacto
'            End Get
'            Set(ByVal value As String)
'                Me.strNombreContacto = value
'            End Set
'        End Property

'        Public Property NumeroIdentifiacion() As String
'            Get
'                Return Me.strNumeroIdentificacion
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroIdentificacion = value
'            End Set
'        End Property

'        Public Property Direccion() As String
'            Get
'                Return Me.strDireccion
'            End Get
'            Set(ByVal value As String)
'                Me.strDireccion = value
'            End Set
'        End Property

'        Public Property Observaciones() As String
'            Get
'                Return Me.strObservaciones
'            End Get
'            Set(ByVal value As String)
'                Me.strObservaciones = value
'            End Set
'        End Property

'        Public Property Estado() As Integer
'            Get
'                Return Me.intEstado
'            End Get
'            Set(ByVal value As Integer)
'                Me.intEstado = value
'            End Set
'        End Property

'        Public Property Telefono() As String
'            Get
'                Return Me.strTelefono
'            End Get
'            Set(ByVal value As String)
'                Me.strTelefono = value
'            End Set
'        End Property

'        Public Property NumeroIdentificacion() As String
'            Get
'                Return Me.strNumeroIdentificacion
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroIdentificacion = value
'            End Set
'        End Property

'        Public Property Codigo() As Long
'            Get
'                Return Me.lonCodigo
'            End Get
'            Set(ByVal value As Long)
'                Me.lonCodigo = value
'            End Set
'        End Property

'        Public Property CantidadOrdenesServio As Integer
'            Get
'                Return Me.intCantidadOrdenesServicio
'            End Get
'            Set(value As Integer)
'                Me.intCantidadOrdenesServicio = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean

'            Try

'                Consultar = True

'                Dim sdrSitioCargueDescargue As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre, TIID_Codigo, TSCD_Codigo, PAIS_Codigo, Estado, Numero_Identificacion,"
'                strSQL += " Contacto, Cargo_Contacto, Direccion, Telefono, Fax, Celular, Email, HOAT_Codigo,"
'                strSQL += " Cargue, Descargue, Observaciones, CIUD_Codigo FROM Sitio_Cargue_Descargues"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrSitioCargueDescargue = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrSitioCargueDescargue.Read() Then
'                    Me.strNombre = sdrSitioCargueDescargue("Nombre").ToString()
'                    Me.objCataTipoIdentificacion.Campo1 = sdrSitioCargueDescargue("TIID_Codigo").ToString()
'                    Me.objCataTipoSitio.Campo1 = sdrSitioCargueDescargue("TSCD_Codigo").ToString()

'                    Me.lonCodiPais = sdrSitioCargueDescargue("PAIS_Codigo").ToString()
'                    Me.intEstado = sdrSitioCargueDescargue("Estado").ToString()
'                    Me.strNumeroIdentificacion = sdrSitioCargueDescargue("Numero_Identificacion").ToString()
'                    Me.strNombreContacto = sdrSitioCargueDescargue("Contacto").ToString()
'                    Me.strCargoContacto = sdrSitioCargueDescargue("Cargo_Contacto").ToString()

'                    Me.strDireccion = sdrSitioCargueDescargue("Direccion").ToString()
'                    Me.strTelefono = sdrSitioCargueDescargue("Telefono").ToString()
'                    Me.strFax = sdrSitioCargueDescargue("Fax").ToString()
'                    Me.strCelular = sdrSitioCargueDescargue("Celular").ToString()
'                    Me.strEmail = sdrSitioCargueDescargue("Email").ToString()

'                    Me.objCataHorarioAtencion.Campo1 = sdrSitioCargueDescargue("HOAT_Codigo").ToString()
'                    Me.intCargue = Val(sdrSitioCargueDescargue("Cargue").ToString())
'                    Me.intDescargue = Val(sdrSitioCargueDescargue("Descargue").ToString())
'                    Me.strObservaciones = sdrSitioCargueDescargue("Observaciones").ToString()
'                    Me.objCiudad.Existe_Registro(Me.objEmpresa, Val(sdrSitioCargueDescargue("CIUD_Codigo").ToString()))

'                    Consultar = True

'                Else
'                    Me.lonCodigo = 0
'                    Consultar = False
'                End If

'                sdrSitioCargueDescargue.Close()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)

'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Codigo As Integer) As Boolean

'            Try
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Dim sdrSitioCargueDescargue As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre, TIID_Codigo, TSCD_Codigo, PAIS_Codigo, Estado, Numero_Identificacion,"
'                strSQL += " Contacto, Cargo_Contacto, Direccion, Telefono, Fax, Celular, Email, HOAT_Codigo,"
'                strSQL += " Cargue, Descargue, Observaciones, CIUD_Codigo FROM Sitio_Cargue_Descargues"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrSitioCargueDescargue = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrSitioCargueDescargue.Read() Then
'                    Me.strNombre = sdrSitioCargueDescargue("Nombre").ToString()
'                    Me.objCataTipoIdentificacion.Campo1 = sdrSitioCargueDescargue("TIID_Codigo").ToString()
'                    Me.objCataTipoSitio.Campo1 = sdrSitioCargueDescargue("TSCD_Codigo").ToString()

'                    Me.lonCodiPais = sdrSitioCargueDescargue("PAIS_Codigo").ToString()
'                    Me.intEstado = sdrSitioCargueDescargue("Estado").ToString()
'                    Me.strNumeroIdentificacion = sdrSitioCargueDescargue("Numero_Identificacion").ToString()
'                    Me.strNombreContacto = sdrSitioCargueDescargue("Contacto").ToString()
'                    Me.strCargoContacto = sdrSitioCargueDescargue("Cargo_Contacto").ToString()

'                    Me.strDireccion = sdrSitioCargueDescargue("Direccion").ToString()
'                    Me.strTelefono = sdrSitioCargueDescargue("Telefono").ToString()
'                    Me.strFax = sdrSitioCargueDescargue("Fax").ToString()
'                    Me.strCelular = sdrSitioCargueDescargue("Celular").ToString()
'                    Me.strEmail = sdrSitioCargueDescargue("Email").ToString()

'                    Me.objCataHorarioAtencion.Consultar(sdrSitioCargueDescargue("HOAT_Codigo").ToString())
'                    Me.intCargue = Val(sdrSitioCargueDescargue("Cargue").ToString())
'                    Me.intDescargue = Val(sdrSitioCargueDescargue("Descargue").ToString())
'                    Me.strObservaciones = sdrSitioCargueDescargue("Observaciones").ToString()
'                    Me.objCiudad.Codigo = Val(sdrSitioCargueDescargue("CIUD_Codigo").ToString())
'                    Consultar = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar = False
'                End If

'                sdrSitioCargueDescargue.Close()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro() As Boolean

'            Try
'                Existe_Registro = True
'                Dim sdrSitioCargueDescargue As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo FROM Sitio_Cargue_Descargues"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrSitioCargueDescargue = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrSitioCargueDescargue.Read() Then
'                    Existe_Registro = True
'                Else
'                    Me.lonCodigo = 0
'                    Existe_Registro = False
'                End If

'                sdrSitioCargueDescargue.Close()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Existe_Registro = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Codigo As Integer) As Boolean
'            Try
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Dim sdrSitioCargueDescargue As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo FROM Sitio_Cargue_Descargues"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo


'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrSitioCargueDescargue = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrSitioCargueDescargue.Read() Then
'                    Existe_Registro = True
'                Else
'                    Me.lonCodigo = 0
'                    Existe_Registro = False
'                End If

'                sdrSitioCargueDescargue.Close()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Existe_Registro = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Guardar(Optional ByRef Mensaje As String = "") As Boolean

'            If Datos_Requeridos(Mensaje) Then
'                If Not bolModificar Then
'                    Guardar = Insertar_SQL()
'                Else
'                    Guardar = Modificar_SQL()
'                End If
'            Else
'                Guardar = False
'            End If
'            Mensaje = Me.strError
'        End Function

'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Eliminar = Borrar_SQL()
'            Mensaje = Me.strError
'        End Function

'        Public Function Guardar(ByRef General As General, Optional ByRef Mensaje As String = "") As Boolean
'            Dim bolGuardo As Boolean

'            If Datos_Requeridos(Mensaje) Then
'                If Not bolModificar Then
'                    bolGuardo = Insertar_SQL(General)
'                Else
'                    bolGuardo = Modificar_SQL(General)
'                End If
'            End If
'            Guardar = bolGuardo
'            Mensaje = Me.strError

'        End Function

'        Public Function CantidadOrdenesServicio() As Boolean
'            Try
'                CantidadOrdenesServicio = True

'                Dim dtsDatos As New DataSet
'                Dim dtaCantidadOrdenServicio As SqlDataAdapter

'                strSQL = "SELECT COUNT(*) as CantidadOrdenesServicio FROM Detalle_Contrato_Clientes "
'                strSQL &= " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL &= " AND (SICD_Descargue_Codigo = " & Me.lonCodigo & " OR SICD_Cargue_Codigo = " & Me.lonCodigo & ")"

'                dtaCantidadOrdenServicio = New SqlDataAdapter(strSQL, Me.objGeneral.CadenaDeConexionSQL)
'                dtaCantidadOrdenServicio.Fill(dtsDatos)

'                If dtsDatos.Tables.Count > General.CERO Then
'                    If dtsDatos.Tables(General.CERO).Rows.Count > General.CERO Then
'                        Integer.TryParse(dtsDatos.Tables(General.CERO).Rows(General.CERO).Item("CantidadOrdenesServicio"), Me.intCantidadOrdenesServicio)
'                        CantidadOrdenesServicio = True
'                    Else
'                        Me.intCantidadOrdenesServicio = General.CERO
'                        CantidadOrdenesServicio = False
'                    End If
'                Else
'                    Me.intCantidadOrdenesServicio = General.CERO
'                    CantidadOrdenesServicio = False
'                End If
'            Catch ex As Exception
'                Me.intCantidadOrdenesServicio = General.CERO
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                CantidadOrdenesServicio = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function


'#End Region

'#Region "Funciones Privadas"

'        Private Function Insertar_SQL() As Boolean

'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_sitio_cargue_descargues", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Direction = ParameterDirection.Output
'                ComandoSQL.Parameters.Add("@par_TIID_Codigo", SqlDbType.Char, 3) : ComandoSQL.Parameters("@par_TIID_Codigo").Value = Me.objCataTipoIdentificacion.Campo1
'                ComandoSQL.Parameters.Add("@par_Numero_Identificacion", SqlDbType.VarChar, 15) : ComandoSQL.Parameters("@par_Numero_Identificacion").Value = Me.strNumeroIdentificacion
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_Contacto", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Contacto").Value = Me.strNombreContacto

'                ComandoSQL.Parameters.Add("@par_TSCD_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_TSCD_Codigo").Value = Val(Me.objCataTipoSitio.Campo1)
'                ComandoSQL.Parameters.Add("@par_PAIS_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_PAIS_Codigo").Value = Val(Me.lonCodiPais)

'                ComandoSQL.Parameters.Add("@par_Cargo_Contacto", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Cargo_Contacto").Value = Me.strCargoContacto
'                ComandoSQL.Parameters.Add("@par_CIUD_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_CIUD_Codigo").Value = Me.objCiudad.Codigo
'                ComandoSQL.Parameters.Add("@par_Direccion", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Direccion").Value = Me.strDireccion
'                ComandoSQL.Parameters.Add("@par_Telefono", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Telefono").Value = Me.strTelefono
'                ComandoSQL.Parameters.Add("@par_Fax", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Fax").Value = Me.strFax

'                ComandoSQL.Parameters.Add("@par_Celular", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Celular").Value = Me.strCelular
'                ComandoSQL.Parameters.Add("@par_Email", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Email").Value = Me.strEmail
'                ComandoSQL.Parameters.Add("@par_HOAT_Codigo", SqlDbType.Decimal, 50) : ComandoSQL.Parameters("@par_HOAT_Codigo").Value = Me.objCataHorarioAtencion.Campo1
'                ComandoSQL.Parameters.Add("@par_Cargue", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Cargue").Value = Me.intCargue
'                ComandoSQL.Parameters.Add("@par_Descargue", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Descargue").Value = Me.intDescargue

'                ComandoSQL.Parameters.Add("@par_Observaciones", SqlDbType.VarChar, 1000) : ComandoSQL.Parameters("@par_Observaciones").Value = Me.strObservaciones
'                ComandoSQL.Parameters.Add("@par_USUA_Crea", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Crea").Value = Me.objUsuario.Codigo
'                ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Estado").Value = Me.intEstado


'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_SQL = True
'                    Me.lonCodigo = ComandoSQL.Parameters("@par_Codigo").Value
'                Else
'                    Insertar_SQL = False
'                End If

'            Catch ex As Exception
'                Insertar_SQL = False
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Private Function Insertar_SQL(ByRef General As General, Optional ByRef Mensaje As String = "") As Boolean

'            Try

'                Dim lonNumeRegi As Long

'                General.ComandoSQL.CommandText = "sp_insertar_sitio_cargue_descargues"
'                General.ComandoSQL.CommandType = CommandType.StoredProcedure
'                General.ComandoSQL.Parameters.Clear()

'                General.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : General.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                General.ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : General.ComandoSQL.Parameters("@par_Codigo").Direction = ParameterDirection.Output
'                General.ComandoSQL.Parameters.Add("@par_TIID_Codigo", SqlDbType.Char, 3) : General.ComandoSQL.Parameters("@par_TIID_Codigo").Value = Me.objCataTipoIdentificacion.Campo1
'                General.ComandoSQL.Parameters.Add("@par_Numero_Identificacion", SqlDbType.VarChar, 15) : General.ComandoSQL.Parameters("@par_Numero_Identificacion").Value = Me.strNumeroIdentificacion
'                General.ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 35) : General.ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                General.ComandoSQL.Parameters.Add("@par_Contacto", SqlDbType.VarChar, 35) : General.ComandoSQL.Parameters("@par_Contacto").Value = Me.strNombreContacto

'                General.ComandoSQL.Parameters.Add("@par_TSCD_Codigo", SqlDbType.Int) : General.ComandoSQL.Parameters("@par_TSCD_Codigo").Value = Val(Me.objCataTipoSitio.Campo1)
'                General.ComandoSQL.Parameters.Add("@par_PAIS_Codigo", SqlDbType.Int) : General.ComandoSQL.Parameters("@par_PAIS_Codigo").Value = Val(Me.lonCodiPais)

'                General.ComandoSQL.Parameters.Add("@par_Cargo_Contacto", SqlDbType.VarChar, 35) : General.ComandoSQL.Parameters("@par_Cargo_Contacto").Value = Me.strCargoContacto
'                General.ComandoSQL.Parameters.Add("@par_CIUD_Codigo", SqlDbType.Int) : General.ComandoSQL.Parameters("@par_CIUD_Codigo").Value = Me.objCiudad.Codigo
'                General.ComandoSQL.Parameters.Add("@par_Direccion", SqlDbType.VarChar, 100) : General.ComandoSQL.Parameters("@par_Direccion").Value = Me.strDireccion
'                General.ComandoSQL.Parameters.Add("@par_Telefono", SqlDbType.VarChar, 20) : General.ComandoSQL.Parameters("@par_Telefono").Value = Me.strTelefono
'                General.ComandoSQL.Parameters.Add("@par_Fax", SqlDbType.VarChar, 20) : General.ComandoSQL.Parameters("@par_Fax").Value = Me.strFax

'                General.ComandoSQL.Parameters.Add("@par_Celular", SqlDbType.VarChar, 20) : General.ComandoSQL.Parameters("@par_Celular").Value = Me.strCelular
'                General.ComandoSQL.Parameters.Add("@par_Email", SqlDbType.VarChar, 50) : General.ComandoSQL.Parameters("@par_Email").Value = Me.strEmail
'                General.ComandoSQL.Parameters.Add("@par_HOAT_Codigo", SqlDbType.Decimal) : General.ComandoSQL.Parameters("@par_HOAT_Codigo").Value = Me.objCataHorarioAtencion.Campo1
'                General.ComandoSQL.Parameters.Add("@par_Cargue", SqlDbType.SmallInt) : General.ComandoSQL.Parameters("@par_Cargue").Value = Me.intCargue
'                General.ComandoSQL.Parameters.Add("@par_Descargue", SqlDbType.SmallInt) : General.ComandoSQL.Parameters("@par_Descargue").Value = Me.intDescargue

'                General.ComandoSQL.Parameters.Add("@par_Observaciones", SqlDbType.VarChar, 1000) : General.ComandoSQL.Parameters("@par_Observaciones").Value = Me.strObservaciones
'                General.ComandoSQL.Parameters.Add("@par_USUA_Crea", SqlDbType.Char, 10) : General.ComandoSQL.Parameters("@par_USUA_Crea").Value = Me.objUsuario.Codigo
'                General.ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.SmallInt) : General.ComandoSQL.Parameters("@par_Estado").Value = Me.intEstado

'                lonNumeRegi = Val(General.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_SQL = True
'                    Me.lonCodigo = General.ComandoSQL.Parameters("@par_Codigo").Value
'                Else
'                    Insertar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Insertar_SQL = False
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Private Function Modificar_SQL(ByRef General As General, Optional ByRef Mensaje As String = "") As Boolean

'            Try

'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                General.ComandoSQL.CommandText = "sp_modificar_sitio_cargue_descargues"
'                General.ComandoSQL.CommandType = CommandType.StoredProcedure
'                General.ComandoSQL.Parameters.Clear()

'                General.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : General.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                General.ComandoSQL.Parameters.Add("@par_TIID_Codigo", SqlDbType.Char, 3) : General.ComandoSQL.Parameters("@par_TIID_Codigo").Value = Me.objCataTipoIdentificacion.Retorna_Codigo()
'                General.ComandoSQL.Parameters.Add("@par_Numero_Identificacion", SqlDbType.VarChar, 15) : General.ComandoSQL.Parameters("@par_Numero_Identificacion").Value = Me.strNumeroIdentificacion
'                General.ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 35) : General.ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                General.ComandoSQL.Parameters.Add("@par_Contacto", SqlDbType.VarChar, 35) : General.ComandoSQL.Parameters("@par_Contacto").Value = Me.strNombreContacto

'                General.ComandoSQL.Parameters.Add("@par_TSCD_Codigo", SqlDbType.Int) : General.ComandoSQL.Parameters("@par_TSCD_Codigo").Value = Me.objCataTipoSitio.Retorna_Codigo()
'                General.ComandoSQL.Parameters.Add("@par_PAIS_Codigo", SqlDbType.Int) : General.ComandoSQL.Parameters("@par_PAIS_Codigo").Value = Val(Me.lonCodiPais)

'                General.ComandoSQL.Parameters.Add("@par_Cargo_Contacto", SqlDbType.VarChar, 35) : General.ComandoSQL.Parameters("@par_Cargo_Contacto").Value = Me.strCargoContacto
'                General.ComandoSQL.Parameters.Add("@par_CIUD_Codigo", SqlDbType.Int) : General.ComandoSQL.Parameters("@par_CIUD_Codigo").Value = Me.objCiudad.Codigo
'                General.ComandoSQL.Parameters.Add("@par_Direccion", SqlDbType.VarChar, 100) : General.ComandoSQL.Parameters("@par_Direccion").Value = Me.strDireccion
'                General.ComandoSQL.Parameters.Add("@par_Telefono", SqlDbType.VarChar, 20) : General.ComandoSQL.Parameters("@par_Telefono").Value = Me.strTelefono
'                General.ComandoSQL.Parameters.Add("@par_Fax", SqlDbType.VarChar, 20) : General.ComandoSQL.Parameters("@par_Fax").Value = Me.strFax

'                General.ComandoSQL.Parameters.Add("@par_Celular", SqlDbType.VarChar, 20) : General.ComandoSQL.Parameters("@par_Celular").Value = Me.strCelular
'                General.ComandoSQL.Parameters.Add("@par_Email", SqlDbType.VarChar, 50) : General.ComandoSQL.Parameters("@par_Email").Value = Me.strEmail
'                General.ComandoSQL.Parameters.Add("@par_HOAT_Codigo", SqlDbType.VarChar, 50) : General.ComandoSQL.Parameters("@par_HOAT_Codigo").Value = Me.objCataHorarioAtencion.Retorna_Codigo
'                General.ComandoSQL.Parameters.Add("@par_Cargue", SqlDbType.VarChar, 50) : General.ComandoSQL.Parameters("@par_Cargue").Value = Me.intCargue
'                General.ComandoSQL.Parameters.Add("@par_Descargue", SqlDbType.VarChar, 50) : General.ComandoSQL.Parameters("@par_Descargue").Value = Me.intDescargue

'                General.ComandoSQL.Parameters.Add("@par_Observaciones", SqlDbType.VarChar, 1000) : General.ComandoSQL.Parameters("@par_Observaciones").Value = Me.strObservaciones
'                General.ComandoSQL.Parameters.Add("@par_USUA_Modifica", SqlDbType.Char, 10) : General.ComandoSQL.Parameters("@par_USUA_Modifica").Value = Me.objUsuario.Codigo
'                General.ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : General.ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo
'                General.ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.SmallInt) : General.ComandoSQL.Parameters("@par_Estado").Value = Me.intEstado



'                lonNumeRegi = Val(General.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Modificar_SQL = False
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Private Function Modificar_SQL() As Boolean

'            Try

'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_sitio_cargue_descargues", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure


'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_TIID_Codigo", SqlDbType.Char, 3) : ComandoSQL.Parameters("@par_TIID_Codigo").Value = Me.objCataTipoIdentificacion.Campo1
'                ComandoSQL.Parameters.Add("@par_Numero_Identificacion", SqlDbType.VarChar, 15) : ComandoSQL.Parameters("@par_Numero_Identificacion").Value = Me.strNumeroIdentificacion
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_Contacto", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Contacto").Value = Me.strNombreContacto

'                ComandoSQL.Parameters.Add("@par_TSCD_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_TSCD_Codigo").Value = Val(Me.objCataTipoSitio.Campo1)
'                ComandoSQL.Parameters.Add("@par_PAIS_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_PAIS_Codigo").Value = Val(Me.lonCodiPais)

'                ComandoSQL.Parameters.Add("@par_Cargo_Contacto", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Cargo_Contacto").Value = Me.strCargoContacto
'                ComandoSQL.Parameters.Add("@par_CIUD_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_CIUD_Codigo").Value = Val(Me.objCiudad.Codigo)
'                ComandoSQL.Parameters.Add("@par_Direccion", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Direccion").Value = Me.strDireccion
'                ComandoSQL.Parameters.Add("@par_Telefono", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Telefono").Value = Me.strTelefono
'                ComandoSQL.Parameters.Add("@par_Fax", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Fax").Value = Me.strFax

'                ComandoSQL.Parameters.Add("@par_Celular", SqlDbType.VarChar, 20) : ComandoSQL.Parameters("@par_Celular").Value = Me.strCelular
'                ComandoSQL.Parameters.Add("@par_Email", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Email").Value = Me.strEmail
'                ComandoSQL.Parameters.Add("@par_HOAT_Codigo", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_HOAT_Codigo").Value = Val(Me.objCataHorarioAtencion.Campo1)
'                ComandoSQL.Parameters.Add("@par_Cargue", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Cargue").Value = Me.intCargue
'                ComandoSQL.Parameters.Add("@par_Descargue", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Descargue").Value = Me.intDescargue

'                ComandoSQL.Parameters.Add("@par_Observaciones", SqlDbType.VarChar, 1000) : ComandoSQL.Parameters("@par_Observaciones").Value = Me.strObservaciones
'                ComandoSQL.Parameters.Add("@par_USUA_Modifica", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Modifica").Value = Me.objUsuario.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Estado").Value = Me.intEstado



'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Modificar_SQL = False
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Private Function Borrar_SQL() As Boolean

'            Try

'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_sitio_cargue_descargues", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                Else
'                    Borrar_SQL = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Borrar_SQL = False
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Private Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean

'            Try

'                Me.strError = ""
'                Dim intCont As Short
'                intCont = 0
'                Datos_Requeridos = True

'                If Me.objCataTipoIdentificacion.Campo1 = "" Then
'                    intCont += 1
'                    Me.strError = intCont & ". Debe seleccionar el tipo de identificación."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If
'                If Len(Trim(Me.strNumeroIdentificacion)) = 0 Then
'                    intCont += 1
'                    Me.strError = intCont & ". Debe digitar el número de identificación."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Me.strNombre.Trim) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar el nombre del sitio de cargue y descargue."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Me.strNombre.Trim) > 35 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El nombre del sitio de cargue y descargue no puede ser superior a 35 caracteres."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.strNombre.Trim <> "" And Len(Me.strNombre.Trim) < General.MINIMO_CUATRO_CARACTERES_INGRESO_NOMBRE Then
'                    intCont += 1
'                    strError += intCont & ". El nombre del sitio de cargue y descargue no puede tener menos de cuatro (4) carácteres"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.Ciudad.Codigo = General.CERO Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar una ciudad válida"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If


'                If Len(Me.strDireccion) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar la dirección del sitio de cargue y descargue"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Me.strDireccion) > 100 Then
'                    intCont += 1
'                    Me.strError += intCont & ". La dirección del sitio de cargue y descargue no puede ser superior a 100 caracteres"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If


'                If Len(Me.strTelefono) > 20 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El teléfono principal del sitio de cargue y descargue no puede ser superior a 20 caracteres"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Me.strNombreContacto.Trim) = General.CERO Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar  el nombre del contacto de cargue y descargue"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                Else
'                    If Len(Me.strNombreContacto) > 40 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El nombre del contacto en este sitio del sitio de cargue y descargue no puede ser superior a 40 caracteres"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If

'                If Len(Me.strCargoContacto.Trim) = General.CERO Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe digitar el cargo del contacto  de cargue y descargue0"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.intCargue = 0 And Me.intDescargue = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Seleccione sitio de cargue y/o descargue"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Trim(Me.strEmail)) > 0 Then
'                    If Not Me.objGeneral.Email_Valido(Me.strEmail) Then
'                        intCont += 1
'                        Me.strError += intCont & ". El Email ingresado no es válido"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos = False
'                    End If
'                End If



'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Datos_Requeridos = False
'            End Try

'        End Function

'#End Region
'    End Class
'End Namespace