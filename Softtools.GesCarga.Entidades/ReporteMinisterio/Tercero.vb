﻿'Imports Microsoft.VisualBasic
'Imports System.Data
'Imports System.Data.SqlClient
'Imports System
'Namespace Entidades.ReporteMinisterio
'    Public Class Tercero
'#Region "Declaracion Variables"

'        Dim strSQL As String
'        Dim objGeneral As General
'        Dim objEmpresa As Empresas

'        Public strError As String
'        Public bolModificar As Boolean
'        Public bolEliminar As Boolean
'        Public bolFoto As Boolean
'        Public isConductor As Boolean
'        Public isEmpleado As Boolean
'        Public isCliente As Boolean
'        Public isTenedor As Boolean
'        Public isPropietario As Boolean
'        Public isProspectoCliente As Boolean
'        Public isAseguradora As Boolean
'        Public isProspecto As Boolean
'        Public GuardaRemitenteDestinatario As Boolean
'        Public bolHuella_Digital As Boolean
'        Public bolTipoTercero As Boolean

'        Private lonCodigo As Long
'        Private objCataTipoNaturaleza As Catalogo
'        Private objCataTipoIdentificacion As Catalogo
'        Private strNumeroIdentificacion As String
'        Private objCiudadExpedicionIdentificacion As Ciudad
'        Private strCodigoPostal As String

'        Private intDigitoChequeo As Byte
'        Private strNombre As String
'        Private strApellido1 As String
'        Private strApellido2 As String
'        Private objCiudadNacimiento As Ciudad

'        Private dteFechaNacimiento As Date
'        Private objCataEstadoCivil As Catalogo
'        Private objCataSexo As Catalogo
'        Private strRepresentanteLegal As String
'        Private objCiudad As Ciudad

'        Private strDireccion As String
'        Private strTelefono1 As String
'        Private strTelefono2 As String
'        Private strFax As String
'        Private strCelular As String

'        Private strEmail As String
'        Private strPaginaWeb As String
'        Private objCataRegimen As Catalogo
'        Private strAdministracionImpuestos As String
'        Private intAutoretenedor As Byte

'        Private objCataTipoSangre As Catalogo
'        Private strNumeroLicencia As String
'        Private intCategoriaLicencia As Byte
'        Private dteFechaLicencia As Date
'        Private intAfiliadoEPS As Byte

'        Private objEPS As Tercero
'        Private dteFechaAfiliacionEPS As Date
'        Private intAfiliadoARP As Byte
'        Private objARP As Tercero
'        Private dteFechaAfiliacionARP As Date

'        Private intAfiliadoFondoPensiones As Byte
'        Private objFondoPensiones As Tercero
'        Private dteFechaAfiliacionFondoPensiones As Date
'        Private intAfiliadoFondoCesantias As Byte
'        Private objFondoCesantias As Tercero

'        Private dteFechaAfiliacionFondoCesantias As Date
'        Private objCataTipoContrato As Catalogo
'        Private dteFechaContrato As Date
'        Private dteFechaVenceContrato As Date
'        Private dblSalarioBasico As Double

'        Private intConductorPropio As Byte
'        Private objBancoTransferencia As Banco
'        Private strNumeroCuentaTransferencia As String
'        Private objCataTipoCuentaTransferencia As Catalogo
'        Private objBeneficiarioTransferencia As Tercero

'        Private strCodigoTransferencia As String
'        Private objCataEstado As Catalogo
'        Private bytFoto As Byte()
'        Private strNombreConyuge As String
'        Private intNumeroHijos As Byte
'        Private bytHuella_Digital As Byte()

'        Private objCataPeriodoLiquidacion As Catalogo
'        Private objCataGrupoLiquidacion As Catalogo
'        Private strObservaciones As String
'        Private objOficinaEmpleado As Oficina
'        Private objCataCargo As Catalogo

'        Private objCataCausaBloqueo As Catalogo
'        Private dblPorcentajeComision As Double
'        Private objCataDiaPago As Catalogo
'        Private objRepresentanteComercial As Tercero
'        Private objSitioCargueDescargue As SitioCargueDescargue

'        Private objCataFormaPago As Catalogo
'        Private intDiasPlazo As Integer

'        Private objUsuario As Usuario
'        Private strJustificacionBloqueo As String
'        Private intEsRepresentanteComercial As Byte
'        Private objValidacion As Validacion
'        Private objEstudioSeguridad As EstudioSeguiridadVehiculos

'        Private dblMargenRentabilidadDespachoNacionalVehiculoPropio As Double
'        Private dblMargenRentabilidadDespachoUrbanoVehiculoPropio As Double
'        Private dblMargenRentabilidadDespachoNacionalVehiculoTercero As Double
'        Private dblMargenRentabilidadDespachoUrbanoVehiculoTercero As Double
'        Private intAplicaReteIca As Short

'        Private objCataTarifarioRepreComercial As Catalogo
'        Private arrPerfilTercero(15) As Integer
'        Private arrVerificacionCliente(15) As Integer
'        Private arrDocumentoEntregados(15) As Integer
'        Private objCataCoordenadaUno As Catalogo
'        Private objCataCoordenadaDos As Catalogo
'        Private intCoordenadaUnoValor As Integer
'        Private intCoordenadaDosValor As Integer
'        Private intZonaEspecial As Integer

'        Private intEsFilial As Integer
'        Private strCodigoFilial As String
'        Private strCodigoContableFilial As String
'        Private strCodigoActividad As String

'        'Seguros
'        Private intTieneSeguro As Byte
'        Private strAseguradoraCliente As String
'        Private dteFechaVencimientoSeguro As Date
'        Private objCataTipoCoberturaSeguro As Catalogo
'        Private dblMontoMaximoSeguro As Double

'        Private objCataGarantiaSeguroCliente As Catalogo
'        'Nuevos campos Condiciones Comerciales
'        Private intDiasBloqueoMora As Integer
'        Private objCataPeriodicidadFacturacionCliente As Catalogo
'        Private intClienteBASC As Byte
'        Private strCondicionesSeguro As String
'        Private intAntiguedadVehiculo As Byte
'        Private intClienteProspecto As Byte
'        Private strReferencioProspecto As String
'        Private strReferenciasVerificadas As String
'        Private objAseguradora As Tercero

'        Public strPostfijo As String
'        Public bolEsEmpleado As Boolean

'        Private intCon As Integer = 0
'        'Campo para registrar la fecha cuando se da de alta un cliente prospecto
'        Private dteFechaIngresoCliente As Date

'        Public bolGuardaPerfilCliente As Boolean
'        Public bolGuardarPerfilDestinatario As Boolean
'        Public bolDesmarcaPerfilEmpleado As Boolean
'        Private intClienteExterior As Integer
'        Private intAplicaReteFuente As Short
'        Private dteFechaUltimoCargue As Date

'        Private objCataCategoriaLicenciaConductor As Catalogo
'        Private objCataTipoIdentificacionTitular As Catalogo
'        Private strNombreTitular As String
'        Private intDigitoChequeoTitular As Short
'        Private strNumeroIdentificacionTitular As String
'        Private intConsultaListaClinton As Byte
'        Private strRazonSocial As String
'        Private intFacturaFaltantes As Byte
'        Private objControlAuditoria As ControlAuditoria
'        Public bolAuditar As Boolean
'        Public strNombrePerfilDatosRequeridos As String
'        Private dteFechNaciCony As Date
'        Private strTeleCony As String
'        Private intIansCodigo As Integer
'        Private objCataRangoIdentTerc As Catalogo
'        Private lonRangInicIdenTerc As Long
'        Private lonRangFinaIdenTerc As Long
'        Private strTipoIden As String

'        Private dblDescuentoFlete As Double
'        Private dblDescuentoManejo As Double
'        Private dblDescuentoMinimoUnidad As Double
'        Private dblDescuentoUrgencia As Double
'        Private dblDescuentoKgAdicional As Double
'        Private dblDescuentoDocumentoRetorno As Double

'        Private dblMinimoKilosCobrar As Double
'        Private dblPorcentajeSeguro As Double
'        Private intPais As Integer
'        Private intAplicReteIva As Integer

'#End Region

'#Region "Declaracion Constantes"
'        Public Const PERFIL_CLIENTE As Byte = 1
'        Public Const PERFIL_EMPRESA_AFILIADORA As Byte = 2
'        Public Const PERFIL_PROPIETARIO As Byte = 3
'        Public Const PERFIL_TENEDOR As Byte = 4
'        Public Const PERFIL_CONDUCTOR As Byte = 5

'        Public Const CODIGO_EMPRESA_PRINCIPAL As Byte = 1
'        Public Const PERFIL_PROVEEDOR As Byte = 6
'        Public Const PERFIL_ASEGURADORA As Byte = 7
'        Public Const PERFIL_SEGURIDAD_SOCIAL As Byte = 8
'        Public Const PERFIL_EMPLEADO As Byte = 9

'        Public Const PERFIL_REMITENTE As Byte = 10
'        Public Const PERFIL_EMPRESA_TRANSPORTADORA As Byte = 11
'        Public Const PERFIL_DESTINATARIO As Byte = 12
'        Public Const REPRESENTANTE_COMERCIAL As Byte = 40

'        Public Const TIPO_CONTRATO_TERMINO_FIJO As Byte = 1
'        Public Const TIPO_CONTRATO_TERMINO_INDEFINIDO As Byte = 0
'        Public Const BYTES_IMAGEN_DEFECTO As Byte = 0

'        Const ESTADO_ESTUDIO_SEGURIDAD_APROBADO As Byte = 1
'        Public Const AUTORRETENEDOR As Byte = 3
'        Public Const APLICA_RETE_ICA As Byte = 1
'        Public Const APLICA_RETE_FUENTE As Byte = 1
'        Public Const APLICA_RETE_IVA As Byte = 1

'        Public Const DESBLOQUEADO As Byte = 1
'        Public Const CAUSA_BLOQUEO_NUEVO_TERCERO As Byte = 24
'        Public Const JUSTIFICACION_BLOQUEO_NUEVO_TERCERO As String = "Nuevo Tercero"

'        Public Const MARGEN_RENTABILIDAD_POR_EMPRESA As Byte = 1
'        Public Const MARGEN_RENTABILIDAD_POR_CLIENTE As Byte = 2
'        Public Const CARGAR_INFORMACION_BASICA As Boolean = False
'        Public Const CARGAR_INFORMACION_COMPLETA As Boolean = True
'        Public Const ESTADO_ACTIVO As String = "1"
'        Public Const CAUSA_DESBLOQUEADO As String = "1"
'        Public Const CALLE_DIAGONAL As Byte = 3
'        Public Const CARRERA_AVENIDA_TRANSVERSAL As Byte = 2
'        Public Const TIENE_SEGURO As Byte = 1
'        Public Const NO_TIENE_SEGURO As Byte = 0
'        Public Const ES_FILIAL As Byte = 1

'        Public Const NATURALEZA_JURIDICA As Byte = 1
'        Public Const NATURALEZA_NATURAL As Byte = 2
'        Public Const IDENTIFICACION_NIT As String = "NIT"
'        Public Const IDENTIFICACION_CEDULA As String = "CC"

'        Public Const REGIMEN_AUTORETENEDOR As Byte = 3
'        Public Const REGIMEN_EXCENTO As Byte = 4

'        Public Const CODIGO_DHL_GLOBAL As Integer = 1001678
'        Public Const CLIENTE_EN_EL_EXTERIOR As Integer = 1
'        Public Const CODIGO_UNO As Integer = 1
'        Public Const INICIO_ACTIVIDAD_DIRECTA As String = "01"
'        Public Const INICIO_ACTIVIDAD_INDIRECTA As String = "02"

'        ' FECHA NULA DE SQL SERVER
'        Public Const FECHA_NULA As String = "01/01/1900"
'        ' Constantes de Validaciones
'        Private Const NOMBRE_CLASE As String = "Tercero"

'        Const VALIDA_FOTO_CLIENTE As String = "clsTercVali1"
'        Const VALIDA_ESTUDIO_SEGURIDAD_CONDUCTOR_PROPIO As String = "clsTercVali2"
'        Const VALIDA_ESTUDIO_SEGURIDAD_CONDUCTOR_TERCERO As String = "clsTercVali3"
'        Const VALIDA_ESTUDIO_SEGURIDAD_PROPIETARIO_TENEDOR As String = "clsTercVali4"
'        Const VALIDA_ZONIFICACION As String = "clsTercVali5"
'        Const VALIDA_HUELLA_DIGITAL_CLIENTE = "clsTercVali1"

'        Const VALIDA_DIAS_BLOQUEO_MORA As String = "clsTercVali6"
'        Const VALIDA_CODIGO_POSTAL As String = "clsTercVali10"

'        Const VALIDA_REFERENCIAS_VERIFICADAS_PROSPECTO_CLIENTE As String = "clsTercVali11"
'        Const VALIDA_ESTUDIO_SEGURIDAD_CONDUCTOR_ANTES_OTRO_PERFIL As String = "clsTercVali12"
'        Const VALIDA_CELULAR_POR_PERFIL As String = "clsTercVali13"
'        Const VALIDA_CIUDAD_LOCALIZACION As String = "clsTercVali14"
'        Const VALIDA_FOTO_CONDUCTOR As String = "clsTercVali15"
'        Const VALIDA_HUELLA_DIGITAL_CONDUCTOR As String = "clsTercVali15"

'        Const VALIDA_INGRESO_ACTIVIDAD_CLIENTE As String = "clsTercVali16"
'        Const VALIDA_FECHAS_VENCIDAS_CONDUCTOR As String = "clsTercVali17"
'        Const ACTUALIZAR_DESTINATARIO_EN_TERCEROS As String = "clsTercFunc1"
'        Const NO_EVALUAR_DIGITO_CHEQUEO As String = "clsTercVali16"

'#End Region

'#Region "Constructor"

'        Public Sub New()

'            Me.objGeneral = New General()
'            Me.objEmpresa = New Empresas(0)
'            Me.lonCodigo = 0
'            Me.objCataTipoNaturaleza = New Catalogo(Me.objEmpresa, "TINA")
'            Me.objCataTipoIdentificacion = New Catalogo(Me.objEmpresa, "TIID")

'            Me.strNumeroIdentificacion = ""
'            Me.objCiudadExpedicionIdentificacion = New Ciudad(Me.objEmpresa)
'            Me.intDigitoChequeo = 0
'            Me.strNombre = ""
'            Me.strApellido1 = ""

'            Me.strApellido2 = ""
'            Me.objCiudadNacimiento = New Ciudad(Me.objEmpresa)
'            Me.dteFechaNacimiento = Date.Today()
'            Me.objCataEstadoCivil = New Catalogo(Me.objEmpresa, "ESCI")
'            Me.objCataSexo = New Catalogo(Me.objEmpresa, "SEXO")

'            Me.strRepresentanteLegal = ""
'            Me.objCiudad = New Ciudad(Me.objEmpresa)
'            Me.strDireccion = ""
'            Me.strTelefono1 = ""
'            Me.strTelefono2 = ""

'            Me.strFax = ""
'            Me.strCelular = ""
'            Me.strEmail = ""
'            Me.strPaginaWeb = ""
'            Me.objCataRegimen = New Catalogo(Me.objEmpresa, "REGI")

'            Me.strAdministracionImpuestos = ""
'            Me.intAutoretenedor = 0
'            Me.objCataTipoSangre = New Catalogo(Me.objEmpresa, "GUSA")
'            Me.strNumeroLicencia = ""
'            Me.intCategoriaLicencia = 0

'            Me.dteFechaLicencia = Date.Today()
'            Me.intAfiliadoEPS = 0
'            Me.dteFechaAfiliacionEPS = Date.Today()
'            Me.intAfiliadoARP = 0
'            Me.dteFechaAfiliacionARP = Date.Today()

'            Me.intAfiliadoFondoPensiones = 0
'            Me.dteFechaAfiliacionFondoPensiones = Date.Today()
'            Me.intAfiliadoFondoCesantias = 0
'            Me.dteFechaAfiliacionFondoCesantias = Date.Today()
'            Me.objCataTipoContrato = New Catalogo(Me.objEmpresa, "TCOL")

'            Me.dteFechaContrato = Date.Today()
'            Me.dteFechaVenceContrato = Date.Today()
'            Me.dblSalarioBasico = 0
'            Me.intConductorPropio = 0
'            Me.objBancoTransferencia = New Banco(Me.objEmpresa)

'            Me.strNumeroCuentaTransferencia = ""
'            Me.objCataTipoCuentaTransferencia = New Catalogo(Me.objEmpresa, "TICB")
'            Me.strCodigoTransferencia = ""
'            Me.objCataEstado = New Catalogo(Me.objEmpresa, "ESTA")
'            Me.strNombreConyuge = ""

'            Me.intNumeroHijos = 0
'            Me.objCataPeriodoLiquidacion = New Catalogo(Me.objEmpresa, "PELI")
'            Me.objCataGrupoLiquidacion = New Catalogo(Me.objEmpresa, "GRLI")
'            Me.strObservaciones = ""
'            Me.objOficinaEmpleado = New Oficina(Me.objEmpresa)

'            Me.objCataCargo = New Catalogo(Me.objEmpresa, "CARG")
'            Me.objCataCausaBloqueo = New Catalogo(Me.objEmpresa, "CABL")
'            Me.objCataDiaPago = New Catalogo(Me.objEmpresa, "DISE")
'            Me.objSitioCargueDescargue = New SitioCargueDescargue(Me.objEmpresa)
'            Me.objUsuario = New Usuario(Me.objEmpresa)

'            Me.strJustificacionBloqueo = ""
'            Me.intEsRepresentanteComercial = 0

'            Me.objCataFormaPago = New Catalogo(Me.objEmpresa, "FPCO")
'            Me.intDiasPlazo = 0

'            Me.objValidacion = New Validacion(Me.objEmpresa)

'            Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio = 0
'            Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio = 0
'            Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero = 0
'            Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero = 0
'            Me.strPostfijo = String.Empty

'            Me.intAplicaReteIca = 0
'            Me.strCodigoPostal = String.Empty
'            Me.objCataCoordenadaUno = New Catalogo(Me.objEmpresa, "COOR")
'            Me.objCataCoordenadaDos = New Catalogo(Me.objEmpresa, "COOR")
'            Me.intCoordenadaUnoValor = 0

'            Me.intCoordenadaDosValor = 0
'            Me.intZonaEspecial = 0
'            Me.intEsFilial = 0
'            Me.strCodigoFilial = ""

'            Me.strCodigoContableFilial = ""

'            'Seguros
'            Me.intTieneSeguro = 0
'            Me.strAseguradoraCliente = String.Empty
'            Me.dteFechaVencimientoSeguro = Date.Today
'            Me.objCataTipoCoberturaSeguro = New Catalogo(Me.objEmpresa, "TCSC")
'            Me.dblMontoMaximoSeguro = 0

'            Me.objCataGarantiaSeguroCliente = New Catalogo(Me.objEmpresa, "GASC")
'            Me.intDiasBloqueoMora = 0
'            Me.objCataPeriodicidadFacturacionCliente = New Catalogo(Me.objEmpresa, "PEFC")

'            Me.intClienteBASC = 0
'            Me.strCondicionesSeguro = String.Empty
'            Me.intAntiguedadVehiculo = 0
'            Me.intClienteProspecto = 0
'            Me.strReferencioProspecto = String.Empty
'            Me.strReferenciasVerificadas = String.Empty

'            Me.dteFechaIngresoCliente = Date.Today
'            Me.intAplicaReteFuente = 0

'            Me.objCataTarifarioRepreComercial = New Catalogo(Me.objEmpresa, "TARC")
'            For Me.intCon = 0 To 15
'                Me.arrPerfilTercero(Me.intCon) = 0
'                Me.arrDocumentoEntregados(Me.intCon) = 0
'                Me.arrVerificacionCliente(Me.intCon) = 0
'            Next

'            Me.intClienteExterior = 0
'            Me.strCodigoActividad = ""
'            Me.dteFechaUltimoCargue = Date.MinValue
'            Me.objCataCategoriaLicenciaConductor = New Catalogo(Me.objEmpresa, "CALI")
'            Me.strNombreTitular = ""

'            Me.strNumeroIdentificacionTitular = ""
'            Me.objCataTipoIdentificacionTitular = New Catalogo(Me.objEmpresa, "TIID")
'            Me.intDigitoChequeoTitular = 0
'            Me.intConsultaListaClinton = 0
'            Me.intFacturaFaltantes = 0
'            Me.objControlAuditoria = New ControlAuditoria
'            Me.strNombrePerfilDatosRequeridos = ""
'            Me.strTeleCony = String.Empty
'            Me.dteFechNaciCony = Date.Parse("01/01/1900")
'            Me.intIansCodigo = 0
'            Me.objCataRangoIdentTerc = New Catalogo(Me.objEmpresa, "RAIT")
'            Me.lonRangInicIdenTerc = 0
'            Me.lonRangFinaIdenTerc = 0
'            Me.strTipoIden = ""

'            Me.dblDescuentoFlete = 0
'            Me.dblDescuentoManejo = 0
'            Me.dblDescuentoMinimoUnidad = 0
'            Me.dblDescuentoUrgencia = 0
'            Me.dblDescuentoKgAdicional = 0
'            Me.dblDescuentoDocumentoRetorno = 0

'            Me.dblMinimoKilosCobrar = 0
'            Me.dblPorcentajeSeguro = 0
'            Me.intPais = 0
'            Me.intAplicReteIva = 0
'        End Sub

'        Public Sub New(ByRef Empresa As Empresas)

'            Me.objGeneral = New General()

'            Me.objEmpresa = Empresa
'            Me.lonCodigo = 0
'            Me.objCataTipoNaturaleza = New Catalogo(Me.objEmpresa, "TINA")
'            Me.objCataTipoIdentificacion = New Catalogo(Me.objEmpresa, "TIID")
'            Me.strNumeroIdentificacion = ""

'            Me.objCiudadExpedicionIdentificacion = New Ciudad(Me.objEmpresa)
'            Me.intDigitoChequeo = 0
'            Me.strNombre = ""
'            Me.strApellido1 = ""
'            Me.strApellido2 = ""

'            Me.objCiudadNacimiento = New Ciudad(Me.objEmpresa)
'            Me.dteFechaNacimiento = Date.Today()
'            Me.objCataEstadoCivil = New Catalogo(Me.objEmpresa, "ESCI")
'            Me.objCataSexo = New Catalogo(Me.objEmpresa, "SEXO")
'            Me.strRepresentanteLegal = ""

'            Me.objCiudad = New Ciudad(Me.objEmpresa)
'            Me.strDireccion = ""
'            Me.strTelefono1 = ""
'            Me.strTelefono2 = ""
'            Me.strFax = ""

'            Me.strCelular = ""
'            Me.strEmail = ""
'            Me.strPaginaWeb = ""
'            Me.objCataRegimen = New Catalogo(Me.objEmpresa, "REGE")
'            Me.strAdministracionImpuestos = ""

'            Me.intAutoretenedor = 0
'            Me.objCataTipoSangre = New Catalogo(Me.objEmpresa, "GUSA")
'            Me.strNumeroLicencia = ""
'            Me.intCategoriaLicencia = 0
'            Me.dteFechaLicencia = Date.Today()

'            Me.intAfiliadoEPS = 0
'            Me.dteFechaAfiliacionEPS = Date.Today()
'            Me.intAfiliadoARP = 0
'            Me.dteFechaAfiliacionARP = Date.Today()
'            Me.intAfiliadoFondoPensiones = 0

'            Me.dteFechaAfiliacionFondoPensiones = Date.Today()
'            Me.intAfiliadoFondoCesantias = 0
'            Me.dteFechaAfiliacionFondoCesantias = Date.Today()
'            Me.objCataTipoContrato = New Catalogo(Me.objEmpresa, "TCOL")
'            Me.dteFechaContrato = Date.Today()

'            Me.dteFechaVenceContrato = Date.Today()
'            Me.dblSalarioBasico = 0
'            Me.intConductorPropio = 0
'            Me.objBancoTransferencia = New Banco(Me.objEmpresa)
'            Me.strNumeroCuentaTransferencia = ""

'            Me.objCataTipoCuentaTransferencia = New Catalogo(Me.objEmpresa, "TICB")
'            Me.strCodigoTransferencia = ""
'            Me.objCataEstado = New Catalogo(Me.objEmpresa, "ESTA")
'            Me.strNombreConyuge = ""
'            Me.intNumeroHijos = 0

'            Me.objCataPeriodoLiquidacion = New Catalogo(Me.objEmpresa, "PELI")
'            Me.objCataGrupoLiquidacion = New Catalogo(Me.objEmpresa, "GRLI")
'            Me.strObservaciones = ""
'            Me.objOficinaEmpleado = New Oficina(Me.objEmpresa)
'            Me.objCataCargo = New Catalogo(Me.objEmpresa, "CARG")

'            Me.objCataCausaBloqueo = New Catalogo(Me.objEmpresa, "CABL")
'            Me.objCataDiaPago = New Catalogo(Me.objEmpresa, "DISE")
'            Me.objSitioCargueDescargue = New SitioCargueDescargue(Me.objEmpresa)
'            Me.objUsuario = New Usuario(Me.objEmpresa)

'            Me.strJustificacionBloqueo = ""
'            Me.intEsRepresentanteComercial = 0

'            Me.objCataFormaPago = New Catalogo(Me.objEmpresa, "FPCO")
'            Me.intDiasPlazo = 0

'            Me.objValidacion = New Validacion(Me.objEmpresa)

'            Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio = 0
'            Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio = 0
'            Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero = 0
'            Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero = 0
'            Me.strPostfijo = String.Empty

'            Me.intAplicaReteIca = 0
'            Me.strCodigoPostal = String.Empty
'            Me.objCataCoordenadaUno = New Catalogo(Me.objEmpresa, "COOR")
'            Me.objCataCoordenadaDos = New Catalogo(Me.objEmpresa, "COOR")
'            Me.intCoordenadaUnoValor = 0

'            Me.intCoordenadaDosValor = 0
'            Me.intZonaEspecial = 0
'            Me.intEsFilial = 0
'            Me.strCodigoFilial = ""

'            Me.strCodigoContableFilial = ""
'            Me.intAplicaReteFuente = 0

'            'Seguros
'            Me.intTieneSeguro = 0
'            Me.strAseguradoraCliente = String.Empty
'            Me.dteFechaVencimientoSeguro = Date.Today
'            Me.objCataTipoCoberturaSeguro = New Catalogo(Me.objEmpresa, "TCSC")
'            Me.dblMontoMaximoSeguro = 0

'            Me.objCataGarantiaSeguroCliente = New Catalogo(Me.objEmpresa, "GASC")
'            Me.intDiasBloqueoMora = 0
'            Me.objCataPeriodicidadFacturacionCliente = New Catalogo(Me.objEmpresa, "PEFC")

'            Me.intClienteBASC = 0
'            Me.strCondicionesSeguro = String.Empty
'            Me.intAntiguedadVehiculo = 0
'            Me.intClienteProspecto = 0
'            Me.strReferencioProspecto = String.Empty
'            Me.strReferenciasVerificadas = String.Empty

'            Me.objCataTarifarioRepreComercial = New Catalogo(Me.objEmpresa, "TARC")
'            Me.dteFechaIngresoCliente = Date.Today

'            For Me.intCon = 0 To 15
'                Me.arrPerfilTercero(Me.intCon) = 0
'                Me.arrDocumentoEntregados(Me.intCon) = 0
'                Me.arrVerificacionCliente(Me.intCon) = 0
'            Next

'            Me.intClienteExterior = 0
'            Me.strCodigoActividad = ""
'            Me.dteFechaUltimoCargue = Date.MinValue

'            Me.objCataCategoriaLicenciaConductor = New Catalogo(Me.objEmpresa, "CALI")
'            Me.strNombreTitular = ""
'            Me.strNumeroIdentificacionTitular = ""
'            Me.intConsultaListaClinton = 0
'            Me.objCataTipoIdentificacionTitular = New Catalogo(Me.objEmpresa, "TIID")
'            Me.intDigitoChequeoTitular = 0

'            Me.intFacturaFaltantes = 0
'            Me.objControlAuditoria = New ControlAuditoria
'            Me.strNombrePerfilDatosRequeridos = ""
'            Me.strTeleCony = String.Empty
'            Me.dteFechNaciCony = General.FECHA_NULA
'            Me.intIansCodigo = 0
'            Me.objCataRangoIdentTerc = New Catalogo(Me.objEmpresa, "RAIT")
'            Me.lonRangInicIdenTerc = 0
'            Me.lonRangFinaIdenTerc = 0
'            Me.strTipoIden = ""

'            Me.dblDescuentoFlete = 0
'            Me.dblDescuentoManejo = 0
'            Me.dblDescuentoMinimoUnidad = 0
'            Me.dblDescuentoUrgencia = 0
'            Me.dblDescuentoKgAdicional = 0
'            Me.dblDescuentoDocumentoRetorno = 0

'            Me.dblMinimoKilosCobrar = 0
'            Me.dblPorcentajeSeguro = 0
'            Me.intPais = 0
'            Me.intAplicReteIva = 0
'        End Sub

'        Public Sub New(ByRef Empresa As Empresas, ByVal Codigo As Boolean)
'            Me.objGeneral = New General()
'            Me.objEmpresa = Empresa
'            Me.lonCodigo = 0
'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property ControlAuditoria() As ControlAuditoria
'            Get
'                Return Me.objControlAuditoria
'            End Get
'            Set(ByVal value As ControlAuditoria)
'                Me.objControlAuditoria = value
'            End Set
'        End Property

'        Public Property CataCategoriaLicenciaConductor() As Catalogo
'            Get
'                Return Me.objCataCategoriaLicenciaConductor
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataCategoriaLicenciaConductor = value
'            End Set
'        End Property

'        Public Property NumeroIdentificacionTitular() As String
'            Get
'                Return Me.strNumeroIdentificacionTitular
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroIdentificacionTitular = value
'            End Set
'        End Property

'        Public Property NombreTitular() As String
'            Get
'                Return Me.strNombreTitular
'            End Get
'            Set(ByVal value As String)
'                Me.strNombreTitular = value
'            End Set
'        End Property

'        Public Property AplicaReteFuente() As Short
'            Get
'                Return Me.intAplicaReteFuente
'            End Get
'            Set(ByVal value As Short)
'                Me.intAplicaReteFuente = Math.Abs(value)
'            End Set
'        End Property

'        Public Property DocumentoEntregados(ByVal Indice As Integer) As Integer
'            Get
'                Return Me.arrDocumentoEntregados(Indice)
'            End Get
'            Set(ByVal value As Integer)
'                Me.arrDocumentoEntregados(Indice) = value
'            End Set

'        End Property

'        Public Property PerfilTercero(ByVal Indice As Integer) As Integer
'            Get
'                Return Me.arrPerfilTercero(Indice)
'            End Get
'            Set(ByVal value As Integer)
'                Me.arrPerfilTercero(Indice) = value
'            End Set

'        End Property

'        Public Property VerificacionCliente(ByVal indice As Integer) As Integer
'            Get
'                Return Me.arrVerificacionCliente(indice)
'            End Get
'            Set(ByVal value As Integer)
'                Me.arrVerificacionCliente(indice) = value
'            End Set
'        End Property

'        Public Property Prospecto() As Boolean
'            Get
'                Return Me.isProspecto
'            End Get
'            Set(ByVal value As Boolean)
'                Me.isProspecto = value
'            End Set
'        End Property

'        Public Property CataTarifarioRepreComercial() As Catalogo
'            Get
'                Return Me.objCataTarifarioRepreComercial
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTarifarioRepreComercial = value
'            End Set
'        End Property

'        Public Property CataFormaPago() As Catalogo
'            Get
'                Return Me.objCataFormaPago
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataFormaPago = value
'            End Set
'        End Property

'        Public Property DiasPlazoPago() As Integer
'            Get
'                Return Me.intDiasPlazo
'            End Get
'            Set(ByVal value As Integer)
'                Me.intDiasPlazo = value
'            End Set
'        End Property

'        Public Property Aseguradora() As Tercero
'            Get
'                If IsNothing(Me.objAseguradora) Then
'                    Me.objAseguradora = New Tercero(Me.objEmpresa)
'                End If
'                Return Me.objAseguradora
'            End Get
'            Set(ByVal value As Tercero)
'                If IsNothing(Me.objAseguradora) Then
'                    Me.objAseguradora = New Tercero(Me.objEmpresa)
'                End If
'                Me.objAseguradora = value
'            End Set
'        End Property

'        Public Property CondicionesSeguro() As String
'            Get
'                Return Me.strCondicionesSeguro
'            End Get
'            Set(ByVal value As String)
'                Me.strCondicionesSeguro = value
'            End Set
'        End Property

'        Public Property ReferencioProspecto() As String
'            Get
'                Return Me.strReferencioProspecto
'            End Get
'            Set(ByVal value As String)
'                Me.strReferencioProspecto = value
'            End Set
'        End Property

'        Public Property ReferenciasVerificadas() As String
'            Get
'                Return Me.strReferenciasVerificadas
'            End Get
'            Set(ByVal value As String)
'                Me.strReferenciasVerificadas = value
'            End Set
'        End Property

'        Public Property ClienteBASC() As Short
'            Get
'                Return Math.Abs(Me.intClienteBASC)
'            End Get
'            Set(ByVal value As Short)
'                Me.intClienteBASC = Math.Abs(value)
'            End Set
'        End Property

'        Public Property AntiguedadVehiculo() As Short
'            Get
'                Return Me.intAntiguedadVehiculo
'            End Get
'            Set(ByVal value As Short)
'                Me.intAntiguedadVehiculo = value
'            End Set
'        End Property

'        Public Property ClienteProspecto() As Short
'            Get
'                Return Me.intClienteProspecto
'            End Get
'            Set(ByVal value As Short)
'                Me.intClienteProspecto = value
'            End Set
'        End Property

'        Public Property TieneSeguro() As Short
'            Get
'                Return Me.intTieneSeguro
'            End Get
'            Set(ByVal value As Short)
'                Me.intTieneSeguro = value
'            End Set
'        End Property

'        Public Property AseguradoraCliente() As String
'            Get
'                Return Me.strAseguradoraCliente
'            End Get
'            Set(ByVal value As String)
'                Me.strAseguradoraCliente = value
'            End Set
'        End Property

'        Public Property FechaVencimientoSeguro() As Date
'            Get
'                Return Me.dteFechaVencimientoSeguro
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaVencimientoSeguro = value
'            End Set
'        End Property


'        Public Property CataTipoCoberturaSeguro() As Catalogo
'            Get
'                Return Me.objCataTipoCoberturaSeguro
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoCoberturaSeguro = value
'            End Set
'        End Property

'        Public Property MontoMaximoSeguro() As Double
'            Get
'                Return Me.dblMontoMaximoSeguro
'            End Get
'            Set(ByVal value As Double)
'                Me.dblMontoMaximoSeguro = value
'            End Set
'        End Property


'        Public Property CataGarantiaSeguroCliente() As Catalogo
'            Get
'                Return Me.objCataGarantiaSeguroCliente
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataGarantiaSeguroCliente = value
'            End Set
'        End Property

'        Public Property DiasBloqueoMora() As Integer
'            Get
'                Return Me.intDiasBloqueoMora
'            End Get
'            Set(ByVal value As Integer)
'                Me.intDiasBloqueoMora = value
'            End Set
'        End Property


'        Public Property CataPeriodicidadFacturacionCliente() As Catalogo
'            Get
'                Return Me.objCataPeriodicidadFacturacionCliente
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataPeriodicidadFacturacionCliente = value
'            End Set
'        End Property


'        Public Property EstudioSeguridad() As EstudioSeguiridadVehiculos
'            Get
'                If IsNothing(Me.objEstudioSeguridad) Then
'                    Me.objEstudioSeguridad = New EstudioSeguiridadVehiculos(Me.objEmpresa)
'                End If
'                Return Me.objEstudioSeguridad
'            End Get
'            Set(ByVal value As EstudioSeguiridadVehiculos)
'                If IsNothing(Me.objEstudioSeguridad) Then
'                    Me.objEstudioSeguridad = New EstudioSeguiridadVehiculos(Me.objEmpresa)
'                End If
'                Me.objEstudioSeguridad = value
'            End Set
'        End Property

'        Public Property PorcentajeComision() As Double
'            Get
'                Return Me.dblPorcentajeComision
'            End Get
'            Set(ByVal value As Double)
'                Me.dblPorcentajeComision = value
'            End Set
'        End Property

'        Public Property SalarioBasico() As Double
'            Get
'                Return Me.dblSalarioBasico
'            End Get
'            Set(ByVal value As Double)
'                Me.dblSalarioBasico = value
'            End Set
'        End Property

'        Public Property FechaAfiliacionARP() As Date
'            Get
'                Return Me.dteFechaAfiliacionARP
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaAfiliacionARP = value
'            End Set
'        End Property

'        Public Property FechaAfiliacionEPS() As Date
'            Get
'                Return Me.dteFechaAfiliacionEPS
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaAfiliacionEPS = value
'            End Set
'        End Property

'        Public Property FechaAfiliacionFondoCesantias() As Date
'            Get
'                Return Me.dteFechaAfiliacionFondoCesantias
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaAfiliacionFondoCesantias = value
'            End Set
'        End Property

'        Public Property FechaAfiliacionFondoPenciones() As Date
'            Get
'                Return Me.dteFechaAfiliacionFondoPensiones
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaAfiliacionFondoPensiones = value
'            End Set
'        End Property

'        Public Property FechaContrato() As Date
'            Get
'                Return Me.dteFechaContrato
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaContrato = value
'            End Set
'        End Property

'        Public Property FechaLicencia() As Date
'            Get
'                Return Me.dteFechaLicencia
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaLicencia = value
'            End Set
'        End Property

'        Public Property FechaNacimiento() As Date
'            Get
'                Return Me.dteFechaNacimiento
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaNacimiento = value
'            End Set
'        End Property

'        Public Property FechaVenceContrato() As Date
'            Get
'                Return Me.dteFechaVenceContrato
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaVenceContrato = value
'            End Set
'        End Property

'        Public Property Foto() As Byte()
'            Get
'                Return Me.bytFoto
'            End Get
'            Set(ByVal value As Byte())
'                Me.bytFoto = value
'            End Set
'        End Property

'        Public Property Huella_Digital() As Byte()
'            Get
'                Return Me.bytHuella_Digital
'            End Get
'            Set(ByVal value As Byte())
'                Me.bytHuella_Digital = value
'            End Set
'        End Property

'        Public Property AplicaReteICA() As Short
'            Get
'                Return Me.intAplicaReteIca
'            End Get
'            Set(ByVal value As Short)
'                Me.intAplicaReteIca = Math.Abs(value)
'            End Set
'        End Property

'        Public Property AfiliadoARP() As Integer
'            Get
'                Return Me.intAfiliadoARP
'            End Get
'            Set(ByVal value As Integer)
'                Me.intAfiliadoARP = value
'            End Set
'        End Property

'        Public Property AfiliadoEPS() As Integer
'            Get
'                Return Me.intAfiliadoEPS
'            End Get
'            Set(ByVal value As Integer)
'                Me.intAfiliadoEPS = value
'            End Set
'        End Property

'        Public Property EsRepresentanteComercial() As Byte
'            Get
'                Return Math.Abs(Me.intEsRepresentanteComercial)
'            End Get
'            Set(ByVal value As Byte)
'                Me.intEsRepresentanteComercial = Math.Abs(value)
'            End Set
'        End Property

'        Public Property AfiliadoFondoPensiones() As Integer
'            Get
'                Return Me.intAfiliadoFondoPensiones
'            End Get
'            Set(ByVal value As Integer)
'                Me.intAfiliadoFondoPensiones = value
'            End Set
'        End Property

'        Public Property AfiliadoFondoCesantias() As Integer
'            Get
'                Return Me.intAfiliadoFondoCesantias
'            End Get
'            Set(ByVal value As Integer)
'                Me.intAfiliadoFondoCesantias = value
'            End Set
'        End Property

'        Public Property Autoretenedor() As Integer
'            Get
'                Return Me.intAutoretenedor
'            End Get
'            Set(ByVal value As Integer)
'                Me.intAutoretenedor = value
'            End Set
'        End Property

'        Public Property CategoriaLicencia() As Integer
'            Get
'                Return Me.intCategoriaLicencia
'            End Get
'            Set(ByVal value As Integer)
'                Me.intCategoriaLicencia = value
'            End Set
'        End Property

'        Public Property ConductorPropio() As Integer
'            Get
'                Return Me.intConductorPropio
'            End Get
'            Set(ByVal value As Integer)
'                Me.intConductorPropio = Math.Abs(value)
'            End Set
'        End Property

'        Public Property DigitoChequeo() As Integer
'            Get
'                Return Me.intDigitoChequeo
'            End Get
'            Set(ByVal value As Integer)
'                Me.intDigitoChequeo = value
'            End Set
'        End Property

'        Public Property NumeroHijos() As Integer
'            Get
'                Return Me.intNumeroHijos
'            End Get
'            Set(ByVal value As Integer)
'                Me.intNumeroHijos = value
'            End Set
'        End Property

'        Public Property Codigo() As Long
'            Get
'                Return Me.lonCodigo
'            End Get
'            Set(ByVal value As Long)
'                Me.lonCodigo = value
'            End Set
'        End Property


'        Public Property ARP() As Tercero
'            Get
'                If IsNothing(Me.objARP) Then
'                    Me.objARP = New Tercero(Me.objEmpresa)
'                End If
'                Return Me.objARP
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objARP = value
'            End Set
'        End Property


'        Public Property BancoTransferencia() As Banco
'            Get
'                Return Me.objBancoTransferencia
'            End Get
'            Set(ByVal value As Banco)
'                Me.objBancoTransferencia = value
'            End Set
'        End Property


'        Public Property BeneficiarioTransferencia() As Tercero
'            Get
'                Return Me.objBeneficiarioTransferencia
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objBeneficiarioTransferencia = value
'            End Set
'        End Property

'        Public Property CataTipoIdentificacionTitular() As Catalogo
'            Get
'                Return Me.objCataTipoIdentificacionTitular
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoIdentificacionTitular = value
'            End Set
'        End Property

'        Public Property DigitoChequeoTitular() As Short
'            Get
'                Return Me.intDigitoChequeoTitular
'            End Get
'            Set(ByVal value As Short)
'                Me.intDigitoChequeoTitular = value
'            End Set
'        End Property


'        Public Property OficinaEmpleado() As Oficina
'            Get
'                Return Me.objOficinaEmpleado
'            End Get
'            Set(ByVal value As Oficina)
'                Me.objOficinaEmpleado = value
'            End Set
'        End Property


'        Public Property CataEstado() As Catalogo
'            Get
'                Return Me.objCataEstado
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataEstado = value
'            End Set
'        End Property


'        Public Property CataEstadoCivil() As Catalogo
'            Get
'                Return Me.objCataEstadoCivil
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataEstadoCivil = value
'            End Set
'        End Property


'        Public Property CataRegimen() As Catalogo
'            Get
'                Return Me.objCataRegimen
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataRegimen = value
'            End Set
'        End Property


'        Public Property CataSexo() As Catalogo
'            Get
'                Return Me.objCataSexo
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataSexo = value
'            End Set
'        End Property


'        Public Property CataTipoContrato() As Catalogo
'            Get
'                Return Me.objCataTipoContrato
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoContrato = value
'            End Set
'        End Property


'        Public Property CataTipoCuentaTransferencia() As Catalogo
'            Get
'                Return Me.objCataTipoCuentaTransferencia
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoCuentaTransferencia = value
'            End Set
'        End Property


'        Public Property CataCargo() As Catalogo
'            Get
'                Return Me.objCataCargo
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataCargo = value
'            End Set
'        End Property


'        Public Property CataCausaBloqueo() As Catalogo
'            Get
'                Return Me.objCataCausaBloqueo
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataCausaBloqueo = value
'            End Set
'        End Property


'        Public Property CataDiaPago() As Catalogo
'            Get
'                Return Me.objCataDiaPago
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataDiaPago = value
'            End Set
'        End Property


'        Public Property CataTipoIdentificacion() As Catalogo
'            Get
'                Return Me.objCataTipoIdentificacion
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoIdentificacion = value
'            End Set
'        End Property


'        Public Property CataTipoNaturaleza() As Catalogo
'            Get
'                Return Me.objCataTipoNaturaleza
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoNaturaleza = value
'            End Set
'        End Property


'        Public Property CataPeriodoLiquidacion() As Catalogo
'            Get
'                Return Me.objCataPeriodoLiquidacion
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataPeriodoLiquidacion = value
'            End Set
'        End Property


'        Public Property CataGrupoLiquidacion() As Catalogo
'            Get
'                Return Me.objCataGrupoLiquidacion
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataGrupoLiquidacion = value
'            End Set
'        End Property


'        Public Property Ciudad() As Ciudad
'            Get
'                Return Me.objCiudad
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudad = value
'            End Set
'        End Property


'        Public Property CiudadExpedicionIdentificacion() As Ciudad
'            Get
'                Return Me.objCiudadExpedicionIdentificacion
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudadExpedicionIdentificacion = value
'            End Set
'        End Property


'        Public Property CiudadNacimiento() As Ciudad
'            Get
'                Return Me.objCiudadNacimiento
'            End Get
'            Set(ByVal value As Ciudad)
'                Me.objCiudadNacimiento = value
'            End Set
'        End Property


'        Public Property Empresa() As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(ByVal value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property


'        Public Property EPS() As Tercero
'            Get
'                If IsNothing(Me.objEPS) Then
'                    Me.objEPS = New Tercero(Me.objEmpresa)
'                End If
'                Return Me.objEPS
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objEPS = value
'            End Set
'        End Property


'        Public Property FondoCesantias() As Tercero
'            Get
'                If IsNothing(Me.objFondoCesantias) Then
'                    Me.objFondoCesantias = New Tercero(Me.objEmpresa)
'                End If
'                Return Me.objFondoCesantias
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objFondoCesantias = value
'            End Set
'        End Property


'        Public Property FondoPensiones() As Tercero
'            Get
'                If IsNothing(Me.objFondoPensiones) Then
'                    Me.objFondoPensiones = New Tercero(Me.objEmpresa)
'                End If
'                Return Me.objFondoPensiones
'            End Get
'            Set(ByVal value As Tercero)
'                Me.objFondoPensiones = value
'            End Set
'        End Property


'        Public Property RepresentanteComercial() As Tercero
'            Get
'                If IsNothing(Me.objRepresentanteComercial) Then
'                    Me.objRepresentanteComercial = New Tercero(Me.objEmpresa)
'                End If
'                Return Me.objRepresentanteComercial
'            End Get
'            Set(ByVal value As Tercero)
'                If IsNothing(Me.objRepresentanteComercial) Then
'                    Me.objRepresentanteComercial = New Tercero(Me.objEmpresa)
'                End If
'                Me.objRepresentanteComercial = value
'            End Set
'        End Property

'        Public Property AdministracionImpuestos() As String
'            Get
'                Return Me.strAdministracionImpuestos
'            End Get
'            Set(ByVal value As String)
'                Me.strAdministracionImpuestos = value
'            End Set
'        End Property

'        Public Property Apellido1() As String
'            Get
'                Return Me.strApellido1
'            End Get
'            Set(ByVal value As String)
'                Me.strApellido1 = value
'            End Set
'        End Property

'        Public Property Apellido2() As String
'            Get
'                Return Me.strApellido2
'            End Get
'            Set(ByVal value As String)
'                Me.strApellido2 = value
'            End Set
'        End Property

'        Public Property Celular() As String
'            Get
'                Return Me.strCelular
'            End Get
'            Set(ByVal value As String)
'                Me.strCelular = value
'            End Set
'        End Property

'        Public Property CodigoTransferencia() As String
'            Get
'                Return Me.strCodigoTransferencia
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigoTransferencia = value
'            End Set
'        End Property

'        Public Property Direccion() As String
'            Get
'                Return Me.strDireccion
'            End Get
'            Set(ByVal value As String)
'                Me.strDireccion = value
'            End Set
'        End Property

'        Public Property Email() As String
'            Get
'                Return Me.strEmail
'            End Get
'            Set(ByVal value As String)
'                Me.strEmail = value
'            End Set
'        End Property

'        Public Property Fax() As String
'            Get
'                Return Me.strFax
'            End Get
'            Set(ByVal value As String)
'                Me.strFax = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'        Public Property NumeroCuentaTransferencia() As String
'            Get
'                Return Me.strNumeroCuentaTransferencia
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroCuentaTransferencia = value
'            End Set
'        End Property

'        Public Property NumeroIdentificacion() As String
'            Get
'                Return Me.strNumeroIdentificacion
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroIdentificacion = value
'            End Set
'        End Property

'        Public Property NumeroLicencia() As String
'            Get
'                Return Me.strNumeroLicencia
'            End Get
'            Set(ByVal value As String)
'                Me.strNumeroLicencia = value
'            End Set
'        End Property

'        Public Property Observaciones() As String
'            Get
'                Return Me.strObservaciones
'            End Get
'            Set(ByVal value As String)
'                Me.strObservaciones = value
'            End Set
'        End Property

'        Public Property PaginaWeb() As String
'            Get
'                Return Me.strPaginaWeb
'            End Get
'            Set(ByVal value As String)
'                Me.strPaginaWeb = value
'            End Set
'        End Property

'        Public Property RepresentanteLegal() As String
'            Get
'                Return Me.strRepresentanteLegal
'            End Get
'            Set(ByVal value As String)
'                Me.strRepresentanteLegal = value
'            End Set
'        End Property

'        Public Property Telefono1() As String
'            Get
'                Return Me.strTelefono1
'            End Get
'            Set(ByVal value As String)
'                Me.strTelefono1 = value
'            End Set
'        End Property

'        Public Property Telefono2() As String
'            Get
'                Return Me.strTelefono2
'            End Get
'            Set(ByVal value As String)
'                Me.strTelefono2 = value
'            End Set
'        End Property


'        Public Property CataTipoSangre() As Catalogo
'            Get
'                Return Me.objCataTipoSangre
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoSangre = value
'            End Set
'        End Property

'        Public Property NombreConyuge() As String
'            Get
'                Return Me.strNombreConyuge
'            End Get
'            Set(ByVal value As String)
'                Me.strNombreConyuge = value
'            End Set
'        End Property


'        Public Property CodigoCargueDescargue() As SitioCargueDescargue
'            Get
'                Return Me.objSitioCargueDescargue
'            End Get
'            Set(ByVal value As SitioCargueDescargue)
'                Me.objSitioCargueDescargue = value
'            End Set
'        End Property


'        Public Property Usuario() As Usuario
'            Get
'                Return Me.objUsuario
'            End Get
'            Set(ByVal value As Usuario)
'                Me.objUsuario = value
'            End Set
'        End Property

'        Public Property JustificacionBloqueo() As String
'            Get
'                Return Me.strJustificacionBloqueo
'            End Get
'            Set(ByVal value As String)
'                Me.strJustificacionBloqueo = value
'            End Set
'        End Property

'        Public Property MargenRentabilidadDespachoNacionalVehiculoPropio() As Double
'            Get
'                Return Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio
'            End Get
'            Set(ByVal value As Double)
'                Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio = value
'            End Set
'        End Property

'        Public Property MargenRentabilidadDespachoUrbanoVehiculoPropio() As Double
'            Get
'                Return Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio
'            End Get
'            Set(ByVal value As Double)
'                Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio = value
'            End Set
'        End Property

'        Public Property MargenRentabilidadDespachoNacionalVehiculoTercero() As Double
'            Get
'                Return Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero
'            End Get
'            Set(ByVal value As Double)
'                Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero = value
'            End Set
'        End Property

'        Public Property MargenRentabilidadDespachoUrbanoVehiculoTercero() As Double
'            Get
'                Return Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero
'            End Get
'            Set(ByVal value As Double)
'                Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero = value
'            End Set
'        End Property

'        Public Property Postfijo() As String
'            Get
'                Return Me.strPostfijo
'            End Get
'            Set(ByVal value As String)
'                Me.strPostfijo = value
'            End Set
'        End Property

'        Public Property CodigoPostal() As String
'            Get
'                Return Me.strCodigoPostal
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigoPostal = value
'            End Set
'        End Property


'        Public Property CataCoordenadaUno() As Catalogo
'            Get
'                Return Me.objCataCoordenadaUno
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataCoordenadaUno = value
'            End Set
'        End Property


'        Public Property CataCoordenadaDos() As Catalogo
'            Get
'                Return Me.objCataCoordenadaDos
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataCoordenadaDos = value
'            End Set
'        End Property

'        Public Property CoordenadaUnoValor() As Integer
'            Get
'                Return Me.intCoordenadaUnoValor
'            End Get
'            Set(ByVal value As Integer)
'                intCoordenadaUnoValor = value
'            End Set
'        End Property

'        Public Property CoordenadaDosValor() As Integer
'            Get
'                Return Me.intCoordenadaDosValor
'            End Get
'            Set(ByVal value As Integer)
'                intCoordenadaDosValor = value
'            End Set
'        End Property

'        Public Property ZonaEspecial() As Integer
'            Get
'                Return Me.intZonaEspecial
'            End Get
'            Set(ByVal value As Integer)
'                intZonaEspecial = value
'            End Set
'        End Property

'        Public Property EsFilial() As Integer
'            Get
'                Return Me.intEsFilial
'            End Get
'            Set(ByVal value As Integer)
'                Me.intEsFilial = value
'            End Set
'        End Property

'        Public Property CodigoFilial() As String
'            Get
'                Return Me.strCodigoFilial
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigoFilial = value
'            End Set
'        End Property

'        Public Property CodigoContableFilial() As String
'            Get
'                Return Me.strCodigoContableFilial
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigoContableFilial = value
'            End Set
'        End Property

'        Public Property FechaIngresoCliente() As Date
'            Get
'                Return Me.dteFechaIngresoCliente
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaIngresoCliente = value
'            End Set
'        End Property

'        Public Property ClienteExterior() As Integer
'            Get
'                Return Me.intClienteExterior
'            End Get
'            Set(ByVal value As Integer)
'                Me.intClienteExterior = value
'            End Set
'        End Property

'        Public Property CodigoActividad() As String
'            Get
'                Return Me.strCodigoActividad
'            End Get
'            Set(ByVal value As String)
'                Me.strCodigoActividad = value
'            End Set
'        End Property

'        Public Property FechaUltimoCargue() As Date
'            Get
'                Return Me.dteFechaUltimoCargue
'            End Get
'            Set(ByVal value As Date)
'                Me.dteFechaUltimoCargue = value
'            End Set
'        End Property

'        Public Property ConsultaListaClinton() As Byte
'            Get
'                Return Me.intConsultaListaClinton
'            End Get
'            Set(ByVal value As Byte)
'                Me.intConsultaListaClinton = value
'            End Set
'        End Property

'        Public Property RazonSocial() As String
'            Get
'                Return Me.strRazonSocial

'            End Get
'            Set(ByVal value As String)
'                Me.strRazonSocial = value

'            End Set
'        End Property

'        Public Property FacturaFaltantes() As Byte
'            Get
'                Return Me.intFacturaFaltantes

'            End Get
'            Set(ByVal value As Byte)
'                Me.intFacturaFaltantes = value
'            End Set
'        End Property
'        Public Property FechaNacimientoConyuge As Date
'            Get
'                Return Me.dteFechNaciCony
'            End Get
'            Set(value As Date)
'                Me.dteFechNaciCony = value
'            End Set
'        End Property
'        Public Property TelefonoConyuge As String
'            Get
'                Return Me.strTeleCony
'            End Get
'            Set(value As String)
'                Me.strTeleCony = value
'            End Set
'        End Property

'        Public Property IansCodigo() As Integer
'            Get
'                Return Me.intIansCodigo
'            End Get
'            Set(ByVal value As Integer)
'                Me.intIansCodigo = value
'            End Set
'        End Property

'        Public Property CataRangIdentTerc() As Catalogo
'            Get
'                Return Me.objCataRangoIdentTerc
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataRangoIdentTerc = value
'            End Set
'        End Property


'        Public Property DescuentoFlete() As Double
'            Get
'                Return Me.dblDescuentoFlete
'            End Get
'            Set(value As Double)
'                Me.dblDescuentoFlete = value
'            End Set
'        End Property

'        Public Property DescuentoManejo() As Double
'            Get
'                Return Me.dblDescuentoManejo
'            End Get
'            Set(value As Double)
'                Me.dblDescuentoManejo = value
'            End Set
'        End Property

'        Public Property DescuentoMinimoUnidad() As Double
'            Get
'                Return Me.dblDescuentoMinimoUnidad
'            End Get
'            Set(value As Double)
'                Me.dblDescuentoMinimoUnidad = value
'            End Set
'        End Property

'        Public Property DescuentoUrgencia() As Double
'            Get
'                Return Me.dblDescuentoUrgencia
'            End Get
'            Set(value As Double)
'                Me.dblDescuentoUrgencia = value
'            End Set
'        End Property

'        Public Property DescuentoKgAdicional() As Double
'            Get
'                Return Me.dblDescuentoKgAdicional
'            End Get
'            Set(value As Double)
'                Me.dblDescuentoKgAdicional = value
'            End Set
'        End Property

'        Public Property DescuentoDocumentoRetorno() As Double
'            Get
'                Return Me.dblDescuentoDocumentoRetorno
'            End Get
'            Set(value As Double)
'                Me.dblDescuentoDocumentoRetorno = value
'            End Set
'        End Property

'        Public Property MinimoKilosCobrar() As Double
'            Get
'                Return Me.dblMinimoKilosCobrar
'            End Get
'            Set(value As Double)
'                Me.dblMinimoKilosCobrar = value
'            End Set
'        End Property

'        Public Property PorcentajeSeguro() As Double
'            Get
'                Return Me.dblPorcentajeSeguro
'            End Get
'            Set(value As Double)
'                Me.dblPorcentajeSeguro = value
'            End Set
'        End Property

'        Public Property CodigoPais() As Integer
'            Get
'                Return Me.intPais
'            End Get
'            Set(value As Integer)
'                Me.intPais = value
'            End Set
'        End Property

'        Public Property AplicaReteIva() As Integer
'            Get
'                Return Me.intAplicReteIva
'            End Get
'            Set(value As Integer)
'                Me.intAplicReteIva = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function getNombreCompleto() As String
'            getNombreCompleto = Me.strNombre & " " & Me.strApellido1 & " " & Me.strApellido2
'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Codigo As Long, Optional ByVal CargaCompleta As Boolean = True) As Boolean
'            Try
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Dim sdrTercero As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Codigo, TINA_Codigo, TIID_Codigo, Numero_Identificacion, "
'                strSQL += "CIUD_Expedicion_Identificacion, Digito_Chequeo, Nombre, Apellido1, Apellido2, "
'                strSQL += "CIUD_Codigo, CIUD_Nacimiento, Fecha_Nacimiento, ESCI_Codigo, Sexo, "
'                strSQL += "Representante_Legal, Direccion, Telefono1, Telefono2, Fax, "
'                strSQL += "Celular, Email, Pagina_Web, REGI_Codigo, Administracion_Impuestos, "
'                strSQL += "Autoretenedor, Tipo_Sangre, Numero_Licencia, Categoria_Licencia, Fecha_Licencia, "
'                strSQL += "Afiliado_EPS, TERC_EPS, Fecha_EPS, Afiliado_ARP, TERC_ARP, "
'                strSQL += "Fecha_ARP, Afiliado_Fondo_Pensiones, TERC_Fondo_Pensiones, Fecha_Fondo_Pensiones, Afiliado_Fondo_Cesantias, "
'                strSQL += "TERC_Fondo_Cesantias, Fecha_Fondo_Cesantias, TICT_Codigo, Fecha_Contrato, Fecha_Vence_Contrato, "
'                strSQL += "Salario_Basico, Conductor_Propio, BANC_Transfe, Numero_Cuenta_Transfe, Tipo_Cuenta_Transfe, "
'                strSQL += "TERC_Beneficiario_Transfe, Codigo_Transfe, Estado, Nombre_Conyuge, Numero_Hijos, "
'                strSQL += "PELI_Codigo, GRLI_Codigo, Observaciones, OFIC_EMPL_Codigo, CARG_Codigo, "
'                strSQL += "CABL_Codigo, Porcentaje_Comision, DIPA_Codigo, TERC_Representante_Comercial, Fecha_Crea, "
'                strSQL += "USUA_Crea, Fecha_Modifica, USUA_Modifica, OFIC_Codigo, Justificacion_Bloqueo, "
'                strSQL += "Representante_Comercial,ESSE_Numero, Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio, Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio, Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero,"
'                strSQL += " Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero, Postfijo, Aplica_Rete_ICA, Codigo_Postal, COOR_Uno_Codigo, "
'                strSQL += "Coordenada_Uno_Valor, COOR_Dos_Codigo, Coordenada_Dos_Valor, Zona_Especial, ZONA_Codigo, "
'                strSQL += "Dias_Bloqueo_Mora, PEFC_Codigo, Tiene_Seguro, Aseguradora_Cliente, Fecha_Vencimiento_Seguro, "
'                strSQL += "TCSC_Codigo, Monto_Maximo_Seguro, GASC_Codigo, Cliente_BASC, Condiciones_Seguro, "
'                strSQL += "Antiguedad_Vehiculo, Cliente_Prospecto, Referencio_Prospecto, Referencias_Verificadas, TERC_Aseguradora, "
'                strSQL += "TARC_Codigo, Es_Filial, Codigo_Filial, Codigo_Contable_Filial, Cliente_Exterior, Fecha_Ingreso_Cliente, "
'                strSQL += "'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END,  Aplica_Rete_Fuente, Codigo_Contable_Filial2, "
'                strSQL += "Fecha_Ultimo_Cargue,CALI_Codigo,Nombre_Titular,Numero_Identificacion_Titular, Consulta_Lista_Clinton, Factura_Faltantes, IANS_Codigo, TIID_Codigo_Titular, Digito_Chequeo_Titular, "
'                strSQL += "'Huella_Digital' = CASE WHEN Huella_Digital IS NULL THEN NULL ELSE 1 END"
'                strSQL += " ,Descuento_Flete, Descuento_Manejo, Descuento_Minimo_Unidad, Descuento_Urgencia, Descuento_Kg_Adicional, Descuento_Documento_Retorno, Minimo_kilos_Cobrar, Porcentaje_Seguro, PAIS_Codigo, Aplica_Rete_IVA"
'                strSQL += " FROM Terceros "
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Call Instanciar_Objetos_Alternos()
'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then
'                    Me.objCataTipoNaturaleza.Existe_Registro(Me.objEmpresa, sdrTercero("TINA_Codigo").ToString())
'                    Me.objCataTipoIdentificacion.Existe_Registro(Me.objEmpresa, sdrTercero("TIID_Codigo").ToString())
'                    Me.strNumeroIdentificacion = sdrTercero("Numero_Identificacion").ToString()
'                    Me.objCiudadExpedicionIdentificacion.Datos_Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Expedicion_Identificacion").ToString()))
'                    Integer.TryParse(sdrTercero("Digito_Chequeo").ToString(), Me.intDigitoChequeo)

'                    Me.strNombre = sdrTercero("Nombre").ToString()
'                    Me.strApellido1 = sdrTercero("Apellido1").ToString()
'                    Me.strApellido2 = sdrTercero("Apellido2").ToString()
'                    Me.objCiudadNacimiento.Datos_Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Nacimiento").ToString()))
'                    Date.TryParse(sdrTercero("Fecha_Nacimiento").ToString(), Me.dteFechaNacimiento)

'                    Me.objCataEstadoCivil.Existe_Registro(Me.objEmpresa, sdrTercero("ESCI_Codigo").ToString())
'                    Me.objCataSexo.Existe_Registro(Me.objEmpresa, sdrTercero("Sexo").ToString())
'                    Me.strRepresentanteLegal = sdrTercero("Representante_legal").ToString()
'                    Me.objCiudad.Datos_Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Codigo").ToString()))
'                    Me.strDireccion = sdrTercero("Direccion").ToString()

'                    Me.strTelefono1 = sdrTercero("Telefono1").ToString()
'                    Me.strTelefono2 = sdrTercero("telefono2").ToString()
'                    Me.strFax = sdrTercero("Fax").ToString()
'                    Me.strCelular = sdrTercero("Celular").ToString()
'                    Me.strEmail = sdrTercero("Email").ToString()

'                    Me.strPaginaWeb = sdrTercero("Pagina_Web").ToString()
'                    Me.objCataRegimen.Existe_Registro(Me.objEmpresa, Val(sdrTercero("REGI_Codigo").ToString()))
'                    Me.strAdministracionImpuestos = sdrTercero("Administracion_Impuestos").ToString()
'                    Integer.TryParse(sdrTercero("Autoretenedor").ToString(), Me.intAutoretenedor)
'                    Me.objCataTipoSangre.Existe_Registro(Me.objEmpresa, sdrTercero("Tipo_Sangre").ToString())

'                    Me.strNumeroLicencia = sdrTercero("Numero_Licencia").ToString()
'                    Integer.TryParse(sdrTercero("Categoria_Licencia").ToString(), Me.intCategoriaLicencia)
'                    Date.TryParse(sdrTercero("Fecha_Licencia").ToString(), Me.dteFechaLicencia)
'                    Me.strPostfijo = Trim(sdrTercero("Postfijo").ToString())
'                    Me.objCataEstado.Existe_Registro(sdrTercero("Estado").ToString())

'                    Me.intAplicaReteIca = Val(sdrTercero("Aplica_Rete_ICA"))
'                    Me.intAplicaReteFuente = Val(sdrTercero("Aplica_Rete_Fuente"))
'                    Integer.TryParse(sdrTercero("Conductor_Propio").ToString(), Me.intConductorPropio)
'                    Me.intClienteExterior = Val(sdrTercero("Cliente_Exterior"))
'                    Date.TryParse(sdrTercero("Fecha_Ultimo_Cargue").ToString(), Me.dteFechaUltimoCargue)

'                    Me.intEsFilial = Val(sdrTercero("Es_Filial").ToString())
'                    Me.strCodigoFilial = sdrTercero("Codigo_Filial").ToString()
'                    Me.strCodigoContableFilial = sdrTercero("Codigo_Contable_Filial").ToString()

'                    Me.objCataCategoriaLicenciaConductor.Campo1 = Val(sdrTercero("CALI_Codigo").ToString())
'                    Me.objCataTipoIdentificacionTitular.Existe_Registro(Me.objEmpresa, sdrTercero("TIID_Codigo_Titular").ToString())
'                    Integer.TryParse(sdrTercero("Digito_Chequeo_Titular").ToString(), Me.intDigitoChequeoTitular)
'                    Me.strNombreTitular = sdrTercero("Nombre_Titular").ToString()
'                    Me.strNumeroIdentificacionTitular = sdrTercero("Numero_Identificacion_Titular").ToString()
'                    Me.intConsultaListaClinton = Val(sdrTercero("Consulta_Lista_Clinton"))
'                    Me.intFacturaFaltantes = Val(sdrTercero("Factura_Faltantes"))
'                    Me.intIansCodigo = Val(sdrTercero("IANS_Codigo"))

'                    If CargaCompleta Then
'                        Integer.TryParse(sdrTercero("Afiliado_EPS").ToString(), Me.intAfiliadoEPS)
'                        If Not IsNothing(Me.objEPS) Then
'                            Me.objEPS.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_EPS").ToString()))
'                        End If
'                        Date.TryParse(sdrTercero("Fecha_EPS").ToString(), Me.dteFechaAfiliacionEPS)
'                        Integer.TryParse(sdrTercero("Afiliado_ARP").ToString(), Me.intAfiliadoARP)

'                        If Not IsNothing(Me.objARP) Then
'                            Me.objARP.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_ARP").ToString()))
'                        End If
'                        Date.TryParse(sdrTercero("Fecha_ARP").ToString(), Me.dteFechaAfiliacionARP)
'                        Integer.TryParse(sdrTercero("Afiliado_Fondo_Pensiones").ToString(), Me.intAfiliadoFondoPensiones)
'                        If Not IsNothing(Me.objFondoPensiones) Then
'                            Me.objFondoPensiones.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Fondo_Pensiones").ToString()))
'                        End If
'                        Date.TryParse(sdrTercero("Fecha_Fondo_Pensiones").ToString(), Me.dteFechaAfiliacionFondoPensiones)
'                        Integer.TryParse(sdrTercero("Afiliado_Fondo_Cesantias").ToString(), Me.intAfiliadoFondoCesantias)
'                        If Not IsNothing(Me.objFondoCesantias) Then
'                            Me.objFondoCesantias.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Fondo_Cesantias").ToString()))
'                        End If
'                        Date.TryParse(sdrTercero("Fecha_Fondo_Cesantias").ToString(), Me.dteFechaAfiliacionFondoCesantias)

'                        Me.objCataTipoContrato.Existe_Registro(Me.objEmpresa, sdrTercero("TICT_Codigo").ToString())
'                        Date.TryParse(sdrTercero("Fecha_Contrato").ToString(), Me.dteFechaContrato)
'                        Date.TryParse(sdrTercero("Fecha_Vence_Contrato").ToString(), Me.dteFechaVenceContrato)
'                        Double.TryParse(sdrTercero("Salario_Basico").ToString(), Me.dblSalarioBasico)

'                        Me.objBancoTransferencia.Codigo = Val(sdrTercero("BANC_Transfe").ToString())
'                        Me.strNumeroCuentaTransferencia = sdrTercero("Numero_Cuenta_Transfe").ToString()
'                        Me.objCataTipoCuentaTransferencia.Existe_Registro(sdrTercero("Tipo_Cuenta_Transfe").ToString())
'                        If Not IsNothing(Me.objBeneficiarioTransferencia) Then
'                            Me.objBeneficiarioTransferencia.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Beneficiario_Transfe").ToString()))
'                        End If
'                        Me.strCodigoTransferencia = sdrTercero("Codigo_Transfe").ToString()

'                        Me.strNombreConyuge = sdrTercero("Nombre_Conyuge").ToString()
'                        Integer.TryParse(sdrTercero("Numero_Hijos").ToString(), Me.intNumeroHijos)
'                        Me.objCataPeriodoLiquidacion.Existe_Registro(Me.objEmpresa, sdrTercero("PELI_Codigo").ToString())
'                        Me.objCataGrupoLiquidacion.Existe_Registro(Me.objEmpresa, sdrTercero("GRLI_Codigo").ToString())

'                        Me.strObservaciones = sdrTercero("Observaciones").ToString()
'                        Me.objOficinaEmpleado.Existe_Registro(Me.objEmpresa, Val(sdrTercero("OFIC_EMPL_Codigo")))
'                        Me.objCataCargo.Existe_Registro(Me.objEmpresa, sdrTercero("CARG_Codigo").ToString())
'                        Me.objCataCausaBloqueo.Existe_Registro(Me.objEmpresa, sdrTercero("CABL_Codigo").ToString())
'                        Me.objCataDiaPago.Existe_Registro(Me.objEmpresa, sdrTercero("DIPA_Codigo").ToString())

'                        Me.dblPorcentajeComision = Val(sdrTercero("Porcentaje_Comision").ToString())
'                        Me.strJustificacionBloqueo = sdrTercero("Justificacion_Bloqueo").ToString()
'                        If Not IsNothing(Me.objRepresentanteComercial) Then
'                            Me.objRepresentanteComercial.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Representante_Comercial").ToString()))
'                        End If
'                        Me.intEsRepresentanteComercial = Val(sdrTercero("Representante_Comercial"))
'                        Me.strCodigoPostal = sdrTercero("Codigo_Postal").ToString()

'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio)
'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio)
'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero)
'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero)
'                        If Not IsNothing(Me.objEstudioSeguridad) Then
'                            If sdrTercero("ESSE_Numero") IsNot DBNull.Value Then
'                                Dim arrCampos(2), arrValoresCampos(2) As String

'                                arrCampos(0) = "Numero"
'                                arrCampos(1) = "Fecha"
'                                arrCampos(2) = "Fecha_Aprueba"

'                                If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Estudio_Seguridad_Vehiculos", arrCampos, arrValoresCampos, arrCampos.Length, General.CAMPO_NUMERICO, "Numero", sdrTercero("ESSE_Numero").ToString, "", Me.strError) Then
'                                    Me.objEstudioSeguridad.Numero = sdrTercero("ESSE_Numero").ToString
'                                    Me.objEstudioSeguridad.Fecha = arrValoresCampos(1)
'                                    Me.objEstudioSeguridad.FechaAprueba = arrValoresCampos(2)
'                                End If
'                            End If
'                        End If

'                        If sdrTercero("Foto") IsNot DBNull.Value Then
'                            bolFoto = True
'                        Else
'                            bolFoto = False
'                        End If

'                        If sdrTercero("Huella_Digital") IsNot DBNull.Value Then
'                            bolHuella_Digital = True
'                        Else
'                            bolHuella_Digital = False
'                        End If

'                        Me.objCataCoordenadaUno.Existe_Registro(Me.objEmpresa, Val(sdrTercero("COOR_Uno_Codigo").ToString()))
'                        Integer.TryParse(sdrTercero("Coordenada_Uno_Valor").ToString(), Me.intCoordenadaUnoValor)
'                        Me.objCataCoordenadaDos.Existe_Registro(Me.objEmpresa, Val(sdrTercero("COOR_Dos_Codigo").ToString()))
'                        Integer.TryParse(sdrTercero("Coordenada_Dos_Valor").ToString(), Me.intCoordenadaDosValor)
'                        Integer.TryParse(sdrTercero("Zona_Especial").ToString(), Me.intZonaEspecial)

'                        Integer.TryParse(sdrTercero("Dias_Bloqueo_Mora").ToString(), Me.intDiasBloqueoMora)
'                        Me.objCataPeriodicidadFacturacionCliente.Existe_Registro(Me.objEmpresa, Val(sdrTercero("PEFC_Codigo").ToString()))
'                        Integer.TryParse(sdrTercero("Tiene_Seguro").ToString(), Me.intTieneSeguro)
'                        Me.strAseguradoraCliente = sdrTercero("Aseguradora_Cliente").ToString()
'                        Date.TryParse(sdrTercero("Fecha_Vencimiento_Seguro").ToString(), Me.dteFechaVencimientoSeguro)

'                        Me.objCataTipoCoberturaSeguro.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TCSC_Codigo").ToString()))
'                        Double.TryParse(sdrTercero("Monto_Maximo_Seguro").ToString(), Me.dblMontoMaximoSeguro)
'                        Me.objCataGarantiaSeguroCliente.Existe_Registro(Me.objEmpresa, Val(sdrTercero("GASC_Codigo").ToString))

'                        Integer.TryParse(sdrTercero("Cliente_BASC").ToString(), Me.intClienteBASC)
'                        Me.strCondicionesSeguro = sdrTercero("Condiciones_Seguro").ToString()
'                        Integer.TryParse(sdrTercero("Antiguedad_Vehiculo").ToString(), Me.intAntiguedadVehiculo)
'                        Integer.TryParse(sdrTercero("Cliente_Prospecto").ToString(), Me.intClienteProspecto)
'                        Me.strReferencioProspecto = sdrTercero("Referencio_Prospecto").ToString()
'                        Me.strReferenciasVerificadas = sdrTercero("Referencias_Verificadas").ToString()

'                        Me.strCodigoActividad = sdrTercero("Codigo_Contable_Filial2").ToString

'                        If Not IsNothing(Me.objAseguradora) Then
'                            Me.objAseguradora.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Aseguradora").ToString()))
'                        End If

'                        Me.objCataTarifarioRepreComercial.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TARC_Codigo").ToString))

'                        Date.TryParse(sdrTercero("Fecha_Ingreso_Cliente").ToString(), Me.dteFechaIngresoCliente)

'                        Double.TryParse(sdrTercero("Descuento_Flete").ToString(), Me.dblDescuentoFlete)
'                        Double.TryParse(sdrTercero("Descuento_Manejo").ToString(), Me.dblDescuentoManejo)
'                        Double.TryParse(sdrTercero("Descuento_Minimo_Unidad").ToString(), Me.dblDescuentoMinimoUnidad)
'                        Double.TryParse(sdrTercero("Descuento_Urgencia").ToString(), Me.dblDescuentoUrgencia)
'                        Double.TryParse(sdrTercero("Descuento_Kg_Adicional").ToString(), Me.dblDescuentoKgAdicional)
'                        Double.TryParse(sdrTercero("Descuento_Documento_Retorno").ToString(), Me.dblDescuentoDocumentoRetorno)

'                        Double.TryParse(sdrTercero("Minimo_Kilos_Cobrar").ToString(), Me.dblMinimoKilosCobrar)
'                        Double.TryParse(sdrTercero("Porcentaje_Seguro").ToString(), Me.dblPorcentajeSeguro)
'                        Me.CodigoPais = Val(sdrTercero("PAIS_Codigo"))
'                        Me.intAplicReteIva = Val(sdrTercero("Aplica_Rete_IVA"))
'                    Else
'                        Me.objOficinaEmpleado.Codigo = Val(sdrTercero("OFIC_EMPL_Codigo"))
'                        If Not IsNothing(Me.objRepresentanteComercial) Then
'                            Me.objRepresentanteComercial.Codigo = Val(sdrTercero("TERC_Representante_Comercial").ToString())
'                        End If
'                        If Not IsNothing(Me.objAseguradora) Then
'                            Me.objAseguradora.Codigo = Val(sdrTercero("TERC_Aseguradora").ToString())
'                        End If
'                        If Not IsNothing(Me.objBeneficiarioTransferencia) Then
'                            Me.objBeneficiarioTransferencia.Codigo = Val(sdrTercero("TERC_Beneficiario_Transfe").ToString())
'                        End If
'                    End If

'                    Consultar = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(Optional ByVal CargaCompleta As Boolean = True) As Boolean
'            Try
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Dim sdrTercero As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Codigo, TINA_Codigo, TIID_Codigo, Numero_Identificacion, "
'                strSQL += "CIUD_Expedicion_Identificacion, Digito_Chequeo, Nombre, Apellido1, Apellido2, "
'                strSQL += "CIUD_Codigo, CIUD_Nacimiento, Fecha_Nacimiento, ESCI_Codigo, Sexo, "
'                strSQL += "Representante_Legal, Direccion, Telefono1, Telefono2, Fax, "
'                strSQL += "Celular, Email, Pagina_Web, REGI_Codigo, Administracion_Impuestos, "
'                strSQL += "Autoretenedor, Tipo_Sangre, Numero_Licencia, Categoria_Licencia, Fecha_Licencia, "
'                strSQL += "Afiliado_EPS, TERC_EPS, Fecha_EPS, Afiliado_ARP, TERC_ARP, "
'                strSQL += "Fecha_ARP, Afiliado_Fondo_Pensiones, TERC_Fondo_Pensiones, Fecha_Fondo_Pensiones, Afiliado_Fondo_Cesantias, "
'                strSQL += "TERC_Fondo_Cesantias, Fecha_Fondo_Cesantias, TICT_Codigo, Fecha_Contrato, Fecha_Vence_Contrato, "
'                strSQL += "Salario_Basico, Conductor_Propio, BANC_Transfe, Numero_Cuenta_Transfe, Tipo_Cuenta_Transfe, "
'                strSQL += "TERC_Beneficiario_Transfe, Codigo_Transfe, Estado, Nombre_Conyuge, Numero_Hijos, "
'                strSQL += "PELI_Codigo, GRLI_Codigo, Observaciones, OFIC_EMPL_Codigo, CARG_Codigo, "
'                strSQL += "CABL_Codigo, Porcentaje_Comision, DIPA_Codigo, TERC_Representante_Comercial, Fecha_Crea, "
'                strSQL += "USUA_Crea, Fecha_Modifica, USUA_Modifica, OFIC_Codigo, Justificacion_Bloqueo, "
'                strSQL += "Representante_Comercial,ESSE_Numero, Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio, Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio, Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero,"
'                strSQL += " Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero, Postfijo, Aplica_Rete_ICA, Codigo_Postal, COOR_Uno_Codigo, "
'                strSQL += "Coordenada_Uno_Valor, COOR_Dos_Codigo, Coordenada_Dos_Valor, Zona_Especial, ZONA_Codigo, "
'                strSQL += "Dias_Bloqueo_Mora, PEFC_Codigo, Tiene_Seguro, Aseguradora_Cliente, Fecha_Vencimiento_Seguro, "
'                strSQL += "TCSC_Codigo, Monto_Maximo_Seguro, GASC_Codigo, Cliente_BASC, Condiciones_Seguro, "
'                strSQL += "Antiguedad_Vehiculo, Cliente_Prospecto, Referencio_Prospecto, Referencias_Verificadas, TERC_Aseguradora, "
'                strSQL += "TARC_Codigo, Es_Filial, Codigo_Filial, Codigo_Contable_Filial, Cliente_Exterior, Fecha_Ingreso_Cliente, "
'                strSQL += "'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END,  Aplica_Rete_Fuente, Codigo_Contable_Filial2, "
'                strSQL += "Fecha_Ultimo_Cargue,CALI_Codigo,Nombre_Titular,Numero_Identificacion_Titular, Consulta_Lista_Clinton, Factura_Faltantes,Fecha_Nacimiento_Conyuge,Telefono_Conyuge, IANS_Codigo, TIID_Codigo_Titular, Digito_Chequeo_Titular, "
'                strSQL += "'Huella_Digital' = CASE WHEN Huella_Digital IS NULL THEN NULL ELSE 1 END, FPCO_Codigo, Dias_Plazo_Pago"
'                strSQL += " ,Descuento_Flete, Descuento_Manejo, Descuento_Minimo_Unidad, Descuento_Urgencia, Descuento_Kg_Adicional, Descuento_Documento_Retorno, Minimo_kilos_Cobrar, Porcentaje_Seguro, PAIS_Codigo, Aplica_Rete_IVA"
'                strSQL += " FROM Terceros"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Call Instanciar_Objetos_Alternos()
'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then
'                    Me.objCataTipoNaturaleza.Existe_Registro(Me.objEmpresa, sdrTercero("TINA_Codigo").ToString())
'                    Me.objCataTipoIdentificacion.Existe_Registro(Me.objEmpresa, sdrTercero("TIID_Codigo").ToString())
'                    Me.strNumeroIdentificacion = sdrTercero("Numero_Identificacion").ToString()
'                    Me.objCiudadExpedicionIdentificacion.Datos_Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Expedicion_Identificacion").ToString()))
'                    Integer.TryParse(sdrTercero("Digito_Chequeo").ToString(), Me.intDigitoChequeo)

'                    Me.strNombre = sdrTercero("Nombre").ToString()
'                    Me.strApellido1 = sdrTercero("Apellido1").ToString()
'                    Me.strApellido2 = sdrTercero("Apellido2").ToString()
'                    Me.objCiudadNacimiento.Datos_Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Nacimiento").ToString()))
'                    Date.TryParse(sdrTercero("Fecha_Nacimiento").ToString(), Me.dteFechaNacimiento)

'                    Me.objCataEstadoCivil.Existe_Registro(Me.objEmpresa, sdrTercero("ESCI_Codigo").ToString())
'                    Me.objCataSexo.Existe_Registro(Me.objEmpresa, sdrTercero("Sexo").ToString())
'                    Me.strRepresentanteLegal = sdrTercero("Representante_legal").ToString()
'                    Me.objCiudad.Datos_Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Codigo").ToString()))
'                    Me.strDireccion = sdrTercero("Direccion").ToString()

'                    Me.strTelefono1 = sdrTercero("Telefono1").ToString()
'                    Me.strTelefono2 = sdrTercero("telefono2").ToString()
'                    Me.strFax = sdrTercero("Fax").ToString()
'                    Me.strCelular = sdrTercero("Celular").ToString()
'                    Me.strEmail = sdrTercero("Email").ToString()

'                    Me.strPaginaWeb = sdrTercero("Pagina_Web").ToString()
'                    Me.objCataRegimen.Existe_Registro(Me.objEmpresa, Val(sdrTercero("REGI_Codigo").ToString()))
'                    Me.strAdministracionImpuestos = sdrTercero("Administracion_Impuestos").ToString()
'                    Integer.TryParse(sdrTercero("Autoretenedor").ToString(), Me.intAutoretenedor)
'                    Me.objCataTipoSangre.Existe_Registro(Me.objEmpresa, sdrTercero("Tipo_Sangre").ToString())

'                    Me.strNumeroLicencia = sdrTercero("Numero_Licencia").ToString()
'                    Integer.TryParse(sdrTercero("Categoria_Licencia").ToString(), Me.intCategoriaLicencia)
'                    Date.TryParse(sdrTercero("Fecha_Licencia").ToString(), Me.dteFechaLicencia)
'                    Me.strPostfijo = Trim(sdrTercero("Postfijo").ToString())
'                    Me.objCataEstado.Existe_Registro(sdrTercero("Estado").ToString())

'                    Me.intAplicaReteIca = Val(sdrTercero("Aplica_Rete_ICA"))
'                    Me.intAplicaReteFuente = Val(sdrTercero("Aplica_Rete_Fuente"))
'                    Integer.TryParse(sdrTercero("Conductor_Propio").ToString(), Me.intConductorPropio)
'                    Me.intClienteExterior = Val(sdrTercero("Cliente_Exterior"))
'                    Date.TryParse(sdrTercero("Fecha_Ultimo_Cargue").ToString(), Me.dteFechaUltimoCargue)

'                    Me.intEsFilial = Val(sdrTercero("Es_Filial").ToString())
'                    Me.strCodigoFilial = sdrTercero("Codigo_Filial").ToString()
'                    Me.strCodigoContableFilial = sdrTercero("Codigo_Contable_Filial").ToString()

'                    Me.objCataCategoriaLicenciaConductor.Campo1 = Val(sdrTercero("CALI_Codigo").ToString())
'                    Me.objCataTipoIdentificacionTitular.Existe_Registro(Me.objEmpresa, sdrTercero("TIID_Codigo_Titular").ToString())
'                    Integer.TryParse(sdrTercero("Digito_Chequeo_Titular").ToString(), Me.intDigitoChequeoTitular)
'                    Me.strNombreTitular = sdrTercero("Nombre_Titular").ToString()
'                    Me.strNumeroIdentificacionTitular = sdrTercero("Numero_Identificacion_Titular").ToString()
'                    Me.intConsultaListaClinton = Val(sdrTercero("Consulta_Lista_Clinton"))
'                    Me.intFacturaFaltantes = Val(sdrTercero("Factura_Faltantes"))
'                    Me.intIansCodigo = Val(sdrTercero("IANS_Codigo"))
'                    Me.objCataFormaPago.intCampoCodigo = Val(sdrTercero("FPCO_Codigo"))
'                    Me.intDiasPlazo = Val(sdrTercero("Dias_Plazo_Pago"))

'                    If CargaCompleta Then
'                        Integer.TryParse(sdrTercero("Afiliado_EPS").ToString(), Me.intAfiliadoEPS)
'                        If Not IsNothing(Me.objEPS) Then
'                            Me.objEPS.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_EPS").ToString()))
'                        End If
'                        Date.TryParse(sdrTercero("Fecha_EPS").ToString(), Me.dteFechaAfiliacionEPS)
'                        Integer.TryParse(sdrTercero("Afiliado_ARP").ToString(), Me.intAfiliadoARP)

'                        If Not IsNothing(Me.objARP) Then
'                            Me.objARP.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_ARP").ToString()))
'                        End If
'                        Date.TryParse(sdrTercero("Fecha_ARP").ToString(), Me.dteFechaAfiliacionARP)
'                        Integer.TryParse(sdrTercero("Afiliado_Fondo_Pensiones").ToString(), Me.intAfiliadoFondoPensiones)
'                        If Not IsNothing(Me.objFondoPensiones) Then
'                            Me.objFondoPensiones.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Fondo_Pensiones").ToString()))
'                        End If
'                        Date.TryParse(sdrTercero("Fecha_Fondo_Pensiones").ToString(), Me.dteFechaAfiliacionFondoPensiones)
'                        Integer.TryParse(sdrTercero("Afiliado_Fondo_Cesantias").ToString(), Me.intAfiliadoFondoCesantias)
'                        If Not IsNothing(Me.objFondoCesantias) Then
'                            Me.objFondoCesantias.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Fondo_Cesantias").ToString()))
'                        End If
'                        Date.TryParse(sdrTercero("Fecha_Fondo_Cesantias").ToString(), Me.dteFechaAfiliacionFondoCesantias)

'                        Me.objCataTipoContrato.Existe_Registro(Me.objEmpresa, sdrTercero("TICT_Codigo").ToString())
'                        Date.TryParse(sdrTercero("Fecha_Contrato").ToString(), Me.dteFechaContrato)
'                        Date.TryParse(sdrTercero("Fecha_Vence_Contrato").ToString(), Me.dteFechaVenceContrato)
'                        Double.TryParse(sdrTercero("Salario_Basico").ToString(), Me.dblSalarioBasico)

'                        Me.objBancoTransferencia.Codigo = Val(sdrTercero("BANC_Transfe"))
'                        Me.strNumeroCuentaTransferencia = sdrTercero("Numero_Cuenta_Transfe").ToString()
'                        Me.objCataTipoCuentaTransferencia.Existe_Registro(sdrTercero("Tipo_Cuenta_Transfe").ToString())
'                        If Not IsNothing(Me.objBeneficiarioTransferencia) Then
'                            Me.objBeneficiarioTransferencia.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Beneficiario_Transfe").ToString()))
'                        End If
'                        Me.strCodigoTransferencia = sdrTercero("Codigo_Transfe").ToString()

'                        Me.strNombreConyuge = sdrTercero("Nombre_Conyuge").ToString()
'                        Integer.TryParse(sdrTercero("Numero_Hijos").ToString(), Me.intNumeroHijos)
'                        Me.objCataPeriodoLiquidacion.Existe_Registro(Me.objEmpresa, sdrTercero("PELI_Codigo").ToString())
'                        Me.objCataGrupoLiquidacion.Existe_Registro(Me.objEmpresa, sdrTercero("GRLI_Codigo").ToString())

'                        Me.strObservaciones = sdrTercero("Observaciones").ToString()
'                        Me.objOficinaEmpleado.Existe_Registro(Me.objEmpresa, Val(sdrTercero("OFIC_EMPL_Codigo")))
'                        Me.objCataCargo.Existe_Registro(Me.objEmpresa, sdrTercero("CARG_Codigo").ToString())
'                        Me.objCataCausaBloqueo.Existe_Registro(Me.objEmpresa, sdrTercero("CABL_Codigo").ToString())
'                        Me.objCataDiaPago.Existe_Registro(Me.objEmpresa, sdrTercero("DIPA_Codigo").ToString())

'                        Me.dblPorcentajeComision = Val(sdrTercero("Porcentaje_Comision").ToString())
'                        Me.strJustificacionBloqueo = sdrTercero("Justificacion_Bloqueo").ToString()
'                        If Not IsNothing(Me.objRepresentanteComercial) Then
'                            Me.objRepresentanteComercial.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Representante_Comercial").ToString()))
'                        End If
'                        Me.intEsRepresentanteComercial = Val(sdrTercero("Representante_Comercial"))
'                        Me.strCodigoPostal = sdrTercero("Codigo_Postal").ToString()

'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio)
'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio)
'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero)
'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero)

'                        If Not IsNothing(Me.objEstudioSeguridad) Then
'                            If sdrTercero("ESSE_Numero") IsNot DBNull.Value Then
'                                Dim arrCampos(2), arrValoresCampos(2) As String

'                                arrCampos(0) = "Numero"
'                                arrCampos(1) = "Fecha"
'                                arrCampos(2) = "Fecha_Aprueba"

'                                If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Estudio_Seguridad_Vehiculos", arrCampos, arrValoresCampos, arrCampos.Length, General.CAMPO_NUMERICO, "Numero", sdrTercero("ESSE_Numero").ToString, "", Me.strError) Then
'                                    Me.objEstudioSeguridad.Numero = sdrTercero("ESSE_Numero").ToString
'                                    Me.objEstudioSeguridad.Fecha = arrValoresCampos(1)
'                                    Me.objEstudioSeguridad.FechaAprueba = arrValoresCampos(2)
'                                End If
'                            End If
'                        End If


'                        If sdrTercero("Foto") IsNot DBNull.Value Then
'                            bolFoto = True
'                        Else
'                            bolFoto = False
'                        End If

'                        If sdrTercero("Huella_Digital") IsNot DBNull.Value Then
'                            bolHuella_Digital = True
'                        Else
'                            bolHuella_Digital = False
'                        End If

'                        Me.objCataCoordenadaUno.Existe_Registro(Me.objEmpresa, Val(sdrTercero("COOR_Uno_Codigo").ToString()))
'                        Integer.TryParse(sdrTercero("Coordenada_Uno_Valor").ToString(), Me.intCoordenadaUnoValor)
'                        Me.objCataCoordenadaDos.Existe_Registro(Me.objEmpresa, Val(sdrTercero("COOR_Dos_Codigo").ToString()))
'                        Integer.TryParse(sdrTercero("Coordenada_Dos_Valor").ToString(), Me.intCoordenadaDosValor)
'                        Integer.TryParse(sdrTercero("Zona_Especial").ToString(), Me.intZonaEspecial)

'                        Integer.TryParse(sdrTercero("Dias_Bloqueo_Mora").ToString(), Me.intDiasBloqueoMora)
'                        Me.objCataPeriodicidadFacturacionCliente.Existe_Registro(Me.objEmpresa, Val(sdrTercero("PEFC_Codigo").ToString()))
'                        Integer.TryParse(sdrTercero("Tiene_Seguro").ToString(), Me.intTieneSeguro)
'                        Me.strAseguradoraCliente = sdrTercero("Aseguradora_Cliente").ToString()
'                        Date.TryParse(sdrTercero("Fecha_Vencimiento_Seguro").ToString(), Me.dteFechaVencimientoSeguro)

'                        Me.objCataTipoCoberturaSeguro.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TCSC_Codigo").ToString()))
'                        Double.TryParse(sdrTercero("Monto_Maximo_Seguro").ToString(), Me.dblMontoMaximoSeguro)
'                        Me.objCataGarantiaSeguroCliente.Existe_Registro(Me.objEmpresa, Val(sdrTercero("GASC_Codigo").ToString))

'                        Integer.TryParse(sdrTercero("Cliente_BASC").ToString(), Me.intClienteBASC)
'                        Me.strCondicionesSeguro = sdrTercero("Condiciones_Seguro").ToString()
'                        Integer.TryParse(sdrTercero("Antiguedad_Vehiculo").ToString(), Me.intAntiguedadVehiculo)
'                        Integer.TryParse(sdrTercero("Cliente_Prospecto").ToString(), Me.intClienteProspecto)
'                        Me.strReferencioProspecto = sdrTercero("Referencio_Prospecto").ToString()
'                        Me.strReferenciasVerificadas = sdrTercero("Referencias_Verificadas").ToString()

'                        Me.strCodigoActividad = sdrTercero("Codigo_Contable_Filial2").ToString

'                        If Not IsNothing(Me.objAseguradora) Then
'                            Me.objAseguradora.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Aseguradora").ToString()))
'                        End If

'                        Me.objCataTarifarioRepreComercial.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TARC_Codigo").ToString))
'                        Date.TryParse(sdrTercero("Fecha_Ingreso_Cliente").ToString(), Me.dteFechaIngresoCliente)
'                        Date.TryParse(sdrTercero("Fecha_Nacimiento_Conyuge").ToString(), Me.dteFechNaciCony)
'                        Me.strTeleCony = sdrTercero("Telefono_Conyuge").ToString()

'                        Double.TryParse(sdrTercero("Descuento_Flete").ToString(), Me.dblDescuentoFlete)
'                        Double.TryParse(sdrTercero("Descuento_Manejo").ToString(), Me.dblDescuentoManejo)
'                        Double.TryParse(sdrTercero("Descuento_Minimo_Unidad").ToString(), Me.dblDescuentoMinimoUnidad)
'                        Double.TryParse(sdrTercero("Descuento_Urgencia").ToString(), Me.dblDescuentoUrgencia)
'                        Double.TryParse(sdrTercero("Descuento_Kg_Adicional").ToString(), Me.dblDescuentoKgAdicional)
'                        Double.TryParse(sdrTercero("Descuento_Documento_Retorno").ToString(), Me.dblDescuentoDocumentoRetorno)

'                        Double.TryParse(sdrTercero("Minimo_Kilos_Cobrar").ToString(), Me.dblMinimoKilosCobrar)
'                        Double.TryParse(sdrTercero("Porcentaje_Seguro").ToString(), Me.dblPorcentajeSeguro)
'                        Me.CodigoPais = Val(sdrTercero("PAIS_Codigo"))
'                        Me.intAplicReteIva = Val(sdrTercero("Aplica_Rete_IVA"))
'                    Else
'                        Me.objOficinaEmpleado.Codigo = Val(sdrTercero("OFIC_EMPL_Codigo"))
'                        If Not IsNothing(Me.objRepresentanteComercial) Then
'                            Me.objRepresentanteComercial.Codigo = Val(sdrTercero("TERC_Representante_Comercial").ToString())
'                        End If
'                        If Not IsNothing(Me.objAseguradora) Then
'                            Me.objAseguradora.Codigo = Val(sdrTercero("TERC_Aseguradora").ToString())
'                        End If
'                        If Not IsNothing(Me.objBeneficiarioTransferencia) Then
'                            Me.objBeneficiarioTransferencia.Codigo = Val(sdrTercero("TERC_Beneficiario_Transfe").ToString())
'                        End If
'                    End If

'                    Consultar = True
'                Else
'                    Me.lonCodigo = 0
'                    Consultar = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & ex.Message.ToString()
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Cargar_Tercero(Optional ByVal CargaCompleta As Boolean = True) As Boolean
'            Try
'                Dim sdrTercero As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Codigo, TINA_Codigo, TIID_Codigo, Numero_Identificacion, "
'                strSQL += "CIUD_Expedicion_Identificacion, Digito_Chequeo, Nombre, Apellido1, Apellido2, "
'                strSQL += "CIUD_Codigo, CIUD_Nacimiento, Fecha_Nacimiento, ESCI_Codigo, Sexo, "
'                strSQL += "Representante_Legal, Direccion, Telefono1, Telefono2, Fax, "
'                strSQL += "Celular, Email, Pagina_Web, REGI_Codigo, Administracion_Impuestos, "
'                strSQL += "Autoretenedor, Tipo_Sangre, Numero_Licencia, Categoria_Licencia, Fecha_Licencia, "
'                strSQL += "Afiliado_EPS, TERC_EPS, Fecha_EPS, Afiliado_ARP, TERC_ARP, "
'                strSQL += "Fecha_ARP, Afiliado_Fondo_Pensiones, TERC_Fondo_Pensiones, Fecha_Fondo_Pensiones, Afiliado_Fondo_Cesantias, "
'                strSQL += "TERC_Fondo_Cesantias, Fecha_Fondo_Cesantias, TICT_Codigo, Fecha_Contrato, Fecha_Vence_Contrato, "
'                strSQL += "Salario_Basico, Conductor_Propio, BANC_Transfe, Numero_Cuenta_Transfe, Tipo_Cuenta_Transfe, "
'                strSQL += "TERC_Beneficiario_Transfe, Codigo_Transfe, Estado, Nombre_Conyuge, Numero_Hijos, "
'                strSQL += "PELI_Codigo, GRLI_Codigo, Observaciones, OFIC_EMPL_Codigo, CARG_Codigo, "
'                strSQL += "CABL_Codigo, Porcentaje_Comision, DIPA_Codigo, TERC_Representante_Comercial, Fecha_Crea, "
'                strSQL += "USUA_Crea, Fecha_Modifica, USUA_Modifica, OFIC_Codigo, Justificacion_Bloqueo, "
'                strSQL += "Representante_Comercial,ESSE_Numero, Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio, Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio, Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero,"
'                strSQL += " Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero, Postfijo, Aplica_Rete_ICA, Codigo_Postal, COOR_Uno_Codigo, "
'                strSQL += "Coordenada_Uno_Valor, COOR_Dos_Codigo, Coordenada_Dos_Valor, Zona_Especial, ZONA_Codigo, "
'                strSQL += "Dias_Bloqueo_Mora, PEFC_Codigo, Tiene_Seguro, Aseguradora_Cliente, Fecha_Vencimiento_Seguro, "
'                strSQL += "TCSC_Codigo, Monto_Maximo_Seguro, GASC_Codigo, Cliente_BASC, Condiciones_Seguro, "
'                strSQL += "Antiguedad_Vehiculo, Cliente_Prospecto, Referencio_Prospecto, Referencias_Verificadas, TERC_Aseguradora, "
'                strSQL += "TARC_Codigo, Es_Filial, Codigo_Filial, Codigo_Contable_Filial, Cliente_Exterior, Fecha_Ingreso_Cliente, "
'                strSQL += "'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END,  Aplica_Rete_Fuente, Codigo_Contable_Filial2, Fecha_Ultimo_Cargue , TIID_Codigo_Titular, Digito_Chequeo_Titular"
'                strSQL += "CALI_Codigo,Nombre_Titular,Numero_Identificacion_Titular, Consulta_Lista_Clinton, Factura_Faltantes,  "
'                strSQL += "'Huella_Digital' = CASE WHEN Huella_Digital IS NULL THEN NULL ELSE 1 END "
'                strSQL += " FROM Terceros "
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Call Instanciar_Objetos_Alternos()

'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then

'                    Me.objCataTipoNaturaleza.Existe_Registro(sdrTercero("TINA_Codigo").ToString())
'                    Me.objCataTipoIdentificacion.Existe_Registro(sdrTercero("TIID_Codigo").ToString())
'                    Me.strNumeroIdentificacion = sdrTercero("Numero_Identificacion").ToString()
'                    Me.objCiudadExpedicionIdentificacion.Codigo = Val(sdrTercero("CIUD_Expedicion_Identificacion").ToString())
'                    Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Ciudades", "Nombre", "Codigo", General.CAMPO_NUMERICO, Val(sdrTercero("CIUD_Expedicion_Identificacion")), Me.objCiudadExpedicionIdentificacion.Nombre, , Me.strError)
'                    Integer.TryParse(sdrTercero("Digito_Chequeo").ToString(), Me.intDigitoChequeo)

'                    Me.strNombre = sdrTercero("Nombre").ToString()
'                    Me.strApellido1 = sdrTercero("Apellido1").ToString()
'                    Me.strApellido2 = sdrTercero("Apellido2").ToString()
'                    Me.objCiudadNacimiento.Codigo = Val(sdrTercero("CIUD_Nacimiento").ToString())
'                    Date.TryParse(sdrTercero("Fecha_Nacimiento").ToString(), Me.dteFechaNacimiento)

'                    Me.objCataEstadoCivil.Campo1 = sdrTercero("ESCI_Codigo").ToString()
'                    Me.objCataSexo.Campo1 = sdrTercero("Sexo").ToString()
'                    Me.strRepresentanteLegal = sdrTercero("Representante_legal").ToString()
'                    Me.objCiudad.Datos_Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Codigo").ToString()))
'                    Me.strDireccion = sdrTercero("Direccion").ToString()

'                    Me.strTelefono1 = sdrTercero("Telefono1").ToString()
'                    Me.strTelefono2 = sdrTercero("Telefono2").ToString()
'                    Me.strFax = sdrTercero("Fax").ToString()
'                    Me.strCelular = sdrTercero("Celular").ToString()
'                    Me.strEmail = sdrTercero("Email").ToString()

'                    Me.strPaginaWeb = sdrTercero("Pagina_Web").ToString()
'                    Me.objCataRegimen.Campo1 = Val(sdrTercero("REGI_Codigo").ToString())
'                    Me.strAdministracionImpuestos = sdrTercero("Administracion_Impuestos").ToString()
'                    Integer.TryParse(sdrTercero("Autoretenedor").ToString(), Me.intAutoretenedor)
'                    Me.objCataTipoSangre.Campo1 = RTrim(sdrTercero("Tipo_Sangre").ToString())

'                    Me.strNumeroLicencia = sdrTercero("Numero_Licencia").ToString()
'                    Integer.TryParse(sdrTercero("Categoria_Licencia").ToString(), Me.intCategoriaLicencia)
'                    Date.TryParse(sdrTercero("Fecha_Licencia").ToString(), Me.dteFechaLicencia)
'                    Me.objCataEstado.Campo1 = sdrTercero("Estado").ToString()
'                    Me.intAplicaReteIca = Val(sdrTercero("Aplica_Rete_ICA"))

'                    Me.intAplicaReteFuente = Val(sdrTercero("Aplica_Rete_Fuente"))
'                    Me.intClienteExterior = Val(sdrTercero("Cliente_Exterior"))
'                    Date.TryParse(sdrTercero("Fecha_Ultimo_Cargue").ToString(), Me.dteFechaUltimoCargue)
'                    Me.objCataCategoriaLicenciaConductor.Campo1 = Val(sdrTercero("CALI_Codigo").ToString())
'                    Me.objCataTipoIdentificacionTitular.Existe_Registro(Me.objEmpresa, sdrTercero("TIID_Codigo_Titular").ToString())
'                    Integer.TryParse(sdrTercero("Digito_Chequeo_Titular").ToString(), Me.intDigitoChequeoTitular)
'                    Me.strNombreTitular = sdrTercero("Nombre_Titular").ToString()
'                    Me.strNumeroIdentificacionTitular = sdrTercero("Numero_Identificacion_Titular").ToString()
'                    Me.intConsultaListaClinton = Val(sdrTercero("Consulta_Lista_Clinton"))
'                    Me.intFacturaFaltantes = Val(sdrTercero("Factura_Faltantes"))


'                    If CargaCompleta Then
'                        Integer.TryParse(sdrTercero("Afiliado_EPS").ToString(), Me.intAfiliadoEPS)
'                        Me.objEPS.Codigo = Val(sdrTercero("TERC_EPS").ToString())
'                        Date.TryParse(sdrTercero("Fecha_EPS").ToString(), Me.dteFechaAfiliacionEPS)

'                        Integer.TryParse(sdrTercero("Afiliado_ARP").ToString(), Me.intAfiliadoARP)
'                        Me.objARP.Codigo = Val(sdrTercero("TERC_ARP").ToString())
'                        Date.TryParse(sdrTercero("Fecha_ARP").ToString(), Me.dteFechaAfiliacionARP)

'                        Integer.TryParse(sdrTercero("Afiliado_Fondo_Pensiones").ToString(), Me.intAfiliadoFondoPensiones)
'                        Me.objFondoPensiones.Codigo = Val(sdrTercero("TERC_Fondo_Pensiones").ToString())
'                        Date.TryParse(sdrTercero("Fecha_Fondo_Pensiones").ToString(), Me.dteFechaAfiliacionFondoPensiones)

'                        Integer.TryParse(sdrTercero("Afiliado_Fondo_Cesantias").ToString(), Me.intAfiliadoFondoCesantias)
'                        Me.objFondoCesantias.Codigo = Val(sdrTercero("TERC_Fondo_Cesantias").ToString())
'                        Date.TryParse(sdrTercero("Fecha_Fondo_Cesantias").ToString(), Me.dteFechaAfiliacionFondoCesantias)

'                        Me.objCataTipoContrato.Campo1 = (sdrTercero("TICT_Codigo").ToString())
'                        Date.TryParse(sdrTercero("Fecha_Contrato").ToString(), Me.dteFechaContrato)
'                        Date.TryParse(sdrTercero("Fecha_Vence_Contrato").ToString(), Me.dteFechaVenceContrato)
'                        Double.TryParse(sdrTercero("Salario_Basico").ToString(), Me.dblSalarioBasico)
'                        Integer.TryParse(sdrTercero("Conductor_Propio").ToString(), Me.intConductorPropio)

'                        Me.objBancoTransferencia.Codigo = Val(sdrTercero("BANC_Transfe").ToString())
'                        Me.strNumeroCuentaTransferencia = sdrTercero("Numero_Cuenta_Transfe").ToString()
'                        Me.objCataTipoCuentaTransferencia.Campo1 = sdrTercero("Tipo_Cuenta_Transfe").ToString()
'                        Me.objBeneficiarioTransferencia.Codigo = Val(sdrTercero("TERC_Beneficiario_Transfe").ToString())
'                        Me.strCodigoTransferencia = sdrTercero("Codigo_Transfe").ToString()


'                        Me.strNombreConyuge = sdrTercero("Nombre_Conyuge").ToString()
'                        Integer.TryParse(sdrTercero("Numero_Hijos").ToString(), Me.intNumeroHijos)
'                        Me.objCataPeriodoLiquidacion.Campo1 = sdrTercero("PELI_Codigo").ToString()
'                        Me.objCataGrupoLiquidacion.Campo1 = sdrTercero("GRLI_Codigo").ToString()

'                        Me.strObservaciones = sdrTercero("Observaciones").ToString()
'                        Me.objOficinaEmpleado.Codigo = Val(sdrTercero("OFIC_EMPL_Codigo"))
'                        Me.objCataCargo.Campo1 = sdrTercero("CARG_Codigo").ToString()
'                        Me.objCataCausaBloqueo.Campo1 = sdrTercero("CABL_Codigo").ToString()
'                        Me.objCataDiaPago.Campo1 = sdrTercero("DIPA_Codigo").ToString()

'                        Me.dblPorcentajeComision = Val(sdrTercero("Porcentaje_Comision").ToString())
'                        Me.objRepresentanteComercial.Codigo = Val(sdrTercero("TERC_Representante_Comercial").ToString())
'                        Me.strJustificacionBloqueo = sdrTercero("Justificacion_Bloqueo").ToString()
'                        Me.intEsRepresentanteComercial = Val(sdrTercero("Representante_Comercial"))
'                        Me.strCodigoPostal = sdrTercero("Codigo_Postal").ToString()

'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio)
'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio)
'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero)
'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero)

'                        If Not IsNothing(Me.objEstudioSeguridad) Then
'                            If sdrTercero("ESSE_Numero") IsNot DBNull.Value Then
'                                Dim arrCampos(2), arrValoresCampos(2) As String

'                                arrCampos(0) = "Numero"
'                                arrCampos(1) = "Fecha"
'                                arrCampos(2) = "Fecha_Aprueba"

'                                If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "Estudio_Seguridad_Vehiculos", arrCampos, arrValoresCampos, 2, General.CAMPO_NUMERICO, "Numero", sdrTercero("ESSE_Numero").ToString, "", Me.strError) Then
'                                    Me.objEstudioSeguridad.Numero = sdrTercero("ESSE_Numero").ToString
'                                    Me.objEstudioSeguridad.Fecha = arrValoresCampos(1)
'                                    Me.objEstudioSeguridad.FechaAprueba = arrValoresCampos(2)
'                                End If
'                            End If
'                        End If

'                        Me.objCataCoordenadaUno.Campo1 = Val(sdrTercero("COOR_Uno_Codigo").ToString())
'                        Integer.TryParse(sdrTercero("Coordenada_Uno_Valor").ToString(), Me.intCoordenadaUnoValor)
'                        Me.objCataCoordenadaDos.Campo1 = Val(sdrTercero("COOR_Dos_Codigo").ToString())
'                        Integer.TryParse(sdrTercero("Coordenada_Dos_Valor").ToString(), Me.intCoordenadaDosValor)
'                        Integer.TryParse(sdrTercero("Zona_Especial").ToString(), Me.intZonaEspecial)

'                        Integer.TryParse(sdrTercero("Dias_Bloqueo_Mora").ToString(), Me.intDiasBloqueoMora)
'                        Me.objCataPeriodicidadFacturacionCliente.Campo1 = Val(sdrTercero("PEFC_Codigo").ToString())
'                        Integer.TryParse(sdrTercero("Tiene_Seguro").ToString(), Me.intTieneSeguro)
'                        Me.strAseguradoraCliente = sdrTercero("Aseguradora_Cliente").ToString()
'                        Date.TryParse(sdrTercero("Fecha_Vencimiento_Seguro").ToString(), Me.dteFechaVencimientoSeguro)

'                        Me.objCataTipoCoberturaSeguro.Campo1 = Val(sdrTercero("TCSC_Codigo").ToString())
'                        Double.TryParse(sdrTercero("Monto_Maximo_Seguro").ToString(), Me.dblMontoMaximoSeguro)
'                        Me.objCataGarantiaSeguroCliente.Campo1 = Val(sdrTercero("GASC_Codigo").ToString)

'                        Integer.TryParse(sdrTercero("Cliente_BASC").ToString(), Me.intClienteBASC)
'                        Me.strCondicionesSeguro = sdrTercero("Condiciones_Seguro").ToString()
'                        Integer.TryParse(sdrTercero("Antiguedad_Vehiculo").ToString(), Me.intAntiguedadVehiculo)
'                        Integer.TryParse(sdrTercero("Cliente_Prospecto").ToString(), Me.intClienteProspecto)
'                        Me.strReferencioProspecto = sdrTercero("Referencio_Prospecto").ToString()
'                        Me.strReferenciasVerificadas = sdrTercero("Referencias_Verificadas").ToString()

'                        Me.intEsFilial = Val(sdrTercero("Es_Filial").ToString())
'                        Me.strCodigoFilial = sdrTercero("Codigo_Filial").ToString()
'                        Me.strCodigoContableFilial = sdrTercero("Codigo_Contable_Filial").ToString()
'                        Me.strCodigoActividad = sdrTercero("Codigo_Contable_Filial2").ToString

'                        If Not IsNothing(Me.objAseguradora) Then
'                            Me.objAseguradora.Codigo = Val(sdrTercero("TERC_Aseguradora").ToString())
'                        End If
'                        Me.objCataTarifarioRepreComercial.Campo1 = Val(sdrTercero("TARC_Codigo").ToString)

'                        Date.TryParse(sdrTercero("Fecha_Ingreso_Cliente").ToString(), Me.dteFechaIngresoCliente)
'                    Else
'                        Me.objOficinaEmpleado.Codigo = Val(sdrTercero("OFIC_EMPL_Codigo"))
'                        If Not IsNothing(Me.objRepresentanteComercial) Then
'                            Me.objRepresentanteComercial.Codigo = Val(sdrTercero("TERC_Representante_Comercial").ToString())
'                        End If
'                        If Not IsNothing(Me.objAseguradora) Then
'                            Me.objAseguradora.Codigo = Val(sdrTercero("TERC_Aseguradora").ToString())
'                        End If
'                        If Not IsNothing(Me.objBeneficiarioTransferencia) Then
'                            Me.objBeneficiarioTransferencia.Codigo = Val(sdrTercero("TERC_Beneficiario_Transfe").ToString())
'                        End If
'                    End If
'                    Me.strPostfijo = Trim(sdrTercero("Postfijo").ToString())

'                    If sdrTercero("Foto") IsNot DBNull.Value Then
'                        bolFoto = True
'                    Else
'                        bolFoto = False
'                    End If

'                    If sdrTercero("Huella_Digital") IsNot DBNull.Value Then
'                        bolHuella_Digital = True
'                    Else
'                        bolHuella_Digital = False
'                    End If

'                    Cargar_Tercero = True
'                Else
'                    Me.lonCodigo = 0
'                    Cargar_Tercero = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()


'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Cargar_Tercero = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Tercero: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Cargar_Tercero_Seguridad_Social(ByRef Empresa As Empresas, ByVal Codigo As Long) As Boolean
'            Try
'                Dim sdrTercero As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Codigo,Nombre, Apellido1,Apellido2, Representante_legal,"
'                strSQL += "Afiliado_EPS, TERC_EPS, Fecha_EPS, Afiliado_ARP, TERC_ARP, "
'                strSQL += "Fecha_ARP, Afiliado_Fondo_Pensiones, TERC_Fondo_Pensiones, Fecha_Fondo_Pensiones, Afiliado_Fondo_Cesantias, "
'                strSQL += "TERC_Fondo_Cesantias, Fecha_Fondo_Cesantias, TICT_Codigo, Fecha_Contrato, Fecha_Vence_Contrato"
'                strSQL += "FROM Terceros "
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Call Instanciar_Objetos_Alternos()


'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then

'                    Me.strNombre = sdrTercero("Nombre").ToString()
'                    Me.strApellido1 = sdrTercero("Apellido1").ToString()
'                    Me.strApellido2 = sdrTercero("Apellido2").ToString()

'                    Me.strRepresentanteLegal = sdrTercero("Representante_legal").ToString()

'                    Integer.TryParse(sdrTercero("Afiliado_EPS").ToString(), Me.intAfiliadoEPS)
'                    Me.objEPS.Codigo = Val(sdrTercero("TERC_EPS").ToString())
'                    Date.TryParse(sdrTercero("Fecha_EPS").ToString(), Me.dteFechaAfiliacionEPS)

'                    Integer.TryParse(sdrTercero("Afiliado_ARP").ToString(), Me.intAfiliadoARP)
'                    Me.objARP.Codigo = Val(sdrTercero("TERC_ARP").ToString())
'                    Date.TryParse(sdrTercero("Fecha_ARP").ToString(), Me.dteFechaAfiliacionARP)

'                    Integer.TryParse(sdrTercero("Afiliado_Fondo_Pensiones").ToString(), Me.intAfiliadoFondoPensiones)
'                    Me.objFondoPensiones.Codigo = Val(sdrTercero("TERC_Fondo_Pensiones").ToString())
'                    Date.TryParse(sdrTercero("Fecha_Fondo_Pensiones").ToString(), Me.dteFechaAfiliacionFondoPensiones)

'                    Integer.TryParse(sdrTercero("Afiliado_Fondo_Cesantias").ToString(), Me.intAfiliadoFondoCesantias)
'                    Me.objFondoCesantias.Codigo = Val(sdrTercero("TERC_Fondo_Cesantias").ToString())
'                    Date.TryParse(sdrTercero("Fecha_Fondo_Cesantias").ToString(), Me.dteFechaAfiliacionFondoCesantias)


'                    If sdrTercero("Foto") IsNot DBNull.Value Then
'                        bolFoto = True
'                    Else
'                        bolFoto = False
'                    End If

'                    If sdrTercero("Huella_Digital") IsNot DBNull.Value Then
'                        bolHuella_Digital = True
'                    Else
'                        bolHuella_Digital = False
'                    End If

'                    Cargar_Tercero_Seguridad_Social = True
'                Else
'                    Me.lonCodigo = 0
'                    Cargar_Tercero_Seguridad_Social = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Cargar_Tercero_Seguridad_Social = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Tercero_Seguridad_Social: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Cargar_Por_Identificacion(ByVal Empresa As Empresas, ByVal NumeroIdentificacion As String, Optional ByVal CargaCompleta As Boolean = True) As Boolean
'            Try
'                Me.objEmpresa = Empresa
'                Me.strNumeroIdentificacion = NumeroIdentificacion

'                Dim sdrTercero As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 EMPR_Codigo, Codigo, TINA_Codigo, TIID_Codigo, Numero_Identificacion, "
'                strSQL += "CIUD_Expedicion_Identificacion, Digito_Chequeo, Nombre, Apellido1, Apellido2, "
'                strSQL += "CIUD_Codigo, CIUD_Nacimiento, Fecha_Nacimiento, ESCI_Codigo, Sexo, "
'                strSQL += "Representante_Legal, Direccion, Telefono1, Telefono2, Fax, "
'                strSQL += "Celular, Email, Pagina_Web, REGI_Codigo, Administracion_Impuestos, "
'                strSQL += "Autoretenedor, Tipo_Sangre, Numero_Licencia, Categoria_Licencia, Fecha_Licencia, "
'                strSQL += "Afiliado_EPS, TERC_EPS, Fecha_EPS, Afiliado_ARP, TERC_ARP, "
'                strSQL += "Fecha_ARP, Afiliado_Fondo_Pensiones, TERC_Fondo_Pensiones, Fecha_Fondo_Pensiones, Afiliado_Fondo_Cesantias, "
'                strSQL += "TERC_Fondo_Cesantias, Fecha_Fondo_Cesantias, TICT_Codigo, Fecha_Contrato, Fecha_Vence_Contrato, "
'                strSQL += "Salario_Basico, Conductor_Propio, BANC_Transfe, Numero_Cuenta_Transfe, Tipo_Cuenta_Transfe, "
'                strSQL += "TERC_Beneficiario_Transfe, Codigo_Transfe, Estado, Nombre_Conyuge, Numero_Hijos, "
'                strSQL += "PELI_Codigo, GRLI_Codigo, Observaciones, OFIC_EMPL_Codigo, CARG_Codigo, "
'                strSQL += "CABL_Codigo, Porcentaje_Comision, DIPA_Codigo, TERC_Representante_Comercial, Fecha_Crea, "
'                strSQL += "USUA_Crea, Fecha_Modifica, USUA_Modifica, OFIC_Codigo, Justificacion_Bloqueo, "
'                strSQL += "Representante_Comercial,ESSE_Numero, Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio, Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio, Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero,"
'                strSQL += " Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero, Postfijo, Aplica_Rete_ICA, Codigo_Postal, COOR_Uno_Codigo, "
'                strSQL += "Coordenada_Uno_Valor, COOR_Dos_Codigo, Coordenada_Dos_Valor, Zona_Especial, ZONA_Codigo, "
'                strSQL += "Dias_Bloqueo_Mora, PEFC_Codigo, Tiene_Seguro, Aseguradora_Cliente, Fecha_Vencimiento_Seguro, "
'                strSQL += "TCSC_Codigo, Monto_Maximo_Seguro, GASC_Codigo, Cliente_BASC, Condiciones_Seguro, "
'                strSQL += "Antiguedad_Vehiculo, Cliente_Prospecto, Referencio_Prospecto, Referencias_Verificadas, TERC_Aseguradora, "
'                strSQL += "TARC_Codigo, Es_Filial, Codigo_Filial, Codigo_Contable_Filial, Cliente_Exterior, Fecha_Ingreso_Cliente, "
'                strSQL += "'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END, Aplica_Rete_Fuente, Codigo_Contable_Filial2, "
'                strSQL += "CALI_Codigo,Nombre_Titular,Numero_Identificacion_Titular, Consulta_Lista_Clinton, Factura_Faltantes, TIID_Codigo_Titular, Digito_Chequeo_Titular, "
'                strSQL += "'Huella_Digital' = CASE WHEN Huella_Digital IS NULL THEN NULL ELSE 1 END"
'                strSQL += " FROM Terceros "
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero_Identificacion = '" & Me.strNumeroIdentificacion & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Call Instanciar_Objetos_Alternos()


'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then

'                    Me.objCataTipoNaturaleza.Existe_Registro(sdrTercero("TINA_Codigo").ToString())
'                    Me.objCataTipoIdentificacion.Existe_Registro(sdrTercero("TIID_Codigo").ToString())
'                    Long.TryParse(sdrTercero("Codigo").ToString(), Me.lonCodigo)
'                    Me.objCiudadExpedicionIdentificacion.Datos_Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Expedicion_Identificacion").ToString()))
'                    Integer.TryParse(sdrTercero("Digito_Chequeo").ToString(), Me.intDigitoChequeo)

'                    Me.strNombre = sdrTercero("Nombre").ToString()
'                    Me.strApellido1 = sdrTercero("Apellido1").ToString()
'                    Me.strApellido2 = sdrTercero("Apellido2").ToString()
'                    Me.objCiudadNacimiento.Datos_Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Nacimiento").ToString()))
'                    Date.TryParse(sdrTercero("Fecha_Nacimiento").ToString(), Me.dteFechaNacimiento)

'                    Me.objCataEstadoCivil.Existe_Registro(sdrTercero("ESCI_Codigo").ToString())
'                    Me.objCataSexo.Campo1 = (sdrTercero("Sexo").ToString())
'                    Me.strRepresentanteLegal = sdrTercero("Representante_legal").ToString()
'                    Me.objCiudad.Datos_Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Codigo").ToString()))
'                    Me.strDireccion = sdrTercero("Direccion").ToString()

'                    Me.strTelefono1 = sdrTercero("Telefono1").ToString()
'                    Me.strTelefono2 = sdrTercero("telefono2").ToString()
'                    Me.strFax = sdrTercero("Fax").ToString()
'                    Me.strCelular = sdrTercero("Celular").ToString()
'                    Me.strEmail = sdrTercero("Email").ToString()

'                    Me.strPaginaWeb = sdrTercero("Pagina_Web").ToString()
'                    Me.objCataRegimen.Existe_Registro(Val(sdrTercero("REGI_Codigo").ToString()))
'                    Me.strAdministracionImpuestos = sdrTercero("Administracion_Impuestos").ToString()
'                    Integer.TryParse(sdrTercero("Autoretenedor").ToString(), Me.intAutoretenedor)
'                    Me.objCataTipoSangre.Existe_Registro(sdrTercero("Tipo_Sangre").ToString())

'                    Me.strNumeroLicencia = sdrTercero("Numero_Licencia").ToString()
'                    Me.objCataEstado.Existe_Registro(sdrTercero("Estado").ToString())
'                    Me.intAplicaReteIca = Val(sdrTercero("Aplica_Rete_ICA"))
'                    Me.intAplicaReteFuente = Val(sdrTercero("Aplica_Rete_Fuente"))
'                    Me.intClienteExterior = Val(sdrTercero("Cliente_Exterior"))

'                    If Not IsNothing(Me.objRepresentanteComercial) Then
'                        Me.objRepresentanteComercial.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Representante_Comercial").ToString()))
'                    End If
'                    Me.objCataCategoriaLicenciaConductor.Campo1 = Val(sdrTercero("CALI_Codigo").ToString())
'                    Me.objCataTipoIdentificacionTitular.Existe_Registro(Me.objEmpresa, sdrTercero("TIID_Codigo_Titular").ToString())
'                    Integer.TryParse(sdrTercero("Digito_Chequeo_Titular").ToString(), Me.intDigitoChequeoTitular)
'                    Me.strNombreTitular = sdrTercero("Nombre_Titular").ToString()
'                    Me.strNumeroIdentificacionTitular = sdrTercero("Numero_Identificacion_Titular").ToString()
'                    Me.intConsultaListaClinton = Val(sdrTercero("Consulta_Lista_Clinton"))
'                    Me.intFacturaFaltantes = Val(sdrTercero("Factura_Faltantes"))


'                    If CargaCompleta Then
'                        Integer.TryParse(sdrTercero("Categoria_Licencia").ToString(), Me.intCategoriaLicencia)
'                        Date.TryParse(sdrTercero("Fecha_Licencia").ToString(), Me.dteFechaLicencia)
'                        Integer.TryParse(sdrTercero("Afiliado_EPS").ToString(), Me.intAfiliadoEPS)
'                        If Not IsNothing(Me.objEPS) Then
'                            Me.objEPS.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_EPS").ToString()))
'                        End If
'                        Date.TryParse(sdrTercero("Fecha_EPS").ToString(), Me.dteFechaAfiliacionEPS)
'                        Integer.TryParse(sdrTercero("Afiliado_ARP").ToString(), Me.intAfiliadoARP)

'                        If Not IsNothing(Me.objARP) Then
'                            Me.objARP.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_ARP").ToString()))
'                        End If
'                        Date.TryParse(sdrTercero("Fecha_ARP").ToString(), Me.dteFechaAfiliacionARP)
'                        Integer.TryParse(sdrTercero("Afiliado_Fondo_Pensiones").ToString(), Me.intAfiliadoFondoPensiones)
'                        If Not IsNothing(Me.objFondoPensiones) Then
'                            Me.objFondoPensiones.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Fondo_Pensiones").ToString()))
'                        End If
'                        Date.TryParse(sdrTercero("Fecha_Fondo_Pensiones").ToString(), Me.dteFechaAfiliacionFondoPensiones)
'                        Integer.TryParse(sdrTercero("Afiliado_Fondo_Cesantias").ToString(), Me.intAfiliadoFondoCesantias)
'                        If Not IsNothing(Me.objFondoCesantias) Then
'                            Me.objFondoCesantias.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Fondo_Cesantias").ToString()))
'                        End If
'                        Date.TryParse(sdrTercero("Fecha_Fondo_Cesantias").ToString(), Me.dteFechaAfiliacionFondoCesantias)

'                        Me.objCataTipoContrato.Existe_Registro(sdrTercero("TICT_Codigo").ToString())
'                        Date.TryParse(sdrTercero("Fecha_Contrato").ToString(), Me.dteFechaContrato)
'                        Date.TryParse(sdrTercero("Fecha_Vence_Contrato").ToString(), Me.dteFechaVenceContrato)
'                        Double.TryParse(sdrTercero("Salario_Basico").ToString(), Me.dblSalarioBasico)
'                        Integer.TryParse(sdrTercero("Conductor_Propio").ToString(), Me.intConductorPropio)

'                        Me.objBancoTransferencia.Codigo = Val(sdrTercero("BANC_Transfe").ToString())
'                        Me.strNumeroCuentaTransferencia = sdrTercero("Numero_Cuenta_Transfe").ToString()
'                        Me.objCataTipoCuentaTransferencia.Existe_Registro(sdrTercero("Tipo_Cuenta_Transfe").ToString())
'                        If Not IsNothing(Me.objBeneficiarioTransferencia) Then
'                            Me.objBeneficiarioTransferencia.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Beneficiario_Transfe").ToString()))
'                        End If
'                        Me.strCodigoTransferencia = sdrTercero("Codigo_Transfe").ToString()

'                        Me.strNombreConyuge = sdrTercero("Nombre_Conyuge").ToString()
'                        Integer.TryParse(sdrTercero("Numero_Hijos").ToString(), Me.intNumeroHijos)
'                        Me.objCataPeriodoLiquidacion.Existe_Registro(sdrTercero("PELI_Codigo").ToString())
'                        Me.objCataGrupoLiquidacion.Existe_Registro(sdrTercero("GRLI_Codigo").ToString())

'                        Me.strObservaciones = sdrTercero("Observaciones").ToString()
'                        Me.objOficinaEmpleado.Existe_Registro(Me.objEmpresa, Val(sdrTercero("OFIC_EMPL_Codigo")))
'                        Me.objCataCargo.Existe_Registro(sdrTercero("CARG_Codigo").ToString())
'                        Me.objCataCausaBloqueo.Existe_Registro(sdrTercero("CABL_Codigo").ToString())
'                        Me.objCataDiaPago.Existe_Registro(sdrTercero("DIPA_Codigo").ToString())

'                        Me.dblPorcentajeComision = Val(sdrTercero("Porcentaje_Comision").ToString())
'                        Me.strJustificacionBloqueo = sdrTercero("Justificacion_Bloqueo").ToString()
'                        If Not IsNothing(Me.objEstudioSeguridad) Then
'                            Me.objEstudioSeguridad.Existe_Registro(Me.objEmpresa, Val(sdrTercero("ESSE_Numero")))
'                        End If
'                        Me.intEsRepresentanteComercial = Val(sdrTercero("Representante_Comercial"))
'                        Me.strCodigoPostal = sdrTercero("Codigo_Postal").ToString()

'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio)
'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio)
'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero)
'                        Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero)

'                        If sdrTercero("Foto") IsNot DBNull.Value Then
'                            bolFoto = True
'                        Else
'                            bolFoto = False
'                        End If

'                        If sdrTercero("Huella_Digital") IsNot DBNull.Value Then
'                            bolHuella_Digital = True
'                        Else
'                            bolHuella_Digital = False
'                        End If

'                        Me.objCataCoordenadaUno.Existe_Registro(Val(sdrTercero("COOR_Uno_Codigo").ToString()))
'                        Integer.TryParse(sdrTercero("Coordenada_Uno_Valor").ToString(), Me.intCoordenadaUnoValor)
'                        Me.objCataCoordenadaDos.Existe_Registro(Val(sdrTercero("COOR_Dos_Codigo").ToString()))
'                        Integer.TryParse(sdrTercero("Coordenada_Dos_Valor").ToString(), Me.intCoordenadaDosValor)
'                        Integer.TryParse(sdrTercero("Zona_Especial").ToString(), Me.intZonaEspecial)

'                        Integer.TryParse(sdrTercero("Dias_Bloqueo_Mora").ToString(), Me.intDiasBloqueoMora)
'                        Me.objCataPeriodicidadFacturacionCliente.Existe_Registro(Me.objEmpresa, Val(sdrTercero("PEFC_Codigo").ToString()))
'                        Integer.TryParse(sdrTercero("Tiene_Seguro").ToString(), Me.intTieneSeguro)
'                        Me.strAseguradoraCliente = sdrTercero("Aseguradora_Cliente").ToString()
'                        Date.TryParse(sdrTercero("Fecha_Vencimiento_Seguro").ToString(), Me.dteFechaVencimientoSeguro)

'                        Me.objCataTipoCoberturaSeguro.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TCSC_Codigo").ToString()))
'                        Double.TryParse(sdrTercero("Monto_Maximo_Seguro").ToString(), Me.dblMontoMaximoSeguro)
'                        Me.objCataGarantiaSeguroCliente.Existe_Registro(Me.objEmpresa, Val(sdrTercero("GASC_Codigo").ToString))

'                        Integer.TryParse(sdrTercero("Cliente_BASC").ToString(), Me.intClienteBASC)
'                        Me.strCondicionesSeguro = sdrTercero("Condiciones_Seguro").ToString()
'                        Integer.TryParse(sdrTercero("Antiguedad_Vehiculo").ToString(), Me.intAntiguedadVehiculo)
'                        Integer.TryParse(sdrTercero("Cliente_Prospecto").ToString(), Me.intClienteProspecto)
'                        Me.strReferencioProspecto = sdrTercero("Referencio_Prospecto").ToString()
'                        Me.strReferenciasVerificadas = sdrTercero("Referencias_Verificadas").ToString()

'                        Me.intEsFilial = Val(sdrTercero("Es_Filial").ToString())
'                        Me.strCodigoFilial = sdrTercero("Codigo_Filial").ToString()
'                        Me.strCodigoContableFilial = sdrTercero("Codigo_Contable_Filial").ToString()
'                        Me.strCodigoActividad = sdrTercero("Codigo_Contable_Filial2").ToString

'                        If Not IsNothing(Me.objAseguradora) Then
'                            Me.objAseguradora.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TERC_Aseguradora").ToString()))
'                        End If

'                        Date.TryParse(sdrTercero("Fecha_Ingreso_Cliente").ToString(), Me.dteFechaIngresoCliente)
'                        Me.objCataTarifarioRepreComercial.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TARC_Codigo").ToString))
'                    Else
'                        Me.objOficinaEmpleado.Codigo = Val(sdrTercero("OFIC_EMPL_Codigo"))
'                        If Not IsNothing(Me.objRepresentanteComercial) Then
'                            Me.objRepresentanteComercial.Codigo = Val(sdrTercero("TERC_Representante_Comercial").ToString())
'                        End If
'                        If Not IsNothing(Me.objAseguradora) Then
'                            Me.objAseguradora.Codigo = Val(sdrTercero("TERC_Aseguradora").ToString())
'                        End If
'                        If Not IsNothing(Me.objBeneficiarioTransferencia) Then
'                            Me.objBeneficiarioTransferencia.Codigo = Val(sdrTercero("TERC_Beneficiario_Transfe").ToString())
'                        End If
'                    End If

'                    Cargar_Por_Identificacion = True
'                Else
'                    Me.lonCodigo = 0
'                    Cargar_Por_Identificacion = False
'                End If


'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception
'                Cargar_Por_Identificacion = False
'                Me.lonCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Por_Identificacion: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Cargar_Cliente() As Boolean
'            Try
'                Dim sdrTercero As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Codigo, TINA_Codigo, TIID_Codigo, Numero_Identificacion, "
'                strSQL += "CIUD_Expedicion_Identificacion, Digito_Chequeo, Nombre, Apellido1, Apellido2, "
'                strSQL += "CIUD_Codigo, CIUD_Nacimiento, Fecha_Nacimiento, ESCI_Codigo, Sexo, "
'                strSQL += "Representante_Legal, Direccion, Telefono1, Telefono2, Fax, "
'                strSQL += "Celular, Email, Pagina_Web, REGI_Codigo, Administracion_Impuestos, "
'                strSQL += "Autoretenedor, Tipo_Sangre, Numero_Licencia, Categoria_Licencia, Fecha_Licencia, "
'                strSQL += "CABL_Codigo, Porcentaje_Comision, DIPA_Codigo, TERC_Representante_Comercial, Fecha_Crea, "
'                strSQL += "USUA_Crea, Fecha_Modifica, USUA_Modifica, OFIC_Codigo, Justificacion_Bloqueo, "
'                strSQL += "Representante_Comercial, Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio, Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio, Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero,"
'                strSQL += " Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero, Postfijo, Aplica_Rete_ICA, Codigo_Postal, "
'                strSQL += "Dias_Bloqueo_Mora, PEFC_Codigo, Tiene_Seguro, Aseguradora_Cliente, Fecha_Vencimiento_Seguro, "
'                strSQL += "TCSC_Codigo, Monto_Maximo_Seguro, GASC_Codigo, Cliente_BASC, Condiciones_Seguro, "
'                strSQL += "Antiguedad_Vehiculo, Cliente_Prospecto, Referencio_Prospecto, Referencias_Verificadas, TERC_Aseguradora, "
'                strSQL += "TARC_Codigo, Es_Filial, Codigo_Filial, Codigo_Contable_Filial, Cliente_Exterior, Fecha_Ingreso_Cliente, "
'                strSQL += "'Foto' = CASE WHEN Foto IS NULL THEN NULL ELSE 1 END,  Aplica_Rete_Fuente, Codigo_Contable_Filial2, "
'                strSQL += "Fecha_Ultimo_Cargue, Estado, Consulta_Lista_Clinton, Factura_Faltantes, "
'                strSQL += "'Huella_Digital' = CASE WHEN Huella_Digital IS NULL THEN NULL ELSE 1 END"
'                strSQL += " FROM Terceros "
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Call Instanciar_Objetos_Alternos_Cliente()


'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then

'                    Me.objCataTipoNaturaleza.Existe_Registro(sdrTercero("TINA_Codigo").ToString())
'                    Me.objCataTipoIdentificacion.Existe_Registro(sdrTercero("TIID_Codigo").ToString())
'                    Me.strNumeroIdentificacion = sdrTercero("Numero_Identificacion").ToString()
'                    Me.objCiudadExpedicionIdentificacion.Datos_Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Expedicion_Identificacion").ToString()))
'                    Integer.TryParse(sdrTercero("Digito_Chequeo").ToString(), Me.intDigitoChequeo)

'                    Me.strNombre = sdrTercero("Nombre").ToString()
'                    Me.strApellido1 = sdrTercero("Apellido1").ToString()
'                    Me.strApellido2 = sdrTercero("Apellido2").ToString()
'                    Me.objCiudadNacimiento.Datos_Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Nacimiento").ToString()))
'                    Date.TryParse(sdrTercero("Fecha_Nacimiento").ToString(), Me.dteFechaNacimiento)

'                    Me.objCataEstadoCivil.Existe_Registro(sdrTercero("ESCI_Codigo").ToString())
'                    Me.objCataSexo.Existe_Registro(sdrTercero("Sexo").ToString())
'                    Me.strRepresentanteLegal = sdrTercero("Representante_legal").ToString()
'                    Me.objCiudad.Existe_Registro(Me.objEmpresa, Val(sdrTercero("CIUD_Codigo").ToString()))
'                    Me.strDireccion = sdrTercero("Direccion").ToString()

'                    Me.strTelefono1 = sdrTercero("Telefono1").ToString()
'                    Me.strTelefono2 = sdrTercero("Telefono2").ToString()
'                    Me.strFax = sdrTercero("Fax").ToString()
'                    Me.strCelular = sdrTercero("Celular").ToString()
'                    Me.strEmail = sdrTercero("Email").ToString()

'                    Me.strPaginaWeb = sdrTercero("Pagina_Web").ToString()
'                    Me.objCataRegimen.Existe_Registro(Val(sdrTercero("REGI_Codigo").ToString()))
'                    Me.strAdministracionImpuestos = sdrTercero("Administracion_Impuestos").ToString()
'                    Integer.TryParse(sdrTercero("Autoretenedor").ToString(), Me.intAutoretenedor)
'                    Me.objCataTipoSangre.Existe_Registro(sdrTercero("Tipo_Sangre").ToString())

'                    Me.strNumeroLicencia = sdrTercero("Numero_Licencia").ToString()
'                    Integer.TryParse(sdrTercero("Categoria_Licencia").ToString(), Me.intCategoriaLicencia)
'                    Date.TryParse(sdrTercero("Fecha_Licencia").ToString(), Me.dteFechaLicencia)
'                    Me.objCataEstado.Existe_Registro(sdrTercero("Estado").ToString())
'                    Me.intAplicaReteIca = Val(sdrTercero("Aplica_Rete_ICA"))

'                    Me.intAplicaReteFuente = Val(sdrTercero("Aplica_Rete_Fuente"))
'                    Me.intClienteExterior = Val(sdrTercero("Cliente_Exterior"))
'                    Date.TryParse(sdrTercero("Fecha_Ultimo_Cargue").ToString(), Me.dteFechaUltimoCargue)

'                    Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio)
'                    Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio)
'                    Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero)
'                    Double.TryParse(sdrTercero("Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero").ToString(), Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero)

'                    If Not IsNothing(Me.objRepresentanteComercial) Then
'                        Me.objRepresentanteComercial.Codigo = Val(sdrTercero("TERC_Representante_Comercial").ToString())
'                    End If
'                    Me.strJustificacionBloqueo = sdrTercero("Justificacion_Bloqueo").ToString()
'                    Me.intEsRepresentanteComercial = Val(sdrTercero("Representante_Comercial"))
'                    Me.strCodigoPostal = sdrTercero("Codigo_Postal").ToString()

'                    Integer.TryParse(sdrTercero("Dias_Bloqueo_Mora").ToString(), Me.intDiasBloqueoMora)
'                    Me.objCataPeriodicidadFacturacionCliente.Existe_Registro(Me.objEmpresa, Val(sdrTercero("PEFC_Codigo").ToString()))
'                    Integer.TryParse(sdrTercero("Tiene_Seguro").ToString(), Me.intTieneSeguro)
'                    Me.strAseguradoraCliente = sdrTercero("Aseguradora_Cliente").ToString()
'                    Date.TryParse(sdrTercero("Fecha_Vencimiento_Seguro").ToString(), Me.dteFechaVencimientoSeguro)

'                    Me.objCataTipoCoberturaSeguro.Existe_Registro(Me.objEmpresa, Val(sdrTercero("TCSC_Codigo").ToString()))
'                    Double.TryParse(sdrTercero("Monto_Maximo_Seguro").ToString(), Me.dblMontoMaximoSeguro)
'                    Me.objCataGarantiaSeguroCliente.Existe_Registro(Me.objEmpresa, Val(sdrTercero("GASC_Codigo").ToString))

'                    Integer.TryParse(sdrTercero("Cliente_BASC").ToString(), Me.intClienteBASC)
'                    Me.strCondicionesSeguro = sdrTercero("Condiciones_Seguro").ToString()
'                    Integer.TryParse(sdrTercero("Cliente_Prospecto").ToString(), Me.intClienteProspecto)
'                    Me.strReferencioProspecto = sdrTercero("Referencio_Prospecto").ToString()
'                    Me.strReferenciasVerificadas = sdrTercero("Referencias_Verificadas").ToString()

'                    Me.intEsFilial = Val(sdrTercero("Es_Filial").ToString())
'                    Me.strCodigoFilial = sdrTercero("Codigo_Filial").ToString()
'                    Me.strCodigoContableFilial = sdrTercero("Codigo_Contable_Filial").ToString()
'                    Me.strCodigoActividad = sdrTercero("Codigo_Contable_Filial2").ToString
'                    Integer.TryParse(sdrTercero("Antiguedad_Vehiculo").ToString(), Me.intAntiguedadVehiculo)

'                    Date.TryParse(sdrTercero("Fecha_Ingreso_Cliente").ToString(), Me.dteFechaIngresoCliente)
'                    Me.objCataDiaPago.Existe_Registro(sdrTercero("DIPA_Codigo").ToString())

'                    If Not IsNothing(Me.objAseguradora) Then
'                        Me.objAseguradora.Codigo = Val(sdrTercero("TERC_Aseguradora").ToString())
'                    End If

'                    If sdrTercero("Foto") IsNot DBNull.Value Then
'                        bolFoto = True
'                    Else
'                        bolFoto = False
'                    End If

'                    'If sdrTercero("Huella_Digital") IsNot DBNull.Value Then
'                    '    bolHuella_Digital = True
'                    'Else
'                    '    bolHuella_Digital = False
'                    'End If

'                    Me.intConsultaListaClinton = Val(sdrTercero("Consulta_Lista_Clinton"))
'                    Me.intFacturaFaltantes = Val(sdrTercero("Factura_Faltantes"))


'                    Cargar_Cliente = True
'                Else
'                    Me.lonCodigo = 0
'                    Cargar_Cliente = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Cargar_Cliente = False
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Cliente: " & ex.Message.ToString()
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Cliente: " & ex.Message.ToString()
'                End Try
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Tiene_Verificacion(ByVal EmpresaVerificacion As Integer) As Boolean
'            Try
'                Tiene_Verificacion = True

'                Dim sdrVerificacion As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo FROM Detalle_Entidades_Verificacion_Creacion_Clientes"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Codigo = " & Me.lonCodigo
'                strSQL += " AND EVCC_Codigo = " & EmpresaVerificacion

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrVerificacion = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrVerificacion.Read() Then
'                    Tiene_Verificacion = True
'                Else
'                    Tiene_Verificacion = False
'                End If

'                sdrVerificacion.Close()
'                sdrVerificacion.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Tiene_Verificacion: " & ex.Message.ToString()
'                Tiene_Verificacion = False
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try

'        End Function

'        Public Function Tiene_Perfil(ByVal Perfil As Integer) As Boolean
'            Try
'                Tiene_Perfil = True

'                Dim sdrTercero As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo FROM Perfil_Terceros"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Codigo = " & Me.lonCodigo
'                strSQL += " AND Codigo = " & Perfil

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then
'                    Tiene_Perfil = True
'                Else
'                    Tiene_Perfil = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Tiene_Perfil: " & ex.Message.ToString()
'                Tiene_Perfil = False
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try

'        End Function

'        Public Function Cargar_Perfil_Terceros(ByRef Empresa As Empresas, ByVal Codigo As Long) As DataSet
'            Try
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Dim daDataAdapter As SqlDataAdapter
'                Dim dsPerfilTerceros As DataSet = New DataSet

'                strSQL = "SELECT CodigoPerfil FROM V_Tercero_Perfiles"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Codigo = " & Me.lonCodigo

'                daDataAdapter = New SqlDataAdapter(strSQL, Me.objGeneral.ConexionSQL)
'                daDataAdapter.Fill(dsPerfilTerceros)

'                Cargar_Perfil_Terceros = dsPerfilTerceros

'                daDataAdapter.Dispose()
'                dsPerfilTerceros.Dispose()

'            Catch ex As Exception
'                Cargar_Perfil_Terceros = New DataSet("Vacio")
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Perfil_Terceros: " & ex.Message.ToString()
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Public Function Existe_Tercero_Con_Perfil(ByVal Identificacion As String, ByVal Perfil As Integer) As Boolean
'            Try
'                If Me.Cargar_Por_Identificacion(Me.objEmpresa, Identificacion) Then
'                    If Me.Tiene_Perfil(Perfil) Then
'                        Existe_Tercero_Con_Perfil = True
'                    Else
'                        Existe_Tercero_Con_Perfil = False
'                    End If
'                Else
'                    Existe_Tercero_Con_Perfil = False
'                End If
'            Catch ex As Exception
'                Existe_Tercero_Con_Perfil = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Tercero_Con_Perfil: " & ex.Message.ToString()
'            End Try
'        End Function

'        Public Function Retorna_Identificacion(ByRef Empresa As Empresas, ByVal Codigo As Long) As Boolean
'            Try
'                Retorna_Identificacion = True

'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Dim sdrTercero As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT * FROM Terceros"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then
'                    Me.strNumeroIdentificacion = sdrTercero("Numero_Identificacion").ToString()
'                    Retorna_Identificacion = True
'                Else
'                    Me.lonCodigo = 0
'                    Retorna_Identificacion = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Retorna_Identificacion = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Retorna_Identificacion: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Guardar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Const REPLICA_INFORMACION As String = "Replica la información a todas las empresas"
'                Dim intCont As Integer
'                Dim intCodigoInicialEmpresa As Integer
'                Dim sdsEmpresas As DataSet

'                If Datos_Requeridos(Mensaje) Then

'                    Me.objGeneral.ConexionSQL.Open()
'                    Me.objGeneral.ComandoSQL = Me.objGeneral.ConexionSQL.CreateCommand()
'                    Me.objGeneral.ComandoSQL.Connection = Me.objGeneral.ConexionSQL()

'                    Me.objGeneral.TransaccionSQL = Me.objGeneral.ConexionSQL.BeginTransaction()
'                    Me.objGeneral.ComandoSQL.Transaction = Me.objGeneral.TransaccionSQL

'                    If Not bolModificar Then
'                        Guardar = Insertar_SQL()
'                    Else
'                        Guardar = Modificar_SQL()
'                    End If
'                    If Guardar Then
'                        If Me.bolGuardaPerfilCliente Then
'                            Me.PerfilTercero(PERFIL_CLIENTE) = General.ESTADO_ACTIVO
'                        End If

'                        If Me.bolGuardarPerfilDestinatario Then
'                            Guardar = Guardar_Perfil_Destinatario()
'                        Else
'                            If Guardar_Perfil() Then
'                                Guardar = True
'                                If Me.PerfilTercero(PERFIL_CLIENTE) = General.ESTADO_ACTIVO And Me.objRepresentanteComercial.Codigo = 0 Then
'                                    Me.objRepresentanteComercial.Codigo = General.UNO
'                                End If
'                            Else
'                                Guardar = False
'                            End If
'                        End If

'                        'instrucciones para guardar detalle seguro cliente
'                        If Me.TieneSeguro = TIENE_SEGURO Then
'                            Guardar = Me.Insertar_Detalle_Seguro_SQL
'                        End If

'                    End If
'                    If Guardar Then
'                        Borrar_Documento_Entregado()
'                        If Guardar_Documento_Entregado() Then
'                            Guardar = True
'                        End If
'                    End If

'                    If Guardar Then
'                        If Me.objValidacion.Consultar(Me.objEmpresa, REPLICA_INFORMACION) Then

'                            intCodigoInicialEmpresa = Me.objEmpresa.Codigo
'                            Me.objControlAuditoria.CodigoEmpresa = Me.objEmpresa.Codigo

'                            strSQL = "SELECT Codigo FROM Empresas WHERE Codigo <> " & Me.objEmpresa.Codigo
'                            sdsEmpresas = Me.objGeneral.Retorna_Dataset(strSQL, Me.strError)
'                            If Len(Trim(Me.strError)) = 0 Then
'                                If sdsEmpresas.Tables(Lista.PRIMERA_TABLA).Rows.Count > 0 Then
'                                    For intCont = 0 To sdsEmpresas.Tables(Lista.PRIMERA_TABLA).Rows.Count - 1
'                                        Me.objEmpresa.Consultar(Val(sdsEmpresas.Tables(Lista.PRIMERA_TABLA).Rows(intCont).Item("Codigo").ToString()))
'                                        If Datos_Requeridos(Mensaje) Then
'                                            If Not bolModificar Then
'                                                Guardar = Insertar_SQL()
'                                            Else
'                                                Guardar = Modificar_SQL()
'                                            End If
'                                        End If
'                                        Mensaje = Me.strError
'                                    Next
'                                End If
'                            End If
'                            Me.objEmpresa.Consultar(intCodigoInicialEmpresa)
'                            sdsEmpresas.Dispose()

'                        End If

'                    End If
'                    If Guardar Then
'                        If Me.Prospecto Then
'                            If Guardar_Verificacion_Creacion() Then
'                                Guardar = True
'                            End If
'                        End If
'                    End If

'                    If bolEsEmpleado And Guardar Then
'                        Call Insertar_Detalle_Curso_Formacion_Empleados()
'                    End If
'                    If bolEsEmpleado And Guardar Then
'                        Call Insertar_Hijo_Empleados()
'                    End If
'                    If bolEsEmpleado And Guardar Then
'                        Call Insertar_Detalle_Examen_Medico_Empleados()
'                    End If

'                    If Guardar Then
'                        Guardar = Guardar_Direcciones_Terceros()
'                    End If

'                    If Guardar Then
'                        Me.objGeneral.TransaccionSQL.Commit()
'                    Else
'                        Me.objGeneral.TransaccionSQL.Dispose()
'                        Me.objGeneral.TransaccionSQL.Rollback()
'                    End If

'                    If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                        Me.objGeneral.ConexionSQL.Close()
'                    End If

'                    If Guardar Then
'                        If Val(Me.objCataEstado.Retorna_Codigo) = ESTADO_ACTIVO Then
'                            If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_FECHAS_VENCIDAS_CONDUCTOR) Then
'                                If Me.Tiene_Perfil(PERFIL_CONDUCTOR) Then
'                                    Verificar_Fechas_Vencidas()
'                                End If
'                            End If
'                        End If
'                    End If
'                Else
'                    Guardar = False
'                End If

'            Catch ex As Exception
'                Guardar = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar: " & ex.Message.ToString()
'            End Try
'            Mensaje = Me.strError

'        End Function

'        Public Function Cambiar_Estado(ByVal intEstado As Integer, ByVal intCausaBloqueo As Integer) As Boolean
'            Try
'                Dim strSQL As String = ""

'                strSQL = "UPDATE Terceros"
'                strSQL += " SET Estado = " & intEstado
'                strSQL += " ,CABL_Codigo = " & intCausaBloqueo
'                strSQL += " ,USUA_Modifica = '" & objUsuario.Codigo & "'"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & lonCodigo

'                Cambiar_Estado = True
'                Me.objGeneral.Ejecutar_SQL(strSQL)
'            Catch ex As Exception
'                Cambiar_Estado = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cambiar_Estado: " & ex.Message.ToString()
'            End Try
'        End Function

'        Public Function Insertar_T_Direcciones_Terceros(ByRef Mensaje As String, Perfil As Integer) As Boolean
'            Try
'                Dim lonNumeRegi As Long
'                Dim intIdDireccion As Integer

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_t_detalle_direccion_terceros", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.objUsuario.Codigo
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_ID", SqlDbType.Int) : ComandoSQL.Parameters("@par_ID").Value = Val(intIdDireccion)
'                ComandoSQL.Parameters.Add("@par_CIUD_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_CIUD_Codigo").Value = Me.objCiudad.Codigo
'                ComandoSQL.Parameters.Add("@par_Direccion", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Direccion").Value = Me.strDireccion
'                ComandoSQL.Parameters.Add("@par_Telefono", SqlDbType.VarChar, 15) : ComandoSQL.Parameters("@par_Telefono").Value = Me.strTelefono1
'                ComandoSQL.Parameters.Add("@par_PETE_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_PETE_Codigo").Value = Perfil
'                ComandoSQL.Parameters.Add("@par_Mensaje", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Mensaje").Direction = ParameterDirection.Output

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_T_Direcciones_Terceros = True
'                Else
'                    Me.strError = ComandoSQL.Parameters("@par_Mensaje").Value
'                    Insertar_T_Direcciones_Terceros = False
'                End If

'            Catch ex As Exception
'                Insertar_T_Direcciones_Terceros = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_T_Direcciones_Terceros: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'                Mensaje = Me.strError
'            End Try
'        End Function

'        Public Function Trasladar_Direcciones_A_Temporal(ByVal Mensaje As String, ByVal CodigoTercero As Integer, ByVal Perfil As Integer) As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_trasladar_detalle_direccion_terceros", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.objUsuario.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Codigo").Value = CodigoTercero
'                ComandoSQL.Parameters.Add("@par_PETE_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_PETE_Codigo").Value = Perfil

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Trasladar_Direcciones_A_Temporal = True
'                Else
'                    Trasladar_Direcciones_A_Temporal = False
'                End If

'            Catch ex As Exception
'                Trasladar_Direcciones_A_Temporal = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_Foto: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'                Mensaje = Me.strError
'            End Try
'        End Function

'        Public Function Eliminar_Direccion_Terceros(ByVal Mensaje As String, ByVal intID As Integer, ByVal Perfil As Integer) As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_eliminar_t_detalle_direccion_terceros", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_ID", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_ID").Value = intID
'                ComandoSQL.Parameters.Add("@par_PETE_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_PETE_Codigo").Value = Perfil
'                ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.objUsuario.Codigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Eliminar_Direccion_Terceros = True
'                Else
'                    Eliminar_Direccion_Terceros = False
'                End If

'            Catch ex As Exception
'                Eliminar_Direccion_Terceros = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_Foto: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'                Mensaje = Me.strError
'            End Try
'        End Function

'        Public Function Eliminar_Foto(ByVal Codigo As Long) As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_eliminar_foto_tercero", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Value = Codigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Eliminar_Foto = True
'                Else
'                    Eliminar_Foto = False
'                End If

'            Catch ex As Exception
'                Eliminar_Foto = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_Foto: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Eliminar_Huella_Digital(ByVal Codigo As Long) As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_eliminar_huella_tercero", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Value = Codigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Eliminar_Huella_Digital = True
'                Else
'                    Eliminar_Huella_Digital = False
'                End If

'            Catch ex As Exception
'                Eliminar_Huella_Digital = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_Huella_Digital: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Guardar_Verificacion_Creacion(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Dim lonNumeRegi As Long
'                Guardar_Verificacion_Creacion = False

'                For Me.intCon = 0 To 15
'                    If Me.VerificacionCliente(intCon) <> 0 Then
'                        Me.objGeneral.ComandoSQL.CommandText = "sp_insertar_entidades_verificacion_creacion_clientes"
'                        Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                        Me.objGeneral.ComandoSQL.Parameters.Clear()

'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_EVCC_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_EVCC_Codigo").Value = Me.VerificacionCliente(intCon)

'                        lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                        If lonNumeRegi = 1 Then
'                            Guardar_Verificacion_Creacion = True
'                        Else
'                            Guardar_Verificacion_Creacion = False
'                        End If
'                    End If
'                Next
'            Catch ex As Exception
'                Guardar_Verificacion_Creacion = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar_Verificacion_Creacion: " & ex.Message.ToString()
'                Mensaje = Me.strError
'            End Try
'            Mensaje = Me.strError
'        End Function

'        Public Function Guardar_Perfil(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Dim lonNumeRegi As Long
'                Guardar_Perfil = True

'                For Me.intCon = 0 To 15
'                    If Me.PerfilTercero(intCon) > 0 Then

'                        Me.objGeneral.ComandoSQL.CommandText = "sp_insertar_perfil_terceros"
'                        Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                        Me.objGeneral.ComandoSQL.Parameters.Clear()

'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo").Value = Me.PerfilTercero(intCon)

'                        lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                        If lonNumeRegi >= 1 Then
'                            Guardar_Perfil = True
'                            If Me.bolAuditar Then
'                                Me.objControlAuditoria.NumeroDocumentoEncabezado = Me.lonCodigo
'                                Me.objControlAuditoria.NumeroDocumentoDetalle = Me.PerfilTercero(intCon)
'                                Me.objControlAuditoria.Insertar_Control_Auditoria_SQL(Me.objGeneral.ComandoSQL.Parameters, "Perfil_Terceros", ControlAuditoria.TRANSACCION_INSERTAR, Me.strError)
'                            End If
'                        Else
'                            Guardar_Perfil = False
'                        End If
'                    End If
'                Next

'            Catch ex As Exception
'                Guardar_Perfil = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar_Perfil: " & ex.Message.ToString()
'                Mensaje = Me.strError
'            End Try
'            Mensaje = Me.strError
'        End Function

'        Public Function Activar() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_activar_tercero", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_CABL_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_CABL_Codigo").Value = Val(Me.objCataCausaBloqueo.Retorna_Codigo())
'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Activar = True
'                Else
'                    Activar = False
'                End If

'            Catch ex As Exception
'                Activar = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Activar: " & ex.Message.ToString()
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Public Function Activar(ByVal CataCausaBloqueo As Catalogo) As Boolean
'            Try
'                Dim lonNumeRegi As Long
'                Me.objCataCausaBloqueo = CataCausaBloqueo

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_activar_tercero", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_CABL_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_CABL_Codigo").Value = Val(Me.objCataCausaBloqueo.Retorna_Codigo())
'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Activar = True
'                Else
'                    Activar = False
'                End If

'            Catch ex As Exception
'                Activar = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Activar: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Bloquear() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_bloquear_tercero", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_CABL_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_CABL_Codigo").Value = Val(Me.objCataCausaBloqueo.Retorna_Codigo())
'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Bloquear = True
'                Else
'                    Bloquear = False
'                End If

'            Catch ex As Exception
'                Bloquear = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Bloquear: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Inactivar() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_inactivar_tercero", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_CABL_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_CABL_Codigo").Value = Val(Me.objCataCausaBloqueo.Retorna_Codigo())
'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Inactivar = True
'                Else
'                    Inactivar = False
'                End If

'            Catch ex As Exception
'                Inactivar = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Inactivar: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Inactivar(ByVal CataCausaBloqueo As Catalogo) As Boolean
'            Try
'                Dim lonNumeRegi As Long
'                Me.objCataCausaBloqueo = CataCausaBloqueo

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_inactivar_tercero", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_CABL_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_CABL_Codigo").Value = Val(Me.objCataCausaBloqueo.Retorna_Codigo())
'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Inactivar = True
'                Else
'                    Inactivar = False
'                End If

'            Catch ex As Exception
'                Inactivar = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Inactivar: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Verificar_Fechas_Vencidas() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_deshabilitar_conductores", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Verificar_Fechas_Vencidas = True
'                Else
'                    Verificar_Fechas_Vencidas = False
'                End If

'            Catch ex As Exception
'                Verificar_Fechas_Vencidas = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Verificar_Fechas_Vencidas: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Agregar_Sitio(ByVal Empresa As Empresas, ByVal SitioCargueDescargue As SitioCargueDescargue, Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Me.objEmpresa = Empresa
'                Me.objSitioCargueDescargue = SitioCargueDescargue

'                If Datos_Cargue_Descargue_Tercero() Then
'                    Dim lonNumeRegi As Long

'                    Me.objGeneral.ConexionSQL.Open()

'                    Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_sitio_cargue_descargue_terceros", Me.objGeneral.ConexionSQL)

'                    ComandoSQL.CommandType = CommandType.StoredProcedure
'                    ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                    ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                    ComandoSQL.Parameters.Add("@par_SICD_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_SICD_Codigo").Value = Me.objSitioCargueDescargue.Codigo
'                    ComandoSQL.Parameters.Add("@par_Mensaje", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Mensaje").Direction = ParameterDirection.Output

'                    lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                    If lonNumeRegi > 0 Then
'                        Agregar_Sitio = True
'                    Else
'                        Agregar_Sitio = False
'                        Mensaje = ComandoSQL.Parameters("@par_Mensaje").Value
'                    End If
'                Else
'                    Mensaje = Me.strError
'                    Agregar_Sitio = False
'                End If
'            Catch ex As Exception
'                Agregar_Sitio = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Agregar_Sitio: " & ex.Message.ToString()
'                Mensaje = Me.strError
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Agregar_Sitio(ByVal General As General, ByVal Empresa As Empresas, ByVal SitioCargueDescargue As SitioCargueDescargue, Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Me.objEmpresa = Empresa
'                Me.objSitioCargueDescargue = SitioCargueDescargue

'                If Datos_Cargue_Descargue_Tercero() Then
'                    Dim lonNumeRegi As Long

'                    General.ComandoSQL.CommandText = "sp_insertar_sitio_cargue_descargue_terceros"
'                    General.ComandoSQL.CommandType = CommandType.StoredProcedure
'                    General.ComandoSQL.Parameters.Clear()

'                    General.ComandoSQL.CommandType = CommandType.StoredProcedure
'                    General.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : General.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                    General.ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Int) : General.ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                    General.ComandoSQL.Parameters.Add("@par_SICD_Codigo", SqlDbType.Int) : General.ComandoSQL.Parameters("@par_SICD_Codigo").Value = Me.objSitioCargueDescargue.Codigo
'                    lonNumeRegi = Val(General.ComandoSQL.ExecuteNonQuery().ToString())

'                    If lonNumeRegi > 0 Then
'                        Agregar_Sitio = True
'                    Else
'                        Agregar_Sitio = False
'                    End If
'                Else
'                    Mensaje = Me.strError
'                    Agregar_Sitio = False
'                End If
'            Catch ex As Exception
'                Agregar_Sitio = False
'                Mensaje = "En la clase " & Me.ToString & " se presentó un error en la función Agregar_Sitio: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Eliminar_Sitio(ByVal Empresa As Empresas, ByVal CodigoSitioCargueDescargue As Long, Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_sitio_cargue_descargue_terceros", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_SICD_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_SICD_Codigo").Value = CodigoSitioCargueDescargue

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Eliminar_Sitio = True
'                Else
'                    Eliminar_Sitio = False
'                End If

'            Catch ex As Exception
'                Eliminar_Sitio = False
'                Mensaje = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar_Sitio: " & ex.Message.ToString()
'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                If Puede_Borrar() Then
'                    Eliminar = Borrar_SQL()
'                Else
'                    Eliminar = False
'                End If
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Eliminar = False
'                Mensaje = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar: " & ex.Message.ToString()
'            End Try
'        End Function

'        Public Function Es_Conductor_Vehiculo(Optional ByRef Placa As String = "") As Boolean
'            Try
'                Dim sdrEsConductor As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Placa"
'                strSQL += " FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Conductor = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()

'                sdrEsConductor = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEsConductor.Read Then
'                    Es_Conductor_Vehiculo = True
'                    Placa = sdrEsConductor.Item(0).ToString
'                Else
'                    Es_Conductor_Vehiculo = False
'                    Placa = ""
'                End If

'                sdrEsConductor.Close()
'                sdrEsConductor.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Es_Conductor_Vehiculo: " & ex.Message.ToString()
'                Es_Conductor_Vehiculo = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Es_Conductor_Manifiesto(Optional ByRef Manifiesto As String = "") As Boolean
'            Try
'                Dim sdrEsConductor As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Numero"
'                strSQL += " FROM Encabezado_Manifiestos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Conductor = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()

'                sdrEsConductor = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEsConductor.Read Then
'                    Es_Conductor_Manifiesto = True
'                    Manifiesto = sdrEsConductor.Item(0).ToString
'                Else
'                    Es_Conductor_Manifiesto = False
'                    Manifiesto = ""
'                End If

'                sdrEsConductor.Close()
'                sdrEsConductor.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Es_Conductor_Vehiculo: " & ex.Message.ToString()
'                Es_Conductor_Manifiesto = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Tiene_Contrato_Cliente(Optional ByRef Orden As String = "") As Boolean
'            Try
'                Dim sdrTieneContratoCliente As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Numero"
'                strSQL += " FROM Encabezado_Contrato_Clientes"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()

'                sdrTieneContratoCliente = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTieneContratoCliente.Read Then
'                    Tiene_Contrato_Cliente = True
'                    Orden = sdrTieneContratoCliente.Item(0).ToString
'                Else
'                    Tiene_Contrato_Cliente = False
'                    Orden = ""
'                End If

'                sdrTieneContratoCliente.Close()
'                sdrTieneContratoCliente.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Tiene_Contrato_Cliente: " & ex.Message.ToString()
'                Tiene_Contrato_Cliente = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_En_Contrato_Cliente(Optional ByRef Orden As String = "") As Boolean
'            Try
'                Dim sdrTieneContratoCliente As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Numero"
'                strSQL += " FROM Encabezado_Contrato_Clientes"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Remitente = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()

'                sdrTieneContratoCliente = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTieneContratoCliente.Read Then
'                    Existe_En_Contrato_Cliente = True
'                    Orden = sdrTieneContratoCliente.Item(0).ToString
'                Else
'                    Existe_En_Contrato_Cliente = False
'                    Orden = ""
'                End If

'                sdrTieneContratoCliente.Close()
'                sdrTieneContratoCliente.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_En_Contrato_Cliente: " & ex.Message.ToString()
'                Existe_En_Contrato_Cliente = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Tiene_Comprobante(Optional ByRef Orden As String = "") As Boolean
'            Try
'                Dim sdrComprobante As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Numero"
'                strSQL += " FROM Encabezado_Documento_Comprobantes"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Codigo = " & Me.lonCodigo
'                strSQL += " OR TERC_Beneficiario = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrComprobante = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrComprobante.Read Then
'                    Tiene_Comprobante = True
'                    Orden = sdrComprobante.Item(0).ToString
'                Else
'                    Tiene_Comprobante = False
'                    Orden = ""
'                End If

'                sdrComprobante.Close()
'                sdrComprobante.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Tiene_Comprobante: " & ex.Message.ToString()
'                Tiene_Comprobante = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function


'        Public Function Tiene_Orden_Compra(Optional ByRef Orden As String = "") As Boolean
'            Try
'                Dim sdrTieneContratoCliente As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 ID"
'                strSQL += " FROM Encabezado_Orden_Compras"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrTieneContratoCliente = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTieneContratoCliente.Read Then
'                    Tiene_Orden_Compra = True
'                    Orden = sdrTieneContratoCliente.Item(0).ToString
'                Else
'                    Tiene_Orden_Compra = False
'                    Orden = ""
'                End If

'                sdrTieneContratoCliente.Close()
'                sdrTieneContratoCliente.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Tiene_Orden_Compra: " & ex.Message.ToString()
'                Tiene_Orden_Compra = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Repre_Comercial_Tiene_Contrato_Cliente(Optional ByRef Orden As String = "") As Boolean
'            Try
'                Dim sdrTieneContratoCliente As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Numero"
'                strSQL += " FROM Encabezado_Cotizacion_Servicio_Transportes"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Comercial = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrTieneContratoCliente = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTieneContratoCliente.Read Then
'                    Repre_Comercial_Tiene_Contrato_Cliente = True
'                    Orden = sdrTieneContratoCliente.Item(0).ToString
'                Else
'                    Repre_Comercial_Tiene_Contrato_Cliente = False
'                    Orden = ""
'                End If

'                sdrTieneContratoCliente.Close()
'                sdrTieneContratoCliente.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Repre_Comercial_Tiene_Contrato_Cliente: " & ex.Message.ToString()
'                Repre_Comercial_Tiene_Contrato_Cliente = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Tiene_Factura_Cliente(Optional ByRef Orden As String = "") As Boolean
'            Try
'                Dim sdrTieneContratoCliente As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Numero"
'                strSQL += " FROM Encabezado_Facturas"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Cliente = " & Me.lonCodigo
'                strSQL += " OR TERC_Facturar = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrTieneContratoCliente = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTieneContratoCliente.Read Then
'                    Tiene_Factura_Cliente = True
'                    Orden = sdrTieneContratoCliente.Item(0).ToString
'                Else
'                    Tiene_Factura_Cliente = False
'                    Orden = ""
'                End If

'                sdrTieneContratoCliente.Close()
'                sdrTieneContratoCliente.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Tiene_Factura_Cliente: " & ex.Message.ToString()
'                Tiene_Factura_Cliente = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Es_Empresa_Afiliadora_Vehiculo(Optional ByRef Placa As String = "") As Boolean
'            Try
'                Dim sdrEsEmpresaAfiliadoraVehiculo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Placa"
'                strSQL += " FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Afiliador = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrEsEmpresaAfiliadoraVehiculo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEsEmpresaAfiliadoraVehiculo.Read Then
'                    Es_Empresa_Afiliadora_Vehiculo = True
'                    Placa = sdrEsEmpresaAfiliadoraVehiculo.Item(0).ToString
'                Else
'                    Es_Empresa_Afiliadora_Vehiculo = False
'                    Placa = ""
'                End If

'                sdrEsEmpresaAfiliadoraVehiculo.Close()
'                sdrEsEmpresaAfiliadoraVehiculo.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Tiene_Factura_Cliente: " & ex.Message.ToString()
'                Es_Empresa_Afiliadora_Vehiculo = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Es_Propietario_Vehiculo(Optional ByRef Placa As String = "") As Boolean
'            Try
'                Dim sdrEsPropietarioVehiculo0 As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Placa"
'                strSQL += " FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Propietario = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrEsPropietarioVehiculo0 = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEsPropietarioVehiculo0.Read Then
'                    Es_Propietario_Vehiculo = True
'                    Placa = sdrEsPropietarioVehiculo0.Item(0).ToString
'                Else
'                    Es_Propietario_Vehiculo = False
'                    Placa = ""
'                End If

'                sdrEsPropietarioVehiculo0.Close()
'                sdrEsPropietarioVehiculo0.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Es_Propietario_Vehiculo: " & ex.Message.ToString()
'                Es_Propietario_Vehiculo = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Es_Propietario_Semiremolque(Optional ByRef Placa As String = "") As Boolean
'            Try
'                Dim sdrEsPropietarioVehiculo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Placa"
'                strSQL += " FROM Semiremolques"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrEsPropietarioVehiculo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEsPropietarioVehiculo.Read Then
'                    Es_Propietario_Semiremolque = True
'                    Placa = sdrEsPropietarioVehiculo.Item(0).ToString
'                Else
'                    Es_Propietario_Semiremolque = False
'                    Placa = ""
'                End If

'                sdrEsPropietarioVehiculo.Close()
'                sdrEsPropietarioVehiculo.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Es_Propietario_Semiremolque: " & ex.Message.ToString()
'                Es_Propietario_Semiremolque = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Es_Propietario_Manifiesto(Optional ByRef Manifiesto As String = "") As Boolean
'            Try
'                Dim sdrEsPropietarioManifiesto As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Numero"
'                strSQL += " FROM Encabezado_Manifiestos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Propietario = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrEsPropietarioManifiesto = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEsPropietarioManifiesto.Read Then
'                    Es_Propietario_Manifiesto = True
'                    Manifiesto = sdrEsPropietarioManifiesto.Item(0).ToString
'                Else
'                    Es_Propietario_Manifiesto = False
'                    Manifiesto = ""
'                End If

'                sdrEsPropietarioManifiesto.Close()
'                sdrEsPropietarioManifiesto.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Es_Propietario_Manifiesto: " & ex.Message.ToString()
'                Es_Propietario_Manifiesto = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Es_Tenedor_Vehiculo(Optional ByRef Placa As String = "") As Boolean
'            Try
'                Dim sdrEsTenedorVehiculo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Placa"
'                strSQL += " FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Tenedor = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrEsTenedorVehiculo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEsTenedorVehiculo.Read Then
'                    Es_Tenedor_Vehiculo = True
'                    Placa = sdrEsTenedorVehiculo.Item(0).ToString
'                Else
'                    Es_Tenedor_Vehiculo = False
'                    Placa = ""
'                End If

'                sdrEsTenedorVehiculo.Close()
'                sdrEsTenedorVehiculo.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Es_Tenedor_Vehiculo: " & ex.Message.ToString()
'                Es_Tenedor_Vehiculo = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Es_Tenedor_Manifiesto(Optional ByRef Manifiesto As String = "") As Boolean
'            Try
'                Dim sdrEsTenedorManifiesto As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Numero"
'                strSQL += " FROM Encabezado_Manifiestos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Tenedor = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrEsTenedorManifiesto = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEsTenedorManifiesto.Read Then
'                    Es_Tenedor_Manifiesto = True
'                    Manifiesto = sdrEsTenedorManifiesto.Item(0).ToString
'                Else
'                    Es_Tenedor_Manifiesto = False
'                    Manifiesto = ""
'                End If

'                sdrEsTenedorManifiesto.Close()
'                sdrEsTenedorManifiesto.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Es_Tenedor_Manifiesto: " & ex.Message.ToString()
'                Es_Tenedor_Manifiesto = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Es_Aseguradora_Vehiculo(Optional ByRef Placa As String = "") As Boolean
'            Try
'                Dim sdrEsAseguradoraVehiculo As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Placa"
'                strSQL += " FROM Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Aseguradora = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrEsAseguradoraVehiculo = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEsAseguradoraVehiculo.Read Then
'                    Es_Aseguradora_Vehiculo = True
'                    Placa = sdrEsAseguradoraVehiculo.Item(0).ToString
'                Else
'                    Es_Aseguradora_Vehiculo = False
'                    Placa = ""
'                End If

'                sdrEsAseguradoraVehiculo.Close()
'                sdrEsAseguradoraVehiculo.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Es_Aseguradora_Vehiculo: " & ex.Message.ToString()
'                Es_Aseguradora_Vehiculo = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Es_Usuario_Sistema(Optional ByRef Usuario As String = "") As Boolean
'            Try
'                Dim sdrEsUsuarioSistema As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Codigo"
'                strSQL += " FROM Usuarios"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND FUNC_Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrEsUsuarioSistema = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrEsUsuarioSistema.Read Then
'                    Es_Usuario_Sistema = True
'                    Usuario = sdrEsUsuarioSistema.Item(0).ToString
'                Else
'                    Es_Usuario_Sistema = False
'                    Usuario = ""
'                End If

'                sdrEsUsuarioSistema.Close()
'                sdrEsUsuarioSistema.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Es_Usuario_Sistema: " & ex.Message.ToString()
'                Es_Usuario_Sistema = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Funcionario_Tiene_Contrato_Cliente(Optional ByRef Orden As String = "") As Boolean
'            Try
'                Dim sdrTieneContratoCliente As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Numero"
'                strSQL += " FROM Encabezado_Contrato_Clientes"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Funcionario = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrTieneContratoCliente = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTieneContratoCliente.Read Then
'                    Funcionario_Tiene_Contrato_Cliente = True
'                    Orden = sdrTieneContratoCliente.Item(0).ToString
'                Else
'                    Funcionario_Tiene_Contrato_Cliente = False
'                    Orden = ""
'                End If

'                sdrTieneContratoCliente.Close()
'                sdrTieneContratoCliente.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Funcionario_Tiene_Contrato_Cliente: " & ex.Message.ToString()
'                Funcionario_Tiene_Contrato_Cliente = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Tiene_Estudio_Seguridad_Tercero(ByVal NumeroIdentificacion As String) As Boolean
'            Try
'                Dim sdrTieneEstudioSeguridad As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Numero"
'                strSQL += " FROM Estudio_seguridad_Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Estado_Estudio = " & ESTADO_ESTUDIO_SEGURIDAD_APROBADO
'                strSQL += " AND Estado = " & General.ESTADO_DEFINITIVO
'                strSQL += " AND OFIC_Codigo = " & Me.objUsuario.Oficina.Codigo
'                strSQL += " AND DATEDIFF(month, Fecha_Aprueba, getdate()) <= " & Me.objEmpresa.MesesVigenciaEstudioSeguridadTerceros
'                strSQL += " AND (Estado = " & General.ESTADO_DEFINITIVO 'En este caso Estado es un comodín con el fin de poder armar el condicional and ( - OR -  OR - OR)
'                If Me.isPropietario Then
'                    strSQL += " OR Identificacion_Propietario = '" & Me.strNumeroIdentificacion & "'"
'                End If

'                If Me.isTenedor Then
'                    strSQL += " OR Identificacion_Tenedor = '" & Me.strNumeroIdentificacion & "'"
'                End If

'                If Me.isConductor Then
'                    strSQL += " OR Identificacion_Conductor = '" & Me.strNumeroIdentificacion & "'"
'                End If

'                strSQL += " )"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrTieneEstudioSeguridad = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTieneEstudioSeguridad.Read Then
'                    Me.objEstudioSeguridad.Numero = sdrTieneEstudioSeguridad.Item("Numero")
'                    Tiene_Estudio_Seguridad_Tercero = True
'                Else
'                    Tiene_Estudio_Seguridad_Tercero = False
'                End If

'                sdrTieneEstudioSeguridad.Close()
'                sdrTieneEstudioSeguridad.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Tiene_Estudio_Seguridad_Tercero: " & ex.Message.ToString()
'                Tiene_Estudio_Seguridad_Tercero = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Guardar_Documento_Entregado(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Dim lonNumeRegi As Long
'                Guardar_Documento_Entregado = False
'                For Me.intCon = 0 To 15
'                    If Me.DocumentoEntregados(intCon) <> 0 Then
'                        Me.objGeneral.ComandoSQL.CommandText = "sp_insertar_documento_entregado_conductor_terceros"
'                        Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                        Me.objGeneral.ComandoSQL.Parameters.Clear()

'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_DOEC_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_DOEC_Codigo").Value = Me.DocumentoEntregados(intCon)

'                        lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                        If lonNumeRegi = 1 Then
'                            Guardar_Documento_Entregado = True
'                        Else
'                            Guardar_Documento_Entregado = False
'                        End If
'                    End If
'                Next

'            Catch ex As Exception
'                Guardar_Documento_Entregado = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar_Documento_Entregado: " & ex.Message.ToString()
'                Mensaje = Me.strError
'            End Try
'            Mensaje = Me.strError
'        End Function

'        Public Function Borrar_Documento_Entregado(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ComandoSQL.CommandText = "sp_borrar_documento_entregado_conductor_terceros"
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                Me.objGeneral.ComandoSQL.Parameters.Clear()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi = 1 Then
'                    Borrar_Documento_Entregado = True
'                Else
'                    Borrar_Documento_Entregado = False
'                End If

'            Catch ex As Exception
'                Borrar_Documento_Entregado = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_Documento_Entregado: " & ex.Message.ToString()
'            End Try
'            Mensaje = Me.strError
'        End Function

'        Public Function Entrego_Documento(ByVal Documento As Integer) As Boolean
'            Try
'                Entrego_Documento = True

'                Dim sdrTercero As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT * FROM Documento_Entregado_Conductor_Terceros"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Codigo = " & Me.lonCodigo
'                strSQL += " AND DOEC_Codigo = " & Documento

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then
'                    Entrego_Documento = True
'                Else
'                    Entrego_Documento = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Entrego_Documento: " & ex.Message.ToString()
'                Entrego_Documento = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Guardar_Perfil_Cliente() As Boolean
'            Try
'                Dim lonNumeRegi As Long
'                Guardar_Perfil_Cliente = False

'                Me.objGeneral.ConexionSQL.Open()

'                For Me.intCon = 0 To 15
'                    If Me.PerfilTercero(intCon) <> 0 Then
'                        Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_perfil_terceros", Me.objGeneral.ConexionSQL)
'                        ComandoSQL.CommandType = CommandType.StoredProcedure
'                        ComandoSQL.Parameters.Clear()

'                        ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                        ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                        ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.PerfilTercero(intCon)

'                        lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                        If lonNumeRegi = 1 Then
'                            Guardar_Perfil_Cliente = True
'                            If Me.bolAuditar Then
'                                Me.objControlAuditoria.NumeroDocumentoEncabezado = Me.lonCodigo
'                                Me.objControlAuditoria.NumeroDocumentoDetalle = Me.PerfilTercero(intCon)
'                                Me.objControlAuditoria.Insertar_Control_Auditoria_SQL(Me.objGeneral.ComandoSQL.Parameters, "Perfil_Terceros", ControlAuditoria.TRANSACCION_INSERTAR, Me.strError)
'                            End If
'                        Else
'                            Guardar_Perfil_Cliente = False
'                        End If
'                    End If
'                Next

'            Catch ex As Exception
'                Guardar_Perfil_Cliente = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar_Perfil_Cliente: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Guardar_Perfil_Destinatario() As Boolean
'            Try
'                Dim lonNumeRegi As Long
'                Guardar_Perfil_Destinatario = False

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_perfil_terceros", Me.objGeneral.ConexionSQL)
'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Clear()

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = PERFIL_DESTINATARIO

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi = 1 Then
'                    Guardar_Perfil_Destinatario = True
'                Else
'                    Guardar_Perfil_Destinatario = False
'                End If

'            Catch ex As Exception
'                Guardar_Perfil_Destinatario = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar_Perfil_Destinatario: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Guardar_Perfil_Remitente() As Boolean
'            Try
'                Dim lonNumeRegi As Long
'                Guardar_Perfil_Remitente = False

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_perfil_terceros", Me.objGeneral.ConexionSQL)
'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Clear()

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = PERFIL_REMITENTE

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi = 1 Then
'                    Guardar_Perfil_Remitente = True
'                Else
'                    Guardar_Perfil_Remitente = False
'                End If

'            Catch ex As Exception
'                Guardar_Perfil_Remitente = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar_Perfil_Remitente: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Guardar_Tercero_Destinatario(ByRef MensajeError As String) As Boolean
'            Try
'                Dim strPerfilDestinatario As String = ""
'                Dim strCodigoAuxiliarTercero As String = ""
'                Guardar_Tercero_Destinatario = False

'                'Verifica si el tercero existe
'                Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Terceros", "Codigo", "Numero_Identificacion", General.CAMPO_ALFANUMERICO, NumeroIdentificacion, strCodigoAuxiliarTercero)
'                If Val(strCodigoAuxiliarTercero) > General.CODIGO_TERCERO_SISTEMA Then
'                    Me.lonCodigo = Val(strCodigoAuxiliarTercero)

'                    Guardar_Tercero_Destinatario = True
'                    If Me.objValidacion.Consultar(Me.objEmpresa, ACTUALIZAR_DESTINATARIO_EN_TERCEROS) Then
'                        If Me.objValidacion.Valida Then
'                            Guardar_Tercero_Destinatario = Modificar_Tercero_Destinatario()
'                        End If
'                    End If

'                    If Guardar_Tercero_Destinatario Then

'                        'verifica si tiene el perfil de destinatario
'                        Me.objGeneral.Retorna_Campo_BD(Me.objEmpresa.Codigo, "Perfil_Terceros", "Codigo", "TERC_Codigo", General.CAMPO_NUMERICO, Me.lonCodigo, strPerfilDestinatario, "Codigo = " & PERFIL_DESTINATARIO)
'                        If Val(strPerfilDestinatario) > General.CERO Then
'                            Guardar_Tercero_Destinatario = True
'                        Else
'                            If Guardar_Perfil_Destinatario() Then
'                                Guardar_Tercero_Destinatario = True
'                            Else
'                                MensajeError = "Se produjo un error al insertar el perfil de destinatario . " & Me.strError
'                            End If
'                        End If
'                    Else
'                        MensajeError = "Se produjo un error al modificar el destinatario . " & Me.strError
'                    End If

'                Else
'                    Guardar_Tercero_Destinatario = Guardar()
'                    If Guardar_Tercero_Destinatario Then
'                        If Guardar_Perfil_Destinatario() Then
'                            Guardar_Tercero_Destinatario = True
'                        Else
'                            MensajeError = "Se produjo un error al insertar el perfil de destinatario . " & Me.strError
'                        End If
'                    End If
'                End If

'            Catch ex As Exception
'                Guardar_Tercero_Destinatario = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar_Tercero_Destinatario: " & ex.Message.ToString()
'            Finally
'                MensajeError = Me.strError
'            End Try
'        End Function

'        Private Function Modificar_Tercero_Destinatario() As Boolean
'            Try

'                Dim strSQL As String
'                Dim objGeneral1 As New General

'                strSQL = "UPDATE Terceros SET Nombre = '" & Me.strNombre & "'"
'                strSQL += " , Apellido1 = '" & Me.strApellido1 & "'"
'                strSQL += " , Apellido2 = '" & Me.strApellido2 & "'"
'                strSQL += " , CIUD_Codigo = " & Me.objCiudad.Codigo
'                strSQL += " , Direccion = '" & Me.strDireccion & "'"
'                strSQL += " , Telefono1 = '" & Me.strTelefono1 & "'"
'                strSQL += " , Fecha_Modifica = GETDATE()"
'                strSQL += " , USUA_Modifica = '" & Me.objUsuario.Codigo & "'"

'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                Modificar_Tercero_Destinatario = Me.objGeneral.Ejecutar_SQL(strSQL, Me.strError)


'            Catch ex As Exception
'                Modificar_Tercero_Destinatario = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'            End Try

'        End Function

'        Public Function Existe_Registro() As Boolean
'            Try
'                Dim sdrTercero As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Numero_Identificacion, Nombre, Apellido1, Apellido2, Telefono1, Estado FROM Terceros"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.Codigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then
'                    Me.strNumeroIdentificacion = sdrTercero("Numero_Identificacion").ToString()
'                    Me.strNombre = sdrTercero("Nombre").ToString()
'                    Me.strApellido1 = sdrTercero("Apellido1").ToString()
'                    Me.strApellido2 = sdrTercero("Apellido2").ToString()
'                    Me.strTelefono1 = sdrTercero("Telefono1").ToString()
'                    Me.objCataEstado.Campo1 = Val(sdrTercero("Estado").ToString())
'                    Existe_Registro = True
'                Else
'                    Me.lonCodigo = 0
'                    Existe_Registro = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception

'                Me.lonCodigo = 0
'                Existe_Registro = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & ex.Message.ToString()

'            Finally

'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If

'            End Try

'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Codigo As Long) As Boolean
'            Try
'                Dim sdrTercero As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                strSQL = "SELECT EMPR_Codigo, TIID_Codigo, Numero_Identificacion, Nombre, Apellido1, Apellido2, Telefono1, Estado, Codigo_Contable_Filial FROM Terceros"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.Codigo
'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Existe_Registro = True
'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then
'                    Me.strNumeroIdentificacion = sdrTercero("Numero_Identificacion").ToString()
'                    Me.strNombre = sdrTercero("Nombre").ToString()
'                    Me.strApellido1 = sdrTercero("Apellido1").ToString()
'                    Me.strApellido2 = sdrTercero("Apellido2").ToString()
'                    Me.strTelefono1 = sdrTercero("Telefono1").ToString()
'                    Me.objCataEstado.Campo1 = Val(sdrTercero("Estado").ToString())
'                    Me.CataTipoIdentificacion.Campo1 = sdrTercero("TIID_Codigo").ToString
'                    Me.CodigoContableFilial = sdrTercero("Codigo_Contable_Filial").ToString
'                    Existe_Registro = True
'                Else
'                    Me.lonCodigo = 0
'                    Existe_Registro = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Existe_Registro = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Registro_Por_Identificacion(ByRef CodigoEmpresa As Integer, ByVal NumeroIdentificacion As String) As Boolean
'            Try
'                Dim sdrTercero As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.strNumeroIdentificacion = NumeroIdentificacion

'                strSQL = "SELECT EMPR_Codigo, Codigo, Nombre, Apellido1, Apellido2, Telefono1, Celular, Estado, TIID_Codigo FROM Terceros"
'                strSQL += " WHERE EMPR_Codigo = " & CodigoEmpresa
'                strSQL += " AND Numero_Identificacion = '" & Me.strNumeroIdentificacion & "'"
'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then
'                    Long.TryParse(sdrTercero("Codigo").ToString(), Me.lonCodigo)
'                    Me.strNombre = sdrTercero("Nombre").ToString()
'                    Me.strApellido1 = sdrTercero("Apellido1").ToString()
'                    Me.strApellido2 = sdrTercero("Apellido2").ToString()
'                    Me.strTelefono1 = sdrTercero("Telefono1").ToString()
'                    Me.strCelular = sdrTercero("Celular").ToString()
'                    Me.objCataEstado.Campo1 = Val(sdrTercero("Estado").ToString())
'                    Me.objCataTipoIdentificacion.Campo1 = sdrTercero("TIID_Codigo").ToString
'                    Existe_Registro_Por_Identificacion = True
'                Else
'                    Me.lonCodigo = 0
'                    Existe_Registro_Por_Identificacion = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception

'                Me.lonCodigo = 0
'                Existe_Registro_Por_Identificacion = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro_Por_Identificacion: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Cliente_Por_Identificacion(ByRef Empresa As Empresas, ByVal NumeroIdentificacion As String) As Boolean
'            Try
'                Dim sdrTercero As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.objEmpresa = Empresa
'                Me.strNumeroIdentificacion = NumeroIdentificacion

'                strSQL = "SELECT EMPR_Codigo, Codigo, Nombre, Apellido1, Apellido2, Telefono1, Estado FROM V_Clientes"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero_Identificacion = '" & Me.strNumeroIdentificacion & "'"
'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then
'                    Long.TryParse(sdrTercero("Codigo").ToString(), Me.lonCodigo)
'                    Me.strNombre = sdrTercero("Nombre").ToString()
'                    Me.strApellido1 = sdrTercero("Apellido1").ToString()
'                    Me.strApellido2 = sdrTercero("Apellido2").ToString()
'                    Me.strTelefono1 = sdrTercero("Telefono1").ToString()
'                    Me.objCataEstado.Campo1 = Val(sdrTercero("Estado").ToString())
'                    Existe_Cliente_Por_Identificacion = True
'                Else
'                    Me.lonCodigo = 0
'                    Existe_Cliente_Por_Identificacion = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Existe_Cliente_Por_Identificacion = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Cliente_Por_Identificacion: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Existe_Empleado_Por_Identificacion(ByRef Empresa As Empresas, ByVal NumeroIdentificacion As String) As Boolean
'            Try
'                Dim sdrTercero As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                Me.objEmpresa = Empresa
'                Me.strNumeroIdentificacion = NumeroIdentificacion

'                strSQL = "SELECT EMPR_Codigo, Codigo, Nombre, Apellido1, Apellido2, Telefono1, Estado FROM V_Empleados"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Numero_Identificacion = '" & Me.strNumeroIdentificacion & "'"
'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrTercero = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTercero.Read() Then
'                    Long.TryParse(sdrTercero("Codigo").ToString(), Me.lonCodigo)
'                    Me.strNombre = sdrTercero("Nombre").ToString()
'                    Me.strApellido1 = sdrTercero("Apellido1").ToString()
'                    Me.strApellido2 = sdrTercero("Apellido2").ToString()
'                    Me.strTelefono1 = sdrTercero("Telefono1").ToString()
'                    Me.objCataEstado.Campo1 = Val(sdrTercero("Estado").ToString())
'                    Existe_Empleado_Por_Identificacion = True
'                Else
'                    Me.lonCodigo = 0
'                    Existe_Empleado_Por_Identificacion = False
'                End If

'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Existe_Empleado_Por_Identificacion = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Empleado_Por_Identificacion: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Limpiar_Tabla_Temporal() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_limpiar_detalle_seguro_clientes", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.objUsuario.Codigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Limpiar_Tabla_Temporal = True
'                Else
'                    Limpiar_Tabla_Temporal = False
'                End If

'            Catch ex As Exception
'                Limpiar_Tabla_Temporal = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Limpiar_Tabla_Temporal: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Limpiar_Tabla_Temporal_Curso_Formacion() As Boolean
'            Dim lonNumeRegi As Long
'            Me.objGeneral.ConexionSQL.Open()
'            Try
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_limpiar_detalle_curso_formacion_empleados", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.Usuario.Codigo
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Limpiar_Tabla_Temporal_Curso_Formacion = True
'                Else
'                    Limpiar_Tabla_Temporal_Curso_Formacion = False
'                End If

'            Catch ex As Exception
'                Limpiar_Tabla_Temporal_Curso_Formacion = False
'                Me.strError = Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'            Return True
'        End Function

'        Public Function Limpiar_Tabla_Temporal_Examen_Medico() As Boolean
'            Dim lonNumeRegi As Long
'            Me.objGeneral.ConexionSQL.Open()
'            Try
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_limpiar_detalle_examen_medico_empleados", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.Usuario.Codigo
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Terc_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Terc_Codigo").Value = Me.lonCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Limpiar_Tabla_Temporal_Examen_Medico = True
'                Else
'                    Limpiar_Tabla_Temporal_Examen_Medico = False
'                End If

'            Catch ex As Exception
'                Limpiar_Tabla_Temporal_Examen_Medico = False
'                Me.strError = Me.objGeneral.Traducir_Error(ex.Message)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'            Return True
'        End Function

'        Public Function Cargar_Tabla_Detalle_Seguro_Temporal() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_trasladar_detalle_seguro_clientes", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.Usuario.Codigo
'                ComandoSQL.Parameters.Add("@par_A_Temporal", SqlDbType.Int) : ComandoSQL.Parameters("@par_A_Temporal").Value = General.HACIA_TEMPORAL

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Cargar_Tabla_Detalle_Seguro_Temporal = True
'                Else
'                    Cargar_Tabla_Detalle_Seguro_Temporal = False
'                End If

'            Catch ex As Exception
'                Cargar_Tabla_Detalle_Seguro_Temporal = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Tabla_Detalle_Seguro_Temporal: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Cargar_Tabla_Examen_Medico_Empleados() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Me.objGeneral.ComandoSQL = New SqlCommand("sp_trasladar_detalle_examen_medico_empleados", Me.objGeneral.ConexionSQL)
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure

'                Me.objGeneral.ComandoSQL.Parameters.Clear()
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_empr_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_empr_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_terc_codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_terc_codigo").Value = Me.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Usua_Codigo", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Usua_Codigo").Value = Me.Usuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_A_Temporal", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_A_Temporal").Value = General.HACIA_TEMPORAL

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Cargar_Tabla_Examen_Medico_Empleados = True
'                Else
'                    Cargar_Tabla_Examen_Medico_Empleados = False
'                End If

'            Catch ex As Exception
'                Cargar_Tabla_Examen_Medico_Empleados = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Tabla_Detalle_Seguro_Temporal: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Cargar_Tabla_Hijo_Empleados() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_trasladar_detalle_Hijo_empleados", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_empr_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_empr_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_terc_codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_terc_codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_usua_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_usua_Codigo").Value = Me.Usuario.Codigo
'                ComandoSQL.Parameters.Add("@par_a_temporar", SqlDbType.Int) : ComandoSQL.Parameters("@par_a_temporar").Value = General.HACIA_TEMPORAL

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Cargar_Tabla_Hijo_Empleados = True
'                Else
'                    Cargar_Tabla_Hijo_Empleados = False
'                End If

'            Catch ex As Exception
'                Cargar_Tabla_Hijo_Empleados = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Tabla_Detalle_Seguro_Temporal: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Cargar_Tabla_Curso_Formacion_Empleados() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_trasladar_detalle_curso_formación_empleados", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.Usuario.Codigo
'                ComandoSQL.Parameters.Add("@par_A_Temporal", SqlDbType.Int) : ComandoSQL.Parameters("@par_A_Temporal").Value = General.HACIA_TEMPORAL

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Cargar_Tabla_Curso_Formacion_Empleados = True
'                Else
'                    Cargar_Tabla_Curso_Formacion_Empleados = False
'                End If

'            Catch ex As Exception
'                Cargar_Tabla_Curso_Formacion_Empleados = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Tabla_Detalle_Seguro_Temporal: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Actualizar_Celular() As Boolean
'            Try
'                Dim strSQL As String = ""

'                strSQL = "UPDATE Terceros"
'                strSQL += " SET Celular = " & Me.strCelular
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & lonCodigo

'                Actualizar_Celular = True
'                Me.objGeneral.Ejecutar_SQL(strSQL)
'            Catch ex As Exception
'                Actualizar_Celular = False
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Actualizar_Celular: " & ex.Message.ToString()
'            End Try
'        End Function

'#End Region

'#Region "Funciones Privadas"

'        Private Sub Instanciar_Objetos_Alternos()
'            Try
'                If IsNothing(Me.objEstudioSeguridad) Then
'                    Me.objEstudioSeguridad = New EstudioSeguiridadVehiculos(Me.objEmpresa)
'                End If

'                If IsNothing(Me.objEPS) Then
'                    Me.objEPS = New Tercero(Me.objEmpresa)
'                End If

'                If IsNothing(Me.objARP) Then
'                    Me.objARP = New Tercero(Me.objEmpresa)
'                End If

'                If IsNothing(Me.objFondoPensiones) Then
'                    Me.objFondoPensiones = New Tercero(Me.objEmpresa)
'                End If

'                If IsNothing(Me.objFondoCesantias) Then
'                    Me.objFondoCesantias = New Tercero(Me.objEmpresa)
'                End If

'                If IsNothing(Me.objBeneficiarioTransferencia) Then
'                    Me.objBeneficiarioTransferencia = New Tercero(Me.objEmpresa)
'                End If

'                If IsNothing(Me.objRepresentanteComercial) Then
'                    Me.objRepresentanteComercial = New Tercero(Me.objEmpresa)
'                End If

'                If IsNothing(Me.objAseguradora) Then
'                    Me.objAseguradora = New Tercero(Me.objEmpresa)
'                End If
'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Instanciar_Objetos_Alternos: " & ex.Message.ToString()
'            End Try
'        End Sub

'        Private Sub Instanciar_Objetos_Alternos_Cliente()
'            Try
'                If IsNothing(Me.objRepresentanteComercial) Then
'                    Me.objRepresentanteComercial = New Tercero(Me.objEmpresa)
'                End If

'                If IsNothing(Me.objAseguradora) Then
'                    Me.objAseguradora = New Tercero(Me.objEmpresa)
'                End If
'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Instanciar_Objetos_Alternos_Cliente: " & ex.Message.ToString()
'            End Try
'        End Sub

'        Private Function Insertar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ComandoSQL.CommandText = "sp_insertar_terceros"
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                Me.objGeneral.ComandoSQL.Parameters.Clear()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo").Direction = ParameterDirection.Output
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIDO_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TIDO_Codigo").Value = TipoDocumentos.TIDO_TERCERO
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TINA_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_TINA_Codigo").Value = Val(Me.objCataTipoNaturaleza.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIID_Codigo", SqlDbType.Char, 3) : Me.objGeneral.ComandoSQL.Parameters("@par_TIID_Codigo").Value = Me.objCataTipoIdentificacion.Retorna_Codigo()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Identificacion", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Identificacion").Value = Me.strNumeroIdentificacion
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Expedicion_Identificacion", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Expedicion_Identificacion").Value = Me.objCiudadExpedicionIdentificacion.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Digito_Chequeo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Digito_Chequeo").Value = Me.intDigitoChequeo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 60) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Apellido1", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Apellido1").Value = Me.strApellido1

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Apellido2", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Apellido2").Value = Me.strApellido2
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Nacimiento", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Nacimiento").Value = Me.objCiudadNacimiento.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Nacimiento", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Nacimiento").Value = Me.dteFechaNacimiento
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_ESCI_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_ESCI_Codigo").Value = Val(Me.objCataEstadoCivil.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Sexo", SqlDbType.Char, 1) : Me.objGeneral.ComandoSQL.Parameters("@par_Sexo").Value = Me.objCataSexo.Retorna_Codigo()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Representante_Legal", SqlDbType.VarChar, 40) : Me.objGeneral.ComandoSQL.Parameters("@par_Representante_Legal").Value = Me.strRepresentanteLegal
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Codigo").Value = Me.objCiudad.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Direccion", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Direccion").Value = Me.strDireccion
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono1", SqlDbType.VarChar, 40) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono1").Value = Me.strTelefono1
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono2", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono2").Value = Me.strTelefono2

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fax", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Fax").Value = Me.strFax
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Celular", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Celular").Value = Me.strCelular
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Email", SqlDbType.VarChar, 500) : Me.objGeneral.ComandoSQL.Parameters("@par_Email").Value = Me.strEmail
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Pagina_Web", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Pagina_Web").Value = Me.strPaginaWeb
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_REGI_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_REGI_Codigo").Value = Val(Me.objCataRegimen.Retorna_Codigo())

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Administracion_Impuestos", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Administracion_Impuestos").Value = Me.strAdministracionImpuestos
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autoretenedor", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Autoretenedor").Value = Me.intAutoretenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Tipo_Sangre", SqlDbType.Char, 3) : Me.objGeneral.ComandoSQL.Parameters("@par_Tipo_Sangre").Value = Me.objCataTipoSangre.Retorna_Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Licencia", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Licencia").Value = Me.strNumeroLicencia
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Categoria_Licencia", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Categoria_Licencia").Value = Me.intCategoriaLicencia

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Licencia", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Licencia").Value = Me.dteFechaLicencia
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Afiliado_EPS", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Afiliado_EPS").Value = Me.intAfiliadoEPS
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_EPS", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_EPS").Value = Me.objEPS.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_EPS", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_EPS").Value = Me.dteFechaAfiliacionEPS
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Afiliado_ARP", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Afiliado_ARP").Value = Me.intAfiliadoARP

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_ARP", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_ARP").Value = Me.objARP.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_ARP", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_ARP").Value = Me.dteFechaAfiliacionARP
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Afiliado_Fondo_Pensiones", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Afiliado_Fondo_Pensiones").Value = Me.intAfiliadoFondoPensiones
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Fondo_Pensiones", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Fondo_Pensiones").Value = Me.objFondoPensiones.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Fondo_Pensiones", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Fondo_Pensiones").Value = Me.dteFechaAfiliacionFondoPensiones

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Afiliado_Fondo_Cesantias", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Afiliado_Fondo_Cesantias").Value = Me.intAfiliadoFondoCesantias
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Fondo_Cesantias", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Fondo_Cesantias").Value = Me.objFondoCesantias.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Fondo_Cesantias", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Fondo_Cesantias").Value = Me.dteFechaAfiliacionFondoCesantias
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TICT_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_TICT_Codigo").Value = Val(Me.objCataTipoContrato.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Contrato", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Contrato").Value = Me.dteFechaContrato

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Vence_Contrato", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Vence_Contrato").Value = Me.dteFechaVenceContrato
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Salario_Basico", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Salario_Basico").Value = Me.dblSalarioBasico
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Conductor_Propio", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Conductor_Propio").Value = Me.intConductorPropio
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_BANC_Transfe", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_BANC_Transfe").Value = Me.objBancoTransferencia.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Cuenta_Transfe", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Cuenta_Transfe").Value = Me.strNumeroCuentaTransferencia

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Tipo_Cuenta_Transfe", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Tipo_Cuenta_Transfe").Value = Val(Me.objCataTipoCuentaTransferencia.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Beneficiario_Transfe", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Beneficiario_Transfe").Value = Me.lonCodigo ' para revisar
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo_Transfe", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo_Transfe").Value = Me.strCodigoTransferencia
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Conyuge", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Conyuge").Value = Me.strNombreConyuge
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Hijos", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Hijos").Value = Me.intNumeroHijos

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_PELI_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_PELI_Codigo").Value = Val(Me.objCataPeriodoLiquidacion.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_GRLI_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_GRLI_Codigo").Value = Val(Me.objCataGrupoLiquidacion.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Observaciones", SqlDbType.VarChar, 500) : Me.objGeneral.ComandoSQL.Parameters("@par_Observaciones").Value = Me.strObservaciones
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_OFIC_EMPL_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_OFIC_EMPL_Codigo").Value = Me.objOficinaEmpleado.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CARG_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_CARG_Codigo").Value = Val(Me.objCataCargo.Retorna_Codigo())

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CABL_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_CABL_Codigo").Value = Val(Me.objCataCausaBloqueo.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_DIPA_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_DIPA_Codigo").Value = Val(Me.objCataDiaPago.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Porcentaje_Comision", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Porcentaje_Comision").Value = Me.dblPorcentajeComision
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Representante_Comercial", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Representante_Comercial").Value = Me.objRepresentanteComercial.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Estado").Value = Val(Me.objCataEstado.Retorna_Codigo())

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_USUA_Crea", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_USUA_Crea").Value = Me.objUsuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_OFIC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_OFIC_Codigo").Value = Me.objUsuario.Oficina.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Justificacion_Bloqueo", SqlDbType.VarChar, 150) : Me.objGeneral.ComandoSQL.Parameters("@par_Justificacion_Bloqueo").Value = Me.strJustificacionBloqueo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Representante_Comercial", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Representante_Comercial").Value = Me.intEsRepresentanteComercial
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Oficina_Principal", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Oficina_Principal").Value = Me.objUsuario.Oficina.Principal

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_ESSE_Numero", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_ESSE_Numero").Value = Me.objEstudioSeguridad.Numero
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio").Value = Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio").Value = Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero").Value = Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero").Value = Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Postfijo", SqlDbType.Char, 5) : Me.objGeneral.ComandoSQL.Parameters("@par_Postfijo").Value = Me.strPostfijo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Aplica_Rete_ICA", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Aplica_Rete_ICA").Value = Me.intAplicaReteIca
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo_Postal", SqlDbType.VarChar, 40) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo_Postal").Value = Me.strCodigoPostal
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_COOR_Uno_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_COOR_Uno_Codigo").Value = Val(Me.objCataCoordenadaUno.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Coordenada_Uno_Valor", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Coordenada_Uno_Valor").Value = Me.intCoordenadaUnoValor

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_COOR_Dos_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_COOR_Dos_Codigo").Value = Val(Me.objCataCoordenadaDos.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Coordenada_Dos_Valor", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Coordenada_Dos_Valor").Value = Me.intCoordenadaDosValor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Zona_Especial", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Zona_Especial").Value = Me.intZonaEspecial
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_ZONA_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_ZONA_Codigo").Value = General.CERO

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Dias_Bloqueo_Mora", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Dias_Bloqueo_Mora").Value = Me.intDiasBloqueoMora
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_PEFC_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_PEFC_Codigo").Value = Val(Me.objCataPeriodicidadFacturacionCliente.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Tiene_Seguro", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Tiene_Seguro").Value = Me.intTieneSeguro
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Aseguradora_Cliente", SqlDbType.VarChar, 40) : Me.objGeneral.ComandoSQL.Parameters("@par_Aseguradora_Cliente").Value = Me.strAseguradoraCliente
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Vencimiento_Seguro", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Vencimiento_Seguro").Value = Me.dteFechaVencimientoSeguro

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TCSC_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_TCSC_Codigo").Value = Val(Me.objCataTipoCoberturaSeguro.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Monto_Maximo_Seguro", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Monto_Maximo_Seguro").Value = Me.dblMontoMaximoSeguro
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_GASC_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_GASC_Codigo").Value = Val(Me.objCataGarantiaSeguroCliente.Retorna_Codigo)

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Cliente_BASC", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Cliente_BASC").Value = Me.intClienteBASC
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Condiciones_Seguro", SqlDbType.VarChar, 1000) : Me.objGeneral.ComandoSQL.Parameters("@par_Condiciones_Seguro").Value = Me.strCondicionesSeguro
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Antiguedad_Vehiculo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Antiguedad_Vehiculo").Value = Me.intAntiguedadVehiculo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Cliente_Prospecto", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Cliente_Prospecto").Value = Me.intClienteProspecto
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Referencio_Prospecto", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Referencio_Prospecto").Value = Me.strReferencioProspecto

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Referencias_Verificadas", SqlDbType.VarChar, 1000) : Me.objGeneral.ComandoSQL.Parameters("@par_Referencias_Verificadas").Value = Me.strReferenciasVerificadas
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Aseguradora", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Aseguradora").Value = Me.objAseguradora.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TARC_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_TARC_Codigo").Value = Val(Me.objCataTarifarioRepreComercial.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Es_Filial", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Es_Filial").Value = Me.intEsFilial
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo_Filial", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo_Filial").Value = Me.strCodigoFilial

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo_Contable_Filial", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo_Contable_Filial").Value = Me.strCodigoContableFilial
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Ingreso_Cliente", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Ingreso_Cliente").Value = Me.dteFechaIngresoCliente
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Cliente_Exterior", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Cliente_Exterior").Value = Me.intClienteExterior
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Aplica_Rete_Fuente", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Aplica_Rete_Fuente").Value = Me.intAplicaReteFuente
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo_Contable_Filial2", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo_Contable_Filial2").Value = Me.strCodigoActividad

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Ultimo_Cargue", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Ultimo_Cargue").Value = Me.dteFechaUltimoCargue

'                If IsNothing(Me.bytFoto) Then
'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value
'                Else
'                    If Me.bytFoto(0) = BYTES_IMAGEN_DEFECTO Then
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value
'                    Else
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Foto").Value = Me.bytFoto
'                    End If
'                End If


'                If IsNothing(Me.bytHuella_Digital) Then
'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Huella_Digital", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Huella_Digital").Value = DBNull.Value
'                Else

'                    If Me.bytHuella_Digital(0) = BYTES_IMAGEN_DEFECTO Then
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_Huella_Digital", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Huella_Digital").Value = DBNull.Value
'                    Else
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_Huella_Digital", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Huella_Digital").Value = Me.bytHuella_Digital
'                    End If
'                End If

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Mensaje", SqlDbType.VarChar, 200) : Me.objGeneral.ComandoSQL.Parameters("@par_Mensaje").Direction = ParameterDirection.Output

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CALI_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_CALI_Codigo").Value = Val(Me.objCataCategoriaLicenciaConductor.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIID_Codigo_Titular", SqlDbType.Char, 3) : Me.objGeneral.ComandoSQL.Parameters("@par_TIID_Codigo_Titular").Value = Me.objCataTipoIdentificacionTitular.Retorna_Codigo()
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Digito_Chequeo_Titular", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Digito_Chequeo_Titular").Value = Me.intDigitoChequeoTitular
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Titular", SqlDbType.VarChar, 60) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Titular").Value = Me.strNombreTitular
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Identificacion_Titular", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Identificacion_Titular").Value = Me.strNumeroIdentificacionTitular
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Consulta_Lista_Clinton", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Consulta_Lista_Clinton").Value = Me.intConsultaListaClinton
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Factura_Faltantes", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Factura_Faltantes").Value = Me.intFacturaFaltantes
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Nacimiento_Conyuge", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Nacimiento_Conyuge").Value = Me.dteFechNaciCony
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_Conyuge", SqlDbType.VarChar) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_Conyuge").Value = Me.strTeleCony
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_IANS_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_IANS_Codigo").Value = Me.intIansCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_FPCO_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_FPCO_Codigo").Value = Me.CataFormaPago.intCampoCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Dias_Plazo_Pago", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Dias_Plazo_Pago").Value = Val(Me.DiasPlazoPago)

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Descuento_Flete", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Descuento_Flete").Value = Val(Me.dblDescuentoFlete)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Descuento_Manejo", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Descuento_Manejo").Value = Val(Me.dblDescuentoManejo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Descuento_Minimo_Unidad", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Descuento_Minimo_Unidad").Value = Val(Me.dblDescuentoMinimoUnidad)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Descuento_Urgencia", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Descuento_Urgencia").Value = Val(Me.dblDescuentoUrgencia)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Descuento_Kg_Adicional", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Descuento_Kg_Adicional").Value = Val(Me.dblDescuentoKgAdicional)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Descuento_Documento_Retorno", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Descuento_Documento_Retorno").Value = Val(Me.dblDescuentoDocumentoRetorno)

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Minimo_Kilos_Cobrar", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Minimo_Kilos_Cobrar").Value = Val(Me.dblMinimoKilosCobrar)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Porcentaje_Seguro", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Porcentaje_Seguro").Value = Val(Me.dblPorcentajeSeguro)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_PAIS_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_PAIS_Codigo").Value = Me.CodigoPais
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Aplica_Rete_IVA", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Aplica_Rete_IVA").Value = Me.intAplicReteIva

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If Me.objGeneral.ComandoSQL.Parameters("@par_Mensaje").Value IsNot DBNull.Value Then
'                    Me.strError = Me.objGeneral.ComandoSQL.Parameters("@par_Mensaje").Value
'                End If

'                If lonNumeRegi > 1 Then
'                    Insertar_SQL = True
'                    Me.lonCodigo = Me.objGeneral.ComandoSQL.Parameters("@par_Codigo").Value
'                    If Me.bolAuditar Then
'                        Me.objControlAuditoria.NumeroDocumentoEncabezado = Me.lonCodigo
'                        Me.objControlAuditoria.Insertar_Control_Auditoria_SQL(Me.objGeneral.ComandoSQL.Parameters, "Terceros", ControlAuditoria.TRANSACCION_INSERTAR, Me.strError)
'                    End If
'                Else
'                    Insertar_SQL = False
'                End If

'                If Me.strError <> "" Then
'                    Insertar_SQL = False
'                End If

'            Catch ex As Exception
'                Insertar_SQL = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_SQL: " & ex.Message.ToString()
'            End Try
'        End Function

'        Private Function Modificar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ComandoSQL.CommandText = "sp_modificar_terceros"
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                Me.objGeneral.ComandoSQL.Parameters.Clear()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TINA_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_TINA_Codigo").Value = Val(Me.objCataTipoNaturaleza.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIID_Codigo", SqlDbType.Char, 3) : Me.objGeneral.ComandoSQL.Parameters("@par_TIID_Codigo").Value = Me.objCataTipoIdentificacion.Retorna_Codigo()
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Identificacion", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Identificacion").Value = Me.strNumeroIdentificacion
'                '5
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Expedicion_Identificacion", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Expedicion_Identificacion").Value = Me.objCiudadExpedicionIdentificacion.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Digito_Chequeo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Digito_Chequeo").Value = Me.intDigitoChequeo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 60) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Apellido1", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Apellido1").Value = Me.strApellido1
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Apellido2", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Apellido2").Value = Me.strApellido2
'                '10
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Nacimiento", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Nacimiento").Value = Me.objCiudadNacimiento.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Nacimiento", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Nacimiento").Value = Me.dteFechaNacimiento
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_ESCI_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_ESCI_Codigo").Value = Val(Me.objCataEstadoCivil.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Sexo", SqlDbType.Char, 1) : Me.objGeneral.ComandoSQL.Parameters("@par_Sexo").Value = Me.objCataSexo.Retorna_Codigo()
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Representante_Legal", SqlDbType.VarChar, 40) : Me.objGeneral.ComandoSQL.Parameters("@par_Representante_Legal").Value = Me.strRepresentanteLegal
'                '15
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Codigo").Value = Me.objCiudad.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Direccion", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Direccion").Value = Me.strDireccion
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono1", SqlDbType.VarChar, 40) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono1").Value = Me.strTelefono1
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono2", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono2").Value = Me.strTelefono2
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fax", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Fax").Value = Me.strFax
'                '20
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Celular", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Celular").Value = Me.strCelular
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Email", SqlDbType.VarChar, 500) : Me.objGeneral.ComandoSQL.Parameters("@par_Email").Value = Me.strEmail
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Pagina_Web", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Pagina_Web").Value = Me.strPaginaWeb
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_REGI_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_REGI_Codigo").Value = Val(Me.objCataRegimen.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Administracion_Impuestos", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Administracion_Impuestos").Value = Me.strAdministracionImpuestos
'                '25
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Autoretenedor", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Autoretenedor").Value = Me.intAutoretenedor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Tipo_Sangre", SqlDbType.Char, 3) : Me.objGeneral.ComandoSQL.Parameters("@par_Tipo_Sangre").Value = Me.objCataTipoSangre.Retorna_Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Licencia", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Licencia").Value = Me.strNumeroLicencia
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Categoria_Licencia", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Categoria_Licencia").Value = Me.intCategoriaLicencia
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Licencia", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Licencia").Value = Me.dteFechaLicencia
'                '30
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Afiliado_EPS", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Afiliado_EPS").Value = Me.intAfiliadoEPS
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_EPS", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_EPS").Value = Me.objEPS.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_EPS", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_EPS").Value = Me.dteFechaAfiliacionEPS
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Afiliado_ARP", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Afiliado_ARP").Value = Me.intAfiliadoARP
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_ARP", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_ARP").Value = Me.objARP.lonCodigo
'                '35
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_ARP", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_ARP").Value = Me.dteFechaAfiliacionARP
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Afiliado_Fondo_Pensiones", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Afiliado_Fondo_Pensiones").Value = Me.intAfiliadoFondoPensiones
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Fondo_Pensiones", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Fondo_Pensiones").Value = Me.objFondoPensiones.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Fondo_Pensiones", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Fondo_Pensiones").Value = Me.dteFechaAfiliacionFondoPensiones
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Afiliado_Fondo_Cesantias", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Afiliado_Fondo_Cesantias").Value = Me.intAfiliadoFondoCesantias
'                '40
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Fondo_Cesantias", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Fondo_Cesantias").Value = Me.objFondoCesantias.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Fondo_Cesantias", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Fondo_Cesantias").Value = Me.dteFechaAfiliacionFondoCesantias
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TICT_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_TICT_Codigo").Value = Val(Me.objCataTipoContrato.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Contrato", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Contrato").Value = Me.dteFechaContrato
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Vence_Contrato", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Vence_Contrato").Value = Me.dteFechaVenceContrato
'                '45
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Salario_Basico", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Salario_Basico").Value = Me.dblSalarioBasico
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Conductor_Propio", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Conductor_Propio").Value = Me.intConductorPropio
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_BANC_Transfe", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_BANC_Transfe").Value = Me.objBancoTransferencia.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Cuenta_Transfe", SqlDbType.VarChar, 20) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Cuenta_Transfe").Value = Me.strNumeroCuentaTransferencia
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Tipo_Cuenta_Transfe", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Tipo_Cuenta_Transfe").Value = Val(Me.objCataTipoCuentaTransferencia.Retorna_Codigo())
'                '50
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Beneficiario_Transfe", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Beneficiario_Transfe").Value = Me.lonCodigo ' para revisar
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo_Transfe", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo_Transfe").Value = Me.strCodigoTransferencia
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Conyuge", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Conyuge").Value = Me.strNombreConyuge
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Hijos", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Hijos").Value = Me.intNumeroHijos
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_PELI_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_PELI_Codigo").Value = Val(Me.objCataPeriodoLiquidacion.Retorna_Codigo())
'                '55
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_GRLI_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_GRLI_Codigo").Value = Val(Me.objCataGrupoLiquidacion.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Observaciones", SqlDbType.VarChar, 500) : Me.objGeneral.ComandoSQL.Parameters("@par_Observaciones").Value = Me.strObservaciones
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_OFIC_EMPL_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_OFIC_EMPL_Codigo").Value = Me.objOficinaEmpleado.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CARG_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_CARG_Codigo").Value = Val(Me.objCataCargo.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CABL_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_CABL_Codigo").Value = Val(Me.objCataCausaBloqueo.Retorna_Codigo())
'                '60
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_DIPA_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_DIPA_Codigo").Value = Val(Me.objCataDiaPago.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Porcentaje_Comision", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Porcentaje_Comision").Value = Me.dblPorcentajeComision
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Representante_Comercial", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Representante_Comercial").Value = Me.objRepresentanteComercial.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Estado", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Estado").Value = Val(Me.objCataEstado.Retorna_Codigo())
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_USUA_Modifica", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_USUA_Modifica").Value = Me.objUsuario.Codigo
'                '65
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_OFIC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_OFIC_Codigo").Value = Me.objUsuario.Oficina.Codigo ' Variable de Sesión
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Justificacion_Bloqueo", SqlDbType.VarChar, 150) : Me.objGeneral.ComandoSQL.Parameters("@par_Justificacion_Bloqueo").Value = Me.strJustificacionBloqueo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Representante_Comercial", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Representante_Comercial").Value = Me.intEsRepresentanteComercial
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Oficina_Principal", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Oficina_Principal").Value = Me.objUsuario.Oficina.Principal
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_ESSE_Numero", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_ESSE_Numero").Value = Me.objEstudioSeguridad.Numero
'                '70
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Propio").Value = Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Propio").Value = Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Margen_Rentabilidad_Despacho_Nacional_Vehiculo_Tercero").Value = Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Margen_Rentabilidad_Despacho_Urbano_Vehiculo_Tercero").Value = Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Postfijo", SqlDbType.Char, 5) : Me.objGeneral.ComandoSQL.Parameters("@par_Postfijo").Value = Me.strPostfijo

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Aplica_Rete_ICA", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Aplica_Rete_ICA").Value = Me.intAplicaReteIca
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo_Postal", SqlDbType.VarChar, 40) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo_Postal").Value = Me.strCodigoPostal
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_COOR_Uno_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_COOR_Uno_Codigo").Value = Val(Me.objCataCoordenadaUno.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Coordenada_Uno_Valor", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Coordenada_Uno_Valor").Value = Me.intCoordenadaUnoValor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_COOR_Dos_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_COOR_Dos_Codigo").Value = Val(Me.objCataCoordenadaDos.Retorna_Codigo)
'                '80
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Coordenada_Dos_Valor", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Coordenada_Dos_Valor").Value = Me.intCoordenadaDosValor
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Zona_Especial", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Zona_Especial").Value = Me.intZonaEspecial
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_ZONA_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_ZONA_Codigo").Value = General.CERO
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Dias_Bloqueo_Mora", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Dias_Bloqueo_Mora").Value = Me.intDiasBloqueoMora
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_PEFC_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_PEFC_Codigo").Value = Val(Me.objCataPeriodicidadFacturacionCliente.Retorna_Codigo)

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Tiene_Seguro", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Tiene_Seguro").Value = Me.intTieneSeguro
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Aseguradora_Cliente", SqlDbType.VarChar, 40) : Me.objGeneral.ComandoSQL.Parameters("@par_Aseguradora_Cliente").Value = Me.strAseguradoraCliente
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Vencimiento_Seguro", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Vencimiento_Seguro").Value = Me.dteFechaVencimientoSeguro
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TCSC_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_TCSC_Codigo").Value = Val(Me.objCataTipoCoberturaSeguro.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Monto_Maximo_Seguro", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Monto_Maximo_Seguro").Value = Me.dblMontoMaximoSeguro
'                '90
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_GASC_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_GASC_Codigo").Value = Val(Me.objCataGarantiaSeguroCliente.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Cliente_BASC", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Cliente_BASC").Value = Me.intClienteBASC
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Condiciones_Seguro", SqlDbType.VarChar, 1000) : Me.objGeneral.ComandoSQL.Parameters("@par_Condiciones_Seguro").Value = Me.strCondicionesSeguro
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Antiguedad_Vehiculo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Antiguedad_Vehiculo").Value = Me.intAntiguedadVehiculo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Cliente_Prospecto", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Cliente_Prospecto").Value = Me.intClienteProspecto
'                '95
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Referencio_Prospecto", SqlDbType.VarChar, 100) : Me.objGeneral.ComandoSQL.Parameters("@par_Referencio_Prospecto").Value = Me.strReferencioProspecto
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Referencias_Verificadas", SqlDbType.VarChar, 1000) : Me.objGeneral.ComandoSQL.Parameters("@par_Referencias_Verificadas").Value = Me.strReferenciasVerificadas
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Aseguradora", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Aseguradora").Value = Me.objAseguradora.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TARC_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_TARC_Codigo").Value = Val(Me.objCataTarifarioRepreComercial.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Es_Filial", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Es_Filial").Value = Me.intEsFilial
'                '100
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo_Filial", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo_Filial").Value = Me.strCodigoFilial
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo_Contable_Filial", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo_Contable_Filial").Value = Me.strCodigoContableFilial
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Ingreso_Cliente", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Ingreso_Cliente").Value = Me.dteFechaIngresoCliente
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Cliente_Exterior", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Cliente_Exterior").Value = Me.intClienteExterior
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Aplica_Rete_Fuente", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Aplica_Rete_Fuente").Value = Me.intAplicaReteFuente

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Codigo_Contable_Filial2", SqlDbType.VarChar, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Codigo_Contable_Filial2").Value = Me.strCodigoActividad
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Ultimo_Cargue", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Ultimo_Cargue").Value = Me.dteFechaUltimoCargue
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CALI_Codigo", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_CALI_Codigo").Value = Val(Me.objCataCategoriaLicenciaConductor.Retorna_Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TIID_Codigo_Titular", SqlDbType.Char, 3) : Me.objGeneral.ComandoSQL.Parameters("@par_TIID_Codigo_Titular").Value = Me.objCataTipoIdentificacionTitular.Retorna_Codigo()
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Digito_Chequeo_Titular", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Digito_Chequeo_Titular").Value = Me.intDigitoChequeoTitular
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Nombre_Titular", SqlDbType.VarChar, 60) : Me.objGeneral.ComandoSQL.Parameters("@par_Nombre_Titular").Value = Me.strNombreTitular
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Identificacion_Titular", SqlDbType.VarChar, 15) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Identificacion_Titular").Value = Me.strNumeroIdentificacionTitular

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Consulta_Lista_Clinton", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Consulta_Lista_Clinton").Value = Me.intConsultaListaClinton
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Factura_Faltantes", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Factura_Faltantes").Value = Me.intFacturaFaltantes
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Fecha_Nacimiento_Conyuge", SqlDbType.DateTime) : Me.objGeneral.ComandoSQL.Parameters("@par_Fecha_Nacimiento_Conyuge").Value = Me.dteFechNaciCony.Date
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono_Conyuge", SqlDbType.VarChar) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono_Conyuge").Value = Me.strTeleCony
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_IANS_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_IANS_Codigo").Value = Me.intIansCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_FPCO_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_FPCO_Codigo").Value = Me.CataFormaPago.intCampoCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Dias_Plazo_Pago", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_Dias_Plazo_Pago").Value = Val(Me.DiasPlazoPago)

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Descuento_Flete", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Descuento_Flete").Value = Val(Me.dblDescuentoFlete)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Descuento_Manejo", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Descuento_Manejo").Value = Val(Me.dblDescuentoManejo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Descuento_Minimo_Unidad", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Descuento_Minimo_Unidad").Value = Val(Me.dblDescuentoMinimoUnidad)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Descuento_Urgencia", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Descuento_Urgencia").Value = Val(Me.dblDescuentoUrgencia)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Descuento_Kg_Adicional", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Descuento_Kg_Adicional").Value = Val(Me.dblDescuentoKgAdicional)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Descuento_Documento_Retorno", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Descuento_Documento_Retorno").Value = Val(Me.dblDescuentoDocumentoRetorno)

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Minimo_Kilos_Cobrar", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Minimo_Kilos_Cobrar").Value = Val(Me.dblMinimoKilosCobrar)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Porcentaje_Seguro", SqlDbType.Money) : Me.objGeneral.ComandoSQL.Parameters("@par_Porcentaje_Seguro").Value = Val(Me.dblPorcentajeSeguro)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_PAIS_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_PAIS_Codigo").Value = Me.CodigoPais
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Aplica_Rete_IVA", SqlDbType.SmallInt) : Me.objGeneral.ComandoSQL.Parameters("@par_Aplica_Rete_IVA").Value = Me.intAplicReteIva

'                If IsNothing(Me.bytFoto) Then
'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value
'                Else
'                    If Me.bytFoto(0) = BYTES_IMAGEN_DEFECTO Then
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Foto").Value = DBNull.Value
'                    Else
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_Foto", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Foto").Value = Me.bytFoto
'                    End If
'                End If


'                If IsNothing(Me.bytHuella_Digital) Then
'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Huella_Digital", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Huella_Digital").Value = DBNull.Value
'                Else

'                    If Me.bytHuella_Digital(0) = BYTES_IMAGEN_DEFECTO Then
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_Huella_Digital", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Huella_Digital").Value = DBNull.Value
'                    Else
'                        Me.objGeneral.ComandoSQL.Parameters.Add("@par_Huella_Digital", SqlDbType.Image) : Me.objGeneral.ComandoSQL.Parameters("@par_Huella_Digital").Value = Me.bytHuella_Digital
'                    End If
'                End If


'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                    If Me.bolAuditar Then
'                        Me.objControlAuditoria.Insertar_Control_Auditoria_SQL(Me.objGeneral.ComandoSQL.Parameters, "Terceros", ControlAuditoria.TRANSACCION_MODIFICAR, Me.strError)
'                    End If
'                Else
'                    Modificar_SQL = False
'                End If

'            Catch ex As Exception
'                Modificar_SQL = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & ex.Message.ToString()
'            End Try
'        End Function

'        Private Function Puede_Borrar() As Boolean
'            Try
'                Dim strDato As String = ""

'                Puede_Borrar = True

'                'perfil cliente
'                If Me.Tiene_Perfil(PERFIL_CLIENTE) Then
'                    If Me.Tiene_Contrato_Cliente(strDato) Then
'                        Me.strError = "No se puede eliminar el cliente " & Me.lonCodigo & " porque esta asociado al contrato de transporte número " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                    If Me.Tiene_Factura_Cliente(strDato) Then
'                        Me.strError = "No se puede eliminar el cliente " & Me.lonCodigo & " porque esta asociado a la factura número " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                    If Me.Tiene_Comprobante(strDato) Then
'                        Me.strError = "No se puede eliminar el tercero " & Me.lonCodigo & " porque esta asociado al comprobante " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                End If

'                'perfil remitente
'                If Me.Tiene_Perfil(PERFIL_REMITENTE) Then
'                    If Me.Existe_En_Contrato_Cliente(strDato) Then
'                        Me.strError = "No se puede eliminar el remitente " & Me.lonCodigo & " porque esta asociado al contrato de transporte número " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                End If

'                'perfil empresa afiliadora
'                If Me.Tiene_Perfil(PERFIL_EMPRESA_AFILIADORA) Then
'                    If Me.Es_Empresa_Afiliadora_Vehiculo(strDato) Then
'                        Me.strError = "No se puede eliminar la empresa afiliadora " & Me.lonCodigo & " porque esta asociado al vehículo " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                End If

'                'perfil propietario
'                If Me.Tiene_Perfil(PERFIL_PROPIETARIO) Then
'                    If Me.Es_Propietario_Vehiculo(strDato) Then
'                        Me.strError = "No se puede eliminar el tercero " & Me.lonCodigo & " porque es propietario del vehículo " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                    If Me.Es_Propietario_Semiremolque(strDato) Then
'                        Me.strError = "No se puede eliminar el tercero " & Me.lonCodigo & " porque es propietario del semiremolque " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                    If Me.Es_Propietario_Manifiesto(strDato) Then
'                        Me.strError = "No se puede puede eliminar el propietario " & Me.lonCodigo & " porque esta asociado al manifiesto " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                End If

'                'perfil tenedor
'                If Me.Tiene_Perfil(PERFIL_TENEDOR) Then
'                    If Me.Es_Tenedor_Vehiculo(strDato) Then
'                        Me.strError = "No se puede eliminar el tercero " & Me.lonCodigo & " porque es tenedor del vehículo " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                    If Me.Es_Tenedor_Manifiesto(strDato) Then
'                        Me.strError = "No se puede puede eliminar el tenedor " & Me.lonCodigo & " porque esta asociado al manifiesto " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                End If

'                'perfil conductor
'                If Me.Tiene_Perfil(PERFIL_CONDUCTOR) Then
'                    If Me.Es_Conductor_Vehiculo(strDato) Then
'                        Me.strError = "No se puede eliminar el tercero " & Me.lonCodigo & " porque es conductor del vehículo " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                    If Me.Es_Conductor_Manifiesto(strDato) Then
'                        Me.strError = "No se puede puede eliminar el conductor " & Me.lonCodigo & " porque esta asociado al manifiesto " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                End If

'                'perfil proveedor
'                If Me.Tiene_Perfil(PERFIL_PROVEEDOR) Then
'                    If Me.Tiene_Orden_Compra(strDato) Then
'                        Me.strError = "No se puede eliminar el proveedor " & Me.lonCodigo & " porque esta asociado a una orden de compra."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                End If

'                'perfil aseguradora
'                If Me.Tiene_Perfil(PERFIL_ASEGURADORA) Then
'                    If Me.Es_Aseguradora_Vehiculo(strDato) Then
'                        Me.strError = "No se puede eliminar la empresa aseguradora " & Me.lonCodigo & " porque ya tiene asignado el vehículo " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                End If

'                'perfil empleado 
'                If Me.Tiene_Perfil(PERFIL_EMPLEADO) Then
'                    If Me.Es_Usuario_Sistema(strDato) Then
'                        Me.strError = "No se puede eliminar el empleado " & Me.lonCodigo & " porque esta asignado al usuario " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                    If Me.Es_Conductor_Vehiculo(strDato) Then
'                        Me.strError = "No se puede eliminar el tercero " & Me.lonCodigo & " porque es conductor del vehículo " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                    If Me.Repre_Comercial_Tiene_Contrato_Cliente(strDato) Then
'                        Me.strError = "No se puede eliminar el representante comercial " & Me.lonCodigo & " porque es representante comercial del Tarifario número " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                    If Me.Funcionario_Tiene_Contrato_Cliente(strDato) Then
'                        Me.strError = "No se puede eliminar el funcionario " & Me.lonCodigo & " porque es un funcionario asociado al contrato de transporte número " & strDato & "."
'                        Puede_Borrar = False
'                        Exit Function
'                    End If
'                End If

'                'perfil seguridad social


'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Puede_Borrar: " & ex.Message.ToString()
'                Puede_Borrar = False
'            End Try
'        End Function

'        Private Function Borrar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_terceros", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                    If Me.bolAuditar Then
'                        Me.objControlAuditoria.NumeroDocumentoEncabezado = Me.lonCodigo
'                        Me.objControlAuditoria.Insertar_Control_Auditoria_SQL(ComandoSQL.Parameters, "Terceros", ControlAuditoria.TRANSACCION_ELIMINAR, Me.strError)
'                    End If
'                Else
'                    Borrar_SQL = False
'                End If

'            Catch ex As Exception
'                Borrar_SQL = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Call Instanciar_Objetos_Alternos()

'                If Me.isEmpleado Then
'                    Datos_Requeridos = Datos_Requeridos_Empleado(Mensaje)
'                ElseIf GuardaRemitenteDestinatario Then
'                    Datos_Requeridos = Datos_Requeridos_Remitente_Destinatario(Me.strNombrePerfilDatosRequeridos, Mensaje)
'                Else
'                    Datos_Requeridos = Datos_Requeridos_Tercero(Mensaje)
'                End If
'            Catch ex As Exception
'                Datos_Requeridos = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & ex.Message.ToString()
'            End Try
'        End Function

'        Private Function Datos_Requeridos_Tercero(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Dim intCont As Short = 0
'                Dim strCodiCiud As String = ""
'                Me.strError = ""
'                Me.objEmpresa.CataTipoParametrizacionMargenRentabilidad = New Catalogo(Me.objEmpresa, "TPMR")
'                Me.objEmpresa.Ciudad = New Ciudad(Me.objEmpresa)
'                Datos_Requeridos_Tercero = True
'                Dim arrCorreos(0) As String

'                Me.objEmpresa.Retorna_Campo(Me.objEmpresa.Codigo, "CIUD_Codigo", strCodiCiud, strError)
'                Me.objEmpresa.Retorna_Campo(Me.objEmpresa.Codigo, "TPMR_Codigo", Me.objEmpresa.CataTipoParametrizacionMargenRentabilidad.Campo1, strError)
'                Me.objEmpresa.Ciudad.Codigo = Val(strCodiCiud)



'                If Len(Trim(Me.strNumeroIdentificacion)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese el número de identificación del Tercero."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Tercero = False
'                Else
'                    If ClienteExterior = General.CERO Then
'                        If Not IsNumeric(Me.strNumeroIdentificacion) Then
'                            intCont += 1
'                            Me.strError += intCont & ". El número de identificación debe ser un valor numérico."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If

'                        Call Validar_Rango_Identificacion()

'                        If Val(Me.objCataTipoNaturaleza.Campo1) <> NATURALEZA_JURIDICA Then
'                            If Val(Me.strNumeroIdentificacion) < Me.lonRangInicIdenTerc Or Val(Me.strNumeroIdentificacion) > Me.lonRangFinaIdenTerc Then
'                                intCont += 1
'                                Me.strError += intCont & ". El número de " & Me.strTipoIden & " ingresado no se encuentra dentro del rango válido."
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False
'                            End If
'                        Else
'                            If Val(Me.strNumeroIdentificacion) < lonRangInicIdenTerc Or Val(Me.strNumeroIdentificacion) > lonRangFinaIdenTerc Then
'                                intCont += 1
'                                Me.strError += intCont & ". El número de " & Me.strTipoIden & " ingresado no se encuentra dentro del rango válido."
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False
'                            End If
'                        End If
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_ESTUDIO_SEGURIDAD_PROPIETARIO_TENEDOR) Then

'                    If Me.isPropietario Or Me.isTenedor Then
'                        If Not bolModificar Then
'                            If Not Me.Tiene_Estudio_Seguridad_Tercero(Me.strNumeroIdentificacion) Then
'                                intCont += 1
'                                Me.strError += intCont & ".Los terceros con perfil Propietario o tenedor deben tener un estudio de seguridad valido"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False
'                            End If
'                        End If
'                    End If
'                Else
'                    If Me.isPropietario Or Me.isTenedor Then
'                        If Not bolModificar Then
'                            If objValidacion.Consultar(Me.objEmpresa, VALIDA_ESTUDIO_SEGURIDAD_CONDUCTOR_ANTES_OTRO_PERFIL) Then
'                                If Not Me.Tiene_Estudio_Seguridad_Tercero(Me.strNumeroIdentificacion) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". El Tercero ingresado no tiene un Estudio de seguridad para este perfil que este vigente y aprobado para esta oficina."
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos_Tercero = False
'                                End If
'                            End If
'                        End If
'                    End If
'                End If

'                If Not bolModificar Then
'                    If Me.isConductor And Me.ConductorPropio <> General.PROPIO Then
'                        If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_ESTUDIO_SEGURIDAD_CONDUCTOR_TERCERO) Then
'                            If Not Me.Tiene_Estudio_Seguridad_Tercero(Me.strNumeroIdentificacion) Then
'                                intCont += 1
'                                Me.strError += intCont & " " & Me.objValidacion.MensajeValidacion
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False
'                            End If
'                        End If
'                    End If
'                End If

'                If Me.bolModificar And Me.Codigo <> General.CODIGO_TERCERO_UNO Then
'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_ESTUDIO_SEGURIDAD_CONDUCTOR_ANTES_OTRO_PERFIL) Then
'                        If Me.isConductor And Me.ConductorPropio <> General.PROPIO Then
'                            If Not Me.Tiene_Perfil(PERFIL_CONDUCTOR) Then
'                                If Not Me.Tiene_Estudio_Seguridad_Tercero(Me.strNumeroIdentificacion) Then
'                                    intCont += 1
'                                    Me.strError += intCont & " " & Me.objValidacion.MensajeValidacion
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos_Tercero = False
'                                End If
'                            End If
'                        End If
'                    End If
'                End If

'                If Val(Me.objCataTipoNaturaleza.Campo1) = Tercero.NATURALEZA_NATURAL Then

'                    If Len(Trim(Me.strNombre)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese el nombre del Tercero."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If


'                    If Me.strNombre <> "" And Len(Me.strNombre) < General.MINIMO_CUATRO_CARACTERES_INGRESO_NOMBRE Then
'                        intCont += 1
'                        strError += intCont & ". El nombre del Tercero no puede tener menos de cuatro (4) carácteres"
'                        strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If


'                    If UCase(Me.objCataTipoIdentificacion.Campo1.Trim) <> General.TIID_NIT Then
'                        If Len(Me.strApellido1) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Ingrese el primer apellido del Tercero."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                    End If
'                End If

'                If UCase(Me.objCataTipoIdentificacion.Campo1.Trim) = General.TIID_NIT Then
'                    If ClienteExterior = General.CERO Then
'                        If Not Me.objValidacion.Consultar(Me.objEmpresa, NO_EVALUAR_DIGITO_CHEQUEO) Then
'                            If Not IsNumeric(Me.intDigitoChequeo) Then
'                                intCont += 1
'                                Me.strError += intCont & ". El dígito de chequeo debe ser numérico."
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False
'                            End If
'                            If Not Me.objGeneral.Valida_Digito_Chequeo(Me.strNumeroIdentificacion, Me.intDigitoChequeo) Then
'                                intCont += 1
'                                Me.strError += intCont & ". Digito de chequeo inválido."
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False
'                            End If
'                        End If
'                    End If
'                End If

'                If UCase(Me.objCataTipoIdentificacion.Campo1.Trim) = General.TIID_CEDULA Then
'                    If Me.objCiudadExpedicionIdentificacion.Codigo = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la ciudad de expedición de la cédula."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                End If

'                If Val(Me.objCataTipoNaturaleza.Campo1) = Tercero.NATURALEZA_JURIDICA Then
'                    If Len(Me.strRepresentanteLegal) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese el nombre de representante legal."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Len(Me.strNombre) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese el nombre de la razón social."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Me.strNombre <> "" And Len(Me.strNombre) < General.MINIMO_CUATRO_CARACTERES_INGRESO_NOMBRE Then
'                        intCont += 1
'                        strError += intCont & ". El nombre de la Razón Social no puede tener menos de cuatro (4) carácteres"
'                        strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If


'                    If Len(Trim(Me.strApellido1)) > 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Por su naturaleza este tercero no debe tener primer apellido."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Len(Trim(Me.strApellido2)) > 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Por su naturaleza este tercero no debe tener segundo apellido."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                End If

'                If Me.CodigoPais = General.CERO Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese el país de localización del tercero."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Tercero = False
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CIUDAD_LOCALIZACION) Then
'                    If Me.objCiudad.Codigo = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la ciudad de localización del tercero."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                End If

'                If Len(Trim(Me.strDireccion)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese la dirección."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Tercero = False
'                End If

'                If Len(Trim(Me.strTelefono1)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese el teléfono1."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Tercero = False
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CODIGO_POSTAL) Then
'                    If Len(Trim(Me.strCodigoPostal)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar el Código Postal"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                End If

'                If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_CELULAR_POR_PERFIL) Then
'                    If Me.isPropietario Or Me.isTenedor Or Me.isConductor Then
'                        If Len(Trim(Me.strCelular)) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar el Celular para terceros con perfiles de Propietario, Tenedor y/o Conductor."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                    End If
'                End If

'                If isCliente Then
'                    If Len(Trim(Me.strEmail)) > 0 Then
'                        If Me.objGeneral.Separar_Correos(Me.strEmail, arrCorreos) Then
'                            For Each Correo In arrCorreos
'                                If Not Me.objGeneral.Email_Valido(Correo) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". El Email ingresado no es válido"
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos_Tercero = False
'                                End If
'                            Next
'                        Else
'                            intCont += 1
'                            Me.strError += intCont & ". El Email ingresado no es válido"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar el Email del cliente"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                End If

'                If Not Me.dteFechaNacimiento = Date.MinValue Then
'                    If Not Me.objGeneral.Fecha_Valida(Me.dteFechaNacimiento) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Fecha de nacimiento inválida, digite dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                Else
'                    Me.dteFechaNacimiento = Date.Parse(FECHA_NULA)
'                End If

'                If Me.dteFechaLicencia = Date.MinValue Then
'                    Me.dteFechaLicencia = Date.Parse(FECHA_NULA)
'                End If

'                If Me.dteFechaVenceContrato = Date.MinValue Then
'                    Me.dteFechaVenceContrato = Date.Parse(FECHA_NULA)
'                End If
'                If Me.intAfiliadoEPS <> 0 Then
'                    If Not Me.dteFechaAfiliacionEPS = General.FECHA_NULA Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFechaAfiliacionEPS) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha afiliación EPS inválida, digite dd/mm/aaaa"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        Else
'                            If Me.dteFechaAfiliacionEPS.Date > Date.Today.Date Then
'                                intCont += 1
'                                Me.strError += intCont & ". La fecha afiliación EPS no puede ser superior a la actual, digite dd/mm/aaaa"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False
'                            End If
'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la fecha afiliación EPS en el formato dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                Else
'                    Me.dteFechaAfiliacionEPS = General.FECHA_NULA
'                End If
'                If Me.intAfiliadoARP <> 0 Then
'                    If Not Me.dteFechaAfiliacionARP = General.FECHA_NULA Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFechaAfiliacionARP) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha afiliación ARP inválida, digite dd/mm/aaaa"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        Else
'                            If Me.dteFechaAfiliacionARP.Date > Date.Today.Date Then
'                                intCont += 1
'                                Me.strError += intCont & ". La fecha afiliación ARP no puede ser superior a la actual, digite dd/mm/aaaa"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False
'                            End If
'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la fecha afiliación ARP en el formato dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                Else
'                    Me.dteFechaAfiliacionARP = General.FECHA_NULA
'                End If

'                If Me.intAfiliadoFondoPensiones <> 0 Then
'                    If Not Me.dteFechaAfiliacionFondoPensiones = General.FECHA_NULA Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFechaAfiliacionFondoPensiones) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha afiliación fondo de pensiones inválida, digite dd/mm/aaaa"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        Else
'                            If Me.dteFechaAfiliacionFondoPensiones.Date > Date.Today.Date Then
'                                intCont += 1
'                                Me.strError += intCont & ". La fecha afiliación fondo de pensiones no puede ser superior a la actual, digite dd/mm/aaaa"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False
'                            End If
'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la fecha afiliación al fondo de pensiones en el formato dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                Else
'                    Me.dteFechaAfiliacionFondoPensiones = General.FECHA_NULA
'                End If

'                If Me.intAfiliadoFondoCesantias <> 0 Then
'                    If Not Me.dteFechaAfiliacionFondoCesantias = General.FECHA_NULA Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFechaAfiliacionFondoCesantias) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha afiliación fondo de cesantias inválida, digite dd/mm/aaaa"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        Else
'                            If Me.dteFechaAfiliacionFondoCesantias.Date > Date.Today.Date Then
'                                intCont += 1
'                                Me.strError += intCont & ". La fecha de afiliación fondo de cesantías no puede ser superior a la actual, digite dd/mm/aaaa"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False

'                            End If

'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la fecha de afiliación al fondo de cesantías en el formato dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                Else
'                    Me.dteFechaAfiliacionFondoCesantias = General.FECHA_NULA
'                End If

'                If Not Me.dteFechaContrato = Date.MinValue Then
'                    If Not Me.objGeneral.Fecha_Valida(Me.dteFechaContrato) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Fecha vinculación contrato inválida, digite dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                Else
'                    Me.dteFechaContrato = Date.Parse(FECHA_NULA)
'                End If

'                If Not Me.dteFechaVenceContrato = Date.MinValue Then
'                    If Not Me.objGeneral.Fecha_Valida(Me.dteFechaVenceContrato) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Fecha vencimiento contrato inválida, digite dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                Else
'                    Me.dteFechaVenceContrato = Date.Parse(FECHA_NULA)
'                End If

'                If Me.isConductor Then
'                    If Len(Trim(Me.strNumeroLicencia)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese el número de licencia del conductor."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Me.CataCategoriaLicenciaConductor.Retorna_Codigo = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la categoria de la licencia del conductor."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Me.dteFechaLicencia = General.FECHA_NULA Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe digitar la fecha de vencimiento de la licencia, digite dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    Else
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFechaLicencia) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha de vencimiento de la licencia inválida, digite dd/mm/aaaa"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        Else
'                            If Me.dteFechaLicencia < Date.Now Then
'                                intCont += 1
'                                Me.strError += intCont & ". Fecha de vencimiento de la licencia debe ser superior a la fecha actual"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False
'                            End If
'                        End If
'                    End If

'                End If

'                If Me.dteFechaVencimientoSeguro = Date.MinValue Then
'                    Me.dteFechaVencimientoSeguro = Date.Parse(FECHA_NULA)
'                End If

'                If Me.isCliente Then
'                    If Me.objRepresentanteComercial.Codigo = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe seleccionar un representante comercial para este cliente"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Me.dblDescuentoFlete < -99 Or dblDescuentoFlete > 99 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El procentaje de descuento para el flete debe estar entre -99% y 99%"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Me.dblDescuentoManejo < -99 Or dblDescuentoManejo > 99 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El procentaje de descuento  manejo debe estar entre -99% y 99%"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Me.dblDescuentoMinimoUnidad < -99 Or dblDescuentoMinimoUnidad > 99 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El procentaje de descuento mínimo unidad debe estar entre -99% y 99%"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Me.dblDescuentoUrgencia < -99 Or dblDescuentoUrgencia > 99 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El procentaje de descuento urgencia debe estar entre -99% y 99%"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Me.dblDescuentoKgAdicional < -99 Or dblDescuentoKgAdicional > 99 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El procentaje de descuento para el Kg adicional debe estar entre -99% y 99%"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Me.dblDescuentoDocumentoRetorno < -99 Or dblDescuentoDocumentoRetorno > 99 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El procentaje de descuento para el valor de documento retorno debe estar entre -99% y 99%"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Me.dblPorcentajeSeguro < 0 Or dblPorcentajeSeguro > 99 Then
'                        intCont += 1
'                        Me.strError += intCont & ". El procentaje del seguro debe estar entre 0% y 99%"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Me.objEmpresa.CataTipoParametrizacionMargenRentabilidad.Retorna_Codigo = MARGEN_RENTABILIDAD_POR_CLIENTE Then
'                        If Me.dblMargenRentabilidadDespachoNacionalVehiculoPropio = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar el porcentaje de margen de rentabilidad nacional para vehículos propios"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                        If Me.dblMargenRentabilidadDespachoNacionalVehiculoTercero = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar el porcentaje de margen de rentabilidad nacional para vehículos terceros"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                        If Me.dblMargenRentabilidadDespachoUrbanoVehiculoPropio = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar el porcentaje de margen de rentabilidad urbano para vehículos propios"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                        If Me.dblMargenRentabilidadDespachoUrbanoVehiculoTercero = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Debe ingresar el porcentaje de margen de rentabilidad urbano para vehículos terceros"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                    End If

'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_FOTO_CLIENTE) Then
'                        If Not bolFoto Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                    End If

'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_HUELLA_DIGITAL_CLIENTE) Then
'                        If Not bolHuella_Digital Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                    End If

'                    'Datos requeridos Cliente Condiciones Comerciales
'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_DIAS_BLOQUEO_MORA) Then
'                        If Me.intDiasBloqueoMora = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                    End If

'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_INGRESO_ACTIVIDAD_CLIENTE) Then
'                        If Me.CodigoContableFilial = "" Then
'                            If Me.CodigoActividad = "" Then
'                                intCont += 1
'                                Me.strError += intCont & ". " & "Ingrese al menos una actividad para el cliente"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False
'                            Else
'                                If Not Me.CodigoActividad.StartsWith(INICIO_ACTIVIDAD_INDIRECTA) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". " & "El código de la Actividad Indirecta debe iniciar con los caracteres: " & INICIO_ACTIVIDAD_INDIRECTA
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos_Tercero = False
'                                End If
'                            End If
'                        Else
'                            If Not Me.CodigoContableFilial.StartsWith(INICIO_ACTIVIDAD_DIRECTA) Then
'                                intCont += 1
'                                Me.strError += intCont & ". " & "El código de la Actividad Directa debe iniciar con los caracteres: " & INICIO_ACTIVIDAD_DIRECTA
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Tercero = False
'                            End If
'                            If Me.CodigoActividad <> "" Then
'                                If Not Me.CodigoActividad.StartsWith(INICIO_ACTIVIDAD_INDIRECTA) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". " & "El código de la Actividad Indirecta debe iniciar con los caracteres: " & INICIO_ACTIVIDAD_INDIRECTA
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos_Tercero = False
'                                End If
'                            End If
'                        End If
'                    End If


'                End If


'                If Val(Me.objCataEstado.Retorna_Codigo) = General.ESTADO_INACTIVO Then
'                    If Val(Me.objCataCausaBloqueo.Retorna_Codigo) = DESBLOQUEADO Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe seleccionar una causa de bloqueo para inactivar a este tercero"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                End If

'                If Val(Me.objCataCausaBloqueo.Retorna_Codigo) <> DESBLOQUEADO Then
'                    If Val(Me.objCataEstado.Retorna_Codigo) = General.ESTADO_ACTIVO Then
'                        intCont += 1
'                        Me.strError += intCont & ". Este tercero no puede tener estado activo puesto que se ha seleccionado una causa de bloqueo"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If

'                    If Len(Trim(Me.strJustificacionBloqueo)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Por favor ingrese la justificación del bloqueo de este tercero"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                End If

'                If Val(Me.objCataEstado.Retorna_Codigo) = 2 Then
'                    If Val(Me.objCataCausaBloqueo.Retorna_Codigo) = DESBLOQUEADO Then
'                        intCont += 1
'                        Me.strError += intCont & ".Debe seleccionar una causa de bloqueo puesto que se ha seleccionado el estado de bloqueado"
'                        Me.strError += "<Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                End If

'                'Se comentaría ya que nadie está utilizando el check de Filial en clientes, pero el campo si es utilizado para identificar 
'                'clientes que tienen sus propias empresas de transporte, como el caso de Espumas SantaFe con R&R

'                'If Me.intEsFilial = 1 Then
'                '    If Trim(Me.strCodigoFilial) = "" Then
'                '        intCont += 1
'                '        Me.strError += intCont & ". Debe ingresar el código de la filial"
'                '        Me.strError += " <Br />"
'                '        Datos_Requeridos_Tercero = False
'                '    End If
'                '    If Trim(Me.strCodigoContableFilial) = "" Then
'                '        intCont += 1
'                '        Me.strError += intCont & ". Debe ingresar el código contable de la filial"
'                '        Me.strError += " <Br />"
'                '        Datos_Requeridos_Tercero = False
'                '    End If
'                'End If

'                'validación de obigatoriedad de referencias verificadas cuando se da de alta un cliente
'                If Me.isProspectoCliente Then
'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_REFERENCIAS_VERIFICADAS_PROSPECTO_CLIENTE) Then
'                        If Trim(Me.strReferenciasVerificadas) = "" Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                    End If
'                End If
'                If Me.dteFechaIngresoCliente = Date.MinValue Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese la fecha de ingreso de cliente en formato dd/mm/aaaa"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Tercero = False
'                End If

'                If Me.isConductor Then
'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_FOTO_CONDUCTOR) Then
'                        If Me.bolFoto = False Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                    End If
'                End If

'                If Me.isConductor Then
'                    If Me.objValidacion.Consultar(Me.objEmpresa, VALIDA_HUELLA_DIGITAL_CONDUCTOR) Then
'                        If Me.bolHuella_Digital = False Then
'                            intCont += 1
'                            Me.strError += intCont & ". " & Me.objValidacion.MensajeValidacion
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Tercero = False
'                        End If
'                    End If
'                End If

'                If Me.isAseguradora Then
'                    If Me.objEstudioSeguridad.Numero > 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Una Aseguradora no puede tener estudio de seguridad"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Tercero = False
'                    End If
'                End If

'                If Me.dteFechaUltimoCargue = Date.MinValue Then
'                    Me.dteFechaUltimoCargue = Date.Parse(FECHA_NULA)
'                End If
'            Catch ex As Exception
'                Datos_Requeridos_Tercero = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos_Tercero: " & ex.Message.ToString()
'            End Try
'        End Function

'        Private Function Datos_Requeridos_Empleado(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Me.strError = ""
'                Dim strCliente As String = ""
'                Dim intCont As Short
'                intCont = 0
'                Dim arrCorreos(0) As String

'                Datos_Requeridos_Empleado = True

'                If Len(Trim(Me.strNumeroIdentificacion)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese el número de identificación del Empleado."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Empleado = False
'                Else
'                    If Not IsNumeric(Me.strNumeroIdentificacion) Then
'                        intCont += 1
'                        Me.strError += intCont & ". El número de identificación debe ser un valor numérico."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If

'                    Call Validar_Rango_Identificacion()

'                    If Val(Me.objCataTipoNaturaleza.Campo1) <> NATURALEZA_JURIDICA Then
'                        If Val(Me.strNumeroIdentificacion) < Me.lonRangInicIdenTerc Or Val(Me.strNumeroIdentificacion) > Me.lonRangFinaIdenTerc Then
'                            intCont += 1
'                            Me.strError += intCont & ". El número de " & Me.strTipoIden & " ingresado no se encuentra dentro del rango válido."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Empleado = False
'                        End If
'                    Else
'                        If Val(Me.strNumeroIdentificacion) < lonRangInicIdenTerc Or Val(Me.strNumeroIdentificacion) > lonRangFinaIdenTerc Then
'                            intCont += 1
'                            Me.strError += intCont & ". El número de " & Me.strTipoIden & " ingresado no se encuentra dentro del rango válido."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Empleado = False
'                        End If
'                    End If
'                End If

'                If Not IsNumeric(Me.intDigitoChequeo) Then
'                    intCont += 1
'                    Me.strError += intCont & ". El dígito de chequeo debe ser numérico."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Empleado = False
'                End If

'                If Len(Trim(Me.strNombre)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese el nombre del Empleado."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Empleado = False
'                End If

'                If Me.strNombre <> "" And Len(Me.strNombre) < General.MINIMO_CUATRO_CARACTERES_INGRESO_NOMBRE Then
'                    intCont += 1
'                    strError += intCont & ". El nombre del Empleado no puede tener menos de cuatro (4) carácteres"
'                    strError += " <Br />"
'                    Datos_Requeridos_Empleado = False
'                End If


'                If Val(Me.objCataTipoNaturaleza.Campo1) = Tercero.NATURALEZA_NATURAL Then
'                    If UCase(Me.objCataTipoIdentificacion.Campo1.Trim) <> General.TIID_NIT Then
'                        If Len(Me.strApellido1) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Ingrese el primer apellido del Empleado."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Empleado = False
'                        End If
'                    End If
'                End If

'                If UCase(Me.objCataTipoIdentificacion.Campo1.Trim) <> General.TIID_CEDULA Then
'                    If Me.objCiudadExpedicionIdentificacion.Codigo = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la ciudad de expedición de la cédula."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If
'                End If

'                If Val(Me.objCataTipoNaturaleza.Campo1) = Tercero.NATURALEZA_JURIDICA Then
'                    If Len(Me.strRepresentanteLegal) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese el nombre del representante legal."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If
'                End If

'                If Len(Trim(Me.strDireccion)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese la dirección."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Empleado = False
'                End If

'                If Len(Trim(Me.strTelefono1)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese el teléfono1."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Empleado = False
'                End If

'                If Len(Trim(Me.strTelefono2)) = 0 Then
'                    Me.strTelefono2 = ""
'                End If

'                If isCliente Then
'                    If Len(Trim(Me.strEmail)) > 0 Then
'                        If Me.objGeneral.Separar_Correos(Me.strEmail, arrCorreos) Then
'                            For Each Correo In arrCorreos
'                                If Not Me.objGeneral.Email_Valido(Correo) Then
'                                    intCont += 1
'                                    Me.strError += intCont & ". El Email ingresado no es válido"
'                                    Me.strError += " <Br />"
'                                    Datos_Requeridos_Empleado = False
'                                End If
'                            Next
'                        Else
'                            intCont += 1
'                            Me.strError += intCont & ". El Email ingresado no es válido"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Empleado = False
'                        End If
'                    End If
'                End If

'                If Not Me.dteFechaNacimiento = Date.MinValue Then
'                    If Not Me.objGeneral.Fecha_Valida(Me.dteFechaNacimiento) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Fecha de nacimiento inválida, digite dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If
'                Else
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese la fecha de nacimiento en el formato dd/mm/aaaa"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Empleado = False
'                End If

'                If Not Me.dteFechaLicencia = Date.MinValue Then
'                    If Not Me.objGeneral.Fecha_Valida(Me.dteFechaLicencia) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la fecha de licencia en el formato dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If
'                Else
'                    Me.dteFechaLicencia = General.FECHA_NULA
'                End If

'                If Me.intAfiliadoEPS <> 0 Then
'                    If Not Me.dteFechaAfiliacionEPS = Date.MinValue Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFechaAfiliacionEPS) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha afiliación EPS inválida, digite dd/mm/aaaa"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Empleado = False
'                        Else
'                            If Me.dteFechaAfiliacionEPS.Date > Date.Today.Date Then
'                                intCont += 1
'                                Me.strError += intCont & ". La fecha afiliación EPS no puede ser superior a la actual, digite dd/mm/aaaa"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Empleado = False
'                            End If
'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la fecha afiliación EPS en el formato dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If
'                Else
'                    Me.dteFechaAfiliacionEPS = General.FECHA_NULA
'                End If

'                If Me.intAfiliadoARP <> 0 Then
'                    If Not Me.dteFechaAfiliacionARP = Date.MinValue Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFechaAfiliacionARP) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha afiliación ARP inválida, digite dd/mm/aaaa"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Empleado = False
'                        Else
'                            If Me.dteFechaAfiliacionARP.Date > Date.Today.Date Then
'                                intCont += 1
'                                Me.strError += intCont & ". La fecha afiliación ARP no puede ser superior a la actual, digite dd/mm/aaaa"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Empleado = False
'                            End If
'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la fecha afiliación ARP en el formato dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If
'                Else
'                    Me.dteFechaAfiliacionARP = General.FECHA_NULA
'                End If

'                If Me.intAfiliadoFondoPensiones <> 0 Then
'                    If Not Me.dteFechaAfiliacionFondoPensiones = Date.MinValue Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFechaAfiliacionFondoPensiones) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha afiliación fondo de pensiones inválida, digite dd/mm/aaaa"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Empleado = False
'                        Else
'                            If Me.dteFechaAfiliacionFondoPensiones.Date > Date.Today.Date Then
'                                intCont += 1
'                                Me.strError += intCont & ". La fecha afiliación fondo de pensiones no puede ser superior a la actual, digite dd/mm/aaaa"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Empleado = False
'                            End If
'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la fecha afiliación al fondo de pensiones en el formato dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If
'                Else
'                    Me.dteFechaAfiliacionFondoPensiones = General.FECHA_NULA
'                End If

'                If Me.intAfiliadoFondoCesantias <> 0 Then
'                    If Not Me.dteFechaAfiliacionFondoCesantias = Date.MinValue Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFechaAfiliacionFondoCesantias) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha afiliación fondo de cesantias inválida, digite dd/mm/aaaa"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Empleado = False
'                        Else
'                            If Me.dteFechaAfiliacionFondoCesantias.Date > Date.Today.Date Then
'                                intCont += 1
'                                Me.strError += intCont & ". La fecha afiliación fondo de cesantías no puede ser superior a la actual, digite dd/mm/aaaa"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Empleado = False

'                            End If

'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la fecha afiliación al fondo de pensiones en el formato dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If
'                Else
'                    Me.dteFechaAfiliacionFondoCesantias = General.FECHA_NULA
'                End If

'                If Me.dblSalarioBasico <= 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El valor del salario básico debe ser superior a (0) cero"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Empleado = False
'                End If

'                If Me.dblPorcentajeComision < 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El porcentaje de la comisión no puede ser menor a (0) cero"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Empleado = False
'                End If

'                If Me.lonCodigo > 0 Then
'                    If Not Me.EsRepresentanteComercial Then
'                        If Me.Es_Representante_Comercial_De_Cliente(strCliente) Then
'                            intCont += 1
'                            Me.strError += intCont & ". El empleado es representante comercial de " & strCliente
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Empleado = False
'                        End If
'                    End If
'                End If



'                If Me.dblPorcentajeComision > 100 Then
'                    intCont += 1
'                    Me.strError += intCont & ". El porcentaje de la comisión no puede superior a 100"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Empleado = False
'                End If

'                If Not Me.dteFechaContrato = Date.MinValue Then
'                    If Not Me.objGeneral.Fecha_Valida(Me.dteFechaContrato) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Fecha de vinculación del contrato inválida, digite dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If
'                Else
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese la fecha de vinculación del contrato en el formato dd/mm/aaaa"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Empleado = False
'                End If

'                If Me.objCataTipoContrato.Retorna_Codigo = Tercero.TIPO_CONTRATO_TERMINO_FIJO Then
'                    If Not Me.dteFechaVenceContrato = Date.MinValue Then
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFechaVenceContrato) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha de vencimiento del contrato inválida, digite dd/mm/aaaa"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Empleado = False
'                        Else
'                            If Me.dteFechaVenceContrato.Date < Me.dteFechaContrato Then
'                                intCont += 1
'                                Me.strError += intCont & ". La fecha de vencimiento del contrato no puede ser inferior a la de creación del mismo"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Empleado = False
'                            End If
'                        End If
'                    Else
'                        intCont += 1
'                        Me.strError += intCont & ". Debe ingresar la fecha de vencimiento del contrato en formato dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If
'                Else
'                    If Me.dteFechaVenceContrato = Date.MinValue Then
'                        Me.dteFechaVenceContrato = General.FECHA_NULA
'                    End If
'                End If
'                If Me.intConductorPropio <> 0 Then
'                    If Len(Trim(Me.strNumeroLicencia)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese el número de licencia del conductor."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If

'                    If Me.CataCategoriaLicenciaConductor.Retorna_Codigo = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese la categoria de la licencia del conductor."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    End If

'                    If Me.dteFechaLicencia = General.FECHA_NULA Then
'                        intCont += 1
'                        Me.strError += intCont & ". Debe digitar la fecha de vencimiento de la licencia, digite dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Empleado = False
'                    Else
'                        If Not Me.objGeneral.Fecha_Valida(Me.dteFechaLicencia) Then
'                            intCont += 1
'                            Me.strError += intCont & ". Fecha de vencimiento de la licencia inválida, digite dd/mm/aaaa"
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Empleado = False

'                        Else
'                            If Me.dteFechaLicencia < Date.Now Then
'                                intCont += 1
'                                Me.strError += intCont & ". Fecha de vencimiento de la licencia debe ser superior a la fecha actual"
'                                Me.strError += " <Br />"
'                                Datos_Requeridos_Empleado = False
'                            End If


'                        End If
'                    End If

'                End If

'                If Me.dteFechaUltimoCargue = Date.MinValue Then
'                    Me.dteFechaUltimoCargue = Date.Parse(FECHA_NULA)
'                End If
'            Catch ex As Exception
'                Datos_Requeridos_Empleado = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos_Empleado: " & ex.Message.ToString()
'            End Try
'        End Function

'        Private Function Datos_Cargue_Descargue_Tercero(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Me.strError = ""
'                Dim intCont As Short
'                intCont = 0
'                Datos_Cargue_Descargue_Tercero = True

'                If Me.objSitioCargueDescargue.Codigo = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Seleccione el sitio de cargue y descargue"
'                    Me.strError += " <Br />"
'                    Datos_Cargue_Descargue_Tercero = False
'                End If
'            Catch ex As Exception
'                Datos_Cargue_Descargue_Tercero = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Cargue_Descargue_Tercero: " & ex.Message.ToString()
'            End Try
'        End Function

'        Private Function Datos_Requeridos_Remitente_Destinatario(ByVal NombrePerfil As String, Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Me.strError = ""
'                Dim intCont As Short
'                intCont = 0
'                Datos_Requeridos_Remitente_Destinatario = True

'                If Me.CataTipoIdentificacion.Campo1.Trim = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar el tipo de identificación del " & NombrePerfil & "."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Remitente_Destinatario = False
'                End If

'                If Len(Trim(Me.strNumeroIdentificacion)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese el número de identificación del Empleado."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Remitente_Destinatario = False
'                Else
'                    If Not IsNumeric(Me.strNumeroIdentificacion) Then
'                        intCont += 1
'                        Me.strError += intCont & ". El número de identificación para el " & NombrePerfil & " debe ser un valor numérico."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Remitente_Destinatario = False
'                    End If
'                    Call Validar_Rango_Identificacion()

'                    If Val(Me.objCataTipoNaturaleza.Campo1) <> NATURALEZA_JURIDICA Then
'                        If Val(Me.strNumeroIdentificacion) < Me.lonRangInicIdenTerc Or Val(Me.strNumeroIdentificacion) > Me.lonRangFinaIdenTerc Then
'                            intCont += 1
'                            Me.strError += intCont & ". El número de " & Me.strTipoIden & " ingresado no se encuentra dentro del rango válido."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Remitente_Destinatario = False
'                        End If
'                    Else
'                        If Val(Me.strNumeroIdentificacion) < lonRangInicIdenTerc Or Val(Me.strNumeroIdentificacion) > lonRangFinaIdenTerc Then
'                            intCont += 1
'                            Me.strError += intCont & ". El número de " & Me.strTipoIden & " ingresado no se encuentra dentro del rango válido."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Remitente_Destinatario = False
'                        End If
'                    End If

'                    'If Val(Me.objCataTipoNaturaleza.Campo1) <> NATURALEZA_JURIDICA Then
'                    '    If Val(Me.strNumeroIdentificacion) < 99999 Or Val(Me.strNumeroIdentificacion) > 9999999999 Then
'                    '        intCont += 1
'                    '        Me.strError += intCont & ". El número de cédula para el " & NombrePerfil & " no se encuentra dentro del rango válido."
'                    '        Me.strError += " <Br />"
'                    '        Datos_Requeridos_Remitente_Destinatario = False
'                    '    End If
'                    'Else
'                    '    If Val(Me.strNumeroIdentificacion) < 100000000 Or Val(Me.strNumeroIdentificacion) > 999999999 Then
'                    '        intCont += 1
'                    '        Me.strError += intCont & ". El NIT Ingresado para el " & NombrePerfil & " no se encuentra dentro del rango válido."
'                    '        Me.strError += " <Br />"
'                    '        Datos_Requeridos_Remitente_Destinatario = False
'                    '    End If
'                    'End If
'                End If

'                If Val(Me.objCataTipoNaturaleza.Campo1) = Tercero.NATURALEZA_NATURAL Then

'                    If Len(Trim(Me.strNombre)) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese el nombre del " & NombrePerfil & "."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Remitente_Destinatario = False
'                    End If


'                    If Me.strNombre <> "" And Len(Me.strNombre) < General.MINIMO_CUATRO_CARACTERES_INGRESO_NOMBRE Then
'                        intCont += 1
'                        strError += intCont & ". El nombre del " & NombrePerfil & " no puede tener menos de cuatro (4) carácteres"
'                        strError += " <Br />"
'                        Datos_Requeridos_Remitente_Destinatario = False
'                    End If


'                    If UCase(Me.objCataTipoIdentificacion.Retorna_Nombre) <> General.TIID_NIT Then
'                        If Len(Me.strApellido1) = 0 Then
'                            intCont += 1
'                            Me.strError += intCont & ". Ingrese el primer apellido del " & NombrePerfil & "."
'                            Me.strError += " <Br />"
'                            Datos_Requeridos_Remitente_Destinatario = False
'                        End If
'                    End If
'                End If


'                If Val(Me.objCataTipoNaturaleza.Campo1) = Tercero.NATURALEZA_JURIDICA Then
'                    If Len(Me.strRepresentanteLegal) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese el nombre de representante legal."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Remitente_Destinatario = False
'                    End If

'                    If Len(Me.strNombre) = 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Ingrese el nombre de la razón social para el " & NombrePerfil & "."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Remitente_Destinatario = False
'                    End If

'                    If Me.strNombre <> "" And Len(Me.strNombre) < General.MINIMO_CUATRO_CARACTERES_INGRESO_NOMBRE Then
'                        intCont += 1
'                        strError += intCont & ". El nombre de la Razón Social para el " & NombrePerfil & " no puede tener menos de cuatro (4) carácteres"
'                        strError += " <Br />"
'                        Datos_Requeridos_Remitente_Destinatario = False
'                    End If


'                    If Len(Trim(Me.strApellido1)) > 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Por su naturaleza este " & NombrePerfil & " no debe tener primer apellido."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Remitente_Destinatario = False
'                    End If

'                    If Len(Trim(Me.strApellido2)) > 0 Then
'                        intCont += 1
'                        Me.strError += intCont & ". Por su naturaleza este " & NombrePerfil & " no debe tener segundo apellido."
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Remitente_Destinatario = False
'                    End If
'                End If

'                If Len(Trim(Me.strDireccion)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese la dirección del " & NombrePerfil & "."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Remitente_Destinatario = False
'                End If

'                If Len(Trim(Me.strTelefono1)) = 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Ingrese el teléfono1 del " & NombrePerfil & "."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos_Remitente_Destinatario = False
'                End If

'                If Not Me.dteFechaNacimiento = Date.MinValue Then
'                    If Not Me.objGeneral.Fecha_Valida(Me.dteFechaNacimiento) Then
'                        intCont += 1
'                        Me.strError += intCont & ". Fecha de nacimiento inválida, digite dd/mm/aaaa"
'                        Me.strError += " <Br />"
'                        Datos_Requeridos_Remitente_Destinatario = False
'                    End If
'                Else
'                    Me.dteFechaNacimiento = Date.Parse(FECHA_NULA)
'                End If

'                If Me.dteFechaLicencia = Date.MinValue Then
'                    Me.dteFechaLicencia = Date.Parse(FECHA_NULA)
'                End If

'                If Me.dteFechaVenceContrato = Date.MinValue Then
'                    Me.dteFechaVenceContrato = Date.Parse(FECHA_NULA)
'                End If

'                If Me.dteFechaAfiliacionEPS = Date.MinValue Then
'                    Me.dteFechaAfiliacionEPS = General.FECHA_NULA
'                End If
'                If Me.dteFechaAfiliacionARP = Date.MinValue Then
'                    Me.dteFechaAfiliacionARP = General.FECHA_NULA
'                End If
'                If Me.dteFechaAfiliacionFondoPensiones = Date.MinValue Then
'                    Me.dteFechaAfiliacionFondoPensiones = General.FECHA_NULA
'                End If
'                If Me.dteFechaAfiliacionFondoCesantias = Date.MinValue Then
'                    Me.dteFechaAfiliacionFondoCesantias = General.FECHA_NULA
'                End If
'                If Me.dteFechaContrato = Date.MinValue Then
'                    Me.dteFechaContrato = Date.Parse(FECHA_NULA)
'                End If
'                If Me.dteFechaVenceContrato = Date.MinValue Then
'                    Me.dteFechaVenceContrato = Date.Parse(FECHA_NULA)
'                End If
'                If Me.dteFechaVencimientoSeguro = Date.MinValue Then
'                    Me.dteFechaVencimientoSeguro = Date.Parse(FECHA_NULA)
'                End If
'                If Me.dteFechaUltimoCargue = Date.MinValue Then
'                    Me.dteFechaUltimoCargue = Date.Parse(FECHA_NULA)
'                End If


'            Catch ex As Exception
'                Datos_Requeridos_Remitente_Destinatario = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Cargue_Descargue_Tercero: " & ex.Message.ToString()
'            End Try
'        End Function

'        Private Function Es_Representante_Comercial_De_Cliente(Optional ByRef Cliente As String = "") As Boolean
'            Try
'                Dim sdrRepresentanteDeCliente As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Nombre"
'                strSQL += " FROM V_Clientes"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Representante_Comercial = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()

'                sdrRepresentanteDeCliente = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrRepresentanteDeCliente.Read Then
'                    Es_Representante_Comercial_De_Cliente = True
'                    Cliente = sdrRepresentanteDeCliente.Item(0).ToString
'                Else
'                    Es_Representante_Comercial_De_Cliente = False
'                    Cliente = ""
'                End If

'                sdrRepresentanteDeCliente.Close()
'                sdrRepresentanteDeCliente.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Es_Representante_Comercial_De_Cliente: " & ex.Message.ToString()
'                Es_Representante_Comercial_De_Cliente = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Insertar_Detalle_Seguro_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ComandoSQL.CommandText = "sp_trasladar_detalle_seguro_clientes"
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                Me.objGeneral.ComandoSQL.Parameters.Clear()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Val(Me.objEmpresa.Codigo)
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_USUA_Codigo").Value = Me.Usuario.Codigo   ' Variable de Sesión
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_A_Temporal", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_A_Temporal").Value = General.HACIA_DEFINITIVA

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_Detalle_Seguro_SQL = True
'                Else
'                    Insertar_Detalle_Seguro_SQL = False
'                End If

'            Catch ex As Exception
'                Insertar_Detalle_Seguro_SQL = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Insertar_Detalle_Seguro_SQL: " & ex.Message.ToString()
'            End Try
'        End Function

'        Private Function Verificar_Estudio_Seguridad_Por_Perfil(ByVal NumeroIdentificacion As String, ByRef Contador As Integer) As Boolean
'            Try
'                Verificar_Estudio_Seguridad_Por_Perfil = True

'                If Me.isConductor Then
'                    If Not Tiene_Estudio_Seguridad_Por_Perfil(NumeroIdentificacion, PERFIL_CONDUCTOR) Then
'                        Contador += 1
'                        Me.strError += Contador & ". No existe un estudio de seguridad para el perfil de conductor."
'                        Me.strError += " <Br />"
'                        Verificar_Estudio_Seguridad_Por_Perfil = False
'                    End If
'                End If

'                If Me.isPropietario Then
'                    If Not Tiene_Estudio_Seguridad_Por_Perfil(NumeroIdentificacion, PERFIL_PROPIETARIO) Then
'                        Contador += 1
'                        Me.strError += Contador & ". No existe un estudio de seguridad para el perfil de propietario."
'                        Me.strError += " <Br />"
'                        Verificar_Estudio_Seguridad_Por_Perfil = False
'                    End If
'                End If

'                If Me.isTenedor Then
'                    If Not Tiene_Estudio_Seguridad_Por_Perfil(NumeroIdentificacion, PERFIL_TENEDOR) Then
'                        Contador += 1
'                        Me.strError += Contador & ". No existe un estudio de seguridad para el perfil de tenedor."
'                        Me.strError += " <Br />"
'                        Verificar_Estudio_Seguridad_Por_Perfil = False
'                    End If
'                End If

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Verificar_Estudio_Seguridad_Por_Perfil: " & ex.Message.ToString()
'                Verificar_Estudio_Seguridad_Por_Perfil = False
'            End Try
'        End Function

'        Private Function Tiene_Estudio_Seguridad_Por_Perfil(ByVal NumeroIdentificacion As String, ByVal Perfil As Short) As Boolean
'            Try
'                Dim sdrTieneEstudioSeguridad As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT TOP 1 Numero"
'                strSQL += " FROM Estudio_seguridad_Vehiculos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Estado_Estudio = " & ESTADO_ESTUDIO_SEGURIDAD_APROBADO
'                strSQL += " AND Estado = " & General.ESTADO_DEFINITIVO

'                If Perfil = PERFIL_PROPIETARIO Then
'                    strSQL += " AND Identificacion_Propietario = '" & Me.strNumeroIdentificacion & "'"
'                End If

'                If Perfil = PERFIL_TENEDOR Then
'                    strSQL += " AND Identificacion_Tenedor = '" & Me.strNumeroIdentificacion & "'"
'                End If

'                If Perfil = PERFIL_CONDUCTOR Then
'                    strSQL += " AND Identificacion_Conductor = '" & Me.strNumeroIdentificacion & "'"
'                End If

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()

'                sdrTieneEstudioSeguridad = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTieneEstudioSeguridad.Read Then
'                    Me.objEstudioSeguridad.Numero = sdrTieneEstudioSeguridad.Item("Numero")
'                    Tiene_Estudio_Seguridad_Por_Perfil = True
'                Else
'                    Tiene_Estudio_Seguridad_Por_Perfil = False
'                End If

'                sdrTieneEstudioSeguridad.Close()
'                sdrTieneEstudioSeguridad.Dispose()

'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Tiene_Estudio_Seguridad_Por_Perfil: " & ex.Message.ToString()
'                Tiene_Estudio_Seguridad_Por_Perfil = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Insertar_Detalle_Curso_Formacion_Empleados() As Boolean
'            Try

'                Dim lonNumeRegi As Long

'                Me.objGeneral.ComandoSQL.CommandText = "sp_trasladar_detalle_curso_formación_empleados"
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                Me.objGeneral.ComandoSQL.Parameters.Clear()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Usua_Codigo", SqlDbType.VarChar) : Me.objGeneral.ComandoSQL.Parameters("@par_Usua_Codigo").Value = Me.objUsuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_A_Temporal", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_A_Temporal").Value = General.CERO

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_Detalle_Curso_Formacion_Empleados = True
'                Else
'                    Insertar_Detalle_Curso_Formacion_Empleados = False
'                End If

'            Catch ex As Exception
'                Insertar_Detalle_Curso_Formacion_Empleados = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & ex.Message.ToString()
'            End Try
'        End Function

'        Public Function Insertar_Hijo_Empleados() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ComandoSQL.CommandText = "sp_trasladar_detalle_Hijo_empleados"
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure

'                Me.objGeneral.ComandoSQL.Parameters.Clear()
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_empr_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_empr_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_terc_codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_terc_codigo").Value = Me.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_usua_Codigo", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_usua_Codigo").Value = Me.Usuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_a_temporar", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_a_temporar").Value = General.HACIA_DEFINITIVA

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_Hijo_Empleados = True
'                Else
'                    Insertar_Hijo_Empleados = False
'                End If

'            Catch ex As Exception
'                Insertar_Hijo_Empleados = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Tabla_Detalle_Seguro_Temporal: " & ex.Message.ToString()
'            End Try
'        End Function

'        Public Function Insertar_Detalle_Examen_Medico_Empleados() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ComandoSQL.CommandText = "sp_trasladar_detalle_examen_medico_empleados"
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure

'                Me.objGeneral.ComandoSQL.Parameters.Clear()
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_empr_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_empr_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_terc_codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_terc_codigo").Value = Me.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_usua_Codigo", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_usua_Codigo").Value = Me.Usuario.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_A_Temporal", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_A_Temporal").Value = General.HACIA_DEFINITIVA

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_Detalle_Examen_Medico_Empleados = True
'                Else
'                    Insertar_Detalle_Examen_Medico_Empleados = False
'                End If

'            Catch ex As Exception
'                Insertar_Detalle_Examen_Medico_Empleados = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Cargar_Tabla_Detalle_Seguro_Temporal: " & ex.Message.ToString()
'            End Try
'        End Function

'        Private Sub Validar_Rango_Identificacion()
'            Dim arraCampos(2), arrValoresCampos(2) As String

'            arraCampos(0) = "RangoMinimo"
'            arraCampos(1) = "RangoMaximo"
'            arraCampos(2) = "Prefijo"

'            If Me.objGeneral.Retorna_Campos(Me.objEmpresa.Codigo, "V_Rango_Identificacion_Terceros", arraCampos, arrValoresCampos, arraCampos.Count, General.CAMPO_NUMERICO, "Codigo", Me.objCataTipoNaturaleza.Campo1, "CATA_Codigo = 'RAIT'") Then
'                Me.lonRangInicIdenTerc = arrValoresCampos(0)
'                Me.lonRangFinaIdenTerc = arrValoresCampos(1)
'                Me.strTipoIden = arrValoresCampos(2)
'            End If
'        End Sub

'        Public Function Guardar_Direcciones_Terceros(Optional ByVal ID_Direccion As Integer = 0) As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                If Not Me.objGeneral.ConexionSQL.State = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Open()
'                    Me.objGeneral.ComandoSQL = Me.objGeneral.ConexionSQL.CreateCommand()
'                    Me.objGeneral.ComandoSQL.Connection = Me.objGeneral.ConexionSQL()
'                End If


'                Me.objGeneral.ComandoSQL.CommandText = "sp_insertar_direcciones_t_detalle_direccion_terceros"
'                Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                Me.objGeneral.ComandoSQL.Parameters.Clear()

'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_TERC_Codigo").Value = Me.lonCodigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_ID", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_ID").Value = ID_Direccion
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_CIUD_Codigo", SqlDbType.Int) : Me.objGeneral.ComandoSQL.Parameters("@par_CIUD_Codigo").Value = Me.Ciudad.Codigo
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Direccion", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Direccion").Value = Me.Direccion
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Telefono", SqlDbType.VarChar, 50) : Me.objGeneral.ComandoSQL.Parameters("@par_Telefono").Value = Me.Telefono1
'                Me.objGeneral.ComandoSQL.Parameters.Add("@par_Usuario", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Usuario").Value = Me.Usuario.Codigo

'                lonNumeRegi = Val(Me.objGeneral.ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Guardar_Direcciones_Terceros = True
'                Else
'                    Guardar_Direcciones_Terceros = False
'                End If

'            Catch ex As Exception
'                Guardar_Direcciones_Terceros = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Guardar_Direcciones_Temporal: " & ex.Message.ToString()
'            End Try
'        End Function

'        Public Function Limpiar_Temporal_Direcciones(ByRef Empresa As Empresas, ByRef Usuario As Usuario) As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_eliminar_t_detalle_direccion_terceros", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Empresa.Codigo
'                ComandoSQL.Parameters.Add("@par_USUA_Codigo", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Codigo").Value = Usuario.Codigo
'                ComandoSQL.Parameters.Add("@par_TERC_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_TERC_Codigo").Value = lonCodigo

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Limpiar_Temporal_Direcciones = True
'                Else
'                    Limpiar_Temporal_Direcciones = False
'                End If

'            Catch ex As Exception
'                Limpiar_Temporal_Direcciones = False
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Limpiar_Temporal_Direcciones: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar_T_Direccion_Terceros(ByRef Empresa As Empresas, ByVal Usuario As Usuario) As Boolean
'            Try
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo
'                Consultar_T_Direccion_Terceros = True

'                Dim sdrTercero As SqlDataReader
'                Dim ComandoSQL As SqlCommand
'                Dim intCantRegistros As Integer
'                Dim intId As Integer
'                Dim StrSQLCount, StrSQLSelect As String
'                Dim ConexionAux As SqlConnection

'                ConexionAux = Me.objGeneral.ConexionSQL

'                StrSQLCount = "SELECT COUNT(*)"
'                StrSQLSelect = "SELECT EMPR_codigo, TERC_Codigo, ID, CIUD_Codigo, Direccion, Telefono"

'                strSQL = " FROM T_Detalle_Direccion_Terceros"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND TERC_Codigo = " & Me.lonCodigo

'                Me.objGeneral.Retorna_COUNT_SQL(StrSQLCount & strSQL, intCantRegistros)

'                ConexionAux.Open()
'                ComandoSQL = New SqlCommand(StrSQLSelect & strSQL, ConexionAux)
'                sdrTercero = ComandoSQL.ExecuteReader()

'                For Cant As Integer = 1 To intCantRegistros
'                    If sdrTercero.Read() Then
'                        Me.lonCodigo = Val(sdrTercero("TERC_Codigo"))
'                        intId = Val(sdrTercero("ID"))
'                        Me.Ciudad.Codigo = Val(sdrTercero("CIUD_Codigo"))
'                        Me.Direccion = sdrTercero("Direccion").ToString
'                        Me.Telefono1 = sdrTercero("Telefono").ToString
'                        Me.Usuario.Codigo = Usuario.Codigo
'                        If Not Guardar_Direcciones_Terceros(intId) Then
'                            Exit For
'                        End If
'                    Else
'                        Me.lonCodigo = 0
'                        Consultar_T_Direccion_Terceros = False
'                    End If
'                Next
'                sdrTercero.Close()
'                sdrTercero.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & ex.Message.ToString()
'                Consultar_T_Direccion_Terceros = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'#End Region
'    End Class
'End Namespace

