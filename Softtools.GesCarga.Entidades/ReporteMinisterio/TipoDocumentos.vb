﻿'Imports System.Data
'Imports System.Data.SqlClient

'Namespace Entidades.ReporteMinisterio

'    Public Class TipoDocumentos
'#Region "Declaracion Atributos"

'        Dim strSQL As String

'        Public strError As String
'        Public bolModificar As Boolean

'        Private objEmpresa As Empresas
'        Private lonCodigo As Long
'        Private strNombre As String
'        Private lonConsecutivo As Long
'        Private lonConsecutivoHasta As Long

'        Private intControlaCosecutivoHasta As Integer
'        Private strNotaInicio As String
'        Private strNotaFin As String
'        Private strNombreTabla As String
'        Private intNumeroDocumentosFaltantesAviso As Integer

'        Private intTipoImpresion As Integer
'        Private objCataTipoGeneracion As Catalogo
'        Private objGeneral As General

'#End Region

'#Region "Constantes"

'        'TIPOS DISTRBUCION URBANA
'        Public Const TIDO_CITA_RECOGIDA As Byte = 60
'        Public Const TIDO_PLANILLA_URBANA As Byte = 61
'        Public Const TIDO_GUIA As Byte = 90
'        Public Const TIDO_LIQUIDACION_PLANILLA_URBANA As Byte = 77
'        Public Const TIDO_PLANILLA_RECOGIDA As Byte = 65
'        Public Const TIDO_GUIA_RECOGIDA As Byte = 95

'        ' TIPOS DOCUMENTOS DESPACHOS
'        Public Const TIDO_CONTRATO_TRANSPORTE As Byte = 10
'        Public Const TIDO_ORDEN_CARGUE As Byte = 11
'        Public Const TIDO_MANIFIESTO As Byte = 12
'        Public Const TIDO_CUMPLIDO As Byte = 13
'        Public Const TIDO_ESTUDIO_SEGURIDAD As Byte = 14
'        Public Const TIDO_ESTUDIO_SEGURIDAD_AUTORIZACION As Byte = 140
'        Public Const TIDO_LIQUIDA_MANIFIESTO_TERCEROS As Byte = 15
'        Public Const TIDO_LIQUIDA_REMESAS_PROVEEDOR As Byte = 230
'        Public Const TIDO_GASTO_CONDUCTORES As Byte = 16
'        Public Const TIDO_MANIFIESTO_OTRA_EMPRESAS As Byte = 17
'        Public Const TIDO_LIQUIDA_CONDUCTORES As Byte = 18
'        Public Const TIDO_RENTABILIDAD_VEHICULOS As Byte = 19
'        Public Const TIDO_FACTURAS As Byte = 30
'        Public Const TIDO_NOTA_FACTURAS As Byte = 31
'        Public Const TIDO_CONTRA_CON_TRANSP As Byte = 32
'        Public Const TIDO_REMESAS As Byte = 33
'        Public Const TIDO_FACTURA_OTRO_CONCEPTOS As Byte = 34

'        Public Const TIDO_LIQUIDA_MANIFIESTO_OTRAS_EMPRESAS As Byte = 29
'        Public Const TIDO_COTIZACION_SERVICIO_TRANSPORTE As Byte = 36
'        Public Const TIDO_TARIFARIO_SERVICIO_TRANSPORTE As Byte = 37
'        Public Const TIDO_ORDEN_SERVICIO_MASIVO As Byte = 38
'        Public Const TIDO_LIQUIDA_REMESA_URBANAS As Byte = 39
'        Public Const TIDO_ALQUILER_EQUIPOS As Byte = 40
'        Public Const TIDO_CUENTAS_COBRO As Byte = 55
'        Public Const TIDO_CUMPLIDO_REMESA_TERCEROS As Byte = 62
'        Public Const TIDO_LIQUIDA_REMESA_TERCEROS As Byte = 70
'        Public Const TIDO_VALE_COMBUSTIBLE As Byte = 72
'        Public Const TIDO_LIQUIDA_MASIVA_REMESA_URBANAS As Byte = 75
'        Public Const TIDO_VALE_CONSUMO As Byte = 80
'        Public Const TIDO_TRAZABILIDAD_ORDEN_SERVICIOS As Byte = 41
'        Public Const TIDO_CIERRE_OPERATIVO As Byte = 210
'        Public Const TIDO_REMESA_PROVEEDOR As Byte = 220
'        ' TIPOS DOCUMENTOS SEGURIDAD VEHÍCULOS
'        Public Const TIDO_SEGUIMIENTO_VEHICULOS As Byte = 35
'        Public Const TIDO_REGISTRO_VEHICULO_REPORTADO As Byte = 170

'        ' TIPOS DOCUMENTOS TESORERIA
'        Public Const TIDO_CUENTA_PAGAR As Byte = 20
'        Public Const TIDO_CUENTA_COBRAR As Byte = 21
'        Public Const TIDO_COMPROBANTE_EGRESO As Byte = 22
'        Public Const TIDO_COMPROBANTE_INGRESO As Byte = 23
'        Public Const TIDO_NOTA_DEBITO As Byte = 24
'        Public Const TIDO_NOTA_CREDITO As Byte = 25
'        Public Const TIDO_CONCEPTOS_CONTABLES As Byte = 26
'        Public Const TIDO_COMPROBANTE_CAUSACION As Byte = 27
'        Public Const TIDO_CONSIGNACION As Byte = 28

'        ' TIPOS DE DOCUMENTOS MANTENIMIENTO
'        Public Const TIDO_ASIGNACION_LLANTAS As Byte = 50
'        Public Const TIDO_TAREA_MANTENIMIENTO As Byte = 51
'        Public Const TIDO_ORDEN_SERVICIO_SEMIMASIVO As Byte = 52
'        Public Const TIDO_REGISTRO_CONSUMO_COMBUSTIBLE As Byte = 53
'        Public Const TIDO_INSPECCION_VEHICULO As Byte = 54

'        ' TIPOS DOCUMENTOS INVENTARIO
'        Public Const TIDO_ORDEN_COMPRA As Byte = 1
'        Public Const TIDO_ENTRADA_ALMACEN As Byte = 2
'        Public Const TIDO_ENTRADA_ALMACEN_TRASLADO As Byte = 2
'        Public Const TIDO_SALIDA_ALMACEN As Byte = 3
'        Public Const TIDO_REINTEGRO_ALMACEN As Byte = 4
'        Public Const TIDO_DEVOLUCION_ALMACEN As Byte = 5
'        Public Const TIDO_AJUSTE_ALMACEN As Byte = 6
'        Public Const TIDO_CONSIGNACION_ALMACEN As Byte = 7

'        ' OTROS TIPOS DOCUMENTOS
'        Public Const TIDO_TERCERO As Short = 100
'        Public Const TIDO_COMPROBANTE_CONTABLE As Byte = 200

'        ' TIPOS DOCUMENTOS CONTABILIDAD
'        Public Const TIDO_CERTIFICADO_RETENCION_FUENTE As Byte = 150
'        Public Const TIDO_CERTIFICADO_RETENCION_ICA As Byte = 160

'        'Tipo de Generacion Numeracion
'        Public Const TIPO_GENERACION_NUMERACION_EMPRESA As Byte = 1
'        Public Const TIPO_GENERACION_NUMERACION_OFICINA As Byte = 2

'        'Tipo documento remesas
'        Public Const REMESA_NACIONAL As Short = 80
'        Public Const REMESA_URBANA As Short = 81
'        Public Const REMESA_PAQUETEO As Short = 82 'Guía
'        Public Const REMESA_PAQUETERIA As Short = 84 'Remesa que se crea automáticamente al momento de crear una manifiesto de paquetería
'        Public Const REMESA_GENSET As Short = 89

'        ' Documento
'        Public Const SOLO_ENCABEZADO As Boolean = True
'        Public Const DOCUMENTO_COMPLETO As Boolean = False
'        Public Const CONTROL_DOCUMENTO_DUPLICADO As Short = 1

'        Public Const TIDO_RELACION_GASTOS_CONDUCTORES As Short = 240
'        Public Const TIDO_LISTA_CHEQUEO_CARGUE As Short = 245
'        Public Const TIDO_LISTA_CHEQUEO_DESCARGUE As Short = 250

'#End Region

'#Region "Constructores"

'        Sub New()
'            Me.objEmpresa = New Empresas(0)
'            Me.objGeneral = New General
'            Me.lonCodigo = 0
'            Me.strNombre = ""
'            Me.lonConsecutivo = 0

'            Me.lonConsecutivoHasta = 0
'            Me.intControlaCosecutivoHasta = 0
'            Me.strNotaInicio = ""
'            Me.strNotaFin = ""
'            Me.strNombreTabla = ""

'            Me.intNumeroDocumentosFaltantesAviso = 0
'            Me.objCataTipoGeneracion = New Catalogo(Me.objEmpresa, "TIGN")
'            Me.intTipoImpresion = 0
'        End Sub

'        Sub New(ByRef Empresa As Empresas)

'            Me.objEmpresa = Empresa
'            Me.objGeneral = New General
'            Me.lonCodigo = 0
'            Me.strNombre = ""
'            Me.lonConsecutivo = 0

'            Me.lonConsecutivoHasta = 0
'            Me.intControlaCosecutivoHasta = 0
'            Me.strNotaInicio = ""
'            Me.strNotaFin = ""
'            Me.strNombreTabla = ""

'            Me.intNumeroDocumentosFaltantesAviso = 0
'            Me.objCataTipoGeneracion = New Catalogo(Me.objEmpresa, "TIGN")
'            Me.intTipoImpresion = 0

'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property Empresa() As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(ByVal value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property

'        Public Property Codigo() As Long
'            Get
'                Return Me.lonCodigo
'            End Get
'            Set(ByVal value As Long)
'                Me.lonCodigo = value
'            End Set
'        End Property

'        Public Property Consecutivo() As Long
'            Get
'                Return Me.lonConsecutivo
'            End Get
'            Set(ByVal value As Long)
'                Me.lonConsecutivo = value
'            End Set
'        End Property

'        Public Property ConsecutivoHasta() As Long
'            Get
'                Return Me.lonConsecutivoHasta
'            End Get
'            Set(ByVal value As Long)
'                Me.lonConsecutivoHasta = value
'            End Set
'        End Property

'        Public Property ControlaConsecutivoHasta() As Long
'            Get
'                Return Me.intControlaCosecutivoHasta
'            End Get
'            Set(ByVal value As Long)
'                Me.intControlaCosecutivoHasta = value
'            End Set
'        End Property

'        Public Property Nombre() As String
'            Get
'                Return Me.strNombre
'            End Get
'            Set(ByVal value As String)
'                Me.strNombre = value
'            End Set
'        End Property

'        Public Property NotasFin() As String
'            Get
'                Return Me.strNotaFin
'            End Get
'            Set(ByVal value As String)
'                Me.strNotaFin = value
'            End Set
'        End Property

'        Public Property NotasInicio() As String
'            Get
'                Return Me.strNotaInicio
'            End Get
'            Set(ByVal value As String)
'                Me.strNotaInicio = value
'            End Set
'        End Property

'        Public Property NombreTabla() As String
'            Get
'                Return Me.strNombreTabla
'            End Get
'            Set(ByVal value As String)
'                Me.strNombreTabla = value
'            End Set
'        End Property

'        Public Property CataTipoGeneracion() As Catalogo
'            Get
'                Return Me.objCataTipoGeneracion
'            End Get
'            Set(ByVal value As Catalogo)
'                Me.objCataTipoGeneracion = value
'            End Set
'        End Property

'        Public Property NumeroDocumentosFaltantesAviso() As Integer
'            Get
'                Return Me.intNumeroDocumentosFaltantesAviso
'            End Get
'            Set(ByVal value As Integer)
'                Me.intNumeroDocumentosFaltantesAviso = value
'            End Set
'        End Property

'        Public Property TipoImpresion() As Integer
'            Get
'                Return Me.intTipoImpresion
'            End Get
'            Set(ByVal value As Integer)
'                Me.intTipoImpresion = value
'            End Set
'        End Property

'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean
'            Try

'                Dim sdrTipoDocumento As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre, Consecutivo, Consecutivo_Hasta, Controla_Consecutivo_Hasta,"
'                strSQL += " Notas_Fin, Notas_Inicio, Nombre_Tabla, TIGN_Codigo, Numero_Documentos_Faltantes_Aviso,"
'                strSQL += "Tipo_Impresion FROM Tipo_Documentos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrTipoDocumento = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTipoDocumento.Read Then
'                    Me.strNombre = sdrTipoDocumento("Nombre").ToString()
'                    Me.lonConsecutivo = Val(sdrTipoDocumento("Consecutivo").ToString())
'                    Me.lonConsecutivoHasta = Val(sdrTipoDocumento("Consecutivo_Hasta").ToString())
'                    Me.intControlaCosecutivoHasta = Val(sdrTipoDocumento("Controla_Consecutivo_Hasta").ToString())
'                    Me.strNotaFin = sdrTipoDocumento("Notas_Fin").ToString()

'                    Me.strNotaInicio = sdrTipoDocumento("Notas_Inicio").ToString()
'                    Me.strNombreTabla = sdrTipoDocumento("Nombre_Tabla").ToString()
'                    Me.objCataTipoGeneracion.Campo1 = sdrTipoDocumento("TIGN_Codigo").ToString()
'                    Me.intNumeroDocumentosFaltantesAviso = Val(sdrTipoDocumento("Numero_Documentos_Faltantes_Aviso").ToString())
'                    Me.intTipoImpresion = Val(sdrTipoDocumento("Tipo_Impresion").ToString())

'                    Consultar = True
'                Else
'                    Consultar = False
'                End If

'                sdrTipoDocumento.Close()
'                sdrTipoDocumento.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Codigo As Long) As Boolean

'            Try
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Dim sdrTipoDocumento As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT Nombre, Consecutivo, Consecutivo_Hasta, Controla_Consecutivo_Hasta,"
'                strSQL += " Notas_Fin, Notas_Inicio, Nombre_Tabla, TIGN_Codigo, Numero_Documentos_Faltantes_Aviso,"
'                strSQL += "Tipo_Impresion FROM Tipo_Documentos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrTipoDocumento = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTipoDocumento.Read Then
'                    Me.strNombre = sdrTipoDocumento("Nombre").ToString()
'                    Me.lonConsecutivo = Val(sdrTipoDocumento("Consecutivo").ToString())
'                    Me.lonConsecutivoHasta = Val(sdrTipoDocumento("Consecutivo_Hasta").ToString())
'                    Me.intControlaCosecutivoHasta = Val(sdrTipoDocumento("Controla_Consecutivo_Hasta").ToString())
'                    Me.strNotaFin = sdrTipoDocumento("Notas_Fin").ToString()

'                    Me.strNotaInicio = sdrTipoDocumento("Notas_Inicio").ToString()
'                    Me.strNombreTabla = sdrTipoDocumento("Nombre_Tabla").ToString()
'                    Me.objCataTipoGeneracion.Campo1 = sdrTipoDocumento("TIGN_Codigo").ToString()
'                    Me.intNumeroDocumentosFaltantesAviso = Val(sdrTipoDocumento("Numero_Documentos_Faltantes_Aviso").ToString())
'                    Me.intTipoImpresion = Val(sdrTipoDocumento("Tipo_Impresion").ToString())

'                    Consecutivo = True
'                Else
'                    Consultar = False
'                End If

'                sdrTipoDocumento.Close()
'                sdrTipoDocumento.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la página " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Consultar = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'            Return Consultar
'        End Function

'        Public Function Existe_Registro(ByRef Empresa As Empresas, ByVal Codigo As Long) As Boolean

'            Try
'                Me.objEmpresa = Empresa
'                Me.lonCodigo = Codigo

'                Dim sdrTipoDocumento As SqlDataReader

'                Dim ComandoSQL As SqlCommand

'                strSQL = "SELECT EMPR_Codigo, Nombre, TIGN_Codigo FROM Tipo_Documentos"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Codigo = " & Me.lonCodigo

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)


'                Me.objGeneral.ConexionSQL.Open()
'                sdrTipoDocumento = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrTipoDocumento.Read Then
'                    Me.strNombre = sdrTipoDocumento("Nombre").ToString()
'                    Me.objCataTipoGeneracion.Campo1 = Val(sdrTipoDocumento("TIGN_Codigo").ToString())
'                    Existe_Registro = True
'                Else
'                    Existe_Registro = False
'                End If

'                sdrTipoDocumento.Close()
'                sdrTipoDocumento.Dispose()
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Me.lonCodigo = 0
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Existe_Registro: " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try
'                Existe_Registro = False
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Guardar(Optional ByRef Mensaje As String = "") As Boolean
'            If Datos_Requeridos(Mensaje) Then
'                If bolModificar Then
'                    Guardar = Modificar_SQL()
'                End If
'            End If
'            Mensaje = Me.strError
'            Return Guardar
'        End Function

'#End Region

'#Region "Funciones Privadas"

'        Private Function Modificar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_tipo_documentos", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Codigo").Value = Me.lonCodigo
'                ComandoSQL.Parameters.Add("@par_Nombre", SqlDbType.VarChar, 35) : ComandoSQL.Parameters("@par_Nombre").Value = Me.strNombre
'                ComandoSQL.Parameters.Add("@par_TIGN_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_TIGN_Codigo").Value = Val(Me.objCataTipoGeneracion.Campo1)
'                ComandoSQL.Parameters.Add("@par_Consecutivo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Consecutivo").Value = Me.lonConsecutivo
'                ComandoSQL.Parameters.Add("@par_Consecutivo_Hasta", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_Consecutivo_Hasta").Value = Me.lonConsecutivoHasta
'                ComandoSQL.Parameters.Add("@par_Controla_Consecutivo_Hasta", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Controla_Consecutivo_Hasta").Value = Me.intControlaCosecutivoHasta
'                ComandoSQL.Parameters.Add("@par_Numero_Documentos_Faltantes_Aviso", SqlDbType.Int) : ComandoSQL.Parameters("@par_Numero_Documentos_Faltantes_Aviso").Value = Me.intNumeroDocumentosFaltantesAviso
'                ComandoSQL.Parameters.Add("@par_Notas_Inicio", SqlDbType.VarChar, 250) : ComandoSQL.Parameters("@par_Notas_Inicio").Value = Me.strNotaInicio
'                ComandoSQL.Parameters.Add("@par_Notas_Fin", SqlDbType.VarChar, 250) : ComandoSQL.Parameters("@par_Notas_Fin").Value = Me.strNotaFin

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Modificar_SQL = False
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Modificar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try

'        End Function

'        Public Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean

'            Try
'                Dim intCont As Short

'                Me.strError = ""
'                intCont = 0
'                Datos_Requeridos = True

'                If Len(Trim(Me.lonCodigo)) = 0 Then
'                    intCont += 1
'                    Me.strError = intCont & ". Debe ingresar el código del Tipo de Documento"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Len(Trim(Me.strNombre)) = 0 Then
'                    intCont += 1
'                    Me.strError = intCont & ". Debe ingresar el nombre del Tipo de Documento"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.strNombre <> "" And Len(Me.strNombre) < General.MINIMO_CUATRO_CARACTERES_INGRESO_NOMBRE Then
'                    intCont += 1
'                    strError += intCont & ". El nombre del Tipo de Documento no puede tener menos de cuatro (4) carácteres"
'                    strError += " <Br />"
'                    Datos_Requeridos = False
'                End If


'                If Me.lonConsecutivo <= 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar un consecutivo mayor a cero"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.lonConsecutivoHasta <= 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar un consecutivo máximo mayor a cero"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.intNumeroDocumentosFaltantesAviso <= 0 Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar los consecutivo faltantes"
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'            Catch ex As Exception
'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Datos_Requeridos: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Datos_Requeridos = False
'            End Try

'        End Function

'#End Region
'    End Class
'End Namespace

