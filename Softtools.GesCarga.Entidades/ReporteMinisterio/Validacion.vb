﻿'Imports System.Data
'Imports System.Data.SqlClient
'Imports System
'Imports Microsoft.VisualBasic

'Namespace Entidades.ReporteMinisterio
'    Public Class Validacion

'#Region "Declaracion Variables"

'        Public strError As String
'        Private strSQL As String
'        Private bolModificar As Boolean

'        Private objGeneral As General
'        Private objEmpresa As Empresas
'        Private strFuncionalidad As String
'        Private strValidacion As String
'        Private strMensajeValidacion As String
'        Private strDescripcion As String
'        Private intEstadoValidacion As Integer
'        Private strCodigoUsuario As String
'        Private bolValida As Boolean

'        Public bolConsultarRegistro As Boolean

'#End Region

'#Region "Constructor"

'        Sub New()
'            Me.objEmpresa = New Empresas(0)
'            Me.objGeneral = New General()
'            Me.strValidacion = ""
'            Me.strMensajeValidacion = ""
'            Me.strDescripcion = ""
'            Me.strFuncionalidad = ""
'            Me.intEstadoValidacion = 0
'            Me.strCodigoUsuario = ""
'        End Sub

'        Sub New(ByRef Empresa As Empresas)
'            Me.objEmpresa = New Empresas(0)
'            Me.objGeneral = New General()
'            Me.strValidacion = ""
'            Me.strMensajeValidacion = ""
'            Me.strDescripcion = ""
'            Me.strFuncionalidad = ""
'            Me.intEstadoValidacion = 0
'            Me.strCodigoUsuario = ""
'        End Sub

'#End Region

'#Region "Propiedades"

'        Public Property Empresa() As Empresas
'            Get
'                Return Me.objEmpresa
'            End Get
'            Set(ByVal value As Empresas)
'                Me.objEmpresa = value
'            End Set
'        End Property

'        Public Property General() As General
'            Get
'                Return Me.objGeneral
'            End Get
'            Set(ByVal value As General)
'                Me.objGeneral = value
'            End Set
'        End Property

'        Public Property Validacion() As String
'            Get
'                Return Me.strValidacion
'            End Get
'            Set(ByVal value As String)
'                Me.strValidacion = value
'            End Set
'        End Property

'        Public Property MensajeValidacion() As String
'            Get
'                Return Me.strMensajeValidacion
'            End Get
'            Set(ByVal value As String)
'                Me.strMensajeValidacion = value
'            End Set
'        End Property

'        Public Property Descripcion() As String
'            Get
'                Return Me.strDescripcion
'            End Get
'            Set(ByVal value As String)
'                Me.strDescripcion = value
'            End Set
'        End Property

'        Public Property Funcionalidad() As String
'            Get
'                Return Me.strFuncionalidad
'            End Get
'            Set(ByVal value As String)
'                Me.strFuncionalidad = value
'            End Set
'        End Property

'        Public Property EstadoValidacion As Integer
'            Get
'                Return Me.intEstadoValidacion
'            End Get
'            Set(value As Integer)
'                Me.intEstadoValidacion = value
'            End Set
'        End Property

'        Public Property Modificar() As Boolean
'            Get
'                Return Me.bolModificar
'            End Get
'            Set(ByVal value As Boolean)
'                Me.bolModificar = value
'            End Set
'        End Property

'        Public Property CodigoUsuario As String
'            Get
'                Return Me.strCodigoUsuario
'            End Get
'            Set(value As String)
'                Me.strCodigoUsuario = value
'            End Set
'        End Property

'        'Propiedad ya no utilizable, requiere actualización de las demas funcionalidades que la referencien
'        ' Es necesario mantenerla hasta que se quite todos los sitios donde se referencia
'        Public Property Valida As Boolean
'            Get
'                Return Me.bolValida
'            End Get
'            Set(value As Boolean)
'                Me.bolValida = value
'            End Set
'        End Property

'        Public Function Eliminar(Optional ByRef Mensaje As String = "") As Boolean
'            Try
'                Eliminar = Borrar_SQL()
'                Mensaje = Me.strError
'            Catch ex As Exception
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Eliminar: " & ex.Message.ToString()
'                Eliminar = False
'            End Try
'        End Function

'#End Region

'#Region "Funciones Publicas"

'        Public Function Consultar() As Boolean
'            Try
'                Dim sdrValidacion As SqlDataReader
'                Dim ComandoSQL As SqlCommand


'                strSQL = "SELECT EMPR_Codigo, Estado_Validacion, Mensaje_Validacion, Descripcion, Funcionalidad FROM Validaciones"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Validacion = '" & Me.strValidacion & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrValidacion = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrValidacion.Read() Then

'                    If Val(sdrValidacion("Estado_Validacion").ToString()) = General.ESTADO_ACTIVO Then
'                        Consultar = True
'                    Else
'                        Consultar = False
'                    End If

'                    Me.intEstadoValidacion = Val(sdrValidacion("Estado_Validacion").ToString())
'                    Me.strMensajeValidacion = sdrValidacion("Mensaje_Validacion").ToString()
'                    Me.strDescripcion = sdrValidacion("Descripcion").ToString()
'                    Me.strFuncionalidad = sdrValidacion("Funcionalidad").ToString()
'                    bolConsultarRegistro = True
'                Else
'                    bolConsultarRegistro = False
'                    Consultar = False
'                End If
'                sdrValidacion.Close()
'                sdrValidacion.Dispose()
'                ComandoSQL.Dispose()

'                Return Consultar
'            Catch ex As Exception
'                Consultar = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name
'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Consultar: " & ex.Message.ToString()
'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Consultar(ByRef Empresa As Empresas, ByVal Validacion As String) As Boolean
'            Try
'                Dim sdrValidacion As SqlDataReader
'                Dim ComandoSQL As SqlCommand

'                Me.objEmpresa = Empresa
'                Me.strValidacion = Validacion

'                strSQL = "SELECT EMPR_Codigo, Estado_Validacion, Mensaje_Validacion, Descripcion, Funcionalidad FROM Validaciones"
'                strSQL += " WHERE EMPR_Codigo = " & Me.objEmpresa.Codigo
'                strSQL += " AND Validacion = '" & Me.strValidacion & "'"

'                ComandoSQL = New SqlCommand(strSQL, Me.objGeneral.ConexionSQL)

'                Me.objGeneral.ConexionSQL.Open()
'                sdrValidacion = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

'                If sdrValidacion.Read() Then

'                    If Val(sdrValidacion("Estado_Validacion").ToString()) = General.ESTADO_ACTIVO Then
'                        Consultar = True
'                        Me.Valida = True
'                    Else
'                        Me.Valida = False
'                        Consultar = False
'                    End If

'                    Me.intEstadoValidacion = Val(sdrValidacion("Estado_Validacion").ToString())
'                    Me.strMensajeValidacion = sdrValidacion("Mensaje_Validacion").ToString()
'                    Me.strDescripcion = sdrValidacion("Descripcion").ToString()
'                    Me.strFuncionalidad = sdrValidacion("Funcionalidad").ToString()

'                Else
'                    Consultar = False
'                    Me.Valida = False
'                End If
'                sdrValidacion.Close()
'                sdrValidacion.Dispose()
'                ComandoSQL.Dispose()

'                Return Consultar
'            Catch ex As Exception
'                Consultar = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Public Function Guardar(Optional ByRef Mensaje As String = "") As Boolean
'            Guardar = False
'            If Datos_Requeridos(Mensaje) Then
'                If Not bolModificar Then
'                    Guardar = Insertar_SQL()
'                Else
'                    Guardar = Modificar_SQL()
'                End If
'            End If
'            Mensaje = Me.strError
'        End Function
'#End Region
'#Region "Funciones Privadas"

'        Private Function Insertar_SQL() As Boolean

'            Try

'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()

'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_validaciones", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Validacion", SqlDbType.VarChar, 30) : ComandoSQL.Parameters("@par_Validacion").Value = Me.strValidacion
'                ComandoSQL.Parameters.Add("@par_Mensaje_Validacion", SqlDbType.VarChar, 250) : ComandoSQL.Parameters("@par_Mensaje_Validacion").Value = Me.strMensajeValidacion
'                ComandoSQL.Parameters.Add("@par_Descripcion", SqlDbType.VarChar, 250) : ComandoSQL.Parameters("@par_Descripcion").Value = Me.strDescripcion
'                ComandoSQL.Parameters.Add("@par_Funcionalidad", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Funcionalidad").Value = Me.strFuncionalidad
'                ComandoSQL.Parameters.Add("@par_Estado_Validacion", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Estado_Validacion").Value = Me.intEstadoValidacion

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Insertar_SQL = True
'                Else
'                    Insertar_SQL = False
'                End If
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Insertar_SQL = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Private Function Modificar_SQL() As Boolean

'            Try

'                Dim lonNumeRegi As Long
'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_modificar_validaciones", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure

'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Decimal) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Validacion", SqlDbType.VarChar, 30) : ComandoSQL.Parameters("@par_Validacion").Value = Me.strValidacion
'                ComandoSQL.Parameters.Add("@par_Mensaje_Validacion", SqlDbType.VarChar, 250) : ComandoSQL.Parameters("@par_Mensaje_Validacion").Value = Me.strMensajeValidacion
'                ComandoSQL.Parameters.Add("@par_Descripcion", SqlDbType.VarChar, 250) : ComandoSQL.Parameters("@par_Descripcion").Value = Me.strDescripcion
'                ComandoSQL.Parameters.Add("@par_Funcionalidad", SqlDbType.VarChar, 100) : ComandoSQL.Parameters("@par_Funcionalidad").Value = Me.strFuncionalidad
'                ComandoSQL.Parameters.Add("@par_Estado_Validacion", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_Estado_Validacion").Value = Me.intEstadoValidacion

'                ComandoSQL.Parameters.Add("@par_USUA_Modifica", SqlDbType.Char, 10) : ComandoSQL.Parameters("@par_USUA_Modifica").Value = Me.strCodigoUsuario

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Modificar_SQL = True
'                Else
'                    Modificar_SQL = False
'                End If
'                ComandoSQL.Dispose()

'            Catch ex As Exception
'                Modificar_SQL = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            Finally
'                Me.objGeneral.ConexionSQL.Close()
'            End Try
'        End Function

'        Private Function Borrar_SQL() As Boolean
'            Try
'                Dim lonNumeRegi As Long

'                Me.objGeneral.ConexionSQL.Open()
'                Dim ComandoSQL As SqlCommand = New SqlCommand("sp_borrar_validaciones", Me.objGeneral.ConexionSQL)

'                ComandoSQL.CommandType = CommandType.StoredProcedure
'                ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.objEmpresa.Codigo
'                ComandoSQL.Parameters.Add("@par_Validacion", SqlDbType.VarChar, 50) : ComandoSQL.Parameters("@par_Validacion").Value = Me.strValidacion

'                lonNumeRegi = Val(ComandoSQL.ExecuteNonQuery().ToString())

'                If lonNumeRegi > 0 Then
'                    Borrar_SQL = True
'                Else
'                    Borrar_SQL = False
'                End If
'                ComandoSQL.Dispose()

'            Catch ex As Exception

'                Me.objGeneral.ExcepcionSQL = ex
'                Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función Borrar_SQL: " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)

'                Borrar_SQL = False

'            Finally
'                If Me.objGeneral.ConexionSQL.State() = ConnectionState.Open Then
'                    Me.objGeneral.ConexionSQL.Close()
'                End If
'            End Try
'        End Function

'        Private Function Datos_Requeridos(Optional ByRef Mensaje As String = "") As Boolean

'            Try

'                Me.strError = ""
'                Dim intCont As Short
'                intCont = 0
'                Datos_Requeridos = True

'                If Me.strMensajeValidacion = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe Digitar el mensaje de la validación."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'                If Me.strDescripcion = "" Then
'                    intCont += 1
'                    Me.strError += intCont & ". Debe ingresar la descripción de la validación."
'                    Me.strError += " <Br />"
'                    Datos_Requeridos = False
'                End If

'            Catch ex As Exception
'                Datos_Requeridos = False
'                Dim SeguimientoPila As New System.Diagnostics.StackTrace()
'                Dim NombreFuncion As String = SeguimientoPila.GetFrame(General.CERO).GetMethod().Name

'                Try
'                    Me.objGeneral.ExcepcionSQL = ex
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(Me.objGeneral.ExcepcionSQL.Number)
'                Catch Exc As Exception
'                    Me.strError = "En la clase " & Me.ToString & " se presentó un error en la función " & NombreFuncion & ": " & Me.objGeneral.Traducir_Error(ex.Message)
'                End Try

'            End Try

'        End Function
'#End Region

'        'Método ya no utilizable, requiere actualización de las demas funcionalidades que lo referencien
'        Function CataPais() As Object
'            Throw New NotImplementedException
'        End Function

'    End Class
'End Namespace