﻿'Imports Microsoft.VisualBasic
'Imports System.Xml
'Imports System.Data.SqlClient
'Imports System.Data
'Namespace Entidades.ReporteMinisterio

'    Public Class VehiculoRNDC

'#Region "vehiculos"
'        Public NUMNITEMPRESATRANSPORTE As String
'        Public NUMPLACA As String
'        Public CODCONFIGURACIONUNIDADCARGA As String
'        Public CODMARCAVEHICULOCARGA As String
'        Public CODLINEAVEHICULOCARGA As String
'        Public NUMEJES As String
'        Public ANOFABRICACIONVEHICULOCARGA As String
'        Public ANOREPOTENCIACION As String
'        Public CODCOLORVEHICULOCARGA As String
'        Public PESOVEHICULOVACIO As String
'        Public CAPACIDADUNIDADCARGA As String
'        Public UNIDADMEDIDACAPACIDAD As String
'        Public CODTIPOCARROCERIA As String
'        Public NUMCHASIS As String
'        Public CODTIPOCOMBUSTIBLE As String
'        Public NUMSEGUROSOAT As String
'        Public FECHAVENCIMIENTOSOAT As String
'        Public NUMNITASEGURADORASOAT As String
'        Public CODTIPOIDPROPIETARIO As String
'        Public NUMIDPROPIETARIO As String
'        Public CODTIPOIDTENEDOR As String
'        Public NUMIDTENEDOR As String


'        Dim objGeneral As General
'        Dim solicitud As Solicitud
'        Dim acceso As Acceso
'        Dim wsManifiestoElectronico As ManifiestoElectronico.IBPMServicesservice
'        Dim objInterfaz As InterfazMinisterioTransporte

'        Const NOMBRE_TAG As String = "variables"
'        Public Const CABEZOTE_DOS_EJES As Integer = 53
'        Public Const CABEZOTE_TRES_EJES As Integer = 54
'        Public Const PESO_MAXIMO_VEHICULO_SIN_REMOLQUE As Integer = 37

'#End Region

'#Region "Constantes"
'        Public Const NOMBRE_TAG_CONSULTA As String = "documento"
'        Public Const ERROR_VEHICULO_DUPLICADO As String = "VEH015: La Placa que esta registrando ya existe con todos los datos iguales. No debe volver a registrarla"
'#End Region

'#Region "Propiedades"
'        Public Property EnlaceMinisterio As String
'            Get
'                Return Me.wsManifiestoElectronico.Url
'            End Get
'            Set(value As String)
'                Me.wsManifiestoElectronico.Url = value
'            End Set
'        End Property
'#End Region

'#Region "Enumeradores"
'        Public Enum enumTipoVehiculo As Integer
'            REMOLQUE = 0
'            VEHICULO = 1
'        End Enum
'#End Region

'#Region "Constructor"
'        Public Sub New(ByVal acceso As Acceso, ByVal solicitud As Solicitud)
'            Me.objGeneral = New General
'            Me.solicitud = New Solicitud
'            Me.acceso = New Acceso
'            Me.wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
'            Me.solicitud = solicitud
'            Me.acceso = acceso
'            Me.objInterfaz = New InterfazMinisterioTransporte(General.UNO)
'        End Sub

'        Sub New()
'            Me.solicitud = New Solicitud
'            Me.acceso = New Acceso
'            objGeneral = New General
'            wsManifiestoElectronico = New ManifiestoElectronico.IBPMServicesservice
'            Me.objInterfaz = New InterfazMinisterioTransporte(General.UNO)
'        End Sub
'#End Region

'#Region "Funciones Privadas"

'#End Region

'#Region "Funciones Publicas"
'        Public Sub Crear_XML_Vehiculo(ByRef XmlDocumento As XmlDocument, ByVal TipoFormato As Tipos_Procesos, ByVal lstVariables As List(Of String))
'            Try
'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)
'                Select Case TipoFormato
'                    Case Tipos_Procesos.FORMATO_INGRESO
'                        Dim txtNodo As XmlText

'                        xmlElementoRoot = XmlDocumento.DocumentElement

'                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NUMPLACA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMPLACA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODCONFIGURACIONUNIDADCARGA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODCONFIGURACIONUNIDADCARGA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODMARCAVEHICULOCARGA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODMARCAVEHICULOCARGA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODLINEAVEHICULOCARGA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODLINEAVEHICULOCARGA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("ANOFABRICACIONVEHICULOCARGA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.ANOFABRICACIONVEHICULOCARGA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODTIPOIDPROPIETARIO")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODTIPOIDPROPIETARIO)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NUMIDPROPIETARIO")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMIDPROPIETARIO)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODTIPOIDTENEDOR")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODTIPOIDTENEDOR)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NUMIDTENEDOR")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMIDTENEDOR)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODTIPOCOMBUSTIBLE")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODTIPOCOMBUSTIBLE)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("PESOVEHICULOVACIO")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.PESOVEHICULOVACIO)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("UNIDADMEDIDACAPACIDAD")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.UNIDADMEDIDACAPACIDAD)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODCOLORVEHICULOCARGA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODCOLORVEHICULOCARGA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODTIPOCARROCERIA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODTIPOCARROCERIA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NUMNITASEGURADORASOAT")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITASEGURADORASOAT)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("FECHAVENCIMIENTOSOAT")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.FECHAVENCIMIENTOSOAT)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NUMSEGUROSOAT")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMSEGUROSOAT)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NUMCHASIS")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMCHASIS)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        If Me.CAPACIDADUNIDADCARGA <> String.Empty And (Me.CODCONFIGURACIONUNIDADCARGA = 50 Or Me.CODCONFIGURACIONUNIDADCARGA = 55 Or Me.CODCONFIGURACIONUNIDADCARGA = 56 Or Me.CODCONFIGURACIONUNIDADCARGA = 64 Or Me.CODCONFIGURACIONUNIDADCARGA = 74 Or Me.CODCONFIGURACIONUNIDADCARGA = 85) Then
'                            NuevoElemento = XmlDocumento.CreateElement("CAPACIDADUNIDADCARGA")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.CAPACIDADUNIDADCARGA)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If
'                    Case Tipos_Procesos.FORTMATO_CONSULTA
'                        Dim strAxuliarCampos As String = String.Empty
'                        For Each Items As String In lstVariables
'                            If strAxuliarCampos = String.Empty Then
'                                strAxuliarCampos = Items
'                            Else
'                                strAxuliarCampos &= "," & Items
'                            End If
'                        Next
'                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        Dim txtNodo As XmlText = XmlDocumento.CreateTextNode("NUMPLACA" & strAxuliarCampos)
'                        xmlElementoRoot.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
'                        xmlElementoRoot.AppendChild(NuevoElemento)
'                        xmlElementoRoot = XmlDocumento.DocumentElement

'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        NuevoElemento = XmlDocumento.CreateElement("NUMPLACA")
'                        txtNodo = XmlDocumento.CreateTextNode("'" & Me.NUMPLACA & "'")
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)


'                End Select
'            Catch ex As Exception
'            End Try
'        End Sub

'        Public Sub Crear_XML_Semirremolque(ByRef XmlDocumento As XmlDocument, ByVal TiposProcesos As Tipos_Procesos, ByVal lstVariables As List(Of String))
'            Try
'                Dim xmlElementoRoot As XmlElement = XmlDocumento.DocumentElement
'                Dim NuevoElemento As XmlElement = XmlDocumento.CreateElement(NOMBRE_TAG)
'                xmlElementoRoot.AppendChild(NuevoElemento)
'                Dim txtNodo As XmlText

'                Select Case TiposProcesos
'                    Case Tipos_Procesos.FORMATO_INGRESO
'                        xmlElementoRoot = XmlDocumento.DocumentElement

'                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NUMPLACA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMPLACA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODCONFIGURACIONUNIDADCARGA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODCONFIGURACIONUNIDADCARGA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODMARCAVEHICULOCARGA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODMARCAVEHICULOCARGA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("ANOFABRICACIONVEHICULOCARGA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.ANOFABRICACIONVEHICULOCARGA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODTIPOIDPROPIETARIO")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODTIPOIDPROPIETARIO)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NUMIDPROPIETARIO")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMIDPROPIETARIO)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODTIPOIDTENEDOR")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODTIPOIDTENEDOR)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("NUMIDTENEDOR")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMIDTENEDOR)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("PESOVEHICULOVACIO")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.PESOVEHICULOVACIO)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("UNIDADMEDIDACAPACIDAD")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.UNIDADMEDIDACAPACIDAD)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        'NuevoElemento = XmlDocumento.CreateElement("CODCOLORVEHICULOCARGA")
'                        'txtNodo = XmlDocumento.CreateTextNode(Me.CODCOLORVEHICULOCARGA)
'                        'xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        'xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CODTIPOCARROCERIA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CODTIPOCARROCERIA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement("CAPACIDADUNIDADCARGA")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.CAPACIDADUNIDADCARGA)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        If Val(Me.NUMEJES) > General.CERO Then
'                            NuevoElemento = XmlDocumento.CreateElement("NUMEJES")
'                            txtNodo = XmlDocumento.CreateTextNode(Me.NUMEJES)
'                            xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                            xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)
'                        End If
'                    Case Tipos_Procesos.FORTMATO_CONSULTA
'                        Dim strAxuliarCampos As String = String.Empty
'                        For Each Items As String In lstVariables
'                            If strAxuliarCampos = String.Empty Then
'                                strAxuliarCampos = Items
'                            Else
'                                strAxuliarCampos &= "," & Items
'                            End If
'                        Next
'                        If strAxuliarCampos <> String.Empty Then strAxuliarCampos = "," & strAxuliarCampos
'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        txtNodo = XmlDocumento.CreateTextNode("NUMPLACA" & strAxuliarCampos)
'                        xmlElementoRoot.LastChild.AppendChild(txtNodo)

'                        NuevoElemento = XmlDocumento.CreateElement(NOMBRE_TAG_CONSULTA)
'                        xmlElementoRoot.AppendChild(NuevoElemento)
'                        xmlElementoRoot = XmlDocumento.DocumentElement

'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        NuevoElemento = XmlDocumento.CreateElement("NUMNITEMPRESATRANSPORTE")
'                        txtNodo = XmlDocumento.CreateTextNode(Me.NUMNITEMPRESATRANSPORTE)
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                        xmlElementoRoot = XmlDocumento.DocumentElement
'                        NuevoElemento = XmlDocumento.CreateElement("NUMPLACA")
'                        txtNodo = XmlDocumento.CreateTextNode("'" & Replace(Me.NUMPLACA, " ", "") & "'")
'                        xmlElementoRoot.LastChild.AppendChild(NuevoElemento)
'                        xmlElementoRoot.LastChild.LastChild.AppendChild(txtNodo)

'                End Select
'            Catch ex As Exception
'            End Try
'        End Sub

'        Public Function Reportar_Vehiculo(ByRef TextoXmlVehiculo As String, ByVal TipoFormato As Tipos_Procesos, ByRef strErrores As String, ByRef lstVariables As List(Of String)) As Double
'            Try
'                strErrores = String.Empty
'                Reportar_Vehiculo = General.CERO
'                solicitud.tipo = Solicitud.SOLICITUD_WS_MINISTERIO_TIPO_INSERTAR
'                solicitud.Procesoid = Solicitud.SOLICITUD_WS_MINISTERIO_PROCESO_VEHICULO

'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)

'                Me.acceso.Crear_XML_Acceso(xmlDoc)
'                Me.solicitud.Crear_XML_Solicitud(xmlDoc, TipoFormato, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_VEHICULO)
'                Me.Crear_XML_Vehiculo(xmlDoc, TipoFormato, lstVariables)
'                TextoXmlVehiculo = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Reportar_Vehiculo = Obtener_Mensaje_XML(Me.wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TipoFormato, strErrores, lstVariables)

'            Catch ex As Exception
'                Reportar_Vehiculo = General.CERO
'                strErrores = ex.Message
'            End Try

'        End Function
'        Public Function Obtener_Mensaje_XML(ByVal strMensaje As String, ByVal TipoFormato As Tipos_Procesos, ByRef strErrores As String, ByRef lstVariables As List(Of String)) As Double
'            Try
'                Obtener_Mensaje_XML = General.CERO
'                Dim xmlRespuesta As New XmlDocument
'                xmlRespuesta.LoadXml(strMensaje)

'                Select Case TipoFormato
'                    Case Tipos_Procesos.FORMATO_INGRESO
'                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
'                            Obtener_Mensaje_XML = General.CERO
'                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
'                        End If
'                        If xmlRespuesta.GetElementsByTagName("ingresoid").Count > 0 Then
'                            Obtener_Mensaje_XML = Val(xmlRespuesta.GetElementsByTagName("ingresoid")(0).InnerText)
'                            strErrores = String.Empty
'                        End If
'                    Case Tipos_Procesos.FORTMATO_CONSULTA
'                        If xmlRespuesta.GetElementsByTagName("ErrorMSG").Count > 0 Then
'                            Obtener_Mensaje_XML = General.CERO
'                            strErrores = xmlRespuesta.GetElementsByTagName("ErrorMSG")(0).InnerText
'                        End If
'                        If xmlRespuesta.GetElementsByTagName("numplaca").Count > 0 Then
'                            Obtener_Mensaje_XML = Val(xmlRespuesta.GetElementsByTagName("numplaca").Count)
'                            strErrores = String.Empty
'                            For i As Integer = 0 To lstVariables.Count - 1
'                                If xmlRespuesta.GetElementsByTagName(lstVariables(i).ToLower).Count > 0 Then
'                                    lstVariables(i) = xmlRespuesta.GetElementsByTagName(lstVariables(i).ToLower)(0).InnerText
'                                Else
'                                    lstVariables(i) = String.Empty
'                                End If
'                            Next
'                        End If
'                End Select
'            Catch ex As Exception
'                strErrores = ex.Message
'            End Try
'        End Function
'        Public Function Reportar_Semirremolque(ByRef TextoXmlSemiremolque As String, ByVal TiposProcesos As Tipos_Procesos, ByRef strErrores As String, ByVal lstVariablesSemiRemo As List(Of String)) As Double
'            Try
'                Reportar_Semirremolque = General.CERO

'                solicitud.tipo = Solicitud.SOLICITUD_WS_MINISTERIO_TIPO_INSERTAR
'                solicitud.Procesoid = Solicitud.SOLICITUD_WS_MINISTERIO_PROCESO_VEHICULO

'                Dim xmlDoc As New XmlDocument
'                Dim declaration As XmlDeclaration
'                declaration = xmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
'                xmlDoc.AppendChild(declaration)
'                Dim ElementoRaiz As XmlElement = xmlDoc.CreateElement("root")
'                xmlDoc.AppendChild(ElementoRaiz)

'                Me.acceso.Crear_XML_Acceso(xmlDoc)
'                Me.solicitud.Crear_XML_Solicitud(xmlDoc, TiposProcesos, Tipos_Documentos.SOLICITUD_WS_MINISTERIO_PROCESO_VEHICULO)
'                Me.Crear_XML_Semirremolque(xmlDoc, TiposProcesos, lstVariablesSemiRemo)
'                TextoXmlSemiremolque = Me.objGeneral.Formatear_XML(xmlDoc.InnerXml) & vbNewLine
'                Reportar_Semirremolque = Obtener_Mensaje_XML(Me.wsManifiestoElectronico.AtenderMensajeBPM(xmlDoc.OuterXml), TiposProcesos, strErrores, lstVariablesSemiRemo)

'            Catch ex As Exception
'                Reportar_Semirremolque = General.CERO
'                strErrores = ex.Message
'            End Try

'        End Function
'        Public Function Guardar_Vehiculo_Confirmacion(ByVal dblNumeroConfirmacion As Double, ByVal lonCodigoEmpresa As Long, ByVal strCodigoUsuario As String, ByRef strMensaje As String, Optional ByVal TipoVehiculo As enumTipoVehiculo = enumTipoVehiculo.VEHICULO) As Boolean
'            Try
'                If dblNumeroConfirmacion <> General.UNO And dblNumeroConfirmacion <> General.CERO Then
'                    Me.objGeneral.ComandoSQL = New SqlCommand
'                    If TipoVehiculo = enumTipoVehiculo.VEHICULO Then
'                        Me.objGeneral.ComandoSQL.CommandText = "sp_vehiculo_reporte_ministerio"
'                    Else
'                        Me.objGeneral.ComandoSQL.CommandText = "sp_remolque_reporte_ministerio"
'                    End If
'                    Me.objGeneral.ComandoSQL.CommandType = CommandType.StoredProcedure
'                    Me.objGeneral.ComandoSQL.Parameters.Clear()
'                    Me.objGeneral.ComandoSQL.Connection = New SqlConnection(Me.objGeneral.CadenaDeConexionSQL)

'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Empr_Codigo", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Empr_Codigo").Value = lonCodigoEmpresa
'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_VEHI_Placa", SqlDbType.VarChar) : Me.objGeneral.ComandoSQL.Parameters("@par_VEHI_Placa").Value = Me.NUMPLACA
'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Numero_Confirmacion", SqlDbType.Decimal) : Me.objGeneral.ComandoSQL.Parameters("@par_Numero_Confirmacion").Value = dblNumeroConfirmacion
'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Usua_reporta", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Usua_reporta").Value = strCodigoUsuario
'                    Me.objGeneral.ComandoSQL.Parameters.Add("@par_Cantidad_Insertados", SqlDbType.Char, 10) : Me.objGeneral.ComandoSQL.Parameters("@par_Cantidad_Insertados").Direction = ParameterDirection.Output

'                    If Me.objGeneral.ComandoSQL.Connection.State = ConnectionState.Closed Then
'                        Me.objGeneral.ComandoSQL.Connection.Open()
'                    End If

'                    Me.objGeneral.ComandoSQL.ExecuteReader()

'                    If Val(Me.objGeneral.ComandoSQL.Parameters("@par_Cantidad_Insertados").Value) > General.CERO Then
'                        Guardar_Vehiculo_Confirmacion = True
'                    Else
'                        Guardar_Vehiculo_Confirmacion = False
'                    End If
'                Else
'                    Guardar_Vehiculo_Confirmacion = True
'                End If
'            Catch ex As Exception
'                strMensaje &= ex.Message
'                Guardar_Vehiculo_Confirmacion = False
'            End Try
'        End Function
'#End Region

'    End Class
'End Namespace
