﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace ServicioCliente
    <JsonObject>
    Public NotInheritable Class ServiciosENCOE

        <JsonProperty>
        Public Property OrdenServicio As EncabezadoSolicitudOrdenServicios
        <JsonProperty>
        Public Property CodigoCaja As String
        <JsonProperty>
        Public Property CentroOperacion As String
        Public Property Descripcion As String
        <JsonProperty>
        Public Property EstadoProgramacion As ValorCatalogos
        <JsonProperty>
        Public Property CausalCierre As ValorCatalogos
    End Class
End Namespace


