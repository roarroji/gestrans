﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Seguridad

Namespace SeguridadUsuarios

    ''' <summary>
    ''' Clase <see cref="EstudioSeguridad"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EstudioSeguridad
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EstudioSeguridad"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EstudioSeguridad"/>
        ''' </summary>
        ''' <param name="lector">Objeto datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            NumeroDocumento = Read(lector, "Numero_Documento")
            Fecha = Read(lector, "Fecha")
            Placa = Read(lector, "Placa")
            EstadoAutorizacion = Read(lector, "Estado_Solicitud")
            Estado = Read(lector, "Estado")
            NumeroAutorizacion = Read(lector, "Numero_Autorizacion")

            Obtener = Read(lector, "Obtener")

            If Obtener = 1 Then 'Obtener

                Oficinas = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "Nombre_Oficina")}

                ClaseRiesgo = Read(lector, "CATA_CRES_Codigo")
                Observaciones = Read(lector, "Observaciones_Estudio_Seguridad")

                MarcaVehiculo = Read(lector, "MAVE_Codigo")
                TipoVehiculo = Read(lector, "CATA_TIVE_Codigo")
                Modelo = Read(lector, "Modelo_Vehiculo")
                ModeloRepotenciado = Read(lector, "Modelo_Repotenciado")
                ColorVehiculo = Read(lector, "COVE_Codigo")

                Aseguradora = New Terceros With {.Codigo = Read(lector, "TERC_Aseguradora_Seguro_Obligatorio"), .NombreCompleto = Read(lector, "Nombre_Aseguradora")}
                NumeroSeguro = Read(lector, "Numero_Seguro_Obligatorio")
                FechaEmisionSeguro = Read(lector, "Fecha_Emision_Seguro_Obligatorio")
                FechaVenceSeguro = Read(lector, "Fecha_Vence_Seguro_Obligatorio")
                NumeroTecnomecanica = Read(lector, "Numero_Revision_Tecnomecanica")
                FechaEmisionTecnomecanica = Read(lector, "Fecha_Emision_Revision_Tecnomecanica")
                FechaVenceTecnomecanica = Read(lector, "Fecha_Vence_Revision_Tecnomecanica")

                NombreEmpresaGPS = Read(lector, "Nombre_Empresa_GPS")
                TelefonoEmpresaGPS = Read(lector, "Telefono_Empresa_GPS")
                UsuarioGPS = Read(lector, "Usuario_GPS")
                ClaveGPS = Read(lector, "Clave_GPS")

                EmpresaAfiliadora = Read(lector, "Empresa_Afiliadora")
                TelefonoAfiliadora = Read(lector, "Telefono_Afiliadora")
                NumeroCarnetAfiliadora = Read(lector, "Numero_Carnet_Afiliadora")
                FechaVenceAfiliadora = Read(lector, "Fecha_Vence_Afiliadora")

                PlacaSemirremolque = Read(lector, "Placa_Semirremolque")
                MarcaSemirremolque = New MarcaSemirremolques With {.Codigo = Read(lector, "MASE_Codigo")}
                TipoSemirremolque = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TISE_Codigo")}
                TipoCarroceriaSemirremolque = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICA_Codigo")}
                ModeloSemirremolque = Read(lector, "Modelo_Semirremolque")
                NumeroEjes = Read(lector, "Numero_Ejes")

                Propietario = New Terceros With {.TipoNaturaleza = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TINT_Propietario")},
                                             .TipoIdentificacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIID_Propietario")},
                                             .NumeroIdentificacion = Read(lector, "Numero_Identificacion_Propietario"),
                                             .Nombre = Read(lector, "Nombre_Propietario"),
                                             .PrimeroApellido = Read(lector, "Apellido1_Propietario"),
                                             .SegundoApellido = Read(lector, "Apellido2_Propietario"),
                                             .RazonSocial = Read(lector, "Razon_Social_Propietario"),
                                             .DigitoChequeo = Read(lector, "Digito_Chequeo_Propietario"),
                                             .CiudadExpedicionIdent = New Ciudades With {.Codigo = Read(lector, "CIUD_Identificacion_Propietario")},
                                             .Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Residencia_Propietario")},
                                             .Direccion = Read(lector, "Direccion_Propietario"),
                                             .Telefonos = Read(lector, "Telefono_Propietario"),
                                             .Celular = Read(lector, "Celular_Propietario")
            }

                PropietarioRemolque = New Terceros With {.TipoNaturaleza = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TINT_Propietario_Remolque")},
                                             .TipoIdentificacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIID_Propietario_Remolque")},
                                             .NumeroIdentificacion = Read(lector, "Numero_Identificacion_Propietario_Remolque"),
                                             .Nombre = Read(lector, "Nombre_Propietario_Remolque"),
                                             .PrimeroApellido = Read(lector, "Apellido1_Propietario_Remolque"),
                                             .SegundoApellido = Read(lector, "Apellido2_Propietario_Remolque"),
                                             .RazonSocial = Read(lector, "Razon_Social_Propietario_Remolque"),
                                             .DigitoChequeo = Read(lector, "Digito_Chequeo_Propietario_Remolque"),
                                             .CiudadExpedicionIdent = New Ciudades With {.Codigo = Read(lector, "CIUD_Identificacion_Propietario_Remolque")},
                                             .Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Residencia_Propietario_Remolque")},
                                             .Direccion = Read(lector, "Direccion_Propietario_Remolque"),
                                             .Telefonos = Read(lector, "Telefono_Propietario_Remolque"),
                                             .Celular = Read(lector, "Celular_Propietario_Remolque")
            }

                Tenedor = New Terceros With {.TipoNaturaleza = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TINT_Tenedor")},
                                             .TipoIdentificacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIID_Tenedor")},
                                             .NumeroIdentificacion = Read(lector, "Numero_Identificacion_Tenedor"),
                                             .Nombre = Read(lector, "Nombre_Tenedor"),
                                             .PrimeroApellido = Read(lector, "Apellido1_Tenedor"),
                                             .SegundoApellido = Read(lector, "Apellido2_Tenedor"),
                                             .RazonSocial = Read(lector, "Razon_Social_Tenedor"),
                                             .DigitoChequeo = Read(lector, "Digito_Chequeo_Tenedor"),
                                             .CiudadExpedicionIdent = New Ciudades With {.Codigo = Read(lector, "CIUD_Identificacion_Tenedor")},
                                             .Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Residencia_Tenedor")},
                                             .Direccion = Read(lector, "Direccion_Tenedor"),
                                             .Telefonos = Read(lector, "Telefono_Tenedor"),
                                             .Celular = Read(lector, "Celular_Tenedor")
            }

                Conductor = New Terceros With {.TipoNaturaleza = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TINT_Conductor")},
                                             .TipoIdentificacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIID_Conductor")},
                                             .NumeroIdentificacion = Read(lector, "Numero_Identificacion_Conductor"),
                                             .Nombre = Read(lector, "Nombre_Conductor"),
                                             .PrimeroApellido = Read(lector, "Apellido1_Conductor"),
                                             .SegundoApellido = Read(lector, "Apellido2_Conductor"),
                                             .RazonSocial = Read(lector, "Razon_Social_Conductor"),
                                             .DigitoChequeo = Read(lector, "Digito_Chequeo_Conductor"),
                                             .CiudadExpedicionIdent = New Ciudades With {.Codigo = Read(lector, "CIUD_Identificacion_Conductor")},
                                             .Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Residencia_Conductor")},
                                             .Direccion = Read(lector, "Direccion_Conductor"),
                                             .Telefonos = Read(lector, "Telefono_Conductor"),
                                             .Celular = Read(lector, "Celular_Conductor")
            }

                Cliente = Read(lector, "Nombre_Cliente")
                MercanciaCliente = Read(lector, "Nombre_Mercancia_Cliente")
                Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo_Cliente"), .Nombre = Read(lector, "Nombre_Ruta")}
                ValorMercancia = Read(lector, "Valor_Mercancia_Cliente")
                ObservacionesCliente = Read(lector, "Observaciones_Despacho_Cliente")
                Estado = Read(lector, "Estado")
                Anulado = Read(lector, "Anulado")
                NumeroAutorizacion = Read(lector, "Numero_Autorizacion")

                CodigosTerceros = Read(lector, "Codigos_Terceros")

                If CodigosTerceros = 1 Then
                    CodigoPropietario = Read(lector, "Codigo_Propietario")
                    CodigoPropietarioRemolque = Read(lector, "Codigo_Propietario_Remolque")
                    CodigoTenedor = Read(lector, "Codigo_Tenedor")
                    CodigoConductor = Read(lector, "Codigo_Conductor")
                End If

            End If

            If Obtener = 0 Then 'Consultar
                TotalRegistros = Read(lector, "TotalRegistros")
                Propietario = New Terceros With {.NombreCompleto = Read(lector, "Nombre_Propietario")}
                Conductor = New Terceros With {.NombreCompleto = Read(lector, "Nombre_Conductor")}
                Tenedor = New Terceros With {.NombreCompleto = Read(lector, "Nombre_Tenedor")}
                FechaSolicito = Read(lector, "Fecha_Solicitud")
                UsuarioSolicito = Read(lector, "Usuario_Solicitud")
                FechaEstudio = Read(lector, "Fecha_Estudio")
                UsuarioEstudio = Read(lector, "Usuario_Estudio")
            End If
        End Sub

        <JsonProperty>
        Public Property CodigoPropietario As Integer
        <JsonProperty>
        Public Property CodigoPropietarioRemolque As Integer
        <JsonProperty>
        Public Property CodigoTenedor As Integer
        <JsonProperty>
        Public Property CodigoConductor As Integer
        <JsonProperty>
        Public Property PlacaSemirremolque As String
        <JsonProperty>
        Public Property MarcaSemirremolque As MarcaSemirremolques
        <JsonProperty>
        Public Property TipoSemirremolque As ValorCatalogos
        <JsonProperty>
        Public Property TipoCarroceriaSemirremolque As ValorCatalogos
        <JsonProperty>
        Public Property ModeloSemirremolque As Integer
        <JsonProperty>
        Public Property NumeroEjes As Integer
        <JsonProperty>
        Public Property Placa As String
        <JsonProperty>
        Public Property NumeroAutorizacion As Long
        <JsonProperty>
        Public Property MarcaVehiculo As Integer
        <JsonProperty>
        Public Property TipoVehiculo As Integer
        <JsonProperty>
        Public Property Modelo As Integer
        <JsonProperty>
        Public Property ModeloRepotenciado As Integer
        <JsonProperty>
        Public Property ColorVehiculo As Integer
        <JsonProperty>
        Public Property Oficinas As Oficinas
        <JsonProperty>
        Public Property EstadoAutorizacion As Integer
        <JsonProperty>
        Public Property ClaseRiesgo As Integer
        <JsonProperty>
        Public Property Aseguradora As Terceros
        <JsonProperty>
        Public Property NumeroSeguro As String
        <JsonProperty>
        Public Property FechaEmisionSeguro As Date
        <JsonProperty>
        Public Property FechaVenceSeguro As Date
        <JsonProperty>
        Public Property NumeroTecnomecanica As String
        <JsonProperty>
        Public Property FechaEmisionTecnomecanica As Date
        <JsonProperty>
        Public Property FechaVenceTecnomecanica As Date
        <JsonProperty>
        Public Property NombreEmpresaGPS As String
        <JsonProperty>
        Public Property TelefonoEmpresaGPS As String
        <JsonProperty>
        Public Property UsuarioGPS As String
        <JsonProperty>
        Public Property ClaveGPS As String
        <JsonProperty>
        Public Property EmpresaAfiliadora As String
        <JsonProperty>
        Public Property TelefonoAfiliadora As String
        <JsonProperty>
        Public Property NumeroCarnetAfiliadora As String
        <JsonProperty>
        Public Property FechaVenceAfiliadora As Date
        <JsonProperty>
        Public Property Propietario As Terceros
        <JsonProperty>
        Public Property PropietarioRemolque As Terceros
        <JsonProperty>
        Public Property Tenedor As Terceros
        <JsonProperty>
        Public Property Conductor As Terceros
        <JsonProperty>
        Public Property Cliente As String
        <JsonProperty>
        Public Property MercanciaCliente As String
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property ValorMercancia As Double
        <JsonProperty>
        Public Property ObservacionesCliente As String
        <JsonProperty>
        Public Property FechaInicio As Date
        <JsonProperty>
        Public Property FechaFin As Date
        <JsonProperty>
        Public Property NombrePropietario As String
        <JsonProperty>
        Public Property NombreConductor As String
        <JsonProperty>
        Public Property NombreTenedor As String
        <JsonProperty>
        Public Property FechaSolicito As Date
        <JsonProperty>
        Public Property UsuarioSolicito As String
        <JsonProperty>
        Public Property FechaEstudio As Date
        <JsonProperty>
        Public Property UsuarioEstudio As String
        <JsonProperty>
        Public Property Numeracion As Integer
        <JsonProperty>
        Public Property ConsularDocumentos As Short
        <JsonProperty>
        Public Property CodigosTerceros As Short
        <JsonProperty>
        Public Property ListadoDocumentos As IEnumerable(Of EstudioSeguridadDocumentos)
        <JsonProperty>
        Public Property Documentos As IEnumerable(Of EstudioSeguridadDocumentos)
        <JsonProperty>
        Public Property ListadoReferencias As IEnumerable(Of DetalleReferenciasEstudioSeguridad)
        <JsonProperty>
        Public Property ListadoEntidades As IEnumerable(Of DetalleEntidadesConsultadasEstudioSeguridad)

    End Class
End Namespace
