﻿Imports Newtonsoft.Json

Namespace SeguridadUsuarios

    ''' <summary>
    ''' Clase <see cref="MenuAplicaciones"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class MenuAplicaciones
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="MenuAplicaciones"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="MenuAplicaciones"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Modulo = New ModuloAplicaciones With {.Codigo = Read(lector, "MOAP_Codigo"), .Nombre = Read(lector, "NombreModulo")}
            Codigo = Read(lector, "CodigoMenu")
            Nombre = Read(lector, "NombreMenu")
            UrlPagina = Read(lector, "Url_Pagina")
            MostrarMenu = Read(lector, "Mostrar_Menu")
            Padre = Read(lector, "Padre_Menu")
            Habilitado = Read(lector, "Aplica_Habilitar")
            AplicaConsultar = Read(lector, "Aplica_Consultar")
            AplicaActualizar = Read(lector, "Aplica_Actualizar")
            AplicaEliminarAnular = Read(lector, "Aplica_Eliminar_Anular")
            AplicaImprimir = Read(lector, "Aplica_Imprimir")
            AplicaContabilidad = Read(lector, "Aplica_Contabilidad")

            OpcionListado = Read(lector, "OpcionListado")
            OpcionPermiso = Read(lector, "OpcionPermiso")

            AplicaPermisoConsultar = Read(lector, "Consultar")
            AplicaPermisoActualizar = Read(lector, "Actualizar")
            AplicaPermisoEliminarAnular = Read(lector, "Eliminar_Anular")
            AplicaPermisoImprimir = Read(lector, "Imprimir")
            AplicaPermisoHabilitar = Read(lector, "Habilitar")
            AplicaPermisoContabilidad = Read(lector, "Contabilidad")

            EstadoModulo = Read(lector, "EstadoModulo")
            Aplicacion = Read(lector, "CATA_APLI_Codigo")

        End Sub

        ''' <summary>
        ''' Obtiene o establece el codigo del modulo
        ''' </summary>
        <JsonProperty>
        Public Property Modulo As ModuloAplicaciones

        ''' <summary>
        ''' Obtiene o establece el codigo del menu
        ''' </summary>
        <JsonProperty>
        Public Property Codigo As Integer

        ''' <summary>
        ''' Obtiene o establece el nombre del menu
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String

        ''' <summary>
        ''' Obtiene o establece la linea de servio transporte
        ''' </summary>
        <JsonProperty>
        Public Property UrlPagina As String
        ''' <summary>
        ''' Obtiene o establece codigo del padre del menu
        ''' </summary>
        <JsonProperty>
        Public Property Padre As Integer

        ''' <summary>
        ''' Obtiene o establece si el usuario esta habilitado
        ''' </summary>
        <JsonProperty>
        Public Property AplicaPermisoHabilitar As Short

        ''' <summary>
        ''' Obtiene o establece el codigo del grupo de seguridad
        ''' </summary>
        <JsonProperty>
        Public Property Habilitado As Short

        ''' <summary>
        ''' Obtiene o establece si el menu tiene opcion de listados
        ''' </summary>
        <JsonProperty>
        Public Property OpcionListado As Integer
        ''' <summary>
        ''' Obtiene o establece si el menu tiene opcion de listados
        ''' </summary>
        <JsonProperty>
        Public Property OpcionPermiso As Integer
        ''' <summary>
        ''' Obtiene o establece la opcion de consultar para el menu 
        ''' </summary>
        <JsonProperty>
        Public Property AplicaConsultar As Integer
        ''' <summary>
        ''' Obtiene o establece la opcion de actualizar para el menu 
        ''' </summary>
        <JsonProperty>
        Public Property AplicaActualizar As Integer
        ''' <summary>
        ''' Obtiene o establece la opcion de eliminar o anular para el menu 
        ''' </summary>
        <JsonProperty>
        Public Property AplicaEliminarAnular As Integer
        ''' <summary>
        ''' Obtiene o establece la opcion contabilidad para el menu 
        ''' </summary>
        <JsonProperty>
        Public Property AplicaContabilidad As Integer

        ''' <summary>
        ''' Obtiene o establece la opcion de imprimir para el menu 
        ''' </summary>
        <JsonProperty>
        Public Property AplicaImprimir As Integer
        ''' <summary>
        ''' Obtiene o establece la opcion de mostrar el menu
        ''' </summary>
        <JsonProperty>
        Public Property MostrarMenu As Integer
        ''' <summary>
        ''' Obtiene o establece el permiso para cunsultar
        ''' </summary>
        <JsonProperty>
        Public Property AplicaPermisoConsultar As Integer
        ''' <summary>
        ''' Obtiene o establece el permiso para actualizar 
        ''' </summary>
        <JsonProperty>
        Public Property AplicaPermisoActualizar As Integer
        ''' <summary>
        ''' Obtiene o establece el permiso para eliminar o anular
        ''' </summary>
        <JsonProperty>
        Public Property AplicaPermisoEliminarAnular As Integer
        ''' <summary>
        ''' Obtiene o establece el permiso para imprimir
        ''' </summary>
        <JsonProperty>
        Public Property AplicaPermisoImprimir As Integer

        ''' <summary>
        ''' Obtiene o establece el permiso para imprimir
        ''' </summary>
        <JsonProperty>
        Public Property AplicaPermisoContabilidad As Integer

        <JsonProperty>
        Public Property EstadoModulo As Integer

        <JsonProperty>
        Public Property Aplicacion As Integer


    End Class
End Namespace
