﻿Imports Newtonsoft.Json

Namespace SeguridadUsuarios

    ''' <summary>
    ''' Clase <see cref="ModuloAplicaciones"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ModuloAplicaciones
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ModuloAplicaciones"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ModuloAplicaciones"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            Estado = Read(lector, "Estado")
            Orden = Read(lector, "Orden")
            Aplicacion = Read(lector, "CATA_APLI_Codigo")
            MostrarModulo = Read(lector, "Mostrar_Menu")
            Imagen = Read(lector, "Imagen")

        End Sub

        ''' <summary>
        ''' Obtiene o establece el codigo del modulo
        ''' </summary>
        <JsonProperty>
        Public Property Codigo As Integer

        ''' <summary>
        ''' Obtiene o establece si el modulo esta habilitado
        ''' </summary>
        <JsonProperty>
        Public Property Estado As Integer

        ''' <summary>
        ''' Obtiene o establece el nombre del Modulo
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String

        ''' <summary>
        ''' Obtiene o establece la linea de servio transporte
        ''' </summary>
        <JsonProperty>
        Public Property Imagen As String

        ''' <summary>
        ''' Obtiene o establece la linea de servio transporte
        ''' </summary>
        <JsonProperty>
        Public Property Aplicacion As Integer
        ''' <summary>
        ''' Obtiene o establece el orden de los modulos 
        ''' </summary>
        <JsonProperty>
        Public Property Orden As Integer
        ''' <summary>
        ''' Obtiene o establece 
        ''' </summary>
        <JsonProperty>
        Public Property MostrarModulo As Integer



    End Class
End Namespace
