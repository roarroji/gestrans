﻿Imports Newtonsoft.Json

Namespace SeguridadUsuarios

    ''' <summary>
    ''' Clase <see cref="PermisoGrupoUsuarios"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class PermisoGrupoUsuarios
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="PermisoGrupoUsuarios"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="PermisoGrupoUsuarios"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")

            Modulo = New ModuloAplicaciones With {.Codigo = Read(lector, "MOAP_Codigo")}

            Menu = New MenuAplicaciones With {
                .Codigo = Read(lector, "MEAP_Codigo"),
                .Nombre = Read(lector, "NombreMenu"),
                .Padre = Read(lector, "Padre_Menu"),
                .MostrarMenu = Read(lector, "Mostrar_Menu"),
                .OpcionListado = Read(lector, "Opcion_Listado")
            }

            Grupo = New GrupoUsuarios With {.Codigo = Read(lector, "GRUS_Codigo")}

            Habilitar = Read(lector, "Habilitar")
            Consultar = Read(lector, "Consultar")
            Actualizar = Read(lector, "Actualizar")
            EliminarAnular = Read(lector, "Eliminar_Anular")
            Imprimir = Read(lector, "Imprimir")
            Permiso = Read(lector, "Permiso")
            Contabilidad = Read(lector, "Contabilidad")

        End Sub

        <JsonProperty>
        Public Property Codigo As Long
        <JsonProperty>
        Public Property Modulo As ModuloAplicaciones

        <JsonProperty>
        Public Property Menu As MenuAplicaciones

        <JsonProperty>
        Public Property Grupo As GrupoUsuarios

        <JsonProperty>
        Public Property Habilitar As Short

        <JsonProperty>
        Public Property Consultar As Short

        <JsonProperty>
        Public Property Actualizar As Short

        <JsonProperty>
        Public Property EliminarAnular As Short

        <JsonProperty>
        Public Property Imprimir As Short
        <JsonProperty>
        Public Property Permiso As Short

        <JsonProperty>
        Public Property Contabilidad As Short

        <JsonProperty>
        Public Property Plantilla As Byte()

    End Class
End Namespace
