﻿Imports Newtonsoft.Json

Namespace SeguridadUsuarios

    ''' <summary>
    ''' Clase <see cref="UsuarioOficinas"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class UsuarioOficinas
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="UsuarioOficinas"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="UsuarioOficinas"/>
        ''' </summary>
        ''' <param name="lector">Objeto datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "CodigoOficina")
            Nombre = Read(lector, "Oficina")
            CodigoUsuario = Read(lector, "CodigoUsuario")
            CodigoCaja = Read(lector, "CodigoCaja")
            Caja = Read(lector, "Caja")
        End Sub

        <JsonProperty>
        Public Property CodigoUsuario As Integer
        <JsonProperty>
        Public Property Nombre As String

        <JsonProperty>
        Public Property CodigoCaja As Integer
        <JsonProperty>
        Public Property Caja As String

    End Class
End Namespace
