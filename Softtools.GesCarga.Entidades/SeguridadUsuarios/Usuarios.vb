﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace SeguridadUsuarios

    ''' <summary>
    ''' Clase <see cref="Usuarios"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class Usuarios
        Inherits Base

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Usuarios"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="Usuarios"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            CodigoUsuario = Read(lector, "Codigo_Usuario")
            Correo = Read(lector, "Emails")
            CorreoManager = Read(lector, "CorreoManager")
            Nombre = Read(lector, "Nombre")
            Descripcion = Read(lector, "Descripcion")
            ExisteUsuario = Read(lector, "ExisteUsuario")
            Clave = Read(lector, "Clave")
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .Nombre = Read(lector, "TerceroCliente")}
            Manager = Read(lector, "Manager")
            Habilitado = Read(lector, "Habilitado")
            Login = Read(lector, "Login")

            FechaUltimoIngreso = Read(lector, "Fecha_Ultimo_Ingreso")
            DiasCambioClave = Read(lector, "Dias_Cambio_Clave")
            FechaUltimoCambioClave = Read(lector, "Fecha_Ultimo_Cambio_Clave")
            Mensaje = Read(lector, "Mensaje_Banner")
            IntentosIngreso = Read(lector, "Intentos_Ingreso")
            CodigoCambioClave = Read(lector, "Codigo_Cambio_Clave")
            FechaVigenciaCambioClave = Read(lector, "Fecha_Vigencia_Cambio_Clave")
            Empleado = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Empleado"), .Nombre = Read(lector, "Empleado")}
            Proveedor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Proveedor"), .Nombre = Read(lector, "Proveedor")}
            Try
                Conductor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Conductor"), .Nombre = Read(lector, "TerceroConductor")}
            Catch
            End Try

            AplicacionUsuario = Read(lector, "CATA_APLI_Codigo")
            NombreAplicacion = Read(lector, "AplicacionUsuario")


            If Read(lector, "Consultar") = 1 Then 'Se valida si es para la pagina de consulta
                TotalRegistros = Read(lector, "TotalRegistros")
                ' Oficina = New Oficinas With {.Codigo = Read(lector, "CodigoOficina"), .Nombre = Read(lector, "NombreOficina")}
            ElseIf Read(lector, "Consultar") = 2 Then ' se valida si la consulta es para la pagina de gestionar

            Else
                NumeroMaximoAplicativo = Read(lector, "NumeroMaximoAplicativo")
                NumeroMaximoGesphone = Read(lector, "NumeroMaximoGesphone")
                NumeroMaximoPortal = Read(lector, "NumeroMaximoPortal")
                UsuariosLogeadosAplicativo = Read(lector, "UsuariosLogeadosAplicativo")
                UsuariosLogeadosPortal = Read(lector, "UsuariosLogeadosPortal")
                UsuariosLogeadosGesphone = Read(lector, "UsuariosLogeadosGesphone")

            End If
            TotalPuntos = Read(lector, "PuntosConductor")
            ConsultaOtrasOficinas = Read(lector, "Consulta_Otras_Oficinas")
        End Sub

        ''' <summary>
        ''' Obtiene o establece el codigo 
        ''' </summary>
        <JsonProperty>
        Public Property Codigo As Integer

        ''' <summary>
        ''' Obtiene o establece el codigo alterno del usuario
        ''' </summary>
        <JsonProperty>
        Public Property CodigoAlterno As String


        ''' <summary>
        ''' Obtiene o establece el codigo del usuario
        ''' </summary>
        <JsonProperty>
        Public Property CodigoUsuario As String

        ''' <summary>
        ''' Obtiene o establece la oficina del usuario
        ''' </summary>
        <JsonProperty>
        Public Property Oficina As Oficinas

        ''' <summary>
        ''' Obtiene o establece el nombre del usuario
        ''' </summary>
        <JsonProperty>
        Public Property Nombre As String

        ''' <summary>
        ''' Obtiene o establece la descripción del usuario
        ''' </summary>
        <JsonProperty>
        Public Property Descripcion As String

        ''' <summary>
        ''' Obtiene o establece condición de seguridad de acceso al sistema
        ''' </summary>
        <JsonProperty>
        Public Property Clave As String

        ''' <summary>
        ''' Obtiene o establece el manager del usuario
        ''' </summary>
        <JsonProperty>
        Public Property Manager As String


        ''' <summary>
        ''' Obtiene o establece el tercero externo del sistema
        ''' </summary>
        <JsonProperty>
        Public Property Cliente As Terceros
        ''' <summary>
        ''' Obtiene o establece el tercero externo del sistema
        ''' </summary>
        <JsonProperty>
        Public Property Proveedor As Terceros

        ''' <summary>
        ''' Obtiene o establece si el usuario esta habilitado
        ''' </summary>
        <JsonProperty>
        Public Property Conductor As Terceros

        ''' <summary>
        ''' Obtiene o establece si el usuario esta habilitado
        ''' </summary>
        <JsonProperty>
        Public Property Habilitado As Short

        ''' <summary>
        ''' Obtiene o establece si el usuario esta Login
        ''' </summary>
        <JsonProperty>
        Public Property Login As Short

        ''' <summary>
        ''' Obtiene o establece la fecha de último ingreso al sistema
        ''' </summary>
        <JsonProperty>
        Public Property FechaUltimoIngreso As DateTime

        ''' <summary>
        ''' Obtiene o establece los dias de cambio de clave
        ''' </summary>
        <JsonProperty>
        Public Property DiasCambioClave As Short

        ''' <summary>
        ''' Obtiene o establece la fecha del ultimo cambio de clave
        ''' </summary>
        <JsonProperty>
        Public Property FechaUltimoCambioClave As DateTime

        ''' <summary>
        ''' Obtiene o establece el mensaje configurado por el usuario
        ''' </summary>
        <JsonProperty>
        Public Property Mensaje As String

        ''' <summary>
        ''' Obtiene o establece los intentos consecutivos de ingreso
        ''' </summary>
        <JsonProperty>
        Public Property IntentosIngreso As Short

        ''' <summary>
        ''' Obtiene o establece el empleado del sistema
        ''' </summary>
        <JsonProperty>
        Public Property Empleado As Terceros

        ''' <summary>
        ''' Obtiene o establece el codigo de aplicacion al que pertenece el usuario
        ''' </summary>
        <JsonProperty>
        Public Property AplicacionUsuario As Integer

        ''' <summary>
        ''' Obtiene o establece  el numero maximo de usuarios para Aplicativo
        ''' </summary>
        <JsonProperty>
        Public Property NumeroMaximoAplicativo As Short

        ''' <summary>
        ''' Obtiene o establece  el numero maximo de usuarios para Gesphone
        ''' </summary>
        <JsonProperty>
        Public Property NumeroMaximoGesphone As Short

        ''' <summary>
        ''' Obtiene o establece el numero maximo de usuarios para portal
        ''' </summary>
        <JsonProperty>
        Public Property NumeroMaximoPortal As Short

        ''' <summary>
        ''' Obtiene o establece la cantidad de usuarios logeados para Aplicativo
        ''' </summary>
        <JsonProperty>
        Public Property UsuariosLogeadosAplicativo As Short

        ''' <summary>
        ''' Obtiene o establece la cantidad de usuarios logeados para Portal
        ''' </summary>
        <JsonProperty>
        Public Property UsuariosLogeadosPortal As Short

        ''' <summary>
        ''' Obtiene o establece la cantidad de usuarios logeados para Gesphone
        ''' </summary>
        <JsonProperty>
        Public Property UsuariosLogeadosGesphone As Short

        <JsonProperty>
        Public Property NombreAplicacion As String

        '''' <summary>
        '''' Obtiene o establece las horarios de acceso al sistema
        '''' </summary>
        '<JsonProperty>
        'Public Property Horarios As IEnumerable(Of UsuarioHorario)

        ''' <summary>
        ''' Obtiene o establece los grupos de seguridad asociadas al usuario
        ''' </summary>
        <JsonProperty>
        Public Property GruposUsuarios As IEnumerable(Of UsuarioGrupoUsuarios)

        ''' <summary>
        ''' Obtiene o establece los menus  asociadas al usuario
        ''' </summary>
        <JsonProperty>
        Public Property ConsultaMenu As IEnumerable(Of MenuAplicaciones)

        ''' <summary>
        ''' Obtiene o establece los modulos  asociadas al usuario
        ''' </summary>
        <JsonProperty>
        Public Property ConsultaModulo As IEnumerable(Of ModuloAplicaciones)

        '''' <summary>
        '''' Obtiene o establece las validaciones 
        '''' </summary>
        '<JsonProperty>
        'Public Property ConsultaValidaciones As IEnumerable(Of Validaciones)



        ''' <summary>
        ''' Obtiene o establece las oficinas asociadas al usuario
        ''' </summary>
        <JsonProperty>
        Public Property Oficinas As IEnumerable(Of Oficinas)

        <JsonProperty>
        Public Property CodigoGrupo As Integer


        <JsonProperty>
        Public Property Token As Short

        <JsonProperty>
        Public Property bolInsertarGrupo As Boolean

        <JsonProperty>
        Public Property OpcionCambioClave As Integer
        ''' <summary>
        ''' Obtiene o establece el catalogo de la hora  milirar de fin
        ''' </summary>
        <JsonProperty>
        Public Property Hora As String
        <JsonProperty>
        Public Property CodigoLineaServicio As Integer
        <JsonProperty>
        Public Property TotalPuntos As Integer
        <JsonProperty>
        Public Property ConsultaOtrasOficinas As Short
        <JsonProperty>
        Public Property Correo As String
        <JsonProperty>
        Public Property CodigoCambioClave As Long
        <JsonProperty>
        Public Property FechaVigenciaCambioClave As DateTime

        <JsonProperty>
        Public Property ExisteUsuario As Long
        <JsonProperty>
        Public Property CorreoManager As String

    End Class
End Namespace
