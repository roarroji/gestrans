﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace ServicioCliente
    <JsonObject>
    Public NotInheritable Class CerrarOrdenServicio
        Inherits BaseDocumento

        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            OrdenServicio = New EncabezadoSolicitudOrdenServicios With {.Numero = Read(lector, "ESOS_Numero"), .NumeroDocumento = Read(lector, "ESOS_NumeroDocumento"), .Fecha = Read(lector, "ESOS_Fecha"),
                                                                        .Cliente = New Terceros With {.Codigo = Read(lector, "CLIE_Codigo"), .NombreCompleto = Read(lector, "CLIE_Nombre")},
                                                                        .Estado = Read(lector, "ESOS_Estado")}
            EstadoProgramacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESSO_Codigo"), .Nombre = Read(lector, "CATA_ESSO_Campo1")}
            CausalCierre = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CCOS_Codigo"), .Nombre = Read(lector, "CATA_CCOS_Campo1")}
            Observaciones = Read(lector, "ESOS_Observaciones_Cierre")
            TotalRegistros = Read(lector, "TotalRegistros")
        End Sub
        <JsonProperty>
        Public Property OrdenServicio As EncabezadoSolicitudOrdenServicios
        <JsonProperty>
        Public Property ENOS_Estado As Integer
        <JsonProperty>
        Public Property EstadoProgramacion As ValorCatalogos
        <JsonProperty>
        Public Property CausalCierre As ValorCatalogos
    End Class
End Namespace

