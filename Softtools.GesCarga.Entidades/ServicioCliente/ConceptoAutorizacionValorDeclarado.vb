﻿
Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.ControlViajes

Namespace ServicioCliente

    <JsonObject>
    Public NotInheritable Class ConceptoAutorizacionValorDeclarado
        Inherits BaseDocumento

        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Autorizacion = New Autorizaciones With {.Codigo = Read(lector, "ENAU_Codigo")}
            Valor = Read(lector, "Valor")
            Facturar = Read(lector, "Facturar")
        End Sub
        <JsonProperty>
        Public Property Autorizacion As Autorizaciones
        <JsonProperty>
        Public Property Valor As Double
        <JsonProperty>
        Public Property Facturar As Short

    End Class
End Namespace
