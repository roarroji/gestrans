﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.ServicioCliente

Namespace ServicioCliente

    ''' <summary>
    ''' Clase <see cref=" DetalleDespachoSolicitudOrdenServicios"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleDespachoSolicitudOrdenServicios
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleDespachoSolicitudOrdenServicios"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleDespachoSolicitudOrdenServicios"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroDocumento = Read(lector, "ESOS_Numero")
            Codigo = Read(lector, "ID")
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "NombreRuta")}
            Ruta.CiudadOrigen = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Origen_Ruta")}
            Ruta.CiudadDestino = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino_Ruta")}

            CodigoTipoRemesa = Read(lector, "Codigo_Tipo_Remesa")
            NombreTipoRemesa = Read(lector, "Nombre_Tipo_Remesa")

            NumeroTarifarioVenta = Read(lector, "ETCV_Numero")
            NumeroTarifarioCompra = Read(lector, "ETCC_Numero")
            NombreTarifarioVenta = Read(lector, "NombreTarifarioVenta")

            LineaNegocioTransporteCarga = New LineaNegocioTransporteCarga With {.Codigo = Read(lector, "LNTC_Codigo"), .Nombre = Read(lector, "NombreLineaNegocio")}
            LineaTipoNegocioTransporteCarga = New LineaNegocioTransporteCarga With {.Codigo = Read(lector, "TLNC_Codigo"), .Nombre = Read(lector, "NombreTipoLineaNegocio")}

            TarifaTransporteCargaCompra = New TarifaTransporteCarga With {.Codigo = Read(lector, "TATC_Codigo_Compra"), .Nombre = Read(lector, "TarifaTransporteCargaCompra")}
            TipoTarifaTransporteCargaCompra = New TipoTarifaTransporteCarga With {.Codigo = Read(lector, "TTTC_Codigo_Compra"), .Nombre = Read(lector, "TipoTarifaTransporteCargaCompra")}

            TarifaTransporteCargaVenta = New TarifaTransporteCarga With {.Codigo = Read(lector, "TATC_Codigo_Venta"), .Nombre = Read(lector, "TarifaTransporteCargaVenta")}
            TipoTarifaTransporteCargaVenta = New TipoTarifaTransporteCarga With {.Codigo = Read(lector, "TTTC_Codigo_Venta"), .Nombre = Read(lector, "TipoTarifaTransporteCargaVenta")}

            ProductoTransportado = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "NombreProducto")}
            Cantidad = Read(lector, "Cantidad")
            PesoKilogramo = Read(lector, "Peso")
            Valor = Read(lector, "Valor_Flete_Cliente")
            DocumentoCliente = Read(lector, "Documento_Cliente")
            CiudadCargue = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Cargue")}
            FechaCargueEsos = Read(lector, "Fecha_Cargue_ESOS")
            SitioCargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Cargue"), .Nombre = Read(lector, "SICD_Nombre_Cargue")}
            DireccionCargue = Read(lector, "Direccion_Cargue")
            TelefonoCargue = Read(lector, "Telefono_Cargue")
            ContactoCargue = Read(lector, "Contacto_Cargue")
            Destinatario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Destinatario"), .Nombre = Read(lector, "NombreDestinatario")}
            CiudadDestinatario = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino")}
            FechaDescargueEsos = Read(lector, "Fecha_Descargue_ESOS")
            SitioDescargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Descargue"), .Nombre = Read(lector, "SICD_Nombre_Descargue")}
            DireccionDestino = Read(lector, "Direccion_Destino")
            TelefonoDestino = Read(lector, "Telefono_Destino")
            ContactoDestino = Read(lector, "Contacto_Destino")
            NumeroContenedor = Read(lector, "Numero_Contenedor")
            MBLContenedor = Read(lector, "MBL_Contenedor")
            HBLContenedor = Read(lector, "HBL_Contenedor")
            DOContenedor = Read(lector, "DO_Contenedor")
            ValorFOB = Read(lector, "Valor_FOB")
            SitioDevolucionContenedor = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Devolucion_Contenedor")}
            FechaDevolucionContenedor = Read(lector, "Fecha_Devolucion_Contenedor")
            ModoTransporteUsado = New ValorCatalogos With {.Codigo = Read(lector, "CATA_MOTR_Codigo")}

            NumeroOrdenCargue = Read(lector, "ENOC_Numero")
            NumeroRemesa = Read(lector, "ENRE_Numero")
            NumeroPlanillaDespacho = Read(lector, "ENPD_Numero")
            NumeroManifiesto = Read(lector, "ENMC_Numero")

            FleteCliente = Read(lector, "Flete_Cliente")
            ValorFleteCliente = Read(lector, "Valor_Flete_Cliente")
            FleteTransportador = Read(lector, "Flete_Transportador")
            ValorFleteTransportador = Read(lector, "Valor_Flete_Transportador")

            ValorEscolta = Read(lector, "Valor_Escolta")
            AplicaEscolta = Read(lector, "Aplica_Escolta")

            NumeroDocumentoOrdenCargue = Read(lector, "ENOC_Numero_Documento")
            NumeroDocumentoRemesa = Read(lector, "ENRE_Numero_Documento")
            NumeroDocumentoPlanillaDespacho = Read(lector, "ENPD_Numero_Documento")
            NumeroDocumentoManifiesto = Read(lector, "ENMC_Numero_Documento")
            ValorCargueTransportador = Read(lector, "Valor_Cargue_Transportador")
            ValorDescargueTransportador = Read(lector, "Valor_Descargue_Transportador")
            FechaCargue = Read(lector, "Fecha_Cargue")
            CiudadDevolucion = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Devolucion_Contenedor")}
            PatioDevolucion = Read(lector, "Patio_Devolcuion")
            FechaDevolucion = Read(lector, "Fecha_Devolcuion")
            PermisoInvias = Read(lector, "Permiso_Invias")
        End Sub

        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property NumeroTarifarioVenta As Long
        <JsonProperty>
        Public Property NombreTarifarioVenta As String
        <JsonProperty>
        Public Property NumeroTarifarioCompra As Long
        <JsonProperty>
        Public Property TarifaTransporteCargaCompra As TarifaTransporteCarga
        <JsonProperty>
        Public Property TipoTarifaTransporteCargaCompra As TipoTarifaTransporteCarga
        <JsonProperty>
        Public Property TarifaTransporteCargaVenta As TarifaTransporteCarga
        <JsonProperty>
        Public Property TipoTarifaTransporteCargaVenta As TipoTarifaTransporteCarga
        <JsonProperty>
        Public Property ProductoTransportado As ProductoTransportados
        <JsonProperty>
        Public Property Cantidad As Integer
        <JsonProperty>
        Public Property PesoKilogramo As Double
        <JsonProperty>
        Public Property Valor As Double
        <JsonProperty>
        Public Property DocumentoCliente As String
        <JsonProperty>
        Public Property CiudadCargue As Ciudades
        <JsonProperty>
        Public Property FechaCargueEsos As DateTime
        <JsonProperty>
        Public Property SitioCargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property DireccionCargue As String
        <JsonProperty>
        Public Property TelefonoCargue As String
        <JsonProperty>
        Public Property ContactoCargue As String
        <JsonProperty>
        Public Property Destinatario As Terceros
        <JsonProperty>
        Public Property CiudadDestinatario As Ciudades
        <JsonProperty>
        Public Property FechaDescargueEsos As DateTime
        <JsonProperty>
        Public Property SitioDescargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property DireccionDestino As String
        <JsonProperty>
        Public Property TelefonoDestino As String
        <JsonProperty>
        Public Property ContactoDestino As String
        <JsonProperty>
        Public Property NumeroContenedor As String
        <JsonProperty>
        Public Property MBLContenedor As String
        <JsonProperty>
        Public Property HBLContenedor As String
        <JsonProperty>
        Public Property DOContenedor As String
        <JsonProperty>
        Public Property SitioDevolucionContenedor As SitiosCargueDescargue
        <JsonProperty>
        Public Property FechaDevolucionContenedor As DateTime
        <JsonProperty>
        Public Property ModoTransporteUsado As ValorCatalogos
        <JsonProperty>
        Public Property ValorFOB As Double
        <JsonProperty>
        Public Property NumeroOrdenCargue As Long
        <JsonProperty>
        Public Property NumeroRemesa As Long
        <JsonProperty>
        Public Property NumeroManifiesto As Long
        <JsonProperty>
        Public Property NumeroDocumentoOrdenCargue As Long
        <JsonProperty>
        Public Property NumeroDocumentoRemesa As Long
        <JsonProperty>
        Public Property NumeroDocumentoPlanillaDespacho As Long
        <JsonProperty>
        Public Property NumeroPlanillaDespacho As Long
        <JsonProperty>
        Public Property NumeroDocumentoManifiesto As Long
        <JsonProperty>
        Public Property NombreRuta As String
        <JsonProperty>
        Public Property NombreProducto As String
        <JsonProperty>
        Public Property FleteCliente As Double
        <JsonProperty>
        Public Property ValorFleteCliente As Double
        <JsonProperty>
        Public Property FleteTransportador As Double
        <JsonProperty>
        Public Property ValorFleteTransportador As Double
        <JsonProperty>
        Public Property ValorEscolta As Double
        <JsonProperty>
        Public Property AplicaEscolta As Byte
        <JsonProperty>
        Public Property CodigoTipoRemesa As Integer
        <JsonProperty>
        Public Property NombreTipoRemesa As String
        <JsonProperty>
        Public Property LineaNegocioTransporteCarga As LineaNegocioTransporteCarga
        <JsonProperty>
        Public Property LineaTipoNegocioTransporteCarga As LineaNegocioTransporteCarga
        <JsonProperty>
        Public Property ValorCargueTransportador As Double
        <JsonProperty>
        Public Property ValorDescargueTransportador As Double
        <JsonProperty>
        Public Property FechaCargue As Date
        <JsonProperty>
        Public Property CiudadDevolucion As Ciudades

        <JsonProperty>
        Public Property PatioDevolucion As String
        <JsonProperty>
        Public Property FechaDevolucion As Date
        <JsonProperty>
        Public Property PermisoInvias As String
    End Class
End Namespace

