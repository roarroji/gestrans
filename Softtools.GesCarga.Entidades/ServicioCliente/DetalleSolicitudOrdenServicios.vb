﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.ServicioCliente
Imports Softtools.GesCarga.Entidades.Comercial.Documentos

Namespace ServicioCliente

    ''' <summary>
    ''' Clase <see cref=" DetalleSolicitudOrdenServicios"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleSolicitudOrdenServicios
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleSolicitudOrdenServicios"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleSolicitudOrdenServicios"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroDocumento = Read(lector, "ESOS_Numero")
            Codigo = Read(lector, "ID")
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "Ruta")}
            Tarifa = New TarifaTransporteCarga With {.Codigo = Read(lector, "TATC_Codigo"), .Nombre = Read(lector, "TarifaTrasnporteCarga")}
            TipoTarifa = New TipoTarifaTransporteCarga With {.Codigo = Read(lector, "TTTC_Codigo"), .Nombre = Read(lector, "TipoTrifaTransporte")}
            TipoLineaNegocioCarga = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo"), .Nombre = Read(lector, "TipoLineaNegocioCarga")}
            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "Producto")}
            Cantidad = Read(lector, "Cantidad")
            Peso = Read(lector, "Peso")
            FleteCliente = Read(lector, "Flete_Cliente")
            ValorFleteCliente = Read(lector, "Valor_Flete_Cliente")
            DocumentoCliente = Read(lector, "Documento_Cliente")
            CiudadCargue = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Cargue"), .Nombre = Read(lector, "CiudadCargue")}
            FechaCargue = Read(lector, "Fecha_Cargue")
            SitioCargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Cargue"), .Nombre = Read(lector, "SICD_Nombre_Cargue")}
            DireccionCargue = Read(lector, "Direccion_Cargue")
            TelefonoCargue = Read(lector, "Telefono_Cargue")
            ContactoCargue = Read(lector, "Contacto_Cargue")
            Destinatario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Destinatario"), .NombreCompleto = Read(lector, "Destinatario")}
            FechaDescargue = Read(lector, "Fecha_Descargue")
            SitioDescargue = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Descargue"), .Nombre = Read(lector, "SICD_Nombre_Descargue")}
            CiudadDestino = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Destino"), .Nombre = Read(lector, "CiudadDescargue")}
            DireccionDestino = Read(lector, "Direccion_Destino")
            TelefonoDestino = Read(lector, "Telefono_Destino")
            ContactoDestino = Read(lector, "Contacto_Destino")
            NumeroContenedor = Read(lector, "Numero_Contenedor")
            MBLContenedor = Read(lector, "MBL_Contenedor")
            HBLContenedor = Read(lector, "HBL_Contenedor")
            DOContenedor = Read(lector, "DO_Contenedor")
            ValorFOB = Read(lector, "Valor_FOB")
            ValorFleteTransportador = Read(lector, "Valor_Flete_Transportador")
            SitioDevolucionContenedor = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Devolucion_Contenedor"), .Nombre = Read(lector, "SitioDevolucion")}
            FechaDevolucionContenedor = Read(lector, "Fecha_Devolucion_Contenedor")
            ModoTransporte = New ValorCatalogos With {.Codigo = Read(lector, "CATA_MOTR_Codigo"), .Nombre = Read(lector, "ModoTransporte")}
            PermisoInvias = Read(lector, "Permiso_Invias")
            CiudadDevolucion = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Devolucion_Contenedor"), .NombreCompleto = Read(lector, "Nombre_Ciudad_Devolucion")}
            PatioDevolucion = New SitiosCargueDescargue With {.Nombre = Read(lector, "Patio_Devolucion")}

        End Sub
        <JsonProperty>
        Public Property PatioDevolucion As SitiosCargueDescargue
        <JsonProperty>
        Public Property CiudadDevolucion As Ciudades
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property Tarifa As TarifaTransporteCarga
        <JsonProperty>
        Public Property TipoTarifa As TipoTarifaTransporteCarga
        <JsonProperty>
        Public Property Producto As ProductoTransportados
        <JsonProperty>
        Public Property TarifarioVentas As TarifarioVentas
        <JsonProperty>
        Public Property LineaNegocioCarga As LineaNegocioTransporteCarga
        <JsonProperty>
        Public Property TipoLineaNegocioCarga As TipoLineaNegocioTransportes
        <JsonProperty>
        Public Property Cantidad As Double
        <JsonProperty>
        Public Property Peso As Double
        <JsonProperty>
        Public Property FleteCliente As Double
        <JsonProperty>
        Public Property ValorFleteCliente As Double
        <JsonProperty>
        Public Property ValorFleteTransportador As Double
        <JsonProperty>
        Public Property DocumentoCliente As String
        <JsonProperty>
        Public Property CiudadCargue As Ciudades
        <JsonProperty>
        Public Property FechaCargue As DateTime
        <JsonProperty>
        Public Property SitioCargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property DireccionCargue As String
        <JsonProperty>
        Public Property TelefonoCargue As String
        <JsonProperty>
        Public Property ContactoCargue As String
        <JsonProperty>
        Public Property Destinatario As Terceros
        <JsonProperty>
        Public Property CiudadDestino As Ciudades
        <JsonProperty>
        Public Property FechaDescargue As DateTime
        <JsonProperty>
        Public Property SitioDescargue As SitiosCargueDescargue
        <JsonProperty>
        Public Property DireccionDestino As String
        <JsonProperty>
        Public Property TelefonoDestino As String
        <JsonProperty>
        Public Property ContactoDestino As String
        <JsonProperty>
        Public Property NumeroContenedor As String
        <JsonProperty>
        Public Property MBLContenedor As String
        <JsonProperty>
        Public Property HBLContenedor As String
        <JsonProperty>
        Public Property DOContenedor As String
        <JsonProperty>
        Public Property SitioDevolucionContenedor As SitiosCargueDescargue
        <JsonProperty>
        Public Property FechaDevolucionContenedor As DateTime
        <JsonProperty>
        Public Property ModoTransporte As ValorCatalogos
        <JsonProperty>
        Public Property ValorFOB As Double
        <JsonProperty>
        Public Property PermisoInvias As String
    End Class
End Namespace
