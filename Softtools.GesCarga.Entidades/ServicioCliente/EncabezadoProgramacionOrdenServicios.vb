﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.Operacion

Namespace ServicioCliente

    ''' <summary>
    ''' Clase <see cref=" EncabezadoProgramacionOrdenServicios"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EncabezadoProgramacionOrdenServicios
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoProgramacionOrdenServicios"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoProgramacionOrdenServicios"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            Fecha = Read(lector, "Fecha")
            OrdenServicio = New EncabezadoSolicitudOrdenServicios With {.Numero = Read(lector, "ESOS_Numero"), .NumeroDocumento = Read(lector, "NumeroOrden"), .Fecha = Read(lector, "FechaOrden")}
            Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .Nombre = Read(lector, "NombreCliente")}
            Observaciones = Read(lector, "Observaciones")
            EstadoProgramacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_EPOS_Codigo")}
            Ruta = New Rutas With {.Nombre = Read(lector, "Ruta")}
            Producto = Read(lector, "Producto")
            SitioCargue = New SitiosTerceroCliente With {.Nombre = Read(lector, "SitioCargue")}
            SitioDescargue = New SitiosTerceroCliente With {.Nombre = Read(lector, "SitioDescargue")}
            TotalRegistros = Read(lector, "TotalRegistros")
            FechaModifica = Read(lector, "Fecha_Modifica")
            UsuarioModifica = New Usuarios With {.Codigo = Read(lector, "Usuario_Modifica"), .Nombre = Read(lector, "Nombre_Usuario")}
        End Sub
        <JsonProperty>
        Public Property OrdenServicio As EncabezadoSolicitudOrdenServicios
        <JsonProperty>
        Public Property DireccionCargue As String
        <JsonProperty>
        Public Property TelefonoCargue As String
        <JsonProperty>
        Public Property ContactoCargue As String
        <JsonProperty>
        Public Property ValorTipoDespacho As Double
        <JsonProperty>
        Public Property SaldoTipoDespacho As Double
        <JsonProperty>
        Public Property SitioCargue As SitiosTerceroCliente
        <JsonProperty>
        Public Property SitioDescargue As SitiosTerceroCliente
        <JsonProperty>
        Public Property DireccionDescargue As String
        <JsonProperty>
        Public Property TelefonoDescargue As String
        <JsonProperty>
        Public Property ContactoDescargue As String
        <JsonProperty>
        Public Property Producto As String
        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property EstadoProgramacion As ValorCatalogos
        <JsonProperty>
        Public Property Detalle As IEnumerable(Of DetalleProgramacionOrdenServicios)
        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property NumeroOrden As Integer
        <JsonProperty>
        Public Property IDdetalle As Integer
        <JsonProperty>
        Public Property ConsultaDetalle As Integer
        <JsonProperty>
        Public Property ID_Detalle As Integer
        <JsonProperty>
        Public Property ConsultaInicial As Integer

    End Class
End Namespace
