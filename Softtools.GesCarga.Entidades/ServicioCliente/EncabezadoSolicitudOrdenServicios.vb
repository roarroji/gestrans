﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Entidades.Basico.ServicioCliente
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.Operacion

Namespace ServicioCliente

    ''' <summary>
    ''' Clase <see cref=" EncabezadoSolicitudOrdenServicios"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EncabezadoSolicitudOrdenServicios
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoSolicitudOrdenServicios"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EncabezadoSolicitudOrdenServicios"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader, ByVal Optional DespachoSolicitud As Boolean = False)

            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            NumeroDocumento = Read(lector, "Numero_Documento")
            Fecha = Read(lector, "Fecha")

            EstadoOrden = New ValorCatalogos With {.Codigo = Read(lector, "CATA_ESSO_Codigo")}
            Obtener = Read(lector, "Obtener")
            If Obtener = 0 Then

                If DespachoSolicitud = True Then

                    CodigoCliente = Read(lector, "TERC_Codigo_Cliente")
                    NombreCliente = Read(lector, "NombreCliente")
                    Observaciones = Read(lector, "Observaciones")

                    Estado = Read(lector, "Estado")
                    Anulado = Read(lector, "Anulado")
                    CodigoUsuarioCrea = Read(lector, "USUA_Codigo_Crea")
                    NombreUsuarioCrea = Read(lector, "USUA_Crea_Nombre")
                    CodigoEstadoSolicitud = Read(lector, "CATA_ESSO_Codigo")
                    OficinaDespacha = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Despacha")}

                    NumeroTarifario = Read(lector, "ETCV_Numero")
                    TipoTarifaVenta = New TipoTarifaTransporteCarga With {.Codigo = Read(lector, "TTTC_Codigo_Venta"), .Nombre = Read(lector, "NombreTipoTarifaVenta")}
                    TarifaVenta = New TarifaTransporteCarga With {.Codigo = Read(lector, "TATC_Codigo_Venta"), .Nombre = Read(lector, "NombreTarifaVenta")}


                    IDRegistroDetalle = Read(lector, "ID_Registro_Detalle")
                    Cantidad = Read(lector, "Cantidad")
                    Peso = Read(lector, "Peso")
                    DocumentoCliente = Read(lector, "Documento_Cliente")
                    DireccionCargue = Read(lector, "Direccion_Cargue")
                    Destinatario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Destinatario"), .NombreCompleto = Read(lector, "NombreDestinatario")}
                    NumeroOrdenCargue = Read(lector, "ENOC_Numero")
                    NumeroManifiesto = Read(lector, "ENMC_Numero")
                    NumeroDocumentoManifiesto = Read(lector, "ENMC_Numero_Documento")
                    NumeroRemesa = Read(lector, "ENRE_Numero")
                    NumeroPlanilla = Read(lector, "ENPD_Numero")

                    NumeroDocumentoOrdenCargue = Read(lector, "ENOC_Numero_Documento")
                    NumeroDocumentoRemesa = Read(lector, "ENRE_Numero_Documento")
                    NumeroDocumentoPlanilla = Read(lector, "ENPD_Numero_Documento")

                    LineaNegocio = New LineaNegocioTransporteCarga With {.Codigo = Read(lector, "LNTC_Codigo"), .Nombre = Read(lector, "NombreLineaNegocio")}
                    TipoLineaNegocio = New TipoLineaNegocioCarga With {.Codigo = Read(lector, "TLNC_Codigo"), .Nombre = Read(lector, "NombreTipoLineaNegocio")}
                    CiudadCargue = New Ciudades With {.Nombre = Read(lector, "CiudadCargue")}
                    PlacaVehiculo = Read(lector, "Placa")

                    TotalRegistros = Read(lector, "TotalRegistros")
                    RegistrosPagina = Read(lector, "RegistrosPagina")
                Else

                    TarifarioVentas = New TarifarioVentas With {.Codigo = Read(lector, "ETCV_Numero"), .Nombre = Read(lector, "NombreTarifario")}
                    LineaNegocioCarga = New LineaNegocioTransporteCarga With {.Codigo = Read(lector, "LNTC_Codigo"), .Nombre = Read(lector, "LineaNegocioTransporteCarga")}
                    TipoLineaNegocioCarga = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo"), .Nombre = Read(lector, "TipoLineaNegocioCarga")}
                    Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .Nombre = Read(lector, "NombreCliente")}
                    Facturar = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Facturar"), .Nombre = Read(lector, "NombreFacturar")}
                    DocumentoCliente = Read(lector, "Documento_Cliente")
                    Comercial = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Comercial"), .Nombre = Read(lector, "NombreComercial")}
                    FacturarDespachar = Read(lector, "Facturar_Despachar")
                    FacturarCantidadPeso = Read(lector, "Facturar_Cantidad_Peso_Cumplido")
                    Destinatario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Destinatario"), .Nombre = Read(lector, "NombreDestinatario")}
                    FechaCargue = Read(lector, "Fecha_Cargue")
                    CiudadCargue = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Cargue"), .Nombre = Read(lector, "Ciudad")}
                    DireccionCargue = Read(lector, "Direccion_Cargue")
                    TelefonoCargue = Read(lector, "Telefono_Cargue")
                    ContactoCargue = Read(lector, "Contacto_Cargue")
                    OficinaDespacha = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Despacha")}
                    Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Despacha"), .Nombre = Read(lector, "Oficina")}
                    Observaciones = Read(lector, "Observaciones")
                    AplicaPoliza = Read(lector, "Aplica_Poliza")
                    Estado = Read(lector, "Estado")
                    Anulado = Read(lector, "Anulado")
                    TotalRegistros = Read(lector, "TotalRegistros")
                    UsuarioCrea = New Usuarios With {.Codigo = Read(lector, "USUA_Codigo_Crea"), .Nombre = Read(lector, "USUA_Crea_Nombre")}
                    CodigoEstadoSolicitud = Read(lector, "CATA_ESSO_Codigo")
                    'SolicitudOrdenNumero = Read(lector, "ESOS_Numero")
                End If


            Else
                TarifarioVentas = New TarifarioVentas With {.Codigo = Read(lector, "ETCV_Numero"), .Nombre = Read(lector, "NombreTarifario")}
                LineaNegocioCarga = New LineaNegocioTransporteCarga With {.Codigo = Read(lector, "LNTC_Codigo"), .Nombre = Read(lector, "LineaNegocioTransporteCarga")}
                TipoLineaNegocioCarga = New TipoLineaNegocioTransportes With {.Codigo = Read(lector, "TLNC_Codigo"), .Nombre = Read(lector, "TipoLineaNegocioCarga")}
                Cliente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Cliente"), .Nombre = Read(lector, "NombreCliente"), .Telefonos = Read(lector, "TelefonosCliente"), .Celular = Read(lector, "CelularesCliente"), .Direccion = lector("DireccionCliente")}

                Cliente.Ciudad = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Cliente"), .NombreCompleto = Read(lector, "NombreCiudadCliente")}
                CodigoEstadoSolicitud = Read(lector, "CATA_ESSO_Codigo")
                Facturar = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Facturar")}
                DocumentoCliente = Read(lector, "Documento_Cliente")
                Comercial = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Comercial")}
                FacturarDespachar = Read(lector, "Facturar_Despachar")
                FacturarCantidadPeso = Read(lector, "Facturar_Cantidad_Peso_Cumplido")
                Destinatario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Destinatario")}
                FechaCargue = Read(lector, "Fecha_Cargue")
                CiudadCargue = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Cargue")}
                DireccionCargue = Read(lector, "Direccion_Cargue")
                TelefonoCargue = Read(lector, "Telefono_Cargue")
                ContactoCargue = Read(lector, "Contacto_Cargue")
                OficinaDespacha = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Despacha")}
                Oficina = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "Oficina")}
                Observaciones = Read(lector, "Observaciones")
                AplicaPoliza = Read(lector, "Aplica_Poliza")
                Estado = Read(lector, "Estado")
                Anulado = Read(lector, "Anulado")

            End If
            Producto = New ProductoTransportados With {.Codigo = Read(lector, "PRTR_Codigo"), .Nombre = Read(lector, "NombreProducto")}
            Ruta = New Rutas With {.Codigo = Read(lector, "RUTA_Codigo"), .Nombre = Read(lector, "NombreRuta")}
            FleteXCantidad = Read(lector, "Aplica_Recalculo")
            TipoDespacho = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TDOS_Codigo"), .Nombre = Read(lector, "Tipo_Despacho")}
            ValorTipoDespacho = Read(lector, "Valor_Tipo_Despacho")
            SaldoTipoDespacho = Read(lector, "Saldo_Tipo_Despacho")
            SitioCargue = New SitiosTerceroCliente With {.SitioCliente = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Cargue"), .Nombre = Read(lector, "SitioCargue")}}
            SitioDescargue = New SitiosTerceroCliente With {.SitioCliente = New SitiosCargueDescargue With {.Codigo = Read(lector, "SICD_Codigo_Descargue"), .Nombre = Read(lector, "SitioDescargue")}}
            DireccionDescargue = Read(lector, "Direccion_Descargue")
            TelefonoDescargue = Read(lector, "Telefono_Descargue")
            ContactoDescargue = Read(lector, "Contacto_Descargue")

            ValorFleteCliente = Read(lector, "Valor_Flete_Cliente")
            ValorFleteTrnasportador = Read(lector, "Valor_Flete_Transportador")
            ValorCargue = Read(lector, "Valor_Cargue")
            ValorDescargue = Read(lector, "Valor_Descargue")
            ValorCargueTransportador = Read(lector, "Valor_Cargue_Transportador")
            ValorDescargueTransportador = Read(lector, "Valor_Descargue_Transportador")
            Remitente = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Remitente")}
            Tarifa = New TarifaTransporteCarga With {.Codigo = Read(lector, "TATC_Codigo"), .Nombre = Read(lector, "NombreTarifa")}
            CiudadDescargue = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Descargue")}


            FechaFinCargue = Read(lector, "Fecha_Fin_Cargue")
            HorasCargue = Read(lector, "Horas_Pactadas_Cargue")
            HorasDescargue = Read(lector, "Horas_Pactadas_Descargue")
            InformacionCompleta = Read(lector, "Informacion_Completa")
            UnidadEmpaque = New UnidadEmpaque With {.Codigo = Read(lector, "UEPT_Codigo"), .Nombre = Read(lector, "UnidadEmpaque")}
            NovedadCalidad = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NCOS_Codigo"), .Nombre = Read(lector, "NovedeadCalidad")}
            SedeFacturacion = New TerceroClienteCupoSedes With {.Codigo = Read(lector, "TEDI_Codigo")}
            TipoCargue = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TCOS_Codigo"), .Nombre = Read(lector, "TipoCargue")}
            TipoVehiculo = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TIVE_Codigo"), .Nombre = Read(lector, "TipoVehiculo")}
            ValorDeclarado = Read(lector, "Valor_Declarado")

            CupoCliente = Read(lector, "CupoCliente")
            DisponibleERP = Read(lector, "DisponibleERP")
            PedidosPorDespachar = Read(lector, "PedidosPorDespachar")
            RemesasPorFacturar = Read(lector, "RemesasPorFacturar")
            DiferentesSitiosDescargue = Read(lector, "Diferentes_Sitios_Descargue")
            NumeroProgramacion = Read(lector, "NumeroProgramacion")
            PermisoInvias = Read(lector, "Permiso_Invias")
            FormaPago = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FPVE_Codigo"), .Nombre = Read(lector, "CATA_FPVE_Nombre")}
        End Sub
        <JsonProperty>
        Public Property TarifarioVentas As TarifarioVentas
        <JsonProperty>
        Public Property LineaNegocioCarga As LineaNegocioTransporteCarga
        <JsonProperty>
        Public Property TipoLineaNegocioCarga As TipoLineaNegocioTransportes

        <JsonProperty>
        Public Property Cliente As Terceros
        <JsonProperty>
        Public Property Facturar As Terceros
        <JsonProperty>
        Public Property Comercial As Terceros
        <JsonProperty>
        Public Property DocumentoCliente As String

        <JsonProperty>
        Public Property FacturarDespachar As Integer

        <JsonProperty>
        Public Property FacturarCantidadPeso As Integer

        <JsonProperty>
        Public Property Destinatario As Terceros
        <JsonProperty>
        Public Property Remitente As Terceros
        <JsonProperty>
        Public Property EstadoOrden As ValorCatalogos

        <JsonProperty>
        Public Property FechaCargue As DateTime

        <JsonProperty>
        Public Property CiudadCargue As Ciudades
        <JsonProperty>
        Public Property CiudadDescargue As Ciudades
        <JsonProperty>
        Public Property DireccionCargue As String

        <JsonProperty>
        Public Property TelefonoCargue As String

        <JsonProperty>
        Public Property ContactoCargue As String

        <JsonProperty>
        Public Property OficinaDespacha As Oficinas

        <JsonProperty>
        Public Property AplicaPoliza As Integer

        <JsonProperty>
        Public Property Detalle As IEnumerable(Of DetalleSolicitudOrdenServicios)
        <JsonProperty>
        Public Property DetalleDespacho As IEnumerable(Of DetalleDespachoSolicitudOrdenServicios)
        <JsonProperty>
        Public Property Conceptos As IEnumerable(Of DetalleConceptoSolicitudOrdenServicios)
        <JsonProperty>
        Public Property Polizas As IEnumerable(Of DetallePolizasSolicitudOrdenServicios)
        <JsonProperty>
        Public Property ImportacionExportacion As ImportacionExportacionSolicitudOrdenServicios
        <JsonProperty>
        Public Property DesdeDespachoOrdenServicio As Boolean
        <JsonProperty>
        Public Property CodigoEstadoSolicitud As Integer
        <JsonProperty>
        Public Property DesdeDespachoSolicitud As Boolean
        <JsonProperty>
        Public Property CodigoCliente As Long
        <JsonProperty>
        Public Property NombreCliente As String
        <JsonProperty>
        Public Property CodigoUsuarioCrea As Integer
        <JsonProperty>
        Public Property NombreUsuarioCrea As String
        <JsonProperty>
        Public Property ConsularDocumentos As Short
        <JsonProperty>
        Public Property Documentos As IEnumerable(Of GestionDocumentalDocumentos)
        <JsonProperty>
        Public Property ListadoDocumentos As IEnumerable(Of GestionDocumentalDocumentos)
        <JsonProperty>
        Public Property EstadoSolicitud As Integer
        <JsonProperty>
        Public Property PlacaVehiculo As String
        <JsonProperty>
        Public Property DocumentosRelacionados As IEnumerable(Of EncabezadoDocumentos)
        <JsonProperty>
        Public Property PendienteGenerar As Integer
        <JsonProperty>
        Public Property SolicitudOrdenServicioNumero As Integer
        <JsonProperty>
        Public Property TipoDespacho As ValorCatalogos
        <JsonProperty>
        Public Property ValorTipoDespacho As Double
        <JsonProperty>
        Public Property SaldoTipoDespacho As Double
        <JsonProperty>
        Public Property InsertaDetalle As Short
        <JsonProperty>
        Public Property ConsultaInicial As Short
        <JsonProperty>
        Public Property FleteXCantidad As Short
        <JsonProperty>
        Public Property Tarifa As TarifaTransporteCarga
        <JsonProperty>
        Public Property SitioCargue As SitiosTerceroCliente
        <JsonProperty>
        Public Property SitioDescargue As SitiosTerceroCliente
        <JsonProperty>
        Public Property DireccionDescargue As String
        <JsonProperty>
        Public Property TelefonoDescargue As String
        <JsonProperty>
        Public Property ContactoDescargue As String
        <JsonProperty>
        Public Property ValorFleteCliente As Double
        <JsonProperty>
        Public Property ValorFleteTrnasportador As Double
        <JsonProperty>
        Public Property ValorCargue As Double
        <JsonProperty>
        Public Property ValorDescargue As Double
        <JsonProperty>
        Public Property ValorCargueTransportador As Double
        <JsonProperty>
        Public Property ValorDescargueTransportador As Double
        <JsonProperty>
        Public Property DiferentesSitiosDescargue As Short
        <JsonProperty>
        Public Property NumeroProgramacion As Integer
        <JsonProperty>
        Public Property PermisoInvias As String
        <JsonProperty>
        Public Property FormaPago As ValorCatalogos

#Region "Campos detalle despacho"

        <JsonProperty>
        Public Property Ruta As Rutas
        <JsonProperty>
        Public Property NumeroTarifario As Long
        <JsonProperty>
        Public Property TipoTarifaVenta As TipoTarifaTransporteCarga
        <JsonProperty>
        Public Property TarifaVenta As TarifaTransporteCarga
        <JsonProperty>
        Public Property Producto As ProductoTransportados
        <JsonProperty>
        Public Property Cantidad As Integer
        <JsonProperty>
        Public Property Peso As Double
        <JsonProperty>
        Public Property NumeroOrdenCargue As Long
        <JsonProperty>
        Public Property NumeroRemesa As Long
        <JsonProperty>
        Public Property NumeroPlanilla As Long
        <JsonProperty>
        Public Property NumeroDocumentoOrdenCargue As Long
        <JsonProperty>
        Public Property NumeroDocumentoRemesa As Long
        <JsonProperty>
        Public Property NumeroDocumentoPlanilla As Long
        <JsonProperty>
        Public Property IDRegistroDetalle As Long
        <JsonProperty>
        Public Property NumeroManifiesto As Long
        <JsonProperty>
        Public Property NumeroDocumentoManifiesto As Long
        <JsonProperty>
        Public Property LineaNegocio As LineaNegocioTransporteCarga
        <JsonProperty>
        Public Property TipoLineaNegocio As TipoLineaNegocioCarga
        <JsonProperty>
        Public Property FechaFinCargue As Date
        <JsonProperty>
        Public Property HorasCargue As Integer
        <JsonProperty>
        Public Property HorasDescargue As Integer
        <JsonProperty>
        Public Property UnidadEmpaque As UnidadEmpaque
        <JsonProperty>
        Public Property InformacionCompleta As Integer
        <JsonProperty>
        Public Property NovedadCalidad As ValorCatalogos
        <JsonProperty>
        Public Property SedeFacturacion As TerceroClienteCupoSedes
        <JsonProperty>
        Public Property TipoCargue As ValorCatalogos
        <JsonProperty>
        Public Property TipoVehiculo As ValorCatalogos

        <JsonProperty>
        Public Property ValorDeclarado As Double
#End Region

#Region "Campos Control Cupo"
        <JsonProperty>
        Public Property CupoCliente As Double
        <JsonProperty>
        Public Property DisponibleERP As Double
        <JsonProperty>
        Public Property PedidosPorDespachar As Double
        <JsonProperty>
        Public Property RemesasPorFacturar As Double
#End Region
    End Class
End Namespace
