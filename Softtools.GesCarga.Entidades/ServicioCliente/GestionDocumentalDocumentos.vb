﻿Imports Newtonsoft.Json

Namespace ServicioCliente

    ''' <summary>
    ''' Clase <see cref="GestionDocumentalDocumentos"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class GestionDocumentalDocumentos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="GestionDocumentalDocumentos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="GestionDocumentalDocumentos"/>
        ''' </summary>
        ''' <param name="lector">Objeto datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Numero = Read(lector, "Numero")
            TIDOCodigo = Read(lector, "TIDO_Codigo")
            CDGDCodigo = Read(lector, "CDGD_Codigo")
            Referencia = Read(lector, "Referencia")
            Emisor = Read(lector, "Emisor")
            FechaEmision = Read(lector, "Fecha_Emision")
            FechaVence = Read(lector, "Fecha_Vence")
            NombreDocumento = Read(lector, "Nombre_Documento")
            Archivo = Read(lector, "Archivo")
            Extension = Read(lector, "Extension_Documento")

        End Sub

        <JsonProperty>
        Public Property TIDOCodigo As Integer
        <JsonProperty>
        Public Property CDGDCodigo As Integer
        <JsonProperty>
        Public Property Referencia As String
        <JsonProperty>
        Public Property Emisor As String
        <JsonProperty>
        Public Property FechaEmision As Date
        <JsonProperty>
        Public Property FechaVence As Date
        <JsonProperty>
        Public Property Archivo As Byte()
        <JsonProperty>
        Public Property NombreDocumento As String
        <JsonProperty>
        Public Property Extension As String

    End Class
End Namespace
