﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace ServicioCliente

    ''' <summary>
    ''' Clase <see cref=" ImportacionExportacionSolicitudOrdenServicios"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class ImportacionExportacionSolicitudOrdenServicios
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ImportacionExportacionSolicitudOrdenServicios"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="ImportacionExportacionSolicitudOrdenServicios"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            NumeroDocumento = Read(lector, "ESOS_Numero")
            Naviera = New ValorCatalogos With {.Codigo = Read(lector, "CATA_NAVI_Codigo"), .Nombre = Read(lector, "Navi")}
            ETAMotonave = Read(lector, "ETA_Motonave")
            Motonave = Read(lector, "Motonave")
            FechaDTA = Read(lector, "Fecha_DTA")
            PaisOrigen = New Paises With {.Codigo = Read(lector, "PAIS_Codigo_Origen"), .Nombre = Read(lector, "PaisOrigen")}
            PuertoPaisOrigen = New PuertosPaises With {.Codigo = Read(lector, "PUPA_Codigo_Origen"), .Nombre = Read(lector, "PuertoPaisOrigen")}
            PaisDestino = New Paises With {.Codigo = Read(lector, "PAIS_Codigo_Destino"), .Nombre = Read(lector, "PaisDestino")}
            PuertoPaisDestino = New PuertosPaises With {.Codigo = Read(lector, "PUPA_Codigo_Destino"), .Nombre = Read(lector, "PuertoPaisDestino")}
            ContactoDestino = Read(lector, "Contacto_Puerto_Destino")
            ContactoOrigen = Read(lector, "Contacto_Puerto_Origen")
            LiberacionAutomaticaFletes = Read(lector, "Liberacion_Automatica_Fletes")
            ExoneracionPagoDeposito = Read(lector, "Exoneracion_Pago_Depositos")
            DiasLibresContenedor = Read(lector, "Dias_Libres_Contenedor")
            ExoneracionPagoDropOFF = Read(lector, "Exoneracion_Pago_Drop_Off")
            ClientePagaFleteMaritimoRuta = Read(lector, "Cliente_Paga_Flete_Maritimo")
            ClientePagaLiquidacionComodato = Read(lector, "Cliente_Paga_Emision_Manejo")
            ClientePagoEmisionManejo = Read(lector, "Cliente_Paga_Liquidacion_Comodato")
            FechaCutOffDocumental = Read(lector, "Fecha_Cut_Off_Documental")
            FechaCutOffFisico = Read(lector, "Fecha_Cut_Off_Fisico")

            BL = Read(lector, "BL")
            NumeroImportacion = Read(lector, "Numero_Importacion")
            DeclaracionImporacion = Read(lector, "Declaracion_Importacion")
            ClaseImportacion = Read(lector, "Clase_Importacion")
            ValorDeclarado = Read(lector, "Valor_Declarado")

            DevolucionContenedor = Read(lector, "Devolucion_Contenedor")
            CiudadDevolucion = New Ciudades With {.Codigo = Read(lector, "CIUD_Codigo_Devolucion_Contenedor")}
            PatioDevolucion = Read(lector, "Patio_Devolcuion")
            FechaDevolucion = Read(lector, "Fecha_Devolcuion")
            NumeroReserva = Read(lector, "Numero_Reserva")
            PatioCargue = Read(lector, "Patio_Cargue")
            FechaVencimiento = Read(lector, "Fecha_Vencimiento")

        End Sub

        <JsonProperty>
        Public Property Naviera As ValorCatalogos
        <JsonProperty>
        Public Property ETAMotonave As Date

        <JsonProperty>
        Public Property Motonave As String

        <JsonProperty>
        Public Property FechaDTA As Date

        <JsonProperty>
        Public Property PaisOrigen As Paises

        <JsonProperty>
        Public Property PuertoPaisOrigen As PuertosPaises

        <JsonProperty>
        Public Property PaisDestino As Paises

        <JsonProperty>
        Public Property PuertoPaisDestino As PuertosPaises

        <JsonProperty>
        Public Property ContactoOrigen As String

        <JsonProperty>
        Public Property ContactoDestino As String
        <JsonProperty>
        Public Property LiberacionAutomaticaFletes As Integer

        <JsonProperty>
        Public Property ExoneracionPagoDeposito As Integer

        <JsonProperty>
        Public Property DiasLibresContenedor As Integer

        <JsonProperty>
        Public Property ExoneracionPagoDropOFF As Integer

        <JsonProperty>
        Public Property ClientePagaFleteMaritimoRuta As Integer
        <JsonProperty>
        Public Property ClientePagoEmisionManejo As Integer

        <JsonProperty>
        Public Property ClientePagaLiquidacionComodato As Integer

        <JsonProperty>
        Public Property FechaCutOffDocumental As Date

        <JsonProperty>
        Public Property FechaCutOffFisico As Date

        <JsonProperty>
        Public Property Catalogo As ValorCatalogos

        <JsonProperty>
        Public Property BL As String
        <JsonProperty>
        Public Property NumeroImportacion As String
        <JsonProperty>
        Public Property DeclaracionImporacion As String
        <JsonProperty>
        Public Property ClaseImportacion As String
        <JsonProperty>
        Public Property ValorDeclarado As Double
        <JsonProperty>
        Public Property DevolucionContenedor As Integer

        <JsonProperty>
        Public Property CiudadDevolucion As Ciudades

        <JsonProperty>
        Public Property PatioDevolucion As String
        <JsonProperty>
        Public Property FechaDevolucion As Date
        <JsonProperty>
        Public Property NumeroReserva As String
        <JsonProperty>
        Public Property PatioCargue As String
        <JsonProperty>
        Public Property FechaVencimiento As Date
    End Class
End Namespace
