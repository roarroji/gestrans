﻿Imports Newtonsoft.Json

Namespace Tesoreria

    <JsonObject>
    Public NotInheritable Class CodigoRespuestaBancolombia
        Inherits BaseDocumento

        Sub New()
        End Sub

        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            CodigoRespuesta = Read(lector, "Codigo")
            Descripcion = Read(lector, "Descripcion")
        End Sub

        <JsonProperty>
        Public Property Descripcion As String
        <JsonProperty>
        Public Property CodigoRespuesta As String



    End Class

End Namespace
