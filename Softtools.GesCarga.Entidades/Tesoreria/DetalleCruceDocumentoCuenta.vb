﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Tesoreria

    <JsonObject>
    Public NotInheritable Class DetalleCruceDocumentoCuenta
        Inherits BaseDocumento

        Sub New()
        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            DocumentoCuenta = New EncabezadoDocumentoCuentas With {.Codigo = Read(lector, "ENDC_Codigo")}
            DocumentoOrigen = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOOR_Codigo")}
            NumeroDocumentoOrigen = Read(lector, "Documento_Origen")
            Valor = Read(lector, "Valor")
            FechaCrea = Read(lector, "Fecha_Crea")
            Anulado = Read(lector, "Anulado")

        End Sub

        <JsonProperty>
        Public Property DocumentoCuenta As EncabezadoDocumentoCuentas

        <JsonProperty>
        Public Property DocumentoOrigen As ValorCatalogos

        <JsonProperty>
        Public Property NumeroDocumentoOrigen As Long

        <JsonProperty>
        Public Property Valor As Double
    End Class

End Namespace
