﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Tesoreria


    <JsonObject>
    Public NotInheritable Class DetalleDocumentoCausaciones
        Inherits BaseDocumento

        Sub New()
        End Sub


        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            CuentaPUC = New PlanUnicoCuentas With {.Codigo = Read(lector, "PLUC_Codigo"), .Nombre = Read(lector, "NombreCuenta"), .CodigoCuenta = Read(lector, "CuentaPUC")}
            Tercero = New Tercero With {.Codigo = Read(lector, "TERC_Codigo"), .Nombre = Read(lector, "NombreTercero")}
            ValorBase = Read(lector, "Valor_Base")
            ValorCredito = Read(lector, "Valor_Credito")
            ValorDebito = Read(lector, "Valor_Debito")
            GeneraCuenta = Read(lector, "Genera_Cuenta")
            Observaciones = Read(lector, "Observaciones")
            DocumentoCruce = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOCR_Codigo"), .Nombre = Read(lector, "DocumentoCruze")}
            TerceroParametrizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TEPC_Codigo"), .Nombre = Read(lector, "TerceroParametrizacion")}
            CentroCostoParametrizacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CCPC_Codigo"), .Nombre = Read(lector, "CentroCostoParametrizacion")}
            NotaDetalle = Read(lector, "Nota_Detalle")
            Prefijo = Read(lector, "Prefijo")
            SufijoCodigoAnexo = Read(lector, "Sufijo")
            CampoAuxiliar = Read(lector, "Auxiliar")
            CodigoAnexo = Read(lector, "Codigo_Anexo")
            Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo")}
            CentroCosto = Read(lector, "Centro_Costo")


        End Sub

        <JsonProperty>
        Public Property CuentaPUC As PlanUnicoCuentas
        <JsonProperty>
        Public Property Tercero As Tercero
        <JsonProperty>
        Public Property ValorBase As Double
        <JsonProperty>
        Public Property ValorDebito As Double
        <JsonProperty>
        Public Property ValorCredito As Double
        <JsonProperty>
        Public Property GeneraCuenta As Integer
        <JsonProperty>
        Public Property Prefijo As String
        <JsonProperty>
        Public Property CodigoAnexo As String
        <JsonProperty>
        Public Property SufijoCodigoAnexo As String
        <JsonProperty>
        Public Property CampoAuxiliar As String
        <JsonProperty>
        Public Property DocumentoCruce As ValorCatalogos
        <JsonProperty>
        Public Property TerceroParametrizacion As ValorCatalogos
        <JsonProperty>
        Public Property CentroCostoParametrizacion As ValorCatalogos

        <JsonProperty>
        Public Property Vehiculo As Vehiculos

        <JsonProperty>
        Public Property CentroCosto As String

        <JsonProperty>
        Public Property NotaDetalle As String

    End Class

End Namespace
