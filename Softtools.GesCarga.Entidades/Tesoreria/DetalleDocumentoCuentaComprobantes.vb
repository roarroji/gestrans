﻿Imports Newtonsoft.Json

Namespace Tesoreria

    ''' <summary>
    ''' Clase <see cref="DetalleDocumentoCuentaComprobantes"/>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class DetalleDocumentoCuentaComprobantes
        Inherits BaseDocumento

        ''' <summary>
        ''' Incializa una nueva instancia de la clase <see cref="DetalleDocumentoCuentaComprobantes"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="DetalleDocumentoCuentaComprobantes"/>
        ''' </summary>
        ''' <param name="lector">Objeto Datareader</param>
        Sub New(lector As IDataReader)



        End Sub

        <JsonProperty>
        Public Property CodigoDocumentoCuenta As Integer
        <JsonProperty>
        Public Property CodigoDocumentoComprobante As Integer
        <JsonProperty>
        Public Property ValorPago As Double
    End Class

End Namespace
