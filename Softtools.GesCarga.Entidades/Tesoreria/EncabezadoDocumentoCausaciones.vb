﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Tesoreria


    <JsonObject>
    Public NotInheritable Class EncabezadoDocumentoCausaciones
        Inherits BaseDocumento


        Sub New()
        End Sub


        Sub New(lector As IDataReader)


            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Numero = Read(lector, "Numero")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            CodigoAlterno = Read(lector, "Codigo_Alterno")
            Fecha = Read(lector, "Fecha")
            Observaciones = Read(lector, "Observaciones")
            NumeroDocumentoOrigen = Read(lector, "Documento_Origen")
            Anulado = Read(lector, "Anulado")
            Estado = Read(lector, "Estado")
            Obtener = Read(lector, "Obtener")
            'CentroCostos = New ValorCatalogos With {.Codigo = Read(lector, "CATA_CCCE_Codigo")}
            OficinaCrea = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo"), .Nombre = Read(lector, "OficinaCrea")}
            OficinaDestino = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Destino"), .Nombre = Read(lector, "OficinaDestino")}
            Proveedor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo"), .NumeroIdentificacion = Read(lector, "IdentTercero"), .NombreCompleto = Read(lector, "NombreTercero")}
            NumeroFactura = Read(lector, "Numero_Factura")
            ValorTotal = Read(lector, "Total")
            FechaFactura = Read(lector, "Fecha_Factura")
            FechaVencimientoFactura = Read(lector, "Fecha_Vence_Factura")
            Subtotal = Read(lector, "Subtotal")
            ValorIva = Read(lector, "Valor_Iva")
            Debito = Read(lector, "Valor_Debito")
            Credito = Read(lector, "Valor_Credito")
            TipoComprobante = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TICC_Codigo")}
            DocumentoComprobante = Read(lector, "EDCO_Codigo")
            DocumentoOrigen = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOOR_Codigo"), .Nombre = Read(lector, "DocumentoOrigen")}


            'OficinaDestino = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Destino"), .Nombre = Read(lector, "OficinaDestino")}
            'Proveedor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo"), .NombreCompleto = Read(lector, "NombreTercero")}
            'Beneficiario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Beneficiario"), .Nombre = Read(lector, "NombreBeneficiario")}
            TotalRegistros = Read(lector, "TotalRegistros")
            'ValorPagoTotal = Read(lector, "Valor_Pago_Recaudo_Total")

            'DocumentoOrigen = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DOOR_Codigo")}
            'DestinoIngreso = New ValorCatalogos With {.Codigo = Read(lector, "CATA_DEIN_Codigo")}
            'FormaPagoDocumento = New ValorCatalogos With {.Codigo = Read(lector, "CATA_FPDC_Codigo")}
            'OficinaDestino = New Oficinas With {.Codigo = Read(lector, "OFIC_Codigo_Destino"), .Nombre = Read(lector, "OficinaDestino")}

            'Proveedor = New Terceros With {.Codigo = Read(lector, "TERC_Codigo"), .NumeroIdentificacion = Read(lector, "IdentTercero"), .NombreCompleto = Read(lector, "NombreTercero")}
            'Caja = New Cajas With {.Codigo = Read(lector, "CAJA_Codigo")}
            'CuentaBancaria = New CuentaBancarias With {.Codigo = Read(lector, "CUBA_Codigo")}
            'Beneficiario = New Terceros With {.Codigo = Read(lector, "TERC_Codigo_Beneficiario"), .NumeroIdentificacion = Read(lector, "IdentBeneficiario"), .NombreCompleto = Read(lector, "NombreBeneficiario")}
            'NumeroPagoRecaudo = Read(lector, "Numero_Pago_Recaudo")
            'FechaPagoRecaudo = Read(lector, "Fecha_Pago_Recaudo")
            'ValorPagocheque = Read(lector, "Valor_Pago_Recaudo_Cheque")
            'ValorPagoEfectivo = Read(lector, "Valor_Pago_Recaudo_Efectivo")
            'ValorPagoTransferencia = Read(lector, "Valor_Pago_Recaudo_Transferencia")
            'ValorPagoTotal = Read(lector, "Valor_Pago_Recaudo_Total")
            Numeracion = Read(lector, "Numeracion")
            'GeneraConsignacion = Read(lector, "Genero_Consignacion")
            'ValorAlterno = Read(lector, "Valor_Alterno")
            'ConceptoContable = Read(lector, "ECCO_Codigo")
            'Vehiculo = New Vehiculos With {.Codigo = Read(lector, "VEHI_Codigo")}
        End Sub


        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property Proveedor As Terceros
        <JsonProperty>
        Public Property Beneficiario As Terceros
        <JsonProperty>
        Public Property NumeroDocumentoOrigen As Integer
        <JsonProperty>
        Public Property Caja As Cajas
        <JsonProperty>
        Public Property CuentaBancaria As CuentaBancarias
        <JsonProperty>
        Public Property NumeroPagoRecaudo As Integer
        <JsonProperty>
        Public Property FechaPagoRecaudo As DateTime
        <JsonProperty>
        Public Property ValorPagoRecaudo As Double
        <JsonProperty>
        Public Property ValorPagoTransferencia As Double
        <JsonProperty>
        Public Property ValorPagoEfectivo As Double
        <JsonProperty>
        Public Property ValorPagocheque As Double
        <JsonProperty>
        Public Property ValorPagoTotal As Double
        <JsonProperty>
        Public Property Numeracion As String
        <JsonProperty>
        Public Property OficinaCrea As Oficinas
        <JsonProperty>
        Public Property OficinaDestino As Oficinas
        <JsonProperty>
        Public Property GeneraConsignacion As Short
        <JsonProperty>
        Public Property DocumentoOrigen As ValorCatalogos
        <JsonProperty>
        Public Property FormaPagoDocumento As ValorCatalogos
        <JsonProperty>
        Public Property DestinoIngreso As ValorCatalogos
        <JsonProperty>
        Public Property ValorAlterno As Double
        <JsonProperty>
        Public Property Fuente As String
        <JsonProperty>
        Public Property FuenteAnula As String
        <JsonProperty>
        Public Property ConceptoContable As Integer

        <JsonProperty>
        Public Property Vehiculo As Vehiculos
        <JsonProperty>
        Public Property Detalle As IEnumerable(Of DetalleDocumentoCausaciones)
        <JsonProperty>
        Public Property Cuentas As IEnumerable(Of EncabezadoDocumentoCuentas)
        <JsonProperty>
        Public Property DetalleCuentas As IEnumerable(Of DetalleDocumentoCuentaComprobantes)
        <JsonProperty>
        Public Property NombreTercero As String
        <JsonProperty>
        Public Property NombreBeneficiario As String
        <JsonProperty>
        Public Property Autorizacion As Short
        <JsonProperty>
        Public Property CodigoTercero As Integer
        <JsonProperty>
        Public Property CodigoLegalizacionGastos As Integer
        <JsonProperty>
        Public Property DocumentosRelacionados As IEnumerable(Of EncabezadoDocumentos)

        <JsonProperty>
        Public Property CentroCostos As ValorCatalogos

        <JsonProperty>
        Public Property CuentaPorPagar As EncabezadoDocumentoCuentas

        <JsonProperty>
        Public Property NumeroFactura As String

        <JsonProperty>
        Public Property FechaFactura As DateTime

        <JsonProperty>
        Public Property FechaVencimientoFactura As DateTime

        <JsonProperty>
        Public Property Subtotal As Double

        <JsonProperty>
        Public Property ValorIva As Double

        <JsonProperty>
        Public Property ValorTotal As Double

        <JsonProperty>
        Public Property Debito As Double

        <JsonProperty>
        Public Property Credito As Double

        <JsonProperty>
        Public Property TipoComprobante As ValorCatalogos

        <JsonProperty>
        Public Property DocumentoComprobante As Long
    End Class

End Namespace
