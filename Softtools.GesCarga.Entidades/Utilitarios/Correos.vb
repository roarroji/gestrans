﻿Imports System.Configuration
Imports Newtonsoft.Json
Imports System.Configuration.ConfigurationManager

Namespace Utilitarios
    <JsonObject>
    Public NotInheritable Class BaseCorreo
        Sub New()
        End Sub

        <JsonProperty>
        Public Property NombreCampo As String
        <JsonProperty>
        Public Property Valor As String

    End Class


    ''' <summary>
    ''' Clase <see cref="Correos"></see>
    ''' </summary>
    <JsonObject>
    Public Class Correos
        Inherits BaseDocumento
        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EventoCorreos"/>
        ''' </summary>
        ''' <param name="lector">Objeto DataReader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
        End Sub


        <JsonProperty>
        Public Property EventoCorreos As EventoCorreos
        <JsonProperty>
        Public Property DistribucionCorreos As ListaDistribucionCorreos
        <JsonProperty>
        Public Property CamposMensaje As IEnumerable(Of BaseCorreo)

        Public Const CampoDireccionDestino As String = "@DireccionDestino"
        Public Const CampoHoraFin As String = "@HoraFin"
        Public Const CampoFechaFin As String = "@FechaFin"
        Public Const CampoIdentificacionTercero As String = "@IdentificacionTercero"
        Public Const CampoTercero As String = "@Tercero"
        Public Const CampoUsuario As String = "@Usuario"
        Public Const CampoConductor As String = "@Conductor"
        Public Const CampoPlaca As String = "@Placa"
        Public Const CampoEstudioSeguridad As String = "@EstudioSeguridad"
        Public Const CampoSolicitudServicio As String = "@SolicitudServicio"
        Public Const CampoOrdenServicio As String = "@OrdenServicio"
        Public Const CampoCliente As String = "@Cliente"
        Public Const CampoLineaNegocio As String = "@LineaNegocio"
        Public Const CampoTipoLineaNegocio As String = "@TipoLineaNegocio"
        Public Const CampoTipoTarifa As String = "@TipoTarifa"
        Public Const CampoOrigen As String = "@Origen"
        Public Const CampoDestino As String = "@Destino"
        Public Const CampoFechaInicio As String = "@FechaInicio"
        Public Const CampoHoraInicio As String = "@HoraInicio"
        Public Const CampoDireccionOrigen As String = "@DireccionOrigen"
        Public Const CampoUnidades As String = "@Unidades"
        Public Const CampoPeso As String = "@Peso"
        Public Const CampoProducto As String = "@Producto"
        Public Const CampoProgramacion As String = "@Programacion"
        Public Const Planilla As String = "@Planilla"

        ''' <summary>
        ''' Cadena de conexión
        ''' </summary>
        Protected ConnectionString As String
        ''' <summary>
        ''' Cadena de conexión documentos
        ''' </summary>
        Protected ConnectionStringDocumentos As String

        Protected Sub New()
            Try
                ConnectionString = ConfigurationManager.ConnectionStrings("GestransCarga").ToString()
                ConnectionStringDocumentos = ConfigurationManager.ConnectionStrings("GestransCargaDocumentos").ToString()

            Catch
                Try
                    ConnectionString = AppSettings.Get("ConexionSQL")
                    ConnectionStringDocumentos = AppSettings.Get("ConexionSQLDocumental")
                Catch ex As Exception

                End Try
            End Try
        End Sub
    End Class

End Namespace