﻿
Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

Namespace Utilitarios

    <JsonObject>
    Public Class DetalleGrupoUsuariosNotificaciones
        Inherits BaseDocumento
        Sub New()

        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Grupo = New GrupoUsuarios With {.Codigo = Read(lector, "GRUS_Codigo")}
            Notificacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_PRNO_Codigo")}
            Consultar = Read(lector, "Consultar")
            Autorizar = Read(lector, "Autorizar")
        End Sub


        <JsonProperty>
        Public Property Grupo As GrupoUsuarios
        <JsonProperty>
        Public Property Notificacion As ValorCatalogos
        <JsonProperty>
        Public Property Consultar As Short
        <JsonProperty>
        Public Property Autorizar As Short
    End Class

End Namespace