﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

Namespace Utilitarios

    ''' <summary>
    ''' Clase <see cref="EventoCorreos"></see>
    ''' </summary>
    <JsonObject>
    Public NotInheritable Class EventoCorreos
        Inherits BaseDocumento

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EventoCorreos"/>
        ''' </summary>
        Sub New()
        End Sub

        ''' <summary>
        ''' Inicializa una nueva instancia de la clase <see cref="EventoCorreos"/>
        ''' </summary>
        ''' <param name="lector">Objeto DataReader</param>
        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Codigo = Read(lector, "Codigo")
            Nombre = Read(lector, "Nombre")
            TipoDocumento = Read(lector, "TIDO_Codigo")
            ModuloAplicacion = New ModuloAplicaciones With {.Codigo = Read(lector, "MOAP_Codigo"), .Nombre = Read(lector, "NombreModulo")}
            MenuAplicaciones = New MenuAplicaciones With {.Codigo = Read(lector, "MEAP_Codigo"), .Nombre = Read(lector, "NombreMenu")}
            TipoEventoCorreoGenera = New ValorCatalogos With {.Codigo = Read(lector, "CATA_TEGC_Codigo")}
            AsuntoCorreo = Read(lector, "Asunto_Correo")
            EncabezadoCorreo = Read(lector, "Encabezado_Correo")
            MensajeCorreo = Read(lector, "Mensaje_Correo")
            AdjuntarArchivo = Read(lector, "Adjuntar_Archivo_Correo")
            MensajeFirma = Read(lector, "Mensaje_Firma")
            Estado = Read(lector, "Estado")


        End Sub


        <JsonProperty>
        Public Property Nombre As String
        <JsonProperty>
        Public Property ModuloAplicacion As ModuloAplicaciones
        <JsonProperty>
        Public Property MenuAplicaciones As MenuAplicaciones
        <JsonProperty>
        Public Property TipoEventoCorreoGenera As ValorCatalogos
        <JsonProperty>
        Public Property AsuntoCorreo As String
        <JsonProperty>
        Public Property MensajeCorreo As String
        <JsonProperty>
        Public Property EncabezadoCorreo As String
        <JsonProperty>
        Public Property AdjuntarArchivo As String
        <JsonProperty>
        Public Property MensajeFirma As String
        <JsonProperty>
        Public Property ListaDistrubicionCorreos As IEnumerable(Of ListaDistribucionCorreos)
        <JsonProperty>
        Public Property CadenaCorreos As String
        <JsonProperty>
        Public Property Evento As EventoCorreos
        <JsonProperty>
        Public Property ConsultarEventoFuncionCorreo As Short

#Region "Constantes"
        ' Constantes Campos Correos
        Public Const CAMPO_TEXTO_CORREO_NUMERO_VIAJE As String = "[NumeroViaje]"
        Public Const CAMPO_TEXTO_CORREO_PLACA As String = "[Placa]"
        Public Const CAMPO_TEXTO_CORREO_FECHA As String = "[Fecha]"
        Public Const CAMPO_TEXTO_CORREO_PUNTO_GESTION As String = "[PuntoGestion]"

#End Region

    End Class

End Namespace