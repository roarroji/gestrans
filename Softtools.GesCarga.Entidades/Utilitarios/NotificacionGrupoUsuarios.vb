﻿Imports Newtonsoft.Json
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Utilitarios

    <JsonObject>
    Public Class NotificacionGrupoUsuarios
        Inherits BaseDocumento
        Sub New()

        End Sub

        Sub New(lector As IDataReader)
            CodigoEmpresa = Read(lector, "EMPR_Codigo")
            Notificacion = New ValorCatalogos With {.Codigo = Read(lector, "CATA_PRNO_Codigo")}

        End Sub

        <JsonProperty>
        Public Property Notificacion As ValorCatalogos
        <JsonProperty>
        Public Property ListaAsignaciones As IEnumerable(Of DetalleGrupoUsuariosNotificaciones)
    End Class

End Namespace
