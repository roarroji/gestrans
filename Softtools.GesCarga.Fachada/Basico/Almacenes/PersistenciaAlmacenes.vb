﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Almacen
Imports Softtools.GesCarga.Repositorio.Basico.Almacen

Namespace Basico.Almacen
    Public NotInheritable Class PersistenciaAlmacenes
        Implements IPersistenciaBase(Of Almacenes)

        Public Function Consultar(filtro As Almacenes) As IEnumerable(Of Almacenes) Implements IPersistenciaBase(Of Almacenes).Consultar
            Return New RepositorioAlmacenes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Almacenes) As Long Implements IPersistenciaBase(Of Almacenes).Insertar
            Return New RepositorioAlmacenes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Almacenes) As Long Implements IPersistenciaBase(Of Almacenes).Modificar
            Return New RepositorioAlmacenes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Almacenes) As Almacenes Implements IPersistenciaBase(Of Almacenes).Obtener
            Return New RepositorioAlmacenes().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Almacenes) As Boolean
            Return New RepositorioAlmacenes().Anular(entidad)
        End Function
    End Class

End Namespace

