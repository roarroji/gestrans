﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Almacen
Imports Softtools.GesCarga.Repositorio.Basico.Almacen

Namespace Basico.Almacen
    Public NotInheritable Class PersistenciaReferenciaAlmacenes
        Implements IPersistenciaBase(Of ReferenciaAlmacenes)

        Public Function Consultar(filtro As ReferenciaAlmacenes) As IEnumerable(Of ReferenciaAlmacenes) Implements IPersistenciaBase(Of ReferenciaAlmacenes).Consultar
            Return New RepositorioReferenciaAlmacenes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ReferenciaAlmacenes) As Long Implements IPersistenciaBase(Of ReferenciaAlmacenes).Insertar
            Return New RepositorioReferenciaAlmacenes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ReferenciaAlmacenes) As Long Implements IPersistenciaBase(Of ReferenciaAlmacenes).Modificar
            Return New RepositorioReferenciaAlmacenes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ReferenciaAlmacenes) As ReferenciaAlmacenes Implements IPersistenciaBase(Of ReferenciaAlmacenes).Obtener
            Return New RepositorioReferenciaAlmacenes().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ReferenciaAlmacenes) As Boolean
            Return New RepositorioReferenciaAlmacenes().Anular(entidad)
        End Function
    End Class

End Namespace

