﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Almacen
Imports Softtools.GesCarga.Repositorio.Basico.Almacen

Namespace Basico.Almacen
    Public NotInheritable Class PersistenciaReferenciaProveedores
        Implements IPersistenciaBase(Of ReferenciaProveedores)

        Public Function Consultar(filtro As ReferenciaProveedores) As IEnumerable(Of ReferenciaProveedores) Implements IPersistenciaBase(Of ReferenciaProveedores).Consultar
            Return New RepositorioReferenciaProveedores().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ReferenciaProveedores) As Long Implements IPersistenciaBase(Of ReferenciaProveedores).Insertar
            Return New RepositorioReferenciaProveedores().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ReferenciaProveedores) As Long Implements IPersistenciaBase(Of ReferenciaProveedores).Modificar
            Return New RepositorioReferenciaProveedores().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ReferenciaProveedores) As ReferenciaProveedores Implements IPersistenciaBase(Of ReferenciaProveedores).Obtener
            Return New RepositorioReferenciaProveedores().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ReferenciaProveedores) As Boolean
            Return New RepositorioReferenciaProveedores().Anular(entidad)
        End Function
    End Class

End Namespace

