﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Almacen
Imports Softtools.GesCarga.Repositorio.Basico.Almacen

Namespace Basico.Almacen
    Public NotInheritable Class PersistenciaReferencias
        Implements IPersistenciaBase(Of Referencias)

        Public Function Consultar(filtro As Referencias) As IEnumerable(Of Referencias) Implements IPersistenciaBase(Of Referencias).Consultar
            Return New RepositorioReferencias().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Referencias) As Long Implements IPersistenciaBase(Of Referencias).Insertar
            Return New RepositorioReferencias().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Referencias) As Long Implements IPersistenciaBase(Of Referencias).Modificar
            Return New RepositorioReferencias().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Referencias) As Referencias Implements IPersistenciaBase(Of Referencias).Obtener
            Return New RepositorioReferencias().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Referencias) As Boolean
            Return New RepositorioReferencias().Anular(entidad)
        End Function
    End Class

End Namespace

