﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Almacen
Imports Softtools.GesCarga.Repositorio.Basico.Almacen

Namespace Basico.Almacen
    Public NotInheritable Class PersistenciaUnidadEmpaqueReferencias
        Implements IPersistenciaBase(Of UnidadEmpaqueReferencias)

        Public Function Consultar(filtro As UnidadEmpaqueReferencias) As IEnumerable(Of UnidadEmpaqueReferencias) Implements IPersistenciaBase(Of UnidadEmpaqueReferencias).Consultar
            Return New RepositorioUnidadEmpaqueReferencias().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As UnidadEmpaqueReferencias) As Long Implements IPersistenciaBase(Of UnidadEmpaqueReferencias).Insertar
            Return New RepositorioUnidadEmpaqueReferencias().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As UnidadEmpaqueReferencias) As Long Implements IPersistenciaBase(Of UnidadEmpaqueReferencias).Modificar
            Return New RepositorioUnidadEmpaqueReferencias().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As UnidadEmpaqueReferencias) As UnidadEmpaqueReferencias Implements IPersistenciaBase(Of UnidadEmpaqueReferencias).Obtener
            Return New RepositorioUnidadEmpaqueReferencias().Obtener(filtro)
        End Function
        Public Function Anular(entidad As UnidadEmpaqueReferencias) As Boolean
            Return New RepositorioUnidadEmpaqueReferencias().Anular(entidad)
        End Function
    End Class

End Namespace

