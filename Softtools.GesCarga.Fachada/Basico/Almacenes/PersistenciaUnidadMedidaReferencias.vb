﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Almacen
Imports Softtools.GesCarga.Repositorio.Basico.Almacen

Namespace Basico.Almacen
    Public NotInheritable Class PersistenciaUnidadMedidaReferencias
        Implements IPersistenciaBase(Of UnidadMedidaReferencias)

        Public Function Consultar(filtro As UnidadMedidaReferencias) As IEnumerable(Of UnidadMedidaReferencias) Implements IPersistenciaBase(Of UnidadMedidaReferencias).Consultar
            Return New RepositorioUnidadMedidaReferencias().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As UnidadMedidaReferencias) As Long Implements IPersistenciaBase(Of UnidadMedidaReferencias).Insertar
            Return New RepositorioUnidadMedidaReferencias().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As UnidadMedidaReferencias) As Long Implements IPersistenciaBase(Of UnidadMedidaReferencias).Modificar
            Return New RepositorioUnidadMedidaReferencias().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As UnidadMedidaReferencias) As UnidadMedidaReferencias Implements IPersistenciaBase(Of UnidadMedidaReferencias).Obtener
            Return New RepositorioUnidadMedidaReferencias().Obtener(filtro)
        End Function
        Public Function Anular(entidad As UnidadMedidaReferencias) As Boolean
            Return New RepositorioUnidadMedidaReferencias().Anular(entidad)
        End Function
    End Class

End Namespace

