﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaFotosVehiculos
        Implements IPersistenciaBase(Of FotosVehiculos)

        Public Function Consultar(filtro As FotosVehiculos) As IEnumerable(Of FotosVehiculos) Implements IPersistenciaBase(Of FotosVehiculos).Consultar
            Return New RepositorioFotosVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As FotosVehiculos) As Long Implements IPersistenciaBase(Of FotosVehiculos).Insertar
            Return New RepositorioFotosVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As FotosVehiculos) As Long Implements IPersistenciaBase(Of FotosVehiculos).Modificar
            Return New RepositorioFotosVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As FotosVehiculos) As FotosVehiculos Implements IPersistenciaBase(Of FotosVehiculos).Obtener
            Return New RepositorioFotosVehiculos().Obtener(filtro)
        End Function
        Public Function InsertarTemporal(entidad As FotosVehiculos) As Long
            Return New RepositorioFotosVehiculos().InsertarTemporal(entidad)
        End Function
        Public Function EliminarFoto(entidad As FotosVehiculos) As Boolean
            Return New RepositorioFotosVehiculos().EliminarTemporal(entidad)
        End Function
        Public Function LimpiarTemporalUsuario(entidad As FotosVehiculos) As Boolean
            Return New RepositorioFotosVehiculos().LimpiarTemporalUsuario(entidad)
        End Function
    End Class

End Namespace

