﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaNovedadesConceptos
        Implements IPersistenciaBase(Of NovedadesConceptos)

        Public Function Consultar(filtro As NovedadesConceptos) As IEnumerable(Of NovedadesConceptos) Implements IPersistenciaBase(Of NovedadesConceptos).Consultar
            Return New RepositorioNovedadesConceptos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As NovedadesConceptos) As Long Implements IPersistenciaBase(Of NovedadesConceptos).Insertar
            Return New RepositorioNovedadesConceptos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As NovedadesConceptos) As Long Implements IPersistenciaBase(Of NovedadesConceptos).Modificar
            Return New RepositorioNovedadesConceptos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As NovedadesConceptos) As NovedadesConceptos Implements IPersistenciaBase(Of NovedadesConceptos).Obtener
            Return New RepositorioNovedadesConceptos().Obtener(filtro)
        End Function

        Public Function Anular(entidad As NovedadesConceptos) As Boolean
            Return New RepositorioNovedadesConceptos().Anular(entidad)
        End Function
    End Class

End Namespace

