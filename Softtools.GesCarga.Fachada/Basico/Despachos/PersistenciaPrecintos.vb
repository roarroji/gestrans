﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaPrecintos
        Implements IPersistenciaBase(Of Precintos)

        Public Function Consultar(filtro As Precintos) As IEnumerable(Of Precintos) Implements IPersistenciaBase(Of Precintos).Consultar
            Return New RepositorioPrecintos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Precintos) As Long Implements IPersistenciaBase(Of Precintos).Insertar
            Return New RepositorioPrecintos().Insertar(entidad)
        End Function

        Public Function InsertarPrecintos(entidad As Precintos) As Precintos
            Return New RepositorioPrecintos().InsertarPrecintos(entidad)
        End Function

        Public Function Modificar(entidad As Precintos) As Long Implements IPersistenciaBase(Of Precintos).Modificar
            Return New RepositorioPrecintos().Modificar(entidad)
        End Function

        Public Function ModificarPrecintos(entidad As Precintos) As Precintos
            Return New RepositorioPrecintos().ModificarPrecintos(entidad)
        End Function

        Public Function Obtener(filtro As Precintos) As Precintos Implements IPersistenciaBase(Of Precintos).Obtener
            Return New RepositorioPrecintos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Precintos) As Boolean
            Return New RepositorioPrecintos().Anular(entidad)
        End Function
        Public Function LiberarPrecinto(entidad As Precintos) As Boolean
            Return New RepositorioPrecintos().LiberarPrecinto(entidad)
        End Function

        Public Function ConsultarUnicoPrecinto(filtro As DetallePrecintos) As IEnumerable(Of DetallePrecintos)
            Return New RepositorioPrecintos().ConsultarDetallePrecinto(filtro)
        End Function
        Public Function ValidarPrecintoPlanillaPaqueteria(entidad As Precintos) As Precintos
            Return New RepositorioPrecintos().ValidarPrecintoPlanillaPaqueteria(entidad)
        End Function

    End Class

End Namespace

