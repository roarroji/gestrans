﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaPuestoControles
        Implements IPersistenciaBase(Of PuestoControles)

        Public Function Consultar(filtro As PuestoControles) As IEnumerable(Of PuestoControles) Implements IPersistenciaBase(Of PuestoControles).Consultar
            Return New RepositorioPuestoControles().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As PuestoControles) As Long Implements IPersistenciaBase(Of PuestoControles).Insertar
            Return New RepositorioPuestoControles().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PuestoControles) As Long Implements IPersistenciaBase(Of PuestoControles).Modificar
            Return New RepositorioPuestoControles().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PuestoControles) As PuestoControles Implements IPersistenciaBase(Of PuestoControles).Obtener
            Return New RepositorioPuestoControles().Obtener(filtro)
        End Function
        Public Function Anular(entidad As PuestoControles) As Boolean
            Return New RepositorioPuestoControles().Anular(entidad)
        End Function
    End Class

End Namespace

