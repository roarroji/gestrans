﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Repositorio.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaRendimientoGalonCombustible
        Implements IPersistenciaBase(Of RendimientoGalonCombustible)

        Public Function Consultar(filtro As RendimientoGalonCombustible) As IEnumerable(Of RendimientoGalonCombustible) Implements IPersistenciaBase(Of RendimientoGalonCombustible).Consultar
            Return New RepositorioRendimientoGalonCombustible().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As RendimientoGalonCombustible) As Long Implements IPersistenciaBase(Of RendimientoGalonCombustible).Insertar
            Return New RepositorioRendimientoGalonCombustible().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As RendimientoGalonCombustible) As Long Implements IPersistenciaBase(Of RendimientoGalonCombustible).Modificar
            Return New RepositorioRendimientoGalonCombustible().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As RendimientoGalonCombustible) As RendimientoGalonCombustible Implements IPersistenciaBase(Of RendimientoGalonCombustible).Obtener
            Return New RepositorioRendimientoGalonCombustible().Obtener(filtro)
        End Function
        Public Function Anular(entidad As RendimientoGalonCombustible) As Boolean
            Return New RepositorioRendimientoGalonCombustible().Anular(entidad)
        End Function

        Public Function Actualizar(entidad As RendimientoGalonCombustible) As Long
            Return New RepositorioRendimientoGalonCombustible().Actualizar(entidad)
        End Function


    End Class

End Namespace

