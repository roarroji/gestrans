﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Repositorio.Basico.Despachos
Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaValorCombustible

        Implements IPersistenciaBase(Of ValorCombustible)

        Public Function Consultar(filtro As ValorCombustible) As IEnumerable(Of ValorCombustible) Implements IPersistenciaBase(Of ValorCombustible).Consultar
            Return New RepositorioValorCombustible().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ValorCombustible) As Long Implements IPersistenciaBase(Of ValorCombustible).Insertar
            Return New RepositorioValorCombustible().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ValorCombustible) As Long Implements IPersistenciaBase(Of ValorCombustible).Modificar
            Return New RepositorioValorCombustible().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ValorCombustible) As ValorCombustible Implements IPersistenciaBase(Of ValorCombustible).Obtener
            Return New RepositorioValorCombustible().Obtener(filtro)
        End Function

        Public Function Anular(entidad As ValorCombustible) As Boolean
            Return New RepositorioValorCombustible().Anular(entidad)
        End Function

    End Class

End Namespace




