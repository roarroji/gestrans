﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Repositorio.Basico.Despachos
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades.Basico.General
Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaVehiculos
        Implements IPersistenciaBase(Of Vehiculos)

        Public Function Consultar(filtro As Vehiculos) As IEnumerable(Of Vehiculos) Implements IPersistenciaBase(Of Vehiculos).Consultar
            Return New RepositorioVehiculos().Consultar(filtro)
        End Function

        Public Function ConsultarPropietarios(filtro As Vehiculos) As IEnumerable(Of PropietarioVehiculo)
            Return New RepositorioVehiculos().ConsultarPropietarios(filtro)
        End Function
        Public Function Insertar(entidad As Vehiculos) As Long Implements IPersistenciaBase(Of Vehiculos).Insertar
            Return New RepositorioVehiculos().Insertar(entidad)
        End Function

        Public Function InsertarPropietarios(entidad As Vehiculos) As Long
            Return New RepositorioVehiculos().InsertarPropietarios(entidad)
        End Function

        Public Function Modificar(entidad As Vehiculos) As Long Implements IPersistenciaBase(Of Vehiculos).Modificar
            Return New RepositorioVehiculos().Modificar(entidad)
        End Function

        Public Function GuardarNovedad(filtro As Vehiculos) As Long
            Return New RepositorioVehiculos().GuardarNovedad(filtro)
        End Function


        Public Function Obtener(filtro As Vehiculos) As Vehiculos Implements IPersistenciaBase(Of Vehiculos).Obtener
            Return New RepositorioVehiculos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Vehiculos) As Boolean
            Return New RepositorioVehiculos().Anular(entidad)
        End Function

        Public Function ConsultarVehiculoEstudioSeguridad(filtro As EstudioSeguridad) As EstudioSeguridad
            Return New RepositorioVehiculos().ConsultarVehiculoEstudioSeguridad(filtro)
        End Function

        Public Function ConsultarDocumentosProximosVencer(filtro As VehiculosDocumentos) As IEnumerable(Of VehiculosDocumentos)
            Return New RepositorioVehiculos().ConsultarDocumentosProximosVencer(filtro)
        End Function

        Public Function ConsultarDocumentos(filtro As VehiculosDocumentos) As IEnumerable(Of VehiculosDocumentos)
            Return New RepositorioVehiculos().ConsultarDocumentos(filtro)
        End Function

        Public Function ConsultarDocumentosListaDocumentosPendientes(filtro As VehiculosDocumentos) As IEnumerable(Of VehiculosDocumentos)
            Return New RepositorioVehiculos().ConsultarDocumentosListaDocumentosPendientes(filtro)
        End Function

        Public Function ConsultarDocumentosSoatRTM(entidad As VehiculosDocumentos) As Boolean
            Return New RepositorioVehiculos().ConsultarDocumentosSoatRTM(entidad)
        End Function

        Public Function ConsultarEstadoListaNegra(entidad As VehiculosDocumentos) As Boolean
            Return New RepositorioVehiculos().ConsultarEstadoListaNegra(entidad)
        End Function

        Public Function GenerarPlanitilla(filtro As Vehiculos) As Vehiculos
            Return New RepositorioVehiculos().GenerarPlanitilla(filtro)
        End Function

        Public Function ConsultarVehiculosListaNegra(filtro As Vehiculos) As IEnumerable(Of Vehiculos)
            Return New RepositorioVehiculos().ConsultarVehiculosListaNegra(filtro)
        End Function

        Public Function ConsultarAuditoria(filtro As Vehiculos) As IEnumerable(Of Vehiculos)
            Return New RepositorioVehiculos().ConsultarAuditoria(filtro)
        End Function



    End Class



End Namespace

