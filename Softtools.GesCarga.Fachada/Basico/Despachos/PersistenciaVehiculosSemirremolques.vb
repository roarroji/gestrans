﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Repositorio.Basico.Despachos
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

Namespace Basico.Despachos
    Public NotInheritable Class PersistenciaSemirremolques
        Implements IPersistenciaBase(Of Semirremolques)

        Public Function Consultar(filtro As Semirremolques) As IEnumerable(Of Semirremolques) Implements IPersistenciaBase(Of Semirremolques).Consultar
            Return New RepositorioSemirremolques().Consultar(filtro)
        End Function

        Public Function ConsultarDocumentos(filtro As SemirremolquesDocumentos) As IEnumerable(Of SemirremolquesDocumentos)
            Return New RepositorioSemirremolques().ConsultarDocumentos(filtro)
        End Function

        Public Function Insertar(entidad As Semirremolques) As Long Implements IPersistenciaBase(Of Semirremolques).Insertar
            Return New RepositorioSemirremolques().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Semirremolques) As Long Implements IPersistenciaBase(Of Semirremolques).Modificar
            Return New RepositorioSemirremolques().Modificar(entidad)
        End Function

        Public Function GuardarNovedad(filtro As Semirremolques) As Long
            Return New RepositorioSemirremolques().GuardarNovedad(filtro)
        End Function

        Public Function Obtener(filtro As Semirremolques) As Semirremolques Implements IPersistenciaBase(Of Semirremolques).Obtener
            Return New RepositorioSemirremolques().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Semirremolques) As Boolean
            Return New RepositorioSemirremolques().Anular(entidad)
        End Function
        Public Function ConsultarSemirremolqueEstudioSeguridad(filtro As EstudioSeguridad) As EstudioSeguridad
            Return New RepositorioSemirremolques().ConsultarSemirremolqueEstudioSeguridad(filtro)
        End Function
        Public Function GenerarPlanitilla(filtro As Semirremolques) As Semirremolques
            Return New RepositorioSemirremolques().GenerarPlanitilla(filtro)
        End Function

    End Class

End Namespace

