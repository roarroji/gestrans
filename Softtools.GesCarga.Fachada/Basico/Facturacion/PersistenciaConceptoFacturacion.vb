﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Facturacion
Imports Softtools.GesCarga.Repositorio.Basico.Facturacion

Namespace Basico.Facturacion
    Public NotInheritable Class PersistenciaConceptoFacturacion
        Implements IPersistenciaBase(Of ConceptoFacturacion)

        Public Function Consultar(filtro As ConceptoFacturacion) As IEnumerable(Of ConceptoFacturacion) Implements IPersistenciaBase(Of ConceptoFacturacion).Consultar
            Return New RepositorioConceptoFacturacion().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ConceptoFacturacion) As Long Implements IPersistenciaBase(Of ConceptoFacturacion).Insertar
            Return New RepositorioConceptoFacturacion().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ConceptoFacturacion) As Long Implements IPersistenciaBase(Of ConceptoFacturacion).Modificar
            Return New RepositorioConceptoFacturacion().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ConceptoFacturacion) As ConceptoFacturacion Implements IPersistenciaBase(Of ConceptoFacturacion).Obtener
            Return New RepositorioConceptoFacturacion().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ConceptoFacturacion) As Boolean
            Return New RepositorioConceptoFacturacion().Anular(entidad)
        End Function

    End Class

End Namespace

