﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Facturacion
Imports Softtools.GesCarga.Repositorio.Basico.Facturacion

Namespace Basico.Facturacion
    Public NotInheritable Class PersistenciaConceptoNotasFacturas
        Implements IPersistenciaBase(Of ConceptoNotasFacturas)

        Public Function Consultar(filtro As ConceptoNotasFacturas) As IEnumerable(Of ConceptoNotasFacturas) Implements IPersistenciaBase(Of ConceptoNotasFacturas).Consultar
            Return New RepositorioConceptoNotasFacturas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ConceptoNotasFacturas) As Long Implements IPersistenciaBase(Of ConceptoNotasFacturas).Insertar
            Return New RepositorioConceptoNotasFacturas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ConceptoNotasFacturas) As Long Implements IPersistenciaBase(Of ConceptoNotasFacturas).Modificar
            Return New RepositorioConceptoNotasFacturas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ConceptoNotasFacturas) As ConceptoNotasFacturas Implements IPersistenciaBase(Of ConceptoNotasFacturas).Obtener
            Return New RepositorioConceptoNotasFacturas().Obtener(filtro)
        End Function

        Public Function Anular(entidad As ConceptoNotasFacturas) As Boolean
            Return New RepositorioConceptoNotasFacturas().Anular(entidad)

        End Function

    End Class
End Namespace