﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.FlotaPropia
Imports Softtools.GesCarga.Repositorio.Basico.FlotaPropia

Namespace Basico.FlotaPropia
    Public NotInheritable Class PersistenciaConceptoGastos
        Implements IPersistenciaBase(Of ConceptoGastos)

        Public Function Consultar(filtro As ConceptoGastos) As IEnumerable(Of ConceptoGastos) Implements IPersistenciaBase(Of ConceptoGastos).Consultar
            Return New RepositorioConceptoGastos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ConceptoGastos) As Long Implements IPersistenciaBase(Of ConceptoGastos).Insertar
            Return New RepositorioConceptoGastos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ConceptoGastos) As Long Implements IPersistenciaBase(Of ConceptoGastos).Modificar
            Return New RepositorioConceptoGastos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ConceptoGastos) As ConceptoGastos Implements IPersistenciaBase(Of ConceptoGastos).Obtener
            Return New RepositorioConceptoGastos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ConceptoGastos) As Boolean
            Return New RepositorioConceptoGastos().Anular(entidad)
        End Function

    End Class

End Namespace

