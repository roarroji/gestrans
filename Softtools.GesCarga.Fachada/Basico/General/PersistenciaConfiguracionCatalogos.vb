﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaConfiguracionCatalogos
        Implements IPersistenciaBase(Of ConfiguracionCatalogos)

        Public Function Consultar(filtro As ConfiguracionCatalogos) As IEnumerable(Of ConfiguracionCatalogos) Implements IPersistenciaBase(Of ConfiguracionCatalogos).Consultar
            Return New RepositorioConfiguracionCatalogos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ConfiguracionCatalogos) As Long Implements IPersistenciaBase(Of ConfiguracionCatalogos).Insertar
            Return New RepositorioConfiguracionCatalogos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ConfiguracionCatalogos) As Long Implements IPersistenciaBase(Of ConfiguracionCatalogos).Modificar
            Return New RepositorioConfiguracionCatalogos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ConfiguracionCatalogos) As ConfiguracionCatalogos Implements IPersistenciaBase(Of ConfiguracionCatalogos).Obtener
            Return New RepositorioConfiguracionCatalogos().Obtener(filtro)
        End Function

    End Class

End Namespace

