﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaDepartamentos
        Implements IPersistenciaBase(Of Departamentos)

        Public Function Consultar(filtro As Departamentos) As IEnumerable(Of Departamentos) Implements IPersistenciaBase(Of Departamentos).Consultar
            Return New RepositorioDepartamentos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Departamentos) As Long Implements IPersistenciaBase(Of Departamentos).Insertar
            Return New RepositorioDepartamentos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Departamentos) As Long Implements IPersistenciaBase(Of Departamentos).Modificar
            Return New RepositorioDepartamentos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Departamentos) As Departamentos Implements IPersistenciaBase(Of Departamentos).Obtener
            Return New RepositorioDepartamentos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Departamentos) As Boolean
            Return New RepositorioDepartamentos().Anular(entidad)
        End Function

    End Class

End Namespace

