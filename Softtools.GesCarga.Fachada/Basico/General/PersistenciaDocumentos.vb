﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaDocumentos
        Implements IPersistenciaBase(Of Documentos)
        Public Function Consultar(filtro As Documentos) As IEnumerable(Of Documentos) Implements IPersistenciaBase(Of Documentos).Consultar
            Return New RepositorioDocumentos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Documentos) As Long Implements IPersistenciaBase(Of Documentos).Insertar
            Return New RepositorioDocumentos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Documentos) As Long Implements IPersistenciaBase(Of Documentos).Modificar
            Return New RepositorioDocumentos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Documentos) As Documentos Implements IPersistenciaBase(Of Documentos).Obtener
            Return New RepositorioDocumentos().Obtener(filtro)
        End Function

        Public Function InsertarTemporal(entidad As Documentos) As Long
            Return New RepositorioDocumentos().InsertarTemporal(entidad)
        End Function
        Public Function EliminarDocumento(entidad As Documentos) As Boolean
            Return New RepositorioDocumentos().EliminarTemporal(entidad)
        End Function

        Public Function GuardarDocumentoTercero(entidad As Documentos) As Long
            Return New RepositorioDocumentos().GuardarDocumentoTercero(entidad)
        End Function

        Public Function InsertarDocumentosCumplidoRemesa(entidad As Documentos) As Long
            Return New RepositorioDocumentos().InsertarDocumentosCumplidoRemesa(entidad)
        End Function

        Public Function EliminarDocumentosCumplidoRemesa(entidad As Documentos) As Boolean
            Return New RepositorioDocumentos().EliminarDocumentosCumplidoRemesa(entidad)
        End Function

        Public Function InsertarDocumentoRemesa(entidad As Documentos) As Long
            Return New RepositorioDocumentos().InsertarDocumentoRemesa(entidad)
        End Function

        Public Function EliminarDocumentoRemesa(entidad As Documentos) As Boolean
            Return New RepositorioDocumentos().EliminarDocumentoRemesa(entidad)
        End Function

    End Class
End Namespace

