﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaEmpresas
        Implements IPersistenciaBase(Of Empresas)

        Public Function Consultar(filtro As Empresas) As IEnumerable(Of Empresas) Implements IPersistenciaBase(Of Empresas).Consultar
            Return New RepositorioEmpresas().Consultar(filtro)
        End Function

        Public Function ConsultarMaster(filtro As Empresas) As IEnumerable(Of Empresas)
            Return New RepositorioEmpresas().ConsultarMaster(filtro)
        End Function

        Public Function Insertar(entidad As Empresas) As Long Implements IPersistenciaBase(Of Empresas).Insertar
            Return New RepositorioEmpresas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Empresas) As Long Implements IPersistenciaBase(Of Empresas).Modificar
            Return New RepositorioEmpresas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Empresas) As Empresas Implements IPersistenciaBase(Of Empresas).Obtener
            Return New RepositorioEmpresas().Obtener(filtro)
        End Function

        Public Function ObtenerNumeroMaximoUsuarios(codigo As Short) As Short
            Return New RepositorioEmpresas().ObtenerNumeroMaximoUsuarios(codigo)
        End Function

        Public Function CambioClave(filtro As Empresas) As IEnumerable(Of Empresas)
            Return New RepositorioEmpresas().CambioClave(filtro)
        End Function
    End Class

End Namespace

