﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaFormularioInspecciones
        Implements IPersistenciaBase(Of FormularioInspecciones)

        Public Function Consultar(filtro As FormularioInspecciones) As IEnumerable(Of FormularioInspecciones) Implements IPersistenciaBase(Of FormularioInspecciones).Consultar
            Return New RepositorioFormularioInspecciones().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As FormularioInspecciones) As Long Implements IPersistenciaBase(Of FormularioInspecciones).Insertar
            Return New RepositorioFormularioInspecciones().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As FormularioInspecciones) As Long Implements IPersistenciaBase(Of FormularioInspecciones).Modificar
            Return New RepositorioFormularioInspecciones().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As FormularioInspecciones) As FormularioInspecciones Implements IPersistenciaBase(Of FormularioInspecciones).Obtener
            Return New RepositorioFormularioInspecciones().Obtener(filtro)
        End Function
        Public Function Anular(entidad As FormularioInspecciones) As Boolean
            Return New RepositorioFormularioInspecciones().Anular(entidad)
        End Function

    End Class

End Namespace

