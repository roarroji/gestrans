﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaGestionDocumentos
        Implements IPersistenciaBase(Of ConfiguracionGestionDocumentos)

        Public Function Consultar(filtro As ConfiguracionGestionDocumentos) As IEnumerable(Of ConfiguracionGestionDocumentos) Implements IPersistenciaBase(Of ConfiguracionGestionDocumentos).Consultar
            Return New RepositorioGestionDocumentos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ConfiguracionGestionDocumentos) As Long Implements IPersistenciaBase(Of ConfiguracionGestionDocumentos).Insertar
            Return New RepositorioGestionDocumentos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ConfiguracionGestionDocumentos) As Long Implements IPersistenciaBase(Of ConfiguracionGestionDocumentos).Modificar
            Return New RepositorioGestionDocumentos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ConfiguracionGestionDocumentos) As ConfiguracionGestionDocumentos Implements IPersistenciaBase(Of ConfiguracionGestionDocumentos).Obtener
            Return New RepositorioGestionDocumentos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ConfiguracionGestionDocumentos) As Boolean
            Return New RepositorioGestionDocumentos().Anular(entidad)
        End Function
    End Class

End Namespace

