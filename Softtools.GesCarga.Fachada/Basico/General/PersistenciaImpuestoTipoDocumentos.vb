﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaImpuestoTipoDocumentos
        Implements IPersistenciaBase(Of ImpuestoTipoDocumentos)

        Public Function Consultar(filtro As ImpuestoTipoDocumentos) As IEnumerable(Of ImpuestoTipoDocumentos) Implements IPersistenciaBase(Of ImpuestoTipoDocumentos).Consultar
            Return New RepositorioImpuestoTipoDocumentos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ImpuestoTipoDocumentos) As Long Implements IPersistenciaBase(Of ImpuestoTipoDocumentos).Insertar
            Return New RepositorioImpuestoTipoDocumentos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ImpuestoTipoDocumentos) As Long Implements IPersistenciaBase(Of ImpuestoTipoDocumentos).Modificar
            Return New RepositorioImpuestoTipoDocumentos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ImpuestoTipoDocumentos) As ImpuestoTipoDocumentos Implements IPersistenciaBase(Of ImpuestoTipoDocumentos).Obtener
            Return New RepositorioImpuestoTipoDocumentos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ImpuestoTipoDocumentos) As Boolean
            Return New RepositorioImpuestoTipoDocumentos().Anular(entidad)
        End Function
    End Class

End Namespace

