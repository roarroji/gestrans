﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaImpuestos
        Implements IPersistenciaBase(Of Impuestos)

        Public Function Consultar(filtro As Impuestos) As IEnumerable(Of Impuestos) Implements IPersistenciaBase(Of Impuestos).Consultar
            Return New RepositorioImpuestos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Impuestos) As Long Implements IPersistenciaBase(Of Impuestos).Insertar
            Return New RepositorioImpuestos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Impuestos) As Long Implements IPersistenciaBase(Of Impuestos).Modificar
            Return New RepositorioImpuestos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Impuestos) As Impuestos Implements IPersistenciaBase(Of Impuestos).Obtener
            Return New RepositorioImpuestos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Impuestos) As Boolean
            Return New RepositorioImpuestos().Anular(entidad)
        End Function

    End Class

End Namespace

