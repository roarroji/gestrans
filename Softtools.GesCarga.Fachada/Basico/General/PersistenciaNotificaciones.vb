﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaNotificaciones
        Implements IPersistenciaBase(Of Notificaciones)

        Public Function Consultar(filtro As Notificaciones) As IEnumerable(Of Notificaciones) Implements IPersistenciaBase(Of Notificaciones).Consultar
            Return New RepositorioNotificaciones().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Notificaciones) As Long Implements IPersistenciaBase(Of Notificaciones).Insertar
            Return New RepositorioNotificaciones().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Notificaciones) As Long Implements IPersistenciaBase(Of Notificaciones).Modificar
            Return New RepositorioNotificaciones().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Notificaciones) As Notificaciones Implements IPersistenciaBase(Of Notificaciones).Obtener
            Return New RepositorioNotificaciones().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Notificaciones) As Boolean
            Return New RepositorioNotificaciones().Anular(entidad)
        End Function

    End Class

End Namespace