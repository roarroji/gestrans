﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaProductoTransportados
        Implements IPersistenciaBase(Of ProductoTransportados)

        Public Function Consultar(filtro As ProductoTransportados) As IEnumerable(Of ProductoTransportados) Implements IPersistenciaBase(Of ProductoTransportados).Consultar
            Return New RepositorioProductoTransportados().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ProductoTransportados) As Long Implements IPersistenciaBase(Of ProductoTransportados).Insertar
            Return New RepositorioProductoTransportados().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ProductoTransportados) As Long Implements IPersistenciaBase(Of ProductoTransportados).Modificar
            Return New RepositorioProductoTransportados().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ProductoTransportados) As ProductoTransportados Implements IPersistenciaBase(Of ProductoTransportados).Obtener
            Return New RepositorioProductoTransportados().Obtener(filtro)
        End Function
        Public Function Anular(entidad As ProductoTransportados) As Boolean
            Return New RepositorioProductoTransportados().Anular(entidad)
        End Function
        Public Function GenerarPlantilla(filtro As ProductoTransportados) As ProductoTransportados
            Return New RepositorioProductoTransportados().GenerarPlantilla(filtro)
        End Function
    End Class

End Namespace

