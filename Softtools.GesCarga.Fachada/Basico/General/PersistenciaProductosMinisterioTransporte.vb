﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaProductosMinisterioTransporte
        Implements IPersistenciaBase(Of ProductosMinisterioTransporte)

        Public Function Consultar(filtro As ProductosMinisterioTransporte) As IEnumerable(Of ProductosMinisterioTransporte) Implements IPersistenciaBase(Of ProductosMinisterioTransporte).Consultar
            Return New RepositorioProductosMinisterioTransporte().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ProductosMinisterioTransporte) As Long Implements IPersistenciaBase(Of ProductosMinisterioTransporte).Insertar
            Return New RepositorioProductosMinisterioTransporte().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ProductosMinisterioTransporte) As Long Implements IPersistenciaBase(Of ProductosMinisterioTransporte).Modificar
            Return New RepositorioProductosMinisterioTransporte().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ProductosMinisterioTransporte) As ProductosMinisterioTransporte Implements IPersistenciaBase(Of ProductosMinisterioTransporte).Obtener
            Return New RepositorioProductosMinisterioTransporte().Obtener(filtro)
        End Function


        Public Function Anular(entidad As ProductosMinisterioTransporte) As Boolean
            Return New RepositorioProductosMinisterioTransporte().Anular(entidad)
        End Function
    End Class

End Namespace

