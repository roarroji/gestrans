﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

Namespace Basico.General
    Public NotInheritable Class PersistenciaTerceros
        Implements IPersistenciaBase(Of Terceros)

        Public Function Consultar(filtro As Terceros) As IEnumerable(Of Terceros) Implements IPersistenciaBase(Of Terceros).Consultar
            Return New RepositorioTerceros().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Terceros) As Long Implements IPersistenciaBase(Of Terceros).Insertar
            Return New RepositorioTerceros().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Terceros) As Long Implements IPersistenciaBase(Of Terceros).Modificar
            Return New RepositorioTerceros().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Terceros) As Terceros Implements IPersistenciaBase(Of Terceros).Obtener
            Return New RepositorioTerceros().Obtener(filtro)
        End Function
        Public Function InsertarDirecciones(Entidad As TerceroDirecciones) As Boolean
            Return New RepositorioTerceros().InsertarDirecciones(Entidad)
        End Function

        Public Function ConsultarTerceroGeneral(filtro As Tercero) As IEnumerable(Of Tercero)
            Return New RepositorioTerceros().ConsultarTerceroGeneral(filtro)
        End Function

        Public Function GuardarNovedad(filtro As Terceros) As Long
            Return New RepositorioTerceros().GuardarNovedad(filtro)
        End Function

        Public Function ConsultarTerceroEstudioSeguridad(filtro As EstudioSeguridad) As EstudioSeguridad
            Return New RepositorioTerceros().ConsultarTerceroEstudioSeguridad(filtro)
        End Function

        Public Function ConsultarDocumentosProximosVencer(filtro As TercerosDocumentos) As IEnumerable(Of TercerosDocumentos)
            Return New RepositorioTerceros().ConsultarDocumentosProximosVencer(filtro)
        End Function

        Public Function ConsultarDocumentos(filtro As TercerosDocumentos) As IEnumerable(Of TercerosDocumentos)
            Return New RepositorioTerceros().ConsultarDocumentos(filtro)
        End Function
        Public Function GenerarPlanitilla(filtro As Terceros) As Terceros
            Return New RepositorioTerceros().GenerarPlanitilla(filtro)
        End Function

        Public Function ConsultarAuditoria(filtro As Tercero) As IEnumerable(Of Tercero)
            Return New RepositorioTerceros().ConsultarAuditoria(filtro)
        End Function
    End Class
End Namespace

