﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.General
    Public NotInheritable Class PersistenciaUnidadEmpaque
        Implements IPersistenciaBase(Of UnidadEmpaque)

        Public Function Consultar(filtro As UnidadEmpaque) As IEnumerable(Of UnidadEmpaque) Implements IPersistenciaBase(Of UnidadEmpaque).Consultar
            Return New RepositorioUnidadEmpaque().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As UnidadEmpaque) As Long Implements IPersistenciaBase(Of UnidadEmpaque).Insertar
            Return New RepositorioUnidadEmpaque().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As UnidadEmpaque) As Long Implements IPersistenciaBase(Of UnidadEmpaque).Modificar
            Return New RepositorioUnidadEmpaque().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As UnidadEmpaque) As UnidadEmpaque Implements IPersistenciaBase(Of UnidadEmpaque).Obtener
            Return New RepositorioUnidadEmpaque().Obtener(filtro)
        End Function

    End Class

End Namespace

