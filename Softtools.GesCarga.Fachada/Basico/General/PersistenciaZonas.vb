﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General
Imports Softtools.GesCarga.Entidades.Paqueteria

Namespace Basico.General
    Public NotInheritable Class PersistenciaZonas
        Implements IPersistenciaBase(Of Zonas)

        Public Function ConsultarZonificacionGuias(filtro As Zonas) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioZonas().ConsultarZonificacionGuias(filtro)
        End Function

        Public Function Consultar(filtro As Zonas) As IEnumerable(Of Zonas) Implements IPersistenciaBase(Of Zonas).Consultar
            Return New RepositorioZonas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Zonas) As Long Implements IPersistenciaBase(Of Zonas).Insertar
            Return New RepositorioZonas().Insertar(entidad)
        End Function
        Public Function InsertarZonificacionGuias(entidad As Zonas) As Long
            Return New RepositorioZonas().InsertarZonificacionGuias(entidad)
        End Function

        Public Function Modificar(entidad As Zonas) As Long Implements IPersistenciaBase(Of Zonas).Modificar
            Return New RepositorioZonas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Zonas) As Zonas Implements IPersistenciaBase(Of Zonas).Obtener
            Return New RepositorioZonas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Zonas) As Boolean
            Return New RepositorioZonas().Anular(entidad)
        End Function

    End Class

End Namespace

