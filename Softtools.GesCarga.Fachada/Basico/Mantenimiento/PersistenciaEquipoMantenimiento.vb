﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.Repositorio.Mantenimiento

Namespace Mantenimiento

    Public NotInheritable Class PersistenciaEquipoMantenimiento
        Implements IPersistenciaBase(Of EquipoMantenimiento)
        Public Function Consultar(filtro As EquipoMantenimiento) As IEnumerable(Of EquipoMantenimiento) Implements IPersistenciaBase(Of EquipoMantenimiento).Consultar
            Return New RepositorioEquipoMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EquipoMantenimiento) As Long Implements IPersistenciaBase(Of EquipoMantenimiento).Insertar
            Return New RepositorioEquipoMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EquipoMantenimiento) As Long Implements IPersistenciaBase(Of EquipoMantenimiento).Modificar
            Return New RepositorioEquipoMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EquipoMantenimiento) As EquipoMantenimiento Implements IPersistenciaBase(Of EquipoMantenimiento).Obtener
            Return New RepositorioEquipoMantenimiento().Obtener(filtro)
        End Function

    End Class
End Namespace
