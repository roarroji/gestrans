﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.Repositorio.Mantenimiento

Namespace Mantenimiento

    Public NotInheritable Class PersistenciaEstadoEquipoMantenimiento
        Implements IPersistenciaBase(Of EstadoEquipoMantenimiento)
        Public Function Consultar(filtro As EstadoEquipoMantenimiento) As IEnumerable(Of EstadoEquipoMantenimiento) Implements IPersistenciaBase(Of EstadoEquipoMantenimiento).Consultar
            Return New RepositorioEstadoEquipoMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EstadoEquipoMantenimiento) As Long Implements IPersistenciaBase(Of EstadoEquipoMantenimiento).Insertar
            Return New RepositorioEstadoEquipoMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EstadoEquipoMantenimiento) As Long Implements IPersistenciaBase(Of EstadoEquipoMantenimiento).Modificar
            Return New RepositorioEstadoEquipoMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EstadoEquipoMantenimiento) As EstadoEquipoMantenimiento Implements IPersistenciaBase(Of EstadoEquipoMantenimiento).Obtener
            Return New RepositorioEstadoEquipoMantenimiento().Obtener(filtro)
        End Function

    End Class
End Namespace
