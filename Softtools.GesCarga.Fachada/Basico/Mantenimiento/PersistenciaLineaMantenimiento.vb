﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.Repositorio.Mantenimiento

Namespace Mantenimiento
    Public NotInheritable Class PersistenciaLineaMantenimiento
        Implements IPersistenciaBase(Of LineaMantenimiento)
        Public Function Consultar(filtro As LineaMantenimiento) As IEnumerable(Of LineaMantenimiento) Implements IPersistenciaBase(Of LineaMantenimiento).Consultar
            Return New RepositorioLineaMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As LineaMantenimiento) As Long Implements IPersistenciaBase(Of LineaMantenimiento).Insertar
            Return New RepositorioLineaMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As LineaMantenimiento) As Long Implements IPersistenciaBase(Of LineaMantenimiento).Modificar
            Return New RepositorioLineaMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As LineaMantenimiento) As LineaMantenimiento Implements IPersistenciaBase(Of LineaMantenimiento).Obtener
            Return New RepositorioLineaMantenimiento().Obtener(filtro)
        End Function
    End Class
End Namespace

