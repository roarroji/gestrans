﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.Repositorio.Mantenimiento

Namespace Mantenimiento

    Public NotInheritable Class PersistenciaMarcaEquipoMantenimiento
        Implements IPersistenciaBase(Of MarcaEquipoMantenimiento)
        Public Function Consultar(filtro As MarcaEquipoMantenimiento) As IEnumerable(Of MarcaEquipoMantenimiento) Implements IPersistenciaBase(Of MarcaEquipoMantenimiento).Consultar
            Return New RepositorioMarcaEquipoMantenimiento().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As MarcaEquipoMantenimiento) As Long Implements IPersistenciaBase(Of MarcaEquipoMantenimiento).Insertar
            Return New RepositorioMarcaEquipoMantenimiento().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As MarcaEquipoMantenimiento) As Long Implements IPersistenciaBase(Of MarcaEquipoMantenimiento).Modificar
            Return New RepositorioMarcaEquipoMantenimiento().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As MarcaEquipoMantenimiento) As MarcaEquipoMantenimiento Implements IPersistenciaBase(Of MarcaEquipoMantenimiento).Obtener
            Return New RepositorioMarcaEquipoMantenimiento().Obtener(filtro)
        End Function

    End Class
End Namespace
