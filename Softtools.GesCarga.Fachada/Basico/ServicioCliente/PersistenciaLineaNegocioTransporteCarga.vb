﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.ServicioCliente
Imports Softtools.GesCarga.Repositorio.Basico.ServicioCliente

Namespace Basico.ServicioCliente
    Public NotInheritable Class PersistenciaLineaNegocioTransporteCarga
        Implements IPersistenciaBase(Of LineaNegocioTransporteCarga)

        Public Function Consultar(filtro As LineaNegocioTransporteCarga) As IEnumerable(Of LineaNegocioTransporteCarga) Implements IPersistenciaBase(Of LineaNegocioTransporteCarga).Consultar
            Return New RepositorioLineaNegocioTransporteCarga().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As LineaNegocioTransporteCarga) As Long Implements IPersistenciaBase(Of LineaNegocioTransporteCarga).Insertar
            Return New RepositorioLineaNegocioTransporteCarga().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As LineaNegocioTransporteCarga) As Long Implements IPersistenciaBase(Of LineaNegocioTransporteCarga).Modificar
            Return New RepositorioLineaNegocioTransporteCarga().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As LineaNegocioTransporteCarga) As LineaNegocioTransporteCarga Implements IPersistenciaBase(Of LineaNegocioTransporteCarga).Obtener
            Return New RepositorioLineaNegocioTransporteCarga().Obtener(filtro)
        End Function

    End Class

End Namespace

