﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.ServicioCliente
Imports Softtools.GesCarga.Repositorio.Basico.ServicioCliente

Namespace Basico.ServicioCliente
    Public NotInheritable Class PersistenciaTipoLineaNegocioCarga
        Implements IPersistenciaBase(Of TipoLineaNegocioCarga)

        Public Function Consultar(filtro As TipoLineaNegocioCarga) As IEnumerable(Of TipoLineaNegocioCarga) Implements IPersistenciaBase(Of TipoLineaNegocioCarga).Consultar
            Return New RepositorioTipoLineaNegocioCarga().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TipoLineaNegocioCarga) As Long Implements IPersistenciaBase(Of TipoLineaNegocioCarga).Insertar
            Return New RepositorioTipoLineaNegocioCarga().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TipoLineaNegocioCarga) As Long Implements IPersistenciaBase(Of TipoLineaNegocioCarga).Modificar
            Return New RepositorioTipoLineaNegocioCarga().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TipoLineaNegocioCarga) As TipoLineaNegocioCarga Implements IPersistenciaBase(Of TipoLineaNegocioCarga).Obtener
            Return New RepositorioTipoLineaNegocioCarga().Obtener(filtro)
        End Function
    End Class

End Namespace

