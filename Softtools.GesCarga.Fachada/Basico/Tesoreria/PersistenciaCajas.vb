﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Repositorio.Basico.Tesoreria

Namespace Basico.Tesoreria
    Public NotInheritable Class PersistenciaCajas
        Implements IPersistenciaBase(Of Cajas)

        Public Function Consultar(filtro As Cajas) As IEnumerable(Of Cajas) Implements IPersistenciaBase(Of Cajas).Consultar
            Return New RepositorioCajas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Cajas) As Long Implements IPersistenciaBase(Of Cajas).Insertar
            Return New RepositorioCajas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Cajas) As Long Implements IPersistenciaBase(Of Cajas).Modificar
            Return New RepositorioCajas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Cajas) As Cajas Implements IPersistenciaBase(Of Cajas).Obtener
            Return New RepositorioCajas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Cajas) As Boolean
            Return New RepositorioCajas().Anular(entidad)
        End Function
    End Class

End Namespace

