﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Repositorio.Basico.Tesoreria

Namespace Basico.Tesoreria
    Public NotInheritable Class PersistenciaCuentaBancarias
        Implements IPersistenciaBase(Of CuentaBancarias)

        Public Function Consultar(filtro As CuentaBancarias) As IEnumerable(Of CuentaBancarias) Implements IPersistenciaBase(Of CuentaBancarias).Consultar
            Return New RepositorioCuentaBancarias().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As CuentaBancarias) As Long Implements IPersistenciaBase(Of CuentaBancarias).Insertar
            Return New RepositorioCuentaBancarias().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As CuentaBancarias) As Long Implements IPersistenciaBase(Of CuentaBancarias).Modificar
            Return New RepositorioCuentaBancarias().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As CuentaBancarias) As CuentaBancarias Implements IPersistenciaBase(Of CuentaBancarias).Obtener
            Return New RepositorioCuentaBancarias().Obtener(filtro)
        End Function
        Public Function Anular(entidad As CuentaBancarias) As Boolean
            Return New RepositorioCuentaBancarias().Anular(entidad)
        End Function
        Public Function CuentaBancariaOficinas(codigoEmpresa As Short, codigo As Short) As IEnumerable(Of CuentaBancariaOficinas)
            Return New RepositorioCuentaBancarias().CuentaBancariaoficinas(codigoEmpresa, codigo)
        End Function

    End Class

End Namespace

