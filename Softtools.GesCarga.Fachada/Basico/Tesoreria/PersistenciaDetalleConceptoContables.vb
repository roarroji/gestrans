﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Repositorio.Basico.Tesoreria

Namespace Basico.Tesoreria
    Public NotInheritable Class PersistenciaDetalleConceptoContables
        Implements IPersistenciaBase(Of DetalleConceptoContables)

        Public Function Consultar(filtro As DetalleConceptoContables) As IEnumerable(Of DetalleConceptoContables) Implements IPersistenciaBase(Of DetalleConceptoContables).Consultar
            Return New RepositorioDetalleConceptoContables().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleConceptoContables) As Long Implements IPersistenciaBase(Of DetalleConceptoContables).Insertar
            Return New RepositorioDetalleConceptoContables().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleConceptoContables) As Long Implements IPersistenciaBase(Of DetalleConceptoContables).Modificar
            Return New RepositorioDetalleConceptoContables().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleConceptoContables) As DetalleConceptoContables Implements IPersistenciaBase(Of DetalleConceptoContables).Obtener
            Return New RepositorioDetalleConceptoContables().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleConceptoContables) As Boolean
            Return New RepositorioDetalleConceptoContables().Anular(entidad)
        End Function
    End Class

End Namespace

