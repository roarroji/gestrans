﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Repositorio.Basico.Tesoreria

Namespace Basico.Tesoreria
    Public NotInheritable Class PersistenciaEncabezadoConceptoContables
        Implements IPersistenciaBase(Of EncabezadoConceptoContables)

        Public Function Consultar(filtro As EncabezadoConceptoContables) As IEnumerable(Of EncabezadoConceptoContables) Implements IPersistenciaBase(Of EncabezadoConceptoContables).Consultar
            Return New RepositorioEncabezadoConceptoContables().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoConceptoContables) As Long Implements IPersistenciaBase(Of EncabezadoConceptoContables).Insertar
            Return New RepositorioEncabezadoConceptoContables().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoConceptoContables) As Long Implements IPersistenciaBase(Of EncabezadoConceptoContables).Modificar
            Return New RepositorioEncabezadoConceptoContables().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoConceptoContables) As EncabezadoConceptoContables Implements IPersistenciaBase(Of EncabezadoConceptoContables).Obtener
            Return New RepositorioEncabezadoConceptoContables().Obtener(filtro)
        End Function
        Public Function Anular(entidad As EncabezadoConceptoContables) As Boolean
            Return New RepositorioEncabezadoConceptoContables().Anular(entidad)
        End Function
    End Class

End Namespace

