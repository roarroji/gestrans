﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Repositorio.Basico.Tesoreria

Namespace Basico.Tesoreria
    Public NotInheritable Class PersistenciaEncabezadoParametrizacionContables
        Implements IPersistenciaBase(Of EncabezadoParametrizacionContables)

        Public Function Consultar(filtro As EncabezadoParametrizacionContables) As IEnumerable(Of EncabezadoParametrizacionContables) Implements IPersistenciaBase(Of EncabezadoParametrizacionContables).Consultar
            Return New RepositorioEncabezadoParametrizacionContables().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoParametrizacionContables) As Long Implements IPersistenciaBase(Of EncabezadoParametrizacionContables).Insertar
            Return New RepositorioEncabezadoParametrizacionContables().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoParametrizacionContables) As Long Implements IPersistenciaBase(Of EncabezadoParametrizacionContables).Modificar
            Return New RepositorioEncabezadoParametrizacionContables().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoParametrizacionContables) As EncabezadoParametrizacionContables Implements IPersistenciaBase(Of EncabezadoParametrizacionContables).Obtener
            Return New RepositorioEncabezadoParametrizacionContables().Obtener(filtro)
        End Function
        Public Function Anular(entidad As EncabezadoParametrizacionContables) As Boolean
            Return New RepositorioEncabezadoParametrizacionContables().Anular(entidad)
        End Function
    End Class

End Namespace

