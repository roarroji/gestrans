﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Repositorio.Basico.Tesoreria

Namespace Basico.Tesoreria
    Public NotInheritable Class PersistenciaPlanUnicoCuentas
        Implements IPersistenciaBase(Of PlanUnicoCuentas)

        Public Function Consultar(filtro As PlanUnicoCuentas) As IEnumerable(Of PlanUnicoCuentas) Implements IPersistenciaBase(Of PlanUnicoCuentas).Consultar
            Return New RepositorioPlanUnicoCuentas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As PlanUnicoCuentas) As Long Implements IPersistenciaBase(Of PlanUnicoCuentas).Insertar
            Return New RepositorioPlanUnicoCuentas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PlanUnicoCuentas) As Long Implements IPersistenciaBase(Of PlanUnicoCuentas).Modificar
            Return New RepositorioPlanUnicoCuentas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PlanUnicoCuentas) As PlanUnicoCuentas Implements IPersistenciaBase(Of PlanUnicoCuentas).Obtener
            Return New RepositorioPlanUnicoCuentas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As PlanUnicoCuentas) As Boolean
            Return New RepositorioPlanUnicoCuentas().Anular(entidad)
        End Function
    End Class

End Namespace

