﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Repositorio

Public Class PersistenciaTercerosSincronizacionCARGAAPP
    Implements IPersistenciaBase(Of TercerosSincronizacionCARGAAPP)

    Public Function Consultar(filtro As TercerosSincronizacionCARGAAPP) As IEnumerable(Of TercerosSincronizacionCARGAAPP) Implements IPersistenciaBase(Of TercerosSincronizacionCARGAAPP).Consultar
        Throw New NotImplementedException()
    End Function

    Public Function Insertar(entidad As TercerosSincronizacionCARGAAPP) As Long Implements IPersistenciaBase(Of TercerosSincronizacionCARGAAPP).Insertar
        Throw New NotImplementedException()
    End Function

    Public Function Modificar(entidad As TercerosSincronizacionCARGAAPP) As Long Implements IPersistenciaBase(Of TercerosSincronizacionCARGAAPP).Modificar
        Throw New NotImplementedException()
    End Function
    Public Function ModificarTerceros(entidad As TercerosModificacionCARGAAPP) As Long
        Return New RepositorioTercerosSincronizacionCARGAAPP().ModificarTerceros(entidad)
    End Function

    Public Function ModificarVehiculos(entidad As VehiculosModificacionCARGAAPP) As Long
        Return New RepositorioTercerosSincronizacionCARGAAPP().ModificarVehiculos(entidad)
    End Function
    Public Function Obtener(filtro As TercerosSincronizacionCARGAAPP) As TercerosSincronizacionCARGAAPP Implements IPersistenciaBase(Of TercerosSincronizacionCARGAAPP).Obtener
        Return New RepositorioTercerosSincronizacionCARGAAPP().Obtener(filtro)
    End Function
End Class
