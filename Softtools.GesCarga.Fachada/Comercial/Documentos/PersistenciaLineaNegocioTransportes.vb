﻿Imports Softtools.GesCarga.IFachada

Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Repositorio.Comercial.Documentos

Namespace Comercial.Documentos
    Public NotInheritable Class PersistenciaLineaNegocioTransportes
        Implements IPersistenciaBase(Of LineaNegocioTransportes)

        Public Function Consultar(filtro As LineaNegocioTransportes) As IEnumerable(Of LineaNegocioTransportes) Implements IPersistenciaBase(Of LineaNegocioTransportes).Consultar
            Return New RepositorioLineaNegocioTransportes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As LineaNegocioTransportes) As Long Implements IPersistenciaBase(Of LineaNegocioTransportes).Insertar
            Return New RepositorioLineaNegocioTransportes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As LineaNegocioTransportes) As Long Implements IPersistenciaBase(Of LineaNegocioTransportes).Modificar
            Return New RepositorioLineaNegocioTransportes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As LineaNegocioTransportes) As LineaNegocioTransportes Implements IPersistenciaBase(Of LineaNegocioTransportes).Obtener
            Return New RepositorioLineaNegocioTransportes().Obtener(filtro)
        End Function

    End Class

End Namespace

