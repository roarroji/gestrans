﻿Imports Softtools.GesCarga.IFachada

Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Repositorio.Comercial.Documentos

Namespace Comercial.Documentos
    Public NotInheritable Class PersistenciaTarifarioCompras
        Implements IPersistenciaBase(Of TarifarioCompras)

        Public Function Consultar(filtro As TarifarioCompras) As IEnumerable(Of TarifarioCompras) Implements IPersistenciaBase(Of TarifarioCompras).Consultar
            Return New RepositorioTarifarioCompras().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TarifarioCompras) As Long Implements IPersistenciaBase(Of TarifarioCompras).Insertar
            Return New RepositorioTarifarioCompras().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TarifarioCompras) As Long Implements IPersistenciaBase(Of TarifarioCompras).Modificar
            Return New RepositorioTarifarioCompras().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TarifarioCompras) As TarifarioCompras Implements IPersistenciaBase(Of TarifarioCompras).Obtener
            Return New RepositorioTarifarioCompras().Obtener(filtro)
        End Function
        Public Function Anular(entidad As TarifarioCompras) As Boolean
            Return New RepositorioTarifarioCompras().Anular(entidad)
        End Function
    End Class

End Namespace

