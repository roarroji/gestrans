﻿Imports Softtools.GesCarga.IFachada

Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Repositorio.Comercial.Documentos

Namespace Comercial.Documentos
    Public NotInheritable Class PersistenciaTarifarioVentas
        Implements IPersistenciaBase(Of TarifarioVentas)

        Public Function Consultar(filtro As TarifarioVentas) As IEnumerable(Of TarifarioVentas) Implements IPersistenciaBase(Of TarifarioVentas).Consultar
            Return New RepositorioTarifarioVentas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TarifarioVentas) As Long Implements IPersistenciaBase(Of TarifarioVentas).Insertar
            Return New RepositorioTarifarioVentas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TarifarioVentas) As Long Implements IPersistenciaBase(Of TarifarioVentas).Modificar
            Return New RepositorioTarifarioVentas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TarifarioVentas) As TarifarioVentas Implements IPersistenciaBase(Of TarifarioVentas).Obtener
            Return New RepositorioTarifarioVentas().Obtener(filtro)
        End Function
        Public Function GenerarPlanitilla(filtro As TarifarioVentas) As TarifarioVentas
            Return New RepositorioTarifarioVentas().GenerarPlanitilla(filtro)
        End Function
        Public Function Anular(entidad As TarifarioVentas) As Boolean
            Return New RepositorioTarifarioVentas().Anular(entidad)
        End Function
        Public Function ConsultarDetalleTarifarioVentas(filtro As DetalleTarifarioVentas) As IEnumerable(Of DetalleTarifarioVentas)
            Return New RepositorioTarifarioVentas().ConsultarDetalleTarifarioVentas(filtro)
        End Function
        Public Function ObtenerDetalleTarifarioPaqueteria(filtro As TarifarioVentas) As TarifarioVentas
            Return New RepositorioTarifarioVentas().ObtenerDetalleTarifarioPaqueteria(filtro)
        End Function
    End Class

End Namespace

