﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Comercial.Documentos
    Public NotInheritable Class PersistenciaTipoTarifaTransportes
        Implements IPersistenciaBase(Of TipoTarifaTransportes)

        Public Function Consultar(filtro As TipoTarifaTransportes) As IEnumerable(Of TipoTarifaTransportes) Implements IPersistenciaBase(Of TipoTarifaTransportes).Consultar
            Return New RepositorioTipoTarifaTransportes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TipoTarifaTransportes) As Long Implements IPersistenciaBase(Of TipoTarifaTransportes).Insertar
            Return New RepositorioTipoTarifaTransportes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TipoTarifaTransportes) As Long Implements IPersistenciaBase(Of TipoTarifaTransportes).Modificar
            Return New RepositorioTipoTarifaTransportes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TipoTarifaTransportes) As TipoTarifaTransportes Implements IPersistenciaBase(Of TipoTarifaTransportes).Obtener
            Return New RepositorioTipoTarifaTransportes().Obtener(filtro)
        End Function

    End Class

End Namespace

