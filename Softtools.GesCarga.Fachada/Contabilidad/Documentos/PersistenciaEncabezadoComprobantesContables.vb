﻿Imports Softtools.GesCarga.Entidades.Contabilidad.Documentos
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Repositorio.Contabilidad

Namespace Contabilidad
    Public NotInheritable Class PersistenciaEncabezadoComprobantesContables
        Implements IPersistenciaBase(Of EncabezadoComprobantesContables)

        Public Function Consultar(filtro As EncabezadoComprobantesContables) As IEnumerable(Of EncabezadoComprobantesContables) Implements IPersistenciaBase(Of EncabezadoComprobantesContables).Consultar
            Return New RepositorioEncabezadoComprobantesContables().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoComprobantesContables) As Long Implements IPersistenciaBase(Of EncabezadoComprobantesContables).Insertar
            Return New RepositorioEncabezadoComprobantesContables().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoComprobantesContables) As Long Implements IPersistenciaBase(Of EncabezadoComprobantesContables).Modificar
            Return New RepositorioEncabezadoComprobantesContables().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoComprobantesContables) As EncabezadoComprobantesContables Implements IPersistenciaBase(Of EncabezadoComprobantesContables).Obtener
            Return New RepositorioEncabezadoComprobantesContables().Obtener(filtro)
        End Function

        Public Function Anular(entidad As EncabezadoComprobantesContables) As Boolean
            Return New RepositorioEncabezadoComprobantesContables().Anular(entidad)
        End Function

    End Class

End Namespace

