﻿Imports Softtools.GesCarga.Entidades.Contabilidad.Procesos
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Repositorio.Contabilidad

Namespace Contabilidad
    Public NotInheritable Class PersistenciaCierreContableDocumentos
        Implements IPersistenciaBase(Of CierreContableDocumentos)

        Public Function Consultar(filtro As CierreContableDocumentos) As IEnumerable(Of CierreContableDocumentos) Implements IPersistenciaBase(Of CierreContableDocumentos).Consultar
            Return New RepositorioCierreContableDocumentos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As CierreContableDocumentos) As Long Implements IPersistenciaBase(Of CierreContableDocumentos).Insertar
            Return New RepositorioCierreContableDocumentos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As CierreContableDocumentos) As Long Implements IPersistenciaBase(Of CierreContableDocumentos).Modificar
            Return New RepositorioCierreContableDocumentos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As CierreContableDocumentos) As CierreContableDocumentos Implements IPersistenciaBase(Of CierreContableDocumentos).Obtener
            Return New RepositorioCierreContableDocumentos().Obtener(filtro)
        End Function

    End Class
End Namespace

