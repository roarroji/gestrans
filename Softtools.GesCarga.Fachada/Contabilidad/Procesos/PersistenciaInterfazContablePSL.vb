﻿Imports Softtools.GesCarga.Entidades.Contabilidad.Procesos
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Repositorio.Contabilidad

Namespace Contabilidad
    Public NotInheritable Class PersistenciaInterfazContablePSL
        Implements IPersistenciaBase(Of InterfazContablePSL)

        Public Function Consultar(filtro As InterfazContablePSL) As IEnumerable(Of InterfazContablePSL) Implements IPersistenciaBase(Of InterfazContablePSL).Consultar
            Return New RepositorioInterfazContablePSL().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As InterfazContablePSL) As Long Implements IPersistenciaBase(Of InterfazContablePSL).Insertar
            Return New RepositorioInterfazContablePSL().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As InterfazContablePSL) As Long Implements IPersistenciaBase(Of InterfazContablePSL).Modificar
            Return New RepositorioInterfazContablePSL().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As InterfazContablePSL) As InterfazContablePSL Implements IPersistenciaBase(Of InterfazContablePSL).Obtener
            Return New RepositorioInterfazContablePSL().Obtener(filtro)
        End Function
    End Class
End Namespace