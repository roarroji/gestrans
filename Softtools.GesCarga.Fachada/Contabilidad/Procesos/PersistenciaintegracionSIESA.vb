﻿Imports Softtools.GesCarga.Entidades.Contabilidad.Documentos
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Repositorio.Contabilidad

Namespace Contabilidad
    Public NotInheritable Class PersistenciaintegracionSIESA
        Implements IPersistenciaBase(Of integracionSIESA)

        Public Function Consultar(filtro As integracionSIESA) As IEnumerable(Of integracionSIESA) Implements IPersistenciaBase(Of integracionSIESA).Consultar
            Return New RepositoriointegracionSIESA().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As integracionSIESA) As Long Implements IPersistenciaBase(Of integracionSIESA).Insertar

        End Function

        Public Function Modificar(entidad As integracionSIESA) As Long Implements IPersistenciaBase(Of integracionSIESA).Modificar
            Return New RepositoriointegracionSIESA().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As integracionSIESA) As integracionSIESA Implements IPersistenciaBase(Of integracionSIESA).Obtener

        End Function

        Public Function Anular(entidad As integracionSIESA) As Boolean

        End Function

    End Class

End Namespace

