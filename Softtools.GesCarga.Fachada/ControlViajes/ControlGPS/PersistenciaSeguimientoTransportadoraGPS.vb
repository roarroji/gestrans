﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.ControlTrafico
Imports Softtools.GesCarga.Repositorio.ControlTrafico

Namespace ControlTrafico
    Public NotInheritable Class PersistenciaSeguimientoTransportadoraGPS
        Implements IPersistenciaBase(Of SeguimientoTransportadoraGPS)

        Public Function Consultar(filtro As SeguimientoTransportadoraGPS) As IEnumerable(Of SeguimientoTransportadoraGPS) Implements IPersistenciaBase(Of SeguimientoTransportadoraGPS).Consultar
            Return New RepositorioSeguimientoTransportadoraGPS().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As SeguimientoTransportadoraGPS) As Long Implements IPersistenciaBase(Of SeguimientoTransportadoraGPS).Insertar
            Return New RepositorioSeguimientoTransportadoraGPS().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As SeguimientoTransportadoraGPS) As Long Implements IPersistenciaBase(Of SeguimientoTransportadoraGPS).Modificar
            Return New RepositorioSeguimientoTransportadoraGPS().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As SeguimientoTransportadoraGPS) As SeguimientoTransportadoraGPS Implements IPersistenciaBase(Of SeguimientoTransportadoraGPS).Obtener
            Return New RepositorioSeguimientoTransportadoraGPS().Obtener(filtro)
        End Function
        Public Function Anular(entidad As SeguimientoTransportadoraGPS) As Boolean
            Return New RepositorioSeguimientoTransportadoraGPS().Anular(entidad)
        End Function
    End Class

End Namespace

