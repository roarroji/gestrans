﻿Imports Softtools.GesCarga.Entidades.ControlViajes
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Repositorio.ControlViajes

Namespace ControlViajes
    Public NotInheritable Class PersistenciaDocumentoInspeccionPreoperacional
        Implements IPersistenciaBase(Of DocumentoInspeccionPreoperacional)

        Public Function Consultar(filtro As DocumentoInspeccionPreoperacional) As IEnumerable(Of DocumentoInspeccionPreoperacional) Implements IPersistenciaBase(Of DocumentoInspeccionPreoperacional).Consultar
            Return New RepositorioDocumentoInspeccionPreoperacional().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DocumentoInspeccionPreoperacional) As Long Implements IPersistenciaBase(Of DocumentoInspeccionPreoperacional).Insertar
            Return New RepositorioDocumentoInspeccionPreoperacional().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DocumentoInspeccionPreoperacional) As Long Implements IPersistenciaBase(Of DocumentoInspeccionPreoperacional).Modificar
            Return New RepositorioDocumentoInspeccionPreoperacional().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DocumentoInspeccionPreoperacional) As DocumentoInspeccionPreoperacional Implements IPersistenciaBase(Of DocumentoInspeccionPreoperacional).Obtener
            Return New RepositorioDocumentoInspeccionPreoperacional().Obtener(filtro)
        End Function
        Public Function InsertarTemporal(entidad As DocumentoInspeccionPreoperacional) As Long
            Return New RepositorioDocumentoInspeccionPreoperacional().InsertarTemporal(entidad)
        End Function

        Public Function EliminarFoto(entidad As DocumentoInspeccionPreoperacional) As Boolean
            Return New RepositorioDocumentoInspeccionPreoperacional().EliminarTemporal(entidad)
        End Function

        Public Function LimpiarTemporalUsuario(entidad As DocumentoInspeccionPreoperacional) As Boolean
            Return New RepositorioDocumentoInspeccionPreoperacional().LimpiarTemporalUsuario(entidad)
        End Function
    End Class

End Namespace

