﻿Imports Softtools.GesCarga.Entidades.ControlViajes
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Repositorio.ControlViajes

Namespace ControlViajes
    Public NotInheritable Class PersistenciaAutorizaciones
        Implements IPersistenciaBase(Of Autorizaciones)

        Public Function Consultar(filtro As Autorizaciones) As IEnumerable(Of Autorizaciones) Implements IPersistenciaBase(Of Autorizaciones).Consultar
            Return New RepositorioAutorizaciones().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Autorizaciones) As Long Implements IPersistenciaBase(Of Autorizaciones).Insertar
            Return New RepositorioAutorizaciones().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Autorizaciones) As Long Implements IPersistenciaBase(Of Autorizaciones).Modificar
            Return New RepositorioAutorizaciones().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Autorizaciones) As Autorizaciones Implements IPersistenciaBase(Of Autorizaciones).Obtener
            Return New RepositorioAutorizaciones().Obtener(filtro)
        End Function

        Public Function Anular(entidad As Autorizaciones) As Boolean
            Return New RepositorioAutorizaciones().Anular(entidad)
        End Function



    End Class

End Namespace

