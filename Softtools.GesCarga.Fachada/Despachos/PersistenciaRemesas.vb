﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Repositorio.Despachos
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Despachos
    Public NotInheritable Class PersistenciaRemesas
        Implements IPersistenciaBase(Of Remesas)

        Public Function Consultar(filtro As Remesas) As IEnumerable(Of Remesas) Implements IPersistenciaBase(Of Remesas).Consultar
            Return New RepositorioRemesas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Remesas) As Long Implements IPersistenciaBase(Of Remesas).Insertar
            Return New RepositorioRemesas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Remesas) As Long Implements IPersistenciaBase(Of Remesas).Modificar
            Return New RepositorioRemesas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Remesas) As Remesas Implements IPersistenciaBase(Of Remesas).Obtener
            Return New RepositorioRemesas().Obtener(filtro)
        End Function

        Public Function ConsultarRemesasPendientesFacturar(filtro As Remesas) As IEnumerable(Of Remesas)
            Return New RepositorioRemesas().ConsultarRemesasPendientesFacturar(filtro)
        End Function
        Public Function ActualizarRemesasExcel(filtro As Remesas) As Long
            Return New RepositorioRemesas().ActualizarRemesasExcel(filtro)
        End Function
        Public Function ConsultarNumerosRemesas(entidad As Remesas) As IEnumerable(Of Remesas)
            Return New RepositorioRemesas().ConsultarNumerosRemesas(entidad)
        End Function

        Public Function CumplirRemesas(entidad As Remesas) As Boolean
            Return New RepositorioRemesas().CumplirRemesas(entidad)
        End Function

        Public Function Anular(entidad As Remesas) As Remesas
            Return New RepositorioRemesas().Anular(entidad)
        End Function

        Public Function ActualizarDistribuido(entidad As Remesas) As Remesas
            Return New RepositorioRemesas().ActualizarDistribuido(entidad)
        End Function

        Public Function MarcarRemesasFacturadas(entidad As Remesas) As Remesas
            Return New RepositorioRemesas().MarcarRemesasFacturadas(entidad)
        End Function

        Public Function ObtenerPrecintosAleatorios(entidad As Remesas) As IEnumerable(Of DetallePrecintos)
            Return New RepositorioRemesas().ObtenerPrecintosAleatorios(entidad)
        End Function
        Public Function ConsultarRemesasPlanillaDespachos(filtro As Remesas) As IEnumerable(Of Remesas)
            Return New RepositorioRemesas().ConsultarRemesasPlanillaDespachos(filtro)
        End Function
        Public Function ConsultarCumplidoRemesas(filtro As Remesas) As IEnumerable(Of Remesas)
            Return New RepositorioRemesas().ConsultarCumplidoRemesas(filtro)
        End Function
    End Class

End Namespace

