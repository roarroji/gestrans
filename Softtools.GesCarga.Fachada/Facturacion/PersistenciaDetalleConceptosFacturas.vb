﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Repositorio.Facturacion

Namespace Facturacion
    Public NotInheritable Class PersistenciaDetalleConceptosFacturas
        Implements IPersistenciaBase(Of DetalleConceptosFacturas)

        Public Function Consultar(filtro As DetalleConceptosFacturas) As IEnumerable(Of DetalleConceptosFacturas) Implements IPersistenciaBase(Of DetalleConceptosFacturas).Consultar
            Return New RepositorioDetalleConceptosFacturas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleConceptosFacturas) As Long Implements IPersistenciaBase(Of DetalleConceptosFacturas).Insertar
            Throw New NotImplementedException()
        End Function

        Public Function Modificar(entidad As DetalleConceptosFacturas) As Long Implements IPersistenciaBase(Of DetalleConceptosFacturas).Modificar
            Throw New NotImplementedException()
        End Function

        Public Function Obtener(filtro As DetalleConceptosFacturas) As DetalleConceptosFacturas Implements IPersistenciaBase(Of DetalleConceptosFacturas).Obtener
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
