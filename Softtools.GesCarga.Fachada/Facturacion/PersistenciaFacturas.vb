﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Repositorio.Facturacion

Namespace Facturacion
    Public NotInheritable Class PersistenciaFacturas
        Implements IPersistenciaBase(Of Facturas)

        Public Function Consultar(filtro As Facturas) As IEnumerable(Of Facturas) Implements IPersistenciaBase(Of Facturas).Consultar
            Return New RepositorioFacturas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Facturas) As Long Implements IPersistenciaBase(Of Facturas).Insertar
            Return New RepositorioFacturas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Facturas) As Long Implements IPersistenciaBase(Of Facturas).Modificar
            Return New RepositorioFacturas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Facturas) As Facturas Implements IPersistenciaBase(Of Facturas).Obtener
            Return New RepositorioFacturas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As Facturas) As Boolean
            Return New RepositorioFacturas().Anular(entidad)
        End Function
        Public Function ObtenerDatosFacturaElectronicaSaphety(filtro As Facturas) As Facturas
            Return New RepositorioFacturas().ObtenerDatosFacturaElectronicaSaphety(filtro)
        End Function
        Public Function GuardarFacturaElectronica(entidad As Facturas) As Long
            Return New RepositorioFacturas().GuardarFacturaElectronica(entidad)
        End Function
        Public Function ObtenerDocumentoReporteSaphety(filtro As Facturas) As Facturas
            Return New RepositorioFacturas().ObtenerDocumentoReporteSaphety(filtro)
        End Function
    End Class

End Namespace

