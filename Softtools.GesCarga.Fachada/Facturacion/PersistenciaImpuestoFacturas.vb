﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Repositorio.Facturacion

Namespace Facturacion
    Public NotInheritable Class PersistenciaImpuestoFacturas
        Implements IPersistenciaBase(Of ImpuestoFacturas)

        Public Function Consultar(filtro As ImpuestoFacturas) As IEnumerable(Of ImpuestoFacturas) Implements IPersistenciaBase(Of ImpuestoFacturas).Consultar
            Return New RepositorioImpuestoFacturas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ImpuestoFacturas) As Long Implements IPersistenciaBase(Of ImpuestoFacturas).Insertar
            Return New RepositorioImpuestoFacturas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ImpuestoFacturas) As Long Implements IPersistenciaBase(Of ImpuestoFacturas).Modificar
            Return New RepositorioImpuestoFacturas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ImpuestoFacturas) As ImpuestoFacturas Implements IPersistenciaBase(Of ImpuestoFacturas).Obtener
            Return New RepositorioImpuestoFacturas().Obtener(filtro)
        End Function
    End Class
End Namespace
