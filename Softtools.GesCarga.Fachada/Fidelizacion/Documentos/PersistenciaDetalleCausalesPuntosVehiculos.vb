﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Fidelizacion.Documentos
Imports Softtools.GesCarga.Repositorio.Fidelizacion.Documentos

Namespace Fidelizacion.Documentos
    Public NotInheritable Class PersistenciaDetalleCausalesPuntosVehiculos
        Implements IPersistenciaBase(Of DetalleCausalesPuntosVehiculos)

        Public Function Consultar(filtro As DetalleCausalesPuntosVehiculos) As IEnumerable(Of DetalleCausalesPuntosVehiculos) Implements IPersistenciaBase(Of DetalleCausalesPuntosVehiculos).Consultar
            Return New RepositorioDetalleCausalesPuntosVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleCausalesPuntosVehiculos) As Long Implements IPersistenciaBase(Of DetalleCausalesPuntosVehiculos).Insertar
            Return New RepositorioDetalleCausalesPuntosVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleCausalesPuntosVehiculos) As Long Implements IPersistenciaBase(Of DetalleCausalesPuntosVehiculos).Modificar
            Return New RepositorioDetalleCausalesPuntosVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleCausalesPuntosVehiculos) As DetalleCausalesPuntosVehiculos Implements IPersistenciaBase(Of DetalleCausalesPuntosVehiculos).Obtener
            Return New RepositorioDetalleCausalesPuntosVehiculos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleCausalesPuntosVehiculos) As Boolean
            Return New RepositorioDetalleCausalesPuntosVehiculos().Anular(entidad)
        End Function
    End Class
End Namespace

