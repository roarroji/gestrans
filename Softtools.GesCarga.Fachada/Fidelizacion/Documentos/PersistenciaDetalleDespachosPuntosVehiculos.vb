﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Fidelizacion.Documentos
Imports Softtools.GesCarga.Repositorio.Fidelizacion.Documentos

Namespace Fidelizacion.Documentos
    Public NotInheritable Class PersistenciaDetalleDespachosPuntosVehiculos
        Implements IPersistenciaBase(Of DetalleDespachosPuntosVehiculos)

        Public Function Consultar(filtro As DetalleDespachosPuntosVehiculos) As IEnumerable(Of DetalleDespachosPuntosVehiculos) Implements IPersistenciaBase(Of DetalleDespachosPuntosVehiculos).Consultar
            Return New RepositorioDetalleDespachosPuntosVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleDespachosPuntosVehiculos) As Long Implements IPersistenciaBase(Of DetalleDespachosPuntosVehiculos).Insertar
            Return New RepositorioDetalleDespachosPuntosVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleDespachosPuntosVehiculos) As Long Implements IPersistenciaBase(Of DetalleDespachosPuntosVehiculos).Modificar
            Return New RepositorioDetalleDespachosPuntosVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleDespachosPuntosVehiculos) As DetalleDespachosPuntosVehiculos Implements IPersistenciaBase(Of DetalleDespachosPuntosVehiculos).Obtener
            Return New RepositorioDetalleDespachosPuntosVehiculos().Obtener(filtro)
        End Function

        Public Function Anular(entidad As DetalleDespachosPuntosVehiculos) As Boolean
            Return New RepositorioDetalleDespachosPuntosVehiculos().Anular(entidad)
        End Function
    End Class
End Namespace