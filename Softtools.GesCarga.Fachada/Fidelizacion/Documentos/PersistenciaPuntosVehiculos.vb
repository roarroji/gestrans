﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Fidelizacion.Documentos
Imports Softtools.GesCarga.Repositorio.Fidelizacion.Documentos

Namespace Fidelizacion.Documentos
    Public NotInheritable Class PersistenciaPuntosVehiculos
        Implements IPersistenciaBase(Of PuntosVehiculos)

        Public Function Consultar(filtro As PuntosVehiculos) As IEnumerable(Of PuntosVehiculos) Implements IPersistenciaBase(Of PuntosVehiculos).Consultar
            Return New RepositorioPuntosVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As PuntosVehiculos) As Long Implements IPersistenciaBase(Of PuntosVehiculos).Insertar
            Return New RepositorioPuntosVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PuntosVehiculos) As Long Implements IPersistenciaBase(Of PuntosVehiculos).Modificar
            Return New RepositorioPuntosVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PuntosVehiculos) As PuntosVehiculos Implements IPersistenciaBase(Of PuntosVehiculos).Obtener
            Return New RepositorioPuntosVehiculos().Obtener(filtro)
        End Function
    End Class
End Namespace

