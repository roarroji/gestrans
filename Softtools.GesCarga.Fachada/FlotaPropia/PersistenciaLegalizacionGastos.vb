﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.FlotaPropia
Imports Softtools.GesCarga.Repositorio.FlotaPropia

Namespace FlotaPropia
    Public NotInheritable Class PersistenciaLegalizacionGastos
        Implements IPersistenciaBase(Of LegalizacionGastos)

        Public Function Consultar(filtro As LegalizacionGastos) As IEnumerable(Of LegalizacionGastos) Implements IPersistenciaBase(Of LegalizacionGastos).Consultar
            Return New RepositorioLegalizacionGastos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As LegalizacionGastos) As Long Implements IPersistenciaBase(Of LegalizacionGastos).Insertar
            Return New RepositorioLegalizacionGastos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As LegalizacionGastos) As Long Implements IPersistenciaBase(Of LegalizacionGastos).Modificar
            Return New RepositorioLegalizacionGastos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As LegalizacionGastos) As LegalizacionGastos Implements IPersistenciaBase(Of LegalizacionGastos).Obtener
            Return New RepositorioLegalizacionGastos().Obtener(filtro)
        End Function

        Public Function Anular(entidad As LegalizacionGastos) As Boolean
            Return New RepositorioLegalizacionGastos().Anular(entidad)
        End Function

        Public Function GuardarFacturaTerpel(entidad As DetalleLegalizacionGastos) As Boolean
            Return New RepositorioLegalizacionGastos().GuardarFacturaTerpel(entidad)
        End Function

    End Class

End Namespace

