﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.FlotaPropia
Imports Softtools.GesCarga.Repositorio.FlotaPropia

Namespace FlotaPropia

    Public NotInheritable Class PersistenciaPlanillasDespachosOtrasEmpresas
        Implements IPersistenciaBase(Of PlanillaDespachosOtrasEmpresas)

        Public Function Consultar(filtro As PlanillaDespachosOtrasEmpresas) As IEnumerable(Of PlanillaDespachosOtrasEmpresas) Implements IPersistenciaBase(Of PlanillaDespachosOtrasEmpresas).Consultar
            Return New RepositorioPlanillasDespachosOtrasEmpresas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As PlanillaDespachosOtrasEmpresas) As Long Implements IPersistenciaBase(Of PlanillaDespachosOtrasEmpresas).Insertar
            Return New RepositorioPlanillasDespachosOtrasEmpresas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PlanillaDespachosOtrasEmpresas) As Long Implements IPersistenciaBase(Of PlanillaDespachosOtrasEmpresas).Modificar
            Return New RepositorioPlanillasDespachosOtrasEmpresas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PlanillaDespachosOtrasEmpresas) As PlanillaDespachosOtrasEmpresas Implements IPersistenciaBase(Of PlanillaDespachosOtrasEmpresas).Obtener
            Return New RepositorioPlanillasDespachosOtrasEmpresas().Obtener(filtro)
        End Function
        Public Function Anular(entidad As PlanillaDespachosOtrasEmpresas) As Boolean
            Return New RepositorioPlanillasDespachosOtrasEmpresas().Anular(entidad)
        End Function

    End Class
End Namespace
