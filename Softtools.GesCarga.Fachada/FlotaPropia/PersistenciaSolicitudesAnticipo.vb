﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.FlotaPropia
Imports Softtools.GesCarga.Repositorio.FlotaPropia

Namespace FlotaPropia
    Public Class PersistenciaSolicitudesAnticipo
        Implements IPersistenciaBase(Of EncabezadoSolicitudAnticipo)

        Public Function Consultar(filtro As EncabezadoSolicitudAnticipo) As IEnumerable(Of EncabezadoSolicitudAnticipo) Implements IPersistenciaBase(Of EncabezadoSolicitudAnticipo).Consultar
            Return New RepositorioSolicitudesAnticipo().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoSolicitudAnticipo) As Long Implements IPersistenciaBase(Of EncabezadoSolicitudAnticipo).Insertar
            Return New RepositorioSolicitudesAnticipo().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoSolicitudAnticipo) As Long Implements IPersistenciaBase(Of EncabezadoSolicitudAnticipo).Modificar
            Return New RepositorioSolicitudesAnticipo().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoSolicitudAnticipo) As EncabezadoSolicitudAnticipo Implements IPersistenciaBase(Of EncabezadoSolicitudAnticipo).Obtener
            Return New RepositorioSolicitudesAnticipo().Obtener(filtro)
        End Function

        Public Function Anular(entidad As EncabezadoSolicitudAnticipo) As Boolean
            Return New RepositorioSolicitudesAnticipo().Anular(entidad)
        End Function


        Public Function ConsultarListaDetalles(entidad As DetalleSolicitudAnticipo) As IEnumerable(Of DetalleSolicitudAnticipo)
            Return New RepositorioSolicitudesAnticipo().ConsultarListaDetalles(entidad)
        End Function
    End Class
End Namespace