﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Gesphone
Imports Softtools.GesCarga.Repositorio.Gesphone

Namespace Gesphone
    Public NotInheritable Class PersistenciaDetalleCheckListVehiculos
        Implements IPersistenciaBase(Of DetalleCheckListVehiculos)

        Public Function Consultar(filtro As DetalleCheckListVehiculos) As IEnumerable(Of DetalleCheckListVehiculos) Implements IPersistenciaBase(Of DetalleCheckListVehiculos).Consultar
            Return New RepositorioDetalleCheckListVehiculos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleCheckListVehiculos) As Long Implements IPersistenciaBase(Of DetalleCheckListVehiculos).Insertar
            Return New RepositorioDetalleCheckListVehiculos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleCheckListVehiculos) As Long Implements IPersistenciaBase(Of DetalleCheckListVehiculos).Modificar
            Return New RepositorioDetalleCheckListVehiculos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleCheckListVehiculos) As DetalleCheckListVehiculos Implements IPersistenciaBase(Of DetalleCheckListVehiculos).Obtener
            Return New RepositorioDetalleCheckListVehiculos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleCheckListVehiculos) As Boolean
            Return New RepositorioDetalleCheckListVehiculos().Anular(entidad)
        End Function
    End Class

End Namespace

