﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Imports Softtools.GesCarga.Repositorio.Paqueteria
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Paqueteria
    Public NotInheritable Class PersistenciaCargueMasivoRecolecciones
        Implements IPersistenciaBase(Of Recolecciones)

        Public Function Consultar(filtro As Recolecciones) As IEnumerable(Of Recolecciones) Implements IPersistenciaBase(Of Recolecciones).Consultar
            Throw New NotImplementedException
        End Function
        Public Function Insertar(entidad As Recolecciones) As Long Implements IPersistenciaBase(Of Recolecciones).Insertar
            Throw New NotImplementedException
        End Function

        Public Function Modificar(entidad As Recolecciones) As Long Implements IPersistenciaBase(Of Recolecciones).Modificar
            Throw New NotImplementedException
        End Function

        Public Function Obtener(filtro As Recolecciones) As Recolecciones Implements IPersistenciaBase(Of Recolecciones).Obtener
            Throw New NotImplementedException
        End Function

        Public Function GenerarPlantilla(filtro As Recolecciones) As Recolecciones
            Return New RepositorioCargueMasivoRecolecciones().GenerarPlantilla(filtro)
        End Function

        Public Function InsertarMasivo(filtro As Recolecciones) As Long
            Return New RepositorioCargueMasivoRecolecciones().InsertarMasivo(filtro)
        End Function

        Public Function ConsultarTercerosIdentificacion(filtro As Terceros) As IEnumerable(Of Terceros)
            Return New RepositorioCargueMasivoRecolecciones().ConsultarTercerosIdentificacion(filtro)
        End Function


        Public Function ObtenerUltimoConsecutivoTercero(filtro As Terceros) As Terceros
            Return New RepositorioCargueMasivoRecolecciones().ObtenerUltimoConsecutivoTercero(filtro)
        End Function


        Public Function ObtenerUltimaRecoleccion(filtro As Recolecciones) As Recolecciones
            Return New RepositorioCargueMasivoRecolecciones().ObtenerUltimaRecoleccion(filtro)
        End Function

        Public Function InsertarNuevosTerceros(filtro As Terceros) As Long
            Return New RepositorioCargueMasivoRecolecciones().InsertarNuevosTerceros(filtro)
        End Function
    End Class

End Namespace

