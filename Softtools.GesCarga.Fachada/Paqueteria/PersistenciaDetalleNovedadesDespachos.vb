﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Imports Softtools.GesCarga.Repositorio.Despachos.Paqueteria

Namespace Despachos.Paqueteria
    Public NotInheritable Class PersistenciaDetalleNovedadesDespachos
        Implements IPersistenciaBase(Of DetalleNovedadesDespachos)

        Public Function Consultar(filtro As DetalleNovedadesDespachos) As IEnumerable(Of DetalleNovedadesDespachos) Implements IPersistenciaBase(Of DetalleNovedadesDespachos).Consultar
            Return New RepositorioDetalleNovedadesDespachos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleNovedadesDespachos) As Long Implements IPersistenciaBase(Of DetalleNovedadesDespachos).Insertar
            Return New RepositorioDetalleNovedadesDespachos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleNovedadesDespachos) As Long Implements IPersistenciaBase(Of DetalleNovedadesDespachos).Modificar
            Throw New NotImplementedException()
        End Function

        Public Function Obtener(filtro As DetalleNovedadesDespachos) As DetalleNovedadesDespachos Implements IPersistenciaBase(Of DetalleNovedadesDespachos).Obtener
            Return New RepositorioDetalleNovedadesDespachos().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleNovedadesDespachos) As Boolean
            Return New RepositorioDetalleNovedadesDespachos().Anular(entidad)
        End Function

    End Class

End Namespace

