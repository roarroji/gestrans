﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Repositorio.Paqueteria

Namespace Paqueteria
    Public Class PersistenciaLegalizarRecaudoRemesas
        Implements IPersistenciaBase(Of LegalizarRecaudoRemesas)

        Public Function Consultar(filtro As LegalizarRecaudoRemesas) As IEnumerable(Of LegalizarRecaudoRemesas) Implements IPersistenciaBase(Of LegalizarRecaudoRemesas).Consultar
            Return New RepositorioLegalizarRecaudoRemesas().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As LegalizarRecaudoRemesas) As Long Implements IPersistenciaBase(Of LegalizarRecaudoRemesas).Insertar
            Return New RepositorioLegalizarRecaudoRemesas().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As LegalizarRecaudoRemesas) As Long Implements IPersistenciaBase(Of LegalizarRecaudoRemesas).Modificar
            Return New RepositorioLegalizarRecaudoRemesas().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As LegalizarRecaudoRemesas) As LegalizarRecaudoRemesas Implements IPersistenciaBase(Of LegalizarRecaudoRemesas).Obtener
            Return New RepositorioLegalizarRecaudoRemesas().Obtener(filtro)
        End Function

        Public Function EliminarGuia(filtro As DetalleLegalizarRecaudoRemesas) As Boolean
            Return New RepositorioLegalizarRecaudoRemesas().EliminarGuia(filtro)
        End Function
    End Class
End Namespace


