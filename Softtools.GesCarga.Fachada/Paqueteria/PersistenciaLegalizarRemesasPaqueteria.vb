﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Repositorio.Paqueteria

Namespace Paqueteria
    Public NotInheritable Class PersistenciaLegalizarRemesasPaqueteria
        Implements IPersistenciaBase(Of LegalizarRemesasPaqueteria)

        Public Function Consultar(filtro As LegalizarRemesasPaqueteria) As IEnumerable(Of LegalizarRemesasPaqueteria) Implements IPersistenciaBase(Of LegalizarRemesasPaqueteria).Consultar
            Return New RepositorioLegalizarRemesasPaqueteria().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As LegalizarRemesasPaqueteria) As Long Implements IPersistenciaBase(Of LegalizarRemesasPaqueteria).Insertar
            Return New RepositorioLegalizarRemesasPaqueteria().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As LegalizarRemesasPaqueteria) As Long Implements IPersistenciaBase(Of LegalizarRemesasPaqueteria).Modificar
            Return New RepositorioLegalizarRemesasPaqueteria().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As LegalizarRemesasPaqueteria) As LegalizarRemesasPaqueteria Implements IPersistenciaBase(Of LegalizarRemesasPaqueteria).Obtener
            Return New RepositorioLegalizarRemesasPaqueteria().Obtener(filtro)
        End Function

        Public Function EliminarRemesa(entidad As LegalizarRemesasPaqueteria) As Boolean
            Return New RepositorioLegalizarRemesasPaqueteria().EliminarRemesa(entidad)
        End Function

        Public Function ConsultarOtrasLegalizaciones(filtro As LegalizarRemesasPaqueteria) As IEnumerable(Of LegalizarRemesasPaqueteria)
            Return New RepositorioLegalizarRemesasPaqueteria().ConsultarOtrasLegalizaciones(filtro)
        End Function
    End Class

End Namespace
