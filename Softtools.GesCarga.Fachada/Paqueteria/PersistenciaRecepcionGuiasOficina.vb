﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Repositorio.Paqueteria

Namespace Paqueteria

    Public Class PersistenciaRecepcionGuiasOficina
        Implements IPersistenciaBase(Of RemesaPaqueteria)

        Public Function Consultar(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria) Implements IPersistenciaBase(Of RemesaPaqueteria).Consultar
            Throw New NotImplementedException()
        End Function

        Public Function Insertar(entidad As RemesaPaqueteria) As Long Implements IPersistenciaBase(Of RemesaPaqueteria).Insertar
            Throw New NotImplementedException()
        End Function

        Public Function Modificar(entidad As RemesaPaqueteria) As Long Implements IPersistenciaBase(Of RemesaPaqueteria).Modificar
            Throw New NotImplementedException()
        End Function

        Public Function Obtener(filtro As RemesaPaqueteria) As RemesaPaqueteria Implements IPersistenciaBase(Of RemesaPaqueteria).Obtener
            Throw New NotImplementedException()
        End Function

        Public Function RecibirGuias(entidad As RemesaPaqueteria) As Long
            Return New RepositorioRecepcionGuias().RecibirGuias(entidad)
        End Function
    End Class
End Namespace

