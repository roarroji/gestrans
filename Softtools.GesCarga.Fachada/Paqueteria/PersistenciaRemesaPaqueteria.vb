﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Repositorio.Paqueteria
Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Paqueteria
    Public NotInheritable Class PersistenciaRemesaPaqueteria
        Implements IPersistenciaBase(Of RemesaPaqueteria)

        Public Function Consultar(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria) Implements IPersistenciaBase(Of RemesaPaqueteria).Consultar
            Return New RepositorioRemesaPaqueteria().Consultar(filtro)
        End Function

        Public Function ConsultarGuiasEtiquetasPreimpresas(filtro As RemesaPaqueteria) As IEnumerable(Of DetalleGuiaPreimpresa)
            Return New RepositorioRemesaPaqueteria().ConsultarGuiasEtiquetasPreimpresas(filtro)
        End Function

        Public Function ConsultarGuiasPlanillaEntrega(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioRemesaPaqueteria().ConsultarGuiasPlanillaEntrega(filtro)
        End Function

        Public Function Insertar(entidad As RemesaPaqueteria) As Long Implements IPersistenciaBase(Of RemesaPaqueteria).Insertar
            Return New RepositorioRemesaPaqueteria().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As RemesaPaqueteria) As Long Implements IPersistenciaBase(Of RemesaPaqueteria).Modificar
            Return New RepositorioRemesaPaqueteria().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As RemesaPaqueteria) As RemesaPaqueteria Implements IPersistenciaBase(Of RemesaPaqueteria).Obtener
            Return New RepositorioRemesaPaqueteria().Obtener(filtro)
        End Function
        Public Function Anular(entidad As RemesaPaqueteria) As Boolean
            Return New RepositorioRemesaPaqueteria().Anular(entidad)
        End Function

        Public Function CumplirGuias(entidad As RemesaPaqueteria) As Boolean
            Return New RepositorioRemesaPaqueteria().CumplirGuias(entidad)
        End Function
        Public Function GenerarPlanitilla(filtro As RemesaPaqueteria) As RemesaPaqueteria
            Return New RepositorioRemesaPaqueteria().GenerarPlanitilla(filtro)
        End Function
        Public Function GuardarBitacoraGuias(Entidad As DetalleEstadosRemesa) As Long
            Return New RepositorioRemesaPaqueteria().GuardarBitacoraGuias(Entidad)
        End Function
        Public Function GuardarEntrega(Entidad As Remesas) As Long
            Return New RepositorioRemesaPaqueteria().GuardarEntrega(Entidad)
        End Function
        Public Function ConsultarIndicadores(Entidad As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioRemesaPaqueteria().ConsultarIndicadores(Entidad)
        End Function
        Public Function AsociarGuiaPlanilla(entidad As RemesaPaqueteria) As Boolean
            Return New RepositorioRemesaPaqueteria().AsociarGuiaPlanilla(entidad)
        End Function
        Public Function ConsultarBitacoraGuias(Entidad As RemesaPaqueteria) As IEnumerable(Of DetalleEstadosRemesa)
            Return New RepositorioRemesaPaqueteria().ConsultarBitacoraGuias(Entidad)
        End Function
        Public Function ConsultarEstadosRemesa(Entidad As RemesaPaqueteria) As IEnumerable(Of DetalleEstadosRemesa)
            Return New RepositorioRemesaPaqueteria().ConsultarEstadosRemesa(Entidad)
        End Function
        Public Function ObtenerControlEntregas(Entidad As RemesaPaqueteria) As RemesaPaqueteria
            Return New RepositorioRemesaPaqueteria().ObtenerControlEntregas(Entidad)
        End Function
        Public Function ConsultarRemesasPendientesControlEntregas(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioRemesaPaqueteria().ConsultarRemesasPendientesControlEntregas(filtro)
        End Function
        Public Function ConsultarRemesasPorPlanillar(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioRemesaPaqueteria().ConsultarRemesasPorPlanillar(filtro)
        End Function
        Public Function ConsultarRemesasRecogerOficina(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioRemesaPaqueteria().ConsultarRemesasRecogerOficina(filtro)
        End Function
        Public Function ConsultarRemesasRetornoContado(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioRemesaPaqueteria().ConsultarRemesasRetornoContado(filtro)
        End Function
        Public Function ValidarPreimpreso(entidad As RemesaPaqueteria) As Long
            Return New RepositorioRemesaPaqueteria().ValidarPreimpreso(entidad)
        End Function

        Public Function CambiarEstadoRemesasPaqueteria(entidad As RemesaPaqueteria) As Long
            Return New RepositorioRemesaPaqueteria().CambiarEstadoRemesasPaqueteria(entidad)
        End Function
        Public Function CambiarEstadoGuiaPaqueteria(entidad As RemesaPaqueteria) As Long
            Return New RepositorioRemesaPaqueteria().CambiarEstadoGuiaPaqueteria(entidad)
        End Function
        Public Function CambiarEstadoTrazabilidadGuia(entidad As RemesaPaqueteria) As Long
            Return New RepositorioRemesaPaqueteria().CambiarEstadoTrazabilidadGuia(entidad)
        End Function
        Public Function ConsultarRemesasPendientesRecibirOficina(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioRemesaPaqueteria().ConsultarRemesasPendientesRecibirOficina(filtro)
        End Function
        Public Function RecibirRemesaPaqueteriaOficinaDestino(entidad As RemesaPaqueteria) As Long
            Return New RepositorioRemesaPaqueteria().RecibirRemesaPaqueteriaOficinaDestino(entidad)
        End Function
        Public Function ConsultarRemesasPorLegalizar(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioRemesaPaqueteria().ConsultarRemesasPorLegalizar(filtro)
        End Function
        Public Function ConsultarRemesasPorLegalizarRecaudo(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Return New RepositorioRemesaPaqueteria().ConsultarRemesasPorLegalizarRecaudo(filtro)
        End Function


        Public Function ActualizarSecuencia(filtro As RemesaPaqueteria) As RemesaPaqueteria
            Return New RepositorioRecepcionGuias().ActualizarSecuencia(filtro)
        End Function
    End Class

End Namespace

