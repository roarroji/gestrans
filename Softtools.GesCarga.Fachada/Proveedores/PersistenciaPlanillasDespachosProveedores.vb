﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Proveedores
Imports Softtools.GesCarga.Repositorio.Proveedores

Namespace Proveedores

    Public NotInheritable Class PersistenciaPlanillasDespachosProveedores
        Implements IPersistenciaBase(Of PlanillasDespachosProveedores)

        Public Function Consultar(filtro As PlanillasDespachosProveedores) As IEnumerable(Of PlanillasDespachosProveedores) Implements IPersistenciaBase(Of PlanillasDespachosProveedores).Consultar
            Return New RepositorioPlanillasDespachosProveedores().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As PlanillasDespachosProveedores) As Long Implements IPersistenciaBase(Of PlanillasDespachosProveedores).Insertar
            Return New RepositorioPlanillasDespachosProveedores().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PlanillasDespachosProveedores) As Long Implements IPersistenciaBase(Of PlanillasDespachosProveedores).Modificar
            Return New RepositorioPlanillasDespachosProveedores().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PlanillasDespachosProveedores) As PlanillasDespachosProveedores Implements IPersistenciaBase(Of PlanillasDespachosProveedores).Obtener
            Return New RepositorioPlanillasDespachosProveedores().Obtener(filtro)
        End Function
        Public Function Anular(entidad As PlanillasDespachosProveedores) As Boolean
            Return New RepositorioPlanillasDespachosProveedores().Anular(entidad)
        End Function

    End Class
End Namespace