﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Repositorio.SeguridadUsuarios

Namespace SeguridadUsuarios
    Public NotInheritable Class PersistenciaEstudioSeguridadDocumentos
        Implements IPersistenciaBase(Of EstudioSeguridadDocumentos)

        Public Function Consultar(filtro As EstudioSeguridadDocumentos) As IEnumerable(Of EstudioSeguridadDocumentos) Implements IPersistenciaBase(Of EstudioSeguridadDocumentos).Consultar
            Return New RepositorioEstudioSeguridadDocumentos().Consultar(filtro)
        End Function
        Public Function Insertar(entidad As EstudioSeguridadDocumentos) As Long Implements IPersistenciaBase(Of EstudioSeguridadDocumentos).Insertar
            Return New RepositorioEstudioSeguridadDocumentos().Insertar(entidad)
        End Function
        Public Function Modificar(entidad As EstudioSeguridadDocumentos) As Long Implements IPersistenciaBase(Of EstudioSeguridadDocumentos).Modificar
            Return New RepositorioEstudioSeguridadDocumentos().Modificar(entidad)
        End Function
        Public Function Obtener(filtro As EstudioSeguridadDocumentos) As EstudioSeguridadDocumentos Implements IPersistenciaBase(Of EstudioSeguridadDocumentos).Obtener
            Return New RepositorioEstudioSeguridadDocumentos().Obtener(filtro)
        End Function
        Public Function InsertarTemporal(entidad As EstudioSeguridadDocumentos) As Long
            Return New RepositorioEstudioSeguridadDocumentos().InsertarTemporal(entidad)
        End Function
        Public Function EliminarDocumento(entidad As EstudioSeguridadDocumentos) As Boolean
            Return New RepositorioEstudioSeguridadDocumentos().EliminarDocumento(entidad)
        End Function
        Public Function EliminarDocumentoDefinitivo(entidad As EstudioSeguridadDocumentos) As Boolean
            Return New RepositorioEstudioSeguridadDocumentos().EliminarDocumentoDefinitivo(entidad)
        End Function
        Public Function LimpiarDocumentoTemporalUsuario(entidad As EstudioSeguridadDocumentos) As Boolean
            Return New RepositorioEstudioSeguridadDocumentos().LimpiarDocumentoTemporalUsuario(entidad)
        End Function

    End Class
End Namespace
