﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Seguridad
Imports Softtools.GesCarga.Repositorio.Seguridad

Namespace Seguridad
    Public NotInheritable Class PersistenciaTipoDocumentoEstudioSeguridad
        Implements IPersistenciaBase(Of TipoDocumentoEstudioSeguridad)
        Public Function Consultar(filtro As TipoDocumentoEstudioSeguridad) As IEnumerable(Of TipoDocumentoEstudioSeguridad) Implements IPersistenciaBase(Of TipoDocumentoEstudioSeguridad).Consultar
            Return New RepositorioTipoDocumentoEstudioSeguridad().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As TipoDocumentoEstudioSeguridad) As Long Implements IPersistenciaBase(Of TipoDocumentoEstudioSeguridad).Insertar
            Return New RepositorioTipoDocumentoEstudioSeguridad().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As TipoDocumentoEstudioSeguridad) As Long Implements IPersistenciaBase(Of TipoDocumentoEstudioSeguridad).Modificar
            Return New RepositorioTipoDocumentoEstudioSeguridad().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As TipoDocumentoEstudioSeguridad) As TipoDocumentoEstudioSeguridad Implements IPersistenciaBase(Of TipoDocumentoEstudioSeguridad).Obtener
            Return New RepositorioTipoDocumentoEstudioSeguridad().Obtener(filtro)
        End Function

    End Class
End Namespace
