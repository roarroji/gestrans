﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Repositorio.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades.Utilitarios

Namespace SeguridadUsuarios
    Public NotInheritable Class PersistenciaPermisoGrupoUsuarios
        Implements IPersistenciaBase(Of PermisoGrupoUsuarios)
        Public Function Consultar(filtro As PermisoGrupoUsuarios) As IEnumerable(Of PermisoGrupoUsuarios) Implements IPersistenciaBase(Of PermisoGrupoUsuarios).Consultar
            Return New RepositorioPermisoGrupoUsuarios().Consultar(filtro)
        End Function

        Public Function ConsultarModulo(codigoEmpresa As Short, Codigo As Long) As IEnumerable(Of ModuloAplicaciones)
            Return New RepositorioPermisoGrupoUsuarios().ConsultarModulo(codigoEmpresa, Codigo)
        End Function

        Public Function Insertar(entidad As PermisoGrupoUsuarios) As Long Implements IPersistenciaBase(Of PermisoGrupoUsuarios).Insertar
            Return New RepositorioPermisoGrupoUsuarios().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As PermisoGrupoUsuarios) As Long Implements IPersistenciaBase(Of PermisoGrupoUsuarios).Modificar
            Return New RepositorioPermisoGrupoUsuarios().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As PermisoGrupoUsuarios) As PermisoGrupoUsuarios Implements IPersistenciaBase(Of PermisoGrupoUsuarios).Obtener
            Return New RepositorioPermisoGrupoUsuarios().Obtener(filtro)
        End Function
        Public Function GenerarPlanitilla(filtro As PermisoGrupoUsuarios) As PermisoGrupoUsuarios
            Return New RepositorioPermisoGrupoUsuarios().GenerarPlanitilla(filtro)
        End Function

        Public Function GuardarAsigancionPerfilesNotificaciones(filtro As NotificacionGrupoUsuarios) As Long
            Return New RepositorioPermisoGrupoUsuarios().GuardarAsigancionPerfilesNotificaciones(filtro)
        End Function
        Public Function ConsultarAsigancionPerfilesNotificaciones(filtro As NotificacionGrupoUsuarios) As IEnumerable(Of DetalleGrupoUsuariosNotificaciones)
            Return New RepositorioPermisoGrupoUsuarios().ConsultarAsigancionPerfilesNotificaciones(filtro)
        End Function
    End Class
End Namespace
