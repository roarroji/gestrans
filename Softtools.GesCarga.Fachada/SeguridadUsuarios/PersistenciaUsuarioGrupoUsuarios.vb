﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Repositorio.SeguridadUsuarios

Namespace SeguridadUsuarios
    Public NotInheritable Class PersistenciaUsuarioGrupoUsuarios
        Implements IPersistenciaBase(Of UsuarioGrupoUsuarios)
        Public Function Consultar(filtro As UsuarioGrupoUsuarios) As IEnumerable(Of UsuarioGrupoUsuarios) Implements IPersistenciaBase(Of UsuarioGrupoUsuarios).Consultar
            Return New RepositorioUsuarioGrupoUsuarios().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As UsuarioGrupoUsuarios) As Long Implements IPersistenciaBase(Of UsuarioGrupoUsuarios).Insertar
            Return New RepositorioUsuarioGrupoUsuarios().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As UsuarioGrupoUsuarios) As Long Implements IPersistenciaBase(Of UsuarioGrupoUsuarios).Modificar
            Return New RepositorioUsuarioGrupoUsuarios().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As UsuarioGrupoUsuarios) As UsuarioGrupoUsuarios Implements IPersistenciaBase(Of UsuarioGrupoUsuarios).Obtener
            Return New RepositorioUsuarioGrupoUsuarios().Obtener(filtro)
        End Function
        Public Function Eliminar(filtro As UsuarioGrupoUsuarios) As Boolean
            Return New RepositorioUsuarioGrupoUsuarios().Eliminar(filtro)
        End Function
    End Class
End Namespace
