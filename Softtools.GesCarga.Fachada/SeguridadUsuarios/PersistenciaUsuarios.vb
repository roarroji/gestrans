﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Repositorio.SeguridadUsuarios

Namespace SeguridadUsuarios
    Public NotInheritable Class PersistenciaUsuarios
        Implements IPersistenciaBase(Of Usuarios)
        Public Function Consultar(filtro As Usuarios) As IEnumerable(Of Usuarios) Implements IPersistenciaBase(Of Usuarios).Consultar
            Return New RepositorioUsuarios().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As Usuarios) As Long Implements IPersistenciaBase(Of Usuarios).Insertar
            Return New RepositorioUsuarios().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As Usuarios) As Long Implements IPersistenciaBase(Of Usuarios).Modificar
            Return New RepositorioUsuarios().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As Usuarios) As Usuarios Implements IPersistenciaBase(Of Usuarios).Obtener
            Return New RepositorioUsuarios().Obtener(filtro)
        End Function

        Public Function Validar(codigoEmpresa As Short, CodigoUsuario As String) As Usuarios
            Return New RepositorioUsuarios().Validar(codigoEmpresa, CodigoUsuario)
        End Function

        'Public Function ObtenerHorario(codigoEmpresa As Short, codigoUsuario As Short, dia As Short) As UsuarioHorario
        '    Return New RepositorioUsuarios().ObtenerHorario(codigoEmpresa, codigoUsuario, dia)
        'End Function

        Public Function ContarUsuariosConectados(codigoEmpresa As Short) As Short
            Return New RepositorioUsuarios().ContarUsuariosConectados(codigoEmpresa)
        End Function

        Public Function ConsultarUsuarioGrupoUsuarios(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of UsuarioGrupoUsuarios)
            Return New RepositorioUsuarios().ConsultarUsuarioGrupoUsuarios(codigoEmpresa, codigoUsuario)
        End Function
        Public Function GuardarIngresoUsuario(codigoEmpresa As Short, codigoUsuario As Short) As Boolean
            Return New RepositorioUsuarios().GuardarIngresoUsuario(codigoEmpresa, codigoUsuario)
        End Function
        Public Function ActualizarIntentosIngreso(codigoEmpresa As Short, codigoUsuario As Short) As Short
            Return New RepositorioUsuarios().ActualizarIntentosIngreso(codigoEmpresa, codigoUsuario)
        End Function

        Public Function DeshabilitarUsuario(codigoEmpresa As Short, codigoUsuario As Short) As Boolean
            Return New RepositorioUsuarios().DeshabilitarUsuario(codigoEmpresa, codigoUsuario)
        End Function

        Public Function InsertarUsuarioGrupoSeguridades(detalle As IEnumerable(Of GrupoUsuarios))
            Return New RepositorioUsuarios().InsertarUsuarioGrupoSeguridades(detalle)
        End Function

        'Public Function InsertarHorario(detalle As IEnumerable(Of UsuarioHorario))
        '    Return New RepositorioUsuarios().InsertarUsuarioHorarios(detalle)
        'End Function

        'Public Function ConsultarHorario(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of UsuarioHorario)
        '    Return New RepositorioUsuarios().ConsultarUsuarioHorario(codigoEmpresa, codigoUsuario)
        'End Function

        Public Function CerrarSesion(codigoEmpresa As Short, codigoUsuario As Short) As Boolean
            Return New RepositorioUsuarios().CerrarSesion(codigoEmpresa, codigoUsuario)
        End Function

        Public Function ConsultarUsuarioOficinas(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of UsuarioOficinas)
            Return New RepositorioUsuarios().ConsultarUsuarioOficinas(codigoEmpresa, codigoUsuario)
        End Function

        Public Function InsertarUsuarioOficina(detalle As IEnumerable(Of UsuarioOficinas))
            Return New RepositorioUsuarios().InsertarUsuarioOficinas(detalle)
        End Function

        Public Function RegistrarActividad(entidad As Usuarios) As Boolean
            Return New RepositorioUsuarios().RegistrarActividad(entidad)
        End Function
    End Class
End Namespace
