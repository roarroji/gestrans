﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Repositorio.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class PersistenciaDetalleConceptoSolicitudOrdenSericios
        Implements IPersistenciaBase(Of DetalleConceptoSolicitudOrdenServicios)

        Public Function Consultar(filtro As DetalleConceptoSolicitudOrdenServicios) As IEnumerable(Of DetalleConceptoSolicitudOrdenServicios) Implements IPersistenciaBase(Of DetalleConceptoSolicitudOrdenServicios).Consultar
            Return New RepositorioDetalleConceptoSolicitudOrdenServicios().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleConceptoSolicitudOrdenServicios) As Long Implements IPersistenciaBase(Of DetalleConceptoSolicitudOrdenServicios).Insertar
            Return New RepositorioDetalleConceptoSolicitudOrdenServicios().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleConceptoSolicitudOrdenServicios) As Long Implements IPersistenciaBase(Of DetalleConceptoSolicitudOrdenServicios).Modificar
            Return New RepositorioDetalleConceptoSolicitudOrdenServicios().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleConceptoSolicitudOrdenServicios) As DetalleConceptoSolicitudOrdenServicios Implements IPersistenciaBase(Of DetalleConceptoSolicitudOrdenServicios).Obtener
            Return New RepositorioDetalleConceptoSolicitudOrdenServicios().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleConceptoSolicitudOrdenServicios) As Boolean
            Return New RepositorioDetalleConceptoSolicitudOrdenServicios().Anular(entidad)
        End Function
    End Class

End Namespace

