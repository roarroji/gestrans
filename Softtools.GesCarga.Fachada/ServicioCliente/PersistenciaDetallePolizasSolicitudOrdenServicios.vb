﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Repositorio.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class PersistenciaDetallePolizasSolicitudOrdenServicios
        Implements IPersistenciaBase(Of DetallePolizasSolicitudOrdenServicios)

        Public Function Consultar(filtro As DetallePolizasSolicitudOrdenServicios) As IEnumerable(Of DetallePolizasSolicitudOrdenServicios) Implements IPersistenciaBase(Of DetallePolizasSolicitudOrdenServicios).Consultar
            Return New RepositorioDetallePolizasSolicitudOrdenServicios().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetallePolizasSolicitudOrdenServicios) As Long Implements IPersistenciaBase(Of DetallePolizasSolicitudOrdenServicios).Insertar
            Return New RepositorioDetallePolizasSolicitudOrdenServicios().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetallePolizasSolicitudOrdenServicios) As Long Implements IPersistenciaBase(Of DetallePolizasSolicitudOrdenServicios).Modificar
            Return New RepositorioDetallePolizasSolicitudOrdenServicios().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetallePolizasSolicitudOrdenServicios) As DetallePolizasSolicitudOrdenServicios Implements IPersistenciaBase(Of DetallePolizasSolicitudOrdenServicios).Obtener
            Return New RepositorioDetallePolizasSolicitudOrdenServicios().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetallePolizasSolicitudOrdenServicios) As Boolean
            Return New RepositorioDetallePolizasSolicitudOrdenServicios().Anular(entidad)
        End Function
    End Class

End Namespace

