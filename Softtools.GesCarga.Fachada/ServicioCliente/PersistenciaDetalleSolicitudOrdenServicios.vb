﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Repositorio.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class PersistenciaDetalleSolicitudOrdenServicios
        Implements IPersistenciaBase(Of DetalleSolicitudOrdenServicios)

        Public Function Consultar(filtro As DetalleSolicitudOrdenServicios) As IEnumerable(Of DetalleSolicitudOrdenServicios) Implements IPersistenciaBase(Of DetalleSolicitudOrdenServicios).Consultar
            Return New RepositorioDetalleSolicitudOrdenServicios().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleSolicitudOrdenServicios) As Long Implements IPersistenciaBase(Of DetalleSolicitudOrdenServicios).Insertar
            Return New RepositorioDetalleSolicitudOrdenServicios().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleSolicitudOrdenServicios) As Long Implements IPersistenciaBase(Of DetalleSolicitudOrdenServicios).Modificar
            Return New RepositorioDetalleSolicitudOrdenServicios().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleSolicitudOrdenServicios) As DetalleSolicitudOrdenServicios Implements IPersistenciaBase(Of DetalleSolicitudOrdenServicios).Obtener
            Return New RepositorioDetalleSolicitudOrdenServicios().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleSolicitudOrdenServicios) As Boolean
            Return New RepositorioDetalleSolicitudOrdenServicios().Anular(entidad)
        End Function
    End Class

End Namespace

