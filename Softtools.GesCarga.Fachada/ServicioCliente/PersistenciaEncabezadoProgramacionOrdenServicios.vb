﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Repositorio.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class PersistenciaEncabezadoProgramacionOrdenServicios
        Implements IPersistenciaBase(Of EncabezadoProgramacionOrdenServicios)

        Public Function Consultar(filtro As EncabezadoProgramacionOrdenServicios) As IEnumerable(Of EncabezadoProgramacionOrdenServicios) Implements IPersistenciaBase(Of EncabezadoProgramacionOrdenServicios).Consultar
            Return New RepositorioEncabezadoProgramacionOrdenServicios().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoProgramacionOrdenServicios) As Long Implements IPersistenciaBase(Of EncabezadoProgramacionOrdenServicios).Insertar
            Return New RepositorioEncabezadoProgramacionOrdenServicios().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoProgramacionOrdenServicios) As Long Implements IPersistenciaBase(Of EncabezadoProgramacionOrdenServicios).Modificar
            Return New RepositorioEncabezadoProgramacionOrdenServicios().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoProgramacionOrdenServicios) As EncabezadoProgramacionOrdenServicios Implements IPersistenciaBase(Of EncabezadoProgramacionOrdenServicios).Obtener
            Return New RepositorioEncabezadoProgramacionOrdenServicios().Obtener(filtro)
        End Function
        Public Function Anular(entidad As EncabezadoProgramacionOrdenServicios) As EncabezadoProgramacionOrdenServicios
            Return New RepositorioEncabezadoProgramacionOrdenServicios().Anular(entidad)
        End Function

        Public Function EliminarDetalle(entidad As DetalleProgramacionOrdenServicios) As Long
            Return New RepositorioEncabezadoProgramacionOrdenServicios().EliminarDetalle(entidad)
        End Function
    End Class

End Namespace

