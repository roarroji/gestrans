﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Repositorio.Tesoreria

Namespace Tesoreria
    Public NotInheritable Class PersistenciaDetalleDocumentoCausaciones
        Implements IPersistenciaBase(Of DetalleDocumentoCausaciones)

        Public Function Consultar(filtro As DetalleDocumentoCausaciones) As IEnumerable(Of DetalleDocumentoCausaciones) Implements IPersistenciaBase(Of DetalleDocumentoCausaciones).Consultar
            Return New RepositorioDetalleDocumentoCausaciones().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleDocumentoCausaciones) As Long Implements IPersistenciaBase(Of DetalleDocumentoCausaciones).Insertar
            Return New RepositorioDetalleDocumentoCausaciones().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleDocumentoCausaciones) As Long Implements IPersistenciaBase(Of DetalleDocumentoCausaciones).Modificar
            Return New RepositorioDetalleDocumentoCausaciones().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleDocumentoCausaciones) As DetalleDocumentoCausaciones Implements IPersistenciaBase(Of DetalleDocumentoCausaciones).Obtener
            Return New RepositorioDetalleDocumentoCausaciones().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleDocumentoCausaciones) As Boolean
            Return New RepositorioDetalleDocumentoCausaciones().Anular(entidad)
        End Function
    End Class

End Namespace

