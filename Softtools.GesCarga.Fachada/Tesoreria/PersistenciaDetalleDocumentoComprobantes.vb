﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Repositorio.Tesoreria

Namespace Tesoreria
    Public NotInheritable Class PersistenciaDetalleDocumentoComprobantes
        Implements IPersistenciaBase(Of DetalleDocumentoComprobantes)

        Public Function Consultar(filtro As DetalleDocumentoComprobantes) As IEnumerable(Of DetalleDocumentoComprobantes) Implements IPersistenciaBase(Of DetalleDocumentoComprobantes).Consultar
            Return New RepositorioDetalleDocumentoComprobantes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleDocumentoComprobantes) As Long Implements IPersistenciaBase(Of DetalleDocumentoComprobantes).Insertar
            Return New RepositorioDetalleDocumentoComprobantes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleDocumentoComprobantes) As Long Implements IPersistenciaBase(Of DetalleDocumentoComprobantes).Modificar
            Return New RepositorioDetalleDocumentoComprobantes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleDocumentoComprobantes) As DetalleDocumentoComprobantes Implements IPersistenciaBase(Of DetalleDocumentoComprobantes).Obtener
            Return New RepositorioDetalleDocumentoComprobantes().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleDocumentoComprobantes) As Boolean
            Return New RepositorioDetalleDocumentoComprobantes().Anular(entidad)
        End Function
    End Class

End Namespace

