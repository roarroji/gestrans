﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Repositorio.Tesoreria

Namespace Tesoreria
    Public NotInheritable Class PersistenciaDetalleDocumentoCuentaComprobantes
        Implements IPersistenciaBase(Of DetalleDocumentoCuentaComprobantes)

        Public Function Consultar(filtro As DetalleDocumentoCuentaComprobantes) As IEnumerable(Of DetalleDocumentoCuentaComprobantes) Implements IPersistenciaBase(Of DetalleDocumentoCuentaComprobantes).Consultar
            Return New RepositorioDetalleDocumentoCuentaComprobantes().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As DetalleDocumentoCuentaComprobantes) As Long Implements IPersistenciaBase(Of DetalleDocumentoCuentaComprobantes).Insertar
            Return New RepositorioDetalleDocumentoCuentaComprobantes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As DetalleDocumentoCuentaComprobantes) As Long Implements IPersistenciaBase(Of DetalleDocumentoCuentaComprobantes).Modificar
            Return New RepositorioDetalleDocumentoCuentaComprobantes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As DetalleDocumentoCuentaComprobantes) As DetalleDocumentoCuentaComprobantes Implements IPersistenciaBase(Of DetalleDocumentoCuentaComprobantes).Obtener
            Return New RepositorioDetalleDocumentoCuentaComprobantes().Obtener(filtro)
        End Function
        Public Function Anular(entidad As DetalleDocumentoCuentaComprobantes) As Boolean
            Return New RepositorioDetalleDocumentoCuentaComprobantes().Anular(entidad)
        End Function
    End Class

End Namespace

