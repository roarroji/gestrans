﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Repositorio.Tesoreria

Namespace Tesoreria
    Public NotInheritable Class PersistenciaEncabezadoDocumentoCausaciones
        Implements IPersistenciaBase(Of EncabezadoDocumentoCausaciones)

        Public Function Consultar(filtro As EncabezadoDocumentoCausaciones) As IEnumerable(Of EncabezadoDocumentoCausaciones) Implements IPersistenciaBase(Of EncabezadoDocumentoCausaciones).Consultar
            Return New RepositorioEncabezadoDocumentoCausaciones().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoDocumentoCausaciones) As Long Implements IPersistenciaBase(Of EncabezadoDocumentoCausaciones).Insertar
            Return New RepositorioEncabezadoDocumentoCausaciones().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoDocumentoCausaciones) As Long Implements IPersistenciaBase(Of EncabezadoDocumentoCausaciones).Modificar
            Return New RepositorioEncabezadoDocumentoCausaciones().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoDocumentoCausaciones) As EncabezadoDocumentoCausaciones Implements IPersistenciaBase(Of EncabezadoDocumentoCausaciones).Obtener
            Return New RepositorioEncabezadoDocumentoCausaciones().Obtener(filtro)
        End Function
        Public Function Anular(entidad As EncabezadoDocumentoCausaciones) As Boolean
            Return New RepositorioEncabezadoDocumentoCausaciones().Anular(entidad)
        End Function
    End Class

End Namespace

