﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Repositorio.Tesoreria

Namespace Tesoreria
    Public NotInheritable Class PersistenciaEncabezadoDocumentoComprobantes
        Implements IPersistenciaBase(Of EncabezadoDocumentoComprobantes)

        Public Function Consultar(filtro As EncabezadoDocumentoComprobantes) As IEnumerable(Of EncabezadoDocumentoComprobantes) Implements IPersistenciaBase(Of EncabezadoDocumentoComprobantes).Consultar
            Return New RepositorioEncabezadoDocumentoComprobantes().Consultar(filtro)
        End Function

        Public Function ConsultarCodigosRespuesaBancolombia(filtro As EncabezadoDocumentoComprobantes) As IEnumerable(Of CodigoRespuestaBancolombia)
            Return New RepositorioEncabezadoDocumentoComprobantes().ConsultarCodigosRespuesaBancolombia(filtro)
        End Function

        Public Function ConsultarCadenaEgresos(filtro As EncabezadoDocumentoComprobantes) As IEnumerable(Of EncabezadoDocumentoComprobantes)
            Return New RepositorioEncabezadoDocumentoComprobantes().ConsultarCadenaEgresos(filtro)
        End Function
        Public Function ConsultarCuentasconEgresos(filtro As EncabezadoDocumentoComprobantes) As IEnumerable(Of EncabezadoDocumentoComprobantes)
            Return New RepositorioEncabezadoDocumentoComprobantes().ConsultarCuentasconEgresos(filtro)
        End Function

        Public Function Insertar(entidad As EncabezadoDocumentoComprobantes) As Long Implements IPersistenciaBase(Of EncabezadoDocumentoComprobantes).Insertar
            Return New RepositorioEncabezadoDocumentoComprobantes().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EncabezadoDocumentoComprobantes) As Long Implements IPersistenciaBase(Of EncabezadoDocumentoComprobantes).Modificar
            Return New RepositorioEncabezadoDocumentoComprobantes().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EncabezadoDocumentoComprobantes) As EncabezadoDocumentoComprobantes Implements IPersistenciaBase(Of EncabezadoDocumentoComprobantes).Obtener
            Return New RepositorioEncabezadoDocumentoComprobantes().Obtener(filtro)
        End Function
        Public Function Anular(entidad As EncabezadoDocumentoComprobantes) As Boolean
            Return New RepositorioEncabezadoDocumentoComprobantes().Anular(entidad)
        End Function

        Public Function ActualizarEgresosArchivoRespuesta(entidad As EncabezadoDocumentoComprobantes) As Long
            Return New RepositorioEncabezadoDocumentoComprobantes().ActualizarEgresosArchivoRespuesta(entidad)
        End Function

        Public Function ValidarSecuenciaLotes(entidad As EncabezadoDocumentoComprobantes) As Long
            Return New RepositorioEncabezadoDocumentoComprobantes().ValidarSecuenciaLotes(entidad)
        End Function
    End Class

End Namespace

