﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Utilitarios
Imports Softtools.GesCarga.Repositorio.Utilitarios

Namespace Utilitarios
    Public NotInheritable Class PersistenciaBandejaSalidaCorreos
        Implements IPersistenciaBase(Of BandejaSalidaCorreos)
        Public Function Consultar(filtro As BandejaSalidaCorreos) As IEnumerable(Of BandejaSalidaCorreos) Implements IPersistenciaBase(Of BandejaSalidaCorreos).Consultar
            Return New RepositorioBandejaSalidaCorreos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As BandejaSalidaCorreos) As Long Implements IPersistenciaBase(Of BandejaSalidaCorreos).Insertar
            Return New RepositorioBandejaSalidaCorreos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As BandejaSalidaCorreos) As Long Implements IPersistenciaBase(Of BandejaSalidaCorreos).Modificar
            Return New RepositorioBandejaSalidaCorreos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As BandejaSalidaCorreos) As BandejaSalidaCorreos Implements IPersistenciaBase(Of BandejaSalidaCorreos).Obtener
            Return New RepositorioBandejaSalidaCorreos().Obtener(filtro)
        End Function
    End Class
End Namespace
