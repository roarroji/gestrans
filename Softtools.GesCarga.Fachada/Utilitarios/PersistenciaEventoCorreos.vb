﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Utilitarios
Imports Softtools.GesCarga.Repositorio.Utilitarios

Namespace Utilitarios
    Public NotInheritable Class PersistenciaEventoCorreos
        Implements IPersistenciaBase(Of EventoCorreos)
        Public Function Consultar(filtro As EventoCorreos) As IEnumerable(Of EventoCorreos) Implements IPersistenciaBase(Of EventoCorreos).Consultar
            Return New RepositorioEventoCorreos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As EventoCorreos) As Long Implements IPersistenciaBase(Of EventoCorreos).Insertar
            Return New RepositorioEventoCorreos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As EventoCorreos) As Long Implements IPersistenciaBase(Of EventoCorreos).Modificar
            Return New RepositorioEventoCorreos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As EventoCorreos) As EventoCorreos Implements IPersistenciaBase(Of EventoCorreos).Obtener
            Return New RepositorioEventoCorreos().Obtener(filtro)
        End Function
    End Class
End Namespace
