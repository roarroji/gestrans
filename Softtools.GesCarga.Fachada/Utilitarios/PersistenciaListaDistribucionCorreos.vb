﻿Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Entidades.Utilitarios
Imports Softtools.GesCarga.Repositorio.Utilitarios

Namespace Utilitarios
    Public NotInheritable Class PersistenciaListaDistribucionCorreos
        Implements IPersistenciaBase(Of ListaDistribucionCorreos)
        Public Function Consultar(filtro As ListaDistribucionCorreos) As IEnumerable(Of ListaDistribucionCorreos) Implements IPersistenciaBase(Of ListaDistribucionCorreos).Consultar
            Return New RepositorioListaDistribucionCorreos().Consultar(filtro)
        End Function

        Public Function Insertar(entidad As ListaDistribucionCorreos) As Long Implements IPersistenciaBase(Of ListaDistribucionCorreos).Insertar
            Return New RepositorioListaDistribucionCorreos().Insertar(entidad)
        End Function

        Public Function Modificar(entidad As ListaDistribucionCorreos) As Long Implements IPersistenciaBase(Of ListaDistribucionCorreos).Modificar
            Return New RepositorioListaDistribucionCorreos().Modificar(entidad)
        End Function

        Public Function Obtener(filtro As ListaDistribucionCorreos) As ListaDistribucionCorreos Implements IPersistenciaBase(Of ListaDistribucionCorreos).Obtener
            Return New RepositorioListaDistribucionCorreos().Obtener(filtro)
        End Function
    End Class
End Namespace
