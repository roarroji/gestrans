﻿Public Enum TipoDocumento As Short

    SinDefinir = 0
    MovimientoCajas = 1
    ComprobanteEgreso = 2
    ComprobanteIngreso = 3
    CuentasPorCobrar = 5
    CuentasPorPagar = 6
    ProgramacionPlanRodamientos = 7
    ComprobanteCausacion = 8
    Tiquetes = 20
    Reservas = 21
    PlanillaIntermunicipal = 22
    Prestamos = 23
    CumplidoEncomiendas = 40
    LiquidacionAfiliados = 50
    Factura = 60
    ContratoEspecial = 80
    ContratoTransportadorEspecial = 85
    SolicitudEspecial = 90
    Terceros = 100
    LiquidacionTransportadorEspecial = 120
    ComprobanteContable = 200
    SeguimientoMovil = 210
    SeguimientoVehiculos = 211
    LegalizacionGastosConductor = 215

End Enum

Public Enum EstadoDocumento As Short
    SinDefinir = 0
    NoAplica = 8654
    Borrador = 8655
    Definitivo = 8656
End Enum

Public Enum EstadoCorreo As Integer
    SinDefinir = 0
    NoEnviado = 157000
    Enviado = 157001
End Enum

Public Enum Perfiles As Short
    SinDefinir = 0
    NoAplica = 2350
    Cliente = 2351
    Remitente = 2352
    EmpresaTransportadora = 2353
    EmpresaAfiliadora = 2354
    Propietario = 2355
    Tenedor = 2356
    Conductor = 2357
    Proveedor = 2358
    Aseguradora = 2359
    SeguridadSocial = 2360
    Empleado = 2361
    Pasajero = 8735
    Destinatario = 8757
    Afiliado = 8830
End Enum


