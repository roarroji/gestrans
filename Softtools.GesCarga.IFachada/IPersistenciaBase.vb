﻿Public Interface IPersistenciaBase(Of T)

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="filtro"></param>
    ''' <returns></returns>
    Function Consultar(filtro As T) As IEnumerable(Of T)

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="entidad"></param>
    ''' <returns></returns>
    Function Insertar(entidad As T) As Long

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="entidad"></param>
    ''' <returns></returns>
    Function Modificar(entidad As T) As Long

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="filtro"></param>
    ''' <returns></returns>
    Function Obtener(filtro As T) As T
End Interface
