﻿Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Despachos.Planilla
Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Entidades.Gesphone
Imports Softtools.GesCarga.Entidades.Paqueteria
''' <summary>
''' 
''' </summary>
''' <typeparam name="T"></typeparam>
Public Interface IPersistenciaSincronizacionApp

    Function Consultar(filtro As FiltroSyncApp) As IEnumerable(Of SyncDetalleTarifarioVenta)

    Function ObtenerEncabezadoTarifario(codigoEmpresa As Integer, codigoTercero As Integer) As SyncEncabezadoTarifarioVenta

    Function ObtenerEncabezadoPlanilla(filtro As PlanillaGuias) As IEnumerable(Of PlanillaGuias)

    Function ObtenerRemesasPaqueteria(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)

    Function ObtenerDetallePlanilla(codigoEmpresa As Integer, numeroPlanilla As Integer) As IEnumerable(Of SyncDetallePlanilla)

    Function ConsultarTerceros(filtro As Terceros) As IEnumerable(Of Terceros)

    Function ConsultarGestionDocumentosTercero(CodiEmpresa As Short, CodTercero As Long) As IEnumerable(Of GestionDocumentoCliente)

    Function ConsultarDetalleGestionDocumentos(CodiEmpresa As Short, CodRemesa As Integer) As IEnumerable(Of GestionDocumentosRemesa)

    Function ConsultarFormasPagoTercero(CodiEmpresa As Short, CodTercero As Long) As IEnumerable(Of FormasPago)

    Function ConsultarCiudades(filtro As Ciudades) As IEnumerable(Of Ciudades)

    Function ConsultarSitiosCargueDescargue(filtro As SitiosCargueDescargue) As IEnumerable(Of SitiosCargueDescargue)

    Function ConsultarProductosTransportados(filtro As ProductoTransportados) As IEnumerable(Of ProductoTransportados)

    Function InsertarDetalleEtiquetasPreimpresas(registro As IEnumerable(Of SyncEtiquetaPreimpresa)) As Boolean

    Function InsertarDetalleIntentoEntregaGuia(registro As DetalleIntentoEntregaGuia) As Long

    Function ConsultarIntentosEntregaGuia(codigoEmpresa As Short, numeroRemesa As Long) As IEnumerable(Of DetalleIntentoEntregaGuia)

    Function ConsultarImagenesRemesaPaqueteria(codigoEmpresa As Short, numeroRemesa As Long) As IEnumerable(Of SyncDetallePlanillaImagenes)

    Function ModificarEstadoRecoleccion(codigoEmpresa As Integer, numeroRecoleccion As Integer, codigoEstado As Integer) As Long

    Function ConsultarDetalleAxiliares(codigoEmpresa As Integer, numeroPlanilla As Long) As IEnumerable(Of DetalleAuxiliaresPlanillas)

    Function ConsultarDetalleAsignacionEtiquetasPreimpresas(codigoEmpresa As Short, codigoOficina As Short, codigoResponsable As Long) As IEnumerable(Of SyncAsignacionEtiquetas)

    Function LiberarEtiquetas(entidad As EtiquetaPreimpresa) As Boolean

    Function GuardarEntrega(Entidad As Remesas) As Long

End Interface
