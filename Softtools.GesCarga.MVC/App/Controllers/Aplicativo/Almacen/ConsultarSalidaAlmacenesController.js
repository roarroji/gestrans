﻿SofttoolsApp.controller("ConsultarSalidaAlmacenesCtrl", ['$scope', '$timeout', '$routeParams', 'blockUI', '$linq', 'DocumentoAlmacenesFactory', function ($scope, $timeout, $routeParams, blockUI, $linq, DocumentoAlmacenesFactory) {
    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    $scope.Buscando = false;
    $scope.ResultadoSinRegistros = '';
    $scope.MensajesError = [];
    var filtros = {};
    $scope.ListadoEstados = [];
    $scope.totalRegistros = 0;
    $scope.paginaActual = 1;
    $scope.cantidadRegistrosPorPagina = 10;
    $scope.totalPaginas = 0;
    $scope.ListadoEstados = [
        { Nombre: '(TODAS)', Codigo: -1 },
        { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
        { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR },
        { Nombre: 'ANULADO', Codigo: ESTADO_ANULADO }
    ];
    $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SALIDA_ALMACENES);
    $scope.MapaSitio = [{ Nombre: 'Almacén' }, { Nombre: 'Documentos' }, { Nombre: 'Salidas Almacén' }];

    //Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
        $scope.FechaInicial = new Date();
        $scope.FechaFinal = new Date();
    }
    //--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };

    //---------------------------------------------------------------------Funcion Buscar Linea de Mantenimiento-----------------------------------------------------------------------------
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            Find()
        }
    };

    function Find() {


        $scope.ListadoSalidaAlmacen = [];
        if (DatosRequeridos()) {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoSalidaAlmacen = [];
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroInicial: $scope.NumeroInicial,
                NumeroFinal: $scope.NumeroFinal,
                FechaInicial: $scope.FechaInicial,
                FechaFinal: $scope.FechaFinal,
                Estado: $scope.Estado,
                NombreAlmacen: $scope.Almacen,
                NumeroOrdenCompra: $scope.OrdenCompra,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_SALIDA_ALMACEN,
                
            };


            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;

                DocumentoAlmacenesFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoSalidaAlmacen = response.data.Datos

                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        }
    };

    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;
        if (($scope.FechaInicial === null || $scope.FechaInicial === undefined || $scope.FechaInicial === '')
            && ($scope.FechaFinal === null || $scope.FechaFinal === undefined || $scope.FechaFinal === '')
            && ($scope.Numero === null || $scope.Numero === undefined || $scope.Numero === '' || $scope.Numero === 0 || isNaN($scope.Numero) === true)

        ) {
            $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
            continuar = false

        } else if (($scope.Numero !== null && $scope.Numero !== undefined && $scope.Numero !== '' && $scope.Numero !== 0)
            || ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
            || ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')

        ) {
            if (($scope.FechaInicial !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')
                && ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')) {
                if ($scope.FechaFinal < $scope.FechaInicial) {
                    $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                    continuar = false
                } else if ((($scope.FechaFinal - $scope.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                    $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                    continuar = false
                }
            }
            else {
                if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                    $scope.FechaFinal = $scope.FechaInicial
                } else {
                    $scope.FechaInicial = $scope.FechaFinal
                }
            }
        }
        return continuar
    }

    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*Validacion boton anular */
    $scope.ConfirmacionAnularSalida = function (numero, Estado) {
        $scope.estadoanulado = Estado;
        $scope.NumeroAnulacion = numero;
        if ($scope.estadoanulado.Codigo !== ESTADO_ANULADO) {
            showModal('modalConfirmacionAnularSalida');
        }
        else {
            closeModal('modalConfirmacionAnularSalida');
        }
    };
    /*Funcion que solicita los datos de la anulación */
    $scope.SolicitarDatosAnulacion = function () {
        $scope.FechaAnulacion = new Date();
        closeModal('modalConfirmacionAnularSalida');
        showModal('modalDatosAnularSalida');
    };
    /*---------------------------------------------------------------------Funcion Anular orden compra-----------------------------------------------------------------------------*/
    $scope.Anular = function () {

        $scope.ListadoSalidaAlmacen.forEach(function (item) {
            if (item.Numero == $scope.NumeroAnulacion) {
                $scope.Estado = item.Estado;
            }
        });

        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Numero: $scope.NumeroAnulacion,
            UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CausaAnulacion: $scope.CausaAnulacion,
            OficinaAnula: $scope.Sesion.UsuarioAutenticado.Oficinas[0],
            TipoDocumento: CODIGO_TIPO_DOCUMENTO_SALIDA_ALMACEN,
        };

        DocumentoAlmacenesFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se anuló la Salida almacén No.' + entidad.Numero);
                    closeModal('modalDatosAnularSalida');
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                ShowError(response.statusText);
            });

    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarSalidaAlmacenes';
        }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Codigo = MascaraNumero($scope.Codigo) } catch (e) { }
    };

    $scope.urlASP = '';
    $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
    $scope.DesplegarInforme = function (Numero) {
        if ($scope.ValidarPermisos.AplicaImprimir == PERMISO_ACTIVO) {
            $scope.FiltroArmado = '';

            //Depende del listado seleccionado se enviará el nombre por parametro
            $scope.Numero = Numero;
            $scope.NombreReporte = 'RepSalidaAlmacen'
            //Arma el filtro
            $scope.ArmarFiltro();
            //Llama a la pagina ASP con el filtro por GET

            window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + 1 + '&TipoDocumento=' + DOCUMENTO_SALIDA_ALMACEN);

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';
        };
    };

    // funcion enviar parametros al proyecto ASP armando el filtro
    $scope.ArmarFiltro = function () {
        $scope.FiltroArmado = '';
        if ($scope.Numero !== undefined && $scope.Numero !== '' && $scope.Numero !== null) {
            $scope.FiltroArmado += '&Numero=' + $scope.Numero;
        }
    }

    if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
        $scope.Numero = $routeParams.Numero;      
        Find();
        $scope.Numero = ''
    }

}]);