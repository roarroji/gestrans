﻿SofttoolsApp.controller("GestionarSalidaAlmacenCtrl", ['$scope', '$timeout', '$routeParams', 'blockUI', '$linq', 'DocumentoAlmacenesFactory', 'AlmacenesFactory', 'ReferenciaAlmacenesFactory', 'ReferenciaProveedoresFactory', 'ReferenciasFactory',
    function ($scope, $timeout, $routeParams, blockUI, $linq, DocumentoAlmacenesFactory, AlmacenesFactory, ReferenciaAlmacenesFactory, ReferenciaProveedoresFactory, ReferenciasFactory) {
        //----------------------------- Decalaracion Variables ------------------------------------------------------// 

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Numero: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            TipoDocumento: CODIGO_TIPO_DOCUMENTO_SALIDA_ALMACEN,
            Oficina: {
                Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre,
            },
        };
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.MapaSitio = [{ Nombre: 'Almacén' }, { Nombre: 'Documentos' }, { Nombre: 'Salidas Almacén' }];
        $scope.Titulo = 'EDICIÓN SALIDAS ALMACÉN';
        $scope.ListadoDetalle = [];
        $scope.Fecha = new Date();
        $scope.ModeloFecha = new Date();
        $scope.ListadoDetalleOrdenCompra = [];
        $scope.ListadoReferenciaProveedor = [];
        $scope.AuxListadoDetalle = [];
        $scope.ListadoAlmacenes = [];
        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + ESTADO_BORRADOR);
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SALIDA_ALMACENES);
        //Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar


        // Obtener parametros del enrutador
        if ($routeParams.Numero !== undefined) {
            $scope.Modelo.Numero = $routeParams.Numero;
            $scope.Modelo.ConsultaKardex = $routeParams.ConsultaKardex;
        }
        else {
            $scope.Modelo.Numero = 0;
        }

        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($scope.Modelo.ConsultaKardex == 0 || $scope.Modelo.ConsultaKardex == undefined) {
                if ($scope.Modelo.Numero > 0) {
                    document.location.href = '#!ConsultarSalidaAlmacenes/' + $scope.Modelo.Numero;
                }
                else {

                    document.location.href = '#!ConsultarSalidaAlmacenes';
                }

            }
            else {
                document.location.href = '#!ConsultarKardex';
            }

        };
        ///-------------------------------------------------------------------------------------------Cargar Combos/Autocomplete----------------------------------------------------------------
        ////Cargar el combo de almacenes
        $scope.ListadoAlmacenes = AlmacenesFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: ESTADO_DEFINITIVO,
            Sync: true
        }).Datos;
        $scope.Almacen = $scope.ListadoAlmacenes[0];


        ////Cargar Autocomplete REFERENCIAS
        $scope.ListadoReferencias = ReferenciasFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: ESTADO_DEFINITIVO,
            Sync: true
        }).Datos;



        /*Cargar el combo de REFERENCIAS PROVEEDOR*/
        ReferenciaProveedoresFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoReferenciaProveedor = [];
                    $scope.ListadoReferenciaProveedor = response.data.Datos;
                }
            }, function (response) {
                ShowError(response.statusText);
            });


        //-------------------------------------------------------- VALIDACIONES -------------------------------------------------------------
        $scope.ValidarReferencia = function (Referencia) {
            if (Referencia != undefined && Referencia != '' && Referencia != null) {
                ReferenciasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Referencia: Referencia }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.MensajesErrorDetalle = [];
                            $scope.Referencia = response.data.Datos;
                            $scope.NombreReferencia = $scope.Referencia.Nombre;
                            ValidarReferenciaAlmacen($scope.Referencia.Codigo) // Validar que la referencia perternesca al almacen seleccionado
                            ValidarReferenciaProveedor($scope.Referencia.ReferenciaProveedor.Codigo) // validdar que la referencia pertenesca a un proveedor
                            $scope.UnidadEmpaque = $scope.Referencia.UnidadEmpaque.NombreCorto
                            $scope.UnidadMedida = $scope.Referencia.UnidadMedida.NombreCorto
                        }
                        else {
                            ShowError('el nombre de la referencia ingresada no es valida');
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        function ValidarReferenciaAlmacen(Referencia) {
            if (Referencia != undefined && Referencia != '' && Referencia != null && Referencia != 0) {
                ReferenciaAlmacenesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Referencia: { Codigo: Referencia }, Almacen: $scope.Almacen }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.ReferenciaAlmacen = response.data.Datos
                                $scope.ExistenciaActual = $scope.ReferenciaAlmacen.ExistenciaActual
                                $scope.UltimoPrecio = $scope.ReferenciaAlmacen.UltimoPrecio
                                $scope.ModalValorUnitario = $scope.ReferenciaAlmacen.ValorPromedio
                            } else {
                                $scope.MensajesErrorDetalle.push('La referencia  no se encuentra asociada al almacén ' + $scope.Almacen.Nombre);
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        function ValidarReferenciaProveedor(Referencia) {
            if (Referencia != undefined && Referencia != '' && Referencia != null && Referencia != 0) {
                ReferenciaProveedoresFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Codigo: Referencia }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.ReferenciaProveedor = response.data.Datos;
                            } else {
                                $scope.MensajesErrorDetalle.push('La referencia  no se encuentra asociada a ningun  proveedor');
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }


        $scope.ConfirmacionGuardarSalida = function () {
            showModal('modalConfirmacionGuardarSalida');
        };

        // Metodo para guardar /modificar
        $scope.Guardar = function () {
            $scope.MaskNumero();
            closeModal('modalConfirmacionGuardarSalida');

            $scope.Modelo.Fecha = $scope.ModeloFecha;
            $scope.Modelo.Almacen = $scope.Almacen;
            $scope.Modelo.Responsable = $scope.Resposable;
            $scope.Modelo.NombreResibeSalida = $scope.EntregadoA;
            $scope.Modelo.ValorTotal = $scope.ModalValorTotal;
            $scope.Modelo.Observaciones = $scope.Observaciones;
            $scope.Modelo.Estado = $scope.Modelo.Estado;
            $scope.Modelo.Detalles = $scope.ListadoDetalle;

            if (DatosRequeridos()) {
                DocumentoAlmacenesFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Numero == 0) {
                                    ShowSuccess('Se guardó la salida almacén No.' + response.data.Datos);
                                    location.href = '#!ConsultarSalidaAlmacenes/' + +response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modifico la salida almacén No.' + response.data.Datos);
                                    location.href = '#!ConsultarSalidaAlmacenes/' + $scope.Modelo.Numero;
                                }

                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.ModeloFecha == '' || $scope.ModeloFecha == undefined || $scope.ModeloFecha == null) {
                $scope.MensajesError.push('Debe ingresar una fecha');
                continuar = false;
            }
            if ($scope.Almacen == '' || $scope.Almacen == undefined || $scope.Almacen == null) {
                $scope.MensajesError.push('Debe seleccionar un almacén');
                continuar = false;
            }
            if ($scope.Modelo.Estado == null || $scope.Modelo.Estado == '' || $scope.Modelo.Estado == undefined) {
                $scope.MensajesError.push('Debe seleccionar el estado');
                continuar = false;
            }
            if ($scope.ListadoDetalle.length == 0) {
                $scope.MensajesError.push('Debe ingresar minimo un detalle ');
                continuar = false;
            }
            return continuar
        }

        //-------------------------------------------------------------------------------------------------------
        if ($scope.Modelo.Numero > 0) {
            // Consultar los datos de la factura correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'EDITAR SALIDA ALMACÉN';
            $scope.Deshabilitar = true;
            $scope.Bloquear = true;
            Obtener();
        }

        // Metodo Obtener Contratos 
        function Obtener() {
            blockUI.start('Cargando salida Número ' + $scope.Modelo.Numero);

            $timeout(function () {
                blockUI.message('Cargando salida Número ' + $scope.Modelo.Numero);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_SALIDA_ALMACEN,
            };

            blockUI.delay = 1000;
            DocumentoAlmacenesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.NumeroDocumento = response.data.Datos.NumeroDocumento;

                        var fecha = new Date(response.data.Datos.Fecha);
                        fecha.setHours(0);
                        fecha.setMinutes(0);
                        fecha.setMilliseconds(0);

                        $scope.ModeloFecha = fecha;

                        $scope.CodigoAlmacen = response.data.Datos.Almacen.Codigo

                        if ($scope.ListadoAlmacenes.length > 0) {
                            $scope.Almacen = $linq.Enumerable().From($scope.ListadoAlmacenes).First('$.Codigo == ' + response.data.Datos.Almacen.Codigo);
                        }

                        $scope.Resposable = response.data.Datos.Responsable;
                        $scope.EntregadoA = response.data.Datos.NombreResibeSalida;

                        $scope.ValorTotal = response.data.Datos.ValorTotal;
                        $scope.Observaciones = response.data.Datos.Observaciones;
        
                        $scope.AuxListadoDetalle = response.data.Datos.Detalles.forEach(item => {
                            item.Referencia = item.ReferenciaAlmacen.Referencia.CodigoReferencia
                            item.ReferenciaAlmacen = { Nombre: item.ReferenciaAlmacen.NombreReferencia, Codigo: item.ReferenciaAlmacen.Codigo}
                            item.UnidadMedida = item.UnidadMedida.NombreCorto
                            item.UnidadEmpaque = item.UnidadEmpaque.NombreCorto
                            item.AuxiCantidad = item.Cantidad;
                            $scope.ListadoDetalle.push(item)
                        });

                 
                    
                        

                        if (response.data.Datos.Estado.Codigo !== ESTADO_ANULADO) {
                            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado.Codigo);
                        }
                        else {
                            $scope.ListadoEstados.push({ Nombre: 'Anulado', Codigo: ESTADO_ANULADO });
                            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado.Codigo);
                            $scope.Deshabilitar = true;
                            $scope.Bloquear = true;
                        }
                        if (response.data.Datos.Estado.Codigo == ESTADO_BORRADOR) {
                            $scope.Deshabilitar = false;
                            $scope.Bloquear = true;
                        }
                        else if (response.data.Datos.Estado.Codigo = ESTADO_DEFINITIVO) {
                            $scope.Deshabilitar = true;
                            $scope.Bloquear = true;
                        }

                    }
                    else {
                        ShowError('No se logro consultar la salida No.' + $scope.Modelo.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarSalidaAlmacenes';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarSalidaAlmacenes';
                });

            blockUI.stop();
        };

        $scope.CalcularDetalle = function () {
            $scope.MaskNumero();
            $scope.MensajesErrorDetalle = [];
            if ($scope.ModalCantidad !== undefined && $scope.ModalCantidad !== null && $scope.ModalCantidad !== '') {
                //if ($scope.ExistenciaActual >= $scope.ModalCantidad) {
                $scope.ValorUnitario = Math.ceil($scope.ModalValorUnitario - 0)
                if ($scope.ValorUnitario > 0) {
                    $scope.ModalValorTotal = 0
                    $scope.ModalValorTotal = Math.ceil($scope.ModalCantidad * $scope.ModalValorUnitario)
                }
                else {
                    $scope.MensajesErrorDetalle.push('El valor unitario debe ser mayor a 0');

                    //  }
                }
            }
            $scope.MaskValores();
        }

        //Modal Modicar/Adicionar Detalle 
        $scope.InicializarVistaModal = function () {
            if ($scope.Almacen == undefined || $scope.Almacen == null || $scope.Almacen == '' || $scope.Almacen.Codigo == 0) {
                window.scrollTo(top, top);
                $scope.MensajesError.push('Debe ingresar un almacén');
            } else {

                $scope.Referencia = '';
                $scope.NombreReferencia = '';
                $scope.MensajesErrorDetalle = [];
                $scope.ModalValorUnitario = 0;
                $scope.ModalCantidad = 0;
                $scope.ModalValorTotal = 0;
                $scope.UnidadMedida = '';


                $scope.UnidadEmpaque = '';
                $scope.TituloDetalle = 'Adicionar Detalle'

                showModal('modalEditarSalida');

                //Inserta el detalle en el grid

                $scope.CargarDetalle = function () {
                    $scope.MensajesErrorDetalle = [];
                    $scope.DetalleIngresado = 0;
                    if ($scope.ListadoDetalle.length > 0) {
                        $scope.ListadoDetalle.forEach(function (item) {
                            if (item.ReferenciaAlmacen.Codigo == $scope.Referencia.Codigo) {
                                $scope.DetalleIngresado = 1;

                            }
                        });

                    }
                    $scope.CalcularDetalle();
                    if ($scope.ValorUnitario > 0) {
                        if ($scope.DetalleIngresado == 0) {
                            if (DatosRequeridosDetalle()) {
                                closeModal('modalEditarSalida');
                                $scope.MaskNumero();


                                $scope.ListadoDetalle.push({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Referencia: $scope.Referencia.CodigoReferencia,
                                    ReferenciaAlmacen: { Nombre: $scope.NombreReferencia, Codigo: $scope.Referencia.Codigo},
                                    UnidadMedida: $scope.UnidadMedida,
                                    UnidadEmpaque: $scope.UnidadEmpaque,
                                    ValorUnitario: $scope.ModalValorUnitario,
                                    Cantidad: $scope.ModalCantidad,
                                    ValorAntesDescuento: $scope.ModalValorTotal,
                                    ValorTotal: $scope.ModalValorTotal
                                });
                            }

                        } else {
                            $scope.MensajesErrorDetalle.push('El Detalle con referencia ' + $scope.Referencia.Nombre + ' ya ha sido ingresado')
                        }
                    } else {
                        $scope.MensajesErrorDetalle.push('La referencia ' + $scope.Referencia.Referencias.Referencia + ' - ' + $scope.Referencia.Referencias.Nombre + ' no tiene un valor asignado')
                    }
                }
            }
        }

   

        //-------------------------------------------------------------------------------------------------------
        //Eliminar detalle orden compra
        $scope.ConfirmacionEliminarDetalle = function (Detalle, indice) {
            $scope.MensajeEliminar = { indice };
            $scope.VarAuxi = indice
            $scope.Detalle = Detalle
            var IndiceAuxi = ''
            showModal('modalEliminarDetalle');
        };

        $scope.EliminarDetalle = function (indice) {
            $scope.ListadoDetalle.forEach(function (item) {
                if ($scope.Detalle.$$hashKey == item.$$hashKey) {
                    $scope.ListadoDetalle.splice($scope.VarAuxi, 1);
                    IndiceAuxi = item.$$hashKey
                }
            });
            closeModal('modalEliminarDetalle');
        };

        function limpiarModal() {

        }
        function DatosRequeridosDetalle() {
            $scope.MensajesErrorDetalle = [];
            var continuar = true;
            if ($scope.Referencia == '' || $scope.Referencia == undefined || $scope.Referencia == null) {
                $scope.MensajesErrorDetalle.push('Debe ingresar una referencia');
                continuar = false;
            }
            if ($scope.ModalCantidad == '' || $scope.ModalCantidad == undefined || $scope.ModalCantidad == null || $scope.ModalCantidad == 0) {
                $scope.MensajesErrorDetalle.push('Debe ingresar una cantidad');
                continuar = false;
            }
            if ($scope.ModalValorUnitario == '' || $scope.ModalValorUnitario == undefined || $scope.ModalValorUnitario == null) {
                $scope.MensajesErrorDetalle.push('El valor unitario debe ser mayor a 0');
                continuar = false;
            }
            if ($scope.ModalCantidad > $scope.ExistenciaActual) {
                $scope.MensajesErrorDetalle.push('La cantidad ingresada es mayor a la cantidad de existencias actuales');
                continuar = false;
            }


            return continuar
        }


        $scope.MaskNumero = function () {
            try { $scope.ModalCantidad = MascaraNumero($scope.ModalCantidad) } catch (e) { }
            try { $scope.ModalValorUnitario = MascaraNumero($scope.ModalValorUnitario) } catch (e) { }
            try { $scope.ModalValorTotal = MascaraNumero($scope.ModalValorTotal) } catch (e) { }
        };
        $scope.MaskValores = function () {
            try { $scope.ModalValorTotal = MascaraValores($scope.ModalValorTotal) } catch (e) { }
            try { $scope.ModalValorUnitario = MascaraValores($scope.ModalValorUnitario) } catch (e) { }
        };
    }]);

