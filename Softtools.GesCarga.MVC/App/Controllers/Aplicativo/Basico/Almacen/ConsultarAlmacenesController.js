﻿SofttoolsApp.controller("ConsultarAlmacenesCtrl", ['$scope', '$timeout', 'AlmacenesFactory', '$linq', 'blockUI', '$routeParams', 'OficinasFactory', function ($scope, $timeout, AlmacenesFactory, $linq, blockUI, $routeParams, OficinasFactory) {

    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Almacén' }, { Nombre: 'Almacenes' }];


    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
    
    var filtros = {};
    $scope.cantidadRegistrosPorPaginas = 10;
    $scope.paginaActual = 1;
    $scope.ModalErrorCompleto = ''
    $scope.ModalError = ''
    $scope.ListadoOficinas = [];
    $scope.ListadoAlmacenes = [];
    $scope.MostrarMensajeError = false
    $scope.ListadoEstados = [
        { Nombre: '(TODOS)', Codigo: -1},
        { Nombre: 'ACTIVO', Codigo: 1 },
        { Nombre: 'INACTIVO', Codigo: 0 }
    ];
    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ALMACENES);

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };


    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarAlmacenes';
        }
    };
    
    OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
            if (response.data.ProcesoExitoso === true) {
                response.data.Datos.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });

                $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==-1');
            }
        }, function (response) {
            ShowError(response.statusText);
        });

    //-------------------------------------------------FUNCIONES--------------------------------------------------------
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            $scope.Codigo = 0
            Find()
        }
    };

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);
        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoAlmacenes = [];
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        if ($scope.Buscando){
            if ($scope.MensajesError.length == 0) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoAlterno: $scope.CodigoAlterno,
                    Nombre: $scope.Nombre,
                    Codigo: $scope.Codigo,
                    NombreCiudad: $scope.NombreCiudad,
                    Oficina: $scope.Oficina,
                    Estado: $scope.ModalEstado.Codigo,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                }
                blockUI.delay = 1000;
                AlmacenesFactory.Consultar(filtros).
                       then(function (response) {
                           if (response.data.ProcesoExitoso === true) {
                               if (response.data.Datos.length > 0) {
                                   $scope.ListadoAlmacenes = response.data.Datos

                                   $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                   $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                   $scope.Buscando = true;
                                   $scope.ResultadoSinRegistros = '';
                               }
                               else {
                                   $scope.totalRegistros = 0;
                                   $scope.totalPaginas = 0;
                                   $scope.paginaActual = 1;
                                   $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                   $scope.Buscando = false;
                               }
                           }
                       }, function (response) {
                           ShowError(response.statusText);
                       });
            }
        }
        blockUI.stop();
    }
    /*------------------------------------------------------------------------------------Eliminar Almacen-----------------------------------------------------------------------*/
    $scope.EliminarAlmacen = function (codigo, Nombre, CodigoAlterno) {
        $scope.AuxCodigo = codigo
        $scope.Nombre = Nombre
        $scope.CodigoAlterno = CodigoAlterno
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        showModal('modalEliminarAlmacen');
    };

    $scope.Eliminar = function () {
        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.AuxCodigo,
        };

        AlmacenesFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se eliminó el almacén '+ $scope.CodigoAlterno+ ' - ' + $scope.Nombre);
                    closeModal('modalEliminarAlmacen');
                    $scope.CodigoAlterno = '';
                    $scope.Nombre = '';
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                var result = ''
                result = InternalServerError(response.statusText)
                showModal('modalMensajeEliminarAlmacen');
                $scope.ModalError = 'No se puede eliminar el almacén ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                $scope.ModalErrorCompleto = response.statusText

            });

    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /* Obtener parametros*/
    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        $scope.Codigo = $routeParams.Codigo;
        Find();
    }

    $scope.CerrarModal = function () {
        closeModal('modalEliminarAlmacen');
        closeModal('modalMensajeEliminarAlmacen');
    }

    $scope.MaskNumero = function () {
        $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno)
        
    };

}]);