﻿SofttoolsApp.controller("GestionarReferenciaProveedoresCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ReferenciasFactory', 'TercerosFactory', 'ReferenciaProveedoresFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, ReferenciasFactory, TercerosFactory, ReferenciaProveedoresFactory) {

    $scope.Titulo = 'GESTIONAR REFERENCIA PROVEEDORES';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Almacén' }, { Nombre: 'Referencia Proveedores' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_REFERENCIA_PROVEEDORES);

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
    $scope.ListadoReferencias = []

    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoAlterno: 0,
        Nombre: '',
        Estado: 0
    }

    $scope.ListadoEstados = [
        { Codigo: 0, Nombre: "INACTIVA" },
        { Codigo: 1, Nombre: "ACTIVA" },
    ]
    $scope.Modelo.Referencia = ''
    $scope.CodigoReferencia = ''
    $scope.Modelo.NombreReferencia = ''
    $scope.ListadoProveedores = [];

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Obtener();
    }
    else {
        $scope.Modelo.Codigo = 0;
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
    }

    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROVEEDOR }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoProveedores = []
                if (response.data.Datos.length > 0) {
                    $scope.ListadoProveedores = response.data.Datos;
                    $scope.ListadoProveedores.push({ NombreCompleto: "", Codigo: 0 })

                    if ($scope.Proveedor > 0) {
                        $scope.Modelo.Proveedor = $linq.Enumerable().From($scope.ListadoProveedores).First('$.Codigo ==' + $scope.Proveedor);
                    } else {
                        $scope.Modelo.Proveedor = $scope.ListadoProveedores[$scope.ListadoProveedores.length - 1];
                    }
                }
                else {
                    $scope.ListadoProveedores = [];
                }
            }
        }, function (response) {
            ShowError(response.statusText);
        });


    /*Cargar el combo de Referencias*/
    ReferenciasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoReferencias = [];
                $scope.ListadoReferencias = response.data.Datos;

                if ($scope.CodigoReferencia !== undefined && $scope.CodigoReferencia !== null && $scope.CodigoReferencia !== '') {
                    $scope.Modelo.NombreReferencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + $scope.CodigoReferencia);
                    $scope.Modelo.Referencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + $scope.CodigoReferencia);
                }
                CarReferencias();
            }
        }, function (response) {
            ShowError(response.statusText);
        });
    function CarReferencias() {

        $timeout(function () {
            if ($('#CmbReferencia')[0].options[0].value == "?") {
                $('#CmbReferencia')[0].options[0].remove()
            }
        }, 50);
        $timeout(function () {
            $('#CmbReferencia').selectpicker()
        }, 200);
        $timeout(function () {
            if ($('#CmbNombreReferencia')[0].options[0].value == "?") {
                $('#CmbNombreReferencia')[0].options[0].remove()
            }
        }, 50);
        $timeout(function () {
            $('#CmbNombreReferencia').selectpicker('refresh')
        }, 200);
    }



    $scope.CarReferencia = function (Referencia) {
        if (Referencia.Codigo !== undefined) {

            $('#CmbNombreReferencia').selectpicker('destroy')
            $timeout(function () {
                if ($('#CmbNombreReferencia')[0].options[0].value == "?" || $('#CmbNombreReferencia')[0].options[0].value == "") {
                    $('#CmbNombreReferencia')[0].options[0].remove()
                }
            }, 50);
            $timeout(function () {
                $('#CmbNombreReferencia').selectpicker()
            }, 200);

            $scope.Modelo.Referencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + Referencia.Codigo);
            $scope.Modelo.NombreReferencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + Referencia.Codigo);
            $scope.CodigoReferencia = $scope.Modelo.NombreReferencia.Codigo
        }
    }

    $scope.CarNombreReferencia = function (Referencia) {
        if (Referencia.Codigo !== undefined) {

            $('#CmbReferencia').selectpicker('destroy')
            $timeout(function () {
                if ($('#CmbReferencia')[0].options[0].value == "?" || $('#CmbReferencia')[0].options[0].value == "") {
                    $('#CmbReferencia')[0].options[0].remove()
                }
            }, 50);
            $timeout(function () {
                $('#CmbReferencia').selectpicker()
            }, 200);

            $scope.Modelo.Referencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + Referencia.Codigo);
            $scope.Modelo.NombreReferencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + Referencia.Codigo);
            $scope.CodigoReferencia = $scope.Modelo.NombreReferencia.Codigo
        }
    }

    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function Obtener() {
        blockUI.start('Cargando referencia proveedor código ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando referencia proveedor Código ' + $scope.Modelo.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        ReferenciaProveedoresFactory.Obtener(filtros).
           then(function (response) {
               if (response.data.ProcesoExitoso === true) {
                   $scope.Modelo = response.data.Datos;
                   $scope.Modelo.Codigo = response.data.Datos.Codigo;


                   $scope.Proveedor = response.data.Datos.Proveedor.Codigo;

                   if ($scope.ListadoProveedores.length > 0) {
                       $scope.Modelo.Proveedor = $linq.Enumerable().From($scope.ListadoProveedores).First('$.Codigo ==' + response.data.Datos.Proveedor.Codigo);
                   }

                   $scope.CodigoReferencia = response.data.Datos.Referencia.Codigo;

                   if ($scope.ListadoReferencias.length > 0) {
                       $scope.Modelo.NombreReferencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + response.data.Datos.Referencia.Codigo);
                       $scope.Modelo.Referencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + response.data.Datos.Referencia.Codigo);
                   }

                   $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

               }
               else {
                   ShowError('No se logro consultar la referencia proveedor código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                   document.location.href = '#!ConsultarReferenciaProveedores';
               }
           }, function (response) {
               ShowError(response.statusText);
               document.location.href = '#!ConsultarReferenciaProveedores';
           });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    $scope.ConfirmacionGuardar = function () {
        showModal('modalConfirmacionGuardar');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardar');
        if (DatosRequeridos()) {

            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
            $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            $scope.Modelo.NombreReferencia = $scope.Modelo.NombreReferencia.Nombre;

            ReferenciaProveedoresFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó la referencia proveedores "' + $scope.Modelo.Proveedor.NombreCompleto + ' - ' + $scope.Modelo.Referencia.Nombre + '"');
                                $scope.Modelo.Codigo = response.data.Datos;
                            }
                            else {
                                ShowSuccess('Se modificó la referencia proveedores "' + $scope.Modelo.Proveedor.NombreCompleto + ' - ' + $scope.Modelo.Referencia.Nombre + '"');
                            }
                            location.href = '#!ConsultarReferenciaProveedores/' + $scope.Modelo.Codigo;
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;
        if ($scope.Modelo.Proveedor == undefined || $scope.Modelo.Proveedor == "" || $scope.Modelo.Proveedor == null || $scope.Modelo.Proveedor.Codigo == 0) {
            $scope.MensajesError.push('Debe ingresar un proveedor');
            continuar = false;
        }
        if ($scope.Modelo.Referencia == undefined || $scope.Modelo.Referencia == "" || $scope.Modelo.Referencia == null) {
            $scope.MensajesError.push('Debe ingresar una referencia valida');
            continuar = false;
        }
        if ($scope.Modelo.ReferenciaProveedor == undefined || $scope.Modelo.ReferenciaProveedor == "" || $scope.Modelo.ReferenciaProveedor == null) {
            $scope.MensajesError.push('Debe ingresar la referencia del proveedor');
            continuar = false;
        }
        return continuar;
    }

    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarReferenciaProveedores/' + $scope.Modelo.Codigo;
    };
    $scope.MaskMayus = function () {
        try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
    };
}]);