﻿SofttoolsApp.controller("GestionarUbicacionAlmacenesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'UbicacionAlmacenesFactory', 'AlmacenesFactory', 'ValorCatalogosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, UbicacionAlmacenesFactory, AlmacenesFactory, ValorCatalogosFactory) {

        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Almacén' }, { Nombre: 'Ubicación Almacén' }, { Nombre: 'Gestionar' }];
        $scope.Titulo = 'EDICIÓN UBICACIÓN ALMACÉN';
        $scope.ListadoDetalle = [];
        $scope.Fecha = new Date();
        $scope.ListadoTipoUbicacion = [];
        $scope.ListadoCantidadColumnas = [];
        ListadoCantidadFilas = [];
        $scope.MensajesErrorDetalleUbicacion = [];
        $scope.DeshabilitarNivel = true
        $scope.Columna1 = [];
        $scope.Columna2 = [];
        $scope.Columna3 = [];
        $scope.Columna4 = [];
        $scope.Columna5 = [];
        $scope.Columna6 = [];
        $scope.Columna7 = [];
        $scope.Columna8 = [];
        $scope.Columna9 = [];
        $scope.Columna10 = [];
        $scope.Puestos = 0
        $scope.DetalleUbicacionAlmacen = [];
        $scope.Detalle = [];
        $scope.ModificarDetalle = false
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_UBICACION_ALMACENES);
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        };

        $scope.CODIGO_VALIDO = 16001; 
        $scope.CODIGO_VACIO = 16002;
        $scope.CODIGO_PASILLO = 16003;
        $scope.CODIGO_NO_APLICA_UBICACION = 16000;

        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
        }
        else {
            $scope.Modelo.Codigo = 0;
        }
        // -------------------------- Constantes ---------------------------------------------------------------------//

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            
            if ($scope.Modelo.Codigo != undefined && $scope.Modelo.Codigo != null && $scope.Modelo.Codigo != '' && isNaN($scope.Modelo.Codigo)) {
                document.location.href = '#!ConsultarUbicacionAlmacenes' + $scope.Modelo.Codigo;
            } else {
                document.location.href = '#!ConsultarUbicacionAlmacenes';
            }
        };
        $scope.ConfirmacionGuardarUbicacion = function () {
            showModal('modalConfirmacionGuardar');
        };
        /*$scope.ConfirmacionGuardarUbicacion = function () {
            showModal('modalConfirmacionGuardarUbicacion');
        };*/
        /*-------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            $scope.Titulo = 'EDITAR UBICACIÓN ALMACÉN';
            Obtener();
        }
        /*-------------------------------------------------------------------------------------------Cargar Combos/Autocomplete----------------------------------------------------------------*/
        /*Cargar el combo de estados*/
        $scope.ListadoEstados = [
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        /*Cargar el combo tipo Ubicacion Almacen*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_UBICACION_ALMACEN } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoUbicacion = [];

                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo !== CODIGO_TIPO_UBICACION_NO_APLICA) {
                            $scope.ListadoTipoUbicacion.push(item)
                        }
                    })
                    $scope.Tipo = $scope.ListadoTipoUbicacion[0];
                    $scope.Asignartipo = $scope.ListadoTipoUbicacion[0];
                    $scope.AsignartipoFila = $scope.ListadoTipoUbicacion[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo de almacenes*/
        AlmacenesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado:  ESTADO_ACTIVO  }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoAlmacenes = [];
                    $scope.ListadoAlmacenes = response.data.Datos
                    if ($scope.CodigoAlmacen !== undefined && $scope.CodigoAlmacen !== null && $scope.CodigoAlmacen > 0) {
                        $scope.Almacen = $linq.Enumerable().From($scope.ListadoAlmacenes).First('$.Codigo == ' + $scope.CodigoAlmacen)
                    }
                    else {
                        $scope.Almacen = response.data.Datos[0];
                    }



                }
            }, function (response) {
                ShowError(response.statusText);
            });

        // Metodo para guardar /modificar
        $scope.Guardar = function () {
            $scope.MaskNumero();
            closeModal('modalConfirmacionGuardar');
            $scope.Detalle = [];


            if ($scope.ModificarDetalle == true) {
                $scope.Detalle = $scope.DetalleUbicacionAlmacen
            }
            else {
                $scope.DetalleUbicacionAlmacen.forEach(function (item) {
                    item.forEach(function (itemDetalle) {
                        $scope.Detalle.push(itemDetalle)
                    })
                })

            }

            $scope.Modelo.NumeroFilas = $scope.Filas;
            $scope.Modelo.Almacen = $scope.Almacen;
            $scope.Modelo.NumeroColumnas = $scope.Columnas;
            $scope.Modelo.NumeroNiveles = $scope.Niveles;
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
            $scope.Modelo.Detalles = $scope.Detalle;

            if (DatosRequeridos()) {
                UbicacionAlmacenesFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó la ubicación para el almacén ' + $scope.Modelo.Almacen.Nombre);
                                    location.href = '#!ConsultarUbicacionAlmacenes/' + response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modificó la ubicación para el almacén ' + $scope.Modelo.Almacen.Nombre);
                                    location.href = '#!ConsultarUbicacionAlmacenes/' + response.data.Datos;
                                }

                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            
        };
        // Funcion Datos requeridos 
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Almacen == undefined || $scope.Almacen == '' || $scope.Almacen == null) {
                $scope.MensajesError.push('Debe seleccionar un almacén');
                continuar = false;
            }
            if ($scope.Filas == '' || $scope.Filas == undefined || $scope.Filas == null) {
                $scope.MensajesError.push('Debe ingresar la cantidad de filas');
                continuar = false;
            }
            if ($scope.Columnas == '' || $scope.Columnas == undefined || $scope.Columnas == null) {
                $scope.MensajesError.push('Debe ingresar la cantidad de  columnas');
                continuar = false;
            }
            if ($scope.Niveles == '' || $scope.Niveles == undefined || $scope.Niveles == null || isNaN($scope.Niveles)) {
                $scope.MensajesError.push('Debe ingresar la cantidad de niveles');
                continuar = false;
            }
            if ($scope.ModalEstado == '' || $scope.ModalEstado == undefined || $scope.ModalEstado == null) {
                $scope.MensajesError.push('Debe seleccionar un estado');
                continuar = false;
            }


            return continuar
        }

        // Metodo Obtener Contratos 
        function Obtener() {
            blockUI.start('Cargando ubicación almacén ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando ubicación almacén ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            UbicacionAlmacenesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ModificarDetalle = true
                        $scope.Modelo.Codigo = response.data.Datos.Codigo;
                        $scope.Filas = response.data.Datos.NumeroFilas;
                        $scope.Columnas = response.data.Datos.NumeroColumnas;
                        $scope.Niveles = response.data.Datos.NumeroNiveles;

                        if ($scope.ListadoAlmacenes == undefined) {
                            $scope.CodigoAlmacen = response.data.Datos.Almacen.Codigo;
                        }
                        else {
                            $scope.Almacen = $linq.Enumerable().From($scope.ListadoAlmacenes).First('$.Codigo == ' + response.data.Datos.Almacen.Codigo)
                        }

                        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        $scope.DetalleUbicacionAlmacen = response.data.Datos.Detalles;

                        $scope.ListadoCantidadColumnas = [];
                        $scope.ListadoCantidadFilas = [];
                        $scope.ListadoCantidadColumnas.push({ Columna: 'Seleccione una columna', Codigo: 0 })
                        $scope.ListadoCantidadFilas.push({ Fila: 'Seleccione una fila', Codigo: 0 })
                        for (var i = 1; i <= $scope.Columnas; i++) {
                            $scope.ListadoCantidadColumnas.push({ Columna: i, Codigo: i })
                        }

                        $scope.ListadoCantidadFilas.push({ Columna: 'Seleccione una fila', Codigo: 0 })
                        for (var i = 1; i <= $scope.Filas; i++) {
                            $scope.ListadoCantidadFilas.push({ Fila: i, Codigo: i })
                        }

                        $scope.AsignarColumna = $scope.ListadoCantidadColumnas[0]
                        $scope.AsignarFila = $scope.ListadoCantidadFilas[0]


                        $scope.DetalleUbicacionAlmacen.forEach(function (item) {
                            $scope.HabilitarDistribucion = true
                            if (item.Columna > 10) {
                                if (item.Fila == 1) {
                                    $scope.Columna1.push(item)
                                }
                                else if (item.Fila == 2) {
                                    $scope.Columna2.push(item)
                                }
                                else if (item.Fila == 3) {
                                    $scope.Columna3.push(item)
                                }
                                else if (item.Fila == 4) {
                                    $scope.Columna4.push(item)
                                }
                                else if (item.Fila == 5) {
                                    $scope.Columna5.push(item)
                                }
                                else if (item.Fila == 6) {
                                    $scope.Columna6.push(item)
                                }
                                else if (item.Fila == 7) {
                                    $scope.Columna7.push(item)
                                }
                                else if (item.Fila == 8) {
                                    $scope.Columna8.push(item)
                                }
                                else if (item.Fila == 9) {
                                    $scope.Columna9.push(item)
                                }
                                else if (item.Fila == 10) {
                                    $scope.Columna10.push(item)
                                }
                            }
                            else {
                                if (item.Columna == 1) {
                                    
                                    $scope.Columna1[item.Fila-1] = item
                                }
                                else if (item.Columna == 2) {
                                  
                                    $scope.Columna2[item.Fila - 1] = item
                                }
                                else if (item.Columna == 3) {
                                   
                                    $scope.Columna3[item.Fila - 1] = item
                                }
                                else if (item.Columna == 4) {
                                
                                    $scope.Columna4[item.Fila - 1] = item
                                }
                                else if (item.Columna == 5) {
                                   
                                    $scope.Columna5[item.Fila - 1] = item
                                }
                                else if (item.Columna == 6) {
                                    $scope.Columna6[item.Fila - 1] = item
                                }
                                else if (item.Columna == 7) {
                                    $scope.Columna7[item.Fila - 1] = item
                                }
                                else if (item.Columna == 8) {
                                    $scope.Columna8[item.Fila - 1] = item
                                }
                                else if (item.Columna == 9) {
                                    $scope.Columna9[item.Fila - 1] = item
                                }
                                else if (item.Columna == 10) {
                                    $scope.Columna10[item.Fila - 1] = item
                                }
                            }
                        })
                        /*$scope.ColumnasTotales = [
                            $scope.Col1 = angular.copy($scope.Columna1),
                            $scope.Col2 = angular.copy($scope.Columna2),
                            $scope.Col3 = angular.copy($scope.Columna3),
                            $scope.Col4 = angular.copy($scope.Columna4),
                            $scope.Col5 = angular.copy($scope.Columna5),
                            $scope.Col6 = angular.copy($scope.Columna6),
                            $scope.Col7 = angular.copy($scope.Columna7),
                            $scope.Col8 = angular.copy($scope.Columna8),
                            $scope.Col9 = angular.copy($scope.Columna9),
                            $scope.Col10 = angular.copy($scope.Columna10)
                        ];

                        $scope.ColumnasTotales.forEach(col => {
                            var newCol = [];                            
                                for (var i = 0; i < col.length; i++) {
                                    /*if (col[i].Fila == i + 1) {
                                        col[i] = col[i];
                                        count++;
                                    }*/
                         /*           newCol[col[i].Fila - 1] = col[i];
                                }
                            col = newCol;
                            
                        });*/

                    }
                    else {
                        ShowError('No se logro consultar la Ubicación Referencias No.' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarUbicacionAlmacenes';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarUbicacionAlmacenes';
                });

            blockUI.stop();
        };

        $scope.SeleccionarUbicacion = function (columna) {
            showModal('modaldetalleAlmacen');
            $scope.Nivel = ''
            $scope.Columna = columna.Columna
            $scope.Fila = columna.Fila
            if (columna.CodigoTipoUbicacion == $scope.CODIGO_VALIDO) {
                if (columna.Nivel == '' || columna.Nivel == undefined) {
                    $scope.Nivel = $scope.Niveles
                }
                else {
                    $scope.Nivel = columna.Nivel;
                }
            }
            if (columna.CodigoTipoUbicacion != $scope.CODIGO_NO_APLICA_UBICACION) {
                $scope.Tipo = $linq.Enumerable().From($scope.ListadoTipoUbicacion).First('$.Codigo == ' + columna.CodigoTipoUbicacion);
            }
            $scope.Ubicacion = columna
            if ($scope.Modelo.Codigo > 0) {
                if (columna.Nivel !== undefined) {
                    $scope.Nivel = columna.Nivel;
                    $scope.Tipo = $linq.Enumerable().From($scope.ListadoTipoUbicacion).First('$.Codigo == ' + columna.CodigoTipoUbicacion);
                    //$scope.ModificarDetalle = true
                }
            }
        }
        $scope.ValidarTipoUbicacion = function (TipoUbicacion) {
            if ($scope.CODIGO_VALIDO == TipoUbicacion.Codigo) {
                if ($scope.Nivel == '' || $scope.Nivel == undefined) {
                    $scope.Nivel = $scope.Niveles
                }
                $scope.DeshabilitarNivel = false
            }
            else {
                $scope.DeshabilitarNivel = true
                $scope.Nivel = ''
            }
        };

        $scope.AsignarColumnas = function () {
            $scope.MensajesError = [];
            if ($scope.AsignarColumna.Codigo !== 0) {
                $scope.Columna1.forEach(function (itemcolumna1) {
                    if (itemcolumna1.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna1.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })

                $scope.Columna2.forEach(function (itemcolumna2) {
                    if (itemcolumna2.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna2.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })

                $scope.Columna3.forEach(function (itemcolumna3) {
                    if (itemcolumna3.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna3.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })

                $scope.Columna4.forEach(function (itemcolumna4) {
                    if (itemcolumna4.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna4.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })

                $scope.Columna5.forEach(function (itemcolumna5) {
                    if (itemcolumna5.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna5.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })
                $scope.Columna6.forEach(function (itemcolumna6) {
                    if (itemcolumna6.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna6.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })
                $scope.Columna7.forEach(function (itemcolumna7) {
                    if (itemcolumna7.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna7.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })
                $scope.Columna8.forEach(function (itemcolumna8) {
                    if (itemcolumna8.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna8.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })
                $scope.Columna9.forEach(function (itemcolumna9) {
                    if (itemcolumna9.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna9.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })
                $scope.Columna10.forEach(function (itemcolumna10) {
                    if (itemcolumna10.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna10.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })
            }
            else {
                $scope.MensajesError.push(' seleccione una columna válida')
            }
        }

        $scope.AsignarFilas = function () {
            $scope.MensajesError = [];
            $scope.ColumnasTotales = [
                $scope.Columna1,
                $scope.Columna2,
                $scope.Columna3,
                $scope.Columna4,
                $scope.Columna5,
                $scope.Columna6,
                $scope.Columna7,
                $scope.Columna8,
                $scope.Columna9,
                $scope.Columna10,
            ];
            if ($scope.AsignarFila.Codigo !== 0) {
                $scope.ColumnasTotales.forEach(col => {
                    if (col[$scope.AsignarFila.Codigo - 1] != undefined && col[$scope.AsignarFila.Codigo - 1] != null) {
                        col[$scope.AsignarFila.Codigo - 1].CodigoTipoUbicacion = $scope.AsignartipoFila.Codigo
                    } else {
                        console.log("error en :" + col.toString());
                    }
                });
            }
            else {
                $scope.MensajesError.push('seleccione una fila válida')
            }
        }

        $scope.AsignarNivel = function (TipoUbicacion, Nivel, Fila) {
            $scope.MensajesErrorDetalleUbicacion = [];
            $scope.Columna1.forEach(function (itemcolumna1) {
                if (itemcolumna1.Columna == $scope.Ubicacion.Columna && itemcolumna1.Fila == $scope.Ubicacion.Fila) {
                    itemcolumna1.CodigoTipoUbicacion = TipoUbicacion.Codigo
                    itemcolumna1.Nivel = Nivel
                }
            })

            $scope.Columna2.forEach(function (itemcolumna2) {
                if (itemcolumna2.Columna == $scope.Ubicacion.Columna && itemcolumna2.Fila == $scope.Ubicacion.Fila) {
                    itemcolumna2.CodigoTipoUbicacion = TipoUbicacion.Codigo
                    itemcolumna2.Nivel = Nivel
                }
            })

            $scope.Columna3.forEach(function (itemcolumna3) {
                if (itemcolumna3.Columna == $scope.Ubicacion.Columna && itemcolumna3.Fila == $scope.Ubicacion.Fila) {
                    itemcolumna3.CodigoTipoUbicacion = TipoUbicacion.Codigo
                    itemcolumna3.Nivel = Nivel
                }
            })

            $scope.Columna4.forEach(function (itemcolumna4) {
                if (itemcolumna4.Columna == $scope.Ubicacion.Columna && itemcolumna4.Fila == $scope.Ubicacion.Fila) {
                    itemcolumna4.CodigoTipoUbicacion = TipoUbicacion.Codigo
                    itemcolumna4.Nivel = Nivel
                }
            })

            $scope.Columna5.forEach(function (itemcolumna5) {
                if (itemcolumna5.Columna == $scope.Ubicacion.Columna && itemcolumna5.Fila == $scope.Ubicacion.Fila) {
                    itemcolumna5.CodigoTipoUbicacion = TipoUbicacion.Codigo
                    itemcolumna5.Nivel = Nivel
                }
            })
            $scope.Columna6.forEach(function (itemcolumna6) {
                if (itemcolumna6.Columna == $scope.Ubicacion.Columna && itemcolumna6.Fila == $scope.Ubicacion.Fila) {
                    itemcolumna6.CodigoTipoUbicacion = TipoUbicacion.Codigo
                    itemcolumna6.Nivel = Nivel
                }
            })
            $scope.Columna7.forEach(function (itemcolumna7) {
                if (itemcolumna7.Columna == $scope.Ubicacion.Columna && itemcolumna7.Fila == $scope.Ubicacion.Fila) {
                    itemcolumna7.CodigoTipoUbicacion = TipoUbicacion.Codigo
                    itemcolumna7.Nivel = Nivel
                }
            })
            $scope.Columna8.forEach(function (itemcolumna8) {
                if (itemcolumna8.Columna == $scope.Ubicacion.Columna && itemcolumna8.Fila == $scope.Ubicacion.Fila) {
                    itemcolumna8.CodigoTipoUbicacion = TipoUbicacion.Codigo
                    itemcolumna8.Nivel = Nivel
                }
            })
            $scope.Columna9.forEach(function (itemcolumna9) {
                if (itemcolumna9.Columna == $scope.Ubicacion.Columna && itemcolumna9.Fila == $scope.Ubicacion.Fila) {
                    itemcolumna9.CodigoTipoUbicacion = TipoUbicacion.Codigo
                    itemcolumna9.Nivel = Nivel
                }
            })
            $scope.Columna10.forEach(function (itemcolumna10) {
                if (itemcolumna10.Columna == $scope.Ubicacion.Columna && itemcolumna10.Fila == $scope.Ubicacion.Fila) {
                    itemcolumna10.CodigoTipoUbicacion = TipoUbicacion.Codigo
                    itemcolumna10.Nivel = Nivel
                }
            })
            closeModal('modaldetalleAlmacen');
        }

        function DatosRequeridosPintarBus() {
            var continuar = true
            $scope.MensajesError = []
            if ($scope.Columnas == undefined || $scope.Columnas == '') {
                $scope.MensajesError.push('Debe ingresar la cantidad de columnas ');
                continuar = false;
            }
            else if ($scope.Columnas > 10) {
                $scope.MensajesError.push('La cantidad de columnas debe ser menor a 10 ');
                continuar = false;
            }

            if ($scope.Filas == undefined || $scope.Filas == '') {
                $scope.MensajesError.push('Debe ingresar la cantidad de filas ');
                continuar = false;
            }

            return continuar;
        }
        //Pintar almacen
        $scope.PintarAlmacen = function () {
            $scope.Columna1 = [];
            $scope.Columna2 = [];
            $scope.Columna3 = [];
            $scope.Columna4 = [];
            $scope.Columna5 = [];
            $scope.Columna6 = [];
            $scope.Columna7 = [];
            $scope.Columna8 = [];
            $scope.Columna9 = [];
            $scope.Columna10 = [];
            $scope.DetalleUbicacionAlmacen = [];
            $scope.ListadoCantidadColumnas = [];
            $scope.ListadoCantidadFilas = [];
            $scope.DetalleUbicacion = [];
            $scope.ModificarDetalle = false;
            $scope.Columnas = Math.ceil($scope.Columnas)
            $scope.Filas = Math.ceil($scope.Filas)

            $scope.ListadoCantidadColumnas.push({ Columna: 'Seleccione una columna', Codigo: 0 })
            for (var i = 1; i <= $scope.Columnas; i++) {
                $scope.ListadoCantidadColumnas.push({ Columna: i, Codigo: i })
            }

            $scope.ListadoCantidadFilas.push({ Fila: 'Seleccione una fila', Codigo: 0 })
            for (var i = 1; i <= $scope.Filas; i++) {
                $scope.ListadoCantidadFilas.push({ Fila: i, Codigo: i })
            }

            $scope.AsignarColumna = $scope.ListadoCantidadColumnas[0]
            $scope.AsignarFila = $scope.ListadoCantidadFilas[0]
            if (DatosRequeridosPintarBus()) {
                $scope.HabilitarDistribucion = true
                if ($scope.Columnas == 10) {
                    for (var i = 1; i <= $scope.Columnas; i++) {
                        for (var j = 1; j <= $scope.Filas; j++) {
                            if (i == 10) {
                                $scope.Columna10.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                            } else {
                                if (i == 9) {
                                    $scope.Columna9.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                } else {
                                    if (i == 8) {
                                        $scope.Columna8.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                    } else {
                                        if (i == 7) {
                                            $scope.Columna7.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                        } else {
                                            if (i == 6) {
                                                $scope.Columna6.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                            } else {
                                                if (i == 5) {
                                                    $scope.Columna5.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                } else {
                                                    if (i == 4) {
                                                        $scope.Columna4.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                    } else {
                                                        if (i == 3) {
                                                            $scope.Columna3.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                        } else {
                                                            if (i == 2) {
                                                                $scope.Columna2.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                            } else {
                                                                if (i == 1) {
                                                                    $scope.Columna1.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if ($scope.Columnas == 9) {
                    for (var i = 1; i <= $scope.Columnas; i++) {
                        for (var j = 1; j <= $scope.Filas; j++) {
                            if (i == 9) {
                                $scope.Columna9.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                            } else {
                                if (i == 8) {
                                    $scope.Columna8.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                } else {
                                    if (i == 7) {
                                        $scope.Columna7.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                    } else {
                                        if (i == 6) {
                                            $scope.Columna6.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                        } else {
                                            if (i == 5) {
                                                $scope.Columna5.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                            } else {
                                                if (i == 4) {
                                                    $scope.Columna4.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                } else {
                                                    if (i == 3) {
                                                        $scope.Columna3.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                    } else {
                                                        if (i == 2) {
                                                            $scope.Columna2.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                        } else {
                                                            if (i == 1) {
                                                                $scope.Columna1.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if ($scope.Columnas == 8) {
                    for (var i = 1; i <= $scope.Columnas; i++) {
                        for (var j = 1; j <= $scope.Filas; j++) {
                            if (i == 8) {
                                $scope.Columna8.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                            } else {
                                if (i == 7) {
                                    $scope.Columna7.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                } else {
                                    if (i == 6) {
                                        $scope.Columna6.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                    } else {
                                        if (i == 5) {
                                            $scope.Columna5.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                        } else {
                                            if (i == 4) {
                                                $scope.Columna4.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                            } else {
                                                if (i == 3) {
                                                    $scope.Columna3.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                } else {
                                                    if (i == 2) {
                                                        $scope.Columna2.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                    } else {
                                                        if (i == 1) {
                                                            $scope.Columna1.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if ($scope.Columnas == 7) {
                    for (var i = 1; i <= $scope.Columnas; i++) {
                        for (var j = 1; j <= $scope.Filas; j++) {
                            if (i == 7) {
                                $scope.Columna7.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                            } else {
                                if (i == 6) {
                                    $scope.Columna6.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                } else {
                                    if (i == 5) {
                                        $scope.Columna5.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                    } else {
                                        if (i == 4) {
                                            $scope.Columna4.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                        } else {
                                            if (i == 3) {
                                                $scope.Columna3.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                            } else {
                                                if (i == 2) {
                                                    $scope.Columna2.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                } else {
                                                    if (i == 1) {
                                                        $scope.Columna1.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if ($scope.Columnas == 6) {
                    for (var i = 1; i <= $scope.Columnas; i++) {
                        for (var j = 1; j <= $scope.Filas; j++) {
                            if (i == 6) {
                                $scope.Columna6.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                            } else {
                                if (i == 5) {
                                    $scope.Columna5.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                } else {
                                    if (i == 4) {
                                        $scope.Columna4.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                    } else {
                                        if (i == 3) {
                                            $scope.Columna3.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                        } else {
                                            if (i == 2) {
                                                $scope.Columna2.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                            } else {
                                                if (i == 1) {
                                                    $scope.Columna1.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if ($scope.Columnas == 5) {
                    for (var i = 1; i <= $scope.Columnas; i++) {
                        for (var j = 1; j <= $scope.Filas; j++) {
                            if (i == 5) {
                                $scope.Columna5.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                            } else {
                                if (i == 4) {
                                    $scope.Columna4.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                } else {
                                    if (i == 3) {
                                        $scope.Columna3.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                    } else {
                                        if (i == 2) {
                                            $scope.Columna2.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                        } else {
                                            if (i == 1) {
                                                $scope.Columna1.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if ($scope.Columnas == 4) {
                    for (var i = 1; i <= $scope.Columnas; i++) {
                        for (var j = 1; j <= $scope.Filas; j++) {
                            if (i == 4) {
                                $scope.Columna4.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                            } else {
                                if (i == 3) {
                                    $scope.Columna3.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                } else {
                                    if (i == 2) {
                                        $scope.Columna2.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                    } else {
                                        if (i == 1) {
                                            $scope.Columna1.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if ($scope.Columnas == 3) {
                    for (var i = 1; i <= $scope.Columnas; i++) {
                        for (var j = 1; j <= $scope.Filas; j++) {
                            if (i == 3) {
                                $scope.Columna3.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                            } else {
                                if (i == 2) {
                                    $scope.Columna2.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                } else {
                                    if (i == 1) {
                                        $scope.Columna1.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                    }
                                }
                            }
                        }
                    }
                }
                else if ($scope.Columnas <= 2) {
                    for (var i = 1; i <= $scope.Columnas; i++) {
                        for (var j = 1; j <= $scope.Filas; j++) {
                            if (i == 2) {
                                $scope.Columna2.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                            } else {
                                if (i == 1) {
                                    $scope.Columna1.push({ Columna: i, Fila: j, CodigoTipoUbicacion: $scope.CODIGO_NO_APLICA_UBICACION, Nivel: '' })
                                }
                            }
                        }
                    }
                }
            }
            if ($scope.Columna1.length > 0) {
                $scope.DetalleUbicacionAlmacen.push($scope.Columna1)
            }
            if ($scope.Columna2.length > 0) {
                $scope.DetalleUbicacionAlmacen.push($scope.Columna2)
            }
            if ($scope.Columna3.length > 0) {
                $scope.DetalleUbicacionAlmacen.push($scope.Columna3)
            }
            if ($scope.Columna4.length > 0) {
                $scope.DetalleUbicacionAlmacen.push($scope.Columna4)
            }
            if ($scope.Columna5.length > 0) {
                $scope.DetalleUbicacionAlmacen.push($scope.Columna5)
            }
            if ($scope.Columna6.length > 0) {
                $scope.DetalleUbicacionAlmacen.push($scope.Columna6)
            }
            if ($scope.Columna7.length > 0) {
                $scope.DetalleUbicacionAlmacen.push($scope.Columna7)
            }
            if ($scope.Columna8.length > 0) {
                $scope.DetalleUbicacionAlmacen.push($scope.Columna8)
            }
            if ($scope.Columna9.length > 0) {
                $scope.DetalleUbicacionAlmacen.push($scope.Columna9)
            }
            if ($scope.Columna10.length > 0) {
                $scope.DetalleUbicacionAlmacen.push($scope.Columna10)
            }
        }
        $scope.AsignarColumnas = function () {
            $scope.MensajesError = [];
            if ($scope.AsignarColumna.Codigo !== 0) {
                $scope.Columna1.forEach(function (itemcolumna1) {
                    if (itemcolumna1.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna1.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })

                $scope.Columna2.forEach(function (itemcolumna2) {
                    if (itemcolumna2.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna2.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })

                $scope.Columna3.forEach(function (itemcolumna3) {
                    if (itemcolumna3.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna3.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })

                $scope.Columna4.forEach(function (itemcolumna4) {
                    if (itemcolumna4.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna4.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })

                $scope.Columna5.forEach(function (itemcolumna5) {
                    if (itemcolumna5.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna5.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })
                $scope.Columna6.forEach(function (itemcolumna6) {
                    if (itemcolumna6.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna6.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })
                $scope.Columna7.forEach(function (itemcolumna7) {
                    if (itemcolumna7.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna7.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })
                $scope.Columna8.forEach(function (itemcolumna8) {
                    if (itemcolumna8.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna8.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })
                $scope.Columna9.forEach(function (itemcolumna9) {
                    if (itemcolumna9.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna9.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })
                $scope.Columna10.forEach(function (itemcolumna10) {
                    if (itemcolumna10.Columna == $scope.AsignarColumna.Columna) {
                        itemcolumna10.CodigoTipoUbicacion = $scope.Asignartipo.Codigo
                    }
                })
            }
            else {
                $scope.MensajesError.push(' seleccione una columna válida')
            }
        }
}]);