﻿SofttoolsApp.controller("ConsultarCombinacionesCvtCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'TercerosFactory', 'ProductoTransportadosFactory',
    'CombinacionContratoVinculacionFactory',
    function ($scope, $timeout, $linq, blockUI, blockUIConfig, $routeParams, TercerosFactory, ProductoTransportadosFactory,
        CombinacionContratoVinculacionFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CONSULTAR COMBINACIONES CVT';
        $scope.MapaSitio = $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Combinaciones Cvt' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_COMBINACIONES_CVT);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.Modelo = {
            Codigo: 0,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
        };
        $scope.ListadoCombinacionesCvt = [];
        $scope.ListadoCliente = [];
        $scope.ListadoRemitente = [];
        $scope.ListadoTenedor = [];
        $scope.ListadoProducto = [];
        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: ESTADO_ACTIVO },
            { Nombre: 'INACTIVO', Codigo: ESTADO_INACTIVO }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==-1');
        //--------------------------Variables-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------AutoComplete
        //--Clientes
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    $scope.ListadoCliente = [];
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //--Clientes
        //--Remitente
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    $scope.ListadoRemitente = [];
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_REMITENTE,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoRemitente);
                }
            }
            return $scope.ListadoRemitente;
        };
        //--Remitente
        //--Tenedor
        $scope.AutocompleteTenedor = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    $scope.ListadoTenedor = [];
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_TENEDOR,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoTenedor = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTenedor);
                }
            }
            return $scope.ListadoTenedor;
        };
        //--Tenedor
        //--Producto
        $scope.AutocompleteProducto = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    $scope.ListadoProducto = [];
                    blockUIConfig.autoBlock = false;
                    var Response = ProductoTransportadosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        AplicaTarifario: AplicaTarifaProducto,
                        Sync: true
                    });
                    $scope.ListadoProducto = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProducto);
                }
            }
            return $scope.ListadoProducto;
        };
        //--Producto
        //----------AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //----Obtener parametros
            if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== '0') {
                $scope.Modelo.Codigo = $routeParams.Codigo;
                PantallaBloqueo(Find);
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //--Limpiar AutoCompletes
        $scope.limpiarControl = function (Control) {
            switch (Control) {
                case "Cliente":
                    $scope.Modelo.Cliente = "";
                    break;
                case "Remitente":
                    $scope.Modelo.Remitente = "";
                    break;
                case "Tenedor":
                    $scope.Modelo.Tenedor = "";
                    break;
                case "Producto":
                    $scope.Modelo.Producto = "";
                    break;
            }
        };
        //--Limpiar Autocompletes
        //----------------------------Funciones Generales---------------------------------//
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            PantallaBloqueo(Find);
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            PantallaBloqueo(Find);
        };
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        //--Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarCombinacionesCvt';
            }
        };
        //--Funcion Buscar
        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0;
                PantallaBloqueo(Find);
            }
        };
        function Find() {
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoCombinacionesCvt = [];
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
                Cliente: $scope.Modelo.Cliente,
                Remitente: $scope.Modelo.Remitente,
                Tenedor: $scope.Modelo.Tenedor,
                Producto: $scope.Modelo.Producto,
                Estado: $scope.Modelo.Estado.Codigo,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas
            };
            CombinacionContratoVinculacionFactory.Consultar(filtro).
                then(function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Codigo = 0;
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCombinacionesCvt = response.data.Datos;
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    ShowError(response.statusText);
                });

        }
        //--Funcion Buscar
        //------------Anular------------//
        $scope.EliminarCombinacion = function (codigo) {
            $scope.AuxiCodigo = codigo;
            $scope.ModalErrorCompleto = '';
            $scope.ModalError = '';
            $scope.MostrarMensajeError = false;
            showModal('modalEliminarCombinacion');
        };
        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.AuxiCodigo
            };

            CombinacionContratoVinculacionFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó la combinación cvt con el código ' + $scope.AuxiCodigo);
                        closeModal('modalEliminarCombinacion');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError('No se pudo eliminar la combinación cvt con el código ' + $scope.AuxiCodigo);
                    showModal('modalMensajeEliminarPeaje');
                });
        };
        //------------Anular------------//
        //----------------------------Funciones Generales---------------------------------//
    }]);