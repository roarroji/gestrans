﻿SofttoolsApp.controller("ConsultarValorCombustibleCtrl", ['$scope', '$linq', 'blockUI', '$routeParams','$timeout', 'blockUIConfig', 'ValorCatalogosFactory','ValorCombustibleFactory',
    function ($scope, $linq, blockUI, $routeParams, $timeout,  blockUIConfig, ValorCatalogosFactory, ValorCombustibleFactory ) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Valor Combustible' }];


  // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        $scope.FechaInicial = new Date();
        $scope.FechaFinal = new Date();
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1; 
        $scope.Titulo = 'VALOR COMBUSTIBLE';
        $scope.TipoCombustible = '';

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO; 


        $scope.Modelo = {

            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            TipoCombustible: {},
            Valor: '',
            Fecha: ''

        }; 

        //---------------------Validacion de permisos --------------------///
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ORDEN_SERVICIO); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);


        ///////////////Funcion lista de catalogo
        $scope.tipoCombustible = ValorCatalogosFactory.Consultar({
             CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
             Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_COMBUSTIBLE },
           Sync: true
         }).Datos;

        $scope.tipoCombustible.push({
            Nombre: '(TODOS)',
            Codigo: -1
        }) 
            $scope.TipoCombustible = $linq.Enumerable().From($scope.tipoCombustible).First('$.Codigo==-1');
               
        if ($routeParams.Numero > CERO) { 
            console.log('llego',$routeParams)
            $scope.TipoCombustible = $linq.Enumerable().From($scope.tipoCombustible).First($routeParams.Numero);
            FindParam(); 
        } 

        //Funcion Nuevo Valor
        $scope.NuevoValor = function () {
                if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                    document.location.href = '#!GestionarValorCombustible';
                } 
            
            }; 
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        }; 


        $scope.EliminarCombustible = function (Fecha, TipoCombustible,CodigoCombus,Valor) {
            $scope.TipoCombustible.Codigo = TipoCombustible, 
            $scope.Modelo.Valor = Valor
            $scope.Modelo.TipoCombustible.Nombre = TipoCombustible
            $scope.Modelo.TipoCombustible.Codigo = CodigoCombus

            $scope.Modelo.Fecha = Fecha
           
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false

           showModal('modalEliminarCombustible');
        };

        $scope.Eliminar = function () { 
            ValorCombustibleFactory.Anular($scope.Modelo).
                then(function (response) {
                        closeModal('modalEliminarCombustible'); 
                    if (response.data.ProcesoExitoso === true) { 
                        ShowSuccess('Se eliminó  el Combustible ' + $scope.Modelo.TipoCombustible.Nombre);
                         Find();
                     }
                     else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarColorVehiculos');
                    $scope.ModalErrorCompleto = response.statusText

                });

        };

       


        function FindParam() {
          
            blockUI.start('Buscando registros ...'); 
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100); 
            $scope.Buscando = true;
            $scope.MensajesError = []; 
            $scope.ListadoValoresCombustible = [];  
            if ($scope.Buscando) {  
                if ($scope.MensajesError.length === 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        FechaInicio: $scope.FechaInicial,
                        FechaFin: $scope.FechaFinal,
                        TipoCombustible: $scope.TipoCombustible
                    }
                    blockUI.delay = 1000;
                  
                    ValorCombustibleFactory.Consultar(filtros). 
                        then(function (response) { 
                            if (response.data.ProcesoExitoso === true) {
                                console.log(response)

                                if (response.data.Datos.length > 0) {
                                    response.data.Datos.forEach(function (item) {
                                        var registro = item;
                                        $scope.ListadoValoresCombustible.push(registro);
                                    });
                                    $scope.ListadoValoresCombustible.forEach(function (item) {
                                        $scope.codigo = item.Codigo;
                                    });
                               
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }

                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            $scope.TipoCombustible = $linq.Enumerable().From($scope.tipoCombustible).First('$.Codigo==-1');
            blockUI.stop();
        }
 




    function Find() {
        blockUI.start('Buscando registros ...');
        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);

        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoValoresCombustible = [];
        if ($scope.Buscando) {
            if ($scope.MensajesError.length === 0) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    FechaInicio: $scope.FechaInicial,
                    FechaFin: $scope.FechaFinal,
                    TipoCombustible: $scope.TipoCombustible
                }
                blockUI.delay = 1000;
                ValorCombustibleFactory.Consultar(filtros).

                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) { 
                            if (response.data.Datos.length > 0) {
                                response.data.Datos.forEach(function (item) {
                                    var registro = item;
                                    $scope.ListadoValoresCombustible.push(registro);
                                });
                                $scope.ListadoValoresCombustible.forEach(function (item) {
                                    $scope.codigo = item.Codigo;
                                });
                                //console.log(ListadoValoresCombustible)
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                            var listado = [];
                            response.data.Datos.forEach(function (item) {
                                listado.push(item);  
                          });
                        }

                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        blockUI.stop();
        }



    }
    ]);
