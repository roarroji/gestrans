﻿SofttoolsApp.controller("GestionarAsignacionPrecintosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory', 'ImpuestosFactory', 'OficinasFactory', 'TercerosFactory', 'PrecintosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ValorCatalogosFactory, ImpuestosFactory, OficinasFactory, TercerosFactory, PrecintosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Asignación Precintos' }, { Nombre: 'Gestionar' }];
        $scope.ModeloCliente = [];
        $scope.ListaResponsable = [];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PRECINTOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.Codigo = 0;
        $scope.ModeloCodigo = 0;
        $scope.ListadoTipoPrecinto = [];

        $scope.ListaPuc = [];
        $scope.ModeloTipoPrecinto = [];
        $scope.ModalImpuesto = [];
        $scope.ModeloCuenta = [];

        $scope.ListadoCiudades = [];
        $scope.ListadoOficinas = [];
        $scope.inicial = {
            Nombre: '(Seleccione Oficina)', Codigo: 0
        };
        $scope.ListadoOficinas.push($scope.inicial);

        if ($routeParams.Numero !== undefined) {
            $scope.Numero = $routeParams.Numero;
        } else {
            $scope.Numero = 0;
        }

        $scope.ModeloFecha = new Date();
        //-----Asignaciones ---------------------

        $scope.AsignarCodigoAlterno = function (CodigoAlterno) {
            $scope.ModeloCodigoAlterno = CodigoAlterno;
        };

        $scope.AutocompleteTercero = function (value) {
            if (value.length > 0) {
                if (value.length % 3 === 0 || value.length === 2) {
                    /*Cargar Autocomplete de propietario*/
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_EMPLEADO, ValorAutocomplete: value, Sync: true, Estado: 1 });
                    $scope.ListaResponsable = ValidarListadoAutocomplete(Response.Datos, $scope.ListaResponsable);
                }
            }
            return $scope.ListaResponsable;
        };

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_PRECINTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoRecaudo = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoPrecinto = response.data.Datos;
                        $scope.ModeloTipoPrecinto = $scope.ListadoTipoPrecinto[0];
                    }
                    else {
                        $scope.ListadoTipoPrecinto = [];
                    }
                }
            }, function (response) {
            });

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item);
                    });

                    $scope.ModeloOficina = $scope.ListadoOficinas[0];
                    $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + CERO);
                }
            }, function (response) {
                ShowError(response.statusText);
            });



        if ($scope.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR OFICINA';
            $scope.Deshabilitar = true;

            Obtener();
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando Impuesto Código ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Impuesto Código ' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo
            };

            blockUI.delay = 1000;
            ImpuestosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        false;
                    }
                    else {
                        ShowError('No se logro consultar el impuesto ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarImpuestos';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarImpuestos';
                });
            blockUI.stop();
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');

        };

        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridosImpuesto()) {
                $scope.objEnviar = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TAPR: { Codigo: CODIGO_CATALOGO_ASIGNACION_PRECINTO },
                    Fecha_Entrega: $scope.ModeloFecha,
                    Tipo_Presinto: { Codigo: $scope.ModeloTipoPrecinto.Codigo },
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Responsable: { Codigo: $scope.ModeloResponsable.Codigo },
                    oficina: { Codigo: $scope.ModeloOficina.Codigo },
                    OficinaDestino: { Codigo: $scope.ModeloOficina.Codigo },
                    PrecintoInicial: $scope.ModeloNumeroPrecintoInicial,
                    PrecintoFinal: $scope.ModeloNumeroPrecintoFinal
                };

                PrecintosFactory.GuardarPrecintos($scope.objEnviar).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Numero > 0) {
                                ShowSuccess('Se Asignaron ' + response.data.Datos.NumeroTranslados + ' precintos a la oficina ' + $scope.ModeloOficina.Nombre);
                                location.href = '#!ConsultarPrecintos/' + response.data.Datos.Numero;
                            }
                            else {
                                ShowError('Uno de los precintos Ingresados en ese rango ya se encuentra creado');
                            }
                        } else {
                            ShowError('Uno de los precintos Ingresados en ese rango ya se encuentra creado');
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        function DatosRequeridosImpuesto() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;
            var diferencia = 0;
            diferencia = $scope.ModeloNumeroPrecintoFinal - $scope.ModeloNumeroPrecintoInicial;
            if ($scope.ModeloOficina.Codigo === -1 || $scope.ModeloOficina.Codigo === undefined || $scope.ModeloOficina.Codigo === null || $scope.ModeloOficina.Codigo === "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar una oficina');
                continuar = false;
            }
            if ($scope.ModeloResponsable.Codigo === -1 || $scope.ModeloResponsable.Codigo === undefined || $scope.ModeloResponsable.Codigo === null || $scope.ModeloResponsable.Codigo === "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar una Responsable');
                continuar = false;
            }
            if ($scope.ModeloTipoPrecinto.Codigo === -1 || $scope.ModeloTipoPrecinto.Codigo === undefined || $scope.ModeloTipoPrecinto.Codigo === null || $scope.ModeloTipoPrecinto.Codigo === "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar un Tipo de Precinto');
                continuar = false;
            }
            if ($scope.ModeloNumeroPrecintoInicial === 0 || $scope.ModeloNumeroPrecintoInicial === undefined || $scope.ModeloNumeroPrecintoInicial === null || $scope.ModeloNumeroPrecintoInicial === "") {
                $scope.MensajesError.push('El valor del precinto inicial mayor debe ser mayor a cero ');
                continuar = false;
            }
            if ($scope.ModeloNumeroPrecintoFinal === 0 || $scope.ModeloNumeroPrecintoFinal === undefined || $scope.ModeloNumeroPrecintoFinal === null || $scope.ModeloNumeroPrecintoFinal === "") {
                $scope.MensajesError.push('El valor del precinto final mayor debe ser mayor a cero ');
                continuar = false;
            }
            if ($scope.ModeloNumeroPrecintoInicial > $scope.ModeloNumeroPrecintoFinal) {
                $scope.MensajesError.push('El precinto incial no puede ser menor al final');
                continuar = false;
            }
            if (diferencia > 1000) {
                $scope.MensajesError.push('El rango de los precintos que se van a generar no puede ser superior a 1000');
                continuar = false;
            }
            return continuar;
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope);
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($routeParams.Numero > 0) {
                document.location.href = '#!ConsultarPrecintos/' + $routeParams.Numero;
            } else {
                document.location.href = '#!ConsultarPrecintos';
            }
        };
    }]);