﻿SofttoolsApp.controller("GestionarCombinacionesCvtCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'TercerosFactory', 'ProductoTransportadosFactory',
    'ValorCatalogosFactory', 'CombinacionContratoVinculacionFactory',
    function ($scope, $timeout, $linq, blockUI, blockUIConfig, $routeParams, TercerosFactory, ProductoTransportadosFactory,
        ValorCatalogosFactory, CombinacionContratoVinculacionFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'GESTIONAR COMBINACIONES CVT';
        $scope.MapaSitio = $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Combinaciones Cvt' }, { Nombre: 'Gestionar' }];
        $scope.Master = "#!ConsultarCombinacionesCvt";
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_COMBINACIONES_CVT);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.Modelo = {
            Codigo: 0,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        };
        $scope.ListadoCliente = [];
        $scope.ListadoRemitente = [];
        $scope.ListadoTenedor = [];
        $scope.ListadoProducto = [];
        $scope.ListadoEstados = [
            { Nombre: 'ACTIVO', Codigo: ESTADO_ACTIVO },
            { Nombre: 'INACTIVO', Codigo: ESTADO_INACTIVO }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + ESTADO_ACTIVO);
        $scope.ListadoTipoCobro = [];
        //--------------------------Variables-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //--Limpiar AutoCompletes
        $scope.limpiarControl = function (Control) {
            switch (Control) {
                case "Cliente":
                    $scope.Modelo.Cliente = "";
                    break;
                case "Remitente":
                    $scope.Modelo.Remitente = "";
                    break;
                case "Tenedor":
                    $scope.Modelo.Tenedor = "";
                    break;
                case "Producto":
                    $scope.Modelo.Producto = "";
                    break;
            }
        };
        //--Limpiar Autocompletes
        //----------AutoComplete
        //--Clientes
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    $scope.ListadoCliente = [];
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //--Clientes
        //--Remitente
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    $scope.ListadoRemitente = [];
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_REMITENTE,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoRemitente);
                }
            }
            return $scope.ListadoRemitente;
        };
        //--Remitente
        //--Tenedor
        $scope.AutocompleteTenedor = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    $scope.ListadoTenedor = [];
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_TENEDOR,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoTenedor = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTenedor);
                }
            }
            return $scope.ListadoTenedor;
        };
        //--Tenedor
        //--Producto
        $scope.AutocompleteProducto = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    $scope.ListadoProducto = [];
                    blockUIConfig.autoBlock = false;
                    var Response = ProductoTransportadosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        AplicaTarifario: -1,
                        Sync: true
                    });
                    $scope.ListadoProducto = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProducto);
                }
            }
            return $scope.ListadoProducto;
        };
        //--Producto
        //----------AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //--Tipo Cobro
            $scope.ListadoTipoCobro = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGOS_TIPO_COBRO.CODIGO },
                Sync: true
            }).Datos;
            $scope.Modelo.TipoCobro = $scope.ListadoTipoCobro[0];

            //----Obtener parametros
            if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== '0') {
                $scope.Modelo.Codigo = $routeParams.Codigo;
                Obtener();
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //-----Obtener
        function Obtener() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo
            };
            CombinacionContratoVinculacionFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = angular.copy(response.data.Datos);
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Estado);
                        if ($scope.Modelo.TipoCobro != undefined) {
                            $scope.Modelo.TipoCobro = $linq.Enumerable().From($scope.ListadoTipoCobro).First('$.Codigo ==' + $scope.Modelo.TipoCobro.Codigo);
                        }
                        $scope.MaskValores();
                    }
                    else {
                        ShowError('No se logro consultar el tarifario ventas código ' + $scope.Modelo.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = $scope.Master;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = $scope.Master;
                });

        }
        //-----Obtener
        //-----Guardar
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var nreg = 0;
            var modelo = $scope.Modelo;
            if (modelo.Cliente !== undefined && modelo.Cliente !== null && modelo.Cliente !== '') {
                nreg += 1;
            }
            if (modelo.Remitente !== undefined && modelo.Remitente !== null && modelo.Remitente !== '') {
                nreg += 1;
            }
            if (modelo.Tenedor !== undefined && modelo.Tenedor !== null && modelo.Tenedor !== '') {
                nreg += 1;
            }
            if (modelo.Producto !== undefined && modelo.Producto !== null && modelo.Producto !== '') {
                nreg += 1;
            }
            if (modelo.TipoCobro !== undefined && modelo.TipoCobro !== null && modelo.TipoCobro !== '') {
                nreg += 1;
            }
            if (modelo.Valor !== undefined && modelo.Valor !== null && modelo.Valor !== '') {
                nreg += 1;
                if (modelo.TipoCobro.Codigo == 23402 && RevertirMV(modelo.Valor) > 100) {
                    $scope.MensajesError.push("el valor no puede exceder el 100 cuando el tipo de cobro es porcentaje");
                    continuar = false;
                }
            }
            if (nreg <= 1) {
                $scope.MensajesError.push("Debe ingresar información");
                continuar = false;
            }
            return continuar;
        }
        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }

        };
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            var combinacion = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
                Cliente: $scope.Modelo.Cliente,
                Remitente: $scope.Modelo.Remitente,
                Tenedor: $scope.Modelo.Tenedor,
                Producto: $scope.Modelo.Producto,
                TipoCobro: $scope.Modelo.TipoCobro,
                Valor: $scope.Modelo.Valor,
                Estado: $scope.Modelo.Estado.Codigo,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            };
            CombinacionContratoVinculacionFactory.Guardar(combinacion).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó el registro con el código ' + response.data.Datos);
                            }
                            else {
                                ShowSuccess('Se modificó el registro con el código ' + response.data.Datos);
                            }
                            location.href = $scope.Master + "/" + response.data.Datos;
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        //-----Guardar
        //-----Volver Master
        $scope.VolverMaster = function () {
            if ($scope.Modelo.Codigo !== undefined)
                document.location.href = $scope.Master + "/" + $scope.Modelo.Codigo;
            else
                document.location.href = $scope.Master;
        };
        //-----Volver Master
        //----------------------------Funciones Generales---------------------------------//
        //----Mascaras
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        //----Mascaras
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        };
    }]);