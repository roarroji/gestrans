﻿SofttoolsApp.controller("GestionarPeajesRutasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'PeajesFactory', 'TercerosFactory', 'blockUIConfig',
    'ValorCatalogosFactory', 'RutasFactory','PeajesRutasFactory' ,
    function ($scope, $routeParams, $timeout, $linq, blockUI, PeajesFactory, TercerosFactory, blockUIConfig,
        ValorCatalogosFactory, RutasFactory, PeajesRutasFactory) {

        $scope.Titulo = 'GESTIONAR PEAJE RUTA';
        $scope.Master = "#!ConsultarPeajesRutas";
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Peajes Rutas' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PEAJES);

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PEAJES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.Deshabilitar = true;
        $scope.ListadoEstados = [];

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            Nombre: '',
            Codigo_Alterno: '',
            Estado: 0,
            TipoPeaje: {}
        };


        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ]
        $scope.Modelo.Estado = $scope.ListadoEstados[0];


        //-------------------------------------- CATALOGOS --------------------------------------//
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_PEAJE } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoPeaje = [];
                    $scope.ListadoTipoPeaje.push({ Codigo: -1, Nombre: 'Seleccione' });
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoTipoPeaje.push(response.data.Datos[i]);
                        }
                        $scope.Modelo.TipoPeaje = $linq.Enumerable().From($scope.ListadoTipoPeaje).First('$.Codigo == -1');
                    }
                    else {
                        $scope.ListadoTipoPeaje = [];
                    }
                }
            }, function (response) {
            });
        //-------------------------------------- CATALOGOS --------------------------------------//

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

     
        function DatosRequeridos() {
            var continuar = true;
            $scope.MensajesError = [];
            window.scrollTo(top, top);
            if ($scope.Modelo.Ruta == undefined || $scope.Modelo.Ruta == "") {
                closeModal('modalConfirmacionGuardar');
                $scope.MensajesError.push('Debe  Seleccionar Una Ruta');
                continuar = false;
           
            }
            if ($scope.Modelo.Peaje == undefined || $scope.Modelo.Peaje == "") {
                closeModal('modalConfirmacionGuardar');
                $scope.MensajesError.push('Debe Seleccionar Un Peaje');
                continuar = false;
              
            }
            if ($scope.Modelo.DuracionHoras == undefined || $scope.Modelo.DuracionHoras == "") {
                closeModal('modalConfirmacionGuardar');
                $scope.MensajesError.push('Debe Ingresar La Cantidad de Horas');
                continuar = false;
             
            }

            return continuar;

        }

        //-------------------------------------- PARAMETROS --------------------------------------//
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
        }
        $scope.AutocompleteRuta = function (value) {
            var ListadoRutas = [];
            var ResponseRuta = RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Nombre: value, Sync: true }).Datos;

            if (ResponseRuta != undefined) {
                ListadoRutas = ResponseRuta;
            }
            return ListadoRutas;
        }

        $scope.AutocompletePeajes = function (value) {
            var ListadoPeajes = [];
            var ResponsePeajes = PeajesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Nombre: value, Sync: true }).Datos;

            if (ResponsePeajes != undefined) {
                ListadoPeajes = ResponsePeajes;

            }

            return ListadoPeajes;
        }
        //-------------------------------------- PARAMETROS --------------------------------------//
        $scope.VolverMaster = function () {
            document.location.href = $scope.Master;
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MasckDecimal = function (option) {
            return MascaraDecimales(option)
        }

        //-------------------------------------------------GASTOS RUTA--------------------------------------------------------//

        //-------------------------------------------------PEAJES RUTA--------------------------------------------------------//
          $scope.AgregarPeaje = function () {
            $scope.Peaje = $scope.Modelo.Peaje;
              $scope.Ruta = $scope.Modelo.Ruta;
              closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
               
                    var GuardarPeajes = ({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.Ruta.Codigo,
                        Peaje: $scope.Peaje,
                        Orden: MascaraNumero($scope.Modelo.CodigoAlterno),
                        DuracionHoras: MascaraNumero($scope.Modelo.DuracionHoras)
                    })

                PeajesRutasFactory.Guardar(GuardarPeajes).
                        then(function (response) {
                            if (response.data.ProcesoExitoso == true) {
                                ShowSuccess('Se guardó el peaje dentro de la ruta  ' + $scope.Ruta.Nombre);
                                location.href = '#!ConsultarPeajesRutas/' + $scope.Modelo.Ruta.Codigo + '/' + $scope.Modelo.Peaje.Codigo;
                            } else {
                                ShowError('falló al guardar')
                            }
                        }, function (response) {

                            ShowError(response.statusText);
                        }

                        );       

           
            }

        }
    }]);