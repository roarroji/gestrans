﻿SofttoolsApp.controller("GestionarSemirremolquesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'SemirremolquesFactory', 'ValorCatalogosFactory', 'MarcaSemirremolquesFactory', 'DocumentosFactory', 'GestionDocumentosFactory', 'blockUIConfig','TercerosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, SemirremolquesFactory, ValorCatalogosFactory, MarcaSemirremolquesFactory, DocumentosFactory, GestionDocumentosFactory, blockUIConfig, TercerosFactory) {
      
        $scope.Titulo = 'GESTIONAR SEMIRREMOLQUES';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Semirremolques' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SEMIRREMOLQUES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoCiudadesDestino = []
        $scope.ListadoCiudadesOrigen = []
        $scope.ListadoEstados = []
        $scope.ListadoPropietarios =[]
        $scope.MensajesError = [];

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: '',
            Nombre: '',
            Estado: 1
        }
        $scope.ItemFoto = {}
        $scope.GPS = {};

        $scope.ListadoNovedades = [];
        $scope.ListadoCompletoNovedades = [];
        $scope.ListaNovedadesSemirremolque = [];
        $scope.ModalNovedades = {};
        $scope.NumeroPaginaNovedades = 1;
        $scope.CantidadRegistrosPorPaginaNovedades = 15;
        $scope.ListadoFiltradoNovedadesSemirremolque = [];
        $scope.totalPaginasNovedades = 0;
        
        $scope.DeshabilitarEstado = false;
        

        var ResponseNovedadesSemirremolque = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NOVEDADES_SEMIRREMOLQUES }, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
        if (ResponseNovedadesSemirremolque != undefined) {
            $scope.ListadoCompletoNovedades = ResponseNovedadesSemirremolque;
        }

        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
        }
        else {
            $scope.Modelo.Codigo = 0;
        }
        $scope.CargarDatosFunciones = function () {
            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

            /*Cargar el combo de tipo semirremolque*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SEMIRREMOLQUES } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoSemirremolques = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoSemirremolques = response.data.Datos;
                            try {
                                $scope.Modelo.TipoSemirremolque = $linq.Enumerable().From($scope.ListadoTipoSemirremolques).First('$.Codigo ==' + $scope.Modelo.TipoSemirremolque.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoSemirremolque = $scope.ListadoTipoSemirremolques[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoSemirremolques = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de tipo carroceria*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CARROCERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoCarroceria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoCarroceria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoCarroceria = $linq.Enumerable().From($scope.ListadoTipoCarroceria).First('$.Codigo ==' + $scope.Modelo.TipoCarroceria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoCarroceria = $scope.ListadoTipoCarroceria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoCarroceria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo Equipo*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_EQUIPO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoEquipo = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoEquipo = response.data.Datos;
                            try {
                                $scope.Modelo.TipoEquipo = $linq.Enumerable().From($scope.ListadoTipoEquipo).First('$.Codigo ==' + $scope.Modelo.TipoEquipo.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoEquipo = $scope.ListadoTipoEquipo[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoEquipo = []
                        }
                    }
                }, function (response) {
                });

            $scope.AsignarJustificacionBloqueo = function (cod) {
                if (cod == 0) {//Bloqueado
                    $('#JustificacionBloqueo').show()
                    $('#CausaInactividad').hide()
                    //$scope.Modelo.CausaInactividad = $scope.ListadoCausa[0]
                }
                //   else if (cod == 3302) {//inactivo
                //    $('#CausaInactividad').show()
                //    $('#JustificacionBloqueo').hide()
                //    $scope.Modelo.JustificacionBloqueo = ''
                //} 
                else {
                    $('#JustificacionBloqueo').hide()
                    $('#CausaInactividad').hide()
                    //$scope.Modelo.CausaInactividad = $scope.ListadoCausa[0]
                    $scope.Modelo.JustificacionBloqueo = ''
                }

            }
            $scope.AsignarJustificacionBloqueo($scope.Modelo.Estado.Codigo)
            $scope.ListadoEstados = [
                { Codigo: 1, Nombre: "ACTIVO" },
                { Codigo: 0, Nombre: "INACTIVO" },
            ]
            try {
                $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Estado.Codigo);
            } catch (e) {
                $scope.Modelo.Estado = $scope.ListadoEstados[0]
            }
        

            $scope.AutocompletePropietario = function (value) {
                if (value.length > 0) {
                    if ((value.length % 3) == 0 || value.length == 2) {
                        /*Cargar Autocomplete de propietario*/
                        blockUIConfig.autoBlock = false;
                        var Response = TercerosFactory.Consultar({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            CadenaPerfiles: PERFIL_PROPIETARIO,
                            ValorAutocomplete: value,
                            Sync: true
                        })
                        $scope.ListadoPropietarios = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoPropietarios)
                    }
                }
                return $scope.ListadoPropietarios
            }
            /*Cargar Autocomplete de marca*/
            MarcaSemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoMarcas = []
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoMarcas = response.data.Datos;
                            try {
                                if ($scope.Modelo.Marca.Codigo > 0) {
                                    $scope.Modelo.Marca = $linq.Enumerable().From($scope.ListadoMarcas).First('$.Codigo ==' + $scope.Modelo.Marca.Codigo);
                                }
                            } catch (e) {
                                $scope.Modelo.Marca = ''
                            }
                        }
                        else {
                            $scope.ListadoMarcas = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            $("#linea").prop('disabled', true);
            //Elimina todos los archivos temporales asociados a este usuario
            DocumentosFactory.EliminarDocumento({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo } }).
                then(function (response) {
                }, function (response) {
                    ShowError(response.statusText);
                });
            //Carga los documentos
            GestionDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, EntidadDocumento: { Codigo: 3 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDocumentos = []
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                var item = response.data.Datos[i]
                                if (item.Habilitado == 1) {
                                    if (item.TipoArchivoDocumento.Codigo == 4301) {
                                        $scope.ItemFoto = item
                                    }
                                    else {
                                        $scope.ListadoDocumentos.push(item)
                                    }
                                }

                            }
                            try {
                                var esImagen = false
                                if ($scope.Modelo.Documentos.length > 0) {
                                    for (var k = 0; k < $scope.Modelo.Documentos.length; k++) {
                                        var doc2 = $scope.Modelo.Documentos[k]
                                        for (var j = 0; j < $scope.ListadoDocumentos.length; j++) {

                                            if ($scope.ListadoDocumentos[j].Codigo == doc2.Codigo) {
                                                $scope.ListadoDocumentos[j].Referencia = doc2.Referencia
                                                $scope.ListadoDocumentos[j].Emisor = doc2.Emisor
                                                try {
                                                    if (new Date(doc2.FechaEmision) < MIN_DATE) {
                                                        $scope.ListadoDocumentos[j].FechaEmision = ''
                                                    } else {
                                                        $scope.ListadoDocumentos[j].FechaEmision = new Date(doc2.FechaEmision)
                                                    }

                                                } catch (e) {
                                                    $scope.ListadoDocumentos[j].FechaEmision = ''
                                                }
                                                try {
                                                    if (new Date(doc2.FechaVence) < MIN_DATE) {
                                                        $scope.ListadoDocumentos[j].FechaVence = ''
                                                    } else {
                                                        $scope.ListadoDocumentos[j].FechaVence = new Date(doc2.FechaVence)
                                                    }
                                                } catch (e) {
                                                    $scope.ListadoDocumentos[j].FechaVence = ''
                                                }
                                                $scope.ListadoDocumentos[j].ValorDocumento = doc2.ValorDocumento

                                            } else {
                                                if (!esImagen) {
                                                    if (doc2.Codigo == $scope.ItemFoto.Codigo) {
                                                        esImagen = true
                                                        doc2.Semirremolques = true
                                                        doc2.CodigoSemirremolques = $scope.Modelo.Codigo
                                                        $scope.ItemFoto.ValorDocumento = doc2.ValorDocumento
                                                        DocumentosFactory.Obtener(doc2).
                                                            then(function (response) {
                                                                if (response.data.ProcesoExitoso === true) {
                                                                    if (response.data.Datos) {
                                                                        doc2.Archivo = response.data.Datos.Archivo
                                                                        doc2.Extencion = response.data.Datos.Extencion
                                                                        doc2.Tipo = response.data.Datos.Tipo
                                                                        $scope.FotoCargada = 'data:' + doc2.Tipo + ';base64,' + doc2.Archivo
                                                                        $('#Foto').hide()
                                                                        $('#FotoCargada').show()
                                                                    }
                                                                } else {
                                                                    ShowError(response.data.MensajeError);
                                                                }
                                                            }, function (response) {
                                                                ShowError(response.statusText);
                                                            });

                                                    }
                                                }

                                            }
                                        }
                                        if (!esImagen) {
                                            if (doc2.Codigo == $scope.ItemFoto.Codigo) {
                                                esImagen = true
                                                doc2.Semirremolques = true
                                                doc2.CodigoSemirremolques = $scope.Modelo.Codigo
                                                $scope.ItemFoto.ValorDocumento = doc2.ValorDocumento
                                                DocumentosFactory.Obtener(doc2).
                                                    then(function (response) {
                                                        if (response.data.ProcesoExitoso === true) {
                                                            if (response.data.Datos) {
                                                                doc2.Archivo = response.data.Datos.Archivo
                                                                doc2.Extencion = response.data.Datos.Extencion
                                                                doc2.Tipo = response.data.Datos.Tipo
                                                                $scope.FotoCargada = 'data:' + doc2.Tipo + ';base64,' + doc2.Archivo
                                                                $('#Foto').hide()
                                                                $('#FotoCargada').show()
                                                            }
                                                        } else {
                                                            ShowError(response.data.MensajeError);
                                                        }
                                                    }, function (response) {
                                                        ShowError(response.statusText);
                                                    });

                                            }
                                        }
                                    }
                                    $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListadoSemirremolques).First('$.Codigo ==' + $scope.Modelo.Semirremolque.Codigo);
                                }
                            } catch (e) {
                            }
                        }
                        else {
                            $scope.Documentos = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        }
        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR SEMIRREMOLQUE';
            $scope.Deshabilitar = true;
            ObtenerSemirremolque();
        } else {
            $scope.CargarDatosFunciones()
        }
        function ObtenerSemirremolque() {
            blockUI.start('Cargando vehículo Código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando vehículo Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            SemirremolquesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Modelo = JSON.parse(JSON.stringify(response.data.Datos))
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.Propietario = $scope.CargarTercero($scope.Modelo.Propietario.Codigo)
                        $scope.DesactivarPlaca = true
                        if (response.data.Datos.EquipoPropio == 1) {
                            $scope.Modelo.EquipoPropio = true
                        } else {
                            $scope.Modelo.EquipoPropio = false
                        }
                        if (response.data.Datos.LevantaEjes == 1) {
                            $scope.Modelo.LevantaEjes = true
                        } else {
                            $scope.Modelo.LevantaEjes = false
                        }
                        $scope.CargarDatosFunciones()

                        $scope.ListadoEstados = [
                            { Codigo: 1, Nombre: "ACTIVO" },
                            { Codigo: 0, Nombre: "INACTIVO" },
                        ];

                        $scope.ListaNovedadesSemirremolque = response.data.Datos.ListaNovedades;
                        $scope.ListaNovedadesSemirremolque.forEach(item => {
                            item.Hora = new Date(item.Fecha).getHours();
                            item.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + item.Estado);
                        });
                        Paginar();
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + $scope.Modelo.Estado.Codigo);

                        blockUI.delay = 1000;
                    }
                    else {
                        ShowError('No se logro consultar el semirremolque No.' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarSemirremolques';
                    }
                }, function (response) {
                    ShowError('No se logro consultar el semirremolque No.' + $scope.Modelo.Codigo + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!ConsultarSemirremolques';
                });

            blockUI.stop();
        };
        $scope.VerificarExistencia = function () {
            if ($scope.Modelo.Placa.length > 5) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Placa: $scope.Modelo.Placa,
                };
                blockUI.delay = 1000;
                SemirremolquesFactory.Obtener(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                ShowError('La placa ingresada ya se encuentra registrada con otro semirremolque')
                                $scope.Modelo.Placa = ''
                            }
                        }
                    }, function (response) {

                    });

                blockUI.stop();
            } else {
                ShowError('Por favor ingrese una placa valida')
            }
        }
        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarSemirremolques';
        };
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };
        function DatosRequeridosSemirremolques() {

            $scope.MensajesError = [];
            window.scrollTo(top, top);
            var modelo = $scope.Modelo
            var continuar = true;
            if (ValidarCampo(modelo.Placa) == 1) {
                $scope.MensajesError.push('Debe ingresar la placa');
                continuar = false;
            }
            if (ValidarCampo(modelo.Propietario) == 1) {
                $scope.MensajesError.push('Debe ingresar un propietario');
                continuar = false;
            } else if (ValidarCampo(modelo.Propietario, undefined, true) == 2) {
                $scope.MensajesError.push('Debe ingresar un propietario valido');
                continuar = false;
            }
            if (ValidarCampo(modelo.Marca) == 1) {
                $scope.MensajesError.push('Debe ingresar una marca');
                continuar = false;
            } else if (ValidarCampo(modelo.Marca, undefined, true) == 2) {
                $scope.MensajesError.push('Debe ingresar una marca valida');
                continuar = false;
            }
            if (modelo.TipoSemirremolque.Codigo == 3400)  /*No aplica*/ {
                $scope.MensajesError.push('Debe seleccionar el tipo de semirremolque');
                continuar = false;
            }
            if (modelo.TipoCarroceria.Codigo == 2300)  /*No aplica*/ {
                $scope.MensajesError.push('Debe seleccionar el tipo de carrocería');
                continuar = false;
            }
           
            if (ValidarCampo(modelo.Modelo) == 1) {
                $scope.MensajesError.push('Debe ingresar el modelo');
                continuar = false;
            }

            if (ValidarCampo(modelo.NumeroEjes) == 1) {
                $scope.MensajesError.push('Debe ingresar el número de ejes');
                continuar = false;
            }
            if (ValidarCampo(modelo.Ancho) == 1) {
                $scope.MensajesError.push('Debe ingresar el ancho del semirremolque');
                continuar = false;
            }
            if (ValidarCampo(modelo.Largo) == 1) {
                $scope.MensajesError.push('Debe ingresar el largo del semirremolque');
                continuar = false;
            }
            if (ValidarCampo(modelo.PesoVacio) == 1) {
                $scope.MensajesError.push('Debe ingresar el peso vacio');
                continuar = false;
            }
            if (ValidarCampo(modelo.Capacidad) == 1) {
                $scope.MensajesError.push('Debe ingresar la capacidad');
                continuar = false;
            }
            if (modelo.Estado.Codigo == 0)  /*Bloqueado*/ {
                if (ValidarCampo(modelo.JustificacionBloqueo) == 1) {
                    $scope.MensajesError.push('Debe ingresar justificación del bloqueo');
                    continuar = false;
                }
            }
            $scope.Modelo.Documentos = []
            //Documentos
            for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                var Cont = 0
                var documento = $scope.ListadoDocumentos[i]
                if (documento.TipoArchivoDocumento.Codigo !== 4301) {

                    if (ValidarCampo(documento.Referencia) == 1) {
                        if (documento.AplicaReferencia == CAMPO_DOCUMENTO_OBLIGATORIO) {
                            $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        }
                    }
                    if (ValidarCampo(documento.Emisor) == 1) {
                        if (documento.AplicaEmisor == CAMPO_DOCUMENTO_OBLIGATORIO) {
                            $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        }
                    }
                    if (ValidarFecha(documento.FechaEmision) == 1) {
                        if (documento.AplicaFechaEmision == CAMPO_DOCUMENTO_OBLIGATORIO) {
                            $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        } else {
                            documento.FechaEmision = ''
                        }
                    } else {
                        var f = new Date();
                        if (documento.FechaEmision > f) {
                            $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        }
                    }
                    if (ValidarFecha(documento.FechaVence) == 1) {
                        if (documento.AplicaFechaVencimiento == CAMPO_DOCUMENTO_OBLIGATORIO) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        } else {
                            documento.FechaVence = ''
                        }
                    } else {
                        var f = new Date();
                        if (documento.FechaVence < f) {
                            $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        }
                    }
                    $scope.Modelo.Documentos.push(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Terceros: true,
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            Configuracion: { Codigo: documento.Codigo },
                            Referencia: documento.Referencia,
                            Emisor: documento.Emisor,
                            FechaEmision: documento.FechaEmision,
                            FechaVence: documento.FechaVence,
                            EliminaDocumento: documento.ValorDocumento
                        }
                    )
                }
            }
            $scope.Modelo.Documentos.push(
                {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Terceros: true,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Configuracion: { Codigo: $scope.ItemFoto.Codigo },
                    Referencia: $scope.ItemFoto.Referencia,
                    Emisor: $scope.ItemFoto.Emisor,
                    FechaEmision: $scope.ItemFoto.FechaEmision,
                    FechaVence: $scope.ItemFoto.FechaVence,
                    EliminaDocumento: $scope.ItemFoto.ValorDocumento
                }
            )

            return continuar


        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridosSemirremolques()) {
                if ($scope.Modelo.EquipoPropio) {
                    $scope.Modelo.EquipoPropio = 1
                }
                else {
                    $scope.Modelo.EquipoPropio = 0
                }
                if ($scope.Modelo.LevantaEjes) {
                    $scope.Modelo.LevantaEjes = 1
                }
                else {
                    $scope.Modelo.LevantaEjes = 0
                }
                $scope.Modelo.Propietario = { Codigo: $scope.Modelo.Propietario.Codigo }
                SemirremolquesFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                ShowSuccess('Se guardó el semirremolque de placas ' + $scope.Modelo.Placa);
                                location.href = '#!ConsultarSemirremolques/' + response.data.Datos
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        $scope.CargarSemirremolqueEstudio = function () {
            BloqueoPantalla.start('Cargando semirremolque ...');
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroAutorizacion: $scope.NumeroEstudioSeguridad
            };
            if ($scope.DatosRequeridosEstudio()) {
                SemirremolquesFactory.ObtenerSemirremolqueEstudioSeguridad(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado === 0) {
                                if ($scope.Modelo.Placa == response.data.Datos.PlacaSemirremolque) {

                                    $scope.Modelo.Modelo = response.data.Datos.ModeloSemirremolque;
                                    $scope.Modelo.NumeroEjes = response.data.Datos.NumeroEjes;

                                    var CodigoProp = response.data.Datos.CodigoPropietarioRemolque;
                                    if (CodigoProp > 0 && CodigoProp !== undefined) {
                                        $scope.Modelo.Propietario = $scope.CargarTercero(CodigoProp);
                                    }

                                    var CodigoMarc = response.data.Datos.MarcaSemirremolque.Codigo;
                                    if (CodigoMarc > 0 && CodigoMarc !== undefined) {
                                        $scope.Modelo.Marca = $linq.Enumerable().From($scope.ListadoMarcas).First('$.Codigo ==' + CodigoMarc);
                                    }

                                    var CodigoTipoSemi = response.data.Datos.TipoSemirremolque.Codigo;
                                    if (CodigoTipoSemi > 0 && CodigoTipoSemi !== undefined) {
                                        $scope.Modelo.TipoSemirremolque = $linq.Enumerable().From($scope.ListadoTipoSemirremolques).First('$.Codigo ==' + CodigoTipoSemi);
                                    }

                                    var CodigoTipoCarro = response.data.Datos.TipoCarroceriaSemirremolque.Codigo;
                                    if (CodigoTipoCarro > 0 && CodigoTipoCarro !== undefined) {
                                        $scope.Modelo.TipoCarroceria = $linq.Enumerable().From($scope.ListadoTipoCarroceria).First('$.Codigo ==' + CodigoTipoCarro);
                                    }                               
                                    ShowSuccess('Información cargada correctamente, por favor diligenciar los campos obligatorios');
                                } else {
                                    ShowError('La placa ' + $scope.Modelo.Placa + ' no corresponde al No. autorización ingresado');
                                }
                            } else {
                                ShowError('El estudio de seguridad autorizado se encuentra anulado');
                            }
                            BloqueoPantalla.stop();
                        } else {
                            ShowError(response.data.MensajeOperacion); +
                                BloqueoPantalla.stop();
                        }
                    }, function (response) {
                        ShowError(response.data.MensajeOperacion);
                        BloqueoPantalla.stop();
                    });
            }
            BloqueoPantalla.stop();
        };

        $scope.DatosRequeridosEstudio = function () {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Modelo.Placa === undefined || $scope.Modelo.Placa === '' || $scope.Modelo.Placa === null) {
                $scope.MensajesError.push('Debe ingresar la placa autorizada en el estudio de seguiridad');
                continuar = false;
            }
            if ($scope.NumeroEstudioSeguridad == undefined || $scope.NumeroEstudioSeguridad == '' || $scope.NumeroEstudioSeguridad == null) {
                $scope.MensajesError.push('Debe ingresar el número de autorización del estudio de seguiridad');
                continuar = false;
            }
            return continuar
        };

        $scope.VerificarAutocomplete = function (Object) {
            if (Object == '' || Object == undefined || Object == null) {
                return Object = ''
            } else if (Object.Codigo == '' || Object.Codigo == undefined || Object.Codigo == null || Object.Codigo == 0 || Object.Codigo == '0') {
                return Object = ''
            } else {
                return Object = Object
            }
        }
        $scope.CargarImagen = function (element) {
            var continuar = true;
            $scope.TipoImagen = element;
            blockUI.start();

            if (element === undefined) {
                ShowError("Warning", "Solo se pueden cargar archivos en formato jpeg o png", false);
                continuar = false;
            }

            if (continuar == true) {
                if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png') {
                    ShowError("Warning", "Solo se pueden cargar archivos en formato jpeg o png", false);
                    continuar = false;
                }
                else if (element.files[0].size >= 2000000) //2 MB
                {
                    ShowError("Warning", "La imagen " + element.files[0].name + " supera los 2 MB, intente con otra imagen", false);
                    continuar = false;
                }
            }

            if (continuar == true) {
                var reader = new FileReader();
                reader.onload = $scope.AsignarImagen;
                reader.readAsDataURL(element.files[0]);
            }

            blockUI.stop();
        }
        $scope.CargarArchivo = function (element) {
            var continuar = true;
            if (element.files.length > 0) {
                $scope.PDF = element
                $scope.TipoDocumento = element
                blockUI.start();
                if (angular.element(this.item)[0] == undefined) {
                    $scope.Documento = angular.element(this.ItemFoto)[0];
                } else {
                    angular.element(this.item)[0].Temporal = true
                    $scope.Documento = angular.element(this.item)[0];
                }
                if (element === undefined) {
                    ShowError("Archivo invalido, poar favor verifique el formato del archivo  /nFormatos permitidos:" + $scope.Documento.TipoArchivoDocumento.CampoAuxiliar2);
                    continuar = false;
                }
                if (continuar == true) {
                    if ($scope.Documento.TipoArchivoDocumento.Codigo !== 4300) {
                        var Formatos = $scope.Documento.TipoArchivoDocumento.CampoAuxiliar2.split(',')
                        var cont = 0
                        for (var i = 0; i < Formatos.length; i++) {
                            Formatos[i] = Formatos[i].replace(' ', '')
                            var Extencion = element.files[0].name.split('.')
                            //if (element.files[0].type == Formatos[i]) {
                            if ('.' + (Extencion[1].toUpperCase()) == Formatos[i].toUpperCase()) {
                                cont++
                            }
                        }
                        if (cont == 0) {
                            ShowError("Archivo invalido, poar favor verifique el formato del archivo  Formatos permitidos:" + $scope.Documento.TipoArchivoDocumento.CampoAuxiliar2);
                            continuar = false;
                        }
                    }
                    else if (element.files[0].size >= $scope.Documento.Size) //8 MB
                    {
                        ShowError("El archivo " + element.files[0].name + " supera los " + $scope.Documento.NombreTamano + ", intente con otro archivo", false);
                        continuar = false;
                    }
                }
                if (continuar == true) {
                    if ($scope.Documento.Archivo != undefined && $scope.Documento.Archivo != '' && $scope.Documento.Archivo != null) {
                        $scope.Documento.ArchivoTemporal = element.files[0]
                        showModal('modalConfirmacionRemplazarDocumento');
                    } else {
                        var reader = new FileReader();
                        $scope.Documento.ArchivoCargado = element.files[0]
                        reader.onload = $scope.AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }

                blockUI.stop();
            }
        }
        $scope.RemplazarDocumento = function () {
            var reader = new FileReader();
            $scope.Documento.ArchivoCargado = $scope.Documento.ArchivoTemporal
            reader.onload = $scope.AsignarArchivo;
            reader.readAsDataURL($scope.Documento.ArchivoCargado);
            closeModal('modalConfirmacionRemplazarDocumento');
        }
        $scope.AsignarArchivo = function (e) {
            $scope.$apply(function () {
                //imagenes, tienen un proceso aparte ya que se deben redimencionar para que no ocupen mucho espacio en la BD
                if ($scope.Documento.ArchivoCargado.type.includes("image")) {
                    var img = new Image()
                    img.src = e.target.result
                    $timeout(function () {
                        $('#Foto').show()
                        $('#FotoCargada').hide()
                        if (img.height > img.width) {
                            RedimencionarImagen('CanvasVertical', img, 699, 499)
                            $scope.Documento.Archivo = document.getElementById('CanvasVertical').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')

                        }
                        //la imagen es horizontal
                        else {
                            RedimencionarImagen('CanvasHorizontal', img, 499, 699)
                            $scope.Documento.Archivo = document.getElementById('CanvasHorizontal').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')
                        }


                        $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type
                        var Extencion = $scope.Documento.ArchivoCargado.name.split('.')
                        $scope.Documento.NombreDocumento = Extencion[0]
                        $scope.Documento.ExtensionDocumento = Extencion[1]
                        if ($scope.Documento.TipoArchivoDocumento.Codigo == 4301) {
                            RedimencionarImagen('Foto', img, 200, 200)
                            $scope.ItemFoto.NombreDocumento = Extencion[0]
                            $scope.ItemFoto.ValorDocumento = 1
                            $scope.ItemFoto.temp = true
                        }
                        $scope.InsertarDocumento()
                    }, 100)
                }
                //Resto de archivos
                else {
                    $scope.Documento.Archivo = e.target.result.replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')
                    $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type
                    var Extencion = $scope.Documento.ArchivoCargado.name.split('.')
                    $scope.Documento.NombreDocumento = Extencion[0]
                    $scope.Documento.ExtensionDocumento = Extencion[1]
                    $scope.InsertarDocumento()
                }
            });
        }
        $scope.InsertarDocumento = function () {
            $timeout(function () {
                // Guardar Archivo temporal
                var Documento = {
                    CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                    Configuracion: { Codigo: $scope.Documento.Codigo },
                    Archivo: $scope.Documento.Archivo,
                    Nombre: $scope.Documento.NombreDocumento,
                    Extension: $scope.Documento.ExtensionDocumento,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Tipo: $scope.Documento.Tipo
                }
                DocumentosFactory.InsertarTemporal(Documento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            // Se guardó correctamente el pdf
                            if (response.data.Datos) {
                                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                                    var item = $scope.ListadoDocumentos[i]
                                    if (item.Codigo == $scope.Documento.Codigo) {
                                        item.ValorDocumento = 1
                                        item.temp = true
                                    }
                                }
                                ShowSuccess('Se cargó el documento adjunto "' + $scope.Documento.NombreDocumento + '"');
                                $scope.Documento = ''
                            }
                        } else {
                            ShowError(response.data.MensajeError);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }, 200)
        }
        $scope.ElimiarDocumento = function (EliminarDocumento) {
            $scope.EliminarDocumento = EliminarDocumento
            showModal('modalConfirmacionEliminarDocumento');
            document.getElementById('mensajearchivo').innerHTML = '¿Está seguro de eliminar el documento?'

        };
        $scope.BorrarDocumento = function () {
            closeModal('modalConfirmacionEliminarDocumento');
            if ($scope.EliminarDocumento.temp) {
                $scope.EliminarDocumento.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                DocumentosFactory.EliminarDocumento($scope.EliminarDocumento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ListadoDocumentos.forEach(function (item) {
                                if (item.Codigo == $scope.EliminarDocumento.Codigo) {
                                    item.ValorDocumento = 0
                                }
                            })
                            if ($scope.EliminarDocumento.TipoArchivoDocumento.Codigo == 4301) {
                                var img = new Image()
                                RedimencionarImagen('Foto', img, 200, 200)
                                $scope.ItemFoto.ValorDocumento = 0
                                $('#Foto').hide()
                                $scope.FotoCargada = ''
                                $('#FotoCargada').hide()
                            }
                        }
                        else {
                            MensajeError("Error", response.data.MensajeError, false);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                    if ($scope.EliminarDocumento.Codigo == $scope.ListadoDocumentos[i].Codigo) {
                        $scope.ListadoDocumentos[i].ValorDocumento = 0
                    }
                }
                if ($scope.EliminarDocumento.TipoArchivoDocumento.Codigo == 4301) {
                    var img = new Image()
                    RedimencionarImagen('Foto', img, 200, 200)
                    $scope.ItemFoto.ValorDocumento = 0
                    $('#Foto').hide()
                    $scope.FotoCargada = ''
                    $('#FotoCargada').hide()
                }

            }
        }
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (item) {
            if (item.temp) {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Semirremolques&Codigo=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&CDGD_Codigo=' + item.Codigo + '&Temp=1');
            }
            else {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Semirremolques&Codigo=' + $scope.Modelo.Codigo + '&CDGD_Codigo=' + item.Codigo + '&Temp=0');
            }


        };

        //Novedades:

        $scope.PrimerPaginaNovedades = function () {
            $scope.NumeroPaginaNovedades = 1;
            Paginar();
        }
        $scope.AnteriorNovedades = function () {
            if ($scope.NumeroPaginaNovedades > 1) {
                $scope.NumeroPaginaNovedades--;
                Paginar();
            }

        }
        $scope.SiguienteNovedades = function () {
            if ($scope.NumeroPaginaNovedades < (Math.ceil($scope.ListaNovedadesSemirremolque.length / $scope.CantidadRegistrosPorPaginaNovedades))) {
                $scope.NumeroPaginaNovedades++;
                Paginar();
            }
        }
        $scope.UltimaPaginaNovedades = function () {
            $scope.NumeroPaginaNovedades = Math.ceil($scope.ListaNovedadesSemirremolque.length / $scope.CantidadRegistrosPorPaginaNovedades);
            Paginar();
        }



        function Paginar() {
            $scope.ListadoFiltradoNovedadesSemirremolque = [];
            var ListaNovedadesOrdenado = $scope.ListaNovedadesSemirremolque.sort((a, b) => { return (new Date(b.Fecha) - new Date(a.Fecha)) })
            $scope.totalPaginasNovedades = Math.ceil(ListaNovedadesOrdenado.length / $scope.CantidadRegistrosPorPaginaNovedades);
            var RegistroInicio = ($scope.NumeroPaginaNovedades * $scope.CantidadRegistrosPorPaginaNovedades) - $scope.CantidadRegistrosPorPaginaNovedades;
            var RegistroFin = ($scope.NumeroPaginaNovedades * $scope.CantidadRegistrosPorPaginaNovedades) - 1
            if (ListaNovedadesOrdenado.length > 0) {
                for (var i = 0; i < ListaNovedadesOrdenado.length; i++) {
                    if (i >= RegistroInicio && i <= RegistroFin) {
                        $scope.ListadoFiltradoNovedadesSemirremolque.push(ListaNovedadesOrdenado[i])
                    }
                }
            }
        }

        $scope.AgregarNovedadSemirremolque = function () {
            $scope.ListadoNovedades = [];

            $scope.ListadoCompletoNovedades.forEach(itemNovedad => {               
                    $scope.ListadoNovedades.push(itemNovedad);                
            });

            $scope.ModalNovedades.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==1');
            if ($scope.ListadoNovedades.length > 0) {
                $scope.ModalNovedades.Novedad = $scope.ListadoNovedades[0];
            }
            $scope.ModalNovedades.Justificacion = '';
            showModal('modalAgregarNovedad');

        }

        $scope.GuardarNovedad = function () {
            var Novedad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
                Estado: $scope.ModalNovedades.Estado,
                Novedad: { Codigo: $scope.ModalNovedades.Novedad.Codigo },
                JustificacionNovedad: $scope.ModalNovedades.Justificacion,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            }

            SemirremolquesFactory.GuardarNovedad(Novedad).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        $scope.ListaNovedadesSemirremolque.push({
                            Fecha: new Date(),
                            Hora: new Date().getHours(),
                            Responsable: { NombreCompleto: $scope.Sesion.UsuarioAutenticado.Empleado.Nombre },
                            Estado: Novedad.Estado,
                            Novedad: $scope.ModalNovedades.Novedad,
                            JustificacionNovedad: Novedad.JustificacionNovedad

                        });

                        $scope.ListaNovedadesSemirremolque.forEach(item => {
                            item.Fecha = new Date(item.Fecha)
                        });

                        $scope.Modelo.JustificacionBloqueo = Novedad.JustificacionNovedad;
                        if (Novedad.Estado.Codigo == ESTADO_ACTIVO) {
                            $('#JustificacionBloqueo').hide();
                        } else {
                            $('#JustificacionBloqueo').show();
                        }
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + Novedad.Estado.Codigo);
                        Paginar();

                        ShowSuccess('Se guardó la novedad con éxito');

                    } else {
                        ShowError(response.statusText)
                    }
                });

            closeModal('modalAgregarNovedad');
        }

        $scope.DesplegarExcel = function (OpcionPDf, OpcionEXCEL, OpcionListado) {

            // $scope.OpcionLista = OpcionListado;

            //Depende del listado seleccionado se enviará el nombre por parametro
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NombreReporte = NOMBRE_LISTADO_EXCEL_NOVEDADES_SEMIRREMOLQUE;




            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&SEMI_Codigo=' + $scope.Modelo.Codigo + '&OpcionExportarPdf=' + OpcionPDf);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&SEMI_Codigo=' + $scope.Modelo.Codigo + '&OpcionExportarExcel=' + OpcionEXCEL);
            }



        };

        //Fin Novedades


        if ($scope.Sesion.UsuarioAutenticado.ManejoEstadosNovedades) {
            $scope.DeshabilitarEstado = true;
        }


        $scope.BorrarFoto = function () {
            $scope.Modelo.Foto = null;
        };
        $scope.MaskPlaca = function () {
            try { $scope.Modelo.Placa = MascaraPlaca($scope.Modelo.Placa) } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function (option) {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };

    }]);