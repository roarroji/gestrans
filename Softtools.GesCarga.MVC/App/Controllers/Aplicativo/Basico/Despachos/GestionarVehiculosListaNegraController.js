﻿SofttoolsApp.controller('GestionarVehiculosListaNegraCtrl', ['$scope', '$timeout', '$routeParams', 'blockUI', '$linq', 'VehiculosFactory', 'VehiculosListaNegraFactory',
    function ($scope, $timeout, $routeParams, blockUI, $linq, VehiculosFactory, VehiculosListaNegraFactory) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Vehículos Lista Negra' }, { Nombre: 'Gestionar' }];

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_VEHICULOS_LISTA_NEGRA);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.NumeroPagina = 1;
        $scope.RegistrosporPagina = 10;

        $scope.MensajesError = [];
        $scope.DeshabilitarActualizar = false;
        
         
        $scope.ListaEstados = [
             
            { Nombre: 'RECHAZADO', Codigo: 1 },
            { Nombre: 'HABILITADO', Codigo: 0 }
        ];

        $scope.Modelo = {
            Vehiculo: {
                Placa : '',
                Codigo: '',
                CodigoListaNegra: ''
            },
            Causa: '',
            OrganismoTransito: '',
            Estado: ''

        }
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo == 1');


        if ($routeParams.Codigo > CERO) {
            $scope.Modelo.Vehiculo.Codigo = $routeParams.Codigo;

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Vehiculo.Codigo
            }
            VehiculosFactory.ConsultarListaNegra(filtros)
                .then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            console.log("resultados: ",response.data.Datos);
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo ==' + response.data.Datos[0].EstadoListaNegra);
                            $scope.Modelo.Causa = response.data.Datos[0].Causa;
                            $scope.Modelo.OrganismoTransito = response.data.Datos[0].OrganismoTransito;
                            $scope.Modelo.Vehiculo.CodigoListaNegra = response.data.Datos[0].CodigoListaNegra;
                            $scope.Modelo.Vehiculo.Placa = response.data.Datos[0].Placa;
                        }
                    }
                });

            $scope.DeshabilitarActualizar = true;
            $('#validacionPlaca').removeAttr('data-toggle');
            $('#validacionPlaca').removeAttr('title');
            
        }


        //Botones:

        //Cancelar:

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarVehiculosListaNegra/' + $scope.Modelo.Vehiculo.Codigo;
        };


        var placaenVehiculos = true;
        
        $('#validacionPlaca').hover(function () {
            $(this).removeAttr('data-toggle');
            $(this).removeAttr('title');
        });
        $scope.OcultarTooltip = function () {
            $('#validacionPlaca').tooltip('hide');
        };
        $scope.ValidarPlaca = function () {
            var input = $('#validacionPlaca');

            input.attr('data-toggle', 'tooltip');
            input.attr('title', '<span>ESTE VEHÍCULO NO ESTÁ REGISTRADO</span>');

            $('#validacionPlaca').tooltip({
                trigger: 'manual',
                html: true
            });

            filtros = {
                CodigoEmpresa : $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Placa : $scope.Modelo.Vehiculo.Placa
            }

            VehiculosFactory.Consultar(filtros)
                .then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                       
                        if (response.data.Datos.length > 0) {    
                           // placaenVehiculos = true;
                            $('#validacionPlaca').tooltip('hide');
                           // $scope.Modelo.Vehiculo.Codigo = response.data.Datos[0].Codigo;
                           
                        } else {
                            //placaenVehiculos = false;
                            $('#validacionPlaca').tooltip('show');
                            $('.tooltip').css('width', '200px');
                            
                        }
                    }
                });           

            VehiculosFactory.ConsultarListaNegra(filtros)
                .then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            console.log(response.data.Datos);
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo ==' + response.data.Datos[0].EstadoListaNegra);
                            $scope.Modelo.Causa = response.data.Datos[0].Causa;
                            $scope.Modelo.OrganismoTransito = response.data.Datos[0].OrganismoTransito;
                            $scope.Modelo.Vehiculo.CodigoListaNegra = response.data.Datos[0].CodigoListaNegra;
                            
                        }
                    }
                });
        }


        $scope.ConfirmacionGuardar = function () {
            $('#validacionPlaca').tooltip('hide');
            $('#validacionPlaca').removeAttr('data-toggle');
            $('#validacionPlaca').removeAttr('title');
            showModal('modalConfirmacionGuardar');
            
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                
                var datos = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Modelo.Vehiculo.CodigoListaNegra,
                    //Codigo: $scope.Modelo.Vehiculo.Codigo,
                    Placa: $scope.Modelo.Vehiculo.Placa,
                    Causa: $scope.Modelo.Causa,
                    OrganismoTransito: $scope.Modelo.OrganismoTransito,
                    EstadoListaNegra:  $scope.Modelo.Estado.Codigo ,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                }
               
               VehiculosListaNegraFactory.Guardar(datos)
                    .then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > CERO) {
                                if ($scope.Modelo.CodigoListaNegra == CERO || $scope.Modelo.CodigoListaNegra == "" || $scope.Modelo.CodigoListaNegra == undefined) {
                                   
                                    ShowSuccess('Se guardó el vehículo '+datos.Placa+' en la lista negra');
                                    
                                } else {
                                    ShowSuccess('Se modificó el vehículo '+datos.Placa+' en la lista negra');
                                }
                                location.href = '#!ConsultarVehiculosListaNegra/' + response.data.Datos;
                            } else {
                                ShowError('no hay datos de retorno');
                            }
                        } else {
                            ShowError('funciona');
                            console.log(response);
                        }
                    }, function (response) {

                        ShowError(response.statusText);

                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Vehiculo.Placa == undefined || $scope.Modelo.Vehiculo.Placa == null || $scope.Modelo.Vehiculo.Placa == '') {
                $scope.MensajesError.push('Debe ingresar una placa');
                continuar = false;
            }
            if (placaenVehiculos == false) {
                $scope.MensajesError.push('La placa ingresada no existe en la base de datos');
                continuar = false;
            }
            if ($scope.Modelo.Causa == undefined || $scope.Modelo.Causa == null || $scope.Modelo.Causa == '') {
                $scope.MensajesError.push('Debe ingresar una causa');
                continuar = false;
            }

            if ($scope.Modelo.OrganismoTransito == undefined || $scope.Modelo.OrganismoTransito == null || $scope.Modelo.OrganismoTransito == '') {
                $scope.MensajesError.push('Debe ingresar un Organismo de Tránsito');
                continuar = false;
            }
            return continuar;
        }

        /*---------------------------------------------------------------------------------------------------Máscaras-----------------------------------------------------------------------------*/
        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope);
        }
    }

   
]);