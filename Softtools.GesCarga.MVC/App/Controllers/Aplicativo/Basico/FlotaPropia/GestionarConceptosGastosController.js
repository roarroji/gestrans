﻿SofttoolsApp.controller("GestionarConceptosGastosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ConceptoGastosFactory', 'PlanUnicoCuentasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ConceptoGastosFactory, PlanUnicoCuentasFactory) {
        console.clear()
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Flota Propia' }, { Nombre: 'Concepto Gastos' }, { Nombre: 'Gestionar' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;



        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONCEPTOS_GASTOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.paginaActual = 1;
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.Codigo = 0;
        $scope.Numero = 0;
        $scope.ModeloFechaCrea = '';

        $scope.Deshabilitar = false;

        $scope.Modelo = {

            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },

        }
        $scope.CargarDatosFunciones = function () {

            $scope.ListadoEstados = [
                { Nombre: 'Activa', Codigo: 1 },
                { Nombre: 'Inactiva', Codigo: 0 }
            ];
            $scope.ListadoOperacion = [
                { Nombre: 'Seleccione Item', Codigo: -1 },
                { Nombre: 'Suma (+)', Codigo: 1 },
                { Nombre: 'Resta (-)', Codigo: 2 }
            ];
            try {
                $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + $scope.Modelo.Estado);
            } catch (e) {
                $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
                $scope.Modelo.Estado = 1
            }
            try {
                $scope.ModeloOperacion = $linq.Enumerable().From($scope.ListadoOperacion).First('$.Codigo == ' + $scope.Modelo.Operacion);
            } catch (e) {
                $scope.ModeloOperacion = $linq.Enumerable().From($scope.ListadoOperacion).First('$.Codigo == -1');
            }

            PlanUnicoCuentasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: ESTADO_ACTIVO
            }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.push({ CodigoNombreCuenta: '', CodigoCuenta: '', Codigo: 0 });
                            $scope.ListaPuc = response.data.Datos;
                            try {
                                $scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListaPuc).First('$.Codigo ==' + $scope.Modelo.CuentaPUC.Codigo)
                            } catch (e) {

                            }

                        }
                        else {
                            $scope.ListaPuc = [];
                            $scope.ModeloCuenta = null;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }




        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);

        };

        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope)
        }

        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };



        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando Planilla Entregada Código ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Planilla Entregada Código ' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            blockUI.delay = 1000;
            ConceptoGastosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = response.data.Datos;
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.CargarDatosFunciones()
                        $scope.ModeloFechaCrea = new Date(response.data.Datos.FechaCrea);
                        $scope.ModeloConceptoSistema = response.data.Datos.ConceptoSistema;
                        if ($scope.ModeloConceptoSistema == 1) {
                            $scope.Deshabilitar = true
                        }
                    }
                    
                    else {
                        ShowError('No se logro consultar la Orden de Cargue ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarConceptosGastos';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarConceptosGastos';
                });
            blockUI.stop();
        };

        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardarConcepto');
            if (DatosRequeridos()) {
               
                ConceptoGastosFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Numero == 0) {
                                    ShowSuccess('Se guardó el concepto : ' + $scope.Modelo.Nombre);
                                }
                                else {
                                    ShowSuccess('Se modificó el concepto : ' + $scope.Modelo.Nombre);
                                }
                                location.href = '#!ConsultarConceptosGastos/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                            ShowError("El registro con el código: " + $scope.Modelo.Codigo+" ya se encuentra en el sistema");
                    });

            }
        };

        $scope.ConfirmacionGuardarConcepto = function () {
            showModal('modalConfirmacionGuardarConcepto');
        };

        function DatosRequeridos() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Modelo.Codigo == '' || $scope.Modelo.Codigo == undefined || $scope.Modelo.Codigo == null) {
                $scope.MensajesError.push('Debe ingresar el Código');
                continuar = false
            }
            if ($scope.Modelo.Nombre == '' || $scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == null) {
                $scope.MensajesError.push('Debe ingresar el Nombre');
                continuar = false
            }
            if ($scope.ModeloOperacion.Codigo == -1) {  //NO APLICA
                $scope.MensajesError.push('Debe Seleccionar una operación  ');
                continuar = false
            }
            return continuar;
        }



        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($routeParams.Numero > 0) {
                document.location.href = '#!ConsultarConceptosGastos/' + $scope.ModeloCodigo;
            } else {
                document.location.href = '#!ConsultarConceptosGastos';
            }
        };


        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        };

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };

        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskDecimales = function () {
            MascaraDecimalesGeneral($scope);
        }

        $scope.MaskTelefono = function () {
            return MascaraTelefono($scope);
        }
        $scope.MaskDireccion = function () {
            return MascaraDireccion($scope)
        }
        if ($routeParams.Codigo !== undefined) {
            $scope.Codigo = $routeParams.Codigo;
            if ($scope.Codigo > 0) {
                Obtener()
            }
        }
        else {
            $scope.CargarDatosFunciones()
            $scope.Codigo = 0;
        }
    }]);