﻿SofttoolsApp.controller("GestionarEmpresasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'EmpresasFactory', 'ValorCatalogosFactory', 'CiudadesFactory', 'PaisesFactory', 'TercerosFactory', 'blockUIConfig',
    function ($scope, $routeParams, $timeout, $linq, blockUI, EmpresasFactory, ValorCatalogosFactory, CiudadesFactory, PaisesFactory, TercerosFactory, blockUIConfig) {

        $scope.Titulo = 'GESTIONAR EMPRESA';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Empresas' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_EMPRESAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoTipoIdentificacion = [];
        $scope.ListadoCiudades = [];
        $scope.ListadoPaises = [];

        $scope.Codigo = 0;


        $scope.ListadoAseguradoras = [];
        $scope.AutocompleteAseguradora = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_ASEGURADORA, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoAseguradoras = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoAseguradoras)
                }
            }
            return $scope.ListadoAseguradoras
        }
        /*Cargar el combo de tipo identificación*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoIdentificacion = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoIdentificacion.push({ Codigo: 0, Nombre: 'Seleccione un tipo' });
                        response.data.Datos.forEach(function (item) {
                            if (item.Codigo !== 100) {
                                $scope.ListadoTipoIdentificacion.push(item)
                            }
                        })
                        if ($scope.CodigoTipoIdentificacion !== undefined && $scope.CodigoTipoIdentificacion !== '' && $scope.CodigoTipoIdentificacion !== null) {
                            $scope.ModalTipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacion);
                        } else {
                            $scope.ModalTipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                        }
                    }
                    else {
                        $scope.ModalTipoIdentificacion = []
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de ciudades*/
        CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListadoCiudades = response.data.Datos;
                        if ($scope.CodigoCiudad !== undefined && $scope.CodigoCiudad !== '' && $scope.CodigoCiudad !== null) {
                            $scope.ModeloCiudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + $scope.CodigoCiudad);
                        } else {
                            $scope.ModeloCiudad = '';
                        }
                    } else {
                        $scope.ListadoCiudades = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de paises*/
        PaisesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListadoPaises = response.data.Datos;
                        if ($scope.CodigoPais !== undefined && $scope.CodigoPais !== '' && $scope.CodigoPais !== null) {
                            $scope.ModeloPais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + $scope.CodigoPais);
                        } else {
                            $scope.ModeloPais = '';
                        }
                    } else {
                        $scope.ListadoPaises = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //----Catalogo Ambientes Factura Electronica
        var ResponseAmbientes = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_AMBIENTE_FACTURA_ELECTRONICA }, Sync: true }).Datos;
            
       
        $scope.ListadoAmbientes = [];
        if (ResponseAmbientes.length > 0) {
            $scope.ListadoAmbientes = ResponseAmbientes;
            if ($scope.ModeloAmbiente === undefined || $scope.ModeloAmbiente === '' || $scope.ModeloAmbiente === null || $scope.ModeloAmbiente.Codigo === CERO) {
                $scope.ModeloAmbiente = $linq.Enumerable().From($scope.ListadoAmbientes).First('$.Codigo ==' + AMBIENTE_FACTURA_ELECTRONICA.PRUEBAS);
            }
        }
        else {
            $scope.ListadoAmbientes = [];
        }
            
            
        //----Catalogo Ambientes Factura Electronica
        //----Catalogo Proveedores Factura Electronica
        $scope.ListadoProveedorFael = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_PROVEEDOR_FACTURA_ELECTRONICA },
            Sync: true
        }).Datos;
        $scope.ListadoProveedorFael.push({ Codigo: 0, Nombre: '(NO APLICA)' });
        $scope.ModeloProveedor = $linq.Enumerable().From($scope.ListadoProveedorFael).First('$.Codigo == 0');
        //----Catalogo Proveedores Factura Electronica

        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Codigo = $routeParams.Codigo;
            if ($scope.Codigo > 0) {
                $scope.Deshabilitar = true;
                ObtenerEmpresa();
            }
        }
        else {
            $scope.Codigo = 0;
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function ObtenerEmpresa() {
            blockUI.start('Cargando empresa código ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando empresa Código ' + $scope.Codigo);
            }, 200);

            filtros = {
                Codigo: $scope.Codigo,
            };

            blockUI.delay = 1000;
            EmpresasFactory.Obtener(filtros).
                then(function (response) {                    
                    if (response.data.ProcesoExitoso === true) {

                        $scope.ModeloRazonSocial = response.data.Datos.RazonSocial
                        $scope.ModeloNombreCortoRazonSocial = response.data.Datos.NombreCortoRazonSocial
                        $scope.ModeloCodigoAlterno = response.data.Datos.CodigoAlterno
                        $scope.ModeloNumeroIdentificacion = response.data.Datos.NumeroIdentificacion
                        $scope.ModeloDiguitoChequeo = response.data.Datos.DigitoChequeo
                        $scope.ModeloDireccion = response.data.Datos.Direccion
                        $scope.ModeloTelefono = response.data.Datos.Telefono
                        $scope.ModeloCodigoPostal = response.data.Datos.CodigoPostal
                        $scope.ModeloCorreo = response.data.Datos.Email
                        $scope.ModeloPaginaWeb = response.data.Datos.PaginaWeb
                        $scope.ModeloMensajeBanner = response.data.Datos.MensajeBanner
                        $scope.ModeloAceptacionElectronica = response.data.Datos.AceptacionElectronica == 1 ? true : false;
                        $scope.ModeloActividadEconomica = response.data.Datos.ActividadEconomica

                        $scope.ModeloCodigoUIAF = response.data.Datos.CodigoUIAF

                        $scope.CodigoTipoIdentificacion = response.data.Datos.CodigoTipoDocumento
                        if ($scope.ListadoTipoIdentificacion.length > 0 && $scope.CodigoTipoIdentificacion > 0) {
                            $scope.ModalTipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo == ' + response.data.Datos.CodigoTipoDocumento);
                        }

                        $scope.CodigoCiudad = response.data.Datos.Ciudad.Codigo
                        if ($scope.ListadoCiudades.length > 0 && $scope.CodigoCiudad > 0) {
                            $scope.ModeloCiudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo == ' + response.data.Datos.Ciudad.Codigo);
                        }

                        $scope.CodigoPais = response.data.Datos.Pais.Codigo
                        if ($scope.ListadoPaises.length > 0 && $scope.CodigoPais > 0) {
                            $scope.ModeloPais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo == ' + response.data.Datos.Pais.Codigo);
                        }

                        $scope.ModeloCodigoRegionalMinistertio = response.data.Datos.ListadoRNDC.CodigoRegionalMinisterio;
                        $scope.ModeloCodigoEmpresaMinisterio = response.data.Datos.ListadoRNDC.CodigoEmpresaMinisterio;
                        $scope.ModeloUsuarioManifiestoElectronico = response.data.Datos.ListadoRNDC.UsuarioManifiestoElectronico;
                        $scope.ModeloClaveManifiestoElectronico = response.data.Datos.ListadoRNDC.ClaveManifiestoElectronico;
                        $scope.ModeloNumeroResolucion = response.data.Datos.ListadoRNDC.NumeroResolucion;
                        $scope.ModeloEnlaceWSMinisterio = response.data.Datos.ListadoRNDC.EnlaceWSMinisterio;

                        if (response.data.Datos.EmpresaFacturaElectronica !== null && response.data.Datos.EmpresaFacturaElectronica !== undefined && response.data.Datos.EmpresaFacturaElectronica !== '') {
                            if (response.data.Datos.EmpresaFacturaElectronica.Proveedor.Codigo !== CERO) {
                                $scope.ModeloProveedor = $linq.Enumerable().From($scope.ListadoProveedorFael).First('$.Codigo ==' + response.data.Datos.EmpresaFacturaElectronica.Proveedor.Codigo);
                            }
                            $scope.ModeloUsuario = response.data.Datos.EmpresaFacturaElectronica.Usuario;
                            $scope.ModeloClave = response.data.Datos.EmpresaFacturaElectronica.Clave;
                            $scope.ModeloOperadorVirtual = response.data.Datos.EmpresaFacturaElectronica.OperadorVirtual;
                            $scope.ModeloPrefijoFactura = response.data.Datos.EmpresaFacturaElectronica.PrefijoFactura;
                            $scope.ModeloClaveTecnicaFactura = response.data.Datos.EmpresaFacturaElectronica.ClaveTecnicaFactura;
                            $scope.ModeloClaveExternaFactura = response.data.Datos.EmpresaFacturaElectronica.ClaveExternaFactura;
                            $scope.ModeloPrefijoNotaCredito = response.data.Datos.EmpresaFacturaElectronica.PrefijoNotaCredito;
                            $scope.ModeloClaveExternaNotaCredito = response.data.Datos.EmpresaFacturaElectronica.ClaveExternaNotaCredito;
                            $scope.ModeloPrefijoNotaDebito = response.data.Datos.EmpresaFacturaElectronica.PrefijoNotaDebito;
                            $scope.ModeloClaveExternaNotaDebito = response.data.Datos.EmpresaFacturaElectronica.ClaveExternaNotaDebito;
                            $scope.ModeloIdEmpresa = response.data.Datos.EmpresaFacturaElectronica.ID_Empresa;
                            if (response.data.Datos.EmpresaFacturaElectronica.Ambiente.Codigo !== CERO) {
                                $scope.ModeloAmbiente = $linq.Enumerable().From($scope.ListadoAmbientes).First('$.Codigo ==' + response.data.Datos.EmpresaFacturaElectronica.Ambiente.Codigo);
                            }
                            $scope.ModeloFirmaDigital = response.data.Datos.EmpresaFacturaElectronica.FirmaDigital;
                        }

                        try {
                            $scope.ModeloAseguradora = $scope.CargarTercero(response.data.Datos.Aseguradora.Codigo)
                        } catch (e) {
                            $scope.ModeloAseguradora = undefined
                        }
                        $scope.ModeloNumeroPoliza = response.data.Datos.NumeroPoliza
                        try {
                            if (new Date(response.data.Datos.FechaVencimientoPoliza) > MIN_DATE) {
                                $scope.ModeloFechaVencimientoPoliza = new Date(response.data.Datos.FechaVencimientoPoliza)
                            }
                        } catch (e) {

                        }

                        $scope.ValorPoliza = response.data.Datos.ValorPoliza
                        $scope.ModeloValorCombustible = response.data.Datos.ValorCombustible

                        $scope.MaskValores()
                    }
                    else {
                        ShowError('No se logro consultar la empresas' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarEmpresas';
                    }
                }, function (response) {
                    ShowError(response.statusText)
                    document.location.href = '#!ConsultarEmpresas';
                });

            blockUI.stop();
        };

        /*--------------------------------------------------------------------------------GUARDAR O MODIFICAR---------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardarEmpresa = function () {
            if (DatosRequeridosEmpresa()) {
                showModal('modalConfirmacionGuardarEmpresa');
            }
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarEmpresa');

            $scope.DetalleListaRNDC = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CodigoRegionalMinisterio: $scope.ModeloCodigoRegionalMinistertio,
                CodigoEmpresaMinisterio: $scope.ModeloCodigoEmpresaMinisterio,
                UsuarioManifiestoElectronico: $scope.ModeloUsuarioManifiestoElectronico,
                NumeroResolucion: $scope.ModeloNumeroResolucion,
                ClaveManifiestoElectronico: $scope.ModeloClaveManifiestoElectronico,
                EnlaceWSMinisterio: $scope.ModeloEnlaceWSMinisterio
            };

            $scope.DetalleListaFactura = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Proveedor: { Codigo: $scope.ModeloProveedor.Codigo },
                Usuario: $scope.ModeloUsuario,
                Clave: $scope.ModeloClave,
                OperadorVirtual: $scope.ModeloOperadorVirtual,
                PrefijoFactura: $scope.ModeloPrefijoFactura,
                ClaveTecnicaFactura: $scope.ModeloClaveTecnicaFactura,
                ClaveExternaFactura: $scope.ModeloClaveExternaFactura,
                PrefijoNotaCredito: $scope.ModeloPrefijoNotaCredito,
                ClaveExternaNotaCredito: $scope.ModeloClaveExternaNotaCredito,
                PrefijoNotaDebito: $scope.ModeloPrefijoNotaDebito,
                ClaveExternaNotaDebito: $scope.ModeloClaveExternaNotaDebito,
                Ambiente: { Codigo: $scope.ModeloAmbiente.Codigo },
                ID_Empresa: $scope.ModeloIdEmpresa,
                FirmaDigital: $scope.ModeloFirmaDigital
            };

            $scope.ModeloAceptacionElectronica = $scope.ModeloAceptacionElectronica == true ? 1 : 0;

            DatosGuardar = {
                Codigo: $scope.Codigo,
                CodigoAlterno: $scope.ModeloCodigoAlterno,
                RazonSocial: $scope.ModeloRazonSocial,
                CodigoUIAF: $scope.ModeloCodigoUIAF,
                NombreCortoRazonSocial: $scope.ModeloNombreCortoRazonSocial,
                CodigoTipoDocumento: $scope.ModalTipoIdentificacion.Codigo,
                NumeroIdentificacion: $scope.ModeloNumeroIdentificacion,
                DigitoChequeo: $scope.ModeloDiguitoChequeo,
                Direccion: $scope.ModeloDireccion,
                Pais: $scope.ModeloPais,
                Ciudad: $scope.ModeloCiudad,
                CodigoPostal: $scope.ModeloCodigoPostal,
                Telefono: $scope.ModeloTelefono,
                Email: $scope.ModeloCorreo,
                PaginaWeb: $scope.ModeloPaginaWeb,
                MensajeBanner: $scope.ModeloMensajeBanner,
                ListadoRNDC: $scope.DetalleListaRNDC,
                EmpresaFacturaElectronica: $scope.DetalleListaFactura,
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                ValorPoliza: $scope.ValorPoliza,
                ValorCombustible: $scope.ModeloValorCombustible,
                AceptacionElectronica: $scope.ModeloAceptacionElectronica,
                ActividadEconomica: $scope.ModeloActividadEconomica
            }
            try {
                DatosGuardar.Aseguradora = { Codigo: $scope.ModeloAseguradora.Codigo }
                DatosGuardar.NumeroPoliza = $scope.ModeloNumeroPoliza
                DatosGuardar.FechaVencimientoPoliza = $scope.ModeloFechaVencimientoPoliza
            } catch (e) {

            }
            EmpresasFactory.Guardar(DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Codigo === 0) {
                                ShowSuccess('Se guardó la empresa ' + $scope.ModeloRazonSocial);
                            }
                            else {
                                ShowSuccess('Se modificó la empresa ' + $scope.ModeloRazonSocial);
                            }
                            location.href = '#!ConsultarEmpresas/' + response.data.Datos;
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        /*--------------------------------------------------------------------------------DATOS REQUERIDOS---------------------------------------------------------------------------------------------------*/
        function DatosRequeridosEmpresa() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.ModeloRazonSocial === undefined || $scope.ModeloRazonSocial === '' || $scope.ModeloRazonSocial === null || $scope.ModeloRazonSocial === 0) {
                $scope.MensajesError.push('Debe ingresar el nombre de la empresa');
                continuar = false;
            }
            if ($scope.ModeloNombreCortoRazonSocial === undefined || $scope.ModeloNombreCortoRazonSocial === '' || $scope.ModeloNombreCortoRazonSocial === null || $scope.ModeloNombreCortoRazonSocial === 0) {
                $scope.MensajesError.push('Debe ingresar el nombre corto de la empresa');
                continuar = false;
            }
            if ($scope.ModalTipoIdentificacion === undefined || $scope.ModalTipoIdentificacion === '' || $scope.ModalTipoIdentificacion === null || $scope.ModalTipoIdentificacion.Codigo === 0) {
                $scope.MensajesError.push('Debe ingresar el tipo de identificación');
                continuar = false;
            }
            $scope.ValidarNumeroIdentificacion = $scope.ModeloNumeroIdentificacion.toString()
            if (($scope.ValidarNumeroIdentificacion.length < 7)) {
                $scope.MensajesError.push('Debe ingresar un número de identificación con más de 6 caracteres');
                continuar = false;
            }
            if ($scope.ModeloDiguitoChequeo === undefined || $scope.ModeloDiguitoChequeo === '' || $scope.ModeloDiguitoChequeo === null) {
                $scope.MensajesError.push('Debe ingresar un digito de chequeo');
                continuar = false;
            }
            if ($scope.ModeloPais.Codigo === undefined || $scope.ModeloPais.Codigo === '' || $scope.ModeloPais.Codigo === null || $scope.ModeloPais.Codigo === 0) {
                $scope.MensajesError.push('Debe ingresar el país');
                continuar = false;
            }
            if ($scope.ModeloCiudad.Codigo === undefined || $scope.ModeloCiudad.Codigo === '' || $scope.ModeloCiudad.Codigo === null || $scope.ModeloCiudad.Codigo === 0) {
                $scope.MensajesError.push('Debe ingresar la ciudad');
                continuar = false;
            }
            if ($scope.ModeloDireccion === undefined || $scope.ModeloDireccion === '' || $scope.ModeloDireccion === null || $scope.ModeloDireccion === 0) {
                $scope.MensajesError.push('Debe ingresar la dirección');
                continuar = false;
            }
            if ($scope.ModeloTelefono === undefined || $scope.ModeloTelefono === '' || $scope.ModeloTelefono === null || $scope.ModeloTelefono === 0) {
                $scope.MensajesError.push('Debe ingresar el teléfono');
                continuar = false;
            }
            if ($scope.ModeloCorreo === undefined || $scope.ModeloCorreo === '' || $scope.ModeloCorreo === null || $scope.ModeloCorreo === 0) {
                $scope.MensajesError.push('Debe ingresar un e-mail valido');
                continuar = false;
            }
            return continuar;
        }

        /*--------------------------------------------------------------------------------VOLVER MASTER---------------------------------------------------------------------------------------------------*/
        $scope.VolverMaster = function () {
            if ($routeParams.Codigo > 0) {
                document.location.href = '#!ConsultarEmpresas/' + $routeParams.Codigo;
            } else {
                document.location.href = '#!ConsultarEmpresas';
            }
        };

        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskTelefono = function (option) {
            try { $scope.ModeloTelefono = MascaraTelefono($scope.ModeloTelefono) } catch (e) { }
        };
        $scope.MaskDireccion = function (option) {
            try { $scope.ModeloDireccion = MascaraDireccion($scope.ModeloDireccion) } catch (e) { }
        };


    }]);