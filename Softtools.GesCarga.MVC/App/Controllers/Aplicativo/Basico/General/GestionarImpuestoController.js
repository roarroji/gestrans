﻿SofttoolsApp.controller("GestionarImpuestoCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory', 'ImpuestosFactory', 'PlanUnicoCuentasFactory', 'OficinasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ValorCatalogosFactory, ImpuestosFactory, PlanUnicoCuentasFactory, OficinasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Impuestos' }, { Nombre: 'Gestionar' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OCION_MENU_IMPUESTOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.Codigo = 0;
        $scope.ModeloCodigo = 0;
        $scope.ListadoTipoImpuesto = [];
        $scope.Recaudovalor = true;
        $scope.ListaPuc = [];
        $scope.ModalRecaudo = [];
        $scope.ModalImpuesto = [];
        $scope.ModeloCuenta = [];

        $scope.ListadoEstados = [
            { Nombre: 'SELECCIONE ITEM', Codigo: -1 },
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];

        $scope.ListadoOperacion = [
            { Nombre: 'SELECCIONE ITEM', Codigo: -1 },
            { Nombre: 'SUMA (+)', Codigo: 1 },
            { Nombre: 'RESTA (-)', Codigo: 2 }
        ];

        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        $scope.ModeloOperacion = $linq.Enumerable().From($scope.ListadoOperacion).First('$.Codigo == -1');
        $scope.ListadoCiudades = [];
        // Se crea la propiedad modelo en el ambito
        //$scope.Modelo = {
        //    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        //    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        //    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        //    Codigo: 0,
        //    Estado: $scope.ListadoEstados[0],

        //}
        /* Obtener parametros del enrutador*/





        if ($routeParams.Codigo !== undefined) {
            $scope.Codigo = $routeParams.Codigo;
        }
        else {
            $scope.Codigo = 0;
        }


        //-----Asignaciones ---------------------

        $scope.AsignarCodigoAlterno = function (CodigoAlterno) {

            $scope.ModeloCodigoAlterno = CodigoAlterno;
        }


        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IMPUESTO_LUGAR } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoRecaudo = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoRecaudo.push({ Codigo: 0, Nombre: '(TODAS)' });
                        $scope.ModalRecaudo = $scope.ListadoTipoRecaudo[0];
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoTipoRecaudo.push(response.data.Datos[i]);


                        }

                        if ($scope.CodigoTipoRecaudo !== undefined && $scope.CodigoTipoRecaudo !== '' && $scope.CodigoTipoRecaudo !== null) {
                            if ($scope.CodigoTipoRecaudo > CERO) {
                                $scope.ModalRecaudo = $linq.Enumerable().From($scope.ListadoTipoRecaudo).First('$.Codigo == ' + $scope.CodigoTipoRecaudo);

                            }
                        }


                    }
                    else {
                        $scope.ListadoTipoRecaudo = []
                    }
                }
            }, function (response) {
            });

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IMPUESTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoImpuesto = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoImpuesto.push({ Codigo: 0, Nombre: '(TODAS)' })

                        $scope.ModalImpuesto = $scope.ListadoTipoImpuesto[0];
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo !== CODIGO_TIPO_IMPUESTO_NINGUNO) {
                                $scope.ListadoTipoImpuesto.push(response.data.Datos[i]);
                            }
                        }
                        if ($scope.CodigoTipoImpuesto !== undefined && $scope.CodigoTipoImpuesto !== '' && $scope.CodigoTipoImpuesto !== null) {
                            if ($scope.CodigoTipoImpuesto > CERO) {
                                $scope.ModalImpuesto = $linq.Enumerable().From($scope.ListadoTipoImpuesto).First('$.Codigo == ' + $scope.CodigoTipoImpuesto);

                            }
                        }


                    }
                    else {
                        $scope.ListadoTipoImpuesto = []
                    }
                }
            }, function (response) {
            });

        PlanUnicoCuentasFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: ESTADO_ACTIVO
        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ CodigoNombreCuenta: '', CodigoCuenta: '', Codigo: 0 });
                        $scope.ListaPuc = response.data.Datos;
                        $scope.ModeloCuenta = $scope.ListaPuc[$scope.ListaPuc.length - 1];

                        if ($scope.CodigoPuc !== undefined && $scope.CodigoPuc !== '' && $scope.CodigoPuc !== null) {
                            if ($scope.CodigoPuc > 0) {
                                $scope.ModeloCuenta = $linq.Enumerable().From($scope.ListaPuc).First('$.Codigo ==' + $scope.CodigoPuc);
                            }
                        }

                    }
                    else {
                        $scope.ListaPuc = [];
                        $scope.ModeloCuenta = null;
                    }

                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //----------------------------------------

        $scope.CambioTipoRecaudo = function (CodigoRecaudo) {
            if (CodigoRecaudo == CERO || CodigoRecaudo == CODIGO_TIPO_RECAUDO_PAIS) {
                $scope.Recaudovalor = true;
            } else {
                $scope.Recaudovalor = false;
            }

        };


        if ($scope.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR OFICINA';
            $scope.Deshabilitar = true;

            ObtenerImpuesto();
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function ObtenerImpuesto() {
            blockUI.start('Cargando Impuesto Código ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Impuesto Código ' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            blockUI.delay = 1000;
            ImpuestosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ModeloCodigo = response.data.Datos.Codigo;
                        $scope.ModeloCodigoAlterno = response.data.Datos.CodigoAlterno;
                        $scope.ModeloNombre = response.data.Datos.Nombre;
                        $scope.ModeloLabel = response.data.Datos.Label;


                        $scope.ModeloValorTarifa = response.data.Datos.Valor_tarifa;
                        $scope.ModeloValorBase = response.data.Datos.valor_base;


                        $scope.CodigoPuc = response.data.Datos.PUC.Codigo;
                        if ($scope.ListaPuc !== null && $scope.ListaPuc !== undefined) {
                            if ($scope.ListaPuc.length > CERO) {
                                $scope.ModeloCuenta = $linq.Enumerable().From($scope.ListaPuc).First('$.Codigo ==' + $scope.CodigoPuc);
                            }
                        }

                        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);

                        $scope.CodigoTipoRecaudo = response.data.Datos.Tipo_Recaudo.Codigo;
                        //if ($scope.ListadoTipoRecaudo !== null && $scope.ListadoTipoRecaudo !== undefined) {
                        if ($scope.ListadoTipoRecaudo.length > 0) {
                            $scope.ModalRecaudo = $linq.Enumerable().From($scope.ListadoTipoRecaudo).First('$.Codigo == ' + $scope.CodigoTipoRecaudo);
                        }
                        if ($scope.ModalRecaudo.Codigo == CODIGO_TIPO_RECAUDO_PAIS) {
                            $scope.Recaudovalor = true;
                        } else {
                            $scope.Recaudovalor = false;
                        }



                        $scope.CodigoTipoImpuesto = response.data.Datos.Tipo_Impuesto.Codigo;
                        //if ($scope.ListadoTipoImpuesto !== null && $scope.ListadoTipoImpuesto !== undefined) {
                        if ($scope.ListadoTipoImpuesto.length > 0) {
                            $scope.ModalImpuesto = $linq.Enumerable().From($scope.ListadoTipoImpuesto).First('$.Codigo == ' + $scope.CodigoTipoImpuesto);
                        }


                        $scope.ModeloOperacion = $linq.Enumerable().From($scope.ListadoOperacion).First('$.Codigo == ' + response.data.Datos.Operacion);

                    }
                    else {
                        ShowError('No se logro consultar el impuesto ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarImpuestos';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarImpuestos';
                });
            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardarImpuesto = function () {
            showModal('modalConfirmacionGuardarImpuesto');

        };

        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardarImpuesto');
            if (DatosRequeridosImpuesto()) {
                $scope.objEnviar = {

                    Codigo: $scope.Codigo,
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoAlterno: $scope.ModeloCodigoAlterno,
                    Nombre: $scope.ModeloNombre,
                    Label: $scope.ModeloLabel,
                    Tipo_Recaudo: $scope.ModalRecaudo,
                    Tipo_Impuesto: $scope.ModalImpuesto,
                    Operacion: $scope.ModeloOperacion.Codigo,
                    PUC: $scope.ModeloCuenta,
                    Valor_tarifa: $scope.ModeloValorTarifa,
                    AplicaDetalle: CERO,
                    valor_base: $scope.ModeloValorBase,
                    Estado: $scope.ModeloEstado.Codigo,
                    Codigo_USUA_Crea: $scope.Sesion.UsuarioAutenticado.Codigo,
                    Codigo_USUA_Modi: $scope.Sesion.UsuarioAutenticado.Codigo,

                };
                $scope.ModeloEstado = $scope.ModeloEstado.Codigo
                ImpuestosFactory.Guardar($scope.objEnviar).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.ModeloCodigo == 0) {
                                    ShowSuccess('Se guardó El Impuesto: ' + $scope.ModeloNombre);
                                    $scope.Codigo = response.data.Datos
                                }
                                else {
                                    ShowSuccess('Se modificó El Impuesto: ' + $scope.ModeloNombre);
                                }
                                location.href = '#!ConsultarImpuestos/' + $scope.Codigo;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        function DatosRequeridosImpuesto() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;


            if ($scope.ModeloNombre == undefined || $scope.ModeloNombre == "") {
                $scope.MensajesError.push('Debe ingresar el nombre del Impuesto');
                continuar = false;
            }


            if ($scope.ModeloLabel == undefined || $scope.ModeloLabel == "") {
                $scope.MensajesError.push('Debe ingresar un label para indetificarlo ');
                continuar = false;
            }

            if ($scope.ModeloOperacion == undefined || $scope.ModeloOperacion == "") {
                $scope.MensajesError.push('Debe un operador + o  -');
                continuar = false;
            }
            if ($scope.ModeloCuenta == undefined || $scope.ModeloCuenta == "" || $scope.ModeloCuenta == 0) {
                $scope.MensajesError.push('Debe Ingresar una cuenta PUC');
                continuar = false;
            }
            if ($scope.ModalRecaudo.Codigo == 801) {
                if ($scope.ModeloValorTarifa == undefined || $scope.ModeloValorTarifa == "" || $scope.ModeloValorTarifa == 0) {
                    $scope.MensajesError.push('El valor de la tarifa debe ser mayor a 0 ');
                    continuar = false;
                }
            } else {
                if ($scope.ModeloValorTarifa == undefined || $scope.ModeloValorTarifa == "") {
                    $scope.ModeloValorTarifa = CERO;
                }
            }


            if ($scope.ModeloEstado.Codigo == -1 || $scope.ModeloEstado.Codigo == undefined || $scope.ModeloEstado.Codigo == null) {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar el estado del impuesto');
                continuar = false;
            }

            if ($scope.ModalRecaudo.Codigo == -1 || $scope.ModalRecaudo.Codigo == undefined || $scope.ModalRecaudo.Codigo == null || $scope.ModalRecaudo.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar el Tipo de Recaudo ');

                continuar = false;
            }

            if ($scope.ModalImpuesto.Codigo == -1 || $scope.ModalImpuesto.Codigo == undefined || $scope.ModalImpuesto.Codigo == null || $scope.ModalImpuesto.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar el Tipo del Impuesto');
                continuar = false;
            }



            return continuar;
        }
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);

        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);

        };
        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope)
        }


        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($routeParams.Codigo > 0) {
                document.location.href = '#!ConsultarImpuestos/' + $routeParams.Codigo;
            } else {
                document.location.href = '#!ConsultarImpuestos';
            }
        };
    }]);