﻿SofttoolsApp.controller("CargarPeajesCtrl", ['$scope', '$timeout', '$linq', 'TercerosFactory', 'blockUI', 'ValorCatalogosFactory', 'PeajesFactory', 'blockUIConfig',
    function ($scope, $timeout, $linq, TercerosFactory, blockUI, ValorCatalogosFactory, PeajesFactory, blockUIConfig) {

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        $scope.Titulo = 'CARGAR PEAJES';
        $scope.Buscando = false;
        $scope.continuar = false
        $scope.VerErrores = false;
        $scope.DeshabilitarCliente = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoTipoPeaje = [];
        $scope.listaProveedores = [];
        $scope.ListadoRutasPuntosGestion = []
        $scope.listaPeajesGuardar = []

        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Procesos' }, { Nombre: 'Cargue Peajes' }];

        $scope.modalTarifasPeaje = { Categoria: '', Valor: '', }


        $scope.ListadoCategorias = [];
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_PEAJE } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCategorias = response.data.Datos;
                    }
                    else {
                        $scope.ListadoCategorias = []
                    }
                }
            }, function (response) {
            });

        $scope.ListadoProveedores = []; 

        $scope.AutocompleteProveedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false; 
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_PROVEEDOR, ValorAutocomplete: value, Sync: true, 
                    }) 
                    $scope.ListadoProveedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProveedores); 
                }
            }  
            return $scope.ListadoProveedores;
        }


       
        //////////Paginacion
 
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 20) {
                $scope.DataActual = []
                $scope.paginaActual = 1
                for (var i = 0; i < 20; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 20
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 20
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 20 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 20
                $scope.DataActual = []
                for (var i = a - 20; i < $scope.DataArchivo.length; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }
 
        $scope.PrimerPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 20) {
                $scope.DataErrorActual = []
                $scope.paginaActualError = 1
                for (var i = 0; i < 20; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }
        $scope.AnteriorModalError = function () {
            if ($scope.paginaActualError > 1) {
                $scope.paginaActualError -= 1
                var a = $scope.paginaActualError * 20
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteModalError = function () {
            if ($scope.paginaActualError < $scope.totalPaginasErrores) {
                $scope.paginaActualError += 1
                var a = $scope.paginaActualError * 20
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    $scope.UltimaPaginaModalError()

                }
            } else if ($scope.paginaActualError == $scope.totalPaginasErrores) {
                $scope.UltimaPaginaModalError()
            }
        }
        $scope.UltimaPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 20 && $scope.totalPaginasErrores > 1) {
                $scope.paginaActualError = $scope.totalPaginasErrores
                var a = $scope.paginaActualError * 20
                $scope.DataErrorActual = []
                for (var i = a - 20; i < $scope.ListaErrores.length; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }
        $scope.PrimerPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 20) {
                $scope.ProgramacionesNoGuardadasActual = []
                $scope.paginaActualNoGuardadas = 1
                for (var i = 0; i < 20; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }
        $scope.AnteriorNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas -= 1
                var a = $scope.paginaActualNoGuardadas * 20
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas < $scope.totalPaginasNoGuardadas) {
                $scope.paginaActualNoGuardadas += 1
                var a = $scope.paginaActualNoGuardadas * 20
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardados()

                }
            } else if ($scope.paginaActualNoGuardadas == $scope.totalPaginasNoGuardadas) {
                $scope.UltimaPaginaNoGuardados()
            }
        }
        $scope.UltimaPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 20 && $scope.totalPaginasNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas = $scope.totalPaginasNoGuardadas
                var a = $scope.paginaActualNoGuardadas * 20
                $scope.ProgramacionesNoGuardadasActual = []
                for (var i = a - 20; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        } 
        $scope.PrimerPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 20) {
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                $scope.paginaActualNoGuardadasRemplazo = 1
                for (var i = 0; i < 20; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }
        $scope.AnteriorNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo -= 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 20
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo < $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.paginaActualNoGuardadasRemplazo += 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 20
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardadosRemplazo()

                }
            } else if ($scope.paginaActualNoGuardadasRemplazo == $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.UltimaPaginaNoGuardadosRemplazo()
            }
        }
        $scope.UltimaPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 20 && $scope.totalPaginasNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo = $scope.totalPaginasNoGuardadasRemplazo
                var a = $scope.paginaActualNoGuardadasRemplazo * 20
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                for (var i = a - 20; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        } 

        ///////////Generar Planilla /////////////////
        $scope.GenerarPlanilla = function () {  
            var entidad = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }
            blockUI.start();
            $timeout(function () {
                blockUI.message('Generando Plantilla..'); 
            }, 1000); 

            PeajesFactory.GenerarPlanitilla(entidad).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    var pdfAsDataUri = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + response.data.Datos.Planitlla;
                    var link = document.createElement('a');
                    link.href = pdfAsDataUri;
                    link.download = "Plantilla_Cargue_Masivo_Peajes.xlsx";
                    link.click();
                    //window.open(pdfAsDataUri);
                    blockUI.stop();
                    ShowSuccess('La plantilla se generó correctamente')
                }
            }, function (response) {
                blockUI.stop();
                ShowError(response.statusText)
            }) 
        }
 
        $scope.ModalConfirmacionNuevoProceso = function () {
            showModal('modalConfirmacionCancelarProceso');
        }

        $scope.CerrarModalNuevoProceso = function (e) {
            var fileVal = document.getElementById("fileModal");
            fileVal.value = '';
            fileVal = document.getElementById("filePrincipal");
            fileVal.value = '';
            closeModal('modalConfirmacionCancelarProceso');


        } 
        $scope.handleFile = function (e) {
            var files = e.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = $scope.CargarDocumento
                reader.readAsArrayBuffer(f);
            }
        }

        $scope.DataArchivo = []
        var X = XLSX;
        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }
        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }
        function Eval(item) {
            if (item == '0' || item == undefined) {
                return true
            } else {
                return false
            }
        }

        $scope.CargarDocumento = function (e) {
            $scope.$apply(function () {
                var data = e.target.result;
                $scope.arr = fixdata(data);
                blockUI.start();
                $timeout(function () {
                    blockUI.message('Subiendo Archivo, Por Favor Espere...');
                }, 1000);
                $timeout(function () {
                    try {
                        $scope.wb = X.read(btoa($scope.arr), { type: 'base64' });
                        process_wb($scope.wb);
                    } catch (e) {
                        ShowError('El arhivo seleccionado no corresponde a una hoja de cálculo válida, por favor intente con otro archivo')
                        blockUI.stop()

                    }
                }, 1500);
            });
        }

        function process_wb(wb) {
            $scope.DataArchivo = []
            $scope.ListaErrores = []
            $scope.DataActual = []
            $scope.DataArchivo = to_json(wb);
            if ($scope.DataArchivo.Valores != undefined) {
                $scope.DataArchivo = { Valores: $scope.DataArchivo.Valores }
                AsignarValoresTabla()
            }
            else {
                ShowError('El arhivo cargado no corresponde al formato de carge masivo del tarifario')
            }
            blockUI.stop()

        }  

        function AsignarValoresTabla() {
            $scope.NumeroCargue = 0

            if ($scope.DataArchivo.Valores.length > 0) {
                //entro

                var DataTemporal = []
                for (var i = 0; i < $scope.DataArchivo.Valores.length; i++) {
                    var item = $scope.DataArchivo.Valores[i]

                   
                    if (!Eval(item.Nombre) ) {

                       
                        DataTemporal.push(item)
                    }
                }
                $scope.DataArchivo = DataTemporal
                $scope.totalRegistros = ($scope.DataArchivo.length)
                $scope.paginaActual = 1
                $scope.totalPaginas = Math.ceil($scope.totalRegistros / 20);
                blockUI.start();
                blockUI.message('Configurando campos, Esto puede tardar algunos minutos, por favor espere...');
                $timeout(function () {
                     try {
                        var data = []
                        
                     for (var i = 0; i < $scope.DataArchivo.length; i++) { 

                            if ($scope.DataArchivo[i].Nombre !== undefined && $scope.DataArchivo[i].Nombre !== '' && $scope.DataArchivo[i].Nombre !== '0') {
                                try { $scope.DataArchivo[i].Nombre = $scope.DataArchivo[i].Nombre.toUpperCase() } catch (e) { }
                            }

                            if ($scope.DataArchivo[i].Codigo_Alternativo !== undefined && $scope.DataArchivo[i].Codigo_Alternativo !== '' && $scope.DataArchivo[i].Codigo_Alternativo !== '0') {
                                try { $scope.DataArchivo[i].Codigo_Alternativo = $scope.DataArchivo[i].Codigo_Alternativo} catch (e) { }
                            }  
                             ////////////Categoria De  peajes
                        

                         if ($scope.DataArchivo[i].CATA_TIPE_Codigo !== undefined && $scope.DataArchivo[i].CATA_TIPE_Codigo !== '' && $scope.DataArchivo[i].CATA_TIPE_Codigo !== '0') {
                             try { $scope.DataArchivo[i].CATA_TIPE_Codigo = $linq.Enumerable().From($scope.ListadoCategorias).First('$.Codigo ==' + $scope.DataArchivo[i].CATA_TIPE_Codigo) } catch (e) {
                                
                             }
                         }

                         if ($scope.DataArchivo[i].TERC_Codigo_Proveedor !== undefined && $scope.DataArchivo[i].TERC_Codigo_Proveedor !== ''
                             && $scope.DataArchivo[i].TERC_Codigo_Proveedor !== '0') {
                             try {
                                 $scope.DataArchivo[i].TERC_Codigo_Proveedor
                                     = TercerosFactory.Consultar({
                                         CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                         Codigo: parseInt($scope.DataArchivo[i].TERC_Codigo_Proveedor), 
                                         CadenaPerfiles:  1409,
                                         Sync: true
                                     }).Datos[0]
                             } catch (e) {
                                 console.log(e)
                             }

                         } 

                            if ($scope.DataArchivo[i].Contacto !== undefined && $scope.DataArchivo[i].Contacto !== '' && $scope.DataArchivo[i].Contacto !== '0') {
                                try { $scope.DataArchivo[i].Contacto = $scope.DataArchivo[i].Contacto.toUpperCase() } catch (e) { }
                            }
                             
                            if ($scope.DataArchivo[i].Telefono !== undefined && $scope.DataArchivo[i].Telefono !== '' && $scope.DataArchivo[i].Telefono !== '0') {
                                try { $scope.DataArchivo[i].Telefono = MascaraTelefono($scope.DataArchivo[i].Telefono) } catch (e) { }
                            } 

                            if ($scope.DataArchivo[i].Celular !== undefined && $scope.DataArchivo[i].Celular !== '' && $scope.DataArchivo[i].Celular !== '0') {
                                try { $scope.DataArchivo[i].Celular = MascaraTelefono($scope.DataArchivo[i].Celular) } catch (e) { }
                            }
                          
                            if ($scope.DataArchivo[i].Ubicacion !== undefined && $scope.DataArchivo[i].Ubicacion !== '' && $scope.DataArchivo[i].Ubicacion !== '0') {
                                try { $scope.DataArchivo[i].Ubicacion = $scope.DataArchivo[i].Ubicacion.toUpperCase() } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Longitud !== undefined && $scope.DataArchivo[i].Longitud !== '' && $scope.DataArchivo[i].Longitud !== '0') {
                                try { $scope.DataArchivo[i].Longitud = MascaraNumeroGeneral($scope.DataArchivo[i].Longitud) } catch (e) { }
                            }

                            if ($scope.DataArchivo[i].Latitud !== undefined && $scope.DataArchivo[i].Latitud !== '' && $scope.DataArchivo[i].Latitud !== '0') {
                                try { $scope.DataArchivo[i].Latitud = MascaraNumeroGeneral($scope.DataArchivo[i].Latitud) } catch (e) { }
                            } 
 

                            }
                        if ($scope.totalRegistros > 20) {
                            for (var i = 0; i < 20; i++) {
                                $scope.DataActual.push($scope.DataArchivo[i])
                            }
                        } else {
                            $scope.DataActual = $scope.DataArchivo
                        }
                        if ($scope.DataArchivo.length > 0) { 
                            blockUI.stop();
                        } else {
                            ShowError('El archivo seleccionado no contiene datos para validar, por favor intente con otro archivo')
                            blockUI.stop()
                            $timeout(blockUI.stop(), 1000)

                        }
                    } catch (e) {
                        ShowError('Error en la lectura del archivo por favor intente nuevamente')
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    }


                }, 1000)
            }

        }

        function restrow(item) {
            item.stRow = ''
            item.stCodigo_Alternativo = ''
            item.stNombre  = ''
            item.stCATA_TIPE_Codigo = ''
            item.stTERC_Codigo_Proveedor = ''
            item.stContacto  = ''
            item.stTelefono  = ''
            item.stCelular   = ''
            item.stUbicacion = ''
            item.stLongitud = ''
            item.stLatitud = ''   
            item.smCodigo_Alternativo = ''
            item.smNombre  = ''
            item.smCATA_TIPE_Codigo = ''
            item.smTERC_Codigo_Proveedor = ''
            item.smContacto  = ''
            item.smTelefono  = ''
            item.smCelular   = ''
            item.smUbicacion = ''
            item.smLongitud = ''
            item.smLatitud  = ''  
        }


        $scope.ValidarDocumento = function () {
            $scope.ListaErrores = []
            $scope.MensajesError = []
            var contadorgeneral = 0
            var contadorRegistros = 0
            $scope.DataVerificada = []
            $scope.DataVerificadatmp = [] 
             for (var i = 0; i < $scope.DataArchivo.length; i++) {
                var item = $scope.DataArchivo[i] 
                //resetea todos los estilos de la fila que se va a evaluar
                restrow(item);
                var errores = 0
                //Condicion para saber si aplican las validaciones (Si el registro no tiene una ruta seleccionada no validara el resto de campos) 
                 if (item.Nombre == undefined || item.Nombre == ' ' || item.Nombre == '') {
                    item.stNombre = 'background:red' 
                    item.msNombre = 'Ingrese el nombre del peaje'
                    errores++  
                 } 

                if (item.CATA_TIPE_Codigo == undefined || item.CATA_TIPE_Codigo == '') { 
                    item.stCATA_TIPE_Codigo = 'background:red'
                    item.msCATA_TIPE_Codigo = 'Seleccione el tipo de peaje' 
                    errores++
                }
                if (item.TERC_Codigo_Proveedor == undefined || item.TERC_Codigo_Proveedor == '') {
                    item.stTERC_Codigo_Proveedor = 'background:red' 
                    item.msTERC_Codigo_Proveedor = 'Seleccione el Proveedor' 
                    errores++
                }
                if (item.Contacto == undefined || item.Contacto == '') { 
                    item.stContacto = 'background:red'
                    item.msContacto = 'Ingrese el Nombre del Contacto del peaje' 
                    errores++
                }
                 if (item.Ubicacion == undefined || item.Ubicacion == '') { 
                    item.stUbicacion = 'background:red'
                    item.msUbicacion = 'Ingrese La Ubicación del peaje' 
                    errores++
                }
                if (errores > 0) {
                    item.stRow = 'background:yellow'
                    $scope.ListaErrores.push(item) 
                    contadorgeneral++ 
            
                }
                else {
                    item.stRow = ''
                    $scope.DataVerificada.push(item)
                }

            }  

            if (contadorgeneral > 0) {
                ShowError('Se encontraron inconsistencias en los datos ingresados, por favor corrija los datos marcados.');
                $scope.paginaActualError = 1
                $scope.DataErrorActual = []
                $scope.totalRegistrosErrores = $scope.ListaErrores.length
                $scope.totalPaginasErrores = Math.ceil($scope.totalRegistrosErrores / 10);
                if ($scope.totalRegistrosErrores > 10) {
                    for (var i = 0; i < 10; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    for (var i = 0; i < $scope.ListaErrores.length; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                console.log('Errores',$scope.DataErrorActual) 
            }
            else {
                showModal('modalConfirmacionGuardarPorgramacion')
                $scope.MSJ = 'Los datos se verificaron correctamente. \n  ¿Desea continuar con el cargue de la información?'
            }


        } 
            $scope.Guardar = function () {
                closeModal('modalConfirmacionGuardarPorgramacion') 
                 for (var i = 0; i < $scope.DataVerificada.length; i++) {
                    var item = $scope.DataVerificada[i] 
                    var ObtjetoPeajes = {  
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Nombre: item.Nombre,
                        Codigo_Alterno: item.Codigo_Alternativo,
                        Proveedor: { Codigo: item.TERC_Codigo_Proveedor.Codigo },
                        Contacto: item.Contacto,
                        Telefono: item.Telefono,
                        Celular: item.Celular,
                        Ubicacion: item.Ubicacion,
                        Estado: 1,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Latitud: item.Latitud,
                        Longitud: item.Longitud,
                       TipoPeaje: { Codigo: item.CATA_TIPE_Codigo.Codigo},
                        Codigo:0
                    } 

                    $scope.listaPeajesGuardar.push(ObtjetoPeajes);

                } 
                
                contGeneral = $scope.DataVerificada.length
                GuardarCompleto(0)
            }

        var contGuard = 0
        var contGuardGeneral = 0
        var Guardaros = 0
            function GuardarCompleto(inicio) {
                contGuard++  
                if (contGuard > $scope.listaPeajesGuardar.length) { 
                    ShowSuccess('Se guardaron ' + contGuardGeneral + ' de ' + contGeneral + ' Peajes')
                    closeModal('modalConfirmacionRemplazarProgramacion');
                    closeModal('ModalErrores');
                    $scope.LimpiarData()                
                } else { 
                    blockUI.start();
                    $timeout(function () {
                        blockUI.message('Guardando Peaje ' + (inicio + 1) + ' de ' + $scope.listaPeajesGuardar.length);
                    }, 1000); 
                    PeajesFactory.Guardar($scope.listaPeajesGuardar[inicio]).then(function (response) {
                        if (response.data.Datos > 0) {
                            Guardaros++
                            contGuardGeneral++
                            GuardarCompleto(contGuard)
                            blockUI.stop();
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        $scope.ProgramacionesNoGuardadas.push($scope.listaPeajesGuardar[inicio])
                        GuardarCompleto(contGuard)
                        blockUI.stop();
            
                    }) 
                }
                
            }

        $scope.LimpiarData = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            $scope.listaPeajesGuardar = []
            $scope.DataVerificada = []
            $scope.DataActual = []
            $scope.DataErrorActual = []
            $scope.DataArchivo = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            $scope.VerErrores = false

        }



    }]);