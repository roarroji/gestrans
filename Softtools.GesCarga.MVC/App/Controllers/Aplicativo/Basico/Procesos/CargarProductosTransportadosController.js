﻿SofttoolsApp.controller("CargarProductosTransportadosCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory', 'RutasFactory', 'CiudadesFactory', 'blockUIConfig', 'ProductoTransportadosFactory','UnidadMedidaFactory','UnidadEmpaqueFactory',
    function ($scope, $timeout, $linq, blockUI, ValorCatalogosFactory, RutasFactory, CiudadesFactory, blockUIConfig, ProductoTransportadosFactory, UnidadMedidaFactory, UnidadEmpaqueFactory) {
        console.clear()



        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.continuar = false
        $scope.VerErrores = false;
        $scope.DeshabilitarCliente = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.ListadoRutasPuntosGestion = []
        $scope.modelo = {
            Fecha: new Date(),
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,

        }
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Procesos' }, { Nombre: 'Cargue Productos Transportados' }];
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
       

        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ];
        $scope.ListadoUnidadMedida = [];
        UnidadMedidaFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoUnidadMedida = response.data.Datos;
                        if ($scope.CodigoUnidadMedida !== undefined && $scope.CodigoUnidadMedida !== '' && $scope.CodigoUnidadMedida !== null) {
                            $scope.UnidadMedida = $linq.Enumerable().From($scope.ListadoUnidadMedida).First('$.Codigo ==' + $scope.CodigoUnidadMedida);
                        }
                        else {
                            $scope.UnidadMedida = $scope.ListadoUnidadMedida[$scope.ListadoUnidadMedida.length];
                        }
                    }
                    else {
                        $scope.ListadoUnidadMedida = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.ListadoUnidadEmpaque = [];
        UnidadEmpaqueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoUnidadEmpaque = response.data.Datos;
                        if ($scope.CodigoUnidadEmpaque !== undefined && $scope.CodigoUnidadEmpaque !== '' && $scope.CodigoUnidadEmpaque !== null) {
                            $scope.UnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo ==' + $scope.CodigoUnidadEmpaque);
                        }
                        else {
                            $scope.UnidadEmpaque = $scope.ListadoUnidadEmpaque[$scope.ListadoUnidadEmpaque.length];
                        }
                    }
                    else {
                        $scope.ListadoUnidadEmpaque = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });


        $scope.ListadoLineaProducto = [];
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_LINEA_PRODUCTOS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoLineaProducto = response.data.Datos;
                        if ($scope.CodigoLineaProducto !== undefined && $scope.CodigoLineaProducto !== '' && $scope.CodigoLineaProducto !== null) {
                            $scope.LineaProducto = $linq.Enumerable().From($scope.ListadoLineaProducto).First('$.Codigo ==' + $scope.CodigoLineaProducto);
                        }
                        else {
                            $scope.LineaProducto = $scope.ListadoLineaProducto[$scope.ListadoLineaProducto.length];
                        }
                    }
                    else {
                        $scope.ListadoLineaProducto = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.ListadoNaturalezaProducto = [];
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NATURALEZA_PRODUCTOS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoNaturalezaProducto = response.data.Datos;
                        if ($scope.CodigoNaturalezaProducto !== undefined && $scope.CodigoNaturalezaProducto !== '' && $scope.CodigoNaturalezaProducto !== null) {
                            $scope.NaturalezaProducto = $linq.Enumerable().From($scope.ListadoNaturalezaProducto).First('$.Codigo ==' + $scope.CodigoNaturalezaProducto);
                        }
                        else {
                            $scope.NaturalezaProducto = $scope.ListadoNaturalezaProducto[$scope.ListadoNaturalezaProducto.length];
                        }
                    }
                    else {
                        $scope.ListadoNaturalezaProducto = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.DataActual = []
                $scope.paginaActual = 1
                for (var i = 0; i < 10; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 10
                $scope.DataActual = []
                for (var i = a - 10; i < $scope.DataArchivo.length; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }

        //Paginacion MOdal de errores

        $scope.PrimerPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 10) {
                $scope.DataErrorActual = []
                $scope.paginaActualError = 1
                for (var i = 0; i < 10; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }
        $scope.AnteriorModalError = function () {
            if ($scope.paginaActualError > 1) {
                $scope.paginaActualError -= 1
                var a = $scope.paginaActualError * 10
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteModalError = function () {
            if ($scope.paginaActualError < $scope.totalPaginasErrores) {
                $scope.paginaActualError += 1
                var a = $scope.paginaActualError * 10
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    $scope.UltimaPaginaModalError()

                }
            } else if ($scope.paginaActualError == $scope.totalPaginasErrores) {
                $scope.UltimaPaginaModalError()
            }
        }
        $scope.UltimaPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 10 && $scope.totalPaginasErrores > 1) {
                $scope.paginaActualError = $scope.totalPaginasErrores
                var a = $scope.paginaActualError * 10
                $scope.DataErrorActual = []
                for (var i = a - 10; i < $scope.ListaErrores.length; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 10) {
                $scope.ProgramacionesNoGuardadasActual = []
                $scope.paginaActualNoGuardadas = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }
        $scope.AnteriorNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas -= 1
                var a = $scope.paginaActualNoGuardadas * 10
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas < $scope.totalPaginasNoGuardadas) {
                $scope.paginaActualNoGuardadas += 1
                var a = $scope.paginaActualNoGuardadas * 10
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardados()

                }
            } else if ($scope.paginaActualNoGuardadas == $scope.totalPaginasNoGuardadas) {
                $scope.UltimaPaginaNoGuardados()
            }
        }
        $scope.UltimaPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 10 && $scope.totalPaginasNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas = $scope.totalPaginasNoGuardadas
                var a = $scope.paginaActualNoGuardadas * 10
                $scope.ProgramacionesNoGuardadasActual = []
                for (var i = a - 10; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 10) {
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                $scope.paginaActualNoGuardadasRemplazo = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }
        $scope.AnteriorNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo -= 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo < $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.paginaActualNoGuardadasRemplazo += 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardadosRemplazo()

                }
            } else if ($scope.paginaActualNoGuardadasRemplazo == $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.UltimaPaginaNoGuardadosRemplazo()
            }
        }
        $scope.UltimaPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 10 && $scope.totalPaginasNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo = $scope.totalPaginasNoGuardadasRemplazo
                var a = $scope.paginaActualNoGuardadasRemplazo * 10
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                for (var i = a - 10; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }


        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardaras = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardadasRemplazo = function (item) {
            if ($scope.Option.name == 'Omitido') {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Omitido'
                }
            } else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Remplazado'
                }
            }

        }
        $scope.LimpiarData = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            $scope.DataVerificada = []
            $scope.DataActual = []
            $scope.DataErrorActual = []
            $scope.DataArchivo = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            $scope.VerErrores = false
        }

        /*-----------------------------------------------------------------------funciones de lectura y apertura de archivos----------------------------------------------------------------------------*/

        $scope.ListaProgramaciones = []
        $scope.DataArchivo = []
        var X = XLSX;
        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }
        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }
        function Eval(item) {
            if (item == '0' || item == undefined || item == '') {
                return true
            } else {
                return false
            }
        }
        function AsignarValoresTabla() {
            $scope.NumeroCargue = 0

            if ($scope.DataArchivo.Valores.length > 0) {
                var DataTemporal = []
                for (var i = 0; i < $scope.DataArchivo.Valores.length; i++) {
                    var item = $scope.DataArchivo.Valores[i]

                    if (!Eval(item.Nombre)) {
                        DataTemporal.push(item)
                    }
                }
                $scope.DataArchivo = DataTemporal
                console.log("dataArchivo en proceso: ", $scope.DataArchivo);
                $scope.totalRegistros = ($scope.DataArchivo.length)
                $scope.paginaActual = 1
                $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                blockUI.start();
                blockUI.message('Configurando campos, Esto puede tardar algunos minutos, por favor espere...');
                $timeout(function () {
                    try {
                        var data = []
                        for (var i = 0; i < $scope.DataArchivo.length; i++) {
                            var DataItem = $scope.DataArchivo[i]
                            if (!Eval(DataItem.Nombre)) { try { DataItem.Nombre = MascaraMayus(DataItem.Nombre); } catch (e) { } }

                            if (!Eval(DataItem.Unidad_Medida)) { try { DataItem.Unidad_Medida = parseInt(DataItem.Unidad_Medida, 10); DataItem.Unidad_Medida = $linq.Enumerable().From($scope.ListadoUnidadMedida).First('$.Codigo == ' + DataItem.Unidad_Medida); } catch (e) { } }
                            if (!Eval(DataItem.Unidad_Empaque)) { try { DataItem.Unidad_Empaque = parseInt(DataItem.Unidad_Empaque, 10); DataItem.Unidad_Empaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo == ' + DataItem.Unidad_Empaque); } catch (e) { } }
                            if (!Eval(DataItem.Linea_Producto)) { try { DataItem.Linea_Producto = parseInt(DataItem.Linea_Producto, 10); DataItem.Linea_Producto = $linq.Enumerable().From($scope.ListadoLineaProducto).First('$.Codigo == ' + DataItem.Linea_Producto); } catch (e) { } }
                            if (!Eval(DataItem.Naturaleza_Producto)) { try { DataItem.Naturaleza_Producto = parseInt(DataItem.Naturaleza_Producto, 10); DataItem.Naturaleza_Producto = $linq.Enumerable().From($scope.ListadoNaturalezaProducto).First('$.Codigo == ' + DataItem.Naturaleza_Producto); } catch (e) { } }
                            if (!Eval(DataItem.Aplica_Tarifario)) {
                                try { DataItem.Aplica_Tarifario = parseInt(DataItem.Aplica_Tarifario, 10); if (DataItem.Aplica_Tarifario == 1) { DataItem.Aplica_T = true } else { DataItem.Aplica_T = false} } catch (e) { } }
                            if (!Eval(DataItem.Estado)) { try { DataItem.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + DataItem.Estado); } catch (e) { } } else {
                                DataItem.Estado = $scope.ListadoEstados[0]
                            }
                        }
                        if ($scope.totalRegistros > 10) {
                            for (var i = 0; i < 10; i++) {
                                $scope.DataActual.push($scope.DataArchivo[i])
                            }
                        } else {
                            $scope.DataActual = $scope.DataArchivo
                        }
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    } catch (e) {
                        ShowError('Error en la lectura del archivo por favor intente nuevamente')
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    }


                }, 1000)
            }

        }
        var conterroresprevalidar = 0

        function process_wb(wb) {
            $scope.DataArchivo = []
            $scope.ListaErrores = []
            $scope.DataActual = []
            $scope.DataArchivo = to_json(wb);
            console.log("wb a formato Json(DataArchivo): ", $scope.DataArchivo);

            if ($scope.DataArchivo.Valores != undefined) {
                $scope.DataArchivo = { Valores: $scope.DataArchivo.Valores }
                AsignarValoresTabla()
            }
            else {
                ShowError('El arhivo cargado no corresponde al formato de carge masivo del tarifario')
            }
            blockUI.stop()

        }

        $scope.handleFile = function (e) {
            var files = e.files;
            console.log("file: ",e,"files: ",e.files);
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = $scope.CargarDocumento
                reader.readAsArrayBuffer(f);
            }
        }

        $scope.CargarDocumento = function (e) {
            $scope.$apply(function () {
                var data = e.target.result;
                console.log("e:",e);
                $scope.arr = fixdata(data);
                console.log("arr:",$scope.arr);
                blockUI.start();
                $timeout(function () {
                    blockUI.message('Subiendo Archivo, Por Favor Espere...');
                }, 1000);
                $timeout(function () {
                    try {
                        $scope.wb = X.read(btoa($scope.arr), { type: 'base64' });
                        console.log("wb", $scope.wb);
                        process_wb($scope.wb);
                    } catch (e) {
                        ShowError('El arhivo seleccionado no corresponde a una hoja de cálculo válida, por favor intente con otro archivo')
                        blockUI.stop()

                    }
                }, 1500);
            });
        }

        function restrow(item) {
            item.st = {}
            item.ms = {}
        }

        $scope.ValidarDocumento = function () {
            $scope.ListaErrores = []
            $scope.MensajesError = []
            /*
                        blockUI.start();
                        $timeout(function() {
                            blockUI.message('Validando Archivo');
                        }, 1000);*/
            var contadorgeneral = 0
            var contadorRegistros = 0
            $scope.DataVerificada = []
            $scope.DataVerificadatmp = []
            for (var i = 0; i < $scope.DataArchivo.length; i++) {
                var item = $scope.DataArchivo[i]
                item.ID = i
                //resetea todos los estilos de la fila que se va a evaluar
                restrow(item);
                var errores = 0
                //Condicion para saber si aplican las validaciones (Si el registro no tiene una ruta seleccionada no validara el resto de campos)
                if (item.Omitido !== true) {
                    contadorRegistros++

                    if (item.Nombre === undefined || item.Nombre === '' && item.Nombre === null) {
                        item.st.Nombre = stError; item.ms.Nombre = ('Debe ingresar el nombre del producto');
                        errores++
                    }
                    if (item.Codigo_Ministerio == undefined || item.Codigo_Ministerio == "") {                        
                        errores++
                    }
                    if (item.Descripcion == undefined || item.Descripcion == "") {                        
                        errores++
                    }
                    if (item.Unidad_Medida == undefined || item.Unidad_Medida == "" || item.Unidad_Medida == 0 || item.Unidad_Medida == null || item.Unidad_Medida == "0") {                          
                            errores++                       
                    }
                    if (item.Unidad_Empaque == undefined || item.Unidad_Empaque == "" || item.Unidad_Empaque == 0 || item.Unidad_Empaque == null || item.Unidad_Empaque == "0") {
                        errores++
                    }
                    if (item.Linea_Producto == undefined || item.Linea_Producto == "" || item.Linea_Producto == 0 || item.Linea_Producto == null || item.Linea_Producto == "0") {
                        errores++
                    }
                    if (item.Naturaleza_Producto == undefined || item.Naturaleza_Producto == "" || item.Naturaleza_Producto == 0 || item.Naturaleza_Producto == null || item.Naturaleza_Producto == "0") {
                        errores++
                    }
                    if (errores > 0) {
                        item.st.Row = stwarning
                        $scope.ListaErrores.push(item)
                        contadorgeneral++
                    }
                    else {
                        item.stRow = ''
                        $scope.DataVerificada.push(item)
                    }
                }
                else {
                    restrow(item);
                }
            }
            //blockUI.stop();

            if (contadorgeneral > 0) {
                ShowError('Se encontraron inconsistencias en los datos ingresados, por favor corrija los datos marcados o seleccione omitirlos.');
                $scope.paginaActualError = 1
                $scope.DataErrorActual = []
                $scope.totalRegistrosErrores = $scope.ListaErrores.length
                $scope.totalPaginasErrores = Math.ceil($scope.totalRegistrosErrores / 10);
                if ($scope.totalRegistrosErrores > 10) {
                    for (var i = 0; i < 10; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    for (var i = 0; i < $scope.ListaErrores.length; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                if (contadorRegistros == 0) {
                    ShowError('No se encontraron registros para validar, por favor revise que la casilla de omitir no se encuentre marcada en al menos un registro')
                } else {
                    ShowWarningConfirm('Los datos se verificaron correctamente.¿Desea continuar con el cargue de la información?', $scope.Guardar)
                }
            }


        }

        $scope.Guardar = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        $scope.GuardarErrados = function () {
            //closeModal('ModalErrores')
            data = []
            for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                if (!$scope.ProgramacionesNoGuardadas[i].Omitido && $scope.ProgramacionesNoGuardadas[i].Option != 'Omitido') {
                    if ($scope.ProgramacionesNoGuardadas[i].Option == 'Remplazado') {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 1
                    } else {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 0
                    }
                    data.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
            $scope.DataVerificada = data
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        var contGuard = 0
        var contGuardGeneral = 0
        var contGeneral = 0
        var Guardaros = 0
        function GuardarCompleto(inicio, opt) {
            contGuard++
            if (contGuard > $scope.DataVerificada.length) {

                ShowSuccess('Se guardaron ' + contGuardGeneral + ' de ' + contGeneral + ' Productos')
                closeModal('modalConfirmacionRemplazarProgramacion');
                closeModal('ModalErrores');
                $scope.LimpiarData()

            } else {

                blockUI.start();
                $timeout(function () {
                    blockUI.message('Guardando Productos');
                }, 1000);
                $scope.DatosGuardar = {

                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: 0,
                    CodigoAlterno: $scope.DataVerificada[inicio].Codigo_Ministerio,
                    Nombre: $scope.DataVerificada[inicio].Nombre,
                    Descripcion: $scope.DataVerificada[inicio].Descripcion,
                    UnidadMedida: $scope.DataVerificada[inicio].Unidad_Medida.Codigo,
                    UnidadEmpaque: $scope.DataVerificada[inicio].Unidad_Empaque.Codigo,
                    LineaProducto: $scope.DataVerificada[inicio].Linea_Producto.Codigo,
                    NaturalezaProducto: $scope.DataVerificada[inicio].Naturaleza_Producto.Codigo,
                    Estado: $scope.DataVerificada[inicio].Estado.Codigo,
                    UsuarioCrea: {Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CodigoProductoMinisterio: $scope.DataVerificada[inicio].Codigo_Ministerio,
                    AplicaTarifario: parseInt($scope.DataVerificada[inicio].Aplica_Tarifario,10)
                };
               
                ProductoTransportadosFactory.Guardar($scope.DatosGuardar).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        Guardaros++
                        contGuardGeneral++
                        GuardarCompleto(contGuard)
                        blockUI.stop();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $scope.ProgramacionesNoGuardadas.push($scope.DataVerificada[inicio])
                    GuardarCompleto(contGuard)
                    blockUI.stop();
                })
            }
        }


        $scope.ModalConfirmacionNuevoProceso = function () {
            showModal('modalConfirmacionCancelarProceso');
        }

        $scope.CerrarModalNuevoProceso = function (e) {
            var fileVal = document.getElementById("fileModal");
            fileVal.value = '';
            fileVal = document.getElementById("filePrincipal");
            fileVal.value = '';
            closeModal('modalConfirmacionCancelarProceso');


        }

        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (o) {
            return MascaraNumero(o)
        }
        $scope.MaskDecimalesGrid = function (o) {
            return MascaraDecimales(o)
        }
        $scope.MaskValoresGrid = function (o) {
            return MascaraValores(o)
        }
        $scope.MaskMayusGrid = function (o) {
            return MascaraMayus(o)
        }
        $scope.MaskplacaGrid = function (o) {
            return MascaraPlaca(o)
        }
        $scope.MaskplacaSemiremolqueGrid = function (o) {
            return MascaraPlacaSemirremolque(o)
        }

        $scope.GenerarPlanilla = function () {
            var entidad = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }
            blockUI.start();
            $timeout(function () {
                blockUI.message('Generando Plantilla..');
            }, 1000);
            ProductoTransportadosFactory.GenerarPlantilla(entidad).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    var pdfAsDataUri = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + response.data.Datos.Plantilla;
                    var link = document.createElement('a');
                    link.href = pdfAsDataUri;
                    link.download = "Plantilla_Cargue_Masivo_Productos_Transportados.xlsx";
                    link.click();
                    //window.open(pdfAsDataUri);
                    blockUI.stop();
                    ShowSuccess('La plantilla se generó correctamente')
                }
            }, function (response) {
                blockUI.stop();
                ShowError(response.statusText)
            })

        }
        console.log("data: ", $scope.DataActual, "errores: ", $scope.VerErrores, "dataArchivo: ", $scope.DataArchivo, "dataTemporal: ", $scope.DataTemporal);
        $scope.DataActual = []
        //$scope.DataActual.push({})
    }]);

