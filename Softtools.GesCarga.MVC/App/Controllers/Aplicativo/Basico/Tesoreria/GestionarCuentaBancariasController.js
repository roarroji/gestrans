﻿SofttoolsApp.controller("GestionarCuentaBancariasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'CuentaBancariasFactory', 'ValorCatalogosFactory', 'PlanUnicoCuentasFactory', 'BancosFactory', 'TercerosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, CuentaBancariasFactory, ValorCatalogosFactory, PlanUnicoCuentasFactory, BancosFactory, TercerosFactory) {

    $scope.Titulo = 'GESTIONAR CUENTAS BANCARIAS';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Cuentas Bancarias' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CUENTAS_BANCARIAS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
    $scope.ListadoBancos = [];
    $scope.ListadoCuentasPUC = [];
    $scope.ListadoTipoCuentaBancaria = [];
    $scope.ListadoProveedores = []
    $scope.ListadoConsulta = [];
    $scope.CuentaPUCValida = true
    $scope.ValidarValorBase = false
    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoAlterno: 0,
        Nombre: '',
        Estado: 0
    }

    $scope.AsignarCuentaPUC = function (CuentaPUC) {
        if (CuentaPUC !== undefined || CuentaPUC !== null) {
            if (angular.isObject(CuentaPUC)) {
                $scope.LongitudCuentaPUC = CuentaPUC.length;
                $scope.CuentaPUCValida = true;
            }
            else if (angular.isString(CuentaPUC)) {
                $scope.LongitudCuentaPUC = CuentaPUC.length;
                $scope.CuentaPUCValida = false;
            }
        }
    };
    $scope.ListadoEstados = [
        { Codigo: 0, Nombre: "INACTIVO" },
        { Codigo: 1, Nombre: "ACTIVO" },
    ]

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Obtener();
    }
    else {
        $scope.Modelo.Codigo = 0;
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
    }

    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CUENTA_BANCARIA } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoConsulta = []
                    for (var i = 0; i < response.data.Datos.length; i++) {
                        if (response.data.Datos[i].Codigo !== 403) {
                            $scope.ListadoConsulta.push(response.data.Datos[i])
                        }
                    }
                    for (i = 0; i <= $scope.ListadoConsulta.length - 1; i++) {
                        var Decimal = $scope.ListadoConsulta[i].Codigo / 100
                        if (Decimal - Math.floor(Decimal) > 0) {
                            $scope.ListadoTipoCuentaBancaria.push($scope.ListadoConsulta[i])
                        }
                    }
                    if ($scope.CodigoTipoCuenta !== undefined && $scope.CodigoTipoCuenta !== '' && $scope.CodigoTipoCuenta !== null) {
                        $scope.Modelo.TipoCuentaBancaria = $linq.Enumerable().From($scope.ListadoTipoCuentaBancaria).First('$.Codigo ==' + $scope.CodigoTipoCuenta)
                    }
                    else {
                        $scope.Modelo.TipoCuentaBancaria = $scope.ListadoTipoCuentaBancaria[0]
                    }
                }
                else {
                    $scope.ListadoTipoCuentaBancaria = []
                }
            }
        }, function (response) {
        });

    TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROVEEDOR }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoProveedores = []
                if (response.data.Datos.length > 0) {
                    $scope.ListadoProveedores = response.data.Datos;
                    $scope.ListadoProveedores.push({ NombreCompleto: "", Codigo: 0 })

                    if ($scope.Tercero > 0) {
                        $scope.Modelo.Tercero = $linq.Enumerable().From($scope.ListadoProveedores).First('$.Codigo ==' + $scope.Tercero);
                    } else {
                        $scope.Modelo.Tercero = $scope.ListadoProveedores[$scope.ListadoProveedores.length - 1];
                    }
                }
                else {
                    $scope.ListadoProveedores = [];
                }
            }
        }, function (response) {
            ShowError(response.statusText);
        });
    /*Cargar el combo de Bancos*/
    BancosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                response.data.Datos.forEach(function (item) {
                    if (item.Codigo > 0) {
                        $scope.ListadoBancos.push(item)
                    }
                })
                if ($scope.CodigoBanco !== undefined && $scope.CodigoBanco !== '' && $scope.CodigoBanco !== null) {
                    $scope.Modelo.Banco = $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo ==' + $scope.CodigoBanco)
                }
                else {
                    $scope.Modelo.Banco = $scope.ListadoBancos[0]
                }
            }
            else {
                $scope.ListadoBancos = []

            }
        }, function (response) {
        });
    /*Cargar el combo de cuentas PUC*/
    PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCuentasPUC = response.data.Datos

                    if ($scope.CodigoCuentaPUC !== undefined && $scope.CodigoCuentaPUC !== '' && $scope.CodigoCuentaPUC !== null) {
                        $scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CodigoCuentaPUC)
                    }
                }
                else {
                    $scope.ListadoCuentasPUC = [];
                }
            }
        }, function (response) {
        });
    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function Obtener() {
        blockUI.start('Cargando cuenta bancaria código No. ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando  cuenta bancaria código No.' + $scope.Modelo.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        CuentaBancariasFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    $scope.Modelo.Codigo = response.data.Datos.Codigo;
                    $scope.Modelo.NumeroCuenta = response.data.Datos.NumeroCuenta
                    $scope.Modelo.Nombre = response.data.Datos.Nombre
                    $scope.Tercero = response.data.Datos.Tercero.Codigo
                    $scope.Modelo.SobregiroAutorizado = response.data.Datos.SobregiroAutorizado
                    $scope.Modelo.SaldoActual = response.data.Datos.SaldoActual

                    $scope.MaskValores();
                    if ($scope.ListadoProveedores.length > 0 && $scope.Tercero > 0) {
                        $scope.Modelo.Tercero = $linq.Enumerable().From($scope.ListadoProveedores).First('$.Codigo ==' + $scope.Tercero)
                    }

                    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                    $scope.CodigoTipoCuenta = response.data.Datos.TipoCuentaBancaria.Codigo

                    if ($scope.ListadoTipoCuentaBancaria.length > 0) {
                        $scope.Modelo.TipoCuentaBancaria = $linq.Enumerable().From($scope.ListadoTipoCuentaBancaria).First('$.Codigo ==' + $scope.CodigoTipoCuenta)
                    }

                    $scope.CodigoCuentaPUC = response.data.Datos.CuentaPUC.Codigo

                    if ($scope.ListadoTipoCuentaBancaria.length > 0) {
                        $scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CodigoCuentaPUC)
                    }

                    $scope.CodigoBanco = response.data.Datos.Banco.Codigo

                    if ($scope.ListadoTipoCuentaBancaria.length > 0) {
                        $scope.Modelo.Banco = $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo ==' + $scope.CodigoBanco)
                    }
                }
                else {
                    ShowError('No se logro consultar la cuenta bancaria código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#!ConsultarCuentaBancarias';
                }
            }, function (response) {
                ShowError(response.statusText);
                document.location.href = '#!ConsultarCuentaBancarias';
            });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    $scope.ValidarValor = function () {
        if ($scope.Modelo.ExigeValorBase === true) {
            $scope.ValidarValorBase = true
        }
        else {
            $scope.ValidarValorBase = false
        }
    }

    $scope.ConfirmacionGuardar = function () {
        showModal('modalConfirmacionGuardar');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardar');
        if (DatosRequeridos()) {
            $scope.MaskNumero();
            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

            CuentaBancariasFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo === 0) {
                                ShowSuccess('Se guardó la cuenta bancaria "' + $scope.Modelo.Nombre + '"');
                            }
                            else {
                                ShowSuccess('Se modificó  la cuenta bancaria "' + $scope.Modelo.Nombre + '"');
                            }
                            location.href = '#!ConsultarCuentaBancarias/' + $scope.Modelo.Codigo;
                        }
                    }
                    $scope.MaskValores();
                }, function (response) {
                    ShowError('No se pudo guardar la cuenta bancaria, porfavor contacte al administrador del sistema');
                });
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;

        if ($scope.Modelo.Nombre === undefined || $scope.Modelo.Nombre === '') {
            $scope.MensajesError.push('Debe ingresar el nombre de la cuenta bancaria');
            continuar = false;
        }
        if ($scope.Modelo.NumeroCuenta === undefined || $scope.Modelo.NumeroCuenta === '') {
            $scope.MensajesError.push('Debe ingresar el número de la cuenta bancaria');
            continuar = false;
        }
        if ($scope.Modelo.Banco === undefined || $scope.Modelo.Banco === '' || $scope.Modelo.Banco === null || $scope.Modelo.Banco.Codigo === 0) {
            $scope.MensajesError.push('Debe seleccionar un banco');
            continuar = false;
        }
        if ($scope.Modelo.TipoCuentaBancaria === undefined || $scope.Modelo.TipoCuentaBancaria === '' || $scope.Modelo.TipoCuentaBancaria === null || $scope.Modelo.TipoCuentaBancaria.Codigo === 0) {
            $scope.MensajesError.push('Debe seleccionar un tipo de cuenta bancaria');
            continuar = false;
        }

        if ($scope.Modelo.CuentaPUC === undefined || $scope.Modelo.CuentaPUC === '' || $scope.Modelo.CuentaPUC === null || $scope.Modelo.CuentaPUC.Codigo === 0 || $scope.CuentaPUCValida === false) {
            $scope.MensajesError.push('Debe seleccionar una cuenta PUC');
            continuar = false;
        }
        if ($scope.Modelo.SobregiroAutorizado === undefined || $scope.Modelo.SobregiroAutorizado === '' || $scope.Modelo.SobregiroAutorizado === null) {
            $scope.Modelo.SobregiroAutorizado = 0
        }
        if ($scope.Modelo.SaldoActual === undefined || $scope.Modelo.SaldoActual === '' || $scope.Modelo.SaldoActual === null) {
            $scope.Modelo.SaldoActual = 0
        }
        if ($scope.Modelo.Tercero === undefined || $scope.Modelo.Tercero === '' || $scope.Modelo.Tercero === null || $scope.Modelo.Tercero.Codigo === 0) {
            $scope.MensajesError.push('Debe ingresar un tercero');
            continuar = false;
        }

        return continuar;
    }

    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarCuentaBancarias/' + $scope.Modelo.Codigo;
    };


    $scope.MaskMayus = function () {
        try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Modelo.NumeroCuenta = MascaraNumero($scope.Modelo.NumeroCuenta) } catch (e) { }
    };
    $scope.MaskValores = function () {
        try { $scope.Modelo.SobregiroAutorizado = MascaraValores($scope.Modelo.SobregiroAutorizado) } catch (e) { }
        try { $scope.Modelo.SaldoActual = MascaraValores($scope.Modelo.SaldoActual) } catch (e) { }
    };
}]);