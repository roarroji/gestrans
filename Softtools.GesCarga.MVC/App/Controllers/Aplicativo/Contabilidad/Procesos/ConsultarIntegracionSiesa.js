﻿SofttoolsApp.controller("ConsultarIntegracionSiesaCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'ValorCatalogosFactory', 'integracionSIESAFactory', 'blockUI',
    function ($scope, $timeout, $linq, $routeParams, ValorCatalogosFactory, integracionSIESAFactory, blockUI) {

        $scope.MapaSitio = [{ Nombre: 'Contabilidad' }, { Nombre: 'Procesos' }, { Nombre: 'Integración SIESA' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        var filtros = {};
        $scope.ListadoEstados = [];
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;
        $scope.FechaInicio = new Date();
        $scope.FechaFin = new Date();
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_INTEGRACION_SIESA);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        // -------------------------- Constantes ---------------------------------------------------------------------//

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        /* Obtener parametros*/
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            if ($routeParams.Numero > 0) {
                $scope.Numero = $routeParams.Numero;
                $scope.TipoDocumentoOrigen = { Codigo: 2600 }
                $scope.EstadoInterfazContable = { Codigo: 12001 }
                $scope.Estado = { Codigo: -1 }
                Find();
                $scope.Numero = 0
            }
        }
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
        $scope.ListadoEstados = [
        { Codigo: -1, Nombre: '(TODOS)' },
        { Codigo: 0, Nombre: 'BORRADOR' },
        { Codigo: 1, Nombre: 'DEFINITIVO' },
        { Codigo: 2, Nombre: 'ANULADO' }
        ];

 

        $scope.Estado = $scope.ListadoEstados[0];

        $scope.ListadoEstadoInterfazContable = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'REPORTADO', Codigo: 1 },
            { Nombre: 'PENDIENTE', Codigo: 0 }
        ];
        $scope.EstadoInterfazContable = $scope.ListadoEstadoInterfazContable[0]; 

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 236 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoDocumentoOrigen = [];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoTipoDocumentoOrigen.push(item);
                    }); 
                    $scope.ListadoTipoDocumentoOrigen.splice(0, 1);
                    $scope.TipoDocumentoOrigen = response.data.Datos[1];
                }
            }, function (response) {
                ShowError(response.statusText);
            });
         
        /*Metodo buscar*/
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                    Find()
                    console.log('listado', $scope.ListadoTipoDocumentoOrigen)  
                }
            }
        };

        $scope.InicializarIntentosInterfaz = function (item) {

            item
 
            filtros = {
                Codigo:1,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                NumeroDocumento: item.NumeroDocumento,
                TipoDocumentoOrigen: $scope.TipoDocumentoOrigen
             
            };

            integracionSIESAFactory.Guardar(filtros).
                then(function (response) {
                 
                    if (response.data.Datos === 1) { 
                        ShowSuccess('Se inicializo el Documento ' + item.NumeroDocumento);
                        Find();
                    } else {
                        ShowWarning('No se encuentra el Numero del docummento' + item.NumeroDocumento);
                        Find();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();


        }
        $scope.AnularDocumento = function (item) {

            console.log('ANULAR ',item)

        }

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#GestionarComprobantesContables';
            }
        };

        // Marcar o Desmarcar
        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListadoComprobanteContable.length; i++) {
                    $scope.ListadoComprobanteContable[i].Marcado = true;
                }
            }
            else {
                for (var i = 0; i < $scope.ListadoComprobanteContable.length; i++) {
                    $scope.ListadoComprobanteContable[i].Marcado = false;
                }
            }
        };

        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoComprobanteContable = [];

         

          
                filtros = {
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Numero,
                    FechaInicial: $scope.FechaInicio,
                    FechaFinal: $scope.FechaFin,
                    DocumentoOrigen:$scope.TipoDocumentoOrigen,
                    
                    NumeroDocumento: $scope.NumeroDocumento,
                    Estado: $scope.Estado.Codigo, 
                    InterfazContable: $scope.EstadoInterfazContable.Codigo,
                };
             
           
            console.log('buscando',  filtros)

            blockUI.delay = 1000;
            integracionSIESAFactory.Consultar (filtros).
                then(function (response) {
                    console.log('llegaron', response.data.Datos)
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoComprobanteContable = response.data.Datos;
                            for (var i = 0; i < $scope.ListadoComprobanteContable.length; i++) {
                                if ($scope.ListadoComprobanteContable[i].FechaInterfazContable == "1900-01-01T00:00:00-05:00") {
                                    $scope.ListadoComprobanteContable[i].FechaInterfazContable = '';
                                }
                               
                            }
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.ListadoComprobanteContable = "";
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.NumeroDocumento === null || $scope.NumeroDocumento === undefined || $scope.NumeroDocumento === '' || $scope.NumeroDocumento === 0 || isNaN($scope.NumeroDocumento) === true)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false
            } else if (($scope.NumeroDocumento !== null && $scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '' && $scope.NumeroDocumento !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')
            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }
                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            return continuar
        }
        /*--------------------------------------------------------------------- Anular -----------------------------------------------------------------------------*/
        /*Validacion boton anular */
        $scope.ConfirmacionAnularComprobante = function (numero) {
            $scope.NumeroAnulacion = numero;
            showModal('modalConfirmacionAnularComprobante');
        };
        /*Funcion que solicita los datos de la anulación */
        $scope.SolicitarDatosAnulacion = function () {
            $scope.FechaAnulacion = new Date();
            closeModal('modalConfirmacionAnularComprobante');
            showModal('modalDatosAnularComprobante');
        };
        /*Funcion anulación */
        $scope.Anular = function () {

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.NumeroAnulacion,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.CausaAnulacion,
                OficinaAnula: $scope.Sesion.UsuarioAutenticado.Oficinas[0]
            };

            comprobanteContableFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se anuló el comprobante  No.' + entidad.Numero);
                        closeModal('modalDatosAnularComprobante');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };

    }]);