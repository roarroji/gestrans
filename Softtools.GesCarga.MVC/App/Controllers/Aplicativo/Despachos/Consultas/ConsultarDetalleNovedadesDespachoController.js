﻿SofttoolsApp.controller("ConsultarDetalleNovedadesDespachoCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'NovedadesDespachosFactory', 'DetalleNovedadesDespachosFactory',
    function ($scope, $timeout, $linq, blockUI, NovedadesDespachosFactory, DetalleNovedadesDespachosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Consultas' }, { Nombre: 'Novedades Despachos' }];
        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.MostrarMensajeError = false;
        $scope.FechaInicio = new Date();
        $scope.FechaFin = new Date();
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_DETALLE_NOVEDADES_DESPACHOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };
        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //Cargar combo de novedades despacho
        NovedadesDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '(TODAS)', Codigo: 0 });
                        $scope.ListadoNovedadesDespachos = response.data.Datos;
                        $scope.Novedad = $linq.Enumerable().From($scope.ListadoNovedadesDespachos).First('$.Codigo == 0');
                    } else {
                        $scope.ListadoNovedadesDespachos = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                if (DatosRequeridos()) {
                    Find();
                }
            }
        };

        function Find() {

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                FechaInicial: $scope.NumeroRemesa > 0 ? undefined : $scope.FechaInicio,
                FechaFinal: $scope.NumeroRemesa > 0 ? undefined : $scope.FechaFin,
                NumeroRemesa: $scope.NumeroRemesa,
                Novedad: $scope.Novedad,
                NumeroPlanilla: $scope.NumeroPlanilla,
                NumeroManifiesto: $scope.NumeroManifiesto,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                ConsultaNovedadesDespacho: 1
            };

            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...'); ConsultarDetalleNovedadesDespacho
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoNovedadesDespacho = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {
                    blockUI.delay = 1000;
                    $scope.Pagina = $scope.paginaActual;
                    $scope.RegistrosPagina = 10;
                    DetalleNovedadesDespachosFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoNovedadesDespacho = response.data.Datos;
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                } else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.NumeroRemesa === null || $scope.NumeroRemesa === undefined || $scope.NumeroRemesa === '' || $scope.NumeroRemesa === 0 || isNaN($scope.NumeroRemesa) === true)) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número remesa');
                continuar = false;

            } else if (($scope.NumeroDocumento !== null && $scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '' && $scope.NumeroDocumento !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {

                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {

                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }

                } else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio;
                    } else {
                        $scope.FechaInicio = $scope.FechaFin;
                    }
                }
            }
            return continuar;
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };

    }]);