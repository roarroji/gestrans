﻿SofttoolsApp.controller("ConsultarPlanillaDespachoCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'DetallePlanillaDespachosFactory', 'NovedadesDespachosFactory',
    'DetalleNovedadesDespachosFactory', 'PlanillaDespachosFactory', 'ValorCatalogosFactory', 'blockUI', 'TercerosFactory', 'VehiculosFactory', 'RutasFactory', 'EmpresasFactory', 'LiquidacionesFactory','TipoDocumentosFactory',
    'OficinasFactory','DocumentoComprobantesFactory',
    function ($scope, $routeParams, $timeout, $linq, DetallePlanillaDespachosFactory, NovedadesDespachosFactory, DetalleNovedadesDespachosFactory,
        PlanillaDespachosFactory, ValorCatalogosFactory, blockUI, TercerosFactory, VehiculosFactory, RutasFactory, EmpresasFactory, LiquidacionesFactory, TipoDocumentosFactory,
        OficinasFactory, DocumentoComprobantesFactory) {
        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Documentos' }, { Nombre: 'Planilla Despachos' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.CodigoLiquidacionPlanillaDetalle = 0;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        var filtros = {};
        $scope.ListadoEstados = [];
        $scope.pref = '';
        $scope.MensajesErrorAnula = [];
        $scope.MostrarMensajeError = false
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.Anulado = 0;
        $scope.numeroplanilla = 0;
        $scope.CodigoLiquidacionPlanilla = 0;
        $scope.NumeroFacturaRemesa = 0;
        $scope.Remesa = [];
        $scope.ListadoNovedadesDespachos = [];
        $scope.ListadoNumerosRemesas = [];
        $scope.ListadoOficinas = [];
        $scope.Modal = {};
        var GenerarCxC = 0
        $scope.Novedades = {
            NumeroPlanilla: 0,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        }
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PLANILLA_DESPACHOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.NumeroInternoPlanilla = 0;

        $('#TabPlanillas').show()
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }

        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        $scope.TmpPlanilla;
        $scope.DeshabilitarAgregarNovedad = false;
        $scope.DeshabilitarActualizarTiempos = false;

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        //Cargar combo de novedades despacho
        NovedadesDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListadoNovedadesDespachos = response.data.Datos;
                        $scope.Novedades.Novedad = $scope.ListadoNovedadesDespachos[$scope.ListadoNovedadesDespachos.length - 1];
                        $scope.ListadoOriginalNovedades = $scope.ListadoNovedadesDespachos;
                        $scope.ListadoNovedadesSinDevolucion = $scope.ListadoNovedadesDespachos.filter(item => item.Codigo != 27);
                    } else {
                        $scope.ListadoNovedadesDespachos = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });







        console.log("ListadoSinDevolucion: ", $scope.ListadoNovedadesSinDevolucion);
        /*Cargar el combo de Proveedores*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, CadenaPerfiles: PERFIL_PROVEEDOR }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaProveedores = response.data.Datos;
                        $scope.Novedades.Proveedor = $scope.ListaProveedores[-1];
                    } else {
                        $scope.ListaProveedores = '';
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IMPUESTO_LUGAR } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoRecaudo = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoRecaudo = response.data.Datos;
                        $scope.ModalRecaudo = $scope.ListadoTipoRecaudo[0];
                    }
                    else {
                        $scope.ListadoTipoRecaudo = []
                    }
                }
            }, function (response) {
            });

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_SITIO_REPORTE_SEGUIMIENDO }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSitioReporteSeguimientos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoSitioReporteSeguimientos = response.data.Datos;
                    }
                    else {
                        $scope.ListadoSitioReporteSeguimientos = []
                    }
                }
            }, function (response) {
            });

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIEMPOS_LOGISTICOS_REMESA }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTiemposLogisticos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTiemposLogisticos = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTiemposLogisticos = []
                    }
                }
            }, function (response) {
            });

        $scope.ListadoOficinas = []
        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item)
                        });

                        $scope.ModalOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                $scope.ModalOficina = $scope.ListadoOficinas[0]
            } else {
                ShowError('El usuario no tiene oficinas asociadas')
            }
        }
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

        /*Metodo buscar*/
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                if (DatosRequeridos()) {
                    $scope.Codigo = 0
                    Find()
                }
            }
        };

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarPlanillasDespachos';
            }
        };


        if ($routeParams.Numero > CERO) {
            //$scope.NumeroDocumento = $routeParams.Numero;
            $scope.NumeroInternoPlanilla = $routeParams.Numero;
            FindParam();
        }
        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function FindParam() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            $scope.ListadoPlanillaDespacho = [];

            filtros = {
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                //Numero: $scope.NumeroDocumento,
                NumeroDocumento: $scope.NumeroInternoPlanilla,
                NumeroManifiesto: $scope.NumeroManifiesto,
                FechaInicial: $scope.NumeroDocumento > 0 ? undefined : $scope.FechaInicio,
                FechaFinal: $scope.NumeroDocumento > 0 ? undefined : $scope.FechaFin,
                Conductor: { Nombre: $scope.ModeloConductor },
                Vehiculo: { Placa: $scope.ModeloPlaca },
                Estado: { Codigo: $scope.ModalEstado.Codigo },
                Ruta: { Nombre: $scope.ModeloRuta },
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                AplicaConsultaMaster: 1,
                Oficina: $scope.ModalOficina,
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                PlanillaDespachosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoPlanillaDespacho = response.data.Datos;
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.ListadoPlanillaDespacho = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }


                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        }

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            $scope.ListadoPlanillaDespacho = [];

            filtros = {
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.NumeroDocumento,
                NumeroManifiesto: $scope.NumeroManifiesto,
                FechaInicial: $scope.NumeroDocumento > 0 ? undefined : $scope.FechaInicio,
                FechaFinal: $scope.NumeroDocumento > 0 ? undefined : $scope.FechaFin,
                Conductor: { Nombre: $scope.ModeloConductor },
                Vehiculo: { Placa: $scope.ModeloPlaca },
                Estado: { Codigo: $scope.ModalEstado.Codigo },
                Ruta: { Nombre: $scope.ModeloRuta },
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                AplicaConsultaMaster: 1,
                Oficina: $scope.ModalOficina,
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                PlanillaDespachosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoPlanillaDespacho = response.data.Datos;
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.ListadoPlanillaDespacho = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }


                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------------------------FUNCIONES NUEVA NOVEDAD---------------------------------------------------------------------------------------*/
        //funcion que muestra la modal novedades 
        $scope.AbrirNovedades = function (item) {
            if ($scope.DeshabilitarActualizar == false) {
                if (item.TarifaTransportes.Codigo == 1) {
                    $scope.ListadoNovedadesDespachos = $scope.ListadoOriginalNovedades;
                } else {
                    $scope.ListadoNovedadesDespachos = $scope.ListadoNovedadesSinDevolucion;
                }
                $scope.DeshabilitarAgregarNovedad = $scope.DeshabilitarGuardar(item);
                console.log("Novedades: ", $scope.Novedades, "item: ", item, "ListadoNovedades: ", $scope.ListadoNovedadesDespachos);
                $scope.DeshabilitarNumeroRemesa = true;
                $scope.DeshabilitarValorCliente = true;
                $scope.DeshabilitarValorCompra = true;
                $scope.NumeroDocumentoPlanilla = item.NumeroDocumento
                $scope.Novedades.NumeroPlanilla = item.Numero
                $scope.ResultadoSinRegistrosNovedad = '';

                //Cargar combo de numeros remesas de planilla
                $scope.Remesa = [];
                $scope.ListadoNumerosRemesas = [];
                DetallePlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroPlanilla: $scope.Novedades.NumeroPlanilla }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoNumerosRemesas = response.data.Datos;
                                $scope.Remesa = $scope.ListadoNumerosRemesas[-1];
                                $scope.CodigoLiquidacionPlanillaDetalle = $scope.ListadoNumerosRemesas[0].NumeroLiquidaPlanilla
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });

                //funcion que consulta nueva novedad
                $scope.ListaNovedades = [];
                var filtroNovedades = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroPlanilla: $scope.Novedades.NumeroPlanilla
                }
                DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListaNovedades = response.data.Datos
                            } else {
                                $scope.ResultadoSinRegistrosNovedad = 'No hay datos para mostrar';
                            }
                        }
                    }, function (response) {
                    });
                showModal('modalNovedades');
                $('#NuevaNovedad').hide(); $('#BotonNuevo').show();
            }
        };

        //funcion que validad si la remesa ingresada se encuentra facturada, para permitir o no el ingreso del campo valor cliente
        $scope.VerificarNumeroFactura = function (remesa) {
            if (remesa !== "") {
                if (remesa.NumeroRemesa == undefined || remesa.NumeroRemesa == null || remesa.NumeroRemesa == '') {
                    $scope.Remesa = ''
                } else {
                    if (remesa.NumeroFacturaRemesa > 0) {
                        $scope.Remesa = '';
                        $scope.Novedades.ValorVenta = '';
                        ShowError('El número de remesa ingresado ya se encuentra facturado')
                    }
                }
            }
        }

        //funcion que validad la remesa ingresada
        $scope.CargarValidacionesNovedad = function (novedad) {
            if (novedad.Codigo !== undefined && novedad.Codigo !== 0) {
                $scope.DeshabilitarNumeroRemesa = false;
                $scope.DeshabilitarValorCompra = false;
                $scope.DeshabilitarValorCliente = false;
                $scope.CodigoFacturacion = '';
                $scope.CodigoLiquidacionPlanilla = '';
                filtrosObtener = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: novedad.Codigo,
                };

                blockUI.delay = 1000;
                NovedadesDespachosFactory.Obtener(filtrosObtener).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.CodigoNovedadConsultada = response.data.Datos.Codigo;
                            $scope.CodigoFacturacion = response.data.Datos.Conceptos.COVECodigo
                            $scope.CodigoLiquidacionPlanilla = response.data.Datos.Conceptos.CLPDCodigo
                        }
                        if ($scope.CodigoLiquidacionPlanilla != 0 && $scope.CodigoFacturacion != 0) {
                            $scope.DeshabilitarNumeroRemesa = false;
                            $scope.DeshabilitarValorCompra = false;
                            $scope.DeshabilitarValorCliente = false;
                        } else {
                            if ($scope.CodigoLiquidacionPlanilla != 0) {
                                $scope.DeshabilitarValorCompra = false;
                            } else {
                                $scope.DeshabilitarValorCompra = true;
                            }
                            if ($scope.CodigoFacturacion != 0) {
                                $scope.DeshabilitarValorCliente = false;
                            } else {
                                $scope.DeshabilitarValorCliente = true;
                            }
                            $scope.DeshabilitarNumeroRemesa = false;
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                blockUI.stop();
            } else {
                $scope.DeshabilitarNumeroRemesa = true;
                $scope.DeshabilitarValorCompra = true;
                $scope.DeshabilitarValorCliente = true;
            }
        }

        //funcion que inserta nueva novedad 
        $scope.InsertarNovedad = function () {
            $scope.ResultadoSinRegistrosNovedad = 'No hay datos para mostrar';
            if (DatosRequeridosModal()) {
                if ($scope.Remesa !== undefined && $scope.Remesa !== '' && $scope.Remesa !== null) {
                    $scope.Novedades.NumeroRemesa = $scope.Remesa.NumeroRemesa
                }
                console.log("Novedades: ", $scope.Novedades, "TipoNovedad: ", $scope.Novedad);
                DetalleNovedadesDespachosFactory.Guardar($scope.Novedades).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $('#NuevaNovedad').hide(500); $('#BotonNuevo').show(500)
                            ShowSuccess('Novedad agregada satisfactoriamente')
                            $scope.Remesa = $scope.ListadoNumerosRemesas[-1];
                            $scope.Novedades.Novedad = $scope.ListadoNovedadesDespachos[$scope.ListadoNovedadesDespachos.length - 1];

                            //funcion que consulta nueva novedad
                            $scope.ListaNovedades = [];
                            var filtroNovedades = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroPlanilla: $scope.Novedades.NumeroPlanilla
                            }
                            DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            $scope.ListaNovedades = response.data.Datos
                                        } else {
                                            $scope.ResultadoSinRegistrosNovedad = 'No hay datos para mostrar';
                                        }
                                    }
                                }, function (response) {
                                });
                            showModal('modalNovedades')
                            $('#NuevaNovedad').hide(); $('#BotonNuevo').show()
                        }
                    }, function (response) {
                    });
            }
        }

        function DatosRequeridosModal() {
            $scope.MensajesErrorModal = [];
            var continuar = true;
            if ($scope.Novedades.Novedad === undefined || $scope.Novedades.Novedad === '' || $scope.Novedades.Novedad === null || $scope.Novedades.Novedad.Codigo === 0) {
                $scope.MensajesErrorModal.push('Debe seleccionar la novedad');
                continuar = false;
            } else {
                if ($scope.CodigoLiquidacionPlanilla > 0 && $scope.CodigoFacturacion > 0) {
                    if (($scope.Novedades.ValorCompra === undefined || $scope.Novedades.ValorCompra === '' || $scope.Novedades.ValorCompra === null || $scope.Novedades.ValorCompra === 0) &&
                        ($scope.Novedades.ValorVenta === undefined || $scope.Novedades.ValorVenta === '' || $scope.Novedades.ValorVenta === null || $scope.Novedades.ValorVenta === 0)) {
                        $scope.MensajesErrorModal.push('Debe ingresar el valor transportador o el valor cliente');
                        continuar = false;
                    }
                } else {
                    if ($scope.CodigoLiquidacionPlanillaDetalle === 0) {
                        if ($scope.CodigoLiquidacionPlanilla > 0) {
                            if ($scope.Novedades.ValorCompra === undefined || $scope.Novedades.ValorCompra === '' || $scope.Novedades.ValorCompra === null || $scope.Novedades.ValorCompra === 0) {
                                $scope.MensajesErrorModal.push('Debe ingresar el valor transportador');
                                continuar = false;
                            }
                        }
                    }
                    if ($scope.CodigoFacturacion > 0) {
                        if ($scope.Remesa.NumeroFacturaRemesa === 0 || $scope.Remesa.NumeroFacturaRemesa === undefined) {
                            if ($scope.Remesa === undefined || $scope.Remesa === '' || $scope.Remesa === null || $scope.Remesa.NumeroRemesa === 0 || $scope.Remesa.NumeroRemesa === undefined || $scope.Remesa.NumeroRemesa === null) {
                                $scope.MensajesErrorModal.push('Debe ingresar el número de la remesa');
                                continuar = false;
                            }
                            if ($scope.Novedades.ValorVenta === undefined || $scope.Novedades.ValorVenta === '' || $scope.Novedades.ValorVenta === null || $scope.Novedades.ValorVenta === 0) {
                                $scope.MensajesErrorModal.push('Debe ingresar el valor cliente');
                                continuar = false;
                            }
                            if ($scope.Novedades.Proveedor.Codigo > 0) {
                                if ($scope.Novedades.ValorCosto > $scope.Novedades.ValorVenta) {
                                    $scope.MensajesErrorModal.push('El valor costo debe ser menor o igual al valor cliente');
                                    continuar = false;
                                }
                            }
                        }
                    }
                }
                if ($scope.Novedades.Proveedor.Codigo > 0) {
                    if ($scope.Novedades.ValorCosto === undefined || $scope.Novedades.ValorCosto === '' || $scope.Novedades.ValorCosto === null || $scope.Novedades.ValorCosto === 0) {
                        $scope.MensajesErrorModal.push('Debe ingresar el valor costo');
                        continuar = false;
                    }
                }
                if ($scope.DeshabilitarValorCompra === true && $scope.DeshabilitarValorCliente === true) {
                    $scope.MensajesErrorModal.push('No se pueden ingresar novedades');
                    continuar = false;
                }
            }
            return continuar;
        }

        //funcion que anula nueva novedad
        $scope.AnularNovedad = function (item) {
            $scope.CausaAnulacion = '';
            $scope.nombrenovedad = item.Novedad.Nombre
            $scope.numeroplanilla = item.NumeroPlanilla
            $scope.Modal.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            $scope.Modal.Codigo = item.Codigo
            $scope.FechaAnulacion = new Date()
            $scope.Modal.UsuarioAnula = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            showModal('modalDatosAnular')
        }

        $scope.AnularNovedades = function (causa) {
            if (causa === '' || causa === undefined || causa === null) {
                ShowError('Debe ingresar la causa de anulación de la novedad')
            }
            else {
                $scope.Modal.CausaAnulacion = causa
                DetalleNovedadesDespachosFactory.Anular($scope.Modal).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anulo la novedad.' + $scope.Modal.Codigo + ' - ' + $scope.nombrenovedad);
                            closeModal('modalDatosAnular');
                            Find();
                            $scope.ListaNovedades = [];
                            var filtroNovedades = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroPlanilla: $scope.numeroplanilla
                            }
                            DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            $scope.ListaNovedades = response.data.Datos
                                        }
                                    }
                                }, function (response) {
                                });
                            showModal('modalNovedades')
                            $('#NuevaNovedad').hide(); $('#BotonNuevo').show()
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }


        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.DeshabilitarGuardar = function (item) {
            var DesabilitarBotonGuardar = false;

            if ($scope.DeshabilitarActualizar) {
                DesabilitarBotonGuardar = true;
            }

            if (item.Cumplido.Numero > 0) {
                DesabilitarBotonGuardar = true;
            }

            var FiltroPlanillafiltros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroPlanilla: item.NumeroDocumento,
                Estado: ESTADO_DEFINITIVO,
                Aprobado: -1,
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO }

            };
            LiquidacionesFactory.Consultar(FiltroPlanillafiltros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            //--Liquidaciones Asociadas
                            $scope.DeshabilitarAgregarNovedad = true;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            return DesabilitarBotonGuardar;
        }



        /* Obtener parametros*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }

        $scope.CerrarModal = function () {
            closeModal('modalAnularImpuesto');
            closeModal('modalMensajeAnuloImpuesto');
        }

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });

        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            if ($scope.DeshabilitarImprimir == false) {
                $scope.urlASP = '';
                $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
                $scope.NumeroReporte = Numero;
                $scope.NombreReporte = NOMBRE_REPORTE_PLDC;

                //Arma el filtro
                $scope.ArmarFiltro();

                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf == 1) {
                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
                }
                if (OpcionEXCEL == 1) {
                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
                }

                //Se limpia el filtro para poder hacer mas consultas
                $scope.FiltroArmado = '';
            }
        };

        $scope.DesplegarInformePoliza = function (Numero, OpcionPDf, OpcionEXCEL) {
            if ($scope.DeshabilitarImprimir == false) {
                $scope.urlASP = '';
                $scope.NumeroReporte = Numero;
                $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

                $scope.NombreReporte = NOMBRE_REPORTE_PLANILLA_POLIZA;
                //Arma el filtro
                $scope.ArmarFiltro();
                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf == 1) {
                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
                }
                if (OpcionEXCEL == 1) {
                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Usuario=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
                }

                //Se limpia el filtro para poder hacer mas consultas
                $scope.FiltroArmado = '';
            }
        };

        $scope.ArmarFiltro = function () {
            if ($scope.NumeroReporte != undefined && $scope.NumeroReporte != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroReporte;
            }
        };
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.NumeroDocumento === null || $scope.NumeroDocumento === undefined || $scope.NumeroDocumento === '' || $scope.NumeroDocumento === 0 || isNaN($scope.NumeroDocumento) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.NumeroDocumento !== null && $scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '' && $scope.NumeroDocumento !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            return continuar
        }

        /*---------------------------------------------------------------------------------------------------Máscaras-----------------------------------------------------------------------------*/

        $scope.MaskNumero = function () {
            $scope.CodigoInicial = MascaraNumero($scope.CodigoInicial)
            $scope.CodigoFinal = MascaraNumero($scope.CodigoFinal)
            $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };
        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }
        $scope.AnularDocumento = function (Numero, NumeroDocumento,item) {
            if ($scope.DeshabilitarEliminarAnular == false) {
                $scope.Numero = Numero
                $scope.ModalErrorCompleto = ''
                $scope.NumeroDocumento = NumeroDocumento
                $scope.ModalError = ''
                $scope.MostrarMensajeError = false
                $scope.Vehiculo = item.Vehiculo.Codigo
                showModal('modalAnular');
            }
        };

        $scope.ValidarComprobantes = function () {
           
           // $scope.Anular(entidad)
            if ($scope.Sesion.UsuarioAutenticado.CxCAnulacionPlanillaDespacho == true) {
                GenerarCxC = 0
                var filtroComprobantesPendientes = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                    NumeroDocumentoOrigen: $scope.NumeroDocumento,
                    Estado: ESTADO_ACTIVO,
                    Sync: true
                }

                var ComprobantesEgresoPendientes = DocumentoComprobantesFactory.Consultar(filtroComprobantesPendientes)

                if (ComprobantesEgresoPendientes != undefined) {
                    if (ComprobantesEgresoPendientes.Datos.length > 0) {
                        var Total = 0
                        ComprobantesEgresoPendientes.Datos.forEach(item => {
                            Total += item.ValorPagoTotal
                        })
                        var TerceroDestino = ''
                        $scope.TerceroDestino = ''
                        $scope.Total = MascaraValores(Total)
                        var ResponseVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Vehiculo, Sync: true }).Datos;
                        if (ResponseVehiculo.TipoDueno.Codigo == 2101/*Tercero*/) {
                            TerceroDestino = 'tenedor'
                            $scope.TerceroDestino = 'tenedor'
                        } else {
                            TerceroDestino = 'conductor'
                            $scope.TerceroDestino = 'conductor'
                        }
                        showModal('ModalConfirmacionCxC')
                        //ShowConfirm('La planilla No. ' + $scope.NumeroDocumento + ' tiene Comprobantes de Egresos relacionados con anticipos por un total de $' + MascaraValores(Total) + '. Desea generar la(s) cuenta(s) por cobrar al ' + TerceroDestino+'?', function () { GenerarCxC = ESTADO_ACTIVO; Anular() }, function () { GenerarCxC = ESTADO_INACTIVO; Anular() });
                    } else {
                        Anular();
                    }
                } else {
                    Anular();
                }
            } else {
                Anular();
            }
        }

        $scope.Anular = function () {
            GenerarCxC = ESTADO_ACTIVO
            Anular();
        }

        function Anular() {
            var aplicaLiquidaciones = false
            var aplicaCumplidos = false
            var aplicaComprobante = false
            var ResponseTipoDocumentoPlanilla = TipoDocumentosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Sync: true }).Datos
           
            if (DatosRequeridosAnular()) {
              
                var entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CausaAnulacion: $scope.ModeloCausaAnula,
                    Numero: $scope.Numero,
                    Estado: { Codigo: GenerarCxC },
                    GeneraComprobanteContable: ResponseTipoDocumentoPlanilla.GeneraComprobanteContable
                };

                PlanillaDespachosFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado > 0) {
                                ShowSuccess('Se anuló la planilla ' + $scope.NumeroDocumento);
                                closeModal('ModalConfirmacionCxC');
                                closeModal('modalAnular');
                                Find();
                            } else {
                                var mensaje = 'No se pudo anular la planilla ya que se encuentra relacionado con los siguientes documentos: '
                                var DocumentosExistentes = 0
                                var Liquidaciones = '\nLiquidaciones: '
                                var Cumplidos = '\nCumplidos: '
                                var Comprobante = '\nComprobantes: '
                                for (var i = 0; i < response.data.Datos.DocumentosRelacionados.length; i++) {
                                    if (response.data.Datos.DocumentosRelacionados[i].Cumplido.Numero > 0) {
                                        Cumplidos += response.data.Datos.DocumentosRelacionados[i].Cumplido.Numero + ','
                                        aplicaCumplidos = true
                                        DocumentosExistentes++;
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Liquidacion.Numero > 0) {
                                        Liquidaciones += response.data.Datos.DocumentosRelacionados[i].Liquidacion.Numero + ','
                                        aplicaLiquidaciones = true
                                        DocumentosExistentes++;
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Comprobante.Numero > 0) {
                                        Comprobante += response.data.Datos.DocumentosRelacionados[i].Comprobante.Numero + ','
                                        aplicaComprobante = true
                                        DocumentosExistentes++;
                                    }


                                }

                                if (aplicaLiquidaciones) {
                                    mensaje += Liquidaciones
                                }
                                if (aplicaCumplidos) {
                                    mensaje += Cumplidos
                                }
                                if (aplicaComprobante) {
                                    mensaje += Comprobante
                                }
                                if (DocumentosExistentes == 0) {
                                    ShowSuccess('Se anuló la planilla ' + $scope.NumeroDocumento);
                                    closeModal('modalAnular');
                                    Find();
                                } else {
                                    ShowError(mensaje)
                                }
                            }

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = ''
                        showModal('modalMensajeAnularOrdenCargue');
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede anular la orden de servicio ' + $scope.NumeroDocumento;
                        $scope.ModalErrorCompleto = response.statusText
                    });
            }


        };

        function GenerarCxC() {
            var RegistroCompleto = $linq.Enumerable().From($scope.ListadoPlanillaDespacho).First('$.NumeroDocumento ==' + $scope.NumeroDocumento)
            var CuentaPorCobrar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR,
                CodigoAlterno: '',
                Fecha: new Date(),
                Tercero: { Codigo: RegistroCompleto.Conductor.Codigo },
                DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO },
                FechaDocumento: new Date(),
                FechaVenceDocumento: new Date(),
                Numeracion: '',
                CuentaPuc: { Codigo: CERO },
                ValorTotal: $scope.ListadoConductoresLegalizacion[K].SaldoEmpresa,
                Abono: 0,
                Saldo: $scope.ListadoConductoresLegalizacion[K].SaldoEmpresa,
                FechaCancelacionPago: $scope.Modelo.Fecha,
                Aprobado: ESTADO_ACTIVO,
                UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                FechaAprobo: $scope.Modelo.Fecha,
                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                Vehiculo: { Codigo: 0 },
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }

            }
        }

        $scope.ShowList = function (item) {
            if (item.show == true) {
                item.show = false
            } else {
                item.show = true
            }
        }
        $scope.AbrirTiempos = function (planilla) {
            if ($scope.DeshabilitarActualizar == false) {
                $scope.Planillatemp = planilla
                $scope.DeshabilitarActualizarTiempos = $scope.DeshabilitarGuardar(planilla);

                //$scope.Planillatemp.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                $scope.ListadoTiemposReporte = []
                $scope.ListadoTiemposRemesas = []
                $scope.ListadoTiemposReporte = $linq.Enumerable().From($scope.ListadoSitioReporteSeguimientos).Where(function (x) { return x.CampoAuxiliar2 > 0 }).OrderBy(function (x) { return x.CampoAuxiliar2 }).Select(function (x) { return x }).ToArray();
                $scope.ListadoTiemposLogisticosRemesas = $linq.Enumerable().From($scope.ListadoTiemposLogisticos).Where(function (x) { return x.CampoAuxiliar2 > 0 }).OrderBy(function (x) { return x.CampoAuxiliar2 }).Select(function (x) { return x }).ToArray();
                for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                    $scope.ListadoTiemposReporte[i].Fecha = undefined
                }
                PlanillaDespachosFactory.Obtener_Detalle_Tiempos($scope.Planillatemp).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                                for (var j = 0; j < response.data.Datos.length; j++) {
                                    if ($scope.ListadoTiemposReporte[i].Codigo == response.data.Datos[j].SitioReporteSeguimientoVehicular.Codigo) {
                                        $scope.ListadoTiemposReporte[i].Fecha = new Date(response.data.Datos[j].FechaHora)
                                    }
                                }
                            }
                        }
                    }
                });
                PlanillaDespachosFactory.Obtener_Detalle_Tiempos_Remesas($scope.Planillatemp).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTiemposRemesas = []
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                if ($scope.ListadoTiemposRemesas.length == 0) {
                                    for (var j = 0; j < $scope.ListadoTiemposLogisticosRemesas.length; j++) {
                                        var ObjetoRemesaXtiempos = angular.copy($scope.ListadoTiemposLogisticosRemesas[j])
                                        ObjetoRemesaXtiempos.Numero = response.data.Datos[i].NumeroDocumentoRemesa
                                        ObjetoRemesaXtiempos.CodigoRemesa = response.data.Datos[i].NumeroRemesa
                                        $scope.ListadoTiemposRemesas.push(ObjetoRemesaXtiempos)
                                    }
                                } else {
                                    var Count = 0
                                    for (var k = 0; k < $scope.ListadoTiemposRemesas.length; k++) {
                                        if ($scope.ListadoTiemposRemesas[k].CodigoRemesa == response.data.Datos[i].NumeroRemesa) {
                                            Count++
                                            break;
                                        }
                                    }
                                    if (Count == 0) {
                                        for (var j = 0; j < $scope.ListadoTiemposLogisticosRemesas.length; j++) {
                                            var ObjetoRemesaXtiempos = angular.copy($scope.ListadoTiemposLogisticosRemesas[j])
                                            ObjetoRemesaXtiempos.Numero = response.data.Datos[i].NumeroDocumentoRemesa
                                            ObjetoRemesaXtiempos.CodigoRemesa = response.data.Datos[i].NumeroRemesa
                                            $scope.ListadoTiemposRemesas.push(ObjetoRemesaXtiempos)
                                        }
                                    }
                                }
                            }
                            $scope.ListadoTiemposRemesas = AgruparListados($scope.ListadoTiemposRemesas, 'Numero');

                            for (var j = 0; j < response.data.Datos.length; j++) {
                                for (var i = 0; i < $scope.ListadoTiemposRemesas.length; i++) {
                                    if ($scope.ListadoTiemposRemesas[i].Numero == response.data.Datos[j].NumeroDocumentoRemesa) {
                                        for (var k = 0; k < $scope.ListadoTiemposRemesas[i].Data.length; k++) {
                                            if ($scope.ListadoTiemposRemesas[i].Data[k].Codigo == response.data.Datos[j].SitioReporteSeguimientoVehicular.Codigo && $scope.ListadoTiemposRemesas[i].Data[k].CodigoRemesa == response.data.Datos[j].NumeroRemesa) {
                                                $scope.ListadoTiemposRemesas[i].Data[k].Fecha = new Date(response.data.Datos[j].FechaHora)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
                showModal('modalTiempos');
            }
        };
        $scope.GuardarTiempos = function () {
            var MilisegundoXdia = 86400000
            var MilisegundoXminuto = 60000
            var fechaPlanilla = new Date($scope.Planillatemp.Fecha)
            $scope.MensajesErrorModalTiempos = []
            for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                var item = $scope.ListadoTiemposReporte[i]
                //Condicionales Basicos de proceso
                if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (item.Fecha >= new Date()) {
                        $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' no puede ser mayor a la fecha actual')
                    }
                    if (i > 0) {
                        if (item.Fecha < $scope.ListadoTiemposReporte[i - 1].Fecha) {
                            $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' no puede ser menor a la fecha de ' + $scope.ListadoTiemposReporte[i - 1].Nombre)
                        }
                    }
                }

                //Condicionales Logisticos
                //---------------------------------------------Salida Cargue-----------------------------------------

                if (item.Codigo == 8212 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    for (var j = 0; j < $scope.ListadoTiemposReporte.length; j++) {
                        var item2 = $scope.ListadoTiemposReporte[j]
                        if (item2.Codigo == 8203) { //Llegada cargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue no puede ser mayor de 3 dias a la fecha de llegada al cargue')
                                }
                            }
                        }
                        if (item2.Codigo == 8211) { //Ingreso cargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue debe ser mayor a 15 minutos de la fecha de ingreso cargue')
                                }
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Fin Cargue-----------------------------------------

                if (item.Codigo == 8205 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    for (var j = 0; j < $scope.ListadoTiemposReporte.length; j++) {
                        var item2 = $scope.ListadoTiemposReporte[j]
                        if (item2.Codigo == 8204) { //Inicio cargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue debe ser mayor a 15 minutos de la fecha de inicio cargue')
                                }
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Ingreso Cargue-----------------------------------------

                if (item.Codigo == 8211 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de ingreso cargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Llegada Cargue-----------------------------------------

                if (item.Codigo == 8203 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de llegada cargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------LLegada Descargue-----------------------------------------

                if (item.Codigo == 8207 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    for (var j = 0; j < $scope.ListadoTiemposReporte.length; j++) {
                        var item2 = $scope.ListadoTiemposReporte[j]
                        if (item2.Codigo == 8212) { //Salida cargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue no puede ser mayor a 15 dias de la fecha de salida cargue')
                                } else if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 30) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue debe ser mayor a 30 minutos de la fecha de salida cargue')
                                }
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de llegada descargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Ingreso Descargue-----------------------------------------

                if (item.Codigo == 8213 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de ingreso descargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Salida Descargue-----------------------------------------

                if (item.Codigo == 8214 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    if (item2.Codigo == 8207) { //Llegada descargue
                        if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                            if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                                $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue no puede ser mayor de 3 dias a la fecha de llegada descargue')
                            }
                        }
                    }
                    if (item2.Codigo == 8213) { //Ingreso descargue
                        if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                            if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue debe ser mayor a 15 minutos de la fecha de ingreso descargue')
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de salida descargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

                //---------------------------------------------Fin Descargue-----------------------------------------

                if (item.Codigo == 8209 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                    for (var j = 0; j < $scope.ListadoTiemposReporte.length; j++) {
                        var item2 = $scope.ListadoTiemposReporte[j]
                        if (item2.Codigo == 8208) { //Inicio descargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue debe ser mayor a 15 minutos de la fecha de inicio descargue')
                                }
                            }
                        }
                    }
                    if (!(item.Fecha >= new Date())) {
                        if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                            $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue no puede tener una antiguedad mayor a 100 dias')
                        }
                    }
                }

            }
            for (var j = 0; j < $scope.ListadoTiemposRemesas.length; j++) {
                for (var i = 0; i < $scope.ListadoTiemposRemesas[j].Data.length; i++) {
                    var item = $scope.ListadoTiemposRemesas[j].Data[i]
                    //Condicionales Basicos de proceso
                    if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (item.Fecha >= new Date()) {
                            $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor a la fecha actual')
                        }
                        if (i > 0) {
                            if (item.Fecha < $scope.ListadoTiemposRemesas[j].Data[i - 1].Fecha) {
                                $scope.MensajesErrorModalTiempos.push('La fecha de ' + item.Nombre + ' de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser menor a la fecha de ' + $scope.ListadoTiemposRemesas[j].Data[i - 1].Nombre)
                            }
                        }
                    }

                    //Condicionales Logisticos
                    //---------------------------------------------Salida Cargue-----------------------------------------

                    if (item.Codigo == 20205 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                            var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                            if (item2.Codigo == 20201) { //Llegada cargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor de 3 dias a la fecha de llegada al cargue')
                                    }
                                }
                            }
                            if (item2.Codigo == 20202) { //Ingreso cargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + '  debe ser mayor a 15 minutos de la fecha de ingreso cargue')
                                    }
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de salida de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Fin Cargue-----------------------------------------

                    if (item.Codigo == 20204 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                            var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                            if (item2.Codigo == 20203) { //Inicio cargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de inicio cargue')
                                    }
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de fin de cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Ingreso Cargue-----------------------------------------

                    if (item.Codigo == 20202 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de ingreso cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Llegada Cargue-----------------------------------------

                    if (item.Codigo == 20201 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de llegada cargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------LLegada Descargue-----------------------------------------

                    if (item.Codigo == 20206 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                            var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                            if (item2.Codigo == 20205) { //Salida cargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor a 15 dias de la fecha de salida cargue')
                                    } else if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 30) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de llegada de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 30 minutos de la fecha de salida cargue')
                                    }
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de llegada descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Ingreso Descargue-----------------------------------------

                    if (item.Codigo == 20207 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de ingreso descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Salida Descargue-----------------------------------------

                    if (item.Codigo == 20210 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        if (item2.Codigo == 20206) { //Llegada descargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) > (MilisegundoXdia * 3) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede ser mayor de 3 dias a la fecha de llegada descargue')
                                }
                            }
                        }
                        if (item2.Codigo == 20207) { //Ingreso descargue
                            if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                    $scope.MensajesErrorModalTiempos.push('La fecha de salida de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de ingreso descargue')
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de salida descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                    //---------------------------------------------Fin Descargue-----------------------------------------

                    if (item.Codigo == 20209 && item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        for (var k = 0; k < $scope.ListadoTiemposRemesas[j].Data.length; k++) {
                            var item2 = $scope.ListadoTiemposRemesas[j].Data[k]
                            if (item2.Codigo == 20208) { //Inicio descargue
                                if (item2.Fecha !== undefined && item2.Fecha !== null && item2.Fecha !== '') {
                                    if ((item.Fecha - item2.Fecha) < (MilisegundoXminuto * 15) && !(item2.Fecha > item.Fecha)) {
                                        $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' debe ser mayor a 15 minutos de la fecha de inicio descargue')
                                    }
                                }
                            }
                        }
                        if (!(item.Fecha >= new Date())) {
                            if ((fechaPlanilla - item.Fecha) > (MilisegundoXdia * 100)) { ///Exeptuar cuando esta guardada
                                $scope.MensajesErrorModalTiempos.push('La fecha de fin de descargue de la remesa ' + $scope.ListadoTiemposRemesas[j].Numero + ' no puede tener una antiguedad mayor a 100 dias')
                            }
                        }
                    }

                }
            }
            if ($scope.MensajesErrorModalTiempos.length == 0) {
                $scope.Planillatemp.DetalleTiempos = []
                for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                    var item = $scope.ListadoTiemposReporte[i]
                    if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                        $scope.Planillatemp.DetalleTiempos.push({
                            SitioReporteSeguimientoVehicular: { Codigo: item.Codigo },
                            FechaHora: item.Fecha
                        })
                    }
                }
                $scope.Planillatemp.DetalleTiemposRemesa = []
                for (var j = 0; j < $scope.ListadoTiemposRemesas.length; j++) {
                    for (var i = 0; i < $scope.ListadoTiemposRemesas[j].Data.length; i++) {
                        var item = $scope.ListadoTiemposRemesas[j].Data[i]
                        if (item.Fecha !== undefined && item.Fecha !== null && item.Fecha !== '') {
                            $scope.Planillatemp.DetalleTiemposRemesa.push({
                                SitioReporteSeguimientoVehicular: { Codigo: item.Codigo },
                                FechaHora: item.Fecha,
                                NumeroRemesa: item.CodigoRemesa
                            })
                        }
                    }
                }
                if ($scope.Planillatemp.DetalleTiempos.length == 0 && $scope.Planillatemp.DetalleTiemposRemesa.length == 0) {
                    ShowError('Debe ingresar al menos una fecha')
                }
                else {
                    $scope.Planillatemp.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    PlanillaDespachosFactory.Insertar_Detalle_Tiempos($scope.Planillatemp).then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos > 0) {
                                ShowSuccess('Las fechas se guardaron correctamente')
                                closeModal('modalTiempos')
                            }
                        }
                    });
                }
            }

        }
        $scope.ReplicarTiemposRemesas = function () {
            for (var i = 0; i < $scope.ListadoTiemposReporte.length; i++) {
                for (var j = 0; j < $scope.ListadoTiemposRemesas.length; j++) {
                    $scope.ListadoTiemposRemesas[j].Data[i].Fecha = angular.copy($scope.ListadoTiemposReporte[i].Fecha)
                }
            }
            ShowSuccess('Los tiempos se replicaron a todas las remesas correctamente ')
        }
        $scope.Replicarplanilla = function (item) {
            $scope.TempItem = item
        }
        $scope.ReplicarTiemposPlanilla = function () {
            var item = $scope.TempItem
            for (var i = 0; i < item.Data.length; i++) {
                $scope.ListadoTiemposReporte[i].Fecha = angular.copy(item.Data[i].Fecha)
            }
            ShowSuccess('Los tiempos se replicaron a la planilla correctamente ')
        }

    }]);