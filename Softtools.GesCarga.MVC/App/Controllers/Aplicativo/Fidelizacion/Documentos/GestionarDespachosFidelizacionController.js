﻿SofttoolsApp.controller("GestionarDespachosFidelizacionCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUIConfig', 'blockUI', 'TercerosFactory', 'VehiculosFactory',
    'PlanillaDespachosFactory', 'PuntosVehiculosFactory', 'RutasFactory', 'ManifiestoFactory', 'DetalleDespachosPuntosVehiculosFactory', 'CumplidosFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUIConfig, blockUI, TercerosFactory, VehiculosFactory,
        PlanillaDespachosFactory, PuntosVehiculosFactory, RutasFactory, ManifiestoFactory, DetalleDespachosPuntosVehiculosFactory, CumplidosFactory) {

        $scope.Titulo = "NUEVO DESPACHO FIDELIZACIÓN";
        $scope.MapaSitio = [{ Nombre: 'Fidelización' }, { Nombre: 'Documentos' }, { Nombre: 'Despachos Fidelización' }, { Nombre: 'Gestionar' }];
        $scope.Master = "#!ConsultarDespachosFidelizacion";
        $scope.DiasVigencia = $scope.Sesion.Empresa.DiasVigenciaPlanPuntos;
        //------------------------------------------------------------DECLARACION VARIABLES------------------------------------------------------------//
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.Deshabilitar = false;
        $scope.DeshabilitarGuardar = false;
        $scope.DeshabilitarEstado = false;
        $scope.MensajesError = [];
        var filtros = {};
        $scope.pref = '';
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_DESPACHOS_FIDELIZACION); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Vehiculo: "",
            FechaInicioVigencia: new Date(),
            Puntos: "",
        }
        $scope.PlanillaInfo;

        $scope.Codigo = CERO;
        $scope.ListadoEstados = [
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'VENCIDO', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        //------------------------------------------------------------OBTENER INFORMACION------------------------------------------------------------//
        //--Valida Existencia Fidelizacion Vehiculo
        $scope.ValidarFidelizacionVehiculo = function () {
            if ($scope.Modelo.Vehiculo.Codigo != undefined) {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Vehiculo: { Codigo: $scope.Modelo.Vehiculo.Codigo },
                    Estado: -1
                };
                PuntosVehiculosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length <= 0) {
                                ShowError("El Vehículo aún no tiene una Fidelización");
                                LimpiarModelo();
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        //--Asignar Fecha Fin Vigencia
        $scope.AsignarFechaFinVigencia = function (FechaIniVigencia) {
            if (FechaIniVigencia != undefined) {
                var DateIniVigencia = new Date(FechaIniVigencia.valueOf());
                var DateFinVigencia = DateIniVigencia.setDate(DateIniVigencia.getDate() + $scope.DiasVigencia);
                var FinalDate = new Date(DateFinVigencia);
                $scope.Modelo.FechaFinVigencia = FinalDate;
            }
        };
        $scope.AsignarFechaFinVigencia($scope.Modelo.FechaInicioVigencia);
        //--Valida Ingreso Planilla
        $scope.ValidaPlanilla = function (NumPlanilla) {
            if (NumPlanilla != undefined && NumPlanilla > 0) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: NumPlanilla,
                    Estado: { Codigo: 1 },
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                };
                PlanillaDespachosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos.length > 0) {
                                $scope.PlanillaInfo = response.data.Datos[0];
                                $scope.Modelo.Vehiculo = {
                                    Codigo: $scope.PlanillaInfo.Vehiculo.Codigo,
                                    Placa: $scope.PlanillaInfo.Vehiculo.Placa,
                                };
                                $scope.Cumplido = { Numero: $scope.PlanillaInfo.Cumplido.Numero };
                                $scope.Modelo.Ruta = {
                                    Codigo: $scope.PlanillaInfo.Ruta.Codigo,
                                    Nombre: $scope.PlanillaInfo.Ruta.Nombre,
                                };
                                $scope.NumDocuManifiesto = $scope.PlanillaInfo.Manifiesto.NumeroDocumento;
                                $scope.Modelo.Manifiesto = {
                                    Numero: $scope.PlanillaInfo.Manifiesto.Numero,
                                    NumeroDocumento: $scope.PlanillaInfo.Manifiesto.NumeroDocumento,
                                };
                                $scope.NumDocuPlanilla = $scope.PlanillaInfo.NumeroDocumento;
                                $scope.Modelo.Planilla = {
                                    Numero: $scope.PlanillaInfo.Numero,
                                    NumeroDocumento: $scope.PlanillaInfo.NumeroDocumento,
                                };
                                if ($scope.PlanillaInfo.Cumplido.Numero > 0) {
                                    if ($scope.Sesion.UsuarioAutenticado.ManejoPesoPuntosFidelizacion == false) {
                                        //Fidelizacion por Ruta
                                        ObtenerPuntosRuta();
                                    }
                                    else {
                                        //Fidelizacion por Peso
                                        ObtenerPesoCumplidoDetalle();
                                    }
                                    ObtenerTenedorConductorVehiculo();
                                    $scope.ValidarFidelizacionVehiculo();
                                }
                                else {
                                    ShowError("No. Planilla no tiene cumplido");
                                    LimpiarModelo();
                                }
                            }
                            else {
                                ShowError("No. Planilla no válida");
                                LimpiarModelo();
                            }
                        }
                        else {
                            ShowError("No. Planilla no válida");
                            LimpiarModelo();
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        LimpiarModelo();
                    });
            }
        };
        //--Valida Ingreso Manifiesto
        $scope.ValidaManifiesto = function (NumManifiesto) {
            if (NumManifiesto != undefined && NumManifiesto > 0) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: NumManifiesto,
                    Estado: { Codigo: 1 },
                };
                ManifiestoFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ManifiestoInfo = response.data.Datos[0];
                                //console.log($scope.ManifiestoInfo); Numero_Cumplido
                                $scope.Modelo.Vehiculo = {
                                    Codigo: $scope.ManifiestoInfo.Vehiculo.Codigo,
                                    Placa: $scope.ManifiestoInfo.Vehiculo.Placa,
                                };
                                $scope.Cumplido = { Numero: $scope.ManifiestoInfo.Numero_Cumplido };
                                $scope.Modelo.Ruta = {
                                    Codigo: $scope.ManifiestoInfo.Ruta.Codigo,
                                    Nombre: $scope.ManifiestoInfo.Ruta.Nombre,
                                };
                                $scope.NumDocuPlanilla = $scope.ManifiestoInfo.Planilla.NumeroDocumento;
                                $scope.Modelo.Planilla = {
                                    Numero: $scope.ManifiestoInfo.Planilla.Numero,
                                    NumeroDocumento: $scope.ManifiestoInfo.Planilla.NumeroDocumento,
                                };
                                $scope.NumDocuManifiesto = $scope.ManifiestoInfo.NumeroDocumento;
                                $scope.Modelo.Manifiesto = {
                                    Numero: $scope.ManifiestoInfo.Numero,
                                    NumeroDocumento: $scope.ManifiestoInfo.NumeroDocumento,
                                };

                                if ($scope.ManifiestoInfo.Numero_Cumplido > 0) {
                                    if ($scope.Sesion.UsuarioAutenticado.ManejoPesoPuntosFidelizacion == false) {
                                        //Fidelizacion por Ruta
                                        ObtenerPuntosRuta();
                                    }
                                    else {
                                        //Fidelizacion por Peso
                                        ObtenerPesoCumplidoDetalle();
                                    }
                                    ObtenerTenedorConductorVehiculo();
                                    $scope.ValidarFidelizacionVehiculo();
                                }
                                else {
                                    ShowError("No. Manifiesto no tiene cumplido");
                                    LimpiarModelo();
                                }
                            }
                            else {
                                ShowError("No. Manifiesto no válido");
                                LimpiarModelo();
                            }
                        }
                        else {
                            ShowError("No. Manifiesto no válido");
                            LimpiarModelo();
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        LimpiarModelo();
                    });
            }
        };
        //--Obtiene Tenedor y Conductor Vehiculo
        function ObtenerTenedorConductorVehiculo() {
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Vehiculo.Codigo
            };
            VehiculosFactory.Consultar(filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.VehiculoInfo = response.data.Datos[0];
                            $scope.Modelo.Conductor = $scope.VehiculoInfo.Conductor;
                            $scope.Modelo.Tenedor = $scope.VehiculoInfo.Tenedor;
                        }
                        else {
                            console.log("Sin Información de Vehiculo");
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        //--Obtiene Puntos de la Ruta
        function ObtenerPuntosRuta() {
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Ruta.Codigo
            };
            RutasFactory.Obtener(filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Puntos = response.data.Datos.PlanPuntos;
                    }
                    else {
                        $scope.Modelo.Puntos = 0;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        //--Limpia Registros
        function LimpiarModelo() {
            $scope.Modelo.Vehiculo = "";
            $scope.Modelo.Ruta = "";
            $scope.Modelo.Puntos = "";
            $scope.NumDocuManifiesto = "";
            $scope.NumDocuPlanilla = "";
        }
        //--Obtiene El detalle del cumplido de las remesas
        function ObtenerPesoCumplidoDetalle() {
            var FiltroPesoCumplido = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Cumplido.Numero,
            };
            CumplidosFactory.Obtener(FiltroPesoCumplido).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        //console.log("Lista Cumplidos Remesas", response.data.Datos.ListaCumplidoRemesas);
                        //$scope.Modelo.Puntos = response.data.Datos.ListaCumplidoRemesas;
                        var TmpPuntosPeso = 0;
                        for (var i = 0; i < response.data.Datos.ListaCumplidoRemesas.length; i++) {
                            TmpPuntosPeso += Math.ceil(response.data.Datos.ListaCumplidoRemesas[i].PesoRecibido / 1000);//Divide para paso de kg a Ton
                        }
                        $scope.Modelo.Puntos = TmpPuntosPeso;
                    }
                    else {
                        $scope.Modelo.Puntos = 0;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        //------------------------------------------------------------FUNCIONES AUTOCOMPLETE------------------------------------------------------------//
        //------------------------------------------------------------MASCARAS------------------------------------------------------------//
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        //------------------------------------------------------------FUNCIONES GENERALES------------------------------------------------------------
        $scope.VolverMaster = function () {
            document.location.href = $scope.Master;
        };

        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
        };
        //--Funcion Guardar Datos
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            var PlanDetalleDespachosPuntosVehiculos = {
                CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                Codigo: $scope.Codigo,
                UsuarioCrea: { Codigo: $scope.Modelo.UsuarioCrea.Codigo },
                UsuarioModifica: { Codigo: $scope.Modelo.UsuarioModifica.Codigo },
                Vehiculo: { Codigo: $scope.Modelo.Vehiculo.Codigo },
                Planilla: { Numero: $scope.Modelo.Planilla.Numero },
                Manifiesto: { Numero: $scope.Modelo.Manifiesto.Numero },
                Ruta: { Codigo: $scope.Modelo.Ruta.Codigo },
                Puntos: $scope.Modelo.Puntos,
                FechaInicioVigencia: $scope.Modelo.FechaInicioVigencia,
                FechaFinVigencia: $scope.Modelo.FechaFinVigencia,
                Estado: $scope.Modelo.Estado.Codigo,
                Anulado: CERO
            }
            DetalleDespachosPuntosVehiculosFactory.Guardar(PlanDetalleDespachosPuntosVehiculos).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > CERO) {
                            if ($scope.Codigo == CERO) {
                                ShowSuccess('Se guardó el despacho de la fidelización correctamente');
                            }
                            else {
                                ShowSuccess('Se modificó el despacho de la fidelización correctamente');
                            }
                            location.href = $scope.Master + '/' + response.data.Datos;
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        //--Funcion Guardar Datos
        //--Funcion Obtener Datos
        function Obtener() {
            blockUI.start('Cargando Despacho No. ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Despacho No. ' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };
            blockUI.delay = 1000;

            DetalleDespachosPuntosVehiculosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = response.data.Datos;
                        $scope.Modelo.UsuarioModifica =  { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };

                        if ($scope.Modelo.Anulado == ESTADO_ANULADO) {
                            $scope.ListadoEstados.push({ Nombre: 'ANULADO', Codigo: 2 });
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + 2);
                            DeshabilitarEstado = true;
                            DeshabilitarGuardar = true;
                        }
                        else {
                            $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        }
                        
                        $scope.Modelo.FechaInicioVigencia = new Date(response.data.Datos.FechaInicioVigencia);
                        $scope.Modelo.FechaFinVigencia = new Date(response.data.Datos.FechaFinVigencia);

                        $scope.NumDocuPlanilla = $scope.Modelo.Planilla.NumeroDocumento;
                        $scope.NumDocuManifiesto = $scope.Modelo.Manifiesto.NumeroDocumento;
                        $scope.Deshabilitar = true;
                        $scope.Titulo = "DESPACHO FIDELIZACIÓN";
                    }
                    else {
                        ShowError('No se logro consultar el despacho de la fidelización' + response.data.MensajeOperacion);
                        document.location.href = $scope.Master;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = $scope.Master;
                });
            blockUI.stop();
        }
        //--Funcion Obtener Datos
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo;

            if ($scope.NumDocuManifiesto == undefined || $scope.NumDocuManifiesto == '' || $scope.NumDocuManifiesto == null) {
                $scope.MensajesError.push('Debe ingresar un No. Manifiesto');
                continuar = false;
            }
            if ($scope.NumDocuPlanilla == undefined || $scope.NumDocuPlanilla == '' || $scope.NumDocuPlanilla == null) {
                $scope.MensajesError.push('Debe ingresar un No. Planilla');
                continuar = false;
            }

            if (Modelo.FechaInicioVigencia == undefined || Modelo.FechaInicioVigencia == '' || Modelo.FechaInicioVigencia == null) {
                $scope.MensajesError.push('Debe ingresar Fecha Inicio Vigencia');
                continuar = false;
            }
            if (Modelo.Estado == undefined || Modelo.Estado == '' || Modelo.Estado == null) {
                $scope.MensajesError.push('Debe ingresar Estado');
                continuar = false;
            }
            else {
                if (Modelo.Estado.Codigo == -1) {
                    $scope.MensajesError.push('Debe Seleccionar un Estado');
                }
            }
            return continuar;
        }
        //------------------------------------------------------------PARAMETROS------------------------------------------------------------//
        try {
            if ($routeParams !== undefined && $routeParams.Codigo !== null) {
                if (parseInt($routeParams.Codigo) > CERO) {
                    $scope.Codigo = parseInt($routeParams.Codigo);
                    Obtener();
                }
            }
        }
        catch (e) {
            if ($routeParams.Codigo > CERO) {
                $scope.Codigo = $routeParams.Codigo;
                Obtener();
            }
        }
    }]);