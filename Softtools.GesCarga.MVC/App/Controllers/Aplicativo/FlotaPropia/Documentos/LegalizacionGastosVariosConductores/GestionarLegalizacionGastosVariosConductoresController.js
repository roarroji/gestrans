﻿SofttoolsApp.controller("GestionarLegalizacionGastosVariosConductoresCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'blockUIConfig', 'LegalizacionGastosFactory', 'PlanillaDespachosFactory', 'RutasFactory', 'PlanillaGuiasFactory',
    'ValorCatalogosFactory', 'TercerosFactory', 'OficinasFactory', 'ConceptoGastosFactory', 'ManifiestoFactory', 'DocumentoComprobantesFactory', 'DocumentoCuentasFactory', 'VehiculosFactory', 'SemirremolquesFactory', 'PeajesFactory',
    'CuentasPorCobrarFactory', 'ValorCombustibleFactory','RendimientoGalonCombustibleFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, blockUIConfig, LegalizacionGastosFactory, PlanillaDespachosFactory, RutasFactory, PlanillaGuiasFactory,
        ValorCatalogosFactory, TercerosFactory, OficinasFactory, ConceptoGastosFactory, ManifiestoFactory, DocumentoComprobantesFactory, DocumentoCuentasFactory, VehiculosFactory, SemirremolquesFactory, PeajesFactory,
        CuentasPorCobrarFactory, ValorCombustibleFactory, RendimientoGalonCombustibleFactory) {
        console.clear();

        $scope.Titulo = 'GESTIONAR LEGALIZACIÓN GASTOS VARIOS CONDUCTORES';
        $scope.MapaSitio = [{ Nombre: 'Flota Propia' }, { Nombre: 'Documentos' }, { Nombre: 'Legalización Gastos Varios Conductores' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + 600102); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.IdentificacionConductor = "";
        $scope.HabilitarCantidadGalones = false;
        $scope.HabilitarTieneChip = false;
        var DocumentosCuentasAgregados = false;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Fecha: new Date()
        };
        $scope.ListaPlanillaConceptos = []
        var OficinaUsuario = {
            Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
        };
        $scope.DeshabilitarControlPlanilla = false;
        $scope.DeshabilitarGuardar = false;
        $scope.ListadoConceptos = []
        var Response = ConceptoGastosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: ESTADO_ACTIVO,
            Sync: true
        })
        $scope.ListadoConceptos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConceptos);
        //$scope.Modelo.DetalleLegalizacionGastos[i].Concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + $scope.Modelo.DetalleLegalizacionGastos[i].Concepto.Codigo);


        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        $scope.CargarDatosFunciones = function () {
            $scope.ListadoEstados = [
                { Nombre: '(TODOS)', Codigo: -1 },
                { Nombre: 'BORRADOR', Codigo: CERO },
                { Nombre: 'DEFINITIVO', Codigo: 1 }
            ]
            try {
                $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Estado);
            } catch (e) {
                $scope.ModeloEstado = $scope.ListadoEstados[1]
            }

            $scope.AutocompleteProveedores = function (value) {
                $scope.ListadoProveedores = []
                if (value != null && value != undefined && value != '') {
                    if (value.length > 3) {
                        $scope.ListadoProveedores = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROVEEDOR, Estado: ESTADO_ACTIVO, Sync : true }).Datos
                    }
                }
                return $scope.ListadoProveedores
            }
            /*Cargar Autocomplete de Grupos proveedores*/
            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROVEEDOR, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoRepresentanteComerciales = []
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoProveedores = response.data.Datos;
                        }
                        else {
                            $scope.ListadoGrupoEmpresariales = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        function Obtener() {
            blockUI.start('Cargando liquidación número ' + $routeParams.Numero);

            $timeout(function () {
                blockUI.message('Cargando liquidación número ' + $routeParams.Numero);
            }, 200);

            TotalBaseImpuestos = 0;
            TotalValorImpuestos = 0;

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ModeloNumero
            };
            blockUI.delay = 1000;
            LegalizacionGastosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = response.data.Datos;
                        $scope.Modelo.Fecha = new Date(response.data.Datos.Fecha);
                        OficinaUsuario = response.data.Datos.Oficina
                        $scope.ModeloOficina = response.data.Datos.Oficina.Nombre;
                        for (var i = 0; i < response.data.Datos.DetalleConductores.length; i++) {
                            var Conductor = $scope.CargarTercero(response.data.Datos.DetalleConductores[i].Conductor.Codigo)

                            Conductor.ValorGastos = MascaraValores(response.data.Datos.DetalleConductores[i].ValorGastos)
                            Conductor.ValorAnticipos = MascaraValores(response.data.Datos.DetalleConductores[i].ValorAnticipos)
                            Conductor.Planilla = {}
                            Conductor.Planilla.ValorAnticipo = MascaraValores(response.data.Datos.DetalleConductores[i].ValorAnticipos)
                            Conductor.ValorReanticipos = MascaraValores(response.data.Datos.DetalleConductores[i].ValorReanticipos)
                            Conductor.SaldoConductor = MascaraValores(response.data.Datos.DetalleConductores[i].SaldoConductor)
                            Conductor.SaldoEmpresa = MascaraValores(response.data.Datos.DetalleConductores[i].SaldoEmpresa)
                            Conductor.Data = []
                            Conductor.Comprobantes = []
                            for (var j = 0; j < response.data.Datos.DetalleLegalizacionGastos.length; j++) {
                                if (response.data.Datos.DetalleLegalizacionGastos[j].Conductor.Codigo == Conductor.Codigo) {
                                    var Concepto = response.data.Datos.DetalleLegalizacionGastos[j];
                                    var tmpNombreConcepto = response.data.Datos.DetalleLegalizacionGastos[j].Concepto.Nombre;
                                    Concepto.Valor = MascaraValores(response.data.Datos.DetalleLegalizacionGastos[j].Valor);
                                    Concepto.FechaFactura = new Date($scope.Modelo.DetalleLegalizacionGastos[j].FechaFactura);
                                    Concepto.ValorGalones = MascaraValores(response.data.Datos.DetalleLegalizacionGastos[j].ValorGalones);
                                    Concepto.CantidadGalones = MascaraValores(response.data.Datos.DetalleLegalizacionGastos[j].CantidadGalones);
                                    //Concepto.Concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + response.data.Datos.DetalleLegalizacionGastos[j].Concepto.Codigo);
                                    if (response.data.Datos.DetalleLegalizacionGastos[j].Concepto.Codigo == CODIGO_GASTO_LEGALIZACION_PEAJES || response.data.Datos.DetalleLegalizacionGastos[j].Concepto.Codigo == CODIGO_GASTO_LEGALIZACION_SALDO_A_FAVOR_CONDUCTOR || response.data.Datos.DetalleLegalizacionGastos[j].Concepto.Codigo == CODIGO_GASTO_LEGALIZACION_SALDO_A_FAVOR_EMPRESA) {

                                        Concepto.Concepto = ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.DetalleLegalizacionGastos[j].Concepto.Codigo, Sync: true }).Datos;
                                    } else {
                                        try { Concepto.Concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + response.data.Datos.DetalleLegalizacionGastos[j].Concepto.Codigo); } catch (e) {
                                            Concepto.Concepto = ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.DetalleLegalizacionGastos[j].Concepto.Codigo, Sync: true }).Datos;
                                        }
                                    }
                                    Concepto.Proveedor = $scope.CargarTercero($scope.Modelo.DetalleLegalizacionGastos[j].Proveedor.Codigo);
                                    Concepto.Concepto.Nombre = tmpNombreConcepto;
                                    Conductor.Data.push(Concepto);
                                    //Conductor.Planilla.ValorReanticipos = MascaraValores($scope.Modelo.DetalleLegalizacionGastos[i].Planilla.ValorReanticipos)
                                }
                            }
                            for (var j = 0; j < response.data.Datos.Comprobantes.length; j++) {
                                if (response.data.Datos.Comprobantes[j].Tercero.Codigo == Conductor.Codigo) {
                                    Conductor.Comprobantes.push(response.data.Datos.Comprobantes[j])
                                }
                            }
                            //Conductor.Data = AgruparListados(Conductor.Data, 'Planilla');
                            if (Conductor.Data != undefined) {
                                for (var k = 0; k < Conductor.Data.length; k++) {
                                    var tmpNombre = Conductor.Data[k].Concepto.Nombre;
                                    if (Conductor.Data[k].Concepto.Codigo == CODIGO_GASTO_LEGALIZACION_PEAJES || Conductor.Data[k].Concepto.Codigo == CODIGO_GASTO_LEGALIZACION_SALDO_A_FAVOR_CONDUCTOR || Conductor.Data[k].Concepto.Codigo == CODIGO_GASTO_LEGALIZACION_SALDO_A_FAVOR_EMPRESA) {

                                        Conductor.Data[k].Concepto = ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Conductor.Data[k].Concepto.Codigo, Sync: true }).Datos;
                                    } else {
                                        try { Conductor.Data[k].Concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + Conductor.Data[k].Concepto.Codigo); } catch (e) {
                                            Conductor.Data[k].Concepto = ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Conductor.Data[k].Concepto.Codigo, Sync: true }).Datos;
                                        }
                                    }
                                    Conductor.Data[k].Concepto.Nombre = tmpNombre;
                                    //Conductor.Data[k].Concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + Conductor.Data[k].Concepto.Codigo);
                                }
                            }
                            try {


                                filtros = {
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Conductor: { Codigo: Conductor.Codigo },
                                    AplicaConsultaMaster: 1,
                                    ConsultaLegalización: 0,
                                    ConsultaLegalizaciónVariosConductores: 1,
                                    Estado: { Codigo: 1 },
                                    Sync: true,
                                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO
                                };
                                var responsepl = PlanillaDespachosFactory.Consultar(filtros)
                                Conductor.ListadoPlanillaDespacho = responsepl.Datos;
                            } catch (e) {

                            }

                            RPlanilla = PlanillaDespachosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: $scope.Modelo.Planilla.Numero, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Sync: true }).Datos
                            if (RPlanilla.ConductoresTrayectos != null) {
                                if (RPlanilla.ConductoresTrayectos.length > 0) {
                                    RPlanilla.ConductoresTrayectos.forEach(itemtrayecto => {
                                        if (itemtrayecto.Conductor.Codigo == Conductor.Codigo) {
                                            var RutaTrayectoConductor = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: itemtrayecto.RutaTrayecto.Codigo, Sync: true }).Datos
                                            Conductor.NombreCompleto = Conductor.NombreCompleto + ' - Trayecto : ' + RutaTrayectoConductor.Nombre
                                            //Conductor.ValorAnticipos = itemtrayecto.ValorAnticipo
                                        }
                                    })


                                }
                            }

                            $scope.ListadoConductoresLegalizacion.push(Conductor)
                        }
                        $scope.CargarDatosFunciones()

                        filtros = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Numero: $scope.Modelo.Planilla.Numero,
                        };


                        blockUI.delay = 1000;
                        PlanillaGuiasFactory.Obtener(filtros).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    //$scope.Planilla.Planilla = response.data.Datos.Planilla
                                    $scope.Planilla = {}
                                    $scope.Planilla.Planilla = {
                                        Numero: response.data.Datos.Planilla.Numero,
                                        Vehiculo: $scope.CargarVehiculos(response.data.Datos.Planilla.Vehiculo.Codigo),
                                        Ruta: RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.Planilla.Ruta.Codigo, Estado: 1, Sync: true }).Datos[0]
                                    }
                                    $scope.Planilla.Numero = response.data.Datos.Planilla.Numero
                                    $scope.Planilla.NumeroDocumento = response.data.Datos.Planilla.NumeroDocumento
                                }
                                else {
                                    ShowError('No se logro consultar la planilla');
                                    $scope.Planilla = {}
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                                $scope.Planilla = {}
                            });



                        if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                            $scope.DeshabilitarGuardar = true
                        }
                        $timeout(function () {
                            $scope.Calcular();
                        }, 1000)
                    }
                    else {
                        ShowError('No se logro consultar la legalizacion. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarLegalizacionGastosVariosConductores';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarLegalizacionGastosVariosConductores';
                });

            blockUI.stop();
        };

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar')
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            window.scrollTo(top, top);
            if (DatosRequeridos()) {
                $scope.Legalizacion = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.ModeloNumero,
                    Planilla: { Numero: $scope.Planilla.Numero },
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Fecha: new Date()
                }
                $scope.Legalizacion.Oficina = OficinaUsuario
                $scope.Legalizacion.Estado = $scope.ModeloEstado.Codigo
                $scope.Legalizacion.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                $scope.Legalizacion.DetalleLegalizacionGastos = []
                $scope.Legalizacion.Observaciones = $scope.Modelo.Observaciones
                $scope.Legalizacion.DetalleConductores = []
                $scope.Legalizacion.Comprobantes = []
                for (var K = 0; K < $scope.ListadoConductoresLegalizacion.length; K++) {
                    var CuentaPorPagar = ''
                    var CuentaPorCobrar = ''
                    if ($scope.Sesion.UsuarioAutenticado.ManejoCuentasPorCobrarPagarLegalizacionesGastos) {
                        if (parseInt($scope.ListadoConductoresLegalizacion[K].SaldoConductor) > 0) {
                            CuentaPorPagar = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                                CodigoAlterno: '',
                                Fecha: new Date(),
                                Tercero: { Codigo: $scope.ListadoConductoresLegalizacion[K] == undefined ? 0 : $scope.ListadoConductoresLegalizacion[K].Codigo },
                                DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_MASIVO },
                                FechaDocumento: $scope.Modelo.Fecha,
                                FechaVenceDocumento: $scope.Modelo.Fecha,
                                Numeracion: '',
                                CuentaPuc: { Codigo: CERO },
                                ValorTotal: $scope.ListadoConductoresLegalizacion[K].SaldoConductor,
                                Abono: 0,
                                Saldo: $scope.ListadoConductoresLegalizacion[K].SaldoConductor,
                                FechaCancelacionPago: $scope.Modelo.Fecha,
                                Aprobado: 1,
                                UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                FechaAprobo: $scope.Modelo.Fecha,
                                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                                Vehiculo: { Codigo: 0 },
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                            }
                        }

                        if (parseInt($scope.ListadoConductoresLegalizacion[K].SaldoEmpresa) > 0) {
                            CuentaPorCobrar = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR,
                                CodigoAlterno: '',
                                Fecha: new Date(),
                                Tercero: { Codigo: $scope.ListadoConductoresLegalizacion[K] == undefined ? 0 : $scope.ListadoConductoresLegalizacion[K].Codigo },
                                DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_MASIVO },
                                FechaDocumento: $scope.Modelo.Fecha,
                                FechaVenceDocumento: $scope.Modelo.Fecha,
                                Numeracion: '',
                                CuentaPuc: { Codigo: CERO },
                                ValorTotal: $scope.ListadoConductoresLegalizacion[K].SaldoEmpresa,
                                Abono: 0,
                                Saldo: $scope.ListadoConductoresLegalizacion[K].SaldoEmpresa,
                                FechaCancelacionPago: $scope.Modelo.Fecha,
                                Aprobado: 1,
                                UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                FechaAprobo: $scope.Modelo.Fecha,
                                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                                Vehiculo: { Codigo: 0 },
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }

                            }
                        }
                    }

                    $scope.Legalizacion.DetalleConductores.push({
                        Conductor: { Codigo: $scope.ListadoConductoresLegalizacion[K].Codigo },
                        ValorGastos: $scope.ListadoConductoresLegalizacion[K].ValorGastos,
                        ValorAnticipos: $scope.ListadoConductoresLegalizacion[K].ValorAnticipos,
                        ValorReanticipos: $scope.ListadoConductoresLegalizacion[K].ValorReanticipos,
                        SaldoConductor: $scope.ListadoConductoresLegalizacion[K].SaldoConductor,
                        SaldoEmpresa: $scope.ListadoConductoresLegalizacion[K].SaldoEmpresa,
                        CuentaPorPagar: CuentaPorPagar == '' ? null : CuentaPorPagar,
                        CuentaPorCobrar: CuentaPorCobrar == '' ? null : CuentaPorCobrar
                    })
                    for (var j = 0; j < $scope.ListadoConductoresLegalizacion[K].Data.length; j++) {
                        var item = $scope.ListadoConductoresLegalizacion[K].Data[j]
                        $scope.Legalizacion.DetalleLegalizacionGastos.push({
                            Planilla: { Numero: $scope.Planilla.Numero },
                            Concepto: { Codigo: item.Concepto.Codigo },
                            Conductor: { Codigo: $scope.ListadoConductoresLegalizacion[K].Codigo },
                            Observaciones: item.Observaciones,
                            Valor: item.Valor,
                            Proveedor: item.Proveedor,
                            NumeroFactura: item.NumeroFactura,
                            FechaFactura: item.FechaFactura,
                            DocumentoCuenta: item.DocumentoCuenta == undefined ? 0 : item.DocumentoCuenta
                        })
                    }
                    for (var j = 0; j < $scope.ListadoConductoresLegalizacion[K].Comprobantes.length; j++) {
                        var item = $scope.ListadoConductoresLegalizacion[K].Comprobantes[j]
                        item.NumeroDocumentoOrigen = $scope.Planilla.NumeroDocumento
                        $scope.Legalizacion.Comprobantes.push(item)
                    }
                }
                LegalizacionGastosFactory.Guardar($scope.Legalizacion).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos === -1) {
                                ShowError('La planilla ya se encuentra liquidada');
                            } else {
                                if (response.data.Datos > 0) {
                                    if ($scope.ModeloNumero === 0) {
                                        ShowSuccess('Se guardó la legalización de gastos No. ' + response.data.Datos);
                                    }
                                    else {
                                        ShowSuccess('Se modificó la la legalización de gastos No. ' + response.data.Datos);
                                    }
                                    location.href = '#!ConsultarLegalizacionGastosVariosConductores/' + response.data.Datos;
                                } else {
                                    ShowError(response.data.MensajeOperacion);
                                }
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var Continuar = true;
            if ($scope.Modelo.Fecha == undefined || $scope.Modelo.Fecha == '' || $scope.Modelo.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha');
                Continuar = false
            }
            //if ($scope.Modelo.Conductor == undefined || $scope.Modelo.Conductor == '' || $scope.Modelo.Conductor == null) {
            //    $scope.MensajesError.push('Debe ingresar el conductor');
            //    Continuar = false
            //}
            if ($scope.ListadoConductoresLegalizacion == undefined || $scope.ListadoConductoresLegalizacion == [] || $scope.ListadoConductoresLegalizacion == null || $scope.ListadoConductoresLegalizacion.length == 0) {
                $scope.MensajesError.push('Debe adicionar al menos un Conductor');
                Continuar = false
            } else {
                var ConceptoNoasociado = false
                var faltavalor = false
                var faltaconcepto = false
                var faltaproveedor = false
                for (var c = 0; c < $scope.ListadoConductoresLegalizacion.length; c++) {
                    var Conductor = $scope.ListadoConductoresLegalizacion[c]
                    if (Conductor.Data.length > 0) {
                        for (var j = 0; j < Conductor.Data.length; j++) {
                            var item = Conductor.Data[j]
                            if (item.Valor == undefined || item.Valor == '' || item.Valor == 0 || item.Valor == '0') {
                                faltavalor = true
                            }
                            if (item.Concepto == undefined || item.Concepto == '' || item.Concepto == null) {
                                faltaconcepto = true
                            }
                            if (item.Proveedor == undefined || item.Proveedor == '' || item.Proveedor == null) {
                                faltaproveedor = true
                            }
                        }
                    }
                    else {
                        ConceptoNoasociado = true
                    }

                }
                if (ConceptoNoasociado) {
                    $scope.MensajesError.push('Debe asociar al menos un concepto a cada planilla Conductor');
                    Continuar = false
                }
                if (faltaconcepto) {
                    $scope.MensajesError.push('Debe seleccionar los conceptos de todos los detalle asociados');
                    Continuar = false
                }
                if (faltavalor) {
                    $scope.MensajesError.push('Debe ingresar todos los valores de los conceptos asociados');
                    Continuar = false
                }
                if (faltaproveedor) {
                    $scope.MensajesError.push('Debe ingresar todos los proveedores de los conceptos asociados');
                    Continuar = false
                }
            }
            return Continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarLegalizacionGastosVariosConductores/' + $scope.Modelo.NumeroDocumento;
        };
        $scope.Calcular = function () {
            for (var c = 0; c < $scope.ListadoConductoresLegalizacion.length; c++) {
                var Conductor = $scope.ListadoConductoresLegalizacion[c]
                Conductor.ValorGastos = 0
                if ($scope.Sesion.UsuarioAutenticado.ProcesoProporcionFleteVariosConductores) {
                    Conductor.ValorAnticipos = Conductor.ValorAnticipos
                } else {
                    Conductor.ValorAnticipos = 0
                }
                Conductor.ValorReanticipos = 0
                Conductor.SaldoConductor = 0
                Conductor.SaldoEmpresa = 0
                Conductor.TotalGalones = 0
                Conductor.TotalGalonesAutorizados = 0
                Conductor.TotalGalonesEstimados = 0
                Conductor.TotalPeajes = 0
                for (var j = 0; j < Conductor.Data.length; j++) {
                    var item = Conductor.Data[j];
                    if (item.Valor !== undefined && item.Valor !== '' && item.Valor !== null) {
                        //se valida que no sea el concepto "salario variable", este no se debe tener en cuenta en la sumatoria: 
                        if (item.Concepto.Codigo != 10012) {
                            if (item.Concepto.Operacion == 1) {
                                Conductor.ValorGastos -= parseInt(MascaraNumero(item.Valor))
                            } else {
                                Conductor.ValorGastos += parseInt(MascaraNumero(item.Valor))
                            }
                        }
                    }

                }
                for (var CO = 0; CO < Conductor.Comprobantes.length; CO++) {
                    if ((Conductor.Comprobantes[CO].DocumentoOrigen.Codigo == 2604 || Conductor.Comprobantes[CO].DocumentoOrigen.Codigo == 2605)) {
                        if ($scope.Sesion.UsuarioAutenticado.ProcesoProporcionFleteVariosConductores) {
                            Conductor.ValorAnticipos = Conductor.ValorAnticipos
                        } else {
                            Conductor.ValorAnticipos += Conductor.Comprobantes[CO].ValorPagoTotal
                        }
                    } else if (Conductor.Comprobantes[CO].DocumentoOrigen.Codigo == 2609) {
                        Conductor.ValorReanticipos += Conductor.Comprobantes[CO].ValorPagoTotal
                    }
                }

                //Conductor.ValorAnticipos += parseInt(MascaraNumero(Conductor.Planilla.ValorAnticipo))
                //Conductor.ValorReanticipos += parseInt(MascaraNumero(Conductor.Planilla.ValorReanticipos))
                //Conductor.SaldoConductor += parseInt(MascaraNumero($scope.ListaPlanillaConceptos[i].Planilla.ValorFleteTransportador))

                //Conductor.TotalGalonesAutorizados = parseFloat(Conductor.TotalGalonesEstimados - Conductor.TotalGalones).toFixed(2)
                //Conductor.SaldoEmpresa = MascaraValores((Conductor.ValorAnticipos + Conductor.ValorReanticipos))
                Conductor.SaldoEmpresa = ((MascaraNumero(Conductor.ValorAnticipos) + MascaraNumero(Conductor.ValorReanticipos)) - MascaraNumero(Conductor.ValorGastos)) <= 0 ? 0 : MascaraValores(((MascaraNumero(Conductor.ValorAnticipos) + MascaraNumero(Conductor.ValorReanticipos)) - Conductor.ValorGastos));
                //Conductor.SaldoConductor = MascaraValores((Conductor.SaldoConductor - Conductor.ValorAnticipos))
                Conductor.SaldoConductor = (Conductor.ValorGastos - (MascaraNumero(Conductor.ValorAnticipos) + MascaraNumero(Conductor.ValorReanticipos))) <= 0 ? 0 : MascaraValores((MascaraNumero(Conductor.ValorGastos) - (MascaraNumero(Conductor.ValorAnticipos) + MascaraNumero(Conductor.ValorReanticipos))));
                Conductor.ValorGastos = MascaraValores(Conductor.ValorGastos);
                try {
                    Conductor.ValorAnticipos = MascaraValores(Conductor.ValorAnticipos);

                    //if (response.data.Datos.ConductoresTrayectos.length > 0) {
                    //    response.data.Datos.ConductoresTrayectos.forEach(itemtrayecto => {
                    //        if (itemtrayecto.Conductor.Codigo == Conductor.Codigo) {
                    //            var RutaTrayectoConductor = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: itemtrayecto.RutaTrayecto.Codigo, Sync: true }).Datos
                    //            Conductor.NombreCompleto = Conductor.NombreCompleto + ' - Trayecto : ' + RutaTrayectoConductor.Nombre
                    //            Conductor.ValorAnticipos = itemtrayecto.ValorAnticipo
                    //        }
                    //    })


                    //}
                    Conductor.ValorReanticipos = MascaraValores(Conductor.ValorReanticipos);
                    Conductor.TotalGalones = MascaraValores(Conductor.TotalGalones);
                    Conductor.TotalGalonesAutorizados = MascaraValores(Conductor.TotalGalonesAutorizados);
                    Conductor.TotalPeajes = MascaraValores(Conductor.TotalPeajes)
                } catch (e) {
                    console.log('Error al cargar : '+e)
                }
            }
        }
        //-------------------------------------------------AUTOCOMPLETE-------------------------------------------------//
        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    //--Cargar Autocomplete de propietario--//
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CONDUCTOR,
                        ConsultaConductoreslegalizacion: 1,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores);
                }
            }
            return $scope.ListadoConductores;
        }
        //-------------------------------------------------AUTOCOMPLETE-------------------------------------------------//

        $scope.AsignarFechas = function () {
            for (var i = 0; i < $scope.ListaPlanillaConceptos.length; i++) {
                for (var j = 0; j < $scope.ListaPlanillaConceptos[i].Data.length; j++) {
                    var item = $scope.ListaPlanillaConceptos[i].Data[j]
                    try {
                        if (new Date(item.FechaFactura) > MIN_DATE) {
                            item.FechaFactura = new Date(item.FechaFactura)
                        }
                    } catch (e) {

                    }
                    try {
                        item.Valor = MascaraValores(item.Valor)
                        item.CantidadGalones = MascaraValores(item.CantidadGalones)
                        item.ValorGalones = MascaraValores(item.ValorGalones)
                    } catch (e) {

                    }

                }
            }
        }
        $scope.ListConceptos = function (item) {
            if (item.lstData == true) {
                item.lstData = false
            } else {
                item.lstData = true
            }
        }
        $scope.AgregarPlanilla = function (ObjConductor) {
            var cont = 0
            if (ObjConductor.Planilla == undefined || ObjConductor.Planilla == '') {
                ShowError('Debe seleccionar una planilla')
            } else {
                try {
                    for (var i = 0; i < ObjConductor.ListaPlanillaConceptos.length; i++) {
                        if (ObjConductor.Planilla.NumeroDocumento == ObjConductor.ListaPlanillaConceptos[i].Planilla.NumeroDocumento) {
                            cont++
                        }
                    }
                } catch (e) {
                    ObjConductor.ListaPlanillaConceptos = []
                }
                if (cont > 0) {
                    ShowError('La planilla seleccionada ya se encuentra asociada ')
                    ObjConductor.Planilla = ''
                } else {
                    var Data = []
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: ObjConductor.Planilla.Ruta.Codigo,
                        ConsultaLegalizacionGastosRuta: true,
                        Sync: true
                    }
                    blockUI.delay = 1000;
                    var response = RutasFactory.Obtener(filtros)
                    if (response.ProcesoExitoso === true) {
                        if (response.Datos.LegalizacionGastosRuta !== undefined) {
                            for (var i = 0; i < response.Datos.LegalizacionGastosRuta.length; i++) {
                                if (response.Datos.LegalizacionGastosRuta[i].TipoVehiculo.Codigo == ObjConductor.Planilla.TipoVehiculoCodigo) {
                                    var concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + response.Datos.LegalizacionGastosRuta[i].Concepto.Codigo)
                                    Data.push({ Concepto: concepto, Valor: MascaraValores(response.Datos.LegalizacionGastosRuta[i].Valor) })
                                }
                            }
                        }
                    }



                    // Añadir a los conceptos el valor total de los peajes:

                    //Obtener Tipo de Vehículo y Número total de Ejes del Semirremolque de la Planilla:
                    var responseVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 0, Placa: $scope.Planilla.Planilla.Vehiculo.Placa, Sync: true });
                    var responseSemirremolque = {};
                    var TotalEjes = 0;
                    if (responseVehiculo.ProcesoExitoso == true) {

                        if ($scope.Planilla.Planilla.Semirremolque.Codigo != 0) {
                            responseSemirremolque = SemirremolquesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Planilla.Planilla.Semirremolque.Codigo, Sync: true });
                            if (responseSemirremolque.ProcesoExitoso == true) {

                                // ejes del Semirremolque:
                                TotalEjes = responseSemirremolque.Datos.NumeroEjes;
                            }
                        }
                        else {
                            //TotalEjes = responseSemirremolque.Datos.NumeroEjes;
                        }


                        $scope.TotalPeajes = 0;


                        //Consultar los peajes de la ruta de la planilla:
                        RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Planilla.Planilla.Ruta.Codigo, ConsultarPeajes: true }).
                            then(function (response) {
                                if (response.data.ProcesoExitoso == true) {

                                    var ListaTarifasPeajesRuta = [];
                                    var PeajesRutasSize = response.data.Datos.PeajeRutas.length;

                                    $scope.ListadoPeajes = [];

                                    //Por cada peaje, consultar las tarifas parametrizadas:
                                    for (let i = 0; i < response.data.Datos.PeajeRutas.length; i++) {
                                        if (response.data.Datos.PeajeRutas[i].Peaje.Estado == ESTADO_ACTIVO) {
                                            let NombrePeaje = response.data.Datos.PeajeRutas[i].Peaje.Nombre;

                                            PeajesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.PeajeRutas[i].Peaje.Codigo, ConsultarTarifasPeajes: true }).
                                                then(function (response) {

                                                    //Comparar si el tipo de vehículo de la planilla, coincide con las tarifas parametrizadas en el peaje:
                                                    for (let j = 0; j < response.data.Datos.TarifasPeajes.length; j++) {
                                                        if (response.data.Datos.TarifasPeajes[j].TipoVehiculo.Codigo == responseVehiculo.Datos.TipoVehiculo.Codigo) {

                                                            // si coincide, verificar primero que la planilla no tenga semirremolque:
                                                            if (TotalEjes > 0) {
                                                                //si lo tiene agregar el valor a los conceptos segun el número de ejes del semirremolque:
                                                                if (Data[0].Concepto == undefined || Data[0].Concepto == null) {
                                                                    if (TotalEjes == 1) {
                                                                        Data[0] = {
                                                                            Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque1Eje), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo)
                                                                        }
                                                                    } else if (TotalEjes == 2) {
                                                                        Data[0] = {
                                                                            Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque2Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo)
                                                                        }
                                                                    } else if (TotalEjes == 3) {
                                                                        Data[0] = {
                                                                            Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque3Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo)
                                                                        }
                                                                    } else if (TotalEjes >= 4) {
                                                                        Data[0] = {
                                                                            Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque4Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo)
                                                                        }
                                                                    }


                                                                } else {

                                                                    if (TotalEjes == 1) {
                                                                        Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque1Eje), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo) });
                                                                    } else if (TotalEjes == 2) {
                                                                        Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque2Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo) });
                                                                    } else if (TotalEjes == 3) {
                                                                        Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque3Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo) });
                                                                    } else if (TotalEjes >= 4) {
                                                                        Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque4Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo) });
                                                                    }

                                                                }
                                                            } else {
                                                                //si no lo tiene, agregar el valor parametrizado por defecto:
                                                                if (Data[0].Concepto == undefined || Data[0].Concepto == null) {
                                                                    Data[0] = {
                                                                        Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].Valor), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo)
                                                                    }
                                                                } else {
                                                                    Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].Valor), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo) });
                                                                }
                                                            }
                                                        }

                                                    }
                                                    ListaTarifasPeajesRuta.push(response.data.Datos);
                                                });
                                        }


                                    }


                                }
                            });



                        //Fin Conceptos de Peajes//

                    }


                    if (Data.length == 0) {
                        Data.push({})
                    }

                    ObjConductor.ListaPlanillaConceptos.push({ Planilla: ObjConductor.Planilla, Data: Data })
                    try {
                        $timeout(function () {
                            $scope.Calcular();
                        }, 1000)
                    } catch (e) { }
                }
                ObjConductor.Planilla = ''
            }
        }



        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskMayusGrid = function (item) {
            return MascaraMayus(item)
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskDecimalesGrid = function (item) {
            return MascaraDecimales(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };
        $scope.EliminarConcepto = function (item, index) {
            item.Data.splice($index, 1)
        }

        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.ModeloNumero = $routeParams.Codigo;
            Obtener();
        }
        else {
            $scope.ModeloNumero = 0;
            $scope.ModeloOficina = OficinaUsuario.Nombre;
            $scope.CargarDatosFunciones()
        };

        $scope.CargarConductoresPlanilla = function () {
            if ($scope.Planilla != undefined) {
                if ($scope.Planilla.NumeroDocumento > 0) {

                    blockUI.start('Cargando Planilla Despachos...');

                    $timeout(function () {
                        blockUI.message('Cargando Planilla Despachos...');
                    }, 200);

                    //filtros = {
                    //    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    //    TipoDocumento: 150,
                    //    NumeroDocumento: $scope.Planilla.NumeroDocumento,
                    //};

                    blockUI.delay = 1000;
                    var ResponsePlanillaMasivo = PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: $scope.Planilla.NumeroDocumento, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true }).Datos[0];
                    if (ResponsePlanillaMasivo == undefined) {
                        ShowError('El número ingresado no existe o no corresponde a una planilla despacho')
                    } else if (ResponsePlanillaMasivo.length == 0) {
                        ShowError('El número ingresado no existe o no corresponde a una planilla despacho')

                    } else {
                        $scope.Planilla.Numero = ResponsePlanillaMasivo.Numero

                        filtros = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Numero: ResponsePlanillaMasivo.Numero,
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO
                        };

                        PlanillaDespachosFactory.Obtener(filtros).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {

                                    //if (response.data.Datos.Planilla !== null) {
                                    if (response.data.Datos.Numero !== null) {
                                        if (response.data.Datos.NumeroLegalizacionGastos == 0) {

                                            //$scope.Planilla.Planilla = response.data.Datos.Planilla
                                            //$scope.Planilla.Planilla = {
                                            //    Numero: response.data.Datos.Planilla.Numero,
                                            //    Vehiculo: $scope.CargarVehiculos(response.data.Datos.Planilla.Vehiculo.Codigo),
                                            //    Ruta: RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.Planilla.Ruta.Codigo, Estado: 1, Sync: true }).Datos[0],
                                            //    Semirremolque: { Codigo: response.data.Datos.Planilla.Semirremolque.Codigo}
                                            //}
                                            $scope.Planilla.Planilla = {
                                                Numero: response.data.Datos.Numero,
                                                Vehiculo: $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo),
                                                Ruta: RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.Ruta.Codigo, Estado: 1, Sync: true }).Datos[0],
                                                Semirremolque: { Codigo: response.data.Datos.Semirremolque.Codigo }
                                            }

                                            if ($scope.Planilla.Planilla.Vehiculo.TipoDueno.Codigo == 2102) {
                                                //$scope.Planilla.Numero = response.data.Datos.Planilla.Numero

                                                //$scope.ListaConductores = response.data.Datos.Conductores
                                                if (response.data.Datos.Conductores == null) {
                                                    response.data.Datos.Conductores = [];
                                                    response.data.Datos.Conductores.push(response.data.Datos.Conductor);
                                                }

                                                if (response.data.Datos.Conductores != null) {
                                                    if (response.data.Datos.Conductores.length > 0) {
                                                        for (var j = 0; j < response.data.Datos.Conductores.length; j++) {
                                                            var Conductor = $scope.CargarTercero(response.data.Datos.Conductores[j].Codigo)
                                                            if (response.data.Datos.ConductoresTrayectos != null) {
                                                                if (response.data.Datos.ConductoresTrayectos.length > 0) {
                                                                    response.data.Datos.ConductoresTrayectos.forEach(itemtrayecto => {
                                                                        if (itemtrayecto.Conductor.Codigo == Conductor.Codigo) {
                                                                            var RutaTrayectoConductor = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: itemtrayecto.RutaTrayecto.Codigo, Sync: true }).Datos
                                                                            Conductor.NombreCompleto = Conductor.NombreCompleto + ' - Trayecto : ' + RutaTrayectoConductor.Nombre
                                                                            Conductor.ValorAnticipos = itemtrayecto.ValorAnticipo
                                                                        }
                                                                    })


                                                                }
                                                            }
                                                            Conductor.ListadoPlanillaDespacho = [];
                                                            Conductor.ValorGastos = 0
                                                            //Conductor.ValorAnticipos = 0
                                                            Conductor.ValorReanticipos = 0
                                                            Conductor.SaldoConductor = 0
                                                            Conductor.SaldoEmpresa = 0

                                                            filtros = {
                                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                                Conductor: { Codigo: Conductor.Codigo },
                                                                Numero: response.data.Datos.Numero,/*response.data.Datos.Planilla.Numero*/
                                                                AplicaConsultaMaster: 1,
                                                                ConsultaLegalización: 1,
                                                                ConsultaLegalizaciónVariosConductores: 1,
                                                                Estado: { Codigo: 1 },
                                                                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                                                                Sync: true
                                                            };
                                                            blockUI.delay = 1000;
                                                            var responsePL = PlanillaDespachosFactory.Consultar(filtros)
                                                            if (responsePL.Datos.length > 0) {
                                                                Conductor.ListadoPlanillaDespacho = responsePL.Datos;
                                                                //Conductor.ListaPlanillaConceptos = []

                                                                var Data = []
                                                                filtros = {
                                                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                                    Codigo: responsePL.Datos[0].Ruta.Codigo,
                                                                    ConsultaLegalizacionGastosRuta: true,
                                                                    Sync: true
                                                                }
                                                                blockUI.delay = 1000;

                                                                var responseR = RutasFactory.Obtener(filtros)
                                                                if (responseR.ProcesoExitoso === true) {
                                                                    if (responseR.Datos.LegalizacionGastosRuta !== undefined) {
                                                                        for (var i = 0; i < responseR.Datos.LegalizacionGastosRuta.length; i++) {
                                                                            if (responseR.Datos.LegalizacionGastosRuta[i].TipoVehiculo.Codigo == responsePL.Datos[0].TipoVehiculoCodigo) {
                                                                                try {
                                                                                    var concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + responseR.Datos.LegalizacionGastosRuta[i].Concepto.Codigo)
                                                                                    //Data.push({ Concepto: concepto, Valor: Valor })
                                                                                    Data.push({ Concepto: concepto, Valor: 0 })
                                                                                } catch (e) {
                                                                                    console.log('No se pudo cargar el concepto : ' + responseR.Datos.LegalizacionGastosRuta[i].Concepto.Nombre + '. Error : '+e)
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }



                                                                // Añadir a los conceptos el valor total de los peajes:
                                                                if (response.data.Datos.ConductoresTrayectos != null) {
                                                                    if (response.data.Datos.ConductoresTrayectos.length > 0) {
                                                                        //Obtener Tipo de Vehículo y Número total de Ejes del Semirremolque de la Planilla:
                                                                        var responseVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 0, Placa: $scope.Planilla.Planilla.Vehiculo.Placa, Sync: true });
                                                                        var responseSemirremolque = {};
                                                                        var TotalEjes = 0;
                                                                        if (responseVehiculo.ProcesoExitoso == true) {

                                                                            if ($scope.Planilla.Planilla.Semirremolque.Codigo != 0) {
                                                                                responseSemirremolque = SemirremolquesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Planilla.Planilla.Semirremolque.Codigo, Sync: true });
                                                                                if (responseSemirremolque.ProcesoExitoso == true) {

                                                                                    // ejes del Semirremolque:
                                                                                    TotalEjes = responseSemirremolque.Datos.NumeroEjes;
                                                                                }
                                                                            }
                                                                            else {
                                                                                //TotalEjes = responseSemirremolque.Datos.NumeroEjes;
                                                                            }


                                                                            $scope.TotalPeajes = 0;


                                                                            //Consultar los peajes del trayecto del conductor:
                                                                            var RutaTrayectoTercero = 0;
                                                                            response.data.Datos.ConductoresTrayectos.forEach(itemtrayecto => {
                                                                                if (itemtrayecto.Conductor.Codigo == Conductor.Codigo) {
                                                                                    RutaTrayectoTercero = itemtrayecto.RutaTrayecto.Codigo
                                                                                }
                                                                            })
                                                                            if (RutaTrayectoTercero > 0) {
                                                                                RRuta = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: RutaTrayectoTercero, ConsultarPeajes: true, Sync: true });
                                                                                //then(function (response) {
                                                                                if (RRuta.ProcesoExitoso == true) {

                                                                                    var ListaTarifasPeajesRuta = [];
                                                                                    var PeajesRutasSize = RRuta.Datos.PeajeRutas.length;

                                                                                    $scope.ListadoPeajes = [];

                                                                                    //Por cada peaje, consultar las tarifas parametrizadas:
                                                                                    for (let i = 0; i < RRuta.Datos.PeajeRutas.length; i++) {
                                                                                        let NombrePeaje = RRuta.Datos.PeajeRutas[i].Peaje.Nombre;

                                                                                        var RPeaje = PeajesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: RRuta.Datos.PeajeRutas[i].Peaje.Codigo, ConsultarTarifasPeajes: true, Sync: true })
                                                                                        //then(function (response) {

                                                                                        //Comparar si el tipo de vehículo de la planilla, coincide con las tarifas parametrizadas en el peaje:
                                                                                        for (let j = 0; j < RPeaje.Datos.TarifasPeajes.length; j++) {
                                                                                            if (RPeaje.Datos.TarifasPeajes[j].TipoVehiculo.Codigo == responseVehiculo.Datos.TipoVehiculo.Codigo) {

                                                                                                // si coincide, verificar primero que la planilla no tenga semirremolque:
                                                                                                if (TotalEjes > 0) {
                                                                                                    //si lo tiene agregar el valor a los conceptos segun el número de ejes del semirremolque:
                                                                                                    if (Data[0] == undefined) {
                                                                                                        Data[0] = {
                                                                                                            Concepto: $scope.ListadoConceptos[0], Valor: '', Observaciones: '', Proveedor: '', Operacion: $scope.ListadoConceptos[0].Operacion, FechaFactura: $scope.Modelo.Fecha
                                                                                                        }

                                                                                                    }
                                                                                                    if (Data[0].Concepto == undefined || Data[0].Concepto == null) {
                                                                                                        if (TotalEjes == 1) {
                                                                                                            Data[0] = {
                                                                                                                Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(RPeaje.Datos.TarifasPeajes[j].ValorRemolque1Eje), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(RPeaje.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha
                                                                                                            }
                                                                                                        } else if (TotalEjes == 2) {
                                                                                                            Data[0] = {
                                                                                                                Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(RPeaje.Datos.TarifasPeajes[j].ValorRemolque2Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(RPeaje.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha
                                                                                                            }
                                                                                                        } else if (TotalEjes == 3) {
                                                                                                            Data[0] = {
                                                                                                                Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(RPeaje.Datos.TarifasPeajes[j].ValorRemolque3Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(RPeaje.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha
                                                                                                            }
                                                                                                        } else if (TotalEjes >= 4) {
                                                                                                            Data[0] = {
                                                                                                                Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(RPeaje.Datos.TarifasPeajes[j].ValorRemolque4Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(RPeaje.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha
                                                                                                            }
                                                                                                        }


                                                                                                    } else {

                                                                                                        if (TotalEjes == 1) {
                                                                                                            Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(RPeaje.Datos.TarifasPeajes[j].ValorRemolque1Eje), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(RPeaje.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha });
                                                                                                        } else if (TotalEjes == 2) {
                                                                                                            Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(RPeaje.Datos.TarifasPeajes[j].ValorRemolque2Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(RPeaje.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha });
                                                                                                        } else if (TotalEjes == 3) {
                                                                                                            Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(RPeaje.Datos.TarifasPeajes[j].ValorRemolque3Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(RPeaje.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha });
                                                                                                        } else if (TotalEjes >= 4) {
                                                                                                            Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(RPeaje.Datos.TarifasPeajes[j].ValorRemolque4Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(RPeaje.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha });
                                                                                                        }

                                                                                                    }
                                                                                                } else {
                                                                                                    //si no lo tiene, agregar el valor parametrizado por defecto:

                                                                                                    if (Data[0] == undefined) {
                                                                                                        Data[0] = {
                                                                                                            Concepto: $scope.ListadoConceptos[0], Valor: '', Observaciones: '', Proveedor: '', Operacion: $scope.ListadoConceptos[0].Operacion, FechaFactura: $scope.Modelo.Fecha
                                                                                                        }

                                                                                                    }

                                                                                                    if (Data[0].Concepto == undefined || Data[0].Concepto == null) {
                                                                                                        Data[0] = {
                                                                                                            Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(RPeaje.Datos.TarifasPeajes[j].Valor), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(RPeaje.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha
                                                                                                        }
                                                                                                    } else {
                                                                                                        Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(RPeaje.Datos.TarifasPeajes[j].Valor), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(RPeaje.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha });
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                        }
                                                                                        //ListaTarifasPeajesRuta.push(response.data.Datos);
                                                                                        // });



                                                                                    }


                                                                                }
                                                                                //});


                                                                            }
                                                                            //Fin Conceptos de Peajes Varios Conductores//

                                                                        }
                                                                    }
                                                                } else {
                                                                    //Obtener Tipo de Vehículo y Número total de Ejes del Semirremolque de la Planilla:
                                                                    var responseVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 0, Placa: $scope.Planilla.Planilla.Vehiculo.Placa, Sync: true });
                                                                    var responseSemirremolque = {};
                                                                    var TotalEjes = 0;
                                                                    if (responseVehiculo.ProcesoExitoso == true) {

                                                                        if ($scope.Planilla.Planilla.Semirremolque.Codigo != 0) {
                                                                            responseSemirremolque = SemirremolquesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Planilla.Planilla.Semirremolque.Codigo, Sync: true });
                                                                            if (responseSemirremolque.ProcesoExitoso == true) {

                                                                                // ejes del Semirremolque:
                                                                                TotalEjes = responseSemirremolque.Datos.NumeroEjes;
                                                                            }
                                                                        }
                                                                        else {
                                                                            //TotalEjes = responseSemirremolque.Datos.NumeroEjes;
                                                                        }


                                                                        $scope.TotalPeajes = 0;


                                                                        //Consultar los peajes de la ruta de la planilla:
                                                                        RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Planilla.Planilla.Ruta.Codigo, ConsultarPeajes: true }).
                                                                            then(function (response) {
                                                                                if (response.data.ProcesoExitoso == true) {

                                                                                    var ListaTarifasPeajesRuta = [];
                                                                                    var PeajesRutasSize = response.data.Datos.PeajeRutas.length;

                                                                                    $scope.ListadoPeajes = [];

                                                                                    //Por cada peaje, consultar las tarifas parametrizadas:
                                                                                    for (let i = 0; i < response.data.Datos.PeajeRutas.length; i++) {
                                                                                        let NombrePeaje = response.data.Datos.PeajeRutas[i].Peaje.Nombre;

                                                                                        PeajesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.PeajeRutas[i].Peaje.Codigo, ConsultarTarifasPeajes: true }).
                                                                                            then(function (response) {

                                                                                                //Comparar si el tipo de vehículo de la planilla, coincide con las tarifas parametrizadas en el peaje:
                                                                                                for (let j = 0; j < response.data.Datos.TarifasPeajes.length; j++) {
                                                                                                    if (response.data.Datos.TarifasPeajes[j].TipoVehiculo.Codigo == responseVehiculo.Datos.TipoVehiculo.Codigo) {

                                                                                                        // si coincide, verificar primero que la planilla no tenga semirremolque:
                                                                                                        if (TotalEjes > 0) {
                                                                                                            //si lo tiene agregar el valor a los conceptos segun el número de ejes del semirremolque:
                                                                                                            if (Data[0].Concepto == undefined || Data[0].Concepto == null) {
                                                                                                                if (TotalEjes == 1) {
                                                                                                                    Data[0] = {
                                                                                                                        Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque1Eje), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha
                                                                                                                    }
                                                                                                                } else if (TotalEjes == 2) {
                                                                                                                    Data[0] = {
                                                                                                                        Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque2Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha
                                                                                                                    }
                                                                                                                } else if (TotalEjes == 3) {
                                                                                                                    Data[0] = {
                                                                                                                        Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque3Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha
                                                                                                                    }
                                                                                                                } else if (TotalEjes >= 4) {
                                                                                                                    Data[0] = {
                                                                                                                        Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque4Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha
                                                                                                                    }
                                                                                                                }


                                                                                                            } else {

                                                                                                                if (TotalEjes == 1) {
                                                                                                                    Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque1Eje), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha });
                                                                                                                } else if (TotalEjes == 2) {
                                                                                                                    Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque2Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha });
                                                                                                                } else if (TotalEjes == 3) {
                                                                                                                    Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque3Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha });
                                                                                                                } else if (TotalEjes >= 4) {
                                                                                                                    Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].ValorRemolque4Ejes), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha });
                                                                                                                }

                                                                                                            }
                                                                                                        } else {
                                                                                                            //si no lo tiene, agregar el valor parametrizado por defecto:
                                                                                                            if (Data[0].Concepto == undefined || Data[0].Concepto == null) {
                                                                                                                Data[0] = {
                                                                                                                    Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].Valor), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha
                                                                                                                }
                                                                                                            } else {
                                                                                                                Data.push({ Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos, Valor: MascaraValores(response.data.Datos.TarifasPeajes[j].Valor), Observaciones: NombrePeaje, Proveedor: $scope.CargarTercero(response.data.Datos.Proveedor.Codigo), FechaFactura: $scope.Modelo.Fecha });
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                }
                                                                                                ListaTarifasPeajesRuta.push(response.data.Datos);
                                                                                            });



                                                                                    }


                                                                                }
                                                                            });



                                                                        //Fin Conceptos de Peajes//

                                                                    }
                                                                }




                                                                //Consultar Cuentas por cobrar pertenecientes al tercero(Conductor) de la planilla:
                                                                if ($scope.Sesion.UsuarioAutenticado.ManejoCuentasPorCobrarPagarLegalizacionesGastos) {
                                                                    
                                                                    var ResponseDocumentosCuentaporCobrarLegalizacion = DocumentoCuentasFactory.Consultar({
                                                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                                        Tercero: { Codigo: response.data.Datos.Conductores[j].Codigo },
                                                                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR,
                                                                        DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_MASIVO },
                                                                        CreadaManual: -1,
                                                                        Sync: true
                                                                    }).Datos;
                                                                    if (ResponseDocumentosCuentaporCobrarLegalizacion != undefined) {
                                                                        ResponseDocumentosCuentaporCobrarLegalizacion.forEach(item => {
                                                                            // se valida que aún tengan saldo
                                                                            if (item.Saldo > 0) {
                                                                                // por cada cuenta, se consulta en la tabla Detalle_Cruce_Documento_Cuentas si se han realizado abonos desde otras legalizaciones:
                                                                                var ResponseHistoricoAbonos = DocumentoCuentasFactory.ConsultarDetalleCruceDocumentos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: item.Codigo, Sync: true }).Datos
                                                                                var UltimoAbono = ''
                                                                                var Observaciones = ''

                                                                                if (ResponseHistoricoAbonos != undefined) {
                                                                                    if (ResponseHistoricoAbonos.length > 0) {
                                                                                        UltimoAbono = $linq.Enumerable().From(ResponseHistoricoAbonos).First('$.NumeroDocumentoOrigen==' + $linq.Enumerable().From(ResponseHistoricoAbonos).Max('$.NumeroDocumentoOrigen'))
                                                                                        Observaciones = item.Observaciones + '. Último Abono : Legalización No. ' + UltimoAbono.NumeroDocumentoOrigen + ', Valor : $' + MascaraValores(UltimoAbono.Valor)
                                                                                    } else {
                                                                                        Observaciones = item.Observaciones
                                                                                    }
                                                                                } else {
                                                                                    Observaciones = item.Observaciones
                                                                                }

                                                                                
                                                                                // se inserta el concepto en la lista:
                                                                                Data.push({
                                                                                    Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_SALDO_A_FAVOR_EMPRESA, Sync: true }).Datos,
                                                                                    Valor: MascaraValores(item.Saldo),
                                                                                    DocumentoCuenta: item.Codigo,
                                                                                    Observaciones: Observaciones
                                                                                });
                                                                   

                                                                                DocumentosCuentasAgregados = true;
                                                                            }
                                                                        });
                                                                    }
                                                                    //--Cuentas Por Cobrar Manuales (10014  - CONCEPTO CxC MANUAL)
                                                                    var resCXCManuales = CuentasPorCobrarFactory.Consultar({
                                                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                                        Tercero: { Codigo: response.data.Datos.Conductores[j].Codigo },
                                                                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR,
                                                                        DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_MASIVO },
                                                                        Estado: ESTADO_DEFINITIVO,
                                                                        Anulado: ESTADO_NO_ANULADO,
                                                                        CreadaManual: CODIGO_UNO,
                                                                        EstadoCuenta: CERO,
                                                                        Sync: true
                                                                    }).Datos;
                                                                    if (resCXCManuales != undefined) {
                                                                        for (var C = 0; C < resCXCManuales.length; C++) {
                                                                            item = resCXCManuales[C];
                                                                            var tmpData = {
                                                                                Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 10014, Sync: true }).Datos,
                                                                                Valor: MascaraValores(item.ValorTotal),
                                                                                DocumentoCuenta: item.Codigo,
                                                                                Observaciones: item.Observaciones
                                                                            };
                                                                            tmpData.Concepto.Nombre += item.DocumentoOrigen.Codigo == 2600 ? "" : " (" + item.NombreConcepto + ")";
                                                                            Data.push(tmpData);
                                                                            DocumentosCuentasAgregados = true;
                                                                        }
                                                                    }

                                                                    var ResponseDocumentosCuentaporPagarLegalizacion = DocumentoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: response.data.Datos.Conductores[j].Codigo }, TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR, DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_MASIVO }, CreadaManual: -1, Sync: true }).Datos;
                                                                    if (ResponseDocumentosCuentaporPagarLegalizacion != undefined) {
                                                                        ResponseDocumentosCuentaporPagarLegalizacion.forEach(item => {
                                                                            if (item.Saldo > 0) {

                                                                                var ResponseHistoricoAbonos = DocumentoCuentasFactory.ConsultarDetalleCruceDocumentos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: item.Codigo, Sync: true }).Datos
                                                                                var UltimoAbono = ''
                                                                                var Observaciones = ''
                                                                                
                                                                                if (ResponseHistoricoAbonos != undefined) {
                                                                                    if (ResponseHistoricoAbonos.length > 0) {
                                                                                        UltimoAbono = $linq.Enumerable().From(ResponseHistoricoAbonos).First('$.NumeroDocumentoOrigen==' + $linq.Enumerable().From(ResponseHistoricoAbonos).Max('$.NumeroDocumentoOrigen'))
                                                                                        Observaciones = item.Observaciones + '. Último Abono : Legalización No. ' + UltimoAbono.NumeroDocumentoOrigen + ', Valor : $' + MascaraValores(UltimoAbono.Valor)
                                                                                    } else {
                                                                                        Observaciones = item.Observaciones
                                                                                    }
                                                                                } else {
                                                                                    Observaciones = item.Observaciones
                                                                                }

                                                                                

                                                                                Data.push({
                                                                                    Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_SALDO_A_FAVOR_CONDUCTOR, Sync: true }).Datos,
                                                                                    Valor: MascaraValores(item.Saldo),
                                                                                    DocumentoCuenta: item.Codigo,
                                                                                    Observaciones: Observaciones
                                                                                });
                                                                               

                                                                                DocumentosCuentasAgregados = true;

                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                               

                                                                try {
                                                                    // Conceptos prima productividad y salario variable:
                                                                    var Valor = 0
                                                                    var tempc = {}
                                                                    var ListaConceptosSistemaAdicionales = [10011, 10012]
                                                                    ListaConceptosSistemaAdicionales.forEach(codigo => {
                                                                        tempc = ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: codigo, Sync: true }).Datos
                                                                        if (tempc != {} || tempc != undefined) {
                                                                            if (tempc.Estado == ESTADO_ACTIVO) {
                                                                                if (response.data.Datos.ConductoresTrayectos != null) {
                                                                                    // Se calcula el valor de estos conceptos de acuerdo al valor flete del trayecto y al porcentaje parametrizado en la modal de Trayectos en Rutas:
                                                                                    var ValorFlete = 0
                                                                                    response.data.Datos.ConductoresTrayectos.forEach(item => {
                                                                                        if (item.Conductor.Codigo == Conductor.Codigo) {
                                                                                            ValorFlete = item.ValorFlete
                                                                                        }
                                                                                    })
                                                                                    Valor = codigo == 10012 ? MascaraValores(Math.ceil((ValorFlete * tempc.Porcentaje) / 100)) : MascaraValores(Math.ceil((ValorFlete * tempc.Porcentaje) / 100))

                                                                                    Data.push({
                                                                                        Concepto: tempc,
                                                                                        Valor: Valor,
                                                                                        Observaciones: codigo == 10012 ? 'Valor pagar transportador : $' + MascaraValores(ValorFlete) + '. Porcentaje : ' + tempc.Porcentaje + '%' : 'Total flete : $' + MascaraValores(ValorFlete) + '. Porcentaje : ' + tempc.Porcentaje + '%',
                                                                                        Proveedor: $scope.CargarTercero(Conductor.Codigo)
                                                                                    });
                                                                                } else {
                                                                                    // Se calcula el valor de estos conceptos de acuerdo al valor flete transportador de la planilla y al porcentaje de estos conceptos:
                                                                                    Valor = codigo == 10012 ? MascaraValores(Math.ceil((response.data.Datos.ValorFleteTransportador * tempc.Porcentaje) / 100)) : MascaraValores(Math.ceil((response.data.Datos.ValorFleteTransportador * tempc.Porcentaje) / 100))

                                                                                    Data.push({
                                                                                        Concepto: tempc,
                                                                                        Valor: Valor,
                                                                                        Observaciones: codigo == 10012 ? 'Valor pagar transportador : $' + MascaraValores(response.data.Datos.ValorFleteTransportador) + '. Porcentaje : ' + tempc.Porcentaje + '%' : 'Total flete : $' + MascaraValores(response.data.Datos.ValorFleteTransportador) + '. Porcentaje : ' + tempc.Porcentaje + '%',
                                                                                        Proveedor: $scope.CargarTercero(Conductor.Codigo)
                                                                                    });
                                                                                }
                                                                            }
                                                                        }
                                                                    });
                                                                } catch (e) {
                                                                    console.log('Error en la consulta de los conceptos : ' + e)
                                                                }

                                                                try {
                                                                    //Calcular el concepto combustible según el tipo de combustible del vehículo:
                                                                    var rVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 0, Placa: $scope.Planilla.Planilla.Vehiculo.Placa, Sync: true }).Datos;
                                                                    var ResponseUltimoValorCombustible = ValorCombustibleFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoCombustible: { Codigo: rVehiculo.TipoCombustible.Codigo }, Sync: true }).Datos;
                                                                    var ResponseRendimientoGalon = RendimientoGalonCombustibleFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Ruta: { Codigo: $scope.Planilla.Planilla.Ruta.Codigo }, TipoVehiculo: { Codigo: rVehiculo.TipoVehiculo.Codigo }, Estado: ESTADO_ACTIVO, Sync: true });

                                                                    if (ResponseRendimientoGalon != undefined) {
                                                                        if (ResponseRendimientoGalon.Datos.length > 0) {
                                                                            filtros = {
                                                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                                                Codigo: $scope.Planilla.Planilla.Ruta.Codigo,
                                                                                Sync: true
                                                                            }
                                                                            //Se obtienen los kilómetros de la ruta 
                                                                            var responseR = RutasFactory.Obtener(filtros).Datos
                                                                            var ValorRendimientoGalon = ResponseRendimientoGalon.Datos[0].RendimientoGalon == undefined ? 0 : ResponseRendimientoGalon.Datos[0].RendimientoGalon;
                                                                            var mult = parseFloat(responseR.Kilometros / ValorRendimientoGalon).toFixed(2)
                                                                            // Se calcula el valor combustible(Rendimiento galón):
                                                                            var ValorConceptoCombustible = parseInt(mult * parseFloat(ResponseUltimoValorCombustible.Valor));
                                                                            if (ResponseUltimoValorCombustible != undefined && ResponseUltimoValorCombustible != null) {
                                                                                var tempc = ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 10003, Sync: true }).Datos
                                                                                Data.push({
                                                                                    Concepto: tempc,
                                                                                    Valor: MascaraValores(ValorConceptoCombustible),
                                                                                    Observaciones: '',
                                                                                    Proveedor: $scope.CargarTercero(1)
                                                                                });
                                                                            }
                                                                        }
                                                                    }
                                                                } catch (e) {
                                                                    console.log('Error en la consulta del concepto combustible(rendimiento galón) : ' + e)
                                                                }

                                                                //Los siguientes conceptos se insertan siempre y cuando el manejo de generación de cuentas por pagar por anulación de planilla esté activo:
                                                                if ($scope.Sesion.UsuarioAutenticado.CxCAnulacionPlanillaDespacho == true) {
                                                                    try {
                                                                        // Se buscan las cuentas por cobrar del conductor de la planilla que sean de documento origen anticipo masivo (2604):
                                                                        var ResponseListaPlanillasDocumentosCuentas = DocumentoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: response.data.Datos.Conductores[j].Codigo }, TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR, DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO }, Sync: true })
                                                                        if (ResponseListaPlanillasDocumentosCuentas != undefined) {
                                                                            if (ResponseListaPlanillasDocumentosCuentas.ProcesoExitoso == true) {
                                                                                if (ResponseListaPlanillasDocumentosCuentas.Datos.length > 0) {
                                                                                    //por cada cuenta se consulta que el documento origen  ya esté anulado:
                                                                                    ResponseListaPlanillasDocumentosCuentas.Datos.forEach(item => {
                                                                                        var ResponsePlanillaAnulada = PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroDocumento: item.CodigoDocumentoOrigen, Estado: { Codigo: 2 }, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Sync: true });
                                                                                        if (ResponsePlanillaAnulada.Datos.length > 0) {
                                                                                            // Si se encontró una planilla anulada se procede a verificar el histórico de abonos en la tabla Detalle_Cruce_Documento_Cuentas:
                                                                                            var ResponseHistoricoAbonos = DocumentoCuentasFactory.ConsultarDetalleCruceDocumentos({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: item.Codigo, Sync: true }).Datos
                                                                                            var UltimoAbono = ''
                                                                                            var Observaciones = ''

                                                                                            if (ResponseHistoricoAbonos != undefined) {
                                                                                                if (ResponseHistoricoAbonos.length > 0) {
                                                                                                    UltimoAbono = $linq.Enumerable().From(ResponseHistoricoAbonos).First('$.NumeroDocumentoOrigen==' + $linq.Enumerable().From(ResponseHistoricoAbonos).Max('$.NumeroDocumentoOrigen'))
                                                                                                    Observaciones = 'Egreso No.: ' + item.Numero + '. Planilla No. : ' + ResponsePlanillaAnulada.Datos[0].NumeroDocumento +  '. Último Abono : Legalización No. ' + UltimoAbono.NumeroDocumentoOrigen + ', Valor : $' + MascaraValores(UltimoAbono.Valor)
                                                                                                } else {
                                                                                                    Observaciones = 'Egreso No.: ' + item.Numero + '. Planilla No. : ' + ResponsePlanillaAnulada.Datos[0].NumeroDocumento 
                                                                                                }
                                                                                            } else {
                                                                                                Observaciones = 'Egreso No.: ' + item.Numero + '. Planilla No. : ' + ResponsePlanillaAnulada.Datos[0].NumeroDocumento 
                                                                                            }
                                                                                            // se inserta el concepto en la lista:
                                                                                            Data.push({
                                                                                                Concepto: ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: 10013, Sync: true }).Datos,
                                                                                                Valor: MascaraValores(item.Saldo),
                                                                                                Observaciones: Observaciones,
                                                                                                DocumentoCuenta: item.Codigo

                                                                                            });
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    console.log('NO se encontraron CxC relacionadas con anticipos de planillas para el conductor ' + response.data.Datos.Conductores[j].Nombre)
                                                                                }

                                                                            } else {
                                                                                console.log('Error en la consulta de los conceptos de CxC anulación planilla : ' + ResponseListaPlanillasDocumentosCuentas.MensajeOperacion)
                                                                            }
                                                                        } else {
                                                                            console.log('Error en la consulta de los conceptos de CxC anulación planilla.Consulte al administrador del sistema')
                                                                        }
                                                                    } catch (e) {
                                                                        console.log('Error en la consulta de los conceptos de CxC anulación planilla : ' + e)
                                                                    }
                                                                }


                                                                if (Data.length == 0) {
                                                                    Data.push({})
                                                                }
                                                                Conductor.Planilla = responsePL.Datos[0]
                                                                Data.forEach(item => {
                                                                    item.FechaFactura = $scope.Modelo.Fecha
                                                                })
                                                                Conductor.Data = Data

                                                            


                                                            }

                                                            var FiltroComprobantes = {
                                                                TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                                Estado: 1,
                                                                CodigoTercero: Conductor.Codigo,
                                                                CodigoLegalizacionGastos: 1,
                                                                Sync: true
                                                            }
                                                            var responseCO = DocumentoComprobantesFactory.Consultar(FiltroComprobantes)
                                                            Conductor.Comprobantes = []
                                                            if (responseCO.Datos.length > 0) {
                                                                for (var c = 0; c < responseCO.Datos.length; c++) {
                                                                    if (responseCO.Datos[c].DocumentoOrigen.Codigo == 2604 || responseCO.Datos[c].DocumentoOrigen.Codigo == 2605 || responseCO.Datos[c].DocumentoOrigen.Codigo == 2609) {
                                                                        if ((responseCO.Datos[c].DocumentoOrigen.Codigo == 2604 || responseCO.Datos[c].DocumentoOrigen.Codigo == 2605) && responseCO.Datos[c].NumeroDocumentoOrigen == $scope.Planilla.NumeroDocumento) {
                                                                            Conductor.Comprobantes.push(responseCO.Datos[c])
                                                                        } else if (responseCO.Datos[c].DocumentoOrigen.Codigo == 2609) {
                                                                            Conductor.Comprobantes.push(responseCO.Datos[c])
                                                                        }
                                                                    }

                                                                }

                                                            }
                                                            Conductor.Data.forEach(item => {
                                                                item.FechaFactura = $scope.Modelo.Fecha
                                                            })
                                                            $scope.ListadoConductoresLegalizacion.push(Conductor)

                                                            try {
                                                                $timeout(function () {
                                                                    $scope.Calcular();
                                                                }, 1000)
                                                            } catch (e) { }
                                                        }
                                                    }
                                                }
                                            } else {
                                                ShowError('La planilla tiene asociado un vehículo con tipo de dueño que no es propio');
                                                $scope.Planilla = {}
                                            }
                                        }
                                        else {
                                            ShowError('La planilla ingresada ya se encuentra asociada a otra legalización');
                                            $scope.Planilla = {}
                                        }
                                    }
                                    else {
                                        ShowError('No se encontro la planilla ingresada');
                                        $scope.Planilla = {}
                                    }
                                }
                                else {
                                    ShowError('No se encontro la planilla ingresada');
                                    $scope.Planilla = {}
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                                $scope.Planilla = {}
                            });
                    }

                    blockUI.stop();
                }
            }
        }

        $scope.CargarComprobantes = function (Conductor) {
            var FiltroComprobantes = {
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: 1,
                CodigoTercero: Conductor.Codigo,
                CodigoLegalizacionGastos: 1,
                Sync: true
            }
            var responseCO = DocumentoComprobantesFactory.Consultar(FiltroComprobantes)
            //Conductor.Comprobantes = []
            if (responseCO.Datos.length > 0) {
                for (var c = 0; c < responseCO.Datos.length; c++) {
                    if (responseCO.Datos[c].DocumentoOrigen.Codigo == 2604 || responseCO.Datos[c].DocumentoOrigen.Codigo == 2605 || responseCO.Datos[c].DocumentoOrigen.Codigo == 2609) {
                        var inserta = false
                        if ((responseCO.Datos[c].DocumentoOrigen.Codigo == 2604 || responseCO.Datos[c].DocumentoOrigen.Codigo == 2605) && responseCO.Datos[c].NumeroDocumentoOrigen == $scope.Planilla.NumeroDocumento) {
                            inserta = true
                        } else if (responseCO.Datos[c].DocumentoOrigen.Codigo == 2609) {
                            inserta = true
                        }
                    }
                    if (inserta) {
                        var cont = 0
                        if (Conductor.Comprobantes.length > 0) {
                            for (var i = 0; i < Conductor.Comprobantes.length; i++) {
                                if (Conductor.Comprobantes[i].Numero == responseCO.Datos[c].Numero) {
                                    cont++
                                }
                            }
                        }
                        if (cont == 0) {
                            Conductor.Comprobantes.push(responseCO.Datos[c])
                        }
                    }

                }

            }
            $scope.Calcular()
        }
        $scope.ListadoConductoresLegalizacion = []
        $scope.AgregarConductor = function (Obtener) {
            var cont = 0
            if ($scope.Conductor == undefined || $scope.Conductor == '') {
                ShowError('Debe seleccionar un Conductor')
            } else {
                for (var i = 0; i < $scope.ListadoConductoresLegalizacion.length; i++) {
                    if ($scope.Conductor.Codigo == $scope.ListadoConductoresLegalizacion[i].Codigo) {
                        cont++
                    }
                } if (cont > 0) {
                    ShowError('El Conductor seleccionado ya se encuentra asociado ')
                    $scope.Conductor = ''
                } else {
                    $scope.obtener = Obtener
                    $scope.Conductor.ListadoPlanillaDespacho = [];
                    $scope.Conductor.ValorGastos = 0
                    $scope.Conductor.ValorAnticipos = 0
                    $scope.Conductor.ValorReanticipos = 0
                    $scope.Conductor.SaldoConductor = 0
                    $scope.Conductor.SaldoEmpresa = 0

                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Conductor: { Codigo: $scope.Conductor.Codigo },
                        AplicaConsultaMaster: 1,
                        ConsultaLegalización: 1,
                        ConsultaLegalizaciónVariosConductores: 1,
                        Estado: { Codigo: 1 },
                        TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO
                    };
                    blockUI.delay = 1000;
                    PlanillaDespachosFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.Conductor.ListadoPlanillaDespacho = response.data.Datos;
                                    $scope.ListadoConductoresLegalizacion.push($scope.Conductor)
                                } else {
                                    ShowError('El conductor ingresado no tiene planillas asociadas')
                                    $scope.Conductor = ''
                                }
                            }
                        }, function (response) {
                            $scope.Buscando = false;
                        });
                }
                $scope.Planilla = ''
            }
        }

        $scope.EliminarComprobante = function (item, Comprobantes) {
            for (var i = 0; i < Comprobantes.length; i++) {
                if (Comprobantes[i].Numero == item.Numero) {
                    Comprobantes.splice(i, 1)
                    break;
                }
            }
        }
        $scope.ListadoConceptosCxC = []
        $scope.GuardarValorSaldoOriginal = function (item, index) {
            var Concidencias = 0
            if ($scope.ListadoConceptosCxC.length > 0) {
                $scope.ListadoConceptosCxC.forEach(itemConcepto => {
                    if (itemConcepto.index == index) {
                        itemConcepto.Valor = MascaraNumero(itemConcepto.Valor) < MascaraNumero(item.Valor) ? item.Valor : itemConcepto.Valor
                        Concidencias++;
                    }
                });
                if (Concidencias == 0) {
                    $scope.ListadoConceptosCxC.push({
                        index: index,
                        Valor: item.Valor
                    });
                }
            } else {
                $scope.ListadoConceptosCxC.push({
                    index: index,
                    Valor: item.Valor
                });
            }
        }


        //Validar Saldo CxC:
        $scope.ValidarSaldoConcepto = function (item, index) {
            $scope.ListadoConceptosCxC.forEach(itemConcepto => {

                if (itemConcepto.index == index) {
                    if (item.Concepto.Codigo == 10013) {
                        if (MascaraNumero(item.Valor) > MascaraNumero(itemConcepto.Valor)) {
                            ShowError('El valor del abono(' + item.Valor + '), no puede superar el valor del saldo actual(' + itemConcepto.Valor + ')')
                            item.Valor = itemConcepto.Valor
                        }
                    }
                }
            });

        }
    }]);