﻿SofttoolsApp.controller("ConsultarTareaMantenimientoEspecialCtrl", ['$scope', '$timeout', '$routeParams', 'blockUI', '$linq', 'EquipoMantenimientoFactory', 'TareaMantenimientoFactory', function ($scope, $timeout, $routeParams, blockUI, $linq, EquipoMantenimientoFactory, TareaMantenimientoFactory) {
    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
    $scope.Buscando = false;
    $scope.ResultadoSinRegistros = '';
    $scope.MensajesError = [];
    var filtros = {};
    $scope.ListadoEstados = [];
    $scope.totalRegistros = 0;
    $scope.paginaActual = 1;
    $scope.Nombre = '';
    $scope.cantidadRegistrosPorPagina = 10;
    $scope.totalPaginas = 0;
    $scope.MapaSitio = [{ Nombre: 'Mantenimiento' }, { Nombre: 'Documentos' }, { Nombre: 'Tareas Mantenimiento' }];
    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_TAREAS_MANTENIMIENTO);

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    // -------------------------- Constantes ---------------------------------------------------------------------//

    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };

    /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

    /*Cargar el combo de estados*/
    $scope.ListadoEstados = [
        { Codigo: -1, Nombre: '(NO APLICA)' },
        { Codigo: 1, Nombre: 'DEFINITIVO' },
        { Codigo: 0, Nombre: 'BORRADOR' },
    ];

    $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
    /*Cargar el combo de Equipo Mantenimiento*/
    EquipoMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoEquipoMantenimiento = [];
                $scope.ListadoEquipoMantenimiento = response.data.Datos;
                $scope.ListadoEquipoMantenimiento.push({ Nombre: '(TODOS)', Codigo: 0 });
                $scope.EquipoMantenimiento = $linq.Enumerable().From($scope.ListadoEquipoMantenimiento).First('$.Codigo == 0');
            }
        }, function (response) {
            ShowError(response.statusText);
        });

    

    /*---------------------------------------------------------------------Funcion Buscar Tareas Mantenimiento-----------------------------------------------------------------------------*/
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            $scope.Numero = '';
            Find()
        }
    };

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);

        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoTareasMantenimiento = [];
        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Numero: $scope.Numero,
            Nombre: $scope.Nombre,
            FechaInicial: $scope.FechaInicial,
            FechaFinal: $scope.FechaFinal,
            Estado: $scope.Estado.Codigo,
            EquipoMantenimiento: $scope.EquipoMantenimiento,
            Placa: $scope.ModeloPlaca,
            Pagina: $scope.paginaActual,
            RegistrosPagina: $scope.cantidadRegistrosPorPagina
        };

        //DatosRequeridos();

        if ($scope.MensajesError.length == 0) {
            blockUI.delay = 1000;

            TareaMantenimientoFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTareasMantenimiento = response.data.Datos

                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        else {
            $scope.Buscando = false;
        }
        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------Anular Contrato-----------------------------------------------------------*/

    /*Validacion boton anular */
    $scope.ConfirmacionAnularContrato = function (numero, EstadoDocumento) {
        $scope.estadoanulado = EstadoDocumento;
        $scope.NumeroAnulacion = numero
        if ($scope.ValidarPermisos.AplicaEliminarAnular == PERMISO_ACTIVO) {
            if ($scope.estadoanulado.Codigo !== 2) {
                showModal('modalConfirmacionAnularTarea');
            }
            else {
                closeModal('modalConfirmacionAnularTarea');
            }
        }
    };
    /*Funcion que solicita los datos de la anulación */
    $scope.SolicitarDatosAnulacion = function () {
        $scope.FechaAnulacion = new Date();
        closeModal('modalConfirmacionAnularTarea');
        showModal('modalDatosAnularTarea');
    };
    /*---------------------------------------------------------------------Funcion Anular Tarea Mantenimiento ----------------------------------------------------------------------------*/
    $scope.Anular = function () {

        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Numero: $scope.NumeroAnulacion,
            UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CausaAnulacion: $scope.CausaAnulacion,
        };

        TareaMantenimientoFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se anuló la tarea de mantenimiento  No.' + entidad.Numero);
                    closeModal('modalDatosAnularTarea');
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                ShowError(response.statusText);
            });
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    $scope.MaskNumero = function (option) {
        MascaraNumero(option)
    };
    $scope.MaskPlaca = function () {
        MascaraPlacaGeneral($scope)
    };

    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarTareasMantenimiento';
        }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Codigo = MascaraNumero($scope.Codigo) } catch (e) { }
    };
    $scope.urlASP = '';
    $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
    $scope.DesplegarInforme = function (Numero) {
        if ($scope.ValidarPermisos.AplicaImprimir == PERMISO_ACTIVO) {
            $scope.FiltroArmado = '';

            //Depende del listado seleccionado se enviará el nombre por parametro
            $scope.Numero = Numero;
            $scope.NombreReporte = 'RepTareaMantenimiento'
            //Arma el filtro
            $scope.ArmarFiltro();
            //Llama a la pagina ASP con el filtro por GET

            window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + 1);

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';
        };
    };

    // funcion enviar parametros al proyecto ASP armando el filtro
    $scope.ArmarFiltro = function () {
        $scope.FiltroArmado = '';
        if ($scope.Numero !== undefined && $scope.Numero !== '' && $scope.Numero !== null) {
            $scope.FiltroArmado += '&Numero=' + $scope.Numero;
        }
    }

    if ($routeParams.Codigo > 0) {
        $scope.Numero = $routeParams.Codigo;
        Find();
    }


    $scope.MaskMayus = function () {
        MascaraMayusGeneral($scope)
    }

    $scope.MaskPlaca = function () {
        MascaraPlacaGeneral($scope);
    }
}]);