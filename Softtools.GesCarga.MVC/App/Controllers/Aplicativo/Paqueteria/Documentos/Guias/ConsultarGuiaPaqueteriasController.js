﻿SofttoolsApp.controller("ConsultarGuiaPaqueteriasCtrl", ['$scope', '$timeout', 'TarifarioVentasFactory', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'ValorCatalogosFactory',
    'RemesaGuiasFactory', 'EmpresasFactory', 'OficinasFactory', 'RemesasFactory', 'PlanillaGuiasFactory', 'SemirremolquesFactory', 'RutasFactory', 'DocumentosFactory',
    function ($scope, $timeout, TarifarioVentasFactory, $linq, blockUI, blockUIConfig, $routeParams, ValorCatalogosFactory,
        RemesaGuiasFactory, EmpresasFactory, OficinasFactory, RemesasFactory, PlanillaGuiasFactory, SemirremolquesFactory, RutasFactory, DocumentosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Paqueteria' }, { Nombre: 'Documentos' }, { Nombre: 'Remesas' }];
        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.pref = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {} },
            Numeracion: ''
        };
        $scope.HabilitarEliminarDocumento = false;
        try {
            try {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GUIAS_PAQUETERIA_GESPHONE);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

            } catch (e) {

                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GUIAS_PAQUETERIA);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
            }
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        $scope.ListadoEstadoGuia = [
            { Codigo: -1, Nombre: '(TODOS)' }
        ]
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 60 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != 6000) {
                                $scope.ListadoEstadoGuia.push(response.data.Datos[i])
                            }
                        }
                    }
                    $scope.Modelo.EstadoRemesaPaqueteria = $scope.ListadoEstadoGuia[0]
                }
            }, function (response) {
            });

        $scope.ListadoOficinasActual = [
            { Codigo: 0, Nombre: '(TODAS)' }
        ];
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinasActual.push(item);
                    })
                    $scope.Modelo.OficinaActual = $scope.ListadoOficinasActual[0];
                    $scope.Modelo.Oficina = $scope.ListadoOficinasActual[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarGuiaPaqueterias';
            }
        };
        $scope.CopiarDocumento = function (Codigo) {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarGuiaPaqueterias/' + Codigo.toString() + ',1';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            if ($routeParams.Codigo > 0) {
                $scope.Modelo.NumeroInicial = $routeParams.Codigo;
                $scope.Modelo.NumeroFinal = $routeParams.Codigo;
                Find();
                $scope.Modelo.NumeroInicial = ''
                $scope.Modelo.NumeroFinal = ''
            }
        }
        /*Cargar el combo de ESTADO DE REMESAS PAQUETERIA*/

        $scope.ListadoEstadoRemesaPaqueteria = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 0, Nombre: 'BORRADOR' }, { Codigo: 1, Nombre: 'DEFINITIVO' }, { Codigo: 2, Nombre: 'ANULADO' }];
        $scope.ModeloEstado = $scope.ListadoEstadoRemesaPaqueteria[0];
        $scope.Modelo.FechaInicial = new Date();
        $scope.Modelo.FechaFinal = new Date();
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                && ($scope.Modelo.NumeroInicial === null || $scope.Modelo.NumeroInicial === undefined || $scope.Modelo.NumeroInicial === '' || $scope.Modelo.NumeroInicial === 0 || isNaN($scope.Modelo.NumeroInicial) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.Modelo.NumeroInicial !== null && $scope.Modelo.NumeroInicial !== undefined && $scope.Modelo.NumeroInicial !== '' && $scope.Modelo.NumeroInicial !== 0)
                || ($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')

            ) {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial
                    } else {
                        $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal
                    }
                }
            }
            return continuar
        }
        function Find() {

            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            if (DatosRequeridos()) {

                blockUI.delay = 1000;
                $scope.Modelo.Pagina = $scope.paginaActual
                $scope.Modelo.RegistrosPagina = 10
                $scope.Modelo.NumeroPlanilla = -1


                $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;

                if ($scope.ModeloEstado !== undefined && $scope.ModeloEstado !== null) {
                    $scope.Modelo.Estado = $scope.ModeloEstado.Codigo;
                } else {
                    $scope.Modelo.Estado = -1;
                }

                var filtro = angular.copy($scope.Modelo);
                filtro.Anulado = -1;


                RemesaGuiasFactory.Consultar(filtro).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.Modelo.Codigo = 0
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoGuias = response.data.Datos
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                            var listado = [];
                            response.data.Datos.forEach(function (item) {
                                listado.push(item);
                            });
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            blockUI.stop();
        }
        /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/
        $scope.EliminarTarifarioVentas = function (codigo, Nombre) {
            $scope.Codigo = codigo
            $scope.Nombre = Nombre
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarTarifarioVentas');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            TarifarioVentasFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se elimino la linea vehículos ' + $scope.Nombre);
                        closeModal('modalEliminarTarifarioVentas');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarTarifarioVentas');
                    $scope.ModalError = 'No se puede eliminar la linea vehículos ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };

        //Función que ejecuta el boton de vista preliminar
        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroFactura = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_GUIA;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';




        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroFactura != undefined && $scope.NumeroFactura != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroFactura;
            }

        }
        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }

        $scope.AnularDocumento = function (Numero, NumeroDocumento) {
            $scope.Numero = Numero

            $scope.ModalErrorCompleto = ''
            $scope.NumeroDocumento = NumeroDocumento
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnular');
        };

        $scope.Anular = function () {
            var aplicaplanilla = false
            var aplicaFactura = false
            var aplicaManifiesto = false

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnula: $scope.ModeloCausaAnula,
                Numero: $scope.Numero,

            };
            if (DatosRequeridosAnular()) {
                RemesasFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado > 0) {
                                ShowSuccess('Se anuló la remesa ' + $scope.NumeroDocumento);
                                closeModal('modalAnular', 1);
                                Find();
                            } else {
                                var mensaje = 'No se pudo anular la remesa ya que se encuentra relacionado con los siguientes documentos: '

                                var Facturas = '\nFacturas: '
                                var Planillas = '\nPlanillas: '
                                var Manifiestos = '\nManifiestos: '
                                for (var i = 0; i < response.data.Datos.DocumentosRelacionados.length; i++) {
                                    if (response.data.Datos.DocumentosRelacionados[i].FacturaVentas.Numero > 0) {
                                        Facturas += response.data.Datos.DocumentosRelacionados[i].FacturaVentas.Numero + ','
                                        aplicaFactura = true
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Planilla.Numero > 0) {
                                        Planillas += response.data.Datos.DocumentosRelacionados[i].Planilla.Numero + ','
                                        aplicaplanilla = true
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Manifiesto.Numero > 0) {
                                        Manifiestos += response.data.Datos.DocumentosRelacionados[i].Manifiesto.Numero + ','
                                        aplicaManifiesto = true
                                    }
                                }

                                if (aplicaplanilla) {
                                    mensaje += Planillas
                                }
                                if (aplicaFactura) {
                                    mensaje += Facturas
                                }
                                if (aplicaManifiesto) {
                                    mensaje += Manifiestos
                                }
                                ShowError(mensaje)
                            }

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = ''
                        showModal('modalMensajeAnularOrdenCargue');
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede anular la orden de servicio ' + $scope.NumeroDocumento;
                        $scope.ModalErrorCompleto = response.statusText
                    });
            }
        };
        $scope.ConsultarEstadosRemesas = function (item) {
            $scope.ListadoEstadoGuias = []
            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero
            }
            RemesaGuiasFactory.ConsultarEstadosRemesa(Filtro).
                then(function (response) {
                    console.log('llego', response)
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoEstadoGuias = response.data.Datos
                            showModal('ModalEstadosRemesas')
                        } else {
                            ShowError('No se encontro registro de estados de la remesa seleccionada')
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        $scope.ConsultarControlEntregas = function (item) {
            $scope.DocumentoCumplido = {
                Nombre: '',
                NombreDocumento: '',
                ExtensionDocumento: ''
            };

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero
            };
            RemesaGuiasFactory.ObtenerControlEntregas(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        //if (response.data.Datos.Remesa.NombreRecibe != null && response.data.Datos.Remesa.NumeroIdentificacionRecibe != null && response.data.Datos.Remesa.TelefonoRecibe != null) {
                            $scope.FotosEntrega = [];
                            $scope.FotosDevolucion = [];
                            //$scope.FotoControlEntrega = response.data.Datos.Remesa.Fotografia;
                            //$scope.FotoControlEntrega.FotoCargada = 'data:' + response.data.Datos.Remesa.Fotografia.TipoFoto + ';base64,' + response.data.Datos.Remesa.Fotografia.FotoBits;
                            $scope.FotosControlEntrega = response.data.Datos.Remesa.Fotografias;
                            $scope.FotosControlEntrega.forEach(foto => {
                                foto.FotoCargada = 'data:' + foto.TipoFoto + ';base64,' + foto.FotoBits;
                                if (foto.EntregaDevolucion == 1) {
                                    $scope.FotosDevolucion.push(foto);
                                } else {
                                    $scope.FotosEntrega.push(foto);
                                }
                            });
                            $scope.ControlEntregaRemesa = response.data.Datos.Remesa;
                            $scope.ControlEntregaRemesa.Latitud = response.data.Datos.Latitud;
                            $scope.ControlEntregaRemesa.Longitud = response.data.Datos.Longitud;
                            if (response.data.Datos.DocumentosCumplidoRemsa != undefined) {
                                
                                for (var i = 0; i < response.data.Datos.DocumentosCumplidoRemsa.length; i++) {
                                    var Documento = response.data.Datos.DocumentosCumplidoRemsa[i];
                                    $scope.DocumentoCumplido = {
                                        Nombre: Documento.NombreDocumento + "." + Documento.ExtensionDocumento,
                                        Numero: $scope.ControlEntregaRemesa.Numero,
                                        NombreDocumento: Documento.NombreDocumento,
                                        ExtensionDocumento: Documento.ExtensionDocumento,
                                        Id: Documento.Id,
                                        Temp: false
                                    };
                                    $scope.ModificaArchivo = false;
                                    break;
                                }
                            }
                            if (response.data.Datos.DocumentosCumplidoRemsa.length > 0) {
                                $scope.HabilitarEliminarDocumento = true;
                            } else {
                                $scope.HabilitarEliminarDocumento = false;
                            }
                            $scope.ControlEntregaRemesa.FirmaImg = "data:image/png;base64," + $scope.ControlEntregaRemesa.Firma;
                        console.log($scope.ControlEntregaRemesa.FirmaImg)

                        showModal('modalControlEntregas');
                        //}
                        //else {
                        //    ShowError('El documento no cuenta con un control de entregas');
                        //}
                    }
                    else {
                        ShowError('El documento no cuenta con un control de entregas');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.VerFotosEntrega = function () {
            showModal('modalFotosEntregas');
        }

        $scope.VerFotosDevolucion = function () {
            showModal('modalFotosDevolucion');
        }
        

        

        //------Gestion Adjunto Documento Cumplido
        $scope.ModificaArchivo = false;
        var regExt = /(?:\.([^.]+))?$/;
        $scope.DocumentoCumplido = {
            Nombre: '',
            NombreDocumento: '',
            ExtensionDocumento: ''
        };
        $scope.CadenaFormatos = ".pdf";
        $scope.CargarArchivo = function (element) {
            var continuar = true;
            if (element.files.length > 0) {
                //$scope.DocumentoCumplido = angular.element(this.DocumentoCumplido)[0];

                if (element === undefined) {
                    ShowError("Archivo invalido, por favor verifique el formato del archivo  /nFormatos permitidos: 0");
                    continuar = false;
                }
                if (continuar == true) {
                    if (element.files[0].size >= 3000000) //3 MB
                    {
                        ShowError("El archivo " + element.files[0].name + " supera los " + "3 MB" + ", intente con otro archivo", false);
                        continuar = false;
                    }
                    var Formatos = $scope.CadenaFormatos.split(',');
                    var cont = 0;
                    var ExtencionArchivo = regExt.exec(element.files[0].name)[1];
                    for (var i = 0; i < Formatos.length; i++) {
                        Formatos[i] = Formatos[i].replace(' ', '');
                        //var Extencion = element.files[0].name.split('.');
                        if ('.' + (ExtencionArchivo.toUpperCase()) == Formatos[i].toUpperCase()) {
                            cont++;
                        }
                    }
                    if (cont == 0) {
                        ShowError("Archivo invalido, por favor verifique el formato del archivo.  Formatos permitidos: (" + $scope.CadenaFormatos + ")");
                        continuar = false;
                    }
                }

                if (continuar == true) {
                    if ($scope.DocumentoCumplido.Archivo != undefined && $scope.DocumentoCumplido.Archivo != '' && $scope.DocumentoCumplido.Archivo != null) {
                        $scope.DocumentoCumplido.ArchivoTemporal = element.files[0];
                        showModal('modalConfirmacionRemplazarDocumentoCumplido');
                    } else {
                        var reader = new FileReader();
                        $scope.DocumentoCumplido.ArchivoCargado = element.files[0];
                        reader.onload = AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }
            }
        };

        $scope.RemplazarDocumentoCumplido = function () {
            var reader = new FileReader();
            $scope.DocumentoCumplido.ArchivoCargado = $scope.DocumentoCumplido.ArchivoTemporal;
            reader.onload = AsignarArchivo;
            reader.readAsDataURL($scope.DocumentoCumplido.ArchivoCargado);
            closeModal('modalConfirmacionRemplazarDocumentoCumplido');
        };

        function AsignarArchivo(e) {
            $scope.$apply(function () {
                $scope.DocumentoCumplido.Numero = $scope.ControlEntregaRemesa.Numero;//--Numero Remesa
                $scope.DocumentoCumplido.Archivo = e.target.result.replace('data:' + $scope.DocumentoCumplido.ArchivoCargado.type + ';base64,', '');
                $scope.DocumentoCumplido.Tipo = $scope.DocumentoCumplido.ArchivoCargado.type;
                $scope.DocumentoCumplido.Nombre = $scope.DocumentoCumplido.ArchivoCargado.name;
                $scope.DocumentoCumplido.NombreDocumento = $scope.DocumentoCumplido.ArchivoCargado.name.substr(0, $scope.DocumentoCumplido.ArchivoCargado.name.lastIndexOf('.'));
                $scope.DocumentoCumplido.ExtensionDocumento = regExt.exec($scope.DocumentoCumplido.ArchivoCargado.name)[1];
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Cargando Documento...");
                $timeout(function () {
                    blockUI.message("Cargando Documento...");
                    InsertarDocumentoCumplido();
                }, 100);
                //Bloqueo Pantalla
            });
        }

        function InsertarDocumentoCumplido() {
            // Guardar Archivo temporal
            var Documento = {};
            Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ControlEntregaRemesa.Numero,
                Nombre: $scope.DocumentoCumplido.NombreDocumento,
                Archivo: $scope.DocumentoCumplido.Archivo,
                Tipo: $scope.DocumentoCumplido.Tipo,
                Extension: $scope.DocumentoCumplido.ExtensionDocumento,
                CumplidoRemesa: true,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.InsertarTemporal(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {
                if (ResponseDocu.Datos > 0) {
                    $scope.DocumentoCumplido.Id = ResponseDocu.Datos;
                    $scope.DocumentoCumplido.Temp = true;
                    $scope.ModificaArchivo = true;
                    ShowSuccess('Se cargó el documento adjunto "' + $scope.DocumentoCumplido.NombreDocumento + '"');
                }
                else {
                    ShowError(ResponseDocu.MensajeOperacion);
                }
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
            blockUI.stop();
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
        }
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (item) {
            if (item.Id != undefined && item.Id != '' && item.Id != null) {
                if (item.Temp) {
                    window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=CumplidoRemesa&ID=' + item.Id + '&Temp=1' + '&ENRE_Numero=' + item.Numero);
                }
                else {
                    window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=CumplidoRemesa&ID=' + item.Id + '&ENRE_Numero=' + item.Numero + '&Temp=0');
                }
            }
            else {
                ShowError("Se debe seleccionar un archivo");
            }
        };
        $scope.GuardarArchivoCumplido = function (item) {
            if (item.Id != undefined && item.Id != '' && item.Id != null) {
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Guardando Documento...");
                $timeout(function () {
                    blockUI.message("Guardando Documento...");
                    TrasladarDocumentoCumplido(item);
                }, 100);
            //Bloqueo Pantalla
            }
        };
        $scope.EliminarArchivo = function (item) {
            Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.EliminarDocumentoCumplidoRemesa(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {
                
                ShowSuccess('Se eliminó el documento adjunto "' + $scope.DocumentoCumplido.NombreDocumento + '"');
                $scope.DocumentoCumplido.Nombre = '';
                $scope.HabilitarEliminarDocumento = false;
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
        }
        function TrasladarDocumentoCumplido(item) {
            var Documento = {};
            Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.InsertarDocumentosCumplidoRemesa(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {
                if (ResponseDocu.Datos > 0) {
                    $scope.DocumentoCumplido.Id = ResponseDocu.Datos;
                    $scope.DocumentoCumplido.Temp = false;
                    $scope.ModificaArchivo = false;
                    ShowSuccess('Se guardo el documento adjunto "' + $scope.DocumentoCumplido.NombreDocumento + '"');
                }
                else {
                    ShowError(ResponseDocu.MensajeOperacion);
                }
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
            blockUI.stop();
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
        }
        //------Gestion Adjunto Documento Cumplido



        $scope.AbrirMapa = function (lat, lng) {
            showModal('modalMostrarMapa');
            iniciarMapaGoogle(lat, lng);
        };
        function iniciarMapaGoogle(lat, lng) {
            var marker = [];
            var map, infoWindow;
            $scope.MapaLatitud = lat != '' ? lat : 0;
            $scope.MapaLongitud = lng != '' ? lng : 0;
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    };
                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.BOUNCE,
                    title: title
                    //icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };
                markers = new window.google.maps.Marker(markerOptions);
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input 
                    $scope.MapaLatitud = this.getPosition().lat();
                    $scope.MapaLongitud = this.getPosition().lng();
                });
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                if ($scope.MapaLatitud != undefined && $scope.MapaLongitud != undefined && $scope.MapaLatitud != 0 && $scope.MapaLongitud != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.MapaLatitud, $scope.MapaLongitud), 'Posición del usuario', 'Destino');
                } else {
                    $scope.MapaLatitud = posicion.coords.latitude;
                    $scope.MapaLongitud = posicion.coords.longitude;
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }
        $scope.AsignarPosicion = function () {
            $scope.Modelo.Remesa.Latitud = $scope.MapaLatitud;
            $scope.Modelo.Remesa.Longitud = $scope.MapaLongitud;
            closeModal('modalMostrarMapa');
        };

    }]);