﻿SofttoolsApp.controller("ConsultarPlanillaGuiasCtrl", ['$scope', '$timeout', 'TercerosFactory', 'DetallePlanillaDespachosFactory', 'NovedadesDespachosFactory',
    'DetalleNovedadesDespachosFactory', 'TarifarioVentasFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'PlanillaGuiasFactory',
    'PlanillaDespachosFactory', 'EmpresasFactory', 'OficinasFactory', 'blockUIConfig', 'ManifiestoFactory',
    function ($scope, $timeout, TercerosFactory, DetallePlanillaDespachosFactory, NovedadesDespachosFactory,
        DetalleNovedadesDespachosFactory, TarifarioVentasFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, PlanillaGuiasFactory,
        PlanillaDespachosFactory, EmpresasFactory, OficinasFactory, blockUIConfig, ManifiestoFactory) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Paqueteria' }, { Nombre: 'Planilla Despachos' }];
        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.CodigoLiquidacionPlanillaDetalle = 0;
        $scope.CodigoLiquidacionPlanilla = 0;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.numeroplanilla = 0;
        $scope.Remesa = []
        $scope.ListadoNovedadesDespachos = [];
        $scope.ListadoNumerosRemesas = [];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.Modal = {};
        $scope.Novedades = {
            NumeroPlanilla: 0,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        }
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Planilla: { Estado: {}, Ruta: {} }
        }
        $scope.pref = '';

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PLANILLA_DESPACHOS_PAQUETERIA); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        //Cargar combo de novedades despacho
        NovedadesDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListadoNovedadesDespachos = response.data.Datos;
                        $scope.Novedades.Novedad = $scope.ListadoNovedadesDespachos[$scope.ListadoNovedadesDespachos.length - 1];
                    } else {
                        $scope.ListadoNovedadesDespachos = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo de cliente*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, CadenaPerfiles: PERFIL_PROVEEDOR }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaProveedores = response.data.Datos;
                        $scope.Novedades.Proveedor = $scope.ListaProveedores[-1];
                    } else {
                        $scope.ListaProveedores = '';
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.ListadoOficinasActual = [
            { Codigo: 0, Nombre: '(TODAS)' }
        ];
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinasActual.push(item);
                    })
                    $scope.Modelo.Oficina = $scope.ListadoOficinasActual[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        EmpresasFactory.Consultar({ Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa}).then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.pref = response.data.Datos[0].Prefijo;
            }
        });
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarPlanillaGuias';
            }
        };
        $scope.CopiarDocumento = function (Codigo) {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarPlanillaGuias/' + Codigo.toString() + ',1';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            if ($routeParams.Codigo > 0) {
                $scope.Modelo.Planilla.Numero = $routeParams.Codigo;
                Find();
            } 
        }

        if ($scope.Sesion.Empresa.GeneraManifiesto === 1) {
            $scope.MostrarGenerarManifiesto = true;
        } else {
            $scope.MostrarGenerarManifiesto = false;
        }

        $scope.ListadoEstados = [
            { Nombre: '(NO APLICA)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: CERO },
            { Nombre: 'ANULADO', Codigo: 2 }
        ]

        $scope.Modelo.Planilla.Estado = $scope.ListadoEstados[CERO]
        $scope.FechaInicio = new Date();
        $scope.FechaFin = new Date();
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.Modelo.Planilla.Numero === null || $scope.Modelo.Planilla.Numero === undefined || $scope.Modelo.Planilla.Numero === '' || $scope.Modelo.Planilla.Numero === 0 || isNaN($scope.Modelo.Planilla.Numero) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.Modelo.Planilla.Numero !== null && $scope.Modelo.Planilla.Numero !== undefined && $scope.Modelo.Planilla.Numero !== '' && $scope.Modelo.Planilla.Numero !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            return continuar
        }
        function Find() {
            $scope.ListadoPlanilla = [];
            if (DatosRequeridos()) {
                blockUI.start('Buscando registros ...');

                $timeout(function () {
                    blockUI.message('Espere por favor ...');
                }, 100);
                $scope.Buscando = true;
                $scope.MensajesError = [];
                $scope.ListadoPlanilla = [];
                if ($scope.Buscando) {
                    if ($scope.MensajesError.length === 0) {

                        blockUI.delay = 1000;
                        $scope.Modelo.Pagina = $scope.paginaActual
                        $scope.Modelo.RegistrosPagina = 10
                        $scope.Modelo.TipoDocumento = 130
                        $scope.Modelo.FechaInicial = $scope.FechaInicio
                        $scope.Modelo.FechaFinal = $scope.FechaFin
                        PlanillaGuiasFactory.Consultar($scope.Modelo).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    $scope.Modelo.Codigo = 0
                                    if (response.data.Datos.length > 0) {
                                        $scope.ListadoPlanilla = response.data.Datos
                                        $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';
                                    }
                                    else {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                    var listado = [];
                                    response.data.Datos.forEach(function (item) {
                                        listado.push(item);
                                    });
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }
                blockUI.stop();
            }
        }
        /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/
        $scope.EliminarTarifarioVentas = function (codigo, Nombre) {
            $scope.Codigo = codigo
            $scope.Nombre = Nombre
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarTarifarioVentas');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            TarifarioVentasFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se elimino la linea vehículos ' + $scope.Nombre);
                        closeModal('modalEliminarTarifarioVentas');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarTarifarioVentas');
                    $scope.ModalError = 'No se puede eliminar la linea vehículos ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        }

        /*---------------------------------------------------------------------------------------FUNCIONES NUEVA NOVEDAD--------------------------------------------------------------------------------*/
        //funcion que muestra la modal novedades 
        $scope.AbrirNovedades = function (item) {
            $scope.DeshabilitarNumeroRemesa = true;
            $scope.DeshabilitarValorCliente = true;
            $scope.DeshabilitarValorCompra = true;
            $scope.NumeroDocumentoPlanilla = item.Planilla.NumeroDocumento
            $scope.Novedades.NumeroPlanilla = item.Planilla.Numero
            $scope.ResultadoSinRegistrosNovedad = '';

            //Cargar combo de numeros remesas de planilla
            $scope.Remesa = [];
            $scope.ListadoNumerosRemesas = [];
            DetallePlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroPlanilla: $scope.Novedades.NumeroPlanilla }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNumerosRemesas = response.data.Datos;
                            $scope.Remesa = $scope.ListadoNumerosRemesas[-1];
                            $scope.CodigoLiquidacionPlanillaDetalle = $scope.ListadoNumerosRemesas[0].NumeroLiquidaPlanilla
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            //funcion que consulta nueva novedad
            $scope.ListaNovedades = [];
            var filtroNovedades = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroPlanilla: $scope.Novedades.NumeroPlanilla
            }
            DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaNovedades = response.data.Datos
                        } else {
                            $scope.ResultadoSinRegistrosNovedad = 'No hay datos para mostrar';
                        }
                    }
                }, function (response) {
                });
            showModal('modalNovedades')
            $('#NuevaNovedad').hide(); $('#BotonNuevo').show()
        }

        //funcion que validad si la remesa ingresada se encuentra facturada, para permitir o no el ingreso del campo valor cliente
        $scope.VerificarNumeroFactura = function (remesa) {
            if (remesa !== "") {
                if (remesa.NumeroRemesa == undefined || remesa.NumeroRemesa == null || remesa.NumeroRemesa == '') {
                    $scope.Remesa = ''
                } else {
                    if (remesa.NumeroFacturaRemesa > 0) {
                        $scope.Remesa = '';
                        $scope.Novedades.ValorVenta = '';
                        ShowError('El número de remesa ingresado ya se encuentra facturado')
                    }
                }
            }
        }

        //funcion que validad la remesa ingresada
        $scope.CargarValidacionesNovedad = function (novedad) {
            if (novedad.Codigo !== undefined && novedad.Codigo !== 0) {
                $scope.DeshabilitarNumeroRemesa = false;
                $scope.DeshabilitarValorCompra = false;
                $scope.DeshabilitarValorCliente = false;
                $scope.CodigoFacturacion = '';
                $scope.CodigoLiquidacionPlanilla = '';
                filtrosObtener = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: novedad.Codigo,
                };

                blockUI.delay = 1000;
                NovedadesDespachosFactory.Obtener(filtrosObtener).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.CodigoNovedadConsultada = response.data.Datos.Codigo;
                            $scope.CodigoFacturacion = response.data.Datos.Conceptos.COVECodigo
                            $scope.CodigoLiquidacionPlanilla = response.data.Datos.Conceptos.CLPDCodigo
                        }
                        if ($scope.CodigoLiquidacionPlanilla != 0 && $scope.CodigoFacturacion != 0) {
                            $scope.DeshabilitarNumeroRemesa = false;
                            $scope.DeshabilitarValorCompra = false;
                            $scope.DeshabilitarValorCliente = false;
                        } else {
                            if ($scope.CodigoLiquidacionPlanilla != 0) {
                                $scope.DeshabilitarValorCompra = false;
                            } else {
                                $scope.DeshabilitarValorCompra = true;
                            }
                            if ($scope.CodigoFacturacion != 0) {
                                $scope.DeshabilitarValorCliente = false;
                            } else {
                                $scope.DeshabilitarValorCliente = true;
                            }
                            $scope.DeshabilitarNumeroRemesa = false;
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });

                blockUI.stop();
            } else {
                $scope.DeshabilitarNumeroRemesa = true;
                $scope.DeshabilitarValorCompra = true;
                $scope.DeshabilitarValorCliente = true;
            }
        }

        //funcion que inserta nueva novedad 
        $scope.InsertarNovedad = function () {
            $scope.ResultadoSinRegistrosNovedad = 'No hay datos para mostrar';
            if (DatosRequeridosModal()) {
                if ($scope.Remesa !== undefined && $scope.Remesa !== '' && $scope.Remesa !== null) {
                    $scope.Novedades.NumeroRemesa = $scope.Remesa.NumeroRemesa
                }
                DetalleNovedadesDespachosFactory.Guardar($scope.Novedades).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $('#NuevaNovedad').hide(500); $('#BotonNuevo').show(500)
                            ShowSuccess('Novedad agregada satisfactoriamente')
                            $scope.Remesa = $scope.ListadoNumerosRemesas[-1];
                            $scope.Novedades.Novedad = $scope.ListadoNovedadesDespachos[$scope.ListadoNovedadesDespachos.length - 1];
                            //funcion que consulta nueva novedad
                            $scope.ListaNovedades = [];
                            var filtroNovedades = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroPlanilla: $scope.Novedades.NumeroPlanilla
                            }
                            DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            $scope.ListaNovedades = response.data.Datos
                                        } else {
                                            $scope.ResultadoSinRegistrosNovedad = 'No hay datos para mostrar';
                                        }
                                    }
                                }, function (response) {
                                });
                            showModal('modalNovedades')
                            $('#NuevaNovedad').hide(); $('#BotonNuevo').show()
                        }
                    }, function (response) {
                    });
            }
        }

        function DatosRequeridosModal() {
            $scope.MensajesErrorModal = [];
            var continuar = true;
            if ($scope.Novedades.Novedad === undefined || $scope.Novedades.Novedad === '' || $scope.Novedades.Novedad === null || $scope.Novedades.Novedad.Codigo === 0) {
                $scope.MensajesErrorModal.push('Debe seleccionar la novedad');
                continuar = false;
            } else {
                if ($scope.CodigoLiquidacionPlanilla > 0 && $scope.CodigoFacturacion > 0) {
                    if (($scope.Novedades.ValorCompra === undefined || $scope.Novedades.ValorCompra === '' || $scope.Novedades.ValorCompra === null || $scope.Novedades.ValorCompra === 0) &&
                        ($scope.Novedades.ValorVenta === undefined || $scope.Novedades.ValorVenta === '' || $scope.Novedades.ValorVenta === null || $scope.Novedades.ValorVenta === 0)) {
                        $scope.MensajesErrorModal.push('Debe ingresar el valor transportador o el valor cliente');
                        continuar = false;
                    }
                } else {
                    if ($scope.CodigoLiquidacionPlanillaDetalle === 0) {
                        if ($scope.CodigoLiquidacionPlanilla > 0) {
                            if ($scope.Novedades.ValorCompra === undefined || $scope.Novedades.ValorCompra === '' || $scope.Novedades.ValorCompra === null || $scope.Novedades.ValorCompra === 0) {
                                $scope.MensajesErrorModal.push('Debe ingresar el valor transportador');
                                continuar = false;
                            }
                        }
                    }
                    if ($scope.CodigoFacturacion > 0) {
                        if ($scope.Remesa.NumeroFacturaRemesa === 0 || $scope.Remesa.NumeroFacturaRemesa === undefined) {
                            if ($scope.Remesa === undefined || $scope.Remesa === '' || $scope.Remesa === null || $scope.Remesa.NumeroRemesa === 0 || $scope.Remesa.NumeroRemesa === undefined || $scope.Remesa.NumeroRemesa === null) {
                                $scope.MensajesErrorModal.push('Debe ingresar el número de la remesa');
                                continuar = false;
                            }
                            if ($scope.Novedades.ValorVenta === undefined || $scope.Novedades.ValorVenta === '' || $scope.Novedades.ValorVenta === null || $scope.Novedades.ValorVenta === 0) {
                                $scope.MensajesErrorModal.push('Debe ingresar el valor cliente');
                                continuar = false;
                            }
                            if ($scope.Novedades.Proveedor.Codigo > 0) {
                                if ($scope.Novedades.ValorCosto > $scope.Novedades.ValorVenta) {
                                    $scope.MensajesErrorModal.push('El valor costo debe ser menor o igual al valor cliente');
                                    continuar = false;
                                }
                            }
                        }
                    }
                }
                if ($scope.Novedades.Proveedor.Codigo > 0) {
                    if ($scope.Novedades.ValorCosto === undefined || $scope.Novedades.ValorCosto === '' || $scope.Novedades.ValorCosto === null || $scope.Novedades.ValorCosto === 0) {
                        $scope.MensajesErrorModal.push('Debe ingresar el valor costo');
                        continuar = false;
                    }
                }
                if ($scope.DeshabilitarValorCompra === true && $scope.DeshabilitarValorCliente === true) {
                    $scope.MensajesErrorModal.push('No se pueden ingresar novedades');
                    continuar = false;
                }
            }
            return continuar;
        }

        //funcion que anula nueva novedad
        $scope.AnularNovedad = function (item) {
            $scope.CausaAnulacion = '';
            $scope.nombrenovedad = item.Novedad.Nombre
            $scope.numeroplanilla = item.NumeroPlanilla
            $scope.Modal.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            $scope.Modal.Codigo = item.Codigo
            $scope.FechaAnulacion = new Date()
            $scope.Modal.UsuarioAnula = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            showModal('modalDatosAnular')
        }

        $scope.Anular = function (causa) {
            if (causa === '' || causa === undefined || causa === null) {
                ShowError('Debe ingresar la causa de anulación de la novedad')
            }
            else {
                $scope.Modal.CausaAnulacion = causa
                DetalleNovedadesDespachosFactory.Anular($scope.Modal).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anulo la novedad.' + $scope.Modal.Codigo + ' - ' + $scope.nombrenovedad);
                            closeModal('modalDatosAnular');
                            Find();
                            $scope.ListaNovedades = [];
                            var filtroNovedades = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroPlanilla: $scope.numeroplanilla
                            }
                            DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            $scope.ListaNovedades = response.data.Datos
                                        }
                                    }
                                }, function (response) {
                                });
                            showModal('modalNovedades')
                            $('#NuevaNovedad').hide(); $('#BotonNuevo').show()
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        /*--------------------------------------------------GENERAR MANIFIESTO DE PAQUETERÍA------------------------------------------------*/
        $scope.GenerarManifiesto = function (item) {
            if ($scope.Sesion.Empresa.GeneraManifiesto === 1 && $scope.Sesion.UsuarioAutenticado.ManejoGenerarManifiestoPaqueteria) {
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Generando Manifiesto...");
                $timeout(function () {
                    blockUI.message("Generando Manifiesto...");
                    GuardarManifiesto(item);
                }, 100);
                //Bloqueo Pantalla
            }
            else {
                ShowError('No tiene permisos para generar manifiesto, contacte con administrador');
            }
        };
        function GuardarManifiesto(item) {
            var Manifiesto = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroPlanilla: item.Planilla.Numero,
                NumeroDocumentoPlanilla: item.Planilla.NumeroDocumento,
                Tipo_Documento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO },
                Usuario_Crea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                Tipo_Manifiesto: { Codigo: CODIGO_CATALOGO_TIPO_MANIFIESTO_PLANILLA_PAQUETERIA },
                Sync: true
            };
            var responseMani = ManifiestoFactory.Guardar(Manifiesto);
            if (responseMani.ProcesoExitoso == true) {
                if (responseMani.Datos > CERO) {
                    ShowSuccess('Se generó el manifiesto el número ' + responseMani.Datos);
                }
                else if (responseMani.Datos == -1) {
                    ShowError('La planilla ' + item.Planilla.NumeroDocuemnto + ' ya se encuentra asociada a un manifiesto');
                }
                else {
                    ShowError(response.statusText);
                }
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
            }
            else {
                ShowError(response.statusText);
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
            }
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }
        $scope.AnularDocumento = function (Numero, NumeroDocumento) {
            $scope.Numero = Numero

            $scope.ModalErrorCompleto = ''
            $scope.NumeroDocumento = NumeroDocumento
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnular');
        };

        $scope.Anular = function () {
            var aplicaLiquidaciones = false
            var aplicaCumplidos = false

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.ModeloCausaAnula,
                Numero: $scope.Numero

            };
            if (DatosRequeridosAnular()) {
                PlanillaGuiasFactory.Anular(entidad). 
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado > 0) {
                                ShowSuccess('Se anuló la planilla ' + $scope.NumeroDocumento);
                                closeModal('modalAnular');
                                Find();
                            } else {
                                var mensaje = 'No se pudo anular la planilla ya que se encuentra relacionado con los siguientes documentos: '

                                var Liquidaciones = '\nLiquidaciones: '
                                var Cumplidos = '\nCumplidos: '
                                for (var i = 0; i < response.data.Datos.DocumentosRelacionados.length; i++) {
                                    if (response.data.Datos.DocumentosRelacionados[i].Cumplido.Numero > 0) {
                                        Cumplidos += response.data.Datos.DocumentosRelacionados[i].Cumplido.Numero + ','
                                        aplicaCumplidos = true
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Liquidacion.Numero > 0) {
                                        Liquidaciones += response.data.Datos.DocumentosRelacionados[i].Liquidacion.Numero + ','
                                        aplicaLiquidaciones = true
                                    }

                                }

                                if (aplicaLiquidaciones) {
                                    mensaje += Liquidaciones
                                }
                                if (aplicaCumplidos) {
                                    mensaje += Cumplidos
                                }

                                ShowError(mensaje)
                            }

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = ''
                        showModal('modalMensajeAnularOrdenCargue');
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede anular la orden de servicio ' + $scope.NumeroDocumento;
                        $scope.ModalErrorCompleto = response.statusText
                    });
            }


        };
        //-----------------------------------------------Reportes-----------------------------------------------//
        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroReporte = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_PLDP;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.ArmarFiltro = function () {
            if ($scope.NumeroReporte != undefined && $scope.NumeroReporte != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroReporte;
            }
        };

        $scope.DesplegarInformeCargue = function (Numero) {
            var urlASP = '';
            var OpcionPDf = 1;
            urlASP = ObtenerURLProyectoASP(urlASP);
            var NombreReporte = NOMBRE_REPORTE_PLCARGUE;
            window.open(urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + NombreReporte + '&Numero=' + Numero + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
        };
        //-----------------------------------------------Reportes-----------------------------------------------//
    }]);