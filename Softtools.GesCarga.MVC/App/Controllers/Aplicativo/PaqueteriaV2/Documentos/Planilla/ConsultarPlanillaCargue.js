﻿SofttoolsApp.controller("ConsultarPlanillaCargueCtrl", ['$scope', '$timeout', 'TercerosFactory', 'DetallePlanillaDespachosFactory', 'NovedadesDespachosFactory',
    'DetalleNovedadesDespachosFactory', 'TarifarioVentasFactory', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'PlanillaPaqueteriaFactory', 'EmpresasFactory',
    'OficinasFactory', 'VehiculosFactory', 'RutasFactory', 'ValidacionPlanillaDespachosFactory',
    function ($scope, $timeout, TercerosFactory, DetallePlanillaDespachosFactory, NovedadesDespachosFactory,
        DetalleNovedadesDespachosFactory, TarifarioVentasFactory, $linq, blockUI, blockUIConfig, $routeParams, PlanillaPaqueteriaFactory, EmpresasFactory,
        OficinasFactory, VehiculosFactory, RutasFactory, ValidacionPlanillaDespachosFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CONSULTAR PLANILLA CARGUE';
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Planilla Cargue' }];

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.PLANILLA_CARGUE_DESCARGUE);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        var filtros = {};
        $scope.CodigoLiquidacionPlanillaDetalle = 0;
        $scope.CodigoLiquidacionPlanilla = 0;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.numeroplanilla = 0;
        $scope.Remesa = [];
        $scope.ListadoNovedadesDespachos = [];
        $scope.ListadoNumerosRemesas = [];
        $scope.Modal = {};
        $scope.Novedades = {
            NumeroPlanilla: 0,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
        };
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Planilla: {
                Numero: '',
                NumeroManifiesto: ''
            },
            Oficina: ''
        };
        $scope.pref = '';
        $scope.ListadoOficinasActual = [];
        $scope.ListadoPlanilla = [];
        $scope.ListaConductor = [];
        $scope.ListaProveedor = [];
        $scope.ListaVehiculos = [];
        $scope.ListaRutas = [];
        $scope.ListadoEstados = [
            { Nombre: '(NO APLICA)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];


        $scope.Modelo.Estado = $scope.ListadoEstados[CERO];
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicial = new Date();
            $scope.FechaFinal = new Date();
        }
        if ($scope.Sesion.Empresa.GeneraManifiesto === 1) {
            $scope.MostrarGenerarManifiesto = true;
        } else {
            $scope.MostrarGenerarManifiesto = false;
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------Autocomplete
        //----Conductor
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CONDUCTOR,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaConductor = ValidarListadoAutocomplete(Response.Datos, $scope.ListaConductor);
                }
            }
            return $scope.ListaConductor;
        };
        //----Conductor
        //----Proveedor
        $scope.AutocompleteProveedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_PROVEEDOR,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaProveedor = ValidarListadoAutocomplete(Response.Datos, $scope.ListaProveedor);
                }
            }
            return $scope.ListaProveedor;
        };
        //----Proveedor
        //----Vehiculo
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListaVehiculos);
                }
            }
            return $scope.ListaVehiculos;
        };
        //----Vehiculo
        //----Rutas
        $scope.AutocompleteRutas = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = RutasFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Nombre: value,
                        Estado: ESTADO_ACTIVO,
                        Sync: true
                    });
                    $scope.ListaRutas = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRutas);
                }
            }
            return $scope.ListaRutas;
        };
        //----Rutas
        //----------Autocomplete
        //----------Init
        $scope.InitLoad = function () {
            //--Novedades despacho
            NovedadesDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.push({ Nombre: '', Codigo: 0 });
                            $scope.ListadoNovedadesDespachos = response.data.Datos;
                            $scope.Novedades.Novedad = $scope.ListadoNovedadesDespachos[$scope.ListadoNovedadesDespachos.length - 1];
                        } else {
                            $scope.ListadoNovedadesDespachos = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            //--Oficina
            //--Oficinas
            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ListadoOficinasActual.push({ Codigo: -1, Nombre: '(TODAS)' });
                            response.data.Datos.forEach(function (item) {
                                $scope.ListadoOficinasActual.push(item);
                            });
                            $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinasActual).First('$.Codigo === -1');
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                $scope.ListadoOficinasActual.push({
                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
                });
                $scope.Modelo.Oficina = $scope.ListadoOficinasActual[0];
            }
            //--Empresa
            EmpresasFactory.Consultar({ Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.pref = response.data.Datos[0].Prefijo;
                }
            });
            //--Obtener Parametros
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
               
                if ($routeParams.Numero > 0) {
                    $scope.Modelo.Numero = $routeParams.Numero; 
                    PantallaBloqueo(Find);
                    
                }
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            PantallaBloqueo(Find);
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            PantallaBloqueo(Find);
        };
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        //--Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarPlanillaCargue';
            }
        };
        $scope.CopiarDocumento = function (Codigo) {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarPlanillaGuias/' + Codigo.toString() + ',1';
            }
        };
        //--Funcion Buscar
        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }

        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO && DatosRequeridos()) {
                $scope.Codigo = 0;
                PantallaBloqueo(Find);
            }
        };
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicial === null || $scope.FechaInicial === undefined || $scope.FechaInicial === '')
                && ($scope.FechaFinal === null || $scope.FechaFinal === undefined || $scope.FechaFinal === '')
                && ($scope.Modelo.Planilla.Numero === null || $scope.Modelo.Planilla.Numero === undefined || $scope.Modelo.Planilla.Numero === '' || $scope.Modelo.Planilla.Numero === 0 || isNaN($scope.Modelo.Planilla.Numero) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.Modelo.Planilla.Numero !== null && $scope.Modelo.Planilla.Numero !== undefined && $scope.Modelo.Planilla.Numero !== '' && $scope.Modelo.Planilla.Numero !== 0)
                || ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                || ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')

            ) {
                if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                    && ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')) {
                    if ($scope.FechaFechaFinalFin < $scope.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if ((($scope.FechaFinal - $scope.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }

                else {
                    if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                        $scope.FechaFinal = $scope.FechaInicial;
                    } else {
                        $scope.FechaInicial = $scope.FechaFinal;
                    }
                }
            }
            return continuar;
        }
        function Find() {
            
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoPlanilla = [];
          

            if ($scope.Buscando) { 
                var Planilla = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    FechaInicial: $scope.FechaInicial,
                    FechaFinal: $scope.FechaFinal,
                    Estado: $scope.Modelo.Estado.Codigo,
                    Oficina: { Codigo: $scope.Modelo.Oficina.Codigo },
                    NumeroDocumento: $scope.Modelo.Numero,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_CARGUE,
                    NumeroPlanilla: $scope.NumeroPlanilla,
                    Vehiculo: { Codigo: $scope.Modelo.Vehiculo == undefined ? 0 : $scope.Modelo.Vehiculo.Codigo },
                    Conductor: { Codigo: $scope.Modelo.Conductor == undefined ? 0 : $scope.Modelo.Conductor.Codigo },
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas
                }; 
                ValidacionPlanillaDespachosFactory.Consultar(Planilla).
                    then(function (response) {
                        
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoPlanilla = response.data.Datos;
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }

                            $scope.Modelo.Numero = '';
                        }

                    }, function (response) {
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                        ShowError(response.statusText);
                    });

            }

            
        }
        //--Funcion Buscar


        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo
            };

            TarifarioVentasFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se elimino la linea vehículos ' + $scope.Nombre);
                        closeModal('modalEliminarTarifarioVentas');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = '';
                    result = InternalServerError(response.statusText);
                    showModal('modalMensajeEliminarTarifarioVentas');
                    $scope.ModalError = 'No se puede eliminar la linea vehículos ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText;

                });

        };
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true;
        };
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        };




        $scope.Anular = function (causa) {
            if (causa === '' || causa === undefined || causa === null) {
                ShowError('Debe ingresar la causa de anulación de la novedad');
            }
            else {
                $scope.Modal.CausaAnulacion = causa;
                DetalleNovedadesDespachosFactory.Anular($scope.Modal).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anulo la novedad.' + $scope.Modal.Codigo + ' - ' + $scope.nombrenovedad);
                            closeModal('modalDatosAnular');
                            Find();
                            $scope.ListaNovedades = [];
                            var filtroNovedades = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroPlanilla: $scope.numeroplanilla
                            };
                            DetalleNovedadesDespachosFactory.Consultar(filtroNovedades).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            $scope.ListaNovedades = response.data.Datos;
                                        }
                                    }
                                }, function (response) {
                                });
                            showModal('modalNovedades')
                            $('#NuevaNovedad').hide(); $('#BotonNuevo').show();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        //---GENERAR MANIFIESTO DE PAQUETERÍA
        //----Anular
        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }
        $scope.AnularDocumento = function (Numero, NumeroDocumento) {
            $scope.Numero = Numero

            $scope.ModalErrorCompleto = ''
            $scope.NumeroDocumento = NumeroDocumento
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnular');
        };
        $scope.Anular = function () {
            var aplicaLiquidaciones = false
            var aplicaCumplidos = false

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.ModeloCausaAnula,
                Numero: $scope.Numero

            };
            if (DatosRequeridosAnular()) {
                ValidacionPlanillaDespachosFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado > 0) {
                                ShowSuccess('Se anuló la planilla ' + $scope.NumeroDocumento);
                                closeModal('modalAnular');
                                Find();
                            } else {
                                var mensaje = 'No se pudo anular la planilla ya que se encuentra relacionado con los siguientes documentos: '



                                ShowError(mensaje)
                            }

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {

                    });
            }


        };
        //----Anular
        //----Reportes
        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroReporte = Numero;
            $scope.NombreReporte = 'repPlanillaCargueDescargue';

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroReporte != undefined && $scope.NumeroReporte != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroReporte;
            }
        };
        $scope.DesplegarInformeCargue = function (Numero) {
            var urlASP = '';
            var OpcionPDf = 1;
            urlASP = ObtenerURLProyectoASP(urlASP);
            var NombreReporte = NOMBRE_REPORTE_PLCARGUE;
            window.open(urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + NombreReporte + '&Numero=' + Numero + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
        };
        //----Reportes
        //--Mascaras
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item);
        };
    }]);