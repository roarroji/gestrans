﻿SofttoolsApp.controller("GestionarPlanillaCargueCtrl", ['$scope', '$linq', '$timeout', 'blockUI', 'blockUIConfig', 'ValidacionPlanillaDespachosFactory', '$routeParams', 'PlanillaDespachosFactory', 'PlanillaGuiasFactory', 'RemesaGuiasFactory', 'PlanillaPaqueteriaFactory', 'VehiculosFactory', 'OficinasFactory',
    function ($scope, $linq, $timeout, blockUI, blockUIConfig, ValidacionPlanillaDespachosFactory, $routeParams, PlanillaDespachosFactory, PlanillaGuiasFactory, RemesaGuiasFactory, PlanillaPaqueteriaFactory, VehiculosFactory, OficinasFactory) {
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Planilla Cargue' }, { Nombre: 'Gestionar' }];

        //Declaración Variables:

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.VALIDAR_PROCESO_CARGUE)
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta página');
            document.location.href = '#';
        }

        $scope.PlacaVehiculo = '';
        $scope.NombreConductor = '';
        $scope.NombreTenedor = '';
        $scope.NombreRuta = '';
        $scope.PlacaSemirremolque = '';
        $scope.MostrarSeccionGuias = false;
        $scope.Planilla = [];
        $scope.ListadoGuias = [];
        $scope.MensajesError = [];
        $scope.MensajesConfirmacion = [];
        $scope.NumeroPlanilla = '';
        $scope.NumeroDocumentoPlanilla = '';
        $scope.Observaciones = '';
        $scope.Cantidad = '';
        $scope.Peso = 0;
        $scope.CodigoPlanilla = '';
        $scope.Deshabilitar = false;
        $scope.Fecha = new Date();
        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: 'BORRADOR' },
            { Codigo: 1, Nombre: 'DEFINITIVO' }
        ];

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==0');
        $scope.Oficina = $scope.Sesion.UsuarioAutenticado.Oficinas;
        //Métodos:

        $scope.AutocompleteVehiculos = function (value) {
            $scope.ListaVehiculos = [];
            var Lista = VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, Sync: true, Estado: ESTADO_ACTIVO }).Datos;
            return Lista;
        }

        $scope.ObtenerVehiculo = function () {
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            $scope.MostrarSeccionGuias = false;
            if ($scope.PlacaVehiculo != undefined && $scope.PlacaVehiculo != null && $scope.PlacaVehiculo != '') {

                var ResponseVehiculo = '';

                ResponseVehiculo = $scope.CargarVehiculosPlaca($scope.PlacaVehiculo.Placa);
                if (ResponseVehiculo != undefined && ResponseVehiculo != '') {

                    $scope.NombreConductor = $scope.CargarTercero(ResponseVehiculo.Conductor.Codigo).NombreCompleto;
                    $scope.NombreTenedor = $scope.CargarTercero(ResponseVehiculo.Tenedor.Codigo).NombreCompleto;

                    $scope.MostrarSeccionGuias = true;
                } else {

                    $scope.MensajesError.push('El vehículo ingresado no es válido');
                    $scope.PlacaVehiculo = '';
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                }

            }
        }
        $scope.ValidarEtiqueta = function () {
            $scope.MensajesError = []; 
            if ($scope.NumeroEtiqueta != undefined && $scope.NumeroEtiqueta != null &&
                $scope.NumeroEtiqueta != '' && !isNaN($scope.NumeroEtiqueta)) {
                var Etiqueta = ''
                Etiqueta = RemesaGuiasFactory.ConsultarGuiasEtiquetasPreimpresas({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroInicial: $scope.NumeroEtiqueta, Estado: ESTADO_ACTIVO, Sync: true, NumeroPlanilla: -1 }).Datos[0];
                 
                if (Etiqueta != '' && Etiqueta != undefined) { 
                    console.log('lista actual', $scope.ListadoGuias);
                    var countListadoGuias = 0;
                    $scope.ListadoGuias.forEach(itemGuia => {
                        if (itemGuia.NumeroUnidad == $scope.NumeroEtiqueta) {
                            countListadoGuias++;
                        }
                    });

                    if (countListadoGuias == 0) {
                        $scope.ListadoGuias.push({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            NumeroRemesa: Etiqueta.Remesa.Numero,
                            NumeroDocumentoRemesa: Etiqueta.Remesa.NumeroDocumento,
                            NumeroUnidad: $scope.NumeroEtiqueta,
                            Peso: 0,
                            ObservacionesVerificacion: ''
                        }); 
                    } else {

                        $scope.MensajesError.push('La Etiqueta ingresada ya se encuentra en la planilla')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);

                    }
                      

                } else {
                  
                    $scope.MensajesError.push('La Etiqueta ingresada no es válida')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                }
            } else {

                console.log('etiqueta 0 ', Etiqueta)

                $scope.MensajesError.push('la  Etiqueta  ingresada no es válida')
                $("#window").animate({
                    scrollTop: $('.breadcrumb').offset().top
                }, 200);
            } 
        } 

        $scope.ValidarGuia = function () { 
            var cont = 0;
            $scope.MensajesError = [];
            if ($scope.NumeroGuia != undefined && $scope.NumeroGuia != null && $scope.NumeroGuia != '' && !isNaN($scope.NumeroGuia)) {
                var Guia = ''
                Guia = RemesaGuiasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroInicial: $scope.NumeroGuia, Estado: ESTADO_ACTIVO, Sync: true, NumeroPlanilla: -1 }).Datos[0];
                 if (Guia != '' && Guia != undefined) {
                    var countListadoGuias = 0;
                     $scope.ListadoGuias.forEach(itemGuia => {
                         if (itemGuia.NumeroDocumentoRemesa == $scope.NumeroGuia
                             && itemGuia.NumeroUnidad < 999) {
                            countListadoGuias++;

                        }
                        console.log('recorrido', countListadoGuias)
                    });

                    var ResponseGuia = ''
                    ResponseGuia = RemesaGuiasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: Guia.Remesa.Numero, Sync: true }).Datos;
                    console.log(Guia.Remesa)
                    if (ResponseGuia != '' && ResponseGuia != undefined) {
                        var ProcesoGuias = false;
                        if (ResponseGuia.OficinaActual.Codigo == $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo) {
                            ProcesoGuias = true;
                        } else if ($scope.TipoPlanilla.Codigo == CODIGO_TIPO_DOCUMENTO_PLANILLA_CARGUE) {
                            ProcesoGuias = false;

                            $scope.MensajesError.push('La guía ingresada no se encuentra en esta oficina')
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);

                        } else {
                            ProcesoGuias = true;
                        } 
                        if (ProcesoGuias) {
                            var NumeroUnidad = 1;
                            if (countListadoGuias == 0) {
                                if (ResponseGuia.ManejaDetalleUnidades > 0) {
                                    if ($scope.ListadoGuias.length > 0) {
                                        console.log('contada', countListadoGuias)
                                        ResponseGuia.DetalleUnidades.forEach(item => {
                                            $scope.ListadoGuias.push({
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                NumeroRemesa: Guia.Remesa.Numero,
                                                NumeroDocumentoRemesa: Guia.Remesa.NumeroDocumento,
                                                NumeroUnidad: NumeroUnidad,
                                                Peso: item.Peso,
                                                ObservacionesVerificacion: ''
                                            });
                                            NumeroUnidad++;
                                        });
                                        $scope.CalcularTotales();

                                    } else {
                                        ResponseGuia.DetalleUnidades.forEach(item => {
                                            $scope.ListadoGuias.push({
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                NumeroRemesa: Guia.Remesa.Numero,
                                                NumeroDocumentoRemesa: Guia.Remesa.NumeroDocumento,
                                                NumeroUnidad: NumeroUnidad,
                                                Peso: item.Peso,
                                                ObservacionesVerificacion: ''
                                            });

                                            NumeroUnidad++;
                                        });
                                        $scope.CalcularTotales();
                                    }
                                }
                                else {

                                    $scope.ListadoGuias.push({
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        NumeroRemesa: Guia.Remesa.Numero,
                                        NumeroDocumentoRemesa: Guia.Remesa.NumeroDocumento,
                                        NumeroUnidad: NumeroUnidad,
                                        Peso: ResponseGuia.Remesa.PesoCliente,
                                        ObservacionesVerificacion: ''
                                    });

                                    $scope.CalcularTotales();
                                }
                            }
                            else { 

                                $scope.MensajesError.push('La guía ingresada ya se encuentra verificada')
                                $("#window").animate({
                                    scrollTop: $('.breadcrumb').offset().top
                                }, 200);
                            }

                            }

                    }
                } else {
                    $scope.MensajesError.push('La guía ingresada no es válida')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                }
            } else {
                $scope.MensajesError.push('La guía ingresada no es válida')
                $("#window").animate({
                    scrollTop: $('.breadcrumb').offset().top
                }, 200);
            }
        }

        $scope.CalcularTotales = function () {
            $scope.Cantidad = '';
            $scope.Peso = 0
            $scope.Cantidad = $scope.ListadoGuias.length;
            $scope.ListadoGuias.forEach(item => {
                $scope.Peso += item.Peso
            });
        }


        $scope.TmpEliminarGuia = {};
        $scope.ConfirmarEliminarGuia = function (item) {
            $scope.TmpEliminarGuia = { Indice: item.indice };
            showModal('modalEliminarGuia');
        };

        $scope.EliminarGuia= function (indice) {
            $scope.ListadoGuias.splice(indice, 1); 
            closeModal('modalEliminarGuia');
        };


        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardarCargue');
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarCargue'); 
            if ($scope.DatosRequeridos()) {
                var Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.CodigoPlanilla,
                    Vehiculo: { Codigo: $scope.PlacaVehiculo.Codigo },
                    TipoVerificacion: { codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_CARGUE },
                    Observaciones: $scope.Observaciones,
                    Cantidad: $scope.Cantidad,
                    Peso: $scope.Peso,
                    Estado: $scope.Estado.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    ListadoGuias: $scope.ListadoGuias,
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo }
                }
                $scope.MensajesConfirmacion = [];
                $scope.MensajesError = []
                ValidacionPlanillaDespachosFactory.Guardar(Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            $scope.MensajesConfirmacion.push('Se han verificado ' + $scope.ListadoGuias.length + ' guías de la planilla ' + $scope.NumeroPlanilla);
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);
                            $scope.MostrarSeccionGuias = false;
                            $scope.ListadoGuias = [];
                            $scope.NumeroDocumentoPlanilla = ''; 
                            $scope.NumeroGuia = '';
                            $scope.PlacaVehiculo = '';
                            $scope.NombreConductor = '';
                            $scope.NombreTenedor = '';
                            $scope.NombreRuta = '';
                            $scope.PlacaSemirremolque = '';
                            $scope.FechaSalida = '';
                            $scope.HoraSalida = '';

                            if ($scope.NumeroPlanilla > 0) {
                                ShowSuccess('Se modificó la planilla con el número ' + response.data.Datos);
                                document.location.href = '#!ConsultarPlanillaCargue/' + response.data.Datos
                                $scope.NumeroPlanilla = '';
                            } else {
                                ShowSuccess('Se guardó la planilla con el número ' + response.data.Datos);
                                 document.location.href = '#!ConsultarPlanillaCargue/' + response.data.Datos
                            }

                        } else {
                            $scope.MensajesError.push(response.data.MensajeOperacion);
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);
                        }
                    });
            }
        }

        $scope.DatosRequeridos = function () {
            var continuar = true;
            if ($scope.ListadoGuias != undefined && $scope.ListadoGuias != null && $scope.ListadoGuias != '') {
                if ($scope.ListadoGuias.length == 0) {
                    $scope.MensajesError.push('debe existir por lo menos una guía verificada')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                    continuar = false;
                }
            } else {
                $scope.MensajesError.push('debe existir por lo menos una guía verificada')
                $("#window").animate({
                    scrollTop: $('.breadcrumb').offset().top
                }, 200);
            }
            return continuar;
        }

        function Obtener() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.CodigoPlanilla
            };
            ValidacionPlanillaDespachosFactory.Obtener(filtros).
                then(function (response) {
                    console.log('llegaron',response)
                    if (response.data.ProcesoExitoso === true) {
                        $scope.NumeroPlanilla = response.data.Datos.NumeroDocumento;
                        $scope.TipoPlanilla = CODIGO_TIPO_DOCUMENTO_PLANILLA_CARGUE
                        $scope.Fecha = new Date(response.data.Datos.Fecha);
                        $scope.PlacaVehiculo = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo);
                        $scope.ObtenerVehiculo();
                        $scope.Observaciones = response.data.Datos.Observaciones;  
                        response.data.Datos.ListadoGuias.forEach(item => {
                            $scope.ListadoGuias.push({
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroRemesa: item.NumeroRemesa,
                                NumeroDocumentoRemesa: item.NumeroDocumentoRemesa,
                                NumeroUnidad: item.NumeroUnidad,
                                Peso: item.PesoRemesa,
                                ObservacionesVerificacion: item.Observaciones
                            });
                        });

                        $scope.CalcularTotales();

                        $scope.Oficina = OficinasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.Oficina.Codigo, Sync: true }).Datos;
                        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + response.data.Datos.Estado);

                        if ($scope.Estado.Codigo == 1) {
                            $scope.Deshabilitar = true
                        }
                        blockUI.stop();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    blockUI.stop();
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } document.location.href = '#!ConsultarPlanillaCargue'; }, 500);
                });
        }

        $scope.VolverMaster = function () {
            if ($scope.NumeroPlanilla > 0) {
                document.location.href = '#!ConsultarPlanillaCargue/' + $scope.NumeroPlanilla;
            } else {
                document.location.href = '#!ConsultarPlanillaCargue';
            }
        }

        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope);
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        }

        if ($routeParams.Numero > 0) {
            $scope.CodigoPlanilla = $routeParams.Numero
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Obteniendo Documento...");
            $timeout(function () { blockUI.message("Obteniendo Documento..."); Obtener(); }, 100);
        }


    }
]);