﻿SofttoolsApp.controller("GestionarPlanillaDescargueCtrl", ['$scope', '$linq', '$timeout', 'blockUI', 'blockUIConfig', 'ValidacionPlanillaDespachosFactory', '$routeParams', 'PlanillaDespachosFactory', 'PlanillaGuiasFactory', 'RemesaGuiasFactory', 'PlanillaPaqueteriaFactory', 'VehiculosFactory', 'OficinasFactory', 'TercerosFactory', 'SemirremolquesFactory','CiudadesFactory',
    function ($scope, $linq, $timeout, blockUI, blockUIConfig, ValidacionPlanillaDespachosFactory, $routeParams, PlanillaDespachosFactory, PlanillaGuiasFactory, RemesaGuiasFactory, PlanillaPaqueteriaFactory, VehiculosFactory, OficinasFactory, TercerosFactory, SemirremolquesFactory, CiudadesFactory) {
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Planilla  Descargue' }, { Nombre: 'Gestionar' }];

        //Declaración Variables:

        $scope.PlacaVehiculo = '';
        $scope.NombreConductor = '';
        $scope.NombreTenedor = '';
        $scope.NombreRuta = '';
        $scope.PlacaSemirremolque = '';
        $scope.MostrarSeccionGuias = false;
        $scope.Planilla = [];
        $scope.ListadoGuias = [];
        $scope.ListadoGuiasActuales = [];
        $scope.MensajesError = [];
        $scope.MensajesConfirmacion = [];
        $scope.NumeroPlanilla = '';
        $scope.NumeroDocumentoPlanilla = '';
        $scope.Observaciones = '';
        $scope.Cantidad = '';
        $scope.Peso = 0;
        $scope.CantidadGuias = 0
        $scope.CodigoPlanilla = '';
        var tmparr = [];
        $scope.totalUnidades = 0
        $scope.IngresadaActual = 0
        $scope.Deshabilitar = false;
        $scope.Fecha = new Date();
        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: 'BORRADOR' },
            { Codigo: 1, Nombre: 'DEFINITIVO' }
        ];

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==0');
        $scope.Oficina = $scope.Sesion.UsuarioAutenticado.Oficinas;
        //Métodos:

        $scope.AutocompleteVehiculos = function (value) {
            $scope.ListaVehiculos = [];
            var Lista = VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, Sync: true, Estado: ESTADO_ACTIVO }).Datos;
            return Lista;
        }

        $scope.ObtenerVehiculo = function () {
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            $scope.MostrarSeccionGuias = false;
            if ($scope.PlacaVehiculo != undefined && $scope.PlacaVehiculo != null && $scope.PlacaVehiculo != '') {

                var ResponseVehiculo = '';

                ResponseVehiculo = $scope.CargarVehiculosPlaca($scope.PlacaVehiculo.Placa);

                if (ResponseVehiculo != undefined && ResponseVehiculo != '') {

                    var ResponsListaNegra = VehiculosFactory.ConsultarEstadoListaNegra({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: ResponseVehiculo.Codigo,
                        Sync: true
                    });

                    if (ResponsListaNegra.ProcesoExitoso == false) {
                        if (VehiculosFactory.ConsultarDocumentosSoatRTM({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Codigo: ResponseVehiculo.Codigo,
                            Sync: true
                        }).ProcesoExitoso) {
                            if (ResponseVehiculo.Estado.Codigo == 0) {
                                ShowError("El vehículo " + ResponseVehiculo.Placa + " esta inactivo por motivo '" + ResponseVehiculo.JustificacionBloqueo + "'");
                            }

                        }
                        else {
                            ShowError("El vehículo tiene documentos proximos a vencerse");
                            ResponseVehiculo = '';
                        }
                    }
                    else {
                        ShowError("El vehículo se encuentra mal matriculado ante el RNDC");
                        ResponseVehiculo = '';
                    }


                    $scope.Semiremolque = ResponseVehiculo.Semirremolque.Placa
                    $scope.NombreConductor = $scope.CargarTercero(ResponseVehiculo.Conductor.Codigo);
               
                    $scope.DocumentoConductor =  ResponseVehiculo.Conductor.Identificacion;
                    
                    $scope.NombreTenedor = $scope.CargarTercero(ResponseVehiculo.Tenedor.Codigo).NombreCompleto;

                    $scope.MostrarSeccionGuias = true;
                } else {

                    $scope.MensajesError.push('El vehículo ingresado no es válido');
                    $scope.PlacaVehiculo = '';
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                }

            }
        }


        $scope.ObternerVehiculo = function (CodigoAlternoVehiculo) {
            if (CodigoAlternoVehiculo != "" && CodigoAlternoVehiculo != undefined && CodigoAlternoVehiculo != null) {
                VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, CodigoAlterno: CodigoAlternoVehiculo }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                //$scope.Modelo.Planilla.Vehiculo = response.data.Datos[0];
                                $scope.PlacaVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos[0].Codigo, Sync: true }).Datos;
                                
                            }
                            else {
                                $scope.CodigoAlternoVehiculo = "";
                            }
                        }
                        else {
                            $scope.CodigoAlternoVehiculo = "";
                        }
                    }, function (response) {
                    });
            }
        };

        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                    
                }
            }
            return $scope.ListadoConductores
        }

        SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSemirremolques = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoSemirremolques = response.data.Datos;
                        try {
                            if ($scope.Modelo.Semirremolque.Codigo > 0) {
                                $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListadoSemirremolques).First('$.Codigo ==' + $scope.Semirremolque.Codigo);
                            }
                        } catch (e) {
                            $scope.Semirremolque = ''
                        }
                    }
                    else {
                        $scope.ListadoSemirremolques = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.ValidarEtiqueta = function () {
            $scope.MensajesError = [];
            if ($scope.NumeroEtiqueta != undefined && $scope.NumeroEtiqueta != null &&
                $scope.NumeroEtiqueta != '' && !isNaN($scope.NumeroEtiqueta)) {
                var Etiqueta = ''
                Etiqueta = RemesaGuiasFactory.ConsultarGuiasEtiquetasPreimpresas({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroInicial: $scope.NumeroEtiqueta, Estado: ESTADO_ACTIVO, Sync: true, NumeroPlanilla: -1 }).Datos[0];
                console.log('Etiqueta', Etiqueta)
                if (Etiqueta != '' && Etiqueta != undefined) {
                    var countListadoGuias = 0;
                    $scope.ListadoGuias.forEach(itemGuia => {
                        if (itemGuia.NumeroUnidad == $scope.NumeroEtiqueta) {
                            countListadoGuias++;
                        }
                    });
                    if (countListadoGuias == 0) {
                        $scope.Peso = Etiqueta.Remesa.PesoCliente
                        $scope.ListadoGuias.push({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            NumeroRemesa: Etiqueta.Remesa.Numero,
                            NumeroDocumentoRemesa: Etiqueta.Remesa.NumeroDocumento,
                            NumeroUnidad: $scope.NumeroEtiqueta,
                            Peso: Etiqueta.Remesa.PesoCliente / Etiqueta.Remesa.CantidadCliente,
                            PesoVolumetrico: Etiqueta.Remesa.PesoVolumetricoCliente / Etiqueta.Remesa.CantidadCliente,
                            UnidadesPendientes: Etiqueta.Remesa.CantidadCliente,
                            UnidadesActuales: Etiqueta.Remesa.CantidadCliente,
                            ObservacionesVerificacion: '',
                            CiudadDestino: Etiqueta.Remesa.CiudadDestinatario.Nombre,
                            faltaUnidad: false,
                            st:''
                        }); 

                        $scope.IngresadaActual = $scope.NumeroGuia;
                        $scope.NumeroEtiqueta = ''
                            $scope.NumeroGuia = '';
                        $scope.CalcularTotales();
                    } else {

                        $scope.MensajesError.push('La Etiqueta ingresada ya se encuentra en la planilla')
                        $("#window").animate({
                            scrollTop: $('.breadcrumb').offset().top
                        }, 200);

                    }


                } else {
                    $scope.MensajesError.push('La etiqueta ingresada no es válida')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                }
            } else {

            }

        }
 

        $scope.ValidarGuia = function () {
            var cont = 0;
            $scope.MensajesError = [];
            if ($scope.NumeroGuia != undefined && $scope.NumeroGuia != null && $scope.NumeroGuia != '' && !isNaN($scope.NumeroGuia)) {
                var Guia = ''
                Guia = RemesaGuiasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroInicial: $scope.NumeroGuia, Estado: ESTADO_ACTIVO, Sync: true, NumeroPlanilla: -1 }).Datos[0];
                if (Guia != '' && Guia != undefined) {
                    var countListadoGuias = 0;
                    $scope.ListadoGuias.forEach(itemGuia => {

                        if (itemGuia.NumeroDocumentoRemesa == $scope.NumeroGuia
                            && itemGuia.NumeroUnidad == $scope.UnidadGuia) {
                            countListadoGuias++;
                        }
                     
                    });

                    var ResponseGuia = ''
                    ResponseGuia = RemesaGuiasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: Guia.Remesa.Numero, Sync: true }).Datos;

                    if (ResponseGuia != '' && ResponseGuia != undefined) {
                        var ProcesoGuias = false;
                        if (ResponseGuia.OficinaActual.Codigo == $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo) {
                            ProcesoGuias = true;
                        } else if ($scope.TipoPlanilla.Codigo == CODIGO_TIPO_DOCUMENTO_PLANILLA_DESCARGUE) {
                            ProcesoGuias = false;

                            $scope.MensajesError.push('La guía ingresada no se encuentra en esta oficina')
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);

                        } else {
                            ProcesoGuias = true;
                        }

                      
                        if (ProcesoGuias) {
                            var NumeroUnidad = 1;
                            if (countListadoGuias == 0) {
                         
                                if (ResponseGuia.ManejaDetalleUnidades > 0) {
                                    if ($scope.ListadoGuias.length > 0) {
                                        console.log('contada', countListadoGuias)
                                        if ($scope.UnidadGuia == undefined || $scope.UnidadGuia == '') {

                                            $scope.unicos = []; 
                                            ResponseGuia.DetalleUnidades.forEach(item => {
                                                $scope.ListadoGuias.push({
                                                    ID: item.ID,
                                                    CodigoEmpresa:$scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                    NumeroRemesa: Guia.Remesa.Numero,
                                                    NumeroDocumentoRemesa: Guia.Remesa.NumeroDocumento,
                                                    NumeroUnidad: NumeroUnidad,
                                                    Peso: item.Peso,
                                                    PesoVolumetrico: item.PesoVolumetrico,
                                                    ObservacionesVerificacion: '',
                                                    CiudadDestino: Guia.Remesa.CiudadDestinatario.Nombre,
                                                    faltaUnidad: false,
                                                    st: ''
                                                });

                                                NumeroUnidad++;
                                            });  

                                            var tmparr = [];
                                            for (var i = 0; i < $scope.ListadoGuias.length; i++) {
                                                var existe = false;
                                                for (var j = 0; j < tmparr.length; j++) {
                                                    if ($scope.ListadoGuias[i].ID == tmparr[j].ID) {
                                                        existe = true;
                                                        break;
                                                    }
                                                }
                                                if (existe == false) {
                                                    tmparr.push($scope.ListadoGuias[i]);
                                                }

                                            }

                                            $scope.ListadoGuias = tmparr;


                                        } else {

                                            $scope.ListadoGuias.push({
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                ID: ResponseGuia.DetalleUnidades[$scope.UnidadGuia - 1].ID, NumeroRemesa: Guia.Remesa.Numero,
                                                UnidadesActuales: ResponseGuia.DetalleUnidades.length,
                                                UnidadesPendientes: ResponseGuia.DetalleUnidades.length,
                                                UnidadesActuales: ResponseGuia.DetalleUnidades.length,
                                                NumeroDocumentoRemesa: Guia.Remesa.NumeroDocumento,
                                                NumeroUnidad: $scope.UnidadGuia,
                                                Peso: Guia.Remesa.PesoCliente / ResponseGuia.DetalleUnidades.length,
                                                PesoVolumetrico: Guia.Remesa.PesoVolumetricoCliente,
                                                ObservacionesVerificacion: '',
                                                CiudadDestino: Guia.Remesa.CiudadDestinatario.Nombre,
                                                st: ''
                                            }); 
                                        }

                                        $scope.Cantidad = $scope.ListadoGuias.length;
                                        $scope.IngresadaActual = $scope.NumeroGuia;
                                        $scope.CalcularTotales();
                                        $scope.NumeroGuia = '';
                                        $scope.UnidadGuia= '';

                                    } else {
                                        if ($scope.UnidadGuia == undefined || $scope.UnidadGuia == '') {
                                            ResponseGuia.DetalleUnidades.forEach(item => {
                                                $scope.ListadoGuias.push({
                                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                    ID: ResponseGuia.DetalleUnidades[$scope.UnidadGuia - 1].ID,
                                                    UnidadesPendientes: ResponseGuia.DetalleUnidades.length,
                                                    UnidadesActuales: ResponseGuia.DetalleUnidades.length,
                                                    NumeroRemesa: Guia.Remesa.Numero,
                                                    NumeroDocumentoRemesa: Guia.Remesa.NumeroDocumento,
                                                    NumeroUnidad: NumeroUnidad,
                                                    Peso: item.Peso,
                                                    PesoVolumetrico: item.PesoVolumetrico,
                                                    ObservacionesVerificacion: '',
                                                    CiudadDestino: Guia.Remesa.CiudadDestinatario.Nombre,
                                                    faltaUnidad: false,
                                                    st: ''
                                                });
                                                NumeroUnidad++;
                                            });

                                        } else {
                                             
                                            $scope.ListadoGuias.push({
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                ID: ResponseGuia.DetalleUnidades[$scope.UnidadGuia - 1].ID,
                                                UnidadesPendientes: ResponseGuia.DetalleUnidades.length,
                                                UnidadesActuales: ResponseGuia.DetalleUnidades.length,
                                                NumeroRemesa: Guia.Remesa.Numero,
                                                NumeroDocumentoRemesa: Guia.Remesa.NumeroDocumento,
                                                NumeroUnidad: $scope.UnidadGuia,
                                                Peso: Guia.Remesa.PesoCliente / ResponseGuia.DetalleUnidades.length,
                                                PesoVolumetrico: Guia.Remesa.PesoVolumetricoCliente , 
                                                ObservacionesVerificacion: '',
                                                CiudadDestino: Guia.Remesa.CiudadDestinatario.Nombre,
                                                faltaUnidad: false,
                                                st: ''
                                            }); 
                                            $scope.IngresadaActual = $scope.NumeroGuia;
                                            $scope.NumeroGuia = '';
                                            $scope.UnidadGuia = '';
                                        }
                                        $scope.Cantidad = $scope.ListadoGuias.length;
                                        $scope.CalcularTotales();
                                    }
                               
                                }
                                else {

                                    $scope.ListadoGuias.push({
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        ID: ResponseGuia.DetalleUnidades[$scope.UnidadGuia - 1].ID,
                                        UnidadesPendientes: ResponseGuia.DetalleUnidades.length,
                                        UnidadesActuales: ResponseGuia.DetalleUnidades.length,
                                        NumeroRemesa: Guia.Remesa.Numero,
                                        NumeroDocumentoRemesa: Guia.Remesa.NumeroDocumento,
                                        NumeroUnidad: NumeroUnidad,
                                        Peso: Guia.Remesa.PesoCliente / ResponseGuia.DetalleUnidades.length,
                                        ObservacionesVerificacion: '',
                                        CiudadDestino: Guia.Remesa.CiudadDestinatario.Nombre,
                                        faltaUnidad: false,
                                        st: ''
                                    });
                                    $scope.Cantidad = $scope.ListadoGuias.length;
                                    $scope.CalcularTotales();
                                }
                                $scope.IngresadaActual = $scope.NumeroGuia;
                                $scope.NumeroGuia = '';
                                $scope.UnidadGuia = '';
                            }
                            else {

                                $scope.MensajesError.push('La guía ingresada ya se encuentra en la planilla')
                                $("#window").animate({
                                    scrollTop: $('.breadcrumb').offset().top
                                }, 200);
                            }

                        }

                    }
                } else {
                    $scope.MensajesError.push('La guía ingresada no es válida')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                }
            } else {
            }
        }

        $scope.CalcularTotales = function () {

            $scope.Cantidad = '';
            $scope.Peso = 0
            $scope.PesoVolumetrico = 0
            $scope.GuiasFaltantes = []


            //CALCULAR UNIDADES CARGADAS
        
       
            for (var i = 0; i < tmparr.length; i++) {
                if ($scope.IngresadaActual  == tmparr[i].NumeroDocumentoRemesa) {
                    tmparr[i].UnidadesActuales = tmparr[i].UnidadesActuales - 1
                }
            }


            //CALCULAR GUIAS INGRESADAS
            
            for (var i = 0; i < $scope.ListadoGuias.length; i++) {
                $scope.ListadoGuias[i].indice = i;
                var existe = false;
                for (var j = 0; j < tmparr.length; j++) {

                    if (tmparr[j].UnidadesActuales != 1) {
                        $scope.ListadoGuias[i].st = stwarning
                    }
                    else {

                        $scope.ListadoGuias[i].st = ''
                    }
                    if ($scope.ListadoGuias[i].NumeroDocumentoRemesa == tmparr[j].NumeroDocumentoRemesa ) {
                        existe = true;
                        break;
                    } 
                }
                if (existe == false) { 
                    $scope.totalUnidades += $scope.ListadoGuias[i].UnidadesPendientes
                 
                    tmparr.push($scope.ListadoGuias[i]);
                    if (tmparr[j].UnidadesActuales > 1) {
                        $scope.ListadoGuias[i].st = stwarning
                    }
                    else {

                        $scope.ListadoGuias[i].st = ''
                    }
                }

            }

            console.log('lista',tmparr)
            $scope.CantidadGuias = tmparr.length
            $scope.UnidadesPendientes = $scope.totalUnidades - $scope.ListadoGuias.length
            $scope.UnidadesCargadas = $scope.ListadoGuias.length


            $scope.ListadoGuias.forEach(item => {
                $scope.Peso += item.Peso;
                $scope.PesoVolumetrico += item.PesoVolumetrico; 
            });
        }

        $scope.TmpEliminarGuia = {};
        $scope.ConfirmarEliminarGuia = function (item) {
            $scope.TmpEliminarGuia = { Indice: item.indice };
            $scope.TmpNumeroDocumentoRemesa = item.NumeroDocumentoRemesa;
            showModal('modalEliminarGuia');
        };

        $scope.EliminarGuia = function (indice) {
            for (var i = 0; i < tmparr.length; i++) {
                if ($scope.TmpNumeroDocumentoRemesa == tmparr[i].NumeroDocumentoRemesa) {
                    tmparr[i].UnidadesActuales = tmparr[i].UnidadesActuales + 1
                }
            }

            $scope.ListadoGuias.splice(indice, 1);
      
            $scope.CalcularTotales();
            closeModal('modalEliminarGuia');
        };


        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardarCargue');
        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarCargue');

            if ($scope.DatosRequeridos()) {
                console.log('Conductor', $scope.NombreConductor)
                var Entidad = {

                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.CodigoPlanilla,
                    Vehiculo: { Codigo: $scope.PlacaVehiculo.Codigo },
                    TipoVerificacion: {
                        codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESCARGUE
                    },
                    Observaciones: $scope.Observaciones,
                    Cantidad: $scope.Cantidad,
                    Peso: $scope.Peso,
                    Conductor: $scope.NombreConductor,
                    Semirremolque: $scope.Semirremolque, 
                    Estado: $scope.Estado.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    ListadoGuias: $scope.ListadoGuias,
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo }
                }
                $scope.MensajesConfirmacion = [];
                $scope.MensajesError = []
                console.log('Guardando', Entidad)

                ValidacionPlanillaDespachosFactory.Guardar(Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            $scope.MensajesConfirmacion.push('Se han verificado ' + $scope.ListadoGuias.length + ' guías de la planilla ' + $scope.NumeroPlanilla);
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);
                            $scope.MostrarSeccionGuias = false;
                            $scope.ListadoGuias = [];
                            $scope.NumeroDocumentoPlanilla = '';

                            $scope.NumeroGuia = '';
                            $scope.UnidadGuia = '';
                            $scope.PlacaVehiculo = '';
                            $scope.NombreConductor = '';
                            $scope.NombreTenedor = '';
                            $scope.NombreRuta = '';
                            $scope.PlacaSemirremolque = '';
                            $scope.FechaSalida = '';
                            $scope.HoraSalida = '';

                            if ($scope.NumeroPlanilla > 0) {
                                ShowSuccess('Se modificó la planilla con el número ' + response.data.Datos);
                                document.location.href = '#!ConsultarPlanillaDescargue/' + response.data.Datos
                                $scope.NumeroPlanilla = '';
                            } else {
                                ShowSuccess('Se guardó la planilla con el número ' + response.data.Datos);
                                document.location.href = '#!ConsultarPlanillaDescargue/' + response.data.Datos
                            }

                        } else {
                            $scope.MensajesError.push(response.data.MensajeOperacion);
                            $("#window").animate({
                                scrollTop: $('.breadcrumb').offset().top
                            }, 200);
                        }
                    });
            }
        }

        $scope.DatosRequeridos = function () {
            var continuar = true;
            if ($scope.ListadoGuias != undefined && $scope.ListadoGuias != null && $scope.ListadoGuias != '') {
                if ($scope.ListadoGuias.length == 0) {
                    $scope.MensajesError.push('debe existir por lo menos una guía verificada')
                    $("#window").animate({
                        scrollTop: $('.breadcrumb').offset().top
                    }, 200);
                    continuar = false;
                }
            } else {
                $scope.MensajesError.push('debe existir por lo menos una guía verificada')
                $("#window").animate({
                    scrollTop: $('.breadcrumb').offset().top
                }, 200);
            }
            return continuar;
        }

        function Obtener() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.CodigoPlanilla
            };
            ValidacionPlanillaDespachosFactory.Obtener(filtros).
                then(function (response) {
                    console.log('llego data', response)
                    if (response.data.ProcesoExitoso === true) {
                        $scope.NumeroPlanilla = response.data.Datos.NumeroDocumento;
                        $scope.TipoPlanilla = CODIGO_TIPO_DOCUMENTO_PLANILLA_DESCARGUE
                        $scope.Fecha = new Date(response.data.Datos.Fecha);
                        $scope.PlacaVehiculo = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo);

                        if (response.data.Datos.Semirremolque.Codigo > 0) {
                            $scope.Semirremolque = $linq.Enumerable().From($scope.ListadoSemirremolques).First('$.Codigo == "' + response.data.Datos.Semirremolque.Codigo + '"');
                        }
                  

                        $scope.NombreConductor = $scope.CargarTercero(response.data.Datos.Conductor.Codigo);
                      
                        $scope.Observaciones = response.data.Datos.Observaciones;

                        response.data.Datos.ListadoGuias.forEach(item => {

                            var CIUDAD = CiudadesFactory.Consultar({
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Codigo: item.CodigoCiudadDestinoRuta
                                , Sync: true
                            }).Datos

                            $scope.ListadoGuias.push({
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroRemesa: item.NumeroRemesa,
                                NumeroDocumentoRemesa: item.NumeroDocumentoRemesa,
                                NumeroUnidad: item.NumeroUnidad,
                                UnidadesActuales: response.data.Datos.ListadoGuias.length ,
                                Peso: item.PesoRemesa,
                                ObservacionesVerificacion: item.Observaciones,
                                CiudadDestino: CIUDAD[0].Nombre,
                                st: ''
                            });  });

                        $scope.CalcularTotales();

                        $scope.MostrarSeccionGuias = true;
                        $scope.Oficina = OficinasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: response.data.Datos.Oficina.Codigo, Sync: true }).Datos;
                        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + response.data.Datos.Estado);

                        if ($scope.Estado.Codigo == 1) {
                            $scope.Deshabilitar = true
                        }
                        blockUI.stop();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    blockUI.stop();
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } document.location.href = '#!ConsultarPlanillaDescargue'; }, 500);
                });
        }

        $scope.VolverMaster = function () {
            if ($scope.NumeroPlanilla > 0) {
                document.location.href = '#!ConsultarPlanillaDescargue/' + $scope.NumeroPlanilla;
            } else {
                document.location.href = '#!ConsultarPlanillaDescargue';
            }
        }

        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope);
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        }

        if ($routeParams.Numero > 0) {
            $scope.CodigoPlanilla = $routeParams.Numero
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Obteniendo Documento...");
            $timeout(function () { blockUI.message("Obteniendo Documento..."); Obtener(); }, 100);
        }


    }
]);