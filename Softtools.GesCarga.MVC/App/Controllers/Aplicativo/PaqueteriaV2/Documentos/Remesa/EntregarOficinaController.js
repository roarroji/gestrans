﻿SofttoolsApp.controller('EntregasPaqueteriaOficinaCtrl', ['$scope', '$timeout', 'DetalleDistribucionRemesasFactory', 'OficinasFactory', '$linq', 'blockUIConfig', '$routeParams', 'blockUI', 'CiudadesFactory', 'RemesasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'RemesaGuiasFactory', 'UsuariosFactory', 'PlanillaGuiasFactory', 'RecoleccionesFactory',
    function ($scope, $timeout, DetalleDistribucionRemesasFactory, OficinasFactory, $linq, blockUIConfig,$routeParams, blockUI, CiudadesFactory, RemesasFactory, ValorCatalogosFactory, TercerosFactory, RemesaGuiasFactory, UsuariosFactory, PlanillaGuiasFactory, RecoleccionesFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CONTROL ENTREGA  OFICINA';
        $scope.MapaSitio = [{ Nombre: 'Entregas' }, { Nombre: 'Paqueteria / Documentos / Guías ' }];

        

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONTROL_ENTREGAS_RECOLECCION); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
       
        var OPCION_MENU = OPCION_MENU_PAQUETERIA.REMESAS;
        $scope.Master = "#!ConsultarRemesasPaqueteria";
        $scope.ListaPlanillas = [];
        $scope.ListaPlanillasEntregadas = [];
        $scope.ListadoRemesas = [];
        $scope.ListadoRecolecciones = [];
        $scope.ListadoDetallesPlanilla = [];
        $scope.ListadoTipoIdentificacion = [
            { Codigo: 101, Nombre: "CC" },
            { Codigo: 102, Nombre: "NIT" },
            { Codigo: 103, Nombre: "CE" },
            { Codigo: 104, Nombre: "PASAPORTE" }
        ];

        $scope.ListadoNovedadesRecolecciones = [];
        $scope.Foto = [];
        $scope.Modal = {};
        $scope.DevolucionRemesa = 0;
        $scope.ListaGestionDocuCliente = [];
        $scope.ListadoConfirmaEntrega = [
            { Codigo: -1, Nombre: "SELECCIONE" },
            { Codigo: 0, Nombre: "NO" },
            { Codigo: 1, Nombre: "SI" }
        ];

        $scope.Modelo = {
            Oficina: ''
        }
        $scope.ListadoOficinasActual = [];
 
        var fechaActual = new Date();
        $scope.MensajesErrorDocu = [];
        //--------------------------Variables-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------Init
        $scope.InitLoad = function () {
          
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 203 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNovedades = response.data.Datos;
                           
                        }
                        else {
                            $scope.ListadoNovedades = []
                        }
                    }
                }, function (response) {
                });
 
          
            //ListadoNovedadesRecoleccion:
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 219 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNovedadesRecolecciones = response.data.Datos;
                        }
                        else {
                            $scope.ListadoNovedadesRecolecciones = []
                        }
                    }
                }, function (response) {
                });
            //Listado tipo identificación:
            //ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
            //    then(function (response) {  
            //        console.log('llegaron',response)
            //        if (response.data.ProcesoExitoso === true) {
            //            if (response.data.Datos.length > 0) { 
            //                $scope.ListadoTipoIdentificacion = response.data.Datos; 
            //            }
            //        }
            //    }, function (response) {
            //    });
            //Obtiene Informacion Tercero Conductor
            try {
                $scope.Conductor = UsuariosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Sesion.UsuarioAutenticado.Codigo, Sync: true }).Datos.Conductor.Codigo
            } catch (e) {
                console.log(e);
            }
            //planillas Entregas:
            filtro = {
                FechaInicial: fechaActual,
                FechaFinal: fechaActual,
                Conductor: { Codigo: $scope.Conductor },
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Planilla: { Estado: { Codigo: 1 } },
                TipoDocumento: 210,
                Sync: true
            };
            try {
                $scope.ListaPlanillasEntregadas = PlanillaGuiasFactory.Consultar(filtro).Datos;
                $scope.ListaPlanillasEntregadas.push({ Planilla: { NumeroDocumento: '(Seleccione)', Numero: 0 } });
                // $scope.ListaPlanillas = $scope.ListaPlanillasEntregadas;
            } catch (e) {
                console.log("error consulta planilla entregas: ", e);
            }
            //planillas Recolecciones:
            filtroRecoleccion = {
                FechaInicial: fechaActual,
                FechaFinal: fechaActual,
                Conductor: { Codigo: $scope.Conductor },
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Planilla: { Estado: { Codigo: 1 } },
                TipoDocumento: 135,
                Sync: true
            }
            try {
                $scope.ListaPlanillasRecolecciones = PlanillaGuiasFactory.Consultar(filtroRecoleccion).Datos;
                $scope.ListaPlanillasRecolecciones.push({ Planilla: { NumeroDocumento: '(Seleccione)', Numero: 0 } })
                $scope.ListaPlanillas = $scope.ListaPlanillasRecolecciones;
            } catch (e) {
                console.log("error consulta planilla recolecciones: ", e);
            }
            //$scope.ListaPlanillas = $scope.ListaPlanillasEntregadas;
            $scope.Planilla = $linq.Enumerable().From($scope.ListaPlanillas).First('$.Planilla.Numero == 0');

            //Otras Oficinas
            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ListadoOficinasActual.push({ Codigo: -1, Nombre: '(No aplica)' });
                            response.data.Datos.forEach(function (item) {
                                $scope.ListadoOficinasActual.push(item);
                            });
                            $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinasActual).First('$.Codigo === -1');
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else { 
                $scope.ListadoOficinasActual.push({
                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
                });
                $scope.ListadoOficinasActual.push({ Codigo: -1, Nombre: '(No aplica)' }); 
            }
            $scope.BuscarRemesa();
        };
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
    
        $scope.ModalDestinatario = [];

        $scope.BuscarRemesa = function () { 

            filtroRemesas = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: 1,
                NumeroInicial: $routeParams.Codigo 
            }; 

            RemesaGuiasFactory.ConsultarRemesasRecogerOficina(filtroRemesas).
                then(function (response) { 
                  
                    $scope.Modal.Remitente = response.data.Datos[0].Remesa.Remitente.Nombre;
                    $scope.Modal.CiudadOrigen = response.data.Datos[0].Remesa.CiudadRemitente.Nombre;
                    $scope.Modal.CiudadDestino = response.data.Datos[0].Remesa.CiudadDestinatario.Nombre;
                    $scope.Modal.FormaPago = response.data.Datos[0].Remesa.FormaPago.Nombre;
                    $scope.Modal.Producto = response.data.Datos[0].Remesa.ProductoTransportado.Nombre;
                    $scope.Modal.TotalValor =$scope.MaskValoresGrid(response.data.Datos[0].Remesa.TotalFleteCliente,),
                    $scope.Modal.Peso = $scope.MaskValoresGrid(response.data.Datos[0].Remesa.PesoCliente);
                    $scope.Modal.Cantidad = $scope.MaskValoresGrid(response.data.Datos[0].Remesa.CantidadCliente);
                  
                    $scope.Modal.Remesa = response.data.Datos[0].Remesa.Numero;
                    $scope.Modal.NumeroDocumento = response.data.Datos[0].Remesa.NumeroDocumento;
                    $scope.Modal.FechaRecibe = new Date();
                    $scope.Modal.Fecha = response.data.Datos[0].Remesa.Fecha

                    $scope.ListaGestionDocuCliente = []; 

                    $scope.ModalDestinatario.Nombre = response.data.Datos[0].Remesa.Destinatario.Nombre
                    $scope.ModalDestinatario.Telefono= response.data.Datos[0].Remesa.Destinatario.Telefonos,
                    $scope.ModalDestinatario.Direccion= response.data.Datos[0].Remesa.Destinatario.Direccion,
                    $scope.ModalDestinatario.NumeroIdentificacion= response.data.Datos[0].Remesa.Destinatario.NumeroIdentificacion
                    $scope.ModalDestinatario.Barrio = response.data.Datos[0].Remesa.BarrioDestinatario

                   
                    $scope.CodigoTipoIdentificacionRecibe = response.data.Datos[0].Remesa.Destinatario.TipoIdentificacion.Codigo
                    $scope.ModalDestinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.CodigoTipoIdentificacionRecibe);
                    $scope.Modal.Oficina = $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre,
                        $scope.Modal.Usuario =  $scope.Sesion.UsuarioAutenticado.Nombre
                }, function (response) {
                    ShowError(response.statusText);
                });

           
        } 
 
        $scope.LimpiarFirma = function () {
            try {
                var canvas = document.getElementById('Firma');
                var context = canvas.getContext('2d')
                context.clearRect(0, 0, canvas.width, canvas.height);
            } catch (e) {

            }

        }

        $scope.ConfirmacionGuardarPaqueteria = function () {
            
            if (DatosRequeridos()) {
                ShowConfirm('¿Está seguro de guardar la información?',
                    $scope.GuardarPaqueteria)
            }
        };

        $scope.ConfirmacionGuardarRecoleccion = function () {
            if (DatosRequeridosRecoleccion()) {
                ShowConfirm('¿Está seguro de guardar la información?', $scope.GuardarRecoleccion)
            }
        };

        $scope.GuardarPaqueteria = function () {
            //closeModal('modalConfirmacionGuardarPaqueteria', 1);
            var canvas = document.getElementById('Firma');
            var image = new Image();
            image.src = canvas.toDataURL("image/png");
            $scope.Firma = image.src.replace(/data:image\/png;base64,/g, '');

            $scope.Modal.FechaRecibe = RetornarFechaEspecificaSinTimeZone($scope.Modal.FechaRecibe);

            var GestionDocumentosRemesa = [];
            if ($scope.ListaGestionDocuCliente.length > 0) {
                for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                    var item = $scope.ListaGestionDocuCliente[i];
                    GestionDocumentosRemesa.push({
                        TipoDocumento: item.TipoDocumento,
                        TipoGestion: item.TipoGestion,
                        Entregado: item.Entregado.Codigo
                    });
                }
            }
            
            if ($scope.Modal.Planilla == undefined) {
                 NumPLanilla  = null
            } else {
                NumPLanilla = $scope.Modal.Planilla.Numero

            }

            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                //Codigo: $scope.Codigo,
                Numero: $scope.Modal.Remesa,
                Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas, 
                FechaRecibe: $scope.Modal.FechaRecibe,
                CantidadRecibe: $scope.Modal.Cantidad,
                PesoRecibe: $scope.Modal.Peso,
                CodigoTipoIdentificacionRecibe: $scope.Modal.TipoIdentificacionRecibe.Codigo,
                NumeroIdentificacionRecibe: parseInt($scope.Modal.NumeroIdentificacion),
                NombreRecibe: $scope.Modal.Nombre,
                TelefonoRecibe: $scope.Modal.Telefono,
                NovedadEntrega: $scope.Modal.NovedadEntrega,
                ObservacionesRecibe: $scope.Modal.Observaciones,
                //Fotografia: $scope.Foto[0],
                Fotografias: $scope.Foto,
                Firma: $scope.Firma,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                DevolucionRemesa: $scope.DevolucionRemesa,
                GestionDocumentosRemesa: GestionDocumentosRemesa,
                Planilla: NumPLanilla
            };


            RemesaGuiasFactory.GuardarEntrega($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.DevolucionRemesa == 0) {
                                ShowSuccess('La remesa fue recibida por ' + $scope.Modal.Nombre);
                                $scope.Foto = [];
                                $scope.LimpiarFirma();
                                $('.modalEntregas').hide();
                            } else {
                                ShowSuccess('Se realizó la devolución de la remesa satisfactoriamente');
                                $scope.Foto = [];
                                $scope.LimpiarFirma();
                                $('.modalEntregas').hide();
                                $('.modalDevolucionRemesas').hide();
                            }

                            $('#ModalRecibirDistribucionPaqueteria').hide(100);
                            $('.modalDevolucionRemesas').hide();
                            $('#ResultadoConsulta').show(100);
                            //closeModal('ModalRecibirDistribucionPaqueteria', 1);
                            document.location.href = $scope.Master + "/"+$scope.Modal.NumeroDocumento+"/" + OPCION_MENU;
                    
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.ObtenerPlanilla = function (item) {
            if (item !== undefined && item !== '' && item > 0) {

                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: item,
                    TipoDocumento: 135
                };

                blockUI.delay = 1000;
                PlanillaGuiasFactory.Obtener(filtros).
                    then(function (response) {
                        console.log('llego planilla', response)
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Planilla != null) {
                                if (response.data.Datos.Planilla.Anulado == 0 && response.data.Datos.Planilla.Estado.Codigo == 1) {
                                    if (response.data.Datos.Planilla.NumeroCumplido == 0 && response.data.Datos.Planilla.NumeroLiquidacion == 0) {
                                        $scope.esObtener = true

                                        $scope.Modal.Planilla = response.data.Datos.Planilla 

                                        if ($scope.Modal.Planilla.FechaSalida.getMinutes().toString().length == 1) {
                                            $scope.Modal.Planilla.HoraSalida += ':0' + $scope.Modal.Planilla.FechaSalida.getMinutes().toString()
                                        } else {
                                            $scope.Modal.Planilla.HoraSalida += ':' + $scope.Modal.Planilla.FechaSalida.getMinutes().toString()
                                        }
                                        $scope.Modal.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                                        $scope.Modal.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                        $scope.Modal.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                        $scope.Modal.Planilla.Fecha = new Date($scope.Modal.Planilla.Fecha)
                                        var conductor = $scope.Modal.Planilla.Vehiculo.Conductor
                                        try {
                                            $scope.Modal.Planilla.Vehiculo = $scope.CargarVehiculos($scope.Modal.Planilla.Vehiculo.Codigo);
                                            $scope.Modal.Planilla.Vehiculo.Conductor = $scope.CargarTercero(conductor.Codigo)
                                            $scope.Modal.Planilla.Vehiculo.Tenedor = $scope.CargarTercero($scope.Modal.Planilla.Vehiculo.Tenedor.Codigo)
                                        } catch (e) {
                                        }

                                    } else {
                                        ShowError('La planilla ingresada ya se encuentra liquidada');
                                        $scope.Planilla = ''
                                    }
                                } else {
                                    ShowError('La planilla ingresada se encuentra en estado borrador o fue anulada');
                                    $scope.Planilla = ''
                                }
                            } else {
                                ShowError('No se encontró una planilla válida con el número ingresado');
                                $scope.Planilla = ''
                            }
                        }
                        else {
                            ShowError('No se encontró una planilla válida con el número ingresado');
                            $scope.Planilla = ''
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        $scope.Planilla = ''
                    });
            } else {
                ShowError('Debe ingresar el número de la planilla')
            }

        } 
       
        $scope.CargarImagen = function (element) {
            var continuar = true;
            $scope.TipoImagen = element
            $scope.NombreDocumento = element.files[0].name
            if (element === undefined) {
                ShowError("Solo se pueden cargar archivos en formato JPEG, PNG", false);
                continuar = false;
            }

            if (continuar == true) {
                if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png') {
                    ShowError("Solo se pueden cargar archivos en formato JPEG, PNG ", false);
                    continuar = false;
                }
            }

            if (continuar == true) {
                var reader = new FileReader();
                reader.onload = $scope.AsignarImagenListado;
                reader.readAsDataURL(element.files[0]);
            }

        }

        $scope.AsignarImagenListado = function (e) {
            try {
                RedimHorizontal('')
                RedimVertical('')
            } catch (e) {
            }
            $scope.$apply(function () {
                $scope.Convertirfoto = null
                var TipoImagen = ''

                var name = $scope.TipoImagen.files[0].name.split('.')

                $scope.NombreFoto = name[0]

                if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
                    var img = new Image()
                    img.src = e.target.result
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        TipoImagen = 'JPEG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/jpeg'
                        AsignarFotoListado()

                    }, 100)
                }
                else if ($scope.TipoImagen.files[0].type == 'image/png') {
                    var img = new Image()
                    img.src = e.target.result
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        TipoImagen = 'PNG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/png'
                        AsignarFotoListado()
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
                    $scope.Convertirfoto = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                    TipoImagen = 'PDF'
                    $scope.TipoFoto = 'application/pdf'
                    $scope.TipoImagen = TipoImagen
                    AsignarFotoListado()

                }
                var input = document.getElementById('inputfile')
                input.value = ''
            });
        }

        $scope.AsignarFoto = function () {
            closeModal('ModalTomarFoto', 1)
            $scope.video.srcObject = undefined
            // $scope.Foto = [];
            $scope.FotoAux = $scope.FotoTomada.toString();
            $scope.FotoBits = $scope.FotoAux.replace(/data:image\/png;base64,/g, '')
            $scope.Foto.push({
                NombreFoto: 'Fotografía',
                TipoFoto: 'image/png',
                ExtensionFoto: 'PNG',
                FotoBits: $scope.FotoBits,
                ValorDocumento: 1,
                FotoCargada: $scope.FotoAux,
                id: $scope.Foto.length
            });
        }

        function AsignarFotoListado() {
            // $scope.Foto = [];
            $scope.Foto.push({
                NombreFoto: $scope.NombreFoto,
                TipoFoto: $scope.TipoFoto,
                ExtensionFoto: $scope.TipoImagen,
                FotoBits: $scope.Convertirfoto,
                ValorDocumento: 1,
                FotoCargada: 'data:' + $scope.TipoFoto + ';base64,' + $scope.Convertirfoto,
                id: $scope.Foto.length
            });
        }

        $scope.EliminarFoto = function (item) {
            $scope.Foto.splice(item.id, 1);
            for (var i = 0; i < $scope.Foto.length; i++) {
                $scope.Foto[i].id = i;
            }
            // $scope.Foto = []
            $scope.ModificarFoto = true;
            //$("#Foto").attr("src", "");
        }

        function RedimVertical(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasVertical');
            if (ctx) {
                // dentro del canvas se dibuja la imagen que se recibe por parametro
                ctx.drawImage(img, 0, 0, 499, 699);
            }
        }

        function RedimHorizontal(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasHorizontal');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 699, 499);
            }
        }

        function DatosRequeridos() {
            $scope.MensajesErrorRecibir = [];
            $scope.MensajesErrorDocu = [];
            var continuar = true;

            if ($scope.Modal.Cantidad === undefined || $scope.Modal.Cantidad === '' || $scope.Modal.Cantidad === null || $scope.Modal.Cantidad === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar la cantidad');
                continuar = false;
            }
            if ($scope.Modal.Peso === undefined || $scope.Modal.Peso === '' || $scope.Modal.Peso === null || $scope.Modal.Peso === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el peso');
                continuar = false;
            }
            if ($scope.Modal.TipoIdentificacionRecibe === undefined || $scope.Modal.TipoIdentificacionRecibe === '' || $scope.Modal.TipoIdentificacionRecibe === null || $scope.Modal.TipoIdentificacionRecibe.Codigo === CODIGO_NO_APLICA_TIPO_IDENTIFICACION) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el tipo de identificación');
                
                continuar = false;
            }
            if ($scope.Modal.NumeroIdentificacion === undefined || $scope.Modal.NumeroIdentificacion === '' || $scope.Modal.NumeroIdentificacion === null || $scope.Modal.NumeroIdentificacion === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el número de identificación');
                continuar = false;
            }
            if ($scope.Modal.Nombre === undefined || $scope.Modal.Nombre === '' || $scope.Modal.Nombre === null || $scope.Modal.Nombre === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el nombre de la persona que recibe');
                continuar = false;
            }
            if ($scope.Modal.Telefono === undefined || $scope.Modal.Telefono === '' || $scope.Modal.Telefono === null || $scope.Modal.Telefono === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el teléfono');
                continuar = false;
            }
            if ($scope.Modal.Observaciones === undefined || $scope.Modal.Observaciones === '' || $scope.Modal.Observaciones === null || $scope.Modal.Observaciones === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar las observaciones');
                continuar = false;
            }
            if ($scope.ListaGestionDocuCliente.length > 0) {
                for (var i = 0; i < $scope.ListaGestionDocuCliente.length; i++) {
                    if ($scope.ListaGestionDocuCliente[i].Entregado.Codigo == -1) {
                        $scope.MensajesErrorDocu.push('Se debe seleccionar en todos los documentos si fueron entregados o no');
                        continuar = false;
                        break;
                    }
                }
            }
            return continuar;
        }

        function DatosRequeridosRecoleccion() {
            $scope.MensajesErrorRecibir = [];
            var continuar = true;

            if ($scope.ModalRecoleccion.Cantidad === undefined || $scope.ModalRecoleccion.Cantidad === '' || $scope.ModalRecoleccion.Cantidad === null || $scope.Modal.Cantidad === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar la cantidad');
                continuar = false;
            }
            if ($scope.ModalRecoleccion.Peso === undefined || $scope.ModalRecoleccion.Peso === '' || $scope.ModalRecoleccion.Peso === null || $scope.ModalRecoleccion.Peso === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el peso');
                continuar = false;
            }

            if ($scope.ModalRecoleccion.Nombre === undefined || $scope.ModalRecoleccion.Nombre === '' || $scope.ModalRecoleccion.Nombre === null || $scope.ModalRecoleccion.Nombre === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el nombre de la persona que recibe');
                continuar = false;
            }
            if ($scope.ModalRecoleccion.Telefono === undefined || $scope.Modal.Telefono === '' || $scope.ModalRecoleccion.Telefono === null || $scope.ModalRecoleccion.Telefono === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el teléfono');
                continuar = false;
            }
            if ($scope.ModalRecoleccion.Observaciones === undefined || $scope.ModalRecoleccion.Observaciones === '' || $scope.ModalRecoleccion.Observaciones === null || $scope.ModalRecoleccion.Observaciones === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar las observaciones');
                continuar = false;
            }
            return continuar;
        }
        //----------------------------Funciones Generales---------------------------------//
        $scope.CancelarPaqueteria = function () {
            document.location.href = $scope.Master + "/0/" + OPCION_MENU;
            
        }  
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        }; 
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        }; 
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item) 
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };


    }]);