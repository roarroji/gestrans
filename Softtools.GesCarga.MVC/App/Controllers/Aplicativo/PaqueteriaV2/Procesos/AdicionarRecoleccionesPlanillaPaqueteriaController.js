﻿SofttoolsApp.controller("AdicionarRecoleccionesPlanillaPaqueteriaCtrl", ['$scope', '$timeout', '$linq', 'blockUI', '$routeParams', 'PlanillaDespachosFactory', 'OficinasFactory', 'RutasFactory', 'TercerosFactory', 'PlanillaRecoleccionesFactory', 'PlanillaDespachosFactory', 'blockUIConfig', 'VehiculosFactory', 'RecoleccionesFactory','CiudadesFactory',
    function ($scope, $timeout, $linq, blockUI, $routeParams, PlanillaDespachosFactory, OficinasFactory, RutasFactory, TercerosFactory, PlanillaRecoleccionesFactory, PlanillaDespachosFactory, blockUIConfig, VehiculosFactory, RecoleccionesFactory, CiudadesFactory) {

        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Procesos' }, { Nombre: 'Adicionar Recolecciones  Planilla' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.paginaActualRecoleccion = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.registrorepetido = 0;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoGuias = [];
        $scope.ListaResultadoFiltroConsulta = [];
        $scope.ListadoRecorridosTotal = [];
        $scope.ListadoAuxiliar = [];
        $scope.CodigoCiudad = 0;
        $scope.ListadoRecoleccionesGeneral = [];
        $scope.Planilla = 0;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {} }
        }
        $scope.Recoleccion = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            NumeroDocumento: '',
            Cliente: '',
            Oficinas: '',
            Zonas: '',
        }
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.ADICIONAR_RECOLECCIONES_PLANILLA); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.seleccionarCheck = false;
        $scope.ListadoOficinasOrigen = [
            { Codigo: 0, Nombre: '(Todas)' }
        ];
        $scope.ListadoOficinasActual = [
            { Codigo: 0, Nombre: '(Todas)' }
        ];

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ModeloCiudadCargue = { Nombre: '', Codigo: 0 }
        $scope.ModeloCliente = { NombreCompleto: '', Codigo: 0 }
        $scope.ModeloPlanillaRecogida = 0
        $scope.ModeloPlanillaDespacho = 0
        $scope.MostrarSeccionRecolecciones = false;
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinasActual.push(item);
                        $scope.ListadoOficinasOrigen.push(item);
                    })
                    $scope.ModeloOficinaOrigen = $linq.Enumerable().From($scope.ListadoOficinasOrigen).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                    $scope.ModeloOficinaActual = $scope.ListadoOficinasActual[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.ListaRutas = []
        $scope.AutocompleteRutas = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = RutasFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListaRutas = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRutas)
                }
            }
            return $scope.ListaRutas
        }
        $scope.ModeloCiudadDescargue = { Nombre: '', Codigo: 0 }

        $scope.ListadoCiudades = [];
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                }
            }
            return $scope.ListadoCiudades
        }

        $scope.ListadoClientes = [];
        $scope.AutocompleteClientes = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, CadenaPerfiles: PERFIL_CLIENTE,Sync: true
                    })
                    $scope.ListadoClientes = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoClientes)
                }
            }
            return $scope.ListadoClientes
        }

        $scope.ListadoRemitentes = [];
        $scope.AutocompleteRemitentes = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, CadenaPerfiles: PERFIL_REMITENTE, Sync: true
                    })
                    $scope.ListadoRemitentes = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoRemitentes)
                }
            }
            return $scope.ListadoRemitentes
        }

        $scope.ListaVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, Sync: true })
                    $scope.ListaVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListaVehiculos)
                }
            }
            return $scope.ListaVehiculos
        }

        $scope.ListaCliente = [];
        $scope.AutocompleteConductores = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListaCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCliente)
                }
            }
            return $scope.ListaCliente
        }

        $scope.MarcarRemesas = function (chk) {
            if (chk) {
                for (var i = 0; i < $scope.ListadoRecorridosTotal.length; i++) {
                    $scope.ListadoRecorridosTotal[i].Seleccionado = true
                }
            } else {
                for (var i = 0; i < $scope.ListadoRecorridosTotal.length; i++) {
                    $scope.ListadoRecorridosTotal[i].Seleccionado = false
                }
            }
        }
        $scope.PrimerPagina = function () {
            /*Verificar si la página actual ya está guardada si no lo esta procede a realizar la busqueda*/
            if ($scope.paginaActual > 1) {

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                    $scope.paginaActual = 1;
                    $scope.ListadoGuias = [];

                    var i = 0;
                    for (i = 0; i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina) {
                            $scope.ListadoGuias.push($scope.ListaResultadoFiltroConsulta[i])
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = 1;
                        $scope.ListadoGuias = [];

                        var i = 0;
                        for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina) {
                                $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                            }
                        }
                    }
                }

            }


        };

        $scope.Siguiente = function () {

            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.ListadoGuias = [];
                $scope.PaginaAuxiliar = $scope.paginaActual;
                $scope.paginaActual += 1;

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoGuias.push($scope.ListaResultadoFiltroConsulta[i]);
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i]);
                            }
                        }
                    }
                }
            }

        }

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                    $scope.ListadoGuias = [];
                    $scope.paginaActual -= 1;

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoGuias.push($scope.ListaResultadoFiltroConsulta[i]);
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.ListadoGuias = [];
                        $scope.paginaActual -= 1;

                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i]);
                            }
                        }
                    }
                }

            }
        };

        $scope.UltimaPagina = function () {

            if ($scope.paginaActual < $scope.totalPaginas) {

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListadoGuias = [];

                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoGuias.push($scope.ListaResultadoFiltroConsulta[i])
                            }
                        }
                    }
                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListadoGuias = [];

                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                            }
                        }
                    }
                }

            }
        }

        $scope.PrimerPaginaRecoleccion = function () {
            if ($scope.totalRegistrosRecoleccion > 10) {
                $scope.ListadoRecolecciones = []
                $scope.paginaActualRecoleccion = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ListadoRecolecciones.push($scope.ListadoRecoleccionesGeneral[i])
                }
            } else {
                for (var i = 0; i < $scope.ListadoRecoleccionesGeneral.length; i++) {
                    $scope.ListadoRecolecciones.push($scope.ListadoRecoleccionesGeneral[i])
                }
            }
        }
        $scope.AnteriorRecoleccion = function () {
            if ($scope.paginaActualRecoleccion > 1) {
                $scope.paginaActualRecoleccion -= 1
                var a = $scope.paginaActualRecoleccion * 10
                if (a < $scope.totalRegistrosRecoleccion) {
                    $scope.ListadoRecolecciones = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoRecolecciones.push($scope.ListadoRecoleccionesGeneral[i])
                    }
                }
            }
            else {
                $scope.PrimerPaginaRecoleccion()
            }
        }
        $scope.SiguienteRecoleccion = function () {
            if ($scope.paginaActualRecoleccion < $scope.totalPaginasRecoleccion) {
                $scope.paginaActualRecoleccion += 1
                var a = $scope.paginaActualRecoleccion * 10
                if (a < $scope.totalRegistrosRecoleccion) {
                    $scope.ListadoRecolecciones = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoRecolecciones.push($scope.ListadoRecoleccionesGeneral[i])
                    }
                } else {
                    $scope.UltimaPaginaRecoleccion()
                }
            } else if ($scope.paginaActualRecoleccion == $scope.totalPaginasRecoleccion) {
                $scope.UltimaPaginaRecoleccion()
            }
        }
        $scope.UltimaPaginaRecoleccion = function () {
            if ($scope.totalRegistrosRecoleccion > 10 && $scope.totalPaginasRecoleccion > 1) {
                $scope.paginaActualRecoleccion = $scope.totalPaginasRecoleccion
                var a = $scope.paginaActualRecoleccion * 10
                $scope.ListadoRecolecciones = []
                for (var i = a - 10; i < $scope.ListadoRecoleccionesGeneral.length; i++) {
                    $scope.ListadoRecolecciones.push($scope.ListadoRecoleccionesGeneral[i])
                }
            } else {
                for (var i = 0; i < $scope.ListadoRecoleccionesGeneral.length; i++) {
                    $scope.ListadoRecolecciones.push($scope.ListadoRecoleccionesGeneral[i])
                }
            }
        }

        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionRecepcionGuias');


            $scope.listadoGuiasSeleccionadas = [];
            $scope.detalleguia = ''
            if ($scope.CodigoCiudad == 0) {
                $scope.CodigoCiudad = $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo;

            }
            $scope.ListadoRecorridosTotal.forEach(function (itemRT) {
                if (itemRT.Seleccionado == true) {

                    $scope.detalleguia += itemRT.Numero + ',';

                    var guias = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Numero: itemRT.Remesa.Numero,
                        EstadoRemesaPaqueteria: { Codigo: CODIGO_CATALOGO_RECIBIDA_OFICINA_DESTINO },
                        CodigoOficinaActual: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                        CodigoCiudad: $scope.CodigoCiudad,
                    }
                    $scope.listadoGuiasSeleccionadas.push(guias)
                }
            })



            $scope.objEnviar = {

                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                //Remesa: { Numero: PERMISO_ACTIVO },
                //actualizaestado: PERMISO_ACTIVO,
                //RemesaPaqueteria: $scope.listadoGuiasSeleccionadas,
                CadenaPlanillas: $scope.detalleguia,
                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                RecibirOficina: true,
                Numero: 1
            };



            PlanillaDespachosFactory.Guardar($scope.objEnviar).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Numero == 0) {
                                ShowSuccess('Se recibieron ' + response.data.Datos + ' de ' + $scope.listadoGuiasSeleccionadas.length + ' planillas.');
                            }
                            else {
                                ShowSuccess('Se recibieron ' + response.data.Datos + ' de ' + $scope.listadoGuiasSeleccionadas.length + ' planillas.');
                            }
                            Find();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };

        $scope.modalConfirmacionRecepcionGuias = function () {
            showModal('modalConfirmacionRecepcionGuias');
        };


        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            if ($routeParams.Codigo > 0) {
                $scope.Modelo.NumeroInicial = $routeParams.Codigo;
                $scope.Modelo.NumeroFinal = $routeParams.Codigo;
                Find();
            }
        }

        function DatosRequeridosFiltrar() {
            $scope.MensajesErrorfil = [];
            bolDatosRequeridosFiltrar = true;
            if ($scope.ModeloFiltroNumeroInicial == '' || $scope.ModeloFiltroNumeroInicial == undefined || $scope.ModeloFiltroNumeroInicial == null) {
                $scope.ModeloFiltroNumeroInicial = 0;
            }
            if ($scope.ModeloFiltroNumeroFinal == '' || $scope.ModeloFiltroNumeroFinal == undefined || $scope.ModeloFiltroNumeroFinal == null) {
                $scope.ModeloFiltroNumeroFinal = $scope.ModeloFiltroNumeroInicial;
            }
            if ($scope.ModeloFiltroNumeroInicial > $scope.ModeloFiltroNumeroFinal) {
                $scope.MensajesErrorfil.push('El número de planilla inicial no puede ser superior al número de planilla final')
                bolDatosRequeridosFiltrar = false;
            }

            if (($scope.modelofiltrofechaInicial !== null && $scope.modelofiltrofechaInicial !== '' && $scope.modelofiltrofechaInicial !== undefined) && ($scope.modelofiltrofechaFinal !== null && $scope.modelofiltrofechaFinal !== undefined && $scope.modelofiltrofechaFinal !== '')) {
                if ($scope.modelofiltrofechaInicial > $scope.modelofiltrofechaFinal) {
                    $scope.MensajesErrorfil.push('La fecha inicial debe ser menor que la fecha final');
                    bolDatosRequeridosFiltrar = false;
                }
            }
            if (($scope.modelofiltrofechaInicial !== null && $scope.modelofiltrofechaInicial !== '' && $scope.modelofiltrofechaInicial !== undefined) && ($scope.modelofiltrofechaFinal == null || $scope.modelofiltrofechaFinal == undefined || $scope.modelofiltrofechaFinal == '')) {
                $scope.modelofiltrofechaFinal = $scope.modelofiltrofechaInicial;
            }
            if (($scope.modelofiltrofechaInicial == null || $scope.modelofiltrofechaInicial == '' || $scope.modelofiltrofechaInicial == undefined) && ($scope.modelofiltrofechaFinal !== null && $scope.modelofiltrofechaFinal !== '' && $scope.modelofiltrofechaFinal !== undefined)) {
                $scope.MensajesErrorfil.push('Debe seleccionar la fecha inicial');
                bolDatosRequeridosFiltrar = false;
            }

        }

        function Find() {
            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);


            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroInicial: $scope.ModeloPlanillaDespacho,
                NumeroFinal: $scope.ModeloPlanillaDespacho,
                Numero: $scope.ModeloPlanillaDespacho,
                Remesa: {
                    Cliente: { Codigo: $scope.ModeloCliente.Codigo },
                    NumeroplanillaDespacho: $scope.ModeloPlanillaDespacho,
                    Numeroplanillarecoleccion: $scope.ModeloPlanillaRecogida
                },
                OficinaActual: { Codigo: $scope.ModeloOficinaActual.Codigo },
                OficinaOrigen: { Codigo: $scope.ModeloOficinaOrigen.Codigo },
               
                //actualizaestado: PERMISO_ACTIVO,
                CodigoCiudad: $scope.ModeloCiudadDescargue.Codigo,
                Ruta: { Codigo: $scope.ModeloCiudadCargue == undefined || $scope.ModeloCiudadCargue == null || $scope.ModeloCiudadCargue == '' ? 0 : $scope.ModeloCiudadCargue.Codigo },
                Vehiculo: { Codigo: $scope.ModeloVehiculo == undefined || $scope.ModeloVehiculo == null || $scope.ModeloVehiculo == '' ? 0: $scope.ModeloVehiculo.Codigo },
                Conductor: { Codigo: $scope.ModeloCOnductor == undefined || $scope.ModeloCOnductor == null || $scope.ModeloCOnductor == '' ? 0 :$scope.ModeloCOnductor.Codigo},
                Estado: { Codigo: 1 },
                TipoDocumento: 135,
                NumeroPlanilla: -1,
                Anulado: -1,
                CodigoOficinaActualUsuario: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                RecibirOficina: true
            }

            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    blockUI.delay = 1000;

                    PlanillaDespachosFactory.Consultar(filtro).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0)
                                    $scope.ListadoGuias = [];

                                $scope.ListadoAuxiliar = [];
                                $scope.ListadoAuxiliar = response.data.Datos;
                                $scope.paginaActual = 1;


                                //Llena la lista $scope.ListadoRecorridosTotal para acumular los recorridos consultados con la factura y los nuevos a traer
                                if ($scope.ListadoGuias.length > 0) {
                                    //Datos consultados
                                    $scope.ListadoRecorridosTotal = [];



                                    //Datos nuevos


                                    for (var i = 0; i < $scope.ListadoAuxiliar.length; i++) {
                                        var existe = true;

                                        for (var j = 0; j < $scope.ListadoRecorridosTotal.length; j++) {
                                            if ($scope.ListadoAuxiliar[i].Remesa.Numero == $scope.ListadoRecorridosTotal[j].Remesa.Numero) {
                                                existe = false;
                                                $scope.registrorepetido += 1;
                                                break;
                                            }
                                        }

                                        if (existe == true) {
                                            $scope.ListadoRecorridosTotal.push($scope.ListadoAuxiliar[i]);
                                        }

                                    }

                                } else {
                                    $scope.ListadoRecorridosTotal = $scope.ListadoAuxiliar;


                                }

                                $scope.registrorepetido = 0;
                                /*se ordena el listado del Grid*/
                                if ($scope.ListadoRecorridosTotal.length > 0) {
                                    $scope.ListadoRecorridosTotal = $linq.Enumerable().From($scope.ListadoRecorridosTotal).OrderBy(function (x) {
                                        return x.Numero
                                    }).ToArray();
                                }

                                /*----------------------------*/

                                //Se muestran la cantidad de recorridos por pagina
                                //En este punto se muestra siempre desde la primera pagina
                                $scope.ListadoRecorridosTotal.forEach(function (item) {
                                    item.Seleccionado = $scope.seleccionarCheck;
                                });

                                var i = 0;
                                for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                                    if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                        $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                                    }
                                }

                                //Se operan los totales de registros y de paginas
                                if ($scope.ListadoRecorridosTotal.length > 0) {
                                    $scope.totalRegistros = $scope.ListadoRecorridosTotal.length;
                                    $scope.totalPaginas = Math.ceil($scope.ListadoRecorridosTotal.length / $scope.cantidadRegistrosPorPagina);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';

                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }


                            }
                        }, function (response) {
                            $scope.Buscando = false;
                        });
                }
                else {
                    $scope.Buscando = false;
                }
                blockUI.stop();
            }

        }

        $scope.ValidarSeleccion = function (item) {
            if (item.Seleccionado == true) {
                $scope.MostrarSeccionRecolecciones = true
                $scope.Planilla = item.Numero;
                $scope.ListadoGuias.forEach(item1 => {
                    if (item1.Numero != item.Numero) {
                        item1.Seleccionado = false;
                    }
                });
            } else {
                $scope.MostrarSeccionRecolecciones = false
                
            }
        }

        $scope.BuscarRecolecciones = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                FindRecolecciones()
            }
        }

        function FindRecolecciones() {

            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoRecolecciones = []; $scope.ListadoRecoleccionesGeneral = [];
            if (DatosRequeridos()) {

                blockUI.delay = 1000;
                $scope.Recoleccion.Pagina = $scope.paginaActualRecoleccion
                $scope.Recoleccion.RegistrosPagina = 0
                $scope.Recoleccion.FechaInicio = $scope.Modelo.FechaInicial;
                $scope.Recoleccion.FechaFin = $scope.Modelo.FechaFinal;
                $scope.Recoleccion.Estado = 1;
                $scope.Recoleccion.AsociarRecoleccionPlanilla = 1;
                $scope.Recoleccion.Sync = true;
                var Response = RecoleccionesFactory.Consultar($scope.Recoleccion);
                
                if (Response.ProcesoExitoso === true) {
                            // $scope.Recoleccion.Numero = 0
                    if (Response.Datos.length > 0) {
                        $scope.ListadoRecoleccionesGeneral = Response.Datos
                        $scope.totalRegistrosRecoleccion = Response.Datos[0].TotalRegistros;
                        $scope.totalPaginasRecoleccion = Math.ceil($scope.totalRegistrosRecoleccion / $scope.cantidadRegistrosPorPaginas);
                        $scope.BuscandoRecoleccion = false;
                        $scope.ResultadoSinRegistrosRecoleccion = '';
                        $scope.PrimerPaginaRecoleccion()
                    }
                    else {
                        $scope.totalRegistrosRecoleccion = 0;
                        $scope.totalPaginasRecoleccion = 0;
                        $scope.paginaActualRecoleccion = 1;
                        $scope.ResultadoSinRegistrosRecoleccion = 'No hay datos para mostrar';
                        $scope.BuscandoRecoleccion = false;
                    }
                }
            
            }
            blockUI.stop();
        }

        function DatosRequeridos() {
            $scope.MensajesErrorRecolecciones = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                && ($scope.Modelo.NumeroInicial === null || $scope.Modelo.NumeroInicial === undefined || $scope.Modelo.NumeroInicial === '' || $scope.Modelo.NumeroInicial === 0 || isNaN($scope.Modelo.NumeroInicial) === true)

            ) {
                if ($scope.tabRecolecciones == true) {
                    if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                        && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                        && ($scope.Recoleccion.Numero === null || $scope.Recoleccion.Numero === undefined || $scope.Recoleccion.Numero === '' || $scope.Modelo.NumeroInicial === 0 || isNaN($scope.Recoleccion.Numero) === true)

                    ) {
                        $scope.MensajesErrorRecolecciones.push('Debe ingresar los filtros de fechas o número');
                        continuar = false
                    }
                } else {
                    $scope.MensajesErrorRecolecciones.push('Debe ingresar los filtros de fechas o número');
                    continuar = false
                }

            } else if (($scope.Modelo.NumeroInicial !== null && $scope.Modelo.NumeroInicial !== undefined && $scope.Modelo.NumeroInicial !== '' && $scope.Modelo.NumeroInicial !== 0)
                || ($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')

            ) {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesErrorRecolecciones.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorRecolecciones.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial
                    } else {
                        $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal
                    }
                }
            }
            return continuar
        }

        $scope.LimpiarFiltro = function () {

            $scope.ModeloFiltroNumeroInicial = '';
            $scope.ModeloFiltroNumeroFinal = '';
            $scope.modelofiltrofechaInicial = '';
            $scope.modelofiltrofechaFinal = '';
            $scope.ListadoGuias = [];
            $scope.ModeloPlanillaRecogida = $linq.Enumerable().From($scope.ListaPlanillaRecogida).First('$.Numero == 0 ');
            $scope.ModeloPlanillaDespacho = $linq.Enumerable().From($scope.ListaPlanillaDespacho).First('$.Numero == 0 ');
            $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==0');
            $scope.ModeloCliente = $linq.Enumerable().From($scope.ListaCliente).First('$.Codigo ==0');

            $scope.ListaResultadoFiltroConsulta = [];

            //Se muestran la cantidad de recorridos por pagina
            //En este punto se muestra siempre desde la primera pagina
            var i = 0;
            for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                    $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                }
            }

            //Se operan los totales de registros y de paginas
            if ($scope.ListadoRecorridosTotal.length > 0) {
                $scope.totalRegistros = $scope.ListadoRecorridosTotal.length;
                $scope.totalPaginas = Math.ceil($scope.ListadoRecorridosTotal.length / $scope.cantidadRegistrosPorPagina);
                $scope.Buscando = false;
                $scope.ResultadoSinRegistros = '';

            }
            else {
                $scope.totalRegistros = 0;
                $scope.totalPaginas = 0;
                $scope.paginaActual = 1;
                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                $scope.Buscando = false;
            }



        }

        $scope.Filtrar = function () {
            DatosRequeridosFiltrar();

            if (bolDatosRequeridosFiltrar == true) {

                if ($scope.ModalRecorridos > 0) {
                    $scope.ValorRecorridosAuxiliar = $scope.ModalRecorridos;
                } else {
                    $scope.ValorRecorridosAuxiliar = 0;
                }

                if ($scope.ListadoRecorridosTotal.length > 0) {
                    $scope.ListadoGuias = [];

                    if ($scope.ModeloFiltroNumeroInicial > 0 && $scope.ModeloFiltroNumeroFinal == 0) {
                        $scope.ModeloFiltroNumeroFinal = $scope.ModeloFiltroNumeroInicial;
                    }
                    if ($scope.ModeloFiltroNumeroInicial == 0 && $scope.ModeloFiltroNumeroFinal > 0) {
                        $scope.ModeloFiltroNumeroInicial = $scope.ModeloFiltroNumeroFinal;
                    }
                    //FOR SACAR NUMERO DOCUMENTO Y FECHA
                    $scope.ListadoRecorridosTotal.forEach(function (item) {
                        item.NumeroDocumentoAuxiliar = item.NumeroDocumento;
                        item.FechaAuxiliarAuxiliar = item.Fecha;

                    })

                    var FiltroConsulta = '$.CodigoEmpresa == ' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;


                    if ($scope.ModeloFiltroNumeroInicial > 0 && $scope.ModeloFiltroNumeroFinal > 0) {
                        FiltroConsulta += ' && $.NumeroDocumentoAuxiliar >= ' + $scope.ModeloFiltroNumeroInicial + ' && $.NumeroDocumentoAuxiliar <=' + $scope.ModeloFiltroNumeroFinal;
                    }

                    if (($scope.modelofiltrofechaInicial !== null && $scope.modelofiltrofechaInicial !== '' && $scope.modelofiltrofechaInicial !== undefined) && ($scope.modelofiltrofechaFinal !== null || $scope.modelofiltrofechaFinal !== undefined || $scope.modelofiltrofechaFinal !== '')) {
                        var FechaInicialAuxiliar = Formatear_Fecha($scope.modelofiltrofechaInicial.toDateString(), FORMATO_FECHA_s);
                        var FechaFinalAuxiliar = Formatear_Fecha($scope.modelofiltrofechaFinal.toDateString(), FORMATO_FECHA_s);

                        FiltroConsulta += ' && $.FechaAuxiliarAuxiliar >="' + FechaInicialAuxiliar + '" && $.FechaAuxiliarAuxiliar <= "' + FechaFinalAuxiliar + '"';

                    }




                    //Ejecutar consulta
                    $scope.ListaResultadoFiltroConsulta = [];
                    $scope.ListaResultadoFiltroConsulta = $linq.Enumerable().From($scope.ListadoRecorridosTotal).Where(FiltroConsulta).ToArray();

                    //La consulta se hace si el resultado del filtro trajo datos

                    //Se muestran la cantidad de recorridos por pagina
                    //En este punto se muestra siempre desde la primera pagina
                    if ($scope.ListaResultadoFiltroConsulta.length > 0) {
                        $scope.ListadoGuias = [];
                        $scope.paginaActual = 1;
                        var i = 0;
                        for (i = 0; i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                            if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                $scope.ListadoGuias.push($scope.ListaResultadoFiltroConsulta[i])
                            }
                        }

                        //Se operan los totales de registros y de paginas
                        if ($scope.ListaResultadoFiltroConsulta.length > 0) {
                            $scope.totalRegistros = $scope.ListaResultadoFiltroConsulta.length;
                            $scope.totalPaginas = Math.ceil($scope.ListaResultadoFiltroConsulta.length / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';

                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }

                    }
                    //Si no hay datos consulta todos los datos de la lista total
                    else {

                        if (($scope.modelofiltrofechaInicial == null || $scope.modelofiltrofechaInicial == '' || $scope.modelofiltrofechaInicial == undefined) || ($scope.modelofiltrofechaFinal == null || $scope.modelofiltrofechaFinal == undefined || $scope.modelofiltrofechaFinal == '')) {
                            if ($scope.ModeloFiltroNumeroInicial == 0 && $scope.ModeloFiltroNumeroFinal == 0) {
                                if ($scope.ListadoRecorridosTotal.length > 0) {

                                    //Se muestran la cantidad de recorridos por pagina
                                    //En este punto se muestra siempre desde la primera pagina
                                    var i = 0;
                                    for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                                        if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                            $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                                        }
                                    }

                                    //Se operan los totales de registros y de paginas
                                    if ($scope.ListadoRecorridosTotal.length > 0) {
                                        $scope.totalRegistros = $scope.ListadoRecorridosTotal.length;
                                        $scope.totalPaginas = Math.ceil($scope.ListadoRecorridosTotal.length / $scope.cantidadRegistrosPorPagina);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';

                                    }
                                    else {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }


                                }
                            }
                        }

                    }

                }

            }

        }

        $scope.MarcarRecorridos = function (RecorridosSeleccionados) {

            $scope.RecorridosSeleccionados = RecorridosSeleccionados;
            if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                if ($scope.RecorridosSeleccionados == true) {
                    $scope.ListaResultadoFiltroConsulta.forEach(function (itemTotal) {
                        itemTotal.Seleccionado = true;
                    });
                    $scope.ListadoGuias.forEach(function (item) {
                        item.Seleccionado = true;
                    });
                } else {
                    $scope.ListaResultadoFiltroConsulta.forEach(function (itemTotal) {
                        itemTotal.Seleccionado = false;
                    });
                    $scope.ListadoGuias.forEach(function (item) {
                        item.Seleccionado = false;
                    });
                }

            }
            else {
                if ($scope.RecorridosSeleccionados == true) {
                    $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                        itemTotal.Seleccionado = true;
                    });
                    $scope.ListadoGuias.forEach(function (item) {
                        item.Seleccionado = true;
                    });
                } else {
                    $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                        itemTotal.Seleccionado = false;
                    });
                    $scope.ListadoGuias.forEach(function (item) {
                        item.Seleccionado = false;
                    });
                }
            }


        }

        $scope.AsociarRecoleccionesPlanillas = function (item) {
            var countmarcadas = 0
            if ($scope.ListadoRecoleccionesGeneral.length > 0) {
                for (var i = 0; i < $scope.ListadoRecoleccionesGeneral.length; i++) {
                    if ($scope.ListadoRecoleccionesGeneral[i].Marcado) {
                        countmarcadas++
                    }
                }
                if (countmarcadas > 0) {
                    ShowWarningConfirm('¿Esta seguro de asociar las recolecciones a la planilla seleccionada?', $scope.AsociarRecoleccionesPlanilla);                    
                } else {
                    ShowError('Debe seleccionar al menos una recolección')
                }
            } else {
                ShowError('Debe seleccionar al menos una recolección')
            }
            //$scope.Remesa = item
        }

        $scope.AsociarRecoleccionesPlanilla = function () {
            var Entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Recolecciones: [],
                NumeroPlanilla: $scope.Planilla,
                TipoDocumento: 135 //Planilla Paquetería
            }

            $scope.Recoleccion.Pagina = $scope.paginaActualRecoleccion;
            $scope.Recoleccion.RegistrosPagina = 0
            $scope.Recoleccion.FechaInicio = $scope.Modelo.FechaInicial;
            $scope.Recoleccion.FechaFin = $scope.Modelo.FechaFinal;
            $scope.Recoleccion.Estado = 1;
            $scope.Recoleccion.AsociarRecoleccionPlanilla = 1;
            $scope.Recoleccion.Sync = true;
            try {
                $scope.ListaResultadoRecolecciones = RecoleccionesFactory.Consultar($scope.Recoleccion).Datos
            } catch (e) {
                console.log(e);
            }
            var Coincidencias = 0;
            var guardar = true;

            for (var i = 0; i < $scope.ListadoRecoleccionesGeneral.length; i++) {
                if ($scope.ListadoRecoleccionesGeneral[i].Marcado) {

                    Coincidencias = 0;
                    $scope.ListaResultadoRecolecciones.forEach(recoleccion => {
                        if ($scope.ListadoRecoleccionesGeneral[i].Numero == recoleccion.Numero) {
                            Entidad.Recolecciones.push({
                                Numero: $scope.ListadoRecoleccionesGeneral[i].Numero,
                                NumeroDocumento: $scope.ListadoRecoleccionesGeneral[i].NumeroDocumento,

                            })
                            Coincidencias++;
                        }

                    });
                    if (Coincidencias == 0) {
                        $scope.ListadoRecoleccionesGeneral[i].backGround = 'yellow';
                        guardar = false;
                    }

                }
            }
            $scope.PrimerPaginaRecoleccion();
            if (guardar) {
                RecoleccionesFactory.AsociarRecoleccionesPlanilla(Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {                           
                            ShowSuccess('Las recolecciones se asociaron correctamente a la planilla')
                            FindRecolecciones()
                        }
                        
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                
            } else {
                ShowError('las recolecciones seleccionadas ya se encuentran asociadas, por favor recargue la página');
            }
        }


        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope);
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        }
    }
]);