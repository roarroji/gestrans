﻿SofttoolsApp.controller("AsignarAforadorEtiquetasPreimpresasCtrl", ['$scope', '$timeout', 'ManifiestoFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'OficinasFactory', 'EmpresasFactory', 'PrecintosFactory', 'blockUIConfig', 'TercerosFactory','EtiquetasPreimpresasFactory',
    function ($scope, $timeout, ManifiestoFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, OficinasFactory, EmpresasFactory, PrecintosFactory, blockUIConfig, TercerosFactory, EtiquetasPreimpresasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Procesos' }, { Nombre: 'Asignación Aforador Etiquetas Preimpresas' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.pref = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoOficinasOrigen = [];
        $scope.ListadoOficinasActual = [];
        $scope.Numero = 0;
        $scope.NumeroDocumento = 0;
        var ListaOriginalConsulta = [];
        var PreimpresoInicialConsulta = 0;
        var PreimpresoFinalConsulta = 0;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.AFORADOR_ETIQUETAS_PREIMPRESAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.ModeloFechaFinal = new Date();
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Paginar();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Paginar();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Paginar();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Paginar();
        };


        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarManifiesto';
            }
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            if ($routeParams.Numero > 0) {
                $scope.ModeloNumero = $routeParams.Numero;
                Find();
            }
        }

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'DISPONIBLE', Codigo: CERO },
            { Nombre: 'ASIGNADO GUÌA', Codigo: CODIGO_UNO },
            { Nombre: 'ANULADO', Codigo: CODIGO_DOS },
            { Nombre: 'PENDIENTE AFORADOR', Codigo: CODIGO_TRES }

        ]

        $scope.AutocompleteTercero = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            CadenaPerfiles: 1415 /*Aforador*/,
                            ValorAutocomplete: value, Sync: true
                        })
                    $scope.ListaResponsable = Response.Datos
                }
            }
            return $scope.ListaResponsable
        }

        $scope.ModeloEstado = $scope.ListadoEstados[CERO]

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });
        $scope.AnularDocumento = function (item) {

            $scope.ModalErrorCompleto = ''
            $scope.NumeroDocumento = item.Numero_Precinto
            $scope.Id = item.IdPreci
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnular');
        };

        $scope.ListadoAforador = []
        $scope.AutocompleteAforadores = function (value) {

            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 1) {

                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: 1415 /*Aforador*/,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoAforador = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoAforador)


                }
            }
            return $scope.ListadoAforador
        }

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                $scope.ListadoOficinasOrigen.push({ Nombre: '(TODAS)', Codigo: -1 })
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinasOrigen.push(item)
                    });
                    if ($scope.Sesion.UsuarioAutenticado.Prefijo == PREFIJO_INTEGRA) {
                        $scope.ModeloOficinaOrigen = $linq.Enumerable().From($scope.ListadoOficinasOrigen).First('$.Codigo == 1');
                    } else {
                        $scope.ModeloOficinaOrigen = $linq.Enumerable().From($scope.ListadoOficinasOrigen).First('$.Codigo == -1');
                    }

                }
            }, function (response) {
                ShowError(response.statusText);
            });

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
               // $scope.ListadoOficinasActual.push({ Nombre: '(TODAS)', Codigo: -1 })
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinasActual.push(item)
                    });
                    if ($scope.Sesion.UsuarioAutenticado.Prefijo == PREFIJO_INTEGRA) {
                        $scope.ModeloOficinaActual = $linq.Enumerable().From($scope.ListadoOficinasActual).First('$.Codigo == 1');
                    } else {
                        $scope.ModeloOficinaActual = $linq.Enumerable().From($scope.ListadoOficinasActual).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                    }

                }
            }, function (response) {
                ShowError(response.statusText);
            });
        function Find() {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Consultando Información...");
            $timeout(function () { blockUI.message("Consultando Información..."); Buscar(); }, 100);
        }

        function Buscar() {
            PreimpresoInicialConsulta = $scope.ModeloNumeroInicial != undefined && $scope.ModeloNumeroInicial != null && $scope.ModeloNumeroInicial != '' ? MascaraNumero($scope.ModeloNumeroInicial) : 0;
            PreimpresoFinalConsulta = $scope.ModeloNumeroFinal != undefined && $scope.ModeloNumeroFinal != null && $scope.ModeloNumeroFinal != '' ? MascaraNumero($scope.ModeloNumeroFinal) : 0;
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroInicial: $scope.ModeloNumeroInicial,
                NumeroFinal: $scope.ModeloNumeroFinal,
                Oficina: { Codigo: $scope.ModeloOficinaActual.Codigo },
                OficinaOrigen: { Codigo: $scope.ModeloOficinaOrigen.Codigo },
                ENPD_Numero: $scope.ModeloNumeroPlanilla,
                ENRE_Numero: $scope.ModeloNumeroRemesa,
                ENMD_Numero: $scope.ModeloNumeroManifiesto,
                Responsable: { Codigo: $scope.ModeloAforador != undefined ? $scope.ModeloAforador.Codigo : 0},
                Estado: $scope.ModeloEstado.Codigo
            };

          
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoPrecintos = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    blockUI.delay = 1000;
                    $scope.Pagina = $scope.paginaActual
                    $scope.RegistrosPagina = 10
                    EtiquetasPreimpresasFactory.ConsultarPrecinto(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Numero = 0
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoPrecintos = response.data.Datos
                                    $scope.ModeloNumeroPreimpresoInicial = 0;
                                    $scope.ModeloNumeroPreimpresoFinal = 0;
                                    ListaOriginalConsulta = $scope.ListadoPrecintos;
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                Paginar();
                                blockUI.stop();
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };

        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }
        $scope.Anular = function () {
            var aplicaLiquidaciones = false
            var aplicaCumplidos = false

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Usuario_Anula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Causa_Anula: $scope.ModeloCausaAnula,
                Numero: $scope.Id,

            };
            if (DatosRequeridosAnular()) {
                EtiquetasPreimpresasFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anuló él preimpreso No. ' + $scope.NumeroDocumento);
                            closeModal('modalAnular');
                            Find();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText)                        
                    });
            }
        };

        $scope.ArmarFiltro = function () {
            if ($scope.Numero != undefined && $scope.Numero != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.Numero;
            }
        }
        $scope.NumeroPrecintoTemp = 0
        $scope.LiberarDocumento = function (NumeroPrecinto) {
            $scope.NumeroPrecintoTemp = NumeroPrecinto
            ShowConfirm('Desea liberar el precinto No. ' + NumeroPrecinto + ' de todos los documentos?', LiberarPrecinto)
        }

        function LiberarPrecinto() {
            var ResponsePrecintoLiberado = PrecintosFactory.LiberarPrecinto({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: $scope.NumeroPrecintoTemp, Sync: true });

            if (ResponsePrecintoLiberado != undefined) {
                if (ResponsePrecintoLiberado.Datos == true) {
                    ShowSuccess('Se liberó el precinto No: ' + $scope.NumeroPrecintoTemp + ' satisfactoriamente.');
                    Find();
                }
            }
        }


        $scope.Filtrar = function (PrecintoInicial, PrecintoFinal) {
            $scope.TempPrecintoInicial = PrecintoInicial;
            $scope.TempPrecintoFinal = PrecintoFinal;
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
                PrecintoInicial: PrecintoInicial,
                PrecintoFinal: PrecintoFinal
            }
            $scope.ListadoRecorridosTotal = [];
            $scope.ListadoPrecintos = [];
            if (DatosRequeridosFiltroGuiasPreimpresas()) {
            
                $scope.TempPrecintoInicial = PrecintoInicial;
                $scope.TempPrecintoFinal = PrecintoFinal;
                var PreimpresoInicial = PrecintoInicial == 0 || PrecintoInicial == undefined || PrecintoInicial == null || PrecintoInicial == '' ? PreimpresoInicialConsulta : PrecintoInicial
                var PreimpresoFinal = PrecintoFinal == 0 || PrecintoFinal == undefined || PrecintoFinal == null || PrecintoFinal == '' ? PreimpresoFinalConsulta : PrecintoFinal
                if (PreimpresoFinal == 0) {
                    $scope.ListadoPrecintos = $linq.Enumerable().From(ListaOriginalConsulta).Where('$.Numero_Precinto >=' + PreimpresoInicial).Select('$').ToArray();
                } else {
                    $scope.ListadoPrecintos = $linq.Enumerable().From(ListaOriginalConsulta).Where('$.Numero_Precinto <=' + PreimpresoFinal + ' && $.Numero_Precinto >=' + PreimpresoInicial).Select('$').ToArray();
                }

                $scope.ListadoPrecintos.forEach(item => {
                    if (item.Numero_Precinto >= $scope.TempPrecintoInicial && item.Numero_Precinto <= $scope.TempPrecintoFinal && item.Anulado != 1 && item.Remesa.NumeroDocumento == 0) {
                        item.Seleccionado = true;
                    }
                });

                Paginar();


            };

        }

        function DatosRequeridosFiltroGuiasPreimpresas() {
            var continuar = true;
            $scope.MensajesError = [];
            if (PreimpresoInicialConsulta != undefined && PreimpresoInicialConsulta != 0 && PreimpresoInicialConsulta != null && PreimpresoInicialConsulta != '') {
                if ($scope.TempPrecintoInicial < PreimpresoInicialConsulta) {
                    $scope.MensajesError.push('El valor inicial del rango de etiquetas ingresado, no puede ser menor a los preimpresos de la consulta inicial.');
                    continuar = false;
                }
                                
            }

            if ($scope.TempPrecintoInicial > $scope.TempPrecintoFinal) {
                $scope.MensajesError.push('El número etiqueta inicial, no puede ser mayor al número etiqueta final.');
                continuar = false;
            }

            if (PreimpresoFinalConsulta != undefined && PreimpresoFinalConsulta != 0 && PreimpresoFinalConsulta != null && PreimpresoFinalConsulta != '') {
                if ($scope.TempPrecintoFinal > PreimpresoFinalConsulta) {
                    $scope.MensajesError.push('El valor final del rango de etiquetas ingresado, no puede ser mayor a los etiquetas de la consulta inicial.');
                    continuar = false;
                }
            }

            return continuar;

        }

        $scope.Asignar = function () {
            var NumerosPreimpresos = '';

            $scope.ListadoPrecintos.forEach(item => {
                if (item.Seleccionado) {
                    NumerosPreimpresos = NumerosPreimpresos + item.Numero_Precinto + ','
                }
            });


            $scope.MensajesError = [];
            if ($scope.ModeloAforador != undefined && $scope.ModeloAforador != null && $scope.ModeloAforador != '') {
                var NumeroSeleccionados = 0;

                $scope.ListadoPrecintos.forEach(item => {
                    if (item.Seleccionado) {
                        NumeroSeleccionados++
                    }
                });

                if (NumeroSeleccionados > 0) {
                    if ($scope.ModeloAforador.Codigo > 0) {
                        var Entidad = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Responsable: { Codigo: $scope.ModeloAforador.Codigo },
                            NumerosPreimpresos: NumerosPreimpresos,
                            Sync: true
                        }
                        ActualizarResponsable(Entidad);
                    }
                } else {
                    $scope.MensajesError.push('Debe seleccionar al menos una etiqueta preimpresa')
                }
            } else {
                $scope.MensajesError.push('Debe seleccionar un aforador')
            }
        }

        $scope.Desasignar = function () {
            var NumeroSeleccionados = 0;

            $scope.ListadoPrecintos.forEach(item => {
                if (item.Seleccionado) {
                    NumeroSeleccionados++
                }               
            });

            if (NumeroSeleccionados > 0) {
                var NumerosPreimpresos = '';

                $scope.ListadoPrecintos.forEach(item => {
                    if (item.Seleccionado) {
                        NumerosPreimpresos = NumerosPreimpresos + item.Numero_Precinto + ','
                    }
                });

                var Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Responsable: { Codigo: 0 },
                    NumerosPreimpresos: NumerosPreimpresos,
                    Sync: true
                }
                ActualizarResponsable(Entidad);
            } else {
                $scope.MensajesError.push('Debe seleccionar al menos una etiqueta preimpresa')
            }
        }

        function ActualizarResponsable(Entidad) {

            var ResponseActualizacionResponsable = EtiquetasPreimpresasFactory.ActualizarResponsable(Entidad);
            if (ResponseActualizacionResponsable != undefined) {
                if (ResponseActualizacionResponsable.ProcesoExitoso) {
                    if (Entidad.Responsable.Codigo > 0) {
                        if (ResponseActualizacionResponsable.Datos > 0) {
                            ShowSuccess('Se actualizaron ' + ResponseActualizacionResponsable.Datos+ ' etiquetas preimpresas con el Aforador ' + $scope.ModeloAforador.NombreCompleto)
                            Find();
                        } else {
                            ShowError('No se actualizaron etiquetas preimpresas, verifique que esté seleccionada al menos una')
                        }
                    } else {
                        if (ResponseActualizacionResponsable.Datos > 0) {
                            ShowSuccess('Se desasignaron ' + ResponseActualizacionResponsable.Datos + ' etiquetas preimpresas')
                            Find();
                        } else {
                            ShowError('No se actualizaron etiquetas preimpresas, verifique que esté seleccionada al menos una')
                        }
                    }
                } else {
                    ShowError(ResponseActualizacionResponsable.MensajeOperacion)
                }
            }
        } 

        $scope.MarcarRecorridos = function (encabezadocheck) {
            $scope.ListadoPrecintos.forEach(item => {
                if (item.Anulado == 0 && item.Remesa.NumeroDocumento == 0) {
                    item.Seleccionado = encabezadocheck
                }
            })

        }

        function Paginar() {
            $scope.ListadoPreimpresos = [];
            for (var i = 0; i < $scope.ListadoPrecintos.length; i++) {
                var item = $scope.ListadoPrecintos[i]
                var RegistroInicial = ($scope.paginaActual * $scope.cantidadRegistrosPorPaginas) - $scope.cantidadRegistrosPorPaginas;
                var RegistroFinal = ($scope.paginaActual * $scope.cantidadRegistrosPorPaginas) - 1;
                if (i >= RegistroInicial && i <= RegistroFinal) {
                    $scope.ListadoPreimpresos.push(item);
                }
                $scope.totalPaginas = Math.ceil($scope.ListadoPrecintos.length / $scope.cantidadRegistrosPorPaginas)
                $scope.totalRegistros = $scope.ListadoPrecintos.length
            }
        }

        $scope.ValidarEstado = function () {
            if ($scope.ModeloEstado.Codigo == CODIGO_TRES) {
                $scope.ModeloAforador = undefined
            }
        }

    }]);