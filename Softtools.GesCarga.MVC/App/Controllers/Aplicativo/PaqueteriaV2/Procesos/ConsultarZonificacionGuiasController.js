﻿SofttoolsApp.controller("ConsultarZonificacionGuiasCtrl", ['$scope', '$routeParams', '$timeout', 'CiudadesFactory', '$linq', 'blockUI', 'OficinasFactory', 'PaisesFactory', 'ZonasFactory', 'DepartamentosFactory', 'OficinasFactory', 'blockUIConfig',
    function ($scope, $routeParams, $timeout, CiudadesFactory, $linq, blockUI, OficinasFactory, PaisesFactory, ZonasFactory, DepartamentosFactory, OficinasFactory, blockUIConfig) {

        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Procesos' }, { Nombre: 'Zonificación Guías' }];
        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ContMedios = 0
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'Activa', Codigo: 1 },
            { Nombre: 'Inactiva', Codigo: 0 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.ListadoEstadosZonas = [
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];
        $scope.ModalEstadoZona = $linq.Enumerable().From($scope.ListadoEstadosZonas).First('$.Codigo == 1');
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //$scope.FechaFinal = new Date();
        //$scope.FechaInicial = new Date();


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CIUDADES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinas = [];
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo != 0) {
                            $scope.ListadoOficinas.push(item);
                        }
                    });
                    $scope.Oficinas = response.data.Datos[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de paises*/

        OficinasFactory.ConsultarRegionesPaises({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaRegiones = response.data.Datos;

                        if ($scope.CodigoRegion !== undefined && $scope.CodigoRegion !== null && $scope.CodigoRegion !== '') {
                            $scope.Modelo.Region = $linq.Enumerable().From($scope.ListaRegiones).First('$.Codigo ==' + $scope.CodigoRegion);
                        }
                    }
                    else {
                        $scope.ListaRegiones = [];
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de Ciudades */

        $scope.ListadoCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades);
                }
            }
            return $scope.ListadoCiudades;
        };


        $scope.ciudadesBuscar = []

        $scope.AsignarCiudades = function () {
            console.log('listado', $scope.Ciudad)
            if ($scope.Ciudad != "" ) {
            $scope.ciudadesBuscar.push($scope.Ciudad) 
            }
            $scope.Ciudad = ''

        };
        $scope.EliminarCiudad = function (indexCiudad, item) {
            $scope.ciudadesBuscar.splice(indexCiudad, 1)
        }


        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarCiudades';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0
                if ($scope.NumeroPlanilla != undefined) {
                    if ($scope.NumeroPlanilla.length > 0) {

                        $scope.FechaInicial = ''
                        $scope.FechaFinal = ''
                    }
                }
              
                Find()
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];


            $scope.CadenaCiudades = ''
            $scope.ciudadesBuscar.forEach(function (item) {
                if (item.Codigo > 0) {
                    $scope.CadenaCiudades = $scope.CadenaCiudades + item.Codigo + ',';
                } else {
                    $scope.CadenaCiudades = ''
                }
            });


            $scope.ListadoCiudades = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        FechaInicial: $scope.FechaInicial,
                        FechaFinal: $scope.FechaFinal,
                        NumeroInicial: $scope.NumeroInicial,
                        RemesaPaqueteria: {
                            NumeroPlanilla: $scope.NumeroPlanilla
                        },
                        CadenaCiudades: $scope.CadenaCiudades,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        sync: true
                    }
                    blockUI.delay = 1000;
                    ZonasFactory.ConsultarZonificacionGuias(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoRemesasPaqueteria = response.data.Datos

                                    for (var i = 0; i < $scope.ListadoRemesasPaqueteria.length; i++) {
                                        $scope.ListadoRemesasPaqueteria[i].listaDeZonas = [];
                                        $scope.listaDeZonas = []

                                        $scope.ListadoRemesasPaqueteria[i].listaDeZonas = ZonasFactory.Consultar({
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            Ciudad: {
                                                Codigo: $scope.ListadoRemesasPaqueteria[i].Remesa.CiudadDestinatario.Codigo
                                            },Estado:ESTADO_ACTIVO
                                            , Sync: true
                                        }).Datos

                                    }

                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);

                                    $scope.Buscando = true;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.ListadoRemesasPaqueteria = []
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                            }
                        }, function (response) {
                            $scope.Buscando = false;
                        });

                }
            }
            blockUI.stop();
        }

        $scope.AsignarZonas = function (item) {
            var countmarcadas = 0;
            if ($scope.ListadoRemesasPaqueteria.length > 0) {

                for (var i = 0; i < $scope.ListadoRemesasPaqueteria.length; i++) {

                    if ($scope.ListadoRemesasPaqueteria[i].Marcado) {
                        console.log('marcada', $scope.ListadoRemesasPaqueteria[i].Marcado)
                        countmarcadas++;
                    }
                }
                if (countmarcadas > 0) {
                    showModal('modalConfirmacionCarbiarZonas');
                }
                else {
                    ShowError('Debe seleccionar al menos una Guía');
                }
            }
            else {
                ShowError('Debe seleccionar al menos una Guía');
            }
        };

        $scope.CambiarZonasGuias = function () {

            $scope.NombreReporte = NOMBRE_REPORTE_ZONIFICACION_GUIA;
            $scope.FiltroArmado = '';
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.CadenaRemesas = '';

            closeModal('modalConfirmacionCarbiarZonas');
            blockUI.start();
            blockUI.message('Procesando Guias...');

            $timeout(function () {
                var Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    ListaZonas: []
                };
                for (var obj = 0; obj < $scope.ListadoRemesasPaqueteria.length; obj++) {
                    if ($scope.ListadoRemesasPaqueteria[obj].Marcado) {

                        $scope.CadenaRemesas = $scope.CadenaRemesas + $scope.ListadoRemesasPaqueteria[obj].Remesa.Numero + ',';


                        Entidad.ListaZonas.push({
                            RemesaPaqueteria: {
                                Numero: $scope.ListadoRemesasPaqueteria[obj].Remesa.Numero,
                                NumeroDocumento: $scope.ListadoRemesasPaqueteria[obj].Remesa.NumeroDocumento
                            },
                            Numero: $scope.ListadoRemesasPaqueteria[obj].Zona.Codigo
                        });
                    }
                }
                $scope.CadenaRemesas = $scope.CadenaRemesas.slice(0, -1);

                console.log('CADENA ', $scope.CadenaRemesas )

                ZonasFactory.InsertarZonificacionGuias(Entidad).
                    then(function (response) {
                        console.log('finproceso', response)
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('las Zonas se modificaron correctamente,Descargando Reporte ');
                            $scope.ArmarFiltro();

                            window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + 1);

                        }

                    }, function (response) {
                        ShowError(response.statusText);
                    });
                blockUI.stop();
                $timeout(blockUI.stop(), 1000);
            }, 1000);

        };




        $scope.ArmarFiltro = function () {

            if ($scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== null && $scope.NumeroInicial > 0) {
                $scope.FiltroArmado += '&Numero=' + $scope.NumeroInicial;
            }
            if ($scope.NumeroPlanilla !== undefined && $scope.NumeroPlanilla !== '' && $scope.NumeroPlanilla !== null && $scope.NumeroFinal > 0) {
                $scope.FiltroArmado += '&Planilla=' + $scope.NumeroPlanilla;
            }

            if ($scope.FechaInicial !== undefined && $scope.FechaInicial !== '' && $scope.FechaInicial !== null) {
                var FechaInicial = Formatear_Fecha_Mes_Dia_Ano($scope.FechaInicial);
                $scope.FiltroArmado += '&Fecha_Incial=' + FechaInicial;
            }
            if ($scope.FechaFinal !== undefined && $scope.FechaFinal !== '' && $scope.FechaFinal !== null) {
                var FechaFinal = Formatear_Fecha_Mes_Dia_Ano($scope.FechaFinal);
                $scope.FiltroArmado += '&Fecha_Final=' + FechaFinal;
            }
            if ($scope.CadenaRemesas != undefined && $scope.CadenaRemesas !== '' && $scope.CadenaRemesas !== null) {
                $scope.FiltroArmado += '&ListaRemesas=' + $scope.CadenaRemesas;
            }
        };
        //----------------------------------------------------------------------------------------------------
        $scope.MaskNumero = function (option) {
            try { $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno) } catch (e) { }
        };

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };


    }]);