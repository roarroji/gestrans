﻿SofttoolsApp.controller("GestionarGuiasPreimpresasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory', 'PrecintosFactory', 'TercerosFactory', 'OficinasFactory', 'blockUIConfig','GuiasPreimpresasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ValorCatalogosFactory, PrecintosFactory, TercerosFactory, OficinasFactory, blockUIConfig, GuiasPreimpresasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Procesos' }, { Nombre: 'Asignación Oficinas Guías Preimpresas' }, { Nombre: 'Gestionar' }];

        $scope.ModeloCliente = []
        $scope.ListaResponsable = []
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.Numero = 0;
        $scope.ModeloNumeroPreimpresoInicial = 0;
        $scope.ModeloNumeroPreimpresoFinal = 0;
        $scope.ModeloFecha = new Date();
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.GUIAS_PREIMPRESAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.Obtenido = false;
        $scope.Ocupado = false;
        $scope.paginaActual = 1;
        $scope.ini = 0;
        $scope.Numero = 0;
        $scope.ModeloCodigo = 0;
        $scope.CodigoOficina = 0;
        $scope.CodigoOficinaDestino = 0;
        $scope.CodigoResponsable = 0;
        $scope.CodigoTipoPresinto = 0;
        $scope.ListadoPrecintos = [];
        $scope.ListadoPrecintosAuxiliar = [];
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.ListaResultadoFiltroConsulta = []
        $scope.ListadoTipoPrecinto = [];
        $scope.ListadoRecorridosTotal = [];
        $scope.ListaPuc = [];
        $scope.ModeloTipoPrecintoNombre = '';
        $scope.ModeloTipoPrecinto = [];
        $scope.ModalImpuesto = [];
        $scope.ModeloCuenta = [];
        $scope.ModeloOficina = [];
        $scope.ModeloOficinaDestino = [];
        $scope.ModeloTipoPrecinto = [];
        $scope.ModeloCliente = [];
        $scope.PrecintoInicial = 0;
        $scope.PrecintoFinal = 0;
        $scope.ValorPrecintoIncial = 0;
        $scope.ValorPrecintoFinal = 0;
        $scope.TempPrecintoInicial = 0;
        $scope.TempPrecintoFinal = 0;
        $scope.Cont = 0;
        $scope.RecorridosSeleccionados = false;
        $scope.ListadoCiudades = [];
        $scope.ListadoOficinas = [];
        $scope.ListadoOficinasDestino = [];
        $scope.inicial = {
            Nombre: '(Seleccione Oficina)', Codigo: 0
        };
        $scope.ListadoOficinas.push($scope.inicial);
        $scope.ListadoOficinasDestino.push($scope.inicial)

        if ($routeParams.Numero !== undefined) {
            $scope.Numero = $routeParams.Numero;
            $scope.Obtenido = true;
        } else {
            $scope.Numero = 0;
            $scope.Obtenido = false;
        }


        //-----Asignaciones ---------------------

        $scope.AsignarCodigoAlterno = function (CodigoAlterno) {

            $scope.ModeloCodigoAlterno = CodigoAlterno;
        }

        $scope.FiltroPrecintos = function (PrecintoInicial, PrecintoFinal) {
            $scope.TempPrecintoInicial = MascaraNumero(PrecintoInicial);
            $scope.TempPrecintoFinal = MascaraNumero(PrecintoFinal);
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
                PrecintoInicial: MascaraNumero(PrecintoInicial),
                PrecintoFinal: MascaraNumero(PrecintoFinal)
            }
            $scope.ListadoRecorridosTotal = [];
            $scope.ListadoPrecintos = [];
            if (DatosRequueridosFiltoOrecintos()) {
                GuiasPreimpresasFactory.Obtener(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if ($scope.ListadoPrecintos !== null && $scope.ListadoPrecintos !== undefined) {

                                response.data.Datos.Lista.forEach(function (item) {
                                    if (item.Estado == 1 && item.Anulado == 0 && item.ENPD_Numero > 0 & item.Estado != 2) {
                                        item = {
                                            IdPreci: item.IdPreci,
                                            Numero_Precinto: item.Numero_Precinto,
                                            Oficina: { Nombre: item.Oficina.Nombre },
                                            OficinaOrigen: { Nombre: item.OficinaOrigen.Nombre },
                                            ENPD_Numero: item.ENPD_Numero,
                                            ENRE_Numero: item.ENRE_Numero,
                                            ENMD_Numero: item.ENMD_Numero,
                                            OrdenCargue: { Numero: item.OrdenCargue.Numero, NumeroDocumento: item.OrdenCargue.NumeroDocumento },
                                            Anulado: item.Anulado,
                                            Estado: item.Estado,
                                            Seleccionado: true
                                        }
                                        $scope.ListadoRecorridosTotal.push(item);
                                    }
                                    else {
                                        item = {
                                            IdPreci: item.IdPreci,
                                            Numero_Precinto: item.Numero_Precinto,
                                            Oficina: { Nombre: item.Oficina.Nombre },
                                            OficinaOrigen: { Nombre: item.OficinaOrigen.Nombre },
                                            OrdenCargue: { Numero: item.OrdenCargue.Numero, NumeroDocumento: item.OrdenCargue.NumeroDocumento },
                                            ENPD_Numero: item.ENPD_Numero,
                                            ENRE_Numero: item.ENRE_Numero,
                                            ENMD_Numero: item.ENMD_Numero,
                                            Anulado: item.Anulado,
                                            Estado: item.Estado,
                                            Seleccionado: false
                                        }
                                        //item.Seleccionado = true;
                                        $scope.ListadoRecorridosTotal.push(item);

                                    }
                                });
                                if ($scope.ListadoRecorridosTotal.length > 0) {
                                    $scope.ListadoRecorridosTotal = $linq.Enumerable().From($scope.ListadoRecorridosTotal).OrderBy(function (x) {
                                        return x.Numero
                                    }).ToArray();
                                }
                                var i = 0;
                                for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                                    if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                        $scope.ListadoPrecintos.push($scope.ListadoRecorridosTotal[i])
                                    }
                                }


                                $scope.totalRegistros = response.data.Datos.Lista.length;
                                $scope.totalPaginas = Math.ceil($scope.ListadoRecorridosTotal.length / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';

                                $scope.ListadoRecorridosTotal.forEach(function (item) {
                                    if (item.Numero_Precinto >= $scope.TempPrecintoInicial && item.Numero_Precinto <= $scope.TempPrecintoFinal && item.Anulado != 1 && item.Estado !=2) {
                                        item.Seleccionado = true;
                                    }
                                });
                            }
                            else {
                                $scope.ListadoPrecintos = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    });
                $scope.TempPrecintoInicial = MascaraNumero(PrecintoInicial);
                $scope.TempPrecintoFinal = MascaraNumero(PrecintoFinal);



            };

        }

        function DatosRequueridosFiltoOrecintos() {
            var continuar = true;
            $scope.MensajesError = [];

            if ($scope.TempPrecintoInicial < $scope.ValorPrecintoIncial) {
                $scope.MensajesError.push('El valor inicial del rango de preimpresos ingresado, no puede ser menor a los preimpresos asignados inicialmente. ');
                continuar = false;
            }

            if ($scope.TempPrecintoFinal > $scope.ValorPrecintoFinal) {
                $scope.MensajesError.push('El valor final del rango de preimpresos ingresado, no puede ser mayor a los preimpresos asignados inicialmente.');
                continuar = false;
            }



            return continuar;

        }


        $scope.PrimerPagina = function () {
            /*Verificar si la página actual ya está guardada si no lo esta procede a realizar la busqueda*/
            if ($scope.paginaActual > 1) {

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                    $scope.paginaActual = 1;
                    $scope.ListadoPrecintos = [];

                    var i = 0;
                    for (i = 0; i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina) {
                            $scope.ListadoPrecintos.push($scope.ListaResultadoFiltroConsulta[i])
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = 1;
                        $scope.ListadoPrecintos = [];

                        var i = 0;
                        for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina) {
                                $scope.ListadoPrecintos.push($scope.ListadoRecorridosTotal[i])
                            }
                        }
                    }
                }

            }


        };

        $scope.Siguiente = function () {

            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.ListadoPrecintos = [];
                $scope.PaginaAuxiliar = $scope.paginaActual;
                $scope.paginaActual += 1;

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoPrecintos.push($scope.ListaResultadoFiltroConsulta[i]);
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoPrecintos.push($scope.ListadoRecorridosTotal[i]);
                            }
                        }
                    }
                }
            }

        }

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                    $scope.ListadoPrecintos = [];
                    $scope.paginaActual -= 1;

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoPrecintos.push($scope.ListaResultadoFiltroConsulta[i]);
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.ListadoPrecintos = [];
                        $scope.paginaActual -= 1;

                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoPrecintos.push($scope.ListadoRecorridosTotal[i]);
                            }
                        }
                    }
                }

            }
        };

        $scope.UltimaPagina = function () {

            if ($scope.paginaActual < $scope.totalPaginas) {

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListadoPrecintos = [];

                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoPrecintos.push($scope.ListaResultadoFiltroConsulta[i])
                            }
                        }
                    }
                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListadoPrecintos = [];

                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoPrecintos.push($scope.ListadoRecorridosTotal[i])
                            }
                        }
                    }
                }

            }
        };

        $scope.AutocompleteOficinasDestino = function (value) {

            $scope.ListadoOficinasDestino = [];
            $scope.ListadoOficinasDestino = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
            return $scope.ListadoOficinasDestino;
        }

        function CargarAutocompletes() {

            //Clientes
            TercerosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: { Codigo: ESTADO_ACTIVO },
                CadenaPerfiles: PERFIL_EMPLEADO
            }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.push({ NombreCompleto: '', Codigo: 0 })
                            $scope.ListaResponsable = response.data.Datos;

                            if ($scope.CodigoCliente != undefined && $scope.CodigoCliente != '' && $scope.CodigoCliente != 0 && $scope.CodigoCliente != null) {
                                if ($scope.CodigoCliente > 0) {
                                    $scope.ModeloCliente = $linq.Enumerable().From($scope.ListaResponsable).First('$.Codigo ==' + $scope.CodigoCliente);
                                } else {
                                    $scope.ModeloCliente = $linq.Enumerable().From($scope.ListaResponsable).First('$.Codigo ==0');
                                }
                            } else {
                                $scope.ModeloCliente = $linq.Enumerable().From($scope.ListaResponsable).First('$.Codigo ==0');
                            }

                        }
                        else {
                            $scope.ListaResponsable = [];
                            $scope.ModeloCliente = null;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });


            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_PRECINTO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoRecaudo = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoPrecinto = response.data.Datos;
                            $scope.ModeloTipoPrecinto = $scope.ListadoTipoPrecinto[0];
                            if ($scope.CodigoTipoPresinto > CERO && $scope.CodigoTipoPresinto !== undefined) {
                                $scope.ModeloTipoPrecinto = $linq.Enumerable().From($scope.ListadoTipoPrecinto).First('$.Codigo ==' + $scope.CodigoTipoPresinto);
                            }
                        }
                        else {
                            $scope.ListadoTipoPrecinto = []
                        }
                    }
                }, function (response) {
                });


            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item);

                        });
                        $scope.ModeloOficina = $scope.ListadoOficinas[0];
                        if ($scope.CodigoOficina > 0 && $scope.CodigoOficina !== undefined) {
                            $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.CodigoOficina);
                        } else {
                            $scope.ModeloOficina = $scope.ListadoOficinas[0];
                        }


                    }
                }, function (response) {

                    ShowError(response.statusText);

                });

            $scope.AutocompleteTercero = function (value) {
                if (value.length > 0) {
                    if ((value.length % 3) == 0 || value.length == 2) {
                        /*Cargar Autocomplete de propietario*/
                        blockUIConfig.autoBlock = false;
                        var Response = TercerosFactory.Consultar(
                            {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                CadenaPerfiles: 1415 /*Aforador*/,
                                ValorAutocomplete: value, Sync: true
                            })
                        $scope.ListaResponsable = ValidarListadoAutocomplete(Response.Datos, $scope.ListaResponsable)
                    }
                }
                return $scope.ListaResponsable
            }

            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoOficinasDestino = [];
                        $scope.ListadoOficinasDestino.push($scope.inicial)
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinasDestino.push(item);
                        });
                        $scope.ModeloOficinaDestino = $scope.ListadoOficinasDestino[0];
                        if ($scope.CodigoOficinaDestino > 0 && $scope.CodigoOficinaDestino !== undefined) {
                            $scope.ModeloOficinaDestino = $linq.Enumerable().From($scope.ListadoOficinasDestino).First('$.Codigo ==' + $scope.CodigoOficinaDestino);
                        } else {
                            $scope.ModeloOficinaDestino = $scope.ListadoOficinasDestino[0];
                        }

                    }
                }, function (response) {

                    ShowError(response.statusText);

                });

        };





        if ($scope.Numero > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR OFICINA';
            $scope.Deshabilitar = true;

            Obtener();
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/

        $scope.Obtener = function (Numero) {
            $scope.Numero = Numero;
            Obtener();
        }

        function Obtener() {
            blockUI.start('Cargando Información ');
            $scope.Obtenido = true;
            $timeout(function () {
                blockUI.message('Cargando Información ');
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero
            };

            blockUI.delay = 1000;
            CargarAutocompletes();
            GuiasPreimpresasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ModeloNumero = response.data.Datos.Numero;
                        $scope.Numero = response.data.Datos.Numero;
                        $scope.ModeloFecha = new Date(response.data.Datos.Fecha_Entrega)
                        $scope.ModeloNumeroPrecintoInicial = response.data.Datos.PrecintoInicial
                        $scope.ModeloNumeroPreimpresoInicial = MascaraValores(response.data.Datos.PrecintoInicial)
                        $scope.ModeloNumeroPrecintoFinal = response.data.Datos.PrecintoFinal
                        $scope.ModeloNumeroPreimpresoFinal = MascaraValores(response.data.Datos.PrecintoFinal)
                        $scope.ValorPrecintoIncial = $scope.ModeloNumeroPrecintoInicial;
                        $scope.ValorPrecintoFinal = $scope.ModeloNumeroPrecintoFinal;

                        if ($scope.ListadoPrecintos !== null && $scope.ListadoPrecintos !== undefined) {
                            console.log("responseObtener: ", response);
                            response.data.Datos.Lista.forEach(function (item) {
                                if (item.Estado == 1 && item.Anulado == 0 && item.ENPD_Numero > 0 && item.Estado != 2) {
                                    item = {
                                        IdPreci: item.IdPreci,
                                        Numero_Precinto: item.Numero_Precinto,
                                        Oficina: { Nombre: item.Oficina.Nombre },
                                        OficinaOrigen: { Nombre: item.OficinaOrigen.Nombre },
                                        ENPD_Numero: item.ENPD_Numero,
                                        ENRE_Numero: item.ENRE_Numero,
                                        ENMD_Numero: item.ENMD_Numero,
                                        Anulado: item.Anulado,
                                        Estado: item.Estado,
                                        Seleccionado: true
                                    }
                                    $scope.ListadoRecorridosTotal.push(item);
                                }
                                else {
                                    item = {
                                        IdPreci: item.IdPreci,
                                        Numero_Precinto: item.Numero_Precinto,
                                        Oficina: { Nombre: item.Oficina.Nombre },
                                        OficinaOrigen: { Nombre: item.OficinaOrigen.Nombre },
                                        ENPD_Numero: item.ENPD_Numero,
                                        ENRE_Numero: item.ENRE_Numero,
                                        ENMD_Numero: item.ENMD_Numero,
                                        Anulado: item.Anulado,
                                        Estado: item.Estado,
                                        Seleccionado: false
                                    }
                                    $scope.ListadoRecorridosTotal.push(item);
                                }
                            });
                            if ($scope.ListadoRecorridosTotal.length > 0) {
                                $scope.ListadoRecorridosTotal = $linq.Enumerable().From($scope.ListadoRecorridosTotal).OrderBy(function (x) {
                                    return x.Numero
                                }).ToArray();
                            }
                            var i = 0;
                            for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                                if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                    $scope.ListadoPrecintos.push($scope.ListadoRecorridosTotal[i])
                                }
                            }


                            $scope.totalRegistros = response.data.Datos.Lista.length;
                            $scope.totalPaginas = Math.ceil($scope.ListadoRecorridosTotal.length / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = true;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.ListadoPrecintos = "";
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }

                        CargarAutocompletes()

                        $scope.CodigoOficina = response.data.Datos.Oficina.Codigo;
                        $scope.CodigoOficinaDestino = response.data.Datos.OficinaDestino.Codigo;

                        $scope.NombreOficinaActual = response.data.Datos.OficinaDestino.Nombre;

                        $scope.ModeloResponsable = $scope.CargarTercero(response.data.Datos.Responsable.Codigo)


                        //$scope.CodigoTipoPresinto = response.data.Datos.Tipo_Presinto.Codigo;

                        if ($scope.ListadoTipoPrecinto.length > CERO && $scope.CodigoTipoPresinto > 0) {
                            $scope.ModeloTipoPrecinto = $linq.Enumerable().From($scope.ListadoTipoPrecinto).First('$.Codigo ==' + $scope.CodigoTipoPresinto);
                            $scope.ModeloTipoPrecintoNombre = $scope.ModeloTipoPrecinto.Nombre
                        }

                        if ($scope.ListadoOficinas.length > CERO && $scope.CodigoOficina > 0) {
                            $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.CodigoOficina);
                        }

                        if ($scope.ListadoOficinasDestino.length > CERO && $scope.CodigoOficinaDestino > 0) {
                            $scope.ModeloOficinaDestino = $linq.Enumerable().From($scope.ListadoOficinasDestino).First('$.Codigo ==' + $scope.CodigoOficinaDestino);
                        }








                    }
                    else {
                        ShowError('No se logro consultar la guía ');
                        blockUI.stop();
                        document.location.href = '#!ConsultarAsignacionOficinasGuiasPreimpresas';
                    }

                    console.log("ObtenerPrecintos: ", $scope.ListadoPrecintos);
                }, function (response) {
                        ShowError(response.statusText);
                        blockUI.stop();
                        document.location.href = '#!ConsultarAsignacionOficinasGuiasPreimpresas';
                });
            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');

        };

        $scope.PantallaBloqueoGuardar = function () {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Guardando Información...");
            $timeout(function () { blockUI.message("Guardando Información..."); $scope.Guardar(); }, 100);
        }

        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridosImpuesto()) {
                if ($scope.Numero > 0) {
                    $scope.ListadoPrecintosAuxiliar = [];
                    $scope.ListadoRecorridosTotal.forEach(function (item) {
                        if (item.Seleccionado == true) {
                            if (!(item.Estado == 1 && item.Anulado == 0 && item.ENPD_Numero > 0)) {

                                if ($scope.ini == 0) {
                                    $scope.PrecintoInicial = item.Numero_Precinto;
                                    $scope.ini = 1;

                                }

                                item = {
                                    IdPreci: item.IdPreci,
                                    Numero_Precinto: item.Numero_Precinto,
                                    Oficina: { Nombre: item.Oficina.Nombre },
                                    OficinaOrigen: { Nombre: item.OficinaOrigen.Nombre },
                                    ENPD_Numero: item.ENPD_Numero,
                                    ENMD_Numero: item.ENMD_Numero,
                                    Anulado: item.Anulado,
                                    Estado: item.Estado
                                },
                                    $scope.ListadoPrecintosAuxiliar.push(item);
                                $scope.PrecintoFinal = item.Numero_Precinto;

                            }
                        }

                    });
                }

                $scope.objEnviar = {
                    Numero: $scope.Numero,
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TAPR: { Codigo: $scope.Numero > 0 ? CODIGO_CATALOGO_TRANSLADO_PRECINTO : CODIGO_CATALOGO_ASIGNACION_PRECINTO },
                    Fecha_Entrega: $scope.ModeloFecha,                   
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Responsable: { Codigo: $scope.ModeloResponsable.Codigo },
                    oficina: { Codigo: $scope.ModeloOficina.Codigo },
                    OficinaDestino: { Codigo: $scope.Numero > 0 ? $scope.ModeloOficinaDestino.Codigo : $scope.ModeloOficina.Codigo},
                    PrecintoInicial: $scope.Numero > 0 ? $linq.Enumerable().From($scope.ListadoPrecintosAuxiliar).Min('$.Numero_Precinto'): MascaraNumero($scope.ModeloNumeroPreimpresoInicial),
                    PrecintoFinal: $scope.Numero > 0 ? $linq.Enumerable().From($scope.ListadoPrecintosAuxiliar).Max('$.Numero_Precinto') : MascaraNumero($scope.ModeloNumeroPreimpresoFinal),
                    Lista: $scope.Numero > 0 ? $scope.ListadoPrecintosAuxiliar : []
                };

                GuiasPreimpresasFactory.GuardarPrecintos($scope.objEnviar).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos.Numero > 0) {
                                if ($scope.Numero > 0) {
                                    ShowSuccess('Se Trasladaron ' + response.data.Datos.NumeroTranslados + ' guías preimpresas a la oficina ' + $scope.ModeloOficinaDestino.Nombre);
                                } else {
                                    ShowSuccess('Se Guardaron ' + response.data.Datos.NumeroTranslados + '  guías preimpresas');
                                }
                                blockUI.stop();
                                location.href = '#!ConsultarAsignacionOficinasGuiasPreimpresas/' + response.data.Datos.Numero;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            blockUI.stop();
           
        };


        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/


        function DatosRequeridosImpuesto() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;
            

                       
            if ($scope.ModeloResponsable.Codigo == -1 || $scope.ModeloResponsable.Codigo == undefined || $scope.ModeloResponsable.Codigo == null || $scope.ModeloResponsable.Codigo == "") {  //NO APLICA
                $scope.MensajesError.push('Debe seleccionar un Responsable');
                continuar = false;
            }
            if ($scope.Numero == 0) {
                var diferencia = 0;
                diferencia = MascaraNumero($scope.ModeloNumeroPreimpresoFinal) - MascaraNumero($scope.ModeloNumeroPreimpresoInicial)
                if (MascaraNumero($scope.ModeloNumeroPreimpresoInicial) == 0 || $scope.ModeloNumeroPreimpresoInicial == undefined || $scope.ModeloNumeroPreimpresoInicial == null || $scope.ModeloNumeroPreimpresoInicial == "") {
                    $scope.MensajesError.push('El valor del preimpreso inicial mayor debe ser mayor a cero ');
                    continuar = false;
                }
                if (MascaraNumero($scope.ModeloNumeroPreimpresoFinal) == 0 || $scope.ModeloNumeroPreimpresoFinal == undefined || $scope.ModeloNumeroPreimpresoFinal == null || $scope.ModeloNumeroPreimpresoFinal == "") {
                    $scope.MensajesError.push('El valor del preimpreso final mayor debe ser mayor a cero ');
                    continuar = false;
                }

                if (MascaraNumero($scope.ModeloNumeroPreimpresoInicial) > MascaraNumero($scope.ModeloNumeroPreimpresoFinal)) {
                    $scope.MensajesError.push('El preimpreso incial no puede ser mayor al final');
                    continuar = false;
                }

                if (diferencia > 1000) {
                    $scope.MensajesError.push('El rango de los preimpresos que se van a generar no puede ser superior a 1000');
                    continuar = false;
                }
            }

            for (var i = 0; i < $scope.ListadoRecorridosTotal.length; i++) {
                if ($scope.ListadoRecorridosTotal[i].ENRE_Numero > 0 || $scope.ListadoRecorridosTotal[i].ENPD_Numero > 0) {
                    $scope.MensajesError.push('No se pueden trasladar preimpresos con una remesa asociada, verifique nuevamente la lista seleccionada');
                    continuar = false;
                    break;
                }
            }

            if ($scope.ModeloFecha == undefined || $scope.ModeloFecha == null || $scope.ModeloFecha == '') {
                $scope.MensajesError.push('Debe ingresar una fecha');
                continuar = false;
            }

            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroInicial: MascaraNumero($scope.ModeloNumeroPreimpresoInicial),
                NumeroFinal: MascaraNumero($scope.ModeloNumeroPreimpresoFinal),
                Sync:true
            };
            if ($scope.Numero == 0) {
                var ResponseGuiasPreimpresasCreadas = GuiasPreimpresasFactory.ConsultarPrecinto(filtros)
                if (ResponseGuiasPreimpresasCreadas != undefined) {
                    if (ResponseGuiasPreimpresasCreadas.ProcesoExitoso === true) {                        
                        if (ResponseGuiasPreimpresasCreadas.Datos.length > 0) {
                            $scope.MensajesError.push('Algunos números preimpresos dentro del rango actual ya están asignados');
                            continuar = false;

                        }
                    }
                }
            }

                

            return continuar;
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);

        };

        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope)
        }

        $scope.MaskValores = function () {
            return MascaraValoresGeneral($scope)
        }
        $scope.MarcarRecorridos = function (RecorridosSeleccionados) {

            $scope.RecorridosSeleccionados = RecorridosSeleccionados;
            if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                if ($scope.RecorridosSeleccionados == true) {
                    $scope.ListaResultadoFiltroConsulta.forEach(function (itemTotal) {
                        if ((itemTotal.Estado == 1 && itemTotal.Anulado == 0 && itemTotal.ENPD_Numero > 0)) {
                            itemTotal.Seleccionado = true;
                        }
                    });
                    $scope.ListadoPrecintos.forEach(function (item) {
                        if ((item.Estado == 1 && item.Anulado == 0 && item.ENPD_Numero > 0) && item.Estado != 2) {
                            item.Seleccionado = true;
                        }

                    });
                } else {
                    $scope.ListaResultadoFiltroConsulta.forEach(function (itemTotal) {
                        if ((itemTotal.Estado == 1 && itemTotal.Anulado == 0 && itemTotal.ENPD_Numero > 0)) {
                            itemTotal.Seleccionado = false;
                        }
                    });
                    $scope.ListadoPrecintos.forEach(function (item) {
                        if ((item.Estado == 1 && item.Anulado == 0 && item.ENPD_Numero > 0 && item.Estado != 2)) {
                            item.Seleccionado = false;
                        }
                    });
                }

            }
            else {
                if ($scope.RecorridosSeleccionados == true) {
                    $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                        if ((itemTotal.Estado == 1 && itemTotal.Anulado == 0 && itemTotal.Numero_Precinto > 0)) {
                            itemTotal.Seleccionado = true;
                        }
                    });
                    $scope.ListadoPrecintos.forEach(function (item) {
                        if ((item.Estado == 1 && item.Anulado == 0 && item.Numero_Precinto > 0 && item.Estado != 2)) {
                            item.Seleccionado = true;
                        }
                    });
                } else {
                    $scope.ListadoRecorridosTotal.forEach(function (itemTotal) {
                        if ((itemTotal.Estado == 1 && itemTotal.Anulado == 0 && itemTotal.Numero_Precinto > 0)) {
                            itemTotal.Seleccionado = false;
                        }
                    });
                    $scope.ListadoPrecintos.forEach(function (item) {
                        if ((item.Estado == 1 && item.Anulado == 0 && item.ENPD_Numero > 0 && item.Estado != 2)) {
                            item.Seleccionado = false;
                        }
                    });
                }
            }
        }


        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($scope.Numero > 0) {
                document.location.href = '#!ConsultarAsignacionOficinasGuiasPreimpresas/' + $scope.Numero;
            } else {
                document.location.href = '#!ConsultarAsignacionOficinasGuiasPreimpresas';
            }
        };

        CargarAutocompletes() 
    }]);