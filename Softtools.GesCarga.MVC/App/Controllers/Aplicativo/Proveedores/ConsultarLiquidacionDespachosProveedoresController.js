﻿SofttoolsApp.controller("ConsultarLiquidacionDespachosProveedoresCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'LiquidacionDespachosProveedoresFactory', 'blockUI',
    function ($scope,$routeParams,$timeout,$linq,LiquidacionDespachosProveedoresFactory, blockUI) {
        $scope.MapaSitio = [{ Nombre: 'Proveedores' }, { Nombre: 'Documentos' }, { Nombre: 'Liquidación Despachos Proveedores' }];

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_LIQUIDACION_DESPACHOS_PROVEEDORES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;

        $scope.FechaInicio = new Date();
        $scope.FechaFin = new Date();
       
        // Botón Nuevo:
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarLiquidacionPlanillasProveedores';
            }
        };

        $scope.ListaEstados = [
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            {Nombre: 'ANULADO',Codigo: 2},
            {Nombre: '(NO APLICA)', Codigo: -1}
        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo==-1')

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                $scope.Numero = 0;
                Find();
            }
        }

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                //NumeroDocumento
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            $scope.ListadoLiquidacionesProveedores = [];

            filtros = {
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero : $scope.Numero,
                NumeroDocumento: parseInt($scope.NumeroDocumento),               
                FechaInicial: $scope.NumeroDocumento > 0 ? undefined : $scope.FechaInicio,
                FechaFinal: $scope.NumeroDocumento > 0 ? undefined : $scope.FechaFin,               
                Estado: $scope.Estado.Codigo,
                Transportador: { Nombre: $scope.NombreTransportador }

            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                LiquidacionDespachosProveedoresFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoLiquidacionesProveedores = response.data.Datos;
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                                console.log($scope.totalRegistros, $scope.cantidadRegistrosPorPaginas);
                            }
                            else {
                                $scope.ListadoDespachosOtrasEmpresas = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }


                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.NumeroDocumento === null || $scope.NumeroDocumento === undefined || $scope.NumeroDocumento === '' || $scope.NumeroDocumento === 0 || isNaN($scope.NumeroDocumento) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.NumeroDocumento !== null && $scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '' && $scope.NumeroDocumento !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            return continuar
        }

        $scope.AnularDocumento = function (liquidacion) {

            LiquidacionDespachosProveedoresFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: liquidacion.Numero }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                       
                        if (response.data.Datos.Comprobante > 0) {
                            ShowError('Esta liquidación se encuentra asociada a un comprobante Egreso');
                        } else {
                            $scope.NumeroAnula = liquidacion.Numero;
                            $scope.NumeroDocumentoAnula = liquidacion.NumeroDocumento;
                            showModal('modalAnularLiquidacion');
                        }

                    } else {
                        console.log("error en la consulta");
                    }

                });


        };


        $scope.Anular = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.ModeloCausaAnula,
                Numero: $scope.NumeroAnula


            };
            if (DatosRequeridosAnular()) {

                LiquidacionDespachosProveedoresFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anuló la liquidación ' + $scope.NumeroDocumentoAnula);
                            closeModal('modalAnularLiquidacion');
                            Find();

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.data.MensajeOperacion);
                    });
            }


        };

        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }

        if ($routeParams.Numero > CERO) {
            $scope.NumeroDocumento = $routeParams.Numero;
            $scope.FechaInicio = '';
            $scope.FechaFin = '';
            Find();
            $scope.NumeroDocumento = '';
        }
        
    }

]);