﻿SofttoolsApp.controller("GestionarUsuariosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'UsuariosFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'blockUIConfig',
    function ($scope, $routeParams, $timeout, $linq, blockUI, UsuariosFactory, ValorCatalogosFactory, TercerosFactory, blockUIConfig) {
        //--------------------------------------------------------------------------------------------------------------------------|
        //------------------------------------------DECLARACIÓN DE VARIABLES--------------------------------------------------------|
        //--------------------------------------------------------------------------------------------------------------------------|

        $scope.Titulo = 'GESTIONAR USUARIOS';
        $scope.MapaSitio = [{ Nombre: 'Seguridad' }, { Nombre: 'Usuarios' }, { Nombre: 'Gestionar' }];
        $scope.Modelo = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, };
        $scope.Usuario = {};
        $scope.Seleccionado = true;
        $scope.MensajesError = [];
        $scope.NombreHabilitado = true;
        $scope.ListadoClientes = [];
        $scope.ListadoProveedor = [];
        $scope.ListadoEmpleados = [];
        $scope.ListadoConductores = [];
        AutoCompleteEmpleadoValido = true;
        AutoCompleteTerceroValido = true;
        $scope.ClienteHabilitado = true;
        $scope.PermisoModificarClaveUsuario = {};
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_USUARIOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        try { $scope.PermisoModificarClaveUsuario = $linq.Enumerable().From($scope.ListadoPermisos).First('$.Codigo == ' + OPCION_PERMISO_USUARIOS); } catch (e) {
            $scope.PermisoModificarClaveUsuario.AplicaActualizar = 0
        }

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.Cliente = { NombreCompleto: '', Codigo: 0 };
        $scope.Empleado = { NombreCompleto: '', Codigo: 0 };
        $scope.Conductor = { NombreCompleto: '', Codigo: 0 };
        $scope.Proveedor = { NombreCompleto: '', Codigo: 0 };
        $scope.ListadoPermisosEspecificos = []
        $scope.CambioContrasenia = false
        $scope.ListadoPermisos.forEach(function (item) {
            if (item.Padre == OPCION_MENU_USUARIOS) {
                $scope.ListadoPermisosEspecificos.push(item)
            }
        });
        $scope.ListadoPermisosEspecificos.forEach(function (item) {
            if (item.Codigo == PERMISO_CAMBIO_CONTRASENIA) {
                $scope.CambioContrasenia = true
            }

        })

        //--------------------------------------------------------------------------------------------------------------------------|
        //-----------------------------------------------AUTOCOMPLETES--------------------------------------------------------------|
        //--------------------------------------------------------------------------------------------------------------------------|
        /*Cargar el combo de Aplicacion*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_APLICACIONES_DE_GESTRANS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoAplicacion = []
                    $scope.ListadoTipoAplicacion = response.data.Datos

                    for (var i = 0; i < $scope.ListadoTipoAplicacion.length; i++) {
                        if ($scope.ListadoTipoAplicacion[i].Codigo == 204) {
                            $scope.ListadoTipoAplicacion.splice(i, 2);
                        }
                    }

                    if ($scope.AplicacionUsuario > 0) {
                        $scope.TipoAplicacion = $linq.Enumerable().From($scope.ListadoTipoAplicacion).First('$.Codigo ==' + $scope.AplicacionUsuario);
                    } else {
                        $scope.TipoAplicacion = $linq.Enumerable().From($scope.ListadoTipoAplicacion).First('$.Codigo ==' + CODIGO_APLICACION_TODAS);
                    }

                }
                else {
                    $scope.ListadoTipoAplicacion = []
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        //Cliente
        $scope.ListadoClientes = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoClientes = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoClientes)
                }
            }
            return $scope.ListadoClientes
        }
        //Proveedor
        $scope.ListadoProveedor = [];
        $scope.AutocompleteProveedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_PROVEEDOR,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoProveedor = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProveedor)
                }
            }
            return $scope.ListadoProveedor
        }
        //Empleado
        $scope.ListadoEmpleados = [];
        $scope.AutocompletePEmpleado = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_EMPLEADO,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoEmpleados = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoEmpleados)
                }
            }
            return $scope.ListadoEmpleados
        }
        //Conductor 
        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CONDUCTOR,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                }
            }
            return $scope.ListadoConductores
        }

        if ($routeParams.Codigo > 0) {
            $scope.Codigo = $routeParams.Codigo;
        }
        else {
            $scope.Codigo = 0;
        }

        //--------------------------------------------------------------------------------------------------------------------------|
        //---------------------------------------------CARGAR COMBOS---------------------------------------------------------------|
        //--------------------------------------------------------------------------------------------------------------------------|




        //--------------------------------------------------------------------------------------------------------------------------|
        //--------------------------------------------VALIDAR AUTOCOMPLETES---------------------------------------------------------|
        //--------------------------------------------------------------------------------------------------------------------------|

        //Cliente


        //Empleado
        $scope.AsignarEmpleado = function () {
            TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Empleado.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Empleado = response.data.Datos
                        try { $scope.EmailFuncionario = $scope.Empleado.Correo } catch (e) { }
                        try { $scope.TelefonoFuncionario = $scope.Empleado.Telefono } catch (e) { }
                        try { $scope.CelularFuncionario = $scope.Empleado.Celular } catch (e) { }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            $scope.AsignarNombre($scope.Empleado)
        };
        $scope.AsignarCliente = function () {
            TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Cliente.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Cliente = response.data.Datos
                        try { $scope.EmailCliente = $scope.Cliente.Correo } catch (e) { }
                        try { $scope.TelefonoCliente = $scope.Cliente.Telefono } catch (e) { }
                        try { $scope.CelularCliente = $scope.Cliente.Celular } catch (e) { }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            $scope.AsignarNombre($scope.Cliente)
        };
        $scope.AsignarProveedor = function () {
            TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Proveedor.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Proveedor = response.data.Datos
                        try { $scope.EmailProveedor = $scope.Proveedor.Correo } catch (e) { }
                        try { $scope.TelefonoProveedor = $scope.Proveedor.Telefono } catch (e) { }
                        try { $scope.CelularProveedor = $scope.Proveedor.Celular } catch (e) { }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            $scope.AsignarNombre($scope.Proveedor)
        };

        $scope.AsignarConductor = function () {
            TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Conductor.Codigo }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Conductor = response.data.Datos
                        try { $scope.EmailConductor = $scope.Conductor.Correo } catch (e) { }
                        try { $scope.TelefonoConductor = $scope.Conductor.Telefono } catch (e) { }
                        try { $scope.CelularConductor = $scope.Conductor.Celular } catch (e) { }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            $scope.AsignarNombre($scope.Conductor)
        };
        $scope.AsignarNombre = function (Empleado) {
            if (Empleado.NombreCompleto != '' && Empleado.NombreCompleto != undefined) {
                $scope.Nombre = Empleado.NombreCompleto;
            } else {
                $scope.Nombre = ''
            }
        }

        //--------------------------------------------------------------------------------------------------------------------------|
        //---------------------------------------------FUNCIONES Y ACCIONES---------------------------------------------------------|
        //--------------------------------------------------------------------------------------------------------------------------|

        if ($scope.Codigo > 0) {
            $scope.Titulo = 'CONSULTAR USUARIO';
            $scope.ClaveHabilitada = true;
            $scope.IdentificadorHabilitado = true;

            ObtenerUsuario();
            //if ($scope.Sesion.UsuarioAutenticado.Codigo == CODIGO_UNO) {
            //    $scope.ClaveHabilitada = false;
            //}
            //if ($scope.ListadoClientes == undefined) {
            //    CargarAutocompleteTercero();
            //}
            //if ($scope.ListadoEmpleados == undefined) {
            //    CargarAutocompleteEmpleado();
            //}
        }


        function ObtenerUsuario() {
            blockUI.start('Cargando Usuario');
            $timeout(function () {
                blockUI.message('Cargando Usuario');
            }, 200);

            filtros =
                {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Codigo,
                };
            blockUI.delay = 1000;
            UsuariosFactory.Obtener(filtros).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.Codigo = response.data.Datos.Codigo;
                    $scope.Identificador = response.data.Datos.CodigoUsuario;
                    $scope.Clave = response.data.Datos.Clave;
                    $scope.ConfirClave = response.data.Datos.Clave;
                    $scope.Nombre = response.data.Datos.Nombre;
                    $scope.Descripcion = response.data.Datos.Descripcion;

                    //Estado cuenta
                    if (response.data.Datos.Habilitado == 1) {
                        $scope.CuentaActiva = true;
                    }
                    if (response.data.Datos.ConsultaOtrasOficinas) {
                        $scope.OtrasOficinas = true
                    }
                    $scope.AplicacionUsuario = response.data.Datos.AplicacionUsuario
                    if ($scope.ListadoTipoAplicacion.length > 0) {
                        $scope.TipoAplicacion = $linq.Enumerable().From($scope.ListadoTipoAplicacion).First('$.Codigo == ' + response.data.Datos.AplicacionUsuario);
                    }
                    $scope.Asignarseccion();
                    //Logueado
                    if (response.data.Datos.Login == 1) {
                        $scope.Logueado = true;
                    }
                    if (response.data.Datos.Manager == 1) {
                        $scope.Manager = true;
                    }
                    
                    if ($scope.PermisoModificarClaveUsuario.AplicaActualizar == PERMISO_ACTIVO) {
                        $scope.ClaveHabilitada = false;
                    }
                    $scope.DiasCambioClave = response.data.Datos.DiasCambioClave;
                    if (response.data.Datos.Cliente.Codigo > 0) {
                        $scope.Cliente = $scope.CargarTercero(response.data.Datos.Cliente.Codigo)
                        $scope.CodigoCliente = response.data.Datos.Cliente.Codigo
                        $scope.AsignarCliente();
                    }
                    if (response.data.Datos.Proveedor.Codigo > 0) {
                        $scope.Proveedor = $scope.CargarTercero(response.data.Datos.Proveedor.Codigo)
                        $scope.CodigoProveedor = response.data.Datos.Proveedor.Codigo
                        $scope.AsignarProveedor();
                    }
                    if (response.data.Datos.Empleado.Codigo > 0) {
                        $scope.CodigoEmpleado = response.data.Datos.Empleado.Codigo;
                        $scope.Empleado = $scope.CargarTercero(response.data.Datos.Empleado.Codigo)
                        $scope.AsignarEmpleado();
                    }
                    if (response.data.Datos.Conductor.Codigo > 0) {
                        $scope.CodigoConductor = response.data.Datos.Conductor.Codigo
                        $scope.Conductor = $scope.CargarTercero(response.data.Datos.Conductor.Codigo)
                        $scope.AsignarConductor();
                    }
                    $scope.Mensaje = response.data.Datos.Mensaje;

                }
                else {
                    ShowError('No se logro consultar el usuario con código ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#ConsultarUsuario';
                }
            },
                function (response) {
                    ShowError('No se logro consultar el usuario con código ' + $scope.Codigo + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#ConsultarUsuario';
                })

            blockUI.stop();
        };

        function DatosRequeridos() {

            if ($scope.Identificador == undefined || $scope.Identificador == '') {
                $scope.MensajesError.push('Debe ingresar un identificador');
                PuedeGuardar = false;
            } else {
                var identificador = $scope.Identificador
                var cont = 0
                for (var i = 0; i < identificador.length; i++) {
                    if (identificador[i] == ' ') {
                        cont++
                    }
                }
                if (cont > 0) {
                    $scope.MensajesError.push('El Código no debe tener espacios');
                    PuedeGuardar = false;
                }
            }



            if ($scope.Clave == undefined || $scope.Clave == '') {
                $scope.MensajesError.push('Debe ingresar la clave');
                PuedeGuardar = false;
            }
            else {
                if ($scope.Codigo != CERO) {
                    if (validarContrasena($scope.Clave) == false) {
                        $scope.MensajesError.push('La contraseña debe ser mínimo de 6 carácteres, debe contener al menos un carácter especial, un número y no debe tener espacios en blanco, recuerde que los los caracteres & y + no estan permitidos');
                        PuedeGuardar = false;
                    }
                    else {
                        if ($scope.ConfirClave != $scope.Clave) {
                            $scope.MensajesError.push('La clave y la confirmación deben coincidir');
                            PuedeGuardar = false;
                        }
                    }
                }
            }
            if ($scope.DiasCambioClave == '' || $scope.DiasCambioClave == undefined || $scope.DiasCambioClave == 0) {
                $scope.MensajesError.push('Debe ingresar los días de cambio de clave');
                PuedeGuardar = false;
            } else {
                if ($scope.DiasCambioClave > 90) {
                    $scope.MensajesErrorCambioClave.push('Los días de cambio de clave no pueden ser mayor a 90 días');
                    PuedeGuardar = false;
                }
                if ($scope.DiasCambioClave == 0) {
                    $scope.MensajesErrorCambioClave.push('Los días de cambio de clave no pueden ser igual a 0');
                    PuedeGuardar = false;
                }
            }
            if ($scope.Descripcion == '' || $scope.Descripcion == undefined) {
                $scope.Descripcion = '';
            }


            if ($scope.Mensaje == '' || $scope.Mensaje == undefined) {
                $scope.Mensaje = '';
            }

            if ($scope.UsuarioGesSoporte == '' || $scope.UsuarioGesSoporte == undefined) {
                $scope.UsuarioGesSoporte = '';
            }

            if ($scope.ClaveGesSoporte == '' || $scope.ClaveGesSoporte == undefined) {
                $scope.ClaveGesSoporte = '';
            }

            if ($scope.Telefono == undefined || $scope.Telefono == '') {
                $scope.Telefono = '';
            }

            if ($scope.Email == undefined || $scope.Email == '') {
                $scope.Email = '';
            }
            else {
                if (validarEmail($scope.Email) == false) {
                    $scope.MensajesError.push('El email debe tener un formato válido');
                    PuedeGuardar = false;
                }
            }

            if ($scope.Celular == undefined || $scope.Celular == '') {
                $scope.Celular = '';
            }
            if ($scope.TipoAplicacion.Codigo == CODIGO_APLICACION_TODAS) {
                if (($scope.Empleado == '' && $scope.Cliente == '' && $scope.Conductor == '' && $scope.Proveedor == '') ||
                    ($scope.Empleado == undefined && $scope.Cliente == undefined && $scope.Conductor == undefined && $scope.Proveedor == undefined)) {
                    $scope.MensajesError.push('Por favor seleccione un funcionario, cliente, proveedor o conductor ');
                    PuedeGuardar = false;
                }
                else if ($scope.Empleado.Codigo == undefined && $scope.Cliente.Codigo == undefined && $scope.Conductor.Codigo == undefined && $scope.Proveedor.Codigo == undefined) {
                    $scope.MensajesError.push('Por favor seleccione un funcionario, cliente, proveedor o conductor valido');
                    PuedeGuardar = false;
                }
                else {
                    if ($scope.Empleado.Codigo == undefined) {
                        $scope.Empleado = { NombreCompleto: '', Codigo: 0 };
                    }
                    if ($scope.Cliente.Codigo == undefined) {
                        $scope.Cliente = { NombreCompleto: '', Codigo: 0 };
                    }
                    if ($scope.Conductor.Codigo == undefined) {
                        $scope.Conductor = { NombreCompleto: '', Codigo: 0 };
                    }
                    if ($scope.Proveedor.Codigo == undefined) {
                        $scope.Proveedor = { NombreCompleto: '', Codigo: 0 };
                    }
                }
            }
            if ($scope.TipoAplicacion.Codigo == CODIGO_APLICACION_TODAS) {
                if ($scope.Empleado == '' || $scope.Empleado == undefined) {
                    $scope.MensajesError.push('Por favor seleccione un empleado');
                    PuedeGuardar = false;
                } else if ($scope.Empleado.Codigo == undefined || $scope.Empleado.Codigo == 0) {
                    $scope.MensajesError.push('Por favor seleccione un empleado valido');
                    PuedeGuardar = false;
                }
                if ($scope.Cliente == '' || $scope.Cliente == undefined) {
                    $scope.MensajesError.push('Por favor seleccione un cliente');
                    PuedeGuardar = false;
                } else if ($scope.Cliente.Codigo == undefined || $scope.Cliente.Codigo == 0) {
                    $scope.MensajesError.push('Por favor seleccione un cliente valido');
                    PuedeGuardar = false;
                }
                if ($scope.Conductor == '' || $scope.Conductor == undefined) {
                    $scope.MensajesError.push('Por favor seleccione un conductor');
                    PuedeGuardar = false;
                } else if ($scope.Conductor.Codigo == undefined || $scope.Conductor.Codigo == 0) {
                    $scope.MensajesError.push('Por favor seleccione un conductor valido');
                    PuedeGuardar = false;
                }
            }

            if ($scope.TipoAplicacion.Codigo == CODIGO_APLICACION_APLICATIVO) {
                if ($scope.Empleado == '' || $scope.Empleado == undefined) {
                    $scope.MensajesError.push('Por favor seleccione un empleado');
                    PuedeGuardar = false;
                } else if ($scope.Empleado.Codigo == undefined || $scope.Empleado.Codigo == 0) {
                    $scope.MensajesError.push('Por favor seleccione un empleado valido');
                    PuedeGuardar = false;
                } else {
                    $scope.Cliente.Codigo = 0
                    $scope.Conductor.Codigo = 0
                    $scope.Proveedor.Codigo = 0
                }
            }

            if ($scope.TipoAplicacion.Codigo == CODIGO_APLICACION_PORTAL) {
                if (($scope.Cliente == '' || $scope.Cliente == undefined || $scope.Cliente.Codigo == undefined || $scope.Cliente.Codigo == 0)
                    && ($scope.Proveedor == '' || $scope.Proveedor == undefined || $scope.Proveedor.Codigo == undefined || $scope.Proveedor.Codigo == 0)) {
                    $scope.MensajesError.push('Por favor seleccione un cliente o un proveedor');
                    PuedeGuardar = false;
                } else {
                    if ($scope.Cliente == '' || $scope.Cliente == undefined || $scope.Cliente.Codigo == undefined || $scope.Cliente.Codigo == 0) {
                        $scope.Cliente.Codigo = 0
                    }
                    else if ($scope.Proveedor == '' || $scope.Proveedor == undefined || $scope.Proveedor.Codigo == undefined || $scope.Proveedor.Codigo == 0) {
                        $scope.Proveedor.Codigo = 0
                    }
                    $scope.Empleado.Codigo = 0
                    $scope.Conductor.Codigo = 0
                }
            }
            if ($scope.TipoAplicacion.Codigo == CODIGO_APLICACION_GESPHONE) {
                if ($scope.Conductor == '' || $scope.Conductor == undefined) {
                    $scope.MensajesError.push('Por favor seleccione un conductor');
                    PuedeGuardar = false;
                } else if ($scope.Conductor.Codigo == undefined || $scope.Conductor.Codigo == 0) {
                    $scope.MensajesError.push('Por favor seleccione un conductor valido');
                    PuedeGuardar = false;
                } else {
                    $scope.Empleado.Codigo = 0
                    $scope.Cliente.Codigo = 0
                    $scope.Proveedor.Codigo = 0
                }
            }

        };

        $scope.ConfirmacionGuardarUsuario = function () {
            showModal('modalConfirmacionGuardarUsuario');
        };

        $scope.GuardarUsuario = function () {
            closeModal('modalConfirmacionGuardarUsuario');

            $scope.MensajesError = [];
            DatosRequeridos();
            if ($scope.MensajesError.length == 0) {

                if ($scope.CuentaActiva == true) {
                    Habilitado = 1;
                }
                else {
                    Habilitado = 0;
                }

                if ($scope.Logueado == true) {
                    Login = 1;
                }
                else {
                    Login = 0;
                }
                if ($scope.Manager == true) {
                    Manager = 1;
                }
                else {
                    Manager = 0;
                }
                if ($scope.OtrasOficinas == true) {
                    ConsultaOtrasOficinas = 1;
                }
                else {
                    ConsultaOtrasOficinas = 0;
                }
                Externo = 0;

                $scope.Correo = ''
                $scope.Telefono = ''
                $scope.Celular = ''

                if ($scope.TipoAplicacion.Codigo == CODIGO_APLICACION_APLICATIVO || $scope.TipoAplicacion.Codigo == CODIGO_APLICACION_TODAS) {
                    $scope.Correo = $scope.EmailFuncionario
                    $scope.Telefono = $scope.TelefonoFuncionario
                    $scope.Celular = $scope.CelularFuncionario
                }
                else if ($scope.TipoAplicacion.Codigo == CODIGO_APLICACION_PORTAL) {
                    if ($scope.Cliente.Codigo > 0) {
                        $scope.Correo = $scope.EmailCliente
                        $scope.Telefono = $scope.TelefonoCliente
                        $scope.Celular = $scope.CelularCliente
                    } else {
                        $scope.Correo = $scope.EmailProveedor
                        $scope.Telefono = $scope.TelefonoProveedor
                        $scope.Celular = $scope.CelularProveedor
                    }

                }
                else if ($scope.TipoAplicacion.Codigo == CODIGO_APLICACION_GESPHONE) {
                    $scope.Correo = $scope.EmailConductor
                    $scope.Telefono = $scope.TelefonoConductor
                    $scope.Celular = $scope.CelularConductor
                }
                parametros =
                    {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.Codigo,
                        CodigoUsuario: $scope.Identificador,
                        Nombre: $scope.Nombre,
                        Descripcion: $scope.Descripcion,
                        Clave: $scope.Clave,
                        Cliente: $scope.Cliente,
                        DiasCambioClave: $scope.DiasCambioClave,
                        Mensaje: $scope.Mensaje,
                        Manager,
                        Empleado: $scope.Empleado,
                        Conductor: $scope.Conductor,
                        Proveedor: $scope.Proveedor,
                        Correo: $scope.Correo,
                        Telefono: $scope.Telefono,
                        Celular: $scope.Celular,
                        AplicacionUsuario: $scope.TipoAplicacion.Codigo,
                        Habilitado,
                        Login,
                        ConsultaOtrasOficinas: ConsultaOtrasOficinas
                        //Externo,
                    };

                UsuariosFactory.Guardar(parametros).then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if ($scope.Codigo == 0) {
                            ShowSuccess('Se guardó el Usuario ' + $scope.Nombre);
                            document.location.href = '#!ConsultarUsuarios/' + response.data.Datos;
                        }
                        else if (response.data.Datos == -1) {
                            ShowError('El identificador ingresado ya se encuentra registrado');
                        }
                        else if (response.data.Datos > 0) {
                            ShowSuccess('Se modificó el Usuario ' + $scope.Nombre);
                            document.location.href = '#!ConsultarUsuarios/' + response.data.Datos;
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            }
        };

        $scope.Asignarseccion = function () {
            try {
                if ($scope.TipoAplicacion.Codigo == CODIGO_APLICACION_TODAS) {
                    $('#Aplicativo').show();
                    $('#Portal').show();
                    $('#Gesphone').show();
                    $scope.NombreHabilitado = false

                }
                if ($scope.TipoAplicacion.Codigo == CODIGO_APLICACION_APLICATIVO) {
                    $('#Aplicativo').show();
                    $('#Portal').hide();
                    $('#Gesphone').hide();
                    $scope.NombreHabilitado = true
                }
                if ($scope.TipoAplicacion.Codigo == CODIGO_APLICACION_PORTAL) {
                    $('#Aplicativo').hide();
                    $('#Portal').show();
                    $('#Gesphone').hide();
                    $scope.NombreHabilitado = true
                }
                if ($scope.TipoAplicacion.Codigo == CODIGO_APLICACION_GESPHONE) {
                    $('#Aplicativo').hide();
                    $('#Portal').hide();
                    $('#Gesphone').show();
                    $scope.NombreHabilitado = true
                }
            } catch (e) {

            }
        }
        $scope.Asignarseccion()

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarUsuarios';
        };

        $scope.MaskNumero = function () {
            try { $scope.DiasCambioClave = MascaraNumero($scope.DiasCambioClave) } catch (e) { }
        };
        $scope.MaskCelular = function (option) {
            try { $scope.Telefono = MascaraCelular($scope.Telefono) } catch (e) { }
            try { $scope.Celular = MascaraCelular($scope.Celular) } catch (e) { }
        };
    }]);