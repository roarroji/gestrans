﻿SofttoolsApp.controller("ConsultarProgramacionOrdenServicioCtrl", ['$scope', '$timeout', 'EncabezadoSolicitudOrdenServiciosFactory', '$linq', 'blockUI', '$routeParams', 'TercerosFactory', 'ValorCatalogosFactory', 'EncabezadoProgramacionOrdenServiciosFactory',
    function ($scope, $timeout, EncabezadoSolicitudOrdenServiciosFactory, $linq, blockUI, $routeParams, TercerosFactory, ValorCatalogosFactory, EncabezadoProgramacionOrdenServiciosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Servicio al Cliente' }, { Nombre: 'Procesos' }, { Nombre: 'Programar Orden Servicio' }];
        $scope.Titulo = "PROGRAMAR ORDEN SERVICIO";
        // ----------------------------- Decalaracion Variables -----------------------------//
        var filtros = {};
        var GestionarSolicitudServicio = 1;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.ListadoOrdenServicios = [];
        $scope.MostrarMensajeError = false
        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];

        $scope.ListadoEstadoSolicitud = []
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.ModalEstadoSolicitus = { Codigo: 0 };
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PROGRAMAR_ORDEN_SERVICIO); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        // ----------------------------- Decalaracion Variables -----------------------------//
        //------Funcion Paginacion E Imprimir
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        //------Funcion Paginacion E Imprimir
        //-- Consulta Clientes

        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaInicial = new Date();
            $scope.ModeloFechaFinal = new Date();
        }

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 186 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEstadoSolicitud = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoEstadoSolicitud.push({ Codigo: 0, Nombre: '(TODAS)' });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoEstadoSolicitud.push(response.data.Datos[i]);
                        }
                        $scope.ModalEstadoSolicitus = $linq.Enumerable().From($scope.ListadoEstadoSolicitud).First('$.Codigo == ' + CERO);
                    }
                    else {
                        $scope.ListadoEstadoSolicitud = []
                    }
                }
            }, function (response) {
            });

        //------Funciones
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    $scope.Codigo = 0
                    Find();
                }
            }
        };
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarPorgrmacionOrdenServicio';
            }
        };
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoOrdenServicios = [];
            if ($scope.Buscando) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.ModeloNumero,
                    NumeroOrden: $scope.ModeloNumeroOrdenServicio,
                    FechaInicial: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaInicial,
                    FechaFinal: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaFinal,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_SOLICITUD_ORDEN_SERVICIO,
                    Estado: $scope.Estado.Codigo,
                    Cliente: $scope.Cliente,
                    Pagina: $scope.paginaActual,
                    EstadoProgramacion: { Codigo: $scope.ModalEstadoSolicitus.Codigo },
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    PendienteGenerar: 1
                }
                blockUI.delay = 1000;
                EncabezadoProgramacionOrdenServiciosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoOrdenServicios = response.data.Datos
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.stuatusText);
                    });
            }
            blockUI.stop();
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.ModeloFechaInicial === null || $scope.ModeloFechaInicial === undefined || $scope.ModeloFechaInicial === '') &&
                ($scope.ModeloFechaFinal === null || $scope.ModeloFechaFinal === undefined || $scope.ModeloFechaFinal === '') &&
                ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true) &&
                ($scope.ModeloNumeroOrdenServicio === null || $scope.ModeloNumeroOrdenServicio === undefined || $scope.ModeloNumeroOrdenServicio === '' || $scope.ModeloNumeroOrdenServicio === 0 || isNaN($scope.ModeloNumeroOrdenServicio) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;
            } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0) ||
                ($scope.ModeloNumeroOrdenServicio !== null && $scope.ModeloNumeroOrdenServicio !== undefined && $scope.ModeloNumeroOrdenServicio !== '' && $scope.ModeloNumeroOrdenServicio !== 0) ||
                ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '') ||
                ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '') &&
                    ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial;
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal;
                    }
                }
            }
            return continuar;
        }

        $scope.MaskMayus = function () {
            try { $scope.Cliente = $scope.Cliente.toUpperCase() } catch (e) { }
        };

        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            $scope.ModeloNumero = $routeParams.Numero;
            $scope.Cliente = { NombreCompleto: '', Codigo: 0 }
            Find();
        }
        //------Funciones
    }]);