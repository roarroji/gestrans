﻿SofttoolsApp.controller("ConsultarListadoServicioClienteCtrl", ['$scope', '$timeout', 'TercerosFactory', 'RutasFactory', 'OficinasFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'EmpresasFactory', 'VehiculosFactory', 'SemirremolquesFactory', 'CiudadesFactory', 'PaisesFactory', 'TipoLineaNegocioTransportesFactory', 'LineaNegocioTransportesFactory',
    function ($scope, $timeout, TercerosFactory, RutasFactory, OficinasFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, EmpresasFactory, VehiculosFactory, SemirremolquesFactory, CiudadesFactory, PaisesFactory, TipoLineaNegocioTransportesFactory, LineaNegocioTransportesFactory) {

        $scope.MapaSitio = [{ Nombre: 'Servicio al Cliente' }, { Nombre: 'Listados' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.NombreReporte = '';
        $scope.FiltroArmado = '';
        $scope.ListadoCliente = [];
        $scope.FiltroTercero = true;
        $scope.FiltroConductor = false;
        $scope.FiltroPlaca = false;
        $scope.FiltroSemirremolque = false;
        $scope.FiltroRuta = false;
        $scope.FiltroSitios = false;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ModeloTercero = [];
        $scope.ModeloConductor = [];
        $scope.ModeloSemirremolque = [];
        $scope.ModeloPlaca = [];
        $scope.ModeloRuta = [];
        $scope.ModeloPais = [];
        $scope.ModeloCiudad = [];
        $scope.ModeloTipoSitio = [];
        $scope.ModalLineaNegocioTransportes = [];
        $scope.ListadoConductor = [];
        $scope.ModeloFechaInicial = new Date();
        $scope.ModeloFechaFinal = new Date();
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {} }
        }

        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OCION_MENU_LISTADOS_SERVICIO_CLIETE);

        // Cargar combos de módulo y listados 
        $scope.ListaServicio = [];
        for (var i = 0; i < $scope.ListadoMenuListados.length; i++) {
            if ($scope.ListadoMenuListados[i].Padre == OCION_MENU_LISTADOS_SERVICIO_CLIETE) {
                $scope.ListaServicio.push({ NombreMenu: $scope.ListadoMenuListados[i].Nombre, Codigo: $scope.ListadoMenuListados[i].Codigo })
            }
        }
        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];

        $scope.ListadoEstadoSolicitud = []
        $scope.ListadoEstadoProgramacion = [];
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        $scope.ModeloServicioCliente = $scope.ListaServicio[0]

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;


        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        LineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoLineaNegocioTransportes = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoLineaNegocioTransportes.push({ Nombre: '(NO APLICA)', Codigo: 0 });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo !== CODIGO_LINEA_NEGOCIO_TRANSPORTE_PAQUETERIA) {
                                $scope.ListadoLineaNegocioTransportes.push(response.data.Datos[i]);
                            }
                        }
                        $scope.ModalLineaNegocioTransportes = $linq.Enumerable().From($scope.ListadoLineaNegocioTransportes).First('$.Codigo == 0');

                        try { $scope.ListadoTarifas = AgruparListados($scope.Modelo.Tarifas, 'LineaNegocioTransportes', 'TipoLineaNegocioTransportes'); } catch (e) { }
                    }
                    else {
                        $scope.ListadoLineaNegocioTransportes = []
                    }
                }
            }, function (response) {
            });


        OficinasFactory.Consultar({ CodigoCiudad: $scope.CodigoCiudad, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinas = [];
                    $scope.HabilitarCiudad = false
                    $scope.ListadoOficinas = [
                        { Codigo: 0, Nombre: '(TODOS)' }
                    ];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item)
                    });
                    if ($scope.ListadoOficinas.length > 0) {
                        $scope.ModeloOficinas = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==0');

                    }

                }
            }, function (response) {
                ShowError(response.statusText);
            });

        

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADOS_PROGRAMACION_ORDEN_SERVICIO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEstadoProgramacion = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoEstadoProgramacion.push({ Codigo: 0, Nombre: '(TODAS)' });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoEstadoProgramacion.push(response.data.Datos[i]);

                        }
                        $scope.ModalEstadoProgramacion = $linq.Enumerable().From($scope.ListadoEstadoProgramacion).First('$.Codigo==0');
                    }
                    else {
                        $scope.ListadoEstadoProgramacion = []
                    }
                }
            }, function (response) {
            });

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_TRAMITE_SOLICITUD } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEstadoSolicitud = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoEstadoSolicitud.push({ Codigo: 0, Nombre: '(TODAS)' });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoEstadoSolicitud.push(response.data.Datos[i]);

                        }
                        $scope.ModalEstadoSolicitus = $linq.Enumerable().From($scope.ListadoEstadoSolicitud).First('$.Codigo == ' + CERO);
                    }
                    else {
                        $scope.ListadoEstadoSolicitud = []
                    }
                }
            }, function (response) {
            });

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });


        /*Cargar el combo de ESTADO DE REMESAS PAQUETERIA*/

        $scope.ListadoEstadoRemesaPaqueteria = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 1, Nombre: 'ACTIVO' }, { Codigo: 0, Nombre: 'INACTIVO' }];
        $scope.ModeloEstado = $scope.ListadoEstadoRemesaPaqueteria[0];


        /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)

        };




        /*Cargar el combo de tipo sitios*/
        ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SITIO }
        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: 'Seleccione tipo sitio', Codigo: 0 })
                        $scope.ListadoSitios = response.data.Datos
                        if ($scope.CodigoSitio !== undefined && $scope.CodigoSitio !== '' && $scope.CodigoSitio !== null) {
                            $scope.ModeloTipoSitio = $linq.Enumerable().From($scope.ListadoSitios).First('$.Codigo ==' + $scope.CodigoSitio);
                        } else {
                            $scope.ModeloTipoSitio = $linq.Enumerable().From($scope.ListadoSitios).First('$.Codigo == 0');
                        }
                    }
                    else {
                        $scope.ListadoSitios = []
                    }
                }
            }, function (response) {
            });


        $scope.AsignarFiltro = function (CodigoLista) {

            if (CodigoLista == CODIGO_LITADO_TERCEROS) {
                $scope.FiltroTercero = true;
                $scope.FiltroConductor = false;
                $scope.FiltroPlaca = false;
                $scope.FiltroSemirremolque = false;
                $scope.FiltroRuta = false;
                $scope.FiltroSitios = false;
            }
            if (CodigoLista == CODIGO_LITADO_VEHICULO) {
                $scope.FiltroTercero = false;
                $scope.FiltroConductor = true;
                $scope.FiltroPlaca = true;
                $scope.FiltroSemirremolque = true;
                $scope.FiltroRuta = false;
                $scope.FiltroSitios = false;
            }
            if (CodigoLista == CODIGO_LITADO_SEMIRREMOLQUE) {
                $scope.FiltroTercero = true;
                $scope.FiltroConductor = false;
                $scope.FiltroPlaca = false;
                $scope.FiltroSemirremolque = true;
                $scope.FiltroRuta = false;
                $scope.FiltroSitios = false;
            }
            if (CodigoLista == CODIGO_LITADO_SITIO_CARGUE) {
                $scope.FiltroTercero = false;
                $scope.FiltroConductor = false;
                $scope.FiltroPlaca = false;
                $scope.FiltroSemirremolque = false;
                $scope.FiltroRuta = false;
                $scope.FiltroSitios = true;
            }
            if (CodigoLista == CODIGO_LITADO_RUTA) {
                $scope.FiltroTercero = false;
                $scope.FiltroConductor = false;
                $scope.FiltroPlaca = false;
                $scope.FiltroSemirremolque = false;
                $scope.FiltroRuta = true;
                $scope.FiltroSitios = false;
            }



        }


        $scope.DesplegarInforme = function (OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);




            //Depende del listado seleccionado se enviará el nombre por parametro

            if ($scope.ModeloServicioCliente.Codigo == CODIGO_LISTADO_SOLICITUD_SERVICIOS_CLIENTE) {
                $scope.NombreReporte = NOMBRE_LISTADO_SOLICITUD_SERVICIOS_CLIENTE
                $scope.FiltroArmado += '&Tipo_Documento=' + CODIGO_TIPO_DOCUMENTO_SOLICITUD_ORDEN_SERVICIO
            }

            if ($scope.ModeloServicioCliente.Codigo == CODIGO_LISTADO_SOLICITUD_SERVICIOS_OFICINA) {
                $scope.NombreReporte = NOMBRE_LISTADO_SOLICITUD_SERVICIOS_OFICINA
                $scope.FiltroArmado += '&Tipo_Documento=' + CODIGO_TIPO_DOCUMENTO_SOLICITUD_ORDEN_SERVICIO
            }

            if ($scope.ModeloServicioCliente.Codigo == CODIGO_LISTADO_ORDEN_SERVICIOS_CLIENTE) {
                $scope.NombreReporte = NOMBRE_LISTADO_ORDEN_SERVICIOS_CLIENTE
                $scope.FiltroArmado += '&Tipo_Documento=' + CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO
            }

            if ($scope.ModeloServicioCliente.Codigo == CODIGO_LISTADO_ORDEN_SERVICIOS_OFICINA) {
                $scope.NombreReporte = NOMBRE_LISTADO_ORDEN_SERVICIOS_OFICINA
                $scope.FiltroArmado += '&Tipo_Documento=' + CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO
            }
            if ($scope.ModeloServicioCliente.Codigo == CODIGO_LISTADO_INFORME_PEDIDOS) {
                $scope.NombreReporte = NOMBRE_LISTADO_INFORME_PEDIDOS
                $scope.FiltroArmado += '&Tipo_Documento=' + CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO
            }
            if ($scope.ModeloServicioCliente.Codigo == CODIGO_LISTADO_INFORME_DETALLE_PROGRAMACIONES) {
                $scope.NombreReporte = NOMBRE_LISTADO_INFORME_DETALLE_PROGRAMACIONES
                $scope.FiltroArmado += '&Tipo_Documento=' + CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO
            }

            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';


        };

        $scope.ArmarFiltro = function () {

            // 



          
            if ($scope.ModeloNumeroInicial !== undefined && $scope.ModeloNumeroInicial !== '' && $scope.ModeloNumeroInicial !== null && $scope.ModeloNumeroInicial > 0) {
                $scope.FiltroArmado += '&Numero_Documento_Inicial=' + $scope.ModeloNumeroInicial;
            }
            if ($scope.ModeloNumeroFinal !== undefined && $scope.ModeloNumeroFinal !== '' && $scope.ModeloNumeroFinal !== null && $scope.ModeloNumeroFinal > 0) {
                $scope.FiltroArmado += '&Numero_Documento_Final=' + $scope.ModeloNumeroFinal;
            }
            if ($scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '' && $scope.ModeloFechaInicial !== null) {
                var ModeloFechaInicial = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaInicial);
                $scope.FiltroArmado += '&Fecha_Incial=' + ModeloFechaInicial;
            }
            if ($scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '' && $scope.ModeloFechaFinal !== null) {
                var ModeloFechaFinal = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaFinal);
                $scope.FiltroArmado += '&Fecha_Final=' + ModeloFechaFinal;
            }

            if ($scope.ModalLineaNegocioTransportes.Codigo !== undefined && $scope.ModalLineaNegocioTransportes.Codigo !== '' && $scope.ModalLineaNegocioTransportes.Codigo !== null && $scope.ModalLineaNegocioTransportes.Codigo > 0) {
                $scope.FiltroArmado += '&Linea_Negocio=' + $scope.ModalLineaNegocioTransportes.Codigo;
            }
            if ($scope.Estado.Codigo !== undefined && $scope.Estado.Codigo !== '' && $scope.Estado.Codigo !== -1 && $scope.Estado.Codigo !== null) {
                $scope.FiltroArmado += '&Estado_Documento=' + $scope.Estado.Codigo;
            }
            if ($scope.ModeloServicioCliente.Codigo != CODIGO_LISTADO_INFORME_DETALLE_PROGRAMACIONES) {
                if ($scope.ModalEstadoSolicitus.Codigo !== undefined && $scope.ModalEstadoSolicitus.Codigo !== '' && $scope.ModalEstadoSolicitus.Codigo !== null && $scope.ModalEstadoSolicitus.Codigo > 0) {
                    $scope.FiltroArmado += '&Estado_Proceso=' + $scope.ModalEstadoSolicitus.Codigo;
                }
            } else {
                if ($scope.ModalEstadoProgramacion.Codigo !== undefined && $scope.ModalEstadoProgramacion.Codigo !== '' && $scope.ModalEstadoProgramacion.Codigo !== null && $scope.ModalEstadoProgramacion.Codigo > 0) {
                    $scope.FiltroArmado += '&Estado_Proceso=' + $scope.ModalEstadoProgramacion.Codigo;
                }
            }
            if ($scope.ModeloTercero !== undefined && $scope.ModeloTercero !== '' && $scope.ModeloTercero !== null) {
                $scope.FiltroArmado += '&Identificacion_Cliente=' + $scope.ModeloTercero;
            }

            if ($scope.ModeloOficinas.Codigo !== undefined && $scope.ModeloOficinas.Codigo !== '' && $scope.ModeloOficinas.Codigo !== null && $scope.ModeloOficinas.Codigo > 0) {
                $scope.FiltroArmado += '&Codigo_Oficina=' + $scope.ModeloOficinas.Codigo;
            }

            $scope.FiltroArmado += '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Nombre;
        }

    }
]);