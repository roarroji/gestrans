﻿SofttoolsApp.controller("GestionarArchivoUIAFController", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ValorCatalogosFactory) {
        
        $scope.MapaSitio = [{ Nombre: 'Tesoreria' }, { Nombre: 'Procesos' }, { Nombre: 'Generar Archivo SIPLAF' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + CODIGO_GENERAR_ARCHIVO_UIAF); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        //Obtiene la URL para llamar el proyecto ASP
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

        $scope.Generar = function () {
            blockUI.start('Generando SIPLAFT ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Generando = true;
            $scope.MensajesError = [];


            if (DatosRequeridos()) {
                var fechainicial = Formatear_Fecha_Dia_Mes_Ano($scope.ModeloFechaInicio);
                var fechafinal = Formatear_Fecha_Dia_Mes_Ano($scope.ModeloFechaFinal);

                var CodigoUsuario = $scope.Sesion.UsuarioAutenticado.Codigo;
                window.open($scope.urlASP + '/Tesoreria/ArchivoUIAF.aspx?UIAF=true&Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&FechInic=' + fechainicial + '&FechFina=' + fechafinal + '&CodigoUsuario=' + CodigoUsuario);

            }


            blockUI.stop();
        };


        function DatosRequeridos() {

            var continuar = true;

            if ($scope.ModeloFechaInicio == null) {
                $scope.MensajesError.push('Debe ingresar un rango de fechas para generar la interfaz');
                continuar = false;
            }

            if ($scope.ModeloFechaInicio !== null && $scope.ModeloFechaFinal !== null) {
                if ($scope.ModeloFechaInicio > $scope.ModeloFechaFinal) {
                    $scope.MensajesError.push('La fecha inicial debe ser menor que la fecha final');
                    continuar = false;
                }
            }
            if ($scope.ModeloFechaInicio !== null && $scope.ModeloFechaFinal == null) {
                $scope.ModeloFechaFinal = $scope.ModeloFechaInicio;
            }
            else if ($scope.ModeloFechaInicio == null && $scope.ModeloFechaFinal !== null) {
                $scope.MensajesError.push('Debe seleccionar la fecha inicial');
                continuar = false;
            }
            return continuar
           
        }


    }]);