﻿SofttoolsApp.controller("GestionarComprobanteEgresosCtrl", ['$scope', '$routeParams', '$linq', 'blockUI', '$timeout', 'TercerosFactory', 'ValorCatalogosFactory', 'OficinasFactory', 'blockUIConfig',
    'DocumentoComprobantesFactory', 'CajasFactory', 'CuentaBancariasFactory', 'PlanUnicoCuentasFactory', 'VehiculosFactory', 'ConceptoContablesFactory', 'DetalleConceptoContablesFactory',
    'blockUIConfig', 'CierreContableDocumentosFactory', 'PlanillaDespachosFactory', 'LiquidacionesFactory', 'RutasFactory', 'ConceptoGastosFactory', 'PeajesFactory', 'ValorCombustibleFactory', 'SemirremolquesFactory', 'RendimientoGalonCombustibleFactory',
    'LiquidacionDespachosProveedoresFactory', 'LegalizacionGastosFactory',
    function ($scope, $routeParams, $linq, blockUI, $timeout, TercerosFactory, ValorCatalogosFactory, OficinasFactory, blockUIConfig,
        DocumentoComprobantesFactory, CajasFactory, CuentaBancariasFactory, PlanUnicoCuentasFactory, VehiculosFactory, ConceptoContablesFactory, DetalleConceptoContablesFactory,
        blockUIConfig, CierreContableDocumentosFactory, PlanillaDespachosFactory, LiquidacionesFactory, RutasFactory, ConceptoGastosFactory, PeajesFactory, ValorCombustibleFactory, SemirremolquesFactory, RendimientoGalonCombustibleFactory,
        LiquidacionDespachosProveedoresFactory, LegalizacionGastosFactory) {
        /*-----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- */
        $scope.Titulo = 'GESTIONAR COMPROBANTES EGRESO';
        $scope.MapaSitio = [{ Nombre: 'Tesorería' }, { Nombre: 'Documentos' }, { Nombre: 'Comprobante de Egreso' }, { Nombre: 'Gestionar' }];

        //console.clear()
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_COMPROBANTE_EGRESOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/

        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 }
        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 0');

        $scope.ListadoCuentasPUC = [];
        $scope.ListadoOficinas = [];
        $scope.ListadoDocumentosOrigen = [];
        $scope.ListadoCuentaBancariaOficinas = [];
        $scope.ListadoConceptoContable = [];
        $scope.ListadoVehiculos = [];
        $scope.ListaFormaPago = [];
        $scope.MensajesError = [];
        $scope.ListadoCuentaBancarias = [];
        $scope.ListadoMovimiento = [];
        $scope.ListadoTodaCajaOficinas = [];
        $scope.ListadoCajaOficinas = [];
        $scope.ListadoNumeroDocumentosOrigen = [];

        $scope.ActivarEfectivo = false
        $scope.ActivarCheque = false
        $scope.ActivarConsignacion = false
        $scope.PlacaValida = true;
        $scope.SeccionAnticipoPaqueteria = false;
        $scope.RutaPlanillaAnticipo = '';
        $scope.ListadoGastosRuta = [];
        $scope.ListadoConceptos = '';
        $scope.AplicaCombustiblePlanillaAnticipo = true;
        $scope.AplicaPeajesPlanillaAnticipo = true;
        $scope.InhabilitarValorFormaPago = false;
        var contProcesosValidarPlanilla = 0;
        $scope.ComprobanteDefinitivo = false;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Numero: 0,
            Fecha: new Date(),
            FechaConsignacion: new Date(),
            Debito: 0,
            Credito: 0,
            Diferencia: 0,
            OficinaCrea: $scope.Sesion.UsuarioAutenticado.Oficinas[0],
        }
        $scope.Modal = {
            TerceroModal: { NumeroIdentificacion: 0 },
            TerceroModal: { NombreCompleto: '' }
        }

        /* Obtener parametros del enrutador*/
        if ($routeParams.Numero !== undefined) {
            $scope.Modelo.Numero = $routeParams.Numero;
        }
        else {
            $scope.Modelo.Numero = 0;
            FindCierre();
        }
        $scope.ValidarCierre = function () {
            ValidarCierre();
        }

        function ValidarCierre() {
            if ($scope.ListadoTipoDocumentos.length > 0) {
                var anoFecha = $scope.Modelo.Fecha.getFullYear();
                var mesFecha = $scope.Modelo.Fecha.getMonth();
                for (var k = 0; k < $scope.ListadoTipoDocumentos.length; k++) {
                    var item = $scope.ListadoTipoDocumentos[k];
                    var mes = MascaraNumero(item.Mes.CampoAuxiliar2);
                    if (item.Ano === anoFecha && mes === (mesFecha + 1) && item.EstadoCierre.Codigo === 18002) {
                        ShowError('La fecha se encuentra en un mes y año con cierre contable');
                        $scope.Modelo.Fecha = '';
                        $scope.PeriodoValido = false
                        break;
                    }
                }
            }
        };

        function FindCierre() {
            var anoFecha = $scope.Modelo.Fecha.getFullYear();
            var mesFecha = $scope.Modelo.Fecha.getMonth();
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentos: { Codigo: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO },
                Ano: anoFecha,
                Mes: { Codigo: ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_MES }, CampoAuxiliar2: (mesFecha + 1), Sync: true }).Datos[0].Codigo },
                EstadoCierre: { Codigo: 18002 },
                Sync: true
            };
            var RCierreContable = CierreContableDocumentosFactory.Consultar(filtros)
            if (RCierreContable.ProcesoExitoso === true) {
                if (RCierreContable.Datos.length > 0) {
                    ShowError('La fecha ingresada no se encuentra en un periodo contable abierto');
                    $scope.Modelo.Fecha = '';
                    $scope.PeriodoValido = false
                    $scope.ListadoTipoDocumentos = RCierreContable.Datos;
                    $scope.ValidarCierre();
                }
            }
        }


        /*Cargar ciudad oficina de la planillla*/
        $scope.ListadoOficinasPlanilla = [];
        OficinasFactory.Consultar({ CodigoCiudad: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinasPlanilla = response.data.Datos;
                    $scope.Oficina = $scope.ListadoOficinasPlanilla[$scope.ListadoOficinasPlanilla.length - 1];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el autocomplete de cuentas PUC*/
        PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCuentasPUC = response.data.Datos

                    if ($scope.CodigoCuentaPUC !== undefined && $scope.CodigoCuentaPUC !== '' && $scope.CodigoCuentaPUC !== null) {
                        $scope.Modal.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CodigoCuentaPUC)
                    }
                }
                else {
                    $scope.ListadoCuentasPUC = [];
                }
            }, function (response) {
            });
        /*Cargar el combo de Oficinas*/
        $scope.ListadoOficinas = [];
        //var ResponseOficinas = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos
        //if (ResponseOficinas != undefined) {

        //    $scope.ListadoOficinas = ResponseOficinas
        //    if ($scope.CodigoOficinaDestino !== undefined && $scope.CodigoOficinaDestino !== '' && $scope.CodigoOficinaDestino !== null) {
        //        $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.CodigoOficinaDestino)
        //    }
        //    else {
        //        $scope.Modelo.OficinaDestino = $scope.ListadoOficinas[0];
        //    }
        //} else {
        //    $scope.ListadoOficinas = [];
        //}



        //Otras Oficinas 
        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            RListadoOficinas = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true })//.
            //then(function (response) {
            $scope.ListadoOficinas.push({ Nombre: '(No aplica)', Codigo: -1 })
            if (RListadoOficinas.ProcesoExitoso === true) {
                RListadoOficinas.Datos.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
            }
            //}, function (response) {
            //    ShowError(response.statusText);
            //});
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas.push({ Nombre: '(No aplica)', Codigo: -1 })
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
            } else {
                ShowError('El usuario no tiene oficinas asociadas')
            }
        }

        /*Cargar el combo de Documenteos Origen*/
        var ResponseListadoDocumentosOrigen = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN }, Sync: true }).Datos
        if (ResponseListadoDocumentosOrigen != undefined) {

            ResponseListadoDocumentosOrigen.forEach(function (item) {
                if (item.CampoAuxiliar2.match(/EGR/)) {
                    $scope.ListadoDocumentosOrigen.push(item);
                }
            });
            $scope.ListadoDocumentosOrigen.push({ Codigo: 2600, Nombre: "(SELECCIONAR)" });

            if ($scope.CodigoDocumentoOrigen !== undefined && $scope.CodigoDocumentoOrigen !== '' && $scope.CodigoDocumentoOrigen !== null) {
                $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == ' + $scope.CodigoDocumentoOrigen);
            }
            else {
                $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == 2600');
            }

        }

        /*Cargar el combo de Forma de pago*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_MEDIO_PAGO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaFormaPago = response.data.Datos

                    if ($scope.CodigoFormaPago !== undefined && $scope.CodigoFormaPago !== '' && $scope.CodigoFormaPago !== null) {
                        $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo ==' + $scope.CodigoFormaPago);
                    }
                    else {
                        $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo ==' + CODIGO_MEDIO_PAGO_NO_APLICA);
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        CuentaBancariasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoCuentaBancaria: { Codigo: 403 }, Estado: 1, }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCuentaCambistas = response.data.Datos
                        if ($scope.CodigoCuentaBancaria !== undefined && $scope.CodigoCuentaBancaria !== null && $scope.CodigoCuentaBancaria !== '' && $scope.CodigoCuentaBancaria !== 0) {
                            $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaCambistas).First('$.Codigo == ' + $scope.CodigoCuentaBancaria);
                        }
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.ListaCentrosCostos = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CENTROS_COSTOS_COMPROBANTE_EGRESO }, Sync: true }).Datos;

        $scope.Modelo.CentroCostos = $linq.Enumerable().From($scope.ListaCentrosCostos).First('$.Codigo == 21500');
        function CargarCuentasBancarias() {
            /*Cargar el combo Cuentas Bancarias por oficina*/
            var ResponseCuentas = CuentaBancariasFactory.CuentaBancariaOficinas({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.OficinaDestino.Codigo, Sync: true })
            //then(function (response) {
            if (ResponseCuentas.ProcesoExitoso === true) {
                $scope.ListadoCuentaBancariaOficinas = [];
                $scope.ListadoCuentaBancariaOficinas = ResponseCuentas.Datos;

                if ($scope.CodigoCuentaBancaria !== undefined && $scope.CodigoCuentaBancaria !== null && $scope.CodigoCuentaBancaria !== '' && $scope.CodigoCuentaBancaria !== 0) {
                    $scope.ListadoCuentaBancariaOficinas.forEach(function (item) {
                        if (item.CuentaBancaria.Codigo > 0) {
                            if (item.Oficina.Codigo == $scope.CodigoOficinaDestino) {
                                $scope.ListadoCuentaBancarias.push(item);
                            }
                        }
                    });

                    // $scope.Modelo.CuentaBancariaCheque = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + $scope.CodigoCuentaBancaria);
                    // $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + $scope.CodigoCuentaBancaria);
                    $scope.Modelo.CuentaBancariaCheque = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CuentaBancaria.Codigo == ' + $scope.CodigoCuentaBancaria);
                    $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CuentaBancaria.Codigo == ' + $scope.CodigoCuentaBancaria);
                }
                else {
                    $scope.ListadoCuentaBancarias = [];
                    $scope.ListadoCuentaBancariaOficinas.forEach(function (item) {
                        if (item.Oficina.Codigo == $scope.Modelo.OficinaDestino.Codigo) {
                            $scope.ListadoCuentaBancarias.push(item);
                        }
                    });
                    if ($scope.ListadoCuentaBancarias.length > 0) {
                        $scope.Modelo.CuentaBancariaConsignacion = $scope.ListadoCuentaBancarias[0];
                        $scope.Modelo.CuentaBancariaCheque = $scope.ListadoCuentaBancarias[0];
                    }
                    else {
                        $scope.MensajesError.push('La oficina seleccionada no tiene ninguna cuenta bancaria asociada');
                    }
                }
            }
            //}, function (response) {
            //    ShowError(response.statusText);
            //});
        }
        function CargarCajas() {
            $scope.ListadoCajaOficinas = [];
            $scope.MensajesError = [];

            CajasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1,
                UsuarioCrea: $scope.Sesion.UsuarioAutenticado
            }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTodaCajaOficinas = [];
                        $scope.ListadoTodaCajaOficinas = response.data.Datos;

                        if ($scope.CodigoCaja !== undefined && $scope.CodigoCaja !== null && $scope.CodigoCaja !== '' && $scope.CodigoCaja !== 0) {
                            $scope.ListadoTodaCajaOficinas.forEach(function (itmOfic) {
                                if (itmOfic.Codigo > 0) {
                                    if (itmOfic.Oficina.Codigo == $scope.CodigoOficinaDestino) {
                                        $scope.ListadoCajaOficinas.push(itmOfic);
                                    }
                                }
                            });

                            $scope.Modelo.Caja = $linq.Enumerable().From($scope.ListadoCajaOficinas).First('$.Codigo == ' + $scope.CodigoCaja);

                        }
                        else {
                            $scope.ListadoCajaOficinas = [];
                            $scope.ListadoTodaCajaOficinas.forEach(function (itmOfic) {
                                if (itmOfic.Codigo > 0) {
                                    if (itmOfic.Oficina.Codigo == $scope.Modelo.OficinaDestino.Codigo) {
                                        $scope.ListadoCajaOficinas.push(itmOfic);
                                    }
                                }
                            });
                            if ($scope.ListadoCajaOficinas.length > 0) {
                                $scope.Modelo.Caja = $scope.ListadoCajaOficinas[0];
                            }
                            else {
                                if ($scope.ValidarCajasOficina == true) {
                                    $scope.MensajesError.push('La oficina seleccionada no tiene ninguna caja asociada');
                                }
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        /*Cargar Combo Conceptos Contables*/
        ConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoConceptoContable = response.data.Datos;

                        if ($scope.CodigoConceptoContable !== undefined && $scope.CodigoConceptoContable !== '' && $scope.CodigoConceptoContable !== null) {
                            if ($scope.CodigoConceptoContable > 0) {
                                $scope.Modelo.ConceptoContable = $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo ==' + $scope.CodigoConceptoContable);
                            }
                        }
                    }
                    else {
                        $scope.ListadoConceptoContable = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.AsignarPlaca = function (Placa) {
            if (Placa != undefined || Placa != null) {
                if (angular.isObject(Placa)) {
                    $scope.LongitudPlaca = Placa.length;
                    $scope.PlacaValida = true;
                    $scope.ListaBeneficiariotercero = []
                    $scope.Modelo.Tercero = $scope.CargarTercero(Placa.Tenedor.Codigo)
                    $scope.Modelo.Beneficiario = $scope.CargarTercero(Placa.Tenedor.Codigo)
                    $scope.ListaBeneficiariotercero.push($scope.CargarTercero(Placa.Tenedor.Codigo))
                    var cont = 0
                    for (var i = 0; i < $scope.ListaBeneficiariotercero.length; i++) {
                        if ($scope.ListaBeneficiariotercero[i].Codigo == Placa.Conductor.Codigo) {
                            cont++
                            break
                        }
                    }
                    if (cont == 0) {
                        $scope.ListaBeneficiariotercero.push($scope.CargarTercero(Placa.Conductor.Codigo))
                    }
                }
                else if (angular.isString(Placa)) {
                    $scope.LongitudPlaca = Placa.length;
                    $scope.PlacaValida = false;
                }
            }
        };
        $scope.AsignarBeneficiario = function () {
            try {
                if ($scope.Modelo.Tercero.Codigo > 0) {
                    $scope.Modelo.Beneficiario = $scope.Modelo.Tercero
                }
            } catch (e) {
                $scope.Modelo.Beneficiario = ''
            }
        }
        $scope.ListadoTerceros = []
        $scope.AutocompleteTerceros = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            //CadenaPerfiles: PERFIL_TENEDOR,
                            ValorAutocomplete: value, Sync: true
                        })
                    $scope.ListadoTerceros = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTerceros)
                }
            }
            return $scope.ListadoTerceros
        }
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 2) == 0 || value.length == 1) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                }
            }
            return $scope.ListadoVehiculos
        }
        $scope.ValidarTercero = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modelo.Tercero = response.data.Datos
                            }
                            else {
                                $scope.Modelo.Tercero = undefined
                                ShowError('La identificación Ingresada no es válida o el tercero no se encuentra registrado')
                            }
                        }
                    });
            }
        };

        $scope.ConsultarTerceros = function (Nombre, opcion) {
            $scope.Opcion = opcion

            if (Nombre !== undefined && Nombre !== '' && Nombre !== null) {
                TercerosFactory.ConsultarTerceroGeneral({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Nombre: Nombre }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoTerceros = response.data.Datos
                                showModal('ConsultaTerceros')
                            }
                            else {
                                showDialog('El tercero ingresado se encuentra inactivo o no existe en el sistema')
                            }
                        }
                    });
            }
        }


        $scope.AsignarTercero = function (item) {
            $scope.ListadoTerceros.forEach(function (itemTercero) {
                if (itemTercero.Codigo == item.Codigo) {
                    if ($scope.Opcion == 1) {
                        $scope.Modelo.Tercero = itemTercero
                    }
                    if ($scope.Opcion == 2) {
                        $scope.Modelo.Beneficiario = itemTercero
                    }
                    if ($scope.Opcion == 3) {
                        $scope.Modelo.Tercero = itemTercero
                    }
                    closeModal('ConsultaTerceros')
                }
            })
        }



        $scope.ValidarTerceroModal = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modal.TerceroModal = response.data.Datos
                            }
                            else {
                                $scope.Modal.TerceroModal = undefined
                                ShowError('La identificación Ingresada no es válida o el tercero no se encuentra registrado')
                            }
                        }
                    });
            }
        };
        $scope.ValidarBeneficiario = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modelo.Beneficiario = response.data.Datos
                            }
                            else {
                                $scope.Modelo.Beneficiario = undefined
                                ShowError('La identificación Ingresada no es válida o el beneficiario no se encuentra registrado')
                            }
                        }
                    });
            }
        };

        $scope.validarFormaPago = function () {
            if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                $scope.ActivarEfectivo = false
                $scope.ActivarCheque = false
                $scope.ActivarConsignacion = true
                if ($scope.Sesion.UsuarioAutenticado.ProcesoSugerirAnticipoComprobanteEgreso) {
                    if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO) {
                        if ($scope.ValorAnticipoPlanillaAnticipo != undefined && $scope.ValorAnticipoPlanillaAnticipo != null && $scope.ValorAnticipoPlanillaAnticipo != '') {
                            $scope.Modelo.ValorPagoTransferencia = $scope.ValorAnticipoPlanillaAnticipo;
                        }
                    }
                    if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA) {
                        $scope.InhabilitarValorFormaPago = false;
                    } else {
                        $scope.InhabilitarValorFormaPago = false;
                    }
                } else {
                    $scope.InhabilitarValorFormaPago = false;
                }
                $scope.Modelo.CuentaBancariaCheque = ''
                $scope.Modelo.FechaCheque = null
                $scope.Modelo.ValorPagoCheque = ''


                $scope.Modelo.Caja = ''
                $scope.Modelo.ValorPagoEfectivo = ''

            }
            if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CAMBISTA) {
                $scope.ActivarEfectivo = false
                $scope.ActivarCheque = false
                $scope.ActivarConsignacion = false
                $scope.ActivarCambista = true

                $scope.Modelo.CuentaBancariaCheque = ''
                $scope.Modelo.FechaCheque = null
                $scope.Modelo.FechaConsignacion = new Date()
                $scope.Modelo.ValorPagoCheque = ''

                $scope.Modelo.Caja = ''
                $scope.Modelo.ValorPagoEfectivo = ''


            }
            else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                $scope.ActivarEfectivo = false
                $scope.ActivarCheque = true
                $scope.ActivarConsignacion = false

                $scope.Modelo.Caja = ''
                $scope.Modelo.ValorPagoEfectivo = ''

                $scope.Modelo.CuentaBancariaConsignacion = ''
                $scope.Modelo.FechaConsignacion = null
                if ($scope.Sesion.UsuarioAutenticado.ProcesoSugerirAnticipoComprobanteEgreso) {
                    if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO) {
                        if ($scope.ValorAnticipoPlanillaAnticipo != undefined && $scope.ValorAnticipoPlanillaAnticipo != null && $scope.ValorAnticipoPlanillaAnticipo != '') {
                            $scope.Modelo.ValorPagoCheque = $scope.ValorAnticipoPlanillaAnticipo;
                        }
                    }

                    if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA) {
                        $scope.InhabilitarValorFormaPago = false;
                    } else {
                        $scope.InhabilitarValorFormaPago = false;
                    }
                } else {
                    $scope.InhabilitarValorFormaPago = false;
                }
                $scope.Modelo.ValorPagoTransferencia = ''


                $scope.Modelo.NumeroConsignacion = ''

            }
            else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                $scope.ActivarEfectivo = true
                $scope.ActivarCheque = false
                $scope.ActivarConsignacion = false

                $scope.Modelo.CuentaBancariaConsignacion = ''
                $scope.Modelo.FechaConsignacion = null
                if ($scope.Sesion.UsuarioAutenticado.ProcesoSugerirAnticipoComprobanteEgreso) {
                    if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO) {
                        if ($scope.ValorAnticipoPlanillaAnticipo != undefined && $scope.ValorAnticipoPlanillaAnticipo != null && $scope.ValorAnticipoPlanillaAnticipo != '') {
                            $scope.Modelo.ValorPagoEfectivo = $scope.ValorAnticipoPlanillaAnticipo;
                        }
                    }

                    if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA) {
                        $scope.InhabilitarValorFormaPago = false;
                    } else {
                        $scope.InhabilitarValorFormaPago = false;
                    }
                } else {
                    $scope.InhabilitarValorFormaPago = false;
                }
                $scope.Modelo.ValorPagoTransferencia = ''

                $scope.Modelo.NumeroConsignacion = ''


                $scope.Modelo.CuentaBancariaCheque = ''
                $scope.Modelo.FechaCheque = null
                $scope.Modelo.ValorPagoCheque = ''
            }
            else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_NO_APLICA) {
                $scope.ActivarEfectivo = false
                $scope.ActivarCheque = false
                $scope.ActivarConsignacion = false

                $scope.Modelo.CuentaBancariaConsignacion = ''
                $scope.Modelo.FechaConsignacion = null

                $scope.Modelo.ValorPagoTransferencia = ''

                $scope.Modelo.NumeroConsignacion = ''


                $scope.Modelo.CuentaBancariaCheque = ''
                $scope.Modelo.FechaCheque = null
                $scope.Modelo.ValorPagoCheque = ''


                $scope.Modelo.Caja = ''
                $scope.Modelo.ValorPagoEfectivo = ''
            }
        }

        $scope.AsignarOficinaDestino = function (Oficina) {
            $scope.MensajesError = [];
            if (Oficina != undefined) {
                if (Oficina.Codigo !== 0 && Oficina.Codigo !== undefined && Oficina.Codigo !== null) {
                    if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                        CargarCajas()
                    }
                    else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE || $scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                        CargarCuentasBancarias()
                    }
                }
            }
        }
        $scope.ValidarFormaPago = function (Oficina) {
            if ($scope.Modelo.FormaPagoRecaudo.Codigo !== CODIGO_MEDIO_PAGO_NO_APLICA) {
                $scope.AsignarOficinaDestino(Oficina)
            }

            if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA) {

                if ($scope.Sesion.UsuarioAutenticado.ProcesoSugerirAnticipoComprobanteEgreso) {
                    if ($scope.Sesion.UsuarioAutenticado.CodigoEmpresa != 13) {
                        $scope.SeccionAnticipoPaqueteria = true;
                    }
                    $scope.InhabilitarValorFormaPago = false;
                    $scope.CalcularAnticipo();
                } else {
                    $scope.InhabilitarValorFormaPago = false;
                }
            } else {
                $scope.SeccionAnticipoPaqueteria = false;
                $scope.InhabilitarValorFormaPago = false;
            }
        }
        /*Funcion Limpiar Modal Movimiento*/
        function limpiarModalMovimiento() {
            $scope.Modal.ValorBase = '';
            $scope.Modal.ValorDebito = '';
            $scope.Modal.ValorCredito = '';
            $scope.Modal.CuentaPUC = '';
            $scope.Modal.CodigoCuentaPUC = '';
        }

        $scope.ModalMovimiento = function () {
            $scope.MensajesError = [];
            $scope.Modal.TerceroModal.NumeroIdentificacion = '';
            $scope.Modal.TerceroModal.NombreCompleto = '';
            if ($scope.Modelo.FormaPagoRecaudo.Codigo !== CODIGO_MEDIO_PAGO_NO_APLICA) {
                $scope.MensajesErrorMovimiento = [];
                if (($scope.Modelo.ValorPagoCheque !== undefined && $scope.Modelo.ValorPagoCheque !== '' && $scope.Modelo.ValorPagoCheque !== null) || ($scope.Modelo.ValorPagoEfectivo !== undefined && $scope.Modelo.ValorPagoEfectivo !== '' && $scope.Modelo.ValorPagoEfectivo !== null) || ($scope.Modelo.ValorPagoTransferencia !== undefined && $scope.Modelo.ValorPagoTransferencia !== '' && $scope.Modelo.ValorPagoTransferencia !== null)) {
                    limpiarModalMovimiento();
                    showModal('modalMovimiento');
                    if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                        $scope.Modal.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                    }
                    if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                        $scope.Modal.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                    }
                    if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                        $scope.Modal.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                    }
                }
                else {
                    $scope.MensajesError.push('Debe Ingresar el valor de la forma de pago seleccionada');
                    Ventana.scrollTop = 0
                }
            }
            else {
                $scope.MensajesError.push('Debe seleccionar una forma de pago');
                Ventana.scrollTop = 0

            }
        };
        $scope.GuardarMovimiento = function () {
            $scope.MensajesErrorMovimiento = [];
            if (DatosRequeridosMovimiento()) {
                CargarlistadoMovimiento()
            }
        }
        function CargarlistadoMovimiento() {
            closeModal('modalMovimiento');
            $scope.ListadoMovimiento.push({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CuentaPUC: $scope.Modal.CuentaPUC,
                //CentroCosto: $scope.Modelo.CentroCosto,
                ValorBase: MascaraNumero($scope.Modal.ValorBase),
                ValorDebito: MascaraNumero($scope.Modal.ValorDebito),
                ValorCredito: MascaraNumero($scope.Modal.ValorCredito),
                Tercero: { Codigo: $scope.Modal.TerceroModal.Codigo },
                //GeneraCuenta: 0,
                //Observaciones: '',
                //Prefijo: '',
                //CodigoAnexo: '',
                //SufijoCodigoAnexo: '',
                //CampoAuxiliar: '',
                TerceroParametrizacion: { Codigo: 3100 },
                DocumentoCruce: { Codigo: 2800 },
                CentroCostoParametrizacion: { Codigo: 3200 },
            });
            CalcularMovimiento()
        }
        /*Funcion Calcular Totales*/
        function CalcularMovimiento() {
            $scope.Modelo.Debito = 0
            $scope.Modelo.Credito = 0
            $scope.Modelo.Diferencia = 0
            /*realiza el recorrido para  obtener  los totales */
            if ($scope.ListadoMovimiento.length > 0) {
                $scope.ListadoMovimiento.forEach(function (item) {
                    $scope.Modelo.Debito = Math.ceil($scope.Modelo.Debito) + Math.ceil(item.ValorDebito)
                    $scope.Modelo.Credito = Math.ceil($scope.Modelo.Credito) + Math.ceil(item.ValorCredito)
                })
            }
            $scope.Modelo.Diferencia = Math.ceil(($scope.Modelo.Debito) - parseInt($scope.Modelo.Credito))
        }

        function DatosRequeridosMovimiento() {
            $scope.MensajesErrorMovimiento = [];
            var con = 0
            if ($scope.ListadoMovimiento.length > 0) {
                $scope.ListadoMovimiento.forEach(function (item) {
                    if (item.CuentaPUC.Codigo == $scope.Modal.CuentaPUC.Codigo) {
                        con++
                    }
                })
            }
            if (con == 0) {
                var continuar = true;
                if ($scope.Modal.CuentaPUC == undefined || $scope.Modal.CuentaPUC == null || $scope.Modal.CuentaPUC == "") {
                    $scope.MensajesErrorMovimiento.push('Debe Seleccionar una Cuenta Puc');
                    continuar = false;
                }
                if ($scope.Modal.CuentaPUC.ExigeValorBase == 1) {
                    if ($scope.Modal.ValorBase == 0 || $scope.Modal.ValorBase == "") {
                        $scope.MensajesErrorMovimiento.push('Debe ingresar el  valor base ');
                        continuar = false;
                    }
                }
                else if ($scope.Modal.ValorBase == undefined || $scope.Modal.ValorBase == null || $scope.Modal.ValorBase == "") {
                    $scope.Modal.ValorBase = 0
                }
                if ($scope.Modal.CuentaPUC.ExigeTercero == 1) {
                    if ($scope.Modal.TerceroModal.NombreCompleto == undefined || $scope.Modal.TerceroModal.NombreCompleto == "" || $scope.Modal.TerceroModal.NombreCompleto == null) {
                        $scope.MensajesErrorMovimiento.push('Debe ingresar el tercero');
                        continuar = false;
                    }
                }
                if ($scope.Modal.ValorDebito > 0 && $scope.Modal.ValorCredito > 0) {
                    $scope.MensajesErrorMovimiento.push('Debe ingresar solo un valor ya sea Crédito o Débito');
                    continuar = false;
                }
                if (($scope.Modal.ValorDebito == undefined || $scope.Modal.ValorDebito == null || $scope.Modal.ValorDebito == "")
                    && ($scope.Modal.ValorCredito !== undefined && $scope.Modal.ValorCredito !== null && $scope.Modal.ValorCredito !== "")) {
                    $scope.Modal.ValorDebito = 0;
                } else if (($scope.Modal.ValorDebito !== undefined && $scope.Modal.ValorDebito !== null && $scope.Modal.ValorDebito !== "") &&
                    ($scope.Modal.ValorCredito == undefined || $scope.Modal.ValorCredito == null || $scope.Modal.ValorCredito == "")) {
                    $scope.Modal.ValorCredito = 0;
                }
                else {
                    $scope.MensajesErrorMovimiento.push('Debe ingresar solo un valor ya sea Crédito o Débito');
                    continuar = false;
                }
            }
            else {
                $scope.MensajesErrorMovimiento.push('La cuenta PUC ' + $scope.Modal.CuentaPUC.Nombre + ' ya se encuentra ingresada');
                continuar = false;
            }
            return continuar;
        }
        $scope.LimpiarMovimiento = function () {
            $scope.ListadoMovimiento = [];
            $scope.Modelo.Debito = 0;
            $scope.Modelo.Credito = 0;
            $scope.Modelo.Diferencia = 0;
        };

        /*Eliminar Movimiento*/
        $scope.ConfirmacionEliminarMovimiento = function (indice) {
            $scope.MensajeEliminar = { indice };
            $scope.VarAuxi = indice
            showModal('modalEliminarMovimiento');
        };

        $scope.EliminarMovimiento = function (indice) {
            $scope.ListadoMovimiento.splice($scope.VarAuxi, 1);
            closeModal('modalEliminarMovimiento');
            CalcularMovimiento()
        };

        $scope.ConsultarMovimiento = function () {
            if (DatosRequeridosConsutarMovimiento()) {
                if ($scope.Modelo.ConceptoContable !== undefined && $scope.Modelo.ConceptoContable !== null && $scope.Modelo.ConceptoContable !== '') {
                    if ($scope.Modelo.ConceptoContable.Codigo == 0) {
                        //Crédito
                        var item = {};
                        if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                            item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                            item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                            item.CuentaPUC = { Nombre: $scope.Caja.PlanUnicoCuentas.Nombre, Codigo: $scope.Caja.PlanUnicoCuentas.Codigo }

                        } else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                            item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoCheque);
                            item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                            item.CuentaPUC = { Nombre: $scope.CuentaBancariaCheque.PlanUnicoCuentas.Nombre, Codigo: $scope.CuentaBancariaCheque.PlanUnicoCuentas.Codigo }

                        } else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA || $scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CAMBISTA) {
                            item.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                            item.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                            item.CuentaPUC = { Nombre: $scope.CuentaBancariaConsignacion.PlanUnicoCuentas.Nombre, Codigo: $scope.CuentaBancariaConsignacion.PlanUnicoCuentas.Codigo }
                        }
                        //item.ValorDebito = 0;
                        //item.GeneraCuenta = 0;
                        //item.Observaciones = '';
                        //item.Prefijo = 0;
                        //item.CodigoAnexo = 0;
                        //item.CampoAuxiliar = '';
                        item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        item.Tercero = { Codigo: $scope.Modelo.Beneficiario.Codigo }
                        item.TerceroParametrizacion = { Codigo: 3100 };
                        item.DocumentoCruce = { Codigo: 2800 };
                        item.CentroCostoParametrizacion = { Codigo: 3200 };
                        $scope.ListadoMovimiento.push(item);

                        CalcularMovimiento();
                    } else {
                        DetalleConceptoContablesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.ConceptoContable.Codigo }).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.length > 0) {
                                        response.data.Datos.forEach(function (item) {
                                            var Concepto = {}
                                            if (item.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_DEBITO) {
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                                                    Concepto.ValorDebito = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                }
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                                                    Concepto.ValorDebito = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                }
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA || $scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CAMBISTA) {
                                                    Concepto.ValorDebito = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                                                    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia)
                                                }
                                                Concepto.ValorCredito = 0;
                                                if (item.CuentaPUC.AplicarMedioPago > 0) {
                                                    if ($scope.ActivarCambista) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                    }
                                                    if ($scope.ActivarCheque) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaCheque.CuentaBancaria.CuentaPUC
                                                    }
                                                    if ($scope.ActivarEfectivo) {
                                                        Concepto.CuentaPUC = $scope.Modelo.Caja.CuentaPUC
                                                    }
                                                    if ($scope.ActivarConsignacion) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                    }

                                                } else {
                                                    Concepto.CuentaPUC = item.CuentaPUC;
                                                }
                                            }
                                            if (item.NaturalezaConceptoContable.Codigo == NATURALEZA_CONTABLE_CREDITO) {
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                                                    Concepto.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                                                }
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                                                    Concepto.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoCheque);
                                                }
                                                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA || $scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CAMBISTA) {
                                                    Concepto.ValorCredito = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                                                    Concepto.ValorBase = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                                                }
                                                Concepto.ValorDebito = 0;
                                                if (item.CuentaPUC.AplicarMedioPago > 0) {
                                                    if ($scope.ActivarCambista) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                    }
                                                    if ($scope.ActivarCheque) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaCheque.CuentaBancaria.CuentaPUC
                                                    }
                                                    if ($scope.ActivarEfectivo) {
                                                        Concepto.CuentaPUC = $scope.Modelo.Caja.CuentaPUC
                                                    }
                                                    if ($scope.ActivarConsignacion) {
                                                        Concepto.CuentaPUC = $scope.Modelo.CuentaBancariaConsignacion.CuentaBancaria.CuentaPUC
                                                    }

                                                } else {
                                                    Concepto.CuentaPUC = item.CuentaPUC;
                                                }
                                            }

                                            Concepto.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                                            Concepto.Tercero = { Codigo: $scope.Modelo.Beneficiario.Codigo };
                                            Concepto.TerceroParametrizacion = { Codigo: 3100 }
                                            Concepto.DocumentoCruce = { Codigo: 2800 }
                                            Concepto.CentroCostoParametrizacion = { Codigo: 3200 }
                                            $scope.ListadoMovimiento.push(Concepto)
                                            CalcularMovimiento()
                                        });
                                    } else {
                                        ShowError('El Concepto contable no se encuentra parametrizado');
                                    }
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }

            }
        };

        function DatosRequeridosConsutarMovimiento() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Modelo.ConceptoContable == undefined || $scope.Modelo.ConceptoContable == null || $scope.Modelo.ConceptoContable == "" || $scope.Modelo.ConceptoContable.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar un concepto contable');
                continuar = false;
            }

            if ($scope.Modelo.Beneficiario == undefined || $scope.Modelo.Beneficiario == null || $scope.Modelo.Beneficiario == "") {
                $scope.MensajesError.push('Debe ingresar un beneficiario');
                continuar = false;
            }
            if ($scope.Modelo.FormaPagoRecaudo.Codigo == 4700) {
                $scope.MensajesError.push('Debe seleccionar una forma de pago');
                continuar = false;
            }
            else {
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                    if ($scope.Modelo.ValorPagoEfectivo == 0) {
                        $scope.MensajesError.push('El valor ingresado debe ser mayor a 0');
                        continuar = false;
                    }
                }
                else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {

                    if ($scope.Modelo.ValorPagoCheque == 0) {
                        $scope.MensajesError.push('El valor ingresado debe ser mayor a 0');
                        continuar = false;
                    }
                }
                else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                    if ($scope.Modelo.ValorPagoTransferencia == 0) {
                        $scope.MensajesError.push('El valor ingresado debe ser mayor a 0');
                        continuar = false;
                    }
                }
                else if (($scope.Modelo.ValorPagoCheque !== undefined && $scope.Modelo.ValorPagoCheque !== '' && $scope.Modelo.ValorPagoCheque !== null && $scope.Modelo.ValorPagoEfectivo !== 0) ||
                    ($scope.Modelo.ValorPagoEfectivo !== undefined && $scope.Modelo.ValorPagoEfectivo !== '' && $scope.Modelo.ValorPagoEfectivo !== null && $scope.Modelo.ValorPagoCheque !== 0) ||
                    ($scope.Modelo.ValorPagoTransferencia !== undefined && $scope.Modelo.ValorPagoTransferencia !== '' && $scope.Modelo.ValorPagoTransferencia !== null && $scope.Modelo.ValorPagoTransferencia !== 0)) {
                }
                else {
                    $scope.MensajesError.push('Debe Ingresar el valor de la forma de pago seleccionada');
                    continuar = false;
                }
            }

            if (continuar == false) {
                Ventana.scrollTop = 0
            }


            return continuar;
        }
        /*----------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                $scope.PeriodoValido = true
                FindCierre();
                if ($scope.PeriodoValido) {
                    showModal('modalConfirmacionGuardar');
                }
            }
            else {
                Ventana.scrollTop = 0
            }
        };


        /*Guardar/Modificar Comprobante De Egreso*/
        $scope.Guardar = function () {
            if (DatosRequeridos()) {
                $scope.Modelo.CodigoAlterno = 0;
                $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO;
                $scope.Modelo.Vehiculo = $scope.Modelo.Placa;
                $scope.Modelo.FormaPagoDocumento = $scope.Modelo.FormaPagoRecaudo;
                $scope.Modelo.Estado = $scope.Estado.Codigo;
                $scope.Modelo.ValorAlterno = 0;

                if ($scope.Modelo.ConceptoContable == undefined || $scope.Modelo.ConceptoContable == "" || $scope.Modelo.ConceptoContable == null) {
                    $scope.Modelo.ConceptoContable = { Codigo: 0 }
                }
                $scope.Modelo.ConceptoContable = $scope.Modelo.ConceptoContable.Codigo

                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                    $scope.Modelo.Caja = $scope.Modelo.Caja;
                    $scope.Modelo.ValorPagoEfectivo = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                    $scope.Modelo.CuentaBancaria = { Codigo: 0 };
                    $scope.Modelo.FechaPagoRecaudo = new Date();
                    $scope.Modelo.DestinoIngreso = { Codigo: 4801 }; //Codigo Destino ingreso Caja
                    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoEfectivo);
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                    $scope.Modelo.CuentaBancaria = { Codigo: $scope.Modelo.CuentaBancariaCheque.CodigoCuenta };
                    $scope.Modelo.FechaPagoRecaudo = $scope.Modelo.FechaCheque;
                    $scope.Modelo.ValorPagoCheque = MascaraNumero($scope.Modelo.ValorPagoCheque);
                    $scope.Modelo.Numeracion = $scope.Modelo.NumeroCheque
                    $scope.Modelo.Caja = { Codigo: 0 };
                    $scope.Modelo.DestinoIngreso = { Codigo: 4802 };//Codigo Destino ingreso Cuenta Bancaria
                    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoCheque);
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                    $scope.Modelo.CuentaBancaria = { Codigo: $scope.Modelo.CuentaBancariaConsignacion.CodigoCuenta };
                    $scope.Modelo.FechaPagoRecaudo = $scope.Modelo.FechaConsignacion;
                    $scope.Modelo.NumeroPagoRecaudo = $scope.Modelo.NumeroConsignacion;
                    $scope.Modelo.ValorPagoTransferencia = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                    $scope.Modelo.Caja = { Codigo: 0 };
                    $scope.Modelo.DestinoIngreso = { Codigo: 4802 };//Codigo Destino ingreso Cuenta Bancaria
                    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CAMBISTA) {
                    $scope.Modelo.CuentaBancaria = { Codigo: $scope.Modelo.CuentaBancariaConsignacion.Codigo };
                    $scope.Modelo.FechaPagoRecaudo = $scope.Modelo.FechaConsignacion;
                    $scope.Modelo.NumeroPagoRecaudo = $scope.Modelo.NumeroConsignacion;
                    $scope.Modelo.ValorPagoTransferencia = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                    $scope.Modelo.Caja = { Codigo: 0 };
                    $scope.Modelo.DestinoIngreso = { Codigo: 4802 };//Codigo Destino ingreso Cuenta Bancaria
                    $scope.Modelo.ValorPagoTotal = MascaraNumero($scope.Modelo.ValorPagoTransferencia);
                }
                $scope.Modelo.GeneraConsignacion = "";
                $scope.Modelo.Detalle = $scope.ListadoMovimiento;
                if ($scope.Modelo.DocumentoOrigen.Codigo == 2609) {
                    $scope.Modelo.NumeroDocumentoOrigen = '';
                } else {
                    $scope.Modelo.NumeroDocumentoOrigen = $scope.Modelo.NumeroDocumentoOrigen.NumeroDocumento;
                }


                var continuar = true
                if ($scope.Modelo.DocumentoOrigen.Codigo == 2605) {
                    for (var i = 0; i < $scope.ListadoNumeroDocumentosOrigen.length; i++) {
                        if ($scope.Modelo.NumeroDocumentoOrigen == $scope.ListadoNumeroDocumentosOrigen[i].Codigo) {
                            if ($scope.ListadoNumeroDocumentosOrigen[i].ValorFleteTransportador > CERO) {
                                if (($scope.ListadoNumeroDocumentosOrigen[i].ValorAnticipo + $scope.Modelo.ValorPagoTotal) > $scope.ListadoNumeroDocumentosOrigen[i].ValorFleteTransportador && $scope.Sesion.UsuarioAutenticado.CodigoEmpresa != 5) {
                                    ShowError('El sobreanticipo ingresado sumado al anticipo ya creado supera el valor del flete')
                                    continuar = false;
                                    closeModal('modalConfirmacionGuardar');

                                    $scope.Modelo.NumeroDocumentoOrigen = '';
                                }
                            }
                        }
                    }
                    $scope.Modelo.Autorizacion = 1
                }
                if ($scope.ValorAnticipoPlanillaAnticipo != undefined && $scope.ValorAnticipoPlanillaAnticipo != null && $scope.ValorAnticipoPlanillaAnticipo != '') {
                    $scope.Modelo.ValorAnticipoPlanilla = MascaraNumero($scope.ValorAnticipoPlanillaAnticipo)
                }

                if (continuar) {
                    DocumentoComprobantesFactory.Guardar($scope.Modelo).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if ($scope.Codigo == 0) {
                                    ShowSuccess('Se guardó el comprobante de egreso N.' + response.data.Datos);
                                }
                                else {
                                    ShowSuccess('Se modificó el comprobante de egreso N.' + response.data.Datos);
                                }
                                closeModal('modalConfirmacionGuardar');
                                document.location.href = '#!ConsultarComprobanteEgresos/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            else {
                Ventana.scrollTop = 0
                closeModal('modalConfirmacionGuardarComprobanteEgreso');
            }
        }
        /*Datos Requeridos Guardar o Modificar Comprobante*/
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Fecha == undefined || $scope.Modelo.Fecha == null || $scope.Modelo.Fecha == "") {
                $scope.MensajesError.push('Debe ingresar la fecha');
                continuar = false;
            }

            if ($scope.Modelo.Tercero == undefined || $scope.Modelo.Tercero == null || $scope.Modelo.Tercero == "") {
                $scope.MensajesError.push('Debe ingresar un tercero ');
                continuar = false;
            }
            if (($scope.Modelo.Placa == undefined || $scope.Modelo.Placa == null || $scope.Modelo.Placa == "") && $scope.Modelo.DocumentoOrigen.Codigo != 2609) {
                $scope.Modelo.Placa = { Codigo: 0, Placa: '' };
                continuar = false;
                $scope.MensajesError.push('Debe Ingresar una placa');
            }
            else if ($scope.PlacaValida == false && $scope.Modelo.DocumentoOrigen.Codigo != 2609) {
                $scope.MensajesError.push('La placa ingresada no es valida');
                continuar = false;
            }
            if ($scope.Modelo.Beneficiario == undefined || $scope.Modelo.Beneficiario == null || $scope.Modelo.Beneficiario == "") {
                $scope.MensajesError.push('Debe ingresar un beneficiario');
                continuar = false;
            }
            if (($scope.Modelo.DocumentoOrigen !== undefined && $scope.Modelo.DocumentoOrigen !== null && $scope.Modelo.DocumentoOrigen.Codigo !== 2600) && $scope.Modelo.DocumentoOrigen.Codigo != 2609) {
                if ($scope.Modelo.NumeroDocumentoOrigen == "" || $scope.Modelo.NumeroDocumentoOrigen == null || $scope.Modelo.NumeroDocumentoOrigen == undefined) {
                    $scope.MensajesError.push('Debe ingresar el número origen');
                    continuar = false;
                }
            }
            if ($scope.Modelo.FormaPagoRecaudo.Codigo == 4700) {
                $scope.MensajesError.push('Debe seleccionar una forma de pago');
                continuar = false;
            }
            if ($scope.Modelo.OficinaDestino.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar una oficina');
                continuar = false;
            }
            else {
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                    if ($scope.Modelo.FechaConsignacion == undefined || $scope.Modelo.FechaConsignacion == null || $scope.Modelo.FechaConsignacion == "") {
                        $scope.MensajesError.push('Debe seleccionar la fecha de la Transferencia ');
                        continuar = false;
                    }
                    if ($scope.Modelo.CuentaBancariaConsignacion == undefined || $scope.Modelo.CuentaBancariaConsignacion == null || $scope.Modelo.CuentaBancariaConsignacion == "") {
                        $scope.MensajesError.push('Debe seleccionar la cuenta bancaria de la Transferencia ');
                        continuar = false;
                    }
                    if ($scope.Modelo.ValorPagoTransferencia == undefined || $scope.Modelo.ValorPagoTransferencia == null || $scope.Modelo.ValorPagoTransferencia == "") {
                        $scope.MensajesError.push('Debe ingresar el valor de la Transferencia ');
                        continuar = false;
                    }
                    if ($scope.Modelo.NumeroConsignacion == undefined || $scope.Modelo.NumeroConsignacion == null || $scope.Modelo.NumeroConsignacion == "") {
                        $scope.Modelo.NumeroConsignacion = 0
                    }
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                    if ($scope.Modelo.FechaCheque == undefined || $scope.Modelo.FechaCheque == null || $scope.Modelo.FechaCheque == "") {
                        $scope.MensajesError.push('Debe seleccionar la fecha del cheque ');
                        continuar = false;
                    }
                    if ($scope.Modelo.NumeroCheque == undefined || $scope.Modelo.NumeroCheque == null || $scope.Modelo.NumeroCheque == "") {
                        $scope.MensajesError.push('Debe ingresar el cheque ');
                        continuar = false;
                    }
                    if ($scope.Modelo.CuentaBancariaCheque == undefined || $scope.Modelo.CuentaBancariaCheque == null || $scope.Modelo.CuentaBancariaCheque == "") {
                        $scope.MensajesError.push('Debe sngresar  la cuenta bancaria del cheque ');
                        continuar = false;
                    }
                    if ($scope.Modelo.ValorPagoCheque == undefined || $scope.Modelo.ValorPagoCheque == null || $scope.Modelo.ValorPagoCheque == "") {
                        $scope.MensajesError.push('Debe ingresar el valor del Cheque  ');
                        continuar = false;
                    }
                }
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                    if ($scope.Modelo.Caja == undefined || $scope.Modelo.Caja == null || $scope.Modelo.Caja == "") {
                        $scope.MensajesError.push('Debe ingresar la caja del efectivo');
                        continuar = false;
                    }
                    if ($scope.Modelo.ValorPagoEfectivo == undefined || $scope.Modelo.ValorPagoEfectivo == null || $scope.Modelo.ValorPagoEfectivo == "") {
                        $scope.MensajesError.push('Debe ingresar el valor del efectivo');
                        continuar = false;
                    }

                }
            }

            if ($scope.Modelo.Debito == 0 && $scope.Modelo.Credito == 0 && $scope.Modelo.Diferencia == 0) {
                $scope.MensajesError.push('Debe ingresar minimo un movimiento en el comprobante de egreso');
                continuar = false;
            }
            if ($scope.Modelo.Diferencia !== 0) {
                $scope.MensajesError.push('El comprobante de egreso se encuentra desbalancedado');
                continuar = false;
            }
            if ($scope.Sesion.UsuarioAutenticado.ManejoCentroCostosComprobanteEgreso && $scope.Modelo.CentroCostos.Codigo == 21500 && $scope.Modelo.DocumentoOrigen.Codigo == 2609) {
                $scope.MensajesError.push('Debe seleccionar un Centro de Operaciones');
                continuar = false;
            }
            if (continuar == false) {
                Ventana.scrollTop = 0
            }
            return continuar;
        }

        // Metodo Obtener Comprobantes de egreso  

        if ($scope.Modelo.Numero > 0) {
            $scope.Titulo = 'EDITAR COMPROBANTE EGRESO';
            $scope.Deshabilitar = true;
            $scope.Bloquear = true;
            Obtener();
        }
        $scope.ValidarCajasOficina = true;
        function Obtener() {
            $scope.ValidarCajasOficina = false;
            blockUI.start('Cargando Comprobante de egreso Número ' + $scope.Modelo.Numero);

            $timeout(function () {
                blockUI.message('Cargando Comprobante de egreso  Número ' + $scope.Modelo.Numero);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Numero,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
            };

            blockUI.delay = 1000;
            DocumentoComprobantesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Codigo = response.data.Datos.Codigo
                        $scope.Modelo.Numero = response.data.Datos.Numero;
                        $scope.Modelo.Observaciones = response.data.Datos.Observaciones;
                        $scope.ListadoMovimiento = response.data.Datos.Detalle;
                        if (response.data.Datos.CentroCostos != undefined && response.data.Datos.CentroCostos.Codigo > 0) {
                            $scope.Modelo.CentroCostos = $linq.Enumerable().From($scope.ListaCentrosCostos).First('$.Codigo ==' + response.data.Datos.CentroCostos.Codigo);
                        }
                        var fecha = new Date(response.data.Datos.Fecha);
                        fecha.setHours(0);
                        fecha.setMinutes(0);
                        fecha.setMilliseconds(0);
                        $scope.Modelo.Fecha = fecha;
                        $scope.Modelo.Tercero = response.data.Datos.Tercero;
                        $scope.Modelo.Beneficiario = response.data.Datos.Beneficiario;
                        $scope.Modelo.Placa = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo)
                        //$scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);

                        if (response.data.Datos.Anulado == 1) {
                            $scope.Deshabilitar = true;
                            $scope.ListadoEstados.push({ Nombre: "ANULADO", Codigo: -1 })
                            $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

                        }
                        else {
                            if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                                $scope.Deshabilitar = true;
                            }
                            else if (response.data.Datos.Estado == 2) { //Pendiente aprobacion
                                $scope.ListadoEstados.push({ Nombre: "PENDIENTE APROBACIÓN", Codigo: 2 })
                                $scope.Deshabilitar = true;
                            }
                            else {
                                $scope.Deshabilitar = false;
                            }
                            $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                        }


                        $scope.CodigoDocumentoOrigen = response.data.Datos.DocumentoOrigen.Codigo
                        if ($scope.ListadoDocumentosOrigen.length > 0) {
                            $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == ' + response.data.Datos.DocumentoOrigen.Codigo);
                        }

                        if ($scope.Modelo.DocumentoOrigen != null && $scope.Modelo.DocumentoOrigen != undefined && $scope.Modelo.DocumentoOrigen != '' && $scope.Modelo.DocumentoOrigen.Codigo != 2600 &&
                            $scope.Modelo.Placa != null && $scope.Modelo.Placa != undefined && $scope.Modelo.Placa != '' && $scope.Modelo.Placa.Placa.length > 3) {
                            switch ($scope.Modelo.DocumentoOrigen.Nombre) {
                                case "Liquidación":
                                    CargarLiquidaciones();
                                    break;
                                case "Anticipo":
                                case "Sobreanticipo":
                                    CargarPlanillas();
                                    break;
                            }
                        }
                        $scope.Modelo.NumeroDocumentoOrigen = { Codigo: response.data.Datos.DocumentoOrigen.Codigo, NumeroDocumento: response.data.Datos.NumeroDocumentoOrigen };
                        $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigen).First('$.Codigo == ' + response.data.Datos.DocumentoOrigen.Codigo)

                        $scope.CodigoOficinaDestino = response.data.Datos.OficinaDestino.Codigo;
                        if ($scope.ListadoOficinas.length > 0) {
                            $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + response.data.Datos.OficinaDestino.Codigo);
                        }

                        $scope.valorAlterno = response.data.Datos.ValorAlterno;
                        $scope.Observaciones = response.data.Datos.Observaciones;
                        $scope.CodigoFormaPago = response.data.Datos.FormaPagoDocumento.Codigo;
                        if ($scope.ListaFormaPago.length > 0) {
                            $scope.Modelo.FormaPagoRecaudo = $linq.Enumerable().From($scope.ListaFormaPago).First('$.Codigo == ' + response.data.Datos.FormaPagoDocumento.Codigo);
                        }
                        $scope.CodigoConceptoContable = response.data.Datos.ConceptoContable
                        if ($scope.ListadoConceptoContable.length > 0) {
                            if ($scope.CodigoConceptoContable > 0) {
                                $scope.Modelo.ConceptoContable = $linq.Enumerable().From($scope.ListadoConceptoContable).First('$.Codigo == ' + response.data.Datos.ConceptoContable);
                            }
                        }
                        $scope.ValidarCajasOficina = false;
                        CargarCajas()
                        //Forma Pago Efectivo
                        if (response.data.Datos.FormaPagoDocumento.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                            $scope.ActivarEfectivo = true
                            $scope.ActivarCheque = false
                            $scope.ActivarConsignacion = false
                            $scope.Modelo.ValorPagoEfectivo = response.data.Datos.ValorPagoEfectivo
                            $scope.CodigoCaja = response.data.Datos.Caja.Codigo;
                            if ($scope.ListadoTodaCajaOficinas.length > 0) {
                                $scope.ListadoCajaOficinas = [];
                                $scope.ListadoTodaCajaOficinas.forEach(function (itmOfic) {
                                    if (itmOfic.CodigoOficina == $scope.CodigoOficinaDestino) {
                                        $scope.ListadoCajaOficinas.push(itmOfic);
                                    }
                                });
                                $scope.Caja = $linq.Enumerable().From($scope.ListadoCajaOficinas).First('$.Codigo == ' + response.data.Datos.Caja.Codigo);
                            }
                        }

                        //Forma Pago Cheque
                        if (response.data.Datos.FormaPagoDocumento.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                            CargarCuentasBancarias()
                            $scope.ActivarEfectivo = false
                            $scope.ActivarCheque = true
                            $scope.ActivarConsignacion = false
                            $scope.Modelo.ValorPagoCheque = response.data.Datos.ValorPagocheque
                            $scope.CodigoCuentaBancaria = response.data.Datos.CuentaBancaria.Codigo
                            if ($scope.ListadoCuentaBancarias.length > 0) {
                                $scope.Modelo.CuentaBancariaCheque = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + response.data.Datos.CuentaBancaria.Codigo);
                            }
                            var fechaCheque = new Date(response.data.Datos.FechaPagoRecaudo);
                            fechaCheque.setHours(0);
                            fechaCheque.setMinutes(0);
                            fechaCheque.setMilliseconds(0);

                            $scope.Modelo.FechaCheque = fechaCheque;
                            $scope.Modelo.NumeroCheque = response.data.Datos.Numeracion
                        }
                        //Forma Pago Cosnignacion
                        if (response.data.Datos.FormaPagoDocumento.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                            CargarCuentasBancarias()
                            $scope.ActivarEfectivo = false
                            $scope.ActivarCheque = false
                            $scope.ActivarConsignacion = true
                            $scope.CodigoCuentaBancaria = response.data.Datos.CuentaBancaria.Codigo
                            $scope.Modelo.NumeroConsignacion = response.data.Datos.NumeroPagoRecaudo
                            var fechaConsignacion = new Date(response.data.Datos.FechaPagoRecaudo);
                            fechaConsignacion.setHours(0);
                            fechaConsignacion.setMinutes(0);
                            fechaConsignacion.setMilliseconds(0);
                            $scope.Modelo.FechaConsignacion = fechaConsignacion
                            $scope.Modelo.ValorPagoTransferencia = response.data.Datos.ValorPagoTransferencia
                            if ($scope.ListadoCuentaBancarias.length > 0) {
                                // $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CodigoCuenta == ' + response.data.Datos.CuentaBancaria.Codigo);
                                try { $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaBancarias).First('$.CuentaBancaria.Codigo == ' + response.data.Datos.CuentaBancaria.Codigo); } catch (e) { $scope.Modelo.CuentaBancariaConsignacion = $scope.ListadoCuentaBancarias[0] }
                            }
                        }
                        if (response.data.Datos.FormaPagoDocumento.Codigo == CODIGO_MEDIO_PAGO_CAMBISTA) {
                            $scope.ActivarEfectivo = false
                            $scope.ActivarCheque = false
                            $scope.ActivarCambista = true
                            $scope.CodigoCuentaBancaria = response.data.Datos.CuentaBancaria.Codigo
                            $scope.Modelo.NumeroConsignacion = response.data.Datos.NumeroPagoRecaudo
                            var fechaConsignacion = new Date(response.data.Datos.FechaPagoRecaudo);
                            fechaConsignacion.setHours(0);
                            fechaConsignacion.setMinutes(0);
                            fechaConsignacion.setMilliseconds(0);
                            $scope.Modelo.FechaConsignacion = fechaConsignacion
                            $scope.Modelo.ValorPagoTransferencia = response.data.Datos.ValorPagoTransferencia
                            if ($scope.ListadoCuentaCambistas.length > 0) {
                                $scope.Modelo.CuentaBancariaConsignacion = $linq.Enumerable().From($scope.ListadoCuentaCambistas).First('$.Codigo == ' + response.data.Datos.CuentaBancaria.Codigo);
                            }
                            $scope.ValorPagoTransferenciaCambista = response.data.Datos.ValorPagoTransferencia

                        }
                        $scope.Modelo.Tercero = $scope.CargarTercero($scope.Modelo.Tercero.Codigo)
                        $scope.Modelo.Beneficiario = $scope.CargarTercero($scope.Modelo.Beneficiario.Codigo)
                        $scope.ListaBeneficiariotercero = []
                        $scope.ListaBeneficiariotercero.push($scope.CargarTercero($scope.Modelo.Tercero.Codigo))
                        var cont = 0
                        for (var i = 0; i < $scope.ListaBeneficiariotercero.length; i++) {
                            if ($scope.ListaBeneficiariotercero[i].Codigo == $scope.Modelo.Beneficiario.Codigo) {
                                cont++
                                break
                            }
                        }
                        if (cont == 0) {
                            $scope.ListaBeneficiariotercero.push($scope.CargarTercero($scope.Modelo.Beneficiario.Codigo))
                        }
                        $scope.Modelo.Beneficiario = $linq.Enumerable().From($scope.ListaBeneficiariotercero).First('$.Codigo ==' + $scope.Modelo.Beneficiario.Codigo);
                        CalcularMovimiento()
                        $scope.MaskValores()

                        if ($scope.Sesion.UsuarioAutenticado.ProcesoSugerirAnticipoComprobanteEgreso) {
                            contProcesosValidarPlanilla++;

                            var FiltroPlanilla = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Estado: { Codigo: ESTADO_ACTIVO },
                                Numero: $scope.Modelo.NumeroDocumentoOrigen.NumeroDocumento,
                                TipoDocumento: $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA ? CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA : CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                                Sync: true
                            };

                            var ResponsePlanilla = PlanillaDespachosFactory.Consultar(FiltroPlanilla).Datos;
                            if (ResponsePlanilla != undefined && ResponsePlanilla.length > 0) {
                                var ResponseObtPlanilla = PlanillaDespachosFactory.Obtener({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Numero: ResponsePlanilla[0].Numero,
                                    TipoDocumento: $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA ? 135 : 150,
                                    Sync: true
                                }).Datos;
                                if (ResponseObtPlanilla != undefined) {
                                    if (ResponseObtPlanilla.Numero > 0) {
                                        $scope.Modelo.NumeroDocumentoOrigen = ResponseObtPlanilla
                                    }
                                }
                            }


                            $scope.ValidarDocumentoPlanilla();
                            if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                                $scope.ComprobanteDefinitivo = true;
                            }
                        }

                    }
                    else {
                        ShowError('No se logro consultar el Comprobante de egreso No.' + $scope.Modelo.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarComprobanteEgresos';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    //ShowError('No se logro consultar el Comprobante de egreso No.' + $scope.Modelo.Numero + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!ConsultarComprobanteEgresos';
                });

            blockUI.stop();
        };
        //--Asginar AutoComplete Numero Documento Origen
        $scope.AsignarListaNumeroDocumento = function () {

            if ($scope.Modelo.DocumentoOrigen != null && $scope.Modelo.DocumentoOrigen != undefined && $scope.Modelo.DocumentoOrigen != '' && $scope.Modelo.DocumentoOrigen.Codigo != 2600 &&
                $scope.Modelo.Placa != null && $scope.Modelo.Placa != undefined && $scope.Modelo.Placa != '' && $scope.Modelo.Placa.Placa.length > 3) {
                //switch ($scope.Modelo.DocumentoOrigen.Nombre) {
                //    case "Liquidación":
                //        CargarLiquidaciones();
                //        break;
                //    case "Anticipo":
                //    case "Sobreanticipo":
                //        CargarPlanillas();
                //        break;

                //}
                if ($scope.Modelo.DocumentoOrigen.Codigo == 2602) {
                    CargarLiquidaciones();
                } else if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_SOBREANTICIPO || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA) {
                    CargarPlanillas();
                }
            }
            if (/*$scope.Modelo.DocumentoOrigen.Codigo == 2604 ||*/ $scope.Modelo.DocumentoOrigen.Codigo == 2605) {
                $scope.Modelo.Tercero = $scope.ListaBeneficiariotercero[0]
                $scope.Modelo.Beneficiario = $scope.ListaBeneficiariotercero[0]
            }
        }

        var ListacomboConceptos = ConceptoGastosFactory.Consultar({ Estado: 1, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ConceptoSistema: ESTADO_ACTIVO, Sync: true });
        if (ListacomboConceptos.ProcesoExitoso == true) {
            if (ListacomboConceptos.Datos.length > 0) {
                $scope.ListadoConceptos = ListacomboConceptos.Datos;
            }
        }

        $scope.ValidarDocumentoPlanilla = function () {
            if ($scope.Sesion.UsuarioAutenticado.ProcesoSugerirAnticipoComprobanteEgreso) {

                if ($scope.Modelo.DocumentoOrigen != undefined) {
                    if (($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO) && $scope.Modelo.NumeroDocumentoOrigen.NumeroDocumento > 0) {
                        var NumeroInternoPlanilla = 0;
                        var ResponseObtenerPlanilla = '';
                        $scope.ListadoGastosRuta = [];
                        //$scope.ListadoPlanillas.forEach(item => {
                        //if ($scope.Modelo.NumeroDocumentoOrigen.NumeroDocumento == item.NumeroDocumento) {
                        ResponseObtenerPlanilla = PlanillaDespachosFactory.Obtener({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Numero: $scope.Modelo.NumeroDocumentoOrigen.Numero,
                            TipoDocumento: $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA ? 135 : 150,
                            Sync: true
                        });

                        if (ResponseObtenerPlanilla != '' && ResponseObtenerPlanilla != undefined) {
                            var responseVehiculo = VehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: ResponseObtenerPlanilla.Datos.Vehiculo.Codigo, Sync: true });
                            $scope.RutaPlanillaAnticipo = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.NumeroDocumentoOrigen.Ruta.Codigo, ConsultaLegalizacionGastosRuta: true, ConsultarPeajes: true, Sync: true }).Datos;
                            $scope.ValorFleteTransportadorPlanillaAnticipo = MascaraValores(ResponseObtenerPlanilla.Datos.ValorFleteTransportador);
                            $scope.ManifiestoPlanillaAnticipo = $scope.Modelo.NumeroDocumentoOrigen.Manifiesto.NumeroDocumento;

                            if ($scope.RutaPlanillaAnticipo.LegalizacionGastosRuta !== undefined) {
                                for (var i = 0; i < $scope.RutaPlanillaAnticipo.LegalizacionGastosRuta.length; i++) {
                                    if ($scope.RutaPlanillaAnticipo.LegalizacionGastosRuta[i].AplicaAnticipo == ESTADO_ACTIVO) {
                                        if ($scope.RutaPlanillaAnticipo.LegalizacionGastosRuta[i].Concepto.Codigo == CODIGO_GASTO_COMBUSTIBLE_RENDIMIENTO_GALON && $scope.Modelo.Placa.TipoVehiculo.Codigo == $scope.RutaPlanillaAnticipo.LegalizacionGastosRuta[i].TipoVehiculo.Codigo) {
                                            //Calcular el concepto combustible según el tipo de combustible del vehículo:


                                            var ResponseUltimoValorCombustible = ValorCombustibleFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoCombustible: { Codigo: responseVehiculo.Datos.TipoCombustible.Codigo }, Sync: true }).Datos;
                                            var ResponseRendimientoGalon = RendimientoGalonCombustibleFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Ruta: { Codigo: $scope.RutaPlanillaAnticipo.Codigo }, TipoVehiculo: { Codigo: responseVehiculo.Datos.TipoVehiculo.Codigo }, Estado: ESTADO_ACTIVO, Sync: true });

                                            if (ResponseRendimientoGalon != undefined) {
                                                if (ResponseRendimientoGalon.Datos.length > 0) {
                                                    var ValorRendimientoGalon = ResponseRendimientoGalon.Datos[0].RendimientoGalon == undefined ? 0 : ResponseRendimientoGalon.Datos[0].RendimientoGalon;
                                                    var mult = parseFloat($scope.RutaPlanillaAnticipo.Kilometros / ValorRendimientoGalon).toFixed(2)
                                                    var ValorConceptoCombustible = parseInt(mult * parseFloat(ResponseUltimoValorCombustible.Valor));
                                                    if (ResponseUltimoValorCombustible != undefined && ResponseUltimoValorCombustible != null) {
                                                        var item = { Concepto: $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + $scope.RutaPlanillaAnticipo.LegalizacionGastosRuta[i].Concepto.Codigo), Valor: MascaraValores(ValorConceptoCombustible) };
                                                        $scope.ListadoGastosRuta.push(item);
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($scope.Modelo.Placa.TipoVehiculo.Codigo == $scope.RutaPlanillaAnticipo.LegalizacionGastosRuta[i].TipoVehiculo.Codigo) {
                                                var concepto = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + $scope.RutaPlanillaAnticipo.LegalizacionGastosRuta[i].Concepto.Codigo)
                                                $scope.ListadoGastosRuta.push({ Concepto: concepto, Valor: MascaraValores($scope.RutaPlanillaAnticipo.LegalizacionGastosRuta[i].Valor) })
                                            }
                                        }
                                    }


                                }
                            }



                            // Añadir a los conceptos el valor total de los peajes:

                            //Obtener Tipo de Vehículo y Número total de Ejes del Semirremolque de la Planilla:

                            $scope.Modelo.Placa = responseVehiculo.Datos;
                            var responseSemirremolque = {};
                            var TotalEjes = 0;
                            if (responseVehiculo.ProcesoExitoso == true) {

                                if (ResponseObtenerPlanilla.Datos.Semirremolque.Codigo != 0) {
                                    responseSemirremolque = SemirremolquesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: ResponseObtenerPlanilla.Datos.Semirremolque.Codigo, Sync: true });
                                    if (responseSemirremolque.ProcesoExitoso == true) {

                                        // ejes del Semirremolque:
                                        TotalEjes = responseSemirremolque.Datos.NumeroEjes;
                                    }
                                }

                                //Consultar los peajes de la ruta de la planilla:


                                var ListaTarifasPeajesRuta = [];
                                var PeajesRutasSize = $scope.RutaPlanillaAnticipo.PeajeRutas.length;

                                $scope.ListadoPeajes = [];

                                //Por cada peaje, consultar las tarifas parametrizadas:
                                for (let i = 0; i < $scope.RutaPlanillaAnticipo.PeajeRutas.length; i++) {
                                    if ($scope.RutaPlanillaAnticipo.PeajeRutas[i].Peaje.Estado == ESTADO_ACTIVO) {
                                        let NombrePeaje = $scope.RutaPlanillaAnticipo.PeajeRutas[i].Peaje.Nombre;
                                        var ResponsePeajesRuta = PeajesFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.RutaPlanillaAnticipo.PeajeRutas[i].Peaje.Codigo, ConsultarTarifasPeajes: true, Sync: true }).Datos;
                                        if (ResponsePeajesRuta != undefined && ResponsePeajesRuta != null) {


                                            //Comparar si el tipo de vehículo de la planilla, coincide con las tarifas parametrizadas en el peaje:
                                            for (let j = 0; j < ResponsePeajesRuta.TarifasPeajes.length; j++) {
                                                if (ResponsePeajesRuta.TarifasPeajes[j].TipoVehiculo.Codigo == responseVehiculo.Datos.TipoVehiculo.Codigo) {
                                                    var tmpConcepto = ConceptoGastosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_GASTO_LEGALIZACION_PEAJES, Sync: true }).Datos;
                                                    tmpConcepto.Nombre = tmpConcepto.Nombre + '(' + NombrePeaje + ')'
                                                    // si coincide, verificar primero que la planilla no tenga semirremolque:
                                                    if (TotalEjes > 0) {
                                                        //si lo tiene agregar el valor a los conceptos segun el número de ejes del semirremolque:                                                           

                                                        if (TotalEjes == 1) {
                                                            $scope.ListadoGastosRuta.push({ Concepto: tmpConcepto, Valor: MascaraValores(ResponsePeajesRuta.TarifasPeajes[j].ValorRemolque1Eje) });
                                                        } else if (TotalEjes == 2) {
                                                            $scope.ListadoGastosRuta.push({ Concepto: tmpConcepto, Valor: MascaraValores(ResponsePeajesRuta.TarifasPeajes[j].ValorRemolque2Ejes) });
                                                        } else if (TotalEjes == 3) {
                                                            $scope.ListadoGastosRuta.push({ Concepto: tmpConcepto, Valor: MascaraValores(ResponsePeajesRuta.TarifasPeajes[j].ValorRemolque3Ejes) });
                                                        } else if (TotalEjes >= 4) {
                                                            $scope.ListadoGastosRuta.push({ Concepto: tmpConcepto, Valor: MascaraValores(ResponsePeajesRuta.TarifasPeajes[j].ValorRemolque4Ejes) });
                                                        }


                                                    } else {
                                                        //si no lo tiene, agregar el valor parametrizado por defecto:                                                            
                                                        $scope.ListadoGastosRuta.push({ Concepto: tmpConcepto, Valor: MascaraValores(ResponsePeajesRuta.TarifasPeajes[j].Valor) });
                                                    }
                                                }

                                            }


                                        }

                                    }

                                }
                                //Fin Conceptos de Peajes//

                            }








                        }
                        //}
                        //});
                        var TotalGastos = 0
                        var TotalGastosSinPeajes = 0
                        var TotalGastosPeajes = 0
                        $scope.ListadoGastosRuta.forEach(item => {
                            if (item.Concepto.Codigo == CODIGO_GASTO_COMBUSTIBLE_RENDIMIENTO_GALON) {
                                TotalGastos += parseInt(MascaraNumero(item.Valor))
                                TotalGastosSinPeajes += parseInt(MascaraNumero(item.Valor))
                            } else {
                                TotalGastos += MascaraNumero(item.Valor);
                                if (item.Concepto.Codigo == CODIGO_GASTO_LEGALIZACION_PEAJES) {
                                    TotalGastosPeajes += MascaraNumero(item.Valor);
                                } else {
                                    TotalGastosSinPeajes += MascaraNumero(item.Valor);
                                }
                            }
                        });
                        //if (contProcesosValidarPlanilla == 1) {
                        //    $scope.ValorAnticipoPlanillaAnticipo = MascaraValores(ResponseObtenerPlanilla.Datos.ValorAnticipo)
                        //} else {
                        $scope.ValorAnticipoPlanillaAnticipo = MascaraValores(TotalGastos);
                        $scope.ValorGastosSinPeajes = MascaraValores(TotalGastosSinPeajes)
                        $scope.ValorGastosPeajes = MascaraValores(TotalGastosPeajes)
                        //}
                        $scope.CalcularAnticipo();
                        if ($scope.Sesion.UsuarioAutenticado.CodigoEmpresa != 13) {
                            $scope.SeccionAnticipoPaqueteria = true;
                        }
                        $scope.InhabilitarValorFormaPago = true;
                    } else {
                        $scope.SeccionAnticipoPaqueteria = false;
                        $scope.InhabilitarValorFormaPago = false;
                    }
                }

            }
        }

        $scope.ValidarAnticipoSugerido = function (valorPago) {
            if ($scope.Sesion.UsuarioAutenticado.ProcesoSugerirAnticipoComprobanteEgreso && ($scope.Modelo.DocumentoOrigen.Codigo == 2604 || $scope.Modelo.DocumentoOrigen.Codigo == 2613)) {
                if (MascaraNumero(valorPago) > MascaraNumero($scope.ValorAnticipoPlanillaAnticipo)) {
                    ShowError('El valor ingresado es mayor al anticipo autorizado');
                    $scope.Modelo.ValorPagoEfectivo = ''
                    $scope.Modelo.ValorPagoCheque = ''
                    $scope.Modelo.ValorPagoTransferencia = ''
                }
            }
        }

        $scope.CalcularAnticipo = function () {
            if ($scope.Sesion.UsuarioAutenticado.ProcesoSugerirAnticipoComprobanteEgreso) {
                var TotalGastos = 0
                var TotalGastosSinPeajes = 0
                var TotalGastosPeajes = 0
                $scope.ListadoGastosRuta.forEach(item => {
                    if (item.Concepto.Codigo == CODIGO_GASTO_COMBUSTIBLE_RENDIMIENTO_GALON) {
                        if ($scope.AplicaCombustiblePlanillaAnticipo) {
                            TotalGastos += parseInt(MascaraNumero(item.Valor));
                            TotalGastosSinPeajes += parseInt(MascaraNumero(item.Valor))
                        }
                    } else if (item.Concepto.Codigo == CODIGO_GASTO_LEGALIZACION_PEAJES) {
                        if ($scope.AplicaPeajesPlanillaAnticipo) {
                            TotalGastos += MascaraNumero(item.Valor);
                        }

                        TotalGastosPeajes += MascaraNumero(item.Valor);



                    } else {
                        TotalGastos += MascaraNumero(item.Valor);
                        TotalGastosSinPeajes += MascaraNumero(item.Valor);
                    }

                });
                $scope.ValorAnticipoPlanillaAnticipo = MascaraValores(TotalGastos);
                $scope.ValorGastosSinPeajes = MascaraValores(TotalGastosSinPeajes)
                $scope.ValorGastosPeajes = MascaraValores(TotalGastosPeajes)
                if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_EFECTIVO) {
                    $scope.Modelo.ValorPagoEfectivo = $scope.ValorAnticipoPlanillaAnticipo

                } else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_TRANSFERENCIA) {
                    $scope.Modelo.ValorPagoTransferencia = $scope.ValorAnticipoPlanillaAnticipo


                } else if ($scope.Modelo.FormaPagoRecaudo.Codigo == CODIGO_MEDIO_PAGO_CHEQUE) {
                    $scope.Modelo.ValorPagoCheque = $scope.ValorAnticipoPlanillaAnticipo

                }
            }
        }

        $scope.ValidarDocumento = function () {
            if ($scope.Modelo.NumeroDocumentoOrigen != undefined && $scope.Modelo.NumeroDocumentoOrigen != null && $scope.Modelo.NumeroDocumentoOrigen != '') {
                var filtrosComprobante = {
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,

                    Estado: -1,
                    NombreBeneficiario: $scope.Beneficiario,
                    NombreTercero: $scope.Tercero,
                    NumeroDocumentoOrigen: $scope.Modelo.NumeroDocumentoOrigen.NumeroDocumento,
                    DocumentoOrigen: { Codigo: $scope.Modelo.DocumentoOrigen.Codigo },
                    Sync: true
                };
                var ResponseComprobanteExistente = DocumentoComprobantesFactory.Consultar(filtrosComprobante).Datos

                if (ResponseComprobanteExistente != undefined) {

                    if (ResponseComprobanteExistente.length == 0) {
                        // Planilla:
                        if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO) {

                            var FiltroPlanilla = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Estado: { Codigo: ESTADO_ACTIVO },
                                Numero: $scope.Modelo.NumeroDocumentoOrigen.NumeroDocumento,
                                TipoDocumento: $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA ? CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA : CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                                Sync: true
                            };

                            var ResponsePlanilla = PlanillaDespachosFactory.Consultar(FiltroPlanilla).Datos;
                            if (ResponsePlanilla != undefined && ResponsePlanilla.length > 0) {
                                var ResponseObtPlanilla = PlanillaDespachosFactory.Obtener({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Numero: ResponsePlanilla[0].Numero,
                                    TipoDocumento: $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA ? 135 : 150,
                                    Sync: true
                                }).Datos;
                                if (ResponseObtPlanilla != undefined) {
                                    if (ResponseObtPlanilla.Numero > 0) {
                                        $scope.Modelo.NumeroDocumentoOrigen = ResponseObtPlanilla
                                        $scope.Modelo.Placa = $scope.CargarVehiculos(ResponseObtPlanilla.Vehiculo.Codigo);
                                        $scope.Modelo.Tercero = $scope.CargarTercero(ResponseObtPlanilla.Conductor.Codigo);
                                        $scope.Modelo.Beneficiario = $scope.Modelo.Tercero
                                        $scope.ValidarDocumentoPlanilla();
                                    }
                                }
                            } else {
                                $scope.Modelo.NumeroDocumentoOrigen = { NumeroDocumento: '' }
                                $scope.Modelo.Placa = ''
                                $scope.Modelo.Tercero = ''
                                $scope.Modelo.Beneficiario = ''
                                ShowError('El número ingresado no es válido')
                            }
                        }
                        //Liquidaciones:
                        if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION_PAQUETERIA || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION) {
                            // en los filtros, el tipo documento debe ser el de la planilla
                            var FiltroLiquidacion = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Estado: ESTADO_DEFINITIVO,
                                Aprobado: -1,
                                NumeroDocumento: $scope.Modelo.NumeroDocumentoOrigen.NumeroDocumento,
                                TipoDocumento: { Codigo: $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION_PAQUETERIA ? CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA : CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO },
                                Sync: true
                            };

                            var ResponseLiquidacion = LiquidacionesFactory.Consultar(FiltroLiquidacion).Datos;
                            if (ResponseLiquidacion != undefined && ResponseLiquidacion.length > 0) {

                                $scope.Modelo.NumeroDocumentoOrigen = ResponseLiquidacion[0]
                                $scope.Modelo.Placa = $scope.CargarVehiculosPlaca(ResponseLiquidacion[0].PlacaVehiculo);
                                $scope.Modelo.Tercero = $scope.CargarTercero(ResponseLiquidacion[0].Tenedor.Codigo);
                                $scope.Modelo.Beneficiario = $scope.Modelo.Tercero

                            } else {
                                $scope.Modelo.NumeroDocumentoOrigen = { NumeroDocumento: '' }
                                $scope.Modelo.Placa = ''
                                $scope.Modelo.Tercero = ''
                                $scope.Modelo.Beneficiario = ''
                                ShowError('El número ingresado no es válido')
                            }
                        } else if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION_PAQUETERIA || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION) {

                            var FiltroLiquidacion = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Estado: ESTADO_DEFINITIVO,
                                Aprobado: -1,
                                NumeroDocumento: $scope.Modelo.NumeroDocumentoOrigen.NumeroDocumento,
                                TipoDocumento: { Codigo: $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION_PAQUETERIA ? CODIGO_TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHO_PAQUETERIA : CODIGO_TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHO },
                                Sync: true
                            };

                            var ResponseLiquidacion = LiquidacionesFactory.Consultar(FiltroLiquidacion).Datos;
                            if (ResponseLiquidacion != undefined && ResponseLiquidacion.length > 0) {

                                $scope.Modelo.NumeroDocumentoOrigen = ResponseLiquidacion[0]
                                $scope.Modelo.Placa = $scope.CargarVehiculosPlaca(ResponseLiquidacion[0].PlacaVehiculo);
                                $scope.Modelo.Tercero = $scope.CargarTercero(ResponseLiquidacion[0].Conductor.Codigo);
                                $scope.Modelo.Beneficiario = $scope.Modelo.Tercero

                            } else {
                                $scope.Modelo.NumeroDocumentoOrigen = { NumeroDocumento: '' }
                                $scope.Modelo.Placa = ''
                                $scope.Modelo.Tercero = ''
                                $scope.Modelo.Beneficiario = ''
                                ShowError('El número ingresado no es válido')
                            }
                        } else if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION_PROVEEDORES) {

                            var FiltroLiquidacion = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroDocumento: $scope.Modelo.NumeroDocumentoOrigen.NumeroDocumento,
                                Estado: ESTADO_DEFINITIVO,
                                Sync: true
                            };

                            var ResponseLiquidacion = LiquidacionDespachosProveedoresFactory.Consultar(FiltroLiquidacion).Datos
                            if (ResponseLiquidacion != undefined && ResponseLiquidacion.length > 0) {

                                $scope.Modelo.NumeroDocumentoOrigen = ResponseLiquidacion[0]
                                $scope.Modelo.Tercero = $scope.CargarTercero(ResponseLiquidacion[0].Transportador.Codigo);
                                $scope.Modelo.Beneficiario = $scope.Modelo.Tercero

                            } else {
                                $scope.Modelo.NumeroDocumentoOrigen = { NumeroDocumento: '' }
                                $scope.Modelo.Placa = ''
                                $scope.Modelo.Tercero = ''
                                $scope.Modelo.Beneficiario = ''
                                ShowError('El número ingresado no es válido')
                            }
                        } else if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_PAQUETERIA || $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_MASIVO) {
                            var FiltroLegalizacion = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroDocumento: $scope.Modelo.NumeroDocumentoOrigen.NumeroDocumento,
                                Estado: ESTADO_DEFINITIVO,
                                TipoDocumento: $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_LEGALIZACION_PAQUETERIA ? CODIGO_TIPO_DOCUMENTO_LEGALIZACION_GASTOS_PAQUETERIA : CODIGO_TIPO_DOCUMENTO_LEGALIZACION_GASTOS,
                                Aprobado: -1,
                                Sync: true
                            };

                            var ResponseLegalizacion = LegalizacionGastosFactory.Consultar(FiltroLegalizacion).Datos;
                            if (ResponseLegalizacion != undefined && ResponseLegalizacion.length > 0) {

                                $scope.Modelo.NumeroDocumentoOrigen = ResponseLegalizacion[0]
                                $scope.Modelo.Placa = $scope.CargarVehiculos(ResponseLegalizacion[0].Planilla.Vehiculo.Codigo);
                                $scope.Modelo.Tercero = $scope.CargarTercero(ResponseLegalizacion[0].Conductor.Codigo);
                                $scope.Modelo.Beneficiario = $scope.Modelo.Tercero

                            } else {
                                $scope.Modelo.NumeroDocumentoOrigen = { NumeroDocumento: '' }
                                $scope.Modelo.Placa = ''
                                $scope.Modelo.Tercero = ''
                                $scope.Modelo.Beneficiario = ''
                                ShowError('El número ingresado no es válido')
                            }



                        } else if ($scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_SOBREANTICIPO) {
                            var FiltroPlanilla = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Estado: { Codigo: ESTADO_ACTIVO },
                                Numero: $scope.Modelo.NumeroDocumentoOrigen.NumeroDocumento,
                                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                                Sync: true
                            };

                            var ResponsePlanilla = PlanillaDespachosFactory.Consultar(FiltroPlanilla).Datos;
                            if (ResponsePlanilla != undefined && ResponsePlanilla.length > 0) {
                                var ResponseObtPlanilla = PlanillaDespachosFactory.Obtener({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Numero: ResponsePlanilla[0].Numero,
                                    TipoDocumento: 150,
                                    Sync: true
                                }).Datos;
                                if (ResponseObtPlanilla != undefined) {
                                    if (ResponseObtPlanilla.Numero > 0) {
                                        $scope.Modelo.NumeroDocumentoOrigen = ResponseObtPlanilla
                                        $scope.Modelo.Placa = $scope.CargarVehiculos(ResponseObtPlanilla.Vehiculo.Codigo);
                                        $scope.Modelo.Tercero = $scope.CargarTercero(ResponseObtPlanilla.Conductor.Codigo);
                                        $scope.Modelo.Beneficiario = $scope.Modelo.Tercero
                                        //$scope.ValidarDocumentoPlanilla();
                                    }
                                }
                            } else if (ResponsePlanilla == undefined || ResponsePlanilla.length == 0) {
                                var FiltroPlanilla = {
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Estado: { Codigo: ESTADO_ACTIVO },
                                    Numero: $scope.Modelo.NumeroDocumentoOrigen.NumeroDocumento,
                                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                                    Sync: true
                                };

                                var ResponsePlanilla = PlanillaDespachosFactory.Consultar(FiltroPlanilla).Datos;
                                if (ResponsePlanilla != undefined && ResponsePlanilla.length > 0) {
                                    var ResponseObtPlanilla = PlanillaDespachosFactory.Obtener({
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        Numero: ResponsePlanilla[0].Numero,
                                        TipoDocumento: 135,
                                        Sync: true
                                    }).Datos;
                                    if (ResponseObtPlanilla != undefined) {
                                        if (ResponseObtPlanilla.Numero > 0) {
                                            $scope.Modelo.NumeroDocumentoOrigen = ResponseObtPlanilla
                                            $scope.Modelo.Placa = $scope.CargarVehiculos(ResponseObtPlanilla.Vehiculo.Codigo);
                                            $scope.Modelo.Tercero = $scope.CargarTercero(ResponseObtPlanilla.Conductor.Codigo);
                                            $scope.Modelo.Beneficiario = $scope.Modelo.Tercero
                                            //$scope.ValidarDocumentoPlanilla();
                                        }
                                    }
                                }
                                else {
                                    $scope.Modelo.NumeroDocumentoOrigen = { NumeroDocumento: '' }
                                    $scope.Modelo.Placa = ''
                                    $scope.Modelo.Tercero = ''
                                    $scope.Modelo.Beneficiario = ''
                                    ShowError('El número ingresado no es válido')
                                }
                            } else {
                                $scope.Modelo.NumeroDocumentoOrigen = { NumeroDocumento: '' }
                                $scope.Modelo.Placa = ''
                                $scope.Modelo.Tercero = ''
                                $scope.Modelo.Beneficiario = ''
                                ShowError('El número ingresado no es válido')
                            }

                        }
                    } else {
                        $scope.Modelo.NumeroDocumentoOrigen = { NumeroDocumento: '' }
                        $scope.Modelo.Placa = ''
                        $scope.Modelo.Tercero = ''
                        $scope.Modelo.Beneficiario = ''
                        ShowError('Ya existe un comprobante para este documento')
                    }
                } else {
                    $scope.Modelo.NumeroDocumentoOrigen = { NumeroDocumento: '' }
                    $scope.Modelo.Placa = ''
                    $scope.Modelo.Tercero = ''
                    $scope.Modelo.Beneficiario = ''
                    ShowError('Este número documento origen no es válido')
                }
            }
        }

        function CargarPlanillas() {
            $scope.ListadoNumeroDocumentosOrigen = [];
            var FiltroPlanilla = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Vehiculo: { Placa: $scope.Modelo.Placa.Placa },
                Estado: { Codigo: 1 },
                TipoDocumento: $scope.Modelo.DocumentoOrigen.Codigo == CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA ? CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA : CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
            };
            blockUI.delay = 1000;
            PlanillaDespachosFactory.Consultar(FiltroPlanilla).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.ListadoPlanillas = response.data.Datos
                                $scope.ListadoNumeroDocumentosOrigen.push({
                                    Codigo: response.data.Datos[i].NumeroDocumento,
                                    NumeroDocumento: response.data.Datos[i].NumeroDocumento,
                                    ValorAnticipo: response.data.Datos[i].ValorAnticipo,
                                    ValorFleteTransportador: response.data.Datos[i].ValorFleteTransportador

                                });
                            }
                        }
                    }
                }, function (response) {
                });
        }

        function CargarLiquidaciones() {
            $scope.ListadoNumeroDocumentosOrigen = [];
            var FiltroLiquidacion = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                PlacaVehiculo: $scope.Modelo.Placa.Placa,
                Estado: 1,
                Aprobado: -1,
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO }
            };
            LiquidacionesFactory.Consultar(FiltroLiquidacion).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.ListadoNumeroDocumentosOrigen.push({
                                    Codigo: response.data.Datos[i].NumeroDocumento,
                                    NumeroDocumento: response.data.Datos[i].NumeroDocumento
                                });
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        //--Asginar AutoComplete Numero Documento Origen
        $scope.VolverMaster = function () {
            if ($routeParams.Numero > 0) {
                document.location.href = '#!ConsultarComprobanteEgresos/' + $scope.Modelo.Numero;
            } else {
                document.location.href = '#!ConsultarComprobanteEgresos';
            }
        };

        $scope.MaskMayus = function () {
            try { $scope.Descripcion = $scope.Descripcion.toUpperCase() } catch (e) { };
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskPlaca = function () {
            try { $scope.Vehiculo.Placa = MascaraPlaca($scope.Vehiculo.Placa) } catch (e) { };
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskNumeroObj = function (Numero) {
            return MascaraNumero(Numero)
        }

    }]);