﻿SofttoolsApp.controller("GestionarListaChequeoCargueDescarguesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'TipoDocumentosFactory', 'ItemListCargueDescarguesFactory', 'CiudadesFactory', 'VehiculosFactory', 'TercerosFactory', 'RemesasFactory', 'EncabezadoCheckListCargueDescarguesFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, TipoDocumentosFactory, ItemListCargueDescarguesFactory, CiudadesFactory, VehiculosFactory, TercerosFactory, RemesasFactory, EncabezadoCheckListCargueDescarguesFactory) {

        $scope.Titulo = 'GESTIONAR LISTA CHEQUEO CARGUE Y DESCARGUE';
        $scope.MapaSitio = [{ Nombre: 'Gesphone' }, { Nombre: 'Lista Chequeo' }, { Nombre: 'Cargue y Descargue' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CHECK_CARGUE_DESCARGUES); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Nombre: '',
            TipoDocumento: CODIGO_TIPO_DOCUMENTO_CARGUE,
            Estado: 0,
            Numero: 0,
            Fecha: new Date(),

        }
        $scope.ListadoTipoDocumentos = [];
        $scope.ListadoItemCargueDescargue = [];
        $scope.ListadoCiudades = [];
        $scope.ListadoVehiculos = [];

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "BORRADOR" },
            { Codigo: 1, Nombre: "DEFINITIVO" },
        ]

        /* Obtener parametros*/
        if ($routeParams.Numero > 0) {
            $scope.Modelo.Numero = $routeParams.Numero;
            Obtener();
        }
        else {
            $scope.Modelo.Numero = 0;
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 0');
        }

        TipoDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoDocumentos = [];
                        response.data.Datos.forEach(function (item) {
                            if (item.Codigo == CODIGO_TIPO_DOCUMENTO_REMESAS || item.Codigo == CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA) {
                                $scope.ListadoTipoDocumentos.push(item)
                            }
                        });

                        if ($scope.CodigoTipoDocumento !== undefined && $scope.CodigoTipoDocumento !== '' && $scope.CodigoTipoDocumento !== null) {
                            $scope.Modelo.TipoDocumentoSoporte = $linq.Enumerable().From($scope.ListadoTipoDocumentos).First('$.Codigo ==' + $scope.CodigoTipoDocumento)
                            $scope.ConsultarDocumento();
                        }
                        else {
                            $scope.Modelo.TipoDocumentoSoporte = $scope.ListadoTipoDocumentos[0];
                        }
                    }
                    else {
                        $scope.ListadoTipoDocumentos = [];
                    }
                }
            }, function (response) {
            });

        ItemListCargueDescarguesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, CodigoChecklist: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoItemCargueDescargue = [];
                        $scope.ListadoItemCargueDescargue = response.data.Datos
                    }
                    else {
                        $scope.ListadoItemCargueDescargue = [];
                    }
                }
            }, function (response) {
            });

        /*Cargar Autocomplete de Ciudades*/
        CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListadoCiudades = response.data.Datos;

                        if ($scope.Ciudad > 0) {
                            $scope.Modelo.CiudadCargue = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + $scope.Ciudad);
                        } else {
                            $scope.Modelo.CiudadCargue = $scope.ListadoCiudades[$scope.ListadoCiudades.length - 1];
                        }
                    }
                    else {
                        $scope.ListadoCiudades = [];
                        $scope.Modelo.CiudadCargue = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*cargar AutoComplete de la placa */
        VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoVehiculos = response.data.Datos;

                        if ($scope.CodigoPlaca !== undefined && $scope.CodigoPlaca !== '' && $scope.CodigoPlaca !== null) {
                            if ($scope.CodigoPlaca > 0) {
                                $scope.Modelo.Placa = $linq.Enumerable().From($scope.ListadoVehiculos).First('$.Codigo == ' + $scope.CodigoPlaca);
                                $scope.AsignarDatos($scope.Modelo.Placa)
                            }
                        }
                    }
                    else {
                        $scope.ListadoVehiculos = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.ValidarCliente = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modelo.Cliente = response.data.Datos
                            }
                            else {
                                $scope.Modelo.Cliente = undefined
                                ShowError('La identificación Ingresada no es válida o el cliente no se encuentra registrado')
                            }
                        }
                    });
            }
        };

        $scope.ValidarConductor = function (identificacion) {
            if (identificacion !== undefined && identificacion !== null && identificacion !== '' && identificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: identificacion,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modelo.Vehiculo.Conductor = response.data.Datos
                            }
                            else {
                                $scope.Modelo.Vehiculo.Conductor = undefined
                                ShowError('La identificación Ingresada no es válida o el conductor no se encuentra registrado')
                            }
                        }
                    });
            }
        };
        /*cargar AutoComplete de la placa */
        $scope.ConsultarDocumento = function () {
            if ($scope.Modelo.NumeroDocumentoSoporte !== undefined && $scope.Modelo.NumeroDocumentoSoporte !== null && $scope.Modelo.NumeroDocumentoSoporte !== '') {
                RemesasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, TipoRemesa: $scope.Modelo.TipoDocumentoSoporte, NumeroDocumento: $scope.Modelo.NumeroDocumentoSoporte }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > CERO) {
                                $scope.DatosDocumento = response.data.Datos[0];
                                if ($scope.DatosDocumento.Numero > CERO) {
                                    $scope.Modelo.FechaSolicitudOperacion = $scope.DatosDocumento.FechaDocumentoCliente
                                    $scope.Modelo.Cliente = $scope.DatosDocumento.Cliente
                                }
                                else {
                                    ShowError('El documento ' + $scope.Modelo.TipoDocumentoSoporte.Nombre + ' con número ' + $scope.Modelo.NumeroDocumentoSoporte + 'no se encuentra registrado en el sistema')
                                }
                            }
                            else {
                                ShowError('El documento ' + $scope.Modelo.TipoDocumentoSoporte.Nombre + ' con número ' + $scope.Modelo.NumeroDocumentoSoporte + 'no se encuentra registrado en el sistema')
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        $scope.AsignarDatos = function (Vehiculo) {
            if (Vehiculo !== undefined && Vehiculo !== null && Vehiculo !== '' && Vehiculo.Codigo > CERO) {
                $scope.Modelo.Vehiculo = Vehiculo
            }
        }

        $scope.CalcularDiferenciaHoras = function (HoraInicio, Horafin) {
            var MILISENGUNDOS_POR_HORA = 1000 * 60 * 60
            var Fecha = new Date()
            HoraInicio = RetornarFechaMasHora(HoraInicio, Fecha);
            Horafin = RetornarFechaMasHora(Horafin, Fecha);
            $scope.Tiempo = Horafin - HoraInicio
            $scope.Tiempo = $scope.Tiempo / MILISENGUNDOS_POR_HORA

            $scope.CantidadHoras = $scope.Tiempo.toFixed(1);
            $scope.CantidadHoras = parseInt($scope.CantidadHoras)

            $scope.CantidadMin = $scope.Tiempo
            $scope.CantidadMin = $scope.CantidadMin - $scope.CantidadHoras
            $scope.CantidadMin = Math.ceil($scope.CantidadMin * 60)

            if ($scope.CantidadMin == 60) {
                $scope.CantidadMin = '00'
                $scope.CantidadHoras = $scope.CantidadHoras + 1

            } else if ($scope.CantidadMin < 9 && $scope.CantidadMin >= 0) {
                $scope.CantidadMin = '0' + $scope.CantidadMin
            }
            if ($scope.CantidadHoras < 10 && $scope.CantidadHoras >= 0) {
                $scope.CantidadHoras = '0' + $scope.CantidadHoras
            }
            else {
                $scope.CantidadHoras = '00'
            }

            $scope.Modelo.TiempoCargue = $scope.CantidadHoras + ':' + $scope.CantidadMin
        }


        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando lista chequeo cargue descague No. ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando lista chequeo cargue descague No.' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
            };

            blockUI.delay = 1000;
            EncabezadoCheckListCargueDescarguesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo = JSON.parse(JSON.stringify(response.data.Datos))
                        $scope.Modelo.Fecha = RetornaFechaSinHoras(response.data.Datos.Fecha)
                        $scope.Modelo.FechaLlegadaCargue = RetornaFechaSinHoras(response.data.Datos.FechaLlegadaCargue)
                        $scope.Modelo.FechaInicioCargue = RetornaFechaSinHoras(response.data.Datos.FechaInicioCargue)
                        $scope.Modelo.FechaFinCargue = RetornaFechaSinHoras(response.data.Datos.FechaFinCargue)
                        $scope.Modelo.FechaSolicitudOperacion = RetornaFechaSinHoras(response.data.Datos.FechaSolicitudOperacion)
                        $scope.Modelo.FechaProgramacionCargue = RetornaFechaSinHoras(response.data.Datos.FechaProgramacionCargue)
                        $scope.Modelo.HoraLlegada = RetornarHoras(response.data.Datos.FechaLlegadaCargue)
                        $scope.Modelo.HoraInicioCargue = RetornarHoras(response.data.Datos.FechaInicioCargue)
                        $scope.Modelo.HoraFinCargue = RetornarHoras(response.data.Datos.FechaFinCargue)

                        $scope.CalcularDiferenciaHoras($scope.Modelo.HoraInicioCargue, $scope.Modelo.HoraFinCargue)

                        $scope.ListadoItemCargueDescargue.forEach(function (item) {
                            response.data.Datos.Detalle.forEach(function (itemDetalle) {
                                if (item.OrdenFormulario == itemDetalle.OrdenFormulario)
                                    item.Valor = itemDetalle.Valor
                            })
                        })

                        $scope.Modelo.Numero = response.data.Datos.Numero;

                        $scope.CodigoTipoDocumento = response.data.Datos.TipoDocumentoSoporte
                        if ($scope.ListadoTipoDocumentos.length > 0 && $scope.CodigoTipoDocumento > 0) {
                            $scope.Modelo.TipoDocumentoSoporte = $linq.Enumerable().From($scope.ListadoTipoDocumentos).First('$.Codigo ==' + response.data.Datos.TipoDocumentoSoporte)
                            $scope.ConsultarDocumento();
                        }

                        $scope.Ciudad = response.data.Datos.CiudadCargue.Codigo
                        if ($scope.ListadoCiudades.length > 0 && $scope.Ciudad > 0) {
                            $scope.Modelo.CiudadCargue = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + response.data.Datos.CiudadCargue.Codigo);
                        }

                        $scope.CodigoPlaca = response.data.Datos.Vehiculo.Codigo
                        if ($scope.ListadoVehiculos.length > 0 && $scope.CodigoPlaca > 0) {
                            $scope.Modelo.Placa = $linq.Enumerable().From($scope.ListadoVehiculos).First('$.Codigo == ' + response.data.Datos.Vehiculo.Codigo);
                            $scope.AsignarDatos($scope.Modelo.Placa)
                        }


                        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);

                        if ($scope.ModalEstado.Codigo == 0) {
                            $scope.Deshabilitar = false;
                        }
                        else if ($scope.ModalEstado.Codigo == 1) {
                            $scope.Deshabilitar = true;
                        }
                        else if (response.data.Datos.Anulado == 1) {
                            $scope.Deshabilitar = true;
                            $scope.ListadoEstados.push({ Nombre: "ANULADO", Codigo: -1 })
                            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
                        }

                    }
                    else {
                        ShowError('No se logro consultar la lista chequeo cargue descague No. ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarListaChequeoCargueDescargue';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarListaChequeoCargueDescargue';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                $scope.Modelo.Semiremolque = $scope.Modelo.Vehiculo.Semirremolque
                $scope.Modelo.Conductor = $scope.Modelo.Vehiculo.Conductor
                $scope.Modelo.TipoDocumentoSoporte = $scope.Modelo.TipoDocumentoSoporte.Codigo
                $scope.Modelo.Responsable = { Codigo: 0 }
                $scope.Modelo.FechaLlegadaCargue = RetornarHoraFecha($scope.Modelo.FechaLlegadaCargue, $scope.Modelo.HoraLlegada)
                $scope.Modelo.FechaInicioCargue = RetornarHoraFecha($scope.Modelo.FechaInicioCargue, $scope.Modelo.HoraInicioCargue)
                $scope.Modelo.FechaFinCargue = RetornarHoraFecha($scope.Modelo.FechaInicioCargue, $scope.Modelo.HoraFinCargue)
                $scope.Modelo.FechaSolicitudOperacion = RetornarFechaEspecificaSinTimeZone($scope.Modelo.FechaSolicitudOperacion)
                $scope.Modelo.FechaProgramacionCargue = RetornarFechaEspecificaSinTimeZone($scope.Modelo.FechaProgramacionCargue)
                $scope.Modelo.Fecha = RetornarFechaEspecificaSinTimeZone($scope.Modelo.Fecha)
                $scope.Modelo.Detalle = $scope.ListadoItemCargueDescargue
                $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                //Estado    
                $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

                EncabezadoCheckListCargueDescarguesFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Numero == 0) {
                                    ShowSuccess('Se guardó la lista chequeo cargue descague No. "' + response.data.Datos + '"');
                                }
                                else {
                                    ShowSuccess('Se modificó la lista chequeo cargue descague No. "' + $scope.Modelo.Numero + '"');
                                }
                                location.href = '#!ConsultarListaChequeoCargueDescargue/' + $scope.Modelo.Numero;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Fecha == undefined || $scope.Modelo.Fecha == '' || $scope.Modelo.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha de la lista de chequeo');
                continuar = false;
            }
            if ($scope.Modelo.TipoDocumentoSoporte == undefined || $scope.Modelo.TipoDocumentoSoporte == '' || $scope.Modelo.TipoDocumentoSoporte == null) {
                $scope.MensajesError.push('Debe seleccionar el documento soporte');
                continuar = false;
            }
            if ($scope.Modelo.NumeroDocumentoSoporte == undefined || $scope.Modelo.NumeroDocumentoSoporte == '' || $scope.Modelo.NumeroDocumentoSoporte == null) {
                $scope.MensajesError.push('Debe ingresar el número documento');
                continuar = false;
            }
            if ($scope.Modelo.Cliente == undefined || $scope.Modelo.Cliente == '' || $scope.Modelo.Cliente == null || $scope.Modelo.Cliente.Codigo == 0) {
                $scope.MensajesError.push('Debe ingresar un cliente ');
                continuar = false;
            }
            if ($scope.Modelo.Fecha == undefined || $scope.Modelo.Fecha == '' || $scope.Modelo.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha');
                continuar = false;
            }
            if ($scope.Modelo.FechaSolicitudOperacion == undefined || $scope.Modelo.FechaSolicitudOperacion == '' || $scope.Modelo.FechaSolicitudOperacion == null) {
                $scope.MensajesError.push('Debe ingresar la fecha solicitud operación');
                continuar = false;
            }
            if ($scope.Modelo.Cliente == undefined || $scope.Modelo.Cliente == '' || $scope.Modelo.Cliente == null || $scope.Modelo.Cliente.Codigo == 0) {
                $scope.MensajesError.push('Debe ingresar un cliente ');
                continuar = false;
            }
            if ($scope.Modelo.CiudadCargue == undefined || $scope.Modelo.CiudadCargue == '' || $scope.Modelo.CiudadCargue == null || $scope.Modelo.CiudadCargue.Codigo == 0) {
                $scope.MensajesError.push('Debe ingresar la ciudad de cargue');
                continuar = false;
            }
            if ($scope.Modelo.ContactoCliente == undefined || $scope.Modelo.ContactoCliente == '' || $scope.Modelo.ContactoCliente == null) {
                $scope.Modelo.ContactoCliente = ''
            }
            if ($scope.Modelo.Placa == undefined || $scope.Modelo.Placa == '' || $scope.Modelo.Placa == null || $scope.Modelo.Placa.Codigo == 0) {
                $scope.MensajesError.push('Debe ingresar la placa ');
                continuar = false;
            }


            return continuar;
        }



        $scope.ValidarFormatoHora = function (Hora, NombreControl) {
            var mensaje = ''
            var Control = Hora
            mensaje = ValidarHoras(Hora, NombreControl)
            if (mensaje !== '') {
                ShowError(mensaje)
                Control = ''
            }
            return Control
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarListaChequeoCargueDescargue/' + $scope.Modelo.Numero;
        };
        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
        };
        $scope.MaskHora = function () {
            try { $scope.Modelo.HoraLlegada = MascaraHora($scope.Modelo.HoraLlegada) } catch (e) { }
            try { $scope.Modelo.HoraInicioCargue = MascaraHora($scope.Modelo.HoraInicioCargue) } catch (e) { }
            try { $scope.Modelo.HoraFinCargue = MascaraHora($scope.Modelo.HoraFinCargue) } catch (e) { }
        };
    }]);