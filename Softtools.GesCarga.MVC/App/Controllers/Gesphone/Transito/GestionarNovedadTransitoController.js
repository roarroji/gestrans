﻿SofttoolsApp.controller("GestionarNovedadTransitoCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'DetalleSeguimientoVehiculosFactory', 'EncabezadoSeguimientoVehiculosFactory', 'ValorCatalogosFactory', 'NovedadTransitoFactory', 'PlanillaDespachosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, DetalleSeguimientoVehiculosFactory, EncabezadoSeguimientoVehiculosFactory, ValorCatalogosFactory, NovedadTransitoFactory, PlanillaDespachosFactory) {

        $scope.Titulo = 'GESTIONAR NOVEDAD TRÁNSITO';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Novedad Tránsito' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_NOVEDAD_TRANSITO); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.DeshabilitarNovedadesSeguimiento = true;
        $scope.ListaNovedadSeguimiento = [];
        $scope.ListaPlanilla = [];
        $scope.ModeloManifiesto = CERO;
        $scope.NumeroManifiesto = CERO;
        $scope.ModeloNumeroPlanilla = { Numero: 0, NumeroDocumento: 0 };

        $scope.ModeloNumeroManifiesto = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Estado: 1,
            TipoDocumento: CODIGO_TIPO_DOCUMENTO_SEGUIMIENTO_VEHICULAR,
            FechaReporte: '',
            TipoReporteSeguimiento: { Codigo: CODIGO_TIPO_REPORTE_SEGUIMIENTO_MOVIL },
            PuestoControl: { Codigo: CODIGO_PUESTO_CONTROL_NO_APLICA },
            ReportarCliente: CERO,
            EnvioReporte: CERO,
            KilometrosVehiculos: CERO,
            Longitud: CERO,
            Latitud: CERO,
            SitioReporteSeguimiento: { Codigo: CERO }
        };

        $scope.ValidarNumeroDocumento = function (Numero) {
            if (Numero > CERO) {
                EncabezadoSeguimientoVehiculosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroDocumento: Numero }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Numero > CERO) {
                                $scope.Modelo.Cliente = response.data.Datos.Cliente;
                                //Formatear campo fecha a dd/mm/aaaa
                                var fecha = new Date(response.data.Datos.FechaDocumento);
                                fecha.setHours(0);
                                fecha.setMinutes(0);
                                fecha.setMilliseconds(0);
                                $scope.Modelo.FechaReporte = fecha;
                                $scope.Modelo.NumeroSeguimiento = response.data.Datos.Numero;
                            }
                            else {
                                ShowError('No existe un seguimiento para la remsea/guia número ' + Numero);
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        /*Cargar el combo de novedad seguimiento*/
        $scope.CargarNovedadesSitioReporte = function (sitioReporte) {
            $scope.ListaNovedadSeguimiento = [];
            if (sitioReporte.Codigo !== undefined && sitioReporte.Codigo !== 8200) {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    SitioReporteSeguimiento: sitioReporte
                };
                DetalleSeguimientoVehiculosFactory.ConsultarNovedadesSitiosReporte(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListaNovedadSeguimiento = response.data.Datos;
                                $scope.DeshabilitarNovedadesSeguimiento = false;
                            } else {
                                $scope.DeshabilitarNovedadesSeguimiento = true;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                $scope.DeshabilitarNovedadesSeguimiento = true;
            }
        };

        /*Asignar manifiesto*/
        $scope.AsignarManifiesto = function (NumeroManifiesto,NumeroDocumentoManifiesto) {
            if (NumeroManifiesto !== "") {
                $scope.ModeloManifiesto = NumeroDocumentoManifiesto;
            }
            $scope.NumeroManifiesto = NumeroManifiesto;
        };

        /*Cargar el combo de novedad seguimiento*/
        PlanillaDespachosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_DEFINITIVO }, TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Conductor: { Codigo: $scope.Sesion.UsuarioAutenticado.Conductor.Codigo } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaPlanilla = [
                        { Numero: 0, NumeroDocumento: 0 }
                    ];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListaPlanilla.push(item);
                    });
                    $scope.ModeloNumeroPlanilla = $linq.Enumerable().From($scope.ListaPlanilla).First('$.Numero == 0');
                }
            }, function (response) {
            });

        /*Cargar el combo de sitio reporte*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_SITIO_REPORTE_SEGUIMIENDO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSitioReporte = [];
                    if (response.data.Datos.length > CERO) {
                        response.data.Datos.forEach(function (item) {
                            if (item.Codigo !== 8201) {
                                $scope.ListadoSitioReporte.push(item);
                            }       
                        });
                        $scope.Modelo.SitioReporteSeguimiento = $scope.ListadoSitioReporte[0];
                    }
                    else {
                        $scope.ListadoSitioReporte = [];
                    }
                }
            }, function (response) {
            });

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
            if ($scope.NumeroManifiesto === undefined || $scope.NumeroManifiesto === '' || $scope.NumeroManifiesto === null) {
                $scope.NumeroManifiesto = CERO;
            }
        };
     
        $scope.Guardar = function () {
            $scope.Codigo = 0;
            $scope.Modelo.Latitud = parseFloat($scope.Modelo.Latitud);
            $scope.Modelo.Longitud = parseFloat($scope.Modelo.Longitud);

            $scope.objEnviar = {
                Codigo: $scope.Codigo,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                NumeroPlanilla: $scope.ModeloNumeroPlanilla.Numero,
                NumeroManifiesto: $scope.NumeroManifiesto,
                Novedad: { Codigo: $scope.Modelo.SitioReporteSeguimiento.Codigo },
                sitio: { Codigo: $scope.Modelo.SitioReporteSeguimiento.Codigo },
                Ubicacion: $scope.Modelo.Ubicacion,
                Latitud: $scope.Modelo.Latitud.toFixed(10),
                Longitud: $scope.Modelo.Longitud.toFixed(10),
                AplicaSeguimiento: 1,
                Observaciones: $scope.Modelo.Observaciones
            };

            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                NovedadTransitoFactory.Guardar($scope.objEnviar).
                    then(function (response) {
                        console.log('llego',response)
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos > CERO) {
                                if ($scope.Modelo.Codigo === CERO) {
                                    ShowSuccess('Se guardó la novedad tránsito "' + $scope.Modelo.SitioReporteSeguimiento.Nombre + '"   para la planilla ' + $scope.ModeloNumeroPlanilla.NumeroDocumento + '"');
                                }
                                location.href = '#!Inicio/' + $scope.Modelo.Codigo;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.ModeloNumeroPlanilla === undefined || $scope.ModeloNumeroPlanilla === '' || $scope.ModeloNumeroPlanilla === null || $scope.ModeloNumeroPlanilla.Numero === CERO) {
                $scope.MensajesError.push('Debe seleccionar una Planilla');
                continuar = false;
            }
            if ($scope.Modelo.SitioReporteSeguimiento === undefined || $scope.Modelo.SitioReporteSeguimiento === '' || $scope.Modelo.SitioReporteSeguimiento === null || $scope.Modelo.SitioReporteSeguimiento.Codigo === CERO) {
                $scope.MensajesError.push('Debe seleccionar el sitio reporte');
                continuar = false;
            }
            else if ($scope.Modelo.SitioReporteSeguimiento.Codigo === CODIGO_SITIO_REPORTE_NO_APLICA) {
                $scope.MensajesError.push('Debe seleccionar el sitio reporte ');
                continuar = false;
            }
            if ($scope.Modelo.Ubicacion === undefined || $scope.Modelo.Ubicacion === '' || $scope.Modelo.Ubicacion === null) {
                $scope.MensajesError.push('Debe ingresar la ubicación');
                continuar = false;
            }
            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!Inicio/';
        };
        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase(); } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.NumeroDocumento = MascaraNumero($scope.Modelo.NumeroDocumento); } catch (e) { }
        };
        //--------------------------------------------------------------------------------------------------------

               //--------------------------------------------------------------------------------------------------------

        function localizacion(posicion) {
            $scope.Modelo.Latitud = posicion.coords.latitude;
            $scope.Modelo.Longitud = posicion.coords.longitude;
        }

        function errores(error) {
            MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
        }

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(localizacion, errores);
        } else {
            MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
        }


    }]);