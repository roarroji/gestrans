﻿(function () {
    'use strict';
    SofttoolsApp.controller("MainCtrl", ['$scope', '$timeout', '$linq', 'EmpresasFactory', 'SeguridadUsuariosFactory', 'authFactory', 'blockUI', 'blockUIConfig',
        'localStorageService', 'TercerosFactory', 'ColorVehiculosFactory', 'CiudadesFactory', 'UsuariosFactory', 'VehiculosFactory', '$interval', 'NotificacionesFactory',
        'AutorizacionesFactory', 'ProductoTransportadosFactory', 'SemirremolquesFactory', 'ValorCatalogosFactory', 'EncabezadoProgramacionOrdenServiciosFactory', 'EncabezadoSolicitudOrdenServiciosFactory','PermisoGrupoUsuariosFactory','InspeccionPreoperacionalFactory',
        function ($scope, $timeout, $linq, EmpresasFactory, SeguridadUsuariosFactory, authFactory, blockUI, blockUIConfig,
            localStorageService, TercerosFactory, ColorVehiculosFactory, CiudadesFactory, UsuariosFactory, VehiculosFactory, $interval,
            NotificacionesFactory, AutorizacionesFactory, ProductoTransportadosFactory, SemirremolquesFactory, ValorCatalogosFactory, EncabezadoProgramacionOrdenServiciosFactory, EncabezadoSolicitudOrdenServiciosFactory, PermisoGrupoUsuariosFactory, InspeccionPreoperacionalFactory) {
            /*Modelo*/
            $scope.URL = document.URL
            //$scope.URL = 'http://localhost:8439/App/Views/Main.html#!/GestionarPorgrmacionOrdenServicio'
            $scope.Sesion = {
                UsuarioAutenticado: {}, Identificador: '', Contrasena: '', FilterLst: RetornarListaAutocomplete, Empresa: {}, CorreoValidar: '', CodigoVerificacion: '', ConfNuevaContrasena: '', NuevaContrasena: ''
            };
            $scope.Titulo = '';
            $scope.FondoPantalla = false;
            $scope.AnoActual = new Date().getFullYear();
            $scope.EstiloContenido = '';
            $scope.ListadoMenu = [];
            $scope.ListadoMenuListados = [];
            $scope.ListaModulo = [];
            $scope.MensajesErrorCambioClave = [];
            $scope.ListaValidaciones = [];
            $scope.ListadoPermisos = [];
            $scope.ImagenFondoPantalla = '';
            $scope.ImagenLogo = '';
            $scope.Version = Version_Aplicativo;
            $scope.Banerstyle = 'Display:none'
            $scope.MensajeBaner = ''
            $('#NombreCaja').hide()
            $('#baner').hide()
            //$('#pcoded').hide(1000)
            $scope.styleTHG = ''
            document.location.href = '#';
            var Prefijo = ''
            var CodigoRegional = ""
            var añohabilitacionministerio = ""
            var NumeroResolucion = ""
            BloqueoPantalla = blockUI
            $scope.ContIntentos = 0
            try {
                $('#PanelMenu > li ').off()
                $('#PanelMenu > ul > li ').off()
                $('#PanelMenu > ul > li > ul > li ').off()
                $('#PanelMenu > ul > li > ul > li > ul > li').off()
            } catch (e) {

            }
            $('#SeccionLogin').show();
            $('#SeccionOficina').hide();

            //Recuperar Contraseña:
            $scope.FechaVigencia = '';
            $scope.UsuaCodigo = ''
            $scope.ModificarClave = false;
            $scope.EstilosMensaje = "alert alert-success";
            $scope.InforecuperacionClave = [];
            $scope.CodigoBD = '';
            $scope.ClaveBD = '';
            $scope.VistaVerificacion = false
            $scope.RecuperarClave = false;
            $scope.BuscarCorreo = function () {
                $scope.InforecuperacionClave = [];
                $scope.RecuperarClave = true;
                $scope.VistaVerificacion = false;
                $scope.ModificarClave = false;
            }

            console.log('empresa',$scope.Empresa)
            $scope.VolverLogin = function () {
                $scope.RecuperarClave = false;
                $scope.VistaVerificacion = false;
                $scope.ModificarClave = false;
            }

            $scope.ValidarCorreo = function () {

                $scope.NoAutorizado = true;
                $scope.UsuaCodigo = '';
                $scope.CodigoBD = '';
                $scope.ClaveBD = '';
                EmpresasFactory.CambioClave({ Codigo: $scope.Empresa.Codigo, Usuario: { Correo: $scope.Sesion.CorreoValidar, CodigoUsuario: $scope.Sesion.Identificador } }).
                    then(function (response) {
                        $scope.NoAutorizado = true;
                        if (response.data.ProcesoExitoso == true) {
                            console.log("datos: ", response.data.Datos);
                            if (response.data.Datos.length > 0) {

                                if (response.data.Datos[0].Usuario.ExisteUsuario == 0) {
                                    $scope.InforecuperacionClave = [];
                                    $scope.EstilosMensaje = "alert alert-danger";
                                    $scope.InforecuperacionClave.push('El usuario ingresado no existe');
                                } else {
                                    $scope.NoAutorizado = true;
                                    $scope.RecuperarClave = false;
                                    $scope.VistaVerificacion = true;
                                    $scope.ModificarClave = false;
                                    $scope.CodigoBD = response.data.Datos[0].Usuario.CodigoCambioClave;
                                    $scope.UsuaCodigo = response.data.Datos[0].Usuario.Codigo;
                                    $scope.ClaveBD = response.data.Datos[0].Usuario.Clave;
                                    $scope.FechaVigencia = response.data.Datos[0].Usuario.FechaVigenciaCambioClave;
                                    $scope.CorreoManager = response.data.Datos[0].Usuario.CorreoManager;
                                    $scope.InforecuperacionClave = [];
                                    $scope.EstilosMensaje = "alert alert-success";
                                    $scope.InforecuperacionClave.push('Hemos enviado un correo electrónico con el código de validación, revise su bandeja de entrada e ingrese el código');
                                }


                            } else {
                                $scope.NoAutorizado = true;
                                $scope.InforecuperacionClave = [];
                                $scope.EstilosMensaje = "alert alert-danger";
                                $scope.InforecuperacionClave.push('la cuenta de correo ingresada, no corresponde con el usuario ingresado');
                            }
                        }
                    }), function (response) {
                        ShowError(response);
                    };
            }

            $scope.ValidarCodigoCorreo = function () {
                if ($scope.Sesion.CodigoVerificacion == $scope.CodigoBD) {
                    var fechaActual = new Date().valueOf();
                    var fBD = new Date($scope.FechaVigencia).valueOf();
                    var difFecha = fechaActual - fBD;
                    if (difFecha <= 1800000) {
                        $scope.InforecuperacionClave = [];
                        $scope.ModificarClave = true;
                        $scope.RecuperarClave = false;
                        $scope.VistaVerificacion = false;
                    } else {
                        $scope.InforecuperacionClave = [];
                        $scope.EstilosMensaje = "alert alert-danger";
                        $scope.InforecuperacionClave.push('Este código ya ha caducado, repita el proceso nuevamente');
                    }

                } else {
                    $scope.InforecuperacionClave = [];
                    $scope.EstilosMensaje = "alert alert-danger";
                    $scope.InforecuperacionClave.push('Este código no es válido');
                }
            }

            $scope.ValidarContrasenas = function () {
                if ($scope.Sesion.NuevaContrasena == $scope.Sesion.ConfNuevaContrasena) {
                    if (validarContrasena($scope.Sesion.NuevaContrasena) == false) {
                        $scope.InforecuperacionClave = [];
                        $scope.EstilosMensaje = "alert alert-danger";
                        $scope.InforecuperacionClave.push('La contraseña debe ser mínimo de 6 carácteres y máximo de 10, debe contener al menos un carácter especial, un número y no debe tener espacios en blanco, recuerde que los caracteres "&" y "+" no están permitidos');

                    } else {
                        EmpresasFactory.CambioClave({ Codigo: $scope.Empresa.Codigo, Usuario: { Codigo: $scope.UsuaCodigo, Clave: $scope.Sesion.NuevaContrasena, Correo: $scope.Sesion.CorreoValidar, CorreoManager: $scope.CorreoManager } }).
                            then(function (response) {
                                $scope.InforecuperacionClave = [];
                                //$scope.EstilosMensaje = "alert alert-success";
                                //$scope.InforecuperacionClave.push('La contraseña se ha modificado con éxito.');
                                $scope.RecuperarClave = false;
                                $scope.VistaVerificacion = false;
                                $scope.ModificarClave = false;
                                ShowSuccess('La contraseña se ha modificado con éxito.');
                            });
                    }
                } else {
                    $scope.InforecuperacionClave = [];
                    $scope.EstilosMensaje = "alert alert-danger";
                    $scope.InforecuperacionClave.push('Las Contraseñas no coinciden, ingrese de nuevo los datos');
                }
            }

            /*Cargar el combo de empresas*/
            EmpresasFactory.Consultar({}).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        $('#SeccionLogin').show();
                        $('#SeccionOficina').hide();
                        $scope.ListadoEmpresas = response.data.Datos;
                        $scope.Empresa = $scope.ListadoEmpresas[0];
                        Prefijo = $scope.Empresa.Prefijo
                        $scope.Sesion.UsuarioAutenticado.Prefijo = $scope.Empresa.Prefijo
                        if (Prefijo !== 'TRGHE') {
                            $scope.styleTHG = 'Display:none'
                        }
                        CodigoRegional = $scope.Empresa.CodigoRegional
                        añohabilitacionministerio = $scope.Empresa.AnioHabilitacion
                        NumeroResolucion = $scope.Empresa.NumeroResolucion
                        if (screen.width >= 1024) {
                            $scope.FondoPantalla = false;
                            $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_' + Prefijo + '.jpg'
                        }
                        else if (screen.width < 1024) {
                            $scope.FondoPantalla = true;
                            //$scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_Movil.jpg'
                        }

                        $scope.ImagenLogo = '../../Css/img/Logo_' + Prefijo + '.jpg'

                        var d = new Date()
                        //setCookie('UsuarioEmpresa', '5', 1)
                        //setCookie('Identificador', 'admin', 1)
                        //setCookie('Contrasena', 'adminges9)', 1)

                        $scope.ver = d.getFullYear().toString() + '.' + (d.getMonth() + 1).toString() + '.' + d.getDate() + '.' + d.getSeconds
                        if (getCookie('UsuarioEmpresa') !== "" && getCookie('UsuarioEmpresa') !== undefined && getCookie('UsuarioEmpresa') !== 0
                            && getCookie('Identificador') !== "" && getCookie('Identificador') !== undefined && getCookie('Identificador') !== 0
                            && getCookie('Contrasena') !== "" && getCookie('Contrasena') !== undefined && getCookie('Contrasena') !== 0
                        ) {
                            $scope.Sesion.Identificador = getCookie('Identificador').toString()
                            $scope.Sesion.Contrasena = getCookie('Contrasena').toString()
                            $scope.Empresa = $linq.Enumerable().From($scope.ListadoEmpresas).First('$.Codigo ==' + getCookie('UsuarioEmpresa').toString());
                            $scope.ValidarUsuarioAlterno()
                            $scope.ValidarUsuario()
                            $scope.AsignarLogos()
                        } else {
                            $scope.NoAutorizado = false;
                            $scope.Autorizado = true;
                            $timeout(function () {
                                $scope.Autorizado = false;
                                $scope.NoAutorizado = true;
                                $scope.AsignarLogos()
                            }, 600)
                            $scope.AsignarLogos()
                        }
                    }
                }, function (response) {
                    throw new Error('No se pueden cargar las empresas. Contacte con el administrador del sistema.');
                    $scope.Autorizado = false;
                    $scope.NoAutorizado = true;
                });

            $scope.AsignarLogos = function () {
                if (screen.width >= 1024) {
                    $scope.FondoPantalla = false;
                    $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_' + $scope.Empresa.Prefijo + '.jpg'
                }
                else if (screen.width < 1024) {
                    $scope.FondoPantalla = true;
                    //$scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_Movil.jpg'
                }

                $scope.ImagenLogo = '../../Css/img/Logo_' + $scope.Empresa.Prefijo + '.jpg'
            }


            $scope.ValidarEmpresa = function (Empresa) {
                $scope.Empresa = Empresa
            }

            $scope.ValidarUsuario = function () {

                var usuarioEmpresa = $scope.Sesion.Identificador + "|" + $scope.Empresa.Codigo;
                $scope.Sesion.Empresa = $scope.Empresa
                $scope.loginData = {
                    userName: usuarioEmpresa,
                    password: $scope.Sesion.Contrasena,
                };
                authFactory.login($scope.loginData).then(function (response) {
                    if (response.data.access_token != undefined) {
                        //$scope.Autorizado = true;
                        //$scope.NoAutorizado = false;
                        $scope.Sesion.DatosToken = response;
                        OBJ_TOKEN_SOFTTOOLS = response;
                        setCookie('UsuarioEmpresa', $scope.Empresa.Codigo, 1)
                        setCookie('Identificador', $scope.Sesion.Identificador, 1)
                        setCookie('Contrasena', $scope.Sesion.Contrasena, 1)
                        ObtenerInformacionUsuario();
                        $scope.EstiloContenido = 'hold-transition skin-blue sidebar-mini';
                        $scope.Banerstyle = 'color:white'
                    } else {
                        ShowWarning('Advertencia', response.data.MensajeOperacion);
                        $scope.Autorizado = false;
                        $scope.NoAutorizado = true;
                    }
                },
                    function (err) {
                        try {
                            if (err.data.error_description == undefined || err.data.error_description == null) {
                                ShowError('Error intentando iniciar sesión');
                                $scope.Autorizado = false;
                                $scope.NoAutorizado = true;
                            }
                            else {
                                ShowError(err.data.error_description);
                                $scope.Autorizado = false;
                                $scope.NoAutorizado = true;
                            }
                        } catch (e) {
                        }

                    });

            };

            $scope.ValidarUsuarioAlterno = function () {

                var usuarioEmpresa = $scope.Sesion.Identificador + "|" + $scope.Empresa.Codigo;
                $scope.Sesion.Empresa = $scope.Empresa
                $scope.loginData = {
                    userName: usuarioEmpresa,
                    password: $scope.Sesion.Contrasena,
                };
                authFactory.login($scope.loginData).then(function (response) {
                    if (response.data.access_token != undefined) {
                        //$scope.Autorizado = true;
                        //$scope.NoAutorizado = false;
                        $scope.Sesion.DatosToken = response;
                        setCookie('UsuarioEmpresa', $scope.Empresa.Codigo, 1)
                        setCookie('Identificador', $scope.Sesion.Identificador, 1)
                        setCookie('Contrasena', $scope.Sesion.Contrasena, 1)
                        ObtenerInformacionUsuario();
                        $scope.EstiloContenido = 'hold-transition skin-blue sidebar-mini';
                        $scope.Banerstyle = 'color:white'
                    } else {
                        ShowWarning('Advertencia', response.data.MensajeOperacion);
                        $scope.Autorizado = false;
                        $scope.NoAutorizado = true;
                    }
                },
                    function (err) {
                        try {
                            if (err.data.error_description == undefined || err.data.error_description == null) {
                                ShowError('Error intentando iniciar sesión');
                                $scope.Autorizado = false;
                                $scope.NoAutorizado = true;
                            }
                            else {
                                ShowError(err.data.error_description);
                                $scope.Autorizado = false;
                                $scope.NoAutorizado = true;
                            }
                        } catch (e) {
                            $scope.ValidarUsuario();
                        }
                    });
            };

            $scope.asignaraccion = function () {
                for (var i = 0; i < $('a').length; i++) {
                    if ($('a')[i].href.includes('unsafe:')) {
                        $('a')[i].href = $('a')[i].href.replace('unsafe:', '')
                    }
                }
            }
            $scope.FormasPago = [
                { Nombre: 'Transferencia', Codigo: 1 },
                { Nombre: 'Otros', Codigo: 2 },

            ]
            function ObtenerInformacionUsuario() {
                SeguridadUsuariosFactory.Consultar({ CodigoEmpresa: $scope.Empresa.Codigo, CodigoUsuario: $scope.Sesion.Identificador, Clave: $scope.Sesion.Contrasena, Token: 1, }).
                    then(function (response) {
                        var i = 0;
                        $scope.ListadoMenu = [];
                        $scope.ListadoMenuListados = [];
                        var o = 0;
                        if (response.data.ProcesoExitoso === true) {
                            //$scope.ImagenFondoPantalla = ''
                            $scope.Sesion.UsuarioAutenticado = response.data.Datos;
                            
                            $scope.Sesion.ValorPoliza = $scope.Empresa.ValorPoliza
                            $scope.Sesion.UsuarioAutenticado.Prefijo = $scope.Empresa.Prefijo
                            for (var i = 0; i < response.data.Datos.ConsultaMenu.length; i++) {
                                if (response.data.Datos.ConsultaMenu[i].Habilitado == 1 && response.data.Datos.ConsultaMenu[i].MostrarMenu == 1) {
                                    $scope.ListadoMenu.push(response.data.Datos.ConsultaMenu[i])
                                }
                                if (response.data.Datos.ConsultaMenu[i].Habilitado == 1 && response.data.Datos.ConsultaMenu[i].OpcionListado == 1) {
                                    $scope.ListadoMenuListados.push(response.data.Datos.ConsultaMenu[i])
                                }
                                if (response.data.Datos.ConsultaMenu[i].OpcionPermiso == 1) {
                                    $scope.ListadoPermisos.push(response.data.Datos.ConsultaMenu[i])
                                }
                            }

                            $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas = false;
                            if (response.data.Datos.ConsultaOtrasOficinas > 0) {
                                $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas = true;
                            }
                            /* Validación Procesos */
                            $scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta = false
                            $scope.Sesion.UsuarioAutenticado.ManejoTarifasProgramacion = false
                            $scope.Sesion.UsuarioAutenticado.ManejoValidaCumplidoPendienteDespacho = false
                            $scope.Sesion.UsuarioAutenticado.ManejoEnturnamientoDespacho = false
                            $scope.Sesion.UsuarioAutenticado.ManejoFidelizacion = false
                            $scope.Sesion.UsuarioAutenticado.ManejoAnticiposPSL = false
                            $scope.Sesion.UsuarioAutenticado.ManejoPesoPuntosFidelizacion = false
                            $scope.Sesion.UsuarioAutenticado.ManejoDespachoProgramacion = false
                            $scope.Sesion.UsuarioAutenticado.ManejoValidarDocumentoClienteSedeOrdenServicio = false
                            $scope.Sesion.UsuarioAutenticado.ManejoEnvioCorreoOrdenServioPorSedeCliente = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoPermitirFacturarRemesasSinCumplir = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoDistintasLineasPlanillaDespachos = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoBloqueoDespachosFechasPosteriores = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoRecalculoFleteLiquidacionPesoCumplido = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoDocumentosProximosVencer = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoEngancheRemesasMasivoPaqueteria = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoRequiereOrdenCargueDespacho = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoGalonCombustible = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoCamposCumplidoLiquidacion = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoModificarValoresFactura = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoColoresEventoSeguimiento = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoNoPermitirFacturarAFacturacion = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoAutorizacionFlete = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoPlanPuntosVehiculos = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoDeshabilitarValoresCargueDescargueProgramacion = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoDeshabilitarCambioProductoProgramacion = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoPorcentajeAfiliado = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoGenerarManifiestoPaqueteria = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoValidarCupoPSL = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoCrearUsuarioConductor = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoHabilitarVariosSitiosDescargueOS = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoHabilitarVariosConductoresPlanillaDespachos = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoHabilitarLiquidacionPlanillaDespachos = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoHabilitarModificarFleteCliente = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoFacturarA = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoCentroCostosComprobanteEgreso = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoRemolqueObligatorio = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoPreimpresoObligatorio = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoCuentasPorCobrarPagarLegalizacionesGastos = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoReexpedicionPorOficina = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoPorcentajesPropietariosVehiculo = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoSitioCargueDescargueGeneral = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoCuentasPorCobrarPagarLiquidacionesPlanilla = false;
                            $scope.Sesion.UsuarioAutenticado.DeshabilitarPaginacionFacturaRemesas = false;
                            $scope.Sesion.UsuarioAutenticado.SugerirAnticipoPlanilla = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoEstadosNovedades = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoDeshabilitarSemirremolqueDespacharProgramacion = false;                            
                            $scope.Sesion.UsuarioAutenticado.ManejoSolicitudAnticipoPlanilla = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoValorDeclarado = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoValidacionInspecciones = false;
                            $scope.Sesion.UsuarioAutenticado.DeshabilitarAnticipoPlanillaPaqueteria = false;
                            $scope.Sesion.UsuarioAutenticado.ProcesoSugerirAnticipoComprobanteEgreso = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoTiemposCumplidoPaqueteria = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoHabilitarCamposImpocoma = false;
                            $scope.Sesion.UsuarioAutenticado.ProcesoOcultarFormaPagoTarifarios = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoValidarImpuestosTercero = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoLiquidacionContratoVinculacionTemporal = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoICACiudadOficinaDespacha = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoConsultarRemesasBorradorEnPlanillaRecoleccion = false;
                            $scope.Sesion.UsuarioAutenticado.CxCAnulacionPlanillaDespacho = false;
                            $scope.Sesion.UsuarioAutenticado.ObligatoriedadPolizasYTecnicoMecanica = false;
                            $scope.Sesion.UsuarioAutenticado.ManejoDespachoFormaDePago = false;
                            $scope.Sesion.UsuarioAutenticado.ProcesoProporcionFleteVariosConductores = false;
                            $scope.Sesion.UsuarioAutenticado.ProcesoListaPreciosWB = false;
                            $scope.Sesion.UsuarioAutenticado.ProcesoNumeroOrdenCompra = false;
                            $scope.Sesion.UsuarioAutenticado.ProcesoDeshabilitarAnticipoPlanillaMasivo = false;
                            $scope.Sesion.UsuarioAutenticado.ProcesoMarcarInhabilitarFacturarDespachar = false;
                            $scope.Sesion.UsuarioAutenticado.ProcesoSugerirTiemposCargueDescargue = false;
                            $scope.Sesion.UsuarioAutenticado.ProcesoFacturarPesoCumplido = false
                            $scope.Sesion.UsuarioAutenticado.DuplicarOrdenServicio = false
                            if ($scope.Empresa.Validaciones !== undefined && $scope.Empresa.Validaciones !== null) {
                                if ($scope.Empresa.Validaciones.length > 0) {
                                    for (var i = 0; i < $scope.Empresa.Validaciones.length; i++) {
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_TARIFA_COMPRA_TARIFARIO_VENTA) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_TARIFAS_PROGRAMACION) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoTarifasProgramacion = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_VALIDA_CUMPLIDO_PENDIENTE_DESPACHO) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoValidaCumplidoPendienteDespacho = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_ENTURNAMIENTO_DESPACHO) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoEnturnamientoDespacho = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_FIDELIZACION) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoFidelizacion = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_ANTICIPOSPSL) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoAnticiposPSL = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_PESOPUNTOSFIDELIZACION) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoPesoPuntosFidelizacion = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_DESPACHO_PROGRAMACION) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoDespachoProgramacion = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_DOCUMENTO_CLIENTE_SEDE_ORDEN_SERVICIO) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoValidarDocumentoClienteSedeOrdenServicio = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_ENVIO_CORREO_ORDEN_SERVICIO_POR_SEDE_CLIENTE) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoEnvioCorreoOrdenServioPorSedeCliente = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_PERMITIR_FACTURAR_REMESAS_SIN_CUMPLIR) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoPermitirFacturarRemesasSinCumplir = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_FECHA_FINAL_DEFAULT_HOY) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_DISTINTAS_LINEAS_PLANILLA_DESPACHOS) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoDistintasLineasPlanillaDespachos = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_BLOQUEO_DESPACHOS_FECHAS_POSTERIORES) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoBloqueoDespachosFechasPosteriores = true
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_RECALCULO_FLETE_LIQUIDACION_PESO_CUMPLIDO) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoRecalculoFleteLiquidacionPesoCumplido = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_DOCUMENTOS_PROXIMOS_VENCER) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoDocumentosProximosVencer = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_ENGANCHE_REMESAS_MASIVO_PQUETERIA) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoEngancheRemesasMasivoPaqueteria = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_REQUIERE_ORDEN_CARGE) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoRequiereOrdenCargueDespacho = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_GALON_COMBUSTIBLE) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoGalonCombustible = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_CAMPOS_CUMPLIDO_LIQUIDACIÓN) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoCamposCumplidoLiquidacion = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MODOFICAR_VALORES_FACTURA) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoModificarValoresFactura = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_SEGUIMIENTO_POR_EVENTOS) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoColoresEventoSeguimiento = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_NO_PERMITIR_FACTURARA_FACTURACION) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoNoPermitirFacturarAFacturacion = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_AUTORIZACION_FLETE) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoAutorizacionFlete = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_PLAN_PUNTOS_VEHICULOS) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoPlanPuntosVehiculos = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_DESHABILITAR_CAMBIO_PRODUCTO_PROGRAMACION) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoDeshabilitarCambioProductoProgramacion = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_DESHABILITAR_VALORES_CARGUE_DESCARGUE_PROGRAMACION) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoDeshabilitarValoresCargueDescargueProgramacion = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_PORCENTAJE_AFILIADO) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoPorcentajeAfiliado = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_GENERAR_MANIFIESTO_PAQUETERIA) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoGenerarManifiestoPaqueteria = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_VALIDAR_CUPO_PSL) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoValidarCupoPSL = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_CREAR_USUARIO_TERCERO_CONDUCTOR) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoCrearUsuarioConductor = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_HABILITAR_VARIOS_SITIOS_DESCARGUE_OS) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoHabilitarVariosSitiosDescargueOS = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_HABILITAR_VARIOS_CONDUCTORES) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoHabilitarVariosConductoresPlanillaDespachos = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_HABILITAR_LIQUIDACION_PLANILLA_DESPACHOS) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoHabilitarLiquidacionPlanillaDespachos = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_HABILITAR_MODIFICAR_FLETE_CLIENTE) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoHabilitarModificarFleteCliente = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_MANEJO_PESO_VOLUMETRICO_REMESA_PAQUETERIA) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_MANEJO_FACTURAR_A) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoFacturarA = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_MANEJO_CENTRO_COSTOS_COMPROBANTE_EGRESO) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoCentroCostosComprobanteEgreso = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_OBLIGATORIEDAD_REMOLQUE_DESPACHO) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoRemolqueObligatorio = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PREIMPRESO_OBLIGATORIO_GUIA_PAQUETERIA) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoPreimpresoObligatorio = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_MANEJO_CUENTAS_POR_COBRAR_PAGAR_LEGALIZACIONES) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoCuentasPorCobrarPagarLegalizacionesGastos = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_MANEJO_REEXPEDICION_POR_OFICINA) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoReexpedicionPorOficina = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_MANEJO_PORCENTAJES_PARTICIPACION_PROPIETARIOS_VEHICULO) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoPorcentajesPropietariosVehiculo = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_MANEJO_SITIO_CARGUE_DESCARGUE_GENERAL) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoSitioCargueDescargueGeneral = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_MANEJO_CUENTAS_POR_COBRAR_PAGAR_LIQUIDACIONES_PLANILLA) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoCuentasPorCobrarPagarLiquidacionesPlanilla = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_DESHABILITAR_PAGINACION_FACTURA_REMESAS) {
                                            $scope.Sesion.UsuarioAutenticado.DeshabilitarPaginacionFacturaRemesas = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_SUGERIR_ANTICIPO_PLANILLA) {
                                            $scope.Sesion.UsuarioAutenticado.SugerirAnticipoPlanilla = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_ESTADOS_NOVEDADES) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoEstadosNovedades = true;
                                        }                                        
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_DESHABILITAR_SEMIRREMOLQUE_DESPACHAR_PROGRAMACÍON) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoDeshabilitarSemirremolqueDespacharProgramacion = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_SOLICITUD_ANTICIPO_PLANILLA) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoSolicitudAnticipoPlanilla = true;
                                        }                                        
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_VALOR_DECLARADO) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoValorDeclarado = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_DESHABILITAR_ANTICIPO_PLANILLA_PAQUETERIA) {
                                            $scope.Sesion.UsuarioAutenticado.DeshabilitarAnticipoPlanillaPaqueteria = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_INSPECCIONES) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoValidacionInspecciones = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_TIEMPOS_CUMPLIDO_PAQUETERIA) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoTiemposCumplidoPaqueteria= true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_CAMPOS_IMPOCOMA) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoHabilitarCamposImpocoma= true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_SUGERIR_ANTICIPO_COMPROBANTE_EGRESO) {
                                            $scope.Sesion.UsuarioAutenticado.ProcesoSugerirAnticipoComprobanteEgreso = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_OCULTAR_FORMA_PAGO_TARIFARIOS) {
                                            $scope.Sesion.UsuarioAutenticado.ProcesoOcultarFormaPagoTarifarios = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_IMPUESTOS_TERCERO) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoValidarImpuestosTercero = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_LIQUIDACION_CONTRATO_VINCULACION_TEMPORAL) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoLiquidacionContratoVinculacionTemporal = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_ICA_CIUDAD_OFICINA_DESTINO) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoICACiudadOficinaDespacha = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_CONSULTAR_REMESAS_BORRADOR_PLANILLA_RECOLECCION) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoConsultarRemesasBorradorEnPlanillaRecoleccion = true;
                                        }                                        
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_GENERAR_CXC_ANULACION_PLANILLA) {
                                            $scope.Sesion.UsuarioAutenticado.CxCAnulacionPlanillaDespacho = true;
                                        }                                        
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_OBLIGATORIEDAD_POLIZAS_Y_TECNICO_MECANICA) {
                                            $scope.Sesion.UsuarioAutenticado.ObligatoriedadPolizasYTecnicoMecanica = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MANEJO_DESPACHO_FORMA_PAGO) {
                                            $scope.Sesion.UsuarioAutenticado.ManejoDespachoFormaDePago = true;
                                        }
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_PROPORCION_FLETE_VARIOS_CONDUCTORES) {
                                            $scope.Sesion.UsuarioAutenticado.ProcesoProporcionFleteVariosConductores = true;
                                        } if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_LISTA_PRECIOS_WB) {
                                            $scope.Sesion.UsuarioAutenticado.ProcesoListaPreciosWB = true;
                                        } if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_NUMERO_ORDEN_COMPRA) {
                                            $scope.Sesion.UsuarioAutenticado.ProcesoNumeroOrdenCompra = true;
                                        }

                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_DESHABILITAR_ANTICIPO_PLANILLA_MASIVO) {
                                            $scope.Sesion.UsuarioAutenticado.ProcesoDeshabilitarAnticipoPlanillaMasivo = true;
                                        }
                                        
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_MARCAR_INHABILITAR_FACTURAR_DESPACHAR) {
                                            $scope.Sesion.UsuarioAutenticado.ProcesoMarcarInhabilitarFacturarDespachar = true;
                                        }
                                        
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_SUGERIR_TIEMPOS_CARGUE_DESCARGUE) {
                                            $scope.Sesion.UsuarioAutenticado.ProcesoSugerirTiemposCargueDescargue = true;
                                        }
                                       
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_FACTURAR_PESO_CUMPLIDO) {
                                            $scope.Sesion.UsuarioAutenticado.ProcesoFacturarPesoCumplido = true;
                                        }

                                        
                                        if ($scope.Empresa.Validaciones[i].Codigo == VALIDACION_PROCESO_DUPLICAR_ORDEN_SERVICIO) {
                                            $scope.Sesion.UsuarioAutenticado.DuplicarOrdenServicio = true;
                                        }
                                    }
                                }
                            }
                            $scope.Sesion.UsuarioAutenticado.ListadoControles = $scope.Empresa.Controles
                            $scope.ListaModulo = response.data.Datos.ConsultaModulo;
                            $scope.LitaGesphone = []
                            for (var i = 0; i < $scope.ListaModulo.length; i++) {
                                if ($scope.ListaModulo[i].Aplicacion == 203 && $scope.ListaModulo[i].MostrarModulo == 1 && $scope.ListaModulo[i].Estado == 1) {

                                    $scope.LitaGesphone.push($scope.ListaModulo[i])
                                }
                            }
                            $timeout(function () {
                                $('#PanelMenu > li ').off()
                                $('#PanelMenu > ul > li ').off()
                                $('#PanelMenu > ul > li > ul > li ').off()
                                $('#PanelMenu > ul > li > ul > li > ul > li').off()
                                try {
                                    $.fn.pcodedmenu();
                                    $(document).off();
                                } catch (e) {
                                }
                            }, 1000)
                            $scope.HoraPermisoUsuario = response.data.Datos.Hora;
                            $scope.ListaValidaciones = response.data.Datos.ConsultaValidaciones;
                            localStorageService.set('opcionesMenu', angular.copy($scope.ListadoMenu));
                            if ($scope.Sesion.UsuarioAutenticado.Mensaje !== '' && $scope.Sesion.UsuarioAutenticado.Mensaje !== undefined) {
                                $scope.MensajeBaner = $scope.Sesion.UsuarioAutenticado.Mensaje
                                $('#baner').show()
                            } else {
                                $scope.MensajeBaner = ''
                                $('#baner').hide()
                            }
                            //$('#pcoded').show(1000)
                            $scope.Sesion.Main = document.URL
                            //Funcion para verificar si el usuario tiene la clave vencida
                            var now = new Date()
                            var past = new Date(response.data.Datos.FechaUltimoCambioClave)
                            var diastrancurridos = now.getTime() - past.getTime()
                            diastrancurridos = Math.round(diastrancurridos / (1000 * 60 * 60 * 24));
                            if (diastrancurridos > response.data.Datos.DiasCambioClave) {
                                showModal('modalCambioContraseña');
                                $scope.CambioObligatorio = true
                                document.getElementById('Mensajecambio').innerHTML = 'Su contraseña se encuentra vencida, la actualizacion es obligatoria para continuar con el uso de la aplicación'
                            }
                            $scope.Sesion.UsuarioAutenticado.ListadoOficinas = []
                            if (response.data.Datos.Oficinas.length > 0) {
                                $scope.Sesion.UsuarioAutenticado.ListadoOficinas = response.data.Datos.Oficinas;
                            }
                            if (response.data.Datos.Oficinas.length > 1 && !window.location.href.includes("Gesphone.html")) {
                                $scope.ListadoOficinas = response.data.Datos.Oficinas
                                $('#SeccionLogin').hide(1000);
                                $('#SeccionOficina').show(1000);
                                $scope.Autorizado = false;
                                $scope.NoAutorizado = true;
                                if (getCookie('Oficina') !== "" && getCookie('Oficina') !== undefined && getCookie('Oficina') !== 0) {
                                    try {
                                        var oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + getCookie('Oficina').toString());
                                        $scope.AsignarOficina(oficina)
                                    } catch (e) {
                                        $scope.ModalOficina = $scope.ListadoOficinas[0]
                                    }
                                } else {
                                    $scope.ModalOficina = $scope.ListadoOficinas[0]
                                }
                            } else {
                                $scope.Sesion.UsuarioAutenticado.Oficinas = response.data.Datos.Oficinas[0]
                                $('#ImgFondoMain').hide()
                                $scope.Autorizado = true;
                                $scope.NoAutorizado = false;
                                document.location.href = $scope.URL;
                                $scope.MarginTop = $('#header').height()
                                $timeout(function () {
                                    $scope.Redimencionar()
                                }, 1000)
                            }
                        } else {
                            ShowWarning('Advertencia', response.data.MensajeOperacion);
                        }
                        if ($scope.Sesion.UsuarioAutenticado.AplicacionUsuario == CODIGO_APLICACION_GESPHONE && $scope.Sesion.UsuarioAutenticado.ManejoDocumentosProximosVencer) {
                            document.location.href = '#!DocumentosProximosVencer';
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        //ShowError('Error intentando iniciar sesión');
                    });

            };
            $scope.AsignarOficina = function (ModalOficina, opt) {
                var Continuar = true;
                $scope.MensajesErrorSeleccionOficina = [];
                if (ModalOficina == undefined || ModalOficina == null || ModalOficina == '') {
                    $scope.MensajesErrorSeleccionOficina.push('Debe seleccionar una oficina');
                    Continuar = false;
                } else {
                    if (ModalOficina.Codigo == 0) {
                        $scope.MensajesErrorSeleccionOficina.push('Debe seleccionar una oficina');
                        Continuar = false;
                    }
                }
                if (Continuar) {
                    $scope.Sesion.UsuarioAutenticado.Oficinas = ModalOficina
                    setCookie('Oficina', ModalOficina.Codigo, 1)

                    //------------------------------Documentos Proximos a vencer
                    var PerfilAdministrador = false;
                    for (var i = 0; i < $scope.Sesion.UsuarioAutenticado.GruposUsuarios.length; i++) {
                        var PerfilUsuario = $scope.Sesion.UsuarioAutenticado.GruposUsuarios[i];
                        if (PerfilUsuario.GrupoUsuarios.Nombre == "ADMINISTRADORES") {
                            PerfilAdministrador = true;
                            break;
                        }
                    }
                    if (PerfilAdministrador == true && $scope.Sesion.UsuarioAutenticado.ManejoDocumentosProximosVencer == true &&
                        ($scope.Sesion.UsuarioAutenticado.AplicacionUsuario == CODIGO_APLICACION_APLICATIVO || $scope.Sesion.UsuarioAutenticado.AplicacionUsuario == CODIGO_APLICACION_TODAS)) {
                        DocumentosProximosVencer();
                    }

                    if (opt > 0) {
                        ShowSuccess('El cambio de oficina se realizó correctamente')
                        closeModal('modalCambioOficina')
                    } else {
                        $('#ImgFondoMain').hide()
                        $scope.Autorizado = true;
                        $scope.NoAutorizado = false;
                        document.location.href = $scope.URL;
                        $scope.MarginTop = $('#header').height()
                        $timeout(function () {
                            $scope.Redimencionar()
                        }, 1000)
                    }
                }
            }
            //------ Cerrar Sesion
            $scope.CerrarSesion = function () {
                setCookie('UsuarioEmpresa', '')
                setCookie('Identificador', '')
                setCookie('Contrasena', '')
                setCookie('Oficina', '')
                $scope.Autorizado = false;
                $scope.NoAutorizado = true;
                //$scope.Sesion = { UsuarioAutenticado: {}, Identificador: '', Contrasena: '', FilterLst: RetornarListaAutocomplete };
                $scope.EstiloContenido = '';
                //document.location.href = '#';
                $('#baner').hide()
                authFactory.logOut();
                authFactory.autauthentication;
                $('#SeccionLogin').show();
                $('#SeccionOficina').hide();
                $('#ImgFondoMain').show()
                if (screen.width > 1024) {
                    $scope.FondoPantalla = false;
                    //$scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_V2_' + Prefijo + '.jpg'
                    $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_V2_COLVI.jpg'
                }
                else if (screen.width < 1024) {
                    $scope.FondoPantalla = true;
                    $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_Movil.jpg'
                }
                else if (screen.width == 1024) {
                    $scope.FondoPantalla = false;
                    $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_V2_COLVI.jpg'
                    //$scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_V2_' + Prefijo + '.jpg'
                }
                $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_' + $scope.Empresa.Prefijo + '.jpg'
                SeguridadUsuariosFactory.CerrarSesion($scope.Sesion.UsuarioAutenticado).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            //$scope.Autorizado = false;
                            //$scope.NoAutorizado = true;
                            $scope.Sesion = { UsuarioAutenticado: {}, Identificador: '', Contrasena: '', FilterLst: RetornarListaAutocomplete };
                            //$scope.EstiloContenido = '';
                            //document.location.href = '#';
                            //$('#baner').hide()
                            //authFactory.logOut();
                            //authFactory.autauthentication;
                            //$('#ImgFondoMain').show()
                            //if (screen.width > 1024) {
                            //    $scope.FondoPantalla = false;
                            //    //$scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_V2_' + Prefijo + '.jpg'
                            //    $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_V2_COLVI.jpg'
                            //}
                            //else if (screen.width < 1024) {
                            //    $scope.FondoPantalla = true;
                            //    $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_Movil.jpg'
                            //}
                            //else if (screen.width == 1024) {
                            //    $scope.FondoPantalla = false;
                            //    $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_V2_COLVI.jpg'
                            //    //$scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_V2_' + Prefijo + '.jpg'
                            //}
                            //$scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_' + $scope.Empresa.Prefijo + '.jpg'

                        }
                    });
            };

            $scope.MostrarModalCambio = function () {
                showModal('modalCambioContraseña');
            }
            $scope.MostrarModalCambioOficina = function () {
                $scope.MensajesErrorSeleccionOficina = []
                showModal('modalCambioOficina');
            }
            $scope.CerrarModalCambio = function () {
                closeModal('modalCambioContraseña');
            }
            $scope.CambiarContrasena = function () {
                $scope.MensajesErrorCambioClave = []
                if ($scope.DatosRequeridosContrasena()) {
                    var filtro = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.Sesion.UsuarioAutenticado.Codigo,
                        Clave: $scope.Clave,
                        OpcionCambioClave: 1,
                        Habilitado: CODIGO_UNO
                    }
                    UsuariosFactory.Guardar(filtro)
                        .then(function (response) {
                            if (response.data.ProcesoExitoso == true) {
                                if (response.data.Datos > 0) {
                                    ShowSuccess('La contraseña se actualizó correctamente');
                                    closeModal('modalCambioContraseña');
                                    $scope.Clave = '';
                                    $scope.ClaveAnterior = '';
                                    $scope.ConfirClave = '';
                                    $scope.CambioObligatorio = false
                                    document.getElementById('Mensajecambio').innerHTML = ''
                                    $scope.MensajesErrorCambioClave = []
                                    $scope.CerrarSesion()
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            $scope.DatosRequeridosContrasena = function () {
                var PuedeGuardar = true

                if ($scope.ClaveAnterior == undefined || $scope.ClaveAnterior == '') {
                    $scope.MensajesErrorCambioClave.push('Debe ingresar la contraseña actual');
                    PuedeGuardar = false;
                } else {
                    if ($scope.ClaveAnterior != $scope.Sesion.UsuarioAutenticado.Clave) {
                        $scope.MensajesErrorCambioClave.push('La contraseña actual no es correcta');
                        PuedeGuardar = false;
                    }
                }
                if ($scope.Clave == undefined || $scope.Clave == '') {
                    $scope.MensajesErrorCambioClave.push('Debe ingresar la contraseña nueva');
                    PuedeGuardar = false;
                }
                else {
                    if ($scope.Clave == $scope.Sesion.UsuarioAutenticado.Clave) {
                        $scope.MensajesErrorCambioClave.push('La contraseña nueva no puede ser igual a la anterior');
                        PuedeGuardar = false;
                    } else {
                        if (validarContrasena($scope.Clave) == false) {
                            $scope.MensajesErrorCambioClave.push('La contraseña debe ser mínimo de 6 caracteres, debe contener al menos un caracter especial, un número y no debe tener espacios en blanco, recuerde que los los caracteres & y + no estan permitidos');
                            PuedeGuardar = false;
                        }
                        else {
                            if ($scope.ConfirClave != $scope.Clave) {
                                $scope.MensajesErrorCambioClave.push('La nueva contraseña y la confirmación deben coincidir');
                                PuedeGuardar = false;
                            }
                        }
                    }
                }
                return PuedeGuardar
            }

            $scope.VerificarAutocomplete = function (Object) {
                if (Object == '' || Object == undefined || Object == null) {
                    return Object = ''
                } else if ((Object.Codigo == '' || Object.Codigo == undefined || Object.Codigo == null || Object.Codigo == 0 || Object.Codigo == '0') && (Object.Numero == '' || Object.Numero == undefined || Object.Numero == null || Object.Numero == 0 || Object.Numero == '0')) {
                    //ShowNotificacionError('No se encontró el elemento')
                    return Object = ''
                } else {
                    return Object = Object
                }
            }
            $scope.CargarTercero = function (Codigo) {
                if (Codigo > CERO) {
                    return TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, Sync: true }).Datos[0]
                }
            }
            $scope.CargarTerceroIdentificacion = function (Identifiacion) {
                if (Identifiacion !== '' && Identifiacion !== undefined) {
                    return TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: Identifiacion, Sync: true }).Datos[0]
                }
            }
            $scope.CargarColor = function (Codigo) {
                if (Codigo > CERO) {
                    return ColorVehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, Sync: true }).Datos[0]
                }
            }
            $scope.CargarCiudad = function (Codigo) {
                if (Codigo > CERO) {
                    return CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, Sync: true }).Datos[0]
                }
            }
            $scope.CargarVehiculos = function (Codigo) {
                if (Codigo > CERO) {
                    return VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, Sync: true }).Datos[0]
                }
            }
            $scope.CargarVehiculosPlaca = function (Placa) {
                if (Placa !== undefined && Placa !== '') {
                    return VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: Placa, Sync: true }).Datos[0]
                }
            }
            $scope.CargarProducto = function (Codigo) {
                if (Codigo > CERO) {
                    return ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, AplicaTarifario: -1 , Sync: true }).Datos[0]
                }
            }
            $scope.CargarSemirremolque = function (Codigo) {

                return SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Placa: Codigo, Sync: true }).Datos[0]

            }
            $scope.CargarSemirremolqueCodigo = function (Codigo) {
                if (Codigo > CERO) {
                    return SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, Sync: true }).Datos[0]
                }
            }
            $scope.MaskNumero = function () {
                MascaraNumeroGeneral($scope)
            };
            $scope.MaskMayus = function () {
                MascaraMayusGeneral($scope)
            };
            $scope.MaskValores = function () {
                MascaraValoresGeneral($scope)
            };
            $scope.MaskHora = function () {
                MascaraHorasGeneral($scope)
            };
            $scope.Redimencionar = function () {
                try {
                    $scope.MarginTop = $('#header').height()
                    $timeout(function () {
                        for (var i = 0; i < $('#contentwrapper')[0].attributes.length; i++) {
                            if ($('#contentwrapper')[0].attributes[i].name == 'style') {

                                $scope.MaxHeigth = ($('.main-sidebar').height() - 51).toString() + 'px;'
                                $('#contentwrapper')[0].attributes[i].value = ('max-height: ' + $scope.MaxHeigth + ';min-height:' + $scope.MaxHeigth + ';')
                            }
                        }
                    }, 500)
                } catch (e) {

                }

            }
            $(window).resize(function () {
                try {
                    $scope.MarginTop = $('#header').height()
                    $timeout(function () {
                        for (var i = 0; i < $('#contentwrapper')[0].attributes.length; i++) {
                            if ($('#contentwrapper')[0].attributes[i].name == 'style') {

                                $scope.MaxHeigth = ($('.main-sidebar').height() - 51).toString() + 'px;'
                                $('#contentwrapper')[0].attributes[i].value = ('max-height: ' + $scope.MaxHeigth + ';min-height:' + $scope.MaxHeigth + ';')
                            }
                        }
                    }, 500)
                } catch (e) {

                }

            }
            );
            $scope.OrdenarPor = function (Lista, Objeto, Atributo, Atributo2) {
                OrderBy(Objeto, Atributo, Lista, Atributo2);
            }


            $scope.MostrarMensajeErrorModal = function (MostrarMensajeError) {
                if (MostrarMensajeError == true) {
                    MostrarMensajeError = false
                } else {
                    MostrarMensajeError = true
                }
                return MostrarMensajeError
            }
            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }

            function getCookie(cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }

            function checkCookie() {
                var user = getCookie("username");
                if (user != "") {

                } else {
                    user = prompt("Please enter your name:", "");
                    if (user != "" && user != null) {
                        setCookie("username", user, 365);
                    }
                }
            }


            //$interval(function () {
            //    if ($scope.Sesion.UsuarioAutenticado.AplicacionUsuario != CODIGO_APLICACION_GESPHONE) {
            //        try {
            //            $('body')[0].style = '';
            //        } catch (e) {
            //        }
            //        if ($scope.Sesion.UsuarioAutenticado.CodigoEmpresa != undefined && $scope.Sesion.UsuarioAutenticado.Codigo != undefined) {
            //            var filtroUsuario = {
            //                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            //                UsuarioNotificacion: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            //                Sync: true
            //            };
            //            var notificaciones = NotificacionesFactory.Consultar(filtroUsuario).Datos;
            //            for (var i = 0; i < notificaciones.length; i++) {
            //                MostrarNotificación(notificaciones[i].Mensaje, $scope.ImagenLogo);
            //            }
            //        }
            //    }
            //}, 15000);

            //var ListadoPermisoNotificaciones = [];

            $scope.Notificaciones = 0
            $scope.ListadoNotificaciones = []
            blockUIConfig.autoBlock = false;
            $interval(function () {
                if ($scope.Sesion.UsuarioAutenticado.AplicacionUsuario != CODIGO_APLICACION_GESPHONE) {
                    if ($scope.Sesion.UsuarioAutenticado.CodigoEmpresa != undefined && $scope.Sesion.UsuarioAutenticado.Codigo != undefined) {
                        try {
                            if ($('#PanelMensajes').attr('class') != 'dropdown user user-menu open') {
                                if (!((document.activeElement.type == "textarea" || document.activeElement.type == "text" || document.activeElement.type == "select-one" || document.activeElement.type == "date") && $('body')[0].className.includes("modal-open"))) {
                                    //ValidarNotificaciones();
                                    $scope.ConsultarNofiticaciones()
                                }
                            }
                        } catch (e) {

                        }
                    }
                }
            }, 30000);

           
         
            

            $scope.ConsultarNofiticaciones = function (e) {
                $scope.ListadoNotificaciones = [];
                //$scope.ValidarPermisoSeguimiento = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_AUTORIZACIONES);
                //$scope.PermisosSeguimiento = ValidarPermisos($scope.ValidarPermisoSeguimiento);
                //if (!$scope.PermisosSeguimiento.DeshabilitarConsulta) {
                    var Filtro = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        EstadoAutorizacion: { Codigo: 18301 },
                        Notificacion: 1,
                        Usuario: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    }
                    blockUIConfig.autoBlock = false;
                    AutorizacionesFactory.Consultar(Filtro).
                        then(function (response) {

                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoNotificaciones = response.data.Datos
                                    $scope.Notificaciones = $scope.ListadoNotificaciones.length
                                }
                                else {
                                    $scope.Notificaciones = 0
                                }
                            }
                        }, function (response) {
                            $scope.Buscando = false;
                        });
                //}
            }
            $scope.LidatosAcciones = [
                { Nombre: 'AUTORIZAR', Codigo: 18302 },
                { Nombre: 'RECHAZAR', Codigo: 18303 }
            ]
            $scope.LidatosAcciones2 = [
                { Nombre: 'RESTABLECER TURNO', Codigo: 18302 },
                { Nombre: 'ANULAR TURNO', Codigo: 18303 }
            ]
            $scope.GestionarSolicitud = function (item) {
                if (item.Autorizar > 0) {
                    $scope.TempItem = item
                    $scope.EstadoAcccion = $scope.LidatosAcciones[0]
                    $scope.Observaciones = ''
                    $('#NuevoDocumento').hide()
                    $('#BotonNuevo').show()
                    showModal('ModalGestionarSolicitud')
                    var entidad = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.TempItem.Codigo,
                    }
                    AutorizacionesFactory.Obtener(entidad).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Solicitud = response.data.Datos;
                                if ($scope.Solicitud.TipoAutorizacion.Codigo == 18404) {
                                    $scope.EstadoAcccion = $scope.LidatosAcciones2[0]
                                }
                                try {
                                    $scope.Solicitud.ValorDeclaradoAutorizacion = MascaraValores($scope.Solicitud.ValorDeclaradoAutorizacion)
                                    $scope.Solicitud.Valor = MascaraValores(parseInt($scope.Solicitud.Valor))

                                } catch (e) {

                                }
                            }
                        }, function (response) {
                            $scope.Buscando = false;
                        });

                    if ($scope.TempItem.TipoAutorizacion.Codigo == 18407) {
                        $scope.HabilitarEliminarDocumento = false;
                        var ListadoConceptosAutorizacion = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 230 }, Sync: true }).Datos;
                        $scope.ListadoConceptosValorDeclarado = [];

                        ListadoConceptosAutorizacion.forEach(itemConcepto => {
                            $scope.ListadoConceptosValorDeclarado.push({
                                Concepto: itemConcepto,
                                Valor: '',
                                Facturar: false,
                                Seleccionado: false
                            });
                        });

                        


                    } else if ($scope.TempItem.TipoAutorizacion.Codigo == 18408/*No Cumplimiento Inspección*/) {
                        var entidad = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Codigo: $scope.TempItem.Codigo,
                            Sync: true
                        }
                        var ResponseAutorizacion = AutorizacionesFactory.Obtener(entidad).Datos;
                        if (ResponseAutorizacion != undefined) {


                            var ResponseInspeccion = InspeccionPreoperacionalFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: MascaraNumero(ResponseAutorizacion.NumeroDocumento), Sync: true }).Datos;
                            if (ResponseInspeccion != undefined) {
                                $scope.NumeroInspeccion = ResponseInspeccion.Numero
                                $scope.Planilla = ResponseInspeccion.Planilla.NumeroDocumento;
                                $scope.OrdenCargue = ResponseInspeccion.OrdenCargue.NumeroDocumento;
                                $scope.Manifiesto = ResponseInspeccion.Planilla.Manifiesto.NumeroDocumento;
                            }
                        }
                    }
                } else {
                    ShowError('No tiene permisos para autorizar esta notificación');
                }
            }
            $scope.ActualizarSolicitud = function () {
                if ($scope.Solicitud.TipoAutorizacion.Codigo == 18402 && $scope.EstadoAcccion.Codigo == 18302 && ($scope.Solicitud.Valor == 0 || $scope.Solicitud.Valor == undefined)) {
                    ShowError('Por favor ingrese el anticipo')
                } else if ($scope.Solicitud.TipoAutorizacion.Codigo == 18407) {
                    var con = 0;
                    if (DatosRequeridosAutorizacionValorDeclarado()) {
                        var DetallesAutorizacion = []
                        $scope.ListadoConceptosValorDeclarado.forEach(itemConcepto => {
                            
                            if (itemConcepto.Seleccionado == true) {
                                con++;
                                DetallesAutorizacion.push({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Codigo: itemConcepto.Concepto.Codigo,
                                    Valor: itemConcepto.Valor == undefined || itemConcepto.Valor == '' ? 0 : MascaraNumero(itemConcepto.Valor),
                                    Facturar: itemConcepto.Facturar == true ? 1 : 0
                                });
                            }
                        });
                        
                        if (con > 0) {

                            var entidad = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Codigo: $scope.TempItem.Codigo,
                                EstadoAutorizacion: $scope.EstadoAcccion,
                                TipoAutorizacion: { Codigo: 18407 },
                                Observaciones: $scope.Solicitud.ObservacionSolicita,
                                UsuarioGestion: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                Valor: MascaraNumero($scope.Solicitud.ValorDeclaradoAutorizacion),
                                DetalleConceptos: DetallesAutorizacion,
                                Documento: $scope.DocumentoAutorizacionValorDeclarado

                            }
                            AutorizacionesFactory.Guardar(entidad).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos > 0) {
                                            closeModal('ModalGestionarSolicitud');
                                            ShowSuccess('La solicitud se gestionó correctamente');
                                            $scope.DocumentoAutorizacionValorDeclarado = {};
                                        }
                                        else {
                                            ShowError('No se logro gestionar la solicitud')
                                        }
                                    }
                                }, function (response) {
                                    $scope.Buscando = false;
                                });
                        } else {
                            ShowError('Debe seleccionar al menos un concepto')
                        }
                    }
                } else {
                    var entidad = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.TempItem.Codigo,
                        EstadoAutorizacion: $scope.EstadoAcccion,
                        Observaciones: $scope.Observaciones,
                        UsuarioGestion: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Valor: $scope.Solicitud.Valor == "" ? 0 : MascaraNumero($scope.Solicitud.Valor)
                    }
                    AutorizacionesFactory.Guardar(entidad).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos > 0) {
                                    ShowSuccess('La solicitud se gestionó correctamente')
                                    closeModal('ModalGestionarSolicitud')
                                }
                                else {
                                    ShowError('No se logro gestionar la solicitud')
                                }
                            }
                        }, function (response) {
                            $scope.Buscando = false;
                        });
                }
            }

            $scope.QuitarModals = function () {
                $('body').removeClass('modal-open')
            }
            //$scope.FechaUltimaActividad = new Date()
            $scope.RegistrarActividadUsuario = function () {
                if ($scope.Sesion.UsuarioAutenticado.Codigo !== undefined) {
                    setCookie('FechaUltimaActividad', new Date(), 1)
                    //$scope.FechaUltimaActividad = new Date()

                }
            }
            var DocumentosTercerosCargados = false;
            var DocumentosVehiculosCargados = false;
            var TimerModalShowDocumentosVencer;
            function DocumentosProximosVencer() {
                DocumentosTercerosCargados = false;
                DocumentosVehiculosCargados = false;

                TercerosFactory.ConsultarDocumentosProximosVencer({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoAplicativo: CODIGO_APLICACION_APLICATIVO }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            DocumentosTercerosCargados = true;
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDocumentoProximosVencerTerceros = response.data.Datos;

                                $scope.ListadoDocumentoProximosVencerTerceros.forEach(function (item) {
                                    if (item.CantidadDiasFaltantes == '0') {
                                        item.CantidadDiasFaltantes = 'VENCIDO';
                                    }
                                })
                            }
                            else {
                                $scope.ListadoDocumentoProximosVencerTerceros = [];
                            }
                        }
                    }, function (response) {
                        DocumentosTercerosCargados = true;
                        ShowError(response.statusText);
                    });

                VehiculosFactory.ConsultarDocumentosProximosVencer({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoAplicativo: CODIGO_APLICACION_APLICATIVO }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            DocumentosVehiculosCargados = true;
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDocumentoProximosVencerVehiculos = response.data.Datos;

                                $scope.ListadoDocumentoProximosVencerVehiculos.forEach(function (item) {
                                    if (item.CantidadDiasFaltantes == '0') {
                                        item.CantidadDiasFaltantes = 'VENCIDO';
                                    }
                                })
                            }
                            else {
                                $scope.ListadoDocumentoProximosVencerVehiculos = [];
                            }
                        }
                    }, function (response) {
                        DocumentosVehiculosCargados = true;
                        ShowError(response.statusText);
                    });

                MostrarModalDocumentosProximosVencer();

            }

            function MostrarModalDocumentosProximosVencer() {
                if (DocumentosTercerosCargados == true && DocumentosVehiculosCargados == true) {
                    clearTimeout(TimerModalShowDocumentosVencer);
                    showModal('modalDocumentoProximosVencer');
                }
                else {
                    clearTimeout(TimerModalShowDocumentosVencer);
                    TimerModalShowDocumentosVencer = setTimeout(MostrarModalDocumentosProximosVencer, 100);
                }
            }

            $interval(function () {
                if ($scope.Sesion.UsuarioAutenticado.Codigo !== undefined && $scope.Sesion.UsuarioAutenticado.Codigo != 1) {
                    if (parseInt((((new Date()) - new Date(getCookie('FechaUltimaActividad'))) / 1000 / 60)) <= 5) {
                        UsuariosFactory.RegistrarActividad({ Codigo: $scope.Sesion.UsuarioAutenticado.Codigo, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).then(function (response) {
                        });
                    }
                    if (parseInt((((new Date()) - new Date(getCookie('FechaUltimaActividad'))) / 1000 / 60)) == 25) {
                        ShowError('Su sesión se cerrara en 5 minutos por causa de inactividad')
                    }
                    else if (parseInt((((new Date()) - new Date(getCookie('FechaUltimaActividad'))) / 1000 / 60)) == 27) {
                        ShowError('Su sesión se cerrara en 3 minutos por causa de inactividad')
                    }
                    if (parseInt((((new Date()) - new Date(getCookie('FechaUltimaActividad'))) / 1000 / 60)) >= 30) {
                        ShowError('Su sesión fue finalizada por causa de inactividad')
                        $scope.CerrarSesion();
                    } else {

                    }
                }
            }, 60000);


            //NotificacionesValorDeclarado:
            function DatosRequeridosAutorizacionValorDeclarado() {
                var cont = true
                //if ($scope.DocumentoAutorizacionValorDeclarado == {}) {
                //    cont = false;
                //}
                                
                return cont
            }

            $scope.DocumentoAutorizacionValorDeclarado = {};
            $scope.ListadoDocumentosValorDeclarado = [];

            $scope.MarcarConceptos = function(item){
               
                if ($scope.ListadoConceptosValorDeclarado != undefined) {
                    if ($scope.ListadoConceptosValorDeclarado.length > 0) {
                        $scope.ListadoConceptosValorDeclarado.forEach(itemConcepto => {
                            if (itemConcepto.Concepto.Codigo != 23001) {
                                itemConcepto.Seleccionado = item;
                            } else if (item == true) {
                                itemConcepto.Seleccionado = false;
                            }
                        });
                    }
                }
                
            }

            $scope.FacturarTodos = function(item){

                if ($scope.ListadoConceptosValorDeclarado != undefined) {
                    if ($scope.ListadoConceptosValorDeclarado.length > 0) {
                        $scope.ListadoConceptosValorDeclarado.forEach(itemConcepto => {
                            if (itemConcepto.Concepto.Codigo != 23001) {
                                itemConcepto.Facturar = item;
                            } else if(item == true){
                                itemConcepto.Facturar = false;
                            }

                        });
                    }
                }

            }

            $scope.ValidarCheck = function(item) {
                if (item.Concepto.Codigo == 23001 && item.Seleccionado) {
                    $scope.ListadoConceptosValorDeclarado.forEach(itemConcepto => {
                        if (itemConcepto.Concepto.Codigo != 23001) {
                            itemConcepto.Seleccionado = false;
                        }
                    });
                } else if (item.Concepto.Codigo != 23001 && item.Seleccionado) {
                    $scope.ListadoConceptosValorDeclarado.forEach(itemConcepto => {
                        if (itemConcepto.Concepto.Codigo == 23001) {
                            itemConcepto.Seleccionado = false;
                        }
                    });
                }
            }

            $scope.DocumentoValorDeclarado = {
                Nombre: '',
                NombreDocumento: '',
                ExtensionDocumento: ''
            };

            $scope.AgregarDocumento = function () {
                $scope.ListadoDocumentosValorDeclarado.push($scope.DocumentoValorDeclarado);
            }

            $scope.CadenaFormatos = ".pdf";

            var regExt = /(?:\.([^.]+))?$/;
            
            $scope.CargarArchivo = function (element) {
                var continuar = true;
                if (element.files.length > 0) {
                    

                    if (element === undefined) {
                        ShowError("Archivo invalido, por favor verifique el formato del archivo  /nFormatos permitidos: 0");
                        continuar = false;
                    }
                    if (continuar == true) {
                        if (element.files[0].size >= 3000000) //3 MB
                        {
                            ShowError("El archivo " + element.files[0].name + " supera los " + "3 MB" + ", intente con otro archivo", false);
                            continuar = false;
                        }
                        var Formatos = $scope.CadenaFormatos.split(',');
                        var cont = 0;
                        var ExtencionArchivo = regExt.exec(element.files[0].name)[1];
                        for (var i = 0; i < Formatos.length; i++) {
                            Formatos[i] = Formatos[i].replace(' ', '');
                            //var Extencion = element.files[0].name.split('.');
                            if ('.' + (ExtencionArchivo.toUpperCase()) == Formatos[i].toUpperCase()) {
                                cont++;
                            }
                        }
                        if (cont == 0) {
                            ShowError("Archivo invalido, por favor verifique el formato del archivo.  Formatos permitidos: (" + $scope.CadenaFormatos + ")");
                            continuar = false;
                        }
                    }

                    if (continuar == true) {
                        
                        var reader = new FileReader();
                        $scope.DocumentoValorDeclarado.ArchivoCargado = element.files[0];
                        reader.onload = AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                        
                    }
                }
            };

            function AsignarArchivo(e) {
                $scope.$apply(function () {
                    
                    $scope.DocumentoValorDeclarado.Archivo = e.target.result.replace('data:' + $scope.DocumentoValorDeclarado.ArchivoCargado.type + ';base64,', '');
                    $scope.DocumentoValorDeclarado.Tipo = $scope.DocumentoValorDeclarado.ArchivoCargado.type;
                    $scope.DocumentoValorDeclarado.Nombre = $scope.DocumentoValorDeclarado.ArchivoCargado.name;
                    $scope.DocumentoValorDeclarado.NombreDocumento = $scope.DocumentoValorDeclarado.ArchivoCargado.name.substr(0, $scope.DocumentoValorDeclarado.ArchivoCargado.name.lastIndexOf('.'));
                    $scope.DocumentoValorDeclarado.ExtensionDocumento = regExt.exec($scope.DocumentoValorDeclarado.ArchivoCargado.name)[1];
                    //Bloqueo Pantalla
                    blockUIConfig.autoBlock = true;
                    blockUIConfig.delay = 0;
                    blockUI.start("Cargando Documento...");
                    $timeout(function () {
                        blockUI.message("Cargando Documento...");
                        InsertarDocumentoValorDeclarado();
                    }, 100);
                    //Bloqueo Pantalla
                });
            }
            function InsertarDocumentoValorDeclarado() {
                // Guardar Archivo temporal
                $scope.DocumentoAutorizacionValorDeclarado = {};
                $scope.DocumentoAutorizacionValorDeclarado = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Nombre: $scope.DocumentoValorDeclarado.NombreDocumento,
                    Archivo: $scope.DocumentoValorDeclarado.Archivo,
                    Tipo: $scope.DocumentoValorDeclarado.Tipo,
                    Extension: $scope.DocumentoValorDeclarado.ExtensionDocumento,
                    Sync: true
                };

               //$scope.ListadoDocumentosValorDeclarado.push(Documento);
                $scope.HabilitarEliminarDocumento = true;
                blockUI.stop();
            }

            $scope.EliminarArchivo = function () {
                $scope.DocumentoAutorizacionValorDeclarado = {};
            }

                

            //Fin Notificaciones Valor Declarado:

            $scope.MaskValoresGrid = function (item) {
                return MascaraValores(item);
            }

        }]);
})();
