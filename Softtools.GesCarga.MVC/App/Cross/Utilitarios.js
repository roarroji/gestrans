﻿//--------------------------------------------------------------------------------------------------------//
//Configure la URL donde se hospeda el proyecto ASP.
function ObtenerURLProyectoASP(URLAsp) {
    return URL_ASP;
}
//---------------------- Nota: por favor no colocar nada debajo de esta función ----------------------------//

/* Agregar Validaciones */
function MaskAlfanumericoEspacio(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /^[a-zA-Z0-9 ]{1,80}$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function MaskNumerico(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function MaskTexto(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /^[a-zA-Z]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function MaskCorreo(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /^[a-zA-Z0-9@.]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function MaskTextoEspacio(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /^[a-zA-Z ]{1,80}$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

//function validarEmail(email) {
//    var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//    if (!expr.test(email)) {
//        return false;
//    }
//    else {
//        return true;
//    }
//}

function RetornarFechaSinHora(fecha) {
    var fecha = new Date(fecha);
    fecha.setHours(0);
    fecha.setMinutes(0);
    fecha.setSeconds(0);
    fecha.setMilliseconds(0);

    return fecha;
}

function validarContrasena(contrasena) {
    var exprletras = /[ABCDEFGHIJKLMÑOPQRSTUVWXYSabcdefghijklmnñopqrstuv]/
    var exprnumeros = /[0123456789]/
    var expcarcateres = /[\/\*\-)\(\%\$\#\¿\?\"\.\{\!\}\¡\=\[\]]/;
    var countnum = 0
    var countcaracter = 0
    var countletra = 0
    for (var i = 0; i < contrasena.length; i++) {
        if (exprletras.test(contrasena[i])) {
            countletra++
        } else if (exprnumeros.test(contrasena[i])) {
            countnum++
        } else if (expcarcateres.test(contrasena[i])) {
            countcaracter++
        } else if (contrasena[i] == '&' || contrasena[i] == '+') {
            return false
        }
    }
    if (contrasena.length < 5) {
        return false
    }
    if (countcaracter > 0 && countletra > 0 && countnum > 0) {
        return true
    } else {
        return false
    }
}

function MaskContrasena(contrasena) {
    var exprletras = /[ABCDEFGHIJKLMÑOPQRSTUVWXYSabcdefghijklmnñopqrstuv]/
    var exprnumeros = /[0123456789]/
    var expcarcateres = /[\/\*\-)\(\%\$\#\¿\?\"\.\{\!\}\¡\=\[\]]/;
    var countnum = 0
    var countcaracter = 0
    var countletra = 0
    for (var i = 0; i < contrasena.length; i++) {
        if (exprletras.test(contrasena[i])) {
            countletra++
        } else if (exprnumeros.test(contrasena[i])) {
            countnum++
        } else if (expcarcateres.test(contrasena[i])) {
            countcaracter++
        } else if (contrasena[i] == '&' || contrasena[i] == '+') {
            return false
        }
    }
    if (contrasena.length < 5) {
        return false
    }
    if (countcaracter > 0 && countletra > 0 && countnum > 0) {
        return true
    } else {
        return false
    }
}


function MaskHoraMilitar(hora) {
    var expr = /^([0-1][0-9]|2[0-3]):[0-5][0-9]$/;
    if (!expr.test(hora)) {
        return false;
    }
    else {
        return true;
    }
}


function checkEmail(field) {
    if (!validarEmail(field.val().trim())) {
        field.parent().parent().addClass('has-error');
        return false;
    } else {
        field.parent().parent().removeClass('has-error');
    }
    return true;
}

function checkVariosEmail(field) {

    var success = true;

    if (field.val() == '') {
        field.parent().parent().addClass('has-error');
        success = false;
    } else {
        var correos = field.val().trim();
        var lista = correos.split(",");

        lista.forEach(function (correo) {
            if (!validarEmail(correo.trim())) {
                field.parent().parent().addClass('has-error');
                success = false;
            }
        });

        if (success == true) {
            field.parent().parent().removeClass('has-error');
        }
    }
    return success;
}

function MascaraDecimalesGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            try {


                var Model = ''
                var Lista = ''
                var conitinue = false
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskDecimales()')) {
                        conitinue = true
                        break;
                    }
                } if (conitinue) {

                    for (var j = 0; j < a[i].attributes.length; j++) {
                        if (a[i].attributes[j].name == 'ng-model') {
                            Model = a[i].attributes[j].value

                        } else if (a[i].attributes[j].name == 'lista') {
                            Lista = a[i].attributes[j].value
                        }
                    }

                    var b = Model.split('.')
                    switch (b.length) {
                        case 1:
                            if (sc[b[0]] !== undefined) {
                                try { sc[b[0]] = MascaraDecimales(sc[b[0]]) } catch (e) { }
                            }
                            break;
                        case 2:
                            if (sc[b[0]][b[1]] !== undefined) {
                                try { sc[b[0]][b[1]] = MascaraDecimales(sc[b[0]][b[1]]) } catch (e) { }
                            }
                            break;
                        case 3:
                            if (sc[b[0]][b[1]][b[2]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]] = MascaraDecimales(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                            }
                            break;
                        case 4:
                            if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraDecimales(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                            }
                            break;
                        case 5:
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraDecimales(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                            }
                            break;
                        case 6:
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraDecimales(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                            }
                            break;
                        case 7:
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraDecimales(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                            }
                            break;
                    }

                }
            } catch (e) {

            }
        }
    }
}

function MascaraDecimales(option, CantidadDecimales) {
    option = option.toString()
    var numeroarray = []
    var numero = ''
    var contpunto = 0
    for (var i = 0; i < option.length; i++) {
        patron = /[0123456789/.]/
        if (i == 0 && option[i] == '-') {
            numeroarray.push(option[i])
        }
        else if (patron.test(option[i])) {
            if (patron.test(option[i]) == ' ') {
                option[i] = '';
            } else {
                if (option[i] == '.') {
                    if (contpunto == 0) {
                        numeroarray.push(option[i])
                    }
                    contpunto++

                } else {
                    numeroarray.push(option[i])
                }
            }
        }
    }

    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }

    if (CantidadDecimales > 0) {
        var arregloNumeros = numero.split('')
        var PsocionPunto = 0
        numero = ''
        for (var i = 0; i < arregloNumeros.length; i++) {
            if (arregloNumeros[i] == '.') {
                PsocionPunto = i
            }
            if (PsocionPunto > 0) {
                if (!(i <= (PsocionPunto + CantidadDecimales))) {
                    arregloNumeros[i] = ''
                }
            }
            numero = numero + arregloNumeros[i]
        }

    }
    return numero
}

function checkField(field) {
    if (field.val() == '' || field.val() == '0') {
        field.parent().parent().addClass('has-error');
        return false;
    } else {
        field.parent().parent().removeClass('has-error');
    }
    return true;
}

function checkddl(field) {
    if (field.attr('value') == '' || field.attr('value') == '0') {
        field.parent().parent().addClass('has-error');
        return false;
    } else {
        field.parent().parent().removeClass('has-error');
    }
    return true;
}

/*Obtener rango de fechas
/JO: 20/05/2016

Entrada: Fecha Inicial y Fecha Final
Salida: Rango en días como numero entero*/

var MILISENGUNDOS_POR_DIA = 1000 * 60 * 60 * 24;

function RetornarRangoFecha(FechaInicial, FechaFinal) {
    var dia1 = new Date(FechaInicial);
    var dia2 = new Date(FechaFinal);
    return CalcularRangoFechas(dia1, dia2);

}
function CalcularRangoFechas(a, b) {
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
    return Math.floor((utc2 - utc1) / MILISENGUNDOS_POR_DIA);
}

Array.prototype.unique = function (a) {
    return function () { return this.filter(a) }
}(function (a, b, c) {
    return c.indexOf(a, b + 1) < 0
});


function RetornarHoras(Fecha) {

    Fecha = new Date(Fecha)
    var arrHora = new Date(angular.copy(Fecha));
    var hora = arrHora.getHours()
    var minuto = arrHora.getMinutes()
    if (hora < 10 && hora >= 0) {
        hora = ("0" + hora)
    }
    if (minuto < 10 && minuto > 0) {
        minuto = ("0" + minuto)
    }
    if (minuto == 0) {
        minuto = (minuto + "0")
    }
    var horaMinutos = (hora + ":" + minuto);
    return horaMinutos;
}


/*----------------------------------------------*/
/*Gestionar Lista Detalle
/JO: 01/05/2016
Entrada: Pagina actual, Lista que llena el Grid
Salida: Lista detalle*/


function GuardarListaDetalle(ListaDetalle, paginaActual, ListadoGrid) {

    PaginaGuardada = 0;

    //Verifica si la página actual se encuentra guardada en alguna de las posiciones de ListaDetalle
    if (ListaDetalle.length > 0) {
        ListaDetalle.forEach(function (itemPaginas) {
            if (itemPaginas.CodigoPagina == paginaActual) {
                PaginaGuardada = 1;
            }
        });
    }

    //Si la página no se encuentra guardada, se procede a guardar el ID de la página (Posición de la página) 
    //y la lista con el resultado que contiene
    if (PaginaGuardada == 0) {

        ItemLista = {
            CodigoPagina: paginaActual,
            Lista: ListadoGrid // Aquí colocar la lista con la cual se carga el Grid
        }

        ListaDetalle.push(ItemLista);
    }
    return ListaDetalle;
}


/*Separar un nombre completo de terceros en Nombre y Apellido
JO: 01/07/2016
Entrada: Nombre completo del tercero
Salida: Arreglo separando cada palabra del nombre*/

function SepararNombresTerceros(NombreCompleto) {
    var ArrNombreTercero = [];
    var i = 0;
    var ArrNombreTercero = NombreCompleto.split(" ");
    return ArrNombreTercero;
}

/*Formatear Fecha
*Ingreso: Fecha.toString() , Formato Fecha (Ver formatos validos en Constantes.js)
Salida: Fecha con formato
JO: 18/07/2016*/
function Formatear_Fecha(Fecha, FormatoFecha) {

    var date = new Date(Fecha);
    var formato = {};
    formato.datejs = date.toString(FormatoFecha);
    var FechaRetorno = formato.datejs;
    return FechaRetorno

}

/*Validador de correo por medio de expresión regular - propiedad ng-pattern a nivel de vista 
EG: 01/08/2016*/
var validadorCorreo = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})((,\s?){1}[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))*$/;

function Formatear_Fecha_Dia_Mes_Ano(Fecha) {
    var arrFecha = Fecha
    var dia = arrFecha.getDate()
    var mes = arrFecha.getMonth()
    var año = arrFecha.getFullYear()
    mes = (mes + 1);
    var FechaResultado = (dia + "/" + mes + "/" + año);

    return FechaResultado;
}
function Formatear_Fecha_Mes_Dia_Ano(Fecha) {
    var arrFecha = Fecha
    var dia = arrFecha.getDate()
    var mes = arrFecha.getMonth()
    var año = arrFecha.getFullYear()
    mes = (mes + 1);
    var FechaResultado = (mes + "/" + dia + "/" + año);

    return FechaResultado;
}

/*Este metodo sirve para formatear fechas ya guardadas y evita la conversión según la ubicación del PC, es decir, que retorne una
fecha diferente a la guardada desfasando un día

Para llamar este metodo desde cualquier controlador lo hace de la forma: 
MiFecha = RetornarFechaEspecifica(response.data.Datos.fecha)

Utilicelo para mostrar datos

JO: 22/08/2016*/


function RetornarFechaEspecifica(fecha) {
    var temp = new Date(fecha);
    var minutos = temp.getTimezoneOffset();
    temp.setMinutes(temp.getMinutes() + minutos);
    return temp;
}
function RetornarFechaEspecificaSinTimeZone(fecha) {
    fecha = new Date(fecha);
    var minutos = fecha.getTimezoneOffset();
    fecha.setMinutes(fecha.getMinutes() - minutos);
    return fecha;
}
function RetornaFechaMasMes(fecha) {
    fecha = new Date(fecha);
    fecha.setMonth(temp.getMonth() + 1);
    return fecha;
}


function ValidarPermisos(permisos) {
    var ListadoPermisos = []
    //if (permisos.AplicaConsultar == PERMISO_ACTIVO) {
    if (permisos.AplicaPermisoConsultar == PERMISO_ACTIVO) {
        ListadoPermisos.DeshabilitarConsulta = false
    }
    else {
        ListadoPermisos.DeshabilitarConsulta = true
    }
    //if (permisos.AplicaEliminarAnular == PERMISO_ACTIVO) {
    if (permisos.AplicaPermisoEliminarAnular == PERMISO_ACTIVO) {
        ListadoPermisos.DeshabilitarEliminarAnular = false
    }
    else {
        ListadoPermisos.DeshabilitarEliminarAnular = true
    }
    //if (permisos.AplicaImprimir == PERMISO_ACTIVO) {
    if (permisos.AplicaPermisoImprimir == PERMISO_ACTIVO) {
        ListadoPermisos.DeshabilitarImprimir = false
    }
    else {
        ListadoPermisos.DeshabilitarImprimir = true
    }
    //if (permisos.AplicaActualizar == PERMISO_ACTIVO) {
    if (permisos.AplicaPermisoActualizar == PERMISO_ACTIVO) {
        ListadoPermisos.DeshabilitarActualizar = false
    }
    else {
        ListadoPermisos.DeshabilitarActualizar = true
    }

    return ListadoPermisos
}

//-------------------------Mascaras y validaciones-------------------------------
var temp = 0
function MascaraHora(option) {
    option = option.toString()
    var numeroarray = []
    var numero = ''
    if (option[0] == '-') {
        numero = ''
    } else {
        for (var i = 0; i < option.length; i++) {
            patron = /[0123456789]/
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    numeroarray.push(option[i])
                }
            }
        }

    }
    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    option = numero
    if (isNaN(option)) {
        return ''
    } else {
        if (option.length == 2) {
            if (event.inputType != 'deleteContentBackward') {
                if (parseInt(option) > 23) {
                    option = '23:'
                } else {
                    option = option + ':'
                }
            }
        }
        else {
            option = option.toString()
            var numeroarray = []
            var numero = ''
            if (option[0] == '-') {
                numero = ''
            } else {
                for (var i = 0; i < option.length; i++) {
                    patron = /[0123456789]/
                    if (patron.test(option[i])) {
                        if (patron.test(option[i]) == ' ') {
                            option[i] = '';
                        } else {
                            numeroarray.push(option[i])
                        }
                    }
                }

            }
            for (var i = 0; i < numeroarray.length; i++) {
                numero = numero + numeroarray[i]
            }
            option = numero
            var temph = ''
            for (var i = 0; i < option.length + 1; i++) {
                if (i == 2) {
                    temph = temph + ':' + option[i].toString();
                } else {
                    if (option[i] != undefined) {
                        temph = temph + option[i].toString();
                    }
                }
            }
            var temph2 = ''
            var temph3 = ''
            for (var i = 0; i < temph.length; i++) {
                if (i <= 4) {
                    if (temph[i] !== undefined) {
                        temph2 = temph2 + temph[i]
                    }
                }
            }
            try {
                if (parseInt(temph2[0].toString() + temph2[1].toString()) > 23) {
                    temph3 = '23:'
                } else {
                    temph3 = temph2[0].toString() + temph2[1].toString() + ':'
                }
            } catch (e) {
                if (temph2[0] !== undefined && temph2[1] !== undefined) {
                    temph3 = temph2[0].toString() + temph2[1].toString() + ':'
                } else if (temph2[0] !== undefined) {
                    temph3 = temph2[0].toString()
                }
            }
            try {
                if (parseInt(temph2[3].toString() + temph2[4].toString()) > 59) {
                    temph3 = temph3 + '59'
                } else {
                    temph3 = temph3 + temph2[3].toString() + temph2[4].toString()
                }
            } catch (e) {
                if (temph2[3] !== undefined && temph2[4] !== undefined) {
                    temph3 = temph3 + temph2[3].toString() + temph2[4].toString()
                } else if (temph2[3] !== undefined) {
                    temph3 = temph3 + temph2[3].toString()
                }
            }
            option = temph3
        }
        return option
    }
}

function MascaraPlaca(option) {
    option = option.toString()
    patron = /[A-Za-zñÑ\s]/
    var opt
    var placaarray = []
    var placa = ''
    if (option.length <= 3) {
        for (var i = 0; i < option.length; i++) {
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    optione[i] = '';
                } else {
                    placaarray.push(option[i])
                }
            }
        }

    } else if (option.length > 3) {
        for (var i = 0; i < 3; i++) {
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    placaarray.push(option[i])
                }
            }
        }
        patron = /\d/
        for (var i = 3; i < option.length; i++) {
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    placaarray.push(option[i])
                }
            }
        }
    }

    if (placaarray.length <= 6) {
        for (var i = 0; i < placaarray.length; i++) {
            placa = placa + placaarray[i]
        }
        option = placa
    }
    else if (option.length > 6) {
        for (var i = 0; i < 6; i++) {
            placa = placa + placaarray[i]
        }
        option = placa
    }

    return option.toUpperCase()
}

function MascaraTelefono(option) {
    option = option.toString()
    var numeroarray = []
    var numero = ''
    if (option[0] == '-' || option[1] == '-' || option[2] == '-' || option[3] == '-' || option[4] == '-' || option[5] == '-' || option[6] == '-') {
        if (option[0] == ':' || option[1] == ':' || option[2] == ':' || option[3] == ':' || option[4] == ':' || option[5] == ':' || option[6] == ':') {
            numero = ''
        }
    } else {
        for (var i = 0; i < option.length; i++) {
            patron = /[0123456789\+\(\)\:\-]/
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    numeroarray.push(option[i])
                }
            }
        }
    }
    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    return numero
}

function MascaraCelular(option) {
    option = option.toString()
    var numeroarray = []
    var numero = ''

    for (var i = 0; i < option.length; i++) {
        patron = /[0123456789\+\(\)]/
        if (patron.test(option[i])) {
            if (patron.test(option[i]) == ' ') {
                option[i] = '';
            } else {
                numeroarray.push(option[i])
            }
        } else if (i >= 10 && i <= 12 || i >= 16 && i <= 18) {
            if (i >= 10 && i <= 12) {
                if (option[i] == ' ' || option[i] == '-') {
                    numeroarray.push(option[i])
                }
            } else if (i >= 15 && i <= 18) {
                if ((option[10] != ' ' && option[10] != '-') && (option[11] != ' ' && option[11] != '-') && (option[12] != ' ' && option[12] != '-')) {
                    if ((option[i] == ' ' || option[i] == '-') && cont == 0) {
                        numeroarray.push(option[i])
                    }
                }
            }

        }
    }

    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    return numero
}

function MascaraNumero(option) {
    var numero = 0;
    if (option != undefined && option != '') {
        option = option.toString();
        var numeroarray = [];
        if (option[0] == '-') {
            numero = '';
        } else {
            for (var i = 0; i < option.length; i++) {
                patron = /[0123456789]/
                if (i == 0 && option[i] == '-') {
                    numeroarray.push(option[i])
                }
                else if (patron.test(option[i])) {
                    if (patron.test(option[i]) == ' ') {
                        option[i] = '';
                    } else {
                        numeroarray.push(option[i])
                    }
                }
            }

        }
        for (var i = 0; i < numeroarray.length; i++) {
            numero = numero + numeroarray[i]
        }
    }

    return parseInt(numero);
}
function MascaraPorcentaje(option) {
    option = option.toString()
    var numeroarray = []
    var numero = ''
    if (option[0] == '-') {
        numero = ''
    } else {
        for (var i = 0; i < option.length; i++) {
            patron = /[0123456789]/
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    numeroarray.push(option[i])
                }
            }
        }

    }
    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    if (parseInt(numero) >= 100) {
        numero = '100'
    }
    return numero
}

function MascaraMoneda(option) {
    option = option.toString()
    var numeroarray = []
    var numero = ''
    if (option[0] == '-') {
        numero = ''
    } else {
        for (var i = 0; i < option.length; i++) {
            patron = /[0123456789/,/./:]/
            if (i == 0 && option[i] == '-') {
                numeroarray.push(option[i])
            }
            else if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    numeroarray.push(option[i])
                }
            }
        }

    }
    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    return numero
}

//function MascaraDecimales(option) {
//    option = option.toString()
//    var numeroarray = []
//    var numero = ''
//    if (option[0] == '-') {
//        numero = ''
//    } else {
//        for (var i = 0; i < option.length; i++) {
//            patron = /[0123456789/.]/
//            if (patron.test(option[i])) {
//                if (patron.test(option[i]) == ' ') {
//                    option[i] = '';
//                } else {
//                    numeroarray.push(option[i])
//                }
//            }
//        }

//    }
//    for (var i = 0; i < numeroarray.length; i++) {
//        numero = numero + numeroarray[i]
//    }
//    return numero
//}

//function MascaraDecimales(option) {
//    option = option.toString()
//    var numeroarray = []
//    var numero = ''
//    if (option[0] == '-') {
//        numero = ''
//    } else {
//        for (var i = 0; i < option.length; i++) {
//            patron = /[0123456789/.]/
//            if (patron.test(option[i])) {
//                if (patron.test(option[i]) == ' ') {
//                    option[i] = '';
//                } else {
//                    numeroarray.push(option[i])
//                }
//            }
//        }

//    }
//    for (var i = 0; i < numeroarray.length; i++) {
//        numero = numero + numeroarray[i]
//    }
//    return numero
//}


function MascaraDireccion(option) {
    option = option.toString()
    var dir = ''

    var direccion = [];
    if (option.length > 250) {
        for (var i = 0; i < 250; i++) {
            direccion[i].push(option[i])
        }
    } else {
        for (var i = 0; i < option.length; i++) {
            direccion[i].push(option[i])
        }
    }
    for (var i = 0; i < direccion.length; i++) {
        dir = dir + direccion[i]
    }
    option = dir
    return option.toUpperCase()

}

function MascaraIdentificacion(option, select) {
    option = option.toString()
    select = select.toString()
    var opt
    var opt2
    var numeroarray = []
    var numero = ''

    if (document.getElementsByName(select)[0]) {
        opt2 = document.getElementsByName(select)[0]
    } else if (document.getElementById(select)) {
        opt2 = document.getElementById(select)
    }
    if (opt2.selectedOptions[0].label == 'PASAPORTE') {
        if (option[0] == '-') {
            numero = ''
        } else {
            for (var i = 0; i < option.length; i++) {
                patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789]/
                var a = option[i].toUpperCase()
                if (patron.test(a)) {
                    if (patron.test(a) == ' ') {
                        a = '';
                    } else {
                        numeroarray.push(a)
                    }
                }
            }

        }
    } else {
        if (option[0] == '-') {
            numero = ''
        } else {
            for (var i = 0; i < option.length; i++) {
                patron = /[0123456789]/
                if (patron.test(option[i])) {
                    if (patron.test(option[i]) == ' ') {
                        option[i] = '';
                    } else {
                        numeroarray.push(option[i])
                    }
                }
            }

        }
    }

    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    option = numero
    return numero
}

function MascaraNaturaleza(option, select) {
    option = option.toString()
    var opt
    var opt2
    if (document.getElementsByName(option)[0]) {
        opt = document.getElementsByName(option)[0]
    } else if (document.getElementById(option)) {
        opt = document.getElementById(option)
    }

    if (document.getElementsByName(select)[0]) {
        opt2 = document.getElementsByName(select)[0]
    } else if (document.getElementById(select)) {
        opt2 = document.getElementById(select)
    }
    if (opt.selectedIndex == 1) {
        opt2.selectedIndex = 0
        opt2.disabled = true
    } else {
        opt2.selectedIndex = 1
        opt2.disabled = false

    }
}

function MascaraModelo(option) {
    option = option.toString()
    var fecha = new Date();
    var ano = fecha.getFullYear();
    var valmax = parseInt(ano) + 2
    var valmin = 1900
    var opt
    var numeroarray = []
    var numero = ''

    for (var i = 0; i < option.length; i++) {
        patron = /[0123456789]/
        if (patron.test(option[i])) {
            if (patron.test(option[i]) == ' ') {
                option[i] = '';
            } else {
                numeroarray[i] = option[i]
            }
        }
    }
    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }

    if (valmax > parseInt(numero) || numero.length == 0) {
        option = numero
    } else if (valmax < parseInt(numero)) {
        option = valmax
    }
    return option


}
var tempvalor = 0
function MascaraValores(option) {
    var numero = MascaraDecimales(option)
    if (numero.split('.').length == 2) {
        numero = formatNumber.new(numero.split('.')[0]).toString() + '.' + numero.split('.')[1].toString()
    } else {
        numero = formatNumber.new(numero)
    }
    if (numero == "NaN") {
        numero = ''
    }
    return numero
}
function MascaraValoresInt(option) {
    var numero = MascaraNumero(option).toString()
    if (numero.split('.').length == 2) {
        numero = formatNumber.new(numero.split('.')[0]).toString() + '.' + numero.split('.')[1].toString()
    } else {
        numero = formatNumber.new(numero)
    }
    if (numero == "NaN") {
        numero = ''
    }
    return numero
}
var formatNumber = {
    separador: ",", // separador para los miles
    sepDecimal: '.', // separador para los decimales
    formatear: function (num) {
        num += '';
        var splitStr = num.split(',');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
        }
        return this.simbol + splitLeft + splitRight;
    },
    new: function (num, simbol) {
        this.simbol = simbol || '';
        return this.formatear(num);
    }
}


function ValidarHoras(horaInicio, NombreControl) {
    var mensaje = ''
    var continuar = true
    if (horaInicio !== undefined && horaInicio !== '') {
        try {
            var arrHora = horaInicio.split(":");
            var hora = arrHora[0];
            var minutos = arrHora[1];
            if (parseInt(hora) < 0 || parseInt(hora) > 23) {
                mensaje = 'La hora ' + NombreControl + '  no tiene el formato correcto';
                continuar = false;
            }

            if (continuar == true) {
                if (parseInt(minutos) < 0 || parseInt(minutos) > 59) {
                    mensaje = 'La hora ' + NombreControl + '  no tiene el formato correcto';
                    continuar = false;
                }
            }
        }
        catch (err) {
            mensaje = 'La hora  ' + NombreControl + ' no tiene el formato correcto';
            continuar = false;
        }
    }
    return mensaje
}


function RetornarHoraFecha(Fecha, Hora) {
    var hora = angular.copy(Hora);
    var fecha = angular.copy(Fecha);
    var arrHora = hora.split(":");
    var fechaHora = new Date(fecha.getFullYear(), fecha.getMonth(), fecha.getDate(), arrHora[0], arrHora[1]);
    //fechaHora = RetornarFechaEspecificaSinTimeZone(fechaHora)
    return fechaHora
}



function RetornaFechaMasDia(fecha) {
    var temp = fecha;
    fecha.setDate(temp.getDate() + 1);
    fecha.setHours(0);
    fecha.setMinutes(0);
    fecha.setMilliseconds(0);

    return fecha;
}

function RetornaFechaSinHoras(fecha) {
    var temp = RetornarFechaEspecifica(fecha);
    temp.setHours(0);
    temp.setMinutes(0);
    temp.setSeconds(0);
    temp.setMilliseconds(0);
    return temp;
}

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

/* JO: 13-DIC-2016 V1

Función que extrae la sólo fecha de un dato tipo Datetime (Fecha y Hora) en el formato dd/MM/yyyy

Ejemplo Llamada: $scope.Fecha = ExtraerFecha($scope.FechaEntrada, $scope.FechaSalida);

NOTA: el $scope.Fecha debe estar en formato date, puede utilizar Fecha = new Date(Fecha) y luego 
convertirla a la fecha especifica quitandole la zona horaria, lo puede hacer utilizar la función "RetornarFechaEspecifica"
Ejemplo completo:

                    var FechaAux = new Date($scope.ModalFechaHora);
                    FechaAux = RetornarFechaEspecifica(FechaAux);
                    var FechaConfirma = ExtraerFecha(FechaAux, FechaConfirma);

*/
function ExtraerFecha(EntradaFecha) {
    var DevuelveFecha = ''
    var Dia = EntradaFecha.getDate();
    var Mes = EntradaFecha.getMonth() + 1;
    var Anio = EntradaFecha.getFullYear();

    if (Dia < 10) { Dia = '0' + Dia }
    if (Mes < 10) { Mes = '0' + Mes }

    //Al mes le suma un 1 debido a que se interpreta Enero como el mes 0
    DevuelveFecha = Dia + '/' + Mes + '/' + Anio;

    return DevuelveFecha;

};

function RetornarFechaMasHora(Hora, Fecha) {
    var Fecha = RetornarFechaEspecifica(Fecha)
    var hora = angular.copy(Hora);
    var fecha = angular.copy(Fecha);
    var arrHora = hora.split(":");
    var fechaHora = new Date(fecha.getFullYear(), fecha.getMonth(), fecha.getDate(), arrHora[0], arrHora[1]);

    return fechaHora;
}

function RetornarHoraMinutos(Fecha) {
    var Fecha = RetornarFechaEspecifica(Fecha)
    var arrHora = Fecha
    hora = arrHora.getHours()
    minuto = arrHora.getMinutes()

    if (hora < 10 && hora >= 0) {
        hora = ("0" + hora)
    }
    if (minuto < 10 && minuto > 0) {
        minuto = ("0" + minuto)
    }
    if (minuto == 0) {
        minuto = (minuto + "0")
    }
    var hora = (hora + ":" + minuto);

    return hora
}



/* JO: 13-DIC-2016 V1

Función que extrae la sólo la hora de un dato tipo Datetime (Fecha y Hora) en el formato HH:mm:ss (Hora militar)

Ejemplo Llamada: $scope.Fecha = ExtraerHoraMilitar($scope.FechaEntrada, $scope.HoraSalida);

NOTA: el $scope.Fecha debe estar en formato date, puede utilizar Fecha = new Date(Fecha) y luego 
convertirla a la fecha especifica quitandole la zona horaria, lo puede hacer utilizar la función "RetornarFechaEspecifica"

Ejemplo completo:

                    var FechaAux = new Date($scope.ModalFechaHora);
                    FechaAux = RetornarFechaEspecifica(FechaAux);
                    var HoraConfirma = ExtraerHoraMilitar(FechaAux, HoraConfirma);
*/
function ExtraerHoraMilitar(EntradaFecha, DevuelveHoraMilitar) {

    var Hora = EntradaFecha.getHours();
    var Minutos = EntradaFecha.getMinutes();
    var Segundos = EntradaFecha.getSeconds();

    if (Hora < 10) { Hora = '0' + Hora }
    if (Minutos < 10) { Minutos = '0' + Minutos }
    if (Segundos < 10) { Segundos = '0' + Segundos }

    DevuelveHoraMilitar = Hora + ':' + Minutos + ':' + Segundos;

    return DevuelveHoraMilitar;

};
//metodo para imprimir un div desde js
function imprSelec(muestra) {
    var ficha = document.getElementById(muestra);
    var ventimp = window.open(' ', 'popimpr');
    ventimp.document.write(ficha.innerHTML);
    var css = ventimp.document.createElement("link");
    css.setAttribute("href", "../../Css/Bootstrap/bootstrap.min.css");
    css.setAttribute("rel", "stylesheet");
    css.setAttribute("type", "text/css");
    ventimp.document.head.appendChild(css);
    ventimp.document.close();
    ventimp.print();
    ventimp.close();
}

function cargaContextoCanvas(idCanvas) {
    var elemento = document.getElementById(idCanvas);
    if (elemento && elemento.getContext) {
        var contexto = elemento.getContext('2d');
        if (contexto) {
            return contexto;
        }
    }
    return false;
}
function MascaraMayus(ITEM) {
    if (ITEM !== undefined) {
        return ITEM.toUpperCase()
    }
}
function MascaraNumeroGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            var Model = ''
            var Lista = ''
            var conitinue = false
            for (var j = 0; j < a[i].attributes.length; j++) {
                if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskNumero()')) {
                    conitinue = true
                    break;
                }
            } if (conitinue) {

                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-model') {
                        Model = a[i].attributes[j].value

                    } else if (a[i].attributes[j].name == 'lista') {
                        Lista = a[i].attributes[j].value
                    }
                }

                var b = Model.split('.')
                switch (b.length) {
                    case 1:
                        if (sc[b[0]] !== undefined) {
                            try { sc[b[0]] = MascaraNumero(sc[b[0]]) } catch (e) { }
                        }
                        break;
                    case 2:
                        if (sc[b[0]][b[1]] !== undefined) {
                            try { sc[b[0]][b[1]] = MascaraNumero(sc[b[0]][b[1]]) } catch (e) { }
                        }
                        break;
                    case 3:
                        if (sc[b[0]][b[1]][b[2]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]] = MascaraNumero(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                        }
                        break;
                    case 4:
                        if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraNumero(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                        }
                        break;
                    case 5:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraNumero(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                        }
                        break;
                    case 6:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraNumero(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                        }
                        break;
                    case 7:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraNumero(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                        }
                        break;
                }

            }
        }
    }
}
function MascaraMayusGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            var Model = ''
            var Lista = ''
            var conitinue = false
            for (var j = 0; j < a[i].attributes.length; j++) {
                if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskMayus()')) {
                    conitinue = true
                    break;
                }
            } if (conitinue) {
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-model') {
                        Model = a[i].attributes[j].value

                    } else if (a[i].attributes[j].name == 'lista') {
                        Lista = a[i].attributes[j].value
                    }
                }
                var b = Model.split('.')
                switch (b.length) {
                    case 1:
                        if (sc[b[0]] !== undefined) {
                            try { sc[b[0]] = MascaraMayus(sc[b[0]]) } catch (e) { }
                        }
                        break;
                    case 2:
                        if (sc[b[0]][b[1]] !== undefined) {
                            try { sc[b[0]][b[1]] = MascaraMayus(sc[b[0]][b[1]]) } catch (e) { }
                        }
                        break;
                    case 3:
                        if (sc[b[0]][b[1]][b[2]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]] = MascaraMayus(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                        }
                        break;
                    case 4:
                        if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraMayus(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                        }
                        break;
                    case 5:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraMayus(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                        }
                        break;
                    case 6:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraMayus(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                        }
                        break;
                    case 7:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraMayus(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                        }
                        break;
                }
            }
        }
    }
}
function MascaraValoresGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            var Model = ''
            var Lista = ''
            var conitinue = false
            for (var j = 0; j < a[i].attributes.length; j++) {
                if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskValores()')) {
                    conitinue = true
                    break;
                }
            } if (conitinue) {
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-model') {
                        Model = a[i].attributes[j].value

                    } else if (a[i].attributes[j].name == 'lista') {
                        Lista = a[i].attributes[j].value
                    }
                }
                var b = Model.split('.')
                switch (b.length) {
                    case 1:
                        try {
                            if (sc[b[0]] !== undefined) {
                                try { sc[b[0]] = MascaraValores(sc[b[0]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                    case 2:
                        try {
                            if (sc[b[0]][b[1]] !== undefined) {
                                try { sc[b[0]][b[1]] = MascaraValores(sc[b[0]][b[1]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                    case 3:
                        try {
                            if (sc[b[0]][b[1]][b[2]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]] = MascaraValores(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                    case 4:
                        try {
                            if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraValores(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                    case 5:
                        try {
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraValores(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                    case 6:
                        try {
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraValores(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                    case 7:
                        try {
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraValores(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                }
            }
        }
    }
}
function MascaraValoresIntGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            var Model = ''
            var Lista = ''
            var conitinue = false
            for (var j = 0; j < a[i].attributes.length; j++) {
                if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskintValores()')) {
                    conitinue = true
                    break;
                }
            } if (conitinue) {
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-model') {
                        Model = a[i].attributes[j].value

                    } else if (a[i].attributes[j].name == 'lista') {
                        Lista = a[i].attributes[j].value
                    }
                }
                var b = Model.split('.')
                switch (b.length) {
                    case 1:
                        try {
                            if (sc[b[0]] !== undefined) {
                                try { sc[b[0]] = MascaraValoresInt(sc[b[0]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                    case 2:
                        try {
                            if (sc[b[0]][b[1]] !== undefined) {
                                try { sc[b[0]][b[1]] = MascaraValoresInt(sc[b[0]][b[1]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                    case 3:
                        try {
                            if (sc[b[0]][b[1]][b[2]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]] = MascaraValoresInt(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                    case 4:
                        try {
                            if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraValoresInt(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                    case 5:
                        try {
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraValoresInt(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                    case 6:
                        try {
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraValoresInt(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                    case 7:
                        try {
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraValoresInt(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                            }
                        } catch (e) {

                        }
                        break;
                }
            }
        }
    }
}
function MascaraHorasGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            try {
                var Model = ''
                var Lista = ''
                var conitinue = false
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskHora()')) {
                        conitinue = true
                        break;
                    }
                } if (conitinue) {
                    for (var j = 0; j < a[i].attributes.length; j++) {
                        if (a[i].attributes[j].name == 'ng-model') {
                            Model = a[i].attributes[j].value

                        } else if (a[i].attributes[j].name == 'lista') {
                            Lista = a[i].attributes[j].value
                        }
                    }
                    var b = Model.split('.')
                    switch (b.length) {
                        case 1:
                            if (sc[b[0]] !== undefined) {
                                try { sc[b[0]] = MascaraHora(sc[b[0]]) } catch (e) { }
                            }
                            break;
                        case 2:
                            if (sc[b[0]][b[1]] !== undefined) {
                                try { sc[b[0]][b[1]] = MascaraHora(sc[b[0]][b[1]]) } catch (e) { }
                            }
                            break;
                        case 3:
                            if (sc[b[0]][b[1]][b[2]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]] = MascaraHora(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                            }
                            break;
                        case 4:
                            if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraHora(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                            }
                            break;
                        case 5:
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraHora(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                            }
                            break;
                        case 6:
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraHora(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                            }
                            break;
                        case 7:
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraHora(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                            }
                            break;
                    }
                }
            } catch (e) {

            }
        }
    }
}
function MascaraMonedaGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            var Model = ''
            var Lista = ''
            var conitinue = false
            for (var j = 0; j < a[i].attributes.length; j++) {
                if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskMoneda()')) {
                    conitinue = true
                    break;
                }
            } if (conitinue) {
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-model') {
                        Model = a[i].attributes[j].value

                    } else if (a[i].attributes[j].name == 'lista') {
                        Lista = a[i].attributes[j].value
                    }
                }
                var b = Model.split('.')
                switch (b.length) {
                    case 1:
                        if (sc[b[0]] !== undefined) {
                            try { sc[b[0]] = MascaraMoneda(sc[b[0]]) } catch (e) { }
                        }
                        break;
                    case 2:
                        if (sc[b[0]][b[1]] !== undefined) {
                            try { sc[b[0]][b[1]] = MascaraMoneda(sc[b[0]][b[1]]) } catch (e) { }
                        }
                        break;
                    case 3:
                        if (sc[b[0]][b[1]][b[2]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]] = MascaraMoneda(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                        }
                        break;
                    case 4:
                        if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraMoneda(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                        }
                        break;
                    case 5:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraMoneda(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                        }
                        break;
                    case 6:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraMoneda(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                        }
                        break;
                    case 7:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraMoneda(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                        }
                        break;
                }
            }
        }
    }
}
function MascaraPlacaGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            var Model = ''
            var Lista = ''
            var conitinue = false
            for (var j = 0; j < a[i].attributes.length; j++) {
                if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskPlaca()')) {
                    conitinue = true
                    break;
                }
            } if (conitinue) {
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-model') {
                        Model = a[i].attributes[j].value

                    } else if (a[i].attributes[j].name == 'lista') {
                        Lista = a[i].attributes[j].value
                    }
                }
                var b = Model.split('.')
                switch (b.length) {
                    case 1:
                        if (sc[b[0]] !== undefined) {
                            try { sc[b[0]] = MascaraPlaca(sc[b[0]]) } catch (e) { }
                        }
                        break;
                    case 2:
                        if (sc[b[0]][b[1]] !== undefined) {
                            try { sc[b[0]][b[1]] = MascaraPlaca(sc[b[0]][b[1]]) } catch (e) { }
                        }
                        break;
                    case 3:
                        if (sc[b[0]][b[1]][b[2]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]] = MascaraPlaca(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                        }
                        break;
                    case 4:
                        if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraPlaca(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                        }
                        break;
                    case 5:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraPlaca(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                        }
                        break;
                    case 6:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraPlaca(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                        }
                        break;
                    case 7:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraPlaca(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                        }
                        break;
                }
            }
        }
    }
}




function InternalServerError(Cadena) {
    var cadena = Cadena
    var TablaExistene = false
    var result = ''
    var cont = 0
    for (var i = 0; i < cadena.length; i++) {
        if (cadena[i].toUpperCase() == 'T') {
            var temp = cadena[i] + cadena[i + 1] + cadena[i + 2] + cadena[i + 3] + cadena[i + 4]
            if (temp.toUpperCase() == 'TABLE') {
                i = i + 5
                TablaExistene = true
            }
        }
        if (TablaExistene) {
            if (cadena[i] == '"') {
                cont++
            }
            else if (cont == 1) {
                result += cadena[i]
            } else if (cont == 2) {
                break
            }
        }
    }
    result = result.replace('dbo.', '')

    return result
}
function ValidarCampo(objeto, minlength, Esobjeto) {
    var resultado = 0
    if (objeto == undefined || objeto == '' || objeto == null || objeto == 0 || objeto == '0') {
        resultado = 1
    } else {
        if (resultado == 0) {
            if (minlength !== undefined) {
                if (objeto.length < minlength) {
                    resultado = 3
                }
                else {
                    resultado = 0
                }
            }
        } if (resultado == 0) {
            if (Esobjeto) {
                if (objeto.Codigo == undefined || objeto.Codigo == '' || objeto.Codigo == null || objeto.Codigo == 0) {
                    resultado = 2
                }
            }
        }
    }
    return resultado
}
function ValidarFecha(objeto, MayorActual, MenorActual) {
    var resultado = 0
    var now = new Date()
    if (objeto == undefined || objeto == '' || objeto == null) {
        resultado = 1
    } else {
        if (resultado == 0) {
            if (MayorActual) {
                if (objeto > now) {
                    resultado = 2
                }
            }
            else if (MenorActual) {
                if (objeto < now) {
                    resultado = 3
                }
            }
        }
    }
    return resultado
}
//isNaN(123) 

/* MO: 30-AGO-2018 V1

Función que compara fechas, si es mayor o igual (mayor = 2, menor = 3, igual = 4)
NOTA: se debe tener en cuenta la diferencia de zona horaria
Ejemplos:
    var ComparaFecha = (fechaIni, fechaFin)//guarda el valor en una variable
*/
function ComparaFecha(fechaIni, fechaFin) {
    if (fechaIni > fechaFin)
        return FECHA_ES_MAYOR;
    else if (fechaIni < fechaFin)
        return FECHA_ES_MENOR;
    else
        return FECHA_ES_IGUAL;
    return null;
}
/* MO: 30-AGO-2018 V1

Prototipo para quitar las horas a las fechas
NOTA: se debe tener en cuenta la diferencia de zona horaria
Ejemplos:
    var date = new Date().withoutTime();
*/
Date.prototype.withoutTime = function () {
    var d = new Date(this);
    d.setHours(0, 0, 0, 0);
    return d;
}
/* MO: 31-AGO-2018 V1

Prototipo para quitar los milisegundos a las fechas
NOTA: se debe tener en cuenta la diferencia de zona horaria
Ejemplos:
    var date = new Date().withoutMilliseconds();
*/
Date.prototype.withoutMilliseconds = function () {
    var d = new Date(this);
    d.setMilliseconds(0);
    return d;
}

Date.prototype.withoutSeconds = function () {
    var d = new Date(this);
    d.setSeconds(0);
    return d;
}


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
function RedimencionarImagen(id, img, Alto, Ancho) {
    var ctx = cargaContextoCanvas(id);
    if (ctx) {
        ctx.drawImage(img, 0, 0, Ancho, Alto);
    }
}
function OrderBy(Objeto, Atributo, Lista, Atributo2) {
    if (Lista.Asc) {
        Lista.sort(dynamicSort(Objeto, Atributo, Atributo2));
        Lista.Asc = false
    } else {
        Lista.sort(dynamicSort("-" + Objeto, Atributo, Atributo2));
        Lista.Asc = true
    }
}

//------------ Consultar listado autocomplete
//JO: retorna una lista propia para un autocomplete según los campos que desee filtrar
// Llamado: $scope.ListaAuto = RetornarListaAutocomplete(CadenaIngreso, Lista, Param1, Param2, Param3)
function RetornarListaAutocomplete(Cadena, Lista, p1, p2, p3) {

    var ListaAux = [];

    for (var i = 0; i < Lista.length; i++) {
        var Coincidencia = 0
        try {
            for (var j = 0; j < Cadena.length; j++) {
                var inserto = false
                var temp = Lista[i][p1].toString()
                if (p1 !== '' && p1 !== undefined && p1 !== null) {
                    if (temp[j] !== undefined) {
                        if (temp[j].toUpperCase() == Cadena[j].toUpperCase()) {
                            Coincidencia++
                            inserto = true
                        }
                    }
                }

                if (p2 !== '' && p2 !== undefined && p2 !== null) {
                    temp = Lista[i][p2].toString()
                    if (temp[j] !== undefined) {
                        if (temp[j].toUpperCase() == Cadena[j].toUpperCase()) {
                            if (!inserto) {
                                Coincidencia++
                                inserto = true
                            }
                        }
                    }
                }
                if (p3 !== '' && p3 !== undefined && p3 !== null) {
                    temp = Lista[i][p3].toString()
                    if (temp[j] !== undefined) {
                        if (temp[j].toUpperCase() == Cadena[j].toUpperCase()) {
                            if (!inserto) {
                                Coincidencia++
                                inserto = true
                            }
                        }
                    }
                }

            }
            if (Coincidencia == Cadena.length) {
                ListaAux.push(Lista[i]);
            }
        } catch (e) { }
    }

    return ListaAux;

}


// Función que ordena una lista por una columna especifica de mayor a menor o viceversa
function dynamicSort(property, cod, cod2) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a, b) {
        if (cod != undefined && cod != null && cod != '') {
            if (cod2 != undefined && cod2 != null && cod2 != '') {
                var result = (a[property][cod][cod2] < b[property][cod][cod2]) ? -1 : (a[property][cod][cod2] > b[property][cod][cod2]) ? 1 : 0;
            } else {
                var result = (a[property][cod] < b[property][cod]) ? -1 : (a[property][cod] > b[property][cod]) ? 1 : 0;
            }
        } else {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        }
        return result * sortOrder;
    }
}
function AgruparListados(Lista, b, c, d, e, f, g, h) {
    var a = JSON.parse(JSON.stringify(Lista));
    var Result = [];
    var cod = "Codigo";
    var dt = "Data";
    var cont = 0;
    if (b !== undefined) {
        for (var i = 0; i < a.length; i++) {
            if ((a[i][b] !== undefined && (typeof a[i][b]) !== "object") || (a[i][b][cod] !== undefined && a[i][b][cod] > 0)) {
                if (Result.length > 0) {
                    var cont = 0
                    for (var j = 0; j < Result.length; j++) {
                        if ((a[i][b][cod] == Result[j][b][cod] && a[i][b][cod] !== undefined) || (a[i][b] == Result[j][b])) {
                            cont++
                            if (c == undefined) {
                                Result[j][dt].push(a[i])
                            }
                            break;
                        }
                    }
                    if (cont == 0) {
                        if (c == undefined) {
                            Result.push({ [b]: a[i][b], [dt]: [a[i]] })
                        }
                        else {
                            Result.push({ [b]: a[i][b], [dt]: [] })
                        }
                    }
                } else {
                    if (c == undefined) {
                        Result.push({ [b]: a[i][b], [dt]: [a[i]] })
                    } else {
                        Result.push({ [b]: a[i][b], [dt]: [] })
                    }
                }
            }
        }
        if (c !== undefined) {
            for (var i = 0; i < a.length; i++) {
                if ((a[i][c] !== undefined && (typeof a[i][c]) !== "object") || (a[i][c][cod] !== undefined && a[i][c][cod] > 0)) {
                    for (var j = 0; j < Result.length; j++) {
                        if ((a[i][b][cod] == Result[j][b][cod] && a[i][b][cod] !== undefined) || (a[i][b] == Result[j][b])) {
                            if (Result[j][dt].length > 0) {
                                var cont = 0
                                for (var k = 0; k < Result[j][dt].length; k++) {
                                    if ((a[i][c][cod] == Result[j][dt][k][c][cod] && a[i][c][cod] != undefined) || (a[i][c] == Result[j][dt][k][c])) {
                                        if (d == undefined) {
                                            Result[j][dt][k][dt].push(a[i])
                                        }
                                        cont++
                                        break;
                                    }
                                }
                                if (cont == 0) {
                                    if (d == undefined) {
                                        Result[j][dt].push({ [c]: a[i][c], [dt]: [a[i]] })
                                    }
                                    else {
                                        Result[j][dt].push({ [c]: a[i][c], [dt]: [] })
                                    }
                                }
                            }
                            else {
                                if (d == undefined) {
                                    Result[j][dt].push({ [c]: a[i][c], [dt]: [a[i]] })
                                }
                                else {
                                    Result[j][dt].push({ [c]: a[i][c], [dt]: [] })
                                }
                            }
                        }
                    }
                }

            }
            if (d !== undefined) {
                for (var i = 0; i < a.length; i++) {
                    if ((a[i][d] !== undefined && (typeof a[i][d]) !== "object") || (a[i][d][cod] !== undefined && a[i][d][cod] > 0)) {
                        for (var j = 0; j < Result.length; j++) {
                            if ((a[i][b][cod] == Result[j][b][cod] && a[i][b][cod] !== undefined) || (a[i][b] == Result[j][b])) {
                                for (var k = 0; k < Result[j][dt].length; k++) {
                                    if ((a[i][c][cod] == Result[j][dt][k][c][cod] && a[i][c][cod] !== undefined) || (a[i][c] == Result[j][dt][k][c])) {
                                        if (Result[j][dt][k][dt].length > 0) {
                                            var cont = 0
                                            for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod] && a[i][d][cod] != undefined) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                    if (e == undefined) {
                                                        Result[j][dt][k][dt][l][dt].push(a[i])
                                                    }
                                                    cont++
                                                }
                                            }
                                            if (cont == 0) {
                                                if (e == undefined) {
                                                    Result[j][dt][k][dt].push({ [d]: a[i][d], [dt]: [a[i]] })
                                                } else {
                                                    Result[j][dt][k][dt].push({ [d]: a[i][d], [dt]: [] })
                                                }
                                            }
                                            cont = 0
                                        }
                                        else {
                                            if (e == undefined) {
                                                Result[j][dt][k][dt].push({ [d]: a[i][d], [dt]: [a[i]] })
                                            } else {
                                                Result[j][dt][k][dt].push({ [d]: a[i][d], [dt]: [] })
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                if (e !== undefined) {
                    for (var i = 0; i < a.length; i++) {
                        if ((a[i][e] !== undefined && (typeof a[i][e]) !== "object") || (a[i][e][cod] !== undefined && a[i][e][cod] > 0)) {
                            for (var j = 0; j < Result.length; j++) {
                                if ((a[i][b][cod] == Result[j][b][cod] && a[i][b][cod] !== undefined) || (a[i][b] == Result[j][b])) {
                                    for (var k = 0; k < Result[j][dt].length; k++) {
                                        if ((a[i][c][cod] == Result[j][dt][k][c][cod] && a[i][c][cod] !== undefined) || (a[i][c] == Result[j][dt][k][c])) {
                                            for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod] && a[i][d][cod] !== undefined) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                    if (Result[j][dt][k][dt][l][dt].length > 0) {
                                                        var cont = 0
                                                        for (var m = 0; m < Result[j][dt][k][dt][l][dt].length; m++) {
                                                            if ((a[i][e][cod] == Result[j][dt][k][dt][l][dt][m][e][cod] && a[i][e][cod] != undefined) || (a[i][e] == Result[j][dt][k][dt][l][dt][m][e])) {
                                                                if (f == undefined) {
                                                                    Result[j][dt][k][dt][l][dt][m][dt].push(a[i])
                                                                }
                                                                cont++
                                                            }
                                                        }
                                                        if (cont == 0) {
                                                            if (f == undefined) {
                                                                Result[j][dt][k][dt][l][dt].push({ [e]: a[i][e], [dt]: [a[i]] })
                                                            } else {
                                                                Result[j][dt][k][dt][l][dt].push({ [e]: a[i][e], [dt]: [] })
                                                            }
                                                        }
                                                        cont = 0
                                                    }
                                                    else {
                                                        if (f == undefined) {
                                                            Result[j][dt][k][dt][l][dt].push({ [e]: a[i][e], [dt]: [a[i]] })
                                                        } else {
                                                            Result[j][dt][k][dt][l][dt].push({ [e]: a[i][e], [dt]: [] })
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    if (f !== undefined) {
                        for (var i = 0; i < a.length; i++) {
                            if ((a[i][f] !== undefined && (typeof a[i][f]) !== "object") || (a[i][f][cod] !== undefined && a[i][f][cod] > 0)) {
                                for (var j = 0; j < Result.length; j++) {
                                    if ((a[i][b][cod] == Result[j][b][cod] && a[i][b][cod] !== undefined) || (a[i][b] == Result[j][b])) {
                                        for (var k = 0; k < Result[j][dt].length; k++) {
                                            if ((a[i][c][cod] == Result[j][dt][k][c][cod] && a[i][c][cod] !== undefined) || (a[i][c] == Result[j][dt][k][c])) {
                                                for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                    if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod] && a[i][d][cod] !== undefined) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                        for (var m = 0; m < Result[j][dt][k][dt][l][dt].length; m++) {
                                                            if ((a[i][e][cod] == Result[j][dt][k][dt][l][dt][m][e][cod] && a[i][e][cod] !== undefined) || (a[i][e] == Result[j][dt][k][dt][l][dt][m][e])) {
                                                                if (Result[j][dt][k][dt][l][dt][m][dt].length > 0) {
                                                                    var cont = 0
                                                                    for (var n = 0; n < Result[j][dt][k][dt][l][dt][m][dt].length; n++) {
                                                                        if ((a[i][f][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][f][cod] && a[i][f][cod] != undefined) || (a[i][f] == Result[j][dt][k][dt][l][dt][m][dt][n][f])) {
                                                                            if (g == undefined) {
                                                                                Result[j][dt][k][dt][l][dt][m][dt][n][dt].push(a[i])
                                                                            }
                                                                            cont++
                                                                        }
                                                                    }
                                                                    if (cont == 0) {
                                                                        if (g == undefined) {
                                                                            Result[j][dt][k][dt][l][dt][m][dt].push({ [f]: a[i][f], [dt]: [a[i]] })
                                                                        } else {
                                                                            Result[j][dt][k][dt][l][dt][m][dt].push({ [f]: a[i][f], [dt]: [] })
                                                                        }
                                                                    }
                                                                    cont = 0
                                                                }
                                                                else {
                                                                    if (g == undefined) {
                                                                        Result[j][dt][k][dt][l][dt][m][dt].push({ [f]: a[i][f], [dt]: [a[i]] })
                                                                    } else {
                                                                        Result[j][dt][k][dt][l][dt][m][dt].push({ [f]: a[i][f], [dt]: [] })
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        if (g !== undefined) {
                            for (var i = 0; i < a.length; i++) {
                                if ((a[i][g] !== undefined && (typeof a[i][g]) !== "object") || (a[i][g][cod] !== undefined && a[i][g][cod] > 0)) {
                                    for (var j = 0; j < Result.length; j++) {
                                        if ((a[i][b][cod] == Result[j][b][cod] && a[i][b][cod] !== undefined) || (a[i][b] == Result[j][b])) {
                                            for (var k = 0; k < Result[j][dt].length; k++) {
                                                if ((a[i][c][cod] == Result[j][dt][k][c][cod] && a[i][c][cod] !== undefined) || (a[i][c] == Result[j][dt][k][c])) {
                                                    for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                        if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod] && a[i][d][cod] !== undefined) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                            for (var m = 0; m < Result[j][dt][k][dt][l][dt].length; m++) {
                                                                if ((a[i][e][cod] == Result[j][dt][k][dt][l][dt][m][e][cod] && a[i][e][cod] !== undefined) || (a[i][e] == Result[j][dt][k][dt][l][dt][m][e])) {
                                                                    for (var n = 0; n < Result[j][dt][k][dt][l][dt][m][dt].length; n++) {
                                                                        if ((a[i][f][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][f][cod] && a[i][f][cod] !== undefined) || (a[i][f] == Result[j][dt][k][dt][l][dt][m][dt][n][f])) {
                                                                            if (Result[j][dt][k][dt][l][dt][m][dt][n][dt].length > 0) {
                                                                                var cont = 0
                                                                                for (var o = 0; o < Result[j][dt][k][dt][l][dt][m][dt][n][dt].length; o++) {
                                                                                    if ((a[i][g][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][g][cod] && a[i][g][cod] != undefined) || (a[i][g] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][g])) {
                                                                                        if (g == undefined) {
                                                                                            Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].push(a[i])
                                                                                        }
                                                                                        cont++
                                                                                    }
                                                                                }
                                                                                if (cont == 0) {
                                                                                    if (h == undefined) {
                                                                                        Result[j][dt][k][dt][l][dt][m][dt][n][dt].push({ [g]: a[i][g], [dt]: [a[i]] })
                                                                                    } else {
                                                                                        Result[j][dt][k][dt][l][dt][m][dt][n][dt].push({ [g]: a[i][g], [dt]: [] })
                                                                                    }
                                                                                }
                                                                                cont = 0
                                                                            }
                                                                            else {
                                                                                if (h == undefined) {
                                                                                    Result[j][dt][k][dt][l][dt][m][dt][n][dt].push({ [g]: a[i][g], [dt]: [a[i]] })
                                                                                } else {
                                                                                    Result[j][dt][k][dt][l][dt][m][dt][n][dt].push({ [g]: a[i][g], [dt]: [] })
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                            if (h !== undefined) {
                                for (var i = 0; i < a.length; i++) {
                                    if ((a[i][h] !== undefined && (typeof a[i][h]) !== "object") || (a[i][h][cod] !== undefined && a[i][h][cod] > 0)) {
                                        for (var j = 0; j < Result.length; j++) {
                                            if ((a[i][b][cod] == Result[j][b][cod] && a[i][b][cod] !== undefined) || (a[i][b] == Result[j][b])) {
                                                for (var k = 0; k < Result[j][dt].length; k++) {
                                                    if ((a[i][c][cod] == Result[j][dt][k][c][cod] && a[i][c][cod] !== undefined) || (a[i][c] == Result[j][dt][k][c])) {
                                                        for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                            if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod] && a[i][d][cod] !== undefined) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                                for (var m = 0; m < Result[j][dt][k][dt][l][dt].length; m++) {
                                                                    if ((a[i][e][cod] == Result[j][dt][k][dt][l][dt][m][e][cod] && a[i][e][cod] !== undefined) || (a[i][e] == Result[j][dt][k][dt][l][dt][m][e])) {
                                                                        for (var n = 0; n < Result[j][dt][k][dt][l][dt][m][dt].length; n++) {
                                                                            if ((a[i][f][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][f][cod] && a[i][f][cod] !== undefined) || (a[i][f] == Result[j][dt][k][dt][l][dt][m][dt][n][f])) {
                                                                                for (var o = 0; o < Result[j][dt][k][dt][l][dt][m][dt][n][dt].length; o++) {
                                                                                    if ((a[i][g][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][g][cod] && a[i][g][cod] !== undefined) || (a[i][g] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][g])) {
                                                                                        if (Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].length > 0) {
                                                                                            var cont = 0
                                                                                            for (var p = 0; p < Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].length; p++) {
                                                                                                if ((a[i][h][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt][p][h][cod] && a[i][h][cod] != undefined) || (a[i][h] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt][p][h])) {
                                                                                                    Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt][p][dt].push(a[i])
                                                                                                    cont++
                                                                                                }
                                                                                            }
                                                                                            if (cont == 0) {
                                                                                                Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].push({ [h]: a[i][h], [dt]: [a[i]] })
                                                                                            }
                                                                                            cont = 0
                                                                                        }
                                                                                        else {
                                                                                            Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].push({ [h]: a[i][h], [dt]: [a[i]] })
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return Result
    //derechos reservados--
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user = getCookie("username");
    if (user != "") {

    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}
function rand_code(chars, lon) {
    code = "";
    for (x = 0; x < lon; x++) {
        rand = Math.floor(Math.random() * chars.length);
        code += chars.substr(rand, 1);
    }
    return code;
}

function GenerarLLave() {
    chars = '1234567890abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ'
    code = "";
    for (x = 0; x < 20; x++) {
        rand = Math.floor(Math.random() * chars.length);
        code += chars.substr(rand, 1);
    }
    return code;
}
caracteres = "0123456789abcdefABCDEF?¿¡!:;";
longitud = 20;
//devuelve una cadena aleatoria de 20 caracteres
function ObjetoEnListado(Objeto, Listado) {
    var resultado = false
    try {
        for (var i = 0; i < Listado.length; i++) {
            if (Objeto.Codigo !== undefined) {
                if (Listado[i].Codigo !== undefined) {
                    if (Objeto.Codigo = Listado[i].Codigo) {
                        resultado = true
                        break
                    }
                } else {
                    if (Objeto.Codigo = Listado[i]) {
                        resultado = true
                        break
                    }
                }

            } else {
                if (Listado[i].Codigo !== undefined) {
                    if (Objeto = Listado[i].Codigo) {
                        resultado = true
                        break
                    }
                } else {
                    if (Objeto = Listado[i]) {
                        resultado = true
                        break
                    }
                }
            }
        }
    } catch (e) {
        resultado = false
    }
    return resultado
}
/*Objteto a String con salto de line <br>
*Ingreso: objeto
Salida: string con atriibutos y valor => atributo: valor<br>
MO: 14/08/2018*/
function JsObjToString(obj) {
    var string = "";
    for (var i in obj) {
        string += i + ": " + obj[i] + "<br>";
    }
    return string;
}
/*Obtener QRCode
*Ingreso: texto
Salida: QRCode codificado en base 64
MO: 15/08/2018*/
var qrcodeGenerator = function (text) {
    /*-TypeNumber: Define la cantidad de bytes que se pueden ingresar(14 para 902
    caracteres)
    -errorCorrelationLevel: Calidad del QR, mejora los pixeles de lectura,
    pero disminuye la cantidad de bytes de entrega
    {
        L=>bajo(7%),
        M=>bajo(15%),
        Q=>bajo(25%),
        H=>bajo(30%),
    }
    -modos: el modo que de los datos ingresados(dejar en bytes como default)
    -Multibyte: el charset que se desea manejar con los datos(dejar en UTF-8
    para caracteres especiales)
    */
    try {
        if (text.length > 902) {
            throw "La cantidad del texto excede la capacidad del qr";
        }
        var TypeNumber = '20';
        var errorCorrelationLevel = ['L', 'M', 'Q', 'H'];
        var modos = ['Numeric', 'Alphanumeric', 'Byte', 'Kanji'];
        var Multibyte = ['default', 'SJIS', 'UTF-8'];
        text = text.replace(/^[\s\u3000]+|[\s\u3000]+$/g, '');
        return create_qrcode(text, TypeNumber, errorCorrelationLevel[1], modos[2], Multibyte[2]);
    }
    catch (err) {
        console.error("Error al crear QR: ", err);
    }
};
//-- Sumar dias a una fecha
Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

var OBJ_TOKEN_SOFTTOOLS = {}
function ajaxRequest(request) {
    var JQDataSend = $.ajax({
        //type: "POST",
        method: "POST",
        crossDomain: true,
        url: request.url,
        async: false,
        headers: {
            "Authorization": OBJ_TOKEN_SOFTTOOLS.data.token_type + " " + OBJ_TOKEN_SOFTTOOLS.data.access_token,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        processData: false,
        async: false,
        dataType: 'json',
        data: JSON.stringify(request.data),
    });
    return JSON.parse(JQDataSend.responseText)
}
function ValidarListadoAutocomplete(ListaConsulta, ListaAutocomplete) {
    if (ListaAutocomplete.length > 0) {
        for (var j = 0; j < ListaConsulta.length; j++) {
            var count = 0
            for (var i = 0; i < ListaAutocomplete.length; i++) {
                if (ListaConsulta[j].Codigo == ListaAutocomplete[i].Codigo) {
                    count++
                    break;
                }
            }
            if (count == 0) {
                ListaAutocomplete.push(ListaConsulta[j])
            }
        }
    } else {
        ListaAutocomplete = ListaConsulta;
    }
    return ListaAutocomplete
}
function ValidarListadoAutocompleteAlterno(ListaConsulta, ListaAutocomplete) {
    if (ListaAutocomplete.length > 0) {
        for (var j = 0; j < ListaConsulta.length; j++) {
            var count = 0
            for (var i = 0; i < ListaAutocomplete.length; i++) {
                if (ListaConsulta[j] == ListaAutocomplete[i]) {
                    count++
                    break;
                }
            }
            if (count == 0) {
                ListaAutocomplete.push(ListaConsulta[j])
            }
        }
    } else {
        ListaAutocomplete = ListaConsulta;
    }
    return ListaAutocomplete
}
var BloqueoPantalla = undefined


function ShowNotificacionInfo(Mensaje) {
    var notificator = new Notificacion(document.querySelector('.notification'));
    notificator.info(Mensaje);
}
function ShowNotificacionWarning(Mensaje) {
    var notificator = new Notificacion(document.querySelector('.notification'));
    notificator.warning(Mensaje);
}
function ShowNotificacionError(Mensaje) {
    var notificator = new Notificacion(document.querySelector('.notification'));
    notificator.error(Mensaje);
}
function MostrarNotificación(Mensaje, icon, funcion, NUM) {
    if (Mensaje !== undefined && Mensaje !== '') {
        if (Notification) {
            if (Notification.permission !== "granted") {
                Notification.requestPermission()
            }
            var title = 'GESCARGA .NET'
            var extra = {
                icon: icon,
                body: Mensaje
            }
            var noti = new Notification(title, extra)
            if (funcion !== undefined) {
                noti.onclick = function () {
                    noti.close()
                    if (typeof funcion == 'function') {
                        funcion(NUM)
                    }
                }
            }
            noti.onclose = {
                // Al cerrar
            }
            setTimeout(function () {
                try {
                    noti.close()
                } catch (e) {

                }
            }, 5000)
        }
    }
}

function FormatoFechaISO8601(Fecha) {
    var año = Fecha.getFullYear()
    var mes = (Fecha.getMonth() + 1)
    var dias = Fecha.getDate()
    var horas = Fecha.getHours()
    var minutos = Fecha.getMinutes()

    if (mes.toString().length == 1) {
        mes = '0' + mes.toString()
    }
    if (dias.toString().length == 1) {
        dias = '0' + dias.toString()
    }
    if (horas.toString().length == 1) {
        horas = '0' + horas.toString()
    }
    if (minutos.toString().length == 1) {
        minutos = '0' + minutos.toString()
    }
    var StringFecha = año + '-' + mes + '-' + dias + 'T' + horas + ':' + minutos + ':00-05:00'

    return new Date(StringFecha)

}
function MascaraPlacaSemirremolque(option) {
    option = option.toString().toUpperCase();
    var numeroarray = [];
    var numero = '';

    for (var i = 0; i < option.length; i++) {
        patron1 = /[RS]/
        patron = /[0123456789]/
        if (i == 0) {
            if (patron1.test(option[i])) {
                if ((option[i]) == ' ') {
                    option[i] = '';
                } else {
                    numeroarray.push(option[i])
                }
            }
        } else {
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    numeroarray.push(option[i])
                }
            }
        }

    }


    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    return numero.toUpperCase()
}
function RevertirMV(Valor) {
    if (Valor == undefined || Valor == '') {
        return 0
    }
    else {

        try {
            return parseFloat(MascaraDecimales(Valor))
        } catch (e) {
            return 0
        }
    }
}
//--Filtrar por porcentaje like sql
//--Ingreso: texto a buscar
//--Salida: resultado math entre el string de busqueda y el string regexp
//-- 30/07/2020
String.prototype.likeFind = function (search) {
    if (typeof search !== 'string' || this === null) { return false; }
    // Remove special chars
    search = search.replace(new RegExp("([\\.\\\\\\+\\*\\?\\[\\^\\]\\$\\(\\)\\{\\}\\=\\!\\<\\>\\|\\:\\-])", "g"), "\\$1");
    // Replace % and _ with equivalent regex
    search = search.replace(/%/g, '.*').replace(/_/g, '.');
    // Check matches
    return RegExp('^' + search + '$', 'gi').test(this);
};
//--Suma Elementos del elemento del objeto de un array
//--Ingreso: atributo o propiedad
//--Salida: sumatoria del contendio del valor de la propiedad
//-- 09/09/2020
Array.prototype.sum = function (prop) {
    var total = 0;
    for (var i = 0, _len = this.length; i < _len; i++) {
        total += this[i][prop];
    }
    return total;
};