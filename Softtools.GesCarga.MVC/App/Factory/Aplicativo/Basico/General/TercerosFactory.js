﻿SofttoolsApp.factory('TercerosFactory', ['$http', function ($http) {

    var urlService = GetUrl('Terceros');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
      
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.InsertarDirecciones = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/InsertarDirecciones';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarTerceroGeneral = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarTerceroGeneral';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ObtenerTerceroEstudioSeguridad = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerTerceroEstudioSeguridad';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarDocumentosProximosVencer = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarDocumentosProximosVencer';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarDocumentos = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarDocumentos';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.GenerarPlanitilla = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GenerarPlanitilla';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.ConsultarAuditoria = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarAuditoria';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.GuardarNovedad = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/GuardarNovedad';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    return factory;
}]);