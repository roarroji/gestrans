﻿SofttoolsApp.factory('AutorizacionesFactory', ['$http', function ($http) {

    var urlService = GetUrl('Autorizaciones');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Notificar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Notificar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Eliminar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Eliminar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    return factory;
}]);