﻿SofttoolsApp.factory('FacturasFactory', ['$http', function ($http) {

    var urlService = GetUrl('Facturas');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.ObtenerDatosFacturaElectronicaSaphety = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerDatosFacturaElectronicaSaphety';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.GuardarFacturaElectronica = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/GuardarFacturaElectronica';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ObtenerDocumentoReporteSaphety = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerDocumentoReporteSaphety';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    return factory;
}]);