﻿SofttoolsApp.factory('LegalizarRemesasPaqueteriaFactory', ['$http', function ($http) {
    var urlService = GetUrl('LegalizarRemesasPaqueteria');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    };
    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.EliminarRemesa = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/EliminarRemesa';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarOtrasLegalizaciones = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarOtrasLegalizaciones';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    return factory;
}]);