var SofttoolsApp = angular.module('SofttoolsApp', ['ngRoute', 'ui.bootstrap', 'ngSignaturePad', 'blockUI', 'angular-linq', 'ui.mask', 'LocalStorageModule']);

SofttoolsApp.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorFactory');
});

SofttoolsApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.

            when('/Mapa', {
                templateUrl: 'Mapa.html',
                controller: 'MapaCtrl'
            }).
            //------------------------------------------//
            //Basico-General
            //Empresas
            when('/ConsultarEmpresas', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarEmpresas.html',
                controller: 'ConsultarEmpresasCtrl'
            }).
            when('/ConsultarEmpresas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarEmpresas.html',
                controller: 'ConsultarEmpresasCtrl'
            }).
            when('/GestionarEmpresas', {
                templateUrl: 'Aplicativo/Basico/General/GestionarEmpresas.html',
                controller: 'ConsultarEmpresasCtrl'
            }).
            when('/GestionarEmpresas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarEmpresas.html',
                controller: 'ConsultarEmpresasCtrl'
            }).
            //Terceros
            when('/ConsultarTerceros', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarTerceros.html',
                controller: 'ConsultarTercerosCtrl'
            }).
            when('/ConsultarTerceros/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarTerceros.html',
                controller: 'ConsultarTercerosCtrl'
            }).
            when('/GestionarTerceros', {
                templateUrl: 'Aplicativo/Basico/General/GestionarTerceros.html',
                controller: 'GestionarTercerosCtrl'
            }).
            when('/GestionarTerceros/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarTerceros.html',
                controller: 'GestionarTercerosCtrl'
            }).
            //Oficinas
            when('/ConsultarOficinas', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarOficinas.html',
                controller: 'ConsultarOficinasCtrl'
            }).
            when('/ConsultarOficinas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarOficinas.html',
                controller: 'ConsultarOficinasCtrl'
            }).
            when('/GestionarOficinas', {
                templateUrl: 'Aplicativo/Basico/General/GestionarOficinas.html',
                controller: 'GestionarOficinasCtrl'
            }).
            when('/GestionarOficinas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarOficinas.html',
                controller: 'GestionarOficinasCtrl'
            }).
            //Puertos Paises
            when('/ConsultarPuertosPaises', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarPuertosPaises.html',
                controller: 'ConsultarPuertosPaisesCtrl'
            }).
            when('/ConsultarPuertosPaises/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarPuertosPaises.html',
                controller: 'ConsultarPuertosPaisesCtrl'
            }).
            when('/GestionarPuertosPaises', {
                templateUrl: 'Aplicativo/Basico/General/GestionarPuertosPaises.html',
                controller: 'GestionarPuertosPaisesCtrl'
            }).
            when('/GestionarPuertosPaises/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarPuertosPaises.html',
                controller: 'GestionarPuertosPaisesCtrl'
            }).
            //Formatos Inspecciones
            when('/ConsultarFormularioInspecciones', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarFormularioInspecciones.html',
                controller: 'ConsultarFormularioInspeccionesCtrl'
            }).
            when('/ConsultarFormularioInspecciones/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarFormularioInspecciones.html',
                controller: 'ConsultarFormularioInspeccionesCtrl'
            }).
            when('/GestionarFormularioInspecciones', {
                templateUrl: 'Aplicativo/Basico/General/GestionarFormularioInspecciones.html',
                controller: 'GestionarFormularioInspeccionesCtrl'
            }).
            when('/GestionarFormularioInspecciones/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarFormularioInspecciones.html',
                controller: 'GestionarFormularioInspeccionesCtrl'
            }).
            //Catalogos
            when('/ConsultarCatalogos', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarCatalogos.html',
                controller: 'ConsultarCatalogosCtrl'
            }).
            when('/ConsultarCatalogos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarCatalogos.html',
                controller: 'ConsultarCatalogosCtrl'
            }).
            when('/GestionarCatalogos', {
                templateUrl: 'Aplicativo/Basico/General/GestionarCatalogos.html',
                controller: 'GestionarCatalogosCtrl'
            }).
            when('/GestionarCatalogos/:CodigoCatalogo/:Codigo/:Consecutivo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarCatalogos.html',
                controller: 'GestionarCatalogosCtrl'
            }).

            //Ciudades
            when('/ConsultarCiudades', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarCiudades.html',
                controller: 'ConsultarCiudadesCtrl'
            }).
            when('/ConsultarCiudades/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarCiudades.html',
                controller: 'ConsultarCiudadesCtrl'
            }).
            when('/GestionarCiudades', {
                templateUrl: 'Aplicativo/Basico/General/GestionarCiudades.html',
                controller: 'GestionarCiudadesCtrl'
            }).
            when('/GestionarCiudades/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarCiudades.html',
                controller: 'GestionarCiudadesCtrl'
            }).
            //Moneda y TRM 

            when('/ConsultarMonedas', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarMoneda.html',
                controller: 'ConsultarMonedaCtrl'
            }).

            when('/ConsultarMonedas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarMoneda.html',
                controller: 'ConsultarMonedaCtrl'
            }).

            when('/GestionarMoneda', {
                templateUrl: 'Aplicativo/Basico/General/GestionarMoneda.html',
                controller: 'GestionarMonedaCtrl'
            }).
            when('/GestionarMoneda/:Local', {
                templateUrl: 'Aplicativo/Basico/General/GestionarMoneda.html',
                controller: 'GestionarMonedaCtrl'
            }).
            when('/GestionarMoneda/:Codigo/:Local', {
                templateUrl: 'Aplicativo/Basico/General/GestionarMoneda.html',
                controller: 'GestionarMonedaCtrl'
            }).

            when('/GestionarTRM/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarTRM.html',
                controller: 'GestionarTRMCtrl'
            }).

            //Impuestos 
            when('/ConsultarImpuestos', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarImpuestos.html',
                controller: 'ConsultarImpuestosCtrl'
            }).
            when('/ConsultarImpuestos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarImpuestos.html',
                controller: 'ConsultarImpuestosCtrl'
            }).

            when('/GestionarImpuesto', {
                templateUrl: 'Aplicativo/Basico/General/GestionarImpuestos.html',
                controller: 'GestionarImpuestoCtrl'
            }).
            when('/GestionarImpuesto/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarImpuestos.html',
                controller: 'GestionarImpuestoCtrl'
            }).
            //Listados Basico  
            when('/ConsultarListadoBasico', {
                templateUrl: 'Aplicativo/Basico/Listados/ConsultarListadoBasico.html',
                controller: 'ConsultarListadoBasicoCtrl'
            }).

            //Tipo Documentos 
            when('/ConsultarTipoDocumentos', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarTipoDocumentos.html',
                controller: 'ConsultarTipoDocumentoCtrl'
            }).
            when('/ConsultarTipoDocumentos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarTipoDocumentos.html',
                controller: 'ConsultarTipoDocumentoCtrl'
            }).
            when('/GestionarTipoDocumentos', {
                templateUrl: 'Aplicativo/Basico/General/GestionarTipoDocumentos.html',
                controller: 'GestionarTipoDocumentoCtrl'
            }).
            when('/GestionarTipoDocumentos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarTipoDocumentos.html',
                controller: 'GestionarTipoDocumentoCtrl'
            }).
            //ConceptosLiquidacion 
            when('/ConsultarConceptosLiquidacion', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarConceptoLiquidacion.html',
                controller: 'ConsultarConceptosLiquidacionCrtr'
            }).
            when('/ConsultarConceptosLiquidacion/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarConceptoLiquidacion.html',
                controller: 'ConsultarConceptosLiquidacionCrtr'
            }).
            when('/GestionarConceptosLiquidacion', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarConceptoLiquidacion.html',
                controller: 'GestionarConceptosLiquidacionCtrl'
            }).
            when('/GestionarConceptosLiquidacion/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarConceptoLiquidacion.html',
                controller: 'GestionarConceptosLiquidacionCtrl'
            }).

            //ConceptosFacturacion
            when('/ConsultarConceptosFacturacion', {
                templateUrl: 'Aplicativo/Basico/Facturacion/ConsultarConceptoFacturacion.html',
                controller: 'ConsultarConceptosFacturacionCrtr'
            }).
            when('/ConsultarConceptosFacturacion/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Facturacion/ConsultarConceptoFacturacion.html',
                controller: 'ConsultarConceptosFacturacionCrtr'
            }).
            when('/GestionarConceptosFacturacion', {
                templateUrl: 'Aplicativo/Basico/Facturacion/GestionarConceptoFacturacion.html',
                controller: 'GestionarConceptosFacturacionCtrl'
            }).
            when('/GestionarConceptosFacturacion/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Facturacion/GestionarConceptoFacturacion.html',
                controller: 'GestionarConceptosFacturacionCtrl'
            }).


            //Precintos
            when('/ConsultarPrecintos', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarPrecintos.html',
                controller: 'ConsultarPrecintoCtrl'
            }).
            when('/ConsultarPrecintos/:Numero', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarPrecintos.html',
                controller: 'ConsultarPrecintoCtrl'
            }).
            when('/GestionarAsignacionPresintos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarAsignacionPrecintos.html',
                controller: 'GestionarAsignacionPrecintosCtrl'
            }).
            when('/GestionarAsignacionPresintos/:Numero', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarAsignacionPrecintos.html',
                controller: 'GestionarAsignacionPrecintosCtrl'
            }).
            when('/GestionarTransladoPresintos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarTransladoPrecintos.html',
                controller: 'GestionarTransladoPrecintosCtrl'
            }).
            when('/GestionarTransladoPresintos/:Numero', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarTransladoPrecintos.html',
                controller: 'GestionarTransladoPrecintosCtrl'
            }).



            //Almacenes
            when('/ConsultarAlmacenes', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarAlmacenes.html',
                controller: 'ConsultarAlmacenesCtrl'
            }).
            when('/ConsultarAlmacenes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarAlmacenes.html',
                controller: 'ConsultarAlmacenesCtrl'
            }).
            when('/GestionarAlmacenes', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarAlmacenes.html',
                controller: 'GestionarAlmacenesCtrl'
            }).
            when('/GestionarAlmacenes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarAlmacenes.html',
                controller: 'GestionarAlmacenesCtrl'
            }).
            //Grupo Referencias
            when('/ConsultarGrupoReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarGrupoReferencia.html',
                controller: 'ConsultarGrupoReferenciasCtrl'
            }).
            when('/ConsultarGrupoReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarGrupoReferencia.html',
                controller: 'ConsultarGrupoReferenciasCtrl'
            }).
            when('/GestionarGrupoReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarGrupoReferencias.html',
                controller: 'GestionarGrupoReferenciasCtrl'
            }).
            when('/GestionarGrupoReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarGrupoReferencias.html',
                controller: 'GestionarGrupoReferenciasCtrl'
            }).
            //Referencia Almacenes
            when('/ConsultarReferenciaAlmacenes', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarReferenciaAlmacenes.html',
                controller: 'ConsultarReferenciaAlmacenesCtrl'
            }).
            when('/ConsultarReferenciaAlmacenes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarReferenciaAlmacenes.html',
                controller: 'ConsultarReferenciaAlmacenesCtrl'
            }).
            when('/GestionarReferenciaAlmacenes', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarReferenciaAlmacenes.html',
                controller: 'GestionarReferenciaAlmacenesCtrl'
            }).
            when('/GestionarReferenciaAlmacenes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarReferenciaAlmacenes.html',
                controller: 'GestionarReferenciaAlmacenesCtrl'
            }).
            // Referencia Proveedores
            when('/ConsultarReferenciaProveedores', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarReferenciaProveedores.html',
                controller: 'ConsultarReferenciaProveedoresCtrl'
            }).
            when('/ConsultarReferenciaProveedores/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarReferenciaProveedores.html',
                controller: 'ConsultarReferenciaProveedoresCtrl'
            }).
            when('/GestionarReferenciaProveedores', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarReferenciaProveedores.html',
                controller: 'GestionarReferenciaProveedoresCtrl'
            }).
            when('/GestionarReferenciaProveedores/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarReferenciaProveedores.html',
                controller: 'GestionarReferenciaProveedoresCtrl'
            }).
            //Referencias
            when('/ConsultarReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarReferencias.html',
                controller: 'ConsultarReferenciasCtrl'
            }).
            when('/ConsultarReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarReferencias.html',
                controller: 'ConsultarReferenciasCtrl'
            }).
            when('/GestionarReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarReferencias.html',
                controller: 'GestionarReferenciasCtrl'
            }).
            when('/GestionarReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarReferencias.html',
                controller: 'GestionarReferenciasCtrl'
            }).
            //Ubicacion Almacenes
            when('/ConsultarUbicacionAlmacenes', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarUbicacionAlmacenes.html',
                controller: 'ConsultarUbicacionAlmacenesCtrl'
            }).
            when('/ConsultarUbicacionAlmacenes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarUbicacionAlmacenes.html',
                controller: 'ConsultarUbicacionAlmacenesCtrl'
            }).
            when('/GestionarUbicacionAlmacenes', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarUbicacionAlmacenes.html',
                controller: 'GestionarUbicacionAlmacenesCtrl'
            }).
            when('/GestionarUbicacionAlmacenes/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarUbicacionAlmacenes.html',
                controller: 'GestionarUbicacionAlmacenesCtrl'
            }).
            //Unidad Empaque
            when('/ConsultarUnidadEmpaqueReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarUnidadEmpaqueReferencias.html',
                controller: 'ConsultarUnidadEmpaqueReferenciasCtrl'
            }).
            when('/ConsultarUnidadEmpaqueReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarUnidadEmpaqueReferencias.html',
                controller: 'ConsultarUnidadEmpaqueReferenciasCtrl'
            }).
            when('/GestionarUnidadEmpaqueReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarUnidadEmpaqueReferencias.html',
                controller: 'GestionarUnidadEmpaqueReferenciasCtrl'
            }).
            when('/GestionarUnidadEmpaqueReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarUnidadEmpaqueReferencias.html',
                controller: 'GestionarUnidadEmpaqueReferenciasCtrl'
            }).
            //Unidad Medida
            when('/ConsultarUnidadMedidaReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarUnidadMedidaReferencias.html',
                controller: 'ConsultarUnidadMedidaReferenciasCtrl'
            }).
            when('/ConsultarUnidadMedidaReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/ConsultarUnidadMedidaReferencias.html',
                controller: 'ConsultarUnidadMedidaReferenciasCtrl'
            }).
            when('/GestionarUnidadMedidaReferencias', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarUnidadMedidaReferencias.html',
                controller: 'GestionarUnidadMedidaReferenciasCtrl'
            }).
            when('/GestionarUnidadMedidaReferencias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Almacen/GestionarUnidadMedidaReferencias.html',
                controller: 'GestionarUnidadMedidaReferenciasCtrl'
            }).
            //Productos Transportados
            when('/ConsultarProductoTransportados', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarProductoTransportados.html',
                controller: 'ConsultarProductoTransportadosCtrl'
            }).
            when('/ConsultarProductoTransportados/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarProductoTransportados.html',
                controller: 'ConsultarProductoTransportadosCtrl'
            }).
            when('/GestionarProductoTransportados', {
                templateUrl: 'Aplicativo/Basico/General/GestionarProductoTransportados.html',
                controller: 'GestionarProductoTransportadosCtrl'
            }).
            when('/GestionarProductoTransportados/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarProductoTransportados.html',
                controller: 'GestionarProductoTransportadosCtrl'
            }).

            //Basico-Despachos
            //ColorVehiculos
            when('/ConsultarColorVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarColorVehiculos.html',
                controller: 'ConsultarColorVehiculosCtrl'
            }).
            when('/ConsultarColorVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarColorVehiculos.html',
                controller: 'ConsultarColorVehiculosCtrl'
            }).
            when('/GestionarColorVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarColorVehiculos.html',
                controller: 'GestionarColorVehiculosCtrl'
            }).
            when('/GestionarColorVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarColorVehiculos.html',
                controller: 'GestionarColorVehiculosCtrl'
            }).
            //MarcaVehiculos
            when('/ConsultarMarcaVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarMarcaVehiculos.html',
                controller: 'ConsultarMarcaVehiculosCtrl'
            }).
            when('/ConsultarMarcaVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarMarcaVehiculos.html',
                controller: 'ConsultarMarcaVehiculosCtrl'
            }).
            when('/GestionarMarcaVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarMarcaVehiculos.html',
                controller: 'GestionarMarcaVehiculosCtrl'
            }).
            when('/GestionarMarcaVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarMarcaVehiculos.html',
                controller: 'GestionarMarcaVehiculosCtrl'
            }).
            //LineaVehiculos
            when('/ConsultarLineaVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarLineaVehiculos.html',
                controller: 'ConsultarLineaVehiculosCtrl'
            }).
            when('/ConsultarLineaVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarLineaVehiculos.html',
                controller: 'ConsultarLineaVehiculosCtrl'
            }).
            when('/GestionarLineaVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarLineaVehiculos.html',
                controller: 'GestionarLineaVehiculosCtrl'
            }).
            when('/GestionarLineaVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarLineaVehiculos.html',
                controller: 'GestionarLineaVehiculosCtrl'
            }).
            //Rutas
            when('/ConsultarRutas', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarRutas.html',
                controller: 'ConsultarRutasCtrl'
            }).
            when('/ConsultarRutas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarRutas.html',
                controller: 'ConsultarRutasCtrl'
            }).
            when('/GestionarRutas', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarRutas.html',
                controller: 'GestionarRutasCtrl'
            }).
            when('/GestionarRutas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarRutas.html',
                controller: 'GestionarRutasCtrl'
            }).
            //Novedades Despachos
            when('/ConsultarNovedadesDespachos', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarNovedadesDespachos.html',
                controller: 'ConsultarNovedadesDespachosCtrl'
            }).
            when('/ConsultarNovedadesDespachos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarNovedadesDespachos.html',
                controller: 'ConsultarNovedadesDespachosCtrl'
            }).
            when('/GestionarNovedadesDespachos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarNovedadesDespachos.html',
                controller: 'GestionarNovedadesDespachosCtrl'
            }).
            when('/GestionarNovedadesDespachos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarNovedadesDespachos.html',
                controller: 'GestionarNovedadesDespachosCtrl'
            }).
            //Sitios Cargue y Descargue
            when('/ConsultarSitiosCargueDescargue', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarSitiosCargueDescargue.html',
                controller: 'ConsultarSitiosCargueDescargueCtrl'
            }).
            when('/ConsultarSitiosCargueDescargue/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarSitiosCargueDescargue.html',
                controller: 'ConsultarSitiosCargueDescargueCtrl'
            }).
            when('/GestionarSitiosCargueDescargue', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarSitiosCargueDescargue.html',
                controller: 'GestionarSitiosCargueDescargueCtrl'
            }).
            when('/GestionarSitiosCargueDescargue/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarSitiosCargueDescargue.html',
                controller: 'GestionarSitiosCargueDescargueCtrl'
            }).
            //Vehiculos
            when('/ConsultarVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarVehiculos.html',
                controller: 'ConsultarVehiculosCtrl'
            }).
            when('/ConsultarVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarVehiculos.html',
                controller: 'ConsultarVehiculosCtrl'
            }).
            when('/GestionarVehiculos', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarVehiculos.html',
                controller: 'GestionarVehiculosCtrl'
            }).
            when('/GestionarVehiculos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarVehiculos.html',
                controller: 'GestionarVehiculosCtrl'
            }).
            //MarcaSemirremolques
            when('/ConsultarMarcaSemirremolques', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarMarcaSemirremolques.html',
                controller: 'ConsultarMarcaSemirremolquesCtrl'
            }).
            when('/ConsultarMarcaSemirremolques/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarMarcaSemirremolques.html',
                controller: 'ConsultarMarcaSemirremolquesCtrl'
            }).
            when('/GestionarMarcaSemirremolques', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarMarcaSemirremolques.html',
                controller: 'GestionarMarcaSemirremolquesCtrl'
            }).
            when('/GestionarMarcaSemirremolques/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarMarcaSemirremolques.html',
                controller: 'GestionarMarcaSemirremolquesCtrl'
            }).
            //Semirremolques
            when('/ConsultarSemirremolques', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarSemirremolques.html',
                controller: 'ConsultarSemirremolquesCtrl'
            }).
            when('/ConsultarSemirremolques/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarSemirremolques.html',
                controller: 'ConsultarSemirremolquesCtrl'
            }).
            when('/GestionarSemirremolques', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarSemirremolques.html',
                controller: 'GestionarSemirremolquesCtrl'
            }).
            when('/GestionarSemirremolques/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarSemirremolques.html',
                controller: 'GestionarSemirremolquesCtrl'
            }).
            //Puestos de control
            when('/ConsultarPuestosControl', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarPuestoControles.html',
                controller: 'ConsultarPuestoControlesCtrl'
            }).
            when('/ConsultarPuestosControl/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/ConsultarPuestoControles.html',
                controller: 'ConsultarPuestoControlesCtrl'
            }).
            when('/GestionarPuestoControles', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarPuestoControles.html',
                controller: 'GestionarPuestoControlesCtrl'
            }).
            when('/GestionarPuestoControles/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Despachos/GestionarPuestoControles.html',
                controller: 'GestionarPuestoControlesCtrl'
            }).

            //Básico->Tesorería
            //Bancos
            when('/ConsultarBancos', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarBancos.html',
                controller: 'ConsultarBancosCtrl'
            }).
            when('/ConsultarBancos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarBancos.html',
                controller: 'ConsultarBancosCtrl'
            }).
            when('/GestionarBancos', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarBancos.html',
                controller: 'GestionarBancosCtrl'
            }).
            when('/GestionarBancos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarBancos.html',
                controller: 'GestionarBancosCtrl'
            }).
            //Plan Unico Cuentas
            when('/ConsultarPlanUnicoCuentas', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarPlanUnicoCuentas.html',
                controller: 'ConsultarPlanUnicoCuentasCtrl'
            }).
            when('/ConsultarPlanUnicoCuentas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarPlanUnicoCuentas.html',
                controller: 'ConsultarPlanUnicoCuentasCtrl'
            }).
            when('/GestionarPlanUnicoCuentas', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarPlanUnicoCuentas.html',
                controller: 'GestionarPlanUnicoCuentasCtrl'
            }).
            when('/GestionarPlanUnicoCuentas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarPlanUnicoCuentas.html',
                controller: 'GestionarPlanUnicoCuentasCtrl'
            }).
            //Cuenta Bancarias
            when('/ConsultarCuentaBancarias', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarCuentaBancarias.html',
                controller: 'ConsultarCuentaBancariasCtrl'
            }).
            when('/ConsultarCuentaBancarias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarCuentaBancarias.html',
                controller: 'ConsultarCuentaBancariasCtrl'
            }).
            when('/GestionarCuentaBancarias', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarCuentaBancarias.html',
                controller: 'GestionarCuentaBancariasCtrl'
            }).
            when('/GestionarCuentaBancarias/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarCuentaBancarias.html',
                controller: 'GestionarCuentaBancariasCtrl'
            }).
            //Chequera cuenta bancarias
            when('/ConsultarChequeras', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarChequeraCuentaBancaria.html',
                controller: 'ConsultarChequeraCuentaBancariasCtrl'
            }).
            when('/ConsultarChequeras/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarChequeraCuentaBancaria.html',
                controller: 'ConsultarChequeraCuentaBancariasCtrl'
            }).
            when('/GestionarChequeras', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarChequeraCuentaBancarias.html',
                controller: 'GestionarChequeraCuentaBancariasCtrl'
            }).
            when('/GestionarChequeras/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarChequeraCuentaBancarias.html',
                controller: 'GestionarChequeraCuentaBancariasCtrl'
            }).
            //cajas
            when('/ConsultarCajas', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarCajas.html',
                controller: 'ConsultarCajasCtrl'
            }).
            when('/ConsultarCajas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarCajas.html',
                controller: 'ConsultarCajasCtrl'
            }).
            when('/GestionarCajas', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarCajas.html',
                controller: 'GestionarCajasCtrl'
            }).
            when('/GestionarCajas/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarCajas.html',
                controller: 'GestionarCajasCtrl'
            }).
            //Conceptos Contables
            when('/ConsultarConceptoContables', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarConceptoContables.html',
                controller: 'ConsultarConceptoContablesCtrl'
            }).
            when('/ConsultarConceptoContables/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarConceptoContables.html',
                controller: 'ConsultarConceptoContablesCtrl'
            }).
            when('/GestionarConceptoContables', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarConceptoContables.html',
                controller: 'GestionarConceptoContablesCtrl'
            }).
            when('/GestionarConceptoContables/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarConceptoContables.html',
                controller: 'GestionarConceptoContablesCtrl'
            }).

            //Sucursales SIESA
            when('/ConsultarSucursalesSIESA', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarSucursalesSIESA.html',
                controller: 'ConsultarSucursalesSIESACtrl'
            }).
            when('/ConsultarSucursalesSIESA/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarSucursalesSIESA.html',
                controller: 'ConsultarSucursalesSIESACtrl'
            }).

            when('/GestionarSucursalSIESA', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarSucursalSIESA.html',
                controller: 'GestionarSucursalSIESACtrl'
            }).

            when('/GestionarSucursalSIESA/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarSucursalSIESA.html',
                controller: 'GestionarSucursalSIESACtrl'
            }).

            //Parametrizacion Contables
            when('/ConsultarParametrizacionContables', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarParametrizacionContables.html',
                controller: 'ConsultarParametrizacionContablesCtrl'
            }).
            when('/ConsultarParametrizacionContables/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/ConsultarParametrizacionContables.html',
                controller: 'ConsultarParametrizacionContablesCtrl'
            }).
            when('/GestionarParametrizacionContables', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarParametrizacionContables.html',
                controller: 'GestionarParametrizacionContablesCtrl'
            }).
            when('/GestionarParametrizacionContables/:Codigo', {
                templateUrl: 'Aplicativo/Basico/Tesoreria/GestionarParametrizacionContables.html',
                controller: 'GestionarParametrizacionContablesCtrl'
            }).

            //Control Viajes
            //Inspección Preoperacional
            when('/ConsultarInspeccionPreoperacional', {
                templateUrl: 'Aplicativo/ControlViajes/Documentos/ConsultarInspeccionPreoperacional.html',
                controller: 'ConsultarInspeccionPreoperacionalCtrl'
            }).
            when('/ConsultarInspeccionPreoperacional/:Numero', {
                templateUrl: 'Aplicativo/ControlViajes/Documentos/ConsultarInspeccionPreoperacional.html',
                controller: 'ConsultarInspeccionPreoperacionalCtrl'
            }).
            when('/GestionarInspeccionPreoperacional', {
                templateUrl: 'Aplicativo/ControlViajes/Documentos/GestionarInspeccionPreoperacional.html',
                controller: 'GestionarInspeccionPreoperacionalCtrl'
            }).
            when('/GestionarInspeccionPreoperacional/:Numero', {
                templateUrl: 'Aplicativo/ControlViajes/Documentos/GestionarInspeccionPreoperacional.html',
                controller: 'GestionarInspeccionPreoperacionalCtrl'
            }).
            //Seguimiento Vehículos
            when('/ConsultarSeguimientoVehiculosTransito', {
                templateUrl: 'Aplicativo/ControlViajes/ConsultarSeguimientoVehiculos.html',
                controller: 'ConsultarSeguimientoVehiculosCtrl'
            }).
            //Consultas Seguimiento Vehículos
            when('/ConsultasSeguimientoVehicular', {
                templateUrl: 'Aplicativo/ControlViajes/ConsultasSeguimiento.html',
                controller: 'ConsultasSeguimientoCtrl'
            }).
            //Autorizaciones
            when('/ConsultarAutorizaciones', {
                templateUrl: 'Aplicativo/ControlViajes/ConsultarSolicitudAutorizaciones.html',
                controller: 'ConsultarSolicitudAutorizacionesCtrl'
            }).
            when('/ConsultarAutorizaciones/:Numero', {
                templateUrl: 'Aplicativo/ControlViajes/ConsultarSolicitudAutorizaciones.html',
                controller: 'ConsultarSolicitudAutorizacionesCtrl'
            }).

            //PAQUETERIA-Documentos
            //Recolecciones
            when('/ConsultarRecolecciones', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/ConsultarRecolecciones.html',
                controller: 'ConsultarRecoleccionesCtrl'
            }).
            when('/ConsultarRecolecciones/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/ConsultarRecolecciones.html',
                controller: 'ConsultarRecoleccionesCtrl'
            }).
            when('/GestionarRecolecciones', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/GestionarRecolecciones.html',
                controller: 'GestionarRecoleccionesCtrl'
            }).
            when('/GestionarRecolecciones/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/GestionarRecolecciones.html',
                controller: 'GestionarRecoleccionesCtrl'
            }).
            //Planilla Recolecciones
            when('/ConsultarPlanillaRecolecciones', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/ConsultarPlanillaRecolecciones.html',
                controller: 'ConsultarPlanillaRecoleccionesCtrl'
            }).
            when('/ConsultarPlanillaRecolecciones/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/ConsultarPlanillaRecolecciones.html',
                controller: 'ConsultarPlanillaRecoleccionesCtrl'
            }).
            when('/GestionarPlanillaRecolecciones', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/GestionarPlanillaRecolecciones.html',
                controller: 'GestionarPlanillaRecoleccionesCtrl'
            }).
            when('/GestionarPlanillaRecolecciones/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/GestionarPlanillaRecolecciones.html',
                controller: 'GestionarPlanillaRecoleccionesCtrl'
            }).
            //CONTABILIDAD
            //Comprobantes Contables
            when('/ConsultarComprobantesContables', {
                templateUrl: 'Aplicativo/Contabilidad/Documentos/ConsultarComprobantesContables.html',
                controller: 'ConsultarComprobantesContablesCtrl'
            }).
            when('/ConsultarComprobantesContables/:Numero', {
                templateUrl: 'Aplicativo/Contabilidad/Documentos/ConsultarComprobantesContables.html',
                controller: 'ConsultarComprobantesContablesCtrl'
            }).
            when('/GestionarComprobantesContables', {
                templateUrl: 'Aplicativo/Contabilidad/Documentos/GestionarComprobantesContables.html',
                controller: 'GestionarComprobantesContablesCtrl'
            }).
            when('/GestionarComprobantesContables/:Numero', {
                templateUrl: 'Aplicativo/Contabilidad/Documentos/GestionarComprobantesContables.html',
                controller: 'GestionarComprobantesContablesCtrl'
            }).
            when('/GenerarInterfazContable', {
                templateUrl: 'Aplicativo/Contabilidad/Procesos/GenerarInterfazContable.html',
                controller: 'GenerarInterfazContableController'
            }).

            when('/ConsultarListadoFlotaPropia', {
                templateUrl: 'Aplicativo/FlotaPropia/Listados/ConsultarListadoFlotaPropia.html',
                controller: 'ConsultarListadoFlotaPropiaController'
            }).
            //Cierre Contable Documentos
            when('/GestionarCierreContable', {
                templateUrl: 'Aplicativo/Contabilidad/Procesos/GestionarCierreContable.html',
                controller: 'GestionarCierreContableCtrl'
            }).


            //SEGURIDAD
            //Estudio Seguridad
            when('/ConsultarEstudioSeguridad', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarEstudioSeguridad.html',
                controller: 'ConsultarEstudioSeguridadCtrl'
            }).
            when('/ConsultarEstudioSeguridad/:Numero', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarEstudioSeguridad.html',
                controller: 'ConsultarEstudioSeguridadCtrl'
            }).
            when('/GestionarEstudioSeguridad', {
                templateUrl: 'Aplicativo/Seguridad/GestionarEstudioSeguridad.html',
                controller: 'GestionarEstudioSeguridadCtrl'
            }).
            when('/GestionarEstudioSeguridad/:Numero', {
                templateUrl: 'Aplicativo/Seguridad/GestionarEstudioSeguridad.html',
                controller: 'GestionarEstudioSeguridadCtrl'
            }).
            //Aprobar Estudio Seguridad
            when('/AprobarEstudioSeguridad', {
                templateUrl: 'Aplicativo/Seguridad/AprobarEstudioSeguridad.html',
                controller: 'AprobarEstudioSeguridadCtrl'
            }).
            when('/AprobarEstudioSeguridad/:Numero', {
                templateUrl: 'Aplicativo/Seguridad/AprobarEstudioSeguridad.html',
                controller: 'AprobarEstudioSeguridadCtrl'
            }).
            //GESPHONE
            //Entregas
            when('/ControlEntregaRemesasDistribucion', {
                templateUrl: 'Gesphone/Entregas/ConsultarControlEntregas.html',
                controller: 'ConsultarControlEntregasCtrl'
            }).
            when('/ConsultarControlEntregas/:Numero', {
                templateUrl: 'Gesphone/Entregas/ConsultarControlEntregas.html',
                controller: 'ConsultarControlEntregasCtrl'
            }).
            when('/GestionarControlEntregas', {
                templateUrl: 'Gesphone/Entregas/GestionarControlEntregas.html',
                controller: 'GestionarControlEntregasCtrl'
            }).
            when('/GestionarControlEntregas/:Numero', {
                templateUrl: 'Gesphone/Entregas/GestionarControlEntregas.html',
                controller: 'GestionarControlEntregasCtrl'
            }).
            //Entrega Remesa Paqueteria
            when('/ControlEntregaGuias', {
                templateUrl: 'Gesphone/EntregasPaqueteria/ConsultarControlEntregasPaqueteria.html',
                controller: 'ConsultarControlEntregasPaqueteriaCtrl'
            }).
            when('/ControlEntregaGuias/:Numero', {
                templateUrl: 'Gesphone/EntregasPaqueteria/ConsultarControlEntregasPaqueteria.html',
                controller: 'ConsultarControlEntregasPaqueteriaCtrl'
            }).
            when('/GestionarControlEntregasGuias', {
                templateUrl: 'Gesphone/EntregasPaqueteria/GestionarControlEntregasPaqueteria.html',
                controller: 'GestionarControlEntregasPaqueteriaCtrl'
            }).
            when('/GestionarControlEntregasGuias/:Numero', {
                templateUrl: 'Gesphone/EntregasPaqueteria/GestionarControlEntregasPaqueteria.html',
                controller: 'GestionarControlEntregasPaqueteriaCtrl'
            }).
            //Departamentos
            when('/ConsultarDepartamentos', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarDepartamentos.html',
                controller: 'ConsultarDepartamentosCtrl'
            }).
            when('/ConsultarDepartamentos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarDepartamentos.html',
                controller: 'ConsultarDepartamentosCtrl'
            }).
            when('/GestionarDepartamentos', {
                templateUrl: 'Aplicativo/Basico/General/GestionarDepartamentos.html',
                controller: 'GestionarDepartamentosCtrl'
            }).
            when('/GestionarDepartamentos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarDepartamentos.html',
                controller: 'GestionarDepartamentosCtrl'
            }).
            //Paises
            when('/ConsultarPaises', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarPaises.html',
                controller: 'ConsultarPaisesCtrl'
            }).
            when('/ConsultarPaises/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/ConsultarPaises.html',
                controller: 'ConsultarPaisesCtrl'
            }).
            when('/GestionarPaises', {
                templateUrl: 'Aplicativo/Basico/General/GestionarPaises.html',
                controller: 'GestionarPaisesCtrl'
            }).
            when('/GestionarPaises/:Codigo', {
                templateUrl: 'Aplicativo/Basico/General/GestionarPaises.html',
                controller: 'GestionarPaisesCtrl'
            }).
            //Novedad Transito
            when('/GestionarNovedadTransito', {
                templateUrl: 'Gesphone/Transito/GestionarNovedadTransito.html',
                controller: 'GestionarNovedadTransitoCtrl'
            }).
            //ListaChequeoCargueDescargues
            when('/ConsultarListaChequeoCargueDescargue', {
                templateUrl: 'Gesphone/ListaChequeo/ConsultarListaChequeCargueDescargues.html',
                controller: 'ConsultarListaChequeoCargueDescarguesCtrl'
            }).
            when('/ConsultarListaChequeoCargueDescargue/:Numero', {
                templateUrl: 'Gesphone/ListaChequeo/ConsultarListaChequeCargueDescargues.html',
                controller: 'ConsultarListaChequeoCargueDescarguesCtrl'
            }).
            when('/GestionarListaChequeoCargueDescargue', {
                templateUrl: 'Gesphone/ListaChequeo/GestionarListaChequeoCargueDescargues.html',
                controller: 'GestionarListaChequeoCargueDescarguesCtrl'
            }).
            when('/GestionarListaChequeoCargueDescargue/:Numero', {
                templateUrl: 'Gesphone/ListaChequeo/GestionarListaChequeoCargueDescargues.html',
                controller: 'GestionarListaChequeoCargueDescarguesCtrl'
            }).
            //ListaChequeoVehiculos
            when('/ConsultarListaChequeoVehiculos', {
                templateUrl: 'Gesphone/ListaChequeo/ConsultarListaChequeoVehiculos.html',
                controller: 'ConsultarListaChequeoVehiculosCtrl'
            }).
            when('/ConsultarListaChequeoVehiculos/:Numero', {
                templateUrl: 'Gesphone/ListaChequeo/ConsultarListaChequeoVehiculos.html',
                controller: 'ConsultarListaChequeoVehiculosCtrl'
            }).
            when('/GestionarListaChequeoVehiculos', {
                templateUrl: 'Gesphone/ListaChequeo/GestionarListaChequeoVehiculos.html',
                controller: 'GestionarListaChequeoVehiculosCtrl'
            }).
            when('/GestionarListaChequeoVehiculos/:Numero', {
                templateUrl: 'Gesphone/ListaChequeo/GestionarListaChequeoVehiculos.html',
                controller: 'GestionarListaChequeoVehiculosCtrl'
            }).

            //TESORERIA
            //Comprobante Egresos
            when('/ConsultarComprobanteEgresos', {
                templateUrl: 'Aplicativo/Tesoreria/ConsultarComprobanteEgresos.html',
                controller: 'ConsultarComprobanteEgresosCtrl'
            }).
            when('/ConsultarComprobanteEgresos/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/ConsultarComprobanteEgresos.html',
                controller: 'ConsultarComprobanteEgresosCtrl'
            }).
            when('/GestionarComprobanteEgresos', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarComprobanteEgresos.html',
                controller: 'GestionarComprobanteEgresosCtrl'
            }).
            when('/GestionarComprobanteEgresos/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarComprobanteEgresos.html',
                controller: 'GestionarComprobanteEgresosCtrl'
            }).

            when('/GenerarArchivouiaf', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarArchivoUIAF.html',
                controller: 'GestionarArchivoUIAFController'
            }).

            //Comprobante Ingresos  
            when('/ConsultarComprobanteIngresos', {
                templateUrl: 'Aplicativo/Tesoreria/ConsultarComprobanteIngresos.html',
                controller: 'ConsultarComprobanteIngresosCtrl'
            }).
            when('/ConsultarComprobanteIngresos/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/ConsultarComprobanteIngresos.html',
                controller: 'ConsultarComprobanteEgresosCtrl'
            }).
            when('/GestionarComprobanteIngresos', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarComprobanteIngresos.html',
                controller: 'GestionarComprobanteEgresosCtrl'
            }).
            when('/GestionarComprobanteIngresos/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/GestionarComprobanteIngresos.html',
                controller: 'GestionarComprobanteIngresosCtrl'
            }).
            //Generar Comprobante Egresos  
            when('/GenerarComprobanteEgresos', {
                templateUrl: 'Aplicativo/Tesoreria/GenerarComprobanteEgresos.html',
                controller: 'GenerarComprobanteEgresosCtrl'
            }).
            when('/GenerarComprobanteIngresos/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/GenerarComprobanteEgresos.html',
                controller: 'GenerarComprobanteEgresosCtrl'
            }).
            //Generar Comprobante Ingresos  
            when('/GenerarComprobanteIngresos', {
                templateUrl: 'Aplicativo/Tesoreria/GenerarComprobanteIngresos.html',
                controller: 'GenerarComprobanteIngresosCtrl'
            }).
            when('/GenerarComprobanteIngresos/:Numero', {
                templateUrl: 'Aplicativo/Tesoreria/GenerarComprobanteIngresos.html',
                controller: 'GenerarComprobanteIngresosCtrl'
            }).

            //listado Tesoreria 
            when('/ConsultarListadoTesoreria', {
                templateUrl: 'Aplicativo/Tesoreria/Listados/ConsultarListadoTesoreria.html',
                controller: 'ConsultarListadoTesoreriaCtrl'
            }).
            //listado Facturacion  
            when('/ConsultarListadoFacturacion', {
                templateUrl: 'Aplicativo/Facturacion/Listados/ConsultarListadoFacturacion.html',
                controller: 'ConsultarListadoFacturacionCtrl'
            }).
            //COMERCIAL
            //Documentos
            //TarifarioVentas
            when('/ConsultarTarifarioVentas', {
                templateUrl: 'Aplicativo/Comercial/Documentos/ConsultarTarifarioVentas.html',
                controller: 'ConsultarTarifarioVentasCtrl'
            }).
            when('/ConsultarTarifarioVentas/:Codigo', {
                templateUrl: 'Aplicativo/Comercial/Documentos/ConsultarTarifarioVentas.html',
                controller: 'ConsultarTarifarioVentasCtrl'
            }).
            when('/GestionarTarifarioVentas', {
                templateUrl: 'Aplicativo/Comercial/Documentos/GestionarTarifarioVentas.html',
                controller: 'GestionarTarifarioVentasCtrl'
            }).
            when('/GestionarTarifarioVentas/:Codigo', {
                templateUrl: 'Aplicativo/Comercial/Documentos/GestionarTarifarioVentas.html',
                controller: 'GestionarTarifarioVentasCtrl'
            }).
            //TarifarioCompras
            when('/ConsultarTarifarioCompras', {
                templateUrl: 'Aplicativo/Comercial/Documentos/ConsultarTarifarioCompras.html',
                controller: 'ConsultarTarifarioComprasCtrl'
            }).
            when('/ConsultarTarifarioCompras/:Codigo', {
                templateUrl: 'Aplicativo/Comercial/Documentos/ConsultarTarifarioCompras.html',
                controller: 'ConsultarTarifarioComprasCtrl'
            }).
            when('/GestionarTarifarioCompras', {
                templateUrl: 'Aplicativo/Comercial/Documentos/GestionarTarifarioCompras.html',
                controller: 'GestionarTarifarioComprasCtrl'
            }).
            when('/GestionarTarifarioCompras/:Codigo', {
                templateUrl: 'Aplicativo/Comercial/Documentos/GestionarTarifarioCompras.html',
                controller: 'GestionarTarifarioComprasCtrl'
            }).
            //Despachos
            //Despachos-Paqueteria
            //ConsultarGuiaPaqueterias
            when('/ConsultarGuiaPaqueterias', {
                templateUrl: 'Aplicativo/Paqueteria/ConsultarGuiaPaqueterias.html',
                controller: 'ConsultarGuiaPaqueteriasCtrl'
            }).
            when('/ConsultarGuiaPaqueterias/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/ConsultarGuiaPaqueterias.html',
                controller: 'ConsultarGuiaPaqueteriasCtrl'
            }).
            when('/GestionarGuiaPaqueterias', {
                templateUrl: 'Aplicativo/Paqueteria/GestionarGuiaPaqueterias.html',
                controller: 'GestionarGuiaPaqueteriasCtrl'
            }).
            when('/GestionarGuiaPaqueterias/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/GestionarGuiaPaqueterias.html',
                controller: 'GestionarGuiaPaqueteriasCtrl'
            }).
            when('/ConsultarListadoGuiaPaqueterias', {
                templateUrl: 'Aplicativo/Paqueteria/Listados/ConsultarListadoGuiaPaqueterias.html',
                controller: 'ConsultarListadoGuiaPaqueteriasCtrl'
            }).


            //Recepción Guías Oficina
            when('/RecepcionRemesaOficina', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/RecepcionRemesaOficina.html',
                controller: 'RecepcionRemesaOficinaCtrl'
            }).
            when('/RecepcionRemesaOficina/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/RecepcionRemesaOficina.html',
                controller: 'RecepcionRemesaOficinaCtrl'
            }).


            when('/ConsultarListadoDocumentosDespachos', {
                templateUrl: 'Aplicativo/Despachos/Listados/ConsultarListadoDespachos.html',
                controller: 'ConsultarListadoDespachosCtrl'
            }).
            //Listados Utilitarios
            when('/ConsultarListadosAuditoriaDocumentos', {
                templateUrl: 'Aplicativo/Utilitarios/ConsultarListadosAuditoriaDocumentos.html',
                controller: 'ConsultarListadosAuditoriaDocumentosCtrl'
            }).

            //Consultar Planillas guias
            when('/ConsultarPlanillaGuias', {
                templateUrl: 'Aplicativo/Paqueteria/ConsultarPlanillaGuias.html',
                controller: 'ConsultarPlanillaGuiasCtrl'
            }).
            when('/ConsultarPlanillaGuias/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/ConsultarPlanillaGuias.html',
                controller: 'ConsultarPlanillaGuiasCtrl'
            }).
            when('/GestionarPlanillaGuias', {
                templateUrl: 'Aplicativo/Paqueteria/GestionarPlanillaGuias.html',
                controller: 'GestionarPlanillaGuiasCtrl'
            }).
            when('/GestionarPlanillaGuias/:Codigo', {
                templateUrl: 'Aplicativo/Paqueteria/GestionarPlanillaGuias.html',
                controller: 'GestionarPlanillaGuiasCtrl'
            }).

            //Consultar Planillas Entregada
            when('/ConsultarPlanillaEntregas', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/ConsultarPlanillasEntregadas.html',
                controller: 'ConsultarPlanillaEntregadaCtrl'
            }).
            when('/ConsultarPlanillaEntregas/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/ConsultarPlanillasEntregadas.html',
                controller: 'ConsultarPlanillaEntregadaCtrl'
            }).
            when('/GestionarPlanillaEntregadas', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/GestionarPlanillasEntregadas.html',
                controller: 'GestionarPlanillaEntregadasCtrl'
            }).
            when('/GestionarPlanillaEntregadas/:Numero', {
                templateUrl: 'Aplicativo/Paqueteria/Documentos/GestionarPlanillasEntregadas.html',
                controller: 'GestionarPlanillaEntregadasCtrl'
            }).

            //Consultar Recogidas
            when('/ConsultarRecogidas', {
                templateUrl: 'Aplicativo/Despachos/Paqueteria/ConsultarRecogidas.html',
                controller: 'ConsultarRecogidasCtrl'
            }).
            when('/ConsultarRecogidas/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Paqueteria/ConsultarRecogidas.html',
                controller: 'ConsultarRecogidasCtrl'
            }).
            when('/GestionarRecogidas', {
                templateUrl: 'Aplicativo/Despachos/Paqueteria/GestionarRecogidas.html',
                controller: 'GestionarRecogidasCtrl'
            }).
            when('/GestionarRecogidas/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Paqueteria/GestionarRecogidas.html',
                controller: 'GestionarRecogidasCtrl'
            }).
            //Cumplido Guias
            when('/CumplidoGuiasPaqueteria', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/CumplidoGuiasPaqueteria.html',
                controller: 'CumplidoGuiasPaqueteriaCtrl'
            }).
            //Consultar Liquidación
            when('/ConsultarLiquidarPlanillas', {
                templateUrl: 'Aplicativo/Despachos/Paqueteria/ConsultarLiquidacionGuias.html',
                controller: 'ConsultarLiquidacionGuiasCtrl'
            }).
            when('/ConsultarLiquidarPlanillas/:Codigo', {
                templateUrl: 'Aplicativo/Despachos/Paqueteria/ConsultarLiquidacionGuias.html',
                controller: 'ConsultarLiquidacionGuiasCtrl'
            }).
            when('/GestionarLiquidarPlanillas', {
                templateUrl: 'Aplicativo/Despachos/Paqueteria/GestionarLiquidacionGuias.html',
                controller: 'GestionarLiquidacionGuiasCtrl'
            }).
            when('/GestionarLiquidarPlanillas/:Codigo', {
                templateUrl: 'Aplicativo/Despachos/Paqueteria/GestionarLiquidacionGuias.html',
                controller: 'GestionarLiquidacionGuiasCtrl'
            }).
            //Despachar Orden Servicio
            when('/ConsultarDespacharOrdenServicios', {
                templateUrl: 'Aplicativo/Despachos/Procesos/DespacharOrdenServicios/ConsultarDespacharOrdenServicios.html',
                controller: 'ConsultarDespacharOrdenServiciosCtrl'
            }).
            when('/ConsultarDespacharOrdenServicios/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Procesos/DespacharOrdenServicios/ConsultarDespacharOrdenServicios.html',
                controller: 'ConsultarDespacharOrdenServiciosCtrl'
            }).
            when('/GestionarDespacharOrdenServicios/:Numero/:Codigo', {
                templateUrl: 'Aplicativo/Despachos/Procesos/DespacharOrdenServicios/GestionarDespacharOrdenServicios.html',
                controller: 'GestionarDespacharOrdenServiciosCtrl'
            }).
            //Manifiestos
            when('/ConsultarManifiestos', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Manifiestos/ConsultarManifiestos.html',
                controller: 'ConsultarManifiestosCtrl'
            }).
            when('/ConsultarManifiestos/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Manifiestos/ConsultarManifiestos.html',
                controller: 'ConsultarManifiestosCtrl'
            }).
            when('/GestionarManifiesto', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Manifiestos/GestionarManifiesto.html',
                controller: 'GestionarManifiestoCtrl'
            }).
            when('/GestionarManifiesto/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Manifiestos/GestionarManifiesto.html',
                controller: 'GestionarManifiestoCtrl'
            }).

            //Orden Cargue
            when('/ConsultarOrdenCargue', {
                templateUrl: 'Aplicativo/Despachos/Documentos/OrdenCargues/ConsultarOrdenCargue.html',
                controller: 'ConsultarOrdenCargueCtrl'
            }).
            when('/ConsultarOrdenCargue/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/OrdenCargues/ConsultarOrdenCargue.html',
                controller: 'ConsultarOrdenCargueCtrl'
            }).
            when('/GestionarOrdenCargue', {
                templateUrl: 'Aplicativo/Despachos/Documentos/OrdenCargues/GestionarOrdenCargue.html',
                controller: 'GestionarOrdenCargueCtrl'
            }).
            when('/GestionarOrdenCargue/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/OrdenCargues/GestionarOrdenCargue.html',
                controller: 'GestionarOrdenCargueCtrl'
            }).
            //Planilla Despacho
            when('/ConsultarPlanillasDespachos', {
                templateUrl: 'Aplicativo/Despachos/Documentos/PlanillaDespachos/ConsultarPlanillaDespachos.html',
                controller: 'ConsultarPlanillaDespachoCtrl'
            }).
            when('/ConsultarPlanillasDespachos/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/PlanillaDespachos/ConsultarPlanillaDespachos.html',
                controller: 'ConsultarPlanillaDespachoCtrl'
            }).
            when('/GestionarPlanillasDespachos', {
                templateUrl: 'Aplicativo/Despachos/Documentos/PlanillaDespachos/GestionarPlanillaDespachos.html',
                controller: 'GestionarPlanillaDespachoCtrl'
            }).
            when('/GestionarPlanillasDespachos/:Codigo', {
                templateUrl: 'Aplicativo/Despachos/Documentos/PlanillaDespachos/GestionarPlanillaDespachos.html',
                controller: 'GestionarPlanillaDespachoCtrl'
            }).

            //Remesas
            when('/ConsultarRemesas', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Remesas/ConsultarRemesas.html',
                controller: 'ConsultarRemesasCtrl'
            }).
            when('/ConsultarRemesas/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Remesas/ConsultarRemesas.html',
                controller: 'ConsultarRemesasCtrl'
            }).
            when('/GestionarRemesa', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Remesas/GestionarRemesa.html',
                controller: 'GestionarRemesaCtrl'
            }).
            when('/GestionarRemesa/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Remesas/GestionarRemesa.html',
                controller: 'GestionarRemesaCtrl'
            }).
            //PROCESOS
            //Cumplir remesas
            when('/CumplidoRemesas', {
                templateUrl: 'Aplicativo/Despachos/Procesos/CumplidoRemesas.html',
                controller: 'CumplidoRemesasCtrl'
            }).
            //Aprobar Liquidación
            when('/AprobarLiquidacionesDespacho', {
                templateUrl: 'Aplicativo/Despachos/Procesos/AprobarLiquidaciones/AprobarLiquidaciones.html',
                controller: 'AprobarLiquidacionesCtrl'
            }).
            when('/AprobarLiquidacionesDespacho/:Codigo', {
                templateUrl: 'Aplicativo/Despachos/Procesos/AprobarLiquidaciones/AprobarLiquidaciones.html',
                controller: 'AprobarLiquidacionesCtrl'
            }).
            //Cumplido Planilla Despacho Masivo
            when('/ConsultarCumplidos', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Cumplidos/ConsultarCumplidos.html',
                controller: 'ConsultarCumplidosCtrl'
            }).
            when('/ConsultarCumplidos/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Cumplidos/ConsultarCumplidos.html',
                controller: 'ConsultarCumplidosCtrl'
            }).
            when('/GestionarCumplido', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Cumplidos/GestionarCumplido.html',
                controller: 'GestionarCumplidoCtrl'
            }).
            when('/GestionarCumplido/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Cumplidos/GestionarCumplido.html',
                controller: 'GestionarCumplidoCtrl'
            }).

            //Liquidación Planilla Despacho Masivo
            when('/ConsultarLiquidaciones', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Liquidaciones/ConsultarLiquidaciones.html',
                controller: 'ConsultarLiquidacionesCtrl'
            }).
            when('/ConsultarLiquidaciones/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Liquidaciones/ConsultarLiquidaciones.html',
                controller: 'ConsultarLiquidacionesCtrl'
            }).
            when('/GestionarLiquidacion', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Liquidaciones/GestionarLiquidacion.html',
                controller: 'GestionarLiquidacionCtrl'
            }).
            when('/GestionarLiquidacion/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Documentos/Liquidaciones/GestionarLiquidacion.html',
                controller: 'GestionarLiquidacionCtrl'
            }).

            //Reportar Despachos RNDC
            when('/ReportarDespachosMinisterio', {
                templateUrl: 'Aplicativo/Despachos/Procesos/ReportarDespachosMinisterio.html',
                controller: 'ReportarDespachosMinisterioCtrl'
            }).
            when('/ReportarDespachosMinisterio/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Procesos/ReportarDespachosMinisterio.html',
                controller: 'ReportarDespachosMinisterioCtrl'
            }).

            //Novedad Despachos
            when('/NovedadesDespacho', {
                templateUrl: 'Aplicativo/Despachos/Procesos/NovedadesDespacho.html',
                controller: 'NovedadesDespachoCtrl'
            }).
            when('/NovedadesDespacho/:Numero', {
                templateUrl: 'Aplicativo/Despachos/Procesos/NovedadesDespacho.html',
                controller: 'NovedadesDespachoCtrl'
            }).

            //Factura Venta
            when('/ConsultarFacturaVentas', {
                templateUrl: 'Aplicativo/Facturacion/ConsultarFacturaVentas.html',
                controller: 'ConsultarFacturaVentasCtrl'
            }).
            when('/ConsultarFacturaVentas/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/ConsultarFacturaVentas.html',
                controller: 'ConsultarFacturaVentasCtrl'
            }).
            when('/GestionarFacturaVentas', {
                templateUrl: 'Aplicativo/Facturacion/GestionarFacturaVentas.html',
                controller: 'GestionarFacturaVentasCtrl'
            }).
            when('/GestionarFacturaVentas/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/GestionarFacturaVentas.html',
                controller: 'GestionarFacturaVentasCtrl'
            }).

            //Factura Otros Conceptos
            when('/ConsultarFacturaOtrosConceptos', {
                templateUrl: 'Aplicativo/Facturacion/ConsultarFacturaOtrosConceptos.html',
                controller: 'ConsultarFacturaOtrosConceptosCtrl'
            }).
            when('/ConsultarFacturaOtrosConceptos/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/ConsultarFacturaOtrosConceptos.html',
                controller: 'ConsultarFacturaOtrosConceptosCtrl'
            }).
            when('/GestionarFacturaOtrosConceptos', {
                templateUrl: 'Aplicativo/Facturacion/GestionarFacturaOtrosConceptos.html',
                controller: 'GestionarFacturaOtrosConceptosCtrl'
            }).
            when('/GestionarFacturaOtrosConceptos/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/GestionarFacturaOtrosConceptos.html',
                controller: 'GestionarFacturaOtrosConceptosCtrl'
            }).

            //Notas Credito Factura
            when('/ConsultarNotaCredito', {
                templateUrl: 'Aplicativo/Facturacion/ConsultarNotaCredito.html',
                controller: 'ConsultarNotaCreditoCtrl'
            }).
            when('/ConsultarNotaCredito/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/ConsultarNotaCredito.html',
                controller: 'ConsultarNotaCreditoCtrl'
            }).
            when('/GestionarNotaCredito', {
                templateUrl: 'Aplicativo/Facturacion/GestionarNotaCredito.html',
                controller: 'GestionarNotaCreditoCtrl'
            }).
            when('/GestionarNotaCredito/:Numero', {
                templateUrl: 'Aplicativo/Facturacion/GestionarNotaCredito.html',
                controller: 'GestionarNotaCreditoCtrl'
            }).

            // Seguridad Usuarios Usuarios 
            when('/ConsultarUsuarios', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarUsuarios.html',
                controller: 'ConsultarUsuariosCtrl'
            }).
            when('/ConsultarUsuarios/:Codigo', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarUsuarios.html',
                controller: 'ConsultarUsuariosCtrl'
            }).
            when('/GestionarUsuarios', {
                templateUrl: 'Aplicativo/Seguridad/GestionarUsuarios.html',
                controller: 'GestionarUsuariosCtrl'
            }).
            when('/GestionarUsuarios/:Codigo', {
                templateUrl: 'Aplicativo/Seguridad/GestionarUsuarios.html',
                controller: 'GestionarUsuariosCtrl'
            }).

            //Seguridad Usuarios Perfil/Grupos
            when('/ConsultarGrupoPerfiles', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarGrupos.html',
                controller: 'ConsultarGrupoPerfilesCtrl'
            }).
            when('/ConsultarGrupoPerfiles/:Codigo', {
                templateUrl: 'Aplicativo/Seguridad/ConsultarGrupos.html',
                controller: 'ConsultarGrupoPerfilesCtrl'
            }).
            when('/GestionarGrupoPerfiles', {
                templateUrl: 'Aplicativo/Seguridad/GestionarGrupos.html',
                controller: 'GestionarGrupoPerfilesCtrl'
            }).
            when('/GestionarGrupoPerfiles/:Codigo', {
                templateUrl: 'Aplicativo/Seguridad/GestionarGrupos.html',
                controller: 'GestionarGrupoPerfilesCtrl'
            }).


            //Utilitarios Bandeja Salida Correos
            when('/ConsultarBandejaSalidaCorreos', {
                templateUrl: 'Aplicativo/Utilitarios/ConsultarBandejaSalidaCorreo.html',
                controller: 'ConsultarBandejaSalidaCorreosCtrl'
            }).
            when('/ConsultarBandejaSalidaCorreos/:Codigo', {
                templateUrl: 'Aplicativo/Utilitarios/ConsultarBandejaSalidaCorreo.html',
                controller: 'ConsultarBandejaSalidaCorreosCtrl'
            }).
            //Utilitarios Configuracion servidor Correos
            when('/ConsultarConfiguracionServidorCorreos', {
                templateUrl: 'Aplicativo/Utilitarios/GestionarConfiguracionServidorCorreos.html',
                controller: 'GestionarConfiguracionServidorCorreosCtrl'
            }).
            when('/ConsultarConfiguracionServidorCorreos/:Codigo', {
                templateUrl: 'Aplicativo/Utilitarios/GestionarConfiguracionServidorCorreos.html',
                controller: 'GestionarConfiguracionServidorCorreosCtrl'
            }).
            //Utilitarios Lista Dristibución Correos
            when('/ConsultarListaDistribucionCorreos', {
                templateUrl: 'Aplicativo/Utilitarios/ConsultarListaDistribucionCorreos.html',
                controller: 'ConsultarListaDistribucionCorreosCtrl'
            }).
            when('/ConsultarListaDistribucionCorreos/:Codigo', {
                templateUrl: 'Aplicativo/Utilitarios/ConsultarListaDistribucionCorreos.html',
                controller: 'ConsultarListaDistribucionCorreosCtrl'
            }).
            when('/GestionarListaDistribucionCorreos', {
                templateUrl: 'Aplicativo/Utilitarios/GestionarListaDistribucionCorreos.html',
                controller: 'GestionarListaDistribucionCorreosCtrl'
            }).
            when('/GestionarListaDistribucionCorreos/:Codigo', {
                templateUrl: 'Aplicativo/Utilitarios/GestionarListaDistribucionCorreos.html',
                controller: 'GestionarListaDistribucionCorreosCtrl'
            }).

            //Orden Servicio
            when('/ConsultarOrdenservicio', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarOrdenServicio.html',
                controller: 'ConsultarOrdenServicioCtrl'
            }).
            when('/ConsultarOrdenservicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarOrdenServicio.html',
                controller: 'ConsultarOrdenServicioCtrl'
            }).
            when('/GestionarOrdenservicio', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarOrdenServicio.html',
                controller: 'GestionarOrdenServicioCtrl'
            }).
            when('/GestionarOrdenservicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarOrdenServicio.html',
                controller: 'GestionarOrdenServicioCtrl'
            }).

            //Solicitud Orden Servicio
            when('/ConsultarSolicitudOrdenServicio', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarSolicitudOrdenServicio.html',
                controller: 'ConsultarSolicitudOrdenServicioCtrl'
            }).
            when('/ConsultarSolicitudOrdenServicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarSolicitudOrdenServicio.html',
                controller: 'ConsultarSolicitudOrdenServicioCtrl'
            }).
            when('/ConsultarSolicitudOrdenServicio/:Numero/:Tipo', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarSolicitudOrdenServicio.html',
                controller: 'ConsultarSolicitudOrdenServicioCtrl'
            }).
            when('/GestionarSolicitudOrdenservicio', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarSolicitudOrdenServicio.html',
                controller: 'GestionarSolicitudOrdenServicioCtrl'
            }).
            when('/GestionarSolicitudOrdenservicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarSolicitudOrdenServicio.html',
                controller: 'GestionarSolicitudOrdenServicioCtrl'
            }).
            when('/GestionarSolicitudOrdenservicio/:Numero/:Tipo', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarSolicitudOrdenServicio.html',
                controller: 'GestionarSolicitudOrdenServicioCtrl'
            }).

            //Generar Orden Servicio
            when('/ConsultarGenerarOrdenServicio', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarGenerarOrdenServicio.html',
                controller: 'ConsultarGenerarOrdenServicioCtrl'
            }).
            when('/ConsultarGenerarOrdenServicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/ConsultarGenerarOrdenServicio.html',
                controller: 'ConsultarGenerarOrdenServicioCtrl'
            }).
            when('/GestionarGenerarOrdenServicio/:Numero', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarGenerarOrdenServicio.html',
                controller: 'GestionarGenerarOrdenServicioCtrl'
            }).
            when('/GestionarGenerarOrdenServicio/:Numero/:Generar', {
                templateUrl: 'Aplicativo/ServicioCliente/GestionarGenerarOrdenServicio.html',
                controller: 'GestionarGenerarOrdenServicioCtrl'
            }).

            //Cargar Remesas
            when('/ConsultarCargueArchivoGuias', {
                templateUrl: 'Aplicativo/Paqueteria/Procesos/CargarRemesasPaqueteria.html',
                controller: 'CargarRemesasPaqueteriaCtrl'
            }).
            //Cargar Tarifas
            when('/CargarTarifario', {
                templateUrl: 'Aplicativo/Comercial/Procesos/CargarTarifario.html',
                controller: 'CargarTarifarioCtrl'
            }).
            //Conceptos Gastos
            when('/ConsultarConceptosGastos', {
                templateUrl: 'Aplicativo/Basico/FlotaPropia/ConsultarConceptosGastos.html',
                controller: 'ConsultarConceptosGastosCrtl'
            }).
            when('/ConsultarConceptosGastos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/FlotaPropia/ConsultarConceptosGastos.html',
                controller: 'ConsultarConceptosGastosCrtl'
            }).
            when('/GestionarConceptosGastos', {
                templateUrl: 'Aplicativo/Basico/FlotaPropia/GestionarConceptosGastos.html',
                controller: 'GestionarConceptosGastosCtrl'
            }).
            when('/GestionarConceptosGastos/:Codigo', {
                templateUrl: 'Aplicativo/Basico/FlotaPropia/GestionarConceptosGastos.html',
                controller: 'GestionarConceptosGastosCtrl'
            }).
            //Legalización Gastos
            when('/ConsultarLegalizacionGastos', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/ConsultarLegalizacionGastos.html',
                controller: 'ConsultarLegalizacionGastosCtrl'
            }).
            when('/ConsultarLegalizacionGastos/:Codigo', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/ConsultarLegalizacionGastos.html',
                controller: 'ConsultarLegalizacionGastosCtrl'
            }).
            when('/GestionarLegalizacionGastos', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/GestionarLegalizacionGastos.html',
                controller: 'GestionarLegalizacionGastosCtrl'
            }).
            when('/GestionarLegalizacionGastos/:Codigo', {
                templateUrl: 'Aplicativo/FlotaPropia/Documentos/GestionarLegalizacionGastos.html',
                controller: 'GestionarLegalizacionGastosCtrl'
            }).

            //Listados Contabilidad
            when('/ConsultarListadoContabilidad', {
                templateUrl: 'Aplicativo/Contabilidad/Listados/ConsultarListadoContabilidad.html',
                controller: 'ConsultarListadoContabilidadCtrl'
            }).
            //Listados Servicio al Cliente
            when('/ConsultarListadoServicioCliente', {
                templateUrl: 'Aplicativo/ServicioCliente/Listados/ConsultarListadoServicioCliente.html',
                controller: 'ConsultarListadoServicioClienteCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });

     
    }]);

//Constantes
//Estandar
// 1) TODO EN MAYUSCULA SIN PRONOMBRES NI CONECTORES.
// Ejemplo
// DEFINIR_CONSTANTES

var CERO = 0;
var CODIGO_UNO = 1;
var CODIGO_TRES = 3;
var CODIGO_DOS = 2;
var CODIGO_INTERNO_DATOS_SISTEMA = 0; 

//------------- Formatos de Fechas ----------------------------//

// Ejemplo fecha ingreso:  Domingo 05 de Abril de 2009

var FORMATO_FECHA_yyyy = 'yyyy'; // Ejemplo: 2009
var FORMATO_FECHA_yy = 'yy'; // Ejemplo: 9
var FORMATO_FECHA_MMMM = 'MMMM'; // Ejemplo: April
var FORMATO_FECHA_MMM = 'MMM'; // Ejemplo: Apr
var FORMATO_FECHA_MM = 'MM'; // Ejemplo: 4
var FORMATO_FECHA_M = 'M'; // Ejemplo: 4
var FORMATO_FECHA_dddd = 'dddd'; // Ejemplo: Sunday
var FORMATO_FECHA_ddd = 'ddd'; // Ejemplo: Sun
var FORMATO_FECHA_dd = 'dd'; // Ejemplo: 5
var FORMATO_FECHA_D = 'D'; // Ejemplo: Sunday, April 05, 2009
var FORMATO_FECHA_d = 'd'; // Ejemplo: 4/5/2009
var FORMATO_FECHA_HH = 'HH'; // Ejemplo: 08
var FORMATO_FECHA_hh = 'hh'; // Ejemplo: 08
var FORMATO_FECHA_H = 'H'; // Ejemplo: 8
var FORMATO_FECHA_h = 'h'; // Ejemplo: 8
var FORMATO_FECHA_mmmm = 'mmmm'; // Ejemplo: 0909
var FORMATO_FECHA_mmm = 'mmm'; // Ejemplo: 099
var FORMATO_FECHA_mm = 'mm'; // Ejemplo: 09
var FORMATO_FECHA_m = 'm'; // Ejemplo: April 05
var FORMATO_FECHA_SSS = 'SSS'; // Ejemplo: ththth
var FORMATO_FECHA_sss = 'sss'; // Ejemplo: 099
var FORMATO_FECHA_SS = 'SS'; // Ejemplo: thth
var FORMATO_FECHA_ss = 'ss'; // Ejemplo: 09
var FORMATO_FECHA_S = 'S'; // Ejemplo: th
var FORMATO_FECHA_s = 's'; // Ejemplo: 2009-04-05T08:09:09
var FORMATO_FECHA_tt = 'tt'; // Ejemplo AM/PM
var FORMATO_FECHA_MINIMA = '01-01-1900'

var CANTIDAD_HORAS_EN_MILISEGUNDOS = 1000 * 60 * 60
//----------------------------------OPCIONES DE MENÚ----------------------------------//

//-----------------------BÁSICO--------------------//
var OPCION_MENU_EMPRESAS = 100111;
var OPCION_MENU_BANCOS = 100302;
var OPCION_MENU_CATALOGOS = 100101;
var OPCION_MENU_CIUDADES = 100102;
var OPCION_MENU_OFICINAS = 100103;
var OPCION_MENU_CIERRE_CONTABLE = 1000202
var OPCION_MENU_PUERTOS_PAISES = 100113;
var OPCION_MENU_TERCEROS = 100104;
var OPCION_MENU_PAISES = 100106;
var OPCION_MENU_TIPO_DOCUMENTOS = 100112;
var OPCION_MENU_FORMATOS_INSPECCIONES = 100114;
var OPCION_MENU_PRODUCTO_TRANSPORTADOS = 100107;
var OPCION_MENU_CONCEPTOS_LIQUIDACION = 100209;
var OPCION_MENU_CONCEPTOS_FACTURA = 100501;
var OPCION_MENU_CONCEPTOS_GASTOS = 100601;

var OPCION_MENU_RECOLECCIONES = 400103;
var OPCION_MENU_PLANILLA_RECOLECCIONES = 400106;
var OPCION_MENU_DEPARTAMENTOS = 100108;
var OPCION_MENU_COLOR_VEHICULOS = 100201;
var OPCION_MENU_MARCA_VEHICULOS = 100202;
var OPCION_MENU_LINEA_VEHICULOS = 100203;
var OPCION_MENU_RUTAS = 100204;
var OPCION_MENU_VEHICULOS = 100205;
var OPCION_MENU_PLAN_UNICO_CUENTAS = 100301;
var OPCION_MENU_CUENTAS_BANCARIAS = 100303;
var OPCION_MENU_CHEQUERAS = 100304;
var OPCION_MENU_CAJAS = 100305;
var OPCION_MENU_CONCEPTOS_CONTABLES = 100306;
var OPCION_MENU_MOVIMIENTO_CONTABLE = 100307;
var OPCION_MENU_MARCA_SEMIRREMOLQUES = 100206;
var OPCION_MENU_SEMIRREMOLQUES = 100207;
var OPCION_MENU_PARAMETRIZACION_CONTABLE = 100307;
var OPCION_MENU_PUESTO_CONTROLES = 100208;
var OPCION_MENU_TARIFARIO_VENTAS = 200101;
var OPCION_MENU_TARIFARIO_COMPRAS = 200102;
var OPCION_MENU_COMPROBANTE_EGRESOS = 800101;
var OPCION_MENU_COMPROBANTE_INGRESOS = 800102;
var OPCION_MENU_GENERAR_COMPROBANTE_EGRESOS = 800201;
var OPCION_MENU_GENERAR_COMPROBANTE_INGRESOS = 800202;
var OPCION_MENU_GUIAS_PAQUETERIA = 400101;
var OPCION_MENU_RECEPCION_GUIAS_OFICINA = 400203;
var OPCION_MENU_PLANILLAS_ENTREGADA = 400107;

var OPCION_MENU_NOVEDADES_DESPACHOS = 100210;
var OPCION_MENU_SITIOS_CARGUE_DESCARGUE = 100211;
var OPCION_MENU_PLANILLA_DESPACHOS_PAQUETERIA = 400102;

var OPCION_MENU_FACTURA_VENTAS = 900101;
var OPCION_MENU_FACTURA_OTROS_CONCEPTOS = 900102;
var OPCION_MENU_NOTA_CREDITO_FACTURAS = 900103;
var OPCION_MENU_ALMACENES = 100401;
var OPCION_MENU_GRUPO_REFERENCIAS = 100402;
var OPCION_MENU_UNIDAD_EMPAQUE = 100403;
var OPCION_MENU_UNIDAD_MEDIDA = 100404;
var OPCION_MENU_REFERENCIAS = 100405;
var OPCION_MENU_REFERENCIA_ALMACENES = 100406;
var OPCION_MENU_REFERENCIA_PROVEEDORES = 100407;
var OPCION_MENU_UBICACION_ALMACENES = 100408;

var OCION_MENU_MONEDA = 100105;
var OCION_MENU_TRM = 100109;
var OCION_MENU_IMPUESTOS = 100110;
var OCION_MENU_LISTADOS_GUIAS = 4503
var OCION_MENU_LISTADOS_DESPACHOS = 4002;
var OCION_MENU_LISTADOS_BASICO = 1007;
var OCION_MENU_LISTADOS_SERVICIO_CLIETE = 2503;
var OCION_MENU_LISTADOS_CONTABILIDAD = 10003;
var OCION_MENU_LISTADOS_FLOTA_PROPIA = 6002;

var OCION_MENU_LISTADOS_FACTURACION = 9002;
var OCION_MENU_LISTADOS_TESORERIA = 8003;

var OPCION_MENU_ORDEN_SERVICIO = 250101;
var OPCION_MENU_SOLICITUD_ORDEN_SERVICIO = 2400101;
var OPCION_MENU_LISTA_DISTRIBUCION_CORREOS = 1400102
var OPCION_MENU_CONSULTAR_AUDITORIA_DOCUMENTOS = 1400201
var OPCION_MENU_LEGALIZACION_GASTOS = 600101
//-----------------------SEGURIDAD--------------------//
var OPCION_MENU_ESTUDIO_SEGURIDAD = 300101; 
var OPCION_MENU_APROBAR_ESTUDIO_SEGURIDAD = 300301;
var OPCION_MENU_GRUPOS_PERFILES = 13001;
var OPCION_MENU_USUARIOS = 13002;
//-----------------------CONTABILIDAD--------------------//
var OPCION_MENU_COMPROBANTE_CONTABLE = 1000101;
//-----------------------CONTROL VIAJES--------------------//
var OPCION_MENU_SEGUIMIENTO_VEHICULAR = 500101;
var OPCION_MENU_AUTORIZACIONES = 500102;
var OPCION_MENU_CONSULTAS_SEGUIMIENTO_VEHICULAR = 500201;
var OPCION_MENU_CONSULTAS_SEGUIMIENTO_VEHICULAR_PORTAL = 2500101;
var OPCION_MENU_INSPECCIONES_PREOPERACIONALES = 500301
//-----------------------GEPHONE--------------------//
var OPCION_MENU_CONTROL_ENTREGAS = 17001;
var OPCION_MENU_NOVEDAD_TRANSITO = 18001;
var OPCION_MENU_CHECK_VEHICULO_CONDUCTORES = 19001;
var OPCION_MENU_CHECK_CARGUE_DESCARGUES = 19002;
var OPCION_MENU_ENTURNAMIENTO = 20001;

//----------------------------------PERMISOS----------------------------------------//
var PERMISO_ACTIVO = 1;
var PERMISO_INACTIVO = 0;

//---------------------------------ESTADOS GENERALES ----------------------------------------//
var ESTADO_ACTIVO = 1;
var ESTADO_INACTIVO = 0;
var ESTADO_DISTRIBUCION_REGISTRADA = 11401;
var CODIGO_CATALOGO_ESTADO_REPARTO_OFICINA_DESTINO = 6025
var CODIGO_ESTADO_REPORTE_INICIAL = 8200
var OCULTAR_CODIGO_NO_APLICA = 202;
var CODIGO_INICIAL_NO_APLICA = 0;

//---------------------------------ESTADO DOCUMENTO-------------------------------------------
var ESTADO_BORRADOR = 0;
var ESTADO_DEFINITIVO = 1;
var ESTADO_NO_ANULADO = 0;
var ESTADO_ANULADO = 1;

//----------------------------------DESPACHOS----------------------------------------//
var OPCION_MENU_MANIFIESTO_CARGA = 400301;
var OPCION_MENU_ORDEN_CARGUE = 400303;
var OPCION_MENU_PLANILLA_DESPACHOS = 400304;
var OPCION_MENU_CUMPLIDOS = 400305;
var OPCION_MENU_LIQUIDACIONES = 400306;
var OPCION_MENU_APROBAR_LIQUIDACIONES = 400130;
var OPCION_MENU_DESPCHAR_ORDEN_SERVICIOS = 400110;
var OPCION_MENU_REMESAS = 400302;
var OPCION_MENU_CUMPLIR_GUIAS = 400202;
var OPCION_MENU_CUMPLIR_REMESAS = 400120;

//----------------------------------CATALOGOS----------------------------------------//
var CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO = 1
var CODIGO_CATALOGO_APLICACIONES_DE_GESTRANS = 2
var CODIGO_CATALOGO_CLASE_CUENTA_PLAN_UNICO_CUENTAS = 3
var CODIGO_CATALOGO_TIPO_CUENTA_BANCARIA = 4
var CODIGO_CATALOGO_TIPO_NATURALEZA_TERCERO = 5
var CODIGO_CATALOGO_SEXO_TERCERO_PERSONA_NATURAL = 6
var CODIGO_CATALOGO_ESTADO_TERCERO = 7
var CODIGO_CATALOGO_TIPO_IMPUESTO_LUGAR = 8
var CODIGO_CATALOGO_TIPO_IMPUESTO = 9
var CODIGO_CATALOGO_TIPO_NUMERACION = 11

var CODIGO_CATALOGO_LINEA_PRODUCTOS = 93
var CODIGO_CATALOGO_NATURALEZA_PRODUCTOS = 94
var CODIGO_CATALOGO_TIPO_SITIO = 10
var CODIGO_CATALOGO_MES = 170
var CODIGO_CATALOGO_TIPO_GENERACION_NUMERACION = 11
var CODIGO_CATALOGO_TIPO_CONTRATO_EMPLEADO = 12
var CODIGO_CATALOGO_PERIODO_LIQUIDACION = 13
var CODIGO_CATALOGO_PERFIL_TERCEROS = 14
var CODIGO_CATALOGO_ENTIDAD_CONSULTADA = 96
var CODIGO_CATALOGO_ESTADO_SOLICITUD = 97
var CODIGO_CATALOGO_TIPO_REFERENCIA = 110
var CODIGO_CATALOGO_CLASE_RIESGO = 98
var CODIGO_CATALOGO_FORMA_PAGO_CLIENTES = 15
var CODIGO_CATALOGO_REGIMEN_TRIBUTARIO = 16
var CODIGO_CATALOGO_TIPO_CONTRATO_CONDUCTOR = 17
var CODIGO_CATALOGO_CATEGORIAS_LICENCIAS_DE_CONDUCCION = 18
var CODIGO_CATALOGO_TIPO_DE_SANGRE = 19
var CODIGO_CATALOGO_CARGOS = 20
var CODIGO_CATALOGO_TIPO_DUEÑO_VEHICULO = 21
var CODIGO_CATALOGO_TIPO_VEHICULO = 22
var CODIGO_CATALOGO_TIPO_CARROCERIA = 23
var CODIGO_CATALOGO_CAUSA_INACTIVIDAD_VEHICULOS = 24
var CODIGO_CATALOGO_ESTADO_VEHICULOS = 25
var CODIGO_CATALOGO_DOCUMENTO_ORIGEN = 26
var CODIGO_CATALOGO_TIPO_CONCEPTO_CONTABLE = 27
var CODIGO_CATALOGO_DOCUMENTO_CRUCE = 28
var CODIGO_CATALOGO_NATURALEZA_CONCEPTO_CONTABLE = 29
var CODIGO_CATALOGO_TIPO_CUENTA_CONCEPTO = 30
var CODIGO_CATALOGO_TERCERO_PARAMETRIZACION_CONTABLE = 31
var CODIGO_CATALOGO_CENTRO_COSTO_PARAMETRIZACION_CONTABLE = 32
var CODIGO_CATALOGO_ESTADO_SEMIRREMOLQUES = 33
var CODIGO_CATALOGO_TIPO_SEMIRREMOLQUES = 34
var CODIGO_CATALOGO_TIPO_EQUIPO = 35
var CODIGO_CATALOGO_TIPO_DOCUMENTO_GENERA = 36
var CODIGO_CATALOGO_TIPO_GENERACION_DETALLE_CONTABLE = 37
var CODIGO_CATALOGO_TIPO_PARAMETRIZACION_CONTABLE = 38
var CODIGO_CATALOGO_TIPO_CONDUCTOR = 39
var CODIGO_CATALOGO_TIPO_SUFIJO_ICA_PARAMETRIZACION = 40
var CODIGO_CATALOGO_DOCUMENTO_CRUCE_PARAMETRIZACION = 41
var CODIGO_CATALOGO_VALOR_PARAMETRIZACION_CONTABLE = 42
var CODIGO_CATALOGO_TIPO_ARCHIVO_DOCUMENTO = 43
var CODIGO_CATALOGO_CAPACIDAD_VEHICULO = 46
var CODIGO_CATALOGO_FORMA_PAGO_DOCUMENTO_COMPROBANTES = 47
var CODIGO_CATALOGO_MEDIO_PAGO = 51
var CODIGO_CATALOGO_TIPO_RUTA = 44
var CODIGO_CATALOGO_DESTINO_INGRESO = 48
var CODIGO_CATALOGO_FORMA_PAGO_VENTAS = 49
var CODIGO_CATALOGO_ESTADO_TRAMITE_SOLICITUD = 92

var CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA = 4900
var CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO = 4901



var CODIGO_CATALOGO_TIPO_REMESAS = 50
var CODIGO_CATALOGO_ESTADO_REMESA_PAQUETERIA = 60
var CODIGO_CATALOGO_FORMA_PAGO_REMESA_PAQUETERIA = 61
var CODIGO_CATALOGO_TIPO_TRANSPORTE_REMESA_PAQUETERIA = 62
var CODIGO_CATALOGO_TIPO_DESPACHO_REMESA_PAQUETERIA = 63
var CODIGO_CATALOGO_TEMPERATURA_PRODUCTO_REMESA_PAQUETERIA = 64
var CODIGO_CATALOGO_TIPO_SERVICIO_REMESA_PAQUETERIA = 65
var CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA = 66
var CODIGO_CATALOGO_TIPO_ASIGNACION_PRECINTO= 68
var CODIGO_CATALOGO_TIPO_PRECINTO = 69

var CODIGO_CATALOGO_TIPO_CONTENEDORES = 70
var CODIGO_CATALOGO_TIPO_DEVOLUCIÓN_CONTENEDORES = 71
var CODIGO_CATALOGO_CAPACIDAD_VEHICULOS = 72
var CODIGO_CATALOGO_RANGO_PESOS_VALOR_KILO = 73
var CODIGO_CATALOGO_RANGO_PESOS_VALOR_FIJO = 74
var CODIGO_CATALOGO_SITIO_REPORTE_SEGUIMIENDO = 82
var CODIGO_CATALOGO_TIPO_REPORTE_SEGUIMIENTO = 83
var CODIGO_CATALOGO_NOVEDAD_SEGUIMIENTO_CARGA = 80
var CODIGO_CATALOGO_NOVEDAD_SEGUIMIENTO_VEHICULOS = 81
var CODIGO_CATALOGO_DEPARTAMENTO_EMPLEADOS = 87
var CODIGO_CATALOGO_FORMA_COBRO = 88
var CODIGO_CATALOGO_ESTADO_DESPACHO = 92
var CODIGO_CATALOGO_TIPO_OFICINA = 99

var CODIGO_CATALOGO_MODO_TRANSPORTE = 100
var CODIGO_CATALOGO_TIPO_POLIZA_ORDEN_SERVICIOS = 101
var CODIGO_CATALOGO_HORARIO_AUTORIZADO_POLIZA = 102
var CODIGO_CATALOGO_NAVIERAS = 103
var CODIGO_CATALOGO_TIPO_REEXPEDICION_REMESA_PAQUETERIA = 111
var CODIGO_CATALOGO_TIPO_UBICACION_ALMACEN = 160
var CODIGO_TIPO_UBICACION_NO_APLICA = 16000

var CODIGO_CATALOGO_RECIBIDA_OFICINA_DESTINO = 6020
var CODIGO_CATALOGO_TIPO_MANIFIESTO_MASIVO = 8804
var CODIGO_CATALOGO_TIPO_MANIFIESTO_OTRA_EMPRESA = 8805
var CODIGO_CATALOGO_TIPO_MANIFIESTO_PLANILLA_PAQUETERIA = 8806
var CODIGO_CATALOGO_TIPO_MANIFIESTO_VACIO = 8807

var CODIGO_CATALOGO_TIPO_CONTROL = 125
var CODIGO_CATALOGO_TIPO_ORIGEN_SEGUIMIENTO = 80
var CODIGO_CATALOGO_NOVEDAD_SEGUIMIENTO = 81
var CODIGO_CATALOGO_SITIO_REPORTE_SEGUIMIENTO = 82
var CODIGO_TIPO_ORIGEN_SEGUIMIENTO_MANUAL = 8001
var CODIGO_CATALOGO_CANTIDAD_LISTA_SEGUIMIENTOS = 113

var CODIGO_CATALOGO_TIPO_VALOR_FACTURA_IMPUESTOS_REMESAS = 11501
var CODIGO_CATALOGO_TIPO_VALOR_FACTURA_IMPUESTOS_OTROS_CONCEPTOS = 11502
var CODIGO_CATALOGO_TIPO_FACTURA = 5201
var CODIGO_CATALOGO_TIPO_FACTURA_OTROS_CONCEPTOS = 5202
var CODIGO_CATALOGO_SISTEMA_CONTABLE= 140
var CODIGO_CATALOGO_TIPO_ARCHIVO_CONTABLE = 145
var CODIGO_TIPO_RECAUDO_TODAS = 10624
var CODIGO_TIPO_RECAUDO_PAIS = 801
var CODIGO_TIPO_RECAUDO_DEPARTAMENTO = 10626
var CODIGO_TIPO_RECAUDO_CIUDAD = 10627

//----------------------------------IMPUESTOS Y RETENCIONES----------------------------------------//
var CODGIO_CATALOGO_IMPUESTO_IVA = 31;
var NOMBRE_IMPUESTO_IVA = "IVA";
var CODIGO_CATALOGO_RETENCION_FUENTE = 30;
var NOMBRE_RETENCION_FUENTE = "RETEFUENTE";
var CODIGO_CATALOGO_RETENCION_ICA = 32;
var NOMBRE_RETENCION_ICA = "RETEICA";

//----------------------------------FORMA_PAGOS----------------------------------------//
var CODIGO_FORMA_PAGO_TRANSFERENCIA = 4701
var CODIGO_FORMA_PAGO_NO_APLICA = 4700
var CODIGO_FORMA_PAGO_CHEQUE = 4702
var CODIGO_FORMA_PAGO_EFECTIVO = 4703
var CODIGO_FORMA_PAGO_CONTADO_CLIENTE = 1502
var CODIGO_FORMA_PAGO_CREDITO_CLIENTE = 1503
var CODIGO_FORMA_PAGO_CONTADO_REMESA = 6102
var CODIGO_FORMA_PAGO_CREDITO_REMESA = 6101

var CODIGO_MEDIO_PAGO_TRANSFERENCIA = 5102
var CODIGO_MEDIO_PAGO_NO_APLICA = 5100
var CODIGO_MEDIO_PAGO_CHEQUE = 5103
var CODIGO_MEDIO_PAGO_EFECTIVO = 5101

//--------------------------------DOCUMENTOS ORIGEN----------------------------------------//
var CODIGO_DOCUMENTO_ORIGEN_FACTURA = 2601
var CODIGO_DOCUMENTO_ORIGEN_LIQUIDACION = 2602
var CODIGO_DOCUMENTO_ORIGEN_ANTICIPO = 2604

//----------------------------------PERFILES  TERCEROS----------------------------------------//

var PERFIL_CLIENTE = 1401
var PERFIL_ASEGURADORA = 1402
var PERFIL_CONDUCTOR = 1403
var PERFIL_DESTINATARIO = 1404
var PERFIL_EMPLEADO = 1405
var PERFIL_EMPRESA_AFILIADORA = 1406
var PERFIL_EMPRESA_TRANSPORTADORA = 1407
var PERFIL_PROPIETARIO = 1408
var PERFIL_PROVEEDOR = 1409
var PERFIL_REMITENTE = 1410
var PERFIL_SEGURIDAD_SOCIAL = 1411
var PERFIL_TENEDOR = 1412
var PERFIL_COMERCIAL = 1413



var CODIGO_TIPO_LINEA_NEGOCIO_IMPORTACION = 103
var CODIGO_TIPO_LINEA_NEGOCIO_EXTEMPORANEO = 104
var CODIGO_TIPO_LINEA_NEGOCIO_DTA = 105
var CODIGO_TIPO_LINEA_NEGOCIO_OTM = 106


//----------------------------------TIPO DOCUMENTOS ----------------------------------------//
var CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO = 30
var CODIGO_TIPO_DOCUMENTO_COMPROBANTE_INGRESO = 40
var CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR = 80
var CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR = 85 
var CODIGO_TIPO_DOCUMENTO_INSPECCIONES = 240 
var CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO = 90
var CODIGO_TIPO_DOCUMENTO_SOLICITUD_ORDEN_SERVICIO = 91
var CODIGO_TIPO_DOCUMENTO_ORDEN_CARGUE = 95
var CODIGO_TIPO_DOCUMENTO_REMESAS = 100
var CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA = 110
var CODIGO_TIPO_DOCUMENTO_SEGUIMIENTO_VEHICULAR = 120
var CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO = 130
var CODIGO_TIPO_DOCUMENTO_MANIFIESTO_CARGA = 140
var CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO = 150
var CODIGO_TIPO_DOCUMENTO_FACTURAS = 170
var CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO = 190
var CODIGO_TIPO_DOCUMENTO_RECOLECCION = 200
var CODIGO_TIPO_DOCUMENTO_ESTUDIO_SEGURIDAD = 220
var CODIGO_TIPO_DOCUMENTO_PLANILLA_RECOLECCION = 205
var CODIGO_TIPO_DOCUMENTO_PLANILLA_ENTREGA = 210
var CODIGO_TIPO_DOCUMENTO_CUMPLIDO_PLANILLA_DESPACHO = 155
var CODIGO_TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHO = 160
var CODIGO_TIPO_DOCUMENTO_DESPACHO_INSPECCIONES = 130


//----------------------------------Tipo Aplicativo ----------------------------------------//
var CODIGO_APLICACION_TODAS = 204
var CODIGO_APLICACION_APLICATIVO = 201
var CODIGO_APLICACION_GESPHONE = 203
var CODIGO_APLICACION_PORTAL = 202
//----------------------------------Forma Pago Ventas ----------------------------------------//
var CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NO_APLICA = 4900
var CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CONTADO = 4902

//---------------------------------Tipo Tarifas paqueteria ------------------------------------//
var CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS = [28, 33]
var CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS = [29, 34]
var CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS = [30, 35]
var CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS = [31, 36]



//----------------------------------Rango  Documentos ----------------------------------------//
var RANGO_MAXIMO_CANTIDAD_DOCUMENTOS = 2000;
var RANGO_MAXIMO_CANTIDAD_DOCUMENTOS_LISTADOS = 2000;

//----------------------------------Tipo Control ----------------------------------------//
var CODIGO_TIPO_CONTROL_NO_APLICA = 12500;
var CODIGO_TIPO_CONTROL_CHECK = 12507
var CODIGO_TIPO_CONTROL_NUMERO = 12502
var CODIGO_TIPO_CONTROL_TEXTO = 12501
var CODIGO_TIPO_CONTROL_DATE = 12503

var CODIGO_CATALOGO_TIPO_ORDEN_SERVICIO_PENDIENTE = 9201

var CODIGO_CATALOGO_ASIGNACION_PRECINTO = 6801
var CODIGO_CATALOGO_TRANSLADO_PRECINTO = 6802

//----------------------------------Tipo Remesas ----------------------------------------//
var CODIGO_TIPO_REMESA_NACIONAL = 8811
var CODIGO_TIPO_REMESA_URBANA = 8812
//----------------------------------Tipo Remesas ----------------------------------------//
var CODIGO_NO_APLICA_TIPO_NATURALEZA = 500
var CODIGO_NO_APLICA_TIPO_IDENTIFICACION = 100

//----------------------------------Constantes para validaciones de documentos----------------------------------------//
var CAMPO_DOCUMENTO_NO_APLICA = 0
var CAMPO_DOCUMENTO_OPCIONAL = 1
var CAMPO_DOCUMENTO_OBLIGATORIO = 2

var MIN_DATE = new Date('1/1/1901')

//----------------------------------Constantes para validaciones de documentos----------------------------------------//
var NATURALEZA_CONTABLE_NO_APLICA = 2900
var NATURALEZA_CONTABLE_DEBITO = 2901
var NATURALEZA_CONTABLE_CREDITO = 2902

//----------------------------------Novedad Seguimiento Carga----------------------------------------//
var CODIGO_NOVEDAD_SEGUIMIENTO_NINGUNA = 8000
var CODIGO_NOVEDAD_SEGUIMIENTO_FIN_ENTREGA = 8001
var CODIGO_NOVEDAD_SEGUIMIENTO_DEVUELTA_CLIENTE = 8002
var CODIGO_NOVEDAD_SEGUIMIENTO_NO_SE_ENCONTRO_DIRECCION = 8003
var CODIGO_NOVEDAD_SEGUIMIENTO_ENTREGA_PARCIAL = 8004

//----------------------------------Tipo Reporte Seguimiento----------------------------------------//
var CODIGO_TIPO_REPORTE_SEGUIMIENTO_NO_APLICA = 8300
var CODIGO_TIPO_REPORTE_SEGUIMIENTO_MOVIL = 8304

//----------------------------------Puestos  de control----------------------------------------//
var CODIGO_PUESTO_CONTROL_NO_APLICA = 0
//----------------------------------Puestos  de control----------------------------------------//
var CODIGO_SITIO_REPORTE_NO_APLICA = 8200
//----------------------------------Puestos  de control----------------------------------------//
var CODIGO_NOVEDAD_SEGUIMIENTO_VEHICULO_NO_APLICA = 8100

//------------------------------ Tipo Entrega Remesa Paqueteria ---------------------------//

var TIPO_ENTREGA_NO_APLICA = 6600;
var TIPO_ENTREGA_NORMAL = 6601;
var TIPO_ENTREGA_URGENTE = 6602;

//------------------------------------------------------------------------------------------

var OBJETO_VALIDO = 0
var OBJETO_SIN_DATOS = 1
var OBJETO_INVALIDO = 2
var OBJETO_TAMAÑO_INVALIDO = 3
var FECHA_VALIDA = 0
var FECHA_SIN_DATOS = 1
var FECHA_ES_MAYOR = 2
var FECHA_ES_MENOR = 3
var FECHA_ES_IGUAL = 4

var CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA = 3

var Ventana = document.getElementById('window')

//Impuestos

var CODIGO_TIPO_IMPUESTO_NINGUNO = 900
var CODIGO_TIMPO_IMPUESTO_COMPRA = 901
var CODIGO_TIMPO_IMPUESTO_VENTA = 902

//----------------------------------  Modulos  ----------------------------------------//
var OPCION_MODULO_APLICATIVO = 220
var OPCION_MODULO_GESPHONE = 150
var OPCION_MODULO_PORTAL = 210
var CODIGO_LINEA_NEGOCIO_TRANSPORTE_PAQUETERIA = 3
var CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO = 1
var CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO = 2
var CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL = 301
var CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA = 302


//----------------------------------  Listados  ----------------------------------------//

var CODIGO_LITADO_TERCEROS = 100701
var CODIGO_LITADO_VEHICULO = 100702
var CODIGO_LITADO_SEMIRREMOLQUE = 100703
var CODIGO_LITADO_SITIO_CARGUE = 100704
var CODIGO_LITADO_RUTA = 100705

var CODIGO_LITADO_GUIAS_OFICINA = 400206
var CODIGO_LITADO_GUIAS_CLIENTE = 400207
var CODIGO_LITADO_ORDEN_CARGUE_OFICINA = 400208
var CODIGO_LITADO_ORDEN_CARGUE_CLIENTE = 400209
var CODIGO_LITADO_REMESA_OFICINA = 400210
var CODIGO_LITADO_REMESA_CLIENTE = 400211
var CODIGO_LISTADO_MANIFIESTO_OFICINA = 400212
var CODIGO_LISTADO_MANIFIESTO_TRANSPORTADOR = 400213
var CODIGO_LISTADO_PLANILLA_DESPACHO_OFICINA = 400214
var CODIGO_LISTADO_PLANILLA_DESPACHO_TRANSPORTADOR = 400215
var CODIGO_LISTADO_CUMPLIDOS_OFICINA = 400216
var CODIGO_LISTADO_LIQUIDACION_OFICINA = 400218
var CODIGO_LISTADO_CUMPLIDOS_TRANSPORTADOR = 400217
var CODIGO_LISTADO_LIQUIDACION_TRANSPORTADOR = 400219

var CODIGO_LISTADO_SOLICITUD_SERVICIOS_CLIENTE  = 250301
var CODIGO_LISTADO_SOLICITUD_SERVICIOS_OFICINA  = 250302
var CODIGO_LISTADO_ORDEN_SERVICIOS_CLIENTE  = 250303
var CODIGO_LISTADO_ORDEN_SERVICIOS_OFICINA = 250304

var CODIGO_LITADO_COMPROBANTE_EGRESO_TERCERO = 800301
var CODIGO_LISTADO_COMPROBANTE_EGRESO_OFICINA = 800302
var CODIGO_GENERAR_ARCHIVO_UIAF = 800203

var CODIGO_LITADO_COMPROBANTE_INGRESO_TERCERO = 800303
var CODIGO_LISTADO_COMPROBANTE_INGRESO_OFICINA = 800304

var CODIGO_LISTADO_REMESA_PENDIENTES_CUMPLIR_CLIENTE = 400220
var CODIGO_LISTADO_REMESA_PENDIENTES_CUMPLIR_OFICINA = 400221
var CODIGO_LISTADO_REMESA_PENDIENTES_FACTURAR_CLIENTE = 400222
var CODIGO_LISTADO_REMESA_PENDIENTES_FACTURAR_OFICINA = 400223

var CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_TRANSPORTADOR = 400224
var CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_OFICINA = 400225
var CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_TRANSPORTADOR = 400226
var CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_OFICINA = 400227

var CODIGO_LISTADO_FACTURAR_CLIENTE = 900201
var CODIGO_LISTADO_FACTURAR_OFICINA = 900202

var CODIGO_LITADO_COMPROBANTE_CONTABLE_DETALLADO = 1000301
var CODIGO_LITADO_COMPROBANTE_CONTABLE_RESUMIDO = 1000302
var CODIGO_LITADO_UIAF = 1000303

var CODIGO_LISTADO_DETALLE_GASTOS_CONDUCTOR = 600201
var CODIGO_LISTADO_COMPROBANTE_CONTABLE_RESUMIDO_CONDUCTOR = 600202


var NOMBRE_LITADO_TERCERO = "lstTercero"
var NOMBRE_LITADO_VEHICULO = "lstVehiculo"
var NOMBRE_LITADO_SEMIRREMOLQUES = "lstSemirremolque"
var NOMBRE_LITADO_SITIO_CARGUE_DESCARGUE = "lstSitioCargueDescargue"
var NOMBRE_LITADO_RUTAS = "lstRuta"



var NOMBRE_LITADO_GUIAS_OFICINA = "lstGuiasOficina"
var NOMBRE_LITADO_GUIAS_CLIENTE = "lstGuiasCliente"
var NOMBRE_LITADO_ORDEN_CARGUE_OFICINA = "lstOrdenCargueOficina"
var NOMBRE_LITADO_ORDEN_CARGUE_CLIENTE = "lstOrdenCargueCliente"
var NOMBRE_LITADO_REMESA_OFICINA = "lstRemesaOficina"
var NOMBRE_LITADO_REMESA_CLIENTE = "lstRemesaCliente"


var NOMBRE_LISTADO_PLANILLA_DESPACHO_TRANSPORTADOR = "lstPlanillaDespachoTransportador"
var NOMBRE_LISTADO_PLANILLA_DESPACHO_OFICINA = "lstPlanillaDespachoOficina"
var NOMBRE_LISTADO_MANIFIESTO_TRANSPORTADOR = "lstManifiestoTransportador"
var NOMBRE_LISTADO_MANIFIESTO_OFICINA = "lstManifiestoOficina"

var NOMBRE_LISTADO_CUMPLIDOS_TRANSPORTADOR = "lstCumplidosTransportador"
var NOMBRE_LISTADO_CUMPLIDOS_OFICINA = "lstCumplidosOficina"
var NOMBRE_LISTADO_LIQUIDACION_TRANSPORTADOR = "lstLiquidacionTransportador"
var NOMBRE_LISTADO_LIQUIDACION_OFICINA = "lstLiquidacionOficina"

var NOMBRE_LITADO_FACTURAS_CLIENTE = "lstFacturasCliente"
var NOMBRE_LISTADO_FACTURAS_OFICINA = "lstFacturasOficina"
var NOMBRE_LITADO_COMPROBANTE_EGRESO_TERCERO = "lstEgresoTercero"
var NOMBRE_LISTADO_COMPROBANTE_EGRESO_OFICINA = "lstEgresoOficina"
var NOMBRE_LITADO_COMPROBANTE_INGRESO_TERCERO = "lstIngresoTercero"
var NOMBRE_LISTADO_COMPROBANTE_INGRESO_OFICINA = "lstIngresoOficina"

var NOMBRE_LITADO_COMPROBANTE_CONTABLE_DETALLADO = "lstComprobanteContableDetallado"
var NOMBRE_LITADO_COMPROBANTE_CONTABLE_RESUMIDO = "lstComprobanteContableResumido"
var NOMBRE_LITADO_UIAF = "lstUIAF"

var NOMBRE_LISTADO_REMESA_PENDIENTES_CUMPLIR_CLIENTE = "lstRemesaPendienteCumplirCliente"
var NOMBRE_LISTADO_REMESA_PENDIENTES_CUMPLIR_OFICINA = "lstRemesaPendienteCumplirOficina"
var NOMBRE_LISTADO_REMESA_PENDIENTES_FACTURAR_CLIENTE = "lstRemesaPendienteFacturarCliente"
var NOMBRE_LISTADO_REMESA_PENDIENTES_FACTURAR_OFICINA = "lstRemesaPendienteFacturarOficina"

var NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_TRANSPORTADOR = "lstPlanillaDespachoPendientesCumplirTransportador"
var NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_OFICINA = "lstPlanillaDespachoPendientesCimplirOficina"
var NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_TRANSPORTADOR = "lstPlanillaDespachoPendientesLiquidarTransportador"
var NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_OFICINA = "lstPlanillaDespachoPendientesLiquidarOficina"


var NOMBRE_LISTADO_LEGALIZACION_GASTOS_RESUMIDO = "lstLegalizacionGastosResumido"
var NOMBRE_LISTADO_LEGALIZACION_GASTOS_DETALLE = "lstLegalizacionGastosDetalle"

var NOMBRE_LISTADO_SOLICITUD_SERVICIOS_CLIENTE = "lstSolicitudServicioCliente"
var NOMBRE_LISTADO_SOLICITUD_SERVICIOS_OFICINA = "lstSolicitudServicioOficina"
var NOMBRE_LISTADO_ORDEN_SERVICIOS_CLIENTE = "lstOrdenServicioCliente"
var NOMBRE_LISTADO_ORDEN_SERVICIOS_OFICINA = "lstOrdenServicioOficina"


var NOMBRE_REPORTE_GUIA = "RepGuia"
var NOMBRE_REPORTE_MANI = "RepMani"
var NOMBRE_REPORTE_REME = "RepReme"
var NOMBRE_REPORTE_ORCA = "RepOrca"
var NOMBRE_REPORTE_FACT = "RepFact"
var NOMBRE_REPORTE_NOTAFACT = "RepNotCre"
var NOMBRE_REPORTE_PLEN = "RepPlen"
var NOMBRE_REPORTE_PLDC = "RepPldc"
var NOMBRE_REPORTE_CUMPLIDO = "RepCmpl"
var NOMBRE_REPORTE_COMPROBANTE_EGRESO   = "RepCoEgr"
var NOMBRE_REPORTE_COMPROBANTE_INGRESO = "RepCoIng"
var NOMBRE_REPORTE_ORDEN_SERVICIO   = "RepOrSer"
var NOMBRE_REPORTE_SOLICITUD_ORDEN_SERVICIO   = "RepSolOrSer"
var NOMBRE_REPORTE_PLAN_RUTA = "RepPlanRuta"
var NOMBRE_REPORTE_LIQUIDACION_PLANILLA_DESPACHO = "RepLiquPlan"



var NOMBRE_REPORTE_PLANILLA_RECOLECCIONES = "RepPlanillaRecolecciones"


//----------- TARIFAS ----------------------//
var TARIFA_CONTENEDOR = 1;
var TARIFRA_DEVOLUCION_CONTENEDOR = 2;
var TARIFA_CUPO_VEHICULO = 3;
var TARIFA_PESO_FLETE = 5;
var TARIFA_MOVILIZACION = 10;
var TARIFA_DIA = 8;
var TARIFA_HORA = 9;
var TARIFA_RANGO_PESO_VALOR_KILO = 200;
var TARIFA_RANGO_PESO_VALOR_FIJO = 201;


//----------- TIPO_RUTAS ----------------------//

var TIPO_RUTA_NACIONAL = 4401
var TIPO_RUTA_URBANA = 4402

var TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA = 302
var TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL = 301
var TIPO_LINEA_NEGOCIO_CARGA_MASIVO_NACIONAL = 101
var TIPO_LINEA_NEGOCIO_CARGA_SEMIMASIVO_NACIONAL = 201

//-------------ESTADOS REMESA PAQUETERIA ---------------------//

var TIPO_OFICINA_PROPIA = 9901;
var TIPO_OFICINA_CLIENTE = 9902;
var TIPO_OFICINA_RECEPTORIA = 9903;

var ESTADO_GUIA_RECOGIDA = 6001;
var ESTADO_GUIA_OFICINA_ORIGEN = 6005;

//----------------------------------  NOTA CREDITO  ----------------------------------------//

var CONCEPTO_TIPO_NOTA_ANULACION = 1;

//----------------------------------  NOTA CREDITO  ----------------------------------------//

//----------------------------------  FACTURACION ELECTRONICA  ----------------------------------------//

var CONCEPTO_CREADO_EN_FACTURA = 0
var CONCEPTO_CREADO_EN_PLANILLA = 1

//----------------------------------  FACTURACION ELECTRONICA  ----------------------------------------//

var GEN_URL_SAPHETY = "https://api-einvoicing-co-qa.saphety.com/api/";
var API_GET_TOKEN = "auth/gettoken";
var API_GET_CUFE = "outbounddocuments/cufe";
var API_POST_FACTURA = "/outbounddocuments/salesinvoice"
var API_POST_NOTA_CREDITO = "/outbounddocuments/creditnote"
var TIPO_REPORTE_FACTURA_GENERA = "Electronic"
var API_GET_ESTADO_FACTURA = "/outbounddocuments";

//----------------------------------  FPermisos ----------------------------------------//
var PERMISO_VISUALIZAR_TARIFARIO = 40011010;

/**
 * @version: 1.0 Alpha-1
 * @author: Coolite Inc. http://www.coolite.com/
 * @date: 2008-05-13
 * @copyright: Copyright (c) 2006-2008, Coolite Inc. (http://www.coolite.com/). All rights reserved.
 * @license: Licensed under The MIT License. See license.txt and http://www.datejs.com/license/. 
 * @website: http://www.datejs.com/
 */
Date.CultureInfo = { name: "en-US", englishName: "English (United States)", nativeName: "English (United States)", dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], abbreviatedDayNames: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], shortestDayNames: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"], firstLetterDayNames: ["S", "M", "T", "W", "T", "F", "S"], monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], abbreviatedMonthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], amDesignator: "AM", pmDesignator: "PM", firstDayOfWeek: 0, twoDigitYearMax: 2029, dateElementOrder: "mdy", formatPatterns: { shortDate: "M/d/yyyy", longDate: "dddd, MMMM dd, yyyy", shortTime: "h:mm tt", longTime: "h:mm:ss tt", fullDateTime: "dddd, MMMM dd, yyyy h:mm:ss tt", sortableDateTime: "yyyy-MM-ddTHH:mm:ss", universalSortableDateTime: "yyyy-MM-dd HH:mm:ssZ", rfc1123: "ddd, dd MMM yyyy HH:mm:ss GMT", monthDay: "MMMM dd", yearMonth: "MMMM, yyyy" }, regexPatterns: { jan: /^jan(uary)?/i, feb: /^feb(ruary)?/i, mar: /^mar(ch)?/i, apr: /^apr(il)?/i, may: /^may/i, jun: /^jun(e)?/i, jul: /^jul(y)?/i, aug: /^aug(ust)?/i, sep: /^sep(t(ember)?)?/i, oct: /^oct(ober)?/i, nov: /^nov(ember)?/i, dec: /^dec(ember)?/i, sun: /^su(n(day)?)?/i, mon: /^mo(n(day)?)?/i, tue: /^tu(e(s(day)?)?)?/i, wed: /^we(d(nesday)?)?/i, thu: /^th(u(r(s(day)?)?)?)?/i, fri: /^fr(i(day)?)?/i, sat: /^sa(t(urday)?)?/i, future: /^next/i, past: /^last|past|prev(ious)?/i, add: /^(\+|aft(er)?|from|hence)/i, subtract: /^(\-|bef(ore)?|ago)/i, yesterday: /^yes(terday)?/i, today: /^t(od(ay)?)?/i, tomorrow: /^tom(orrow)?/i, now: /^n(ow)?/i, millisecond: /^ms|milli(second)?s?/i, second: /^sec(ond)?s?/i, minute: /^mn|min(ute)?s?/i, hour: /^h(our)?s?/i, week: /^w(eek)?s?/i, month: /^m(onth)?s?/i, day: /^d(ay)?s?/i, year: /^y(ear)?s?/i, shortMeridian: /^(a|p)/i, longMeridian: /^(a\.?m?\.?|p\.?m?\.?)/i, timezone: /^((e(s|d)t|c(s|d)t|m(s|d)t|p(s|d)t)|((gmt)?\s*(\+|\-)\s*\d\d\d\d?)|gmt|utc)/i, ordinalSuffix: /^\s*(st|nd|rd|th)/i, timeContext: /^\s*(\:|a(?!u|p)|p)/i }, timezones: [{ name: "UTC", offset: "-000" }, { name: "GMT", offset: "-000" }, { name: "EST", offset: "-0500" }, { name: "EDT", offset: "-0400" }, { name: "CST", offset: "-0600" }, { name: "CDT", offset: "-0500" }, { name: "MST", offset: "-0700" }, { name: "MDT", offset: "-0600" }, { name: "PST", offset: "-0800" }, { name: "PDT", offset: "-0700" }] };
(function () {
    var $D = Date, $P = $D.prototype, $C = $D.CultureInfo, p = function (s, l) {
        if (!l) { l = 2; }
        return ("000" + s).slice(l * -1);
    }; $P.clearTime = function () { this.setHours(0); this.setMinutes(0); this.setSeconds(0); this.setMilliseconds(0); return this; }; $P.setTimeToNow = function () { var n = new Date(); this.setHours(n.getHours()); this.setMinutes(n.getMinutes()); this.setSeconds(n.getSeconds()); this.setMilliseconds(n.getMilliseconds()); return this; }; $D.today = function () { return new Date().clearTime(); }; $D.compare = function (date1, date2) { if (isNaN(date1) || isNaN(date2)) { throw new Error(date1 + " - " + date2); } else if (date1 instanceof Date && date2 instanceof Date) { return (date1 < date2) ? -1 : (date1 > date2) ? 1 : 0; } else { throw new TypeError(date1 + " - " + date2); } }; $D.equals = function (date1, date2) { return (date1.compareTo(date2) === 0); }; $D.getDayNumberFromName = function (name) {
        var n = $C.dayNames, m = $C.abbreviatedDayNames, o = $C.shortestDayNames, s = name.toLowerCase(); for (var i = 0; i < n.length; i++) { if (n[i].toLowerCase() == s || m[i].toLowerCase() == s || o[i].toLowerCase() == s) { return i; } }
        return -1;
    }; $D.getMonthNumberFromName = function (name) {
        var n = $C.monthNames, m = $C.abbreviatedMonthNames, s = name.toLowerCase(); for (var i = 0; i < n.length; i++) { if (n[i].toLowerCase() == s || m[i].toLowerCase() == s) { return i; } }
        return -1;
    }; $D.isLeapYear = function (year) { return ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0); }; $D.getDaysInMonth = function (year, month) { return [31, ($D.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month]; }; $D.getTimezoneAbbreviation = function (offset) {
        var z = $C.timezones, p; for (var i = 0; i < z.length; i++) { if (z[i].offset === offset) { return z[i].name; } }
        return null;
    }; $D.getTimezoneOffset = function (name) {
        var z = $C.timezones, p; for (var i = 0; i < z.length; i++) { if (z[i].name === name.toUpperCase()) { return z[i].offset; } }
        return null;
    }; $P.clone = function () { return new Date(this.getTime()); }; $P.compareTo = function (date) { return Date.compare(this, date); }; $P.equals = function (date) { return Date.equals(this, date || new Date()); }; $P.between = function (start, end) { return this.getTime() >= start.getTime() && this.getTime() <= end.getTime(); }; $P.isAfter = function (date) { return this.compareTo(date || new Date()) === 1; }; $P.isBefore = function (date) { return (this.compareTo(date || new Date()) === -1); }; $P.isToday = function () { return this.isSameDay(new Date()); }; $P.isSameDay = function (date) { return this.clone().clearTime().equals(date.clone().clearTime()); }; $P.addMilliseconds = function (value) { this.setMilliseconds(this.getMilliseconds() + value); return this; }; $P.addSeconds = function (value) { return this.addMilliseconds(value * 1000); }; $P.addMinutes = function (value) { return this.addMilliseconds(value * 60000); }; $P.addHours = function (value) { return this.addMilliseconds(value * 3600000); }; $P.addDays = function (value) { this.setDate(this.getDate() + value); return this; }; $P.addWeeks = function (value) { return this.addDays(value * 7); }; $P.addMonths = function (value) { var n = this.getDate(); this.setDate(1); this.setMonth(this.getMonth() + value); this.setDate(Math.min(n, $D.getDaysInMonth(this.getFullYear(), this.getMonth()))); return this; }; $P.addYears = function (value) { return this.addMonths(value * 12); }; $P.add = function (config) {
        if (typeof config == "number") { this._orient = config; return this; }
        var x = config; if (x.milliseconds) { this.addMilliseconds(x.milliseconds); }
        if (x.seconds) { this.addSeconds(x.seconds); }
        if (x.minutes) { this.addMinutes(x.minutes); }
        if (x.hours) { this.addHours(x.hours); }
        if (x.weeks) { this.addWeeks(x.weeks); }
        if (x.months) { this.addMonths(x.months); }
        if (x.years) { this.addYears(x.years); }
        if (x.days) { this.addDays(x.days); }
        return this;
    }; var $y, $m, $d; $P.getWeek = function () {
        var a, b, c, d, e, f, g, n, s, w; $y = (!$y) ? this.getFullYear() : $y; $m = (!$m) ? this.getMonth() + 1 : $m; $d = (!$d) ? this.getDate() : $d; if ($m <= 2) { a = $y - 1; b = (a / 4 | 0) - (a / 100 | 0) + (a / 400 | 0); c = ((a - 1) / 4 | 0) - ((a - 1) / 100 | 0) + ((a - 1) / 400 | 0); s = b - c; e = 0; f = $d - 1 + (31 * ($m - 1)); } else { a = $y; b = (a / 4 | 0) - (a / 100 | 0) + (a / 400 | 0); c = ((a - 1) / 4 | 0) - ((a - 1) / 100 | 0) + ((a - 1) / 400 | 0); s = b - c; e = s + 1; f = $d + ((153 * ($m - 3) + 2) / 5) + 58 + s; }
        g = (a + b) % 7; d = (f + g - e) % 7; n = (f + 3 - d) | 0; if (n < 0) { w = 53 - ((g - s) / 5 | 0); } else if (n > 364 + s) { w = 1; } else { w = (n / 7 | 0) + 1; }
        $y = $m = $d = null; return w;
    }; $P.getISOWeek = function () { $y = this.getUTCFullYear(); $m = this.getUTCMonth() + 1; $d = this.getUTCDate(); return p(this.getWeek()); }; $P.setWeek = function (n) { return this.moveToDayOfWeek(1).addWeeks(n - this.getWeek()); }; $D._validate = function (n, min, max, name) {
        if (typeof n == "undefined") { return false; } else if (typeof n != "number") { throw new TypeError(n + " is not a Number."); } else if (n < min || n > max) { throw new RangeError(n + " is not a valid value for " + name + "."); }
        return true;
    }; $D.validateMillisecond = function (value) { return $D._validate(value, 0, 999, "millisecond"); }; $D.validateSecond = function (value) { return $D._validate(value, 0, 59, "second"); }; $D.validateMinute = function (value) { return $D._validate(value, 0, 59, "minute"); }; $D.validateHour = function (value) { return $D._validate(value, 0, 23, "hour"); }; $D.validateDay = function (value, year, month) { return $D._validate(value, 1, $D.getDaysInMonth(year, month), "day"); }; $D.validateMonth = function (value) { return $D._validate(value, 0, 11, "month"); }; $D.validateYear = function (value) { return $D._validate(value, 0, 9999, "year"); }; $P.set = function (config) {
        if ($D.validateMillisecond(config.millisecond)) { this.addMilliseconds(config.millisecond - this.getMilliseconds()); }
        if ($D.validateSecond(config.second)) { this.addSeconds(config.second - this.getSeconds()); }
        if ($D.validateMinute(config.minute)) { this.addMinutes(config.minute - this.getMinutes()); }
        if ($D.validateHour(config.hour)) { this.addHours(config.hour - this.getHours()); }
        if ($D.validateMonth(config.month)) { this.addMonths(config.month - this.getMonth()); }
        if ($D.validateYear(config.year)) { this.addYears(config.year - this.getFullYear()); }
        if ($D.validateDay(config.day, this.getFullYear(), this.getMonth())) { this.addDays(config.day - this.getDate()); }
        if (config.timezone) { this.setTimezone(config.timezone); }
        if (config.timezoneOffset) { this.setTimezoneOffset(config.timezoneOffset); }
        if (config.week && $D._validate(config.week, 0, 53, "week")) { this.setWeek(config.week); }
        return this;
    }; $P.moveToFirstDayOfMonth = function () { return this.set({ day: 1 }); }; $P.moveToLastDayOfMonth = function () { return this.set({ day: $D.getDaysInMonth(this.getFullYear(), this.getMonth()) }); }; $P.moveToNthOccurrence = function (dayOfWeek, occurrence) {
        var shift = 0; if (occurrence > 0) { shift = occurrence - 1; }
        else if (occurrence === -1) {
            this.moveToLastDayOfMonth(); if (this.getDay() !== dayOfWeek) { this.moveToDayOfWeek(dayOfWeek, -1); }
            return this;
        }
        return this.moveToFirstDayOfMonth().addDays(-1).moveToDayOfWeek(dayOfWeek, +1).addWeeks(shift);
    }; $P.moveToDayOfWeek = function (dayOfWeek, orient) { var diff = (dayOfWeek - this.getDay() + 7 * (orient || +1)) % 7; return this.addDays((diff === 0) ? diff += 7 * (orient || +1) : diff); }; $P.moveToMonth = function (month, orient) { var diff = (month - this.getMonth() + 12 * (orient || +1)) % 12; return this.addMonths((diff === 0) ? diff += 12 * (orient || +1) : diff); }; $P.getOrdinalNumber = function () { return Math.ceil((this.clone().clearTime() - new Date(this.getFullYear(), 0, 1)) / 86400000) + 1; }; $P.getTimezone = function () { return $D.getTimezoneAbbreviation(this.getUTCOffset()); }; $P.setTimezoneOffset = function (offset) { var here = this.getTimezoneOffset(), there = Number(offset) * -6 / 10; return this.addMinutes(there - here); }; $P.setTimezone = function (offset) { return this.setTimezoneOffset($D.getTimezoneOffset(offset)); }; $P.hasDaylightSavingTime = function () { return (Date.today().set({ month: 0, day: 1 }).getTimezoneOffset() !== Date.today().set({ month: 6, day: 1 }).getTimezoneOffset()); }; $P.isDaylightSavingTime = function () { return (this.hasDaylightSavingTime() && new Date().getTimezoneOffset() === Date.today().set({ month: 6, day: 1 }).getTimezoneOffset()); }; $P.getUTCOffset = function () { var n = this.getTimezoneOffset() * -10 / 6, r; if (n < 0) { r = (n - 10000).toString(); return r.charAt(0) + r.substr(2); } else { r = (n + 10000).toString(); return "+" + r.substr(1); } }; $P.getElapsed = function (date) { return (date || new Date()) - this; }; if (!$P.toISOString) {
        $P.toISOString = function () {
            function f(n) { return n < 10 ? '0' + n : n; }
            return '"' + this.getUTCFullYear() + '-' +
            f(this.getUTCMonth() + 1) + '-' +
            f(this.getUTCDate()) + 'T' +
            f(this.getUTCHours()) + ':' +
            f(this.getUTCMinutes()) + ':' +
            f(this.getUTCSeconds()) + 'Z"';
        };
    }
    $P._toString = $P.toString; $P.toString = function (format) {
        var x = this; if (format && format.length == 1) { var c = $C.formatPatterns; x.t = x.toString; switch (format) { case "d": return x.t(c.shortDate); case "D": return x.t(c.longDate); case "F": return x.t(c.fullDateTime); case "m": return x.t(c.monthDay); case "r": return x.t(c.rfc1123); case "s": return x.t(c.sortableDateTime); case "t": return x.t(c.shortTime); case "T": return x.t(c.longTime); case "u": return x.t(c.universalSortableDateTime); case "y": return x.t(c.yearMonth); } }
        var ord = function (n) { switch (n * 1) { case 1: case 21: case 31: return "st"; case 2: case 22: return "nd"; case 3: case 23: return "rd"; default: return "th"; } }; return format ? format.replace(/(\\)?(dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|S)/g, function (m) {
            if (m.charAt(0) === "\\") { return m.replace("\\", ""); }
            x.h = x.getHours; switch (m) { case "hh": return p(x.h() < 13 ? (x.h() === 0 ? 12 : x.h()) : (x.h() - 12)); case "h": return x.h() < 13 ? (x.h() === 0 ? 12 : x.h()) : (x.h() - 12); case "HH": return p(x.h()); case "H": return x.h(); case "mm": return p(x.getMinutes()); case "m": return x.getMinutes(); case "ss": return p(x.getSeconds()); case "s": return x.getSeconds(); case "yyyy": return p(x.getFullYear(), 4); case "yy": return p(x.getFullYear()); case "dddd": return $C.dayNames[x.getDay()]; case "ddd": return $C.abbreviatedDayNames[x.getDay()]; case "dd": return p(x.getDate()); case "d": return x.getDate(); case "MMMM": return $C.monthNames[x.getMonth()]; case "MMM": return $C.abbreviatedMonthNames[x.getMonth()]; case "MM": return p((x.getMonth() + 1)); case "M": return x.getMonth() + 1; case "t": return x.h() < 12 ? $C.amDesignator.substring(0, 1) : $C.pmDesignator.substring(0, 1); case "tt": return x.h() < 12 ? $C.amDesignator : $C.pmDesignator; case "S": return ord(x.getDate()); default: return m; }
        }) : this._toString();
    };
}());
(function () {
    var $D = Date, $P = $D.prototype, $C = $D.CultureInfo, $N = Number.prototype; $P._orient = +1; $P._nth = null; $P._is = false; $P._same = false; $P._isSecond = false; $N._dateElement = "day"; $P.next = function () { this._orient = +1; return this; }; $D.next = function () { return $D.today().next(); }; $P.last = $P.prev = $P.previous = function () { this._orient = -1; return this; }; $D.last = $D.prev = $D.previous = function () { return $D.today().last(); }; $P.is = function () { this._is = true; return this; }; $P.same = function () { this._same = true; this._isSecond = false; return this; }; $P.today = function () { return this.same().day(); }; $P.weekday = function () {
        if (this._is) { this._is = false; return (!this.is().sat() && !this.is().sun()); }
        return false;
    }; $P.at = function (time) { return (typeof time === "string") ? $D.parse(this.toString("d") + " " + time) : this.set(time); }; $N.fromNow = $N.after = function (date) { var c = {}; c[this._dateElement] = this; return ((!date) ? new Date() : date.clone()).add(c); }; $N.ago = $N.before = function (date) { var c = {}; c[this._dateElement] = this * -1; return ((!date) ? new Date() : date.clone()).add(c); }; var dx = ("sunday monday tuesday wednesday thursday friday saturday").split(/\s/), mx = ("january february march april may june july august september october november december").split(/\s/), px = ("Millisecond Second Minute Hour Day Week Month Year").split(/\s/), pxf = ("Milliseconds Seconds Minutes Hours Date Week Month FullYear").split(/\s/), nth = ("final first second third fourth fifth").split(/\s/), de; $P.toObject = function () {
        var o = {}; for (var i = 0; i < px.length; i++) { o[px[i].toLowerCase()] = this["get" + pxf[i]](); }
        return o;
    }; $D.fromObject = function (config) { config.week = null; return Date.today().set(config); }; var df = function (n) {
        return function () {
            if (this._is) { this._is = false; return this.getDay() == n; }
            if (this._nth !== null) {
                if (this._isSecond) { this.addSeconds(this._orient * -1); }
                this._isSecond = false; var ntemp = this._nth; this._nth = null; var temp = this.clone().moveToLastDayOfMonth(); this.moveToNthOccurrence(n, ntemp); if (this > temp) { throw new RangeError($D.getDayName(n) + " does not occur " + ntemp + " times in the month of " + $D.getMonthName(temp.getMonth()) + " " + temp.getFullYear() + "."); }
                return this;
            }
            return this.moveToDayOfWeek(n, this._orient);
        };
    }; var sdf = function (n) {
        return function () {
            var t = $D.today(), shift = n - t.getDay(); if (n === 0 && $C.firstDayOfWeek === 1 && t.getDay() !== 0) { shift = shift + 7; }
            return t.addDays(shift);
        };
    }; for (var i = 0; i < dx.length; i++) { $D[dx[i].toUpperCase()] = $D[dx[i].toUpperCase().substring(0, 3)] = i; $D[dx[i]] = $D[dx[i].substring(0, 3)] = sdf(i); $P[dx[i]] = $P[dx[i].substring(0, 3)] = df(i); }
    var mf = function (n) {
        return function () {
            if (this._is) { this._is = false; return this.getMonth() === n; }
            return this.moveToMonth(n, this._orient);
        };
    }; var smf = function (n) { return function () { return $D.today().set({ month: n, day: 1 }); }; }; for (var j = 0; j < mx.length; j++) { $D[mx[j].toUpperCase()] = $D[mx[j].toUpperCase().substring(0, 3)] = j; $D[mx[j]] = $D[mx[j].substring(0, 3)] = smf(j); $P[mx[j]] = $P[mx[j].substring(0, 3)] = mf(j); }
    var ef = function (j) {
        return function () {
            if (this._isSecond) { this._isSecond = false; return this; }
            if (this._same) {
                this._same = this._is = false; var o1 = this.toObject(), o2 = (arguments[0] || new Date()).toObject(), v = "", k = j.toLowerCase(); for (var m = (px.length - 1) ; m > -1; m--) {
                    v = px[m].toLowerCase(); if (o1[v] != o2[v]) { return false; }
                    if (k == v) { break; }
                }
                return true;
            }
            if (j.substring(j.length - 1) != "s") { j += "s"; }
            return this["add" + j](this._orient);
        };
    }; var nf = function (n) { return function () { this._dateElement = n; return this; }; }; for (var k = 0; k < px.length; k++) { de = px[k].toLowerCase(); $P[de] = $P[de + "s"] = ef(px[k]); $N[de] = $N[de + "s"] = nf(de); }
    $P._ss = ef("Second"); var nthfn = function (n) {
        return function (dayOfWeek) {
            if (this._same) { return this._ss(arguments[0]); }
            if (dayOfWeek || dayOfWeek === 0) { return this.moveToNthOccurrence(dayOfWeek, n); }
            this._nth = n; if (n === 2 && (dayOfWeek === undefined || dayOfWeek === null)) { this._isSecond = true; return this.addSeconds(this._orient); }
            return this;
        };
    }; for (var l = 0; l < nth.length; l++) { $P[nth[l]] = (l === 0) ? nthfn(-1) : nthfn(l); }
}());
(function () {
    Date.Parsing = { Exception: function (s) { this.message = "Parse error at '" + s.substring(0, 10) + " ...'"; } }; var $P = Date.Parsing; var _ = $P.Operators = {
        rtoken: function (r) { return function (s) { var mx = s.match(r); if (mx) { return ([mx[0], s.substring(mx[0].length)]); } else { throw new $P.Exception(s); } }; }, token: function (s) { return function (s) { return _.rtoken(new RegExp("^\s*" + s + "\s*"))(s); }; }, stoken: function (s) { return _.rtoken(new RegExp("^" + s)); }, until: function (p) {
            return function (s) {
                var qx = [], rx = null; while (s.length) {
                    try { rx = p.call(this, s); } catch (e) { qx.push(rx[0]); s = rx[1]; continue; }
                    break;
                }
                return [qx, s];
            };
        }, many: function (p) {
            return function (s) {
                var rx = [], r = null; while (s.length) {
                    try { r = p.call(this, s); } catch (e) { return [rx, s]; }
                    rx.push(r[0]); s = r[1];
                }
                return [rx, s];
            };
        }, optional: function (p) {
            return function (s) {
                var r = null; try { r = p.call(this, s); } catch (e) { return [null, s]; }
                return [r[0], r[1]];
            };
        }, not: function (p) {
            return function (s) {
                try { p.call(this, s); } catch (e) { return [null, s]; }
                throw new $P.Exception(s);
            };
        }, ignore: function (p) { return p ? function (s) { var r = null; r = p.call(this, s); return [null, r[1]]; } : null; }, product: function () {
            var px = arguments[0], qx = Array.prototype.slice.call(arguments, 1), rx = []; for (var i = 0; i < px.length; i++) { rx.push(_.each(px[i], qx)); }
            return rx;
        }, cache: function (rule) {
            var cache = {}, r = null; return function (s) {
                try { r = cache[s] = (cache[s] || rule.call(this, s)); } catch (e) { r = cache[s] = e; }
                if (r instanceof $P.Exception) { throw r; } else { return r; }
            };
        }, any: function () {
            var px = arguments; return function (s) {
                var r = null; for (var i = 0; i < px.length; i++) {
                    if (px[i] == null) { continue; }
                    try { r = (px[i].call(this, s)); } catch (e) { r = null; }
                    if (r) { return r; }
                }
                throw new $P.Exception(s);
            };
        }, each: function () {
            var px = arguments; return function (s) {
                var rx = [], r = null; for (var i = 0; i < px.length; i++) {
                    if (px[i] == null) { continue; }
                    try { r = (px[i].call(this, s)); } catch (e) { throw new $P.Exception(s); }
                    rx.push(r[0]); s = r[1];
                }
                return [rx, s];
            };
        }, all: function () { var px = arguments, _ = _; return _.each(_.optional(px)); }, sequence: function (px, d, c) {
            d = d || _.rtoken(/^\s*/); c = c || null; if (px.length == 1) { return px[0]; }
            return function (s) {
                var r = null, q = null; var rx = []; for (var i = 0; i < px.length; i++) {
                    try { r = px[i].call(this, s); } catch (e) { break; }
                    rx.push(r[0]); try { q = d.call(this, r[1]); } catch (ex) { q = null; break; }
                    s = q[1];
                }
                if (!r) { throw new $P.Exception(s); }
                if (q) { throw new $P.Exception(q[1]); }
                if (c) { try { r = c.call(this, r[1]); } catch (ey) { throw new $P.Exception(r[1]); } }
                return [rx, (r ? r[1] : s)];
            };
        }, between: function (d1, p, d2) { d2 = d2 || d1; var _fn = _.each(_.ignore(d1), p, _.ignore(d2)); return function (s) { var rx = _fn.call(this, s); return [[rx[0][0], r[0][2]], rx[1]]; }; }, list: function (p, d, c) { d = d || _.rtoken(/^\s*/); c = c || null; return (p instanceof Array ? _.each(_.product(p.slice(0, -1), _.ignore(d)), p.slice(-1), _.ignore(c)) : _.each(_.many(_.each(p, _.ignore(d))), px, _.ignore(c))); }, set: function (px, d, c) {
            d = d || _.rtoken(/^\s*/); c = c || null; return function (s) {
                var r = null, p = null, q = null, rx = null, best = [[], s], last = false; for (var i = 0; i < px.length; i++) {
                    q = null; p = null; r = null; last = (px.length == 1); try { r = px[i].call(this, s); } catch (e) { continue; }
                    rx = [[r[0]], r[1]]; if (r[1].length > 0 && !last) { try { q = d.call(this, r[1]); } catch (ex) { last = true; } } else { last = true; }
                    if (!last && q[1].length === 0) { last = true; }
                    if (!last) {
                        var qx = []; for (var j = 0; j < px.length; j++) { if (i != j) { qx.push(px[j]); } }
                        p = _.set(qx, d).call(this, q[1]); if (p[0].length > 0) { rx[0] = rx[0].concat(p[0]); rx[1] = p[1]; }
                    }
                    if (rx[1].length < best[1].length) { best = rx; }
                    if (best[1].length === 0) { break; }
                }
                if (best[0].length === 0) { return best; }
                if (c) {
                    try { q = c.call(this, best[1]); } catch (ey) { throw new $P.Exception(best[1]); }
                    best[1] = q[1];
                }
                return best;
            };
        }, forward: function (gr, fname) { return function (s) { return gr[fname].call(this, s); }; }, replace: function (rule, repl) { return function (s) { var r = rule.call(this, s); return [repl, r[1]]; }; }, process: function (rule, fn) { return function (s) { var r = rule.call(this, s); return [fn.call(this, r[0]), r[1]]; }; }, min: function (min, rule) {
            return function (s) {
                var rx = rule.call(this, s); if (rx[0].length < min) { throw new $P.Exception(s); }
                return rx;
            };
        }
    }; var _generator = function (op) {
        return function () {
            var args = null, rx = []; if (arguments.length > 1) { args = Array.prototype.slice.call(arguments); } else if (arguments[0] instanceof Array) { args = arguments[0]; }
            if (args) { for (var i = 0, px = args.shift() ; i < px.length; i++) { args.unshift(px[i]); rx.push(op.apply(null, args)); args.shift(); return rx; } } else { return op.apply(null, arguments); }
        };
    }; var gx = "optional not ignore cache".split(/\s/); for (var i = 0; i < gx.length; i++) { _[gx[i]] = _generator(_[gx[i]]); }
    var _vector = function (op) { return function () { if (arguments[0] instanceof Array) { return op.apply(null, arguments[0]); } else { return op.apply(null, arguments); } }; }; var vx = "each any all".split(/\s/); for (var j = 0; j < vx.length; j++) { _[vx[j]] = _vector(_[vx[j]]); }
}()); (function () {
    var $D = Date, $P = $D.prototype, $C = $D.CultureInfo; var flattenAndCompact = function (ax) {
        var rx = []; for (var i = 0; i < ax.length; i++) { if (ax[i] instanceof Array) { rx = rx.concat(flattenAndCompact(ax[i])); } else { if (ax[i]) { rx.push(ax[i]); } } }
        return rx;
    }; $D.Grammar = {}; $D.Translator = {
        hour: function (s) { return function () { this.hour = Number(s); }; }, minute: function (s) { return function () { this.minute = Number(s); }; }, second: function (s) { return function () { this.second = Number(s); }; }, meridian: function (s) { return function () { this.meridian = s.slice(0, 1).toLowerCase(); }; }, timezone: function (s) { return function () { var n = s.replace(/[^\d\+\-]/g, ""); if (n.length) { this.timezoneOffset = Number(n); } else { this.timezone = s.toLowerCase(); } }; }, day: function (x) { var s = x[0]; return function () { this.day = Number(s.match(/\d+/)[0]); }; }, month: function (s) { return function () { this.month = (s.length == 3) ? "jan feb mar apr may jun jul aug sep oct nov dec".indexOf(s) / 4 : Number(s) - 1; }; }, year: function (s) { return function () { var n = Number(s); this.year = ((s.length > 2) ? n : (n + (((n + 2000) < $C.twoDigitYearMax) ? 2000 : 1900))); }; }, rday: function (s) { return function () { switch (s) { case "yesterday": this.days = -1; break; case "tomorrow": this.days = 1; break; case "today": this.days = 0; break; case "now": this.days = 0; this.now = true; break; } }; }, finishExact: function (x) {
            x = (x instanceof Array) ? x : [x]; for (var i = 0; i < x.length; i++) { if (x[i]) { x[i].call(this); } }
            var now = new Date(); if ((this.hour || this.minute) && (!this.month && !this.year && !this.day)) { this.day = now.getDate(); }
            if (!this.year) { this.year = now.getFullYear(); }
            if (!this.month && this.month !== 0) { this.month = now.getMonth(); }
            if (!this.day) { this.day = 1; }
            if (!this.hour) { this.hour = 0; }
            if (!this.minute) { this.minute = 0; }
            if (!this.second) { this.second = 0; }
            if (this.meridian && this.hour) { if (this.meridian == "p" && this.hour < 12) { this.hour = this.hour + 12; } else if (this.meridian == "a" && this.hour == 12) { this.hour = 0; } }
            if (this.day > $D.getDaysInMonth(this.year, this.month)) { throw new RangeError(this.day + " is not a valid value for days."); }
            var r = new Date(this.year, this.month, this.day, this.hour, this.minute, this.second); if (this.timezone) { r.set({ timezone: this.timezone }); } else if (this.timezoneOffset) { r.set({ timezoneOffset: this.timezoneOffset }); }
            return r;
        }, finish: function (x) {
            x = (x instanceof Array) ? flattenAndCompact(x) : [x]; if (x.length === 0) { return null; }
            for (var i = 0; i < x.length; i++) { if (typeof x[i] == "function") { x[i].call(this); } }
            var today = $D.today(); if (this.now && !this.unit && !this.operator) { return new Date(); } else if (this.now) { today = new Date(); }
            var expression = !!(this.days && this.days !== null || this.orient || this.operator); var gap, mod, orient; orient = ((this.orient == "past" || this.operator == "subtract") ? -1 : 1); if (!this.now && "hour minute second".indexOf(this.unit) != -1) { today.setTimeToNow(); }
            if (this.month || this.month === 0) { if ("year day hour minute second".indexOf(this.unit) != -1) { this.value = this.month + 1; this.month = null; expression = true; } }
            if (!expression && this.weekday && !this.day && !this.days) {
                var temp = Date[this.weekday](); this.day = temp.getDate(); if (!this.month) { this.month = temp.getMonth(); }
                this.year = temp.getFullYear();
            }
            if (expression && this.weekday && this.unit != "month") { this.unit = "day"; gap = ($D.getDayNumberFromName(this.weekday) - today.getDay()); mod = 7; this.days = gap ? ((gap + (orient * mod)) % mod) : (orient * mod); }
            if (this.month && this.unit == "day" && this.operator) { this.value = (this.month + 1); this.month = null; }
            if (this.value != null && this.month != null && this.year != null) { this.day = this.value * 1; }
            if (this.month && !this.day && this.value) { today.set({ day: this.value * 1 }); if (!expression) { this.day = this.value * 1; } }
            if (!this.month && this.value && this.unit == "month" && !this.now) { this.month = this.value; expression = true; }
            if (expression && (this.month || this.month === 0) && this.unit != "year") { this.unit = "month"; gap = (this.month - today.getMonth()); mod = 12; this.months = gap ? ((gap + (orient * mod)) % mod) : (orient * mod); this.month = null; }
            if (!this.unit) { this.unit = "day"; }
            if (!this.value && this.operator && this.operator !== null && this[this.unit + "s"] && this[this.unit + "s"] !== null) { this[this.unit + "s"] = this[this.unit + "s"] + ((this.operator == "add") ? 1 : -1) + (this.value || 0) * orient; } else if (this[this.unit + "s"] == null || this.operator != null) {
                if (!this.value) { this.value = 1; }
                this[this.unit + "s"] = this.value * orient;
            }
            if (this.meridian && this.hour) { if (this.meridian == "p" && this.hour < 12) { this.hour = this.hour + 12; } else if (this.meridian == "a" && this.hour == 12) { this.hour = 0; } }
            if (this.weekday && !this.day && !this.days) { var temp = Date[this.weekday](); this.day = temp.getDate(); if (temp.getMonth() !== today.getMonth()) { this.month = temp.getMonth(); } }
            if ((this.month || this.month === 0) && !this.day) { this.day = 1; }
            if (!this.orient && !this.operator && this.unit == "week" && this.value && !this.day && !this.month) { return Date.today().setWeek(this.value); }
            if (expression && this.timezone && this.day && this.days) { this.day = this.days; }
            return (expression) ? today.add(this) : today.set(this);
        }
    }; var _ = $D.Parsing.Operators, g = $D.Grammar, t = $D.Translator, _fn; g.datePartDelimiter = _.rtoken(/^([\s\-\.\,\/\x27]+)/); g.timePartDelimiter = _.stoken(":"); g.whiteSpace = _.rtoken(/^\s*/); g.generalDelimiter = _.rtoken(/^(([\s\,]|at|@|on)+)/); var _C = {}; g.ctoken = function (keys) {
        var fn = _C[keys]; if (!fn) {
            var c = $C.regexPatterns; var kx = keys.split(/\s+/), px = []; for (var i = 0; i < kx.length; i++) { px.push(_.replace(_.rtoken(c[kx[i]]), kx[i])); }
            fn = _C[keys] = _.any.apply(null, px);
        }
        return fn;
    }; g.ctoken2 = function (key) { return _.rtoken($C.regexPatterns[key]); }; g.h = _.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2]|[1-9])/), t.hour)); g.hh = _.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2])/), t.hour)); g.H = _.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3]|[0-9])/), t.hour)); g.HH = _.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3])/), t.hour)); g.m = _.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/), t.minute)); g.mm = _.cache(_.process(_.rtoken(/^[0-5][0-9]/), t.minute)); g.s = _.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/), t.second)); g.ss = _.cache(_.process(_.rtoken(/^[0-5][0-9]/), t.second)); g.hms = _.cache(_.sequence([g.H, g.m, g.s], g.timePartDelimiter)); g.t = _.cache(_.process(g.ctoken2("shortMeridian"), t.meridian)); g.tt = _.cache(_.process(g.ctoken2("longMeridian"), t.meridian)); g.z = _.cache(_.process(_.rtoken(/^((\+|\-)\s*\d\d\d\d)|((\+|\-)\d\d\:?\d\d)/), t.timezone)); g.zz = _.cache(_.process(_.rtoken(/^((\+|\-)\s*\d\d\d\d)|((\+|\-)\d\d\:?\d\d)/), t.timezone)); g.zzz = _.cache(_.process(g.ctoken2("timezone"), t.timezone)); g.timeSuffix = _.each(_.ignore(g.whiteSpace), _.set([g.tt, g.zzz])); g.time = _.each(_.optional(_.ignore(_.stoken("T"))), g.hms, g.timeSuffix); g.d = _.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1]|\d)/), _.optional(g.ctoken2("ordinalSuffix"))), t.day)); g.dd = _.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1])/), _.optional(g.ctoken2("ordinalSuffix"))), t.day)); g.ddd = g.dddd = _.cache(_.process(g.ctoken("sun mon tue wed thu fri sat"), function (s) { return function () { this.weekday = s; }; })); g.M = _.cache(_.process(_.rtoken(/^(1[0-2]|0\d|\d)/), t.month)); g.MM = _.cache(_.process(_.rtoken(/^(1[0-2]|0\d)/), t.month)); g.MMM = g.MMMM = _.cache(_.process(g.ctoken("jan feb mar apr may jun jul aug sep oct nov dec"), t.month)); g.y = _.cache(_.process(_.rtoken(/^(\d\d?)/), t.year)); g.yy = _.cache(_.process(_.rtoken(/^(\d\d)/), t.year)); g.yyy = _.cache(_.process(_.rtoken(/^(\d\d?\d?\d?)/), t.year)); g.yyyy = _.cache(_.process(_.rtoken(/^(\d\d\d\d)/), t.year)); _fn = function () { return _.each(_.any.apply(null, arguments), _.not(g.ctoken2("timeContext"))); }; g.day = _fn(g.d, g.dd); g.month = _fn(g.M, g.MMM); g.year = _fn(g.yyyy, g.yy); g.orientation = _.process(g.ctoken("past future"), function (s) { return function () { this.orient = s; }; }); g.operator = _.process(g.ctoken("add subtract"), function (s) { return function () { this.operator = s; }; }); g.rday = _.process(g.ctoken("yesterday tomorrow today now"), t.rday); g.unit = _.process(g.ctoken("second minute hour day week month year"), function (s) { return function () { this.unit = s; }; }); g.value = _.process(_.rtoken(/^\d\d?(st|nd|rd|th)?/), function (s) { return function () { this.value = s.replace(/\D/g, ""); }; }); g.expression = _.set([g.rday, g.operator, g.value, g.unit, g.orientation, g.ddd, g.MMM]); _fn = function () { return _.set(arguments, g.datePartDelimiter); }; g.mdy = _fn(g.ddd, g.month, g.day, g.year); g.ymd = _fn(g.ddd, g.year, g.month, g.day); g.dmy = _fn(g.ddd, g.day, g.month, g.year); g.date = function (s) { return ((g[$C.dateElementOrder] || g.mdy).call(this, s)); }; g.format = _.process(_.many(_.any(_.process(_.rtoken(/^(dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?)/), function (fmt) { if (g[fmt]) { return g[fmt]; } else { throw $D.Parsing.Exception(fmt); } }), _.process(_.rtoken(/^[^dMyhHmstz]+/), function (s) { return _.ignore(_.stoken(s)); }))), function (rules) { return _.process(_.each.apply(null, rules), t.finishExact); }); var _F = {}; var _get = function (f) { return _F[f] = (_F[f] || g.format(f)[0]); }; g.formats = function (fx) {
        if (fx instanceof Array) {
            var rx = []; for (var i = 0; i < fx.length; i++) { rx.push(_get(fx[i])); }
            return _.any.apply(null, rx);
        } else { return _get(fx); }
    }; g._formats = g.formats(["\"yyyy-MM-ddTHH:mm:ssZ\"", "yyyy-MM-ddTHH:mm:ssZ", "yyyy-MM-ddTHH:mm:ssz", "yyyy-MM-ddTHH:mm:ss", "yyyy-MM-ddTHH:mmZ", "yyyy-MM-ddTHH:mmz", "yyyy-MM-ddTHH:mm", "ddd, MMM dd, yyyy H:mm:ss tt", "ddd MMM d yyyy HH:mm:ss zzz", "MMddyyyy", "ddMMyyyy", "Mddyyyy", "ddMyyyy", "Mdyyyy", "dMyyyy", "yyyy", "Mdyy", "dMyy", "d"]); g._start = _.process(_.set([g.date, g.time, g.expression], g.generalDelimiter, g.whiteSpace), t.finish); g.start = function (s) {
        try { var r = g._formats.call({}, s); if (r[1].length === 0) { return r; } } catch (e) { }
        return g._start.call({}, s);
    }; $D._parse = $D.parse; $D.parse = function (s) {
        var r = null; if (!s) { return null; }
        if (s instanceof Date) { return s; }
        try { r = $D.Grammar.start.call({}, s.replace(/^\s*(\S*(\s+\S+)*)\s*$/, "$1")); } catch (e) { return null; }
        return ((r[1].length === 0) ? r[0] : null);
    }; $D.getParseFunction = function (fx) {
        var fn = $D.Grammar.formats(fx); return function (s) {
            var r = null; try { r = fn.call({}, s); } catch (e) { return null; }
            return ((r[1].length === 0) ? r[0] : null);
        };
    }; $D.parseExact = function (s, fx) { return $D.getParseFunction(fx)(s); };
}());
function ShowWarning(titulo, mensaje) {
    swal({
        title: titulo,
        text: mensaje,
        type: 'warning',
        confirmButtonText: 'Aceptar'
    });
}

function ShowError(mensaje) {    
    if (mensaje == 'Unauthorized') {
        mensaje = "Su sesión a caducado por favor inicie sesión nuevamente.";
        //location.href = '#';
    }
    
    swal({
        title: 'GESTRANS.NET CARGA',
        text: mensaje,
        type: 'error',
        confirmButtonText: 'Aceptar'
    });
}

function ShowInfo(mensaje) {
    swal({
        title: 'GESTRANS.NET CARGA',
        text: mensaje,
        type: 'info',
        confirmButtonText: 'Aceptar'
    });
}

function ShowSuccess(mensaje) {
    swal({
        title: 'GESTRANS.NET CARGA',
        text: mensaje,
        type: 'success',
        confirmButtonText: 'Aceptar'
    });
}
//! moment.js
//! version : 2.14.1
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com

; (function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    global.moment = factory()
}(this, function () {
    'use strict';

    var hookCallback;

    function utils_hooks__hooks() {
        return hookCallback.apply(null, arguments);
    }

    // This is done to register the method called with moment()
    // without creating circular dependencies.
    function setHookCallback(callback) {
        hookCallback = callback;
    }

    function isArray(input) {
        return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
    }

    function isObject(input) {
        return Object.prototype.toString.call(input) === '[object Object]';
    }

    function isObjectEmpty(obj) {
        var k;
        for (k in obj) {
            // even if its not own property I'd still call it non-empty
            return false;
        }
        return true;
    }

    function isDate(input) {
        return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
    }

    function map(arr, fn) {
        var res = [], i;
        for (i = 0; i < arr.length; ++i) {
            res.push(fn(arr[i], i));
        }
        return res;
    }

    function hasOwnProp(a, b) {
        return Object.prototype.hasOwnProperty.call(a, b);
    }

    function extend(a, b) {
        for (var i in b) {
            if (hasOwnProp(b, i)) {
                a[i] = b[i];
            }
        }

        if (hasOwnProp(b, 'toString')) {
            a.toString = b.toString;
        }

        if (hasOwnProp(b, 'valueOf')) {
            a.valueOf = b.valueOf;
        }

        return a;
    }

    function create_utc__createUTC(input, format, locale, strict) {
        return createLocalOrUTC(input, format, locale, strict, true).utc();
    }

    function defaultParsingFlags() {
        // We need to deep clone this object.
        return {
            empty: false,
            unusedTokens: [],
            unusedInput: [],
            overflow: -2,
            charsLeftOver: 0,
            nullInput: false,
            invalidMonth: null,
            invalidFormat: false,
            userInvalidated: false,
            iso: false,
            parsedDateParts: [],
            meridiem: null
        };
    }

    function getParsingFlags(m) {
        if (m._pf == null) {
            m._pf = defaultParsingFlags();
        }
        return m._pf;
    }

    var some;
    if (Array.prototype.some) {
        some = Array.prototype.some;
    } else {
        some = function (fun) {
            var t = Object(this);
            var len = t.length >>> 0;

            for (var i = 0; i < len; i++) {
                if (i in t && fun.call(this, t[i], i, t)) {
                    return true;
                }
            }

            return false;
        };
    }

    function valid__isValid(m) {
        if (m._isValid == null) {
            var flags = getParsingFlags(m);
            var parsedParts = some.call(flags.parsedDateParts, function (i) {
                return i != null;
            });
            m._isValid = !isNaN(m._d.getTime()) &&
                flags.overflow < 0 &&
                !flags.empty &&
                !flags.invalidMonth &&
                !flags.invalidWeekday &&
                !flags.nullInput &&
                !flags.invalidFormat &&
                !flags.userInvalidated &&
                (!flags.meridiem || (flags.meridiem && parsedParts));

            if (m._strict) {
                m._isValid = m._isValid &&
                    flags.charsLeftOver === 0 &&
                    flags.unusedTokens.length === 0 &&
                    flags.bigHour === undefined;
            }
        }
        return m._isValid;
    }

    function valid__createInvalid(flags) {
        var m = create_utc__createUTC(NaN);
        if (flags != null) {
            extend(getParsingFlags(m), flags);
        }
        else {
            getParsingFlags(m).userInvalidated = true;
        }

        return m;
    }

    function isUndefined(input) {
        return input === void 0;
    }

    // Plugins that add properties should also add the key here (null value),
    // so we can properly clone ourselves.
    var momentProperties = utils_hooks__hooks.momentProperties = [];

    function copyConfig(to, from) {
        var i, prop, val;

        if (!isUndefined(from._isAMomentObject)) {
            to._isAMomentObject = from._isAMomentObject;
        }
        if (!isUndefined(from._i)) {
            to._i = from._i;
        }
        if (!isUndefined(from._f)) {
            to._f = from._f;
        }
        if (!isUndefined(from._l)) {
            to._l = from._l;
        }
        if (!isUndefined(from._strict)) {
            to._strict = from._strict;
        }
        if (!isUndefined(from._tzm)) {
            to._tzm = from._tzm;
        }
        if (!isUndefined(from._isUTC)) {
            to._isUTC = from._isUTC;
        }
        if (!isUndefined(from._offset)) {
            to._offset = from._offset;
        }
        if (!isUndefined(from._pf)) {
            to._pf = getParsingFlags(from);
        }
        if (!isUndefined(from._locale)) {
            to._locale = from._locale;
        }

        if (momentProperties.length > 0) {
            for (i in momentProperties) {
                prop = momentProperties[i];
                val = from[prop];
                if (!isUndefined(val)) {
                    to[prop] = val;
                }
            }
        }

        return to;
    }

    var updateInProgress = false;

    // Moment prototype object
    function Moment(config) {
        copyConfig(this, config);
        this._d = new Date(config._d != null ? config._d.getTime() : NaN);
        // Prevent infinite loop in case updateOffset creates new moment
        // objects.
        if (updateInProgress === false) {
            updateInProgress = true;
            utils_hooks__hooks.updateOffset(this);
            updateInProgress = false;
        }
    }

    function isMoment(obj) {
        return obj instanceof Moment || (obj != null && obj._isAMomentObject != null);
    }

    function absFloor(number) {
        if (number < 0) {
            // -0 -> 0
            return Math.ceil(number) || 0;
        } else {
            return Math.floor(number);
        }
    }

    function toInt(argumentForCoercion) {
        var coercedNumber = +argumentForCoercion,
            value = 0;

        if (coercedNumber !== 0 && isFinite(coercedNumber)) {
            value = absFloor(coercedNumber);
        }

        return value;
    }

    // compare two arrays, return the number of differences
    function compareArrays(array1, array2, dontConvert) {
        var len = Math.min(array1.length, array2.length),
            lengthDiff = Math.abs(array1.length - array2.length),
            diffs = 0,
            i;
        for (i = 0; i < len; i++) {
            if ((dontConvert && array1[i] !== array2[i]) ||
                (!dontConvert && toInt(array1[i]) !== toInt(array2[i]))) {
                diffs++;
            }
        }
        return diffs + lengthDiff;
    }

    function warn(msg) {
        if (utils_hooks__hooks.suppressDeprecationWarnings === false &&
                (typeof console !== 'undefined') && console.warn) {
            console.warn('Deprecation warning: ' + msg);
        }
    }

    function deprecate(msg, fn) {
        var firstTime = true;

        return extend(function () {
            if (utils_hooks__hooks.deprecationHandler != null) {
                utils_hooks__hooks.deprecationHandler(null, msg);
            }
            if (firstTime) {
                warn(msg + '\nArguments: ' + Array.prototype.slice.call(arguments).join(', ') + '\n' + (new Error()).stack);
                firstTime = false;
            }
            return fn.apply(this, arguments);
        }, fn);
    }

    var deprecations = {};

    function deprecateSimple(name, msg) {
        if (utils_hooks__hooks.deprecationHandler != null) {
            utils_hooks__hooks.deprecationHandler(name, msg);
        }
        if (!deprecations[name]) {
            warn(msg);
            deprecations[name] = true;
        }
    }

    utils_hooks__hooks.suppressDeprecationWarnings = false;
    utils_hooks__hooks.deprecationHandler = null;

    function isFunction(input) {
        return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
    }

    function locale_set__set(config) {
        var prop, i;
        for (i in config) {
            prop = config[i];
            if (isFunction(prop)) {
                this[i] = prop;
            } else {
                this['_' + i] = prop;
            }
        }
        this._config = config;
        // Lenient ordinal parsing accepts just a number in addition to
        // number + (possibly) stuff coming from _ordinalParseLenient.
        this._ordinalParseLenient = new RegExp(this._ordinalParse.source + '|' + (/\d{1,2}/).source);
    }

    function mergeConfigs(parentConfig, childConfig) {
        var res = extend({}, parentConfig), prop;
        for (prop in childConfig) {
            if (hasOwnProp(childConfig, prop)) {
                if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
                    res[prop] = {};
                    extend(res[prop], parentConfig[prop]);
                    extend(res[prop], childConfig[prop]);
                } else if (childConfig[prop] != null) {
                    res[prop] = childConfig[prop];
                } else {
                    delete res[prop];
                }
            }
        }
        for (prop in parentConfig) {
            if (hasOwnProp(parentConfig, prop) &&
                    !hasOwnProp(childConfig, prop) &&
                    isObject(parentConfig[prop])) {
                // make sure changes to properties don't modify parent config
                res[prop] = extend({}, res[prop]);
            }
        }
        return res;
    }

    function Locale(config) {
        if (config != null) {
            this.set(config);
        }
    }

    var keys;

    if (Object.keys) {
        keys = Object.keys;
    } else {
        keys = function (obj) {
            var i, res = [];
            for (i in obj) {
                if (hasOwnProp(obj, i)) {
                    res.push(i);
                }
            }
            return res;
        };
    }

    var defaultCalendar = {
        sameDay: '[Today at] LT',
        nextDay: '[Tomorrow at] LT',
        nextWeek: 'dddd [at] LT',
        lastDay: '[Yesterday at] LT',
        lastWeek: '[Last] dddd [at] LT',
        sameElse: 'L'
    };

    function locale_calendar__calendar(key, mom, now) {
        var output = this._calendar[key] || this._calendar['sameElse'];
        return isFunction(output) ? output.call(mom, now) : output;
    }

    var defaultLongDateFormat = {
        LTS: 'h:mm:ss A',
        LT: 'h:mm A',
        L: 'MM/DD/YYYY',
        LL: 'MMMM D, YYYY',
        LLL: 'MMMM D, YYYY h:mm A',
        LLLL: 'dddd, MMMM D, YYYY h:mm A'
    };

    function longDateFormat(key) {
        var format = this._longDateFormat[key],
            formatUpper = this._longDateFormat[key.toUpperCase()];

        if (format || !formatUpper) {
            return format;
        }

        this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
            return val.slice(1);
        });

        return this._longDateFormat[key];
    }

    var defaultInvalidDate = 'Invalid date';

    function invalidDate() {
        return this._invalidDate;
    }

    var defaultOrdinal = '%d';
    var defaultOrdinalParse = /\d{1,2}/;

    function ordinal(number) {
        return this._ordinal.replace('%d', number);
    }

    var defaultRelativeTime = {
        future: 'in %s',
        past: '%s ago',
        s: 'a few seconds',
        m: 'a minute',
        mm: '%d minutes',
        h: 'an hour',
        hh: '%d hours',
        d: 'a day',
        dd: '%d days',
        M: 'a month',
        MM: '%d months',
        y: 'a year',
        yy: '%d years'
    };

    function relative__relativeTime(number, withoutSuffix, string, isFuture) {
        var output = this._relativeTime[string];
        return (isFunction(output)) ?
            output(number, withoutSuffix, string, isFuture) :
            output.replace(/%d/i, number);
    }

    function pastFuture(diff, output) {
        var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
        return isFunction(format) ? format(output) : format.replace(/%s/i, output);
    }

    var aliases = {};

    function addUnitAlias(unit, shorthand) {
        var lowerCase = unit.toLowerCase();
        aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
    }

    function normalizeUnits(units) {
        return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
    }

    function normalizeObjectUnits(inputObject) {
        var normalizedInput = {},
            normalizedProp,
            prop;

        for (prop in inputObject) {
            if (hasOwnProp(inputObject, prop)) {
                normalizedProp = normalizeUnits(prop);
                if (normalizedProp) {
                    normalizedInput[normalizedProp] = inputObject[prop];
                }
            }
        }

        return normalizedInput;
    }

    var priorities = {};

    function addUnitPriority(unit, priority) {
        priorities[unit] = priority;
    }

    function getPrioritizedUnits(unitsObj) {
        var units = [];
        for (var u in unitsObj) {
            units.push({ unit: u, priority: priorities[u] });
        }
        units.sort(function (a, b) {
            return a.priority - b.priority;
        });
        return units;
    }

    function makeGetSet(unit, keepTime) {
        return function (value) {
            if (value != null) {
                get_set__set(this, unit, value);
                utils_hooks__hooks.updateOffset(this, keepTime);
                return this;
            } else {
                return get_set__get(this, unit);
            }
        };
    }

    function get_set__get(mom, unit) {
        return mom.isValid() ?
            mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]() : NaN;
    }

    function get_set__set(mom, unit, value) {
        if (mom.isValid()) {
            mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
        }
    }

    // MOMENTS

    function stringGet(units) {
        units = normalizeUnits(units);
        if (isFunction(this[units])) {
            return this[units]();
        }
        return this;
    }


    function stringSet(units, value) {
        if (typeof units === 'object') {
            units = normalizeObjectUnits(units);
            var prioritized = getPrioritizedUnits(units);
            for (var i = 0; i < prioritized.length; i++) {
                this[prioritized[i].unit](units[prioritized[i].unit]);
            }
        } else {
            units = normalizeUnits(units);
            if (isFunction(this[units])) {
                return this[units](value);
            }
        }
        return this;
    }

    function zeroFill(number, targetLength, forceSign) {
        var absNumber = '' + Math.abs(number),
            zerosToFill = targetLength - absNumber.length,
            sign = number >= 0;
        return (sign ? (forceSign ? '+' : '') : '-') +
            Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
    }

    var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;

    var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;

    var formatFunctions = {};

    var formatTokenFunctions = {};

    // token:    'M'
    // padded:   ['MM', 2]
    // ordinal:  'Mo'
    // callback: function () { this.month() + 1 }
    function addFormatToken(token, padded, ordinal, callback) {
        var func = callback;
        if (typeof callback === 'string') {
            func = function () {
                return this[callback]();
            };
        }
        if (token) {
            formatTokenFunctions[token] = func;
        }
        if (padded) {
            formatTokenFunctions[padded[0]] = function () {
                return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
            };
        }
        if (ordinal) {
            formatTokenFunctions[ordinal] = function () {
                return this.localeData().ordinal(func.apply(this, arguments), token);
            };
        }
    }

    function removeFormattingTokens(input) {
        if (input.match(/\[[\s\S]/)) {
            return input.replace(/^\[|\]$/g, '');
        }
        return input.replace(/\\/g, '');
    }

    function makeFormatFunction(format) {
        var array = format.match(formattingTokens), i, length;

        for (i = 0, length = array.length; i < length; i++) {
            if (formatTokenFunctions[array[i]]) {
                array[i] = formatTokenFunctions[array[i]];
            } else {
                array[i] = removeFormattingTokens(array[i]);
            }
        }

        return function (mom) {
            var output = '', i;
            for (i = 0; i < length; i++) {
                output += array[i] instanceof Function ? array[i].call(mom, format) : array[i];
            }
            return output;
        };
    }

    // format date using native date object
    function formatMoment(m, format) {
        if (!m.isValid()) {
            return m.localeData().invalidDate();
        }

        format = expandFormat(format, m.localeData());
        formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);

        return formatFunctions[format](m);
    }

    function expandFormat(format, locale) {
        var i = 5;

        function replaceLongDateFormatTokens(input) {
            return locale.longDateFormat(input) || input;
        }

        localFormattingTokens.lastIndex = 0;
        while (i >= 0 && localFormattingTokens.test(format)) {
            format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
            localFormattingTokens.lastIndex = 0;
            i -= 1;
        }

        return format;
    }

    var match1 = /\d/;            //       0 - 9
    var match2 = /\d\d/;          //      00 - 99
    var match3 = /\d{3}/;         //     000 - 999
    var match4 = /\d{4}/;         //    0000 - 9999
    var match6 = /[+-]?\d{6}/;    // -999999 - 999999
    var match1to2 = /\d\d?/;         //       0 - 99
    var match3to4 = /\d\d\d\d?/;     //     999 - 9999
    var match5to6 = /\d\d\d\d\d\d?/; //   99999 - 999999
    var match1to3 = /\d{1,3}/;       //       0 - 999
    var match1to4 = /\d{1,4}/;       //       0 - 9999
    var match1to6 = /[+-]?\d{1,6}/;  // -999999 - 999999

    var matchUnsigned = /\d+/;           //       0 - inf
    var matchSigned = /[+-]?\d+/;      //    -inf - inf

    var matchOffset = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z
    var matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z

    var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123

    // any word (or two) characters or numbers including two/three word month in arabic.
    // includes scottish gaelic two word and hyphenated months
    var matchWord = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i;


    var regexes = {};

    function addRegexToken(token, regex, strictRegex) {
        regexes[token] = isFunction(regex) ? regex : function (isStrict, localeData) {
            return (isStrict && strictRegex) ? strictRegex : regex;
        };
    }

    function getParseRegexForToken(token, config) {
        if (!hasOwnProp(regexes, token)) {
            return new RegExp(unescapeFormat(token));
        }

        return regexes[token](config._strict, config._locale);
    }

    // Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
    function unescapeFormat(s) {
        return regexEscape(s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
            return p1 || p2 || p3 || p4;
        }));
    }

    function regexEscape(s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    }

    var tokens = {};

    function addParseToken(token, callback) {
        var i, func = callback;
        if (typeof token === 'string') {
            token = [token];
        }
        if (typeof callback === 'number') {
            func = function (input, array) {
                array[callback] = toInt(input);
            };
        }
        for (i = 0; i < token.length; i++) {
            tokens[token[i]] = func;
        }
    }

    function addWeekParseToken(token, callback) {
        addParseToken(token, function (input, array, config, token) {
            config._w = config._w || {};
            callback(input, config._w, config, token);
        });
    }

    function addTimeToArrayFromToken(token, input, config) {
        if (input != null && hasOwnProp(tokens, token)) {
            tokens[token](input, config._a, config, token);
        }
    }

    var YEAR = 0;
    var MONTH = 1;
    var DATE = 2;
    var HOUR = 3;
    var MINUTE = 4;
    var SECOND = 5;
    var MILLISECOND = 6;
    var WEEK = 7;
    var WEEKDAY = 8;

    var indexOf;

    if (Array.prototype.indexOf) {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function (o) {
            // I know
            var i;
            for (i = 0; i < this.length; ++i) {
                if (this[i] === o) {
                    return i;
                }
            }
            return -1;
        };
    }

    function daysInMonth(year, month) {
        return new Date(Date.UTC(year, month + 1, 0)).getUTCDate();
    }

    // FORMATTING

    addFormatToken('M', ['MM', 2], 'Mo', function () {
        return this.month() + 1;
    });

    addFormatToken('MMM', 0, 0, function (format) {
        return this.localeData().monthsShort(this, format);
    });

    addFormatToken('MMMM', 0, 0, function (format) {
        return this.localeData().months(this, format);
    });

    // ALIASES

    addUnitAlias('month', 'M');

    // PRIORITY

    addUnitPriority('month', 8);

    // PARSING

    addRegexToken('M', match1to2);
    addRegexToken('MM', match1to2, match2);
    addRegexToken('MMM', function (isStrict, locale) {
        return locale.monthsShortRegex(isStrict);
    });
    addRegexToken('MMMM', function (isStrict, locale) {
        return locale.monthsRegex(isStrict);
    });

    addParseToken(['M', 'MM'], function (input, array) {
        array[MONTH] = toInt(input) - 1;
    });

    addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
        var month = config._locale.monthsParse(input, token, config._strict);
        // if we didn't find a month name, mark the date as invalid.
        if (month != null) {
            array[MONTH] = month;
        } else {
            getParsingFlags(config).invalidMonth = input;
        }
    });

    // LOCALES

    var MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/;
    var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
    function localeMonths(m, format) {
        return isArray(this._months) ? this._months[m.month()] :
            this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format) ? 'format' : 'standalone'][m.month()];
    }

    var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
    function localeMonthsShort(m, format) {
        return isArray(this._monthsShort) ? this._monthsShort[m.month()] :
            this._monthsShort[MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'][m.month()];
    }

    function units_month__handleStrictParse(monthName, format, strict) {
        var i, ii, mom, llc = monthName.toLocaleLowerCase();
        if (!this._monthsParse) {
            // this is not used
            this._monthsParse = [];
            this._longMonthsParse = [];
            this._shortMonthsParse = [];
            for (i = 0; i < 12; ++i) {
                mom = create_utc__createUTC([2000, i]);
                this._shortMonthsParse[i] = this.monthsShort(mom, '').toLocaleLowerCase();
                this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
            }
        }

        if (strict) {
            if (format === 'MMM') {
                ii = indexOf.call(this._shortMonthsParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._longMonthsParse, llc);
                return ii !== -1 ? ii : null;
            }
        } else {
            if (format === 'MMM') {
                ii = indexOf.call(this._shortMonthsParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._longMonthsParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._longMonthsParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortMonthsParse, llc);
                return ii !== -1 ? ii : null;
            }
        }
    }

    function localeMonthsParse(monthName, format, strict) {
        var i, mom, regex;

        if (this._monthsParseExact) {
            return units_month__handleStrictParse.call(this, monthName, format, strict);
        }

        if (!this._monthsParse) {
            this._monthsParse = [];
            this._longMonthsParse = [];
            this._shortMonthsParse = [];
        }

        // TODO: add sorting
        // Sorting makes sure if one month (or abbr) is a prefix of another
        // see sorting in computeMonthsParse
        for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = create_utc__createUTC([2000, i]);
            if (strict && !this._longMonthsParse[i]) {
                this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
                this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
            }
            if (!strict && !this._monthsParse[i]) {
                regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
                this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
                return i;
            } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
                return i;
            } else if (!strict && this._monthsParse[i].test(monthName)) {
                return i;
            }
        }
    }

    // MOMENTS

    function setMonth(mom, value) {
        var dayOfMonth;

        if (!mom.isValid()) {
            // No op
            return mom;
        }

        if (typeof value === 'string') {
            if (/^\d+$/.test(value)) {
                value = toInt(value);
            } else {
                value = mom.localeData().monthsParse(value);
                // TODO: Another silent failure?
                if (typeof value !== 'number') {
                    return mom;
                }
            }
        }

        dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
        return mom;
    }

    function getSetMonth(value) {
        if (value != null) {
            setMonth(this, value);
            utils_hooks__hooks.updateOffset(this, true);
            return this;
        } else {
            return get_set__get(this, 'Month');
        }
    }

    function getDaysInMonth() {
        return daysInMonth(this.year(), this.month());
    }

    var defaultMonthsShortRegex = matchWord;
    function monthsShortRegex(isStrict) {
        if (this._monthsParseExact) {
            if (!hasOwnProp(this, '_monthsRegex')) {
                computeMonthsParse.call(this);
            }
            if (isStrict) {
                return this._monthsShortStrictRegex;
            } else {
                return this._monthsShortRegex;
            }
        } else {
            if (!hasOwnProp(this, '_monthsShortRegex')) {
                this._monthsShortRegex = defaultMonthsShortRegex;
            }
            return this._monthsShortStrictRegex && isStrict ?
                this._monthsShortStrictRegex : this._monthsShortRegex;
        }
    }

    var defaultMonthsRegex = matchWord;
    function monthsRegex(isStrict) {
        if (this._monthsParseExact) {
            if (!hasOwnProp(this, '_monthsRegex')) {
                computeMonthsParse.call(this);
            }
            if (isStrict) {
                return this._monthsStrictRegex;
            } else {
                return this._monthsRegex;
            }
        } else {
            if (!hasOwnProp(this, '_monthsRegex')) {
                this._monthsRegex = defaultMonthsRegex;
            }
            return this._monthsStrictRegex && isStrict ?
                this._monthsStrictRegex : this._monthsRegex;
        }
    }

    function computeMonthsParse() {
        function cmpLenRev(a, b) {
            return b.length - a.length;
        }

        var shortPieces = [], longPieces = [], mixedPieces = [],
            i, mom;
        for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = create_utc__createUTC([2000, i]);
            shortPieces.push(this.monthsShort(mom, ''));
            longPieces.push(this.months(mom, ''));
            mixedPieces.push(this.months(mom, ''));
            mixedPieces.push(this.monthsShort(mom, ''));
        }
        // Sorting makes sure if one month (or abbr) is a prefix of another it
        // will match the longer piece.
        shortPieces.sort(cmpLenRev);
        longPieces.sort(cmpLenRev);
        mixedPieces.sort(cmpLenRev);
        for (i = 0; i < 12; i++) {
            shortPieces[i] = regexEscape(shortPieces[i]);
            longPieces[i] = regexEscape(longPieces[i]);
        }
        for (i = 0; i < 24; i++) {
            mixedPieces[i] = regexEscape(mixedPieces[i]);
        }

        this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._monthsShortRegex = this._monthsRegex;
        this._monthsStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
        this._monthsShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
    }

    // FORMATTING

    addFormatToken('Y', 0, 0, function () {
        var y = this.year();
        return y <= 9999 ? '' + y : '+' + y;
    });

    addFormatToken(0, ['YY', 2], 0, function () {
        return this.year() % 100;
    });

    addFormatToken(0, ['YYYY', 4], 0, 'year');
    addFormatToken(0, ['YYYYY', 5], 0, 'year');
    addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

    // ALIASES

    addUnitAlias('year', 'y');

    // PRIORITIES

    addUnitPriority('year', 1);

    // PARSING

    addRegexToken('Y', matchSigned);
    addRegexToken('YY', match1to2, match2);
    addRegexToken('YYYY', match1to4, match4);
    addRegexToken('YYYYY', match1to6, match6);
    addRegexToken('YYYYYY', match1to6, match6);

    addParseToken(['YYYYY', 'YYYYYY'], YEAR);
    addParseToken('YYYY', function (input, array) {
        array[YEAR] = input.length === 2 ? utils_hooks__hooks.parseTwoDigitYear(input) : toInt(input);
    });
    addParseToken('YY', function (input, array) {
        array[YEAR] = utils_hooks__hooks.parseTwoDigitYear(input);
    });
    addParseToken('Y', function (input, array) {
        array[YEAR] = parseInt(input, 10);
    });

    // HELPERS

    function daysInYear(year) {
        return isLeapYear(year) ? 366 : 365;
    }

    function isLeapYear(year) {
        return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    }

    // HOOKS

    utils_hooks__hooks.parseTwoDigitYear = function (input) {
        return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
    };

    // MOMENTS

    var getSetYear = makeGetSet('FullYear', true);

    function getIsLeapYear() {
        return isLeapYear(this.year());
    }

    function createDate(y, m, d, h, M, s, ms) {
        //can't just apply() to create a date:
        //http://stackoverflow.com/questions/181348/instantiating-a-javascript-object-by-calling-prototype-constructor-apply
        var date = new Date(y, m, d, h, M, s, ms);

        //the date constructor remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0 && isFinite(date.getFullYear())) {
            date.setFullYear(y);
        }
        return date;
    }

    function createUTCDate(y) {
        var date = new Date(Date.UTC.apply(null, arguments));

        //the Date.UTC function remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0 && isFinite(date.getUTCFullYear())) {
            date.setUTCFullYear(y);
        }
        return date;
    }

    // start-of-first-week - start-of-year
    function firstWeekOffset(year, dow, doy) {
        var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
            fwd = 7 + dow - doy,
            // first-week day local weekday -- which local weekday is fwd
            fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;

        return -fwdlw + fwd - 1;
    }

    //http://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
    function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
        var localWeekday = (7 + weekday - dow) % 7,
            weekOffset = firstWeekOffset(year, dow, doy),
            dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
            resYear, resDayOfYear;

        if (dayOfYear <= 0) {
            resYear = year - 1;
            resDayOfYear = daysInYear(resYear) + dayOfYear;
        } else if (dayOfYear > daysInYear(year)) {
            resYear = year + 1;
            resDayOfYear = dayOfYear - daysInYear(year);
        } else {
            resYear = year;
            resDayOfYear = dayOfYear;
        }

        return {
            year: resYear,
            dayOfYear: resDayOfYear
        };
    }

    function weekOfYear(mom, dow, doy) {
        var weekOffset = firstWeekOffset(mom.year(), dow, doy),
            week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
            resWeek, resYear;

        if (week < 1) {
            resYear = mom.year() - 1;
            resWeek = week + weeksInYear(resYear, dow, doy);
        } else if (week > weeksInYear(mom.year(), dow, doy)) {
            resWeek = week - weeksInYear(mom.year(), dow, doy);
            resYear = mom.year() + 1;
        } else {
            resYear = mom.year();
            resWeek = week;
        }

        return {
            week: resWeek,
            year: resYear
        };
    }

    function weeksInYear(year, dow, doy) {
        var weekOffset = firstWeekOffset(year, dow, doy),
            weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
        return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
    }

    // FORMATTING

    addFormatToken('w', ['ww', 2], 'wo', 'week');
    addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

    // ALIASES

    addUnitAlias('week', 'w');
    addUnitAlias('isoWeek', 'W');

    // PRIORITIES

    addUnitPriority('week', 5);
    addUnitPriority('isoWeek', 5);

    // PARSING

    addRegexToken('w', match1to2);
    addRegexToken('ww', match1to2, match2);
    addRegexToken('W', match1to2);
    addRegexToken('WW', match1to2, match2);

    addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
        week[token.substr(0, 1)] = toInt(input);
    });

    // HELPERS

    // LOCALES

    function localeWeek(mom) {
        return weekOfYear(mom, this._week.dow, this._week.doy).week;
    }

    var defaultLocaleWeek = {
        dow: 0, // Sunday is the first day of the week.
        doy: 6  // The week that contains Jan 1st is the first week of the year.
    };

    function localeFirstDayOfWeek() {
        return this._week.dow;
    }

    function localeFirstDayOfYear() {
        return this._week.doy;
    }

    // MOMENTS

    function getSetWeek(input) {
        var week = this.localeData().week(this);
        return input == null ? week : this.add((input - week) * 7, 'd');
    }

    function getSetISOWeek(input) {
        var week = weekOfYear(this, 1, 4).week;
        return input == null ? week : this.add((input - week) * 7, 'd');
    }

    // FORMATTING

    addFormatToken('d', 0, 'do', 'day');

    addFormatToken('dd', 0, 0, function (format) {
        return this.localeData().weekdaysMin(this, format);
    });

    addFormatToken('ddd', 0, 0, function (format) {
        return this.localeData().weekdaysShort(this, format);
    });

    addFormatToken('dddd', 0, 0, function (format) {
        return this.localeData().weekdays(this, format);
    });

    addFormatToken('e', 0, 0, 'weekday');
    addFormatToken('E', 0, 0, 'isoWeekday');

    // ALIASES

    addUnitAlias('day', 'd');
    addUnitAlias('weekday', 'e');
    addUnitAlias('isoWeekday', 'E');

    // PRIORITY
    addUnitPriority('day', 11);
    addUnitPriority('weekday', 11);
    addUnitPriority('isoWeekday', 11);

    // PARSING

    addRegexToken('d', match1to2);
    addRegexToken('e', match1to2);
    addRegexToken('E', match1to2);
    addRegexToken('dd', function (isStrict, locale) {
        return locale.weekdaysMinRegex(isStrict);
    });
    addRegexToken('ddd', function (isStrict, locale) {
        return locale.weekdaysShortRegex(isStrict);
    });
    addRegexToken('dddd', function (isStrict, locale) {
        return locale.weekdaysRegex(isStrict);
    });

    addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
        var weekday = config._locale.weekdaysParse(input, token, config._strict);
        // if we didn't get a weekday name, mark the date as invalid
        if (weekday != null) {
            week.d = weekday;
        } else {
            getParsingFlags(config).invalidWeekday = input;
        }
    });

    addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
        week[token] = toInt(input);
    });

    // HELPERS

    function parseWeekday(input, locale) {
        if (typeof input !== 'string') {
            return input;
        }

        if (!isNaN(input)) {
            return parseInt(input, 10);
        }

        input = locale.weekdaysParse(input);
        if (typeof input === 'number') {
            return input;
        }

        return null;
    }

    function parseIsoWeekday(input, locale) {
        if (typeof input === 'string') {
            return locale.weekdaysParse(input) % 7 || 7;
        }
        return isNaN(input) ? null : input;
    }

    // LOCALES

    var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');
    function localeWeekdays(m, format) {
        return isArray(this._weekdays) ? this._weekdays[m.day()] :
            this._weekdays[this._weekdays.isFormat.test(format) ? 'format' : 'standalone'][m.day()];
    }

    var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');
    function localeWeekdaysShort(m) {
        return this._weekdaysShort[m.day()];
    }

    var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');
    function localeWeekdaysMin(m) {
        return this._weekdaysMin[m.day()];
    }

    function day_of_week__handleStrictParse(weekdayName, format, strict) {
        var i, ii, mom, llc = weekdayName.toLocaleLowerCase();
        if (!this._weekdaysParse) {
            this._weekdaysParse = [];
            this._shortWeekdaysParse = [];
            this._minWeekdaysParse = [];

            for (i = 0; i < 7; ++i) {
                mom = create_utc__createUTC([2000, 1]).day(i);
                this._minWeekdaysParse[i] = this.weekdaysMin(mom, '').toLocaleLowerCase();
                this._shortWeekdaysParse[i] = this.weekdaysShort(mom, '').toLocaleLowerCase();
                this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
            }
        }

        if (strict) {
            if (format === 'dddd') {
                ii = indexOf.call(this._weekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else if (format === 'ddd') {
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            }
        } else {
            if (format === 'dddd') {
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else if (format === 'ddd') {
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._minWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            }
        }
    }

    function localeWeekdaysParse(weekdayName, format, strict) {
        var i, mom, regex;

        if (this._weekdaysParseExact) {
            return day_of_week__handleStrictParse.call(this, weekdayName, format, strict);
        }

        if (!this._weekdaysParse) {
            this._weekdaysParse = [];
            this._minWeekdaysParse = [];
            this._shortWeekdaysParse = [];
            this._fullWeekdaysParse = [];
        }

        for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already

            mom = create_utc__createUTC([2000, 1]).day(i);
            if (strict && !this._fullWeekdaysParse[i]) {
                this._fullWeekdaysParse[i] = new RegExp('^' + this.weekdays(mom, '').replace('.', '\.?') + '$', 'i');
                this._shortWeekdaysParse[i] = new RegExp('^' + this.weekdaysShort(mom, '').replace('.', '\.?') + '$', 'i');
                this._minWeekdaysParse[i] = new RegExp('^' + this.weekdaysMin(mom, '').replace('.', '\.?') + '$', 'i');
            }
            if (!this._weekdaysParse[i]) {
                regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
                this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (strict && format === 'dddd' && this._fullWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (strict && format === 'ddd' && this._shortWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (strict && format === 'dd' && this._minWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
                return i;
            }
        }
    }

    // MOMENTS

    function getSetDayOfWeek(input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
        if (input != null) {
            input = parseWeekday(input, this.localeData());
            return this.add(input - day, 'd');
        } else {
            return day;
        }
    }

    function getSetLocaleDayOfWeek(input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
        return input == null ? weekday : this.add(input - weekday, 'd');
    }

    function getSetISODayOfWeek(input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }

        // behaves the same as moment#day except
        // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
        // as a setter, sunday should belong to the previous week.

        if (input != null) {
            var weekday = parseIsoWeekday(input, this.localeData());
            return this.day(this.day() % 7 ? weekday : weekday - 7);
        } else {
            return this.day() || 7;
        }
    }

    var defaultWeekdaysRegex = matchWord;
    function weekdaysRegex(isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysStrictRegex;
            } else {
                return this._weekdaysRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                this._weekdaysRegex = defaultWeekdaysRegex;
            }
            return this._weekdaysStrictRegex && isStrict ?
                this._weekdaysStrictRegex : this._weekdaysRegex;
        }
    }

    var defaultWeekdaysShortRegex = matchWord;
    function weekdaysShortRegex(isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysShortStrictRegex;
            } else {
                return this._weekdaysShortRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysShortRegex')) {
                this._weekdaysShortRegex = defaultWeekdaysShortRegex;
            }
            return this._weekdaysShortStrictRegex && isStrict ?
                this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
        }
    }

    var defaultWeekdaysMinRegex = matchWord;
    function weekdaysMinRegex(isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysMinStrictRegex;
            } else {
                return this._weekdaysMinRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysMinRegex')) {
                this._weekdaysMinRegex = defaultWeekdaysMinRegex;
            }
            return this._weekdaysMinStrictRegex && isStrict ?
                this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
        }
    }


    function computeWeekdaysParse() {
        function cmpLenRev(a, b) {
            return b.length - a.length;
        }

        var minPieces = [], shortPieces = [], longPieces = [], mixedPieces = [],
            i, mom, minp, shortp, longp;
        for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already
            mom = create_utc__createUTC([2000, 1]).day(i);
            minp = this.weekdaysMin(mom, '');
            shortp = this.weekdaysShort(mom, '');
            longp = this.weekdays(mom, '');
            minPieces.push(minp);
            shortPieces.push(shortp);
            longPieces.push(longp);
            mixedPieces.push(minp);
            mixedPieces.push(shortp);
            mixedPieces.push(longp);
        }
        // Sorting makes sure if one weekday (or abbr) is a prefix of another it
        // will match the longer piece.
        minPieces.sort(cmpLenRev);
        shortPieces.sort(cmpLenRev);
        longPieces.sort(cmpLenRev);
        mixedPieces.sort(cmpLenRev);
        for (i = 0; i < 7; i++) {
            shortPieces[i] = regexEscape(shortPieces[i]);
            longPieces[i] = regexEscape(longPieces[i]);
            mixedPieces[i] = regexEscape(mixedPieces[i]);
        }

        this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._weekdaysShortRegex = this._weekdaysRegex;
        this._weekdaysMinRegex = this._weekdaysRegex;

        this._weekdaysStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
        this._weekdaysShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
        this._weekdaysMinStrictRegex = new RegExp('^(' + minPieces.join('|') + ')', 'i');
    }

    // FORMATTING

    function hFormat() {
        return this.hours() % 12 || 12;
    }

    function kFormat() {
        return this.hours() || 24;
    }

    addFormatToken('H', ['HH', 2], 0, 'hour');
    addFormatToken('h', ['hh', 2], 0, hFormat);
    addFormatToken('k', ['kk', 2], 0, kFormat);

    addFormatToken('hmm', 0, 0, function () {
        return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
    });

    addFormatToken('hmmss', 0, 0, function () {
        return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2) +
            zeroFill(this.seconds(), 2);
    });

    addFormatToken('Hmm', 0, 0, function () {
        return '' + this.hours() + zeroFill(this.minutes(), 2);
    });

    addFormatToken('Hmmss', 0, 0, function () {
        return '' + this.hours() + zeroFill(this.minutes(), 2) +
            zeroFill(this.seconds(), 2);
    });

    function meridiem(token, lowercase) {
        addFormatToken(token, 0, 0, function () {
            return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
        });
    }

    meridiem('a', true);
    meridiem('A', false);

    // ALIASES

    addUnitAlias('hour', 'h');

    // PRIORITY
    addUnitPriority('hour', 13);

    // PARSING

    function matchMeridiem(isStrict, locale) {
        return locale._meridiemParse;
    }

    addRegexToken('a', matchMeridiem);
    addRegexToken('A', matchMeridiem);
    addRegexToken('H', match1to2);
    addRegexToken('h', match1to2);
    addRegexToken('HH', match1to2, match2);
    addRegexToken('hh', match1to2, match2);

    addRegexToken('hmm', match3to4);
    addRegexToken('hmmss', match5to6);
    addRegexToken('Hmm', match3to4);
    addRegexToken('Hmmss', match5to6);

    addParseToken(['H', 'HH'], HOUR);
    addParseToken(['a', 'A'], function (input, array, config) {
        config._isPm = config._locale.isPM(input);
        config._meridiem = input;
    });
    addParseToken(['h', 'hh'], function (input, array, config) {
        array[HOUR] = toInt(input);
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('hmm', function (input, array, config) {
        var pos = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos));
        array[MINUTE] = toInt(input.substr(pos));
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('hmmss', function (input, array, config) {
        var pos1 = input.length - 4;
        var pos2 = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos1));
        array[MINUTE] = toInt(input.substr(pos1, 2));
        array[SECOND] = toInt(input.substr(pos2));
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('Hmm', function (input, array, config) {
        var pos = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos));
        array[MINUTE] = toInt(input.substr(pos));
    });
    addParseToken('Hmmss', function (input, array, config) {
        var pos1 = input.length - 4;
        var pos2 = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos1));
        array[MINUTE] = toInt(input.substr(pos1, 2));
        array[SECOND] = toInt(input.substr(pos2));
    });

    // LOCALES

    function localeIsPM(input) {
        // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
        // Using charAt should be more compatible.
        return ((input + '').toLowerCase().charAt(0) === 'p');
    }

    var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;
    function localeMeridiem(hours, minutes, isLower) {
        if (hours > 11) {
            return isLower ? 'pm' : 'PM';
        } else {
            return isLower ? 'am' : 'AM';
        }
    }


    // MOMENTS

    // Setting the hour should keep the time, because the user explicitly
    // specified which hour he wants. So trying to maintain the same hour (in
    // a new timezone) makes sense. Adding/subtracting hours does not follow
    // this rule.
    var getSetHour = makeGetSet('Hours', true);

    var baseConfig = {
        calendar: defaultCalendar,
        longDateFormat: defaultLongDateFormat,
        invalidDate: defaultInvalidDate,
        ordinal: defaultOrdinal,
        ordinalParse: defaultOrdinalParse,
        relativeTime: defaultRelativeTime,

        months: defaultLocaleMonths,
        monthsShort: defaultLocaleMonthsShort,

        week: defaultLocaleWeek,

        weekdays: defaultLocaleWeekdays,
        weekdaysMin: defaultLocaleWeekdaysMin,
        weekdaysShort: defaultLocaleWeekdaysShort,

        meridiemParse: defaultLocaleMeridiemParse
    };

    // internal storage for locale config files
    var locales = {};
    var globalLocale;

    function normalizeLocale(key) {
        return key ? key.toLowerCase().replace('_', '-') : key;
    }

    // pick the locale from the array
    // try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
    // substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
    function chooseLocale(names) {
        var i = 0, j, next, locale, split;

        while (i < names.length) {
            split = normalizeLocale(names[i]).split('-');
            j = split.length;
            next = normalizeLocale(names[i + 1]);
            next = next ? next.split('-') : null;
            while (j > 0) {
                locale = loadLocale(split.slice(0, j).join('-'));
                if (locale) {
                    return locale;
                }
                if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                    //the next array item is better than a shallower substring of this one
                    break;
                }
                j--;
            }
            i++;
        }
        return null;
    }

    function loadLocale(name) {
        var oldLocale = null;
        // TODO: Find a better way to register and load all the locales in Node
        if (!locales[name] && (typeof module !== 'undefined') &&
                module && module.exports) {
            try {
                oldLocale = globalLocale._abbr;
                require('./locale/' + name);
                // because defineLocale currently also sets the global locale, we
                // want to undo that for lazy loaded locales
                locale_locales__getSetGlobalLocale(oldLocale);
            } catch (e) { }
        }
        return locales[name];
    }

    // This function will load locale and then set the global locale.  If
    // no arguments are passed in, it will simply return the current global
    // locale key.
    function locale_locales__getSetGlobalLocale(key, values) {
        var data;
        if (key) {
            if (isUndefined(values)) {
                data = locale_locales__getLocale(key);
            }
            else {
                data = defineLocale(key, values);
            }

            if (data) {
                // moment.duration._locale = moment._locale = data;
                globalLocale = data;
            }
        }

        return globalLocale._abbr;
    }

    function defineLocale(name, config) {
        if (config !== null) {
            var parentConfig = baseConfig;
            config.abbr = name;
            if (locales[name] != null) {
                deprecateSimple('defineLocaleOverride',
                        'use moment.updateLocale(localeName, config) to change ' +
                        'an existing locale. moment.defineLocale(localeName, ' +
                        'config) should only be used for creating a new locale ' +
                        'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.');
                parentConfig = locales[name]._config;
            } else if (config.parentLocale != null) {
                if (locales[config.parentLocale] != null) {
                    parentConfig = locales[config.parentLocale]._config;
                } else {
                    // treat as if there is no base config
                    deprecateSimple('parentLocaleUndefined',
                            'specified parentLocale is not defined yet. See http://momentjs.com/guides/#/warnings/parent-locale/');
                }
            }
            locales[name] = new Locale(mergeConfigs(parentConfig, config));

            // backwards compat for now: also set the locale
            locale_locales__getSetGlobalLocale(name);

            return locales[name];
        } else {
            // useful for testing
            delete locales[name];
            return null;
        }
    }

    function updateLocale(name, config) {
        if (config != null) {
            var locale, parentConfig = baseConfig;
            // MERGE
            if (locales[name] != null) {
                parentConfig = locales[name]._config;
            }
            config = mergeConfigs(parentConfig, config);
            locale = new Locale(config);
            locale.parentLocale = locales[name];
            locales[name] = locale;

            // backwards compat for now: also set the locale
            locale_locales__getSetGlobalLocale(name);
        } else {
            // pass null for config to unupdate, useful for tests
            if (locales[name] != null) {
                if (locales[name].parentLocale != null) {
                    locales[name] = locales[name].parentLocale;
                } else if (locales[name] != null) {
                    delete locales[name];
                }
            }
        }
        return locales[name];
    }

    // returns locale data
    function locale_locales__getLocale(key) {
        var locale;

        if (key && key._locale && key._locale._abbr) {
            key = key._locale._abbr;
        }

        if (!key) {
            return globalLocale;
        }

        if (!isArray(key)) {
            //short-circuit everything else
            locale = loadLocale(key);
            if (locale) {
                return locale;
            }
            key = [key];
        }

        return chooseLocale(key);
    }

    function locale_locales__listLocales() {
        return keys(locales);
    }

    function checkOverflow(m) {
        var overflow;
        var a = m._a;

        if (a && getParsingFlags(m).overflow === -2) {
            overflow =
                a[MONTH] < 0 || a[MONTH] > 11 ? MONTH :
                a[DATE] < 1 || a[DATE] > daysInMonth(a[YEAR], a[MONTH]) ? DATE :
                a[HOUR] < 0 || a[HOUR] > 24 || (a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0)) ? HOUR :
                a[MINUTE] < 0 || a[MINUTE] > 59 ? MINUTE :
                a[SECOND] < 0 || a[SECOND] > 59 ? SECOND :
                a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND :
                -1;

            if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
                overflow = DATE;
            }
            if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
                overflow = WEEK;
            }
            if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
                overflow = WEEKDAY;
            }

            getParsingFlags(m).overflow = overflow;
        }

        return m;
    }

    // iso 8601 regex
    // 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
    var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?/;
    var basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?/;

    var tzRegex = /Z|[+-]\d\d(?::?\d\d)?/;

    var isoDates = [
        ['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/],
        ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/],
        ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/],
        ['GGGG-[W]WW', /\d{4}-W\d\d/, false],
        ['YYYY-DDD', /\d{4}-\d{3}/],
        ['YYYY-MM', /\d{4}-\d\d/, false],
        ['YYYYYYMMDD', /[+-]\d{10}/],
        ['YYYYMMDD', /\d{8}/],
        // YYYYMM is NOT allowed by the standard
        ['GGGG[W]WWE', /\d{4}W\d{3}/],
        ['GGGG[W]WW', /\d{4}W\d{2}/, false],
        ['YYYYDDD', /\d{7}/]
    ];

    // iso time formats and regexes
    var isoTimes = [
        ['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/],
        ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/],
        ['HH:mm:ss', /\d\d:\d\d:\d\d/],
        ['HH:mm', /\d\d:\d\d/],
        ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/],
        ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/],
        ['HHmmss', /\d\d\d\d\d\d/],
        ['HHmm', /\d\d\d\d/],
        ['HH', /\d\d/]
    ];

    var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i;

    // date from iso format
    function configFromISO(config) {
        var i, l,
            string = config._i,
            match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
            allowTime, dateFormat, timeFormat, tzFormat;

        if (match) {
            getParsingFlags(config).iso = true;

            for (i = 0, l = isoDates.length; i < l; i++) {
                if (isoDates[i][1].exec(match[1])) {
                    dateFormat = isoDates[i][0];
                    allowTime = isoDates[i][2] !== false;
                    break;
                }
            }
            if (dateFormat == null) {
                config._isValid = false;
                return;
            }
            if (match[3]) {
                for (i = 0, l = isoTimes.length; i < l; i++) {
                    if (isoTimes[i][1].exec(match[3])) {
                        // match[2] should be 'T' or space
                        timeFormat = (match[2] || ' ') + isoTimes[i][0];
                        break;
                    }
                }
                if (timeFormat == null) {
                    config._isValid = false;
                    return;
                }
            }
            if (!allowTime && timeFormat != null) {
                config._isValid = false;
                return;
            }
            if (match[4]) {
                if (tzRegex.exec(match[4])) {
                    tzFormat = 'Z';
                } else {
                    config._isValid = false;
                    return;
                }
            }
            config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
            configFromStringAndFormat(config);
        } else {
            config._isValid = false;
        }
    }

    // date from iso format or fallback
    function configFromString(config) {
        var matched = aspNetJsonRegex.exec(config._i);

        if (matched !== null) {
            config._d = new Date(+matched[1]);
            return;
        }

        configFromISO(config);
        if (config._isValid === false) {
            delete config._isValid;
            utils_hooks__hooks.createFromInputFallback(config);
        }
    }

    utils_hooks__hooks.createFromInputFallback = deprecate(
        'moment construction falls back to js Date. This is ' +
        'discouraged and will be removed in upcoming major ' +
        'release. Please refer to ' +
        'http://momentjs.com/guides/#/warnings/js-date/ for more info.',
        function (config) {
            config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
        }
    );

    // Pick the first defined of two or three arguments.
    function defaults(a, b, c) {
        if (a != null) {
            return a;
        }
        if (b != null) {
            return b;
        }
        return c;
    }

    function currentDateArray(config) {
        // hooks is actually the exported moment object
        var nowValue = new Date(utils_hooks__hooks.now());
        if (config._useUTC) {
            return [nowValue.getUTCFullYear(), nowValue.getUTCMonth(), nowValue.getUTCDate()];
        }
        return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
    }

    // convert an array to a date.
    // the array should mirror the parameters below
    // note: all values past the year are optional and will default to the lowest possible value.
    // [year, month, day , hour, minute, second, millisecond]
    function configFromArray(config) {
        var i, date, input = [], currentDate, yearToUse;

        if (config._d) {
            return;
        }

        currentDate = currentDateArray(config);

        //compute day of the year from weeks and weekdays
        if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
            dayOfYearFromWeekInfo(config);
        }

        //if the day of the year is set, figure out what it is
        if (config._dayOfYear) {
            yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);

            if (config._dayOfYear > daysInYear(yearToUse)) {
                getParsingFlags(config)._overflowDayOfYear = true;
            }

            date = createUTCDate(yearToUse, 0, config._dayOfYear);
            config._a[MONTH] = date.getUTCMonth();
            config._a[DATE] = date.getUTCDate();
        }

        // Default to current date.
        // * if no year, month, day of month are given, default to today
        // * if day of month is given, default month and year
        // * if month is given, default only year
        // * if year is given, don't default anything
        for (i = 0; i < 3 && config._a[i] == null; ++i) {
            config._a[i] = input[i] = currentDate[i];
        }

        // Zero out whatever was not defaulted, including time
        for (; i < 7; i++) {
            config._a[i] = input[i] = (config._a[i] == null) ? (i === 2 ? 1 : 0) : config._a[i];
        }

        // Check for 24:00:00.000
        if (config._a[HOUR] === 24 &&
                config._a[MINUTE] === 0 &&
                config._a[SECOND] === 0 &&
                config._a[MILLISECOND] === 0) {
            config._nextDay = true;
            config._a[HOUR] = 0;
        }

        config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
        // Apply timezone offset from input. The actual utcOffset can be changed
        // with parseZone.
        if (config._tzm != null) {
            config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
        }

        if (config._nextDay) {
            config._a[HOUR] = 24;
        }
    }

    function dayOfYearFromWeekInfo(config) {
        var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow;

        w = config._w;
        if (w.GG != null || w.W != null || w.E != null) {
            dow = 1;
            doy = 4;

            // TODO: We need to take the current isoWeekYear, but that depends on
            // how we interpret now (local, utc, fixed offset). So create
            // a now version of current config (take local/utc/offset flags, and
            // create now).
            weekYear = defaults(w.GG, config._a[YEAR], weekOfYear(local__createLocal(), 1, 4).year);
            week = defaults(w.W, 1);
            weekday = defaults(w.E, 1);
            if (weekday < 1 || weekday > 7) {
                weekdayOverflow = true;
            }
        } else {
            dow = config._locale._week.dow;
            doy = config._locale._week.doy;

            weekYear = defaults(w.gg, config._a[YEAR], weekOfYear(local__createLocal(), dow, doy).year);
            week = defaults(w.w, 1);

            if (w.d != null) {
                // weekday -- low day numbers are considered next week
                weekday = w.d;
                if (weekday < 0 || weekday > 6) {
                    weekdayOverflow = true;
                }
            } else if (w.e != null) {
                // local weekday -- counting starts from begining of week
                weekday = w.e + dow;
                if (w.e < 0 || w.e > 6) {
                    weekdayOverflow = true;
                }
            } else {
                // default to begining of week
                weekday = dow;
            }
        }
        if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
            getParsingFlags(config)._overflowWeeks = true;
        } else if (weekdayOverflow != null) {
            getParsingFlags(config)._overflowWeekday = true;
        } else {
            temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
            config._a[YEAR] = temp.year;
            config._dayOfYear = temp.dayOfYear;
        }
    }

    // constant that refers to the ISO standard
    utils_hooks__hooks.ISO_8601 = function () { };

    // date from string and format string
    function configFromStringAndFormat(config) {
        // TODO: Move this to another part of the creation flow to prevent circular deps
        if (config._f === utils_hooks__hooks.ISO_8601) {
            configFromISO(config);
            return;
        }

        config._a = [];
        getParsingFlags(config).empty = true;

        // This array is used to make a Date, either with `new Date` or `Date.UTC`
        var string = '' + config._i,
            i, parsedInput, tokens, token, skipped,
            stringLength = string.length,
            totalParsedInputLength = 0;

        tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

        for (i = 0; i < tokens.length; i++) {
            token = tokens[i];
            parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
            // console.log('token', token, 'parsedInput', parsedInput,
            //         'regex', getParseRegexForToken(token, config));
            if (parsedInput) {
                skipped = string.substr(0, string.indexOf(parsedInput));
                if (skipped.length > 0) {
                    getParsingFlags(config).unusedInput.push(skipped);
                }
                string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
                totalParsedInputLength += parsedInput.length;
            }
            // don't parse if it's not a known token
            if (formatTokenFunctions[token]) {
                if (parsedInput) {
                    getParsingFlags(config).empty = false;
                }
                else {
                    getParsingFlags(config).unusedTokens.push(token);
                }
                addTimeToArrayFromToken(token, parsedInput, config);
            }
            else if (config._strict && !parsedInput) {
                getParsingFlags(config).unusedTokens.push(token);
            }
        }

        // add remaining unparsed input length to the string
        getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
        if (string.length > 0) {
            getParsingFlags(config).unusedInput.push(string);
        }

        // clear _12h flag if hour is <= 12
        if (config._a[HOUR] <= 12 &&
            getParsingFlags(config).bigHour === true &&
            config._a[HOUR] > 0) {
            getParsingFlags(config).bigHour = undefined;
        }

        getParsingFlags(config).parsedDateParts = config._a.slice(0);
        getParsingFlags(config).meridiem = config._meridiem;
        // handle meridiem
        config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);

        configFromArray(config);
        checkOverflow(config);
    }


    function meridiemFixWrap(locale, hour, meridiem) {
        var isPm;

        if (meridiem == null) {
            // nothing to do
            return hour;
        }
        if (locale.meridiemHour != null) {
            return locale.meridiemHour(hour, meridiem);
        } else if (locale.isPM != null) {
            // Fallback
            isPm = locale.isPM(meridiem);
            if (isPm && hour < 12) {
                hour += 12;
            }
            if (!isPm && hour === 12) {
                hour = 0;
            }
            return hour;
        } else {
            // this is not supposed to happen
            return hour;
        }
    }

    // date from string and array of format strings
    function configFromStringAndArray(config) {
        var tempConfig,
            bestMoment,

            scoreToBeat,
            i,
            currentScore;

        if (config._f.length === 0) {
            getParsingFlags(config).invalidFormat = true;
            config._d = new Date(NaN);
            return;
        }

        for (i = 0; i < config._f.length; i++) {
            currentScore = 0;
            tempConfig = copyConfig({}, config);
            if (config._useUTC != null) {
                tempConfig._useUTC = config._useUTC;
            }
            tempConfig._f = config._f[i];
            configFromStringAndFormat(tempConfig);

            if (!valid__isValid(tempConfig)) {
                continue;
            }

            // if there is any input that was not parsed add a penalty for that format
            currentScore += getParsingFlags(tempConfig).charsLeftOver;

            //or tokens
            currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

            getParsingFlags(tempConfig).score = currentScore;

            if (scoreToBeat == null || currentScore < scoreToBeat) {
                scoreToBeat = currentScore;
                bestMoment = tempConfig;
            }
        }

        extend(config, bestMoment || tempConfig);
    }

    function configFromObject(config) {
        if (config._d) {
            return;
        }

        var i = normalizeObjectUnits(config._i);
        config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
            return obj && parseInt(obj, 10);
        });

        configFromArray(config);
    }

    function createFromConfig(config) {
        var res = new Moment(checkOverflow(prepareConfig(config)));
        if (res._nextDay) {
            // Adding is smart enough around DST
            res.add(1, 'd');
            res._nextDay = undefined;
        }

        return res;
    }

    function prepareConfig(config) {
        var input = config._i,
            format = config._f;

        config._locale = config._locale || locale_locales__getLocale(config._l);

        if (input === null || (format === undefined && input === '')) {
            return valid__createInvalid({ nullInput: true });
        }

        if (typeof input === 'string') {
            config._i = input = config._locale.preparse(input);
        }

        if (isMoment(input)) {
            return new Moment(checkOverflow(input));
        } else if (isArray(format)) {
            configFromStringAndArray(config);
        } else if (isDate(input)) {
            config._d = input;
        } else if (format) {
            configFromStringAndFormat(config);
        } else {
            configFromInput(config);
        }

        if (!valid__isValid(config)) {
            config._d = null;
        }

        return config;
    }

    function configFromInput(config) {
        var input = config._i;
        if (input === undefined) {
            config._d = new Date(utils_hooks__hooks.now());
        } else if (isDate(input)) {
            config._d = new Date(input.valueOf());
        } else if (typeof input === 'string') {
            configFromString(config);
        } else if (isArray(input)) {
            config._a = map(input.slice(0), function (obj) {
                return parseInt(obj, 10);
            });
            configFromArray(config);
        } else if (typeof (input) === 'object') {
            configFromObject(config);
        } else if (typeof (input) === 'number') {
            // from milliseconds
            config._d = new Date(input);
        } else {
            utils_hooks__hooks.createFromInputFallback(config);
        }
    }

    function createLocalOrUTC(input, format, locale, strict, isUTC) {
        var c = {};

        if (typeof (locale) === 'boolean') {
            strict = locale;
            locale = undefined;
        }

        if ((isObject(input) && isObjectEmpty(input)) ||
                (isArray(input) && input.length === 0)) {
            input = undefined;
        }
        // object construction must be done this way.
        // https://github.com/moment/moment/issues/1423
        c._isAMomentObject = true;
        c._useUTC = c._isUTC = isUTC;
        c._l = locale;
        c._i = input;
        c._f = format;
        c._strict = strict;

        return createFromConfig(c);
    }

    function local__createLocal(input, format, locale, strict) {
        return createLocalOrUTC(input, format, locale, strict, false);
    }

    var prototypeMin = deprecate(
        'moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/',
        function () {
            var other = local__createLocal.apply(null, arguments);
            if (this.isValid() && other.isValid()) {
                return other < this ? this : other;
            } else {
                return valid__createInvalid();
            }
        }
    );

    var prototypeMax = deprecate(
        'moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/',
        function () {
            var other = local__createLocal.apply(null, arguments);
            if (this.isValid() && other.isValid()) {
                return other > this ? this : other;
            } else {
                return valid__createInvalid();
            }
        }
    );

    // Pick a moment m from moments so that m[fn](other) is true for all
    // other. This relies on the function fn to be transitive.
    //
    // moments should either be an array of moment objects or an array, whose
    // first element is an array of moment objects.
    function pickBy(fn, moments) {
        var res, i;
        if (moments.length === 1 && isArray(moments[0])) {
            moments = moments[0];
        }
        if (!moments.length) {
            return local__createLocal();
        }
        res = moments[0];
        for (i = 1; i < moments.length; ++i) {
            if (!moments[i].isValid() || moments[i][fn](res)) {
                res = moments[i];
            }
        }
        return res;
    }

    // TODO: Use [].sort instead?
    function min() {
        var args = [].slice.call(arguments, 0);

        return pickBy('isBefore', args);
    }

    function max() {
        var args = [].slice.call(arguments, 0);

        return pickBy('isAfter', args);
    }

    var now = function () {
        return Date.now ? Date.now() : +(new Date());
    };

    function Duration(duration) {
        var normalizedInput = normalizeObjectUnits(duration),
            years = normalizedInput.year || 0,
            quarters = normalizedInput.quarter || 0,
            months = normalizedInput.month || 0,
            weeks = normalizedInput.week || 0,
            days = normalizedInput.day || 0,
            hours = normalizedInput.hour || 0,
            minutes = normalizedInput.minute || 0,
            seconds = normalizedInput.second || 0,
            milliseconds = normalizedInput.millisecond || 0;

        // representation for dateAddRemove
        this._milliseconds = +milliseconds +
            seconds * 1e3 + // 1000
            minutes * 6e4 + // 1000 * 60
            hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
        // Because of dateAddRemove treats 24 hours as different from a
        // day when working around DST, we need to store them separately
        this._days = +days +
            weeks * 7;
        // It is impossible translate months into days without knowing
        // which months you are are talking about, so we have to store
        // it separately.
        this._months = +months +
            quarters * 3 +
            years * 12;

        this._data = {};

        this._locale = locale_locales__getLocale();

        this._bubble();
    }

    function isDuration(obj) {
        return obj instanceof Duration;
    }

    // FORMATTING

    function offset(token, separator) {
        addFormatToken(token, 0, 0, function () {
            var offset = this.utcOffset();
            var sign = '+';
            if (offset < 0) {
                offset = -offset;
                sign = '-';
            }
            return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~(offset) % 60, 2);
        });
    }

    offset('Z', ':');
    offset('ZZ', '');

    // PARSING

    addRegexToken('Z', matchShortOffset);
    addRegexToken('ZZ', matchShortOffset);
    addParseToken(['Z', 'ZZ'], function (input, array, config) {
        config._useUTC = true;
        config._tzm = offsetFromString(matchShortOffset, input);
    });

    // HELPERS

    // timezone chunker
    // '+10:00' > ['10',  '00']
    // '-1530'  > ['-15', '30']
    var chunkOffset = /([\+\-]|\d\d)/gi;

    function offsetFromString(matcher, string) {
        var matches = ((string || '').match(matcher) || []);
        var chunk = matches[matches.length - 1] || [];
        var parts = (chunk + '').match(chunkOffset) || ['-', 0, 0];
        var minutes = +(parts[1] * 60) + toInt(parts[2]);

        return parts[0] === '+' ? minutes : -minutes;
    }

    // Return a moment from input, that is local/utc/zone equivalent to model.
    function cloneWithOffset(input, model) {
        var res, diff;
        if (model._isUTC) {
            res = model.clone();
            diff = (isMoment(input) || isDate(input) ? input.valueOf() : local__createLocal(input).valueOf()) - res.valueOf();
            // Use low-level api, because this fn is low-level api.
            res._d.setTime(res._d.valueOf() + diff);
            utils_hooks__hooks.updateOffset(res, false);
            return res;
        } else {
            return local__createLocal(input).local();
        }
    }

    function getDateOffset(m) {
        // On Firefox.24 Date#getTimezoneOffset returns a floating point.
        // https://github.com/moment/moment/pull/1871
        return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
    }

    // HOOKS

    // This function will be called whenever a moment is mutated.
    // It is intended to keep the offset in sync with the timezone.
    utils_hooks__hooks.updateOffset = function () { };

    // MOMENTS

    // keepLocalTime = true means only change the timezone, without
    // affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
    // 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
    // +0200, so we adjust the time as needed, to be valid.
    //
    // Keeping the time actually adds/subtracts (one hour)
    // from the actual represented time. That is why we call updateOffset
    // a second time. In case it wants us to change the offset again
    // _changeInProgress == true case, then we have to adjust, because
    // there is no such time in the given timezone.
    function getSetOffset(input, keepLocalTime) {
        var offset = this._offset || 0,
            localAdjust;
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        if (input != null) {
            if (typeof input === 'string') {
                input = offsetFromString(matchShortOffset, input);
            } else if (Math.abs(input) < 16) {
                input = input * 60;
            }
            if (!this._isUTC && keepLocalTime) {
                localAdjust = getDateOffset(this);
            }
            this._offset = input;
            this._isUTC = true;
            if (localAdjust != null) {
                this.add(localAdjust, 'm');
            }
            if (offset !== input) {
                if (!keepLocalTime || this._changeInProgress) {
                    add_subtract__addSubtract(this, create__createDuration(input - offset, 'm'), 1, false);
                } else if (!this._changeInProgress) {
                    this._changeInProgress = true;
                    utils_hooks__hooks.updateOffset(this, true);
                    this._changeInProgress = null;
                }
            }
            return this;
        } else {
            return this._isUTC ? offset : getDateOffset(this);
        }
    }

    function getSetZone(input, keepLocalTime) {
        if (input != null) {
            if (typeof input !== 'string') {
                input = -input;
            }

            this.utcOffset(input, keepLocalTime);

            return this;
        } else {
            return -this.utcOffset();
        }
    }

    function setOffsetToUTC(keepLocalTime) {
        return this.utcOffset(0, keepLocalTime);
    }

    function setOffsetToLocal(keepLocalTime) {
        if (this._isUTC) {
            this.utcOffset(0, keepLocalTime);
            this._isUTC = false;

            if (keepLocalTime) {
                this.subtract(getDateOffset(this), 'm');
            }
        }
        return this;
    }

    function setOffsetToParsedOffset() {
        if (this._tzm) {
            this.utcOffset(this._tzm);
        } else if (typeof this._i === 'string') {
            this.utcOffset(offsetFromString(matchOffset, this._i));
        }
        return this;
    }

    function hasAlignedHourOffset(input) {
        if (!this.isValid()) {
            return false;
        }
        input = input ? local__createLocal(input).utcOffset() : 0;

        return (this.utcOffset() - input) % 60 === 0;
    }

    function isDaylightSavingTime() {
        return (
            this.utcOffset() > this.clone().month(0).utcOffset() ||
            this.utcOffset() > this.clone().month(5).utcOffset()
        );
    }

    function isDaylightSavingTimeShifted() {
        if (!isUndefined(this._isDSTShifted)) {
            return this._isDSTShifted;
        }

        var c = {};

        copyConfig(c, this);
        c = prepareConfig(c);

        if (c._a) {
            var other = c._isUTC ? create_utc__createUTC(c._a) : local__createLocal(c._a);
            this._isDSTShifted = this.isValid() &&
                compareArrays(c._a, other.toArray()) > 0;
        } else {
            this._isDSTShifted = false;
        }

        return this._isDSTShifted;
    }

    function isLocal() {
        return this.isValid() ? !this._isUTC : false;
    }

    function isUtcOffset() {
        return this.isValid() ? this._isUTC : false;
    }

    function isUtc() {
        return this.isValid() ? this._isUTC && this._offset === 0 : false;
    }

    // ASP.NET json date format regex
    var aspNetRegex = /^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?\d*)?$/;

    // from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
    // somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
    // and further modified to allow for strings containing both week and day
    var isoRegex = /^(-)?P(?:(-?[0-9,.]*)Y)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)W)?(?:(-?[0-9,.]*)D)?(?:T(?:(-?[0-9,.]*)H)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)S)?)?$/;

    function create__createDuration(input, key) {
        var duration = input,
            // matching against regexp is expensive, do it on demand
            match = null,
            sign,
            ret,
            diffRes;

        if (isDuration(input)) {
            duration = {
                ms: input._milliseconds,
                d: input._days,
                M: input._months
            };
        } else if (typeof input === 'number') {
            duration = {};
            if (key) {
                duration[key] = input;
            } else {
                duration.milliseconds = input;
            }
        } else if (!!(match = aspNetRegex.exec(input))) {
            sign = (match[1] === '-') ? -1 : 1;
            duration = {
                y: 0,
                d: toInt(match[DATE]) * sign,
                h: toInt(match[HOUR]) * sign,
                m: toInt(match[MINUTE]) * sign,
                s: toInt(match[SECOND]) * sign,
                ms: toInt(match[MILLISECOND]) * sign
            };
        } else if (!!(match = isoRegex.exec(input))) {
            sign = (match[1] === '-') ? -1 : 1;
            duration = {
                y: parseIso(match[2], sign),
                M: parseIso(match[3], sign),
                w: parseIso(match[4], sign),
                d: parseIso(match[5], sign),
                h: parseIso(match[6], sign),
                m: parseIso(match[7], sign),
                s: parseIso(match[8], sign)
            };
        } else if (duration == null) {// checks for null or undefined
            duration = {};
        } else if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
            diffRes = momentsDifference(local__createLocal(duration.from), local__createLocal(duration.to));

            duration = {};
            duration.ms = diffRes.milliseconds;
            duration.M = diffRes.months;
        }

        ret = new Duration(duration);

        if (isDuration(input) && hasOwnProp(input, '_locale')) {
            ret._locale = input._locale;
        }

        return ret;
    }

    create__createDuration.fn = Duration.prototype;

    function parseIso(inp, sign) {
        // We'd normally use ~~inp for this, but unfortunately it also
        // converts floats to ints.
        // inp may be undefined, so careful calling replace on it.
        var res = inp && parseFloat(inp.replace(',', '.'));
        // apply sign while we're at it
        return (isNaN(res) ? 0 : res) * sign;
    }

    function positiveMomentsDifference(base, other) {
        var res = { milliseconds: 0, months: 0 };

        res.months = other.month() - base.month() +
            (other.year() - base.year()) * 12;
        if (base.clone().add(res.months, 'M').isAfter(other)) {
            --res.months;
        }

        res.milliseconds = +other - +(base.clone().add(res.months, 'M'));

        return res;
    }

    function momentsDifference(base, other) {
        var res;
        if (!(base.isValid() && other.isValid())) {
            return { milliseconds: 0, months: 0 };
        }

        other = cloneWithOffset(other, base);
        if (base.isBefore(other)) {
            res = positiveMomentsDifference(base, other);
        } else {
            res = positiveMomentsDifference(other, base);
            res.milliseconds = -res.milliseconds;
            res.months = -res.months;
        }

        return res;
    }

    function absRound(number) {
        if (number < 0) {
            return Math.round(-1 * number) * -1;
        } else {
            return Math.round(number);
        }
    }

    // TODO: remove 'name' arg after deprecation is removed
    function createAdder(direction, name) {
        return function (val, period) {
            var dur, tmp;
            //invert the arguments, but complain about it
            if (period !== null && !isNaN(+period)) {
                deprecateSimple(name, 'moment().' + name + '(period, number) is deprecated. Please use moment().' + name + '(number, period). ' +
                'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.');
                tmp = val; val = period; period = tmp;
            }

            val = typeof val === 'string' ? +val : val;
            dur = create__createDuration(val, period);
            add_subtract__addSubtract(this, dur, direction);
            return this;
        };
    }

    function add_subtract__addSubtract(mom, duration, isAdding, updateOffset) {
        var milliseconds = duration._milliseconds,
            days = absRound(duration._days),
            months = absRound(duration._months);

        if (!mom.isValid()) {
            // No op
            return;
        }

        updateOffset = updateOffset == null ? true : updateOffset;

        if (milliseconds) {
            mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
        }
        if (days) {
            get_set__set(mom, 'Date', get_set__get(mom, 'Date') + days * isAdding);
        }
        if (months) {
            setMonth(mom, get_set__get(mom, 'Month') + months * isAdding);
        }
        if (updateOffset) {
            utils_hooks__hooks.updateOffset(mom, days || months);
        }
    }

    var add_subtract__add = createAdder(1, 'add');
    var add_subtract__subtract = createAdder(-1, 'subtract');

    function getCalendarFormat(myMoment, now) {
        var diff = myMoment.diff(now, 'days', true);
        return diff < -6 ? 'sameElse' :
                diff < -1 ? 'lastWeek' :
                diff < 0 ? 'lastDay' :
                diff < 1 ? 'sameDay' :
                diff < 2 ? 'nextDay' :
                diff < 7 ? 'nextWeek' : 'sameElse';
    }

    function moment_calendar__calendar(time, formats) {
        // We want to compare the start of today, vs this.
        // Getting start-of-today depends on whether we're local/utc/offset or not.
        var now = time || local__createLocal(),
            sod = cloneWithOffset(now, this).startOf('day'),
            format = utils_hooks__hooks.calendarFormat(this, sod) || 'sameElse';

        var output = formats && (isFunction(formats[format]) ? formats[format].call(this, now) : formats[format]);

        return this.format(output || this.localeData().calendar(format, this, local__createLocal(now)));
    }

    function clone() {
        return new Moment(this);
    }

    function isAfter(input, units) {
        var localInput = isMoment(input) ? input : local__createLocal(input);
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
        if (units === 'millisecond') {
            return this.valueOf() > localInput.valueOf();
        } else {
            return localInput.valueOf() < this.clone().startOf(units).valueOf();
        }
    }

    function isBefore(input, units) {
        var localInput = isMoment(input) ? input : local__createLocal(input);
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
        if (units === 'millisecond') {
            return this.valueOf() < localInput.valueOf();
        } else {
            return this.clone().endOf(units).valueOf() < localInput.valueOf();
        }
    }

    function isBetween(from, to, units, inclusivity) {
        inclusivity = inclusivity || '()';
        return (inclusivity[0] === '(' ? this.isAfter(from, units) : !this.isBefore(from, units)) &&
            (inclusivity[1] === ')' ? this.isBefore(to, units) : !this.isAfter(to, units));
    }

    function isSame(input, units) {
        var localInput = isMoment(input) ? input : local__createLocal(input),
            inputMs;
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(units || 'millisecond');
        if (units === 'millisecond') {
            return this.valueOf() === localInput.valueOf();
        } else {
            inputMs = localInput.valueOf();
            return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
        }
    }

    function isSameOrAfter(input, units) {
        return this.isSame(input, units) || this.isAfter(input, units);
    }

    function isSameOrBefore(input, units) {
        return this.isSame(input, units) || this.isBefore(input, units);
    }

    function diff(input, units, asFloat) {
        var that,
            zoneDelta,
            delta, output;

        if (!this.isValid()) {
            return NaN;
        }

        that = cloneWithOffset(input, this);

        if (!that.isValid()) {
            return NaN;
        }

        zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;

        units = normalizeUnits(units);

        if (units === 'year' || units === 'month' || units === 'quarter') {
            output = monthDiff(this, that);
            if (units === 'quarter') {
                output = output / 3;
            } else if (units === 'year') {
                output = output / 12;
            }
        } else {
            delta = this - that;
            output = units === 'second' ? delta / 1e3 : // 1000
                units === 'minute' ? delta / 6e4 : // 1000 * 60
                units === 'hour' ? delta / 36e5 : // 1000 * 60 * 60
                units === 'day' ? (delta - zoneDelta) / 864e5 : // 1000 * 60 * 60 * 24, negate dst
                units === 'week' ? (delta - zoneDelta) / 6048e5 : // 1000 * 60 * 60 * 24 * 7, negate dst
                delta;
        }
        return asFloat ? output : absFloor(output);
    }

    function monthDiff(a, b) {
        // difference in months
        var wholeMonthDiff = ((b.year() - a.year()) * 12) + (b.month() - a.month()),
            // b is in (anchor - 1 month, anchor + 1 month)
            anchor = a.clone().add(wholeMonthDiff, 'months'),
            anchor2, adjust;

        if (b - anchor < 0) {
            anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor - anchor2);
        } else {
            anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor2 - anchor);
        }

        //check for negative zero, return zero if negative zero
        return -(wholeMonthDiff + adjust) || 0;
    }

    utils_hooks__hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
    utils_hooks__hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

    function toString() {
        return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
    }

    function moment_format__toISOString() {
        var m = this.clone().utc();
        if (0 < m.year() && m.year() <= 9999) {
            if (isFunction(Date.prototype.toISOString)) {
                // native implementation is ~50x faster, use it when we can
                return this.toDate().toISOString();
            } else {
                return formatMoment(m, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
            }
        } else {
            return formatMoment(m, 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
        }
    }

    function format(inputString) {
        if (!inputString) {
            inputString = this.isUtc() ? utils_hooks__hooks.defaultFormatUtc : utils_hooks__hooks.defaultFormat;
        }
        var output = formatMoment(this, inputString);
        return this.localeData().postformat(output);
    }

    function from(time, withoutSuffix) {
        if (this.isValid() &&
                ((isMoment(time) && time.isValid()) ||
                 local__createLocal(time).isValid())) {
            return create__createDuration({ to: this, from: time }).locale(this.locale()).humanize(!withoutSuffix);
        } else {
            return this.localeData().invalidDate();
        }
    }

    function fromNow(withoutSuffix) {
        return this.from(local__createLocal(), withoutSuffix);
    }

    function to(time, withoutSuffix) {
        if (this.isValid() &&
                ((isMoment(time) && time.isValid()) ||
                 local__createLocal(time).isValid())) {
            return create__createDuration({ from: this, to: time }).locale(this.locale()).humanize(!withoutSuffix);
        } else {
            return this.localeData().invalidDate();
        }
    }

    function toNow(withoutSuffix) {
        return this.to(local__createLocal(), withoutSuffix);
    }

    // If passed a locale key, it will set the locale for this
    // instance.  Otherwise, it will return the locale configuration
    // variables for this instance.
    function locale(key) {
        var newLocaleData;

        if (key === undefined) {
            return this._locale._abbr;
        } else {
            newLocaleData = locale_locales__getLocale(key);
            if (newLocaleData != null) {
                this._locale = newLocaleData;
            }
            return this;
        }
    }

    var lang = deprecate(
        'moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',
        function (key) {
            if (key === undefined) {
                return this.localeData();
            } else {
                return this.locale(key);
            }
        }
    );

    function localeData() {
        return this._locale;
    }

    function startOf(units) {
        units = normalizeUnits(units);
        // the following switch intentionally omits break keywords
        // to utilize falling through the cases.
        switch (units) {
            case 'year':
                this.month(0);
                /* falls through */
            case 'quarter':
            case 'month':
                this.date(1);
                /* falls through */
            case 'week':
            case 'isoWeek':
            case 'day':
            case 'date':
                this.hours(0);
                /* falls through */
            case 'hour':
                this.minutes(0);
                /* falls through */
            case 'minute':
                this.seconds(0);
                /* falls through */
            case 'second':
                this.milliseconds(0);
        }

        // weeks are a special case
        if (units === 'week') {
            this.weekday(0);
        }
        if (units === 'isoWeek') {
            this.isoWeekday(1);
        }

        // quarters are also special
        if (units === 'quarter') {
            this.month(Math.floor(this.month() / 3) * 3);
        }

        return this;
    }

    function endOf(units) {
        units = normalizeUnits(units);
        if (units === undefined || units === 'millisecond') {
            return this;
        }

        // 'date' is an alias for 'day', so it should be considered as such.
        if (units === 'date') {
            units = 'day';
        }

        return this.startOf(units).add(1, (units === 'isoWeek' ? 'week' : units)).subtract(1, 'ms');
    }

    function to_type__valueOf() {
        return this._d.valueOf() - ((this._offset || 0) * 60000);
    }

    function unix() {
        return Math.floor(this.valueOf() / 1000);
    }

    function toDate() {
        return new Date(this.valueOf());
    }

    function toArray() {
        var m = this;
        return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
    }

    function toObject() {
        var m = this;
        return {
            years: m.year(),
            months: m.month(),
            date: m.date(),
            hours: m.hours(),
            minutes: m.minutes(),
            seconds: m.seconds(),
            milliseconds: m.milliseconds()
        };
    }

    function toJSON() {
        // new Date(NaN).toJSON() === null
        return this.isValid() ? this.toISOString() : null;
    }

    function moment_valid__isValid() {
        return valid__isValid(this);
    }

    function parsingFlags() {
        return extend({}, getParsingFlags(this));
    }

    function invalidAt() {
        return getParsingFlags(this).overflow;
    }

    function creationData() {
        return {
            input: this._i,
            format: this._f,
            locale: this._locale,
            isUTC: this._isUTC,
            strict: this._strict
        };
    }

    // FORMATTING

    addFormatToken(0, ['gg', 2], 0, function () {
        return this.weekYear() % 100;
    });

    addFormatToken(0, ['GG', 2], 0, function () {
        return this.isoWeekYear() % 100;
    });

    function addWeekYearFormatToken(token, getter) {
        addFormatToken(0, [token, token.length], 0, getter);
    }

    addWeekYearFormatToken('gggg', 'weekYear');
    addWeekYearFormatToken('ggggg', 'weekYear');
    addWeekYearFormatToken('GGGG', 'isoWeekYear');
    addWeekYearFormatToken('GGGGG', 'isoWeekYear');

    // ALIASES

    addUnitAlias('weekYear', 'gg');
    addUnitAlias('isoWeekYear', 'GG');

    // PRIORITY

    addUnitPriority('weekYear', 1);
    addUnitPriority('isoWeekYear', 1);


    // PARSING

    addRegexToken('G', matchSigned);
    addRegexToken('g', matchSigned);
    addRegexToken('GG', match1to2, match2);
    addRegexToken('gg', match1to2, match2);
    addRegexToken('GGGG', match1to4, match4);
    addRegexToken('gggg', match1to4, match4);
    addRegexToken('GGGGG', match1to6, match6);
    addRegexToken('ggggg', match1to6, match6);

    addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
        week[token.substr(0, 2)] = toInt(input);
    });

    addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
        week[token] = utils_hooks__hooks.parseTwoDigitYear(input);
    });

    // MOMENTS

    function getSetWeekYear(input) {
        return getSetWeekYearHelper.call(this,
                input,
                this.week(),
                this.weekday(),
                this.localeData()._week.dow,
                this.localeData()._week.doy);
    }

    function getSetISOWeekYear(input) {
        return getSetWeekYearHelper.call(this,
                input, this.isoWeek(), this.isoWeekday(), 1, 4);
    }

    function getISOWeeksInYear() {
        return weeksInYear(this.year(), 1, 4);
    }

    function getWeeksInYear() {
        var weekInfo = this.localeData()._week;
        return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
    }

    function getSetWeekYearHelper(input, week, weekday, dow, doy) {
        var weeksTarget;
        if (input == null) {
            return weekOfYear(this, dow, doy).year;
        } else {
            weeksTarget = weeksInYear(input, dow, doy);
            if (week > weeksTarget) {
                week = weeksTarget;
            }
            return setWeekAll.call(this, input, week, weekday, dow, doy);
        }
    }

    function setWeekAll(weekYear, week, weekday, dow, doy) {
        var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
            date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);

        this.year(date.getUTCFullYear());
        this.month(date.getUTCMonth());
        this.date(date.getUTCDate());
        return this;
    }

    // FORMATTING

    addFormatToken('Q', 0, 'Qo', 'quarter');

    // ALIASES

    addUnitAlias('quarter', 'Q');

    // PRIORITY

    addUnitPriority('quarter', 7);

    // PARSING

    addRegexToken('Q', match1);
    addParseToken('Q', function (input, array) {
        array[MONTH] = (toInt(input) - 1) * 3;
    });

    // MOMENTS

    function getSetQuarter(input) {
        return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
    }

    // FORMATTING

    addFormatToken('D', ['DD', 2], 'Do', 'date');

    // ALIASES

    addUnitAlias('date', 'D');

    // PRIOROITY
    addUnitPriority('date', 9);

    // PARSING

    addRegexToken('D', match1to2);
    addRegexToken('DD', match1to2, match2);
    addRegexToken('Do', function (isStrict, locale) {
        return isStrict ? locale._ordinalParse : locale._ordinalParseLenient;
    });

    addParseToken(['D', 'DD'], DATE);
    addParseToken('Do', function (input, array) {
        array[DATE] = toInt(input.match(match1to2)[0], 10);
    });

    // MOMENTS

    var getSetDayOfMonth = makeGetSet('Date', true);

    // FORMATTING

    addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

    // ALIASES

    addUnitAlias('dayOfYear', 'DDD');

    // PRIORITY
    addUnitPriority('dayOfYear', 4);

    // PARSING

    addRegexToken('DDD', match1to3);
    addRegexToken('DDDD', match3);
    addParseToken(['DDD', 'DDDD'], function (input, array, config) {
        config._dayOfYear = toInt(input);
    });

    // HELPERS

    // MOMENTS

    function getSetDayOfYear(input) {
        var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
        return input == null ? dayOfYear : this.add((input - dayOfYear), 'd');
    }

    // FORMATTING

    addFormatToken('m', ['mm', 2], 0, 'minute');

    // ALIASES

    addUnitAlias('minute', 'm');

    // PRIORITY

    addUnitPriority('minute', 14);

    // PARSING

    addRegexToken('m', match1to2);
    addRegexToken('mm', match1to2, match2);
    addParseToken(['m', 'mm'], MINUTE);

    // MOMENTS

    var getSetMinute = makeGetSet('Minutes', false);

    // FORMATTING

    addFormatToken('s', ['ss', 2], 0, 'second');

    // ALIASES

    addUnitAlias('second', 's');

    // PRIORITY

    addUnitPriority('second', 15);

    // PARSING

    addRegexToken('s', match1to2);
    addRegexToken('ss', match1to2, match2);
    addParseToken(['s', 'ss'], SECOND);

    // MOMENTS

    var getSetSecond = makeGetSet('Seconds', false);

    // FORMATTING

    addFormatToken('S', 0, 0, function () {
        return ~~(this.millisecond() / 100);
    });

    addFormatToken(0, ['SS', 2], 0, function () {
        return ~~(this.millisecond() / 10);
    });

    addFormatToken(0, ['SSS', 3], 0, 'millisecond');
    addFormatToken(0, ['SSSS', 4], 0, function () {
        return this.millisecond() * 10;
    });
    addFormatToken(0, ['SSSSS', 5], 0, function () {
        return this.millisecond() * 100;
    });
    addFormatToken(0, ['SSSSSS', 6], 0, function () {
        return this.millisecond() * 1000;
    });
    addFormatToken(0, ['SSSSSSS', 7], 0, function () {
        return this.millisecond() * 10000;
    });
    addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
        return this.millisecond() * 100000;
    });
    addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
        return this.millisecond() * 1000000;
    });


    // ALIASES

    addUnitAlias('millisecond', 'ms');

    // PRIORITY

    addUnitPriority('millisecond', 16);

    // PARSING

    addRegexToken('S', match1to3, match1);
    addRegexToken('SS', match1to3, match2);
    addRegexToken('SSS', match1to3, match3);

    var token;
    for (token = 'SSSS'; token.length <= 9; token += 'S') {
        addRegexToken(token, matchUnsigned);
    }

    function parseMs(input, array) {
        array[MILLISECOND] = toInt(('0.' + input) * 1000);
    }

    for (token = 'S'; token.length <= 9; token += 'S') {
        addParseToken(token, parseMs);
    }
    // MOMENTS

    var getSetMillisecond = makeGetSet('Milliseconds', false);

    // FORMATTING

    addFormatToken('z', 0, 0, 'zoneAbbr');
    addFormatToken('zz', 0, 0, 'zoneName');

    // MOMENTS

    function getZoneAbbr() {
        return this._isUTC ? 'UTC' : '';
    }

    function getZoneName() {
        return this._isUTC ? 'Coordinated Universal Time' : '';
    }

    var momentPrototype__proto = Moment.prototype;

    momentPrototype__proto.add = add_subtract__add;
    momentPrototype__proto.calendar = moment_calendar__calendar;
    momentPrototype__proto.clone = clone;
    momentPrototype__proto.diff = diff;
    momentPrototype__proto.endOf = endOf;
    momentPrototype__proto.format = format;
    momentPrototype__proto.from = from;
    momentPrototype__proto.fromNow = fromNow;
    momentPrototype__proto.to = to;
    momentPrototype__proto.toNow = toNow;
    momentPrototype__proto.get = stringGet;
    momentPrototype__proto.invalidAt = invalidAt;
    momentPrototype__proto.isAfter = isAfter;
    momentPrototype__proto.isBefore = isBefore;
    momentPrototype__proto.isBetween = isBetween;
    momentPrototype__proto.isSame = isSame;
    momentPrototype__proto.isSameOrAfter = isSameOrAfter;
    momentPrototype__proto.isSameOrBefore = isSameOrBefore;
    momentPrototype__proto.isValid = moment_valid__isValid;
    momentPrototype__proto.lang = lang;
    momentPrototype__proto.locale = locale;
    momentPrototype__proto.localeData = localeData;
    momentPrototype__proto.max = prototypeMax;
    momentPrototype__proto.min = prototypeMin;
    momentPrototype__proto.parsingFlags = parsingFlags;
    momentPrototype__proto.set = stringSet;
    momentPrototype__proto.startOf = startOf;
    momentPrototype__proto.subtract = add_subtract__subtract;
    momentPrototype__proto.toArray = toArray;
    momentPrototype__proto.toObject = toObject;
    momentPrototype__proto.toDate = toDate;
    momentPrototype__proto.toISOString = moment_format__toISOString;
    momentPrototype__proto.toJSON = toJSON;
    momentPrototype__proto.toString = toString;
    momentPrototype__proto.unix = unix;
    momentPrototype__proto.valueOf = to_type__valueOf;
    momentPrototype__proto.creationData = creationData;

    // Year
    momentPrototype__proto.year = getSetYear;
    momentPrototype__proto.isLeapYear = getIsLeapYear;

    // Week Year
    momentPrototype__proto.weekYear = getSetWeekYear;
    momentPrototype__proto.isoWeekYear = getSetISOWeekYear;

    // Quarter
    momentPrototype__proto.quarter = momentPrototype__proto.quarters = getSetQuarter;

    // Month
    momentPrototype__proto.month = getSetMonth;
    momentPrototype__proto.daysInMonth = getDaysInMonth;

    // Week
    momentPrototype__proto.week = momentPrototype__proto.weeks = getSetWeek;
    momentPrototype__proto.isoWeek = momentPrototype__proto.isoWeeks = getSetISOWeek;
    momentPrototype__proto.weeksInYear = getWeeksInYear;
    momentPrototype__proto.isoWeeksInYear = getISOWeeksInYear;

    // Day
    momentPrototype__proto.date = getSetDayOfMonth;
    momentPrototype__proto.day = momentPrototype__proto.days = getSetDayOfWeek;
    momentPrototype__proto.weekday = getSetLocaleDayOfWeek;
    momentPrototype__proto.isoWeekday = getSetISODayOfWeek;
    momentPrototype__proto.dayOfYear = getSetDayOfYear;

    // Hour
    momentPrototype__proto.hour = momentPrototype__proto.hours = getSetHour;

    // Minute
    momentPrototype__proto.minute = momentPrototype__proto.minutes = getSetMinute;

    // Second
    momentPrototype__proto.second = momentPrototype__proto.seconds = getSetSecond;

    // Millisecond
    momentPrototype__proto.millisecond = momentPrototype__proto.milliseconds = getSetMillisecond;

    // Offset
    momentPrototype__proto.utcOffset = getSetOffset;
    momentPrototype__proto.utc = setOffsetToUTC;
    momentPrototype__proto.local = setOffsetToLocal;
    momentPrototype__proto.parseZone = setOffsetToParsedOffset;
    momentPrototype__proto.hasAlignedHourOffset = hasAlignedHourOffset;
    momentPrototype__proto.isDST = isDaylightSavingTime;
    momentPrototype__proto.isLocal = isLocal;
    momentPrototype__proto.isUtcOffset = isUtcOffset;
    momentPrototype__proto.isUtc = isUtc;
    momentPrototype__proto.isUTC = isUtc;

    // Timezone
    momentPrototype__proto.zoneAbbr = getZoneAbbr;
    momentPrototype__proto.zoneName = getZoneName;

    // Deprecations
    momentPrototype__proto.dates = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
    momentPrototype__proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
    momentPrototype__proto.years = deprecate('years accessor is deprecated. Use year instead', getSetYear);
    momentPrototype__proto.zone = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
    momentPrototype__proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

    var momentPrototype = momentPrototype__proto;

    function moment__createUnix(input) {
        return local__createLocal(input * 1000);
    }

    function moment__createInZone() {
        return local__createLocal.apply(null, arguments).parseZone();
    }

    function preParsePostFormat(string) {
        return string;
    }

    var prototype__proto = Locale.prototype;

    prototype__proto.calendar = locale_calendar__calendar;
    prototype__proto.longDateFormat = longDateFormat;
    prototype__proto.invalidDate = invalidDate;
    prototype__proto.ordinal = ordinal;
    prototype__proto.preparse = preParsePostFormat;
    prototype__proto.postformat = preParsePostFormat;
    prototype__proto.relativeTime = relative__relativeTime;
    prototype__proto.pastFuture = pastFuture;
    prototype__proto.set = locale_set__set;

    // Month
    prototype__proto.months = localeMonths;
    prototype__proto.monthsShort = localeMonthsShort;
    prototype__proto.monthsParse = localeMonthsParse;
    prototype__proto.monthsRegex = monthsRegex;
    prototype__proto.monthsShortRegex = monthsShortRegex;

    // Week
    prototype__proto.week = localeWeek;
    prototype__proto.firstDayOfYear = localeFirstDayOfYear;
    prototype__proto.firstDayOfWeek = localeFirstDayOfWeek;

    // Day of Week
    prototype__proto.weekdays = localeWeekdays;
    prototype__proto.weekdaysMin = localeWeekdaysMin;
    prototype__proto.weekdaysShort = localeWeekdaysShort;
    prototype__proto.weekdaysParse = localeWeekdaysParse;

    prototype__proto.weekdaysRegex = weekdaysRegex;
    prototype__proto.weekdaysShortRegex = weekdaysShortRegex;
    prototype__proto.weekdaysMinRegex = weekdaysMinRegex;

    // Hours
    prototype__proto.isPM = localeIsPM;
    prototype__proto.meridiem = localeMeridiem;

    function lists__get(format, index, field, setter) {
        var locale = locale_locales__getLocale();
        var utc = create_utc__createUTC().set(setter, index);
        return locale[field](utc, format);
    }

    function listMonthsImpl(format, index, field) {
        if (typeof format === 'number') {
            index = format;
            format = undefined;
        }

        format = format || '';

        if (index != null) {
            return lists__get(format, index, field, 'month');
        }

        var i;
        var out = [];
        for (i = 0; i < 12; i++) {
            out[i] = lists__get(format, i, field, 'month');
        }
        return out;
    }

    // ()
    // (5)
    // (fmt, 5)
    // (fmt)
    // (true)
    // (true, 5)
    // (true, fmt, 5)
    // (true, fmt)
    function listWeekdaysImpl(localeSorted, format, index, field) {
        if (typeof localeSorted === 'boolean') {
            if (typeof format === 'number') {
                index = format;
                format = undefined;
            }

            format = format || '';
        } else {
            format = localeSorted;
            index = format;
            localeSorted = false;

            if (typeof format === 'number') {
                index = format;
                format = undefined;
            }

            format = format || '';
        }

        var locale = locale_locales__getLocale(),
            shift = localeSorted ? locale._week.dow : 0;

        if (index != null) {
            return lists__get(format, (index + shift) % 7, field, 'day');
        }

        var i;
        var out = [];
        for (i = 0; i < 7; i++) {
            out[i] = lists__get(format, (i + shift) % 7, field, 'day');
        }
        return out;
    }

    function lists__listMonths(format, index) {
        return listMonthsImpl(format, index, 'months');
    }

    function lists__listMonthsShort(format, index) {
        return listMonthsImpl(format, index, 'monthsShort');
    }

    function lists__listWeekdays(localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
    }

    function lists__listWeekdaysShort(localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
    }

    function lists__listWeekdaysMin(localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
    }

    locale_locales__getSetGlobalLocale('en', {
        ordinalParse: /\d{1,2}(th|st|nd|rd)/,
        ordinal: function (number) {
            var b = number % 10,
                output = (toInt(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
            return number + output;
        }
    });

    // Side effect imports
    utils_hooks__hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', locale_locales__getSetGlobalLocale);
    utils_hooks__hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', locale_locales__getLocale);

    var mathAbs = Math.abs;

    function duration_abs__abs() {
        var data = this._data;

        this._milliseconds = mathAbs(this._milliseconds);
        this._days = mathAbs(this._days);
        this._months = mathAbs(this._months);

        data.milliseconds = mathAbs(data.milliseconds);
        data.seconds = mathAbs(data.seconds);
        data.minutes = mathAbs(data.minutes);
        data.hours = mathAbs(data.hours);
        data.months = mathAbs(data.months);
        data.years = mathAbs(data.years);

        return this;
    }

    function duration_add_subtract__addSubtract(duration, input, value, direction) {
        var other = create__createDuration(input, value);

        duration._milliseconds += direction * other._milliseconds;
        duration._days += direction * other._days;
        duration._months += direction * other._months;

        return duration._bubble();
    }

    // supports only 2.0-style add(1, 's') or add(duration)
    function duration_add_subtract__add(input, value) {
        return duration_add_subtract__addSubtract(this, input, value, 1);
    }

    // supports only 2.0-style subtract(1, 's') or subtract(duration)
    function duration_add_subtract__subtract(input, value) {
        return duration_add_subtract__addSubtract(this, input, value, -1);
    }

    function absCeil(number) {
        if (number < 0) {
            return Math.floor(number);
        } else {
            return Math.ceil(number);
        }
    }

    function bubble() {
        var milliseconds = this._milliseconds;
        var days = this._days;
        var months = this._months;
        var data = this._data;
        var seconds, minutes, hours, years, monthsFromDays;

        // if we have a mix of positive and negative values, bubble down first
        // check: https://github.com/moment/moment/issues/2166
        if (!((milliseconds >= 0 && days >= 0 && months >= 0) ||
                (milliseconds <= 0 && days <= 0 && months <= 0))) {
            milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
            days = 0;
            months = 0;
        }

        // The following code bubbles up values, see the tests for
        // examples of what that means.
        data.milliseconds = milliseconds % 1000;

        seconds = absFloor(milliseconds / 1000);
        data.seconds = seconds % 60;

        minutes = absFloor(seconds / 60);
        data.minutes = minutes % 60;

        hours = absFloor(minutes / 60);
        data.hours = hours % 24;

        days += absFloor(hours / 24);

        // convert days to months
        monthsFromDays = absFloor(daysToMonths(days));
        months += monthsFromDays;
        days -= absCeil(monthsToDays(monthsFromDays));

        // 12 months -> 1 year
        years = absFloor(months / 12);
        months %= 12;

        data.days = days;
        data.months = months;
        data.years = years;

        return this;
    }

    function daysToMonths(days) {
        // 400 years have 146097 days (taking into account leap year rules)
        // 400 years have 12 months === 4800
        return days * 4800 / 146097;
    }

    function monthsToDays(months) {
        // the reverse of daysToMonths
        return months * 146097 / 4800;
    }

    function as(units) {
        var days;
        var months;
        var milliseconds = this._milliseconds;

        units = normalizeUnits(units);

        if (units === 'month' || units === 'year') {
            days = this._days + milliseconds / 864e5;
            months = this._months + daysToMonths(days);
            return units === 'month' ? months : months / 12;
        } else {
            // handle milliseconds separately because of floating point math errors (issue #1867)
            days = this._days + Math.round(monthsToDays(this._months));
            switch (units) {
                case 'week': return days / 7 + milliseconds / 6048e5;
                case 'day': return days + milliseconds / 864e5;
                case 'hour': return days * 24 + milliseconds / 36e5;
                case 'minute': return days * 1440 + milliseconds / 6e4;
                case 'second': return days * 86400 + milliseconds / 1000;
                    // Math.floor prevents floating point math errors here
                case 'millisecond': return Math.floor(days * 864e5) + milliseconds;
                default: throw new Error('Unknown unit ' + units);
            }
        }
    }

    // TODO: Use this.as('ms')?
    function duration_as__valueOf() {
        return (
            this._milliseconds +
            this._days * 864e5 +
            (this._months % 12) * 2592e6 +
            toInt(this._months / 12) * 31536e6
        );
    }

    function makeAs(alias) {
        return function () {
            return this.as(alias);
        };
    }

    var asMilliseconds = makeAs('ms');
    var asSeconds = makeAs('s');
    var asMinutes = makeAs('m');
    var asHours = makeAs('h');
    var asDays = makeAs('d');
    var asWeeks = makeAs('w');
    var asMonths = makeAs('M');
    var asYears = makeAs('y');

    function duration_get__get(units) {
        units = normalizeUnits(units);
        return this[units + 's']();
    }

    function makeGetter(name) {
        return function () {
            return this._data[name];
        };
    }

    var milliseconds = makeGetter('milliseconds');
    var seconds = makeGetter('seconds');
    var minutes = makeGetter('minutes');
    var hours = makeGetter('hours');
    var days = makeGetter('days');
    var months = makeGetter('months');
    var years = makeGetter('years');

    function weeks() {
        return absFloor(this.days() / 7);
    }

    var round = Math.round;
    var thresholds = {
        s: 45,  // seconds to minute
        m: 45,  // minutes to hour
        h: 22,  // hours to day
        d: 26,  // days to month
        M: 11   // months to year
    };

    // helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
    function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
        return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
    }

    function duration_humanize__relativeTime(posNegDuration, withoutSuffix, locale) {
        var duration = create__createDuration(posNegDuration).abs();
        var seconds = round(duration.as('s'));
        var minutes = round(duration.as('m'));
        var hours = round(duration.as('h'));
        var days = round(duration.as('d'));
        var months = round(duration.as('M'));
        var years = round(duration.as('y'));

        var a = seconds < thresholds.s && ['s', seconds] ||
                minutes <= 1 && ['m'] ||
                minutes < thresholds.m && ['mm', minutes] ||
                hours <= 1 && ['h'] ||
                hours < thresholds.h && ['hh', hours] ||
                days <= 1 && ['d'] ||
                days < thresholds.d && ['dd', days] ||
                months <= 1 && ['M'] ||
                months < thresholds.M && ['MM', months] ||
                years <= 1 && ['y'] || ['yy', years];

        a[2] = withoutSuffix;
        a[3] = +posNegDuration > 0;
        a[4] = locale;
        return substituteTimeAgo.apply(null, a);
    }

    // This function allows you to set the rounding function for relative time strings
    function duration_humanize__getSetRelativeTimeRounding(roundingFunction) {
        if (roundingFunction === undefined) {
            return round;
        }
        if (typeof (roundingFunction) === 'function') {
            round = roundingFunction;
            return true;
        }
        return false;
    }

    // This function allows you to set a threshold for relative time strings
    function duration_humanize__getSetRelativeTimeThreshold(threshold, limit) {
        if (thresholds[threshold] === undefined) {
            return false;
        }
        if (limit === undefined) {
            return thresholds[threshold];
        }
        thresholds[threshold] = limit;
        return true;
    }

    function humanize(withSuffix) {
        var locale = this.localeData();
        var output = duration_humanize__relativeTime(this, !withSuffix, locale);

        if (withSuffix) {
            output = locale.pastFuture(+this, output);
        }

        return locale.postformat(output);
    }

    var iso_string__abs = Math.abs;

    function iso_string__toISOString() {
        // for ISO strings we do not use the normal bubbling rules:
        //  * milliseconds bubble up until they become hours
        //  * days do not bubble at all
        //  * months bubble up until they become years
        // This is because there is no context-free conversion between hours and days
        // (think of clock changes)
        // and also not between days and months (28-31 days per month)
        var seconds = iso_string__abs(this._milliseconds) / 1000;
        var days = iso_string__abs(this._days);
        var months = iso_string__abs(this._months);
        var minutes, hours, years;

        // 3600 seconds -> 60 minutes -> 1 hour
        minutes = absFloor(seconds / 60);
        hours = absFloor(minutes / 60);
        seconds %= 60;
        minutes %= 60;

        // 12 months -> 1 year
        years = absFloor(months / 12);
        months %= 12;


        // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
        var Y = years;
        var M = months;
        var D = days;
        var h = hours;
        var m = minutes;
        var s = seconds;
        var total = this.asSeconds();

        if (!total) {
            // this is the same as C#'s (Noda) and python (isodate)...
            // but not other JS (goog.date)
            return 'P0D';
        }

        return (total < 0 ? '-' : '') +
            'P' +
            (Y ? Y + 'Y' : '') +
            (M ? M + 'M' : '') +
            (D ? D + 'D' : '') +
            ((h || m || s) ? 'T' : '') +
            (h ? h + 'H' : '') +
            (m ? m + 'M' : '') +
            (s ? s + 'S' : '');
    }

    var duration_prototype__proto = Duration.prototype;

    duration_prototype__proto.abs = duration_abs__abs;
    duration_prototype__proto.add = duration_add_subtract__add;
    duration_prototype__proto.subtract = duration_add_subtract__subtract;
    duration_prototype__proto.as = as;
    duration_prototype__proto.asMilliseconds = asMilliseconds;
    duration_prototype__proto.asSeconds = asSeconds;
    duration_prototype__proto.asMinutes = asMinutes;
    duration_prototype__proto.asHours = asHours;
    duration_prototype__proto.asDays = asDays;
    duration_prototype__proto.asWeeks = asWeeks;
    duration_prototype__proto.asMonths = asMonths;
    duration_prototype__proto.asYears = asYears;
    duration_prototype__proto.valueOf = duration_as__valueOf;
    duration_prototype__proto._bubble = bubble;
    duration_prototype__proto.get = duration_get__get;
    duration_prototype__proto.milliseconds = milliseconds;
    duration_prototype__proto.seconds = seconds;
    duration_prototype__proto.minutes = minutes;
    duration_prototype__proto.hours = hours;
    duration_prototype__proto.days = days;
    duration_prototype__proto.weeks = weeks;
    duration_prototype__proto.months = months;
    duration_prototype__proto.years = years;
    duration_prototype__proto.humanize = humanize;
    duration_prototype__proto.toISOString = iso_string__toISOString;
    duration_prototype__proto.toString = iso_string__toISOString;
    duration_prototype__proto.toJSON = iso_string__toISOString;
    duration_prototype__proto.locale = locale;
    duration_prototype__proto.localeData = localeData;

    // Deprecations
    duration_prototype__proto.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', iso_string__toISOString);
    duration_prototype__proto.lang = lang;

    // Side effect imports

    // FORMATTING

    addFormatToken('X', 0, 0, 'unix');
    addFormatToken('x', 0, 0, 'valueOf');

    // PARSING

    addRegexToken('x', matchSigned);
    addRegexToken('X', matchTimestamp);
    addParseToken('X', function (input, array, config) {
        config._d = new Date(parseFloat(input, 10) * 1000);
    });
    addParseToken('x', function (input, array, config) {
        config._d = new Date(toInt(input));
    });

    // Side effect imports


    utils_hooks__hooks.version = '2.14.1';

    setHookCallback(local__createLocal);

    utils_hooks__hooks.fn = momentPrototype;
    utils_hooks__hooks.min = min;
    utils_hooks__hooks.max = max;
    utils_hooks__hooks.now = now;
    utils_hooks__hooks.utc = create_utc__createUTC;
    utils_hooks__hooks.unix = moment__createUnix;
    utils_hooks__hooks.months = lists__listMonths;
    utils_hooks__hooks.isDate = isDate;
    utils_hooks__hooks.locale = locale_locales__getSetGlobalLocale;
    utils_hooks__hooks.invalid = valid__createInvalid;
    utils_hooks__hooks.duration = create__createDuration;
    utils_hooks__hooks.isMoment = isMoment;
    utils_hooks__hooks.weekdays = lists__listWeekdays;
    utils_hooks__hooks.parseZone = moment__createInZone;
    utils_hooks__hooks.localeData = locale_locales__getLocale;
    utils_hooks__hooks.isDuration = isDuration;
    utils_hooks__hooks.monthsShort = lists__listMonthsShort;
    utils_hooks__hooks.weekdaysMin = lists__listWeekdaysMin;
    utils_hooks__hooks.defineLocale = defineLocale;
    utils_hooks__hooks.updateLocale = updateLocale;
    utils_hooks__hooks.locales = locale_locales__listLocales;
    utils_hooks__hooks.weekdaysShort = lists__listWeekdaysShort;
    utils_hooks__hooks.normalizeUnits = normalizeUnits;
    utils_hooks__hooks.relativeTimeRounding = duration_humanize__getSetRelativeTimeRounding;
    utils_hooks__hooks.relativeTimeThreshold = duration_humanize__getSetRelativeTimeThreshold;
    utils_hooks__hooks.calendarFormat = getCalendarFormat;
    utils_hooks__hooks.prototype = momentPrototype;

    var _moment = utils_hooks__hooks;

    return _moment;

}));
//! moment.js
//! version : 2.14.1
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com
!function (a, b) { "object" == typeof exports && "undefined" != typeof module ? module.exports = b() : "function" == typeof define && define.amd ? define(b) : a.moment = b() }(this, function () {
    "use strict"; function a() { return md.apply(null, arguments) }
    // This is done to register the method called with moment()
    // without creating circular dependencies.
    function b(a) { md = a } function c(a) { return a instanceof Array || "[object Array]" === Object.prototype.toString.call(a) } function d(a) { return "[object Object]" === Object.prototype.toString.call(a) } function e(a) {
        var b; for (b in a)
            // even if its not own property I'd still call it non-empty
            return !1; return !0
    } function f(a) { return a instanceof Date || "[object Date]" === Object.prototype.toString.call(a) } function g(a, b) { var c, d = []; for (c = 0; c < a.length; ++c) d.push(b(a[c], c)); return d } function h(a, b) { return Object.prototype.hasOwnProperty.call(a, b) } function i(a, b) { for (var c in b) h(b, c) && (a[c] = b[c]); return h(b, "toString") && (a.toString = b.toString), h(b, "valueOf") && (a.valueOf = b.valueOf), a } function j(a, b, c, d) { return qb(a, b, c, d, !0).utc() } function k() {
        // We need to deep clone this object.
        return { empty: !1, unusedTokens: [], unusedInput: [], overflow: -2, charsLeftOver: 0, nullInput: !1, invalidMonth: null, invalidFormat: !1, userInvalidated: !1, iso: !1, parsedDateParts: [], meridiem: null }
    } function l(a) { return null == a._pf && (a._pf = k()), a._pf } function m(a) { if (null == a._isValid) { var b = l(a), c = nd.call(b.parsedDateParts, function (a) { return null != a }); a._isValid = !isNaN(a._d.getTime()) && b.overflow < 0 && !b.empty && !b.invalidMonth && !b.invalidWeekday && !b.nullInput && !b.invalidFormat && !b.userInvalidated && (!b.meridiem || b.meridiem && c), a._strict && (a._isValid = a._isValid && 0 === b.charsLeftOver && 0 === b.unusedTokens.length && void 0 === b.bigHour) } return a._isValid } function n(a) { var b = j(NaN); return null != a ? i(l(b), a) : l(b).userInvalidated = !0, b } function o(a) { return void 0 === a } function p(a, b) { var c, d, e; if (o(b._isAMomentObject) || (a._isAMomentObject = b._isAMomentObject), o(b._i) || (a._i = b._i), o(b._f) || (a._f = b._f), o(b._l) || (a._l = b._l), o(b._strict) || (a._strict = b._strict), o(b._tzm) || (a._tzm = b._tzm), o(b._isUTC) || (a._isUTC = b._isUTC), o(b._offset) || (a._offset = b._offset), o(b._pf) || (a._pf = l(b)), o(b._locale) || (a._locale = b._locale), od.length > 0) for (c in od) d = od[c], e = b[d], o(e) || (a[d] = e); return a }
    // Moment prototype object
    function q(b) { p(this, b), this._d = new Date(null != b._d ? b._d.getTime() : NaN), pd === !1 && (pd = !0, a.updateOffset(this), pd = !1) } function r(a) { return a instanceof q || null != a && null != a._isAMomentObject } function s(a) { return 0 > a ? Math.ceil(a) || 0 : Math.floor(a) } function t(a) { var b = +a, c = 0; return 0 !== b && isFinite(b) && (c = s(b)), c }
    // compare two arrays, return the number of differences
    function u(a, b, c) { var d, e = Math.min(a.length, b.length), f = Math.abs(a.length - b.length), g = 0; for (d = 0; e > d; d++) (c && a[d] !== b[d] || !c && t(a[d]) !== t(b[d])) && g++; return g + f } function v(b) { a.suppressDeprecationWarnings === !1 && "undefined" != typeof console && console.warn && console.warn("Deprecation warning: " + b) } function w(b, c) { var d = !0; return i(function () { return null != a.deprecationHandler && a.deprecationHandler(null, b), d && (v(b + "\nArguments: " + Array.prototype.slice.call(arguments).join(", ") + "\n" + (new Error).stack), d = !1), c.apply(this, arguments) }, c) } function x(b, c) { null != a.deprecationHandler && a.deprecationHandler(b, c), qd[b] || (v(c), qd[b] = !0) } function y(a) { return a instanceof Function || "[object Function]" === Object.prototype.toString.call(a) } function z(a) {
        var b, c; for (c in a) b = a[c], y(b) ? this[c] = b : this["_" + c] = b; this._config = a,
        // Lenient ordinal parsing accepts just a number in addition to
        // number + (possibly) stuff coming from _ordinalParseLenient.
        this._ordinalParseLenient = new RegExp(this._ordinalParse.source + "|" + /\d{1,2}/.source)
    } function A(a, b) {
        var c, e = i({}, a); for (c in b) h(b, c) && (d(a[c]) && d(b[c]) ? (e[c] = {}, i(e[c], a[c]), i(e[c], b[c])) : null != b[c] ? e[c] = b[c] : delete e[c]); for (c in a) h(a, c) && !h(b, c) && d(a[c]) && (
        // make sure changes to properties don't modify parent config
        e[c] = i({}, e[c])); return e
    } function B(a) { null != a && this.set(a) } function C(a, b, c) { var d = this._calendar[a] || this._calendar.sameElse; return y(d) ? d.call(b, c) : d } function D(a) { var b = this._longDateFormat[a], c = this._longDateFormat[a.toUpperCase()]; return b || !c ? b : (this._longDateFormat[a] = c.replace(/MMMM|MM|DD|dddd/g, function (a) { return a.slice(1) }), this._longDateFormat[a]) } function E() { return this._invalidDate } function F(a) { return this._ordinal.replace("%d", a) } function G(a, b, c, d) { var e = this._relativeTime[c]; return y(e) ? e(a, b, c, d) : e.replace(/%d/i, a) } function H(a, b) { var c = this._relativeTime[a > 0 ? "future" : "past"]; return y(c) ? c(b) : c.replace(/%s/i, b) } function I(a, b) { var c = a.toLowerCase(); zd[c] = zd[c + "s"] = zd[b] = a } function J(a) { return "string" == typeof a ? zd[a] || zd[a.toLowerCase()] : void 0 } function K(a) { var b, c, d = {}; for (c in a) h(a, c) && (b = J(c), b && (d[b] = a[c])); return d } function L(a, b) { Ad[a] = b } function M(a) { var b = []; for (var c in a) b.push({ unit: c, priority: Ad[c] }); return b.sort(function (a, b) { return a.priority - b.priority }), b } function N(b, c) { return function (d) { return null != d ? (P(this, b, d), a.updateOffset(this, c), this) : O(this, b) } } function O(a, b) { return a.isValid() ? a._d["get" + (a._isUTC ? "UTC" : "") + b]() : NaN } function P(a, b, c) { a.isValid() && a._d["set" + (a._isUTC ? "UTC" : "") + b](c) }
    // MOMENTS
    function Q(a) { return a = J(a), y(this[a]) ? this[a]() : this } function R(a, b) { if ("object" == typeof a) { a = K(a); for (var c = M(a), d = 0; d < c.length; d++) this[c[d].unit](a[c[d].unit]) } else if (a = J(a), y(this[a])) return this[a](b); return this } function S(a, b, c) { var d = "" + Math.abs(a), e = b - d.length, f = a >= 0; return (f ? c ? "+" : "" : "-") + Math.pow(10, Math.max(0, e)).toString().substr(1) + d }
    // token:    'M'
    // padded:   ['MM', 2]
    // ordinal:  'Mo'
    // callback: function () { this.month() + 1 }
    function T(a, b, c, d) { var e = d; "string" == typeof d && (e = function () { return this[d]() }), a && (Ed[a] = e), b && (Ed[b[0]] = function () { return S(e.apply(this, arguments), b[1], b[2]) }), c && (Ed[c] = function () { return this.localeData().ordinal(e.apply(this, arguments), a) }) } function U(a) { return a.match(/\[[\s\S]/) ? a.replace(/^\[|\]$/g, "") : a.replace(/\\/g, "") } function V(a) { var b, c, d = a.match(Bd); for (b = 0, c = d.length; c > b; b++) Ed[d[b]] ? d[b] = Ed[d[b]] : d[b] = U(d[b]); return function (b) { var e, f = ""; for (e = 0; c > e; e++) f += d[e] instanceof Function ? d[e].call(b, a) : d[e]; return f } }
    // format date using native date object
    function W(a, b) { return a.isValid() ? (b = X(b, a.localeData()), Dd[b] = Dd[b] || V(b), Dd[b](a)) : a.localeData().invalidDate() } function X(a, b) { function c(a) { return b.longDateFormat(a) || a } var d = 5; for (Cd.lastIndex = 0; d >= 0 && Cd.test(a) ;) a = a.replace(Cd, c), Cd.lastIndex = 0, d -= 1; return a } function Y(a, b, c) { Wd[a] = y(b) ? b : function (a, d) { return a && c ? c : b } } function Z(a, b) { return h(Wd, a) ? Wd[a](b._strict, b._locale) : new RegExp($(a)) }
    // Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
    function $(a) { return _(a.replace("\\", "").replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (a, b, c, d, e) { return b || c || d || e })) } function _(a) { return a.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&") } function aa(a, b) { var c, d = b; for ("string" == typeof a && (a = [a]), "number" == typeof b && (d = function (a, c) { c[b] = t(a) }), c = 0; c < a.length; c++) Xd[a[c]] = d } function ba(a, b) { aa(a, function (a, c, d, e) { d._w = d._w || {}, b(a, d._w, d, e) }) } function ca(a, b, c) { null != b && h(Xd, a) && Xd[a](b, c._a, c, a) } function da(a, b) { return new Date(Date.UTC(a, b + 1, 0)).getUTCDate() } function ea(a, b) { return c(this._months) ? this._months[a.month()] : this._months[(this._months.isFormat || fe).test(b) ? "format" : "standalone"][a.month()] } function fa(a, b) { return c(this._monthsShort) ? this._monthsShort[a.month()] : this._monthsShort[fe.test(b) ? "format" : "standalone"][a.month()] } function ga(a, b, c) {
        var d, e, f, g = a.toLocaleLowerCase(); if (!this._monthsParse) for (
            // this is not used
        this._monthsParse = [], this._longMonthsParse = [], this._shortMonthsParse = [], d = 0; 12 > d; ++d) f = j([2e3, d]), this._shortMonthsParse[d] = this.monthsShort(f, "").toLocaleLowerCase(), this._longMonthsParse[d] = this.months(f, "").toLocaleLowerCase(); return c ? "MMM" === b ? (e = sd.call(this._shortMonthsParse, g), -1 !== e ? e : null) : (e = sd.call(this._longMonthsParse, g), -1 !== e ? e : null) : "MMM" === b ? (e = sd.call(this._shortMonthsParse, g), -1 !== e ? e : (e = sd.call(this._longMonthsParse, g), -1 !== e ? e : null)) : (e = sd.call(this._longMonthsParse, g), -1 !== e ? e : (e = sd.call(this._shortMonthsParse, g), -1 !== e ? e : null))
    } function ha(a, b, c) {
        var d, e, f; if (this._monthsParseExact) return ga.call(this, a, b, c);
        // TODO: add sorting
        // Sorting makes sure if one month (or abbr) is a prefix of another
        // see sorting in computeMonthsParse
        for (this._monthsParse || (this._monthsParse = [], this._longMonthsParse = [], this._shortMonthsParse = []), d = 0; 12 > d; d++) {
            // test the regex
            if (e = j([2e3, d]), c && !this._longMonthsParse[d] && (this._longMonthsParse[d] = new RegExp("^" + this.months(e, "").replace(".", "") + "$", "i"), this._shortMonthsParse[d] = new RegExp("^" + this.monthsShort(e, "").replace(".", "") + "$", "i")), c || this._monthsParse[d] || (f = "^" + this.months(e, "") + "|^" + this.monthsShort(e, ""), this._monthsParse[d] = new RegExp(f.replace(".", ""), "i")), c && "MMMM" === b && this._longMonthsParse[d].test(a)) return d; if (c && "MMM" === b && this._shortMonthsParse[d].test(a)) return d; if (!c && this._monthsParse[d].test(a)) return d
        }
    }
    // MOMENTS
    function ia(a, b) {
        var c; if (!a.isValid())
            // No op
            return a; if ("string" == typeof b) if (/^\d+$/.test(b)) b = t(b); else
                // TODO: Another silent failure?
                if (b = a.localeData().monthsParse(b), "number" != typeof b) return a; return c = Math.min(a.date(), da(a.year(), b)), a._d["set" + (a._isUTC ? "UTC" : "") + "Month"](b, c), a
    } function ja(b) { return null != b ? (ia(this, b), a.updateOffset(this, !0), this) : O(this, "Month") } function ka() { return da(this.year(), this.month()) } function la(a) { return this._monthsParseExact ? (h(this, "_monthsRegex") || na.call(this), a ? this._monthsShortStrictRegex : this._monthsShortRegex) : (h(this, "_monthsShortRegex") || (this._monthsShortRegex = ie), this._monthsShortStrictRegex && a ? this._monthsShortStrictRegex : this._monthsShortRegex) } function ma(a) { return this._monthsParseExact ? (h(this, "_monthsRegex") || na.call(this), a ? this._monthsStrictRegex : this._monthsRegex) : (h(this, "_monthsRegex") || (this._monthsRegex = je), this._monthsStrictRegex && a ? this._monthsStrictRegex : this._monthsRegex) } function na() {
        function a(a, b) { return b.length - a.length } var b, c, d = [], e = [], f = []; for (b = 0; 12 > b; b++) c = j([2e3, b]), d.push(this.monthsShort(c, "")), e.push(this.months(c, "")), f.push(this.months(c, "")), f.push(this.monthsShort(c, "")); for (
            // Sorting makes sure if one month (or abbr) is a prefix of another it
            // will match the longer piece.
        d.sort(a), e.sort(a), f.sort(a), b = 0; 12 > b; b++) d[b] = _(d[b]), e[b] = _(e[b]); for (b = 0; 24 > b; b++) f[b] = _(f[b]); this._monthsRegex = new RegExp("^(" + f.join("|") + ")", "i"), this._monthsShortRegex = this._monthsRegex, this._monthsStrictRegex = new RegExp("^(" + e.join("|") + ")", "i"), this._monthsShortStrictRegex = new RegExp("^(" + d.join("|") + ")", "i")
    }
    // HELPERS
    function oa(a) { return pa(a) ? 366 : 365 } function pa(a) { return a % 4 === 0 && a % 100 !== 0 || a % 400 === 0 } function qa() { return pa(this.year()) } function ra(a, b, c, d, e, f, g) {
        //can't just apply() to create a date:
        //http://stackoverflow.com/questions/181348/instantiating-a-javascript-object-by-calling-prototype-constructor-apply
        var h = new Date(a, b, c, d, e, f, g);
        //the date constructor remaps years 0-99 to 1900-1999
        return 100 > a && a >= 0 && isFinite(h.getFullYear()) && h.setFullYear(a), h
    } function sa(a) {
        var b = new Date(Date.UTC.apply(null, arguments));
        //the Date.UTC function remaps years 0-99 to 1900-1999
        return 100 > a && a >= 0 && isFinite(b.getUTCFullYear()) && b.setUTCFullYear(a), b
    }
    // start-of-first-week - start-of-year
    function ta(a, b, c) {
        var// first-week day -- which january is always in the first week (4 for iso, 1 for other)
        d = 7 + b - c,
        // first-week day local weekday -- which local weekday is fwd
        e = (7 + sa(a, 0, d).getUTCDay() - b) % 7; return -e + d - 1
    }
    //http://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
    function ua(a, b, c, d, e) { var f, g, h = (7 + c - d) % 7, i = ta(a, d, e), j = 1 + 7 * (b - 1) + h + i; return 0 >= j ? (f = a - 1, g = oa(f) + j) : j > oa(a) ? (f = a + 1, g = j - oa(a)) : (f = a, g = j), { year: f, dayOfYear: g } } function va(a, b, c) { var d, e, f = ta(a.year(), b, c), g = Math.floor((a.dayOfYear() - f - 1) / 7) + 1; return 1 > g ? (e = a.year() - 1, d = g + wa(e, b, c)) : g > wa(a.year(), b, c) ? (d = g - wa(a.year(), b, c), e = a.year() + 1) : (e = a.year(), d = g), { week: d, year: e } } function wa(a, b, c) { var d = ta(a, b, c), e = ta(a + 1, b, c); return (oa(a) - d + e) / 7 }
    // HELPERS
    // LOCALES
    function xa(a) { return va(a, this._week.dow, this._week.doy).week } function ya() { return this._week.dow } function za() { return this._week.doy }
    // MOMENTS
    function Aa(a) { var b = this.localeData().week(this); return null == a ? b : this.add(7 * (a - b), "d") } function Ba(a) { var b = va(this, 1, 4).week; return null == a ? b : this.add(7 * (a - b), "d") }
    // HELPERS
    function Ca(a, b) { return "string" != typeof a ? a : isNaN(a) ? (a = b.weekdaysParse(a), "number" == typeof a ? a : null) : parseInt(a, 10) } function Da(a, b) { return "string" == typeof a ? b.weekdaysParse(a) % 7 || 7 : isNaN(a) ? null : a } function Ea(a, b) { return c(this._weekdays) ? this._weekdays[a.day()] : this._weekdays[this._weekdays.isFormat.test(b) ? "format" : "standalone"][a.day()] } function Fa(a) { return this._weekdaysShort[a.day()] } function Ga(a) { return this._weekdaysMin[a.day()] } function Ha(a, b, c) { var d, e, f, g = a.toLocaleLowerCase(); if (!this._weekdaysParse) for (this._weekdaysParse = [], this._shortWeekdaysParse = [], this._minWeekdaysParse = [], d = 0; 7 > d; ++d) f = j([2e3, 1]).day(d), this._minWeekdaysParse[d] = this.weekdaysMin(f, "").toLocaleLowerCase(), this._shortWeekdaysParse[d] = this.weekdaysShort(f, "").toLocaleLowerCase(), this._weekdaysParse[d] = this.weekdays(f, "").toLocaleLowerCase(); return c ? "dddd" === b ? (e = sd.call(this._weekdaysParse, g), -1 !== e ? e : null) : "ddd" === b ? (e = sd.call(this._shortWeekdaysParse, g), -1 !== e ? e : null) : (e = sd.call(this._minWeekdaysParse, g), -1 !== e ? e : null) : "dddd" === b ? (e = sd.call(this._weekdaysParse, g), -1 !== e ? e : (e = sd.call(this._shortWeekdaysParse, g), -1 !== e ? e : (e = sd.call(this._minWeekdaysParse, g), -1 !== e ? e : null))) : "ddd" === b ? (e = sd.call(this._shortWeekdaysParse, g), -1 !== e ? e : (e = sd.call(this._weekdaysParse, g), -1 !== e ? e : (e = sd.call(this._minWeekdaysParse, g), -1 !== e ? e : null))) : (e = sd.call(this._minWeekdaysParse, g), -1 !== e ? e : (e = sd.call(this._weekdaysParse, g), -1 !== e ? e : (e = sd.call(this._shortWeekdaysParse, g), -1 !== e ? e : null))) } function Ia(a, b, c) {
        var d, e, f; if (this._weekdaysParseExact) return Ha.call(this, a, b, c); for (this._weekdaysParse || (this._weekdaysParse = [], this._minWeekdaysParse = [], this._shortWeekdaysParse = [], this._fullWeekdaysParse = []), d = 0; 7 > d; d++) {
            // test the regex
            if (e = j([2e3, 1]).day(d), c && !this._fullWeekdaysParse[d] && (this._fullWeekdaysParse[d] = new RegExp("^" + this.weekdays(e, "").replace(".", ".?") + "$", "i"), this._shortWeekdaysParse[d] = new RegExp("^" + this.weekdaysShort(e, "").replace(".", ".?") + "$", "i"), this._minWeekdaysParse[d] = new RegExp("^" + this.weekdaysMin(e, "").replace(".", ".?") + "$", "i")), this._weekdaysParse[d] || (f = "^" + this.weekdays(e, "") + "|^" + this.weekdaysShort(e, "") + "|^" + this.weekdaysMin(e, ""), this._weekdaysParse[d] = new RegExp(f.replace(".", ""), "i")), c && "dddd" === b && this._fullWeekdaysParse[d].test(a)) return d; if (c && "ddd" === b && this._shortWeekdaysParse[d].test(a)) return d; if (c && "dd" === b && this._minWeekdaysParse[d].test(a)) return d; if (!c && this._weekdaysParse[d].test(a)) return d
        }
    }
    // MOMENTS
    function Ja(a) { if (!this.isValid()) return null != a ? this : NaN; var b = this._isUTC ? this._d.getUTCDay() : this._d.getDay(); return null != a ? (a = Ca(a, this.localeData()), this.add(a - b, "d")) : b } function Ka(a) { if (!this.isValid()) return null != a ? this : NaN; var b = (this.day() + 7 - this.localeData()._week.dow) % 7; return null == a ? b : this.add(a - b, "d") } function La(a) {
        if (!this.isValid()) return null != a ? this : NaN;
        // behaves the same as moment#day except
        // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
        // as a setter, sunday should belong to the previous week.
        if (null != a) { var b = Da(a, this.localeData()); return this.day(this.day() % 7 ? b : b - 7) } return this.day() || 7
    } function Ma(a) { return this._weekdaysParseExact ? (h(this, "_weekdaysRegex") || Pa.call(this), a ? this._weekdaysStrictRegex : this._weekdaysRegex) : (h(this, "_weekdaysRegex") || (this._weekdaysRegex = pe), this._weekdaysStrictRegex && a ? this._weekdaysStrictRegex : this._weekdaysRegex) } function Na(a) { return this._weekdaysParseExact ? (h(this, "_weekdaysRegex") || Pa.call(this), a ? this._weekdaysShortStrictRegex : this._weekdaysShortRegex) : (h(this, "_weekdaysShortRegex") || (this._weekdaysShortRegex = qe), this._weekdaysShortStrictRegex && a ? this._weekdaysShortStrictRegex : this._weekdaysShortRegex) } function Oa(a) { return this._weekdaysParseExact ? (h(this, "_weekdaysRegex") || Pa.call(this), a ? this._weekdaysMinStrictRegex : this._weekdaysMinRegex) : (h(this, "_weekdaysMinRegex") || (this._weekdaysMinRegex = re), this._weekdaysMinStrictRegex && a ? this._weekdaysMinStrictRegex : this._weekdaysMinRegex) } function Pa() {
        function a(a, b) { return b.length - a.length } var b, c, d, e, f, g = [], h = [], i = [], k = []; for (b = 0; 7 > b; b++) c = j([2e3, 1]).day(b), d = this.weekdaysMin(c, ""), e = this.weekdaysShort(c, ""), f = this.weekdays(c, ""), g.push(d), h.push(e), i.push(f), k.push(d), k.push(e), k.push(f); for (
            // Sorting makes sure if one weekday (or abbr) is a prefix of another it
            // will match the longer piece.
        g.sort(a), h.sort(a), i.sort(a), k.sort(a), b = 0; 7 > b; b++) h[b] = _(h[b]), i[b] = _(i[b]), k[b] = _(k[b]); this._weekdaysRegex = new RegExp("^(" + k.join("|") + ")", "i"), this._weekdaysShortRegex = this._weekdaysRegex, this._weekdaysMinRegex = this._weekdaysRegex, this._weekdaysStrictRegex = new RegExp("^(" + i.join("|") + ")", "i"), this._weekdaysShortStrictRegex = new RegExp("^(" + h.join("|") + ")", "i"), this._weekdaysMinStrictRegex = new RegExp("^(" + g.join("|") + ")", "i")
    }
    // FORMATTING
    function Qa() { return this.hours() % 12 || 12 } function Ra() { return this.hours() || 24 } function Sa(a, b) { T(a, 0, 0, function () { return this.localeData().meridiem(this.hours(), this.minutes(), b) }) }
    // PARSING
    function Ta(a, b) { return b._meridiemParse }
    // LOCALES
    function Ua(a) {
        // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
        // Using charAt should be more compatible.
        return "p" === (a + "").toLowerCase().charAt(0)
    } function Va(a, b, c) { return a > 11 ? c ? "pm" : "PM" : c ? "am" : "AM" } function Wa(a) { return a ? a.toLowerCase().replace("_", "-") : a }
    // pick the locale from the array
    // try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
    // substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
    function Xa(a) {
        for (var b, c, d, e, f = 0; f < a.length;) {
            for (e = Wa(a[f]).split("-"), b = e.length, c = Wa(a[f + 1]), c = c ? c.split("-") : null; b > 0;) {
                if (d = Ya(e.slice(0, b).join("-"))) return d; if (c && c.length >= b && u(e, c, !0) >= b - 1)
                    //the next array item is better than a shallower substring of this one
                    break; b--
            } f++
        } return null
    } function Ya(a) {
        var b = null;
        // TODO: Find a better way to register and load all the locales in Node
        if (!we[a] && "undefined" != typeof module && module && module.exports) try {
            b = se._abbr, require("./locale/" + a),
            // because defineLocale currently also sets the global locale, we
            // want to undo that for lazy loaded locales
            Za(b)
        } catch (c) { } return we[a]
    }
    // This function will load locale and then set the global locale.  If
    // no arguments are passed in, it will simply return the current global
    // locale key.
    function Za(a, b) {
        var c;
        // moment.duration._locale = moment._locale = data;
        return a && (c = o(b) ? ab(a) : $a(a, b), c && (se = c)), se._abbr
    } function $a(a, b) {
        if (null !== b) {
            var c = ve;
            // treat as if there is no base config
            // backwards compat for now: also set the locale
            return b.abbr = a, null != we[a] ? (x("defineLocaleOverride", "use moment.updateLocale(localeName, config) to change an existing locale. moment.defineLocale(localeName, config) should only be used for creating a new locale See http://momentjs.com/guides/#/warnings/define-locale/ for more info."), c = we[a]._config) : null != b.parentLocale && (null != we[b.parentLocale] ? c = we[b.parentLocale]._config : x("parentLocaleUndefined", "specified parentLocale is not defined yet. See http://momentjs.com/guides/#/warnings/parent-locale/")), we[a] = new B(A(c, b)), Za(a), we[a]
        }
        // useful for testing
        return delete we[a], null
    } function _a(a, b) {
        if (null != b) {
            var c, d = ve;
            // MERGE
            null != we[a] && (d = we[a]._config), b = A(d, b), c = new B(b), c.parentLocale = we[a], we[a] = c,
            // backwards compat for now: also set the locale
            Za(a)
        } else
            // pass null for config to unupdate, useful for tests
            null != we[a] && (null != we[a].parentLocale ? we[a] = we[a].parentLocale : null != we[a] && delete we[a]); return we[a]
    }
    // returns locale data
    function ab(a) { var b; if (a && a._locale && a._locale._abbr && (a = a._locale._abbr), !a) return se; if (!c(a)) { if (b = Ya(a)) return b; a = [a] } return Xa(a) } function bb() { return rd(we) } function cb(a) { var b, c = a._a; return c && -2 === l(a).overflow && (b = c[Zd] < 0 || c[Zd] > 11 ? Zd : c[$d] < 1 || c[$d] > da(c[Yd], c[Zd]) ? $d : c[_d] < 0 || c[_d] > 24 || 24 === c[_d] && (0 !== c[ae] || 0 !== c[be] || 0 !== c[ce]) ? _d : c[ae] < 0 || c[ae] > 59 ? ae : c[be] < 0 || c[be] > 59 ? be : c[ce] < 0 || c[ce] > 999 ? ce : -1, l(a)._overflowDayOfYear && (Yd > b || b > $d) && (b = $d), l(a)._overflowWeeks && -1 === b && (b = de), l(a)._overflowWeekday && -1 === b && (b = ee), l(a).overflow = b), a }
    // date from iso format
    function db(a) {
        var b, c, d, e, f, g, h = a._i, i = xe.exec(h) || ye.exec(h); if (i) {
            for (l(a).iso = !0, b = 0, c = Ae.length; c > b; b++) if (Ae[b][1].exec(i[1])) { e = Ae[b][0], d = Ae[b][2] !== !1; break } if (null == e) return void (a._isValid = !1); if (i[3]) {
                for (b = 0, c = Be.length; c > b; b++) if (Be[b][1].exec(i[3])) {
                    // match[2] should be 'T' or space
                    f = (i[2] || " ") + Be[b][0]; break
                } if (null == f) return void (a._isValid = !1)
            } if (!d && null != f) return void (a._isValid = !1); if (i[4]) { if (!ze.exec(i[4])) return void (a._isValid = !1); g = "Z" } a._f = e + (f || "") + (g || ""), jb(a)
        } else a._isValid = !1
    }
    // date from iso format or fallback
    function eb(b) { var c = Ce.exec(b._i); return null !== c ? void (b._d = new Date(+c[1])) : (db(b), void (b._isValid === !1 && (delete b._isValid, a.createFromInputFallback(b)))) }
    // Pick the first defined of two or three arguments.
    function fb(a, b, c) { return null != a ? a : null != b ? b : c } function gb(b) {
        // hooks is actually the exported moment object
        var c = new Date(a.now()); return b._useUTC ? [c.getUTCFullYear(), c.getUTCMonth(), c.getUTCDate()] : [c.getFullYear(), c.getMonth(), c.getDate()]
    }
    // convert an array to a date.
    // the array should mirror the parameters below
    // note: all values past the year are optional and will default to the lowest possible value.
    // [year, month, day , hour, minute, second, millisecond]
    function hb(a) {
        var b, c, d, e, f = []; if (!a._d) {
            // Default to current date.
            // * if no year, month, day of month are given, default to today
            // * if day of month is given, default month and year
            // * if month is given, default only year
            // * if year is given, don't default anything
            for (d = gb(a), a._w && null == a._a[$d] && null == a._a[Zd] && ib(a), a._dayOfYear && (e = fb(a._a[Yd], d[Yd]), a._dayOfYear > oa(e) && (l(a)._overflowDayOfYear = !0), c = sa(e, 0, a._dayOfYear), a._a[Zd] = c.getUTCMonth(), a._a[$d] = c.getUTCDate()), b = 0; 3 > b && null == a._a[b]; ++b) a._a[b] = f[b] = d[b];
            // Zero out whatever was not defaulted, including time
            for (; 7 > b; b++) a._a[b] = f[b] = null == a._a[b] ? 2 === b ? 1 : 0 : a._a[b];
            // Check for 24:00:00.000
            24 === a._a[_d] && 0 === a._a[ae] && 0 === a._a[be] && 0 === a._a[ce] && (a._nextDay = !0, a._a[_d] = 0), a._d = (a._useUTC ? sa : ra).apply(null, f),
            // Apply timezone offset from input. The actual utcOffset can be changed
            // with parseZone.
            null != a._tzm && a._d.setUTCMinutes(a._d.getUTCMinutes() - a._tzm), a._nextDay && (a._a[_d] = 24)
        }
    } function ib(a) { var b, c, d, e, f, g, h, i; b = a._w, null != b.GG || null != b.W || null != b.E ? (f = 1, g = 4, c = fb(b.GG, a._a[Yd], va(rb(), 1, 4).year), d = fb(b.W, 1), e = fb(b.E, 1), (1 > e || e > 7) && (i = !0)) : (f = a._locale._week.dow, g = a._locale._week.doy, c = fb(b.gg, a._a[Yd], va(rb(), f, g).year), d = fb(b.w, 1), null != b.d ? (e = b.d, (0 > e || e > 6) && (i = !0)) : null != b.e ? (e = b.e + f, (b.e < 0 || b.e > 6) && (i = !0)) : e = f), 1 > d || d > wa(c, f, g) ? l(a)._overflowWeeks = !0 : null != i ? l(a)._overflowWeekday = !0 : (h = ua(c, d, e, f, g), a._a[Yd] = h.year, a._dayOfYear = h.dayOfYear) }
    // date from string and format string
    function jb(b) {
        // TODO: Move this to another part of the creation flow to prevent circular deps
        if (b._f === a.ISO_8601) return void db(b); b._a = [], l(b).empty = !0;
        // This array is used to make a Date, either with `new Date` or `Date.UTC`
        var c, d, e, f, g, h = "" + b._i, i = h.length, j = 0; for (e = X(b._f, b._locale).match(Bd) || [], c = 0; c < e.length; c++) f = e[c], d = (h.match(Z(f, b)) || [])[0], d && (g = h.substr(0, h.indexOf(d)), g.length > 0 && l(b).unusedInput.push(g), h = h.slice(h.indexOf(d) + d.length), j += d.length), Ed[f] ? (d ? l(b).empty = !1 : l(b).unusedTokens.push(f), ca(f, d, b)) : b._strict && !d && l(b).unusedTokens.push(f);
        // add remaining unparsed input length to the string
        l(b).charsLeftOver = i - j, h.length > 0 && l(b).unusedInput.push(h),
        // clear _12h flag if hour is <= 12
        b._a[_d] <= 12 && l(b).bigHour === !0 && b._a[_d] > 0 && (l(b).bigHour = void 0), l(b).parsedDateParts = b._a.slice(0), l(b).meridiem = b._meridiem,
        // handle meridiem
        b._a[_d] = kb(b._locale, b._a[_d], b._meridiem), hb(b), cb(b)
    } function kb(a, b, c) {
        var d;
        // Fallback
        return null == c ? b : null != a.meridiemHour ? a.meridiemHour(b, c) : null != a.isPM ? (d = a.isPM(c), d && 12 > b && (b += 12), d || 12 !== b || (b = 0), b) : b
    }
    // date from string and array of format strings
    function lb(a) { var b, c, d, e, f; if (0 === a._f.length) return l(a).invalidFormat = !0, void (a._d = new Date(NaN)); for (e = 0; e < a._f.length; e++) f = 0, b = p({}, a), null != a._useUTC && (b._useUTC = a._useUTC), b._f = a._f[e], jb(b), m(b) && (f += l(b).charsLeftOver, f += 10 * l(b).unusedTokens.length, l(b).score = f, (null == d || d > f) && (d = f, c = b)); i(a, c || b) } function mb(a) { if (!a._d) { var b = K(a._i); a._a = g([b.year, b.month, b.day || b.date, b.hour, b.minute, b.second, b.millisecond], function (a) { return a && parseInt(a, 10) }), hb(a) } } function nb(a) {
        var b = new q(cb(ob(a)));
        // Adding is smart enough around DST
        return b._nextDay && (b.add(1, "d"), b._nextDay = void 0), b
    } function ob(a) { var b = a._i, d = a._f; return a._locale = a._locale || ab(a._l), null === b || void 0 === d && "" === b ? n({ nullInput: !0 }) : ("string" == typeof b && (a._i = b = a._locale.preparse(b)), r(b) ? new q(cb(b)) : (c(d) ? lb(a) : f(b) ? a._d = b : d ? jb(a) : pb(a), m(a) || (a._d = null), a)) } function pb(b) {
        var d = b._i; void 0 === d ? b._d = new Date(a.now()) : f(d) ? b._d = new Date(d.valueOf()) : "string" == typeof d ? eb(b) : c(d) ? (b._a = g(d.slice(0), function (a) { return parseInt(a, 10) }), hb(b)) : "object" == typeof d ? mb(b) : "number" == typeof d ?
        // from milliseconds
        b._d = new Date(d) : a.createFromInputFallback(b)
    } function qb(a, b, f, g, h) {
        var i = {};
        // object construction must be done this way.
        // https://github.com/moment/moment/issues/1423
        return "boolean" == typeof f && (g = f, f = void 0), (d(a) && e(a) || c(a) && 0 === a.length) && (a = void 0), i._isAMomentObject = !0, i._useUTC = i._isUTC = h, i._l = f, i._i = a, i._f = b, i._strict = g, nb(i)
    } function rb(a, b, c, d) { return qb(a, b, c, d, !1) }
    // Pick a moment m from moments so that m[fn](other) is true for all
    // other. This relies on the function fn to be transitive.
    //
    // moments should either be an array of moment objects or an array, whose
    // first element is an array of moment objects.
    function sb(a, b) { var d, e; if (1 === b.length && c(b[0]) && (b = b[0]), !b.length) return rb(); for (d = b[0], e = 1; e < b.length; ++e) b[e].isValid() && !b[e][a](d) || (d = b[e]); return d }
    // TODO: Use [].sort instead?
    function tb() { var a = [].slice.call(arguments, 0); return sb("isBefore", a) } function ub() { var a = [].slice.call(arguments, 0); return sb("isAfter", a) } function vb(a) {
        var b = K(a), c = b.year || 0, d = b.quarter || 0, e = b.month || 0, f = b.week || 0, g = b.day || 0, h = b.hour || 0, i = b.minute || 0, j = b.second || 0, k = b.millisecond || 0;
        // representation for dateAddRemove
        this._milliseconds = +k + 1e3 * j +// 1000
        6e4 * i +// 1000 * 60
        1e3 * h * 60 * 60,//using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
        // Because of dateAddRemove treats 24 hours as different from a
        // day when working around DST, we need to store them separately
        this._days = +g + 7 * f,
        // It is impossible translate months into days without knowing
        // which months you are are talking about, so we have to store
        // it separately.
        this._months = +e + 3 * d + 12 * c, this._data = {}, this._locale = ab(), this._bubble()
    } function wb(a) { return a instanceof vb }
    // FORMATTING
    function xb(a, b) { T(a, 0, 0, function () { var a = this.utcOffset(), c = "+"; return 0 > a && (a = -a, c = "-"), c + S(~~(a / 60), 2) + b + S(~~a % 60, 2) }) } function yb(a, b) { var c = (b || "").match(a) || [], d = c[c.length - 1] || [], e = (d + "").match(Ge) || ["-", 0, 0], f = +(60 * e[1]) + t(e[2]); return "+" === e[0] ? f : -f }
    // Return a moment from input, that is local/utc/zone equivalent to model.
    function zb(b, c) {
        var d, e;
        // Use low-level api, because this fn is low-level api.
        return c._isUTC ? (d = c.clone(), e = (r(b) || f(b) ? b.valueOf() : rb(b).valueOf()) - d.valueOf(), d._d.setTime(d._d.valueOf() + e), a.updateOffset(d, !1), d) : rb(b).local()
    } function Ab(a) {
        // On Firefox.24 Date#getTimezoneOffset returns a floating point.
        // https://github.com/moment/moment/pull/1871
        return 15 * -Math.round(a._d.getTimezoneOffset() / 15)
    }
    // MOMENTS
    // keepLocalTime = true means only change the timezone, without
    // affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
    // 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
    // +0200, so we adjust the time as needed, to be valid.
    //
    // Keeping the time actually adds/subtracts (one hour)
    // from the actual represented time. That is why we call updateOffset
    // a second time. In case it wants us to change the offset again
    // _changeInProgress == true case, then we have to adjust, because
    // there is no such time in the given timezone.
    function Bb(b, c) { var d, e = this._offset || 0; return this.isValid() ? null != b ? ("string" == typeof b ? b = yb(Td, b) : Math.abs(b) < 16 && (b = 60 * b), !this._isUTC && c && (d = Ab(this)), this._offset = b, this._isUTC = !0, null != d && this.add(d, "m"), e !== b && (!c || this._changeInProgress ? Sb(this, Mb(b - e, "m"), 1, !1) : this._changeInProgress || (this._changeInProgress = !0, a.updateOffset(this, !0), this._changeInProgress = null)), this) : this._isUTC ? e : Ab(this) : null != b ? this : NaN } function Cb(a, b) { return null != a ? ("string" != typeof a && (a = -a), this.utcOffset(a, b), this) : -this.utcOffset() } function Db(a) { return this.utcOffset(0, a) } function Eb(a) { return this._isUTC && (this.utcOffset(0, a), this._isUTC = !1, a && this.subtract(Ab(this), "m")), this } function Fb() { return this._tzm ? this.utcOffset(this._tzm) : "string" == typeof this._i && this.utcOffset(yb(Sd, this._i)), this } function Gb(a) { return this.isValid() ? (a = a ? rb(a).utcOffset() : 0, (this.utcOffset() - a) % 60 === 0) : !1 } function Hb() { return this.utcOffset() > this.clone().month(0).utcOffset() || this.utcOffset() > this.clone().month(5).utcOffset() } function Ib() { if (!o(this._isDSTShifted)) return this._isDSTShifted; var a = {}; if (p(a, this), a = ob(a), a._a) { var b = a._isUTC ? j(a._a) : rb(a._a); this._isDSTShifted = this.isValid() && u(a._a, b.toArray()) > 0 } else this._isDSTShifted = !1; return this._isDSTShifted } function Jb() { return this.isValid() ? !this._isUTC : !1 } function Kb() { return this.isValid() ? this._isUTC : !1 } function Lb() { return this.isValid() ? this._isUTC && 0 === this._offset : !1 } function Mb(a, b) {
        var c, d, e, f = a,
        // matching against regexp is expensive, do it on demand
        g = null;// checks for null or undefined
        return wb(a) ? f = { ms: a._milliseconds, d: a._days, M: a._months } : "number" == typeof a ? (f = {}, b ? f[b] = a : f.milliseconds = a) : (g = He.exec(a)) ? (c = "-" === g[1] ? -1 : 1, f = { y: 0, d: t(g[$d]) * c, h: t(g[_d]) * c, m: t(g[ae]) * c, s: t(g[be]) * c, ms: t(g[ce]) * c }) : (g = Ie.exec(a)) ? (c = "-" === g[1] ? -1 : 1, f = { y: Nb(g[2], c), M: Nb(g[3], c), w: Nb(g[4], c), d: Nb(g[5], c), h: Nb(g[6], c), m: Nb(g[7], c), s: Nb(g[8], c) }) : null == f ? f = {} : "object" == typeof f && ("from" in f || "to" in f) && (e = Pb(rb(f.from), rb(f.to)), f = {}, f.ms = e.milliseconds, f.M = e.months), d = new vb(f), wb(a) && h(a, "_locale") && (d._locale = a._locale), d
    } function Nb(a, b) {
        // We'd normally use ~~inp for this, but unfortunately it also
        // converts floats to ints.
        // inp may be undefined, so careful calling replace on it.
        var c = a && parseFloat(a.replace(",", "."));
        // apply sign while we're at it
        return (isNaN(c) ? 0 : c) * b
    } function Ob(a, b) { var c = { milliseconds: 0, months: 0 }; return c.months = b.month() - a.month() + 12 * (b.year() - a.year()), a.clone().add(c.months, "M").isAfter(b) && --c.months, c.milliseconds = +b - +a.clone().add(c.months, "M"), c } function Pb(a, b) { var c; return a.isValid() && b.isValid() ? (b = zb(b, a), a.isBefore(b) ? c = Ob(a, b) : (c = Ob(b, a), c.milliseconds = -c.milliseconds, c.months = -c.months), c) : { milliseconds: 0, months: 0 } } function Qb(a) { return 0 > a ? -1 * Math.round(-1 * a) : Math.round(a) }
    // TODO: remove 'name' arg after deprecation is removed
    function Rb(a, b) {
        return function (c, d) {
            var e, f;
            //invert the arguments, but complain about it
            return null === d || isNaN(+d) || (x(b, "moment()." + b + "(period, number) is deprecated. Please use moment()." + b + "(number, period). See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info."), f = c, c = d, d = f), c = "string" == typeof c ? +c : c, e = Mb(c, d), Sb(this, e, a), this
        }
    } function Sb(b, c, d, e) { var f = c._milliseconds, g = Qb(c._days), h = Qb(c._months); b.isValid() && (e = null == e ? !0 : e, f && b._d.setTime(b._d.valueOf() + f * d), g && P(b, "Date", O(b, "Date") + g * d), h && ia(b, O(b, "Month") + h * d), e && a.updateOffset(b, g || h)) } function Tb(a, b) { var c = a.diff(b, "days", !0); return -6 > c ? "sameElse" : -1 > c ? "lastWeek" : 0 > c ? "lastDay" : 1 > c ? "sameDay" : 2 > c ? "nextDay" : 7 > c ? "nextWeek" : "sameElse" } function Ub(b, c) {
        // We want to compare the start of today, vs this.
        // Getting start-of-today depends on whether we're local/utc/offset or not.
        var d = b || rb(), e = zb(d, this).startOf("day"), f = a.calendarFormat(this, e) || "sameElse", g = c && (y(c[f]) ? c[f].call(this, d) : c[f]); return this.format(g || this.localeData().calendar(f, this, rb(d)))
    } function Vb() { return new q(this) } function Wb(a, b) { var c = r(a) ? a : rb(a); return this.isValid() && c.isValid() ? (b = J(o(b) ? "millisecond" : b), "millisecond" === b ? this.valueOf() > c.valueOf() : c.valueOf() < this.clone().startOf(b).valueOf()) : !1 } function Xb(a, b) { var c = r(a) ? a : rb(a); return this.isValid() && c.isValid() ? (b = J(o(b) ? "millisecond" : b), "millisecond" === b ? this.valueOf() < c.valueOf() : this.clone().endOf(b).valueOf() < c.valueOf()) : !1 } function Yb(a, b, c, d) { return d = d || "()", ("(" === d[0] ? this.isAfter(a, c) : !this.isBefore(a, c)) && (")" === d[1] ? this.isBefore(b, c) : !this.isAfter(b, c)) } function Zb(a, b) { var c, d = r(a) ? a : rb(a); return this.isValid() && d.isValid() ? (b = J(b || "millisecond"), "millisecond" === b ? this.valueOf() === d.valueOf() : (c = d.valueOf(), this.clone().startOf(b).valueOf() <= c && c <= this.clone().endOf(b).valueOf())) : !1 } function $b(a, b) { return this.isSame(a, b) || this.isAfter(a, b) } function _b(a, b) { return this.isSame(a, b) || this.isBefore(a, b) } function ac(a, b, c) {
        var d, e, f, g;// 1000
        // 1000 * 60
        // 1000 * 60 * 60
        // 1000 * 60 * 60 * 24, negate dst
        // 1000 * 60 * 60 * 24 * 7, negate dst
        return this.isValid() ? (d = zb(a, this), d.isValid() ? (e = 6e4 * (d.utcOffset() - this.utcOffset()), b = J(b), "year" === b || "month" === b || "quarter" === b ? (g = bc(this, d), "quarter" === b ? g /= 3 : "year" === b && (g /= 12)) : (f = this - d, g = "second" === b ? f / 1e3 : "minute" === b ? f / 6e4 : "hour" === b ? f / 36e5 : "day" === b ? (f - e) / 864e5 : "week" === b ? (f - e) / 6048e5 : f), c ? g : s(g)) : NaN) : NaN
    } function bc(a, b) {
        // difference in months
        var c, d, e = 12 * (b.year() - a.year()) + (b.month() - a.month()),
        // b is in (anchor - 1 month, anchor + 1 month)
        f = a.clone().add(e, "months");
        //check for negative zero, return zero if negative zero
        // linear across the month
        // linear across the month
        return 0 > b - f ? (c = a.clone().add(e - 1, "months"), d = (b - f) / (f - c)) : (c = a.clone().add(e + 1, "months"), d = (b - f) / (c - f)), -(e + d) || 0
    } function cc() { return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ") } function dc() { var a = this.clone().utc(); return 0 < a.year() && a.year() <= 9999 ? y(Date.prototype.toISOString) ? this.toDate().toISOString() : W(a, "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]") : W(a, "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]") } function ec(b) { b || (b = this.isUtc() ? a.defaultFormatUtc : a.defaultFormat); var c = W(this, b); return this.localeData().postformat(c) } function fc(a, b) { return this.isValid() && (r(a) && a.isValid() || rb(a).isValid()) ? Mb({ to: this, from: a }).locale(this.locale()).humanize(!b) : this.localeData().invalidDate() } function gc(a) { return this.from(rb(), a) } function hc(a, b) { return this.isValid() && (r(a) && a.isValid() || rb(a).isValid()) ? Mb({ from: this, to: a }).locale(this.locale()).humanize(!b) : this.localeData().invalidDate() } function ic(a) { return this.to(rb(), a) }
    // If passed a locale key, it will set the locale for this
    // instance.  Otherwise, it will return the locale configuration
    // variables for this instance.
    function jc(a) { var b; return void 0 === a ? this._locale._abbr : (b = ab(a), null != b && (this._locale = b), this) } function kc() { return this._locale } function lc(a) {
        // the following switch intentionally omits break keywords
        // to utilize falling through the cases.
        switch (a = J(a)) {
            case "year": this.month(0);/* falls through */
            case "quarter": case "month": this.date(1);/* falls through */
            case "week": case "isoWeek": case "day": case "date": this.hours(0);/* falls through */
            case "hour": this.minutes(0);/* falls through */
            case "minute": this.seconds(0);/* falls through */
            case "second": this.milliseconds(0)
        }
        // weeks are a special case
        // quarters are also special
        return "week" === a && this.weekday(0), "isoWeek" === a && this.isoWeekday(1), "quarter" === a && this.month(3 * Math.floor(this.month() / 3)), this
    } function mc(a) {
        // 'date' is an alias for 'day', so it should be considered as such.
        return a = J(a), void 0 === a || "millisecond" === a ? this : ("date" === a && (a = "day"), this.startOf(a).add(1, "isoWeek" === a ? "week" : a).subtract(1, "ms"))
    } function nc() { return this._d.valueOf() - 6e4 * (this._offset || 0) } function oc() { return Math.floor(this.valueOf() / 1e3) } function pc() { return new Date(this.valueOf()) } function qc() { var a = this; return [a.year(), a.month(), a.date(), a.hour(), a.minute(), a.second(), a.millisecond()] } function rc() { var a = this; return { years: a.year(), months: a.month(), date: a.date(), hours: a.hours(), minutes: a.minutes(), seconds: a.seconds(), milliseconds: a.milliseconds() } } function sc() {
        // new Date(NaN).toJSON() === null
        return this.isValid() ? this.toISOString() : null
    } function tc() { return m(this) } function uc() { return i({}, l(this)) } function vc() { return l(this).overflow } function wc() { return { input: this._i, format: this._f, locale: this._locale, isUTC: this._isUTC, strict: this._strict } } function xc(a, b) { T(0, [a, a.length], 0, b) }
    // MOMENTS
    function yc(a) { return Cc.call(this, a, this.week(), this.weekday(), this.localeData()._week.dow, this.localeData()._week.doy) } function zc(a) { return Cc.call(this, a, this.isoWeek(), this.isoWeekday(), 1, 4) } function Ac() { return wa(this.year(), 1, 4) } function Bc() { var a = this.localeData()._week; return wa(this.year(), a.dow, a.doy) } function Cc(a, b, c, d, e) { var f; return null == a ? va(this, d, e).year : (f = wa(a, d, e), b > f && (b = f), Dc.call(this, a, b, c, d, e)) } function Dc(a, b, c, d, e) { var f = ua(a, b, c, d, e), g = sa(f.year, 0, f.dayOfYear); return this.year(g.getUTCFullYear()), this.month(g.getUTCMonth()), this.date(g.getUTCDate()), this }
    // MOMENTS
    function Ec(a) { return null == a ? Math.ceil((this.month() + 1) / 3) : this.month(3 * (a - 1) + this.month() % 3) }
    // HELPERS
    // MOMENTS
    function Fc(a) { var b = Math.round((this.clone().startOf("day") - this.clone().startOf("year")) / 864e5) + 1; return null == a ? b : this.add(a - b, "d") } function Gc(a, b) { b[ce] = t(1e3 * ("0." + a)) }
    // MOMENTS
    function Hc() { return this._isUTC ? "UTC" : "" } function Ic() { return this._isUTC ? "Coordinated Universal Time" : "" } function Jc(a) { return rb(1e3 * a) } function Kc() { return rb.apply(null, arguments).parseZone() } function Lc(a) { return a } function Mc(a, b, c, d) { var e = ab(), f = j().set(d, b); return e[c](f, a) } function Nc(a, b, c) { if ("number" == typeof a && (b = a, a = void 0), a = a || "", null != b) return Mc(a, b, c, "month"); var d, e = []; for (d = 0; 12 > d; d++) e[d] = Mc(a, d, c, "month"); return e }
    // ()
    // (5)
    // (fmt, 5)
    // (fmt)
    // (true)
    // (true, 5)
    // (true, fmt, 5)
    // (true, fmt)
    function Oc(a, b, c, d) { "boolean" == typeof a ? ("number" == typeof b && (c = b, b = void 0), b = b || "") : (b = a, c = b, a = !1, "number" == typeof b && (c = b, b = void 0), b = b || ""); var e = ab(), f = a ? e._week.dow : 0; if (null != c) return Mc(b, (c + f) % 7, d, "day"); var g, h = []; for (g = 0; 7 > g; g++) h[g] = Mc(b, (g + f) % 7, d, "day"); return h } function Pc(a, b) { return Nc(a, b, "months") } function Qc(a, b) { return Nc(a, b, "monthsShort") } function Rc(a, b, c) { return Oc(a, b, c, "weekdays") } function Sc(a, b, c) { return Oc(a, b, c, "weekdaysShort") } function Tc(a, b, c) { return Oc(a, b, c, "weekdaysMin") } function Uc() { var a = this._data; return this._milliseconds = Ue(this._milliseconds), this._days = Ue(this._days), this._months = Ue(this._months), a.milliseconds = Ue(a.milliseconds), a.seconds = Ue(a.seconds), a.minutes = Ue(a.minutes), a.hours = Ue(a.hours), a.months = Ue(a.months), a.years = Ue(a.years), this } function Vc(a, b, c, d) { var e = Mb(b, c); return a._milliseconds += d * e._milliseconds, a._days += d * e._days, a._months += d * e._months, a._bubble() }
    // supports only 2.0-style add(1, 's') or add(duration)
    function Wc(a, b) { return Vc(this, a, b, 1) }
    // supports only 2.0-style subtract(1, 's') or subtract(duration)
    function Xc(a, b) { return Vc(this, a, b, -1) } function Yc(a) { return 0 > a ? Math.floor(a) : Math.ceil(a) } function Zc() {
        var a, b, c, d, e, f = this._milliseconds, g = this._days, h = this._months, i = this._data;
        // if we have a mix of positive and negative values, bubble down first
        // check: https://github.com/moment/moment/issues/2166
        // The following code bubbles up values, see the tests for
        // examples of what that means.
        // convert days to months
        // 12 months -> 1 year
        return f >= 0 && g >= 0 && h >= 0 || 0 >= f && 0 >= g && 0 >= h || (f += 864e5 * Yc(_c(h) + g), g = 0, h = 0), i.milliseconds = f % 1e3, a = s(f / 1e3), i.seconds = a % 60, b = s(a / 60), i.minutes = b % 60, c = s(b / 60), i.hours = c % 24, g += s(c / 24), e = s($c(g)), h += e, g -= Yc(_c(e)), d = s(h / 12), h %= 12, i.days = g, i.months = h, i.years = d, this
    } function $c(a) {
        // 400 years have 146097 days (taking into account leap year rules)
        // 400 years have 12 months === 4800
        return 4800 * a / 146097
    } function _c(a) {
        // the reverse of daysToMonths
        return 146097 * a / 4800
    } function ad(a) {
        var b, c, d = this._milliseconds; if (a = J(a), "month" === a || "year" === a) return b = this._days + d / 864e5, c = this._months + $c(b), "month" === a ? c : c / 12; switch (b = this._days + Math.round(_c(this._months)), a) {
            case "week": return b / 7 + d / 6048e5; case "day": return b + d / 864e5; case "hour": return 24 * b + d / 36e5; case "minute": return 1440 * b + d / 6e4; case "second": return 86400 * b + d / 1e3;
                // Math.floor prevents floating point math errors here
            case "millisecond": return Math.floor(864e5 * b) + d; default: throw new Error("Unknown unit " + a)
        }
    }
    // TODO: Use this.as('ms')?
    function bd() { return this._milliseconds + 864e5 * this._days + this._months % 12 * 2592e6 + 31536e6 * t(this._months / 12) } function cd(a) { return function () { return this.as(a) } } function dd(a) { return a = J(a), this[a + "s"]() } function ed(a) { return function () { return this._data[a] } } function fd() { return s(this.days() / 7) }
    // helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
    function gd(a, b, c, d, e) { return e.relativeTime(b || 1, !!c, a, d) } function hd(a, b, c) { var d = Mb(a).abs(), e = jf(d.as("s")), f = jf(d.as("m")), g = jf(d.as("h")), h = jf(d.as("d")), i = jf(d.as("M")), j = jf(d.as("y")), k = e < kf.s && ["s", e] || 1 >= f && ["m"] || f < kf.m && ["mm", f] || 1 >= g && ["h"] || g < kf.h && ["hh", g] || 1 >= h && ["d"] || h < kf.d && ["dd", h] || 1 >= i && ["M"] || i < kf.M && ["MM", i] || 1 >= j && ["y"] || ["yy", j]; return k[2] = b, k[3] = +a > 0, k[4] = c, gd.apply(null, k) }
    // This function allows you to set the rounding function for relative time strings
    function id(a) { return void 0 === a ? jf : "function" == typeof a ? (jf = a, !0) : !1 }
    // This function allows you to set a threshold for relative time strings
    function jd(a, b) { return void 0 === kf[a] ? !1 : void 0 === b ? kf[a] : (kf[a] = b, !0) } function kd(a) { var b = this.localeData(), c = hd(this, !a, b); return a && (c = b.pastFuture(+this, c)), b.postformat(c) } function ld() {
        // for ISO strings we do not use the normal bubbling rules:
        //  * milliseconds bubble up until they become hours
        //  * days do not bubble at all
        //  * months bubble up until they become years
        // This is because there is no context-free conversion between hours and days
        // (think of clock changes)
        // and also not between days and months (28-31 days per month)
        var a, b, c, d = lf(this._milliseconds) / 1e3, e = lf(this._days), f = lf(this._months); a = s(d / 60), b = s(a / 60), d %= 60, a %= 60, c = s(f / 12), f %= 12;
        // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
        var g = c, h = f, i = e, j = b, k = a, l = d, m = this.asSeconds(); return m ? (0 > m ? "-" : "") + "P" + (g ? g + "Y" : "") + (h ? h + "M" : "") + (i ? i + "D" : "") + (j || k || l ? "T" : "") + (j ? j + "H" : "") + (k ? k + "M" : "") + (l ? l + "S" : "") : "P0D"
    } var md, nd; nd = Array.prototype.some ? Array.prototype.some : function (a) { for (var b = Object(this), c = b.length >>> 0, d = 0; c > d; d++) if (d in b && a.call(this, b[d], d, b)) return !0; return !1 };
    // Plugins that add properties should also add the key here (null value),
    // so we can properly clone ourselves.
    var od = a.momentProperties = [], pd = !1, qd = {}; a.suppressDeprecationWarnings = !1, a.deprecationHandler = null; var rd; rd = Object.keys ? Object.keys : function (a) { var b, c = []; for (b in a) h(a, b) && c.push(b); return c }; var sd, td = { sameDay: "[Today at] LT", nextDay: "[Tomorrow at] LT", nextWeek: "dddd [at] LT", lastDay: "[Yesterday at] LT", lastWeek: "[Last] dddd [at] LT", sameElse: "L" }, ud = { LTS: "h:mm:ss A", LT: "h:mm A", L: "MM/DD/YYYY", LL: "MMMM D, YYYY", LLL: "MMMM D, YYYY h:mm A", LLLL: "dddd, MMMM D, YYYY h:mm A" }, vd = "Invalid date", wd = "%d", xd = /\d{1,2}/, yd = { future: "in %s", past: "%s ago", s: "a few seconds", m: "a minute", mm: "%d minutes", h: "an hour", hh: "%d hours", d: "a day", dd: "%d days", M: "a month", MM: "%d months", y: "a year", yy: "%d years" }, zd = {}, Ad = {}, Bd = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g, Cd = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g, Dd = {}, Ed = {}, Fd = /\d/, Gd = /\d\d/, Hd = /\d{3}/, Id = /\d{4}/, Jd = /[+-]?\d{6}/, Kd = /\d\d?/, Ld = /\d\d\d\d?/, Md = /\d\d\d\d\d\d?/, Nd = /\d{1,3}/, Od = /\d{1,4}/, Pd = /[+-]?\d{1,6}/, Qd = /\d+/, Rd = /[+-]?\d+/, Sd = /Z|[+-]\d\d:?\d\d/gi, Td = /Z|[+-]\d\d(?::?\d\d)?/gi, Ud = /[+-]?\d+(\.\d{1,3})?/, Vd = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i, Wd = {}, Xd = {}, Yd = 0, Zd = 1, $d = 2, _d = 3, ae = 4, be = 5, ce = 6, de = 7, ee = 8; sd = Array.prototype.indexOf ? Array.prototype.indexOf : function (a) {
        // I know
        var b; for (b = 0; b < this.length; ++b) if (this[b] === a) return b; return -1
    }, T("M", ["MM", 2], "Mo", function () { return this.month() + 1 }), T("MMM", 0, 0, function (a) { return this.localeData().monthsShort(this, a) }), T("MMMM", 0, 0, function (a) { return this.localeData().months(this, a) }), I("month", "M"), L("month", 8), Y("M", Kd), Y("MM", Kd, Gd), Y("MMM", function (a, b) { return b.monthsShortRegex(a) }), Y("MMMM", function (a, b) { return b.monthsRegex(a) }), aa(["M", "MM"], function (a, b) { b[Zd] = t(a) - 1 }), aa(["MMM", "MMMM"], function (a, b, c, d) { var e = c._locale.monthsParse(a, d, c._strict); null != e ? b[Zd] = e : l(c).invalidMonth = a });
    // LOCALES
    var fe = /D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/, ge = "January_February_March_April_May_June_July_August_September_October_November_December".split("_"), he = "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"), ie = Vd, je = Vd;
    // FORMATTING
    T("Y", 0, 0, function () { var a = this.year(); return 9999 >= a ? "" + a : "+" + a }), T(0, ["YY", 2], 0, function () { return this.year() % 100 }), T(0, ["YYYY", 4], 0, "year"), T(0, ["YYYYY", 5], 0, "year"), T(0, ["YYYYYY", 6, !0], 0, "year"),
    // ALIASES
    I("year", "y"),
    // PRIORITIES
    L("year", 1),
    // PARSING
    Y("Y", Rd), Y("YY", Kd, Gd), Y("YYYY", Od, Id), Y("YYYYY", Pd, Jd), Y("YYYYYY", Pd, Jd), aa(["YYYYY", "YYYYYY"], Yd), aa("YYYY", function (b, c) { c[Yd] = 2 === b.length ? a.parseTwoDigitYear(b) : t(b) }), aa("YY", function (b, c) { c[Yd] = a.parseTwoDigitYear(b) }), aa("Y", function (a, b) { b[Yd] = parseInt(a, 10) }),
    // HOOKS
    a.parseTwoDigitYear = function (a) { return t(a) + (t(a) > 68 ? 1900 : 2e3) };
    // MOMENTS
    var ke = N("FullYear", !0);
    // FORMATTING
    T("w", ["ww", 2], "wo", "week"), T("W", ["WW", 2], "Wo", "isoWeek"),
    // ALIASES
    I("week", "w"), I("isoWeek", "W"),
    // PRIORITIES
    L("week", 5), L("isoWeek", 5),
    // PARSING
    Y("w", Kd), Y("ww", Kd, Gd), Y("W", Kd), Y("WW", Kd, Gd), ba(["w", "ww", "W", "WW"], function (a, b, c, d) { b[d.substr(0, 1)] = t(a) }); var le = {
        dow: 0,// Sunday is the first day of the week.
        doy: 6
    };
    // FORMATTING
    T("d", 0, "do", "day"), T("dd", 0, 0, function (a) { return this.localeData().weekdaysMin(this, a) }), T("ddd", 0, 0, function (a) { return this.localeData().weekdaysShort(this, a) }), T("dddd", 0, 0, function (a) { return this.localeData().weekdays(this, a) }), T("e", 0, 0, "weekday"), T("E", 0, 0, "isoWeekday"),
    // ALIASES
    I("day", "d"), I("weekday", "e"), I("isoWeekday", "E"),
    // PRIORITY
    L("day", 11), L("weekday", 11), L("isoWeekday", 11),
    // PARSING
    Y("d", Kd), Y("e", Kd), Y("E", Kd), Y("dd", function (a, b) { return b.weekdaysMinRegex(a) }), Y("ddd", function (a, b) { return b.weekdaysShortRegex(a) }), Y("dddd", function (a, b) { return b.weekdaysRegex(a) }), ba(["dd", "ddd", "dddd"], function (a, b, c, d) {
        var e = c._locale.weekdaysParse(a, d, c._strict);
        // if we didn't get a weekday name, mark the date as invalid
        null != e ? b.d = e : l(c).invalidWeekday = a
    }), ba(["d", "e", "E"], function (a, b, c, d) { b[d] = t(a) });
    // LOCALES
    var me = "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"), ne = "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"), oe = "Su_Mo_Tu_We_Th_Fr_Sa".split("_"), pe = Vd, qe = Vd, re = Vd; T("H", ["HH", 2], 0, "hour"), T("h", ["hh", 2], 0, Qa), T("k", ["kk", 2], 0, Ra), T("hmm", 0, 0, function () { return "" + Qa.apply(this) + S(this.minutes(), 2) }), T("hmmss", 0, 0, function () { return "" + Qa.apply(this) + S(this.minutes(), 2) + S(this.seconds(), 2) }), T("Hmm", 0, 0, function () { return "" + this.hours() + S(this.minutes(), 2) }), T("Hmmss", 0, 0, function () { return "" + this.hours() + S(this.minutes(), 2) + S(this.seconds(), 2) }), Sa("a", !0), Sa("A", !1),
    // ALIASES
    I("hour", "h"),
    // PRIORITY
    L("hour", 13), Y("a", Ta), Y("A", Ta), Y("H", Kd), Y("h", Kd), Y("HH", Kd, Gd), Y("hh", Kd, Gd), Y("hmm", Ld), Y("hmmss", Md), Y("Hmm", Ld), Y("Hmmss", Md), aa(["H", "HH"], _d), aa(["a", "A"], function (a, b, c) { c._isPm = c._locale.isPM(a), c._meridiem = a }), aa(["h", "hh"], function (a, b, c) { b[_d] = t(a), l(c).bigHour = !0 }), aa("hmm", function (a, b, c) { var d = a.length - 2; b[_d] = t(a.substr(0, d)), b[ae] = t(a.substr(d)), l(c).bigHour = !0 }), aa("hmmss", function (a, b, c) { var d = a.length - 4, e = a.length - 2; b[_d] = t(a.substr(0, d)), b[ae] = t(a.substr(d, 2)), b[be] = t(a.substr(e)), l(c).bigHour = !0 }), aa("Hmm", function (a, b, c) { var d = a.length - 2; b[_d] = t(a.substr(0, d)), b[ae] = t(a.substr(d)) }), aa("Hmmss", function (a, b, c) { var d = a.length - 4, e = a.length - 2; b[_d] = t(a.substr(0, d)), b[ae] = t(a.substr(d, 2)), b[be] = t(a.substr(e)) }); var se, te = /[ap]\.?m?\.?/i, ue = N("Hours", !0), ve = { calendar: td, longDateFormat: ud, invalidDate: vd, ordinal: wd, ordinalParse: xd, relativeTime: yd, months: ge, monthsShort: he, week: le, weekdays: me, weekdaysMin: oe, weekdaysShort: ne, meridiemParse: te }, we = {}, xe = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?/, ye = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?/, ze = /Z|[+-]\d\d(?::?\d\d)?/, Ae = [["YYYYYY-MM-DD", /[+-]\d{6}-\d\d-\d\d/], ["YYYY-MM-DD", /\d{4}-\d\d-\d\d/], ["GGGG-[W]WW-E", /\d{4}-W\d\d-\d/], ["GGGG-[W]WW", /\d{4}-W\d\d/, !1], ["YYYY-DDD", /\d{4}-\d{3}/], ["YYYY-MM", /\d{4}-\d\d/, !1], ["YYYYYYMMDD", /[+-]\d{10}/], ["YYYYMMDD", /\d{8}/],
    // YYYYMM is NOT allowed by the standard
    ["GGGG[W]WWE", /\d{4}W\d{3}/], ["GGGG[W]WW", /\d{4}W\d{2}/, !1], ["YYYYDDD", /\d{7}/]], Be = [["HH:mm:ss.SSSS", /\d\d:\d\d:\d\d\.\d+/], ["HH:mm:ss,SSSS", /\d\d:\d\d:\d\d,\d+/], ["HH:mm:ss", /\d\d:\d\d:\d\d/], ["HH:mm", /\d\d:\d\d/], ["HHmmss.SSSS", /\d\d\d\d\d\d\.\d+/], ["HHmmss,SSSS", /\d\d\d\d\d\d,\d+/], ["HHmmss", /\d\d\d\d\d\d/], ["HHmm", /\d\d\d\d/], ["HH", /\d\d/]], Ce = /^\/?Date\((\-?\d+)/i; a.createFromInputFallback = w("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to http://momentjs.com/guides/#/warnings/js-date/ for more info.", function (a) { a._d = new Date(a._i + (a._useUTC ? " UTC" : "")) }),
    // constant that refers to the ISO standard
    a.ISO_8601 = function () { }; var De = w("moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/", function () { var a = rb.apply(null, arguments); return this.isValid() && a.isValid() ? this > a ? this : a : n() }), Ee = w("moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/", function () { var a = rb.apply(null, arguments); return this.isValid() && a.isValid() ? a > this ? this : a : n() }), Fe = function () { return Date.now ? Date.now() : +new Date }; xb("Z", ":"), xb("ZZ", ""),
    // PARSING
    Y("Z", Td), Y("ZZ", Td), aa(["Z", "ZZ"], function (a, b, c) { c._useUTC = !0, c._tzm = yb(Td, a) });
    // HELPERS
    // timezone chunker
    // '+10:00' > ['10',  '00']
    // '-1530'  > ['-15', '30']
    var Ge = /([\+\-]|\d\d)/gi;
    // HOOKS
    // This function will be called whenever a moment is mutated.
    // It is intended to keep the offset in sync with the timezone.
    a.updateOffset = function () { };
    // ASP.NET json date format regex
    var He = /^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?\d*)?$/, Ie = /^(-)?P(?:(-?[0-9,.]*)Y)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)W)?(?:(-?[0-9,.]*)D)?(?:T(?:(-?[0-9,.]*)H)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)S)?)?$/; Mb.fn = vb.prototype; var Je = Rb(1, "add"), Ke = Rb(-1, "subtract"); a.defaultFormat = "YYYY-MM-DDTHH:mm:ssZ", a.defaultFormatUtc = "YYYY-MM-DDTHH:mm:ss[Z]"; var Le = w("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.", function (a) { return void 0 === a ? this.localeData() : this.locale(a) });
    // FORMATTING
    T(0, ["gg", 2], 0, function () { return this.weekYear() % 100 }), T(0, ["GG", 2], 0, function () { return this.isoWeekYear() % 100 }), xc("gggg", "weekYear"), xc("ggggg", "weekYear"), xc("GGGG", "isoWeekYear"), xc("GGGGG", "isoWeekYear"),
    // ALIASES
    I("weekYear", "gg"), I("isoWeekYear", "GG"),
    // PRIORITY
    L("weekYear", 1), L("isoWeekYear", 1),
    // PARSING
    Y("G", Rd), Y("g", Rd), Y("GG", Kd, Gd), Y("gg", Kd, Gd), Y("GGGG", Od, Id), Y("gggg", Od, Id), Y("GGGGG", Pd, Jd), Y("ggggg", Pd, Jd), ba(["gggg", "ggggg", "GGGG", "GGGGG"], function (a, b, c, d) { b[d.substr(0, 2)] = t(a) }), ba(["gg", "GG"], function (b, c, d, e) { c[e] = a.parseTwoDigitYear(b) }),
    // FORMATTING
    T("Q", 0, "Qo", "quarter"),
    // ALIASES
    I("quarter", "Q"),
    // PRIORITY
    L("quarter", 7),
    // PARSING
    Y("Q", Fd), aa("Q", function (a, b) { b[Zd] = 3 * (t(a) - 1) }),
    // FORMATTING
    T("D", ["DD", 2], "Do", "date"),
    // ALIASES
    I("date", "D"),
    // PRIOROITY
    L("date", 9),
    // PARSING
    Y("D", Kd), Y("DD", Kd, Gd), Y("Do", function (a, b) { return a ? b._ordinalParse : b._ordinalParseLenient }), aa(["D", "DD"], $d), aa("Do", function (a, b) { b[$d] = t(a.match(Kd)[0], 10) });
    // MOMENTS
    var Me = N("Date", !0);
    // FORMATTING
    T("DDD", ["DDDD", 3], "DDDo", "dayOfYear"),
    // ALIASES
    I("dayOfYear", "DDD"),
    // PRIORITY
    L("dayOfYear", 4),
    // PARSING
    Y("DDD", Nd), Y("DDDD", Hd), aa(["DDD", "DDDD"], function (a, b, c) { c._dayOfYear = t(a) }),
    // FORMATTING
    T("m", ["mm", 2], 0, "minute"),
    // ALIASES
    I("minute", "m"),
    // PRIORITY
    L("minute", 14),
    // PARSING
    Y("m", Kd), Y("mm", Kd, Gd), aa(["m", "mm"], ae);
    // MOMENTS
    var Ne = N("Minutes", !1);
    // FORMATTING
    T("s", ["ss", 2], 0, "second"),
    // ALIASES
    I("second", "s"),
    // PRIORITY
    L("second", 15),
    // PARSING
    Y("s", Kd), Y("ss", Kd, Gd), aa(["s", "ss"], be);
    // MOMENTS
    var Oe = N("Seconds", !1);
    // FORMATTING
    T("S", 0, 0, function () { return ~~(this.millisecond() / 100) }), T(0, ["SS", 2], 0, function () { return ~~(this.millisecond() / 10) }), T(0, ["SSS", 3], 0, "millisecond"), T(0, ["SSSS", 4], 0, function () { return 10 * this.millisecond() }), T(0, ["SSSSS", 5], 0, function () { return 100 * this.millisecond() }), T(0, ["SSSSSS", 6], 0, function () { return 1e3 * this.millisecond() }), T(0, ["SSSSSSS", 7], 0, function () { return 1e4 * this.millisecond() }), T(0, ["SSSSSSSS", 8], 0, function () { return 1e5 * this.millisecond() }), T(0, ["SSSSSSSSS", 9], 0, function () { return 1e6 * this.millisecond() }),
    // ALIASES
    I("millisecond", "ms"),
    // PRIORITY
    L("millisecond", 16),
    // PARSING
    Y("S", Nd, Fd), Y("SS", Nd, Gd), Y("SSS", Nd, Hd); var Pe; for (Pe = "SSSS"; Pe.length <= 9; Pe += "S") Y(Pe, Qd); for (Pe = "S"; Pe.length <= 9; Pe += "S") aa(Pe, Gc);
    // MOMENTS
    var Qe = N("Milliseconds", !1);
    // FORMATTING
    T("z", 0, 0, "zoneAbbr"), T("zz", 0, 0, "zoneName"); var Re = q.prototype; Re.add = Je, Re.calendar = Ub, Re.clone = Vb, Re.diff = ac, Re.endOf = mc, Re.format = ec, Re.from = fc, Re.fromNow = gc, Re.to = hc, Re.toNow = ic, Re.get = Q, Re.invalidAt = vc, Re.isAfter = Wb, Re.isBefore = Xb, Re.isBetween = Yb, Re.isSame = Zb, Re.isSameOrAfter = $b, Re.isSameOrBefore = _b, Re.isValid = tc, Re.lang = Le, Re.locale = jc, Re.localeData = kc, Re.max = Ee, Re.min = De, Re.parsingFlags = uc, Re.set = R, Re.startOf = lc, Re.subtract = Ke, Re.toArray = qc, Re.toObject = rc, Re.toDate = pc, Re.toISOString = dc, Re.toJSON = sc, Re.toString = cc, Re.unix = oc, Re.valueOf = nc, Re.creationData = wc,
    // Year
    Re.year = ke, Re.isLeapYear = qa,
    // Week Year
    Re.weekYear = yc, Re.isoWeekYear = zc,
    // Quarter
    Re.quarter = Re.quarters = Ec,
    // Month
    Re.month = ja, Re.daysInMonth = ka,
    // Week
    Re.week = Re.weeks = Aa, Re.isoWeek = Re.isoWeeks = Ba, Re.weeksInYear = Bc, Re.isoWeeksInYear = Ac,
    // Day
    Re.date = Me, Re.day = Re.days = Ja, Re.weekday = Ka, Re.isoWeekday = La, Re.dayOfYear = Fc,
    // Hour
    Re.hour = Re.hours = ue,
    // Minute
    Re.minute = Re.minutes = Ne,
    // Second
    Re.second = Re.seconds = Oe,
    // Millisecond
    Re.millisecond = Re.milliseconds = Qe,
    // Offset
    Re.utcOffset = Bb, Re.utc = Db, Re.local = Eb, Re.parseZone = Fb, Re.hasAlignedHourOffset = Gb, Re.isDST = Hb, Re.isLocal = Jb, Re.isUtcOffset = Kb, Re.isUtc = Lb, Re.isUTC = Lb,
    // Timezone
    Re.zoneAbbr = Hc, Re.zoneName = Ic,
    // Deprecations
    Re.dates = w("dates accessor is deprecated. Use date instead.", Me), Re.months = w("months accessor is deprecated. Use month instead", ja), Re.years = w("years accessor is deprecated. Use year instead", ke), Re.zone = w("moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/", Cb), Re.isDSTShifted = w("isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information", Ib); var Se = Re, Te = B.prototype; Te.calendar = C, Te.longDateFormat = D, Te.invalidDate = E, Te.ordinal = F, Te.preparse = Lc, Te.postformat = Lc, Te.relativeTime = G, Te.pastFuture = H, Te.set = z,
    // Month
    Te.months = ea, Te.monthsShort = fa, Te.monthsParse = ha, Te.monthsRegex = ma, Te.monthsShortRegex = la,
    // Week
    Te.week = xa, Te.firstDayOfYear = za, Te.firstDayOfWeek = ya,
    // Day of Week
    Te.weekdays = Ea, Te.weekdaysMin = Ga, Te.weekdaysShort = Fa, Te.weekdaysParse = Ia, Te.weekdaysRegex = Ma, Te.weekdaysShortRegex = Na, Te.weekdaysMinRegex = Oa,
    // Hours
    Te.isPM = Ua, Te.meridiem = Va, Za("en", { ordinalParse: /\d{1,2}(th|st|nd|rd)/, ordinal: function (a) { var b = a % 10, c = 1 === t(a % 100 / 10) ? "th" : 1 === b ? "st" : 2 === b ? "nd" : 3 === b ? "rd" : "th"; return a + c } }),
    // Side effect imports
    a.lang = w("moment.lang is deprecated. Use moment.locale instead.", Za), a.langData = w("moment.langData is deprecated. Use moment.localeData instead.", ab); var Ue = Math.abs, Ve = cd("ms"), We = cd("s"), Xe = cd("m"), Ye = cd("h"), Ze = cd("d"), $e = cd("w"), _e = cd("M"), af = cd("y"), bf = ed("milliseconds"), cf = ed("seconds"), df = ed("minutes"), ef = ed("hours"), ff = ed("days"), gf = ed("months"), hf = ed("years"), jf = Math.round, kf = {
        s: 45,// seconds to minute
        m: 45,// minutes to hour
        h: 22,// hours to day
        d: 26,// days to month
        M: 11
    }, lf = Math.abs, mf = vb.prototype; mf.abs = Uc, mf.add = Wc, mf.subtract = Xc, mf.as = ad, mf.asMilliseconds = Ve, mf.asSeconds = We, mf.asMinutes = Xe, mf.asHours = Ye, mf.asDays = Ze, mf.asWeeks = $e, mf.asMonths = _e, mf.asYears = af, mf.valueOf = bd, mf._bubble = Zc, mf.get = dd, mf.milliseconds = bf, mf.seconds = cf, mf.minutes = df, mf.hours = ef, mf.days = ff, mf.weeks = fd, mf.months = gf, mf.years = hf, mf.humanize = kd, mf.toISOString = ld, mf.toString = ld, mf.toJSON = ld, mf.locale = jc, mf.localeData = kc,
    // Deprecations
    mf.toIsoString = w("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)", ld), mf.lang = Le,
    // Side effect imports
    // FORMATTING
    T("X", 0, 0, "unix"), T("x", 0, 0, "valueOf"),
    // PARSING
    Y("x", Rd), Y("X", Ud), aa("X", function (a, b, c) { c._d = new Date(1e3 * parseFloat(a, 10)) }), aa("x", function (a, b, c) { c._d = new Date(t(a)) }),
    // Side effect imports
    a.version = "2.14.1", b(rb), a.fn = Se, a.min = tb, a.max = ub, a.now = Fe, a.utc = j, a.unix = Jc, a.months = Pc, a.isDate = f, a.locale = Za, a.invalid = n, a.duration = Mb, a.isMoment = r, a.weekdays = Rc, a.parseZone = Kc, a.localeData = ab, a.isDuration = wb, a.monthsShort = Qc, a.weekdaysMin = Tc, a.defineLocale = $a, a.updateLocale = _a, a.locales = bb, a.weekdaysShort = Sc, a.normalizeUnits = J, a.relativeTimeRounding = id, a.relativeTimeThreshold = jd, a.calendarFormat = Tb, a.prototype = Se; var nf = a; return nf
});
$(document).ready(function () {
    //$(".navbar-nav li a").click(function (event) {
    //    $(".navbar-collapse").collapse('hide');
    //});
});

$(function () {
    var form = $(".login-form");

    form.css({
        opacity: 1,
        "-webkit-transform": "scale(1)",
        "transform": "scale(1)",
        "-webkit-transition": ".5s",
        "transition": ".5s"
    });

    $(".select2").select2();
});

function showDialog(id) {
    var dialog = $("#" + id).data('dialog');
    if (!dialog.element.data('opened')) {
        dialog.open();
    } else {
        dialog.close();
    }
}

function showModal(id) {
    $("#" + id).modal('show');
}

function closeModal(id, opt) {
    $("#" + id).hide();
    $("#" + id).modal('hide');
    if (opt != undefined && opt == 1) {
        setTimeout("$('body').addClass('modal-open')", 1000)
    }
    else {
        setTimeout("$('body').removeClass('modal-open')", 1000)

    }
}

function backToTop() {
    $('html,body,div').animate({ scrollTop: 0 }, 'slow');
}

function HideMenu() {
    $(".navbar-collapse").collapse('hide');
}

function ObtenerMinutosUTC(date) {
    var minutos = date.getTimezoneOffset();
    return minutos;
}

function ObtenerIntervaloTiempo(date1, date2, interval) {
    var second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
    date1 = new Date(date1);
    date2 = new Date(date2);
    var timediff = date2 - date1;
    if (isNaN(timediff)) return NaN;
    switch (interval) {
        case "years": return date2.getFullYear() - date1.getFullYear();
        case "months": return (
            (date2.getFullYear() * 12 + date2.getMonth())
            -
            (date1.getFullYear() * 12 + date1.getMonth())
        );
        case "weeks": return Math.floor(timediff / week);
        case "days": return Math.floor(timediff / day);
        case "hours": return Math.floor(timediff / hour);
        case "minutes": return Math.floor(timediff / minute);
        case "seconds": return Math.floor(timediff / second);
        default: return undefined;
    }
}

function Notification(htmlElement) {

    this.htmlElement = htmlElement;
    this.icon = htmlElement.querySelector('.icon > i');
    this.text = htmlElement.querySelector('.text');
    this.close = htmlElement.querySelector('.close');
    this.isRunning = false;
    this.timeout;

    this.bindEvents();
};

Notification.prototype.bindEvents = function () {
    var self = this;
    this.close.addEventListener('click', function () {
        window.clearTimeout(self.timeout);
        self.reset();
    });
}

Notification.prototype.info = function (message) {
    this.reset();
    if (this.isRunning) return false;

    this.text.innerHTML = message;
    this.htmlElement.className = 'notification info';
    this.icon.className = 'fa fa-2x fa-info-circle';

    this.show();
}

Notification.prototype.warning = function (message) {
    this.reset();
    if (this.isRunning) return false;

    this.text.innerHTML = message;
    this.htmlElement.className = 'notification warning';
    this.icon.className = 'fa fa-2x fa-exclamation-triangle';

    this.show();
}

Notification.prototype.error = function (message) {
    this.reset();
    if (this.isRunning) return false;

    this.text.innerHTML = message;
    this.htmlElement.className = 'notification error';
    this.icon.className = 'fa fa-2x fa-exclamation-circle';

    this.show();
}

Notification.prototype.show = function () {
    if (!this.htmlElement.classList.contains('visible'))
        this.htmlElement.classList.add('visible');

    this.isRunning = true;
    this.autoReset();
};

Notification.prototype.autoReset = function () {
    var self = this;
    this.timeout = window.setTimeout(function () {
        self.reset();
    }, 2000);
}

Notification.prototype.reset = function () {
    this.htmlElement.className = "notification";
    this.icon.className = "";
    this.isRunning = false;
};

//document.addEventListener('DOMContentLoaded', init);

function init() {
    var info = document.getElementById('info');
    var warn = document.getElementById('warn');
    var error = document.getElementById('error');

    var notificator = new Notification(document.querySelector('.notification'));

    info.onclick = function () {
        notificator.info('Esta es una información');
    }

    warn.onclick = function () {
        notificator.warning('Te te te advieeeerto!');
    }

    error.onclick = function () {
        notificator.error('Le causaste derrame al sistema');
    }
}


//--------------------------------------------------------------------------------------------------------//
//Configure la URL donde se hospeda el proyecto ASP.
function ObtenerURLProyectoASP(URLAsp) {
    //URLAsp = 'http://181.143.147.114:8111/WebASP';
    URLAsp = 'http://localhost:4041';
    return URLAsp;
}
//---------------------- Nota: por favor no colocar nada debajo de esta función ----------------------------//

/* Agregar Validaciones */
function MaskAlfanumericoEspacio(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /^[a-zA-Z0-9 ]{1,80}$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function MaskNumerico(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function MaskTexto(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /^[a-zA-Z]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function MaskCorreo(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /^[a-zA-Z0-9@.]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function MaskTextoEspacio(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /^[a-zA-Z ]{1,80}$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

//function validarEmail(email) {
//    var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//    if (!expr.test(email)) {
//        return false;
//    }
//    else {
//        return true;
//    }
//}

function RetornarFechaSinHora(fecha) {
    var fecha = new Date(fecha);
    fecha.setHours(0);
    fecha.setMinutes(0);
    fecha.setSeconds(0);
    fecha.setMilliseconds(0);

    return fecha;
}

function validarContrasena(contrasena) {
    var exprletras = /[ABCDEFGHIJKLMÑOPQRSTUVWXYSabcdefghijklmnñopqrstuv]/
    var exprnumeros = /[0123456789]/
    var expcarcateres = /[\/\*\-)\(\%\$\#\¿\?\"\.\{\!\}\¡\=\[\]]/;
    var countnum = 0
    var countcaracter = 0
    var countletra = 0
    for (var i = 0; i < contrasena.length; i++) {
        if (exprletras.test(contrasena[i])) {
            countletra++
        } else if (exprnumeros.test(contrasena[i])) {
            countnum++
        } else if (expcarcateres.test(contrasena[i])) {
            countcaracter++
        } else if (contrasena[i] == '&' || contrasena[i] == '+') {
            return false
        }
    }
    if (contrasena.length < 5) {
        return false
    }
    if (countcaracter > 0 && countletra > 0 && countnum > 0) {
        return true
    } else {
        return false
    }
}

function MaskContrasena(contrasena) {
    var exprletras = /[ABCDEFGHIJKLMÑOPQRSTUVWXYSabcdefghijklmnñopqrstuv]/
    var exprnumeros = /[0123456789]/
    var expcarcateres = /[\/\*\-)\(\%\$\#\¿\?\"\.\{\!\}\¡\=\[\]]/;
    var countnum = 0
    var countcaracter = 0
    var countletra = 0
    for (var i = 0; i < contrasena.length; i++) {
        if (exprletras.test(contrasena[i])) {
            countletra++
        } else if (exprnumeros.test(contrasena[i])) {
            countnum++
        } else if (expcarcateres.test(contrasena[i])) {
            countcaracter++
        } else if (contrasena[i] == '&' || contrasena[i] == '+') {
            return false
        }
    }
    if (contrasena.length < 5) {
        return false
    }
    if (countcaracter > 0 && countletra > 0 && countnum > 0) {
        return true
    } else {
        return false
    }
}


function MaskHoraMilitar(hora) {
    var expr = /^([0-1][0-9]|2[0-3]):[0-5][0-9]$/;
    if (!expr.test(hora)) {
        return false;
    }
    else {
        return true;
    }
}


function checkEmail(field) {
    if (!validarEmail(field.val().trim())) {
        field.parent().parent().addClass('has-error');
        return false;
    } else {
        field.parent().parent().removeClass('has-error');
    }
    return true;
}

function checkVariosEmail(field) {

    var success = true;

    if (field.val() == '') {
        field.parent().parent().addClass('has-error');
        success = false;
    } else {
        var correos = field.val().trim();
        var lista = correos.split(",");

        lista.forEach(function (correo) {
            if (!validarEmail(correo.trim())) {
                field.parent().parent().addClass('has-error');
                success = false;
            }
        });

        if (success == true) {
            field.parent().parent().removeClass('has-error');
        }
    }
    return success;
}

function MascaraDecimalesGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            try {


                var Model = ''
                var Lista = ''
                var conitinue = false
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskDecimales()')) {
                        conitinue = true
                        break;
                    }
                } if (conitinue) {

                    for (var j = 0; j < a[i].attributes.length; j++) {
                        if (a[i].attributes[j].name == 'ng-model') {
                            Model = a[i].attributes[j].value

                        } else if (a[i].attributes[j].name == 'lista') {
                            Lista = a[i].attributes[j].value
                        }
                    }

                    var b = Model.split('.')
                    switch (b.length) {
                        case 1:
                            if (sc[b[0]] !== undefined) {
                                try { sc[b[0]] = MascaraDecimales(sc[b[0]]) } catch (e) { }
                            }
                            break;
                        case 2:
                            if (sc[b[0]][b[1]] !== undefined) {
                                try { sc[b[0]][b[1]] = MascaraDecimales(sc[b[0]][b[1]]) } catch (e) { }
                            }
                            break;
                        case 3:
                            if (sc[b[0]][b[1]][b[2]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]] = MascaraDecimales(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                            }
                            break;
                        case 4:
                            if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraDecimales(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                            }
                            break;
                        case 5:
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraDecimales(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                            }
                            break;
                        case 6:
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraDecimales(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                            }
                            break;
                        case 7:
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraDecimales(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                            }
                            break;
                    }

                }
            } catch (e) {

            }
        }
    }
}

function MascaraDecimales(option) {
    option = option.toString()
    var numeroarray = []
    var numero = ''
    var contpunto = 0
    for (var i = 0; i < option.length; i++) {
        patron = /[0123456789/.]/
        if (i == 0 && option[i] == '-') {
            numeroarray.push(option[i])
        }
        else if (patron.test(option[i])) {
            if (patron.test(option[i]) == ' ') {
                option[i] = '';
            } else {
                if (option[i] == '.') {
                    if (contpunto == 0) {
                        numeroarray.push(option[i])
                    }
                    contpunto++
                } else {
                    numeroarray.push(option[i])
                }
            }
        }
    }

    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    return numero
}


function checkField(field) {
    if (field.val() == '' || field.val() == '0') {
        field.parent().parent().addClass('has-error');
        return false;
    } else {
        field.parent().parent().removeClass('has-error');
    }
    return true;
}

function checkddl(field) {
    if (field.attr('value') == '' || field.attr('value') == '0') {
        field.parent().parent().addClass('has-error');
        return false;
    } else {
        field.parent().parent().removeClass('has-error');
    }
    return true;
}

/*Obtener rango de fechas
/JO: 20/05/2016

Entrada: Fecha Inicial y Fecha Final
Salida: Rango en días como numero entero*/

var MILISENGUNDOS_POR_DIA = 1000 * 60 * 60 * 24;

function RetornarRangoFecha(FechaInicial, FechaFinal) {
    var dia1 = new Date(FechaInicial);
    var dia2 = new Date(FechaFinal);
    return CalcularRangoFechas(dia1, dia2);

}
function CalcularRangoFechas(a, b) {
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
    return Math.floor((utc2 - utc1) / MILISENGUNDOS_POR_DIA);
}

Array.prototype.unique = function (a) {
    return function () { return this.filter(a) }
}(function (a, b, c) {
    return c.indexOf(a, b + 1) < 0
});


function RetornarHoras(Fecha) {

    Fecha = new Date(Fecha)
    var arrHora = new Date(angular.copy(Fecha));
    var hora = arrHora.getHours()
    var minuto = arrHora.getMinutes()
    if (hora < 10 && hora >= 0) {
        hora = ("0" + hora)
    }
    if (minuto < 10 && minuto > 0) {
        minuto = ("0" + minuto)
    }
    if (minuto == 0) {
        minuto = (minuto + "0")
    }
    var horaMinutos = (hora + ":" + minuto);
    return horaMinutos;
}


/*----------------------------------------------*/
/*Gestionar Lista Detalle
/JO: 01/05/2016
Entrada: Pagina actual, Lista que llena el Grid
Salida: Lista detalle*/


function GuardarListaDetalle(ListaDetalle, paginaActual, ListadoGrid) {

    PaginaGuardada = 0;

    //Verifica si la página actual se encuentra guardada en alguna de las posiciones de ListaDetalle
    if (ListaDetalle.length > 0) {
        ListaDetalle.forEach(function (itemPaginas) {
            if (itemPaginas.CodigoPagina == paginaActual) {
                PaginaGuardada = 1;
            }
        });
    }

    //Si la página no se encuentra guardada, se procede a guardar el ID de la página (Posición de la página) 
    //y la lista con el resultado que contiene
    if (PaginaGuardada == 0) {

        ItemLista = {
            CodigoPagina: paginaActual,
            Lista: ListadoGrid // Aquí colocar la lista con la cual se carga el Grid
        }

        ListaDetalle.push(ItemLista);
    }
    return ListaDetalle;
}


/*Separar un nombre completo de terceros en Nombre y Apellido
JO: 01/07/2016
Entrada: Nombre completo del tercero
Salida: Arreglo separando cada palabra del nombre*/

function SepararNombresTerceros(NombreCompleto) {
    var ArrNombreTercero = [];
    var i = 0;
    var ArrNombreTercero = NombreCompleto.split(" ");
    return ArrNombreTercero;
}

/*Formatear Fecha
*Ingreso: Fecha.toString() , Formato Fecha (Ver formatos validos en Constantes.js)
Salida: Fecha con formato
JO: 18/07/2016*/
function Formatear_Fecha(Fecha, FormatoFecha) {

    var date = new Date(Fecha);
    var formato = {};
    formato.datejs = date.toString(FormatoFecha);
    var FechaRetorno = formato.datejs;
    return FechaRetorno

}

/*Validador de correo por medio de expresión regular - propiedad ng-pattern a nivel de vista 
EG: 01/08/2016*/
var validadorCorreo = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})((,\s?){1}[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))*$/;

function Formatear_Fecha_Dia_Mes_Ano(Fecha) {
    var arrFecha = Fecha
    var dia = arrFecha.getDate()
    var mes = arrFecha.getMonth()
    var año = arrFecha.getFullYear()
    mes = (mes + 1);
    var FechaResultado = (dia + "/" + mes + "/" + año);

    return FechaResultado;
}
function Formatear_Fecha_Mes_Dia_Ano(Fecha) {
    var arrFecha = Fecha
    var dia = arrFecha.getDate()
    var mes = arrFecha.getMonth()
    var año = arrFecha.getFullYear()
    mes = (mes + 1);
    var FechaResultado = (mes + "/" + dia + "/" + año);

    return FechaResultado;
}

/*Este metodo sirve para formatear fechas ya guardadas y evita la conversión según la ubicación del PC, es decir, que retorne una
fecha diferente a la guardada desfasando un día

Para llamar este metodo desde cualquier controlador lo hace de la forma: 
MiFecha = RetornarFechaEspecifica(response.data.Datos.fecha)

Utilicelo para mostrar datos

JO: 22/08/2016*/


function RetornarFechaEspecifica(fecha) {
    var temp = new Date(fecha);
    var minutos = temp.getTimezoneOffset();
    temp.setMinutes(temp.getMinutes() + minutos);
    return temp;
}
function RetornarFechaEspecificaSinTimeZone(fecha) {
    fecha = new Date(fecha);
    var minutos = fecha.getTimezoneOffset();
    fecha.setMinutes(fecha.getMinutes() - minutos);
    return fecha;
}
function RetornaFechaMasMes(fecha) {
    fecha = new Date(fecha);
    fecha.setMonth(temp.getMonth() + 1);
    return fecha;
}


function ValidarPermisos(permisos) {
    var ListadoPermisos = []
    if (permisos.AplicaConsultar == PERMISO_ACTIVO) {
        ListadoPermisos.DeshabilitarConsulta = false
    }
    else {
        ListadoPermisos.DeshabilitarConsulta = true
    }
    if (permisos.AplicaEliminarAnular == PERMISO_ACTIVO) {
        ListadoPermisos.DeshabilitarEliminarAnular = false
    }
    else {
        ListadoPermisos.DeshabilitarEliminarAnular = true
    }
    if (permisos.AplicaImprimir == PERMISO_ACTIVO) {
        ListadoPermisos.DeshabilitarImprimir = false
    }
    else {
        ListadoPermisos.DeshabilitarImprimir = true
    }
    if (permisos.AplicaActualizar == PERMISO_ACTIVO) {
        ListadoPermisos.DeshabilitarActualizar = false
    }
    else {
        ListadoPermisos.DeshabilitarActualizar = true
    }

    return ListadoPermisos
}

//-------------------------Mascaras y validaciones-------------------------------
var temp = 0
function MascaraHora(option) {
    option = option.toString()
    var numeroarray = []
    var numero = ''
    if (option[0] == '-') {
        numero = ''
    } else {
        for (var i = 0; i < option.length; i++) {
            patron = /[0123456789]/
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    numeroarray.push(option[i])
                }
            }
        }

    }
    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    option = numero
    if (isNaN(option)) {
        return ''
    } else {
        if (option.length == 2) {
            if (event.inputType != 'deleteContentBackward') {
                if (parseInt(option) > 23) {
                    option = '23:'
                } else {
                    option = option + ':'
                }
            }
        }
        else {
            option = option.toString()
            var numeroarray = []
            var numero = ''
            if (option[0] == '-') {
                numero = ''
            } else {
                for (var i = 0; i < option.length; i++) {
                    patron = /[0123456789]/
                    if (patron.test(option[i])) {
                        if (patron.test(option[i]) == ' ') {
                            option[i] = '';
                        } else {
                            numeroarray.push(option[i])
                        }
                    }
                }

            }
            for (var i = 0; i < numeroarray.length; i++) {
                numero = numero + numeroarray[i]
            }
            option = numero
            var temph = ''
            for (var i = 0; i < option.length + 1; i++) {
                if (i == 2) {
                    temph = temph + ':' + option[i].toString();
                } else {
                    if (option[i] != undefined) {
                        temph = temph + option[i].toString();
                    }
                }
            }
            var temph2 = ''
            var temph3 = ''
            for (var i = 0; i < temph.length; i++) {
                if (i <= 4) {
                    if (temph[i] !== undefined) {
                        temph2 = temph2 + temph[i]
                    }
                }
            }
            try {
                if (parseInt(temph2[0].toString() + temph2[1].toString()) > 23) {
                    temph3 = '23:'
                } else {
                    temph3 = temph2[0].toString() + temph2[1].toString() + ':'
                }
            } catch (e) {
                if (temph2[0] !== undefined && temph2[1] !== undefined) {
                    temph3 = temph2[0].toString() + temph2[1].toString() + ':'
                } else if (temph2[0] !== undefined) {
                    temph3 = temph2[0].toString()
                }
            }
            try {
                if (parseInt(temph2[3].toString() + temph2[4].toString()) > 59) {
                    temph3 = temph3 + '59'
                } else {
                    temph3 = temph3 + temph2[3].toString() + temph2[4].toString()
                }
            } catch (e) {
                if (temph2[3] !== undefined && temph2[4] !== undefined) {
                    temph3 = temph3 + temph2[3].toString() + temph2[4].toString()
                } else if (temph2[3] !== undefined) {
                    temph3 = temph3 + temph2[3].toString()
                }
            }
            option = temph3
        }
        return option
    }
}

function MascaraPlaca(option) {
    option = option.toString()
    patron = /[A-Za-zñÑ\s]/
    var opt
    var placaarray = []
    var placa = ''
    if (option.length <= 3) {
        for (var i = 0; i < option.length; i++) {
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    optione[i] = '';
                } else {
                    placaarray.push(option[i])
                }
            }
        }

    } else if (option.length > 3) {
        for (var i = 0; i < 3; i++) {
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    placaarray.push(option[i])
                }
            }
        }
        patron = /\d/
        for (var i = 3; i < option.length; i++) {
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    placaarray.push(option[i])
                }
            }
        }
    }

    if (placaarray.length <= 6) {
        for (var i = 0; i < placaarray.length; i++) {
            placa = placa + placaarray[i]
        }
        option = placa
    }
    else if (option.length > 6) {
        for (var i = 0; i < 6; i++) {
            placa = placa + placaarray[i]
        }
        option = placa
    }

    return option.toUpperCase()
}

function MascaraTelefono(option) {
    option = option.toString()
    var numeroarray = []
    var numero = ''
    if (option[0] == '-' || option[1] == '-' || option[2] == '-' || option[3] == '-' || option[4] == '-' || option[5] == '-' || option[6] == '-') {
        if (option[0] == ':' || option[1] == ':' || option[2] == ':' || option[3] == ':' || option[4] == ':' || option[5] == ':' || option[6] == ':') {
            numero = ''
        }
    } else {
        for (var i = 0; i < option.length; i++) {
            patron = /[0123456789\+\(\)\:\-]/
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    numeroarray.push(option[i])
                }
            }
        }
    }
    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    return numero
}

function MascaraCelular(option) {
    option = option.toString()
    var numeroarray = []
    var numero = ''

    for (var i = 0; i < option.length; i++) {
        patron = /[0123456789\+\(\)]/
        if (patron.test(option[i])) {
            if (patron.test(option[i]) == ' ') {
                option[i] = '';
            } else {
                numeroarray.push(option[i])
            }
        } else if (i >= 10 && i <= 12 || i >= 16 && i <= 18) {
            if (i >= 10 && i <= 12) {
                if (option[i] == ' ' || option[i] == '-') {
                    numeroarray.push(option[i])
                }
            } else if (i >= 15 && i <= 18) {
                if ((option[10] != ' ' && option[10] != '-') && (option[11] != ' ' && option[11] != '-') && (option[12] != ' ' && option[12] != '-')) {
                    if ((option[i] == ' ' || option[i] == '-') && cont == 0) {
                        numeroarray.push(option[i])
                    }
                }
            }

        }
    }

    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    return numero
}

function MascaraNumero(option) {
    option = option.toString()
    var numeroarray = []
    var numero = ''
    if (option[0] == '-') {
        numero = ''
    } else {
        for (var i = 0; i < option.length; i++) {
            patron = /[0123456789]/
            if (i == 0 && option[i] == '-') {
                numeroarray.push(option[i])
            }
            else if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    numeroarray.push(option[i])
                }
            }
        }

    }
    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    return parseInt(numero)
}
function MascaraPorcentaje(option) {
    option = option.toString()
    var numeroarray = []
    var numero = ''
    if (option[0] == '-') {
        numero = ''
    } else {
        for (var i = 0; i < option.length; i++) {
            patron = /[0123456789]/
            if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    numeroarray.push(option[i])
                }
            }
        }

    }
    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    if (parseInt(numero) >= 100) {
        numero = '100'
    }
    return numero
}

function MascaraMoneda(option) {
    option = option.toString()
    var numeroarray = []
    var numero = ''
    if (option[0] == '-') {
        numero = ''
    } else {
        for (var i = 0; i < option.length; i++) {
            patron = /[0123456789/,/./:]/
            if (i == 0 && option[i] == '-') {
                numeroarray.push(option[i])
            }
            else if (patron.test(option[i])) {
                if (patron.test(option[i]) == ' ') {
                    option[i] = '';
                } else {
                    numeroarray.push(option[i])
                }
            }
        }

    }
    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    return numero
}

//function MascaraDecimales(option) {
//    option = option.toString()
//    var numeroarray = []
//    var numero = ''
//    if (option[0] == '-') {
//        numero = ''
//    } else {
//        for (var i = 0; i < option.length; i++) {
//            patron = /[0123456789/.]/
//            if (patron.test(option[i])) {
//                if (patron.test(option[i]) == ' ') {
//                    option[i] = '';
//                } else {
//                    numeroarray.push(option[i])
//                }
//            }
//        }

//    }
//    for (var i = 0; i < numeroarray.length; i++) {
//        numero = numero + numeroarray[i]
//    }
//    return numero
//}

//function MascaraDecimales(option) {
//    option = option.toString()
//    var numeroarray = []
//    var numero = ''
//    if (option[0] == '-') {
//        numero = ''
//    } else {
//        for (var i = 0; i < option.length; i++) {
//            patron = /[0123456789/.]/
//            if (patron.test(option[i])) {
//                if (patron.test(option[i]) == ' ') {
//                    option[i] = '';
//                } else {
//                    numeroarray.push(option[i])
//                }
//            }
//        }

//    }
//    for (var i = 0; i < numeroarray.length; i++) {
//        numero = numero + numeroarray[i]
//    }
//    return numero
//}


function MascaraDireccion(option) {
    option = option.toString()
    var dir = ''

    var direccion = [];
    if (option.length > 250) {
        for (var i = 0; i < 250; i++) {
            direccion[i].push(option[i])
        }
    } else {
        for (var i = 0; i < option.length; i++) {
            direccion[i].push(option[i])
        }
    }
    for (var i = 0; i < direccion.length; i++) {
        dir = dir + direccion[i]
    }
    option = dir
    return option.toUpperCase()

}

function MascaraIdentificacion(option, select) {
    option = option.toString()
    select = select.toString()
    var opt
    var opt2
    var numeroarray = []
    var numero = ''

    if (document.getElementsByName(select)[0]) {
        opt2 = document.getElementsByName(select)[0]
    } else if (document.getElementById(select)) {
        opt2 = document.getElementById(select)
    }
    if (opt2.selectedOptions[0].label == 'PASAPORTE') {
        if (option[0] == '-') {
            numero = ''
        } else {
            for (var i = 0; i < option.length; i++) {
                patron = /[ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789]/
                var a = option[i].toUpperCase()
                if (patron.test(a)) {
                    if (patron.test(a) == ' ') {
                        a = '';
                    } else {
                        numeroarray.push(a)
                    }
                }
            }

        }
    } else {
        if (option[0] == '-') {
            numero = ''
        } else {
            for (var i = 0; i < option.length; i++) {
                patron = /[0123456789]/
                if (patron.test(option[i])) {
                    if (patron.test(option[i]) == ' ') {
                        option[i] = '';
                    } else {
                        numeroarray.push(option[i])
                    }
                }
            }

        }
    }

    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }
    option = numero
    return numero
}

function MascaraNaturaleza(option, select) {
    option = option.toString()
    var opt
    var opt2
    if (document.getElementsByName(option)[0]) {
        opt = document.getElementsByName(option)[0]
    } else if (document.getElementById(option)) {
        opt = document.getElementById(option)
    }

    if (document.getElementsByName(select)[0]) {
        opt2 = document.getElementsByName(select)[0]
    } else if (document.getElementById(select)) {
        opt2 = document.getElementById(select)
    }
    if (opt.selectedIndex == 1) {
        opt2.selectedIndex = 0
        opt2.disabled = true
    } else {
        opt2.selectedIndex = 1
        opt2.disabled = false

    }
}

function MascaraModelo(option) {
    option = option.toString()
    var fecha = new Date();
    var ano = fecha.getFullYear();
    var valmax = parseInt(ano) + 2
    var valmin = 1900
    var opt
    var numeroarray = []
    var numero = ''

    for (var i = 0; i < option.length; i++) {
        patron = /[0123456789]/
        if (patron.test(option[i])) {
            if (patron.test(option[i]) == ' ') {
                option[i] = '';
            } else {
                numeroarray[i] = option[i]
            }
        }
    }
    for (var i = 0; i < numeroarray.length; i++) {
        numero = numero + numeroarray[i]
    }

    if (valmax > parseInt(numero) || numero.length == 0) {
        option = numero
    } else if (valmax < parseInt(numero)) {
        option = valmax
    }
    return option


}
var tempvalor = 0
function MascaraValores(option) {
    var numero = MascaraDecimales(option)
    if (numero.split('.').length == 2) {
        numero = formatNumber.new(numero.split('.')[0]).toString() + '.' + numero.split('.')[1].toString()
    } else {
        numero = formatNumber.new(numero)
    }
    if (numero == "NaN") {
        numero = ''
    }
    return numero
}

var formatNumber = {
    separador: ",", // separador para los miles
    sepDecimal: '.', // separador para los decimales
    formatear: function (num) {
        num += '';
        var splitStr = num.split(',');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
        }
        return this.simbol + splitLeft + splitRight;
    },
    new: function (num, simbol) {
        this.simbol = simbol || '';
        return this.formatear(num);
    }
}


function ValidarHoras(horaInicio, NombreControl) {
    var mensaje = ''
    var continuar = true
    if (horaInicio !== undefined && horaInicio !== '') {
        try {
            var arrHora = horaInicio.split(":");
            var hora = arrHora[0];
            var minutos = arrHora[1];
            if (parseInt(hora) < 0 || parseInt(hora) > 23) {
                mensaje = 'La hora ' + NombreControl + '  no tiene el formato correcto';
                continuar = false;
            }

            if (continuar == true) {
                if (parseInt(minutos) < 0 || parseInt(minutos) > 59) {
                    mensaje = 'La hora ' + NombreControl + '  no tiene el formato correcto';
                    continuar = false;
                }
            }
        }
        catch (err) {
            mensaje = 'La hora  ' + NombreControl + ' no tiene el formato correcto';
            continuar = false;
        }
    }
    return mensaje
}


function RetornarHoraFecha(Fecha, Hora) {
    var hora = angular.copy(Hora);
    var fecha = angular.copy(Fecha);
    var arrHora = hora.split(":");
    var fechaHora = new Date(fecha.getFullYear(), fecha.getMonth(), fecha.getDate(), arrHora[0], arrHora[1]);
    fechaHora = RetornarFechaEspecificaSinTimeZone(fechaHora)
    return fechaHora
}



function RetornaFechaMasDia(fecha) {
    var temp = fecha;
    fecha.setDate(temp.getDate() + 1);
    fecha.setHours(0);
    fecha.setMinutes(0);
    fecha.setMilliseconds(0);

    return fecha;
}

function RetornaFechaSinHoras(fecha) {
    var temp = RetornarFechaEspecifica(fecha);
    temp.setHours(0);
    temp.setMinutes(0);
    temp.setSeconds(0);
    temp.setMilliseconds(0);
    return temp;
}

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

/* JO: 13-DIC-2016 V1

Función que extrae la sólo fecha de un dato tipo Datetime (Fecha y Hora) en el formato dd/MM/yyyy

Ejemplo Llamada: $scope.Fecha = ExtraerFecha($scope.FechaEntrada, $scope.FechaSalida);

NOTA: el $scope.Fecha debe estar en formato date, puede utilizar Fecha = new Date(Fecha) y luego 
convertirla a la fecha especifica quitandole la zona horaria, lo puede hacer utilizar la función "RetornarFechaEspecifica"
Ejemplo completo:

                    var FechaAux = new Date($scope.ModalFechaHora);
                    FechaAux = RetornarFechaEspecifica(FechaAux);
                    var FechaConfirma = ExtraerFecha(FechaAux, FechaConfirma);

*/
function ExtraerFecha(EntradaFecha) {
    var DevuelveFecha = ''
    var Dia = EntradaFecha.getDate();
    var Mes = EntradaFecha.getMonth() + 1;
    var Anio = EntradaFecha.getFullYear();

    if (Dia < 10) { Dia = '0' + Dia }
    if (Mes < 10) { Mes = '0' + Mes }

    //Al mes le suma un 1 debido a que se interpreta Enero como el mes 0
    DevuelveFecha = Dia + '/' + Mes + '/' + Anio;

    return DevuelveFecha;

};

function RetornarFechaMasHora(Hora, Fecha) {
    var Fecha = RetornarFechaEspecifica(Fecha)
    var hora = angular.copy(Hora);
    var fecha = angular.copy(Fecha);
    var arrHora = hora.split(":");
    var fechaHora = new Date(fecha.getFullYear(), fecha.getMonth(), fecha.getDate(), arrHora[0], arrHora[1]);

    return fechaHora;
}

function RetornarHoraMinutos(Fecha) {
    var Fecha = RetornarFechaEspecifica(Fecha)
    var arrHora = Fecha
    hora = arrHora.getHours()
    minuto = arrHora.getMinutes()

    if (hora < 10 && hora >= 0) {
        hora = ("0" + hora)
    }
    if (minuto < 10 && minuto > 0) {
        minuto = ("0" + minuto)
    }
    if (minuto == 0) {
        minuto = (minuto + "0")
    }
    var hora = (hora + ":" + minuto);

    return hora
}



/* JO: 13-DIC-2016 V1

Función que extrae la sólo la hora de un dato tipo Datetime (Fecha y Hora) en el formato HH:mm:ss (Hora militar)

Ejemplo Llamada: $scope.Fecha = ExtraerHoraMilitar($scope.FechaEntrada, $scope.HoraSalida);

NOTA: el $scope.Fecha debe estar en formato date, puede utilizar Fecha = new Date(Fecha) y luego 
convertirla a la fecha especifica quitandole la zona horaria, lo puede hacer utilizar la función "RetornarFechaEspecifica"

Ejemplo completo:

                    var FechaAux = new Date($scope.ModalFechaHora);
                    FechaAux = RetornarFechaEspecifica(FechaAux);
                    var HoraConfirma = ExtraerHoraMilitar(FechaAux, HoraConfirma);
*/
function ExtraerHoraMilitar(EntradaFecha, DevuelveHoraMilitar) {

    var Hora = EntradaFecha.getHours();
    var Minutos = EntradaFecha.getMinutes();
    var Segundos = EntradaFecha.getSeconds();

    if (Hora < 10) { Hora = '0' + Hora }
    if (Minutos < 10) { Minutos = '0' + Minutos }
    if (Segundos < 10) { Segundos = '0' + Segundos }

    DevuelveHoraMilitar = Hora + ':' + Minutos + ':' + Segundos;

    return DevuelveHoraMilitar;

};
//metodo para imprimir un div desde js
function imprSelec(muestra) {
    var ficha = document.getElementById(muestra);
    var ventimp = window.open(' ', 'popimpr');
    ventimp.document.write(ficha.innerHTML);
    var css = ventimp.document.createElement("link");
    css.setAttribute("href", "../../Css/Bootstrap/bootstrap.min.css");
    css.setAttribute("rel", "stylesheet");
    css.setAttribute("type", "text/css");
    ventimp.document.head.appendChild(css);
    ventimp.document.close();
    ventimp.print();
    ventimp.close();
}

function cargaContextoCanvas(idCanvas) {
    var elemento = document.getElementById(idCanvas);
    if (elemento && elemento.getContext) {
        var contexto = elemento.getContext('2d');
        if (contexto) {
            return contexto;
        }
    }
    return FALSE;
}
function MascaraMayus(ITEM) {
    if (ITEM !== undefined) {
        return ITEM.toUpperCase()
    }
}
function MascaraNumeroGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            var Model = ''
            var Lista = ''
            var conitinue = false
            for (var j = 0; j < a[i].attributes.length; j++) {
                if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskNumero()')) {
                    conitinue = true
                    break;
                }
            } if (conitinue) {

                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-model') {
                        Model = a[i].attributes[j].value

                    } else if (a[i].attributes[j].name == 'lista') {
                        Lista = a[i].attributes[j].value
                    }
                }

                var b = Model.split('.')
                switch (b.length) {
                    case 1:
                        if (sc[b[0]] !== undefined) {
                            try { sc[b[0]] = MascaraNumero(sc[b[0]]) } catch (e) { }
                        }
                        break;
                    case 2:
                        if (sc[b[0]][b[1]] !== undefined) {
                            try { sc[b[0]][b[1]] = MascaraNumero(sc[b[0]][b[1]]) } catch (e) { }
                        }
                        break;
                    case 3:
                        if (sc[b[0]][b[1]][b[2]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]] = MascaraNumero(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                        }
                        break;
                    case 4:
                        if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraNumero(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                        }
                        break;
                    case 5:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraNumero(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                        }
                        break;
                    case 6:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraNumero(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                        }
                        break;
                    case 7:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraNumero(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                        }
                        break;
                }

            }
        }
    }
}
function MascaraMayusGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            var Model = ''
            var Lista = ''
            var conitinue = false
            for (var j = 0; j < a[i].attributes.length; j++) {
                if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskMayus()')) {
                    conitinue = true
                    break;
                }
            } if (conitinue) {
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-model') {
                        Model = a[i].attributes[j].value

                    } else if (a[i].attributes[j].name == 'lista') {
                        Lista = a[i].attributes[j].value
                    }
                }
                var b = Model.split('.')
                switch (b.length) {
                    case 1:
                        if (sc[b[0]] !== undefined) {
                            try { sc[b[0]] = MascaraMayus(sc[b[0]]) } catch (e) { }
                        }
                        break;
                    case 2:
                        if (sc[b[0]][b[1]] !== undefined) {
                            try { sc[b[0]][b[1]] = MascaraMayus(sc[b[0]][b[1]]) } catch (e) { }
                        }
                        break;
                    case 3:
                        if (sc[b[0]][b[1]][b[2]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]] = MascaraMayus(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                        }
                        break;
                    case 4:
                        if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraMayus(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                        }
                        break;
                    case 5:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraMayus(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                        }
                        break;
                    case 6:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraMayus(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                        }
                        break;
                    case 7:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraMayus(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                        }
                        break;
                }
            }
        }
    }
}
function MascaraValoresGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            var Model = ''
            var Lista = ''
            var conitinue = false
            for (var j = 0; j < a[i].attributes.length; j++) {
                if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskValores()')) {
                    conitinue = true
                    break;
                }
            } if (conitinue) {
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-model') {
                        Model = a[i].attributes[j].value

                    } else if (a[i].attributes[j].name == 'lista') {
                        Lista = a[i].attributes[j].value
                    }
                }
                var b = Model.split('.')
                switch (b.length) {
                    case 1:
                        if (sc[b[0]] !== undefined) {
                            try { sc[b[0]] = MascaraValores(sc[b[0]]) } catch (e) { }
                        }
                        break;
                    case 2:
                        if (sc[b[0]][b[1]] !== undefined) {
                            try { sc[b[0]][b[1]] = MascaraValores(sc[b[0]][b[1]]) } catch (e) { }
                        }
                        break;
                    case 3:
                        if (sc[b[0]][b[1]][b[2]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]] = MascaraValores(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                        }
                        break;
                    case 4:
                        if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraValores(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                        }
                        break;
                    case 5:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraValores(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                        }
                        break;
                    case 6:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraValores(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                        }
                        break;
                    case 7:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraValores(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                        }
                        break;
                }
            }
        }
    }
}
function MascaraHorasGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            try {
                var Model = ''
                var Lista = ''
                var conitinue = false
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskHora()')) {
                        conitinue = true
                        break;
                    }
                } if (conitinue) {
                    for (var j = 0; j < a[i].attributes.length; j++) {
                        if (a[i].attributes[j].name == 'ng-model') {
                            Model = a[i].attributes[j].value

                        } else if (a[i].attributes[j].name == 'lista') {
                            Lista = a[i].attributes[j].value
                        }
                    }
                    var b = Model.split('.')
                    switch (b.length) {
                        case 1:
                            if (sc[b[0]] !== undefined) {
                                try { sc[b[0]] = MascaraHora(sc[b[0]]) } catch (e) { }
                            }
                            break;
                        case 2:
                            if (sc[b[0]][b[1]] !== undefined) {
                                try { sc[b[0]][b[1]] = MascaraHora(sc[b[0]][b[1]]) } catch (e) { }
                            }
                            break;
                        case 3:
                            if (sc[b[0]][b[1]][b[2]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]] = MascaraHora(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                            }
                            break;
                        case 4:
                            if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraHora(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                            }
                            break;
                        case 5:
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraHora(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                            }
                            break;
                        case 6:
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraHora(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                            }
                            break;
                        case 7:
                            if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                                try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraHora(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                            }
                            break;
                    }
                }
            } catch (e) {

            }
        }
    }
}
function MascaraMonedaGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            var Model = ''
            var Lista = ''
            var conitinue = false
            for (var j = 0; j < a[i].attributes.length; j++) {
                if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskMoneda()')) {
                    conitinue = true
                    break;
                }
            } if (conitinue) {
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-model') {
                        Model = a[i].attributes[j].value

                    } else if (a[i].attributes[j].name == 'lista') {
                        Lista = a[i].attributes[j].value
                    }
                }
                var b = Model.split('.')
                switch (b.length) {
                    case 1:
                        if (sc[b[0]] !== undefined) {
                            try { sc[b[0]] = MascaraMoneda(sc[b[0]]) } catch (e) { }
                        }
                        break;
                    case 2:
                        if (sc[b[0]][b[1]] !== undefined) {
                            try { sc[b[0]][b[1]] = MascaraMoneda(sc[b[0]][b[1]]) } catch (e) { }
                        }
                        break;
                    case 3:
                        if (sc[b[0]][b[1]][b[2]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]] = MascaraMoneda(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                        }
                        break;
                    case 4:
                        if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraMoneda(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                        }
                        break;
                    case 5:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraMoneda(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                        }
                        break;
                    case 6:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraMoneda(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                        }
                        break;
                    case 7:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraMoneda(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                        }
                        break;
                }
            }
        }
    }
}
function MascaraPlacaGeneral(sc) {
    if ($('input').length > 0) {
        var a = $('input')
        for (var i = 0; i < a.length; i++) {
            var Model = ''
            var Lista = ''
            var conitinue = false
            for (var j = 0; j < a[i].attributes.length; j++) {
                if (a[i].attributes[j].name == 'ng-change' && a[i].attributes[j].value.includes('MaskPlaca()')) {
                    conitinue = true
                    break;
                }
            } if (conitinue) {
                for (var j = 0; j < a[i].attributes.length; j++) {
                    if (a[i].attributes[j].name == 'ng-model') {
                        Model = a[i].attributes[j].value

                    } else if (a[i].attributes[j].name == 'lista') {
                        Lista = a[i].attributes[j].value
                    }
                }
                var b = Model.split('.')
                switch (b.length) {
                    case 1:
                        if (sc[b[0]] !== undefined) {
                            try { sc[b[0]] = MascaraPlaca(sc[b[0]]) } catch (e) { }
                        }
                        break;
                    case 2:
                        if (sc[b[0]][b[1]] !== undefined) {
                            try { sc[b[0]][b[1]] = MascaraPlaca(sc[b[0]][b[1]]) } catch (e) { }
                        }
                        break;
                    case 3:
                        if (sc[b[0]][b[1]][b[2]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]] = MascaraPlaca(sc[b[0]][b[1]][b[2]]) } catch (e) { }
                        }
                        break;
                    case 4:
                        if (sc[b[0]][b[1]][b[2]][b[3]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]] = MascaraPlaca(sc[b[0]][b[1]][b[2]][b[3]]) } catch (e) { }
                        }
                        break;
                    case 5:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]] = MascaraPlaca(sc[b[0]][b[1]][b[2]][b[3]][b[4]]) } catch (e) { }
                        }
                        break;
                    case 6:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]] = MascaraPlaca(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]]) } catch (e) { }
                        }
                        break;
                    case 7:
                        if (sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] !== undefined) {
                            try { sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]] = MascaraPlaca(sc[b[0]][b[1]][b[2]][b[3]][b[4]][b[5]][b[6]]) } catch (e) { }
                        }
                        break;
                }
            }
        }
    }
}




function InternalServerError(Cadena) {
    var cadena = Cadena
    var TablaExistene = false
    var result = ''
    var cont = 0
    for (var i = 0; i < cadena.length; i++) {
        if (cadena[i].toUpperCase() == 'T') {
            var temp = cadena[i] + cadena[i + 1] + cadena[i + 2] + cadena[i + 3] + cadena[i + 4]
            if (temp.toUpperCase() == 'TABLE') {
                i = i + 5
                TablaExistene = true
            }
        }
        if (TablaExistene) {
            if (cadena[i] == '"') {
                cont++
            }
            else if (cont == 1) {
                result += cadena[i]
            } else if (cont == 2) {
                break
            }
        }
    }
    result = result.replace('dbo.', '')

    return result
}
function ValidarCampo(objeto, minlength, Esobjeto) {
    var resultado = 0
    if (objeto == undefined || objeto == '' || objeto == null || objeto == 0 || objeto == '0') {
        resultado = 1
    } else {
        if (resultado == 0) {
            if (minlength !== undefined) {
                if (objeto.length < minlength) {
                    resultado = 3
                }
                else {
                    resultado = 0
                }
            }
        } if (resultado == 0) {
            if (Esobjeto) {
                if (objeto.Codigo == undefined || objeto.Codigo == '' || objeto.Codigo == null || objeto.Codigo == 0) {
                    resultado = 2
                }
            }
        }
    }
    return resultado
}
function ValidarFecha(objeto, MayorActual, MenorActual) {
    var resultado = 0
    var now = new Date()
    if (objeto == undefined || objeto == '' || objeto == null) {
        resultado = 1
    } else {
        if (resultado == 0) {
            if (MayorActual) {
                if (objeto > now) {
                    resultado = 2
                }
            }
            else if (MenorActual) {
                if (objeto < now) {
                    resultado = 3
                }
            }
        }
    }
    return resultado
}
//isNaN(123) 

/* MO: 30-AGO-2018 V1

Función que compara fechas, si es mayor o igual (mayor = 1, menor = -1, igual = 0)
NOTA: se debe tener en cuenta la diferencia de zona horaria
Ejemplos:
    var ComparaFecha = (fechaIni, fechaFin)//guarda el valor en una variable
    if((fechaIni, fechaFin)){}//comparacion directa en if
*/
function ComparaFecha(fechaIni, fechaFin) {
    if (fechaIni > fechaFin)
        return FECHA_ES_MAYOR;
    else if (fechaIni < fechaFin)
        return FECHA_ES_MENOR;
    else
        return FECHA_ES_IGUAL;
    return null;
}
/* MO: 30-AGO-2018 V1

Prototipo para quitar las horas a las fechas
NOTA: se debe tener en cuenta la diferencia de zona horaria
Ejemplos:
    var date = new Date().withoutTime();
*/
Date.prototype.withoutTime = function () {
    var d = new Date(this);
    d.setHours(0, 0, 0, 0);
    return d;
}
/* MO: 31-AGO-2018 V1

Prototipo para quitar los milisegundos a las fechas
NOTA: se debe tener en cuenta la diferencia de zona horaria
Ejemplos:
    var date = new Date().withoutMilliseconds();
*/
Date.prototype.withoutMilliseconds = function () {
    var d = new Date(this);
    d.setMilliseconds(0);
    return d;
}

Date.prototype.withoutSeconds = function () {
    var d = new Date(this);
    d.setSeconds(0);
    return d;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
function RedimencionarImagen(id, img, Alto, Ancho) {
    var ctx = cargaContextoCanvas(id);
    if (ctx) {
        ctx.drawImage(img, 0, 0, Ancho, Alto);
    }
}
function OrderBy(Objeto, Atributo, Lista, Atributo2) {
    if (Lista.Asc) {
        Lista.sort(dynamicSort(Objeto, Atributo, Atributo2));
        Lista.Asc = false
    } else {
        Lista.sort(dynamicSort("-" + Objeto, Atributo, Atributo2));
        Lista.Asc = true
    }
}

//------------ Consultar listado autocomplete
//JO: retorna una lista propia para un autocomplete según los campos que desee filtrar
// Llamado: $scope.ListaAuto = RetornarListaAutocomplete(CadenaIngreso, Lista, Param1, Param2, Param3)
function RetornarListaAutocomplete(Cadena, Lista, p1, p2, p3) {

    var ListaAux = [];

    for (var i = 0; i < Lista.length; i++) {
        var Coincidencia = 0
        try {
            for (var j = 0; j < Cadena.length; j++) {
                var inserto = false
                var temp = Lista[i][p1].toString()
                if (p1 !== '' && p1 !== undefined && p1 !== null) {
                    if (temp[j] !== undefined) {
                        if (temp[j].toUpperCase() == Cadena[j].toUpperCase()) {
                            Coincidencia++
                            inserto = true
                        }
                    }
                }

                if (p2 !== '' && p2 !== undefined && p2 !== null) {
                    temp = Lista[i][p2].toString()
                    if (temp[j] !== undefined) {
                        if (temp[j].toUpperCase() == Cadena[j].toUpperCase()) {
                            if (!inserto) {
                                Coincidencia++
                                inserto = true
                            }
                        }
                    }
                }
                if (p3 !== '' && p3 !== undefined && p3 !== null) {
                    temp = Lista[i][p3].toString()
                    if (temp[j] !== undefined) {
                        if (temp[j].toUpperCase() == Cadena[j].toUpperCase()) {
                            if (!inserto) {
                                Coincidencia++
                                inserto = true
                            }
                        }
                    }
                }

            }
            if (Coincidencia == Cadena.length) {
                ListaAux.push(Lista[i]);
            }
        } catch (e) { }
    }

    return ListaAux;

}


// Función que ordena una lista por una columna especifica de mayor a menor o viceversa
function dynamicSort(property, cod, cod2) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a, b) {
        if (cod != undefined && cod != null && cod != '') {
            if (cod2 != undefined && cod2 != null && cod2 != '') {
                var result = (a[property][cod][cod2] < b[property][cod][cod2]) ? -1 : (a[property][cod][cod2] > b[property][cod][cod2]) ? 1 : 0;
            } else {
                var result = (a[property][cod] < b[property][cod]) ? -1 : (a[property][cod] > b[property][cod]) ? 1 : 0;
            }
        } else {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        }
        return result * sortOrder;
    }
}
function AgruparListados(Lista, b, c, d, e, f, g, h) {
    var a = JSON.parse(JSON.stringify(Lista));
    var Result = [];
    var cod = "Codigo";
    var dt = "Data";
    var cont = 0;
    if (b !== undefined) {
        for (var i = 0; i < a.length; i++) {
            if ((a[i][b] !== undefined && (typeof a[i][b]) !== "object") || (a[i][b][cod] !== undefined && a[i][b][cod] > 0)) {
                if (Result.length > 0) {
                    var cont = 0
                    for (var j = 0; j < Result.length; j++) {
                        if ((a[i][b][cod] == Result[j][b][cod] && a[i][b][cod] !== undefined) || (a[i][b] == Result[j][b])) {
                            cont++
                            if (c == undefined) {
                                Result[j][dt].push(a[i])
                            }
                            break;
                        }
                    }
                    if (cont == 0) {
                        if (c == undefined) {
                            Result.push({ [b]: a[i][b], [dt]: [a[i]] })
                        }
                        else {
                            Result.push({ [b]: a[i][b], [dt]: [] })
                        }
                    }
                } else {
                    if (c == undefined) {
                        Result.push({ [b]: a[i][b], [dt]: [a[i]] })
                    } else {
                        Result.push({ [b]: a[i][b], [dt]: [] })
                    }
                }
            }
        }
        if (c !== undefined) {
            for (var i = 0; i < a.length; i++) {
                if ((a[i][c] !== undefined && (typeof a[i][c]) !== "object") || (a[i][c][cod] !== undefined && a[i][c][cod] > 0)) {
                    for (var j = 0; j < Result.length; j++) {
                        if ((a[i][b][cod] == Result[j][b][cod]) || (a[i][b] == Result[j][b])) {
                            if (Result[j][dt].length > 0) {
                                var cont = 0
                                for (var k = 0; k < Result[j][dt].length; k++) {
                                    if ((a[i][c][cod] == Result[j][dt][k][c][cod] && a[i][c][cod] != undefined) || (a[i][c] == Result[j][dt][k][c])) {
                                        if (d == undefined) {
                                            Result[j][dt][k][dt].push(a[i])
                                        }
                                        cont++
                                        break;
                                    }
                                }
                                if (cont == 0) {
                                    if (d == undefined) {
                                        Result[j][dt].push({ [c]: a[i][c], [dt]: [a[i]] })
                                    }
                                    else {
                                        Result[j][dt].push({ [c]: a[i][c], [dt]: [] })
                                    }
                                }
                            }
                            else {
                                if (d == undefined) {
                                    Result[j][dt].push({ [c]: a[i][c], [dt]: [a[i]] })
                                }
                                else {
                                    Result[j][dt].push({ [c]: a[i][c], [dt]: [] })
                                }
                            }
                        }
                    }
                }

            }
            if (d !== undefined) {
                for (var i = 0; i < a.length; i++) {
                    if ((a[i][d] !== undefined && (typeof a[i][d]) !== "object") || (a[i][d][cod] !== undefined && a[i][d][cod] > 0)) {
                        for (var j = 0; j < Result.length; j++) {
                            if ((a[i][b][cod] == Result[j][b][cod]) || (a[i][b] == Result[j][b])) {
                                for (var k = 0; k < Result[j][dt].length; k++) {
                                    if ((a[i][c][cod] == Result[j][dt][k][c][cod]) || (a[i][c] == Result[j][dt][k][c])) {
                                        if (Result[j][dt][k][dt].length > 0) {
                                            var cont = 0
                                            for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod] && a[i][d][cod] != undefined) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                    if (e == undefined) {
                                                        Result[j][dt][k][dt][l][dt].push(a[i])
                                                    }
                                                    cont++
                                                }
                                            }
                                            if (cont == 0) {
                                                if (e == undefined) {
                                                    Result[j][dt][k][dt].push({ [d]: a[i][d], [dt]: [a[i]] })
                                                } else {
                                                    Result[j][dt][k][dt].push({ [d]: a[i][d], [dt]: [] })
                                                }
                                            }
                                            cont = 0
                                        }
                                        else {
                                            if (e == undefined) {
                                                Result[j][dt][k][dt].push({ [d]: a[i][d], [dt]: [a[i]] })
                                            } else {
                                                Result[j][dt][k][dt].push({ [d]: a[i][d], [dt]: [] })
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                if (e !== undefined) {
                    for (var i = 0; i < a.length; i++) {
                        if ((a[i][e] !== undefined && (typeof a[i][e]) !== "object") || (a[i][e][cod] !== undefined && a[i][e][cod] > 0)) {
                            for (var j = 0; j < Result.length; j++) {
                                if ((a[i][b][cod] == Result[j][b][cod]) || (a[i][b] == Result[j][b])) {
                                    for (var k = 0; k < Result[j][dt].length; k++) {
                                        if ((a[i][c][cod] == Result[j][dt][k][c][cod]) || (a[i][c] == Result[j][dt][k][c])) {
                                            for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod]) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                    if (Result[j][dt][k][dt][l][dt].length > 0) {
                                                        var cont = 0
                                                        for (var m = 0; m < Result[j][dt][k][dt][l][dt].length; m++) {
                                                            if ((a[i][e][cod] == Result[j][dt][k][dt][l][dt][m][e][cod] && a[i][e][cod] != undefined) || (a[i][e] == Result[j][dt][k][dt][l][dt][m][e])) {
                                                                if (f == undefined) {
                                                                    Result[j][dt][k][dt][l][dt][m][dt].push(a[i])
                                                                }
                                                                cont++
                                                            }
                                                        }
                                                        if (cont == 0) {
                                                            if (f == undefined) {
                                                                Result[j][dt][k][dt][l][dt].push({ [e]: a[i][e], [dt]: [a[i]] })
                                                            } else {
                                                                Result[j][dt][k][dt][l][dt].push({ [e]: a[i][e], [dt]: [] })
                                                            }
                                                        }
                                                        cont = 0
                                                    }
                                                    else {
                                                        if (f == undefined) {
                                                            Result[j][dt][k][dt][l][dt].push({ [e]: a[i][e], [dt]: [a[i]] })
                                                        } else {
                                                            Result[j][dt][k][dt][l][dt].push({ [e]: a[i][e], [dt]: [] })
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    if (f !== undefined) {
                        for (var i = 0; i < a.length; i++) {
                            if ((a[i][f] !== undefined && (typeof a[i][f]) !== "object") || (a[i][f][cod] !== undefined && a[i][f][cod] > 0)) {
                                for (var j = 0; j < Result.length; j++) {
                                    if ((a[i][b][cod] == Result[j][b][cod]) || (a[i][b] == Result[j][b])) {
                                        for (var k = 0; k < Result[j][dt].length; k++) {
                                            if ((a[i][c][cod] == Result[j][dt][k][c][cod]) || (a[i][c] == Result[j][dt][k][c])) {
                                                for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                    if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod]) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                        for (var m = 0; m < Result[j][dt][k][dt][l][dt].length; m++) {
                                                            if ((a[i][e][cod] == Result[j][dt][k][dt][l][dt][m][e][cod]) || (a[i][e] == Result[j][dt][k][dt][l][dt][m][e])) {
                                                                if (Result[j][dt][k][dt][l][dt][m][dt].length > 0) {
                                                                    var cont = 0
                                                                    for (var n = 0; n < Result[j][dt][k][dt][l][dt][m][dt].length; n++) {
                                                                        if ((a[i][f][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][f][cod] && a[i][f][cod] != undefined) || (a[i][f] == Result[j][dt][k][dt][l][dt][m][dt][n][f])) {
                                                                            if (g == undefined) {
                                                                                Result[j][dt][k][dt][l][dt][m][dt][n][dt].push(a[i])
                                                                            }
                                                                            cont++
                                                                        }
                                                                    }
                                                                    if (cont == 0) {
                                                                        if (g == undefined) {
                                                                            Result[j][dt][k][dt][l][dt][m][dt].push({ [f]: a[i][f], [dt]: [a[i]] })
                                                                        } else {
                                                                            Result[j][dt][k][dt][l][dt][m][dt].push({ [f]: a[i][f], [dt]: [] })
                                                                        }
                                                                    }
                                                                    cont = 0
                                                                }
                                                                else {
                                                                    if (g == undefined) {
                                                                        Result[j][dt][k][dt][l][dt][m][dt].push({ [f]: a[i][f], [dt]: [a[i]] })
                                                                    } else {
                                                                        Result[j][dt][k][dt][l][dt][m][dt].push({ [f]: a[i][f], [dt]: [] })
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        if (g !== undefined) {
                            for (var i = 0; i < a.length; i++) {
                                if ((a[i][g] !== undefined && (typeof a[i][g]) !== "object") || (a[i][g][cod] !== undefined && a[i][g][cod] > 0)) {
                                    for (var j = 0; j < Result.length; j++) {
                                        if ((a[i][b][cod] == Result[j][b][cod]) || (a[i][b] == Result[j][b])) {
                                            for (var k = 0; k < Result[j][dt].length; k++) {
                                                if ((a[i][c][cod] == Result[j][dt][k][c][cod]) || (a[i][c] == Result[j][dt][k][c])) {
                                                    for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                        if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod]) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                            for (var m = 0; m < Result[j][dt][k][dt][l][dt].length; m++) {
                                                                if ((a[i][e][cod] == Result[j][dt][k][dt][l][dt][m][e][cod]) || (a[i][e] == Result[j][dt][k][dt][l][dt][m][e])) {
                                                                    for (var n = 0; n < Result[j][dt][k][dt][l][dt][m][dt].length; n++) {
                                                                        if ((a[i][f][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][f][cod]) || (a[i][f] == Result[j][dt][k][dt][l][dt][m][dt][n][f])) {
                                                                            if (Result[j][dt][k][dt][l][dt][m][dt][n][dt].length > 0) {
                                                                                var cont = 0
                                                                                for (var o = 0; o < Result[j][dt][k][dt][l][dt][m][dt][n][dt].length; o++) {
                                                                                    if ((a[i][g][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][g][cod] && a[i][g][cod] != undefined) || (a[i][g] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][g])) {
                                                                                        if (g == undefined) {
                                                                                            Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].push(a[i])
                                                                                        }
                                                                                        cont++
                                                                                    }
                                                                                }
                                                                                if (cont == 0) {
                                                                                    if (h == undefined) {
                                                                                        Result[j][dt][k][dt][l][dt][m][dt][n][dt].push({ [g]: a[i][g], [dt]: [a[i]] })
                                                                                    } else {
                                                                                        Result[j][dt][k][dt][l][dt][m][dt][n][dt].push({ [g]: a[i][g], [dt]: [] })
                                                                                    }
                                                                                }
                                                                                cont = 0
                                                                            }
                                                                            else {
                                                                                if (h == undefined) {
                                                                                    Result[j][dt][k][dt][l][dt][m][dt][n][dt].push({ [g]: a[i][g], [dt]: [a[i]] })
                                                                                } else {
                                                                                    Result[j][dt][k][dt][l][dt][m][dt][n][dt].push({ [g]: a[i][g], [dt]: [] })
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                            if (h !== undefined) {
                                for (var i = 0; i < a.length; i++) {
                                    if ((a[i][h] !== undefined && (typeof a[i][h]) !== "object") || (a[i][h][cod] !== undefined && a[i][h][cod] > 0)) {
                                        for (var j = 0; j < Result.length; j++) {
                                            if ((a[i][b][cod] == Result[j][b][cod]) || (a[i][b] == Result[j][b])) {
                                                for (var k = 0; k < Result[j][dt].length; k++) {
                                                    if ((a[i][c][cod] == Result[j][dt][k][c][cod]) || (a[i][c] == Result[j][dt][k][c])) {
                                                        for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                            if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod]) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                                for (var m = 0; m < Result[j][dt][k][dt][l][dt].length; m++) {
                                                                    if ((a[i][e][cod] == Result[j][dt][k][dt][l][dt][m][e][cod]) || (a[i][e] == Result[j][dt][k][dt][l][dt][m][e])) {
                                                                        for (var n = 0; n < Result[j][dt][k][dt][l][dt][m][dt].length; n++) {
                                                                            if ((a[i][f][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][f][cod]) || (a[i][f] == Result[j][dt][k][dt][l][dt][m][dt][n][f])) {
                                                                                for (var o = 0; o < Result[j][dt][k][dt][l][dt][m][dt][n][dt].length; o++) {
                                                                                    if ((a[i][g][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][g][cod]) || (a[i][g] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][g])) {
                                                                                        if (Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].length > 0) {
                                                                                            var cont = 0
                                                                                            for (var p = 0; p < Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].length; p++) {
                                                                                                if ((a[i][h][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt][p][h][cod] && a[i][h][cod] != undefined) || (a[i][h] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt][p][h])) {
                                                                                                    Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt][p][dt].push(a[i])
                                                                                                    cont++
                                                                                                }
                                                                                            }
                                                                                            if (cont == 0) {
                                                                                                Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].push({ [h]: a[i][h], [dt]: [a[i]] })
                                                                                            }
                                                                                            cont = 0
                                                                                        }
                                                                                        else {
                                                                                            Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].push({ [h]: a[i][h], [dt]: [a[i]] })
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return Result
    //derechos reservados--
}
function AgruparListadosDocumentos(Lista, b, c, d, e, f, g, h) {
    var a = JSON.parse(JSON.stringify(Lista));
    var Result = [];
    var cod = "Numero";
    var dt = "Data";
    var cont = 0;
    if (b !== undefined) {
        for (var i = 0; i < a.length; i++) {
            if ((a[i][b] !== undefined && (typeof a[i][b]) !== "object") || (a[i][b][cod] !== undefined && a[i][b][cod] > 0)) {
                if (Result.length > 0) {
                    var cont = 0
                    for (var j = 0; j < Result.length; j++) {
                        if ((a[i][b][cod] == Result[j][b][cod] && a[i][b][cod] !== undefined) || (a[i][b] == Result[j][b])) {
                            cont++
                            if (c == undefined) {
                                Result[j][dt].push(a[i])
                            }
                            break;
                        }
                    }
                    if (cont == 0) {
                        if (c == undefined) {
                            Result.push({ [b]: a[i][b], [dt]: [a[i]] })
                        }
                        else {
                            Result.push({ [b]: a[i][b], [dt]: [] })
                        }
                    }
                } else {
                    if (c == undefined) {
                        Result.push({ [b]: a[i][b], [dt]: [a[i]] })
                    } else {
                        Result.push({ [b]: a[i][b], [dt]: [] })
                    }
                }
            }
        }
        if (c !== undefined) {
            for (var i = 0; i < a.length; i++) {
                if ((a[i][c] !== undefined && (typeof a[i][c]) !== "object") || (a[i][c][cod] !== undefined && a[i][c][cod] > 0)) {
                    for (var j = 0; j < Result.length; j++) {
                        if ((a[i][b][cod] == Result[j][b][cod]) || (a[i][b] == Result[j][b])) {
                            if (Result[j][dt].length > 0) {
                                var cont = 0
                                for (var k = 0; k < Result[j][dt].length; k++) {
                                    if ((a[i][c][cod] == Result[j][dt][k][c][cod] && a[i][c][cod] != undefined) || (a[i][c] == Result[j][dt][k][c])) {
                                        if (d == undefined) {
                                            Result[j][dt][k][dt].push(a[i])
                                        }
                                        cont++
                                        break;
                                    }
                                }
                                if (cont == 0) {
                                    if (d == undefined) {
                                        Result[j][dt].push({ [c]: a[i][c], [dt]: [a[i]] })
                                    }
                                    else {
                                        Result[j][dt].push({ [c]: a[i][c], [dt]: [] })
                                    }
                                }
                            }
                            else {
                                if (d == undefined) {
                                    Result[j][dt].push({ [c]: a[i][c], [dt]: [a[i]] })
                                }
                                else {
                                    Result[j][dt].push({ [c]: a[i][c], [dt]: [] })
                                }
                            }
                        }
                    }
                }

            }
            if (d !== undefined) {
                for (var i = 0; i < a.length; i++) {
                    if ((a[i][d] !== undefined && (typeof a[i][d]) !== "object") || (a[i][d][cod] !== undefined && a[i][d][cod] > 0)) {
                        for (var j = 0; j < Result.length; j++) {
                            if ((a[i][b][cod] == Result[j][b][cod]) || (a[i][b] == Result[j][b])) {
                                for (var k = 0; k < Result[j][dt].length; k++) {
                                    if ((a[i][c][cod] == Result[j][dt][k][c][cod]) || (a[i][c] == Result[j][dt][k][c])) {
                                        if (Result[j][dt][k][dt].length > 0) {
                                            var cont = 0
                                            for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod] && a[i][d][cod] != undefined) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                    if (e == undefined) {
                                                        Result[j][dt][k][dt][l][dt].push(a[i])
                                                    }
                                                    cont++
                                                }
                                            }
                                            if (cont == 0) {
                                                if (e == undefined) {
                                                    Result[j][dt][k][dt].push({ [d]: a[i][d], [dt]: [a[i]] })
                                                } else {
                                                    Result[j][dt][k][dt].push({ [d]: a[i][d], [dt]: [] })
                                                }
                                            }
                                            cont = 0
                                        }
                                        else {
                                            if (e == undefined) {
                                                Result[j][dt][k][dt].push({ [d]: a[i][d], [dt]: [a[i]] })
                                            } else {
                                                Result[j][dt][k][dt].push({ [d]: a[i][d], [dt]: [] })
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                if (e !== undefined) {
                    for (var i = 0; i < a.length; i++) {
                        if ((a[i][e] !== undefined && (typeof a[i][e]) !== "object") || (a[i][e][cod] !== undefined && a[i][e][cod] > 0)) {
                            for (var j = 0; j < Result.length; j++) {
                                if ((a[i][b][cod] == Result[j][b][cod]) || (a[i][b] == Result[j][b])) {
                                    for (var k = 0; k < Result[j][dt].length; k++) {
                                        if ((a[i][c][cod] == Result[j][dt][k][c][cod]) || (a[i][c] == Result[j][dt][k][c])) {
                                            for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod]) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                    if (Result[j][dt][k][dt][l][dt].length > 0) {
                                                        var cont = 0
                                                        for (var m = 0; m < Result[j][dt][k][dt][l][dt].length; m++) {
                                                            if ((a[i][e][cod] == Result[j][dt][k][dt][l][dt][m][e][cod] && a[i][e][cod] != undefined) || (a[i][e] == Result[j][dt][k][dt][l][dt][m][e])) {
                                                                if (f == undefined) {
                                                                    Result[j][dt][k][dt][l][dt][m][dt].push(a[i])
                                                                }
                                                                cont++
                                                            }
                                                        }
                                                        if (cont == 0) {
                                                            if (f == undefined) {
                                                                Result[j][dt][k][dt][l][dt].push({ [e]: a[i][e], [dt]: [a[i]] })
                                                            } else {
                                                                Result[j][dt][k][dt][l][dt].push({ [e]: a[i][e], [dt]: [] })
                                                            }
                                                        }
                                                        cont = 0
                                                    }
                                                    else {
                                                        if (f == undefined) {
                                                            Result[j][dt][k][dt][l][dt].push({ [e]: a[i][e], [dt]: [a[i]] })
                                                        } else {
                                                            Result[j][dt][k][dt][l][dt].push({ [e]: a[i][e], [dt]: [] })
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                    if (f !== undefined) {
                        for (var i = 0; i < a.length; i++) {
                            if ((a[i][f] !== undefined && (typeof a[i][f]) !== "object") || (a[i][f][cod] !== undefined && a[i][f][cod] > 0)) {
                                for (var j = 0; j < Result.length; j++) {
                                    if ((a[i][b][cod] == Result[j][b][cod]) || (a[i][b] == Result[j][b])) {
                                        for (var k = 0; k < Result[j][dt].length; k++) {
                                            if ((a[i][c][cod] == Result[j][dt][k][c][cod]) || (a[i][c] == Result[j][dt][k][c])) {
                                                for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                    if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod]) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                        for (var m = 0; m < Result[j][dt][k][dt][l][dt].length; m++) {
                                                            if ((a[i][e][cod] == Result[j][dt][k][dt][l][dt][m][e][cod]) || (a[i][e] == Result[j][dt][k][dt][l][dt][m][e])) {
                                                                if (Result[j][dt][k][dt][l][dt][m][dt].length > 0) {
                                                                    var cont = 0
                                                                    for (var n = 0; n < Result[j][dt][k][dt][l][dt][m][dt].length; n++) {
                                                                        if ((a[i][f][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][f][cod] && a[i][f][cod] != undefined) || (a[i][f] == Result[j][dt][k][dt][l][dt][m][dt][n][f])) {
                                                                            if (g == undefined) {
                                                                                Result[j][dt][k][dt][l][dt][m][dt][n][dt].push(a[i])
                                                                            }
                                                                            cont++
                                                                        }
                                                                    }
                                                                    if (cont == 0) {
                                                                        if (g == undefined) {
                                                                            Result[j][dt][k][dt][l][dt][m][dt].push({ [f]: a[i][f], [dt]: [a[i]] })
                                                                        } else {
                                                                            Result[j][dt][k][dt][l][dt][m][dt].push({ [f]: a[i][f], [dt]: [] })
                                                                        }
                                                                    }
                                                                    cont = 0
                                                                }
                                                                else {
                                                                    if (g == undefined) {
                                                                        Result[j][dt][k][dt][l][dt][m][dt].push({ [f]: a[i][f], [dt]: [a[i]] })
                                                                    } else {
                                                                        Result[j][dt][k][dt][l][dt][m][dt].push({ [f]: a[i][f], [dt]: [] })
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        if (g !== undefined) {
                            for (var i = 0; i < a.length; i++) {
                                if ((a[i][g] !== undefined && (typeof a[i][g]) !== "object") || (a[i][g][cod] !== undefined && a[i][g][cod] > 0)) {
                                    for (var j = 0; j < Result.length; j++) {
                                        if ((a[i][b][cod] == Result[j][b][cod]) || (a[i][b] == Result[j][b])) {
                                            for (var k = 0; k < Result[j][dt].length; k++) {
                                                if ((a[i][c][cod] == Result[j][dt][k][c][cod]) || (a[i][c] == Result[j][dt][k][c])) {
                                                    for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                        if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod]) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                            for (var m = 0; m < Result[j][dt][k][dt][l][dt].length; m++) {
                                                                if ((a[i][e][cod] == Result[j][dt][k][dt][l][dt][m][e][cod]) || (a[i][e] == Result[j][dt][k][dt][l][dt][m][e])) {
                                                                    for (var n = 0; n < Result[j][dt][k][dt][l][dt][m][dt].length; n++) {
                                                                        if ((a[i][f][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][f][cod]) || (a[i][f] == Result[j][dt][k][dt][l][dt][m][dt][n][f])) {
                                                                            if (Result[j][dt][k][dt][l][dt][m][dt][n][dt].length > 0) {
                                                                                var cont = 0
                                                                                for (var o = 0; o < Result[j][dt][k][dt][l][dt][m][dt][n][dt].length; o++) {
                                                                                    if ((a[i][g][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][g][cod] && a[i][g][cod] != undefined) || (a[i][g] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][g])) {
                                                                                        if (g == undefined) {
                                                                                            Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].push(a[i])
                                                                                        }
                                                                                        cont++
                                                                                    }
                                                                                }
                                                                                if (cont == 0) {
                                                                                    if (h == undefined) {
                                                                                        Result[j][dt][k][dt][l][dt][m][dt][n][dt].push({ [g]: a[i][g], [dt]: [a[i]] })
                                                                                    } else {
                                                                                        Result[j][dt][k][dt][l][dt][m][dt][n][dt].push({ [g]: a[i][g], [dt]: [] })
                                                                                    }
                                                                                }
                                                                                cont = 0
                                                                            }
                                                                            else {
                                                                                if (h == undefined) {
                                                                                    Result[j][dt][k][dt][l][dt][m][dt][n][dt].push({ [g]: a[i][g], [dt]: [a[i]] })
                                                                                } else {
                                                                                    Result[j][dt][k][dt][l][dt][m][dt][n][dt].push({ [g]: a[i][g], [dt]: [] })
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                            if (h !== undefined) {
                                for (var i = 0; i < a.length; i++) {
                                    if ((a[i][h] !== undefined && (typeof a[i][h]) !== "object") || (a[i][h][cod] !== undefined && a[i][h][cod] > 0)) {
                                        for (var j = 0; j < Result.length; j++) {
                                            if ((a[i][b][cod] == Result[j][b][cod]) || (a[i][b] == Result[j][b])) {
                                                for (var k = 0; k < Result[j][dt].length; k++) {
                                                    if ((a[i][c][cod] == Result[j][dt][k][c][cod]) || (a[i][c] == Result[j][dt][k][c])) {
                                                        for (var l = 0; l < Result[j][dt][k][dt].length; l++) {
                                                            if ((a[i][d][cod] == Result[j][dt][k][dt][l][d][cod]) || (a[i][d] == Result[j][dt][k][dt][l][d])) {
                                                                for (var m = 0; m < Result[j][dt][k][dt][l][dt].length; m++) {
                                                                    if ((a[i][e][cod] == Result[j][dt][k][dt][l][dt][m][e][cod]) || (a[i][e] == Result[j][dt][k][dt][l][dt][m][e])) {
                                                                        for (var n = 0; n < Result[j][dt][k][dt][l][dt][m][dt].length; n++) {
                                                                            if ((a[i][f][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][f][cod]) || (a[i][f] == Result[j][dt][k][dt][l][dt][m][dt][n][f])) {
                                                                                for (var o = 0; o < Result[j][dt][k][dt][l][dt][m][dt][n][dt].length; o++) {
                                                                                    if ((a[i][g][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][g][cod]) || (a[i][g] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][g])) {
                                                                                        if (Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].length > 0) {
                                                                                            var cont = 0
                                                                                            for (var p = 0; p < Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].length; p++) {
                                                                                                if ((a[i][h][cod] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt][p][h][cod] && a[i][h][cod] != undefined) || (a[i][h] == Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt][p][h])) {
                                                                                                    Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt][p][dt].push(a[i])
                                                                                                    cont++
                                                                                                }
                                                                                            }
                                                                                            if (cont == 0) {
                                                                                                Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].push({ [h]: a[i][h], [dt]: [a[i]] })
                                                                                            }
                                                                                            cont = 0
                                                                                        }
                                                                                        else {
                                                                                            Result[j][dt][k][dt][l][dt][m][dt][n][dt][o][dt].push({ [h]: a[i][h], [dt]: [a[i]] })
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return Result
    //derechos reservados--
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user = getCookie("username");
    if (user != "") {

    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}
function rand_code(chars, lon) {
    code = "";
    for (x = 0; x < lon; x++) {
        rand = Math.floor(Math.random() * chars.length);
        code += chars.substr(rand, 1);
    }
    return code;
}

function GenerarLLave() {
    chars = '1234567890abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ'
    code = "";
    for (x = 0; x < 20; x++) {
        rand = Math.floor(Math.random() * chars.length);
        code += chars.substr(rand, 1);
    }
    return code;
}
caracteres = "0123456789abcdefABCDEF?¿¡!:;";
longitud = 20;
//devuelve una cadena aleatoria de 20 caracteres
function ObjetoEnListado(Objeto, Listado) {
    var resultado = false
    try {
        for (var i = 0; i < Listado.length; i++) {
            if (Objeto.Codigo !== undefined) {
                if (Listado[i].Codigo !== undefined) {
                    if (Objeto.Codigo = Listado[i].Codigo) {
                        resultado = true
                        break
                    }
                } else {
                    if (Objeto.Codigo = Listado[i]) {
                        resultado = true
                        break
                    }
                }

            } else {
                if (Listado[i].Codigo !== undefined) {
                    if (Objeto = Listado[i].Codigo) {
                        resultado = true
                        break
                    }
                } else {
                    if (Objeto = Listado[i]) {
                        resultado = true
                        break
                    }
                }
            }
        }
    } catch (e) {
        resultado = false
    }
    return resultado
}
/*Objteto a String con salto de line <br>
*Ingreso: objeto
Salida: string con atriibutos y valor => atributo: valor<br>
MO: 14/08/2018*/
function JsObjToString(obj) {
    var string = "";
    for (var i in obj) {
        string += i + ": " + obj[i] + "<br>";
    }
    return string;
}
/*Obtener QRCode
*Ingreso: texto
Salida: QRCode codificado en base 64
MO: 15/08/2018*/
var qrcodeGenerator = function (text) {
    /*-TypeNumber: Define la cantidad de bytes que se pueden ingresar(14 para 902
    caracteres)
    -errorCorrelationLevel: Calidad del QR, mejora los pixeles de lectura,
    pero disminuye la cantidad de bytes de entrega
    {
        L=>bajo(7%),
        M=>bajo(15%),
        Q=>bajo(25%),
        H=>bajo(30%),
    }
    -modos: el modo que de los datos ingresados(dejar en bytes como default)
    -Multibyte: el charset que se desea manejar con los datos(dejar en UTF-8
    para caracteres especiales)
    */
    try {
        if (text.length > 902) {
            throw "La cantidad del texto excede la capacidad del qr";
        }
        var TypeNumber = '14';
        var errorCorrelationLevel = ['L', 'M', 'Q', 'H'];
        var modos = ['Numeric', 'Alphanumeric', 'Byte', 'Kanji'];
        var Multibyte = ['default', 'SJIS', 'UTF-8'];
        text = text.replace(/^[\s\u3000]+|[\s\u3000]+$/g, '');
        return create_qrcode(text, TypeNumber, errorCorrelationLevel[1], modos[2], Multibyte[2]);
    }
    catch (err) {
        console.error("Error al crear QR: ", err);
    }
    //-- Sumar dias a una fecha
    Date.prototype.addDays = function (days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    }
};

function ajaxRequest(request) {
    var JQDataSend = $.ajax({
        type: "POST",
        url: request.url,
        async: false,
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        dataType: 'JSON',
        data: JSON.stringify(request.data),
    });
    return JSON.parse(JQDataSend.responseText)
}
function ValidarListadoAutocomplete(ListaConsulta, ListaAutocomplete) {
    if (ListaAutocomplete.length > 0) {
        for (var j = 0; j < ListaConsulta.length; j++) {
            var count = 0
            for (var i = 0; i < ListaAutocomplete.length; i++) {
                if (ListaConsulta[j].Codigo == ListaAutocomplete[i].Codigo) {
                    count++
                    break;
                }
            }
            if (count == 0) {
                ListaAutocomplete.push(ListaConsulta[j])
            }
        }
    } else {
        ListaAutocomplete = ListaConsulta;
    }
    return ListaAutocomplete
}

function ShowNotificacionInfo(Mensaje) {
    var notificator = new Notification(document.querySelector('.notification'));
    notificator.info(Mensaje);
}
function ShowNotificacionWarning(Mensaje) {
    var notificator = new Notification(document.querySelector('.notification'));
    notificator.warning(Mensaje);
}
function ShowNotificacionError(Mensaje) {
    var notificator = new Notification(document.querySelector('.notification'));
    notificator.error(Mensaje);
}
//Validaciones
SofttoolsApp.controller("InicioCtrl", ['$scope','$linq', function ($scope,$linq) {
    $scope.Nada = ''
}]);
(function () {
    'use strict';
    SofttoolsApp.controller("MainCtrl", ['$scope', '$timeout', '$linq', 'EmpresasFactory', 'SeguridadUsuariosFactory', 'authFactory', 'blockUI', 'blockUIConfig', 'localStorageService', 'TercerosFactory', 'ColorVehiculosFactory', 'CiudadesFactory', 'UsuariosFactory', 'VehiculosFactory',
        function ($scope, $timeout, $linq, EmpresasFactory, SeguridadUsuariosFactory, authFactory, blockUI, blockUIConfig, localStorageService, TercerosFactory, ColorVehiculosFactory, CiudadesFactory, UsuariosFactory, VehiculosFactory) {
            /*Modelo*/
            $scope.URL = document.URL
            $scope.Sesion = {
                UsuarioAutenticado: {}, Identificador: '', Contrasena: '', FilterLst: RetornarListaAutocomplete, Empresa: {}
            };
            $scope.Titulo = '';
            $scope.FondoPantalla = false;
            $scope.AnoActual = new Date().getFullYear();
            $scope.EstiloContenido = '';
            $scope.ListadoMenu = [];
            $scope.ListadoMenuListados = [];
            $scope.ListaModulo = [];
            $scope.MensajesErrorCambioClave = [];
            $scope.ListaValidaciones = [];
            $scope.ListadoPermisos = [];
            $scope.ImagenFondoPantalla = '';
            $scope.ImagenLogo = '';
            $scope.Banerstyle = 'Display:none'
            $scope.MensajeBaner = ''
            $('#NombreCaja').hide()
            $('#baner').hide()
            $scope.styleTHG = ''
            document.location.href = '#';
            var Prefijo = ''
            var CodigoRegional = ""
            var añohabilitacionministerio = ""
            var NumeroResolucion = ""
            $scope.ContIntentos = 0
            /*Cargar el combo de empresas*/
            EmpresasFactory.Consultar({}).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoEmpresas = response.data.Datos;
                        $scope.Empresa = $scope.ListadoEmpresas[0];
                        Prefijo = $scope.Empresa.Prefijo
                        $scope.Sesion.UsuarioAutenticado.Prefijo = $scope.Empresa.Prefijo
                        if (Prefijo !== 'TRGHE') {
                            $scope.styleTHG = 'Display:none'
                        }
                        CodigoRegional = $scope.Empresa.CodigoRegional
                        añohabilitacionministerio = $scope.Empresa.AnioHabilitacion
                        NumeroResolucion = $scope.Empresa.NumeroResolucion
                        if (screen.width >= 1024) {
                            $scope.FondoPantalla = false;
                            $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_' + Prefijo + '.jpg'                            
                        }
                        else if (screen.width < 1024) {
                            $scope.FondoPantalla = true;
                            //$scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_Movil.jpg'
                        }

                        $scope.ImagenLogo = '../../Css/img/Logo_' + Prefijo + '.jpg'

                        var d = new Date()
                        //setCookie('UsuarioEmpresa', '2', 1)
                        //setCookie('Identificador', 'admin', 1)
                        //setCookie('Contrasena', 'adminges9)', 1)
                        $scope.ver = d.getFullYear().toString() + '.' + (d.getMonth() + 1).toString() + '.' + d.getDate() + '.' + d.getSeconds
                        if (getCookie('UsuarioEmpresa') !== "" && getCookie('UsuarioEmpresa') !== undefined && getCookie('UsuarioEmpresa') !== 0
                            && getCookie('Identificador') !== "" && getCookie('Identificador') !== undefined && getCookie('Identificador') !== 0
                            && getCookie('Contrasena') !== "" && getCookie('Contrasena') !== undefined && getCookie('Contrasena') !== 0
                        ) {
                            $scope.Sesion.Identificador = getCookie('Identificador').toString()
                            $scope.Sesion.Contrasena = getCookie('Contrasena').toString()
                            $scope.Empresa = $linq.Enumerable().From($scope.ListadoEmpresas).First('$.Codigo ==' + getCookie('UsuarioEmpresa').toString());
                            $scope.ValidarUsuarioAlterno()
                            //$scope.ValidarUsuario()
                        } else {
                            $scope.Autorizado = false;
                            $scope.NoAutorizado = true;
                        }
                        $scope.AsignarLogos()

                    }
                }, function (response) {
                    throw new Error('No se pueden cargar las empresas. Contacte con el administrador del sistema.');
                    $scope.Autorizado = false;
                    $scope.NoAutorizado = true;
                });

            $scope.AsignarLogos = function () {
                if (screen.width >= 1024) {
                    $scope.FondoPantalla = false;
                    $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_' + $scope.Empresa.Prefijo + '.jpg'                    
                }
                else if (screen.width < 1024) {
                    $scope.FondoPantalla = true;
                    //$scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_Movil.jpg'
                }
                
                $scope.ImagenLogo = '../../Css/img/Logo_' + $scope.Empresa.Prefijo + '.jpg'
            }


            $scope.ValidarEmpresa = function (Empresa) {
                $scope.Empresa = Empresa
            }

            $scope.ValidarUsuario = function () {

                var usuarioEmpresa = $scope.Sesion.Identificador + "|" + $scope.Empresa.Codigo;
                $scope.Sesion.Empresa = $scope.Empresa
                $scope.loginData = {
                    userName: usuarioEmpresa,
                    password: $scope.Sesion.Contrasena,
                };
                authFactory.login($scope.loginData).then(function (response) {
                    if (response.data.access_token != undefined) {
                        $scope.Autorizado = true;
                        $scope.NoAutorizado = false;
                        $scope.Sesion.DatosToken = response;
                        setCookie('UsuarioEmpresa', $scope.Empresa.Codigo, 1)
                        setCookie('Identificador', $scope.Sesion.Identificador, 1)
                        setCookie('Contrasena', $scope.Sesion.Contrasena, 1)
                        ObtenerInformacionUsuario();
                        $scope.EstiloContenido = 'hold-transition skin-blue sidebar-mini';
                        $scope.Banerstyle = 'color:white'
                    } else {
                        ShowWarning('Advertencia', response.data.MensajeOperacion);
                        $scope.Autorizado = false;
                        $scope.NoAutorizado = true;
                    }
                },
                    function (err) {
                        try {
                            if (err.data.error_description == undefined || err.data.error_description == null) {
                                ShowError('Error intentando iniciar sesión');
                                $scope.Autorizado = false;
                                $scope.NoAutorizado = true;
                            }
                            else {
                                ShowError(err.data.error_description);
                                $scope.Autorizado = false;
                                $scope.NoAutorizado = true;
                            }
                        } catch (e) {
                        }

                    });

            };

            $scope.ValidarUsuarioAlterno = function () {

                var usuarioEmpresa = $scope.Sesion.Identificador + "|" + $scope.Empresa.Codigo;
                $scope.Sesion.Empresa = $scope.Empresa
                $scope.loginData = {
                    userName: usuarioEmpresa,
                    password: $scope.Sesion.Contrasena,
                };
                authFactory.login($scope.loginData).then(function (response) {
                    if (response.data.access_token != undefined) {
                        $scope.Autorizado = true;
                        $scope.NoAutorizado = false;
                        $scope.Sesion.DatosToken = response;
                        setCookie('UsuarioEmpresa', $scope.Empresa.Codigo, 1)
                        setCookie('Identificador', $scope.Sesion.Identificador, 1)
                        setCookie('Contrasena', $scope.Sesion.Contrasena, 1)
                        ObtenerInformacionUsuario();
                        $scope.EstiloContenido = 'hold-transition skin-blue sidebar-mini';
                        $scope.Banerstyle = 'color:white'
                    } else {
                        ShowWarning('Advertencia', response.data.MensajeOperacion);
                        $scope.Autorizado = false;
                        $scope.NoAutorizado = true;
                    }
                },
                    function (err) {
                        try {
                            if (err.data.error_description == undefined || err.data.error_description == null) {
                                ShowError('Error intentando iniciar sesión');
                                $scope.Autorizado = false;
                                $scope.NoAutorizado = true;
                            }
                            else {
                                ShowError(err.data.error_description);
                                $scope.Autorizado = false;
                                $scope.NoAutorizado = true;
                            }
                        } catch (e) {
                            $scope.ValidarUsuario();
                        }
                    });
            };

            $scope.asignaraccion = function () {
                for (var i = 0; i < $('a').length; i++) {
                    if ($('a')[i].href.includes('unsafe:')) {
                        $('a')[i].href = $('a')[i].href.replace('unsafe:', '')
                    }
                }
            }

            function ObtenerInformacionUsuario() {
                SeguridadUsuariosFactory.Consultar({ CodigoEmpresa: $scope.Empresa.Codigo, CodigoUsuario: $scope.Sesion.Identificador, Clave: $scope.Sesion.Contrasena, Token: 1, }).
                    then(function (response) {
                        var i = 0;
                        $scope.ListadoMenu = [];
                        $scope.ListadoMenuListados = [];
                        var o = 0;
                        if (response.data.ProcesoExitoso === true) {
                            //$scope.ImagenFondoPantalla = ''
                            $('#ImgFondoMain').hide()
                            $scope.Sesion.UsuarioAutenticado = response.data.Datos;
                            $scope.Sesion.UsuarioAutenticado.Prefijo = $scope.Empresa.Prefijo
                            $scope.Sesion.UsuarioAutenticado.Oficinas = response.data.Datos.Oficinas[0]
                            for (var i = 0; i < response.data.Datos.ConsultaMenu.length; i++) {
                                if (response.data.Datos.ConsultaMenu[i].Habilitado == 1 && response.data.Datos.ConsultaMenu[i].MostrarMenu == 1) {
                                    $scope.ListadoMenu.push(response.data.Datos.ConsultaMenu[i])
                                }
                                if (response.data.Datos.ConsultaMenu[i].Habilitado == 1 && response.data.Datos.ConsultaMenu[i].OpcionListado == 1) {
                                    $scope.ListadoMenuListados.push(response.data.Datos.ConsultaMenu[i])
                                }
                                if (response.data.Datos.ConsultaMenu[i].OpcionPermiso == 1) {
                                    $scope.ListadoPermisos.push(response.data.Datos.ConsultaMenu[i])
                                }
                            }
                            $scope.MarginTop = $('#header').height()
                            $scope.ListaModulo = response.data.Datos.ConsultaModulo;
                            $scope.HoraPermisoUsuario = response.data.Datos.Hora;
                            $scope.ListaValidaciones = response.data.Datos.ConsultaValidaciones;
                            localStorageService.set('opcionesMenu', angular.copy($scope.ListadoMenu));
                            if ($scope.Sesion.UsuarioAutenticado.Mensaje !== '' && $scope.Sesion.UsuarioAutenticado.Mensaje !== undefined) {
                                $scope.MensajeBaner = $scope.Sesion.UsuarioAutenticado.Mensaje
                                $('#baner').show()
                            } else {
                                $scope.MensajeBaner = ''
                                $('#baner').hide()
                            }
                            $scope.Sesion.Main = document.URL
                            document.location.href = $scope.URL;
                            //Funcion para verificar si el usuario tiene la clave vencida
                            var now = new Date()
                            var past = new Date(response.data.Datos.FechaUltimoCambioClave)
                            var diastrancurridos = now.getTime() - past.getTime()
                            diastrancurridos = Math.round(diastrancurridos / (1000 * 60 * 60 * 24));
                            if (diastrancurridos > response.data.Datos.DiasCambioClave) {
                                showModal('modalCambioContraseña');
                                $scope.CambioObligatorio = true
                                document.getElementById('Mensajecambio').innerHTML = 'Su contraseña se encuentra vencida, la actualizacion es obligatoria para continuar con el uso de la aplicación'
                            }
                            $scope.Redimencionar()
                            $scope.Autorizado = true;
                            $scope.NoAutorizado = false;
                        } else {
                            ShowWarning('Advertencia', response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        //ShowError('Error intentando iniciar sesión');
                    });
            };

            //------ Cerrar Sesion
            $scope.CerrarSesion = function () {
                setCookie('UsuarioEmpresa', '')
                setCookie('Identificador', '')
                setCookie('Contrasena', '')
                SeguridadUsuariosFactory.CerrarSesion($scope.Sesion.UsuarioAutenticado).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.Autorizado = false;
                            $scope.NoAutorizado = true;
                            $scope.Sesion = { UsuarioAutenticado: {}, Identificador: '', Contrasena: '', FilterLst: RetornarListaAutocomplete };
                            $scope.EstiloContenido = '';
                            document.location.href = '#';
                            $('#baner').hide()
                            authFactory.logOut();
                            authFactory.autauthentication;
                            $('#ImgFondoMain').show()
                            if (screen.width > 1024) {
                                $scope.FondoPantalla = false;
                                //$scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_V2_' + Prefijo + '.jpg'
                                $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_V2_COLVI.jpg'
                            }
                            else if (screen.width < 1024) {
                                $scope.FondoPantalla = true;
                                $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_Movil.jpg'
                            }
                            else if (screen.width == 1024) {
                                $scope.FondoPantalla = false;
                                $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_V2_COLVI.jpg'
                                //$scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_V2_' + Prefijo + '.jpg'
                            }
                            $scope.ImagenFondoPantalla = '../../Css/img/Fondo_Gestrans_' + $scope.Empresa.Prefijo + '.jpg'

                        }
                    });
            };

            $scope.MostrarModalCambio = function () {
                showModal('modalCambioContraseña');
            }
            $scope.CerrarModalCambio = function () {
                closeModal('modalCambioContraseña');
            }
            $scope.CambiarContrasena = function () {
                $scope.MensajesErrorCambioClave = []
                if ($scope.DatosRequeridosContrasena()) {
                    var filtro = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.Sesion.UsuarioAutenticado.Codigo,
                        Clave: $scope.Clave,
                        OpcionCambioClave: 1,
                        Habilitado: CODIGO_UNO
                    }
                    UsuariosFactory.Guardar(filtro)
                        .then(function (response) {
                            if (response.data.ProcesoExitoso == true) {
                                if (response.data.Datos > 0) {
                                    ShowSuccess('La contraseña se actualizó correctamente');
                                    closeModal('modalCambioContraseña');
                                    $scope.Clave = '';
                                    $scope.ClaveAnterior = '';
                                    $scope.ConfirClave = '';
                                    $scope.CambioObligatorio = false
                                    document.getElementById('Mensajecambio').innerHTML = ''
                                    $scope.MensajesErrorCambioClave = []
                                    $scope.CerrarSesion()
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            $scope.DatosRequeridosContrasena = function () {
                var PuedeGuardar = true

                if ($scope.ClaveAnterior == undefined || $scope.ClaveAnterior == '') {
                    $scope.MensajesErrorCambioClave.push('Debe ingresar la contraseña actual');
                    PuedeGuardar = false;
                } else {
                    if ($scope.ClaveAnterior != $scope.Sesion.UsuarioAutenticado.Clave) {
                        $scope.MensajesErrorCambioClave.push('La contraseña actual no es correcta');
                        PuedeGuardar = false;
                    }
                }
                if ($scope.Clave == undefined || $scope.Clave == '') {
                    $scope.MensajesErrorCambioClave.push('Debe ingresar la contraseña nueva');
                    PuedeGuardar = false;
                }
                else {
                    if ($scope.Clave == $scope.Sesion.UsuarioAutenticado.Clave) {
                        $scope.MensajesErrorCambioClave.push('La contraseña nueva no puede ser igual a la anterior');
                        PuedeGuardar = false;
                    } else {
                        if (validarContrasena($scope.Clave) == false) {
                            $scope.MensajesErrorCambioClave.push('La contraseña debe ser mínimo de 6 caracteres, debe contener al menos un caracter especial, un número y no debe tener espacios en blanco, recuerde que los los caracteres & y + no estan permitidos');
                            PuedeGuardar = false;
                        }
                        else {
                            if ($scope.ConfirClave != $scope.Clave) {
                                $scope.MensajesErrorCambioClave.push('La nueva contraseña y la confirmación deben coincidir');
                                PuedeGuardar = false;
                            }
                        }
                    }
                }
                return PuedeGuardar
            }

            $scope.Version = '23.05.2017.1'
            $scope.VerificarAutocomplete = function (Object) {
                if (Object == '' || Object == undefined || Object == null) {
                    return Object = ''
                } else if ((Object.Codigo == '' || Object.Codigo == undefined || Object.Codigo == null || Object.Codigo == 0 || Object.Codigo == '0') && (Object.Numero == '' || Object.Numero == undefined || Object.Numero == null || Object.Numero == 0 || Object.Numero == '0')) {
                    //ShowNotificacionError('No se encontró el elemento')
                    return Object = ''
                } else {
                    return Object = Object
                }
            }
            $scope.CargarTercero = function (Codigo) {
                return TercerosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: Codigo,
                    Sync: true
                }).Datos[0]
            }
            $scope.CargarColor = function (Codigo) {
                return ColorVehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, Sync: true }).Datos[0]
            }
            $scope.CargarCiudad = function (Codigo) {
                return CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, Sync: true }).Datos[0]
            }
            $scope.CargarVehiculos = function (Codigo) {
                return VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, Sync: true }).Datos[0]
            }
            $scope.MaskNumero = function () {
                MascaraNumeroGeneral($scope)
            };
            $scope.MaskMayus = function () {
                MascaraMayusGeneral($scope)
            };
            $scope.MaskValores = function () {
                MascaraValoresGeneral($scope)
            };
            $scope.MaskHora = function () {
                MascaraHorasGeneral($scope)
            };
            $scope.Redimencionar = function () {
                $scope.MarginTop = $('#header').height()
                $timeout(function () {
                    for (var i = 0; i < $('#contentwrapper')[0].attributes.length; i++) {
                        if ($('#contentwrapper')[0].attributes[i].name == 'style') {

                            $scope.MaxHeigth = ($('.main-sidebar').height() - 51).toString() + 'px;'
                            $('#contentwrapper')[0].attributes[i].value = ('max-height: ' + $scope.MaxHeigth + ';min-height:' + $scope.MaxHeigth + ';')
                        }
                    }
                }, 500)
            }
            $(window).resize(function () {
                $scope.MarginTop = $('#header').height()
                $timeout(function () {
                    for (var i = 0; i < $('#contentwrapper')[0].attributes.length; i++) {
                        if ($('#contentwrapper')[0].attributes[i].name == 'style') {

                            $scope.MaxHeigth = ($('.main-sidebar').height() - 51).toString() + 'px;'
                            $('#contentwrapper')[0].attributes[i].value = ('max-height: ' + $scope.MaxHeigth + ';min-height:' + $scope.MaxHeigth + ';')
                        }
                    }
                }, 500)
            }
            );
            $scope.OrdenarPor = function (Lista, Objeto, Atributo, Atributo2) {
                OrderBy(Objeto, Atributo, Lista, Atributo2);
            }


            $scope.MostrarMensajeErrorModal = function (MostrarMensajeError) {
                if (MostrarMensajeError == true) {
                    MostrarMensajeError = false
                } else {
                    MostrarMensajeError = true
                }
                return MostrarMensajeError
            }
            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }

            function getCookie(cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }

            function checkCookie() {
                var user = getCookie("username");
                if (user != "") {

                } else {
                    user = prompt("Please enter your name:", "");
                    if (user != "" && user != null) {
                        setCookie("username", user, 365);
                    }
                }
            }
        }]);

})();

SofttoolsApp.controller("MapaCtrl", ['$scope', function ($scope) {

}]);