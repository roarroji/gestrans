﻿SofttoolsApp.controller("ConsultarEntradaAlmacenesCtrl", ['$scope', '$timeout', '$routeParams', 'blockUI', '$linq', 'DocumentoAlmacenesFactory', function ($scope, $timeout, $routeParams, blockUI, $linq, DocumentoAlmacenesFactory) {
    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    $scope.Buscando = false;
    $scope.ResultadoSinRegistros = '';
    $scope.MensajesError = [];
    var filtros = {};
    $scope.ListadoEstados = [];
    $scope.totalRegistros = 0;
    $scope.paginaActual = 1;
    $scope.cantidadRegistrosPorPagina = 10;
    $scope.totalPaginas = 0;
    $scope.ListadoEstados = [
        { Nombre: '(TODAS)', Codigo: -1 },
        { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
        { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR },
        { Nombre: 'ANULADO', Codigo: ESTADO_ANULADO }
    ];

    $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ENTRADA_ALMACENES);
    $scope.MapaSitio = [{ Nombre: 'Almacén' }, { Nombre: 'Documentos' }, { Nombre: 'Entradas Almacén' }];
    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
        $scope.FechaInicial = new Date();
        $scope.FechaFinal = new Date();
    }

    // -------------------------- Constantes ---------------------------------------------------------------------//


    /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };


    /*---------------------------------------------------------------------Funcion Buscar Linea de Mantenimiento-----------------------------------------------------------------------------*/
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            Find()
        }
    };

    function Find() {

        $scope.ListadoEntradaAlmacen = [];
        if (DatosRequeridos()) {

            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoEntradaAlmacen = [];
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.Numero,
                FechaInicial: $scope.FechaInicial,
                FechaFinal: $scope.FechaFinal,
                Estado: $scope.Estado.Codigo,
                NombreAlmacen: $scope.Almacen,
                NumeroOrdenCompra: $scope.OrdenCompra,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                TipoDocumento: 335 // se debe crear una constante el dato no puede quedar quemado
            };


            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;

                DocumentoAlmacenesFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoEntradaAlmacen = response.data.Datos

                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        }
    };



    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;
        $scope.NumeroFinal = 0;
        if ($scope.FechaInicial !== null && $scope.FechaFinal !== null) {
            if ($scope.FechaInicial > $scope.FechaFinal) {
                $scope.MensajesError.push('La fecha inicial debe ser menor que la fecha final');
                continuar = false;
            }

        }

        if ($scope.NumeroInicial > 0 && $scope.NumeroFinal > 0) {
            if ($scope.NumeroInicial > $scope.NumeroFinal) {
                $scope.MensajesError.push('El número inicial debe ser menor que el número final');
                continuar = false;
            }
        }


        if ($scope.NumeroInicial > 0 && ($scope.NumeroFinal <= 0 || $scope.NumeroFinal == undefined || $scope.NumeroFinal == '')) {
            $scope.NumeroFinal = $scope.NumeroInicial;
        }
        if ($scope.NumeroInicial <= 0 && $scope.NumeroFinal > 0) {
            $scope.MensajesError.push('El número inicial debe ser mayor a cero');
            continuar = false;
        }


        if ($scope.FechaInicial !== null && $scope.FechaFinal !== null) {
            if ($scope.FechaInicial > $scope.FechaFinal) {
                $scope.MensajesError.push('La fecha inicial debe ser menor que la fecha final');
                continuar = false;
            }
        }
        if ($scope.FechaInicial !== null && $scope.FechaFinal == null) {
            $scope.FechaFinal = $scope.FechaInicial;
        }
        if (($scope.FechaInicial == null || $scope.FechaInicial == undefined) && ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined)) {
            $scope.MensajesError.push('Debe seleccionar la fecha inicial');
            continuar = false;
        }
        if ($scope.NumeroInicial > 0 && $scope.NumeroFinal > 0) {

        }
        else if (($scope.FechaInicial == null && $scope.FechaFinal == null) && (($scope.NumeroInicial == 0 || $scope.NumeroInicial == undefined || $scope.NumeroInicial == '') && ($scope.NumeroFinal == 0 || $scope.NumeroFinal == undefined || $scope.NumeroFinal == ''))) {
            $scope.MensajesError.push('Debe ingresar al menos una fecha o rango de fechas o un número');
            continuar = false;
        }
        return continuar;

    }


    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    /*Validacion boton anular */
    $scope.ConfirmacionAnularEntrada = function (Numero) {
        $scope.NumeroAnulacion = Numero;
        showModal('modalConfirmacionAnularEntrada');
    };
    /*Funcion que solicita los datos de la anulación */
    $scope.SolicitarDatosAnulacion = function () {
        $scope.FechaAnulacion = new Date();
        closeModal('modalConfirmacionAnularEntrada');
        showModal('modalDatosAnularEntrada');
    };
    //---------------------------------------------------------------------Funcion Anular orden compra-----------------------------------------------------------------------------
    $scope.Anular = function () {

        $scope.ListadoEntradaAlmacen.forEach(function (item) {
            if (item.Numero == $scope.NumeroAnulacion) {
                $scope.Estado = item.Estado;
                $scope.NumeroDocumento = item.NumeroDocumento;
            }
        });

        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Numero: $scope.NumeroAnulacion,
            NumeroDocumento: $scope.NumeroDocumento,
            UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CausaAnulacion: $scope.CausaAnulacion,
            Oficina: {
                Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre,
            },
            TipoDocumento: 335
        };

        DocumentoAlmacenesFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se anuló la Entrada almacén No.' + entidad.NumeroDocumento);
                    closeModal('modalDatosAnularEntrada');
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                ShowError(response.statusText);
            });

    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarEntradaAlmacenes';
        }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Codigo = MascaraNumero($scope.Codigo) } catch (e) { }
    };

    $scope.urlASP = '';
    $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
    $scope.DesplegarInforme = function (Numero) {
        if ($scope.ValidarPermisos.AplicaImprimir == PERMISO_ACTIVO) {
            $scope.FiltroArmado = '';

            //Depende del listado seleccionado se enviará el nombre por parametro
            $scope.Numero = Numero;
            $scope.NombreReporte = 'RepEntradaAlmacen'
            //Arma el filtro
            $scope.ArmarFiltro();
            //Llama a la pagina ASP con el filtro por GET

            //window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + 1 + '&TipoDocumento=' + DOCUMENTO_ENTRADA_ALMACEN);

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';
        };
    };

    // funcion enviar parametros al proyecto ASP armando el filtro
    $scope.ArmarFiltro = function () {
        $scope.FiltroArmado = '';
        if ($scope.Numero !== undefined && $scope.Numero !== '' && $scope.Numero !== null) {
            $scope.FiltroArmado += '&Numero=' + $scope.Numero;
        }
    }

    if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
        $scope.Numero = $routeParams.Numero;
        $scope.Estado = { Codigo: 0, Nombre: '(NO Aplica)' }
        Find();
        $scope.NumeroInicial = ''
        $scope.NumeroFinal = ''
    }


}]);