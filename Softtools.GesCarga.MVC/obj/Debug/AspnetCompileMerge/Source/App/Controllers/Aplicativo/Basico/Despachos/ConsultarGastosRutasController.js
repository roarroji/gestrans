﻿SofttoolsApp.controller("ConsultarGastosRutasCtrl", ['$scope', '$timeout', 'RutasFactory', 'LegalizacionGastosRutaFactory', '$linq', 'blockUI', '$routeParams', 'PuestoControlesFactory', 'ValorCatalogosFactory', 'ConceptoGastosFactory',
    function ($scope, $timeout, RutasFactory, LegalizacionGastosRutaFactory, $linq, blockUI, $routeParams, PuestoControlesFactory, ValorCatalogosFactory, ConceptoGastosFactory,
         ) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Gastos Rutas' }];


        $scope.listadosRutas = [];
        $scope.ListaGastosRuta = [];
        $scope.ListadoTipoVehiculo = [];
        $scope.ListadoConceptosGastos = [];  
        $scope.Modelo = { 
           
        };  
   
              //---------------------Validacion de permisos --------------------///
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GASTOS_RUTAS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        } 
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        //Elementos cargados al iniciar la vista
        

        $scope.InitLoad = function  () {   
            //Listado Tipos Vehiculos 
            var ResponseTiposVehiculo = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO }, Estado: 1, Sync: true }).Datos;
            if (ResponseTiposVehiculo != undefined) {
                //then(function (response) {
                //if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoVehiculo.push({ Codigo: -1, Nombre: '(TODOS)' });
                if (ResponseTiposVehiculo.length > 0) {
                    for (var i = 0; i < ResponseTiposVehiculo.length; i++) {
                        if (ResponseTiposVehiculo[i].Codigo != CODIGO_TIPO_VEHICULO_NO_APLICA) {
                            $scope.ListadoTipoVehiculo.push({ Nombre: ResponseTiposVehiculo[i].Nombre, Codigo: ResponseTiposVehiculo[i].Codigo });
                            }
                        }
                    }
                    else {
                        $scope.ListadoTipoVehiculo = [];
                    }
                //}
                $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == -1');
            }
                //}, function (response) {
               // });   
            //Listados Conceptos  
            var ResponseConceptos = ConceptoGastosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, ConceptoSistema: ESTADO_ACTIVO, Sync: true }).Datos;
               // then(function (response) {
            if (ResponseConceptos != undefined) {
                //if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoConceptosGastos.push({ Codigo: -1, Nombre: '(TODOS)' });
                    //if (response.data.ProcesoExitoso === true) {
                if (ResponseConceptos.length > 0) {
                    for (var i = 0; i < ResponseConceptos.length; i++) {
                        if (ResponseConceptos[i].ConceptoSistema == 1) {
                            if (ResponseConceptos[i].Codigo == CODIGO_GASTO_COMBUSTIBLE_RENDIMIENTO_GALON) {
                                $scope.ListadoConceptosGastos.push({ Nombre: ResponseConceptos[i].Nombre, Codigo: ResponseConceptos[i].Codigo });
                                    }
                                } else {
                            $scope.ListadoConceptosGastos.push({ Nombre: ResponseConceptos[i].Nombre, Codigo: ResponseConceptos[i].Codigo });
                                }
                            }
                        }
                        else {
                            $scope.ListadoConceptosGastos = [];
                        }
                    //}
                //}
                $scope.Modelo.Concepto = $linq.Enumerable().From($scope.ListadoConceptosGastos).First('$.Codigo == -1');

                //}, function (response) {
                //    ShowError(response.statusText);
                //});
            }
            //Listado de rutas 
            RutasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: 1 
                 }).then(function (response) { 
                $scope.listadosRutas = response.data.Datos;  
                }, function (response) {
                    ShowError(response.statusText);
                }); 
                
        } 


        //Funcion Buscar
        $scope.Buscar = function () {   
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Ruta: $scope.Modelo.Ruta,            
                Concepto: $scope.Modelo.Concepto,
                TipoVehiculo: $scope.Modelo.TipoVehiculo,
                ConsultaLegalizacionGastosRuta: true
            }           
          
            LegalizacionGastosRutaFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos !== undefined) {
                            $scope.ListaGastosRuta = response.data.Datos
                            for (var i = 0; i < $scope.ListaGastosRuta.length; i++) {
                                $scope.ListaGastosRuta[i].Valor = MascaraValores($scope.ListaGastosRuta[i].Valor);
                                $scope.ListaGastosRuta[i].AplicaAnticipo = $scope.ListaGastosRuta[i].AplicaAnticipo == 1 ? true : false;
                                }
                        }

                    }
                    $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == -1');
                    $scope.Modelo.Concepto = $linq.Enumerable().From($scope.ListadoConceptosGastos).First('$.Codigo == -1');

                }, function (response) {
                    ShowError(response.statusText);
                });
            
        } 
            /////ACTUALIZAR 
        $scope.Actualizar = function (item) {
            $scope.TmpActualizarGastosRuta = { item };
            showModal('modalActualizarGastosRuta');
            
        }


        $scope.ActualizarGastosRuta = function (item) {


            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Ruta: item.item.Ruta,
                Concepto: item.item.Concepto,
                TipoVehiculo: item.item.TipoVehiculo,
                Concepto: item.item.Concepto,
                Valor: $scope.MaskNumeroGrid($scope.MaskValoresGrid(item.item.Valor,)),
                Codigo: 1,
                AplicaAnticipo: item.item.AplicaAnticipo == true ? 1 : 0 
            }
           
            LegalizacionGastosRutaFactory.Guardar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se actualizo el gasto de la Ruta',)
                        $scope.Buscar();
                    }

                }, function (response) {
                    ShowError(response.statusText);
                });

            closeModal('modalActualizarGastosRuta');
        };


     
        $scope.TmpEliminarGastosRuta = {};
        $scope.ConfirmarEliminarGastosRuta = function (item) { 
            $scope.TmpEliminarGastosRuta = {item};
            showModal('modalEliminarGastosRuta');
        };

        $scope.EliminarGastosRuta = function (item) {
        
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Ruta: item.item.Ruta,
                Concepto: item.item.Concepto,
                TipoVehiculo: item.item.TipoVehiculo   
            }  

            LegalizacionGastosRutaFactory.Anular(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó el gasto de la Ruta', )
                        $scope.Buscar();
                    }
                  
                }, function (response) {
                    ShowError(response.statusText);
                }); 

            closeModal('modalEliminarGastosRuta');
        };
       
        $scope.NuevoGastoRuta = function () {

            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarGastosRutas'; 
                     } 

        }; 

        if ($routeParams.Ruta > CERO) {
            
            $scope.InitLoad();
           //$scope.Modelo.Ruta.Codigo = MascaraNumero($routeParams.Ruta)
            $scope.Modelo.Concepto.Codigo = MascaraNumero($routeParams.Concepto)
            $scope.Modelo.TipoVehiculo.Codigo = MascaraNumero($routeParams.Vehiculo)
            $scope.Buscar(); 
   
        } 


        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };

        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
       
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

    }]);