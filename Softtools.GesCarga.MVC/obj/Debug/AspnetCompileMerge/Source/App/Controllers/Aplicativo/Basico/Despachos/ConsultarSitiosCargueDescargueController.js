﻿SofttoolsApp.controller("ConsultarSitiosCargueDescargueCtrl", ['$scope', '$timeout', 'ValorCatalogosFactory', 'PaisesFactory', 'CiudadesFactory', 'SitiosCargueDescargueFactory', '$linq', 'blockUI', '$routeParams', 'blockUIConfig',
    function ($scope, $timeout, ValorCatalogosFactory, PaisesFactory, CiudadesFactory, SitiosCargueDescargueFactory, $linq, blockUI, $routeParams, blockUIConfig) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Sitios Cargue Descargue' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.ListadoPaises = [];
        $scope.ListadoCiudades = [];
        $scope.ListadoSitios = [];

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SITIOS_CARGUE_DESCARGUE);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /* Obtener parametros*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.TipoSitio = { Codigo: -1 }
            $scope.Codigo = $routeParams.Codigo;
            Find();
        } else {
            $scope.Codigo = '';
        }

        /*---------------------------------------------------------------------------------------COMBOS Y AUTOCOMPLETE------------------------------------------------------------------------------------*/
        /*Cargar el combo de paises*/
        PaisesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoPaises = response.data.Datos;
                    } else {
                        $scope.ListadoPaises = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        ///*Cargar el autocomplete de ciudades*/
        //$scope.CargarCiudades = function (pais) {
        //    $scope.PaisCiudad = pais;
        //    CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Paises: $scope.Pais }).
        //        then(function (response) {
        //            if (response.data.ProcesoExitoso === true) {
        //                if (response.data.Datos.length > 0) {
        //                    $scope.ListadoCiudades = response.data.Datos;
        //                } else {
        //                    $scope.ListadoCiudades = [];
        //                }
        //            }
        //        }, function (response) {
        //            ShowError(response.statusText);
        //        });
        //}

        $scope.ListadoCiudades = [];
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                }
            }
            return $scope.ListadoCiudades
        }

        /*Cargar el combo de tipo sitios*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SITIO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoSitios.push({ Nombre: '(TODOS)', Codigo: -1 })
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoSitios.push(item)
                        });
                        $scope.TipoSitio = $scope.ListadoSitios[0];
                    }
                    else {
                        $scope.ListadoSitios = []
                    }
                }
            }, function (response) {
            });

        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarSitiosCargueDescargue';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0;
                Find()
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoSitiosCargueDescargue = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {

                    var Filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.Codigo,
                        Nombre: $scope.Nombre,
                        CodigoTipoSitio: $scope.TipoSitio.Codigo,
                        Pais: $scope.Pais,
                        Ciudad: $scope.Ciudad,
                        Estado: $scope.ModalEstado.Codigo,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    }
                    blockUI.delay = 1000;
                    SitiosCargueDescargueFactory.Consultar(Filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    response.data.Datos.forEach(function (item) {
                                        var registro = item;
                                        $scope.ListadoSitiosCargueDescargue.push(registro);
                                    });

                                    $scope.ListadoSitiosCargueDescargue.forEach(function (item) {
                                        $scope.codigo = item.Codigo;
                                    });
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/
        $scope.EliminarSitiosCargueDescargue = function (codigo, Nombre) {
            $scope.CodigoSitio = codigo
            $scope.NombreSitio = Nombre
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarSitiosCargueDescargue');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.CodigoSitio,
            };

            SitiosCargueDescargueFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó el sitio de cargue/descargue ' + $scope.CodigoSitio + ' - ' + $scope.NombreSitio);
                        closeModal('modalEliminarSitiosCargueDescargue');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarSitiosCargueDescargue');
                    $scope.ModalError = 'No se puede eliminar el sitio cargue/descargue ' + $scope.NombreSitio + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.CerrarModal = function () {
            closeModal('modalEliminarSitiosCargueDescargue');
            closeModal('modalMensajeEliminarSitiosCargueDescargue');
        }

        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

    }]);