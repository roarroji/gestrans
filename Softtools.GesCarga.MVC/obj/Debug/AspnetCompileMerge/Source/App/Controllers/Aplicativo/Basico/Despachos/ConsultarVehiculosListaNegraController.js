﻿SofttoolsApp.controller("ConsultarVehiculosListaNegraCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'VehiculosFactory','VehiculosListaNegraFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, VehiculosFactory, VehiculosListaNegraFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Vehículos Lista Negra' }];

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_VEHICULOS_LISTA_NEGRA);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.MensajesError = [];
        $scope.ListadoVehiculosListaNegra = [];
        $scope.RegistrosPagina = 10;  
        $scope.paginaActual = 1; 


        $scope.ListaEstados = [
            { Nombre: '(NO APLICA)', Codigo: -1 },
            { Nombre: 'RECHAZADO', Codigo: 1 },
            { Nombre: 'HABILITADO', Codigo: 0 }
        ];

        $scope.Modelo = {
            Codigo: '',
            Placa: '',
            Estado: '',
            CodigoAlterno: ''
        }

        console.log("parametrosUrl:", $routeParams);
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            console.log($routeParams);
            $scope.Modelo.Codigo = $routeParams.Codigo;

             Find();
        }

        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo == -1');

        /*Metodo buscar*/
        $scope.Buscar = function () {

            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {

                //if (DatosRequeridos()) {
                    $scope.Modelo.Codigo = 0;
                    Find()
                //}
            }
        };

        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroReporte = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_VEHICULOS_LISTA_NEGRA;

            //Arma el filtro
            //$scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarVehiculosListaNegra';
            }
        };

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        $scope.FiltroArmado = '';

        function Find() {
            $scope.FiltroArmado = '';

            if ($scope.Modelo.Placa != undefined && $scope.Modelo.Placa != '' && $scope.Modelo.Placa != null) {
                $scope.FiltroArmado = '&Placa=' + $scope.Modelo.Placa;
            }
            if ($scope.Modelo.CodigoAlterno != undefined && $scope.Modelo.CodigoAlterno != null && $scope.Modelo.CodigoAlterno != '') {
                $scope.FiltroArmado += '&CodigoAlterno=' + $scope.Modelo.CodigoAlterno;
            }
            if ($scope.Modelo.Codigo != undefined && $scope.Modelo.Codigo != null && $scope.Modelo.Codigo != '') {
                $scope.FiltroArmado += '&Codigo=' + $scope.Modelo.Codigo;
            }
            if ($scope.Modelo.Estado.Codigo != undefined && $scope.Modelo.Estado.Codigo != null && $scope.Modelo.Estado.Codigo != '' && $scope.Modelo.Estado.Codigo != -1) {
                $scope.FiltroArmado += '&Estado=' + $scope.Modelo.Estado.Codigo;
            }
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                //NumeroDocumento
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            $scope.ListadoVehiculosListaNegra = [];

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Placa: $scope.Modelo.Placa,
                CodigoAlterno: $scope.Modelo.CodigoAlterno,
                RegistrosPagina: $scope.RegistrosPagina,
                Pagina: $scope.paginaActual,
                Codigo: $scope.Modelo.Codigo,
                Estado: { Codigo: $scope.Modelo.Estado.Codigo }
            }

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                VehiculosFactory.ConsultarListaNegra(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoVehiculosListaNegra = response.data.Datos;
                                console.log(response.data.Datos);
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.RegistrosPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                                console.log($scope.totalRegistros, $scope.RegistrosPagina);
                                
                            }
                            else {
                                $scope.ListadoVehiculosListaNegra =[];
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }


                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };


        //Eliminar:
        $scope.EliminarVehiculo = function (codigo, nombre) {
            $scope.CodigoVehiculo = codigo
            $scope.NombreVehiculo = nombre
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminar');
        };
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.CodigoVehiculo,
            };

            VehiculosListaNegraFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó el Vehiculo ' + $scope.NombreVehiculo +' de la lista negra.');
                        closeModal('modalEliminar');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminar');
                    $scope.ModalError = 'No se puede eliminar el Vehiculo ' + $scope.AuxiNombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };

        $scope.CerrarModal = function () {
            closeModal('modalEliminar');
            closeModal('modalMensajeEliminar');
        }

    /*---------------------------------------------------------------------------------------------------Máscaras-----------------------------------------------------------------------------*/
        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope);
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        }
    }
]);