﻿SofttoolsApp.controller("ConsultarTercerosCtrl", ['$scope', '$timeout', '$linq', 'TercerosFactory', 'blockUI', 'ValorCatalogosFactory', '$routeParams',
    function ($scope, $timeout, $linq, TercerosFactory, blockUI, ValorCatalogosFactory, $routeParams) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Terceros' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
        }
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_TERCEROS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.ListadoCiudades = [];
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoEstados = [
            { Codigo: -1, Nombre: "(NO APLICA)" },
            { Codigo: 0, Nombre: "INACTIVO" },
            { Codigo: 1, Nombre: "ACTIVO" },
        ]
        $scope.Modelo.Estado = $scope.ListadoEstados[0]

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_PERFIL_TERCEROS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoPerfiles = response.data.Datos;
                        $scope.Modelo.Perfil = $scope.ListadoPerfiles[0]
                        OrderBy('Nombre', undefined, $scope.perfilTerceros);
                    }
                    else {
                        $scope.ListadoPerfiles = []
                    }
                }
            }, function (response) {
            });

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
            FindAuditoria();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
                FindAuditoria();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
                FindAuditoria();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
            FindAuditoria();
        };
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

        /*Metodo buscar*/
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                Find()
            }
        };

        $scope.BuscarDetallleAuditoria = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                FindAuditoria()

            }
        };

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarTerceros';
            }
        };
        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
            $scope.Buscar()
            FindAuditoria();
        }
        else {
            $scope.Modelo.Codigo = undefined;
        }

        //Modal consultar auditoria tercero
        $scope.modalAuditoriaTercero = function (CodigoTercero, NombreTercero) {
            $scope.TerceroModal = CodigoTercero;
            $scope.NombreTercero = NombreTercero;
            showModal('modalAuditoriaTercero');
        }

        // Limpiar detalle auditoria 
        $scope.LimpiarDetalleAuditoria = function () {
            $scope.ListadoAuditoria = []
        }




        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/

        function FindAuditoria() {
            blockUI.start('Buscando registros...');

            $timeout(function () {
                blockUI.message('Espere por favor...');
            });

            $scope.Buscando = true;
            $scope.ListadoAuditoria = [];
            $scope.Modelo.Pagina = $scope.paginaActual
            $scope.Modelo.RegistrosPagina = $scope.cantidadRegistrosPorPagina
            if ($scope.MensajesError.length === 0) {
                //blockUI.delay = 1000;
                $scope.ListadoAuditoria = [];
                TercerosFactory.ConsultarAuditoria({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.TerceroModal, FechaInicial: $scope.ModeloFechaInicial, FechaFinal: $scope.ModeloFechaFinal }).
                    then(function (response) {
                        console.log(response);
                         if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoAuditoria = response.data.Datos;
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            } else {
                                $scope.ListadoAuditoria = [];
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                    });
            }
            blockUI.stop();
        }


        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.ListadoTerceros = [];
            try {
                $scope.Modelo.CadenaPerfiles = $scope.Modelo.Perfil.Codigo
            } catch (e) {
            }
            $scope.Modelo.Pagina = $scope.paginaActual
            $scope.Modelo.RegistrosPagina = $scope.cantidadRegistrosPorPagina
            if ($scope.MensajesError.length === 0) {
                blockUI.delay = 1000;
                TercerosFactory.Consultar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                response.data.Datos.forEach(function (item) {

                                    if (item.Telefonos !== undefined && item.Telefonos !== "" && item.Telefonos !== null) {
                                        item.Telefonos = item.Telefonos.replace(';', ' - ')
                                    }
                                    $scope.ListadoTerceros.push(item);
                                });
                                $scope.Modelo.Codigo = undefined;
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.ListadoTerceros = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        function DatosRequeridos() {
            if (filtros.Codigo === undefined) {
                filtros.Codigo = 0;
            }

            if ($scope.CodigoInicial !== undefined && $scope.CodigoFinal === undefined) {
                $scope.CodigoFinal = $scope.CodigoInicial
            }

            if (($scope.CodigoFinal !== undefined) && ($scope.CodigoInicial === undefined)) {
                $scope.MensajesError.push('Debe ingresar el código inicial para filtrar');
                continuar = false;
            }

            if (($scope.CodigoInicial !== undefined) && ($scope.CodigoFinal !== undefined)) {
                if ($scope.CodigoInicial > $scope.CodigoFinal) {
                    $scope.MensajesError.push('El código inicial debe ser menor al código final');
                    continuar = false;
                }
            }
        }
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    }]);