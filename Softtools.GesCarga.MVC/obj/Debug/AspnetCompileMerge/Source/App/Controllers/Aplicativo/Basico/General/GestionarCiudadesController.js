﻿SofttoolsApp.controller("GestionarCiudadesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'CiudadesFactory', 'DepartamentosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, CiudadesFactory, DepartamentosFactory) {

    $scope.Titulo = 'GESTIONAR CIUDADES';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Ciudades' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CIUDADES);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    $scope.DepartamentoValido = true;
    $scope.CodigoDANEValido = true;
    $scope.ListadoDepartamentos = [];
    $scope.ListadoCodigosDane = [];

    $scope.Modelo = {};

    $scope.Codigo = 0;

    $scope.ListadoEstadoCalcularICAOficina = [
        { Nombre: 'ACTIVO', Codigo: ESTADO_ACTIVO },
        { Nombre: 'INACTIVO', Codigo: ESTADO_INACTIVO }
    ];
    $scope.Modelo.CalcularICAOficina = $linq.Enumerable().From($scope.ListadoEstadoCalcularICAOficina).First('$.Codigo==' + ESTADO_ACTIVO);

    $scope.ListadoEstados = [
        { Nombre: 'ACTIVA', Codigo: 1 },
        { Nombre: 'INACTIVA', Codigo: 0 }
    ];
    $scope.ModalEstado = $scope.ListadoEstados[0];
    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Codigo = $routeParams.Codigo;
    }
    else {
        $scope.Codigo = 0;
    }

    DepartamentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    response.data.Datos.push({ Nombre: '', Codigo: 0 })
                    $scope.ListadoDepartamentos = response.data.Datos;
                    if ($scope.CodigoDepartamento === undefined || $scope.CodigoDepartamento === '' || $scope.CodigoDepartamento === 0 || $scope.CodigoDepartamento === null) {
                        $scope.Departamento = $scope.ListadoDepartamentos[$scope.ListadoDepartamentos.length - 1];
                    }
                }
                else {
                    $scope.ListadoDepartamentos = [];
                    $scope.Departamento = null;
                }
            }
        }, function (response) {
            ShowError(response.statusText);
        });

    CiudadesFactory.ConsultarCodigoDane({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {

                    $scope.ListadoCodigosDane = response.data.Datos;
                    if ($scope.CodigoDane !== undefined && $scope.CodigoDane !== '' && $scope.CodigoDane !== 0 && $scope.CodigoDane !== null) {
                        $scope.CodigoDane = Math.ceil($scope.CodigoDane)
                        $scope.Modelo.CodigoDane = $linq.Enumerable().From($scope.ListadoCodigosDane).First('$.Codigo ==' + $scope.CodigoDane);
                    }
                    else {
                        $scope.Modelo.CodigoDane = $scope.ListadoCodigosDane[0]
                    }
                }
                else {
                    $scope.ListadoCodigosDane = [];
                    $scope.Departamento = null;
                }
            }
        }, function (response) {
            ShowError(response.statusText);
        });

    $scope.AsignarDepartamento = function (Departamento) {
        if (Departamento !== undefined || Departamento !== null) {
            if (angular.isObject(Departamento)) {
                $scope.LongitudDepartamento = Departamento.length;
                $scope.DepartamentoValido = true;
            }
            else if (angular.isString(Departamento)) {
                $scope.LongitudDepartamento = Departamento.length;
                $scope.DepartamentoValido = false;
            }
        }
    };

    $scope.AsignarCodigoDane = function (CodigoDANE) {
        if (CodigoDANE !== undefined || CodigoDANE !== null) {
            if (angular.isObject(CodigoDANE)) {
                $scope.LongitudCodigoDANE = CodigoDANE.length;
                $scope.CodigoDANEValido = true;
            }
            else if (angular.isString(CodigoDANE)) {
                $scope.LongitudCodigoDANE = CodigoDANE.length;
                $scope.CodigoDANEValido = false;
            }
        }
    };

    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    if ($scope.Codigo > 0) {
        $scope.Titulo = 'CONSULTAR CIUDADES';
        $scope.Deshabilitar = true;
        ObtenerCiudad();
    }
    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function ObtenerCiudad() {
        blockUI.start('Cargando ciudad código ' + $scope.Codigo);

        $timeout(function () {
            blockUI.message('Cargando ciudad Código ' + $scope.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Codigo,
        };

        blockUI.delay = 1000;
        CiudadesFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    $scope.Ciudad = response.data.Datos.Nombre;
                    $scope.Departamento = response.data.Datos.Departamento;
                    $scope.TarifaICA = response.data.Datos.TarifaImpuesto1;
                    $scope.ValorBase = response.data.Datos.TarifaImpuesto2;

                    $scope.CodigoDane = response.data.Datos.CodigoAlterno;

                    $scope.Modelo.CodigoPostal = response.data.Datos.CodigoPostal
                    $scope.Modelo.CalcularICAOficina = $linq.Enumerable().From($scope.ListadoEstadoCalcularICAOficina).First('$.Codigo==' + response.data.Datos.CalcularICAOficina);

                    if ($scope.ListadoCodigosDane.length > 0) {
                        $scope.Modelo.CodigoDane = $linq.Enumerable().From($scope.ListadoCodigosDane).First('$.Codigo ==' + $scope.CodigoDane);
                    }

                    $scope.CodigoDepartamento = response.data.Datos.Departamento.Codigo
                    if ($scope.ListadoDepartamentos.length > 0 && $scope.CodigoDepartamento > 0) {
                        $scope.Departamento = $linq.Enumerable().From($scope.ListadoDepartamentos).First('$.Codigo == ' + response.data.Datos.Departamento.Codigo);
                    }

                    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);

                    if (response.data.Datos.AplicaRutaEmpresaPasajeros === 1) {
                        $scope.AplicaRuta = true;
                    }
                }
                else {
                    ShowError('No se logro consultar la ciudad' + response.data.MensajeOperacion);
                    document.location.href = '#!onsultarCiudades';
                }
            }, function (response) {
                ShowError(response.statusText)
                document.location.href = '#!ConsultarCiudades';
            });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    $scope.ConfirmacionGuardarCiudad = function () {
        showModal('modalConfirmacionGuardarCiudad');
    };

    $scope.Guardar = function () {

        closeModal('modalConfirmacionGuardarCiudad');
        if (DatosRequeridosCiudad()) {
            if ($scope.AplicaRuta === true) {
                AplicaRutaEmpresaPasajeros = 1
            }
            else {
                AplicaRutaEmpresaPasajeros = 0
            }
            filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Departamento: $scope.Departamento,
                Nombre: $scope.Ciudad,
                CodigoAlterno: $scope.Modelo.CodigoDane.Codigo,
                TarifaImpuesto1: $scope.TarifaICA,
                TarifaImpuesto2: $scope.ValorBase,
                CodigoCiudad: $scope.Codigo,
                CodigoPostal: $scope.Modelo.CodigoPostal,
                CalcularICAOficina: $scope.Modelo.CalcularICAOficina.Codigo,
                Estado: $scope.ModalEstado.Codigo,
                UsuarioCrea: $scope.Sesion.UsuarioAutenticado.Codigo,
                UsuarioModifica: $scope.Sesion.UsuarioAutenticado.Codigo
            }
            CiudadesFactory.Guardar(filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Codigo === 0) {
                                ShowSuccess('Se guardó la ciudad ' + $scope.Ciudad);
                                location.href = '#!ConsultarCiudades/' + response.data.Datos;
                            } else {
                                ShowSuccess('Se modificó la ciudad ' + $scope.Ciudad);
                                location.href = '#!ConsultarCiudades/' + $scope.Codigo;
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
    };


    function DatosRequeridosCiudad() {
        window.scrollTo(top, top);
        $scope.MensajesError = [];
        var continuar = true;

        if ($scope.Modelo.CodigoDane === undefined || $scope.Modelo.CodigoDane.Codigo === '' || $scope.Modelo.CodigoDane.Codigo === undefined || $scope.Modelo.CodigoDane.Codigo === 0) {
            $scope.Modelo.CodigoDane.Codigo = 0
        }
        if ($scope.Ciudad === undefined || $scope.Ciudad === '') {
            $scope.MensajesError.push('Debe ingresar el nombre de la ciudad');
            continuar = false;
        }
        if ($scope.Departamento === undefined || $scope.Departamento.Codigo === '' || $scope.Departamento.Codigo === undefined || $scope.DepartamentoValido === false) {
            $scope.MensajesError.push('Debe ingresar el departamento');
            continuar = false;
        }
        //if ($scope.TarifaICA == undefined || $scope.TarifaICA == '') {
        //    $scope.TarifaICA = 0;
        //}
        //if ($scope.ValorBase == undefined || $scope.ValorBase == '') {
        //    $scope.ValorBase = 0;
        //}
        if ($scope.TarifaICA > 9) {
            $scope.MensajesError.push('La tarifa ICA debe ser menor a 9');
            continuar = false;
        }

        return continuar;
    }

    $scope.VolverMaster = function () {
        if ($routeParams.Codigo > 0) {
            document.location.href = '#!ConsultarCiudades/' + $routeParams.Codigo;
        } else {
            document.location.href = '#!ConsultarCiudades';
        }
    };
    $scope.MaskNumero = function () {
        MascaraNumeroGeneral($scope)
    };

    $scope.MaskMayus = function () {
        MascaraMayusGeneral($scope)
    };

    $scope.MaskMoneda = function () {
        $scope.TarifaICA = MascaraDecimales($scope.TarifaICA)
    }
    $scope.MaskNumero = function (option) {
        try { $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno) } catch (e) { }
        try { $scope.ValorBase = MascaraNumero($scope.ValorBase) } catch (e) { }
    };
}]);