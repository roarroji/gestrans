﻿SofttoolsApp.controller('GestionarCodigosMunicipiosDaneCtrl', ['$scope', '$linq', '$timeout', '$routeParams', 'blockUI', 'CodigosDaneCiudadesFactory',
    function ($scope, $linq, $timeout, $routeParams, blockUI, CodigosDaneCiudadesFactory) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Codigos Municipios DANE' }, { Nombre: 'Gestionar' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CODIGOS_MUNICIPIOS_DANE);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.MensajesError = [];

        $scope.Codigo = '';
        $scope.Nombre = '';
        $scope.CodigoCiudad = '';
        $scope.CodigoDepartamento = '';
        $scope.CodigoDivision = '';
        $scope.Obtenido = 0;
        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarCodigosMunicipiosDane/';
        };

        //Obtener Parametros:
        if ($routeParams.Codigo > 0) {
            $scope.Codigo = $routeParams.Codigo;
            Obtener();
        }

        //Guardar:
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Codigo,
                    Nombre: $scope.Nombre,
                    Ciudad: { CodigoAlterno: $scope.CodigoCiudad },
                    Departamento: { Codigo: $scope.CodigoDepartamento },          
                    CodigoDivision: $scope.CodigoDivision,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Obtenido: $scope.Obtenido
                }

                CodigosDaneCiudadesFactory.Guardar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Obtenido == 0) {
                                    ShowSuccess('Se guardó el Municipio "' + $scope.Nombre + '"');
                                }
                                else {
                                    ShowSuccess('Se Modificó el Municipio "' + $scope.Nombre + '"');
                                }
                                location.href = '#!ConsultarCodigosMunicipiosDane/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Codigo == undefined || $scope.Codigo == null || $scope.Codigo == '') {
                $scope.MensajesError.push('Debe ingresar un código');
            }
            if ($scope.Nombre == undefined || $scope.Nombre == '' || $scope.Nombre == null) {
                $scope.MensajesError.push('Debe ingresar un Nombre');
                continuar = false;
            }
            if ($scope.CodigoCiudad == undefined || $scope.CodigoCiudad == '' || $scope.CodigoCiudad == null) {
                $scope.MensajesError.push('Debe ingresar un código ciudad');
                continuar = false;
            }
            if ($scope.CodigoDepartamento == undefined || $scope.CodigoDepartamento == '' || $scope.CodigoDepartamento == null) {
                $scope.MensajesError.push('Debe ingresar un código departamento');
                continuar = false;
            }
           
            return continuar;
        }

        function Obtener() {
            blockUI.start('Cargando Municipio código No. ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Municipio Código No.' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            blockUI.delay = 1000;
            CodigosDaneCiudadesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Obtenido = response.data.Datos.Codigo;

                        $scope.Codigo = response.data.Datos.Codigo;                        
                        $scope.Nombre = response.data.Datos.Nombre;
                        $scope.CodigoCiudad = response.data.Datos.Ciudad.Codigo;
                        $scope.CodigoDepartamento = response.data.Datos.Departamento.Codigo;
                        $scope.CodigoDivision = response.data.Datos.CodigoDivision;

                    }
                    else {
                        ShowError('No se logro consultar el Municipio código ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarCodigosMunicipiosDane';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                        document.location.href = '#!ConsultarCodigosMunicipiosDane';
                });

            blockUI.stop();
        };

        //Máscaras:
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        $scope.MaskNumero = function () {
            MascaraValoresGeneral($scope);
        }
    }
]);