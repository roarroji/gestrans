﻿SofttoolsApp.controller("GestionarDepartamentosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'DepartamentosFactory', 'PaisesFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, DepartamentosFactory, PaisesFactory) {

        $scope.Titulo = 'GESTIONAR DEPARTAMENTOS';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Departamentos' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_DEPARTAMENTOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0
        }

        $scope.ListadoPaises = []
        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVO" },
            { Codigo: 1, Nombre: "ACTIVO" },
        ]

        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        }
        PaisesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListadoPaises = response.data.Datos;

                        if ($scope.Pais > 0) {
                            $scope.Modelo.Pais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + $scope.Pais);
                        } else {
                            $scope.Modelo.Pais = $scope.ListadoPaises[$scope.ListadoPaises.length - 1];
                        }
                    }
                    else {
                        $scope.ListadoPaises = [];
                        $scope.Modelo.Pais = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });


        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando departamento código No. ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando departamento Código No.' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            DepartamentosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Modelo.Codigo = response.data.Datos.Codigo;
                        $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno;
                        $scope.Modelo.Nombre = response.data.Datos.Nombre;
                        $scope.Modelo.NombreCorto = response.data.Datos.NombreCorto;
                        $scope.Pais = response.data.Datos.Pais.Codigo;
                        if ($scope.ListadoPaises.length > 0 && $scope.Pais > 0) {
                            $scope.Modelo.Pais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + response.data.Datos.Pais.Codigo);
                        }

                        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                    }
                    else {
                        ShowError('No se logro consultar el departamento código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarDepartamentos';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarDepartamentos';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                //Estado
                $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

                DepartamentosFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el departamento "' + $scope.Modelo.Nombre + '"');
                                }
                                else {
                                    ShowSuccess('Se modificó el departamento "' + $scope.Modelo.Nombre + '"');
                                }
                                location.href = '#!ConsultarDepartamentos/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Modelo.CodigoAlterno == undefined || $scope.Modelo.CodigoAlterno == null) {
                $scope.Modelo.CodigoAlterno = ''
            }
            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
                $scope.MensajesError.push('Debe ingresar el nombre del departamento');
                continuar = false;
            }
            if ($scope.Modelo.Pais == undefined || $scope.Modelo.Pais == '' || $scope.Modelo.Pais == null || $scope.Modelo.Pais.Codigo == 0) {
                $scope.MensajesError.push('Debe ingresar el país del departamento');
                continuar = false;
            }
            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarDepartamentos/' + $scope.Modelo.Codigo;
        };
        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
            try { $scope.Modelo.NombreCorto = $scope.Modelo.NombreCorto.toUpperCase() } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
        };
    }]);