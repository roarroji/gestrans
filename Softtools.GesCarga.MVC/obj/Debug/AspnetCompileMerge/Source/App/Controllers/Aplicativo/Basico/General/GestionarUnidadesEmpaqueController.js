﻿SofttoolsApp.controller('GestionarUnidadesEmpaqueCtrl', ['$scope', '$timeout', '$routeParams', '$linq', 'blockUI', 'UnidadesEmpaqueProductosTransportadosFactory',
    function ($scope, $timeout, $routeParams, $linq, blockUI, UnidadesEmpaqueProductosTransportadosFactory) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Unidades Empaque' }, { Nombre: 'Gestionar'}];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_UNIDADES_EMPAQUE);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.MensajesError = [];


        $scope.ListadoEstados = [
            { Nombre: 'ACTIVO', Codigo: 1 },
            {Nombre: 'INACTIVO', Codigo: 0}
        ];

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        $scope.Nombre = '';
        $scope.CodigoAlterno = '';
        $scope.Codigo = '';
        $scope.NombreCorto = '';


        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarUnidadesEmpaque/';
        };

        //Obtener Parametros:
        if ($routeParams.Codigo > 0) {
            $scope.Codigo = $routeParams.Codigo;
            Obtener();
        }


        //Guardar:
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo : $scope.Codigo,
                    Nombre: $scope.Nombre,
                    CodigoAlterno: $scope.CodigoAlterno,
                    NombreCorto: $scope.NombreCorto,
                    Estado: $scope.Estado.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                }
               
                UnidadesEmpaqueProductosTransportadosFactory.Guardar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Codigo == 0) {
                                    ShowSuccess('Se guardó la Unidad "' + $scope.Nombre + '"');
                                }
                                else {
                                    ShowSuccess('Se Modificó la Unidad "' + $scope.Nombre + '"');
                                }
                                location.href = '#!ConsultarUnidadesEmpaque/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.CodigoAlterno == undefined || $scope.CodigoAlterno == null || $scope.CodigoAlterno == '') {
                $scope.MensajesError.push('Debe ingresar un codigo alterno');
            }
            if ($scope.Nombre == undefined || $scope.Nombre == '' || $scope.Nombre == null) {
                $scope.MensajesError.push('Debe ingresar un Nombre');
                continuar = false;
            }
            if ($scope.NombreCorto == undefined || $scope.NombreCorto == '' || $scope.NombreCorto == null) {
                $scope.MensajesError.push('Debe ingresar un nombre corto');
                continuar = false;
            }
            return continuar;
        }


        function Obtener() {
            blockUI.start('Cargando Uniad código No. ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Unidad Código No.' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            blockUI.delay = 1000;
            UnidadesEmpaqueProductosTransportadosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Codigo = response.data.Datos.Codigo;
                        $scope.CodigoAlterno = response.data.Datos.CodigoAlterno;
                        $scope.Nombre = response.data.Datos.Nombre;
                        $scope.NombreCorto = response.data.Datos.NombreCorto;
                        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                    }
                    else {
                        ShowError('No se logro consultar La Unidad código ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarUnidadesEmpaque';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                        document.location.href = '#!ConsultarUnidadesEmpaque';
                });

            blockUI.stop();
        };


        //Máscaras:
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };


    }
]);