﻿SofttoolsApp.controller("GestionarSubSistemasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'SubSistemasFactory', 'ValorCatalogosFactory', 'SistemasFactory', 'TipoSubsistemasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, SubSistemasFactory, ValorCatalogosFactory, SistemasFactory, TipoSubsistemasFactory) {

        $scope.Titulo = 'GESTIONAR SUBSISTEMAS DE MANTENIMIENTO';


        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SUBSISTEMAS);

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        // Se crea la propiedad modelo en el ambito
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Mantenimineto' }, { Nombre: 'Subsistemas' }];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CodigoAlterno: ''
        };
        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo;

        }
        else {
            $scope.Modelo.Codigo = 0;
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar el combo de estados*/

        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ]
        $scope.Modelo.Estado = $scope.ListadoEstados[0];

    /*Cargar el combo de Sistema mantenimiento*/

        SistemasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoSistemaMantenimiento = [];
                    $scope.ListadoSistemaMantenimiento = response.data.Datos;
                    $scope.ListadoSistemaMantenimiento.push({ Nombre: '(TODOS)', Codigo: 0 });
                    $scope.ListadoSistemaMantenimiento.Asc = true
                    OrderBy('Nombre', undefined, $scope.ListadoSistemaMantenimiento)
                    $scope.SistemaMantenimiento = $linq.Enumerable().From($scope.ListadoSistemaMantenimiento).First('$.Codigo == 0');
                    $timeout(function () {
                        $('#sistemaMantenimiento').selectpicker()
                    }, 100)

                }
            }, function (response) {
                ShowError(response.statusText);
            });
         
        /*Cargar el combo de Tipo Subsistema*/
        TipoSubsistemasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoSistemaMantenimiento = [];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoTipoSistemaMantenimiento.push(item);
                    });
                    $scope.TipoSubSistema = response.data.Datos[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR SUBSISTEMA MANTENIMIENTO';
            $scope.Deshabilitar = true;
            Obtener();
        }
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando Subsistema Código ' + $scope.Modelo.Codigo); 
            $timeout(function () {
                blockUI.message('Cargando Subsistema Código ' + $scope.Modelo.Codigo);
            }, 200); 
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            }; 
            blockUI.delay = 1000;
            SubSistemasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Nombre = response.data.Datos.Nombre
                        $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado.Codigo); 
                        $scope.SistemaMantenimiento = $linq.Enumerable().From($scope.ListadoSistemaMantenimiento).First('$.Codigo ==' + response.data.Datos.SistemaMantenimiento.Codigo);
                        $scope.TipoSubSistema = $linq.Enumerable().From($scope.ListadoTipoSistemaMantenimiento).First('$.Codigo ==' + response.data.Datos.TipoSubsistema.Codigo);
                    }
                    else {
                        ShowError('No se logro consultar el Subsistema No.' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarSubsistemas';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    //ShowError('No se logro consultar el Subsistema No.' + $scope.Modelo.Codigo + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!ConsultarSubsistemas';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardarSubsistema');
        };
        // Metodo para guardar /modificar
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarSubsistema');
            $scope.Modelo.Nombre = $scope.Modelo.Nombre; 
            $scope.Modelo.SistemaMantenimiento = $scope.SistemaMantenimiento;
            $scope.Modelo.TipoSubsistema = $scope.TipoSubSistema;
            console.log($scope.Modelo.SistemaMantenimiento            )

            if (DatosRequeridos()) { 
                SubSistemasFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el Subsistema ' + $scope.Modelo.Nombre);
                                    document.location.href = '#!ConsultarSubsistemas/' + response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modificó el Subsistema ' + $scope.Modelo.Nombre);
                                    document.location.href = '#!ConsultarSubsistemas/' + response.data.Datos;
                                }
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == "") {
                $scope.MensajesError.push('Debe ingresar el nombre del Subsistema');
                continuar = false;
            }

            if ($scope.Modelo.SistemaMantenimiento == undefined || $scope.Modelo.SistemaMantenimiento.Codigo == 0 || $scope.Modelo.SistemaMantenimiento == null) {
                $scope.MensajesError.push('Debe ingresar el Sistema  de Mantenimiento');
                continuar = false;
            }
           
            //if ($scope.Modelo.TipoSubSistema == undefined || $scope.Modelo.TipoSubSistema == "" || $scope.Modelo.TipoSubSistema == null) {
            //    $scope.MensajesError.push('Debe ingresar el tipo de Subsistema');
            //    continuar = false;
            //}
            return continuar;
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if (parseInt($scope.Modelo.Codigo) > 0) {
                document.location.href = '#!ConsultarSubsistemas/' + $scope.Modelo.Codigo;
            }
            else {
                document.location.href = '#!ConsultarSubsistemas';
            }
        };
        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
    }]);