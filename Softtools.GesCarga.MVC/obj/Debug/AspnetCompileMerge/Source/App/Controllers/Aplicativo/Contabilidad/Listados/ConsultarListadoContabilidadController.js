﻿SofttoolsApp.controller("ConsultarListadoContabilidadCtrl", ['$scope', '$timeout', 'TercerosFactory', 'RutasFactory', 'OficinasFactory', 'TarifarioVentasFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'EmpresasFactory',
    function ($scope, $timeout, TercerosFactory, RutasFactory, OficinasFactory, TarifarioVentasFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, EmpresasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Contabilidad' }, { Nombre: 'Listados' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.NombreReporte = '';
        $scope.FiltroArmado = '';
        $scope.ListadoCliente = [];
        $scope.FiltroCliente = true;
        $scope.FiltroTransportador = false;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.ModeloTransportador = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {} }
        }

        $scope.ModeloFechaInicial = new Date();
        $scope.ModeloFechaFinal = new Date();

        try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OCION_MENU_LISTADOS_CONTABILIDAD);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        // Cargar combos de módulo y listados 
        $scope.ListaDespacho = [];
        for (var i = 0; i < $scope.ListadoMenuListados.length; i++) {
            if ($scope.ListadoMenuListados[i].Padre == OCION_MENU_LISTADOS_CONTABILIDAD) {
                $scope.ListaDespacho.push({ NombreMenu: $scope.ListadoMenuListados[i].Nombre, Codigo: $scope.ListadoMenuListados[i].Codigo })
            }
        }

        $scope.ModeloDocumentosDespachos = $scope.ListaDespacho[0]

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;


        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });


        /*Cargar el combo de ESTADO DE REMESAS PAQUETERIA*/

        $scope.ListadoEstadoRemesaPaqueteria = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 0, Nombre: 'BORRADOR' }, { Codigo: 1, Nombre: 'DEFINITIVO' }, { CodiEstadoGuiago: 2, Nombre: 'ANULADO' }];
        $scope.ModeloEstadoGuia = $scope.ListadoEstadoRemesaPaqueteria[0];


        /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)

        };


        OficinasFactory.Consultar({ CodigoCiudad: $scope.CodigoCiudad, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinas = [];
                    $scope.HabilitarCiudad = false
                    $scope.ListadoOficinas = [
                        { Codigo: 0, Nombre: '(TODOS)' }
                    ];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item)
                    });
                    if ($scope.ListadoOficinas.length > 0) {
                        $scope.ModeloOficinas = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==0');

                    }

                }
            }, function (response) {
                ShowError(response.statusText);
            });




        $scope.DesplegarInforme = function (OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.DatosRequeridos();

            if (DatosRequeridos == true) {

                //Depende del listado seleccionado se enviará el nombre por parametro

                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_COMPROBANTE_CONTABLE_DETALLADO) {
                    $scope.NombreReporte = NOMBRE_LITADO_COMPROBANTE_CONTABLE_DETALLADO
                }

                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_COMPROBANTE_CONTABLE_RESUMIDO) {
                    $scope.NombreReporte = NOMBRE_LITADO_COMPROBANTE_CONTABLE_RESUMIDO
                }

                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_UIAF) {
                    $scope.NombreReporte = NOMBRE_LITADO_UIAF
                }

                $scope.ArmarFiltro();

                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
                }
                if (OpcionEXCEL == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf);
                }

                //Se limpia el filtro para poder hacer mas consultas
                $scope.FiltroArmado = '';

            }
        };

        $scope.DatosRequeridos = function () {
            DatosRequeridos = true;
            $scope.MensajesError = [];
            $scope.Rango = $scope.ModeloNumeroFinal - $scope.ModeloNumeroInicial;

            if (($scope.ModeloFechaInicial == null || $scope.ModeloFechaInicial == undefined || $scope.ModeloFechaInicial == '')
                && ($scope.ModeloFechaFinal == null || $scope.ModeloFechaFinal == undefined || $scope.ModeloFechaFinal == '')
                && ($scope.ModeloNumeroInicial == null || $scope.ModeloNumeroInicial == undefined || $scope.ModeloNumeroInicial == '' || $scope.ModeloNumeroInicial == 0)
                && ($scope.ModeloNumeroFinal == null || $scope.ModeloNumeroFinal == undefined || $scope.ModeloNumeroFinal == '' || $scope.ModeloNumeroFinal == 0)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de números o fechas');
                DatosRequeridos = false

            } else if (($scope.ModeloNumeroInicial !== null && $scope.ModeloNumeroInicial !== undefined && $scope.ModeloNumeroInicial !== '' && $scope.ModeloNumeroInicial !== 0)
                || ($scope.ModeloNumeroFinal !== null && $scope.ModeloNumeroFinal !== undefined && $scope.ModeloNumeroFinal !== '' && $scope.ModeloNumeroFinal !== 0)
                || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloNumeroInicial !== null && $scope.ModeloNumeroInicial !== undefined && $scope.ModeloNumeroInicial !== '' && $scope.ModeloNumeroInicial !== 0)
                    && ($scope.ModeloNumeroFinal !== null && $scope.ModeloNumeroFinal !== undefined && $scope.ModeloNumeroFinal !== '' && $scope.ModeloNumeroFinal !== 0)) {
                    if ($scope.ModeloNumeroFinal < $scope.ModeloNumeroInicial) {
                        $scope.MensajesError.push('El número final debe ser mayor al número final');
                        DatosRequeridos = false
                    }
                    else if ($scope.Rango > RANGO_MAXIMO_CANTIDAD_DOCUMENTOS_LISTADOS) {
                        $scope.MensajesError.push('La diferencia del rango entre el numero inicial y el numero final no puede ser mayor a 2000');
                        DatosRequeridos = false
                    }
                } else {
                    if (($scope.ModeloNumeroInicial !== null && $scope.ModeloNumeroInicial !== undefined && $scope.ModeloNumeroInicial !== '' && $scope.ModeloNumeroInicial !== 0)) {
                        $scope.ModeloNumeroFinal = $scope.ModeloNumeroInicial
                    } else {
                        $scope.ModeloNumeroInicial = $scope.ModeloNumeroFinal
                    }
                }
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        DatosRequeridos = false
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > 60) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a 60 dias');
                        DatosRequeridos = false
                    }
                }

                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal
                    }
                }
            }

            return DatosRequeridos;

        }

        $scope.ArmarFiltro = function () {
            // 

            if ($scope.ModeloNumeroInicial !== undefined && $scope.ModeloNumeroInicial !== '' && $scope.ModeloNumeroInicial !== null && $scope.ModeloNumeroInicial > 0) {
                $scope.FiltroArmado += '&Numero_Documento_Inicial=' + $scope.ModeloNumeroInicial;
            }
            if ($scope.ModeloNumeroFinal !== undefined && $scope.ModeloNumeroFinal !== '' && $scope.ModeloNumeroFinal !== null && $scope.ModeloNumeroFinal > 0) {
                $scope.FiltroArmado += '&Numero_Documento_Final=' + $scope.ModeloNumeroFinal;
            }
            if ($scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '' && $scope.ModeloFechaInicial !== null) {
                var ModeloFechaInicial = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaInicial);
                $scope.FiltroArmado += '&Fecha_Incial=' + ModeloFechaInicial;
            }
            if ($scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '' && $scope.ModeloFechaFinal !== null) {
                var ModeloFechaFinal = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaFinal);
                $scope.FiltroArmado += '&Fecha_Final=' + ModeloFechaFinal;
            }
            if ($scope.ModeloCliente !== undefined && $scope.ModeloCliente !== '' && $scope.ModeloCliente !== null) {
                $scope.FiltroArmado += '&Identificacion_Cliente=' + $scope.ModeloCliente;
            }

            if ($scope.ModeloOficinas.Codigo !== undefined && $scope.ModeloOficinas.Codigo !== '' && $scope.ModeloOficinas.Codigo !== null && $scope.ModeloOficinas.Codigo > 0) {
                $scope.FiltroArmado += '&Codigo_Oficina=' + $scope.ModeloOficinas.Codigo;
            }
            if ($scope.ModeloRuta !== undefined && $scope.ModeloRuta !== '' && $scope.ModeloRuta !== null) {
                $scope.FiltroArmado += '&Ruta=' + $scope.ModeloRuta;
            }
            if ($scope.ModeloEstadoGuia.Codigo != -1) {
                $scope.FiltroArmado += '&Estado=' + $scope.ModeloEstadoGuia.Codigo;
            }
        }
    }]);