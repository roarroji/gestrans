﻿SofttoolsApp.controller('ConsultarControlContenedoresCtrl', ['$scope', '$timeout', '$linq', 'blockUI','OficinasFactory','RemesasFactory',
    function ($scope, $timeout, $linq, blockUI, OficinasFactory, RemesasFactory) {
        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Consultas' }, { Nombre: 'Control Contenedores' }];

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONTROL_CONTENEDORES); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.FiltroArmado = '';
        $scope.FechaInicial = new Date();
        $scope.FechaFinal = new Date();
        $scope.Planilla = '';
        $scope.Remesa = '';
        $scope.Cliente = '';
        $scope.Contenedor = '';
        $scope.DocumentoCliente = '';
        $scope.Placa = '';
        $scope.NumeroDocumentoOrdenServicio = '';

        

        $scope.ListadoOficinas = [];

        OficinasFactory.Consultar({CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa}).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {
                    $scope.ListadoOficinas = response.data.Datos;
                    $scope.ListadoOficinas.push({ Codigo: -1, Nombre: '(TODAS)' });
                    $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                }
            });


        $scope.ListadoEstados = [
            { Codigo: -1, Nombre: '(NO APLICA)' },
            { Codigo: 0, Nombre: 'DEVUELTO' },
            { Codigo: 1, Nombre: 'SIN DEVOLUCIÓN' }

        ];

        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');


        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            $scope.Buscar();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                $scope.Buscar();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                $scope.Buscar();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            $scope.Buscar();
        };


        $scope.Buscar = function () {
            
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoControl = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        FechaInicial: $scope.FechaInicial,
                        FechaFinal: $scope.FechaFinal,
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        NumeroPlanilla: $scope.Planilla,
                        NumeroDocumento: $scope.Remesa,
                        FiltroCliente: $scope.Cliente,
                        NumeroContenedor: $scope.Contenedor,
                        NumeroDocumentoCliente: $scope.DocumentoCliente,
                        Vehiculo: {Placa: $scope.Placa},
                        Estado: $scope.Estado.Codigo,
                        Oficina: {Codigo: $scope.Oficina.Codigo},
                        TarifaTransporte : 1, //Contenedor
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        NumeroDocumentoOrdenServicio: $scope.NumeroDocumentoOrdenServicio
                    }
                    blockUI.delay = 1000;
                    RemesasFactory.Consultar(filtros).
                        then(function (response) {
                            $scope.ListadoControl = [];
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoControl = response.data.Datos;
                                    for (var i = 0; i < response.data.Datos.length; i++) {
                                        if (new Date(response.data.Datos[i].DSOS.FechaDevolucionContenedor) <= new Date(FORMATO_FECHA_MINIMA) || new Date(response.data.Datos[i].DSOS.FechaDevolucionContenedor) <= new Date('1900-01-01T00:00:00-05:00')) {
                                            response.data.Datos[i].DSOS.FechaDevolucionContenedor = '';
                                        }
                                        if (response.data.Datos[i].FechaDevolucion <= FORMATO_FECHA_MINIMA || response.data.Datos[i].FechaDevolucion <= new Date('1900-01-01T00:00:00-05:00')) {
                                            response.data.Datos[i].FechaDevolucion = '';
                                        }
                                        if (response.data.Datos[i].FechaDescargueMercancia <= FORMATO_FECHA_MINIMA || response.data.Datos[i].FechaDescargueMercancia <= new Date('1900-01-01T00:00:00-05:00')) {
                                            response.data.Datos[i].FechaDescargueMercancia = '';
                                        }

                                        var fechaActual = moment(new Date().setUTCHours(0,0,0))
                                        var fechateoricaDevolucion = moment(new Date(response.data.Datos[i].DSOS.FechaDevolucionContenedor));

                                        if (response.data.Datos[i].FechaDevolucion <= FORMATO_FECHA_MINIMA || response.data.Datos[i].FechaDevolucion <= new Date('1900-01-01T00:00:00-05:00')) {
                                            if (fechateoricaDevolucion.diff(fechaActual, 'days') <= 1) {
                                                $scope.ListadoControl[i].EstadoDevolucion = 'label-danger';
                                            } else if ((fechateoricaDevolucion.diff(fechaActual, 'days') > 1) && (fechateoricaDevolucion.diff(fechaActual, 'days') <= 2)) {
                                                $scope.ListadoControl[i].EstadoDevolucion = 'label-warning';
                                            } else {
                                                $scope.ListadoControl[i].EstadoDevolucion = 'label-success';
                                            }
                                            
                                        } else {

                                            $scope.ListadoControl[i].EstadoDevolucion = '';
                                            
                                        }
                                    }
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }

                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }

        $scope.AsignarFechas = function (item) {
            item.FechaReal = new Date(item.FechaDevolucion);
            item.FechaMercancia = new Date(item.FechaDescargueMercancia);
        }

        $scope.GuardarCambios = function (item) {
            var fechaGrid = item.FechaDevolucion;
            var FechaActual = new Date().setUTCHours(0, 0, 0, 0);
            var FechaMin = new Date(FechaActual)
            FechaMin.setDate(FechaMin.getDate() - 4)
            item.FechaDevolucion = item.FechaReal;
            var fechaModal = new Date(item.FechaReal);
            //if (fechaModal < FechaMin) {
                //item.FechaDevolucion = fechaGrid;
                //ShowError('La fecha real de devolución incumple los parámetros de almacenamiento de información');
                
           // } else {
                RemesasFactory.Guardar(item).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            ShowSuccess('se guardó la información');
                        }
                    });
            //}
        }

        $scope.DesplegarInforme = function (OpcionPDf, OpcionEXCEL) {  
            $scope.ArmarFiltro();
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
                    
            
                //Depende del listado seleccionado se enviará el nombre por parametro
                
            $scope.NombreReporte = NOMBRE_LISTADO_CONTROL_CONTENEDORES;
                  
                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
                }
                if (OpcionEXCEL == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL);
                }

               

            
        };


        $scope.ArmarFiltro = function () {
            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';
          
            if ($scope.FechaInicial !== undefined && $scope.FechaInicial !== '' && $scope.FechaInicial !== null) {
                var ModeloFechaInicial = Formatear_Fecha_Mes_Dia_Ano($scope.FechaInicial);
                $scope.FiltroArmado += '&Fecha_Incial=' + ModeloFechaInicial;
            }
            if ($scope.FechaFinal !== undefined && $scope.FechaFinal !== '' && $scope.FechaFinal !== null) {
                var FechaFinal = Formatear_Fecha_Mes_Dia_Ano($scope.FechaFinal);
                $scope.FiltroArmado += '&Fecha_Final=' + FechaFinal;
            }
            if ($scope.NumeroDocumentoOrdenServicio !== undefined && $scope.NumeroDocumentoOrdenServicio !== '' && $scope.NumeroDocumentoOrdenServicio !== null) {
                $scope.FiltroArmado += '&Orden_Servicio=' + $scope.NumeroDocumentoOrdenServicio;
            }
            if ($scope.Remesa !== undefined && $scope.Remesa !== '' && $scope.Remesa !== null) {
                $scope.FiltroArmado += '&Remesa=' + $scope.Remesa;
            }
            if ($scope.Cliente !== undefined && $scope.Cliente !== '' && $scope.Cliente !== null) {
                $scope.FiltroArmado += '&Cliente=' + $scope.Cliente;
            }
            if ($scope.Contenedor !== undefined && $scope.Contenedor !== '' && $scope.Contenedor !== null) {
                $scope.FiltroArmado += '&Contenedor=' + $scope.Contenedor;
            }
            if ($scope.Placa !== undefined && $scope.Placa !== '' && $scope.Placa !== null ) {
                $scope.FiltroArmado += '&Placa=' + $scope.Placa;
            }
            if ($scope.Oficina.Codigo != -1 && $scope.Oficina.Codigo > 0 && $scope.Oficina != undefined && $scope.Oficina != null && $scope.Oficina != '') {
                $scope.FiltroArmado += '&Oficina=' + $scope.Oficina.Codigo;
            }
            if ($scope.Estado.Codigo != -1 && $scope.Estado.Codigo > 0 && $scope.Estado != undefined && $scope.Estado != null && $scope.Estado != '') {
                $scope.FiltroArmado += '&Estado=' + $scope.Estado.Codigo;
            }
            $scope.FiltroArmado += '&Tarifa=1';

        }


        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope);
        }

      
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        }
    }
     
]);