﻿SofttoolsApp.controller("ConsultarPrecintosUnicosCtrl", ['$scope', '$timeout', 'ManifiestoFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'OficinasFactory', 'EmpresasFactory', 'PrecintosFactory',
    function ($scope, $timeout, ManifiestoFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, OficinasFactory, EmpresasFactory, PrecintosFactory) {
        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Consultar' }, { Nombre: 'Precintos' }];
        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.pref = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoOficinasOrigen = [];
        $scope.ListadoOficinasActual = [];
        $scope.Numero = 0;
        $scope.NumeroDocumento = 0;
        var MeapCodigo = 0;
        var OPCION_MENU = OPCION_MENU_CONSULTAS_PRECINTOS;
        if ($routeParams.MeapCodigo !== undefined && $routeParams.MeapCodigo !== null && $routeParams.MeapCodigo !== '' && $routeParams.MeapCodigo !== 0) {
            MeapCodigo = parseInt($routeParams.MeapCodigo);
            if (MeapCodigo > 0) {
                switch (MeapCodigo) {
                    case OPCION_MENU_CONSULTAS_PRECINTOS:
                        OPCION_MENU = OPCION_MENU_CONSULTAS_PRECINTOS;
                        break;
                    case OPCION_MENU_PAQUETERIA.CONSULTA_PRECINTOS:
                        OPCION_MENU = OPCION_MENU_PAQUETERIA.CONSULTA_PRECINTOS;
                        $scope.MapaSitio = [{ Nombre: 'Paqueteria' }, { Nombre: 'Consultar' }, { Nombre: 'Precintos' }];
                        break;
                    default:
                }
            }
        }
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.ModeloFechaFinal = new Date();
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarManifiesto';
            }
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            if ($routeParams.Numero > 0) {
                $scope.ModeloNumero = $routeParams.Numero;
                Find();
            }
        }

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'DISPONIBLE', Codigo: CERO },
            { Nombre: 'OCUPADO', Codigo: CODIGO_UNO },
            { Nombre: 'ANULADO', Codigo: CODIGO_DOS },
        ]

        $scope.ModeloEstado = $scope.ListadoEstados[CERO]

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });
        $scope.AnularDocumento = function (NumeroDocumento) {

            $scope.ModalErrorCompleto = ''
            $scope.NumeroDocumento = NumeroDocumento
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnular');
        };

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                $scope.ListadoOficinasOrigen.push({ Nombre: '(TODAS)', Codigo: -1 })
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinasOrigen.push(item)
                    });
                    if ($scope.Sesion.UsuarioAutenticado.Prefijo == PREFIJO_INTEGRA) {
                        $scope.ModeloOficinaOrigen = $linq.Enumerable().From($scope.ListadoOficinasOrigen).First('$.Codigo == 1');
                    } else {
                        $scope.ModeloOficinaOrigen = $linq.Enumerable().From($scope.ListadoOficinasOrigen).First('$.Codigo == -1');
                    }

                }
            }, function (response) {
                ShowError(response.statusText);
            });

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                $scope.ListadoOficinasActual.push({ Nombre: '(TODAS)', Codigo: -1 })
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinasActual.push(item)
                    });
                    if ($scope.Sesion.UsuarioAutenticado.Prefijo == PREFIJO_INTEGRA) {
                        $scope.ModeloOficinaActual = $linq.Enumerable().From($scope.ListadoOficinasActual).First('$.Codigo == 1');
                    } else {
                        $scope.ModeloOficinaActual = $linq.Enumerable().From($scope.ListadoOficinasActual).First('$.Codigo == -1');
                    }

                }
            }, function (response) {
                ShowError(response.statusText);
            });

        function Find() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroInicial: $scope.ModeloNumeroInicial,
                NumeroFinal: $scope.ModeloNumeroFinal,
                Oficina: { Codigo: $scope.ModeloOficinaActual.Codigo },
                OficinaOrigen: { Codigo: $scope.ModeloOficinaOrigen.Codigo },
                ENPD_Numero: $scope.ModeloNumeroPlanilla,
                ENRE_Numero: $scope.ModeloNumeroRemesa,
                ENMD_Numero: $scope.ModeloNumeroManifiesto,
                Estado: $scope.ModeloEstado.Codigo,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas
            };

            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoPrecintos = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    blockUI.delay = 1000;
                    $scope.Pagina = $scope.paginaActual
                    $scope.RegistrosPagina = 10
                    PrecintosFactory.ConsultarPrecinto(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Numero = 0
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoPrecintos = response.data.Datos
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }

                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };

        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }
        $scope.Anular = function () {
            var aplicaLiquidaciones = false
            var aplicaCumplidos = false

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Usuario_Anula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Causa_Anula: $scope.ModeloCausaAnula,
                Numero: $scope.NumeroDocumento,

            };
            if (DatosRequeridosAnular()) {
                PrecintosFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anuló el precinto  ' + $scope.NumeroDocumento);
                            closeModal('modalAnular');
                            Find();
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = ''
                        showModal('modalMensajeAnularOrdenCargue');
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede anular la orden de servicio ' + $scope.NumeroDocumento;
                        $scope.ModalErrorCompleto = response.statusText
                    });
            }
        };

        $scope.ArmarFiltro = function () {
            if ($scope.Numero != undefined && $scope.Numero != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.Numero;
            }
        }
        $scope.NumeroPrecintoTemp = 0
        $scope.LiberarDocumento = function (NumeroPrecinto) {
            $scope.NumeroPrecintoTemp = NumeroPrecinto
            ShowConfirm('Desea liberar el precinto No. ' + NumeroPrecinto+' de todos los documentos?', LiberarPrecinto)
        }

        function LiberarPrecinto() {
            var ResponsePrecintoLiberado = PrecintosFactory.LiberarPrecinto({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: $scope.NumeroPrecintoTemp, Sync: true });
            
            if (ResponsePrecintoLiberado != undefined) {
                if (ResponsePrecintoLiberado.Datos == true) {
                    ShowSuccess('Se liberó el precinto No: ' + $scope.NumeroPrecintoTemp + ' satisfactoriamente.');
                    Find();
                }
            }
        }

    }]);