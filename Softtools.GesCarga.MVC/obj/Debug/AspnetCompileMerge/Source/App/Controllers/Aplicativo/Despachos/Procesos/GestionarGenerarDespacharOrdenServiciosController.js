﻿SofttoolsApp.controller("GestionarGenerarDespacharOrdenServiciosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ValorCatalogosFactory', 'EncabezadoSolicitudOrdenServiciosFactory', 'OficinasFactory', 'VehiculosFactory', 'SemirremolquesFactory', 'TercerosFactory', 'CiudadesFactory', 'TarifarioComprasFactory', 'TarifarioVentasFactory', 'TipoTarifaTransporteCargaFactory', 'OrdenCargueFactory', 'RemesasFactory', 'PlanillaDespachosFactory', 'ManifiestoFactory', 'RutasFactory', 'ImpuestosFactory', 'blockUIConfig', 'ProductoTransportadosFactory', 'ReporteMinisterioFactory', 'TipoTarifaTransportesFactory', 'EnturnamientoFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ValorCatalogosFactory, EncabezadoSolicitudOrdenServiciosFactory, OficinasFactory, VehiculosFactory, SemirremolquesFactory, TercerosFactory, CiudadesFactory, TarifarioComprasFactory, TarifarioVentasFactory, TipoTarifaTransporteCargaFactory, OrdenCargueFactory, RemesasFactory, PlanillaDespachosFactory, ManifiestoFactory, RutasFactory, ImpuestosFactory, blockUIConfig, ProductoTransportadosFactory, ReporteMinisterioFactory, TipoTarifaTransportesFactory, EnturnamientoFactory) {
        console.clear()
        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Procesos' }, { Nombre: 'Despachar Orden Servicio' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.CodigoEstadoSolicitud = 0;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_DESPCHAR_ORDEN_SERVICIOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.Numero = 0;
        $scope.ListaOficinasDespacha = [];
        $scope.ListadoPermisosEspecificos = []
        $scope.NoVisualizarDocumento = false
        $scope.DeshabilitarValorEscolta = true;
        $scope.ListaPlaca = [];
        $scope.ListaPlacaSemi = [];
        $scope.ListaTenedores = [];
        $scope.ListaConductores = [];
        $scope.DeshabilitarRemesa = false;
        $scope.ModeloFecha = new Date();
        $scope.ListaAuxiliares = [];
        $scope.CodigoConductor = 0;
        $scope.ModalTotalFleteCliente = 0
        $scope.CodigoSemirremolque = 0;
        $scope.DeshabilitarRemesa = false;
        $scope.DesabilitarConfirmacionRemesa = false;
        $scope.DeshabilitarDespacho = false;
        $scope.DeshabilitarPlanilla = false;
        $scope.DeshabilitarOrdenCargue = false;
        $scope.DeshabilitarTarifaTransportador = false;
        $scope.DeshabilitarTarifaCliente = false;
        $scope.ModaloDocumentoCliente = '';
        $scope.ManejoCVCPDeseDespachar = false
        $scope.DeshabilitarVehiculo = false
        $scope.ValidaPeso = true
        $scope.ModalSeguroAnticipo = 0
        $scope.AutorizacionFlete = 0
        var OrdenServicio = {};
        var fechaBloqueo1 = new Date(new Date().setSeconds(86400));
        fechaBloqueo1.setHours(0)
        fechaBloqueo1.setMinutes(0)
        fechaBloqueo1.setMinutes(0)

        var fechaBloqueo2 = new Date(new Date().setSeconds(172800));
        fechaBloqueo2.setHours(0)
        fechaBloqueo2.setMinutes(0)
        fechaBloqueo2.setMinutes(0)

        $scope.fechaBloqueo1 = fechaBloqueo1
        $scope.fechaBloqueo2 = fechaBloqueo2

        $scope.Radio = { Anticipo: 'Conductor' }
                       
        $scope.ListadoPermisos.forEach(function (item) {
            if (item.Padre === OPCION_MENU_DESPCHAR_ORDEN_SERVICIOS) {
                $scope.ListadoPermisosEspecificos.push(item)
            }
        });
        $scope.VisualizacionTarifario = false
        $scope.CambioVehiculoEnturnado = false

        $scope.ListadoPermisosEspecificos.forEach(function (item) {
            if (item.Codigo === PERMISO_VISUALIZAR_TARIFARIO) {
                $scope.VisualizacionTarifario = true
            }
            if (item.Codigo === PERMISO_CAMBIAR_VEHICULO_ENTURNADO) {
                $scope.CambioVehiculoEnturnado = true
            }

        })

        //--------------- CARGAR COMBOS ---------------------------------//

        //ComboOficinaDespacha
        $scope.CargarOficinaDespacha = function () {
            $scope.ListaOficinasDespacha = []
            var Response = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true });
            $scope.ListaOficinasDespacha = ValidarListadoAutocomplete(Response.Datos, $scope.ListaOficinasDespacha)
            $scope.ModeloOficinaDespacha = $scope.ListaOficinasDespacha[0];
            if ($scope.CodigoOficinaDespacha !== undefined && $scope.CodigoOficinaDespacha !== null && $scope.CodigoOficinaDespacha !== '') {
                if ($scope.CodigoOficinaDespacha > 0) {
                    $scope.ModeloOficinaDespacha = $linq.Enumerable().From($scope.ListaOficinasDespacha).First('$.Codigo ==' + $scope.CodigoOficinaDespacha);
                }
            }
        }
        $scope.CargarOficinaDespacha()

        //Listado Tipo Tarifas
        $scope.CargarListadoTipoTarifaTransportes = function () {
            $scope.ListadoTipoTarifaTransportes = [];
            var Response = TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true });
            $scope.ListadoTipoTarifaTransportes = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTipoTarifaTransportes);
            TipoTarifaCarga = true;
        };
        $scope.CargarListadoTipoTarifaTransportes();

        //Autocomplete Funcionario
        $scope.ListaFuncionario = [];
        $scope.AutocompleteFuncionario = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_EMPLEADO,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaFuncionario = ValidarListadoAutocomplete(Response.Datos, $scope.ListaFuncionario);
                }
            }
            return $scope.ListaFuncionario;
        }

        //Autocomplete Vehículos
        $scope.ListaPlaca = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 2) == 0 || value.length == 1) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaPlaca = ValidarListadoAutocomplete(Response.Datos, $scope.ListaPlaca);
                }
            }
            return $scope.ListaPlaca;
        };

        //Autocomplete Tenedores
        $scope.ListaTenedores = [];
        $scope.AutocompleteTenedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_TENEDOR,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaTenedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListaTenedores);
                }
            }
            return $scope.ListaTenedores;
        };

        //Autocomplete Remitentes
        $scope.ListaRemitente = [];
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_REMITENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRemitente);
                }
            }
            return $scope.ListaRemitente;
        };

        //Autocomplete Destinatario
        $scope.ListaDestinatario = [];
        $scope.AutocompleteDestinatario = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_DESTINATARIO,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaDestinatario = ValidarListadoAutocomplete(Response.Datos, $scope.ListaDestinatario);
                }
            }
            return $scope.ListaDestinatario;
        };

        //Autocomplete Conductores
        $scope.ListaConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CONDUCTOR,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListaConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListaConductores);
                }
            }
            return $scope.ListaConductores;
        };

        //Autocomplete Ciudades
        $scope.ListaCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListaCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCiudades);
                }
            }
            return $scope.ListaCiudades;
        }

        //--Tipo Precinto
        $scope.ListadoTipoPrecinto = [];
        $scope.ListadoTipoPrecinto = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_PRECINTO },
            Sync: true
        }).Datos;
        $scope.ModeloTipoPrecinto = $scope.ListadoTipoPrecinto[0];
        //--Tipo Precinto

        //--Novedad Enturnamiento Despacho
        $scope.ListadoCausas = [];
        $scope.ListadoCausas = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_NOVEDAD_ENTURNAMIENTO_DESPACHO },
            Sync: true
        }).Datos;
        //--Novedad Enturnamiento Despacho

        //--Rango Peso Valor Kilo
        $scope.ListadoRangoPesoValorKilo = [];
        $scope.ListadoRangoPesoValorKilo = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_RANGO_PESOS_VALOR_KILO },
            Sync: true
        }).Datos;
        //--Rango Peso Valor Kilo

        //--Rango Peso Valor Fijo
        $scope.ListadoRangoPesoValorFijo = [];
        $scope.ListadoRangoPesoValorFijo = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_RANGO_PESOS_VALOR_FIJO },
            Sync: true
        }).Datos;
        //--Rango Peso Valor Fijo

        //-- Productos Transportados --//
        $scope.CargarProductos = function () {
            $scope.ListaProductoTransportado = [];
            var Response = ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1, Sync: true })
            $scope.ListaProductoTransportado = ValidarListadoAutocomplete(Response.Datos, $scope.ListaProductoTransportado);
            $scope.ListaProductoTransportado.push({ Nombre: '', Codigo: 0 });
        };
        $scope.CargarProductos();

        $scope.ListaFormaPago = [
            { Nombre: 'Otros', Codigo: 1 },
            { Nombre: 'Transferecia', Codigo: 2 }
        ];
        $scope.FormaPagoRecaudo = $scope.ListaFormaPago[0];

        //----------------------------------------------------------FUNCIONES DE CARGUE Y OBTENER ------------------------------------------------------------------------//
        console.clear();

        $scope.ObtenerCliente = function () {
            var tmpTercero = $scope.CargarTercero($scope.ModeloCliente.Codigo);
            $scope.ModalCiudadRemitente = $scope.CargarCiudad(tmpTercero.Ciudad.Codigo);
            $scope.ModalDireccionRemitente = tmpTercero.Direccion;
            $scope.ModalTelefonosRemitente = tmpTercero.Telefonos;
        };

        $scope.ListadoTarifasFiltradas = [];
        function ObtenerTarifarioVenta() {
            var Response = TarifarioVentasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.ModalTarifarioVenta.Codigo, TipoConsulta: 1, Sync: true });
            $scope.TarifarioVentas = Response.Datos;
            $scope.ListadoRutas = [];
            $scope.ListadoTarifasFiltradas = [];
            for (var i = 0; i < $scope.TarifarioVentas.Tarifas.length; i++) {
                var item = $scope.TarifarioVentas.Tarifas[i]
                if ($scope.TarifaCupoTonelada) { //Cupo Tonelada
                    if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.CodigoLineaNegocio
                        && item.TipoLineaNegocioTransportes.Codigo == $scope.ModeloTipoLineaNegocio.Codigo
                        && item.TipoTarifaTransportes.TarifaTransporte.Codigo == 301
                    ) {

                        $scope.ListadoTarifasFiltradas.push(item);

                        if ($scope.ListadoRutas.length == 0) {
                            $scope.ListadoRutas.push(item.Ruta);
                        } else {
                            Cont = 0
                            for (var j = 0; j < $scope.ListadoRutas.length; j++) {
                                if ($scope.ListadoRutas[j].Codigo == item.Ruta.Codigo) {
                                    Cont++;
                                    break;
                                }
                            }
                            if (Cont == 0) {
                                $scope.ListadoRutas.push(item.Ruta);
                            }
                        }
                    }
                }
                if ($scope.TarifaCantidadViajes) { //Cantidad Viajes
                    if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.CodigoLineaNegocio
                        && item.TipoLineaNegocioTransportes.Codigo == $scope.ModeloTipoLineaNegocio.Codigo
                        && item.TipoTarifaTransportes.TarifaTransporte.Codigo != 303
                    ) {
                        $scope.ListadoTarifasFiltradas.push(item);
                        if ($scope.ListadoRutas.length == 0) {
                            $scope.ListadoRutas.push(item.Ruta);
                        } else {
                            Cont = 0
                            for (var j = 0; j < $scope.ListadoRutas.length; j++) {
                                if ($scope.ListadoRutas[j].Codigo == item.Ruta.Codigo) {
                                    Cont++;
                                    break;
                                }
                            }
                            if (Cont == 0) {
                                $scope.ListadoRutas.push(item.Ruta);
                            }
                        }
                    }
                }
            }
        }

        $scope.ObtenerRuta = function () {
            if ($scope.ModalRuta.Codigo > 0) {
                $scope.PorcentajeAnticipoRuta = 0
                blockUI.delay = 1000;
                var Response = RutasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.ModalRuta.Codigo, Sync: true })
                $scope.PorcentajeAnticipoRuta = Response.Datos.PorcentajeAnticipo
                $scope.CiudadOrigenRuta = Response.Datos.CiudadOrigen
                $scope.CiudadDestinoRuta = Response.Datos.CiudadDestino
                $scope.TipoRuta = Response.Datos.TipoRuta
                if ($scope.ModalTotalFleteTransportador !== undefined) {
                    $scope.ModalAnticipoPlanilla = MascaraValores(Math.round(((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))))
                }
                if ($scope.ModalPlaca != undefined && $scope.ModalPlaca != "" && $scope.ModalPlaca != null) { // se valida si se ha asignado vehiculo o ruta
                    $scope.ConsultarTarifarioCompra($scope.ModalTenedor.Codigo);
                    $scope.AsignarTarifa();
                    $scope.GestionarFletesCompra();
                    $scope.GestionarFletesVenta();
                }
            }
        };

        $scope.ObtenerOrdenCargue = function (NumeroOrdenCargue) {

            var OrdenCargue = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: NumeroOrdenCargue,
                Sync: true
            }


            Response = OrdenCargueFactory.Obtener(OrdenCargue)

            if (Response.ProcesoExitoso === true) {

                //Datos redunante entre documentos (Podría cambiar estos datos si tiene remesa generada)
                $scope.CodigoVehiculo = 0;
                if (Response.Datos.Vehiculo !== undefined && Response.Datos.Vehiculo !== null) {
                    if (Response.Datos.Vehiculo.Codigo > 0) {
                        $scope.ModalPlaca = $scope.CargarVehiculos(Response.Datos.Vehiculo.Codigo)
                        $scope.AsignarDatosVehiculo($scope.ModalPlaca);
                    }
                }

                $scope.CodigoSemirremolque = 0;
                if (Response.Datos.Semirremolque !== undefined && Response.Datos.Semirremolque !== null) {
                    if (Response.Datos.Semirremolque.Codigo > 0) {
                        $scope.CodigoSemirremolque = Response.Datos.Semirremolque.Codigo;
                    }
                }


                if ($scope.ListaPlacaSemi !== undefined && $scope.ListaPlacaSemi !== null) {
                    if ($scope.ListaPlacaSemi.length > 0 && $scope.CodigoSemirremolque > 0) {
                        try {
                            var tempsemi = SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true, Codigo: $scope.CodigoSemirremolque }).Datos[0] //$linq.Enumerable().From($scope.ListaPlacaSemi).First('$.Codigo ==' + $scope.CodigoSemirremolque);
                            $scope.ModalSemirremolque = tempsemi
                        } catch (e) {

                        }
                    }
                }

                $scope.ModalNumeroOrdenCargue = Response.Datos.NumeroDocumento;
                $scope.NumeroOrdenCargue = Response.Datos.Numero;
                $scope.NumeroDocumentoOrdenCargue = Response.Datos.NumeroDocumento;

                $scope.ModalFechaOrdenCargue = Response.Datos.Fecha;
                $scope.ModalValorAnticipo = Response.Datos.ValorAnticipo;
                $scope.ModelCiudCarg = $scope.CargarCiudad(Response.Datos.CiudadCargue.Codigo);
                $scope.ModelDireCarg = Response.Datos.DireccionCargue;
                $scope.ModalTeleCarg = Response.Datos.TelefonosCargue;
                $scope.ModalContacCarg = Response.Datos.ContactoCargue;
                $scope.ModelCiudDesc = $scope.CargarCiudad(Response.Datos.CiudadDescargue.Codigo);
                $scope.ModalCantidad = Response.Datos.CantidadCliente
                $scope.ModelDireDesc = Response.Datos.DireccionDescargue;
                $scope.ModalTeleDesc = Response.Datos.TelefonoDescargue;
                $scope.ModalContacDesc = Response.Datos.ContactoDescargue;
                $scope.ModalObservacionesOrdenCargue = Response.Datos.Observaciones;
                $scope.DeshabilitarDespacho = true;
                $scope.DeshabilitarOrdenCargue = true;
            }
        }

        $scope.ObtenerRemesa = function (NumeroRemesa) {

            var Remesa = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: NumeroRemesa
            }


            RemesasFactory.Obtener(Remesa).then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    //Datos redundante entre documentos (Podría cambiar estos datos si tiene remesa generada)
                    $scope.ListaPrecintos = response.data.Datos.ListaPrecintos
                    $scope.CodigoVehiculo = 0;
                    if (response.data.Datos.Vehiculo !== undefined && response.data.Datos.Vehiculo !== null) {
                        if (response.data.Datos.Vehiculo.Codigo > 0) {
                            $scope.ModalPlaca = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo)
                            $scope.AsignarDatosVehiculo($scope.ModalPlaca);
                        }
                    }
                    $scope.CodigoSemirremolque = 0;
                    if (response.data.Datos.Semirremolque !== undefined && response.data.Datos.Semirremolque !== null) {
                        if (response.data.Datos.Semirremolque.Codigo > 0) {
                            $scope.CodigoSemirremolque = response.data.Datos.Semirremolque.Codigo;
                        }
                    }

                    if ($scope.ListaPlacaSemi !== undefined && $scope.ListaPlacaSemi !== null) {
                        if ($scope.ListaPlacaSemi.length > 0 && $scope.CodigoSemirremolque > 0) {
                            try {
                                var tempsemi = SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true, Codigo: $scope.CodigoSemirremolque }).Datos[0] // $linq.Enumerable().From($scope.ListaPlacaSemi).First('$.Codigo ==' + $scope.CodigoSemirremolque);
                                $scope.ModalSemirremolque = tempsemi
                            } catch (e) {

                            }
                        }
                    }

                    $scope.ModalNumeroRemesa = response.data.Datos.NumeroDocumento;
                    $scope.NumeroRemesa = response.data.Datos.Numero;
                    $scope.NumeroDocumentoRemesa = response.data.Datos.NumeroDocumento;
                    $scope.ModalFechaRemesa = response.data.Datos.Fecha;
                    $scope.ModalDocCliente = response.data.Datos.NumeroDocumentoCliente;
                    $scope.ModalTipoRemesa = response.data.Datos.TipoRemesa.Nombre;
                    $scope.ModalRemitente = $scope.CargarTercero(response.data.Datos.Remitente.Codigo)
                    $scope.ModalDireccionRemitente = response.data.Datos.DireccionRemitente;
                    $scope.ModalTelefonosRemitente = response.data.Datos.TelefonoRemitente;
                    $scope.ModalNumeroConfirmacionMinisterio = response.data.Datos.NumeroConfirmacionMinisterioRemesa;
                    try { $scope.ModalCiudadRemitente = $scope.CargarCiudad(response.data.Datos.Remitente.Ciudad.Codigo); } catch (e) { }
                    try { $scope.ModalCiudadDestinatario = $scope.CargarCiudad(response.data.Datos.Destinatario.Ciudad.Codigo); } catch (e) { }
                    $scope.ModalDestinatario = $scope.CargarTercero(response.data.Datos.Destinatario.Codigo)
                    $scope.ModalDireccionDestinatario = response.data.Datos.DireccionDestinatario;
                    $scope.ModalTelefonosDestinatario = response.data.Datos.TelefonoDestinatario;

                    $scope.ModalObservacionesRemesa = response.data.Datos.Observaciones;
                    $scope.DeshabilitarRemesa = true;
                    $scope.DeshabilitarDespacho = true;
                    $scope.DeshabilitarTarifaTransportador = true;
                    $scope.DesabilitarConfirmacionRemesa = true;

                    $scope.ModalNumeroContenedor = response.data.Datos.NumeroContenedor;
                    $scope.ModalImportacionCiudadDevolucion = $scope.CargarCiudad(response.data.Datos.CiudadDevolucion.Codigo)
                    $scope.ModalImportacionPatioDevolucion = response.data.Datos.PatioDevolucion;
                    $scope.ModalImportacionFechaDevolucion = new Date(response.data.Datos.FechaDevolucion);
                    $scope.ModalMBLContenedor = response.data.Datos.MBLContenedor;
                    $scope.ModalHBLContenedor = response.data.Datos.HBLContenedor;
                    $scope.ModalDOContenedor = response.data.Datos.DOContenedor;
                    $scope.ModalValorFOB = response.data.Datos.ValorFOB;
                }
            });
        }

        $scope.ObtenerPlanillaDespacho = function (NumeroPlanillaDespacho) {

            var PlanillaDespacho = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                Numero: NumeroPlanillaDespacho
            }


            PlanillaDespachosFactory.Obtener(PlanillaDespacho).then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    //Datos redundante entre documentos (Podría cambiar estos datos si tiene remesa generada)
                    //$scope.ModalPlaca = $linq.Enumerable().From($scope.ListaPlaca).First('$.Codigo ==' + response.data.Datos.Vehiculo.Codigo);
                    $scope.AsignarDatosVehiculo($scope.ModalPlaca);
                    //$scope.ModalSemirremolque = $linq.Enumerable().From($scope.ListaPlacaSemi).First('$.Codigo ==' + response.data.Datos.Semirremolque.Codigo);
                    $scope.Radio.Anticipo = response.data.Datos.AnticipoPagadoA

                    $scope.ModalNumeroPlanilla = response.data.Datos.NumeroDocumento;
                    $scope.ModalFechaSalida = response.data.Datos.FechaHoraSalida;
                    $scope.ModalHoraSalida = RetornarHoras(response.data.Datos.FechaHoraSalida);
                    try {
                        $scope.ModalManifiesto = response.data.Datos.Manifiesto.NumeroDocumento
                    } catch (e) {
                    }
                    $scope.ModalAnticipoPlanilla = MascaraValores(Math.round(response.data.Datos.ValorAnticipo))
                    $scope.ModalObservacionesPlanilla = response.data.Datos.Observaciones;

                    for (var i = 0; i < response.data.Datos.ListadoAuxiliar.length; i++) {
                        $scope.ListaAuxiliares.push({
                            Tercero: { Nombre: response.data.Datos.ListadoAuxiliar[i].Tercero.Nombre },
                            Horas: response.data.Datos.ListadoAuxiliar[i].Horas,
                            Valor: response.data.Datos.ListadoAuxiliar[i].Valor,
                            Observaciones: response.data.Datos.ListadoAuxiliar[i].Observaciones,
                        })
                    }
                    $scope.ModalSeguroAnticipo = MascaraValores(response.data.Datos.ValorSeguroPoliza)
                    if (response.data.Datos.PagoAnticipo > 0) {
                        $scope.FormaPagoRecaudo = $scope.ListaFormaPago[1]
                    } else {
                        $scope.FormaPagoRecaudo = $scope.ListaFormaPago[0]
                    }

                    $scope.DeshabilitarPlanilla = true;
                    $timeout(function () {
                        $scope.ModalAnticipoPlanilla = MascaraValores(Math.round(response.data.Datos.ValorAnticipo));
                        $scope.ModalTotalFleteTransportador = MascaraValores(response.data.Datos.ValorFleteTransportador);
                    }, 1000);
                }

            });
        }

        $scope.DesplegarDespacharViaje = function (DetalleDespacho) {

            $scope.CodigoDetalleSolicitud = DetalleDespacho.Codigo;
            $scope.ModalRuta = DetalleDespacho.Ruta;
            $scope.ModalCantidad = MascaraValores(DetalleDespacho.Cantidad);
            $scope.ModalPeso = MascaraValores(DetalleDespacho.PesoKilogramo);
            $scope.ModelCiudCarg = $scope.CargarCiudad(DetalleDespacho.CiudadCargue.Codigo);
            $scope.ModelCiudDesc = $scope.CargarCiudad(DetalleDespacho.CiudadDestinatario.Codigo);
            $scope.ModalCiudadDestinatario = $scope.CargarCiudad(DetalleDespacho.CiudadDestinatario.Codigo);
            $scope.ModelDireCarg = DetalleDespacho.DireccionCargue;
            $scope.ModalDireccionRemitente = DetalleDespacho.DireccionCargue;
            $scope.ModalTeleCarg = DetalleDespacho.TelefonoCargue;
            $scope.ModalContacCarg = DetalleDespacho.ContactoCargue;
            $scope.ModaloDocumentoCliente = DetalleDespacho.DocumentoCliente
            $scope.ModelDireDesc = DetalleDespacho.DireccionDestino;
            $scope.ModalDireccionDestinatario = DetalleDespacho.DireccionDestino;
            $scope.ModalTeleDesc = DetalleDespacho.TelefonoDestino;
            $scope.ModalTelefonosDestinatario = DetalleDespacho.TelefonoDestino;
            $scope.ModalContacDesc = DetalleDespacho.ContactoDestino;
            //$scope.ModalFleteCliente = MascaraValores(DetalleDespacho.FleteCliente); //--se comentarea por problemas de calculos
            //$scope.ModalFleteTransportador = MascaraValores(DetalleDespacho.FleteTransportador); //--se comentarea por problemas de calculos
            $scope.ModalValorCargueTransportador = MascaraValores(DetalleDespacho.ValorCargueTransportador);
            $scope.ModalValorDescargueTransportador = MascaraValores(DetalleDespacho.ValorDescargueTransportador);
            $scope.ModalTotalFleteCliente = Math.round(DetalleDespacho.ValorFleteCliente);
            $scope.ModalTotalFleteTransportador = MascaraValores(DetalleDespacho.ValorFleteTransportador);
            $scope.ModalTotalFleteTransportadorTEMP = MascaraValores(DetalleDespacho.ValorFleteTransportador);
            $scope.ModalFechaOrdenCargue = $scope.ModeloFecha;
            $scope.ModalFechaRemesa = $scope.ModeloFecha;
            $scope.ModalFechaSalida = $scope.ModeloFecha;
            $scope.ModalRemitente = $scope.CargarTercero($scope.ModeloCliente.Codigo);
            $scope.ModalNumeroContenedor = DetalleDespacho.NumeroContenedor;
            $scope.ModalImportacionCiudadDevolucion = $scope.CargarCiudad(DetalleDespacho.CiudadDevolucion.Codigo);
            $scope.ModalImportacionPatioDevolucion = DetalleDespacho.PatioDevolucion;
            $scope.ModalImportacionFechaDevolucion = new Date(DetalleDespacho.FechaDevolucion);
            $scope.ModalMBLContenedor = DetalleDespacho.MBLContenedor;
            $scope.ModalHBLContenedor = DetalleDespacho.HBLContenedor;
            $scope.ModalDOContenedor = DetalleDespacho.DOContenedor;
            $scope.ModalValorFOB = DetalleDespacho.ValorFOB;
            if (DetalleDespacho.CiudadCargue !== undefined) {
                $scope.ModalCiudadRemitente = $scope.CargarCiudad(DetalleDespacho.CiudadCargue.Codigo);
            } else if ($scope.ModeloCliente.Ciudad !== undefined) {
                $scope.ModalCiudadRemitente = $scope.CargarCiudad($scope.ModeloCliente.Ciudad.Codigo);
            }
            $scope.ModalDestinatario = $scope.CargarTercero($scope.ModeloCliente.Codigo)
            $scope.ModalDireccionDestinatario = DetalleDespacho.DireccionDestino;
            $scope.ModalTelefonosDestinatario = DetalleDespacho.TelefonoDestino;
            $scope.ModalTipoRemesa = DetalleDespacho.NombreTipoRemesa;
            $scope.ModalTarifaCompra = DetalleDespacho.TarifaTransporteCargaVenta;
            $scope.ModalTarifaVenta = DetalleDespacho.TarifaTransporteCargaVenta;
            //$scope.CodigoRuta = DetalleDespacho.Ruta.Codigo;
            $scope.CodigoTipoTarifaContrato = DetalleDespacho.TipoTarifaTransporteCargaVenta.Codigo;
            if (DetalleDespacho.ProductoTransportado.Codigo > 0) {
                $scope.ModalProducto = $scope.CargarProducto(DetalleDespacho.ProductoTransportado.Codigo);
            }
            $scope.ModalPermisoInvias = DetalleDespacho.PermisoInvias;

            $scope.CodigoTipoRemesa = DetalleDespacho.CodigoTipoRemesa;
            DetalleDespacho.FechaCargue = new Date(DetalleDespacho.FechaCargue)
            DetalleDespacho.FechaCargue.setHours(1)
            DetalleDespacho.FechaCargue.setMinutes(0)
            DetalleDespacho.FechaCargue.setMinutes(0)
            $scope.ValidaPeso = true
            if (DetalleDespacho.FechaCargue < $scope.fechaBloqueo2 && DetalleDespacho.FechaCargue >= $scope.fechaBloqueo1 && $scope.Sesion.UsuarioAutenticado.ManejoBloqueoDespachosFechasPosteriores) {
                $scope.NoVisualizarDocumento = true
            }
            if (DetalleDespacho.AplicaEscolta > 0) {
                $scope.ModalAplicaEscolta = true;
                $scope.ValorEscolta = DetalleDespacho.ValorEscolta;
            } else {
                $scope.ModalAplicaEscolta = false;
                $scope.ValorEscolta = 0;
            }

            if (DetalleDespacho.NumeroRemesa > 0) {
                $scope.ObtenerRemesa(DetalleDespacho.NumeroRemesa);
                $scope.ValidaPeso = false
            }
            if (DetalleDespacho.NumeroOrdenCargue > 0) {
                $scope.ObtenerOrdenCargue(DetalleDespacho.NumeroOrdenCargue);
                $scope.ValidaPeso = false
            }
            if (DetalleDespacho.NumeroPlanillaDespacho > 0) {
                $scope.ObtenerPlanillaDespacho(DetalleDespacho.NumeroPlanillaDespacho);
                $scope.ValidaPeso = false
            }
            try {
                $scope.ObtenerRuta()
            } catch (e) {

            }

            if (DetalleDespacho.NumeroOrdenCargue == 0 && DetalleDespacho.NumeroRemesa == 0 && DetalleDespacho.NumeroOrdenCargue == 0 && DetalleDespacho.NumeroPlanillaDespacho == 0 && $scope.Sesion.UsuarioAutenticado.ManejoEnturnamientoDespacho) {//Carga Vehículo enturnado
                $scope.ManejoVehiculoEnturne = true

                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    OficinaEnturna: $scope.ModeloOficinaDespacha,

                }
                blockUI.delay = 1000;
                //var Response1 = 
                $scope.ListaRegionesDestino = CiudadesFactory.ConsultarRegiones({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.CiudadDestinoRuta.Codigo, Sync: true }).Datos
                if ($scope.ListaRegionesDestino.length == 0) {
                    ShowError('La ciudad destino de la ruta no tiene regiones asociadas, asocie las regiones respectivas y vuelva a intentar')
                    $scope.DeshabilitarVehiculo = true
                } else {
                    EnturnamientoFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoEnturnes = response.data.Datos
                                    $scope.EnturneSeleccionado = undefined
                                    for (var i = 0; i < $scope.ListadoEnturnes.length; i++) {
                                        if ($scope.ListadoEnturnes[i].EstadoTurno.Codigo == 19002 && $scope.ListadoEnturnes[i].TipoVehiculo.Codigo == $scope.TipoVehiculoOrdenServicio.Codigo && ($scope.ListadoEnturnes[i].CiudadDestino1.Codigo == $scope.CiudadDestinoRuta.Codigo || $scope.ListadoEnturnes[i].CiudadDestino1.Codigo == $scope.CiudadDestinoRuta.Codigo)) {
                                            $scope.EnturneSeleccionado = $scope.ListadoEnturnes[i]
                                            break
                                        }
                                    }
                                    $scope.EnturneSeleccionadoTemp = undefined

                                    if ($scope.EnturneSeleccionado == undefined && $scope.TipoVehiculoOrdenServicio.Codigo > 0) {
                                        for (var i = 0; i < $scope.ListadoEnturnes.length; i++) {
                                            if ($scope.ListadoEnturnes[i].EstadoTurno.Codigo == 19002 && $scope.ListadoEnturnes[i].TipoVehiculo.Codigo == $scope.TipoVehiculoOrdenServicio.Codigo) {
                                                if ($scope.EnturneSeleccionadoTemp == undefined) {
                                                    $scope.EnturneSeleccionadoTemp = $scope.ListadoEnturnes[i]
                                                }
                                                for (var J = 0; J < $scope.ListaRegionesDestino.length; J++) {
                                                    if ($scope.ListadoEnturnes[i].RegionPaisDestino.Codigo == $scope.ListaRegionesDestino[J].Codigo || $scope.ListadoEnturnes[i].RegionPaisDestino2.Codigo == $scope.ListaRegionesDestino[J].Codigo) {
                                                        $scope.EnturneSeleccionado = $scope.ListadoEnturnes[i]
                                                        break
                                                    }
                                                }
                                            }
                                        }
                                        if ($scope.EnturneSeleccionado == undefined && $scope.EnturneSeleccionadoTemp != undefined) {
                                            $scope.EnturneSeleccionado = $scope.EnturneSeleccionadoTemp
                                        }
                                        if ($scope.EnturneSeleccionado == undefined) {
                                            ShowError('No se encontraron vehículos enturnados con el tipo de vehículo ingresado para la orden de servicio')
                                        } else {
                                            try {
                                                $scope.ModalPlaca = $scope.CargarVehiculos($scope.EnturneSeleccionado.Vehiculo.Codigo)

                                                if (!$scope.ValidarVehiculoDocumentos($scope.ModalPlaca)) {
                                                    $scope.CausaCancelacion = 'Documentos Proximos a vencerse'
                                                    ShowWarningConfirm("El vehículo enturnado :" + $scope.ModalPlaca.Placa + "  tiene documentos proximos a vencerse", $scope.RechazarVehiculo)
                                                } else {
                                                    $scope.AsignarDatosVehiculo($scope.ModalPlaca);
                                                }
                                                if (!$scope.CambioVehiculoEnturnado) {
                                                    $scope.DeshabilitarVehiculo = true
                                                } else {
                                                    $scope.DeshabilitarVehiculo = false
                                                }
                                            } catch (e) {
                                            }
                                        }
                                    } else {
                                        if ($scope.TipoVehiculoOrdenServicio.Codigo == 0) {
                                            $scope.ManejoVehiculoEnturne = false
                                        }
                                        try {
                                            $scope.ModalPlaca = $scope.CargarVehiculos($scope.EnturneSeleccionado.Vehiculo.Codigo)
                                            if (!$scope.ValidarVehiculoDocumentos($scope.ModalPlaca)) {
                                                $scope.CausaCancelacion = 'Documentos Proximos a vencerse'
                                                ShowWarningConfirm("El vehículo enturnado :" + $scope.ModalPlaca.Placa + "  tiene documentos proximos a vencerse", $scope.RechazarVehiculo)
                                            } else {
                                                $scope.AsignarDatosVehiculo($scope.ModalPlaca);
                                            }

                                            if (!$scope.CambioVehiculoEnturnado) {
                                                $scope.DeshabilitarVehiculo = true
                                            } else {
                                                $scope.DeshabilitarVehiculo = false
                                            }
                                        } catch (e) {
                                        }
                                    }

                                }
                                else {
                                    ShowError('No se encontraron vehículos enturnados para la oficina de despacho')
                                    if (!$scope.CambioVehiculoEnturnado) {
                                        $scope.DeshabilitarVehiculo = true
                                    } else {
                                        $scope.DeshabilitarVehiculo = false
                                    }
                                }

                            }
                            $scope.GestionarFletesCompra();
                            $scope.GestionarFletesVenta()
                        }, function (response) {
                            $scope.GestionarFletesCompra();
                            $scope.GestionarFletesVenta()
                            ShowError(response.statusText);
                        });

                }
            }

            if ($scope.Sesion.Empresa.GeneraManifiesto === 1) {
                if (DetalleDespacho.CodigoTipoRemesa == CODIGO_TIPO_REMESA_NACIONAL) {
                    $scope.MostrarCampoManifiesto = true;
                }
            }
        }

        $scope.RechazarVehiculo = function () {
            if ($scope.CausaCancelacion == undefined || $scope.CausaCancelacion == '') {
                ShowError('Debe ingresar el motivo de rechazo del turno')
            } else {
                $scope.ModalConductor = '';
                $scope.ModalTenedor = '';
                $scope.ModalSemirremolque = '';
                $scope.ModalPlaca = '';
                $scope.ItemAnula = $scope.EnturneSeleccionado
                $scope.ItemAnula.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                $scope.ItemAnula.Novedad = $scope.CausaCancelacion
                $scope.ItemAnula.CausaAnulacion = $scope.CausaAnulacion
                $scope.ItemAnula.UsuarioAnula = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                $scope.ItemAnula.Autorizacion = 1
                EnturnamientoFactory.Anular($scope.ItemAnula).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('El turno se rechazó correctamente')
                            closeModal('ModalConfirmacionRechazarTurno')
                            closeModal('ModalRechazarTurno')
                            $scope.EnturneSeleccionadoalterno = angular.copy($scope.EnturneSeleccionado)
                            $scope.EnturneSeleccionado = undefined
                            for (var i = 0; i < $scope.ListadoEnturnes.length; i++) {
                                if ($scope.ListadoEnturnes[i].EstadoTurno.Codigo == 19002 && $scope.ListadoEnturnes[i].ID !== $scope.EnturneSeleccionadoalterno.ID && $scope.ListadoEnturnes[i].TipoVehiculo.Codigo == $scope.TipoVehiculoOrdenServicio.Codigo && ($scope.ListadoEnturnes[i].CiudadDestino1.Codigo == $scope.CiudadDestinoRuta.Codigo || $scope.ListadoEnturnes[i].CiudadDestino1.Codigo == $scope.CiudadDestinoRuta.Codigo)) {
                                    $scope.EnturneSeleccionado = $scope.ListadoEnturnes[i]
                                    break
                                }
                            }
                            $scope.EnturneSeleccionadoTemp = undefined
                            if ($scope.EnturneSeleccionado == undefined && $scope.TipoVehiculoOrdenServicio.Codigo > 0) {
                                for (var i = 0; i < $scope.ListadoEnturnes.length; i++) {
                                    if ($scope.ListadoEnturnes[i].EstadoTurno.Codigo == 19002 && $scope.ListadoEnturnes[i].ID !== $scope.EnturneSeleccionadoalterno.ID && $scope.ListadoEnturnes[i].TipoVehiculo.Codigo == $scope.TipoVehiculoOrdenServicio.Codigo) {
                                        if ($scope.EnturneSeleccionadoTemp == undefined) {
                                            $scope.EnturneSeleccionadoTemp = $scope.ListadoEnturnes[i]
                                        }
                                        for (var J = 0; J < $scope.ListaRegionesDestino.length; J++) {
                                            if ($scope.ListadoEnturnes[i].RegionPaisDestino.Codigo == $scope.ListaRegionesDestino[J].Codigo || $scope.ListadoEnturnes[i].RegionPaisDestino2.Codigo == $scope.ListaRegionesDestino[J].Codigo) {
                                                $scope.EnturneSeleccionado = $scope.ListadoEnturnes[i]
                                                break
                                            }
                                        }
                                    }
                                }
                                if ($scope.EnturneSeleccionado == undefined && $scope.EnturneSeleccionadoTemp != undefined) {
                                    $scope.EnturneSeleccionado = $scope.EnturneSeleccionadoTemp
                                }
                                if ($scope.EnturneSeleccionado == undefined) {
                                    ShowError('No se encontraron vehículos enturnados con el tipo de vehículo ingresado para la orden de servicio')
                                } else {
                                    try {
                                        $scope.ModalPlaca = $scope.CargarVehiculos($scope.EnturneSeleccionado.Vehiculo.Codigo)
                                        $scope.AsignarDatosVehiculo($scope.ModalPlaca);

                                        if (!$scope.CambioVehiculoEnturnado) {
                                            $scope.DeshabilitarVehiculo = true
                                        } else {
                                            $scope.DeshabilitarVehiculo = false
                                        }
                                    } catch (e) {
                                    }
                                }
                            } else {
                                try {
                                    if ($scope.TipoVehiculoOrdenServicio.Codigo == 0) {
                                        $scope.ManejoVehiculoEnturne = false
                                    }
                                    $scope.ModalPlaca = $scope.CargarVehiculos($scope.EnturneSeleccionado.Vehiculo.Codigo)
                                    $scope.AsignarDatosVehiculo($scope.ModalPlaca);

                                    if (!$scope.CambioVehiculoEnturnado) {
                                        $scope.DeshabilitarVehiculo = true
                                    } else {
                                        $scope.DeshabilitarVehiculo = false
                                    }
                                } catch (e) {
                                }
                            }

                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        function ObtenerOrdenServicio() {

            blockUI.start('Cargando Orden de Servicio');

            $timeout(function () {
                blockUI.message('Cargando Orden de Servicio');
            }, 200);

            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
                IDRegistroDetalle: $scope.IDCodigo,
                DesdeDespachoOrdenServicio: true,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO
            };

            blockUI.delay = 1000;
            EncabezadoSolicitudOrdenServiciosFactory.Obtener(filtros).
                then(function (response) {
                    OrdenServicio = response.data.Datos;
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ModeloNumero = response.data.Datos.NumeroDocumento;
                        $scope.ModeloOficinaDespacha = $linq.Enumerable().From($scope.ListaOficinasDespacha).First('$.Codigo ==' + response.data.Datos.OficinaDespacha.Codigo);
                        $scope.ModaloDocumentoCliente = response.data.Datos.DocumentoCliente
                        $scope.CodigoEstadoSolicitud = response.data.Datos.CodigoEstadoSolicitud;
                        $scope.ModeloCliente = response.data.Datos.Cliente;
                        $scope.ObtenerCliente();
                        $scope.ModeloLineaNegocioCarga = response.data.Datos.LineaNegocioCarga;
                        $scope.ModalDocCliente = response.data.Datos.DocumentoCliente;
                        $scope.ListaOrdenesServicios = [];
                        if ($scope.IDCodigo > 0) {
                            $scope.ListaOrdenesServicios[0] = $linq.Enumerable().From(response.data.Datos.DetalleDespacho).First('$.Codigo ==' + parseInt($scope.IDCodigo));
                            $scope.ModalTipoTarifaVenta = $scope.ListaOrdenesServicios[0].TipoTarifaTransporteCargaVenta.Nombre;
                            $scope.ModalTarifaVenta = $scope.ListaOrdenesServicios[0].TarifaTransporteCargaVenta;
                        }
                        $scope.ModalTarifarioVenta = response.data.Datos.TarifarioVentas;
                        //Para filtrar tarifas en el detalle
                        $scope.CodigoLineaNegocio = response.data.Datos.LineaNegocioCarga.Codigo;
                        $scope.ModeloLineaNegocio = response.data.Datos.LineaNegocioCarga;
                        $scope.ModeloTipoLineaNegocio = response.data.Datos.TipoLineaNegocioCarga;
                        $scope.TipoVehiculoOrdenServicio = response.data.Datos.TipoVehiculo;
                        $scope.TarifaCupoTonelada = false;
                        $scope.TarifaCantidadViajes = false;
                        $scope.TarifaViajes = false;
                        if (response.data.Datos.TipoDespacho.Codigo == 18202) {
                            $scope.Saldo = response.data.Datos.SaldoTipoDespacho;
                            $scope.TarifaCupoTonelada = true;
                            if ($scope.ManejoCVCPDeseDespachar) {
                                $scope.ModalTarifaCompra = { Codigo: 301 }
                                $scope.ModalTarifaVenta = { Codigo: 301 }
                                ObtenerTarifarioVenta();
                            }
                        } else if (response.data.Datos.TipoDespacho.Codigo == 18203) {
                            $scope.TarifaCantidadViajes = true;
                            if ($scope.ManejoCVCPDeseDespachar) {
                                ObtenerTarifarioVenta();
                            }
                        } else {
                            $scope.TarifaViajes = true;
                        }
                        if ($scope.IDCodigo > 0) {
                            $scope.DesplegarDespacharViaje($scope.ListaOrdenesServicios[0]);
                        }
                        if ($scope.CodigoLineaNegocio == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO && !(response.data.Datos.TipoDespacho.Codigo == 18202 || response.data.Datos.TipoDespacho.Codigo == 18202)) {
                            $scope.DeshabilitarTarifaTransportador = true;
                        }
                    }
                    else {
                        ShowError('No se logro consultar el despacho para la Orden de Servicio No. ' + $scope.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarDespacharOrdenServicios';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarDespacharOrdenServicios';
                });
            blockUI.stop();
        };

        function ObtenerOrdenServicioSync() {

            blockUI.start('Cargando Orden de Servicio');

            $timeout(function () {
                blockUI.message('Cargando Orden de Servicio');
            }, 200);

            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,
                IDRegistroDetalle: $scope.IDCodigo,
                DesdeDespachoOrdenServicio: true,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
                Sync: true

            };

            blockUI.delay = 1000;
            Response = EncabezadoSolicitudOrdenServiciosFactory.Obtener(filtros)
            if (Response.ProcesoExitoso === true) {
                $scope.ModeloNumero = Response.Datos.NumeroDocumento;
                $scope.ModeloOficinaDespacha = $linq.Enumerable().From($scope.ListaOficinasDespacha).First('$.Codigo ==' + Response.Datos.OficinaDespacha.Codigo);
                $scope.ModaloDocumentoCliente = Response.Datos.DocumentoCliente
                $scope.CodigoEstadoSolicitud = Response.Datos.CodigoEstadoSolicitud;
                $scope.ModeloCliente = Response.Datos.Cliente;
                $scope.ObtenerCliente()
                $scope.ModeloLineaNegocioCarga = Response.Datos.LineaNegocioCarga;
                $scope.ModalDocCliente = Response.Datos.DocumentoCliente;
                $scope.ListaOrdenesServicios = [];
                if ($scope.IDCodigo > 0) {
                    $scope.ListaOrdenesServicios[0] = $linq.Enumerable().From(Response.Datos.DetalleDespacho).First('$.Codigo ==' + parseInt($scope.IDCodigo));
                    $scope.ModalTipoTarifaVenta = $scope.ListaOrdenesServicios[0].TipoTarifaTransporteCargaVenta.Nombre;
                    $scope.ModalTarifaVenta = $scope.ListaOrdenesServicios[0].TarifaTransporteCargaVenta
                }
                $scope.ModalTarifarioVenta = Response.Datos.TarifarioVentas;
                //Para filtrar tarifas en el detalle
                $scope.CodigoLineaNegocio = Response.Datos.LineaNegocioCarga.Codigo;
                $scope.ModeloLineaNegocio = Response.Datos.LineaNegocioCarga;
                $scope.ModeloTipoLineaNegocio = Response.Datos.TipoLineaNegocioCarga;
                $scope.TipoVehiculoOrdenServicio = Response.Datos.TipoVehiculo;
                $scope.TarifaCupoTonelada = false
                $scope.TarifaCantidadViajes = false
                $scope.TarifaViajes = false
                if (Response.Datos.TipoDespacho.Codigo == 18202) {
                    $scope.Saldo = Response.Datos.SaldoTipoDespacho;
                    $scope.TarifaCupoTonelada = true
                    if ($scope.ManejoCVCPDeseDespachar) {
                        ObtenerTarifarioVenta()
                    }
                } else if (Response.Datos.TipoDespacho.Codigo == 18203) {
                    $scope.TarifaCantidadViajes = true
                    if ($scope.ManejoCVCPDeseDespachar) {
                        ObtenerTarifarioVenta()
                    }
                } else {
                    $scope.TarifaViajes = true
                }
                if ($scope.IDCodigo > 0) {
                    $scope.DesplegarDespacharViaje($scope.ListaOrdenesServicios[0]);
                }
                if ($scope.CodigoLineaNegocio == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                    $scope.DeshabilitarTarifaTransportador = true;
                }
            }
            else {
                ShowError('No se logro consultar el despacho para la Orden de Servicio No. ' + $scope.Numero + '. ' + Response.MensajeOperacion);
                document.location.href = '#!ConsultarDespacharOrdenServicios';
            }
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


        //----------------------------------------------------------------------FUNCIONES-------------------------------------------------------------------------------------------------------//

        $scope.AgregarAuxiliar = function () {
            if ($scope.ModeloFuncionario !== '' && $scope.ModeloFuncionario !== undefined && $scope.ModeloFuncionario !== null && $scope.ModeloFuncionario.Codigo !== 0 && $scope.ModeloFuncionario.Codigo !== undefined) {
                var concidencias = 0
                if ($scope.ListaAuxiliares.length > 0) {
                    for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                        if ($scope.ModeloFuncionario.Codigo === $scope.ListaAuxiliares[i].Tercero.Codigo) {
                            concidencias++
                            break;
                        }
                    }
                    if (concidencias > 0) {
                        ShowError('El funcionario ya fue ingresado')
                        $scope.ModeloFuncionario = '';
                    } else {
                        $scope.ListaAuxiliares.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto }, Modificarfuncionario: true });
                        $scope.ModeloFuncionario = '';
                    }
                } else {
                    $scope.ListaAuxiliares.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto }, Modificarfuncionario: true });
                    $scope.ModeloFuncionario = '';
                }
            } else {
                ShowError('Debe ingresar un funcionario valido');
            }
            $scope.ValidarDatosFuncionario();
        }

        $scope.ValidarDatosFuncionario = function () {
            for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                if ($scope.ListaAuxiliares[i].Horas === '' || $scope.ListaAuxiliares[i].Horas === undefined || $scope.ListaAuxiliares[i].Horas === null || $scope.ListaAuxiliares[i].Horas === 0) {
                    ShowError('Debe ingresar los detalles de horas trabajadas');
                } else {
                    $scope.ListaAuxiliares[i].Modificarfuncionario = false;
                }
            }
        }

        $scope.EliminarFuncionario = function (codigo) {
            if ($scope.ListaAuxiliares.length > 0) {
                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                    if (codigo === $scope.ListaAuxiliares[i].Codigo) {
                        $scope.ListaAuxiliares.splice(i, 1);
                    }
                }
            }
        };

        $scope.AsignarTarifa = function () {

            $scope.ListadoTarifasTransporte = []

            if ($scope.TarifaCantidadViajes && !($scope.IDCodigo > 0)) {

                for (var i = 0; i < $scope.ListadoTarifasFiltradas.length; i++) {

                    if ($scope.ModalRuta.Codigo == $scope.ListadoTarifasFiltradas[i].Ruta.Codigo) {
                        var conttarifaenventas = 0;
                        if ($scope.ListadoTarifasCompras.length > 0) {

                            for (var j = 0; j < $scope.ListadoTarifasCompras.length; j++) {
                                if ($scope.ListadoTarifasCompras[j].Codigo == $scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                    conttarifaenventas++;
                                }
                            }
                        }
                        if (conttarifaenventas > 0) {
                            if ($scope.ListadoTarifasTransporte.length == 0) {
                                $scope.ListadoTarifasTransporte.push($scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.TarifaTransporte)
                            } else {
                                var cont = 0;
                                for (var j = 0; j < $scope.ListadoTarifasTransporte.length; j++) {
                                    if ($scope.ListadoTarifasTransporte[j].Codigo == $scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                        cont++;
                                        break;
                                    }
                                }
                                if (cont == 0) {
                                    $scope.ListadoTarifasTransporte.push($scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.TarifaTransporte);
                                }
                            }
                        }
                    }
                }
                if ($scope.ListadoTarifasTransporte.length === 0) {
                    ShowError('No se encontraron tarifas que coincidan en los tarifarios compra y venta para la ruta ingresada');
                } else {
                    //--Asigna las Tarifas de compra y de ventas por las filtradas al ListadoTarifasTransporte
                    $scope.ModalTarifaCompra = $scope.ListadoTarifasTransporte[0];
                    $scope.ModalTarifaVenta = $scope.ListadoTarifasTransporte[0];                    
                    $scope.CambiarSeleccionTarifaCompra($scope.ModalTarifaCompra);//--Una vez asignado las tarifas predetarminadas, se hace ejecuta el cambio de tarifa, para filtrar el tipo de tarifas relacionada a la tarifa
                    try {
                        for (var i = 0; i < $scope.ListadoTarifasFiltradas.length; i++) {
                            if ($scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.TarifaTransporte.Codigo == $scope.ModalTarifaVenta.Codigo
                                && $scope.ListadoTarifasFiltradas[i].Ruta.Codigo == $scope.ModalRuta.Codigo
                                && $scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.Codigo == $scope.ModalTipoTarifaCompra.Codigo

                            ) {
                                $scope.ModalTipoTarifaVenta = $scope.ModalTipoTarifaCompra.Nombre
                                $scope.ModalFleteCliente = $scope.ListadoTarifasFiltradas[i].ValorFlete
                                break;
                            }
                        }

                    } catch (e) {
                    }
                }
                
            } else {
                for (var i = 0; i < $scope.ListadoTarifasFiltradas.length; i++) {
                    if ($scope.ModalRuta.Codigo == $scope.ListadoTarifasFiltradas[i].Ruta.Codigo
                        && $scope.ModalTarifaCompra.Codigo == $scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.TarifaTransporte.Codigo) {
                        $scope.ListadoTarifasTransporte.push($scope.ListadoTarifasFiltradas[i]);
                        $scope.ModalTarifaCompra = $scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.TarifaTransporte;
                        $scope.ModalTarifaVenta = $scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.TarifaTransporte;
                        $scope.CambiarSeleccionTarifaCompra($scope.ModalTarifaCompra);//--Una vez asignado las tarifas predetarminadas, se hace ejecuta el cambio de tarifa, para filtrar el tipo de tarifas relacionada a la tarifa
                        if (!$scope.TarifaCantidadViajes) {
                            $scope.ModalTipoTarifaVenta = '(No Aplica)';
                            $scope.ModalFleteCliente = $scope.ListadoTarifasFiltradas[i].ValorFlete;
                        }
                    }
                }
            }
        }

        //Calculo Tarifas Transportador
        $scope.GestionarFletesCompra = function () {
            var ModalCantidad = 0;
            var ModalFleteTransportador = 0;
            var ModalPeso = 0;

            if ($scope.TarifaCupoTonelada) {
                var tmpModalPeso = $scope.ModalPeso == undefined ? 0 : parseInt(MascaraNumero($scope.ModalPeso));
                if ($scope.Saldo < tmpModalPeso && $scope.ValidaPeso && !$scope.Sesion.UsuarioAutenticado.ManejoDespachoProgramacion) {
                    ShowError('El peso no puede ser superior a ' + MascaraValores($scope.Saldo).toString() + ' Kg');
                    $scope.ModalPeso = 0;
                } else {

                    ModalCantidad = $scope.ModalCantidad == undefined ? 0 : MascaraNumero(MascaraValores($scope.ModalCantidad));
                    ModalFleteTransportador = $scope.ModalFleteTransportador == undefined ? 0 : MascaraNumero(MascaraValores($scope.ModalFleteTransportador));
                    ModalPeso = $scope.ModalPeso == undefined ? 0 : MascaraNumero(MascaraValores($scope.ModalPeso));

                    $scope.ModalTotalFleteTransportador = MascaraValores(Math.round((parseInt(ModalFleteTransportador) / 1000) * parseInt(ModalPeso)));
                }
            }
            else if ($scope.ModalTarifaCompra !== undefined && $scope.ModalTarifaCompra !== null) {

                ModalCantidad = $scope.ModalCantidad == undefined ? 0 : MascaraNumero(MascaraValores($scope.ModalCantidad));
                ModalFleteTransportador = $scope.ModalFleteTransportador == undefined ? 0 : MascaraNumero(MascaraValores($scope.ModalFleteTransportador));
                ModalPeso = $scope.ModalPeso == undefined ? 0 : MascaraNumero(MascaraValores($scope.ModalPeso));

                $scope.ModalTotalFleteTransportador = 0;

                /*
                    VF: Valor Total Flete
                    FT: Flete Transportador
                    C: Cantidad
                    PE: Peso Kg
                    RI: Rango Inferior
                    RS: Rango Superior
                */

                //Contenedor (VF = FT * C)
                if ($scope.ModalTarifaCompra.Codigo == TARIFA_CONTENEDOR) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalCantidad) * parseInt(ModalFleteTransportador);
                }

                //Devolucion Contenedor (VF = FT * C)
                if ($scope.ModalTarifaCompra.Codigo == TARIFRA_DEVOLUCION_CONTENEDOR) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalCantidad) * parseInt(ModalFleteTransportador);
                }

                //Cupo Vehiculo (VF = FT * C)
                if ($scope.ModalTarifaCompra.Codigo == TARIFA_CUPO_VEHICULO) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalFleteTransportador);
                }

                //Tarifa Dia / Hora (VF = FT * C)
                if ($scope.ModalTarifaCompra.Codigo == TARIFA_DIA || $scope.ModalTarifaCompra.Codigo == TARIFA_HORA) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalCantidad) * parseInt(ModalFleteTransportador);
                }
                //Movilización
                if ($scope.ModalTarifaCompra.Codigo == TARIFA_MOVILIZACION) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalFleteTransportador);
                }
                //Peso flete
                if ($scope.ModalTarifaVenta.Codigo == 301) {
                    $scope.ModalTotalFleteTransportador = (parseInt(ModalFleteTransportador) / 1000) * parseInt(ModalPeso);
                }
                if ($scope.ModalTarifaCompra.Codigo == TARIFA_PESO_FLETE) {
                    $scope.ModalTotalFleteTransportador = (parseInt(ModalFleteTransportador) / 1000) * parseInt(ModalPeso);
                }
                if ($scope.ModalTarifaVenta.Codigo == 303) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalFleteTransportador)
                }
                //Producto
                if ($scope.ModalTarifaCompra.Codigo == 302) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalCantidad) * parseInt(ModalFleteTransportador);
                }
                //Peso Barril galon
                if ($scope.ModalTarifaCompra.Codigo == 6 || $scope.ModalTarifaCompra.Codigo == 300) {
                    $scope.ModalTotalFleteTransportador = parseInt(ModalFleteTransportador) * parseFloat(ModalCantidad);
                    inserto = true
                }
                if ($scope.ModalTarifaCompra.Codigo == 301) {
                    $scope.ModalTotalFleteTransportador = (parseInt(ModalFleteTransportador) / 1000) * parseFloat(ModalPeso);
                    inserto = true
                }

                $scope.ModalTotalFleteTransportadorTEMP = $scope.ModalTotalFleteTransportador
                $scope.ModalTotalFleteTransportador = MascaraValores($scope.ModalTotalFleteTransportador);

                $scope.ModalAnticipoPlanilla = MascaraValores(Math.round((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))))
            }
        }

        //Tarifario Compra
        $scope.ConsultarTarifarioCompra = function (CodigoTenedor) {
            // JCB: 30-JUL-2020
            // Se adicionar manejo de variable de sesion ManejoTarifaCompraTarifarioVenta
            if ($scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta) {
                // JCB: 30-JUL-2020
                // Se cargan las Tarifas y Tipo Tarifas Compras ($scope.ListadoTarifasCompras, $scope.ListaTipoTarifasCompra) con base en el ListadoTarifasFiltradas que se cargo en el ObtenerTarifarioVenta
                if ($scope.ModalRuta !== undefined && $scope.ModalRuta !== null && $scope.ModalRuta !== '' && $scope.ModalRuta.Codigo > 0) {
                    $scope.ListaTipoTarifasCompra = [];
                    $scope.ListadoTarifasCompras = [];
                    for (var i = 0; i < $scope.ListadoTarifasFiltradas.length; i++) {
                        var item = $scope.ListadoTarifasFiltradas[i];
                        if ($scope.ModalRuta.Codigo === $scope.ListadoTarifasFiltradas[i].Ruta.Codigo) {
                            var TarifaAux = {
                                Codigo: item.TipoTarifaTransportes.Codigo,
                                Nombre: item.TipoTarifaTransportes.Nombre,
                                CodgioTarifa: item.TipoTarifaTransportes.TarifaTransporte.Codigo,
                                ValorFlete: item.ValorFleteTransportador
                            };
                            var Existe = false;
                            for (var j = 0; j < $scope.ListadoTarifasCompras.length; j++) {
                                if ($scope.ListadoTarifasCompras[j].Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                    Existe = true;
                                    break;
                                }
                            }
                            if (Existe == false) {
                                $scope.ListadoTarifasCompras.push(item.TipoTarifaTransportes.TarifaTransporte);
                            }
                            $scope.ListaTipoTarifasCompra.push(TarifaAux);
                        }
                    }

                    $scope.ModalTarifarioCompra = {};
                    $scope.ModalTarifarioCompra = $scope.ModalTarifarioVenta;
                                        
                    //$scope.AsignarTarifa();

                    //try {
                    //    $scope.ModalTipoTarifaCompra = $linq.Enumerable().From($scope.ListaTipoTarifasCompra).First('$.Nombre =="' + $scope.ModalTipoTarifaVenta + '"');
                    //    $scope.ModalFleteTransportador = MascaraValores($scope.ModalTipoTarifaCompra.ValorFlete);
                    //} catch (e) {
                    //    $scope.ModalTipoTarifaCompra = $scope.ListaTipoTarifasCompra[0];
                    //    $scope.ModalFleteTransportador = MascaraValores(MascaraNumero($scope.ListaTipoTarifasCompra[0].ValorFlete));
                    //}
                }

            }
            else {
                // JCB: Manejo con Tarifario Compra asociado al Tenedor del vehículo
                if (CodigoTenedor !== undefined && CodigoTenedor !== null && CodigoTenedor > 0) {
                    $scope.CodigoTenedor = CodigoTenedor
                    if ($scope.ModalRuta !== undefined) {
                        if ($scope.ModalRuta.Codigo > 0) {
                            var FiltroTarifa = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Codigo: CodigoTenedor,
                                TarifarioProveedores: true,
                                CodigoLineaNegocio: $scope.CodigoLineaNegocio,
                                CodigoTipoLineaNegocio: $scope.ModeloTipoLineaNegocio.Codigo,
                                TarifaCarga: $scope.ModalTarifaVenta,
                                CodigoRuta: $scope.ModalRuta.Codigo,
                                Sync: true
                            }
                            $scope.ModalTarifarioCompra = {};

                            var response = TercerosFactory.Consultar(FiltroTarifa)

                            if (response.ProcesoExitoso === true) {
                                if (response.Datos.length > 0) {
                                    $scope.ModalTarifarioCompra = response.Datos;
                                    if (!($scope.TarifaCupoTonelada)) {
                                        $scope.ModalFleteTransportador = MascaraValores(MascaraNumero(response.Datos[0].ValorFlete))
                                    }
                                    $scope.ListaTipoTarifasCompra = [];
                                    $scope.ListadoTarifasCompras = []
                                    response.Datos.forEach(function (item) {
                                        if ($scope.TarifaCantidadViajes && $scope.ListadoTarifasFiltradas.length > 0) {
                                            var insertar = 0
                                            for (var i = 0; i < $scope.ListadoTarifasFiltradas.length; i++) {
                                                if (item.TarifaCarga.Codigo == $scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.TarifaTransporte.Codigo && item.TipoTarifaCarga.Codigo == $scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.Codigo) {
                                                    insertar++
                                                }
                                            }
                                            if (insertar > 0) {
                                                var TarifaAux = {
                                                    Codigo: item.TipoTarifaCarga.Codigo,
                                                    Nombre: item.TipoTarifaCarga.Nombre,
                                                    CodgioTarifa: item.TarifaCarga.Codigo,
                                                    ValorFlete: item.ValorFlete
                                                }
                                                $scope.ListadoTarifasCompras.push(item.TarifaCarga);
                                                $scope.ListaTipoTarifasCompra.push(TarifaAux);
                                            }
                                        } else {
                                            var TarifaAux = {
                                                Codigo: item.TipoTarifaCarga.Codigo,
                                                Nombre: item.TipoTarifaCarga.Nombre,
                                                CodgioTarifa: item.TarifaCarga.Codigo,
                                                ValorFlete: item.ValorFlete
                                            }
                                            $scope.ListaTipoTarifasCompra.push(TarifaAux);
                                        }
                                        $scope.ModalFleteTransportador = MascaraValores(item.ValorFlete);

                                    });

                                    //$scope.AsignarTarifa();
                                    
                                    try {
                                        $scope.ModalTipoTarifaCompra = $linq.Enumerable().From($scope.ListaTipoTarifasCompra).First('$.Nombre =="' + $scope.ModalTipoTarifaVenta + '"');
                                        $scope.ModalFleteTransportador = MascaraValores($scope.ModalTipoTarifaCompra.ValorFlete);
                                    } catch (e) {
                                        $scope.ModalTipoTarifaCompra = $scope.ListaTipoTarifasCompra[0];
                                        $scope.ModalFleteTransportador = MascaraValores(MascaraNumero($scope.ListaTipoTarifasCompra[0].ValorFlete));
                                    }
                                } else {

                                    ShowError("No existe un tarifario de compra para el vehículo y tenedor ingresados o no se encontraron tarifas disponibles")
                                    $scope.ModalConductor = ''
                                    $scope.ModalTenedor = ''
                                    $scope.ModalSemirremolque = ''
                                    $scope.ModalPlaca = ''

                                }
                            } else {
                                ShowError("No existe un tarifario de compra para el vehículo y tenedor ingresados")
                                $scope.ModalConductor = ''
                                $scope.ModalTenedor = ''
                                $scope.ModalSemirremolque = ''
                                $scope.ModalPlaca = ''
                            }

                        }
                    }

                } else {
                    if (!$scope.DeshabilitarDespacho) {
                        ShowError("No hay un tenedor ingresado para consultar el tarifario de compra")
                        $scope.ModalConductor = ''
                        $scope.ModalTenedor = ''
                        $scope.ModalSemirremolque = ''
                        $scope.ModalPlaca = ''
                        $scope.ModalTarifarioCompra = {};
                    }
                }
            }
        }

        //--Validar Cambio Tarifa
        $scope.ListadoTipoTarifaFiltradas = [];
        $scope.CambiarSeleccionTarifaCompra = function (Tarifa) {
            if (Tarifa !== undefined) {
                $scope.ModalTarifaVenta = Tarifa;//--Asigna Tarifa Ventas a la Nueva Tarifa Seleccionada
                $scope.ListadoTipoTarifaFiltradas = [];
                //--Realiza el Filtro de los tipos de tarifas, a partir de la tarifa seleccionada
                for (var i = 0; i < $scope.ListaTipoTarifasCompra.length; i++) {
                    if ($scope.ListaTipoTarifasCompra[i].CodgioTarifa == Tarifa.Codigo) {
                        $scope.ListadoTipoTarifaFiltradas.push($scope.ListaTipoTarifasCompra[i]);
                    }
                }
                $scope.ModalTipoTarifaCompra = $scope.ListadoTipoTarifaFiltradas[0];//--Asigna el primer tipo de tarifa que se encuentre en la lista
                $scope.CambiarSeleccionTipoTarifaCompra($scope.ModalTipoTarifaCompra);//--Gestiona el cambio del tipo de tarifa, para los valores de calculos
            }
        };
        //--Validar Cambio Tarifa
        //--Validar Cambio Tipo Tarifa
        $scope.CambiarSeleccionTipoTarifaCompra = function (TipoTarifa) {
            if (TipoTarifa != undefined) {
                //--Verifica si el tipo de tarifa asignado se encuentra en alguna de las tarifas del tarifario cliente
                for (var i = 0; i < $scope.ListadoTarifasFiltradas.length; i++) {
                    if ($scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.TarifaTransporte.Codigo == $scope.ModalTarifaCompra.Codigo
                        && $scope.ListadoTarifasFiltradas[i].Ruta.Codigo == $scope.ModalRuta.Codigo
                        && $scope.ListadoTarifasFiltradas[i].TipoTarifaTransportes.Codigo == TipoTarifa.Codigo

                    ) {
                        $scope.ModalTipoTarifaVenta = $scope.ModalTipoTarifaCompra.Nombre;//--Actualiza el nombre de la tarifa ventas
                        $scope.ModalFleteCliente = $scope.ListadoTarifasFiltradas[i].ValorFlete;//--Actualiza el valor del flete del cliente de la tarifa que concuerda
                        break;
                    }
                }
                $scope.ModalFleteTransportador = MascaraValores($scope.ModalTipoTarifaCompra.ValorFlete);
                //--realiza los calculos de totales flete, tanto compra y venta
                $scope.GestionarFletesCompra();
                $scope.GestionarFletesVenta();
            }
        };
        //--Validar Cambio Tipo Tarifa


        //----------------- GESTION DE TARIFAS ------------------------//
        $scope.ValidarFleteTransportador = function () {
            $scope.AutorizacionFlete = 0
            $scope.AutorizacionAnticipo = 0
            if ($scope.Sesion.UsuarioAutenticado.ManejoAutorizacionFlete) {
                if (parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)) > parseInt(MascaraNumero($scope.ModalTotalFleteTransportadorTEMP))) {
                    ShowConfirm('El valor del flete ingresado es superior al estipulado en el tarifario. ¿Desea solicitar autorización para el cambio de flete?', $scope.CambiarFlete, $scope.RechazoCambioFlete)
                } else {
                    $scope.calcularAnticipo()
                }
            } else {
                $scope.calcularAnticipo()
            }
        }

        $scope.CambiarFlete = function () {
            $scope.AutorizacionFlete = 1
            $scope.AutorizacionAnticipo = 0
            $scope.calcularAnticipo()
        }

        $scope.RechazoCambioFlete = function () {
            $scope.AutorizacionFlete = 0
            $scope.ModalTotalFleteTransportador = MascaraValores($scope.ModalTotalFleteTransportadorTEMP)
            $scope.calcularAnticipo()
        }

        $scope.ValidarAnticipoPlanilla = function (anticipo) {
            $scope.AutorizacionAnticipo = 0
            anticipo = parseInt(MascaraNumero(anticipo))
            if (parseFloat($scope.PorcentajeAnticipoRuta) > 0) {
                if (anticipo > parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))) {
                    ShowError('El anticipo no puede se mayor al flete del transportador')
                    anticipo = 0
                    $scope.AutorizacionAnticipo = 0
                }
                else if (anticipo > ((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))) {
                    $scope.AnticipoAutorizado = MascaraValores((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))
                    showModal('modalConfirmacionAnticipo')
                } else {
                    $scope.AutorizacionAnticipo = 0
                }
                $scope.ModalAnticipoPlanilla = MascaraValores(Math.round(parseInt(anticipo)))
            }
        }

        $scope.ValidarAnticipoOrdenCargue = function (anticipo) {
            anticipo = parseInt(MascaraNumero(anticipo))
            if (parseFloat($scope.PorcentajeAnticipoRuta) > 0) {
                if (anticipo > ((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)))) {
                    ShowError('El anticipo no puede ser mayor a ' + ((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))))
                    $scope.ModalValorAnticipo = 0
                } else {
                    $scope.ModalValorAnticipo = MascaraValores(parseInt(anticipo))
                }
            }
        };

        $scope.GenerarDespacho = function () {
            if (DatosRequeridosGenerarDesacho()) {
                BloqueoPantalla.start('Guardando...');
                var OrdenServicio = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Numero,
                    Estado: 1,
                    UsuarioCrea: $scope.Sesion.UsuarioAutenticado,
                    TarifarioVentas: $scope.TarifarioVentas,
                    LineaNegocioCarga: { Codigo: $scope.CodigoLineaNegocio },
                    InsertaDetalle: 1,
                    Detalle: [{
                        Ruta: $scope.ModalRuta,
                        TipoLineaNegocioCarga: $scope.ModeloTipoLineaNegocio,
                        Tarifa: $scope.ModalTarifaVenta,
                        TipoTarifa: { Codigo: $scope.ModalTipoTarifaCompra.Codigo },
                        Producto: $scope.ModalProducto,
                        PermisoInvias: $scope.ModalPermisoInvias,
                        Cantidad: $scope.ModalCantidad,
                        Peso: $scope.ModalPeso,
                        ValorFleteTransportador: Math.round(MascaraNumero(MascaraValores($scope.ModalTotalFleteTransportador))),
                        ValorFleteCliente: Math.round(MascaraNumero(MascaraValores($scope.ModalTotalFleteCliente))),
                        DocumentoCliente: $scope.ModaloDocumentoCliente,
                        CiudadCargue: $scope.ModelCiudCarg,
                        DireccionCargue: $scope.ModelDireCarg,
                        TelefonoCargue: $scope.ModalTeleCarg,
                        ContactoCargue: $scope.ModalContacCarg,
                        Destinatario: $scope.ModeloCliente,
                        CiudadDestino: $scope.ModelCiudDesc,
                        DireccionDestino: $scope.ModelDireDesc,
                        TelefonoDestino: $scope.ModalTeleDesc,
                        ContactoDestino: $scope.ModalContacDesc,
                        FechaCargue: new Date(),
                        FechaDescargue : new Date()
                    }]
                }
                EncabezadoSolicitudOrdenServiciosFactory.Guardar(OrdenServicio).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                ShowSuccess('Se guardó el despacho correctamente');
                                $scope.ManejoCVCPDeseDespachar = false
                                $scope.IDCodigo = response.data.Datos;
                                $scope.CodigoDetalleSolicitud = response.data.Datos;
                                $scope.ModalFechaOrdenCargue = new Date()
                                $scope.GuardarOrdenCargueSync()
                                ObtenerOrdenServicio()
                                //document.location.href = '#!GestionarDespacharOrdenServicios/' + $scope.Numero + '/' + response.data.Datos;
                                BloqueoPantalla.stop();
                            }
                            else {
                                ShowError(response.statusText);
                                BloqueoPantalla.stop();
                            }
                        }
                        else {
                            ShowError(response.statusText);
                            BloqueoPantalla.stop();
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        BloqueoPantalla.stop();
                    });
            }
        }

        $scope.CalcularAnticipo = function () {
            $scope.ModalAnticipoPlanilla = MascaraValores(Math.round((parseFloat($scope.PorcentajeAnticipoRuta) / 100) * parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))))
        };

        //Calculo Tarifas Cliente
        $scope.GestionarFletesVenta = function () {
            var ModalCantidad = 0;
            var ModalFleteCliente = 0;
            var ModalPeso = 0;


            var tmpModalPeso = $scope.ModalPeso == undefined ? 0 : parseInt(MascaraNumero($scope.ModalPeso));
            if ($scope.Saldo < tmpModalPeso && $scope.TarifaCupoTonelada && $scope.ValidaPeso && !$scope.Sesion.UsuarioAutenticado.ManejoDespachoProgramacion) {
                ShowError('El peso no puede ser superior a ' + MascaraValores($scope.Saldo).toString() + ' Kg')
                $scope.ModalPeso = 0
            }
            else if ($scope.ModalTarifaVenta !== undefined && $scope.ModalTarifaVenta !== null) {

                $scope.ModalTotalFleteCliente = 0;

                /*
                    VF: Valor Total Flete
                    FC: Flete Cliente
                    C: Cantidad
                    PE: Peso Kg
                    RI: Rango Inferior
                    RS: Rango Superior
                */

                ModalCantidad = $scope.ModalCantidad == undefined ? 0 : MascaraNumero(MascaraValores($scope.ModalCantidad));
                ModalFleteCliente = $scope.ModalFleteCliente == undefined ? 0 : MascaraNumero(MascaraValores($scope.ModalFleteCliente));
                ModalPeso = $scope.ModalPeso == undefined ? 0 : MascaraNumero(MascaraValores($scope.ModalPeso));

                //Contenedor (VF = FC * C)
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_CONTENEDOR) {
                    $scope.ModalTotalFleteCliente = Math.round(ModalCantidad * ModalFleteCliente)
                }

                //Devolucion Contenedor (VF = FC * C)
                if ($scope.ModalTarifaVenta.Codigo == TARIFRA_DEVOLUCION_CONTENEDOR) {
                    $scope.ModalTotalFleteCliente = Math.round(ModalCantidad * ModalFleteCliente)
                }

                //Cupo Vehiculo (VF = FC * C)
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_CUPO_VEHICULO) {
                    $scope.ModalTotalFleteCliente = Math.round(ModalFleteCliente)
                }

                //Tarifa Dia / Hora (VF = FC * C)
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_DIA || $scope.ModalTarifaVenta.Codigo == TARIFA_HORA) {
                    $scope.ModalTotalFleteCliente = Math.round(ModalCantidad * ModalFleteCliente)
                }
                //Movilización
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_MOVILIZACION) {
                    $scope.ModalTotalFleteCliente = Math.round(parseInt(ModalFleteCliente))
                }
                //Peso flete
                if ($scope.ModalTarifaVenta.Codigo == 301) {
                    $scope.ModalTotalFleteCliente = Math.round((parseInt(ModalFleteCliente) / 1000) * parseInt(ModalPeso))
                }
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_PESO_FLETE) {
                    $scope.ModalTotalFleteCliente = Math.round((parseInt(ModalFleteCliente) / 1000) * parseInt(ModalPeso))
                }
                if ($scope.ModalTarifaVenta.Codigo == 302) {
                    $scope.ModalTotalFleteCliente = Math.round(ModalCantidad * ModalFleteCliente);
                }
                //Peso Barril galon
                if ($scope.ModalTarifaVenta.Codigo == 6 || $scope.ModalTarifaVenta.Codigo == 300) {
                    $scope.ModalTotalFleteCliente = parseInt(ModalFleteCliente) * parseFloat(ModalCantidad);
                    inserto = true
                }

                //Peso flete
                if ($scope.ModalTarifaVenta.Codigo == 303) {
                    $scope.ModalTotalFleteCliente = Math.round(parseInt(ModalFleteCliente))
                }
                //Rango Peso Valor Kilo (SI (PE >= RI Y PE <= RS) ENTONCES  VF = FC * PE)
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_RANGO_PESO_VALOR_KILO) {
                    if ($scope.ListadoRangoPesoValorKilo.lenght) {
                        for (var i = 0; i < $scope.ListadoRangoPesoValorKilo.length; i++) {
                            if (ModalPeso >= $scope.ListadoRangoPesoValorKilo[i].CampoAuxiliar2 && ModalPeso <= $scope.ListadoRangoPesoValorKilo[i].CampoAuxiliar3) {
                                $scope.ModalTotalFleteCliente = Math.round(ModalFleteCliente * ModalPeso);
                            }
                        }
                    }
                }

                //Rango Peso Valor Fijo (SI (PE >= RI Y PE <= RS) ENTONCES  VF = FC)
                if ($scope.ModalTarifaVenta.Codigo == TARIFA_RANGO_PESO_VALOR_FIJO) {
                    if ($scope.ListadoRangoPesoValorFijo.lenght) {
                        for (var i = 0; i < $scope.ListadoRangoPesoValorFijo.length; i++) {
                            if ($scope.ModalPeso >= $scope.ListadoRangoPesoValorFijo[i].CampoAuxiliar2 && $scope.ModalPeso <= $scope.ListadoRangoPesoValorFijo[i].CampoAuxiliar3) {
                                $scope.ModalTotalFleteCliente = Math.round($scope.ModalFleteCliente);
                            }
                        }
                    }
                }

                //Reformatea valores
                $scope.ModalCantidad = MascaraValores(MascaraNumero(ModalCantidad));
                $scope.ModalFleteCliente = MascaraValores(MascaraNumero(ModalFleteCliente));
                $scope.ModalCantidad = MascaraValores(MascaraNumero(ModalCantidad));
                $scope.ModalPeso = MascaraValores(MascaraNumero(ModalPeso));

            }
        };

        //---------------- GENERAR DOCUMENTOS ---------------------------//

        //Ventana Confirmacion
        $scope.ConfirmarGuardarDocumentos = function (Documento) {
            window.scrollTo(top, top);
            $scope.DocumentoGenerar = Documento;

            //1 = Orden Cargue
            if (Documento == 1) {
                $scope.MensajeGuardar = '¿Desea guardar la Orden de Cargue?'
                showModal('modalConfirmacionGuardarDocumentos');
            }
            //2 = Remesa
            if (Documento == 2) {
                $scope.MensajeGuardar = '¿Desea guardar la Remesa?'
                showModal('modalConfirmacionGuardarDocumentos');
            }
            //3 = Planilla Despacho
            if (Documento == 3) {
                $scope.MensajeGuardar = '¿Desea guardar la Planilla Despacho?'
                showModal('modalConfirmacionGuardarDocumentos');
            }
        }

        $scope.CerrarConfirmarGuardarDocumentos = function () {
            closeModal('modalConfirmacionGuardarDocumentos', 1);
        }
        //-----------------------

        //--Datos requeridos
        function DatosRequeridosDocumentos() {

            var Continuar = true;
            $scope.MensajesError = [];

            // 0 - Generales
            if ($scope.CodigoLineaNegocio !== CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                if ($scope.ModalPlaca == undefined || $scope.ModalPlaca == null || $scope.ModalPlaca == '') {

                    $scope.MensajesError.push('Debe ingresar un vehículo');
                    Continuar = false;
                } else {
                    if ($scope.ModalPlaca.Placa == '') {
                        $scope.MensajesError.push('Debe ingresar un vehículo');
                        Continuar = false;
                    } else {

                        if ($scope.ModalPlaca.TipoVehiculo.CampoAuxiliar3 == ESTADO_ACTIVO) {
                            if ($scope.ModalSemirremolque == undefined || $scope.ModalSemirremolque == null || $scope.ModalSemirremolque == '') {
                                $scope.MensajesError.push('El tipo de vehículo actual exige que ingrese un Semirremolque');
                                Continuar = false;
                            } else if ($scope.ModalSemirremolque.Codigo == 0) {
                                $scope.MensajesError.push('El tipo de vehículo actual exige que ingrese un Semirremolque');
                                Continuar = false;
                            }
                        }
                        if ($scope.ModalPlaca.Capacidad < $scope.ModalPeso) {
                            $scope.MensajesError.push('El peso ingresado supera la capacidad del vehículo');
                            Continuar = false;
                        }
                    }
                }

                if ($scope.ModalTenedor == undefined || $scope.ModalTenedor == null || $scope.ModalTenedor == '') {
                    $scope.MensajesError.push('No hay un tenedor seleccionado');
                    Continuar = false;
                } else {
                    if ($scope.ModalTenedor.Codigo == 0) {
                        $scope.MensajesError.push('Debe seleccionar un tenedor');
                        Continuar = false;
                    }
                }
                
                if ($scope.ModalTarifarioCompra == undefined || $scope.ModalTarifarioCompra == null || $scope.ModalTarifarioCompra == '') {
                    $scope.MensajesError.push('No hay un tarifario de compra que cumpla las condiciones especificadas del transportador');
                    Continuar = false;
                } else {
                    if ($scope.ModalTarifarioCompra.length == 0) {
                        $scope.MensajesError.push('Debe ingresar un tarifario de compra según condiciones para el transportador');
                        Continuar = false;
                    }
                }

            }

            if ($scope.ModalTarifarioVenta == undefined || $scope.ModalTarifarioVenta == null || $scope.ModalTarifarioVenta == '') {
                $scope.MensajesError.push('No hay un tarifario de venta que cumpla las condiciones especificadas del cliente');
                Continuar = false;
            } else {
                if ($scope.ModalTarifarioVenta.Codigo == 0) {
                    $scope.MensajesError.push('Debe ingresar un tarifario de venta según condiciones para el cliente');
                    Continuar = false;
                }
            }

            return Continuar;
        }

        function DatosRequeridosGenerarDesacho() {

            var Continuar = true;
            $scope.MensajesError = [];

            // 0 - Generales

            if ($scope.CodigoLineaNegocio !== CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                if ($scope.ModalPlaca == undefined || $scope.ModalPlaca == null || $scope.ModalPlaca == '') {

                    $scope.MensajesError.push('Debe ingresar un vehículo');
                    Continuar = false;
                } else {
                    if ($scope.ModalPlaca.Placa == '') {
                        $scope.MensajesError.push('Debe ingresar un vehículo');
                        Continuar = false;
                    }
                }

                if ($scope.ModalTenedor == undefined || $scope.ModalTenedor == null || $scope.ModalTenedor == '') {
                    $scope.MensajesError.push('No hay un tenedor seleccionado');
                    Continuar = false;
                } else {
                    if ($scope.ModalTenedor.Codigo == 0) {
                        $scope.MensajesError.push('Debe seleccionar un tenedor');
                        Continuar = false;
                    }
                }

                if ($scope.ModalTarifarioCompra == undefined || $scope.ModalTarifarioCompra == null || $scope.ModalTarifarioCompra == '') {
                    $scope.MensajesError.push('No hay un tarifario de compra que cumpla las condiciones especificadas del transportador');
                    Continuar = false;
                } else {
                    if ($scope.ModalTarifarioCompra.length == 0) {
                        $scope.MensajesError.push('Debe ingresar un tarifario de compra según condiciones para el transportador');
                        Continuar = false;
                    }
                }
            }


            if ($scope.ModalTarifarioVenta == undefined || $scope.ModalTarifarioVenta == null || $scope.ModalTarifarioVenta == '') {
                $scope.MensajesError.push('No hay un tarifario de venta que cumpla las condiciones especificadas del cliente');
                Continuar = false;
            } else {
                if ($scope.ModalTarifarioVenta.Codigo == 0) {
                    $scope.MensajesError.push('Debe ingresar un tarifario de venta según condiciones para el cliente');
                    Continuar = false;
                }
            }
            if ($scope.ModalRuta == undefined || $scope.ModalRuta == null || $scope.ModalRuta == '') {
                $scope.MensajesError.push('Debe Ingresar la Ruta');
                Continuar = false;
            }
            if ($scope.ModalProducto == undefined || $scope.ModalProducto == null || $scope.ModalProducto == '') {
                $scope.MensajesError.push('Debe Ingresar el producto');
                Continuar = false;
            }

            if ($scope.ModalProducto !== undefined || $scope.ModalProducto !== null || $scope.ModalProducto !== '') {
                if ($scope.ModalProducto.NaturalezaProducto == 9403 || $scope.ModalProducto.NaturalezaProducto == 9404) {//Estrapesada o estradimnensionada 
                    if (($scope.ModalPermisoInvias == undefined || $scope.ModalPermisoInvias == null || $scope.ModalPermisoInvias == "")) {
                        $scope.MensajesError.push('Debe ingresar permiso INVIAS');
                        continuar = false;
                    }
                }
            }
            
            if ($scope.ModalCantidad == undefined || $scope.ModalCantidad == null || $scope.ModalCantidad == '' || $scope.ModalCantidad == 0) {
                $scope.MensajesError.push('Debe Ingresar la cantidad');
                Continuar = false;
            }
            if ($scope.ModalPeso == undefined || $scope.ModalPeso == null || $scope.ModalPeso == '' || $scope.ModalPeso == 0) {
                $scope.MensajesError.push('Debe Ingresar el peso');
                Continuar = false;
            }
            if ($scope.ModelCiudCarg == undefined || $scope.ModelCiudCarg == null || $scope.ModelCiudCarg == '') {
                $scope.MensajesError.push('Debe Ingresar la ciudad de cargue');
                Continuar = false;
            }
            if ($scope.ModelDireCarg == undefined || $scope.ModelDireCarg == null || $scope.ModelDireCarg == '') {
                $scope.MensajesError.push('Debe Ingresar la dirección de cargue');
                Continuar = false;
            }
            if ($scope.ModalTeleCarg == undefined || $scope.ModalTeleCarg == null || $scope.ModalTeleCarg == '') {
                $scope.MensajesError.push('Debe Ingresar teléfono de cargue');
                Continuar = false;
            }
            if ($scope.ModalContacCarg == undefined || $scope.ModalContacCarg == null || $scope.ModalContacCarg == '') {
                $scope.MensajesError.push('Debe Ingresar el contacto de cargue');
                Continuar = false;
            }
            if ($scope.ModelCiudDesc == undefined || $scope.ModelCiudDesc == null || $scope.ModelCiudDesc == '') {
                $scope.MensajesError.push('Debe Ingresar la ciudad de descargue');
                Continuar = false;
            }
            if ($scope.ModelDireDesc == undefined || $scope.ModelDireDesc == null || $scope.ModelDireDesc == '') {
                $scope.MensajesError.push('Debe Ingresar la dirección de descargue');
                Continuar = false;
            }
            if ($scope.ModalTeleDesc == undefined || $scope.ModalTeleDesc == null || $scope.ModalTeleDesc == '') {
                $scope.MensajesError.push('Debe Ingresar teléfono de descargue');
                Continuar = false;
            }
            if ($scope.ModalContacDesc == undefined || $scope.ModalContacDesc == null || $scope.ModalContacDesc == '') {
                $scope.MensajesError.push('Debe Ingresar el contacto de descargue');
                Continuar = false;
            }
            return Continuar;
        }

        // 1 - Orden de Cargue
        function DatosRequeridosOrdenCargue() {
            var Continuar = true;
            if (DatosRequeridosDocumentos() == false) {
                Continuar = false;
            }
            $scope.MensajesErrorOrdenCargue = [];
            if ($scope.ModalFechaOrdenCargue == undefined || $scope.ModalFechaOrdenCargue == null || $scope.ModalFechaOrdenCargue == '') {
                $scope.MensajesErrorOrdenCargue.push('Debe ingresar la fecha de cargue');
                Continuar = false;
            }
            window.scrollTo(top, top);
            return Continuar;
        }

        // 2 - Remesa
        function DatosRequeridosRemesa() {
            var Continuar = true;
            if (DatosRequeridosDocumentos() == false) {
                Continuar = false;
            }
            $scope.MensajesErrorRemesa = [];
            if ($scope.ModalFechaRemesa == undefined || $scope.ModalFechaRemesa == null || $scope.ModalFechaRemesa == '') {
                $scope.MensajesErrorRemesa.push('Debe ingresar la fecha de la remesa');
                Continuar = false;
            }
            if (($scope.ModalNumeroOrdenCargue == 0 || $scope.ModalNumeroOrdenCargue == undefined || $scope.ModalNumeroOrdenCargue == '' || $scope.ModalNumeroOrdenCargue == null) && $scope.Sesion.UsuarioAutenticado.ManejoRequiereOrdenCargueDespacho) {
                $scope.MensajesErrorRemesa.push('Debe generar la orden de cargue');
                Continuar = false;
            }

            //--Validar Origen y Destino Remesa Ruta
            var ConcuerdaOrigenDestino = 0;
            if ($scope.ModalCiudadRemitente.Codigo == $scope.ModalRuta.CiudadOrigen.Codigo || $scope.ModalCiudadRemitente.Codigo == $scope.ModalRuta.CiudadDestino.Codigo) {
                ConcuerdaOrigenDestino += 1;
            }
            if ($scope.ModalCiudadDestinatario.Codigo == $scope.ModalRuta.CiudadOrigen.Codigo || $scope.ModalCiudadDestinatario.Codigo == $scope.ModalRuta.CiudadDestino.Codigo) {
                ConcuerdaOrigenDestino += 1;
            }
            if (ConcuerdaOrigenDestino == 0) {
                $scope.MensajesErrorRemesa.push('La ciudad del remitente o destinatario debe coincidir con alguna ciudad de la ruta');
                Continuar = false;
            }
            //--Validar Origen y Destino Remesa Ruta

            window.scrollTo(top, top);
            return Continuar;
        }

        // 3 - Planilla Despacho
        function DatosRequeridosPlanillaDespacho() {
            var Continuar = true;
            if (DatosRequeridosDocumentos() == false) {
                Continuar = false;
            }
            $scope.MensajesErrorPlanilla = [];
            if ($scope.ModalNumeroRemesa == undefined || $scope.ModalNumeroRemesa == null || $scope.ModalNumeroRemesa == '' || $scope.ModalNumeroRemesa == 0) {
                $scope.MensajesErrorPlanilla.push('La Planilla de Despacho no se puede guardar porque no hay una remesa generada para asociar');
                Continuar = false;
            }

            if ($scope.ModalFechaSalida == undefined || $scope.ModalFechaSalida == null || $scope.ModalFechaSalida == '') {
                $scope.MensajesErrorPlanilla.push('Debe ingresar la fecha de salida del viaje en la planilla de despacho');
                Continuar = false;
            }

            if ($scope.ModalHoraSalida == undefined || $scope.ModalHoraSalida == null || $scope.ModalHoraSalida == '') {
                $scope.MensajesErrorPlanilla.push('Debe ingresar la hora de salida del viaje');
                Continuar = false;
            }

            window.scrollTo(top, top);
            return Continuar;
        }

        //--Guardar Documentos
        $scope.GuardarDocumentos = function () {

            if ($scope.DocumentoGenerar == 1) {
                $scope.GuardarOrdenCargue();
            }
            if ($scope.DocumentoGenerar == 2) {
                $scope.GuardarRemesa();
            }
            if ($scope.DocumentoGenerar == 3) {
                $scope.GuardarPlanillaDespacho();
            }

        }

        $scope.GuardarOrdenCargue = function () {
            closeModal('modalConfirmacionGuardarDocumentos', 1);

            if (DatosRequeridosOrdenCargue() == true) {
                $scope.ModalNumeroOrdenCargue = 0;

                $scope.DocumentoOrdenCargue = {

                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    DesdeDespacharSolicitud: true,
                    IDDetalleSolicitud: $scope.CodigoDetalleSolicitud,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_ORDEN_CARGUE },

                    NumeroSolicitudServicio: $scope.Numero,
                    Fecha: $scope.ModeloFecha,
                    TerceroCliente: $scope.ModeloCliente,
                    TerceroRemitente: { Codigo: $scope.ModeloCliente.Codigo },

                    Vehiculo: $scope.ModalPlaca,
                    Semirremolque: $scope.ModalSemirremolque,
                    TerceroConductor: $scope.ModalConductor,
                    CiudadCargue: $scope.ModelCiudCarg,
                    DireccionCargue: $scope.ModelDireCarg,
                    TelefonosCargue: $scope.ModalTeleCarg,
                    ContactoCargue: $scope.ModalContacCarg,
                    Observaciones: $scope.ModalObservacionesOrdenCargue,
                    Ruta: $scope.ModalRuta,
                    Producto: $scope.ModalProducto,
                    CantidadCliente: MascaraDecimales($scope.ModalCantidad),
                    PesoCliente: $scope.ModalPeso,
                    ValorAnticipo: $scope.ModalValorAnticipo,
                    CiudadDescargue: $scope.ModelCiudDesc,
                    DireccionDescargue: $scope.ModelDireDesc,
                    TelefonoDescargue: $scope.ModalTeleDesc,
                    ContactoDescargue: $scope.ModalContacDesc,

                    Estado: ESTADO_ACTIVO,

                    CodigoUsuarioCrea: $scope.Sesion.UsuarioAutenticado,
                    Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
                    Numeracion: ''
                }
                BloqueoPantalla.start('Guardando...');

                OrdenCargueFactory.Guardar($scope.DocumentoOrdenCargue).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            $scope.ModalNumeroOrdenCargue = response.data.Datos;

                            for (var i = 0; i < $scope.ListaOrdenesServicios.length; i++) {
                                if ($scope.ListaOrdenesServicios[i].Codigo == $scope.CodigoDetalleSolicitud) {
                                    $scope.ListaOrdenesServicios[i].NumeroOrdenCargue = response.data.Numero;
                                    $scope.ListaOrdenesServicios[i].NumeroDocumentoOrdenCargue = response.data.Datos;
                                    $scope.NumeroOrdenCargue = response.data.Numero;
                                    $scope.NumeroDocumentoOrdenCargue = response.data.Datos;
                                    $scope.DeshabilitarOrdenCargue = true;
                                    $scope.DeshabilitarDespacho = true;
                                }
                            }

                            ShowSuccess('Se guardó la Orden de Cargue No. ' + $scope.ModalNumeroOrdenCargue + ' correctamente.\n');
                            BloqueoPantalla.stop()
                        }
                        else if (response.data.Datos == -1) {
                            ShowWarning('', 'Ya se generó una orden de cargue para el despacho actual');
                        }
                        else if (response.data.Datos == -2) {
                            ShowWarning('', 'El vehículo ingresado tiene planillas pendientes por cumplir');
                        } else if (response.data.Datos == -3) {
                            ShowWarning('', 'No es posible generar documentos desde el despacho actual dado que el registro ya no existe debido a que los valores del despacho fuerón modificados desde la programación lo cual genero un nuevo detalle con los nuevos valores');
                        }
                        BloqueoPantalla.stop()
                    }

                });
            }

        }

        $scope.GuardarOrdenCargueSync = function () {
            closeModal('modalConfirmacionGuardarDocumentos', 1);

            if (DatosRequeridosOrdenCargue() == true) {
                $scope.ModalNumeroOrdenCargue = 0;

                $scope.DocumentoOrdenCargue = {

                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    DesdeDespacharSolicitud: true,
                    IDDetalleSolicitud: $scope.CodigoDetalleSolicitud,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_ORDEN_CARGUE },

                    NumeroSolicitudServicio: $scope.Numero,
                    Fecha: $scope.ModeloFecha,
                    TerceroCliente: $scope.ModeloCliente,
                    TerceroRemitente: { Codigo: $scope.ModeloCliente.Codigo },

                    Vehiculo: $scope.ModalPlaca,
                    Semirremolque: $scope.ModalSemirremolque,
                    TerceroConductor: $scope.ModalConductor,
                    CiudadCargue: $scope.ModelCiudCarg,
                    DireccionCargue: $scope.ModelDireCarg,
                    TelefonosCargue: $scope.ModalTeleCarg,
                    ContactoCargue: $scope.ModalContacCarg,
                    Observaciones: $scope.ModalObservacionesOrdenCargue,
                    Ruta: $scope.ModalRuta,
                    Producto: $scope.ModalProducto,
                    CantidadCliente: $scope.ModalCantidad,
                    PesoCliente: $scope.ModalPeso,
                    ValorAnticipo: $scope.ModalValorAnticipo,
                    CiudadDescargue: $scope.ModelCiudDesc,
                    DireccionDescargue: $scope.ModelDireDesc,
                    TelefonoDescargue: $scope.ModalTeleDesc,
                    ContactoDescargue: $scope.ModalContacDesc,

                    Estado: ESTADO_ACTIVO,

                    CodigoUsuarioCrea: $scope.Sesion.UsuarioAutenticado,
                    Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
                    Numeracion: '',
                    Sync: true
                }
                BloqueoPantalla.start('Guardando...');

                Response = OrdenCargueFactory.Guardar($scope.DocumentoOrdenCargue)

                if (Response.ProcesoExitoso === true) {
                    if (Response.Datos > 0) {
                        $scope.ModalNumeroOrdenCargue = Response.Datos;

                        for (var i = 0; i < $scope.ListaOrdenesServicios.length; i++) {
                            if ($scope.ListaOrdenesServicios[i].Codigo == $scope.CodigoDetalleSolicitud) {
                                $scope.ListaOrdenesServicios[i].NumeroOrdenCargue = Response.Numero;
                                $scope.ListaOrdenesServicios[i].NumeroDocumentoOrdenCargue = Response.Datos;
                                $scope.NumeroOrdenCargue = Response.Numero;
                                $scope.NumeroDocumentoOrdenCargue = Response.Datos;
                                $scope.DeshabilitarOrdenCargue = true;
                                $scope.DeshabilitarDespacho = true;
                            }
                        }

                        ShowSuccess('Se guardó la Orden de Cargue No. ' + $scope.ModalNumeroOrdenCargue + ' correctamente.\n');
                        BloqueoPantalla.stop()
                    }
                    else if (Response.Datos == -1) {
                        ShowWarning('', 'Ya se generó una orden de cargue para el despacho actual');
                    }
                    else if (Response.Datos == -2) {
                        ShowWarning('', 'El vehículo ingresado tiene planillas pendientes por cumplir');
                    }
                    else if (response.data.Datos == -3) {
                        ShowWarning('', 'No es posible generar documentos desde el despacho actual dado que el registro ya no existe debido a que los valores del despacho fuerón modificados desde la programación lo cual genero un nuevo detalle con los nuevos valores');
                    }
                    BloqueoPantalla.stop()
                }

            }

        }

        $scope.GuardarRemesa = function () {

            closeModal('modalConfirmacionGuardarDocumentos', 1);

            if (DatosRequeridosRemesa() == true) {
                $scope.ModalNumeroRemesa = 0;

                if ($scope.NumeroOrdenCargue == undefined || $scope.NumeroOrdenCargue == null || $scope.NumeroOrdenCargue == '') {
                    $scope.NumeroOrdenCargue = 0;
                }

                $scope.NumeroTarifarioCompra = 0;
                if ($scope.ModalTarifarioCompra !== undefined && $scope.ModalTarifarioCompra !== null) {
                    if ($scope.ModalTarifarioCompra.length > 0) {
                        $scope.NumeroTarifarioCompra = $scope.ModalTarifarioCompra[0].NumeroTarifarioCompra;
                    }
                }



                $scope.DocumentoRemesa = {

                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    DesdeDespacharSolicitud: true,
                    IDDetalleSolicitud: $scope.CodigoDetalleSolicitud,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS },
                    NumeroSolicitudServicio: $scope.Numero,
                    TipoRemesa: { Codigo: $scope.CodigoTipoRemesa },

                    NumeroDocumentoCliente: $scope.ModalDocCliente,
                    FechaDocumentoCliente: new Date(),
                    Fecha: $scope.ModeloFecha,
                    Ruta: { Codigo: $scope.ModalRuta.Codigo },
                    ProductoTransportado: { Codigo: $scope.ModalProducto.Codigo },
                    PermisoInvias: $scope.ModalPermisoInvias,
                    FormaPago: { Codigo: CODIGO_FORMA_PAGO_CREDITO_REMESA },

                    Cliente: { Codigo: $scope.ModeloCliente.Codigo },
                    FacturarA: OrdenServicio.Facturar.Codigo,
                    Remitente: { Codigo: $scope.ModalRemitente.Codigo },
                    CiudadRemitente: { Codigo: $scope.ModalCiudadRemitente.Codigo },
                    DireccionRemitente: $scope.ModalDireccionRemitente,
                    TelefonoRemitente: $scope.ModalTelefonosRemitente,
                    Observaciones: $scope.ModalObservacionesRemesa,
                    CantidadCliente: MascaraNumero(MascaraValores($scope.ModalCantidad)),
                    PesoCliente: MascaraNumero(MascaraValores($scope.ModalPeso)),
                    PesoVolumetricoCliente: 0,
                    DetalleTarifaVenta: { Codigo: $scope.IDCodigo },

                    ValorFleteCliente: MascaraNumero(MascaraValores($scope.ModalFleteCliente)),
                    ValorManejoCliente: 0, // Pendiente -> Porcentaje manejo * Valor mercancia
                    ValorSeguroCliente: 0, // Pendiente -> Porcenatje Seguro * Valor Mercancia
                    ValorDescuentoCliente: 0,
                    TotalFleteCliente: Math.round(MascaraNumero(MascaraValores($scope.ModalTotalFleteCliente))),
                    ValorComercialCliente: 0,
                    CantidadTransportador: MascaraNumero(MascaraValores($scope.ModalCantidad)),
                    PesoTransportador: MascaraNumero(MascaraValores($scope.ModalPeso)),
                    ValorFleteTransportador: MascaraNumero(MascaraValores($scope.ModalFleteTransportador)),
                    TotalFleteTransportador: MascaraNumero(MascaraValores($scope.ModalTotalFleteTransportador)),
                    Destinatario: { Codigo: $scope.ModalDestinatario.Codigo },
                    CiudadDestinatario: { Codigo: $scope.ModalCiudadDestinatario.Codigo },
                    DireccionDestinatario: $scope.ModalDireccionDestinatario,
                    TelefonoDestinatario: $scope.ModalTelefonosDestinatario,
                    Estado: ESTADO_ACTIVO,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Oficina: { Codigo: $scope.ModeloOficinaDespacha.Codigo },
                    Numeracion: '',
                    TarifarioCompra: { Numero: $scope.NumeroTarifarioCompra },
                    TarifarioVenta: { Codigo: $scope.ModalTarifarioVenta.Codigo },
                    NumeroOrdenServicio: $scope.Numero,
                    Vehiculo: { Codigo: $scope.ModalPlaca.Codigo },
                    Conductor: { Codigo: $scope.ModalConductor.Codigo },
                    OrdenCargue: { Numero: $scope.NumeroOrdenCargue },
                    NumeroContenedor: $scope.ModalNumeroContenedor,
                    MBLContenedor: $scope.ModalMBLContenedor,
                    HBLContenedor: $scope.ModalHBLContenedor,
                    DOContenedor: $scope.ModalDOContenedor,
                    ValorFOB: $scope.ModalValorFOB,
                    CiudadDevolucion: $scope.ModalImportacionCiudadDevolucion,
                    PatioDevolucion: $scope.ModalImportacionPatioDevolucion,
                    FechaDevolucion: $scope.ModalImportacionFechaDevolucion,
                    ListaPrecintos: $scope.ListaPrecintos
                }
                if ($scope.ModalSemirremolque !== undefined && $scope.ModalSemirremolque !== null && $scope.ModalSemirremolque !== '') {
                    $scope.DocumentoRemesa.Semirremolque = { Codigo: $scope.ModalSemirremolque.Codigo }
                } else {
                    $scope.DocumentoRemesa.Semirremolque = { Codigo: 0 }
                }

                BloqueoPantalla.start('Guardando...');
                RemesasFactory.Guardar($scope.DocumentoRemesa).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            $scope.ModalNumeroRemesa = response.data.Datos;
                            for (var i = 0; i < $scope.ListaOrdenesServicios.length; i++) {
                                if ($scope.ListaOrdenesServicios[i].Codigo == $scope.CodigoDetalleSolicitud) {
                                    $scope.ListaOrdenesServicios[i].NumeroRemesa = response.data.Numero;
                                    $scope.NumeroRemesa = response.data.Numero;
                                    $scope.ListaOrdenesServicios[i].NumeroDocumentoRemesa = response.data.Datos;
                                    $scope.NumeroDocumentoRemesa = response.data.Datos;
                                    $scope.ModalNumeroConfirmacionMinisterio = response.data.Datos.NumeroConfirmacionMinisterioRemesa;
                                    $scope.DesabilitarConfirmacionRemesa = true;
                                    $scope.DeshabilitarRemesa = true;
                                    $scope.DeshabilitarDespacho = true;
                                    $scope.DeshabilitarTarifaTransportador = true;
                                }
                            }
                            ShowSuccess('Se guardó la Remesa No. ' + $scope.ModalNumeroRemesa + ' correctamente.\n');
                        } else if (response.data.Datos == -1) {
                            ShowWarning('', 'Ya se generó una remesa para el despacho actual');
                        } else if (response.data.Datos == -2) {
                            ShowWarning('', 'El vehículo ingresado tiene planillas pendientes por cumplir');
                        } else if (response.data.Datos == -3) {
                            ShowWarning('', 'No es posible generar documentos desde el despacho actual dado que el registro ya no existe debido a que los valores del despacho fuerón modificados desde la programación lo cual genero un nuevo detalle con los nuevos valores');
                        } else if (response.data.Datos == -4) {
                            ShowWarning('', 'El peso que intenta ingresar sumado a lo pendiente por despachar excede el saldo de la orden de servicio, intente con otro valor');
                        }
                    }
                    BloqueoPantalla.stop()
                });

            }
        }

        $scope.ListaPrecintos = []
        $scope.AgregarPrecintos = function () {
            if ($scope.CantidadPrecintos > 0 && $scope.CantidadPrecintos !== undefined && $scope.CantidadPrecintos !== null) {
                RemesasFactory.ObtenerPrecintosAleatorios({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Oficina: { Codigo: $scope.ModeloOficinaDespacha.Codigo },
                    TipoPrecinto: { Codigo: $scope.ModeloTipoPrecinto.Codigo },
                    CantidadPrecintos: $scope.CantidadPrecintos
                }).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                $scope.ListaPrecintos.push(response.data.Datos[i])
                            }
                            if (response.data.Datos.length < parseInt($scope.CantidadPrecintos)) {
                                ShowWarning('', "La cantidad solicitada no se encuentra disponible, se cargarón " + response.data.Datos.length + " Precintos")
                            }
                        } else {
                            ShowWarning('', 'No se encontraron precintos para la oficina actual')
                        }
                    }
                });
            } else {
                ShowError("Por favor ingrese la cantidad de precintos")
            }
        }

        $scope.GuardarPlanillaDespacho = function () {
            closeModal('modalConfirmacionGuardarDocumentos', 1);
            if (DatosRequeridosPlanillaDespacho() == true) {
                BloqueoPantalla.start('Guardando...');
                ImpuestosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, CodigoTipoDocumento: 150, CodigoCiudad: $scope.CiudadOrigenRuta.Codigo, AplicaTipoDocumento: 1 }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ListadoImpuestos = []
                            $scope.ListadoImpuestosFiltrado = []
                            $scope.TotalImpuestos = 0
                            if (response.data.Datos.length > CERO) {
                                $scope.ListadoImpuestos = response.data.Datos;
                                $scope.ListadoImpuestosFiltrado = []
                                if ($scope.ListadoImpuestos.length > 0) {
                                    for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                                        var impuesto = {
                                            Nombre: $scope.ListadoImpuestos[i].Nombre,
                                            CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                                            ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                                            ValorBase: $scope.ListadoImpuestos[i].valor_base,
                                        }
                                        if ($scope.ListadoImpuestos[i].valor_base < parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))) {
                                            impuesto.ValorBase = parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))
                                        }
                                        impuesto.ValorImpuesto = impuesto.ValorTarifa * impuesto.ValorBase
                                        $scope.TotalImpuestos += impuesto.ValorImpuesto
                                        $scope.ListadoImpuestosFiltrado.push(impuesto)
                                    }
                                }
                            }
                            else {
                                $scope.ListadoImpuestos = [];
                                $scope.ListadoImpuestosFiltrado = [];
                            }
                            $scope.ModalNumeroPlanilla = 0;
                            $scope.DetallePlanilla = [];
                            var RemesaPlanillar = {
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Numero: $scope.NumeroPlanilla,
                                Remesa: { Numero: $scope.NumeroRemesa, NumeroDocumento: $scope.NumeroDocumentoRemesa }
                            }
                            $scope.DetallePlanilla.push(RemesaPlanillar);
                            var FechaAux = Formatear_Fecha_Mes_Dia_Ano($scope.ModalFechaSalida) + ' ' + $scope.ModalHoraSalida;
                            FechaAux = new Date(FechaAux);
                            var ResponseTipoDocumentoPlanilla = TipoDocumentosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO, Sync: true }).Datos
                            $scope.DocumentoPlanillaDespacho = {

                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                DesdeDespacharSolicitud: true,
                                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO,
                                IDDetalleSolicitud: $scope.CodigoDetalleSolicitud,
                                NumeroSolicitud: $scope.Numero,

                                Numero: 0,
                                Fecha: $scope.ModeloFecha,
                                FechaHoraSalida: FechaAux,
                                Ruta: { Codigo: $scope.ModalRuta.Codigo },
                                Vehiculo: {
                                    Codigo: $scope.ModalPlaca.Codigo,
                                    TipoDueno: { Codigo: $scope.ModalPlaca.TipoDueno.Codigo }
                                },
                                Tenedor: { Codigo: $scope.ModalTenedor.Codigo },

                                Conductor: { Codigo: $scope.ModalConductor.Codigo },
                                Semirremolque: $scope.ModalSemirremolque,

                                Cantidad: $scope.ModalCantidad,
                                Peso: $scope.ModalPeso,
                                ValorFleteTransportador: $scope.AutorizacionFlete > 0 ? parseInt(MascaraNumero($scope.ModalTotalFleteTransportadorTEMP)) : parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)),
                                strValorFleteTransportador: MascaraValores($scope.AutorizacionFlete > 0 ? parseInt(MascaraNumero($scope.ModalTotalFleteTransportadorTEMP)) : parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))),
                                ValorAnticipo: $scope.ModalAnticipoPlanilla,
                                AutorizacionAnticipo: $scope.AutorizacionAnticipo,
                                AutorizacionFlete: $scope.AutorizacionFlete,
                                ValorFleteAutorizacion: parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)),
                                strValorFleteAutorizacion: MascaraValores(parseInt(MascaraNumero($scope.ModalTotalFleteTransportador))),
                                ValorImpuestos: 0,

                                ValorPagarTransportador: parseInt(MascaraNumero($scope.ModalTotalFleteTransportador)) - $scope.TotalImpuestos - parseInt(MascaraNumero($scope.ModalAnticipoPlanilla)),
                                ValorFleteCliente: $scope.ModalFleteCliente,
                                ValorSeguroMercancia: 0,
                                ValorOtrosCobros: 0,
                                ValorTotalCredito: 0,
                                ValorTotalContado: 0,
                                ValorTotalAlcobro: 0,
                                ValorSeguroPoliza: ($scope.ModalSeguroAnticipo !== undefined && $scope.ModalSeguroAnticipo !== '' && $scope.ModalSeguroAnticipo !== null) ? parseInt(MascaraNumero($scope.ModalSeguroAnticipo)) : 0,

                                TarifaTransportes: { Codigo: $scope.ModalTarifaCompra.Codigo },
                                TipoTarifaTransportes: $scope.ModalTipoTarifaCompra,


                                LineaNegocioTransportes: { Codigo: $scope.CodigoLineaNegocio },
                                TipoLineaNegocioTransportes: { Codigo: $scope.ModeloTipoLineaNegocio.Codigo },
                                NumeroTarifarioCompra: $scope.Sesion.UsuarioAutenticado.ManejoTarifaCompraTarifarioVenta === true ? 0 : $scope.ModalTarifarioCompra[0].Codigo,
                                ValorAuxiliares: 0,
                                Numeracion: '',

                                Observaciones: $scope.ModalObservacionesPlanilla,
                                Estado: { Codigo: ESTADO_ACTIVO },
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                Detalles: $scope.DetallePlanilla,
                                Oficina: { Codigo: $scope.ModeloOficinaDespacha.Codigo },
                                DetalleImpuesto: $scope.ListadoImpuestosFiltrado,
                                AnticipoPagadoA: $scope.Radio.Anticipo,
                                GeneraComprobanteContable: ResponseTipoDocumentoPlanilla.GeneraComprobanteContable
                            }
                            if ($scope.ListaAuxiliares.length > 0) {
                                $scope.DocumentoPlanillaDespacho.DetallesAuxiliares = []
                                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                                    if (!$scope.ListaAuxiliares[i].Modificarfuncionario) {
                                        $scope.DocumentoPlanillaDespacho.DetallesAuxiliares.push({
                                            Funcionario: $scope.ListaAuxiliares[i].Tercero
                                            , NumeroHorasTrabajadas: $scope.ListaAuxiliares[i].Horas
                                            , Valor: $scope.ListaAuxiliares[i].Valor
                                            , Observaciones: $scope.ListaAuxiliares[i].Observaciones
                                        });
                                    }
                                }
                            }
                            if (parseInt($scope.ModalAnticipoPlanilla) > CERO) {
                                $scope.DocumentoPlanillaDespacho.CuentaPorPagar = {
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                                    CodigoAlterno: '',
                                    Fecha: $scope.ModeloFecha,
                                    Tercero: { Codigo: ($scope.Radio.Tenedor == 'Tenedor' ? $scope.CodigoTenedor : $scope.ModalConductor.Codigo) },
                                    AplicaPSL: ($scope.Sesion.UsuarioAutenticado.ManejoAnticiposPSL && $scope.FormaPagoRecaudo.Codigo == 2) ? 1 : 0,
                                    DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO },
                                    CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO,
                                    //Numero: $scope.NumeroPlanilla,

                                    Numeracion: '',
                                    //Observaciones: 'PAGO ANTICIPO PLANILLA No. ' + $scope.NumeroDocumentoPlanilla + ' VALOR $ ' + $scope.ModalAnticipoPlanilla,
                                    CuentaPuc: { Codigo: 0 },
                                    ValorTotal: $scope.ModalAnticipoPlanilla,
                                    Abono: 0,
                                    Saldo: $scope.ModalAnticipoPlanilla,
                                    FechaCancelacionPago: $scope.ModeloFecha,
                                    Aprobado: 1,
                                    UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    FechaAprobo: $scope.ModeloFecha,
                                    Oficina: { Codigo: $scope.ModeloOficinaDespacha.Codigo },
                                    Vehiculo: { Codigo: $scope.ModalPlaca.Codigo },
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                };
                            }

                            //--Valida Manifiesto Para Generacion
                            if ($scope.Sesion.Empresa.GeneraManifiesto === 1 && $scope.TipoRuta.Codigo == 4401) {
                                //JV: 2018-06-20: Se genera el manifiesto a partir de la planilla si el país de la empresa lo aplica
                                $scope.ModeloAceptacionElectronica = $scope.ModeloAceptacionElectronica == true ? 1 : 0;
                                $scope.DocumentoPlanillaDespacho.Manifiesto = {
                                    Numero: 0,
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    NumeroPlanilla: 0,
                                    Tipo_Documento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO_MASIVO },
                                    Usuario_Crea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                                    Tipo_Manifiesto: { Codigo: CODIGO_CATALOGO_TIPO_MANIFIESTO_MASIVO },
                                    NumeroSolicitudServicio: $scope.Numero,
                                    IdDetalleSolicitudServicio: $scope.CodigoDetalleSolicitud,
                                    Valor_Anticipo: $scope.ModalAnticipoPlanilla,
                                    CuentaPorPagar: {}
                                };
                            }
                            //--Valida Manifiesto Para Generacion

                            PlanillaDespachosFactory.Guardar($scope.DocumentoPlanillaDespacho).then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos > 0) {
                                        $scope.ModalNumeroPlanilla = response.data.Numero;
                                        $scope.DeshabilitarPlanilla = true;
                                        for (var i = 0; i < $scope.ListaOrdenesServicios.length; i++) {
                                            if ($scope.ListaOrdenesServicios[i].Codigo == $scope.CodigoDetalleSolicitud) {
                                                $scope.ListaOrdenesServicios[i].NumeroPlanillaDespacho = response.data.Numero;
                                                $scope.ListaOrdenesServicios[i].NumeroDocumentoPlanillaDespacho = response.data.Datos;
                                                $scope.NumeroPlanilla = response.data.Numero;
                                                $scope.NumeroDocumentoPlanilla = response.data.Datos;
                                            }
                                        }
                                        ShowSuccess('Se guardó la Planilla de Despacho No. ' + $scope.ModalNumeroPlanilla + ' correctamente.');
                                    }
                                    else if (response.data.Datos == -1) {
                                        ShowWarning('', 'Ya se generó una planilla para el despacho actual');
                                    } else if (response.data.Datos == -2) {
                                        ShowWarning('', 'Ya se generó una planilla para el despacho actual');
                                    } else if (response.data.Datos == -3) {
                                        ShowWarning('', 'El vehículo ingresado tiene planillas pendientes por cumplir');
                                    }
                                    else if (response.data.Datos == -4) {
                                        ShowWarning('', 'No es posible generar documentos desde el despacho actual dado que el registro ya no existe debido a que los valores del despacho fuerón modificados desde la programación lo cual genero un nuevo detalle con los nuevos valores');
                                    }
                                    else {
                                        ShowWarning('', 'Ya se generó una planilla para el despacho actual');
                                    }
                                    BloqueoPantalla.stop()
                                }
                            });
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        BloqueoPantalla.stop()
                    });



            }
        }
        //---------------------------------------------------------------//

        //------------------------------------- MASRCARAS -----------------------------------------//
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };

        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);
        };

        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope);
        }

        $scope.MaskNumeroGrid = function (item) {
            if (item !== undefined) {
                return MascaraNumero(item.toString())
            }
            else {
                return item
            }
        };

        $scope.MaskValoresGrid = function (item) {
            if (item !== undefined) {
                return MascaraValores(item.toString())
            }
            else {
                return item
            }
        };

        /*-----Asignaciones ----------------------------------------------------------- */

        $scope.AsignarValorEscolta = function (item) {
            if (item !== true) {
                $scope.ModalValorEscolta = 0;
                $scope.DeshabilitarValorEscolta = true;
            } else {
                $scope.ModalValorEscolta = $scope.ValorEscolta;
                $scope.DeshabilitarValorEscolta = false;
            }
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ValidarVehiculoDocumentos = function (Vehiculo, opt) {
            if (Vehiculo != undefined && Vehiculo != null && Vehiculo != '') {
                if (VehiculosFactory.ConsultarDocumentosSoatRTM(
                    {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: Vehiculo.Codigo,
                        CodigoDetalleEpos: $scope.IdDetale,
                        Sync: true
                    }).ProcesoExitoso
                ) {
                    $scope.AsignarDatosVehiculo($scope.ModalPlaca);

                    var vehiculoalterno = $scope.CargarVehiculos(Vehiculo.Codigo);
                    if (vehiculoalterno.Estado.Codigo == 0) {
                        ShowError("El vehículo " + vehiculoalterno.Placa + " esta inactivo por motivo '" + vehiculoalterno.JustificacionBloqueo + "'")
                        $scope.ModalConductor = '';
                        $scope.ModalTenedor = '';
                        $scope.ModalSemirremolque = '';
                        $scope.ModalPlaca = '';
                        return false
                    } else {
                        var TerceroAlterno = $scope.CargarTercero(vehiculoalterno.Conductor.Codigo)
                        if (TerceroAlterno.Estado.Codigo == 0) {
                            ShowError("El conductor del vehículo " + vehiculoalterno.Placa + " de nombre '" + TerceroAlterno.NombreCompleto + "' esta inactivo por motivo '" + TerceroAlterno.JustificacionBloqueo + "'")
                            $scope.ModalConductor = '';
                            $scope.ModalTenedor = '';
                            $scope.ModalSemirremolque = '';
                            $scope.ModalPlaca = '';
                            return false
                        } else {
                            if ($scope.TipoVehiculoOrdenServicio.Codigo !== 0) {
                                if (Vehiculo.TipoVehiculo.Codigo !== $scope.TipoVehiculoOrdenServicio.Codigo) {
                                    ShowError('El tipo vehículo del vehículo ingresado no corresponde al asignado en la orden de servicio')
                                    $scope.ModalConductor = '';
                                    $scope.ModalTenedor = '';
                                    $scope.ModalSemirremolque = '';
                                    $scope.ModalPlaca = '';
                                    return false
                                } else {
                                    return true
                                }
                            } else {
                                return true
                            }
                        }
                    }
                }
                else {
                    if (opt > 0) {
                        ShowError("El vehículo tiene documentos proximos a vencerse")
                    }
                    $scope.ModalConductor = '';
                    $scope.ModalTenedor = '';
                    $scope.ModalSemirremolque = '';
                    $scope.ModalPlaca = '';
                    return false
                }
            }
        }

        $scope.AsignarListaplaca = function ($viewValue) {
            return RetornarListaAutocomplete($viewValue, $scope.ListaPlaca, 'Placa', 'CodigoAlterno');
        }

        $scope.AsignarDatosVehiculo = function (Vehiculo) {
            $scope.ModalConductor = $scope.CargarTercero(Vehiculo.Conductor.Codigo);
            $scope.ModalTenedor = $scope.CargarTercero(Vehiculo.Tenedor.Codigo);
            if (Vehiculo.Semirremolque !== undefined) {
                if (Vehiculo.Semirremolque.Codigo > 0) {
                    try {
                        $scope.ModalSemirremolque = SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true, Codigo: Vehiculo.Semirremolque.Codigo }).Datos[0] // $linq.Enumerable().From($scope.ListaPlacaSemi).First('$.Codigo ==' + Vehiculo.Semirremolque.Codigo);
                    } catch (e) {
                    }
                } else if (Vehiculo.Semirremolque.Placa !== undefined && Vehiculo.Semirremolque.Placa !== '' && Vehiculo.Semirremolque.Placa !== null) {
                    try {
                        $scope.ModalSemirremolque = SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true, Placa: Vehiculo.Semirremolque.Placa }).Datos[0]  //$linq.Enumerable().From($scope.ListaPlacaSemi).First('$.Placa == "' + Vehiculo.Semirremolque.Placa + '"');
                    } catch (e) {
                    }
                }
            }


            if ($scope.CodigoLineaNegocio != 2) {
                $scope.AplicAnticipoTenedor = true
                //$scope.Radio = { Anticipo: 'Tenedor' }
            }
            try {
                var FiltroTarifa = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.ModalTenedor.Codigo,
                    Sync: true
                }
                var response = TercerosFactory.Obtener(FiltroTarifa)
                if (response.ProcesoExitoso === true) {
                    if (response.Datos.Codigo > 0) {
                        if (response.Datos.TipoAnticipo.Codigo == 18702) {
                            $scope.AplicAnticipoTenedor = true
                            $scope.Radio = { Anticipo: 'Tenedor' }

                        } else {
                            $scope.AplicAnticipoTenedor = false
                            $scope.Radio = { Anticipo: 'Conductor' }
                        }
                    }
                }
                $scope.ConsultarTarifarioCompra($scope.ModalTenedor.Codigo);
            } catch (e) {

            }

            if ($scope.ModalRuta != undefined && $scope.ModalRuta != "" && $scope.ModalRuta != null) {// se valida si se ha asignado vehiculo o ruta
                $scope.ConsultarTarifarioCompra($scope.ModalTenedor.Codigo);
                $scope.AsignarTarifa();
                $scope.GestionarFletesCompra();
                $scope.GestionarFletesVenta();
            }
        };

        $scope.AsignarListasemirremolque = function ($viewValue) {

            return SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: ESTADO_ACTIVO }, Sync: true }).Datos
            //return RetornarListaAutocomplete($viewValue, $scope.ListaPlacaSemi, 'Placa', 'CodigoAlterno');
        }

        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($scope.NumeroProgramacion > 0 && $scope.IdDetale > 0) {
                document.location.href = '#!ConsultarProcesoProgramarOrdenServicio/' + $scope.IdDetale;
            }
            else if ($routeParams.Numero > 0) {
                document.location.href = '#!ConsultarDespacharOrdenServicios/' + $routeParams.Numero;
            } else {
                document.location.href = '#!ConsultarDespacharOrdenServicios';
            }
        };

        /* Obtener parametros del enrutador*/
        //Parametros
        //'/GestionarDespacharOrdenServicios/:Numero/:Codigo'
        ///GestionarDespacharOrdenServicios/:Numero/:Codigo/:Programacion/:IdDetalle
        //Parametros Orden------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        if ($routeParams.Numero !== undefined) {
            $scope.Numero = $routeParams.Numero;
        }
        else {
            $scope.Numero = 0;
        }

        ////Parametros programación-----------------------------------------------------------------------------------------------------------------------------------------------------------------
        //if ($routeParams.Programacion !== undefined && $routeParams.IdDetalle !== undefined) {
        //    $scope.NumeroProgramacion = $routeParams.Programacion
        //    $scope.IdDetale = $routeParams.IdDetalle
        //    $scope.ValidaPeso = false
        //}

        //Parametros Cantidad viajes y cupo tonelada desde Despachar O.S--------------------------------------------------------------------------------------------------------------------------
        if ($routeParams.Codigo !== undefined) {
            if ($routeParams.Codigo === '0') {
                $scope.ManejoCVCPDeseDespachar = true;
            }
            $scope.IDCodigo = parseInt($routeParams.Codigo);
        }
        else {
            $scope.IDCodigo = 0;
            $scope.ManejoCVCPDeseDespachar = true;
        }

        //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        if ($scope.Numero > 0) {
            $scope.Titulo = 'DESPACHAR ORDEN SERVICIO';
            $scope.Deshabilitar = true;
            ObtenerOrdenServicio();
        }


    }]);