﻿SofttoolsApp.controller("ConsultarNotaFacturasCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'OficinasFactory', 'NotasFacturasFactory', 'blockUI', 'EmpresasFactory',
    'TercerosFactory', 'ConceptoNotasFacturasFactory', 'blockUIConfig', 'EncabezadoComprobantesContablesFactory', 'ValorCatalogosFactory',
    function ($scope, $timeout, $linq, $routeParams, OficinasFactory, NotasFacturasFactory, blockUI, EmpresasFactory,
        TercerosFactory, ConceptoNotasFacturasFactory, blockUIConfig, EncabezadoComprobantesContablesFactory, ValorCatalogosFactory) {
        $scope.Titulo = 'CONSULTAR NOTAS FACTURA';
        $scope.MapaSitio = [{ Nombre: 'Facturación' }, { Nombre: 'Documentos' }, { Nombre: 'Nota Factura' }];
        /*-----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- */
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.Deshabilitar = false;
        $scope.MensajesError = [];
        var filtros = {};
        $scope.pref = '';
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.ArchivoNota = "";
        $scope.MostrarInputjson = false;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_NOTA_CREDITO_FACTURAS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 }
        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        $scope.ListadoTipoNotas = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'CRÉDITO', Codigo: CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO },
            { Nombre: 'DÉBITO', Codigo: CODIGO_TIPO_DOCUMENTO_NOTA_DEBITO }
        ];
        $scope.TipoNota = $linq.Enumerable().From($scope.ListadoTipoNotas).First('$.Codigo == -1');

        $scope.Estados = {
            Adquiente: '',
            Notificacion: '',
            Fecha: ''
        };
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
        //----------------------------------------VERIFICAR PROVEEDOR FACTURA ELECTRONICA---------------------------------------//
        $scope.MostrarModuloNotaElectronica = false;
        if ($scope.Sesion.Empresa.EmpresaFacturaElectronica != undefined && $scope.Sesion.Empresa.EmpresaFacturaElectronica != '' && $scope.Sesion.Empresa.EmpresaFacturaElectronica != null) {
            $scope.MostrarModuloNotaElectronica = true;
        }
        //----------------------------------------VERIFICAR PROVEEDOR FACTURA ELECTRONICA---------------------------------------//
        //-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR ---------------------------------------------------------------------------------//
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        //Obtiene la URL para llamar el proyecto ASP
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

        //Función que ejecuta el boton de vista preliminar
        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            if ($scope.ValidarPermisos.AplicaImprimir == PERMISO_ACTIVO) {
                $scope.FiltroArmado = '';

                //Depende del listado seleccionado se enviará el nombre por parametro
                $scope.NombreReporte = NOMBRE_REPORTE_NOTAFACT;
                //Arma el filtro
                $scope.ArmarFiltro(Numero);
                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf == 1) {
                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
                }
                if (OpcionEXCEL == 1) {
                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL + '&Prefijo=' + $scope.pref);
                }
                //Se limpia el filtro para poder hacer mas consultas
                $scope.FiltroArmado = '';
            };
        };
        // funcion enviar parametros al proyecto ASP armando el filtro
        $scope.ArmarFiltro = function (Numero) {
            $scope.FiltroArmado = '';
            if (Numero !== undefined && Numero !== '' && Numero !== null) {
                $scope.FiltroArmado += '&Numero=' + Numero;
            }
        }
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

        if ($routeParams !== undefined && $routeParams.Numero !== null) {
            $scope.ListaNostasCredito = [];
            if (parseInt($routeParams.Numero) > CERO) {
                $scope.Numero = parseInt($routeParams.Numero);
                //-- Filtro de busqueda en Get 
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Numero,
                    Estado: -1,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina
                };
                NotasFacturasFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {

                            if (response.data.Datos.length > 0) {
                                $scope.ListaNostasCredito = response.data.Datos;
                                //----------------------------//
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                //-- Filtro de busqueda en Get 
            }
        }

        //-- cargar autocomplete clientes --//
        $scope.ListadoClientes = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoClientes = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoClientes);
                }
            }
            return $scope.ListadoClientes;
        };
        //-- Cargar el combo de oficinas --//
        $scope.ListadoOficinas = [];
        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item)
                        });
                        $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                $scope.Oficina = $scope.ListadoOficinas[0]
            } else {
                ShowError('El usuario no tiene oficinas asociadas')
            }
        }
        //-- Cargar el combo de oficinas --//
        //-- Cargar Prefijo Empresa --//
        EmpresasFactory.Consultar({ Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.pref = response.data.Datos[0].Prefijo;
                }
            });
        //-- Cargar Prefijo Empresa --//
        //-- Funcion Nuevo Documento --//
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarNotaFacturas';
            }
        };
        //-- Lista Clientes --//
        $scope.AsignarListadoClientes = function (value) {
            return RetornarListaAutocomplete(value, $scope.ListadoClientes, 'NombreCompleto', 'NumeroIdentificacion');
        }
        //-- Lista Clientes --//
        //--Conceptos Notas Facturas
        var ResponseConceptosNotas = ConceptoNotasFacturasFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: ESTADO_DEFINITIVO,
            TipoDocumento: -1,
            Sync: true
        });

        $scope.ListadoConceptoNotas = [];
        var CodigoAnulacionNota = 0;
        if (ResponseConceptosNotas.ProcesoExitoso) {
            if (ResponseConceptosNotas.Datos.length > CERO) {
                $scope.ListadoConceptoNotas = ResponseConceptosNotas.Datos;
                for (var i = 0; i < $scope.ListadoConceptoNotas.length; i++) {
                    if ($scope.ListadoConceptoNotas[i].TipoDocumento == CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO && $scope.ListadoConceptoNotas[i].Nombre.indexOf(CONCEPTO_TIPO_NOTA_ANULACION) > -1) {
                        CodigoAnulacionNota = $scope.ListadoConceptoNotas[i].Codigo;
                        break;
                    }
                }
            }
            else {
                $scope.ListadoConceptoNotas = [];
            }
        }
        //--Conceptos Notas Facturas
        //--Cargar Estados Factura Electronica
        $scope.ListadoEstadosFael = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_ESTADOS_FACTURA_ELECTRONICA },
            Sync: true
        }).Datos;
        $scope.ListadoEstadosFael.push({ Nombre: "Pendiente", CampoAuxiliar2: "label label-warning" });

        /// ------------------------------------------------------------- Funcion Buscar Notas Credito ------------------------------------------------------------- ///
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                    Find();
                }
            }
        };

        function Find() {
            $scope.ListaNostasCredito = [];
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.NumeroInicial,
                TipoDocumento: $scope.TipoNota.Codigo,
                FechaInicial: $scope.NumeroInicial > 0 ? undefined : $scope.FechaInicial,
                FechaFinal: $scope.NumeroInicial > 0 ? undefined : $scope.FechaFinal,
                OficinaNota: { Codigo: $scope.Oficina.Codigo },
                Clientes: { Codigo: $scope.Clientes == undefined ? 0 : $scope.Clientes.Codigo },
                Estado: $scope.Estado.Codigo,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
            };
            blockUI.delay = 1000;
            NotasFacturasFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaNostasCredito = response.data.Datos;
                            /*----------------------------*/
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
        };
        /// ------------------------------------------------------------- Funcion Buscar Notas Credito ------------------------------------------------------------- ///
        /// ------------------------------------------------------------- Funcion validar datos requeridos ------------------------------------------------------------- ///
        $scope.FechaInicial = new Date();
        $scope.FechaFinal = new Date();
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicial = new Date();
            $scope.FechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicial === null || $scope.FechaInicial === undefined || $scope.FechaInicial === '')
                && ($scope.FechaFinal === null || $scope.FechaFinal === undefined || $scope.FechaFinal === '')
                && ($scope.NumeroInicial === null || $scope.NumeroInicial === undefined || $scope.NumeroInicial === '' || $scope.NumeroInicial === 0 || isNaN($scope.NumeroInicial) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)
                || ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                || ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')

            ) {
                if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                    && ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')) {
                    if ($scope.FechaFinal < $scope.FechaInicial) {
                        $scope.MensajesError.push('La fecha final debe ser mayor a la fecha inicial');
                        continuar = false;
                    } else if ((($scope.FechaFinal - $scope.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }
                else {
                    if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                        $scope.FechaFinal = $scope.FechaInicial;
                    }
                    else {
                        $scope.FechaInicial = $scope.FechaFinal;
                    }
                }
            }

            return continuar;
        }
        /// ------------------------------------------------------------- Funcion validar datos requeridos ------------------------------------------------------------- ///
        /// ------------------------------------------------------------- Funciones Nota Electronica ------------------------------------------------------------- ///
        $scope.ProcesarNotaElectronica = function (Nota) {
            if (ValidarProcesNotaElectronica(Nota)) {
                switch ($scope.Sesion.Empresa.EmpresaFacturaElectronica.Proveedor.Codigo) {
                    case PROVEEDOR_FACTURA_ELECTRONICA.SAPHETY:
                        var Response = NotasFacturasFactory.ObtenerDatosNotaElectronicaSaphety({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Numero: Nota.Numero,
                            Sync: true
                        });
                        //console.log("Informacion Nota: ", Response.Datos);
                        if (Response.Datos.Numero == 0) {
                            ShowError("La factura que se referencia en esta nota, no se ha emitido electrónicamente");
                        }
                        else {
                            //Bloqueo Pantalla
                            blockUIConfig.autoBlock = true;
                            blockUIConfig.delay = 0;
                            blockUI.start("Reportando Nota...");
                            $timeout(function () {
                                blockUI.message("Reportando Nota...");
                                Gestionar_Nota_Electronica_SAPHETY(Response.Datos);
                            }, 100);
                        }
                        break;
                    default:
                        ShowError("No se ha asignado proveerdor electrónico");
                        break;
                }
            }
        };
        function ValidarProcesNotaElectronica(nota) {
            var continuar = true;
            var txtErrores = "";
            if (nota.Estado == ESTADO_BORRADOR) {
                continuar = false;
                txtErrores += "</br>- La Nota debe estar en estado definitivo";
            }
            if (nota.Anulado == ESTADO_ANULADO) {
                continuar = false;
                txtErrores += "</br>- La Nota no puede estar anulada";
            }
            if (nota.NotaElectronica == ESTADO_DEFINITIVO) {
                continuar = false;
                txtErrores += "</br>- La Nota ya fue emitida electrónicamente";
            }
            if (txtErrores != "") {
                ShowError("Error al reportar la Nota: " + txtErrores);
            }
            return continuar;
        }
        function Gestionar_Nota_Electronica_SAPHETY(Nota) {
            try {
                //--Valida Destinatario
                var Destinatario = "";
                if (Nota.FacturarA.Codigo != undefined && Nota.FacturarA.Codigo != null && Nota.FacturarA.Codigo != '' && Nota.FacturarA.Codigo > 0) {
                    Destinatario = Nota.FacturarA;
                }
                else {
                    Destinatario = Nota.Clientes;
                }
                //--Valida Destinatario
                //-- Obtener Token Saphety
                var TokenObj = Obtener_Token_Saphety();
                //console.log("TOKEN: ", TokenObj);
                if (TokenObj.IsValid == false) {
                    throw "No se pudo generar el token de acceso: " + TokenObj.Errors[0].Code + " " + TokenObj.Errors[0].Description;
                }
                //-- Obtener Token Saphety
                //-- Obtener CUDE Saphety
                var prefijoNota = "";
                switch (Nota.TipoDocumento) {
                    case CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO:
                        prefijoNota = $scope.Sesion.Empresa.EmpresaFacturaElectronica.PrefijoNotaCredito;
                        break;
                    case CODIGO_TIPO_DOCUMENTO_NOTA_DEBITO:
                        prefijoNota = $scope.Sesion.Empresa.EmpresaFacturaElectronica.PrefijoNotaDebito;
                        break;
                }

                var CUDE = Obtener_CUDE_Saphety({
                    "IssueDate": Nota.FechaCreacion,
                    "CustomerDocumentNumber": Destinatario.NumeroIdentificacion,
                    "IssuerNitNumber": Nota.Empresas.NumeroIdentificacion,
                    //"IssuerNitNumber": 830031653,
                    "DocumentNumber": prefijoNota + Nota.NumeroDocumento.toString(),
                    "TotalIva": '0',
                    "TotalInc": '0',
                    "TotalIca": '0',
                    "TotalGrossAmount": Nota.ValorNota.toString(),
                    "TotalPayableAmount": Nota.ValorNota.toString(),
                    "SupplierTimeZoneCode": "America/Bogota"
                }, TokenObj.ResultData);
                if (CUDE.IsValid == false) {
                    throw "No se pudo generar el CUDE: " + CUDE.Errors[0].Code + " " + TokenObj.Errors[0].Description;
                }
                //console.log("CUDE: ", CUDE.ResultData);
                //-- Obtener CUDE Saphety
                //-- Almacena Informacion Parcial de nota electronica(para generar Reporte nota)
                var ResponseAlmacenaNoel = NotasFacturasFactory.GuardarNotaElectronica({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: Nota.Numero,
                    FechaEnvio: Nota.FechaCreacion.toString(),
                    CUDE: CUDE.ResultData,
                    Sync: true
                });
                if (ResponseAlmacenaNoel.ProcesoExitoso != true) {
                    throw "Error al guardar nota electronica :" + ResponseAlmacenaNoel.MensajeOperacion;
                }
                //-- Almacena Informacion Parcial de nota electronica(para generar Reporte nota)
                //--Generar Reporte Factura (PDF)
                Generar_PDF_Nota_Saphety({
                    Empresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NombRepo: NOMBRE_REPORTE_NOTAFACT,
                    Numero: Nota.Numero,
                    OpcionExportarPdf: 1,
                    Prefijo: $scope.pref,
                    TipoExportar: TIPO_REPORTE_FACTURA_GENERA
                });
                //--Generar Reporte Factura (PDF)
                //-- Cargar Nota En Base 64
                var ResponseDocumentoReporte = NotasFacturasFactory.ObtenerDocumentoReporteSaphety({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: Nota.Numero,
                    Sync: true
                });
                //console.log("documento reporte nota: ", ResponseDocumentoReporte);
                if (ResponseDocumentoReporte.ProcesoExitoso != true) {
                    throw "Error al obtener documento reporte nota :" + ResponseDocumentoReporte.MensajeOperacion;
                }
                else {
                    if (ResponseDocumentoReporte.Datos.Archivo == null) {
                        throw "Error al obtener documento reporte nota: No existe reporte nota, contacte con administrador";
                    }
                }
                //-- Cargar Nota En Base 64
                //-- Generar Json Oject para el reporte de nota a saphety
                var JsonNota = GenerarJsonNota(Nota, ResponseDocumentoReporte.Datos.Archivo);
                //-- Generar Json Oject para el reporte de nota a saphety
                //--Envio Factura Electronica
                var RespEnvioPruebas = null;
                var RespEnvioHabilitacion = null;
                var RespEnvioProduccion = null;
                var objArchivo = null;
                switch ($scope.Sesion.Empresa.EmpresaFacturaElectronica.Ambiente.Codigo) {
                    case AMBIENTE_FACTURA_ELECTRONICA.PRUEBAS:
                        RespEnvioPruebas = Emision_Nota_Saphety(JsonNota, Nota.TipoDocumento, TokenObj.ResultData);
                        if (RespEnvioPruebas.IsValid == false) {
                            console.log("Pruebas: ", RespEnvioPruebas);
                            throw "No se pudo generar la emisión - prueba de la nota: " + RespEnvioPruebas.Errors[0].Code;
                        }
                        objArchivo = {
                            Archivo: RespEnvioPruebas.ResultData.Content,
                            TipoArchivo: RespEnvioPruebas.ResultData.ContentType,
                            DecripcionArchivo: "UBL Documento"
                        };
                        break;
                    case AMBIENTE_FACTURA_ELECTRONICA.HABILITACION:
                        RespEnvioHabilitacion = Habilitar_Nota_Saphety(JsonNota, Nota.TipoDocumento, TokenObj.ResultData);
                        if (RespEnvioHabilitacion.IsValid == false) {
                            console.log("Habilitacion: ", RespEnvioHabilitacion);
                            throw "No se pudo generar la habilitación de la nota: " + RespEnvioHabilitacion.Errors[0].Code;
                        }
                        objArchivo = {
                            Archivo: RespEnvioHabilitacion.ResultData.Content,
                            TipoArchivo: RespEnvioHabilitacion.ResultData.ContentType,
                            DecripcionArchivo: "UBL Documento"
                        };
                        break;
                    case AMBIENTE_FACTURA_ELECTRONICA.PRODUCCION:
                        RespEnvioProduccion = Emision_Nota_Saphety(JsonNota, Nota.TipoDocumento, TokenObj.ResultData);
                        if (RespEnvioProduccion.IsValid == false) {
                            console.log("Producción: ", RespEnvioProduccion);
                            throw "No se pudo generar la emisión - Producción de la nota: " + RespEnvioProduccion.Errors[0].Code;
                        }
                        objArchivo = {
                            Archivo: RespEnvioProduccion.ResultData.Content,
                            TipoArchivo: RespEnvioProduccion.ResultData.ContentType,
                            DecripcionArchivo: "UBL Documento"
                        };
                        break;
                }
                //--Envio Factura Electronica
                //--Guarda Informacion Final de nota enviada
                var ResponseActualizaNoel = NotasFacturasFactory.GuardarNotaElectronica({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: Nota.Numero,
                    ID_Pruebas: RespEnvioPruebas == null ? '' : RespEnvioPruebas.ResultData.Id,
                    ID_Habilitacion: RespEnvioHabilitacion == null ? '' : RespEnvioHabilitacion.ResultData.Id,
                    ID_Emision: RespEnvioProduccion == null ? '' : RespEnvioProduccion.ResultData.Id,
                    Archivo: objArchivo == null ? '' : objArchivo.Archivo,
                    TipoArchivo: objArchivo == null ? '' : objArchivo.TipoArchivo,
                    DecripcionArchivo: objArchivo == null ? '' : objArchivo.DecripcionArchivo,
                    NotaElectronica: CODIGO_UNO,
                    Sync: true
                });
                if (ResponseActualizaNoel.ProcesoExitoso != true) {
                    throw "Error al guardar nota electronica :" + ResponseActualizaNoel.MensajeOperacion;
                }
                //--Guarda Informacion Final de nota enviada
                ShowSuccess("Nota Electrónica Generada Exitósamente");
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                Find();
            }
            catch (err) {
                ShowError(err);
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
            }
        }
        //-- Genera JSON Oject para la Nota Factura
        function GenerarJsonNota(Nota, PDFReporte) {
            //--Valida Destinatario
            var Destinatario = "";
            if (Nota.FacturarA.Codigo != undefined && Nota.FacturarA.Codigo != null && Nota.FacturarA.Codigo != '' && Nota.FacturarA.Codigo > 0) {
                Destinatario = Nota.FacturarA;
            }
            else {
                Destinatario = Nota.Clientes;
            }
            //--Valida Destinatario
            var JsonNota = {};
            var PrefixCorrelationId = "";
            JsonNota.SerieNumber = Nota.NumeroDocumento;
            switch (Nota.TipoDocumento) {
                case CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO:
                    PrefixCorrelationId = "NC";
                    JsonNota.SeriePrefix = $scope.Sesion.Empresa.EmpresaFacturaElectronica.PrefijoNotaCredito;
                    JsonNota.SerieExternalKey = $scope.Sesion.Empresa.EmpresaFacturaElectronica.ClaveExternaNotaCredito;
                    if (Nota.ConceptoNotas.Codigo == CodigoAnulacionNota) {
                        //JsonNota.ReasonCredit = "CancellationInvoice";
                        JsonNota.ReasonCredit = "2";
                    }
                    else {
                        //JsonNota.ReasonCredit = "Others";
                        JsonNota.ReasonCredit = "5"; // Anexo Tecnico 1.8 Se cambio 6 por 5=Otros
                    }
                    break;
                case CODIGO_TIPO_DOCUMENTO_NOTA_DEBITO:
                    PrefixCorrelationId = "ND";
                    JsonNota.SeriePrefix = $scope.Sesion.Empresa.EmpresaFacturaElectronica.PrefijoNotaDebito;
                    JsonNota.SerieExternalKey = $scope.Sesion.Empresa.EmpresaFacturaElectronica.ClaveExternaNotaDebito;
                    //JsonNota.ReasonDebit = "1";
                    JsonNota.ReasonDebit = "4";
                    break;
            }
            JsonNota.DeliveryDate = Nota.FechaCreacion.toString();
            JsonNota.IssueDate = Nota.FechaCreacion.toString();
            JsonNota.DueDate = Nota.FechaVence.toString();
            JsonNota.Currency = "COP";
            JsonNota.CorrelationDocumentId = PrefixCorrelationId + JsonNota.SeriePrefix + Nota.NumeroDocumento.toString() + "-" + new Date(Nota.FechaCreacion).getTime();
            JsonNota.PaymentMeans = [{
                Code: "1",
                Mean: "1",
                DueDate: Formatear_Fecha(Nota.FechaVence.toString(), FORMATO_FECHA_yyyy + "-" + FORMATO_FECHA_MM + "-" + FORMATO_FECHA_dd)
            }];
            JsonNota.IssuerParty = {
                DocumentContacts: [
                    {
                        Name: Nota.Empresas.RazonSocial,
                        //Name: 'SOFTTOOLS SAS',
                        Telephone: Nota.Empresas.Telefono,
                        //Telephone: (7446586).toString(),
                        Email: Nota.Empresas.Email,
                        //Email: 'gerencia@softtools.co',
                        Type: "SellerContact"
                    }
                ],
                Identification: {
                    DocumentNumber: Nota.Empresas.NumeroIdentificacion,
                    //DocumentNumber: '830031653',
                    DocumentType: Nota.Empresas.TipoNumeroIdentificacion,
                    CountryCode: "CO",
                    CheckDigit: Nota.Empresas.DigitoChequeo
                    //CheckDigit: '3'
                }
            };
            JsonNota.CustomerParty = {
                DocumentContacts: [
                    {
                        Name: Destinatario.NombreCompleto,
                        Telephone: Destinatario.Telefonos,
                        Email: Destinatario.Correo,
                        Type: "BuyerContact"
                    }
                ],
                LegalType: Destinatario.TipoNaturaleza.Nombre,
                Identification: {
                    DocumentNumber: Destinatario.NumeroIdentificacion,
                    DocumentType: Destinatario.TipoIdentificacion.Nombre,
                    CountryCode: "CO",
                    //CheckDigit: Destinatario.DigitoChequeo.toString()
                },
                Name: Destinatario.NombreCompleto,
                Email: Destinatario.Correo,
                Address: {
                    //DepartmentName: Destinatario.CiudadDireccion.Departamento.Nombre,
                    DepartmentCode: Destinatario.Departamentos.CodigoAlterno.toString(),
                    CityCode: Destinatario.Ciudad.CodigoAlterno.toString(),
                    //CityName: Destinatario.CiudadDireccion.Nombre,
                    AddressLine: Destinatario.Direccion,
                    PostalCode: Destinatario.Ciudad.CodigoPostal.toString(),
                    Country: "CO"
                },
                TaxScheme: "ZA", // Anexo Tecnico 1.8 Cambio 49 por ZA
                Person: {
                    firstName: Destinatario.Nombre,
                    middleName: '',
                    familyName: Destinatario.PrimeroApellido + " " + Destinatario.SegundoApellido
                },
                //ResponsabilityTypes: ["O-06", "O-07", "O-09", "O-14", "O-48", "O-99", "A-29"]
                ResponsabilityTypes: ["R-99-PN"]
            };
            if ((Destinatario.TipoIdentificacion.Nombre).toUpperCase() == 'NIT') {
                JsonNota.CustomerParty.Identification.CheckDigit = Destinatario.DigitoChequeo.toString();
            }

            JsonNota.Lines = [{
                Number: "1",
                Quantity: "1",
                QuantityUnitOfMeasure: "NAR",//--Unidad de medida en el estandar
                TaxSubTotals: [{
                    TaxCategory: NOMBRE_IMPUESTO_IVA,
                    TaxPercentage: "0",
                    TaxableAmount: "0",
                    TaxAmount: "0"
                }],
                TaxTotals: [{ TaxCategory: NOMBRE_IMPUESTO_IVA, TaxAmount: "0" }],
                UnitPrice: Nota.ValorNota.toString(),
                GrossAmount: Nota.ValorNota.toString(),
                NetAmount: Nota.ValorNota.toString(),
                Item: { Description: Nota.ConceptoNotas.Nombre }
            }];
            JsonNota.TaxSubTotals = [{
                TaxCategory: NOMBRE_IMPUESTO_IVA,
                TaxPercentage: "0",
                TaxableAmount: "0",
                TaxAmount: "0"
            }];
            JsonNota.TaxTotals = [{ TaxCategory: NOMBRE_IMPUESTO_IVA, TaxAmount: "0" }];
            JsonNota.Total = {
                GrossAmount: Nota.ValorNota.toString(),
                TotalBillableAmount: Nota.ValorNota.toString(),
                PayableAmount: Nota.ValorNota.toString(),
                TaxableAmount: "0"
            };
            JsonNota.DocumentReferences = [{
                DocumentReferred: Nota.Factura.Prefijo + Nota.Factura.NumeroDocumento.toString(),
                IssueDate: Nota.Factura.FacturaElectronica.FechaCreacion.toString(),
                Type: "InvoiceReference",
                DocumentReferredCUFE: Nota.Factura.FacturaElectronica.CUFE
            }];
            //--Pdf Reporte
            JsonNota.PdfData = { Pdf: PDFReporte };
            //--Pdf Reporte
            //console.log("Nota: ", JSON.stringify(JsonNota));
            return JsonNota;
        }
        //-- Genera JSON Oject para la Nota Factura
        //-- Ejecuta servicio de Obtener token por jquery
        function Obtener_Token_Saphety() {
            var JQDataSend = $.ajax({
                type: "POST",
                url: ObtenerUrlApiSaphety() + API_GET_TOKEN,
                headers: {
                    "Accept": "application / json",
                    'Content-Type': 'application/json'
                },
                dataType: 'json',
                async: false,
                data: JSON.stringify({
                    username: $scope.Sesion.Empresa.EmpresaFacturaElectronica.Usuario,
                    password: $scope.Sesion.Empresa.EmpresaFacturaElectronica.Clave,
                    virtual_operator: $scope.Sesion.Empresa.EmpresaFacturaElectronica.OperadorVirtual
                })
            });
            return JSON.parse(JQDataSend.responseText);
        }
        //-- Ejecuta servicio de Obtener token por jquery
        //-- Ejecuta servicio de Obtener CUDE por jquery
        function Obtener_CUDE_Saphety(configCUDE, TOKEN) {
            var JQDataSend = $.ajax({
                type: "POST",
                url: ObtenerUrlApiSaphety() + API_GET_CUDE.replace(REPLACE_VIRTUAL_OPERATOR, $scope.Sesion.Empresa.EmpresaFacturaElectronica.OperadorVirtual),
                crossDomain: true,
                headers: {
                    "Authorization": TOKEN.token_type + " " + TOKEN.access_token,
                    "Content-Type": "application/json"
                },
                processData: false,
                async: false,
                dataType: 'json',
                data: JSON.stringify(configCUDE),
            });
            return JSON.parse(JQDataSend.responseText);
        }
        //-- Ejecuta servicio de Obtener CUDE por jquery
        //-- Ejecuta servicio de Habilitacion Factura Electronica por jquery
        function Habilitar_Nota_Saphety(JsonNota, TipoNota, TOKEN) {
            var url = "";
            switch (TipoNota) {
                case CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO:
                    url = ObtenerUrlApiSaphety() + API_POST_HABILITA_NOTA_CREDITO.replace(REPLACE_VIRTUAL_OPERATOR, $scope.Sesion.Empresa.EmpresaFacturaElectronica.OperadorVirtual);
                    break;
                case CODIGO_TIPO_DOCUMENTO_NOTA_DEBITO:
                    url = ObtenerUrlApiSaphety() + API_POST_HABILITA_NOTA_DEBITO.replace(REPLACE_VIRTUAL_OPERATOR, $scope.Sesion.Empresa.EmpresaFacturaElectronica.OperadorVirtual);
                    break;
            }
            var JQDataSend = $.ajax({
                type: "POST",
                url: url,
                crossDomain: true,
                headers: {
                    "Authorization": TOKEN.token_type + " " + TOKEN.access_token,
                    "Content-Type": "application/json"
                },
                processData: false,
                async: false,
                dataType: 'json',
                data: JSON.stringify(JsonNota)
            });
            return JSON.parse(JQDataSend.responseText);
        }
        //-- Ejecuta servicio de Habilitacion Factura Electronica por jquery
        //-- Ejecuta servicio de Emision Factura Electronica por jquery
        function Emision_Nota_Saphety(jsonfactura, TipoNota, TOKEN) {
            var url = "";
            switch (TipoNota) {
                case CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO:
                    url = ObtenerUrlApiSaphety() + API_POST_EMISION_NOTA_CREDITO.replace(REPLACE_VIRTUAL_OPERATOR, $scope.Sesion.Empresa.EmpresaFacturaElectronica.OperadorVirtual);
                    break;
                case CODIGO_TIPO_DOCUMENTO_NOTA_DEBITO:
                    url = ObtenerUrlApiSaphety() + API_POST_EMISION_NOTA_DEBITO.replace(REPLACE_VIRTUAL_OPERATOR, $scope.Sesion.Empresa.EmpresaFacturaElectronica.OperadorVirtual);
                    break;
            }
            var JQDataSend = $.ajax({
                type: "POST",
                url: url,
                crossDomain: true,
                headers: {
                    "Authorization": TOKEN.token_type + " " + TOKEN.access_token,
                    "Content-Type": "application/json"
                },
                processData: false,
                async: false,
                dataType: 'json',
                data: JSON.stringify(jsonfactura)
            });
            return JSON.parse(JQDataSend.responseText);
        }
        //-- Ejecuta servicio de Habilitacion Factura Electronica por jquery
        //-- Genera Documento PDF Nota Electronica
        function Generar_PDF_Nota_Saphety(configNotaObj) {
            var ResuDataObje = {
                dataResult: '',
                dataError: ''
            };
            var JQDataSend = $.ajax({
                type: "GET",
                url: $scope.urlASP + '/Reportes/Reportes.aspx',
                async: false,
                data: $.param(configNotaObj),
                success: function (data) {
                    ResuDataObje.dataResult = true;
                },
                error: function (data) {
                    ResuDataObje.dataResult = false;
                    ResuDataObje.dataError = { error: true, descript: "Error " + data.status };
                }
            });
            return ResuDataObje;
        }
        //-- Genera Documento PDF Nota Electronica
        function ObtenerUrlApiSaphety() {
            var url = "";
            switch ($scope.Sesion.Empresa.EmpresaFacturaElectronica.Ambiente.Codigo) {
                case AMBIENTE_FACTURA_ELECTRONICA.PRUEBAS:
                case AMBIENTE_FACTURA_ELECTRONICA.HABILITACION:
                    url = GEN_URL_SAPHETY;
                    break;
                case AMBIENTE_FACTURA_ELECTRONICA.PRODUCCION:
                    url = GEN_URL_SAPHETY_PRODUCCION;
                    break;
            }
            return url;
        }
        //-- Generar Archivo Nota
        $scope.GenerarArchivosNota = function (Nota) {
            switch ($scope.Sesion.Empresa.EmpresaFacturaElectronica.Proveedor.Codigo) {
                case PROVEEDOR_FACTURA_ELECTRONICA.SAPHETY:
                    var Response = NotasFacturasFactory.ObtenerDatosNotaElectronicaSaphety({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Numero: Nota.Numero,
                        Sync: true
                    });
                    //console.log("Informacion Nota: ", Response.Datos);
                    if (Response.Datos.Numero == 0) {
                        ShowError("La factura que se referencia en esta nota, no se ha emitido electrónicamente");
                    }
                    else {
                        //Bloqueo Pantalla
                        blockUIConfig.autoBlock = true;
                        blockUIConfig.delay = 0;
                        blockUI.start("Generando Archivo Nota...");
                        $timeout(function () {
                            blockUI.message("Generando Archivo Nota...");
                            Gestionar_Archivo_Nota_SAPHETY(Response.Datos);
                        }, 100);
                        //Bloqueo Pantalla
                    }
                    break;
                default:
                    ShowError("No se ha asignado proveerdor electrónico");
                    break;
            }
        };
        function Gestionar_Archivo_Nota_SAPHETY(Nota) {
            try {
                //--Generar Reporte Nota (PDF)
                Generar_PDF_Nota_Saphety({
                    Empresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NombRepo: NOMBRE_REPORTE_NOTAFACT,
                    Numero: Nota.Numero,
                    OpcionExportarPdf: 1,
                    Prefijo: $scope.pref,
                    TipoExportar: TIPO_REPORTE_FACTURA_GENERA
                });
                //--Generar Reporte Nota (PDF)
                //-- Cargar Nota En Base 64
                var ResponseDocumentoReporte = NotasFacturasFactory.ObtenerDocumentoReporteSaphety({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: Nota.Numero,
                    Sync: true
                });
                //console.log("documento reporte nota: ", ResponseDocumentoReporte);
                if (ResponseDocumentoReporte.ProcesoExitoso != true) {
                    throw "Error al obtener documento reporte nota :" + ResponseDocumentoReporte.MensajeOperacion;
                }
                else {
                    if (ResponseDocumentoReporte.Datos.Archivo == null) {
                        throw "Error al obtener documento reporte nota: No existe reporte nota, contacte con administrador";
                    }
                }
                //-- Cargar Nota En Base 64
                //-- Generar Json Oject para el reporte de factura a saphety
                showModal('modalArchivoNota');
                var JsonNota = GenerarJsonNota(Nota, ResponseDocumentoReporte.Datos.Archivo);
                $scope.ArchivoNota = JSON.stringify(JsonNota, undefined, 2);
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                //-- Generar Json Oject para el reporte de factura a saphety
            }
            catch (err) {
                ShowError(err);
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
            }
        }
        //-- Generar Archivo Nota
        //--Estados Nota Electronica
        $scope.EstadosNotaElectronica = function (Nota) {
            if (ValidarProcesoEstadosNotaElectronica(Nota)) {
                switch ($scope.Sesion.Empresa.EmpresaFacturaElectronica.Proveedor.Codigo) {
                    case PROVEEDOR_FACTURA_ELECTRONICA.SAPHETY:
                        var Response = NotasFacturasFactory.ObtenerDatosNotaElectronicaSaphety({
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Numero: Nota.Numero,
                            Sync: true
                        });
                        //Bloqueo Pantalla
                        blockUIConfig.autoBlock = true;
                        blockUIConfig.delay = 0;
                        blockUI.start("Generando Archivo Nota...");
                        $timeout(function () {
                            blockUI.message("Generando Archivo Nota...");
                            Gestionar_Estados_Nota_Electronica_SAPHETY(Response.Datos);
                        }, 100);
                        //Bloqueo Pantalla
                        break;
                    default:
                        ShowError("No se ha asignado proveerdor electrónico");
                        break;
                }
            }
        };

        function ValidarProcesoEstadosNotaElectronica(nota) {
            var continuar = true;
            var txtErrores = "";
            if (nota.Estado === ESTADO_BORRADOR) {
                continuar = false;
                txtErrores += "</br>- La Nota debe estar en estado definitivo";
            }
            if (nota.NotaElectronica === ESTADO_INACTIVO) {
                continuar = false;
                txtErrores += "</br>- La Nota debe estar emitida electrónicamente";
            }
            if (txtErrores !== "") {
                ShowError("Error al reportar la Nota: " + txtErrores);
            }
            return continuar;
        }

        function Gestionar_Estados_Nota_Electronica_SAPHETY(Nota) {
            var IdDocumento = "";
            if (Nota.ID_Pruebas != "") {
                IdDocumento = Nota.ID_Pruebas;
            }
            else if (Nota.ID_Habilitacion != "") {
                IdDocumento = Nota.ID_Habilitacion;
            }
            else {
                IdDocumento = Nota.ID_Emision;
            }

            try {
                if (IdDocumento != "") {
                    //-- Obtener Token Saphety
                    var TokenObj = Obtener_Token_Saphety();
                    if (TokenObj.IsValid === false) {
                        throw "No se pudo generar el token de acceso: " + TokenObj.Errors[0].Code + " " + TokenObj.Errors[0].Description;
                    }
                    //-- Obtener Token Saphety
                    var ObjEstado = Obtener_Estados_Saphety(IdDocumento, TokenObj.ResultData);
                    if (ObjEstado.IsValid === false) {
                        throw "No se encontro el estado de la factura";
                    }
                    else {
                        for (var i = 0; i < $scope.ListadoEstadosFael.length; i++) {
                            if ($scope.ListadoEstadosFael[i].CampoAuxiliar2 == ObjEstado.ResultData.DocumentStatus) {
                                $scope.Estados.Adquiente = $scope.ListadoEstadosFael[i];
                            }
                            if ($scope.ListadoEstadosFael[i].CampoAuxiliar2 == ObjEstado.ResultData.MainEmailNotificationStatus) {
                                $scope.Estados.Notificacion = $scope.ListadoEstadosFael[i];
                            }
                        }
                        if ($scope.Estados.Adquiente == '') {
                            $scope.Estados.Adquiente = $scope.ListadoEstadosFael[$scope.ListadoEstadosFael.length - 1];
                        }

                        if ($scope.Estados.Notificacion == '') {
                            $scope.Estados.Notificacion = $scope.ListadoEstadosFael[$scope.ListadoEstadosFael.length - 1];
                        }
                        $scope.Estados.Fecha = new Date(ObjEstado.ResultData.DocumentStatusDate);
                        showModal('modalEstadosNotaElectronica');
                    }

                    blockUI.stop();
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                }
                else {
                    throw "No se encontro el Id del documento de la factura";
                }

            } catch (err) {
                if (err instanceof SyntaxError) {
                    ShowError("El Id del documento no es válido");
                }
                else {
                    ShowError(err);
                }
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
            }
        }

        function Obtener_Estados_Saphety(Id_Dodumento, TOKEN) {
            var JQDataSend = $.ajax({
                type: "GET",
                url: ObtenerUrlApiSaphety() + API_GET_ESTADOS.replace(REPLACE_VIRTUAL_OPERATOR, $scope.Sesion.Empresa.EmpresaFacturaElectronica.OperadorVirtual) + Id_Dodumento,
                crossDomain: true,
                headers: {
                    "Authorization": TOKEN.token_type + " " + TOKEN.access_token,
                    "Content-Type": "application/json"
                },
                processData: false,
                async: false
            });
            return JSON.parse(JQDataSend.responseText);
        }
        //--Estados Nota Electronica
        /// ------------------------------------------------------------- Funciones Nota Electronica ------------------------------------------------------------- ///
        $scope.GenerarMovimientoContable = function (NumeroDocumento, TipoDocumento) {
            EncabezadoComprobantesContablesFactory.Guardar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroDocumento: NumeroDocumento, TipoDocumentoOrigen: { Codigo: TipoDocumento } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('El movimiento contable se regeneró correctamente');
                    } else {
                        ShowError('No se generó el movimiento contable, por favor verifique la parametrización ');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
    }]);