﻿SofttoolsApp.controller("ConsultarLiquidacionGuiasCtrl", ['$scope', '$timeout', 'LiquidacionesFactory', 'TercerosFactory', '$linq', 'blockUI', '$routeParams', 'OficinasFactory', 'EmpresasFactory',
    function ($scope, $timeout, LiquidacionesFactory, TercerosFactory, $linq, blockUI, $routeParams, OficinasFactory, EmpresasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Paqueteria' }, { Nombre: 'Liquidación Planilla Paquetería' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.MostrarMensajeError = false
        $scope.NumeroAnular;
        $scope.NumeroDocumentoAnular;
        $scope.pref = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoOficinas = [];

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + 400105); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarLiquidarPlanillas';
            }
        };
        $scope.ModeloOficina = {}
        $scope.ModeloEstado = {}
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };
        $scope.ListadoDocumentos = [
            { Nombre: 'PLANILLA DESPACHOS', Codigo: 130 },
            { Nombre: 'PLANILLA ENTREGAS', Codigo: 210 },
            { Nombre: 'PLANILLA RECOLECCIONES', Codigo: 205 }
        ]
        $scope.TipoDocuumeto = $scope.ListadoDocumentos[0]

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'BORRADOR', Codigo: CERO },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ]

        $scope.ModeloEstado = $scope.ListadoEstados[CERO]

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinas = response.data.Datos;
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    $scope.ModeloOficina = $scope.ListadoOficinas[$scope.ListadoOficinas.length - 1];
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.ModeloFechaInicial = new Date();
        $scope.ModeloFechaFinal = new Date();
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaInicial = new Date();
            $scope.ModeloFechaFinal = new Date();
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.ModeloFechaInicial === null || $scope.ModeloFechaInicial === undefined || $scope.ModeloFechaInicial === '')
                && ($scope.ModeloFechaFinal === null || $scope.ModeloFechaFinal === undefined || $scope.ModeloFechaFinal === '')
                && ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0)
                || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal
                    }
                }
            }
            return continuar
        }


        function Find() {
            if (DatosRequeridos()) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: $scope.ModeloNumero,
                    FechaInicial: $scope.ModeloFechaInicial,
                    FechaFinal: $scope.ModeloFechaFinal,
                    NumeroPlanilla: $scope.ModeloPlanilla,
                    NumeroManifiesto: $scope.ModeloManifiesto,
                    PlacaVehiculo: $scope.ModeloPlaca,
                    Conductor: { Nombre: $scope.ModeloConductor },
                    Tenedor: { Nombre: $scope.ModeloTenedor },
                    Oficina: $scope.ModeloOficina,
                    Estado: $scope.ModeloEstado.Codigo,
                    Pagina: $scope.paginaActual,
                    Numero: 0,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    Aprobado: -1,
                    TipoDocumento: { Codigo: $scope.TipoDocuumeto.Codigo }
                };

                blockUI.start('Buscando registros ...');
                $timeout(function () {
                    blockUI.message('Espere por favor ...');
                }, 100);
                $scope.Buscando = true;
                $scope.MensajesError = [];
                $scope.ListadoLiquidaciones = [];
                if ($scope.Buscando) {
                    if ($scope.MensajesError.length === 0) {
                        blockUI.delay = 1000;
                        $scope.Pagina = $scope.paginaActual
                        $scope.RegistrosPagina = 10
                        LiquidacionesFactory.Consultar(filtros).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    $scope.Numero = 0
                                    if (response.data.Datos.length > 0) {
                                        $scope.ListadoLiquidaciones = response.data.Datos
                                        $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';
                                    }
                                    else {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                    var listado = [];
                                    response.data.Datos.forEach(function (item) {
                                        listado.push(item);
                                    });
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }
                blockUI.stop();
            }
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskValoresGrid = function (valor) {
            return MascaraValores(valor)
        };
        $scope.Anular = function (Numero, NumeroDocumento, Anulado) {
            $scope.MensajesErrorAnula = [];
            $scope.ModeloCausaAnula = '';
            $scope.NumeroAnular = Numero;
            $scope.NumeroDocumentoAnular = NumeroDocumento;
            if (Anulado === 0) {
                showModal('modalAnular')
            } else {
                ShowError('La liquidación ' + NumeroDocumento + ' ya se encuentra anulada')
            }
        }

        $scope.ConfirmaAnular = function () {
            if ($scope.ModeloCausaAnula !== undefined && $scope.ModeloCausaAnula !== null && $scope.ModeloCausaAnula !== '') {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.NumeroAnular,
                    UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CausaAnula: $scope.ModeloCausaAnula
                }
                LiquidacionesFactory.Anular(filtros).
                    then(function (response) {
                        closeModal('modalAnular')
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.ProcesoExitoso === true) {
                                ShowSuccess('Liquidación anulada');
                                Find();
                            }
                            else {
                                ShowError('Se presentó un error al anular la liquidación')
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                $scope.MensajesErrorAnula.push('Debe ingresar la causa de anulación');
            }
        }

        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_LIQUIDACION_PLANILLA_DESPACHO;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroDocumento;
            }

        }
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            if ($routeParams.Numero > 0 && $routeParams.TIDO_Codigo > 0) {
                $scope.TipoDocuumeto = $linq.Enumerable().From($scope.ListadoDocumentos).First('$.Codigo ==' + $routeParams.TIDO_Codigo);
                $scope.ModeloNumero = $routeParams.Numero;
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                $scope.ModeloOficina = $scope.ListadoOficinas[$scope.ListadoOficinas.length - 1];
                Find();
            } 
        } 

    }]);