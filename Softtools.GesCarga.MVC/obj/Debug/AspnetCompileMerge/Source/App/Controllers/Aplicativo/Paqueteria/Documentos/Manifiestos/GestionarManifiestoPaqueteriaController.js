﻿SofttoolsApp.controller("GestionarManifiestoPaqueteriaCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'RutasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'CiudadesFactory', 'VehiculosFactory', 'ManifiestoFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, RutasFactory, ValorCatalogosFactory, TercerosFactory, CiudadesFactory, VehiculosFactory, ManifiestoFactory) {

        $scope.Titulo = 'GESTIONAR MANIFIESTO';
        $scope.MapaSitio = $scope.MapaSitio = [{ Nombre: 'Paqueteria' }, { Nombre: 'Documentos' }, { Nombre: 'Manifiesto' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_MANIFIESTO_CARGA);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.Modelo = {
            Numero: '',
            NumeroDocumento: '',
            NumeroElectronico: '',
            Fecha: '',
            PlacaVehiculo: '',
            PlacaSemiremolque: '',
            Tenedor: '',
            IdentificacionConductor: '',
            NombreConductor: '',
            TelefonoConductor: '',
            Ruta: '',
            Observaciones: '',
            FechaInicio: '',
            HoraInicio: '',
            FechaCumplimiento: '',
            Cantidad: '',
            Peso: '',
            Flete: '',
            ReteFuente: '',
            ReteIca: '',
            Descuentos: '',
            ValorAnticipo: '',
            ValorPagar: '',
            ListadoRemesas: []
        };

        $scope.Calcular = function () {

        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/

        if ($routeParams.Numero > 0) {
            $scope.Modelo.Numero = $routeParams.Numero;
            Obtener();
            $scope.Titulo = 'CONSULTAR MANIFIESTO CARGA';
        }
        else {
            $scope.Modelo.Numero = 0;
        };

        function Obtener() {
            blockUI.start('Cargando Manifiesto Carga...');

            $timeout(function () {
                blockUI.message('Cargando Manifiesto Carga...');
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
                Obtener: 1
            };

            blockUI.delay = 1000;
            ManifiestoFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Numero = response.data.Datos.Numero;
                        $scope.Modelo.NumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.Modelo.Planilla = response.data.Datos.Planilla;
                        $scope.Modelo.NumeroElectronico = response.data.Datos.Numero_Manifiesto_Electronico;
                        $scope.Modelo.Fecha = response.data.Datos.Fecha;
                        $scope.Modelo.PlacaVehiculo = response.data.Datos.Vehiculo.Placa;
                        $scope.Modelo.PlacaSemiremolque = response.data.Datos.Semirremolque.Placa;
                        $scope.Modelo.Tenedor = response.data.Datos.Tenedor.NombreCompleto;

                        $scope.Modelo.IdentificacionConductor = response.data.Datos.Conductor.NumeroIdentificacion;
                        $scope.Modelo.NombreConductor = response.data.Datos.Conductor.NombreCompleto;
                        $scope.Modelo.TelefonoConductor = response.data.Datos.Conductor.Telefonos;
                        $scope.Modelo.Ruta = response.data.Datos.Ruta.Nombre;
                        $scope.Modelo.Observaciones = response.data.Datos.Observaciones;

                        $scope.Modelo.FechaInicio = new Date(response.data.Datos.Fecha);
                        //$scope.Modelo.HoraInicio = response.data.Datos.Tenedor.NombreCompleto;

                        $scope.Modelo.FechaCumplimiento = response.data.Datos.Fecha_Cumplimiento;
                        $scope.Modelo.Cantidad = MascaraValores(response.data.Datos.Cantidad_Total);
                        $scope.Modelo.Peso = MascaraValores(response.data.Datos.Peso_Total);
                        $scope.Modelo.Flete = $scope.MaskMoneda(response.data.Datos.Valor_Flete);
                        $scope.Modelo.ReteFuente = $scope.MaskMoneda(response.data.Datos.Valor_Retencion_Fuente);
                        $scope.Modelo.ReteIca = $scope.MaskMoneda(response.data.Datos.Valor_ICA);
                        $scope.Modelo.Descuentos = $scope.MaskMoneda(response.data.Datos.Valor_Otros_Descuentos);
                        $scope.Modelo.ValorAnticipo = $scope.MaskMoneda(response.data.Datos.Valor_Anticipo);
                        $scope.Modelo.ValorPagar = $scope.MaskMoneda(response.data.Datos.Valor_Pagar);
                        $scope.Modelo.ListadoRemesas = response.data.Datos.ListaDetalleManifiesto;
                        $scope.MaskValores()
                    }

                    else {
                        ShowError('No se logro consultar el manifiesto número ' + $scope.Modelo.Documento + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ManifiestosCargaPaqueteria/' + $scope.Modelo.NumeroDocumento;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ManifiestosCargaPaqueteria/' + $scope.Modelo.NumeroDocumento;
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var modelo = $scope.Modelo
            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ManifiestosCargaPaqueteria/' + $scope.Modelo.NumeroDocumento;
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };

        $scope.MaskValores = function () {
            MascaraValores($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMoneda = function (item) {
            return MascaraValores(item)
        };

        /* Obtener parametros*/
        try {
            var Parametros = $routeParams.Codigo.split(',')
            if (Parametros.length > 1) {
                $scope.Modelo.Numero = Parametros[0];
                $scope.aplicabase = true
                Obtener();
            } else {
                if ($routeParams.Codigo > 0) {
                    $scope.Modelo.Numero = $routeParams.Codigo;
                    Obtener();
                }
                else {
                    $scope.Modelo.Numero = 0;
                }

            }
        } catch (e) {
            if ($routeParams.Codigo > 0) {
                $scope.Modelo.Numero = $routeParams.Codigo;
                Obtener();
            }
            else {
                $scope.Modelo.Numero = 0;
            }

        }
    }]);