﻿SofttoolsApp.controller("ConsultarRecoleccionesCtrl", ['$scope', '$timeout', '$linq', 'blockUI', '$routeParams', 'RecoleccionesFactory', 'OficinasFactory', 'ZonasFactory', 'blockUIConfig', 'VehiculosFactory','PlanillaDespachosFactory',
    function ($scope, $timeout, $linq, blockUI, $routeParams, RecoleccionesFactory, OficinasFactory, ZonasFactory, blockUIConfig, VehiculosFactory, PlanillaDespachosFactory) {

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Recolecciones' }];
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListaEstadoProgramacion = [];
        $scope.Numero = 0
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.ListaRecorridosConsultados = [];
        $scope.HabilitarFecha = false;
        $scope.FechaInicio = new Date();
        $scope.FechaFin = new Date();
        $scope.paginaActualModalPlanillas = 1;
        $scope.cantidadRegistrosPorPaginaModalPlanillas = 10;
        try {
            try {
                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_RECOLECCIONES);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

            } catch (e) {

                $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_RECOLECCIONES_GESPHONE);
                $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
                $scope.DeshabilitarEliminarAnular = true                
                $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
                $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
            }
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        $scope.HabilitarFecha = false

    /*--------COMBOS Y AUTOCOMPLETE------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ListaVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, Sync: true })
                    $scope.ListaVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListaVehiculos)
                }
            }
            return $scope.ListaVehiculos
        }

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'DEFINITIVA', Codigo: 1 },
            { Nombre: 'PLANILLADA', Codigo: 2 },
            { Nombre: 'ANULADA', Codigo: 3 }
        ];

        $scope.ModeloEstado = $scope.ListadoEstados[0]  

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: -1, CodigoCiudad: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad.Codigo }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinasCiudad = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoOficinasCiudad.push({ Nombre: '(TODAS)', Codigo: 0 })
                        response.data.Datos.forEach(function (item) {
                            if (item.Codigo !== 0) {
                                $scope.ListadoOficinasCiudad.push(item)
                            }
                        })
                        $scope.ModeloOficinasCiudad = $scope.ListadoOficinasCiudad[0]
                    }
                    else {
                        $scope.ModeloOficinasCiudad = $scope.ListadoOficinasCiudad[0]
                    }
                }
            }, function (response) {
            });
                       
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
       // $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarRecolecciones';
            }
        };

        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

        /*Metodo buscar*/
        $scope.Buscar = function () {
            if (DatosRequeridos()) {
                if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                    Find()
                }
            }
        };

        /* Obtener parametros*/
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            if ($routeParams.Numero > 0) {
                $scope.ModeloOficinasCiudad = { Codigo: 0};
                $scope.NumeroDocumento = $routeParams.Numero;
                Find();
                $scope.NumeroDocumento = '';
            }
        }

        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);            
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoRecolecciones = []

            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Numero,         
                NumeroDocumento: $scope.NumeroDocumento,  
                Oficinas: $scope.ModeloOficinasCiudad,
                Ciudad: $scope.Sesion.UsuarioAutenticado.Oficinas.Ciudad,
                NombreZona: $scope.ModeloZonas,
                FechaInicio: $scope.FechaInicio,
                FechaFin: $scope.FechaFin,
                NombreCliente: $scope.ModeloCliente,
                NombreRemitente: $scope.ModeloRemitente,
                Estado: $scope.ModeloEstado.Codigo,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina
            }            
                blockUI.delay = 1000;
                RecoleccionesFactory.Consultar(Filtro).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoRecolecciones = response.data.Datos

                                for (var i = 0; i < $scope.ListadoRecolecciones.length; i++) {
                                    if (new Date($scope.ListadoRecolecciones[i].FechaPlanilla) < MIN_DATE) {
                                        $scope.ListadoRecolecciones[i].FechaPlanilla = ''
                                    }
                                }
                                for (var j = 0; j < $scope.ListadoRecolecciones.length; j++) {
                                    if ($scope.ListadoRecolecciones[j].Peso === 0) {
                                        $scope.ListadoRecolecciones[j].Peso = ''
                                    }
                                }
                                for (var k = 0; k < $scope.ListadoRecolecciones.length; k++) {
                                    if ($scope.ListadoRecolecciones[k].NumeroPlanilla === 0) {
                                        $scope.ListadoRecolecciones[k].NumeroPlanilla = ''
                                    }
                                }
                                for (var l = 0; l < $scope.ListadoRecolecciones.length; l++) {
                                    if ($scope.ListadoRecolecciones[l].Cliente.Codigo === 0) {
                                        $scope.ListadoRecolecciones[l].Cliente.Nombre = ''
                                    }
                                }
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.ListadoRecolecciones = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            blockUI.stop();
        };
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.NumeroDocumento === null || $scope.NumeroDocumento === undefined || $scope.NumeroDocumento === '' || $scope.NumeroDocumento === 0 || isNaN($scope.NumeroDocumento) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.NumeroDocumento !== null && $scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '' && $scope.NumeroDocumento !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO+' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            return continuar
        } 

        /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/
        $scope.EliminarRecoleccion = function (numero, nombreMercancia,Documento) {
            $scope.CodigoNumero = numero
            $scope.NumeroDocumento = Documento
            $scope.NombreMercancia = nombreMercancia
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarRecolecciones');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.CodigoNumero,
            };

            RecoleccionesFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se anulo la recolección ' + $scope.NumeroDocumento + ' - ' + $scope.NombreMercancia);
                        closeModal('modalEliminarRecolecciones');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarRecolecciones');
                    $scope.ModalError = 'No se puede anular la recolección de ' + $scope.NombreMercancia + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };

      
        $scope.CerrarModal = function () {
            closeModal('modalEliminarRecolecciones');
            closeModal('modalMensajeEliminarRecolecciones');
        }

    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.AdicionarPlanillaRecoleccion = function (item) {
            $scope.ModalPlanillas = {
                Fecha: new Date(),
                Numero: '',
                Vehiculo: ''
            }
            $scope.RecoleccionActual = item;
            showModal('modalAdicionarPlanilla')

        }
     
    
        function DatosRequeridosModalPlanillas() {
            $scope.MensajesErrorModalPlanillas = [];
            var continuar = true
            if ($scope.ModalPlanillas != undefined) {
                if ($scope.ModalPlanillas.Fecha == undefined || $scope.ModalPlanillas.Fecha == null || $scope.ModalPlanillas.Fecha == '') {
                    $scope.MensajesErrorModalPlanillas.push('Debe seleccionar una fecha');
                    continuar = false
                }
            }
            return continuar
        }

        $scope.BuscarModalPlanillas = function () {
            $scope.ListadoGuias = [];
            if (DatosRequeridosModalPlanillas()) {
                blockUI.start('Buscando registros ...');
                $timeout(function () {
                    blockUI.message('Espere por favor ...');
                }, 100);


                var filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    FechaInicial: $scope.ModalPlanillas.Fecha,
                    FechaFinal: $scope.ModalPlanillas.Fecha,
                    //NumeroInicial: $scope.ModeloPlanillaDespacho,
                    //NumeroFinal: $scope.ModeloPlanillaDespacho,
                    //Numero: $scope.ModeloPlanillaDespacho,
                    //Remesa: {
                    //    //    Cliente: { Codigo: $scope.ModeloCliente.Codigo },
                    //    NumeroplanillaDespacho: $scope.ModalPlanillas.NumeroDocumentoPlanilla == undefined || $scope.ModalPlanillas.NumeroDocumentoPlanilla == null || $scope.ModalPlanillas.NumeroDocumentoPlanilla == '' ? 0 : MascaraNumero($scope.ModalPlanillas.NumeroDocumentoPlanilla) ,
                    //    //    Numeroplanillarecoleccion: $scope.ModeloPlanillaRecogida
                    //},
                    Numero: $scope.ModalPlanillas.NumeroDocumentoPlanilla == undefined || $scope.ModalPlanillas.NumeroDocumentoPlanilla == null || $scope.ModalPlanillas.NumeroDocumentoPlanilla == '' ? 0 : MascaraNumero($scope.ModalPlanillas.NumeroDocumentoPlanilla),
                    //OficinaActual: { Codigo: $scope.ModeloOficinaActual.Codigo },
                    //OficinaOrigen: { Codigo: $scope.ModeloOficinaOrigen.Codigo },

                    ////actualizaestado: PERMISO_ACTIVO,
                    //CodigoCiudad: $scope.ModeloCiudadDescargue.Codigo,
                    //Ruta: { Codigo: $scope.ModeloCiudadCargue == undefined || $scope.ModeloCiudadCargue == null || $scope.ModeloCiudadCargue == '' ? 0 : $scope.ModeloCiudadCargue.Codigo },
                    Vehiculo: { Codigo: $scope.ModalPlanillas.Vehiculo == undefined || $scope.ModalPlanillas.Vehiculo == null || $scope.ModalPlanillas.Vehiculo == '' ? 0 : $scope.ModalPlanillas.Vehiculo.Codigo },
                    // Conductor: { Codigo: $scope.ModeloCOnductor == undefined || $scope.ModeloCOnductor == null || $scope.ModeloCOnductor == '' ? 0 : $scope.ModeloCOnductor.Codigo },
                    Estado: { Codigo: 1 },
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_RECOLECCION,
                    NumeroPlanilla: -1,
                    Anulado: -1,
                    CodigoOficinaActualUsuario: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    RecibirOficina: true
                }

                $scope.Buscando = true;
                $scope.MensajesError = [];
                $scope.ListadoGuias = [];
                if ($scope.Buscando) {
                    if ($scope.MensajesError.length == 0) {
                        blockUI.delay = 1000;

                        PlanillaDespachosFactory.Consultar(filtro).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.length > 0)
                                        $scope.ListadoGuias = [];

                                    $scope.ListadoAuxiliar = [];
                                    $scope.ListadoAuxiliar = response.data.Datos;
                                    $scope.paginaActualModalPlanillas = 1;


                                    //Llena la lista $scope.ListadoRecorridosTotal para acumular los recorridos consultados con la factura y los nuevos a traer
                                    if ($scope.ListadoGuias.length > 0) {
                                        //Datos consultados
                                        $scope.ListadoRecorridosTotal = [];



                                        //Datos nuevos


                                        for (var i = 0; i < $scope.ListadoAuxiliar.length; i++) {
                                            var existe = true;

                                            for (var j = 0; j < $scope.ListadoRecorridosTotal.length; j++) {
                                                if ($scope.ListadoAuxiliar[i].Remesa.Numero == $scope.ListadoRecorridosTotal[j].Remesa.Numero) {
                                                    existe = false;
                                                    $scope.registrorepetido += 1;
                                                    break;
                                                }
                                            }

                                            if (existe == true) {
                                                $scope.ListadoRecorridosTotal.push($scope.ListadoAuxiliar[i]);
                                            }

                                        }

                                    } else {
                                        $scope.ListadoRecorridosTotal = $scope.ListadoAuxiliar;


                                    }

                                    $scope.registrorepetido = 0;
                                    /*se ordena el listado del Grid*/
                                    if ($scope.ListadoRecorridosTotal.length > 0) {
                                        $scope.ListadoRecorridosTotal = $linq.Enumerable().From($scope.ListadoRecorridosTotal).OrderBy(function (x) {
                                            return x.Numero
                                        }).ToArray();
                                    }

                                    /*----------------------------*/

                                    //Se muestran la cantidad de recorridos por pagina
                                    //En este punto se muestra siempre desde la primera pagina
                                    $scope.ListadoRecorridosTotal.forEach(function (item) {
                                        item.Seleccionado = $scope.seleccionarCheck;
                                    });

                                    var i = 0;
                                    for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                                        if (i <= $scope.cantidadRegistrosPorPaginaModalPlanillas - 1) {
                                            $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                                        }
                                    }

                                    //Se operan los totales de registros y de paginas
                                    if ($scope.ListadoRecorridosTotal.length > 0) {
                                        $scope.totalRegistrosModalPlanillas = $scope.ListadoRecorridosTotal.length;
                                        $scope.totalPaginasModalPlanillas = Math.ceil($scope.ListadoRecorridosTotal.length / $scope.cantidadRegistrosPorPaginaModalPlanillas);
                                        $scope.BuscandoModalPlanillasModalPlanillas = false;
                                        $scope.ResultadoSinRegistrosModalPlanillas = '';

                                    }
                                    else {
                                        $scope.totalRegistrosModalPlanillas = 0;
                                        $scope.totalPaginasModalPlanillas = 0;
                                        $scope.paginaActualModalPlanillas = 1;
                                        $scope.ResultadoSinRegistrosModalPlanillas = 'No hay datos para mostrar';
                                        $scope.BuscandoModalPlanillas = false;
                                    }


                                }
                            }, function (response) {
                                    $scope.BuscandoModalPlanillas = false;
                            });
                    }
                    else {
                        $scope.BuscandoModalPlanillas = false;
                    }
                    blockUI.stop();
                }
            }
        }
        $scope.ValidarSeleccion = function (item) {
            if (item.Seleccionado == true) {
               // $scope.MostrarSeccionRecolecciones = true
                $scope.Planilla = item.Numero;
                $scope.ListadoGuias.forEach(item1 => {
                    if (item1.Numero != item.Numero) {
                        item1.Seleccionado = false;
                    }
                });
            } else {
                //$scope.MostrarSeccionRecolecciones = false

            }
        }


        $scope.PrimerPaginaModalPlanillas = function () {
            /*Verificar si la página actual ya está guardada si no lo esta procede a realizar la busqueda*/
            if ($scope.paginaActualModalPlanillas > 1) {

                
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActualModalPlanillas = 1;
                        $scope.ListadoGuiasModalPlanillas = [];

                        var i = 0;
                        for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPaginaModalPlanillas) {
                                $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                            }
                        }
                    }
                

            }


        };

        $scope.SiguienteModalPlanillas = function () {

            if ($scope.paginaActualModalPlanillas < $scope.totalPaginasModalPlanillas) {
                $scope.ListadoGuias = [];
                $scope.PaginaAuxiliar = $scope.paginaActualModalPlanillas;
                $scope.paginaActualModalPlanillas += 1;

                
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPaginaModalPlanillas * $scope.PaginaAuxiliar); i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPaginaModalPlanillas * $scope.paginaActualModalPlanillas) {
                                $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i]);
                            }
                        }
                    }
                
            }

        }

        $scope.AnteriorModalPlanillas = function () {
            if ($scope.paginaActualModalPlanillas > 1) {

                
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.ListadoGuias = [];
                        $scope.paginaActualModalPlanillas -= 1;

                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPaginaModalPlanillas * $scope.paginaActualModalPlanillas) - ($scope.cantidadRegistrosPorPaginaModalPlanillas); i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPaginaModalPlanillas * $scope.paginaActualModalPlanillas) {
                                $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i]);
                            }
                        }
                    }
                

            }
        };

        $scope.UltimaPaginaModalPlanillas = function () {

            if ($scope.paginaActualModalPlanillas < $scope.totalPaginasModalPlanillas) {

                if ($scope.ListadoRecorridosTotal.length > 0) {
                    $scope.paginaActualModalPlanillas = $scope.totalPaginasModalPlanillas;
                    $scope.ListadoGuias = [];

                    var i = 0;
                    for (i = ($scope.paginaActualModalPlanillas * $scope.cantidadRegistrosPorPaginaModalPlanillas) - $scope.cantidadRegistrosPorPaginaModalPlanillas; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPaginaModalPlanillas * $scope.paginaActualModalPlanillas) {
                            $scope.ListadoGuias.push($scope.ListadoRecorridosTotal[i])
                        }
                    }
                }
                

            }
        }

        $scope.AdicionarPlanilla = function () {
           
            
            ShowWarningConfirm('¿Esta seguro de asociar la recolección a la planilla seleccionada?', $scope.AsociarRecoleccionesPlanilla);
                
        }

        $scope.AsociarRecoleccionesPlanilla = function () {
            var Entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Recolecciones: [],
                NumeroPlanilla: $scope.Planilla,
                TipoDocumento: 135 //Planilla Paquetería
            }

            //$scope.Recoleccion.Pagina = $scope.paginaActualRecoleccion;
            //$scope.Recoleccion.RegistrosPagina = 0
            //$scope.Recoleccion.FechaInicio = $scope.Modelo.FechaInicial;
            //$scope.Recoleccion.FechaFin = $scope.Modelo.FechaFinal;
            //$scope.Recoleccion.Estado = 1;
            //$scope.Recoleccion.AsociarRecoleccionPlanilla = 1;
            //$scope.Recoleccion.Sync = true;
            //try {
            //    $scope.ListaResultadoRecolecciones = RecoleccionesFactory.Consultar($scope.Recoleccion).Datos
            //} catch (e) {
            //    console.log(e);
            //}
            var Coincidencias = 0;
            var guardar = true;

            //for (var i = 0; i < $scope.ListadoRecoleccionesGeneral.length; i++) {
            //    if ($scope.ListadoRecoleccionesGeneral[i].Marcado) {

            //        Coincidencias = 0;
            //        $scope.ListaResultadoRecolecciones.forEach(recoleccion => {
            //            if ($scope.ListadoRecoleccionesGeneral[i].Numero == recoleccion.Numero) {
                            Entidad.Recolecciones.push({
                                //Numero: $scope.ListadoRecoleccionesGeneral[i].Numero,
                                //NumeroDocumento: $scope.ListadoRecoleccionesGeneral[i].NumeroDocumento,
                                Numero: $scope.RecoleccionActual.Numero,
                                NumeroDocumento: $scope.RecoleccionActual.NumeroDocumento,
                            })
                    //        Coincidencias++;
                    //    }

                    //});
                    //if (Coincidencias == 0) {
                    //    $scope.ListadoRecoleccionesGeneral[i].backGround = 'yellow';
                    //    guardar = false;
                    //}

               // }
            //}
            //$scope.PrimerPaginaRecoleccion();
            if (guardar) {
                RecoleccionesFactory.AsociarRecoleccionesPlanilla(Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Las recolecciones se asociaron correctamente a la planilla')
                            closeModal('modalAdicionarPlanilla');
                            Find();
                        }

                    }, function (response) {
                        ShowError(response.statusText);
                    });

            } else {
                ShowError('las recolecciones seleccionadas ya se encuentran asociadas, por favor recargue la página');
            }
        }

        $scope.CancelarRecoleccion = function (item) {
            $scope.item = item
            ShowConfirm('Desea cancelar esta recolección?',Cancelar)
        }

        function Cancelar(){
            if ($scope.item.Numero != undefined) {
                RecoleccionesFactory.Cancelar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: $scope.item.Numero}).
                    then(function (response){
                        if (response.data.ProcesoExitoso == true) {
                            
                                ShowSuccess('se canceló la recolección satisfactoriamente')
                                Find();
                            
                        }
                    
                    });
            }
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };

    }]);