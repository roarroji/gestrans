﻿SofttoolsApp.controller("ConsultarIndicadorEntregasCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUIConfig', 'TercerosFactory', 'SitiosCargueDescargueFactory', 'RemesaGuiasFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUIConfig, TercerosFactory, SitiosCargueDescargueFactory, RemesaGuiasFactory) {

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Indicadores' }, { Nombre: 'Tiempo Entregas' }];
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.ListadoConsulta = [];
        $scope.ListadoTiposGrafica = [];
        $scope.MensajesError = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
        }
        var IngresoSitio = 0;
        //--------------Parametros URL
        if ($routeParams !== undefined && $routeParams.Codigo !== null) {
            IngresoSitio = parseInt($routeParams.Codigo);
        }

        switch (IngresoSitio) {
            case CODIGO_APLICACION_PORTAL:
                try {
                    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_INDICADORES_ENTREGAS_PORTAL);
                    if ($scope.Sesion.UsuarioAutenticado.Cliente.Codigo > 0 && $scope.Sesion.UsuarioAutenticado.Cliente.Codigo != undefined && $scope.Sesion.UsuarioAutenticado.Cliente.Codigo != '') {
                        $scope.Modelo.Cliente = $scope.CargarTercero($scope.Sesion.UsuarioAutenticado.Cliente.Codigo);
                        $scope.DeshabilitarCliente = true;
                        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
                        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
                        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
                        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
                    }
                    else {
                        if ($scope.Sesion.UsuarioAutenticado.Codigo > 0) {
                            ShowError('El usuario ingresado no tiene clientes asociados');
                            $scope.DeshabilitarConsulta = true;
                            $scope.DeshabilitarEliminarAnular = true;
                            $scope.DeshabilitarImprimir = true;
                            $scope.DeshabilitarActualizar = true;

                        }
                    }
                }
                catch (e) {
                    ShowError('No tiene permiso para visualizar esta pagina');
                    document.location.href = '#';
                }
                break;
            default:
                try {
                    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_INDICADORES_ENTREGAS);
                    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

                    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
                    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
                    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
                    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
                }
                catch (e) {
                    ShowError('No tiene permiso para visualizar esta pagina');
                    document.location.href = '#';
                }
                break;
        }
        //-- Consulta Clientes --//
        $scope.ListadoCliente = []
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente)
                }
            }
            return $scope.ListadoCliente
        }
        //-- Consulta Destinatarios --//
        $scope.ListadoDestinatarios = []
        $scope.AutocompleteDestinatario = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_DESTINATARIO,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoDestinatarios = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoDestinatarios)
                }
            }
            return $scope.ListadoDestinatarios
        }
        //-- Consulta Remitentes --//
        $scope.ListadoRemitente = []
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_REMITENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoRemitente)
                }
            }
            return $scope.ListadoRemitente
        }
        //-- Consulta Remitentes --//
        $scope.ListadoSitiosDescargue = []
        $scope.AutocompleteSitiosDescargue = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosCargueDescargueFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Estado: ESTADO_ACTIVO,
                        Sync: true
                    })
                    $scope.ListadoSitiosDescargue = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosDescargue)
                }
            }
            return $scope.ListadoSitiosDescargue
        }
        $scope.ListadoTiposGrafica = [
            { Nombre: 'Circular', Codigo: 2 },
            { Nombre: 'Barras Verticales', Codigo: 0 },
            { Nombre: 'Barras Horizontales', Codigo: 1 },
            { Nombre: 'Linea', Codigo: 3 },
        ];
        $scope.Modelo.TipoGrafica = $scope.ListadoTiposGrafica[0]


        $scope.BuscarGrafica = function () {
        }




        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        function Find() {

            $scope.MensajesError = [];
            $scope.ListadoConsulta = [];
            if (DatosRequeridos()) {
                BloqueoPantalla.start('Buscando registros ...');
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        FechaInicial: $scope.Modelo.FechaInicial,
                        FechaFinal: $scope.Modelo.FechaFinal,
                        Remesa: {
                            Cliente: $scope.Modelo.Cliente,
                            Remitente: $scope.Modelo.Remitente,
                            Destinatario: $scope.Modelo.Destinatario,
                        },
                        SitioDescargue: $scope.Modelo.SitioDescargue,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    }

                    RemesaGuiasFactory.ConsultarIndicadores(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoConsulta = response.data.Datos
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.ResultadoSinRegistros = '';
                                    ConsultarGrafica()
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                            BloqueoPantalla.stop();
                        }, function (response) {
                            ShowError(response.statusText);
                            BloqueoPantalla.stop();
                        });
                }
            }
        }

        $scope.ValidarFechas = function () {
            if ($scope.Modelo.Tipo.Codigo == 0) {
                $scope.DeshabilitarFechas = false
            }
            else {
                $scope.DeshabilitarFechas = true
            }
        }

        function ConsultarGrafica() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                FechaInicial: $scope.Modelo.FechaInicial,
                FechaFinal: $scope.Modelo.FechaFinal,
                Remesa: {
                    Cliente: $scope.Modelo.Cliente,
                    Remitente: $scope.Modelo.Remitente,
                    Destinatario: $scope.Modelo.Destinatario,
                },
                SitioDescargue: $scope.Modelo.SitioDescargue,
                InidicadorEntregas: 1,
            }

            RemesaGuiasFactory.ConsultarIndicadores(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            Graficar(response.data.Datos)
                        }
                    }
                    BloqueoPantalla.stop();
                }, function (response) {
                    ShowError(response.statusText);
                    BloqueoPantalla.stop();
                });
        }


        //inicializacion de divisiones
        $('#Grafica').show(); $('#ConsultaViajes').hide();


        $scope.MostrarGrafica = function () {
            $('#Grafica').show()
            $('#ConsultaViajes').hide();
            Find()
        }
        $scope.MostrarConsulta = function () {
            $('#Grafica').hide()
            $('#ConsultaViajes').show();
            Find()
        }
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial == null || $scope.Modelo.FechaInicial == undefined || $scope.Modelo.FechaInicial == '')
                && ($scope.Modelo.FechaFinal == null || $scope.Modelo.FechaFinal == undefined || $scope.Modelo.FechaFinal == '')
            ) {
                $scope.MensajesError.push('Debe ingresar al menos un rango de fechas');
                continuar = false

            } else if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')

            ) {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }
            }

            return continuar
        }

        function Graficar(Datos) {

            var DatosAjustados = [];

            //// Load the Visualization API and the corechart package.
            google.charts.load('current', { 'packages': ['corechart'] });

            // Set a callback to run when the Google Visualization API is loaded.
            google.charts.setOnLoadCallback(drawChart);

            // Callback that creates and populates a data table,
            // instantiates the pie chart, passes in the data and
            // draws it.
            function drawChart() {

                // Create the data table.
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Novedad');
                data.addColumn('number', 'Cantidad');

                for (var i = 0; i < Datos.length; i++) {
                    DatosAjustados.push([Datos[i].Nombre, Datos[i].Cantidad])
                }
                data.addRows(
                    DatosAjustados
                );
                //// Set chart options
                //var options = {
                //    'title': 'How Much Pizza I Ate Last Night',
                //    'width': 400,
                //    'height': 300
                //};

                var options = {
                    title: 'ENTREGAS A TIEMPO Y FUERA DE TIEMPO',
                    pieHole: 0.4,
                    is3D: true,
                };

                if ($scope.Modelo.TipoGrafica.Codigo == 0) {
                    var chart = new google.visualization.ColumnChart(document.getElementById('donutchart'));
                    chart.draw(data, options);
                }
                else if ($scope.Modelo.TipoGrafica.Codigo == 2) {
                    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
                    chart.draw(data, options);
                }
                else if ($scope.Modelo.TipoGrafica.Codigo == 3) {
                    var chart = new google.visualization.LineChart(document.getElementById('donutchart'));
                    chart.draw(data, options);
                }
                else if ($scope.Modelo.TipoGrafica.Codigo == 1) {
                    var chart = new google.visualization.BarChart(document.getElementById('donutchart'));
                    chart.draw(data, options);
                }
                //// Instantiate and draw our chart, passing in some options.
                //var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
                //chart.draw(data, options);
            }
        }


        //Desplegar Reporte
        $scope.DesplegarInforme = function () {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

            $scope.FiltroArmado = '';

            //Depende del listado seleccionado se enviará el nombre por parametro
            $scope.NombreReporte = 'lstIndicadoresEnturnamientos'
            //Arma el filtro
            $scope.ArmarFiltro();
            //Llama a la pagina ASP con el filtro por GET

            window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + 1);

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        }

        $scope.ArmarFiltro = function () {
            $scope.FiltroArmado = '';

            if ($scope.Modelo.Tipo.Codigo !== undefined && $scope.Modelo.Tipo.Codigo !== '' && $scope.Modelo.Tipo.Codigo !== null) {
                $scope.FiltroArmado += '&Tipo=' + $scope.Modelo.Tipo.Codigo;
            }
            if ($scope.Modelo.Cliente !== undefined && $scope.Modelo.Cliente !== '' && $scope.Modelo.Cliente !== null) {
                $scope.FiltroArmado += '&Cliente=' + $scope.Modelo.Cliente.Codigo;
            }
            if ($scope.Modelo.Generador !== undefined && $scope.Modelo.Generador !== '' && $scope.Modelo.Generador !== null) {
                $scope.FiltroArmado += '&Generador=' + $scope.Modelo.Generador.Codigo;
            }
            if ($scope.Modelo.EmpresaTransportadora !== undefined && $scope.Modelo.EmpresaTransportadora !== '' && $scope.Modelo.EmpresaTransportadora !== null) {
                $scope.FiltroArmado += '&EmpresaTransportadora=' + $scope.Modelo.EmpresaTransportadora.Codigo;
            }
            if ($scope.Modelo.PuntoGestion !== undefined && $scope.Modelo.PuntoGestion !== '' && $scope.Modelo.PuntoGestion !== null) {
                $scope.FiltroArmado += '&PuntoGestion=' + $scope.Modelo.PuntoGestion.Codigo;
            }
            if ($scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '' && $scope.Modelo.FechaInicial !== null) {
                var FechaInicial = Formatear_Fecha_Mes_Dia_Ano($scope.Modelo.FechaInicial);
                $scope.FiltroArmado += '&FechaInicial=' + FechaInicial;
            }
            if ($scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '' && $scope.Modelo.FechaFinal !== null) {
                var FechaFinal = Formatear_Fecha_Mes_Dia_Ano($scope.Modelo.FechaFinal);
                $scope.FiltroArmado += '&FechaFinal=' + FechaFinal;
            }

        }

        $scope.DesplegarDescargaConsulta = function () {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                FechaInicial: $scope.Modelo.FechaInicial,
                FechaFinal: $scope.Modelo.FechaFinal,
                Remesa: {
                    Cliente: $scope.Modelo.Cliente,
                    Remitente: $scope.Modelo.Remitente,
                    Destinatario: $scope.Modelo.Destinatario,
                },
                SitioDescargue: $scope.Modelo.SitioDescargue,
            };

            RemesaGuiasFactory.ConsultarIndicadores(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            var ListadoIndicadores = response.data.Datos;
                            var xmlObj = '<?xml version="1.0" encoding="UTF-8"?>';
                            var Columnas = {
                                columna1: "Número",
                                columna2: "Fecha",
                                columna3: "Cliente",
                                columna4: "Ruta",
                                columna5: "Oficina_Actual",
                                columna6: "Observaciones",
                                columna7: "Documento_Cliente",
                                columna8: "Sitio_Descargue",
                                columna9: "Novedad_Entrega",
                                columna10: "Obervaciones_Entrega",
                                columna11: "Fecha_Entrega"
                            };
                            xmlObj += "<row>";
                            for (var i = 0; i < ListadoIndicadores.length; i++) {
                                var sitioDescargue = ListadoIndicadores[i].SitioDescargue.Nombre == null ? "" : ListadoIndicadores[i].SitioDescargue.Nombre;
                                Formatear_Fecha
                                xmlObj += "<reg " + Columnas.columna1 + "='" + ListadoIndicadores[i].Remesa.NumeroDocumento + "' " +
                                    Columnas.columna2 + "='" + Formatear_Fecha(ListadoIndicadores[i].Remesa.Fecha, FORMATO_FECHA_dd + "/" + FORMATO_FECHA_MM + "/" + FORMATO_FECHA_yyyy) + "' " +
                                    Columnas.columna3 + "='" + ListadoIndicadores[i].Remesa.Cliente.NombreCompleto + "' " +
                                    Columnas.columna4 + "='" + ListadoIndicadores[i].Remesa.CiudadRemitente.Nombre + "-" + ListadoIndicadores[i].Remesa.CiudadDestinatario.Nombre + "' " +
                                    Columnas.columna5 + "='" + ListadoIndicadores[i].OficinaActual.Nombre + "' " +
                                    Columnas.columna6 + "='" + ListadoIndicadores[i].Remesa.Observaciones + "' " +
                                    Columnas.columna7 + "='" + ListadoIndicadores[i].Remesa.NumeroDocumentoCliente + "' " +
                                    Columnas.columna8 + "='" + sitioDescargue + "' " +
                                    Columnas.columna9 + "='" + ListadoIndicadores[i].Remesa.NovedadEntrega.Nombre + "' " +
                                    Columnas.columna10 + "='" + ListadoIndicadores[i].Remesa.ObservacionesRecibe + "' " +
                                    Columnas.columna11 + "='" + Formatear_Fecha(ListadoIndicadores[i].Remesa.FechaRecibe, FORMATO_FECHA_dd + "/" + FORMATO_FECHA_MM + "/" + FORMATO_FECHA_yyyy + " " + FORMATO_FECHA_HH + ":" + FORMATO_FECHA_mm) + "' />";
                            }
                            xmlObj += "</row>";
                            download('IndicadoresTiemposEntregas.xls', xmlObj);
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        function download(filename, text) {
            var element = document.createElement('a');
            element.setAttribute('href', 'data:text/xml;charset=utf-8,' + encodeURIComponent(text));
            element.setAttribute('download', filename);
            element.style.display = 'none';
            document.body.appendChild(element);
            $timeout(function () { element.click(); }, 300);
            document.body.removeChild(element);
        }

    }]);