﻿SofttoolsApp.controller("ConsultarGuiasPreimpresasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'ValorCatalogosFactory', 'blockUI', 'PrecintosFactory', 'OficinasFactory', 'GuiasPreimpresasFactory', 'blockUIConfig','TercerosFactory',
    function ($scope, $routeParams, $timeout, $linq, ValorCatalogosFactory, blockUI, PrecintosFactory, OficinasFactory, GuiasPreimpresasFactory, blockUIConfig, TercerosFactory) {
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Procesos' }, { Nombre: 'Asignación Oficinas Guías Preimpresas' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        var filtros = {};
        $scope.ListadoEstados = [];
        $scope.MostrarMensajeError = false;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.Codigo = 0;
        $scope.ListadoOficinas = [];
        $scope.ListadoOficinasDestino = [];
        $scope.ListadoTipoPrecinto = [];
        $scope.ModeloFecha = new Date();
        $scope.ModeloFechaInicial = new Date();
        $scope.ModeloFechaFinal = new Date();
        $scope.ModeloTipoPrecinto = [];
        $scope.ModeloOficinaDestino = [];
        $scope.ModeloOficina = [];
        $scope.ListaResponsable = [];
        $scope.ModeloTipoAsignacionPrecinto = '';
        $scope.ModeloResponsable = '';
        blockUI.stop();
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PRECINTOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoTipoPrecinto.push({ Nombre: '(TODOS)', Codigo: 0 });
        $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 });
        $scope.ListadoOficinasDestino.push({ Nombre: '(TODAS)', Codigo: -1 });

        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        var ResponseTiposAsignacion = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ASIGNACION_PRECINTO }, Sync: true }).Datos;
        if (ResponseTiposAsignacion != undefined) {
            ResponseTiposAsignacion.push({ Codigo: -1, Nombre: '(TODOS)' });
            $scope.ListadoTiposAsignacionPrecinto = ResponseTiposAsignacion
            $scope.ModeloTipoAsignacionPrecinto = $linq.Enumerable().From($scope.ListadoTiposAsignacionPrecinto).First('$.Codigo==-1')
        }

        $scope.AutocompleteTercero = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            CadenaPerfiles: 1415 /*Aforador*/,
                            ValorAutocomplete: value, Sync: true
                        })
                    $scope.ListaResponsable = ValidarListadoAutocomplete(Response.Datos, $scope.ListaResponsable)
                }
            }
            return $scope.ListaResponsable
        }

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_PRECINTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoTipoPrecinto.push(item);
                        });
                        $scope.ModeloTipoPrecinto = $linq.Enumerable().From($scope.ListadoTipoPrecinto).First('$.Codigo ==' + CERO)
                    }
                    else {
                        $scope.ListadoTipoPrecinto = []
                    }
                }
            }, function (response) {
            });

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item);
                    });
                    if ($scope.Sesion.UsuarioAutenticado.Prefijo == PREFIJO_INTEGRA) {
                        $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 1');
                    } else {
                        $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinasDestino.push(item);
                    });
                    if ($scope.Sesion.UsuarioAutenticado.Prefijo == PREFIJO_INTEGRA) {
                        $scope.ModeloOficinaDestino = $linq.Enumerable().From($scope.ListadoOficinasDestino).First('$.Codigo == 1');
                    } else {
                        $scope.ModeloOficinaDestino = $linq.Enumerable().From($scope.ListadoOficinasDestino).First('$.Codigo == -1');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
        /*Metodo buscar*/
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Numero = 0;
                Find();
            }
        };

        //Funcion Nuevo Documento
        $scope.Asignacion = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarGuiasPreimpresas';
            }
        };

        //Funcion Nuevo Documento
        $scope.Translado = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarGuiasPreimpresas';
            }
        };

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IMPUESTO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoImpuesto = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoImpuesto.push({ Codigo: 0, Nombre: '(TODAS)' })
                        $scope.ModalImpuesto = $scope.ListadoTipoImpuesto[0];
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != CODIGO_TIPO_IMPUESTO_NINGUNO) {
                                $scope.ListadoTipoImpuesto.push(response.data.Datos[i]);
                            }
                        }
                    }
                    else {
                        $scope.ListadoTipoImpuesto = []
                    }
                }
            }, function (response) {
            });

        /*-------------------------------------------------------------------------------------------Funcion $routeParams----------------------------------------------------------------*/
        if ($routeParams.Numero !== undefined) {
            $scope.Numero = $routeParams.Numero;
            $scope.ModeloFecha = '';
            $scope.ModeloFechaInicial = '';
            $scope.ModeloFechaFinal = '';
            $scope.ModeloTipoPrecinto = $linq.Enumerable().From($scope.ListadoTipoPrecinto).First('$.Codigo ==' + CERO);
            $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
            $scope.ModeloOficinaDestino = $linq.Enumerable().From($scope.ListadoOficinasDestino).First('$.Codigo == -1');
            Find();
        }

        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            $scope.ListadoPrecintos = [];
            // $scope.ModeloFecha = RetornarFechaEspecificaSinTimeZone($scope.ModeloFecha);
            //$scope.ModeloFecha =new Date($scope.ModeloFecha);
            // $scope.FiltroFecha = new Date($scope.ModeloFecha.getTime() - $scope.ModeloFecha.getTimezoneOffset() * 60000).toISOString();
            filtros = {
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                Numero: $scope.Numero,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Responsable: $scope.ModeloResponsable == undefined || $scope.ModeloResponsable == '' ? { Codigo: 0 } : { Codigo: $scope.ModeloResponsable.Codigo },
                FechaInicial: $scope.ModeloFechaInicial,
                FechaFinal: $scope.ModeloFechaFinal,
                TAPR: { Codigo: $scope.ModeloTipoAsignacionPrecinto.Codigo },
                Oficina: { Codigo: $scope.ModeloOficina.Codigo },
                OficinaDestino: { Codigo: $scope.ModeloOficinaDestino.Codigo }/*,*/
                //Tipo_Presinto: { Codigo: $scope.ModeloTipoPrecinto.Codigo }
            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                GuiasPreimpresasFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoPrecintos = [];
                                response.data.Datos.forEach(function (item) {
                                    if (item.Numero > 0) {
                                        $scope.ListadoPrecintos.push(item);
                                    }
                                });
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.ListadoPrecintos = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };
        Find();

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskDecimalesGrid = function (item) {
            return MascaraDecimales(item)
        };
        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope)
        }
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };


    }]);