﻿SofttoolsApp.controller("EntregasOficinaCtrl", ['$scope', '$timeout', 'TarifarioVentasFactory', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'ValorCatalogosFactory',
    'RemesaGuiasFactory', 'EmpresasFactory', 'OficinasFactory', 'RemesasFactory', 'DocumentosFactory',
    function ($scope, $timeout, TarifarioVentasFactory, $linq, blockUI, blockUIConfig, $routeParams, ValorCatalogosFactory,
        RemesaGuiasFactory, EmpresasFactory, OficinasFactory, RemesasFactory, DocumentosFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CONSULTAR GUÍAS';
        $scope.MapaSitio = [{ Nombre: 'Paqueteria' }, { Nombre: 'Procesos' }, { Nombre: 'Entregas en Oficina' }];
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.REMESAS);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.pref = '';
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {} },
            Numeracion: ''
        };
        $scope.HabilitarEliminarDocumento = false;
        $scope.ListadoEstadoGuia = [
            { Codigo: -1, Nombre: '(TODOS)' }
        ];
        $scope.ListadoOficinas = [];
        $scope.ListadoOficinasActual = [];
        $scope.ListadoTipoIdentificacion = [];
        $scope.ListadoNovedades = [];
        $scope.ModalEntrega = {};
        $scope.ListadoEstadoRemesaPaqueteria = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 0, Nombre: 'BORRADOR' }, { Codigo: 1, Nombre: 'DEFINITIVO' }, { Codigo: 2, Nombre: 'ANULADO' }];
        $scope.Foto = [];
        $scope.ModeloEstado = $scope.ListadoEstadoRemesaPaqueteria[0];
        $scope.ResponseGuia = '';
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------Init
        $scope.InitLoad = function () {

            //ListadoNovedadesRemesas: 
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 203 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNovedades = response.data.Datos;
                        }
                        else {
                            $scope.ListadoNovedades = []
                        }
                    }
                }, function (response) {
                });

            //--Estado Guias
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 60 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                if (response.data.Datos[i].Codigo != 6000) {
                                    $scope.ListadoEstadoGuia.push(response.data.Datos[i]);
                                }
                            }
                        }
                        $scope.Modelo.EstadoRemesaPaqueteria = $scope.ListadoEstadoGuia[0];
                    }
                }, function (response) {
                });

            //Listado tipo identificación:
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {

                            $scope.ListadoTipoIdentificacion = response.data.Datos;

                        }
                    }
                }, function (response) {
                });

            //--Oficinas
            var responseOficina = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, Sync: true });
            if (responseOficina.ProcesoExitoso) {
                $scope.ListadoOficinas.push({ Codigo: -1, Nombre: '(TODAS)' });
                responseOficina.Datos.forEach(function (item) {
                    $scope.ListadoOficinas.push(item);
                });
                $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
            }

            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                $scope.ListadoOficinasActual = $scope.ListadoOficinas;
                $scope.Modelo.OficinaActual = $linq.Enumerable().From($scope.ListadoOficinasActual).First('$.Codigo === -1');
            }
            else {
                $scope.ListadoOficinasActual.push({
                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
                });
                $scope.Modelo.OficinaActual = $scope.ListadoOficinasActual[0];
            }
            //--Obtener Parametros
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
                $scope.Modelo.NumeroInicial = $routeParams.Numero;
                $scope.Modelo.NumeroFinal = $routeParams.Numero;
                PantallaBloqueo(Find);
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            PantallaBloqueo(Find);
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            PantallaBloqueo(Find);
        };
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        //--Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarRemesasPaqueteria';
            }
        };
        $scope.CopiarDocumento = function (Codigo) {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarRemesasPaqueteria/' + Codigo.toString() + ',1';
            }
        };
        //--Funcion Buscar
        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO && DatosRequeridos()) {
                $scope.Codigo = 0;
                PantallaBloqueo(Find);
            }
        };
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                && ($scope.Modelo.NumeroInicial === null || $scope.Modelo.NumeroInicial === undefined || $scope.Modelo.NumeroInicial === '' || $scope.Modelo.NumeroInicial === 0 || isNaN($scope.Modelo.NumeroInicial) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.Modelo.NumeroInicial !== null && $scope.Modelo.NumeroInicial !== undefined && $scope.Modelo.NumeroInicial !== '' && $scope.Modelo.NumeroInicial !== 0)
                || ($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')

            ) {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }

                else {
                    if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial;
                    } else {
                        $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal;
                    }
                }
            }
            return continuar;
        }
        function Find() {
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            $scope.Modelo.Pagina = $scope.paginaActual;
            $scope.Modelo.RegistrosPagina = 10;
            $scope.Modelo.NumeroPlanilla = -1;
            $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
            if ($scope.ModeloEstado !== undefined && $scope.ModeloEstado !== null) {
                $scope.Modelo.Estado = $scope.ModeloEstado.Codigo;
            } else {
                $scope.Modelo.Estado = -1;
            }
            $scope.Modelo.OficinaActual = { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo }
            var filtro = angular.copy($scope.Modelo);
            filtro.Anulado = -1;
            RemesaGuiasFactory.Consultar(filtro).
                then(function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Codigo = 0;
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoGuias = response.data.Datos;
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                        var listado = [];
                        response.data.Datos.forEach(function (item) {
                            listado.push(item);
                        });
                    }
                }, function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    ShowError(response.statusText);
                });

        }
        //--Funcion Buscar
        $scope.EliminarTarifarioVentas = function (codigo, Nombre) {
            $scope.Codigo = codigo;
            $scope.Nombre = Nombre;
            $scope.ModalErrorCompleto = '';
            $scope.ModalError = '';
            $scope.MostrarMensajeError = false;
            showModal('modalEliminarTarifarioVentas');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            TarifarioVentasFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se elimino la linea vehículos ' + $scope.Nombre);
                        closeModal('modalEliminarTarifarioVentas');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarTarifarioVentas');
                    $scope.ModalError = 'No se puede eliminar la linea vehículos ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true;
        };
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        };
        //-------Función que ejecuta el boton de vista preliminar
        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroFactura = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_GUIA;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';




        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroFactura != undefined && $scope.NumeroFactura != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroFactura;
            }
        };

        $scope.ImprimirEtiquetas = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroFactura = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_ETIQUETAS_GUIA;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';




        };
        function DatosRequeridosAnular() {
            var continuar = true;
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false;
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación');
            }
            return continuar;
        }
        //---Anular
        $scope.AnularDocumento = function (Numero, NumeroDocumento) {
            $scope.Numero = Numero;

            $scope.ModalErrorCompleto = '';
            $scope.NumeroDocumento = NumeroDocumento;
            $scope.ModalError = '';
            $scope.MostrarMensajeError = false;
            showModal('modalAnular');
        };
        $scope.Anular = function () {
            var aplicaplanilla = false;
            var aplicaFactura = false;
            var aplicaManifiesto = false;

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnula: $scope.ModeloCausaAnula,
                Numero: $scope.Numero
            };
            if (DatosRequeridosAnular()) {
                RemesasFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado > 0) {
                                ShowSuccess('Se anuló la remesa ' + $scope.NumeroDocumento);
                                closeModal('modalAnular', 1);
                                Find();
                            } else {
                                var mensaje = 'No se pudo anular la remesa ya que se encuentra relacionado con los siguientes documentos: ';

                                var Facturas = '\nFacturas: ';
                                var Planillas = '\nPlanillas: ';
                                var Manifiestos = '\nManifiestos: ';
                                for (var i = 0; i < response.data.Datos.DocumentosRelacionados.length; i++) {
                                    if (response.data.Datos.DocumentosRelacionados[i].FacturaVentas.Numero > 0) {
                                        Facturas += response.data.Datos.DocumentosRelacionados[i].FacturaVentas.Numero + ',';
                                        aplicaFactura = true;
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Planilla.Numero > 0) {
                                        Planillas += response.data.Datos.DocumentosRelacionados[i].Planilla.Numero + ',';
                                        aplicaplanilla = true;
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Manifiesto.Numero > 0) {
                                        Manifiestos += response.data.Datos.DocumentosRelacionados[i].Manifiesto.Numero + ',';
                                        aplicaManifiesto = true;
                                    }
                                }

                                if (aplicaplanilla) {
                                    mensaje += Planillas;
                                }
                                if (aplicaFactura) {
                                    mensaje += Facturas;
                                }
                                if (aplicaManifiesto) {
                                    mensaje += Manifiestos;
                                }
                                ShowError(mensaje);
                            }

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = '';
                        showModal('modalMensajeAnularOrdenCargue');
                        result = InternalServerError(response.statusText);
                        $scope.ModalError = 'No se puede anular la orden de servicio ' + $scope.NumeroDocumento;
                        $scope.ModalErrorCompleto = response.statusText;
                    });
            }
        };
        //---Anular
        $scope.ConsultarEstadosRemesas = function (item) {
            $scope.ListadoEstadoGuias = [];
            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero
            };
            RemesaGuiasFactory.ConsultarEstadosRemesa(Filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoEstadoGuias = response.data.Datos;
                            showModal('ModalEstadosRemesas');
                        } else {
                            ShowError('No se encontro registro de estados de la remesa seleccionada');
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        //--Control Entregas
        $scope.ConsultarControlEntregas = function (item) {
            $scope.DocumentoCumplido = {
                Nombre: '',
                NombreDocumento: '',
                ExtensionDocumento: ''
            };

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero
            };
            RemesaGuiasFactory.ObtenerControlEntregas(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Remesa.NombreRecibe != null && response.data.Datos.Remesa.NumeroIdentificacionRecibe != null && response.data.Datos.Remesa.TelefonoRecibe != null) {
                            $scope.FotosEntrega = [];
                            $scope.FotosDevolucion = [];
                            //$scope.FotoControlEntrega = response.data.Datos.Remesa.Fotografia;
                            //$scope.FotoControlEntrega.FotoCargada = 'data:' + response.data.Datos.Remesa.Fotografia.TipoFoto + ';base64,' + response.data.Datos.Remesa.Fotografia.FotoBits;
                            $scope.FotosControlEntrega = response.data.Datos.Remesa.Fotografias;
                            $scope.FotosControlEntrega.forEach(foto => {
                                foto.FotoCargada = 'data:' + foto.TipoFoto + ';base64,' + foto.FotoBits;
                                if (foto.EntregaDevolucion == 1) {
                                    $scope.FotosDevolucion.push(foto);
                                } else {
                                    $scope.FotosEntrega.push(foto);
                                }
                            });
                            $scope.ControlEntregaRemesa = response.data.Datos.Remesa;
                            $scope.ControlEntregaRemesa.Latitud = response.data.Datos.Latitud;
                            $scope.ControlEntregaRemesa.Longitud = response.data.Datos.Longitud;
                            if (response.data.Datos.DocumentosCumplidoRemsa != undefined) {

                                for (var i = 0; i < response.data.Datos.DocumentosCumplidoRemsa.length; i++) {
                                    var Documento = response.data.Datos.DocumentosCumplidoRemsa[i];
                                    $scope.DocumentoCumplido = {
                                        Nombre: Documento.NombreDocumento + "." + Documento.ExtensionDocumento,
                                        Numero: $scope.ControlEntregaRemesa.Numero,
                                        NombreDocumento: Documento.NombreDocumento,
                                        ExtensionDocumento: Documento.ExtensionDocumento,
                                        Id: Documento.Id,
                                        Temp: false
                                    };
                                    $scope.ModificaArchivo = false;
                                    break;
                                }
                            }
                            if (response.data.Datos.DocumentosCumplidoRemsa.length > 0) {
                                $scope.HabilitarEliminarDocumento = true;
                            } else {
                                $scope.HabilitarEliminarDocumento = false;
                            }
                            $scope.ControlEntregaRemesa.FirmaImg = "data:image/png;base64," + $scope.ControlEntregaRemesa.Firma;
                            showModal('modalControlEntregas');
                        }
                        else {
                            ShowError('El documento no cuenta con un control de entregas');
                        }
                    }
                    else {
                        ShowError('El documento no cuenta con un control de entregas');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        $scope.VerFotosEntrega = function () {
            showModal('modalFotosEntregas');
        };
        $scope.VerFotosDevolucion = function () {
            showModal('modalFotosDevolucion');
        };
        //--Control Entregas
        //------Gestion Adjunto Documento Cumplido
        $scope.ModificaArchivo = false;
        var regExt = /(?:\.([^.]+))?$/;
        $scope.DocumentoCumplido = {
            Nombre: '',
            NombreDocumento: '',
            ExtensionDocumento: ''
        };
        $scope.CadenaFormatos = ".pdf";
        $scope.CargarArchivo = function (element) {
            var continuar = true;
            if (element.files.length > 0) {
                //$scope.DocumentoCumplido = angular.element(this.DocumentoCumplido)[0];

                if (element === undefined) {
                    ShowError("Archivo invalido, por favor verifique el formato del archivo  /nFormatos permitidos: 0");
                    continuar = false;
                }
                if (continuar == true) {
                    if (element.files[0].size >= 3000000) //3 MB
                    {
                        ShowError("El archivo " + element.files[0].name + " supera los " + "3 MB" + ", intente con otro archivo", false);
                        continuar = false;
                    }
                    var Formatos = $scope.CadenaFormatos.split(',');
                    var cont = 0;
                    var ExtencionArchivo = regExt.exec(element.files[0].name)[1];
                    for (var i = 0; i < Formatos.length; i++) {
                        Formatos[i] = Formatos[i].replace(' ', '');
                        //var Extencion = element.files[0].name.split('.');
                        if ('.' + (ExtencionArchivo.toUpperCase()) == Formatos[i].toUpperCase()) {
                            cont++;
                        }
                    }
                    if (cont == 0) {
                        ShowError("Archivo invalido, por favor verifique el formato del archivo.  Formatos permitidos: (" + $scope.CadenaFormatos + ")");
                        continuar = false;
                    }
                }

                if (continuar == true) {
                    if ($scope.DocumentoCumplido.Archivo != undefined && $scope.DocumentoCumplido.Archivo != '' && $scope.DocumentoCumplido.Archivo != null) {
                        $scope.DocumentoCumplido.ArchivoTemporal = element.files[0];
                        showModal('modalConfirmacionRemplazarDocumentoCumplido');
                    } else {
                        var reader = new FileReader();
                        $scope.DocumentoCumplido.ArchivoCargado = element.files[0];
                        reader.onload = AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }
            }
        };
        $scope.RemplazarDocumentoCumplido = function () {
            var reader = new FileReader();
            $scope.DocumentoCumplido.ArchivoCargado = $scope.DocumentoCumplido.ArchivoTemporal;
            reader.onload = AsignarArchivo;
            reader.readAsDataURL($scope.DocumentoCumplido.ArchivoCargado);
            closeModal('modalConfirmacionRemplazarDocumentoCumplido');
        };
        function AsignarArchivo(e) {
            $scope.$apply(function () {
                $scope.DocumentoCumplido.Numero = $scope.ControlEntregaRemesa.Numero;//--Numero Remesa
                $scope.DocumentoCumplido.Archivo = e.target.result.replace('data:' + $scope.DocumentoCumplido.ArchivoCargado.type + ';base64,', '');
                $scope.DocumentoCumplido.Tipo = $scope.DocumentoCumplido.ArchivoCargado.type;
                $scope.DocumentoCumplido.Nombre = $scope.DocumentoCumplido.ArchivoCargado.name;
                $scope.DocumentoCumplido.NombreDocumento = $scope.DocumentoCumplido.ArchivoCargado.name.substr(0, $scope.DocumentoCumplido.ArchivoCargado.name.lastIndexOf('.'));
                $scope.DocumentoCumplido.ExtensionDocumento = regExt.exec($scope.DocumentoCumplido.ArchivoCargado.name)[1];
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Cargando Documento...");
                $timeout(function () {
                    blockUI.message("Cargando Documento...");
                    InsertarDocumentoCumplido();
                }, 100);
                //Bloqueo Pantalla
            });
        }
        function InsertarDocumentoCumplido() {
            // Guardar Archivo temporal
            var Documento = {};
            Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ControlEntregaRemesa.Numero,
                Nombre: $scope.DocumentoCumplido.NombreDocumento,
                Archivo: $scope.DocumentoCumplido.Archivo,
                Tipo: $scope.DocumentoCumplido.Tipo,
                Extension: $scope.DocumentoCumplido.ExtensionDocumento,
                CumplidoRemesa: true,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.InsertarTemporal(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {
                if (ResponseDocu.Datos > 0) {
                    $scope.DocumentoCumplido.Id = ResponseDocu.Datos;
                    $scope.DocumentoCumplido.Temp = true;
                    $scope.ModificaArchivo = true;
                    ShowSuccess('Se cargó el documento adjunto "' + $scope.DocumentoCumplido.NombreDocumento + '"');
                }
                else {
                    ShowError(ResponseDocu.MensajeOperacion);
                }
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
        }
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (item) {
            if (item.Id != undefined && item.Id != '' && item.Id != null) {
                if (item.Temp) {
                    window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=CumplidoRemesa&ID=' + item.Id + '&Temp=1' + '&ENRE_Numero=' + item.Numero);
                }
                else {
                    window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=CumplidoRemesa&ID=' + item.Id + '&ENRE_Numero=' + item.Numero + '&Temp=0');
                }
            }
            else {
                ShowError("Se debe seleccionar un archivo");
            }
        };
        $scope.GuardarArchivoCumplido = function (item) {
            if (item.Id != undefined && item.Id != '' && item.Id != null) {
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Guardando Documento...");
                $timeout(function () {
                    blockUI.message("Guardando Documento...");
                    TrasladarDocumentoCumplido(item);
                }, 100);
                //Bloqueo Pantalla
            }
        };
        $scope.EliminarArchivo = function (item) {
            Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.EliminarDocumentoCumplidoRemesa(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {

                ShowSuccess('Se eliminó el documento adjunto "' + $scope.DocumentoCumplido.NombreDocumento + '"');
                $scope.DocumentoCumplido.Nombre = '';
                $scope.HabilitarEliminarDocumento = false;
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
        }
        function TrasladarDocumentoCumplido(item) {
            var Documento = {};
            Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.InsertarDocumentosCumplidoRemesa(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {
                if (ResponseDocu.Datos > 0) {
                    $scope.DocumentoCumplido.Id = ResponseDocu.Datos;
                    $scope.DocumentoCumplido.Temp = false;
                    $scope.ModificaArchivo = false;
                    ShowSuccess('Se guardo el documento adjunto "' + $scope.DocumentoCumplido.NombreDocumento + '"');
                }
                else {
                    ShowError(ResponseDocu.MensajeOperacion);
                }
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
            blockUI.stop();
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
        }
        //------Gestion Adjunto Documento Cumplido
        //------Mapa Google
        $scope.AbrirMapa = function (lat, lng) {
            showModal('modalMostrarMapa');
            iniciarMapaGoogle(lat, lng);
        };
        function iniciarMapaGoogle(lat, lng) {
            var marker = [];
            var map, infoWindow;
            $scope.MapaLatitud = lat != '' ? lat : 0;
            $scope.MapaLongitud = lng != '' ? lng : 0;
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    };
                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.BOUNCE,
                    title: title
                    //icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };
                markers = new window.google.maps.Marker(markerOptions);
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input 
                    $scope.MapaLatitud = this.getPosition().lat();
                    $scope.MapaLongitud = this.getPosition().lng();
                });
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                if ($scope.MapaLatitud != undefined && $scope.MapaLongitud != undefined && $scope.MapaLatitud != 0 && $scope.MapaLongitud != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.MapaLatitud, $scope.MapaLongitud), 'Posición del usuario', 'Destino');
                } else {
                    $scope.MapaLatitud = posicion.coords.latitude;
                    $scope.MapaLongitud = posicion.coords.longitude;
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }
        $scope.AsignarPosicion = function () {
            $scope.Modelo.Remesa.Latitud = $scope.MapaLatitud;
            $scope.Modelo.Remesa.Longitud = $scope.MapaLongitud;
            closeModal('modalMostrarMapa');
        };

        $scope.filtroListadoGuias = function (item) {
            if (item.EstadoGuia.Codigo != 6030) {
                return item;
            }
            
        }

        $scope.CargarImagen = function (element) {
            var continuar = true;
            $scope.TipoImagen = element
            $scope.NombreDocumento = element.files[0].name
            if (element === undefined) {
                ShowError("Solo se pueden cargar archivos en formato JPEG, PNG", false);
                continuar = false;
            }

            if (continuar == true) {
                if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png') {
                    ShowError("Solo se pueden cargar archivos en formato JPEG, PNG ", false);
                    continuar = false;
                }
            }

            if (continuar == true) {
                var reader = new FileReader();
                reader.onload = $scope.AsignarImagenListado;
                reader.readAsDataURL(element.files[0]);
            }

        }

        $scope.AsignarImagenListado = function (e) {
            try {
                RedimHorizontal('')
                RedimVertical('')
            } catch (e) {
            }
            $scope.$apply(function () {
                $scope.Convertirfoto = null
                var TipoImagen = ''

                var name = $scope.TipoImagen.files[0].name.split('.')

                $scope.NombreFoto = name[0]

                if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
                    var img = new Image()
                    img.src = e.target.result
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        TipoImagen = 'JPEG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/jpeg'
                        AsignarFotoListado()

                    }, 100)
                }
                else if ($scope.TipoImagen.files[0].type == 'image/png') {
                    var img = new Image()
                    img.src = e.target.result
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        TipoImagen = 'PNG'
                        $scope.TipoImagen = TipoImagen
                        $scope.TipoFoto = 'image/png'
                        AsignarFotoListado()
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
                    $scope.Convertirfoto = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                    TipoImagen = 'PDF'
                    $scope.TipoFoto = 'application/pdf'
                    $scope.TipoImagen = TipoImagen
                    AsignarFotoListado()

                }
                var input = document.getElementById('inputfile')
                input.value = ''
            });
        }



        function AsignarFotoListado() {
            // $scope.Foto = [];
            $scope.Foto.push({
                NombreFoto: $scope.NombreFoto,
                TipoFoto: $scope.TipoFoto,
                ExtensionFoto: $scope.TipoImagen,
                FotoBits: $scope.Convertirfoto,
                ValorDocumento: 1,
                FotoCargada: 'data:' + $scope.TipoFoto + ';base64,' + $scope.Convertirfoto,
                id: $scope.Foto.length
            });
        }

        $scope.EliminarFoto = function (item) {
            $scope.Foto.splice(item.id, 1);
            for (var i = 0; i < $scope.Foto.length; i++) {
                $scope.Foto[i].id = i;
            }
            // $scope.Foto = []
            $scope.ModificarFoto = true;
            //$("#Foto").attr("src", "");
        }

        function RedimVertical(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasVertical');
            if (ctx) {
                // dentro del canvas se dibuja la imagen que se recibe por parametro
                ctx.drawImage(img, 0, 0, 499, 699);
            }
        }

        function RedimHorizontal(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasHorizontal');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 699, 499);
            }
        }

        $scope.LimpiarFirma = function () {
            try {
                var canvas = document.getElementById('Firma');
                var context = canvas.getContext('2d')
                context.clearRect(0, 0, canvas.width, canvas.height);
            } catch (e) {

            }

        }

        $scope.EntregarGuia = function (item) {
            $scope.ResponseGuia = RemesaGuiasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: item.Remesa.Numero, Sync:true }).Datos
            var ResponseRecibe = $scope.CargarTercero($scope.ResponseGuia.Remesa.Destinatario.Codigo)
            $scope.Foto = [];
            $scope.ModalEntrega = {
                NumeroDocumento: item.Remesa.NumeroDocumento,
                TipoIdentificacionRecibe: $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo==' + ResponseRecibe.TipoIdentificacion.Codigo),
                NumeroIdentificacion: ResponseRecibe.NumeroIdentificacion,
                Nombre: ResponseRecibe.NombreCompleto,
                Telefono: ResponseRecibe.Telefonos,
                NovedadEntrega: $scope.ListadoNovedades[0],
                Observaciones : ''
            }
            showModal('modalEntregarGuia')
            
            //Entrega = {
            //    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            //    Remesa: { Numero: item.Remesa.Numero },
            //    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
            //    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            //}

            //RemesaGuiasFactory.EntregarOficina(Entrega).
            //    then(function (response) {
            //        if (response.data.ProcesoExitoso == true) {
            //            ShowSuccess('La guía se ha entregado satisfactoriamente');
            //            $scope.PrimerPagina(Find);
            //        }
            //    });
        }

        $scope.ConfirmacionGuardarPaqueteria = function () {
            if (DatosRequeridosEntregaGuia()) {
                ShowConfirm('¿Está seguro de guardar la información?', $scope.GuardarPaqueteria)
            }
        };

        function DatosRequeridosEntregaGuia() {
            $scope.MensajesErrorRecibir = [];
            var continuar = true;

            //if ($scope.ModalEntrega.Cantidad === undefined || $scope.ModalEntrega.Cantidad === '' || $scope.ModalEntrega.Cantidad === null || $scope.ModalEntrega.Cantidad === 0) {
            //    $scope.MensajesErrorRecibir.push('Debe ingresar la cantidad');
            //    continuar = false;
            //}
            //if ($scope.ModalEntrega.Peso === undefined || $scope.ModalEntrega.Peso === '' || $scope.ModalEntrega.Peso === null || $scope.ModalEntrega.Peso === 0) {
            //    $scope.MensajesErrorRecibir.push('Debe ingresar el peso');
            //    continuar = false;
            //}
            if ($scope.ModalEntrega.TipoIdentificacionRecibe === undefined || $scope.ModalEntrega.TipoIdentificacionRecibe === '' || $scope.ModalEntrega.TipoIdentificacionRecibe === null || $scope.ModalEntrega.TipoIdentificacionRecibe.Codigo === CODIGO_NO_APLICA_TIPO_IDENTIFICACION) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el tipo de identificación');
                continuar = false;
            }
            if ($scope.ModalEntrega.NumeroIdentificacion === undefined || $scope.ModalEntrega.NumeroIdentificacion === '' || $scope.ModalEntrega.NumeroIdentificacion === null || $scope.ModalEntrega.NumeroIdentificacion === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el número de identificación');
                continuar = false;
            }
            if ($scope.ModalEntrega.Nombre === undefined || $scope.ModalEntrega.Nombre === '' || $scope.ModalEntrega.Nombre === null || $scope.ModalEntrega.Nombre === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el nombre de la persona que recibe');
                continuar = false;
            }
            if ($scope.ModalEntrega.Telefono === undefined || $scope.ModalEntrega.Telefono === '' || $scope.ModalEntrega.Telefono === null || $scope.ModalEntrega.Telefono === 0) {
                $scope.MensajesErrorRecibir.push('Debe ingresar el teléfono');
                continuar = false;
            }
            //if ($scope.Modal.Observaciones === undefined || $scope.Modal.Observaciones === '' || $scope.Modal.Observaciones === null || $scope.Modal.Observaciones === 0) {
            //    $scope.MensajesErrorRecibir.push('Debe ingresar las observaciones');
            //    continuar = false;
            //}
            return continuar;
        }

        $scope.GuardarPaqueteria = function () {
            //closeModal('modalConfirmacionGuardarPaqueteria', 1);
            var canvas = document.getElementById('Firma');
            var image = new Image();
            image.src = canvas.toDataURL("image/png");
            $scope.Firma = image.src.replace(/data:image\/png;base64,/g, '');

            //$scope.Modal.FechaRecibe = RetornarFechaEspecificaSinTimeZone($scope.Modal.FechaRecibe);

            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                //Codigo: $scope.Codigo,
                Numero: $scope.ResponseGuia.Remesa.Numero,
                //FechaRecibe: $scope.Modal.FechaRecibe,
                CantidadRecibe: $scope.ResponseGuia.Remesa.CantidadCliente,
                PesoRecibe: $scope.ResponseGuia.Remesa.PesoCliente,
                CodigoTipoIdentificacionRecibe: $scope.ModalEntrega.TipoIdentificacionRecibe.Codigo,
                NumeroIdentificacionRecibe: parseInt($scope.ModalEntrega.NumeroIdentificacion),
                NombreRecibe: $scope.ModalEntrega.Nombre,
                TelefonoRecibe: $scope.ModalEntrega.Telefono,
                NovedadEntrega: $scope.ModalEntrega.NovedadEntrega,
                ObservacionesRecibe: $scope.ModalEntrega.Observaciones,
                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                Fotografias: $scope.Foto,
                Firma: $scope.Firma,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                //DevolucionRemesa: $scope.DevolucionRemesa
            };

            RemesaGuiasFactory.GuardarEntrega($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                           
                            ShowSuccess('La guía fue recibida por ' + $scope.ModalEntrega.Nombre);
                            $scope.Foto = [];
                            $scope.LimpiarFirma();
                               
                            $scope.PrimerPagina(Find);
                            closeModal('modalEntregarGuia')
                            
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };


        //------Mapa Google
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };
        //----------------------------Funciones Generales---------------------------------//
    }
]);