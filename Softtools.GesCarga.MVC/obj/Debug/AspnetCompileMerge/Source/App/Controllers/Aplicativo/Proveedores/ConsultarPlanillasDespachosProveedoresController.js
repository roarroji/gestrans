﻿SofttoolsApp.controller("ConsultarPlanillasDespachosProveedoresCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'PlanillasDespachosOtrasEmpresasFactory', 'blockUI', 'EmpresasFactory', 'PlanillasDespachosProveedoresFactory',
    function ($scope, $routeParams, $timeout, $linq, PlanillasDespachosOtrasEmpresasFactory, blockUI, EmpresasFactory, PlanillasDespachosProveedoresFactory) {

        $scope.MapaSitio = [{ Nombre: 'Proveedores' }, { Nombre: 'Documentos' }, { Nombre: 'Planillas Despachos Proveedores' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        var filtros = {};
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.MostrarMensajeError = false;
        $scope.NumeroAnular;
        $scope.NumeroDocumentoAnular;
        $scope.pref = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PLANILLA_DESPACHOS_PROVEEDORES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }



        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.ListaEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }


        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListaEstados).First('$.Codigo == -1');

        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;


        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };



        /*Metodo buscar*/
        $scope.Buscar = function () {

            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {

                if (DatosRequeridos()) {
                    $scope.Codigo = 0
                    Find()
                }
            }
        };


        // Botón Nuevo:
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarPlanillasDespachosProveedores';
            }
        };

        if ($routeParams.Numero > CERO) {
            $scope.NumeroDocumento = $routeParams.Numero;
            $scope.FechaInicio = '';
            $scope.FechaFin = '';
            Find();
            $scope.NumeroDocumento = $routeParams.Numero;
           
        }



        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                //NumeroDocumento
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            $scope.ListadoDespachosOtrasEmpresas = [];

            filtros = {
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: parseInt($scope.NumeroDocumento),
                //NumeroDocumentoTransporte: $scope.NumeroDocumentoTransporte,
                FechaInicial: moment($scope.FechaInicio).format('YYYY/MM/DD'),
                FechaFinal: moment($scope.FechaFin).format('YYYY/MM/DD'),
                Vehiculo: $scope.PlacaVehiculo,
                Cliente: { Nombre: $scope.NombreCliente },
                Estado: $scope.Estado.Codigo,
                Transportador: {Nombre: $scope.NombreTransportador}

            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                PlanillasDespachosProveedoresFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoDespachosOtrasEmpresas = response.data.Datos;
                                console.log($scope.ListadoDespachosOtrasEmpresas);
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                                console.log($scope.totalRegistros, $scope.cantidadRegistrosPorPaginas);
                            }
                            else {
                                $scope.ListadoDespachosOtrasEmpresas = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }


                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };




        $scope.ArmarFiltro = function () {

        };

        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.NumeroDocumento === null || $scope.NumeroDocumento === undefined || $scope.NumeroDocumento === '' || $scope.NumeroDocumento === 0 || isNaN($scope.NumeroDocumento) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.NumeroDocumento !== null && $scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '' && $scope.NumeroDocumento !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            return continuar
        };


        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }

        $scope.AnularDocumento = function (planilla) {

            PlanillasDespachosProveedoresFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Numero: planilla.Numero }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        console.log("resultado validacion anula: ", response.data.Datos);


                        if (response.data.Datos.EncabezadoFactura == 1 || response.data.Datos.EncabezadoFactura == '1') {
                            ShowError('Esta planilla se encuentra asociada a una factura');
                        } else {
                            $scope.NumeroAnula = planilla.Numero;
                            $scope.NumeroDocumentoAnula = planilla.Numero;
                            showModal('modalAnularPlanilla');
                        }

                    } else {
                        console.log("error en la consulta");
                    }

                });


        };




        $scope.Anular = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.ModeloCausaAnula,
                Numero: $scope.NumeroAnula


            };
            if (DatosRequeridosAnular()) {

                PlanillasDespachosProveedoresFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Se anuló la planilla ' + $scope.NumeroDocumentoAnula);
                            closeModal('modalAnularPlanilla');
                            Find();

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.data.MensajeOperacion);
                    });
            }


        };


        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });

        //Reporte:

        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroReporte = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_PRO;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };

        $scope.ArmarFiltro = function () {
            if ($scope.NumeroReporte != undefined && $scope.NumeroReporte != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroReporte;
            }
        };


        //Buscar automáticamente cuando se redirecciona(si se envía un parametro por el request):

        //if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        //    $scope.Numero = $routeParams.Numero;
           
        //    Find();
        //}


        /*---------------------------------------------------------------------------------------------------Máscaras-----------------------------------------------------------------------------*/

        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);
        };

        $scope.MaskPlaca = function () {
            MascaraPlacaGeneral($scope);
        }
    }
]);