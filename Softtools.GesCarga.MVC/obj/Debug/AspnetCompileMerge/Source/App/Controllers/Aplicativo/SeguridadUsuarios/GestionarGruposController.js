﻿SofttoolsApp.controller("GestionarGrupoPerfilesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'GrupoUsuariosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, GrupoUsuariosFactory)
{

    //------------------------------------------DECLARACIÓN DE VARIABLES--------------------------------------------------------|
    $scope.Titulo = 'GESTIONAR GRUPO';
    $scope.MapaSitio = [{ Nombre: 'Seguridad Usuarios' }, { Nombre: 'Grupo/Perfiles' }, { Nombre: 'Gestionar' }];
    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
    };
    $scope.Seleccionado = true;
    $scope.MensajesError = [];

    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GRUPOS_PERFILES); } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }
    
    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    //------------------------------------------INSTANCIANCIAMIENTO DE MODELOS--------------------------------------------------|

    if ($routeParams.Codigo > 0)
    {
        $scope.Modelo.Codigo = $routeParams.Codigo;
    }
    else
    {
        $scope.Modelo.Codigo = 0;
    }
    //---------------------------------------------FUNCIONES Y ACCIONES---------------------------------------------------------|
    if ($scope.Modelo.Codigo > 0) {
        $scope.Titulo = 'CONSULTAR GRUPO';
        $scope.Deshabilitar = true;
        ObtenerGrupo();
    }

    function ObtenerGrupo()
    {
        blockUI.start('Cargando Grupo');
        $timeout(function ()
        {
            blockUI.message('Cargando Grupo');
        }, 200);

        filtros =
            {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };
        blockUI.delay = 1000;
        GrupoUsuariosFactory.Obtener(filtros).then(function (response)
        {
            if (response.data.ProcesoExitoso === true)
            {
                $scope.Modelo.Nombre = response.data.Datos.Nombre;
                $scope.Modelo.CodigoGrupo = response.data.Datos.CodigoGrupo;
                $scope.Modelo.Codigo = response.data.Datos.Codigo;
            }
            else
            {
                ShowError('No se logro consultar el grupo con código ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                document.location.href = '#!ConsultarGrupoPerfiles';
            }
        },
           function (response)
           {
               ShowError('No se logro consultar el grupo con código ' + $scope.Codigo + '. Por favor contacte el administrador del sistema.');
               document.location.href = '#!ConsultarGrupoPerfiles';
           })

        blockUI.stop();
    };
        

    $scope.ConfirmacionGuardarGrupo = function () {
        showModal('modalConfirmacionGuardarGrupo');



    };
    // Metodo para guardar 
    $scope.GuardarGrupo = function () {
        /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
        closeModal('modalConfirmacionGuardarGrupo');

        $scope.Modelo.TipoGrupoSeguridad = $scope.ModalTipoGrupoSeguridad;

        if (DatosRequeridosGrupo()) {
            GrupoUsuariosFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó el grupo ' + $scope.Modelo.Nombre);
                            }
                            else if ($scope.Modelo.Codigo > 0) {
                                ShowSuccess('Se modificó el grupo ' + $scope.Modelo.Nombre);
                            }
                            location.href = '#!ConsultarGrupoPerfiles';
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
    };

    /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar-------------------------------------------------------------------*/
    function DatosRequeridosGrupo() {
        $scope.MensajesError = [];
        var continuar = true;
        if ($scope.Modelo.CodigoGrupo == undefined || $scope.Modelo.CodigoGrupo == null) {
            $scope.Modelo.CodigoGrupo = ""
        }
        if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == "") {
            $scope.MensajesError.push('Debe ingresar el nombre del grupo');
            continuar = false;
        }

        return continuar;
    }
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


    $scope.VolverMaster = function ()
    {
        document.location.href = '#!ConsultarGrupoPerfiles';
    };
}]);