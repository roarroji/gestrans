﻿SofttoolsApp.controller("ConsultarListadosSeguridadUsuariosCtrl", ['$scope', '$linq', 'ValorCatalogosFactory', 'EmpresasFactory','GrupoUsuariosFactory',
    function ($scope, $linq, ValorCatalogosFactory, EmpresasFactory, GrupoUsuariosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Seguridad Usuarios' }, { Nombre: 'Listados' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.DeshabilitarPDF = true;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.NombreReporte = '';
        $scope.FiltroArmado = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.MostrarEstado = true;
        $scope.ListadoTipoAplicacion = [];
        $scope.ModeloLoguueado = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },

        }

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_LISTADO_SEGURIDAD_USUARIOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        // Cargar combos de módulo y listados 
        
    
        $scope.ListaListados = [];
        for (var i = 0; i < $scope.ListadoMenuListados.length; i++) {
            if ($scope.ListadoMenuListados[i].Padre == OPCION_MENU_LISTADO_SEGURIDAD_USUARIOS) {
                let rawName = $scope.ListadoMenuListados[i].Nombre;
                if (rawName.split(" ").length > 2) {
                    var nMenu = rawName.split(" ")[1] + " " + rawName.split(" ")[2] + " " + rawName.split(" ")[3]
                } else {
                    var nMenu = rawName.split(" ")[1];
                }
                $scope.ListaListados.push({ NombreMenu: nMenu, Codigo: $scope.ListadoMenuListados[i].Codigo });
            }
        }
        $scope.ListadosSeguridadUsuarios = $scope.ListaListados[0];

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        //Ajustar Filtros:
        $scope.filtroUsuarios = false;
        $scope.AjustarFiltros = function () {
            if ($scope.ListadosSeguridadUsuarios.Codigo == CODIGO_LISTADO_USUARIOS || $scope.ListadosSeguridadUsuarios.Codigo == CODIGO_LISTADO_USUARIOS_POR_GRUPO) {
                $scope.filtroUsuarios = true;
                
            } else if ($scope.ListadosSeguridadUsuarios.Codigo == CODIGO_LISTADO_GRUPOS || $scope.ListadosSeguridadUsuarios.Codigo == CODIGO_LISTADO_PERMISOS_POR_GRUPO){
                $scope.filtroUsuarios = false;
              
            }
        }


        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });

        //Combo Cuenta Activa

        $scope.ListadoEstadoCuentaActiva = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'SI', Codigo: 1 },
            { Nombre: 'NO', Codigo: 0 },
           
        ]
        $scope.ModeloCuentaActiva = $linq.Enumerable().From($scope.ListadoEstadoCuentaActiva).First('$.Codigo == -1 ');



        /*Cargar el combo de Estados*/

        $scope.ListadoEstadoLogueado = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'LOGUEADO', Codigo: CODIGO_UNO },
            { Nombre: 'DESLOGUEADO', Codigo: CERO }
        ]
        $scope.ModeloLoguueado = $linq.Enumerable().From($scope.ListadoEstadoLogueado).First('$.Codigo == -1 ');

        /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true;
        }

        //Cargar el combo de grupos:
        $scope.Grupo = '';
        $scope.ListadoGrupoUsuarios = [];
        GrupoUsuariosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    
                    $scope.ListadoGrupoUsuarios.push({ Codigo: -1, Nombre: '(NO APLICA)' });
                    for (var i = 0; i < response.data.Datos.length; i++) {
                        $scope.ListadoGrupoUsuarios.push(response.data.Datos[i]);
                    }
                 
                    $scope.Grupo = $linq.Enumerable().From($scope.ListadoGrupoUsuarios).First('$.Codigo == -1' );
                    
                }
            });

        /*Cargar el combo de Aplicacion*/

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_APLICACIONES_DE_GESTRANS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoAplicacion = []
                    $scope.ListadoTipoAplicacion = response.data.Datos;
                    $scope.TipoAplicacion = $linq.Enumerable().From($scope.ListadoTipoAplicacion).First('$.Codigo ==' + response.data.Datos[0].Codigo);
                }
                else {
                    $scope.ListadoTipoAplicacion = []
                }
            }, function (response) {
                ShowError(response.statusText);
            });


        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };
        
        $scope.DesplegarInforme = function (OpcionPDf, OpcionEXCEL) {
            $scope.FiltroArmado = '';
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            var DatosRequeridos = true;

            switch ($scope.ListadosSeguridadUsuarios.Codigo) {
                case CODIGO_LISTADO_USUARIOS:
                    $scope.NombreReporte = NOMBRE_LISTADO_USUARIOS;

                    DatosRequeridos = DatosRequeridosSeguridad();
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_GRUPOS:
                    $scope.NombreReporte = NOMBRE_LISTADO_GRUPOS;
                    DatosRequeridos = DatosRequeridosSeguridad();
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_USUARIOS_POR_GRUPO:
                    $scope.NombreReporte = NOMBRE_LISTADO_USUARIOS_GRUPO;
                    DatosRequeridos = DatosRequeridosSeguridad();
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_PERMISOS_POR_GRUPO:
                    $scope.NombreReporte = NOMBRE_LISTADO_PERMISOS_GRUPO;
                    DatosRequeridos = DatosRequeridosSeguridad();
                    $scope.ArmarFiltro();
                    break;
            }


            if (DatosRequeridos == true) {
                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
                }
                if (OpcionEXCEL == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf);
                }

                //Se limpia el filtro para poder hacer mas consultas
                $scope.FiltroArmado = '';

            }
        };

        function DatosRequeridosSeguridad (){
            DatosRequeridos = true;
            $scope.MensajesError = [];
            var count = 0;

            if ($scope.ListadosSeguridadUsuarios.Codigo == CODIGO_LISTADO_USUARIOS) {

                if ($scope.Identificador == null || $scope.Identificador == undefined || $scope.Identificador == '') {
                    count++;
                }

                if ($scope.Nombre == null || $scope.Nombre == undefined || $scope.Nombre == '') {
                    count++;
                }

                if ($scope.TipoAplicacion == null || $scope.TipoAplicacion == undefined || $scope.TipoAplicacion == '') {
                    count++;
                }

                if ($scope.ModeloCuentaActiva == null || $scope.ModeloCuentaActiva == undefined || $scope.ModeloCuentaActiva == '') {
                    count++;
                }


                if (count >= 4) {
                    DatosRequeridos = false;
                    $scope.MensajesError.push('Debe ingresar algún criterio de búsqueda');
                }
                return DatosRequeridos;
            }

            if ($scope.ListadosSeguridadUsuarios.Codigo == CODIGO_LISTADO_GRUPOS) {

                /*if ($scope.Identificador == null || $scope.Identificador == undefined || $scope.Identificador == '') {
                    count++;
                }

                if ($scope.Nombre == null || $scope.Nombre == undefined || $scope.Nombre == '') {
                    count++;
                }               


                if (count >= 2) {
                    DatosRequeridos = false;
                    $scope.MensajesError.push('Debe ingresar algún criterio de búsqueda');
                }*/
                return DatosRequeridos;
            }
            if ($scope.ListadosSeguridadUsuarios.Codigo == CODIGO_LISTADO_USUARIOS_POR_GRUPO) {

                return DatosRequeridos;
            }
            if ($scope.ListadosSeguridadUsuarios.Codigo == CODIGO_LISTADO_PERMISOS_POR_GRUPO) {

                return DatosRequeridos;
            }

        }

        $scope.ArmarFiltro = function () {
            
            if ($scope.ListadosSeguridadUsuarios.Codigo == CODIGO_LISTADO_USUARIOS || $scope.ListadosSeguridadUsuarios.Codigo == CODIGO_LISTADO_USUARIOS_POR_GRUPO) {
                if ($scope.Identificador !== undefined && $scope.Identificador !== '' && $scope.Identificador !== null) {
                    $scope.FiltroArmado += '&Identificador=' + $scope.Identificador;
                }

                if ($scope.Nombre !== undefined && $scope.Nombre !== '' && $scope.Nombre !== null) {
                    $scope.FiltroArmado += '&Nombre=' + $scope.Nombre;
                }

                if ($scope.TipoAplicacion !== undefined && $scope.TipoAplicacion !== '' && $scope.TipoAplicacion !== null) {
                    $scope.FiltroArmado += '&Aplicacion=' + $scope.TipoAplicacion.Codigo;
                }

                if ($scope.Grupo !== undefined && $scope.Grupo !== '' && $scope.Grupo !== null) {
                    $scope.FiltroArmado += '&Grupo=' + $scope.Grupo.Codigo;
                }

                if ($scope.ModeloCuentaActiva !== undefined && $scope.ModeloCuentaActiva !== '' && $scope.ModeloCuentaActiva !== null) {
                    $scope.FiltroArmado += '&CuentaActiva=' + $scope.ModeloCuentaActiva.Codigo;
                }
            }

            if ($scope.ListadosSeguridadUsuarios.Codigo == CODIGO_LISTADO_GRUPOS) {
                if ($scope.Identificador !== undefined && $scope.Identificador !== '' && $scope.Identificador !== null) {
                    $scope.FiltroArmado += '&Identificador=' + $scope.Identificador;
                }

                if ($scope.Nombre !== undefined && $scope.Nombre !== '' && $scope.Nombre !== null) {
                    $scope.FiltroArmado += '&Nombre=' + $scope.Nombre;
                }
               
            }

            if ($scope.ListadosSeguridadUsuarios.Codigo == CODIGO_LISTADO_PERMISOS_POR_GRUPO) {
                if ($scope.Identificador !== undefined && $scope.Identificador !== '' && $scope.Identificador !== null) {
                    $scope.FiltroArmado += '&Identificador=' + $scope.Identificador;
                }

                if ($scope.Nombre !== undefined && $scope.Nombre !== '' && $scope.Nombre !== null) {
                    $scope.FiltroArmado += '&Nombre=' + $scope.Nombre;
                }

                if ($scope.TipoAplicacion !== undefined && $scope.TipoAplicacion !== '' && $scope.TipoAplicacion !== null) {
                    $scope.FiltroArmado += '&Aplicacion=' + $scope.TipoAplicacion.Codigo;
                }

                if ($scope.Grupo !== undefined && $scope.Grupo !== '' && $scope.Grupo !== null) {
                    $scope.FiltroArmado += '&Grupo=' + $scope.Grupo.Codigo;
                }

                if ($scope.ModeloCuentaActiva !== undefined && $scope.ModeloCuentaActiva !== '' && $scope.ModeloCuentaActiva !== null) {
                    $scope.FiltroArmado += '&CuentaActiva=' + $scope.ModeloCuentaActiva.Codigo;
                }
            }
        }
    }]);