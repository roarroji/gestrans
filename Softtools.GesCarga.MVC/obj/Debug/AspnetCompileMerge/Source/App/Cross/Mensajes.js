﻿function ShowWarning(titulo, mensaje) {
    swal({
        title: mensaje,
        text: titulo,
        type: 'warning',
        confirmButtonText: 'Aceptar'
    });
}

function ShowError(mensaje) {
    if (mensaje == 'Unauthorized') {
        mensaje = "Su sesión a caducado por favor inicie sesión nuevamente.";
        //location.href = '#';
    }

    swal({
        title: mensaje,
        text: 'GESTRANS.NET CARGA',
        type: 'error',
        confirmButtonText: 'Aceptar'
    });
}

function ShowInfo(mensaje) {
    swal({
        title: mensaje,
        text: 'GESTRANS.NET CARGA',
        type: 'info',
        confirmButtonText: 'Aceptar'
    });
}

function ShowSuccess(mensaje) {
    swal({
        title: mensaje,
        text: 'GESTRANS.NET CARGA',
        type: 'success',
        confirmButtonText: 'Aceptar'
    });
}

function ShowConfirm(mensaje, funcionConfirm, funcionCancel) {
    swal.fire({
        title: mensaje,
        type: 'warning',
        text: 'GESTRANS.NET CARGA',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#3085d6',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Aceptar'
    }).then((result) => {
        if (result.value) {
            funcionConfirm()
        } else {
            funcionCancel()
        }
    })
}
function ShowWarningConfirm(mensaje, funcionConfirm, funcionCancel) {
    swal.fire({
        title: mensaje,
        type: 'warning',
        text: 'GESTRANS.NET CARGA',
        cancelButtonColor: '#d33',
        confirmButtonColor: '#3085d6',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Aceptar',
    }).then((result) => {
        if (result.value) {
            funcionConfirm()
        } else {
            funcionCancel()
        }
    })
}
