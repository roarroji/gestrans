﻿SofttoolsApp.factory('RutasFactory', ['$http', function ($http) {

    var urlService = GetUrl('Rutas');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarGastosRutas = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarGastosRutas';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
     
    };

    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.InsertarPuestosControl = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/InsertarPuestosControl';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };

    factory.InsertarLegalizacionGastosRuta = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/InsertarLegalizacionGastosRuta';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.InsertarPeaje = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/InsertarPeaje';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.GenerarPlanitilla = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GenerarPlanitilla';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    
    factory.ConsultarTrayectos = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarTrayectos';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    
    factory.InsertarTrayectos = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/InsertarTrayectos';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    return factory;
}]);