﻿SofttoolsApp.factory('RemesasFactory', ['$http', function ($http) {

    var urlService = GetUrl('Remesas');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarRemesasPendientesFacturar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPendientesFacturar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ActualizarDistribuido = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ActualizarDistribuido';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.MarcarRemesasFacturadas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/MarcarRemesasFacturadas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.CumplirRemesas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/CumplirRemesas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarNumerosRemesas = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarNumerosRemesas';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ObtenerPrecintosAleatorios = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerPrecintosAleatorios';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarRemesasPlanillaDespachos = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPlanillaDespachos';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ConsultarCumplidoRemesas = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarCumplidoRemesas';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ActualizarRemesasExcel = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ActualizarRemesasExcel';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    
    return factory;
}]);