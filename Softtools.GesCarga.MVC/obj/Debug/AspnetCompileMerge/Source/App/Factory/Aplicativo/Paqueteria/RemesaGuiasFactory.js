﻿SofttoolsApp.factory('RemesaGuiasFactory', ['$http', function($http) {

    var urlService = GetUrl('RemesaPaqueteria');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function(filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Obtener = function(filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.Anular = function(entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Guardar = function(entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.GuardarBitacoraGuias = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GuardarBitacoraGuias';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.CumplirGuias = function(entidad) {
        request.method = 'POST';
        request.url = urlService + '/CumplirGuias';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.GenerarPlanitilla = function(entidad) {
        request.method = 'POST';
        request.url = urlService + '/GenerarPlanitilla';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.GuardarEntrega = function(entidad) {
        request.method = 'POST';
        request.url = urlService + '/GuardarEntrega';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ConsultarIndicadores = function(entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarIndicadores';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.AsociarGuiaPlanilla = function(entidad) {
        request.method = 'POST';
        request.url = urlService + '/AsociarGuiaPlanilla';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.CambiarEstadoRemesas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/CambiarEstadoRemesasPaqueteria';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.CambiarEstadoGuia = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/CambiarEstadoGuia';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    }; 
    factory.CambiarEstadoTrazabilidadGuia = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/CambiarEstadoTrazabilidadGuia';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    }; 

    factory.ConsultarEstadosRemesa = function(entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarEstadosRemesa';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ObtenerControlEntregas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerControlEntregas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarRemesasPendientesControlEntregas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPendientesControlEntregas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarRemesasPorPlanillar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPorPlanillar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarRemesasRecogerOficina = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasRecogerOficina';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ConsultarRemesasRetornoContado = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasRetornoContado';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ValidarPreimpreso = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ValidarPreimpreso';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarRemesasPendientesRecibirOficina = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPendientesRecibirOficina';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    // Nueva funcionalidad Bitacora Guias 
    factory.ConsultarBitacoraGuias = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarBitacoraGuias';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    
    factory.ConsultarRemesasPendientesRecibirOficina = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPendientesRecibirOficina';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.RecibirRemesaPaqueteriaOficinaDestino = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/RecibirRemesaPaqueteriaOficinaDestino';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.EntregarOficina = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/EntregarOficina';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarRemesasPorLegalizar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPorLegalizar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    factory.ConsultarRemesasPorLegalizarRecaudo = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesasPorLegalizarRecaudo';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    
    factory.ConsultarGuiasPlanillaEntrega = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarGuiasPlanillaEntrega';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ConsultarGuiasEtiquetasPreimpresas = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarGuiasEtiquetasPreimpresas';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ActualizarSecuencia = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/ActualizarSecuencia';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
     
    return factory; 

}]);