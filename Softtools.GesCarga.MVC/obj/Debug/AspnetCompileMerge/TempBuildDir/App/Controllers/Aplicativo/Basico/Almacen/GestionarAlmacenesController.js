﻿SofttoolsApp.controller("GestionarAlmacenesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'CiudadesFactory', 'PlanUnicoCuentasFactory', 'OficinasFactory', 'AlmacenesFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, CiudadesFactory, PlanUnicoCuentasFactory, OficinasFactory, AlmacenesFactory) {

    $scope.Titulo = 'GESTIONAR ALMACENES';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Almacén' }, { Nombre: 'Almacenes' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ALMACENES);

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoAlterno: 0,
        Nombre: '',
        Estado: 0
    }
    $scope.Modelo.Ciudad = '';
    $scope.ListadoOficinas = [];
    $scope.ListadoCiudades = [];
    $scope.ListadoCuentasPUC = [];

    $scope.ListadoEstados = [
        { Codigo: 0, Nombre: "INACTIVO" },
        { Codigo: 1, Nombre: "ACTIVO" },
    ]

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Obtener();
    }
    else {
        $scope.Modelo.Codigo = 0;
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
    }

    $scope.LongitudCiudad = 0;// verifica la cantidad de datos ingresados  de la ciudad
    $scope.IngresoValidaCiudad = true

    $scope.AsignarCiudad = function (Ciudad) {
        if (Ciudad != undefined || Ciudad != null) {
            if (angular.isObject(Ciudad)) {
                $scope.LongitudCiudad = Ciudad.length;
                $scope.IngresoValidaCiudad = true;
            }
            else if (angular.isString(Ciudad)) {
                $scope.LongitudCiudad = Ciudad.length;
                $scope.IngresoValidaCiudad = false;
            }
        }
    };

    $scope.LongitudCuentaPUC = 0;// verifica la cantidad de datos ingresados  de la ciudad
    $scope.IngresoValidaCuentaPUC = true

    $scope.AsignarCuentaPUC = function (CuentaPUC) {
            $scope.IngresoValidaCuentaPUC = true
            if (CuentaPUC != undefined && CuentaPUC != null &&  CuentaPUC != "" && CuentaPUC.Codigo != 0) {
                if (angular.isObject(CuentaPUC)) {
                    $scope.LongitudCuentaPUC = CuentaPUC.length;
                    $scope.IngresoValidaCuentaPUC = true;
                    $scope.NombreCuentaPUC = CuentaPUC.Nombre
                }
                else if (angular.isString(CuentaPUC)) {
                    $scope.LongitudCuentaPUC = CuentaPUC.length;
                    $scope.IngresoValidaCuentaPUC = false;
                    $scope.NombreCuentaPUC = ''
                }
            } else {
                $scope.NombreCuentaPUC = ''
            }
    };
    /*Cargar Autocomplete de Ciudades*/
    CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
   then(function (response) {
       if (response.data.ProcesoExitoso === true) {
           if (response.data.Datos.length > 0) {
               response.data.Datos.push({ Nombre: '', Codigo: 0 })
               $scope.ListadoCiudades = response.data.Datos;

               if ($scope.Ciudad > 0) {
                   $scope.Modelo.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + $scope.Ciudad);
               } else {
                  // $scope.Modelo.Ciudad = $scope.ListadoCiudades[$scope.ListadoCiudades.length - 1];
               }
           }
           else {
               $scope.ListadoCiudades = [];
               $scope.Modelo.Ciudad = null;
           }
       }
   }, function (response) {
       ShowError(response.statusText);
   });
    /*Cargar Autocomplete de Cuentas PUC*/
        PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
         then(function (response) {
             if (response.data.ProcesoExitoso === true) {
                 if (response.data.Datos.length > 0) {
                     response.data.Datos.push({ CodigoCuenta: '', Codigo: 0 })
                     $scope.ListadoCuentasPUC = response.data.Datos;

                     if ($scope.CuentaPUC > 0) {
                         $scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CuentaPUC);
                         $scope.NombreCuentaPUC = $scope.Modelo.CuentaPUC.Nombre
                     } else {
                         $scope.Modelo.CuentaPUC = '';
                     }
                 }
                 else {
                     $scope.ListadoCuentasPUC = [];
                     $scope.Modelo.CuentaPUC = null;
                 }
             }
         }, function (response) {
             ShowError(response.statusText);
         });


    /*Cargar el combo de oficinas*/
    OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoOficinas = [];
                response.data.Datos.forEach(function (item) {
                    if (item.Codigo > 0) {
                        $scope.ListadoOficinas.push(item);
                    }
                })
                if ($scope.Oficina > 0) {
                    $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.Oficina);
                } else {
                    $scope.Modelo.Oficina = $scope.ListadoOficinas[0];
                }

            }
        }, function (response) {
            ShowError(response.statusText);
        });
    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function Obtener() {
        blockUI.start('Cargando Almacén código ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando Almacén Código ' + $scope.Modelo.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        AlmacenesFactory.Obtener(filtros).
           then(function (response) {
               if (response.data.ProcesoExitoso === true) {
                   $scope.Modelo = response.data.Datos
                   $scope.Modelo.Codigo = response.data.Datos.Codigo;

                   $scope.Ciudad = response.data.Datos.Ciudad.Codigo;
                   if ($scope.ListadoCiudades.length > 0 && $scope.Ciudad > 0) {
                       $scope.Modelo.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + response.data.Datos.Ciudad.Codigo);
                   }
                   $scope.CuentaPUC = response.data.Datos.CuentaPUC.Codigo;
                   if ($scope.ListadoCuentasPUC.length > 0 && $scope.CuentaPUC > 0) {
                       $scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + response.data.Datos.CuentaPUC.Codigo);
                       $scope.NombreCuentaPUC = $scope.Modelo.CuentaPUC.Nombre
                   }
                   $scope.Oficina = response.data.Datos.Oficina.Codigo;
                   if ($scope.ListadoOficinas.length > 0 && $scope.Oficina > 0) {
                       $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + response.data.Datos.Oficina.Codigo);
                   }

                   $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

               }
               else {
                   ShowError('No se logro consultar el almacén código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                   document.location.href = '#!ConsultarAlmacenes';
               }
           }, function (response) {
               ShowError(response.statusText);
               document.location.href = '#!ConsultarAlmacenes';
           });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    $scope.ConfirmacionGuardar = function () {
        showModal('modalConfirmacionGuardar');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardar');
        if (DatosRequeridos()) {

            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
            $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            AlmacenesFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó el almacén "' + $scope.Modelo.Nombre + '"');
                                $scope.Modelo.Codigo = response.data.Datos;
                            }
                            else {
                                ShowSuccess('Se modificó el almacén "' + $scope.Modelo.Nombre + '"');
                            }
                            location.href = '#!ConsultarAlmacenes/' + $scope.Modelo.Codigo;
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;

        if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
            $scope.MensajesError.push('Debe ingresar el nombre del almacén');
            continuar = false;
        }
        if ($scope.Modelo.Ciudad == undefined || $scope.Modelo.Ciudad == '' || $scope.Modelo.Ciudad.Codigo == 0 || $scope.IngresoValidaCiudad == false) {
            $scope.MensajesError.push('Debe ingresar la ciudad del almacén');
            continuar = false;
        }
        if ($scope.Modelo.Telefono == undefined || $scope.Modelo.Telefono == '' || $scope.Modelo.Telefono == null) {
            $scope.MensajesError.push('Debe ingresar el teléfono del almacén');
            continuar = false;
        }
        if ($scope.Modelo.Direccion == undefined || $scope.Modelo.Direccion == '' || $scope.Modelo.Direccion == null) {
            $scope.MensajesError.push('Debe ingresar la dirección del almacén');
            continuar = false;
        }
        if ($scope.Modelo.Contacto == undefined || $scope.Modelo.Contacto == null) {
            $scope.Modelo.Contacto == ''
        }
        if ($scope.Modelo.Celular == undefined || $scope.Modelo.Celular == null) {
            $scope.Modelo.Celular == ''
        }
        if ($scope.Modelo.Oficina == undefined || $scope.Modelo.Oficina == '' || $scope.Modelo.Oficina.Codigo == 0 || $scope.Modelo.Oficina == null) {
            $scope.MensajesError.push('Debe seleccionar la oficina del almacén');
            continuar = false;
        }
        if ($scope.IngresoValidaCuentaPUC == false) {
            $scope.MensajesError.push('Debe ingresar una cuenta PUC valida');
            continuar = false;
        }
        if ($scope.Modelo.CuentaPUC == undefined || $scope.Modelo.CuentaPUC == '' || $scope.Modelo.CuentaPUC == null) {
            $scope.Modelo.CuentaPUC = { Codigo: 0, CodigoCuenta: '', Nombre:'' }
        }
        return continuar;
    }

    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarAlmacenes/' + $scope.Modelo.Codigo;
    };

    $scope.MaskNumero = function (option) {
        MascaraNumeroGeneral($scope)
    };
    $scope.MaskMayus = function (option) {
        MascaraMayusGeneral($scope)
    };

    $scope.MaskDireccion = function (option) {
        try { $scope.Modelo.Direccion = MascaraDireccion($scope.Modelo.Direccion) } catch (e) { }
    };
    $scope.MaskTelefono = function (option) {
        try { $scope.Modelo.Telefono = MascaraTelefono($scope.Modelo.Telefono) } catch (e) { }
    };
    $scope.MaskCelular = function (option) {
        try { $scope.Modelo.Celular = MascaraCelular($scope.Modelo.Celular) } catch (e) { }
    };

}]);