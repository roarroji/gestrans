﻿SofttoolsApp.controller("GestionarReferenciaAlmacenesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ReferenciaAlmacenesFactory', 'ReferenciasFactory', 'AlmacenesFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, ReferenciaAlmacenesFactory, ReferenciasFactory, AlmacenesFactory) {

        $scope.Titulo = 'GESTIONAR REFERENCIA ALMACENES';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Almacén' }, { Nombre: 'Referencia Almacenes' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_REFERENCIA_ALMACENES);

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,
            ExistenciaMinima: 0,
            ExistenciaMaxima: 0,
            ExistenciaActual: 0,
            UltimoPrecio: 0,
            ValorPromedio: 0,
        }
        $scope.Modelo.Referencia = ''
        $scope.CodigoReferencia = ''
        $scope.Modelo.NombreReferencia = ''




        $scope.ListadoReferencias = [];
        $scope.ListadoAlmacenes = [];

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVA" },
            { Codigo: 1, Nombre: "ACTIVA" },
        ]
       
        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;      
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        }
    /*Cargar el combo de Referencias*/
        ReferenciasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoReferencias = [];
                    $scope.ListadoReferencias = response.data.Datos;

                    if ($scope.CodigoReferencia !== undefined && $scope.CodigoReferencia !== null && $scope.CodigoReferencia !== '') {
                        $scope.Modelo.NombreReferencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + $scope.CodigoReferencia);
                        $scope.Modelo.Referencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + $scope.CodigoReferencia);
                    }
                    CarReferencias();
                }
            }, function (response) {
                ShowError(response.statusText);
            });

    /*Cargar el combo de almacenes*/
    AlmacenesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoAlmacenes = response.data.Datos;
    
                    if ($scope.Almacen > 0) {
                        $scope.Modelo.Almacen = $linq.Enumerable().From($scope.ListadoAlmacenes).First('$.Codigo ==' + $scope.Almacen);
                    } else {
                        $scope.Modelo.Almacen = $scope.ListadoAlmacenes[0]
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        function CarReferencias() {

            $timeout(function () {
                if ($('#CmbReferencia')[0].options[0].value == "?") {
                    $('#CmbReferencia')[0].options[0].remove()
                }
            }, 50);
            $timeout(function () {
                $('#CmbReferencia').selectpicker()
            }, 200);
            $timeout(function () {
                if ($('#CmbNombreReferencia')[0].options[0].value == "?") {
                    $('#CmbNombreReferencia')[0].options[0].remove()
                }
            }, 50);
            $timeout(function () {
                $('#CmbNombreReferencia').selectpicker('refresh')
            }, 200);
        }



        $scope.CarReferencia = function (Referencia) {
            if (Referencia.Codigo !== undefined) {

                $('#CmbNombreReferencia').selectpicker('destroy')
                $timeout(function () {
                    if ($('#CmbNombreReferencia')[0].options[0].value == "?" || $('#CmbNombreReferencia')[0].options[0].value == "") {
                        $('#CmbNombreReferencia')[0].options[0].remove()
                    }
                }, 50);
                $timeout(function () {
                    $('#CmbNombreReferencia').selectpicker()
                }, 200);

                $scope.Modelo.Referencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + Referencia.Codigo);
                $scope.Modelo.NombreReferencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + Referencia.Codigo);
                $scope.CodigoReferencia = $scope.Modelo.NombreReferencia.Codigo
            }
        }

        $scope.CarNombreReferencia = function (Referencia) {
            if (Referencia.Codigo !== undefined) {

                $('#CmbReferencia').selectpicker('destroy')
                $timeout(function () {
                    if ($('#CmbReferencia')[0].options[0].value == "?" || $('#CmbReferencia')[0].options[0].value == "") {
                        $('#CmbReferencia')[0].options[0].remove()
                    }
                }, 50);
                $timeout(function () {
                    $('#CmbReferencia').selectpicker()
                }, 200);

                $scope.Modelo.Referencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + Referencia.Codigo);
                $scope.Modelo.NombreReferencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + Referencia.Codigo);
                $scope.CodigoReferencia = $scope.Modelo.NombreReferencia.Codigo
            }
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando referencia almacén código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando referencia almacén Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            ReferenciaAlmacenesFactory.Obtener(filtros).
               then(function (response) {
                   if (response.data.ProcesoExitoso === true) {

                       $scope.Modelo = response.data.Datos;
                       try { $scope.Modelo.CodigoAlterno = MascaraValores($scope.Modelo.CodigoAlterno) } catch (e) { }
                       try { $scope.Modelo.UltimoPrecio = MascaraValores($scope.Modelo.UltimoPrecio) } catch (e) { }
                       try { $scope.Modelo.ValorPromedio = MascaraValores($scope.Modelo.ValorPromedio) } catch (e) { }
                       try { $scope.Modelo.ExistenciaMinima = MascaraValores($scope.Modelo.ExistenciaMinima) } catch (e) { }
                       try { $scope.Modelo.ExistenciaMaxima = MascaraValores($scope.Modelo.ExistenciaMaxima) } catch (e) { }
                       try { $scope.Modelo.ExistenciaActual = MascaraValores($scope.Modelo.ExistenciaActual) } catch (e) { }
                       $scope.Modelo.Codigo = response.data.Datos.Codigo;

                       $scope.Almacen = response.data.Datos.Almacen.Codigo;
                       if ($scope.ListadoAlmacenes.length > 0 && $scope.Almacen > 0) {
                           $scope.Modelo.Almacen = $linq.Enumerable().From($scope.ListadoAlmacenes).First('$.Codigo ==' + response.data.Datos.Almacen.Codigo);
                       }


                       $scope.CodigoReferencia = response.data.Datos.Referencia.Codigo;

                       if ($scope.ListadoReferencias.length > 0) {
                           $scope.Modelo.NombreReferencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + response.data.Datos.Referencia.Codigo);
                           $scope.Modelo.Referencia = $linq.Enumerable().From($scope.ListadoReferencias).First('$.Codigo ==' + response.data.Datos.Referencia.Codigo);
                       }
                        
                       $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                   }
                   else {
                       ShowError('No se logro consultar la referencia almacén código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                       document.location.href = '#!ConsultarReferenciaAlmacenes';
                   }
               }, function (response) {
                   ShowError(response.statusText);
                   document.location.href = '#!ConsultarReferenciaAlmacenes';
               });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                //Estado
                $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
                $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                $scope.Modelo.NombreReferencia = $scope.Modelo.NombreReferencia.Nombre

                try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
                try { $scope.Modelo.UltimoPrecio = MascaraNumero($scope.Modelo.UltimoPrecio) } catch (e) { }
                try { $scope.Modelo.ValorPromedio = MascaraNumero($scope.Modelo.ValorPromedio) } catch (e) { }
                try { $scope.Modelo.ExistenciaMinima = MascaraNumero($scope.Modelo.ExistenciaMinima) } catch (e) { }
                try { $scope.Modelo.ExistenciaMaxima = MascaraNumero($scope.Modelo.ExistenciaMaxima) } catch (e) { }
                try { $scope.Modelo.ExistenciaActual = MascaraNumero($scope.Modelo.ExistenciaActual) } catch (e) { }
                ReferenciaAlmacenesFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó la referencia almacén "' + $scope.Modelo.Almacen.Nombre + ' - ' + $scope.Modelo.Referencia.Nombre + '"');
                                    $scope.Modelo.Codigo = response.data.Datos; 
                                }
                                else {
                                    ShowSuccess('Se modificó la referencia almacén "' + $scope.Modelo.Almacen.Nombre + ' - ' + $scope.Modelo.Referencia.Nombre + '"');
                                }
                                location.href = '#!ConsultarReferenciaAlmacenes/' + $scope.Modelo.Codigo;
                            }                          
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };
        
        
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Almacen == undefined || $scope.Modelo.Almacen == "" || $scope.Modelo.Almacen == null) {
                $scope.MensajesError.push('Debe ingresar el nombre del almacén');
                continuar = false;
            }
            if ($scope.Modelo.Referencia == undefined || $scope.Modelo.Referencia == "" || $scope.Modelo.Referencia == null) {
                $scope.MensajesError.push('Debe ingresar una referencia valida');
                continuar = false;
            }
            if ($scope.Modelo.ExistenciaMinima > $scope.Modelo.ExistenciaMaxima) {
                $scope.MensajesError.push('La existencia mínima debe ser menor a la existencia máxima');
                continuar = false;
            }
            if ($scope.Modelo.ExistenciaMinima == undefined || $scope.Modelo.ExistenciaMinima == "" || $scope.Modelo.ExistenciaMinima == null) {
                $scope.ExistenciaMinima = 0;
            }
            if ($scope.Modelo.ExistenciaMaxima == undefined || $scope.Modelo.ExistenciaMaxima == "" || $scope.Modelo.ExistenciaMaxima == null) {
                $scope.Modelo.ExistenciaMaxima = 0;
            }

            if ($scope.Modelo.ExistenciaActual == undefined || $scope.Modelo.ExistenciaActual == "" || $scope.Modelo.ExistenciaActual == null) {
                $scope.Modelo.ExistenciaActual = 0;
            }
            if ($scope.Modelo.UltimoPrecio == undefined || $scope.Modelo.UltimoPrecio == "" || $scope.Modelo.UltimoPrecio == null) {
                $scope.Modelo.UltimoPrecio = 0;
            }
            if ($scope.Modelo.ValorPromedio == undefined || $scope.Modelo.ValorPromedio == "" || $scope.Modelo.ValorPromedio == null) {
                $scope.Modelo.ValorPromedio = 0;
            }
            if ($scope.Modelo.UbicacionAlmacen == undefined || $scope.Modelo.UbicacionAlmacen == "" || $scope.Modelo.UbicacionAlmacen == null) {
                $scope.Modelo.UbicacionAlmacen = '';
            }
            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarReferenciaAlmacenes/' + $scope.Modelo.Codigo;
        };

        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
            try { $scope.Modelo.UltimoPrecio = MascaraNumero($scope.Modelo.UltimoPrecio) } catch (e) { }
            try { $scope.Modelo.ValorPromedio = MascaraNumero($scope.Modelo.ValorPromedio) } catch (e) { }
            try { $scope.Modelo.ExistenciaMinima = MascaraNumero($scope.Modelo.ExistenciaMinima) } catch (e) { }
            try { $scope.Modelo.ExistenciaMaxima = MascaraNumero($scope.Modelo.ExistenciaMaxima) } catch (e) { }
            try { $scope.Modelo.ExistenciaActual = MascaraNumero($scope.Modelo.ExistenciaActual) } catch (e) { }
        };
        $scope.MaskValores = function () {
            //try { $scope.Modelo.UltimoPrecio = MascaraValores($scope.Modelo.UltimoPrecio) } catch (e) { }
            //try { $scope.Modelo.ValorPromedio = MascaraValores($scope.Modelo.ValorPromedio) } catch (e) { }
            MascaraValoresGeneral($scope);
        };
}]);