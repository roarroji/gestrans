﻿SofttoolsApp.controller("ConsultarPeajesRutasCtrl", ['$scope', '$timeout', 'RutasFactory', '$linq', 'blockUI', '$routeParams', 'PuestoControlesFactory', 'ValorCatalogosFactory',
    'PeajesFactory', 'RutasFactory', 'PeajesRutasFactory',
    function ($scope, $timeout, RutasFactory, $linq, blockUI, $routeParams, PuestoControlesFactory, ValorCatalogosFactory,
        PeajesFactory, RutasFactory, PeajesRutasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Peajes Rutas' }];

        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 


        var filtros = {};
        $scope.InputValor = true;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.ListadoTipoRuta = [];
        $scope.ListadoSitiosCargueDescargue = [];
        $scope.ListadoSitiosCargueDescargueFiltrado = [];
        $scope.ListaCargueDescargue = [];
        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];
        $scope.ModalCargueDescargue = {
            Sitio: '',
            TipoVehiculo: '',
            ValorCargue: '',
            ValorDescargue: '',
            AplicaAnticipo: 0
        }
        $scope.Modelo = {

        };

        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        //Combo Sitios Cargue y Descargue:
        $scope.ListadoSitiosCargueDescargue.push({ Codigo: -1, Nombre: '(Seleccione un sitio...)' });
        $scope.ModalCargueDescargue.Sitio = $linq.Enumerable().From($scope.ListadoSitiosCargueDescargue).First('$.Codigo==-1');
        //Fin Combo Sitios Cargue y Descargue

        /*Cargar el combo de tipo ruta*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_RUTA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoRuta = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoRuta = response.data.Datos;
                        $scope.TipoRuta = $scope.ListadoTipoRuta[0]
                    } else {
                        $scope.ListadoTipoRutas = []
                    }
                }
            }, function (response) {
            });

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_RUTAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarPeajesRutas';
            }
        };

        //------------------------------------------------- PARAMETROS --------------------------------------------------------//

        $scope.AutocompleteRuta = function (value) {
            var ListadoRutas = [];
            var ResponseRuta = RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Nombre: value, Sync: true }).Datos;

            if (ResponseRuta != undefined) {
                ListadoRutas = ResponseRuta;

            }

            return ListadoRutas;
        }


        $scope.AutocompletePeajes = function (value) {
            var ListadoPeajes = [];
            var ResponsePeajes = PeajesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Nombre: value, Sync: true }).Datos;

            if (ResponsePeajes != undefined) {
                ListadoPeajes = ResponsePeajes;

            }

            return ListadoPeajes;
        }


        //-------------------------------------------------GASTOS RUTA--------------------------------------------------------//

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };


        PuestoControlesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoPuestoControles = []
                        response.data.Datos.forEach(function (item) {
                            var registro = item;
                            $scope.ListadoPuestoControles.push(registro);
                        });
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });



        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoRutas = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        //Estado: $scope.ModalEstado.Codigo, 
                        Ruta: $scope.Modelo.Ruta,
                        Peaje: $scope.Modelo.Peaje,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    }

                    console.log('llego', filtros)
                    blockUI.delay = 1000;
                    PeajesRutasFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    response.data.Datos.forEach(function (item) {
                                        var registro = item;
                                        $scope.ListadoRutas.push(registro);
                                    });

                                    $scope.ListadoRutas.forEach(function (item) {
                                        $scope.codigo = item.Codigo;
                                    });
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }



        /*------------------------------------------------------------------------------------Eliminar Rutas-----------------------------------------------------------------------*/

        $scope.EliminarPeajeRuta = function (item) {
            $scope.Peaje = item.Peaje;
            $scope.Ruta = item.Ruta;
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false

            showModal('modalEliminarPeajeRuta');
        };

        $scope.EliminarPeaje = function () {
            try {
                var ResponseEliminarPeaje = PeajesRutasFactory.EliminarPeajesRutas({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Ruta: { Codigo: $scope.Ruta.Codigo }, Peaje: { Codigo: $scope.Peaje.Codigo }, Sync: true })
                if (ResponseEliminarPeaje != undefined) {
                    if (ResponseEliminarPeaje.ProcesoExitoso == true) {
                        ShowSuccess('Se eliminó el peaje de  ' + $scope.Peaje.Nombre + ' en la ruta ' + $scope.Ruta.Nombre);
                        Find();
                    } else {
                        ShowError('No se eliminó el peaje de la ruta')
                        Find();
                    }
                } else {
                    ShowError('falló al guardar')
                }
            } catch (e) {
                ShowError('este peaje ya se encuentra en esta ruta')
                console.log(e);
            }
            closeModal('modalEliminarPeajeRuta');

        }




        $scope.ValidarConcepto = function () {
            if ($scope.ModalGastosRuta.Concepto.Codigo == CODIGO_GASTO_COMBUSTIBLE_RENDIMIENTO_GALON) {
                $scope.ModalGastosRuta.Valor = MascaraValores(0);
            }
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /* Obtener parametros*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Modelo = {};
            $scope.Modelo = {};
            $scope.Modelo.Peaje = { Codigo: $routeParams.Peaje };
            $scope.Modelo.Ruta = { Codigo: $routeParams.Codigo };
            Find();
            $scope
            $scope.Modelo.Peaje = '';
            $scope.Modelo.Ruta = '';
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarRutas');
            closeModal('modalMensajeEliminarRutas');
        }

        /*$scope.MaskNumero = function () {
            $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno)

        };*/
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };

        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

    }]);