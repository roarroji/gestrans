﻿SofttoolsApp.controller("GestionarValorCombustibleCtrl", ['$scope', '$linq', 'blockUI', '$routeParams', 'blockUIConfig', 'ValorCatalogosFactory', 'ValorCombustibleFactory',
    function ($scope, $linq, blockUI, $routeParams, blockUIConfig, ValorCatalogosFactory, ValorCombustibleFactory ) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Valor Combustible' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

         
      
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.FechaActual = new Date();
        $scope.BloqueoFecha = true;


        $scope.Titulo = 'GESTIONAR VALOR COMBUSTIBLE';

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        //---------------------Validacion de permisos --------------------///
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ORDEN_SERVICIO); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }



        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);


        ///////////////Funcion lista de catalogo
        $scope.tipoCombustible = ValorCatalogosFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_COMBUSTIBLE },
            Sync: true
        }).Datos;


        $scope.tipoCombustible.push({
            Nombre: '(TODOS)',
            Codigo: -1
        })


        //Funcion Nuevo Valor
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            TipoCombustible: $linq.Enumerable().From($scope.tipoCombustible).First('$.Codigo==-1'),
            Valor: '',
            Fecha:  ''

        };

        ///////////////////Botones/////////

        $scope.Regresar = function () { 
            console.log('regresando')
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!ConsultarValorCombustible';
            } 
        }; 


        $scope.ConfirmarGuardar = function () {
           
            showModal('modaConfirmar'); 

        };
         
        $scope.Guardar = function () {
           
            $scope.Modelo.Fecha = $scope.FechaActual
            if (DatosRequeridos()) {

                ValorCombustibleFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos > 0) {
                                closeModal('modaConfirmar');
                                ShowSuccess('Se guardó el valor combustible')
                                location.href = '#!ConsultarValorCombustible/' + $scope.Modelo.TipoCombustible.Codigo;
                            }
                            else {
                                ShowError(response.statusText)
                            }
                        } else {
                            ShowError('No se guardo el Valor')

                        }
                    }, function (response) {
                        closeModal('modaConfirmar');
                        ShowError('Lo sentimos El tipo de Combustible <br> ' + $scope.Modelo.TipoCombustible.Nombre + ' <br>ya se encuentra registrado, por favor cambie el tipo de combustible o la fecha registrada ')
                    });
            }
            $scope.MaskValores = function () {
                MascaraValoresGeneral($scope)
            }
        }



        function DatosRequeridos() {
            $scope.MensajesError = [];
            window.scrollTo(top, top);
            var modelo = $scope.Modelo
            var continuar = true;
            console.log('codigo:',$scope.Modelo.TipoCombustible.Codigo)
            if ($scope.Modelo.TipoCombustible.Codigo == -1) {
                closeModal('modaConfirmar');  
                $scope.MensajesError.push('Debe  Seleccionar Un Tipo Combustible');
                continuar = false;
            }
            if ($scope.Modelo.Valor == '') {
                $scope.MensajesError.push('Debe Ingresar un valor para el Combustible  ');
                closeModal('modaConfirmar');
                console.log('valor', $scope.Modelo.Valor)
                continuar = false;
            } 

            return continuar;
        }


    }

]);
