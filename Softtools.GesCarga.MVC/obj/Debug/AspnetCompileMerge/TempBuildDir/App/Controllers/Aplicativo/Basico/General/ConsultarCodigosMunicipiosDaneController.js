﻿SofttoolsApp.controller('ConsultarCodigosMunicipiosDaneCtrl', ['$scope', '$timeout', '$linq', 'blockUI', '$routeParams', 'CodigosDaneCiudadesFactory',
    function ($scope, $timeout, $linq, blockUI, $routeParams, CodigosDaneCiudadesFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Códigos Municipios DANE' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CODIGOS_MUNICIPIOS_DANE);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.MensajesError = [];
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPaginas = 10;


        $scope.Codigo = '';
        $scope.Nombre = '';
        $scope.CodigoCiudad = '';
        $scope.CodigoDepartamento = '';
        $scope.Master = 0;

        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarCodigosMunicipiosDane';
            }
        };

        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Master = 1;
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }


        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN E IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        /*Metodo buscar*/
        $scope.Buscar = function () {

            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Master = 0;
                Find()
            }
        };

        function Find() {
            $scope.ListadoUnidades = [];
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];

            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,                       
                        Nombre: $scope.Nombre,
                        Codigo: $scope.Codigo,
                        Ciudad: { Codigo: $scope.CodigoCiudad },
                        Departamento: { Codigo: $scope.CodigoDepartamento },
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        Master : $scope.Master
                    }
                    blockUI.delay = 1000;
                    CodigosDaneCiudadesFactory.Consultar(filtros).
                        then(function (response) {
                            $scope.ListadoCodigos = [];
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoCodigos = response.data.Datos;
                                    console.log(response.data.Datos);
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }

                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }


        //Eliminar:

        $scope.EliminarMunicipio = function (codigo, Nombre) {
            $scope.CodigoMunicipio = codigo
            $scope.NombreMunicipio = Nombre


            showModal('modalEliminarMunicipio');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.CodigoMunicipio,
            };

            CodigosDaneCiudadesFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Codigo = '';
                        ShowSuccess('Se eliminó el Municipio ' + $scope.NombreMunicipio);
                        closeModal('modalEliminarMunicipio');
                        $scope.CodigoAlterno = '';
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarMunicipio');
                    $scope.ModalError = 'No se puede eliminar el Municipio  ' + $scope.NombreMunicipio + ' ya que se encuentra relacionado con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };


        $scope.CerrarModal = function () {
            closeModal('modalEliminarMunicipio');
            closeModal('modalMensajeEliminarMunicipio');
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        };
    }
]);