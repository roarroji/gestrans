﻿SofttoolsApp.controller('ConsultarUnidadesEmpaqueCtrl', ['$scope','$timeout','$routeParams','$linq','blockUI','UnidadesEmpaqueProductosTransportadosFactory',
    function ($scope, $timeout, $routeParams, $linq, blockUI, UnidadesEmpaqueProductosTransportadosFactory) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Unidades Empaque' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_UNIDADES_EMPAQUE);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.MensajesError = [];
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPaginas = 10;

        $scope.Codigo = '';
        $scope.Nombre = '';
        $scope.CodigoAlterno = '';

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 }
        ];

        
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');


        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarUnidadesEmpaque';
            }
        };

        /* Obtener parametros*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }

        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };



        /*Metodo buscar*/
        $scope.Buscar = function () {

            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {               
                    $scope.Codigo = 0
                    Find()                
            }
        };

        function Find() {
            $scope.ListadoUnidades = [];
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            
            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CodigoAlterno: $scope.CodigoAlterno,
                        Nombre: $scope.Nombre,
                        Codigo: $scope.Codigo,
                        Estado: $scope.Estado.Codigo,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    }
                    blockUI.delay = 1000;
                    UnidadesEmpaqueProductosTransportadosFactory.Consultar(filtros).
                        then(function (response) {
                            $scope.ListadoUnidades = [];
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoUnidades = response.data.Datos;
                                    console.log(response.data.Datos);
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }

        //Eliminar:

        $scope.EliminarUnidad = function (codigo, Nombre) {
            $scope.CodigoUnidad = codigo
            $scope.NombreUnidad = Nombre
            
           
            showModal('modalEliminarUnidad');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.CodigoUnidad,
            };

            UnidadesEmpaqueProductosTransportadosFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó la Unidad ' + $scope.NombreUnidad);
                        closeModal('modalEliminarUnidad');
                        $scope.CodigoAlterno = '';
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                        showModal('modalMensajeEliminarUnidades');
                        $scope.ModalError = 'No se puede eliminar la Unidad  ' + $scope.NombreUnidad + ' ya que se encuentra relacionada con ' +result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };


        $scope.CerrarModal = function () {
            closeModal('modalEliminarUnidad');
            closeModal('modalMensajeEliminarUnidades');
        }

        $scope.MaskMayus = function () {
            try { $scope.Nombre = $scope.Nombre.toUpperCase() } catch (e) { }
        };
    }
]);