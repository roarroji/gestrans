﻿SofttoolsApp.controller("GestionarProductoTransportadosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ProductosMinisterioTransporteFactory', 'ValorCatalogosFactory', 'UnidadMedidaFactory', 'UnidadEmpaqueFactory', 'ProductoTransportadosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ProductosMinisterioTransporteFactory, ValorCatalogosFactory, UnidadMedidaFactory, UnidadEmpaqueFactory, ProductoTransportadosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Productos Transportados' }, { Nombre: 'Gestionar' }];
        $scope.Titulo = 'GESTIONAR PRODUCTOS TRANSPORTADOS'
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PRODUCTO_TRANSPORTADOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/

        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.Deshabilitar = false
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoCiudades = [];
        $scope.ListadoUnidadMedida = [];
        $scope.ListadoUnidadEmpaque = [];
        $scope.ListadoLineaProducto = [];
        $scope.ListadoNivelRiesgo = [];

        $scope.ListadoNaturalezaProducto = [];
        $scope.ModeloAplicaTarifa = false;
        $scope.AplicaTarifario = 0;
        $scope.CodigoGproducto = 0;
        $scope.AplicaManifiesto = false;

        $scope.ListadoEstados = [
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 }
        ];
        $scope.ModeloEstado = $scope.ListadoEstados[0]

        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.Codigo = $routeParams.Codigo;
        }
        else {
            $scope.Codigo = 0
        }

        //----------------------------------------------------------------------------Asignaciones -------------------------------------------------------------

        if ($scope.Sesion.Empresa.GeneraManifiesto == 1) {
            $scope.AplicaManifiesto = true
        } else {
            $scope.AplicaManifiesto = false
        }

        ProductosMinisterioTransporteFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoProductos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoProductos = response.data.Datos;
                        for (i = 0; i < $scope.ListadoProductos.length; i++) {
                            $scope.ListadoProductos[i].NombreCompleto = $scope.ListadoProductos[i].Nombre + ' - ' + $scope.ListadoProductos[i].CodigoAlterno.toString();
                        }
                    }
                    else {
                        $scope.ListadoProductos = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });


        $scope.ObtenerCamposCod = function (producto) {

            if (producto.Codigo !== undefined) {
                $scope.Productos = producto.Nombre;
                filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Nombre: $scope.Productos,
                    CodigoAlterno: producto.CodigoAlterno
                };
                EjecutarFactory(filtro);
            } else {
                $scope.Producto = '';
                $scope.CodigoAlterno = '';
                $scope.Descripcion = '';
                $scope.CodigoMinisterio = '';

            }
        };

        $scope.ObtenerCamposNom = function (codigo) {
            $scope.CodigoNombre = codigo;
            if ($scope.CodigoNombre !== undefined && $scope.CodigoNombre !== '') {
                filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoAlterno: $scope.CodigoNombre
                };
                EjecutarFactory(filtro);
            } else {
                $scope.Producto = '';
                $scope.CodigoAlterno = '';
                $scope.Descripcion = '';
                $scope.CodigoMinisterio = '';
            }
        };

        function EjecutarFactory(filtro) {
            ProductosMinisterioTransporteFactory.Obtener(filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Codigo > 0) {
                            $scope.CodigoAlterno = response.data.Datos.CodigoAlterno
                            $scope.Producto = response.data.Datos
                            $scope.Descripcion = response.data.Datos.Descripcion
                            $scope.CodigoMinisterio = response.data.Datos.CodigoAlterno
                        } else {
                            ShowError('El código del producto ministerio ingresado no existe');
                            $scope.Producto = '';
                            $scope.CodigoAlterno = '';
                            $scope.Descripcion = '';
                            $scope.CodigoMinisterio = '';
                        }
                    } else {
                        ShowError('No se logro consultar el producto ministerio');
                    }


                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        //---------------------------------------COMBOS Y AUTOCOMPLETE---------------------------------------------------

        UnidadMedidaFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoUnidadMedida = response.data.Datos;
                        if ($scope.CodigoUnidadMedida !== undefined && $scope.CodigoUnidadMedida !== '' && $scope.CodigoUnidadMedida !== null) {
                            $scope.UnidadMedida = $linq.Enumerable().From($scope.ListadoUnidadMedida).First('$.Codigo ==' + $scope.CodigoUnidadMedida);
                        }
                        else {
                            $scope.UnidadMedida = $scope.ListadoUnidadMedida[$scope.ListadoUnidadMedida.length];
                        }
                    }
                    else {
                        $scope.ListadoUnidadMedida = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        UnidadEmpaqueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoUnidadEmpaque = response.data.Datos;
                        if ($scope.CodigoUnidadEmpaque !== undefined && $scope.CodigoUnidadEmpaque !== '' && $scope.CodigoUnidadEmpaque !== null) {
                            $scope.UnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo ==' + $scope.CodigoUnidadEmpaque);
                        }
                        else {
                            $scope.UnidadEmpaque = $scope.ListadoUnidadEmpaque[$scope.ListadoUnidadEmpaque.length];
                        }
                    }
                    else {
                        $scope.ListadoUnidadEmpaque = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_LINEA_PRODUCTOS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoLineaProducto = response.data.Datos;
                        if ($scope.CodigoLineaProducto !== undefined && $scope.CodigoLineaProducto !== '' && $scope.CodigoLineaProducto !== null) {
                            $scope.LineaProducto = $linq.Enumerable().From($scope.ListadoLineaProducto).First('$.Codigo ==' + $scope.CodigoLineaProducto);
                        }
                        else {
                            $scope.LineaProducto = $scope.ListadoLineaProducto[$scope.ListadoLineaProducto.length];
                        }
                    }
                    else {
                        $scope.ListadoLineaProducto = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NIVEL_RIESGO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoNivelRiesgo = response.data.Datos;
                        if ($scope.CodigoNivelRiesgo !== undefined && $scope.CodigoNivelRiesgo !== '' && $scope.CodigoNivelRiesgo !== null) {
                            $scope.NivelRiesgo = $linq.Enumerable().From($scope.ListadoNivelRiesgo).First('$.Codigo ==' + $scope.CodigoNivelRiesgo);
                        }
                        else {
                            $scope.NivelRiesgo = $scope.ListadoNivelRiesgo[$scope.ListadoNivelRiesgo.length];
                        }
                    }
                    else {
                        $scope.ListadoNivelRiesgo = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });


        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NATURALEZA_PRODUCTOS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoNaturalezaProducto = response.data.Datos;
                        if ($scope.CodigoNaturalezaProducto !== undefined && $scope.CodigoNaturalezaProducto !== '' && $scope.CodigoNaturalezaProducto !== null) {
                            $scope.NaturalezaProducto = $linq.Enumerable().From($scope.ListadoNaturalezaProducto).First('$.Codigo ==' + $scope.CodigoNaturalezaProducto);
                        }
                        else {
                            $scope.NaturalezaProducto = $scope.ListadoNaturalezaProducto[$scope.ListadoNaturalezaProducto.length];
                        }
                    }
                    else {
                        $scope.ListadoNaturalezaProducto = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        if ($scope.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR PRODUCTO TRANSPORTADO';
            $scope.Deshabilitar = true;
            ObtenerProducto();
        }
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function ObtenerProducto() {
            blockUI.start('Cargando Producto Código ' + $scope.Codigo);
            $timeout(function () {
                blockUI.message('Cargando Producto Código ' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            blockUI.delay = 1000;
            ProductoTransportadosFactory.Obtener(filtros).
                then(function (response) {

                    if (response.data.ProcesoExitoso === true) {
                        $scope.Nombre = response.data.Datos.Nombre;
                        $scope.Producto = response.data.Datos.ProductoMinisterio,
                            $scope.Descripcion = response.data.Datos.Descripcion;
                        $scope.CodigoMinisterio = response.data.Datos.CodigoAlterno;
                        $scope.Peso = response.data.Datos.Peso;
                        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);

                        $scope.ObtenerCamposNom($scope.CodigoMinisterio);

                        if (response.data.Datos.AplicaTarifario == 1) {
                            $scope.ModeloAplicaTarifa = true;
                        } else {
                            $scope.ModeloAplicaTarifa = false;
                        }
                        if ($scope.Sesion.Empresa.GeneraManifiesto == 1) {
                            if (response.data.Datos.CodigoAlterno === null && response.data.Datos.CodigoAlterno === undefined && response.data.Datos.CodigoAlterno === 0) {
                                $scope.CodigoMinisterio = ''
                            } else {
                                $scope.CodigoMinisterio = response.data.Datos.CodigoAlterno;

                            }
                        } else {
                            if (response.data.Datos.CodigoAlterno === null && response.data.Datos.CodigoAlterno === undefined && response.data.Datos.CodigoAlterno === 0) {
                                $scope.CodigoAlterno = ''
                            } else {
                                $scope.CodigoAlterno = response.data.Datos.CodigoAlterno;
                            }
                        }

                        $scope.CodigoUnidadMedida = response.data.Datos.UnidadMedida
                        if ($scope.ListadoUnidadMedida.length > 0 && $scope.CodigoUnidadMedida > 0) {
                            $scope.UnidadMedida = $linq.Enumerable().From($scope.ListadoUnidadMedida).First('$.Codigo == ' + response.data.Datos.UnidadMedida);
                        }

                        $scope.CodigoUnidadEmpaque = response.data.Datos.UnidadEmpaque
                        if ($scope.ListadoUnidadEmpaque.length > 0 && $scope.CodigoUnidadEmpaque > 0) {
                            $scope.UnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo == ' + response.data.Datos.UnidadEmpaque);
                        }

                        $scope.CodigoLineaProducto = response.data.Datos.LineaProducto
                        if ($scope.ListadoLineaProducto.length > 0 && $scope.CodigoLineaProducto > 0) {
                            $scope.LineaProducto = $linq.Enumerable().From($scope.ListadoLineaProducto).First('$.Codigo == ' + response.data.Datos.LineaProducto);
                        }
                        $scope.CodigoNivelRiesgo = response.data.Datos.NivelRiesgo
                        if ($scope.ListadoNivelRiesgo.length > 0 && $scope.CodigoNivelRiesgo > 0) {
                            $scope.NivelRiesgo = $linq.Enumerable().From($scope.ListadoNivelRiesgo).First('$.Codigo == ' + response.data.Datos.NivelRiesgo);
                        }
                        $scope.CodigoNaturalezaProducto = response.data.Datos.NaturalezaProducto
                        if ($scope.ListadoNaturalezaProducto.length > 0 && $scope.CodigoNaturalezaProducto > 0) {
                            $scope.NaturalezaProducto = $linq.Enumerable().From($scope.ListadoNaturalezaProducto).First('$.Codigo == ' + response.data.Datos.NaturalezaProducto);
                        }

                    }
                    else {
                        ShowError('No se logro consultar el producto ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarProductoTransportados';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarProductoTransportados';
                });
            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
        };
        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardar');
            console.log('Guardando')

            if ($scope.Producto.Codigo == undefined) {
                $scope.codigoProducto = $scope.CodigoMinisterio
                console.log('producto1', $scope.codigoProducto)
            }
            else {
                $scope.codigoProducto = $scope.Producto.Codigo
                console.log('producto2', $scope.codigoProducto)
            }

            var codigoalterno = 0;
            if ($scope.Sesion.Empresa.GeneraManifiesto == 1) {
                if ($scope.CodigoMinisterio == null || $scope.CodigoMinisterio == undefined || $scope.CodigoMinisterio == '') {
                    codigoalterno = 0;
                } else {
                    codigoalterno = $scope.CodigoMinisterio
                }
            } else {
                if ($scope.CodigoAlterno == null || $scope.CodigoAlterno == undefined || $scope.CodigoAlterno == '') {
                    codigoalterno = $scope.CodigoAlterno;
                } else {
                    codigoalterno = 0;
                }
            }

            if ($scope.ModeloAplicaTarifa == true) {
                $scope.AplicaTarifario = 1
            } else {
                $scope.AplicaTarifario = 0
            }
            $scope.DatosGuardar = {

                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                CodigoAlterno: codigoalterno,
                Nombre: $scope.Nombre,
                Descripcion: $scope.Descripcion,
                UnidadMedida: $scope.UnidadMedida.Codigo,
                UnidadEmpaque: $scope.UnidadEmpaque.Codigo,
                LineaProducto: $scope.LineaProducto.Codigo,
                NaturalezaProducto: $scope.NaturalezaProducto.Codigo,
                Estado: $scope.ModeloEstado.Codigo,
                UsuarioCrea: $scope.Sesion.UsuarioAutenticado,
                AplicaTarifario: $scope.AplicaTarifario,
                Peso: $scope.Peso,
                NivelRiesgo: $scope.NivelRiesgo.Codigo,
                CodigoProductoMinisterio: $scope.codigoProducto

            };


            $scope.ModeloEstado = $scope.ModeloEstado.Codigo;
            console.log('Guardando', $scope.DatosGuardar)
            ProductoTransportadosFactory.Guardar($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Codigo === 0) {
                                ShowSuccess('Se guardó el Producto : ' + $scope.Nombre);
                            }
                            else {
                                ShowSuccess('Se modificó el Producto : ' + $scope.Nombre);
                            }
                            location.href = '#!ConsultarProductoTransportados/' + $scope.Codigo;
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };


        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        function DatosRequeridos() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Nombre === undefined || $scope.Nombre === "" || $scope.Nombre === null) {
                $scope.MensajesError.push('Debe ingresar el nombre del Producto');
                continuar = false;
            }

            if ($scope.ModeloEstado === undefined || $scope.ModeloEstado === "" || $scope.ModeloEstado === null) {
                $scope.MensajesError.push('Debe ingresar el Estado');
                continuar = false;
            }

            if ($scope.UnidadEmpaque === undefined || $scope.UnidadEmpaque === "" || $scope.UnidadEmpaque === null) {
                $scope.MensajesError.push('Debe ingresar la Unidad de Empaque');
                continuar = false;
            }

            if ($scope.UnidadMedida === undefined || $scope.UnidadMedida === "" || $scope.UnidadMedida === null) {
                $scope.MensajesError.push('Debe ingresar la Unidad de Medida');
                continuar = false;
            }
            if ($scope.NaturalezaProducto === undefined || $scope.NaturalezaProducto === "" || $scope.NaturalezaProducto === null) {
                $scope.MensajesError.push('Debe Ingresar la Naturaleza del Producto');
                continuar = false;
            }

            if ($scope.LineaProducto === undefined || $scope.LineaProducto === "" || $scope.LineaProducto === null) {
                $scope.MensajesError.push('Debe Ingresar la Linea del Producto');
                continuar = false;
            }
            if ($scope.NivelRiesgo === undefined || $scope.NivelRiesgo === "" || $scope.NivelRiesgo === null) {
                $scope.MensajesError.push('Debe Ingresar El Nivel Riesgo');
                continuar = false;
            }


            return continuar;
        }

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        };

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);

        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope);
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($routeParams.Codigo > 0) {
                document.location.href = '#!ConsultarProductoTransportados/' + $routeParams.Codigo;
            } else {
                document.location.href = '#!ConsultarProductoTransportados';
            }
        };
    }]);