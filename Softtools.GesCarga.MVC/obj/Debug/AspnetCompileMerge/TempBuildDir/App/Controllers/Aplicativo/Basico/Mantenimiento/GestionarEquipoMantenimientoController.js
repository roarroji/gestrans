﻿SofttoolsApp.controller("GestionarEquipoMantenimientoCtrl", ['$scope', '$routeParams', 'blockUI',  'blockUIConfig','$timeout', 'ValorCatalogosFactory', '$linq', 'TercerosFactory', 'EquipoMantenimientoFactory', 'VehiculosFactory', 'EstadoEquipoMantenimientoFactory', 'MarcaEquipoMantenimientoFactory', 'UnidadReferenciaMantenimientoFactory', 'TipoEquipoMantenimientoFactory', 'DocumentoEquipoMantenimientoFactory',
    function ($scope, $routeParams, blockUI, blockUIConfig, $timeout, ValorCatalogosFactory, $linq, TercerosFactory,
        EquipoMantenimientoFactory, VehiculosFactory, EstadoEquipoMantenimientoFactory, MarcaEquipoMantenimientoFactory, UnidadReferenciaMantenimientoFactory, TipoEquipoMantenimientoFactory, DocumentoEquipoMantenimientoFactory) {
        $scope.MensajesError = [];
        $scope.MensajesErrorDetalle = [];
        $scope.ListadoEstados = [];
        $scope.ModificarDetalle = 0;
        $scope.Operacion = 'Adicionar';
        $scope.Propietario = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_EQUIPO_MANTENIMIENTO);
        $scope.ListadoDocumentos = [{
            idRow: 1,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Referencia: '',
            Documento: '',
            Extencion: ''
        }]
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        // Se crea la propiedad modelo en el ambito
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Mantenimiento' }, { Nombre: 'Equipos ' }];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        };
        $scope.Modelo.ID_Documentos_Eliminar = [];

        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
        }
        else {
            $scope.Modelo.Codigo = 0;
        }
        $scope.ListadoPropietarios = [];
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROPIETARIO}).
            then(function (response) {
                if (response.data.ProcesoExitoso == true) {
                    for (var i = 0; i < response.data.Datos.length; i++) {
                       // if (response.data.Datos[i].CadenaPerfiles == PERFIL_PROPIETARIO) {
                            $scope.ListadoPropietarios.push(response.data.Datos[i]);
                       // }
                    }
                }
            });
            
            
            
        
       
         ///////////////////PLACA

        $scope.AutocompleteVehiculos = function (value) {
            VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, ValorAutocomplete: value }).
                then(function (response) {
                    console.log(response) 
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos.length > 0) {
                            //response.data.Datos.push({ Placa: '', Codigo: 0 })
                            $scope.ListaPlaca = response.data.Datos;
                            /*if ($scope.Placa == undefined || $scope.Placa == null || $scope.Placa == '') {
                                $scope.Placa = $scope.ListaPlaca[$scope.ListaPlaca.length - 1];
                            }
                            if ($scope.CodigoPlaca != undefined && $scope.CodigoPlaca > 0) {
                                $scope.Placa = $linq.Enumerable().From($scope.ListaPlaca).First('$.Codigo == ' + $scope.CodigoPlaca);
                            }*/
                        }
                        else {
                            $scope.ListaPlaca = [];
                        }
                    } else {
                        ShowError(response.statusText);
                    }

                }, function (response) {
                    ShowError(response.statusText);
                });
            return $scope.ListaPlaca;
        }
        
        $scope.AsignarDatosVehiculo = function (item) {
            if (item.Modelo != undefined) {
                if (item.Modelo.toString().trim().length > 0) {
                    $scope.ModeloEquipo = item.Modelo.toString().trim()
                } else {
                    $scope.ModeloEquipo = 0
                }
            }
            if (item.NumeroMotor.toString().trim().length > 0) {
                $scope.Serie = item.NumeroMotor.toString().trim()
            } else {
                $scope.Serie = 0
            }
            $scope.Propietario = $linq.Enumerable().From($scope.ListadoPropietarios).First('$.Codigo == ' + item.Propietario.Codigo);
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar el combo de estados*/
     
        $scope.ListadoEstados = [];
        $scope.ListadoEstados = [
           
            { Codigo: 0, Nombre: 'Inactivo' },
            { Codigo: 1, Nombre: 'Activo' }
        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First("$.Codigo == 1");
    /*Cargar el combo Equipo  Mantenimiento*/
        EstadoEquipoMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: {Codigo: 1} }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEstadoMantenimiento = [];
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo != ESTADO_REGISTRO_NO_APLICA) {
                            $scope.ListadoEstadoMantenimiento.push(item);    
                        } 
                    });
                    $scope.EstadoMantenimiento = response.data.Datos[0];
                }
                $scope.ListadoEstadoMantenimiento.push({ Nombre: '(TODOS)', Codigo: -1 }); 
                $scope.EstadoMantenimiento = $linq.Enumerable().From($scope.ListadoEstadoMantenimiento).First("$.Codigo == -1");
             }, function (response) {
                ShowError(response.statusText);
            });
    /*Cargar el combo Marca Equipo  Mantenimiento*/
        MarcaEquipoMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoMarcasMantenimiento = [];
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo != ESTADO_REGISTRO_NO_APLICA) {
                            $scope.ListadoMarcasMantenimiento.push(item);
                        }
                    });
                    $scope.Marca = response.data.Datos[0];
                }
                $scope.ListadoMarcasMantenimiento.push({ Nombre: '(TODOS)', Codigo: -1 });
                $scope.Marca = $linq.Enumerable().From($scope.ListadoMarcasMantenimiento).First("$.Codigo == -1");
            }, function (response) {
                ShowError(response.statusText);
            });
    /*Cargar el combo Unidad frecuencia */
        //////////// El campo Todos lo ponenen en Undefine
    UnidadReferenciaMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoUnidadFrecuenciaMantenimiento = [];
                    $scope.ListadoUnidadFrecuenciaMantenimiento = response.data.Datos
                    $scope.UnidadFrecuencia = response.data.Datos[0];
                } else {
                    $scope.ListadoUnidadFrecuenciaMantenimiento = [];
                } 
                $scope.ListadoUnidadFrecuenciaMantenimiento.push({ NombreCorto: '(TODOS)', Codigo: -1 });
                $scope.UnidadFrecuencia = $linq.Enumerable().From($scope.ListadoUnidadFrecuenciaMantenimiento).First("$.Codigo == -1"); 
            }, function (response) {
                    ShowError(response.statusText);
            });


    /*Cargar el combo Tipo Equipo*/
        TipoEquipoMantenimientoFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoEquipo = [];
                    $scope.ListadoTipoEquipo = response.data.Datos
                    $scope.TipoEquipo = response.data.Datos[0];
                } else {
                    $scope.ListadoTipoEquipo = [];
                }
                $scope.ListadoTipoEquipo.push({ Nombre: '(TODOS)', Codigo: -1 });
                $scope.TipoEquipo = $linq.Enumerable().From($scope.ListadoTipoEquipo).First("$.Codigo == -1");

            }, function (response) {
                ShowError(response.statusText);
            });
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR EQUIPO';
            Obtener();
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando equipo  ' + $scope.Modelo.Codigo); 
            $timeout(function () {
                blockUI.message('Cargando equipo ' + $scope.Modelo.Codigo);
            }, 200); 
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;

           
            EquipoMantenimientoFactory.Obtener(filtros).
                then(function (response) {
                    //console.log('llegaron',response) 
                    if (response.data.ProcesoExitoso === true) { 
                        $scope.Nombre = response.data.Datos.Nombre
                        $scope.Codigo_Alterno = response.data.Datos.Codigo_Alterno
                        $scope.UnidadFrecuencia = $linq.Enumerable().From($scope.ListadoUnidadFrecuenciaMantenimiento).First('$.Codigo == ' + response.data.Datos.UnidadReferenciaMantenimiento.Codigo);
                        
                        $scope.ValorUso = response.data.Datos.Valor_Uso
                        $scope.Alto = response.data.Datos.Alto
                        $scope.CapacidadCargue = response.data.Datos.CapacidadCargue
                        $scope.Ancho = response.data.Datos.Ancho
                        $scope.Largo = response.data.Datos.Largo

                        $scope.EstadoMantenimiento = $linq.Enumerable().From($scope.ListadoEstadoMantenimiento).First('$.Codigo == ' + response.data.Datos.EstadoEquipoMantenimiento.Codigo);
                        $scope.TipoEquipo = $linq.Enumerable().From($scope.ListadoTipoEquipo).First('$.Codigo == ' + response.data.Datos.TipoEquipoMantenimiento.Codigo);
                        $scope.Marca = $linq.Enumerable().From($scope.ListadoMarcasMantenimiento).First('$.Codigo == ' + response.data.Datos.MarcaEquipoMantenimiento.Codigo);
                        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        $scope.Propietario = $scope.CargarTercero(response.data.Datos.Propietario.Codigo);

                        try {
                             $scope.Placa = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo);
                        } catch (e) {
                        
                            $scope.Placa = $scope.CargarVehiculos(response.data.Datos.Vehiculo.Codigo);
                            $scope.CodigoPlaca = response.data.Datos.Vehiculo.Codigo
                        }
                        $scope.Serie = response.data.Datos.Serie
                        $scope.ModeloEquipo = response.data.Datos.Modelo
                        if (response.data.Datos.DocumentosEquipoMantenimiento.length > 0) {
                            $scope.ListadoDocumentos = []
                        }
                        for (var i = 0; i < response.data.Datos.DocumentosEquipoMantenimiento.length; i++) {
                            $scope.ListadoDocumentos.push({
                                idRow: i + 1,
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Referencia: response.data.Datos.DocumentosEquipoMantenimiento[i].Referencia,
                                Documento: response.data.Datos.DocumentosEquipoMantenimiento[i].Documento,
                                Extencion: 'pdf',
                                ValorDocumento: response.data.Datos.DocumentosEquipoMantenimiento[i].ValorDocumento,
                                ID_Documento: response.data.Datos.DocumentosEquipoMantenimiento[i].ID_Documento
                            })
                        }
                    }
                    else {
                        ShowError('No se logro consultar el equipo ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarEquipoMantenimiento';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    //ShowError('No se logro consultar el plan de mantenimiento No.' + $scope.Modelo.Codigo + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!ConsultarEquipoMantenimiento';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($scope.Modelo.Codigo > 0) {
                document.location.href = '#!ConsultarEquipoMantenimiento/' + $scope.Modelo.Codigo;
            }
            else {
                document.location.href = '#!ConsultarEquipoMantenimiento';
            }
            
        };

        // Metodo para guardar /modificar

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        }
        $scope.Guardar = function () {

            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) { 
                blockUI.start('Guardando archivos');
                $scope.Modelo.Nombre = $scope.Nombre
                $scope.Modelo.Codigo_Alterno = $scope.Codigo_Alterno
                $scope.Modelo.UnidadReferenciaMantenimiento = $scope.UnidadFrecuencia
                $scope.Modelo.Valor_Uso = MascaraNumero($scope.ValorUso);
                $scope.Modelo.EstadoEquipoMantenimiento = $scope.EstadoMantenimiento
                $scope.Modelo.TipoEquipoMantenimiento = $scope.TipoEquipo
                $scope.Modelo.Vehiculo = $scope.Placa
                $scope.Modelo.MarcaEquipoMantenimiento = $scope.Marca
                $scope.Modelo.Propietario = { Codigo: $scope.Propietario.Codigo }
                $scope.Modelo.Serie = $scope.Serie 
                $scope.Modelo.Modelo = $scope.ModeloEquipo 
                $scope.Modelo.CapacidadCargue = $scope.CapacidadCargue
                $scope.Modelo.Ancho = $scope.Ancho
                $scope.Modelo.Largo = $scope.Largo
                $scope.Modelo.Alto = $scope.Alto 
                $scope.Modelo.Estado = $scope.Estado.Codigo
                var ListadDocumentosConfirmados = []
                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                    if ($scope.ListadoDocumentos[i].Referencia !== '' && $scope.ListadoDocumentos[i].Referencia !== undefined) {
                        if ($scope.ListadoDocumentos[i].Documento !== '' && $scope.ListadoDocumentos[i].Documento !== undefined) {
                            ListadDocumentosConfirmados.push($scope.ListadoDocumentos[i])
                        }
                    }
                }
                $scope.Modelo.DocumentosEquipoMantenimiento = ListadDocumentosConfirmados
                console.log('insertar',$scope.Modelo)

                EquipoMantenimientoFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    blockUI.stop();
                                    ShowSuccess('Se guardó el equipo ' + $scope.Modelo.Nombre);
                                    location.href = '#!ConsultarEquipoMantenimiento/' + response.data.Datos;
                                }
                                else {
                                    blockUI.stop();
                                    ShowSuccess('Se modificó el equipo ' + $scope.Modelo.Nombre);
                                    location.href = '#!ConsultarEquipoMantenimiento/' + $scope.Modelo.Codigo;
                                }

                            }
                            else {
                                blockUI.stop();
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            blockUI.stop();
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                blockUI.stop();

            }
        } 

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Valida los datos requridos 
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Nombre == '' || $scope.Nombre == undefined) {
                $scope.MensajesError.push('Debe ingresar el nombre del equipo de mantenimiento');
                continuar = false;
            } 
            if ($scope.UnidadFrecuencia.Codigo == -1 || $scope.UnidadFrecuencia == undefined) {
                $scope.MensajesError.push('Debe ingresar la unidad de frecuencia');
                continuar = false;
            }
            if ($scope.ValorUso == '' || $scope.ValorUso == undefined) {
                $scope.MensajesError.push('Debe ingresar el valor actual de uso');
                continuar = false;
            }
            if ($scope.EstadoMantenimiento.Codigo == -1 || $scope.EstadoMantenimiento == undefined) {
                $scope.MensajesError.push('Debe ingresar el estado de mantenimiento');
                continuar = false;
            }
            if ($scope.TipoEquipo.Codigo == -1  || $scope.TipoEquipo == undefined) {
                $scope.MensajesError.push('Debe ingresar el tipo de equipo');
                continuar = false;
            }
            if ($scope.Propietario == '' || $scope.Propietario == undefined) {
                $scope.MensajesError.push('Debe ingresar el propietario');
                continuar = false;
            } else if ($scope.Propietario.Codigo == 0 || $scope.Propietario.Codigo == undefined) {
                $scope.MensajesError.push('Debe ingresar un propietario valido');
                continuar = false;
            }
            if ($scope.Placa == '' || $scope.Placa == undefined) {
                $scope.MensajesError.push('Debe ingresar la placa');
                continuar = false;
            } else if ($scope.Placa.Codigo == 0 || $scope.Placa.Codigo == undefined) {
                $scope.MensajesError.push('Debe ingresar una placa válida  que corresponda a un vehiculo registrado');
                continuar = false;
            }
            if ($scope.Marca.Codigo == -1 || $scope.Marca == undefined) {
                $scope.MensajesError.push('Debe ingresar la marca');
                continuar = false;
            }
            if ($scope.Serie == '' || $scope.Serie == undefined) {
                $scope.ModeloEquipo = 0
                continuar = false;
            }
            if ($scope.ModeloEquipo == '(TODOS)' || $scope.ModeloEquipo == undefined) {
                $scope.ModeloEquipo = 0
            }
            if ($scope.Estado == '' || $scope.Estado == undefined) {
                $scope.MensajesError.push('Debe ingresar el estado del equipo de mantenimiento');
                continuar = false;
            }
            var counterrors = 0
            for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                if ($scope.ListadoDocumentos[i].Documento !== '' && $scope.ListadoDocumentos[i].Documento !== undefined) {
                    if ($scope.ListadoDocumentos[i].Referencia == '' || $scope.ListadoDocumentos[i].Referencia == undefined) {
                        counterrors++
                    }
                }
            }
            if (counterrors > 0) {
                $scope.MensajesError.push('Debe ingresar las referencias de todos los documentos cargados');
                continuar = false;
            }

            return continuar

        }

        $scope.MaskMayus = function () {
            try { $scope.Nombre = $scope.Nombre.toUpperCase() } catch (e) { }
        };
        $scope.NuevoDocumento = function () {
            var countRow = $scope.ListadoDocumentos.length + 1
            if ($scope.ListadoDocumentos[$scope.ListadoDocumentos.length - 1].Referencia == '' || $scope.ListadoDocumentos[$scope.ListadoDocumentos.length - 1].Referencia == undefined
                || $scope.ListadoDocumentos[$scope.ListadoDocumentos.length - 1].Documento == '' || $scope.ListadoDocumentos[$scope.ListadoDocumentos.length - 1].Referencia == undefined
            ) {

                ShowError("Para adicionar un nuevo documento, el documento anterior debe tener un archivo y una referencia asignada");

            } else {
                $scope.ListadoDocumentos.push({
                    idRow: countRow,
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Referencia: '',
                    Documento: '',
                    Extencion: ''
                })
            }
        }
        var ref = ''
        var idRowInsert = 0
        var archivo
        $scope.CargarPDF = function (element) {
            var continuar = true;
            $scope.PDF = element
            blockUI.start();
            $scope.ObjetoListado = angular.element(this.item);


            if (element === undefined) {
                ShowError("Solo se pueden cargar archivos en formato PDF", false);
                continuar = false;
            }

            if (continuar == true) {
                if (element.files[0].type !== 'application/pdf' && element.files[0].type !== 'application/pdf') {
                    ShowError("Solo se pueden cargar archivos en formato PDF", false);
                    continuar = false;
                }
                else if (element.files[0].size >= 3000000) //3 MB
                {
                    ShowError("El archivo " + element.files[0].name + " supera los 3 MB, intente con otro archivo", false);
                    continuar = false;
                }
            }

            if (continuar == true) {
                if ($scope.ObjetoListado[0].Documento) {
                    archivo = element
                    showModal('modalConfirmacionRemplazarDocumento');
                    ref = this.item.Referencia
                    idRowInsert = this.item.idRow
                }
                else {
                    var reader = new FileReader();
                    ref = this.item.Referencia
                    idRowInsert = this.item.idRow
                    reader.onload = $scope.AsignarPDFListado;
                    reader.readAsDataURL(element.files[0]);
                    ShowSuccess('Se cargó el documento adjunto "' + element.files[0].name + '"');
                }
            }
            blockUI.stop();
        }
        $scope.RemplazarDocumento = function () {
            var reader = new FileReader();
            reader.onload = $scope.AsignarPDFListado;
            reader.readAsDataURL(archivo.files[0]);
            ShowSuccess('Se cargó el documento adjunto "' + archivo.files[0].name + '"');
            closeModal('modalConfirmacionRemplazarDocumento');
        }

        $scope.AsignarPDFListado = function (e) {
            $scope.$apply(function () {
                var ConvertirPDF = null
                if ($scope.PDF.files[0].type == 'application/pdf') {
                    ConvertirPDF = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                }
                var ID_Modifica = 0
                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                    if ($scope.ListadoDocumentos[i].idRow == idRowInsert) {
                        if ($scope.ListadoDocumentos[i].ID_Documento !== '' && $scope.ListadoDocumentos[i].ID_Documento !== undefined && $scope.ListadoDocumentos[i].ID_Documento > 0) {
                            ID_Modifica = $scope.ListadoDocumentos[i].ID_Documento
                        }
                    }
                }
                DocumentoEquipoMantenimientoFactory.InsertarTemporal({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Modelo.Codigo, Documento: ConvertirPDF, ID_Documento_Modifica: ID_Modifica }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                                    if ($scope.ListadoDocumentos[i].idRow == idRowInsert) {
                                        if ($scope.ListadoDocumentos[i].ID_Documento !== '' && $scope.ListadoDocumentos[i].ID_Documento !== undefined && $scope.ListadoDocumentos[i].ID_Documento > 0) {
                                            $scope.ListadoDocumentos[i].ID_Documento_temporal = response.data.Datos
                                            $scope.ListadoDocumentos[i].temp = 1
                                        }
                                        else {
                                            $scope.ListadoDocumentos[i].ID_Documento_temporal = response.data.Datos
                                            $scope.ListadoDocumentos[i].temp = 1
                                            $scope.ListadoDocumentos[i].CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                                            $scope.ListadoDocumentos[i].Referencia = ref
                                            $scope.ListadoDocumentos[i].Documento = 1
                                            $scope.ListadoDocumentos[i].Extencion = 'pdf'
                                        }
                                        idRowInsert = 0
                                    }
                                }
                            }
                            else {
                                blockUI.stop();
                                idRowInsert = 0
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            blockUI.stop();
                            idRowInsert = 0
                            ShowError(response.data.MensajeOperacion);
                        }
                        ref = ''

                    }, function (response) {
                        idRowInsert = 0
                        ShowError(response.statusText);
                        ref = ''

                    });

            });
        }
        $scope.ResetTemp = function () {
            idRowInsert = 0
            ref = ''
        }
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (ID_Documento, Referencia) {
            if (Referencia == '' || Referencia == undefined) {
                Referencia = ''
            }
            window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=EquipoMantenimiento&Codigo=' + ID_Documento + '&Nombrefile=' + Referencia);
        };

        $scope.SolicitarDatosEliminarDocumento = function (EliminarDocumento) {
            $scope.EliminarDocumento = EliminarDocumento
            showModal('modalConfirmacionEliminarDocumento');
            document.getElementById('mensajearchivo').innerHTML = '¿Está seguro de eliminar el documento?'
        };

        $scope.BorrarDocumento = function () {
            closeModal('modalConfirmacionEliminarDocumento');
            var cont = $scope.ListadoDocumentos.length
            for (var i = 0; i < cont; i++) {
                if ($scope.ListadoDocumentos[i].idRow == $scope.EliminarDocumento.idRow) {
                    if ($scope.ListadoDocumentos[i].temp != 1) {
                        $scope.Modelo.ID_Documentos_Eliminar.push($scope.ListadoDocumentos[i].ID_Documento)
                        if (i == 0 && cont == 1) {
                            $scope.ListadoDocumentos[i].idRow = 1
                            $scope.ListadoDocumentos[i].CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                $scope.ListadoDocumentos[i].Referencia = '',
                                $scope.ListadoDocumentos[i].Documento = '',
                                $scope.ListadoDocumentos[i].Extencion = ''
                        }
                        else if (i == 0 && cont > 1) {
                            $scope.ListadoDocumentos.shift()
                        }
                        else {
                            $scope.ListadoDocumentos.splice(i, i);
                        }
                        i = cont
                    }

                }
            }
        }
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);
        }
        $scope.MaskNumero = function (option) {
            try { $scope.ValorUso = MascaraNumero($scope.ValorUso) } catch (e) { }
            try { $scope.ModeloEquipo = MascaraNumero($scope.ModeloEquipo) } catch (e) { }
        };
        $scope.MaskPlaca = function () {
            try { $scope.Placa = MascaraPlaca($scope.Placa) } catch (e) { }
        };
        $scope.MaskMayus = function (option) {
            try { $scope.Nombre = $scope.Nombre.toUpperCase() } catch (e) { }
            try { $scope.Serie = $scope.Serie.toUpperCase() } catch (e) { }
        };

    }]);