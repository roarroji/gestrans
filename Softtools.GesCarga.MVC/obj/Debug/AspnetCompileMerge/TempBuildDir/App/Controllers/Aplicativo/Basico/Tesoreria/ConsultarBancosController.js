﻿SofttoolsApp.controller("ConsultarBancosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'BancosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, BancosFactory) {

    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Bancos' }];


    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

    var filtros = {};
    $scope.CantidadRegistrosPorPaginas = 10;
    $scope.paginaActual = 1;
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_BANCOS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    $scope.ListadoEstados = [
        { Codigo: -1, Nombre: "NO APLICA" },
        { Codigo: 0, Nombre: "INACTIVO" },
        { Codigo: 1, Nombre: "ACTIVO" },
    ]

    $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
    /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };

    if ($routeParams.Codigo > 0) {
        $scope.Codigo = $routeParams.Codigo;
        Find();
    }


    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#GestionarBancos';
        }
    };
    //-------------------------------------------------FUNCIONES--------------------------------------------------------
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            $scope.Codigo = 0;
            Find()
        }
    };

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);
        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoBancos = [];

        if ($scope.Buscando) {
            if ($scope.MensajesError.length == 0) {
                filtros = {
                    sLista: 0,
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Codigo,
                    CodigoAlterno: $scope.CodigoAlterno,
                    Nombre: $scope.Nombre,
                    Estado: $scope.Estado.Codigo,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.CantidadRegistrosPorPaginas,
                }
                blockUI.delay = 1000;
                BancosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                response.data.Datos.forEach(function (item) {
                                    var registro = item;
                                    if (registro.Codigo > 0) {
                                        if (registro.CodigoAlterno != 0) {
                                            $scope.ListadoBancos.push(registro);
                                        }
                                    }
                                });

                                $scope.ListadoBancos.forEach(function (item) {
                                    $scope.codigo = item.Codigo;
                                });


                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.CantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        blockUI.stop();
    }
    $scope.MaskNumero = function () {
        try { $scope.Codigo = MascaraNumero($scope.Codigo) } catch (e) { }
    };
}]);