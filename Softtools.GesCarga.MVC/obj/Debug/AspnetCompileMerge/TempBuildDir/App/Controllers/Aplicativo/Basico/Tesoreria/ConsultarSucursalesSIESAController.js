﻿SofttoolsApp.controller("ConsultarSucursalesSIESACtrl", ['$scope', '$timeout', 'SucursalesSIESAFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory',  function ($scope, $timeout, SucursalesSIESAFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory) {

    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Sucursales SIESA' }];


    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

    var filtros = {};
    $scope.cantidadRegistrosPorPaginas = 10;
    $scope.paginaActual = 1;
    $scope.ModalErrorCompleto = ''
    $scope.ModalError = ''
    $scope.MostrarMensajeError = false
    $scope.Modelo = {}
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SUCURSALES_SIESA);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };

    /*Cargar el combo de Perfiles Terceros*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_PERFIL_TERCEROS } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoPerfil = []
                    $scope.ListadoPerfil = response.data.Datos
                    $scope.Modelo.Perfil = ''
                }
                else {
                    $scope.ListadoPerfil = []
                }
            }
        }, function (response) {
        });

    /*Cargar el combo de Tipo conectores*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPOS_CONECTOR_SIESA } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoTipoConector = []
                    $scope.ListadoTipoConector = response.data.Datos
                    $scope.Modelo.TipoConector =''
                }
                else {
                    $scope.ListadoTipoConector = []
                }
            }
        }, function (response) {
        });

    /*Cargar el combo de Formas de Pago*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_CLIENTES } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoFormasDePago = []
                    $scope.ListadoFormasDePago = response.data.Datos
                    $scope.Modelo.FormasDePago = ''
                }
                else {
                    $scope.ListadoFormasDePago = []
                }
            }
        }, function (response) {
        });

    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarSucursalSIESA';
        }
    };
    //-------------------------------------------------FUNCIONES--------------------------------------------------------
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            $scope.Codigo = 0
            Find()
        }
    };

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);
        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoSucursalesSIESA = [];
        if ($scope.Buscando) {
            if ($scope.MensajesError.length == 0) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Perfil_Terceros: $scope.Modelo.Perfil == '(NO APLICA)' ? '' : $scope.Modelo.Perfil,
                    Tipo_Conector: $scope.Modelo.TipoConector == '(NO APLICA)' ? '' : $scope.Modelo.TipoConector,
                    Forma_Pago: $scope.Modelo.FormasDePago == '(NO APLICA)' ? '' : $scope.Modelo.FormasDePago,
                    Codigo_SIESA: $scope.CodigoSIESA,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                }
                blockUI.delay = 1000;
                SucursalesSIESAFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                response.data.Datos.forEach(function (item) {
                                    var registro = item;
                                    $scope.ListadoSucursalesSIESA.push(registro);
                                });

                                $scope.ListadoSucursalesSIESA.forEach(function (item) {
                                    $scope.codigo = item.Codigo;
                                });
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                            var listado = [];
                            response.data.Datos.forEach(function (item) {
                                listado.push(item);
                            });
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        blockUI.stop();
    }

    /*------------------------------------------------------------------------------------Eliminar Sucursal SIESA-----------------------------------------------------------------------*/
    $scope.EliminarSucursalSIESA = function (codigo, Nombre) {
        $scope.CodigoSucursal = codigo
        $scope.Nombre = Nombre
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        showModal('modalEliminarSucursalSIESA');
    };

    $scope.Eliminar = function () {
        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.CodigoSucursal,
        };

        SucursalesSIESAFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se eliminó la sucursal' + $scope.Nombre);
                    closeModal('modalEliminarSucursalSIESA');
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                var result = ''
                result = InternalServerError(response.statusText)
                showModal('modalMensajeEliminarSucuralSIESA');
                $scope.ModalError = 'No se puede eliminar la Sucursal ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                $scope.ModalErrorCompleto = response.statusText

            });

    };

    $scope.CerrarModal = function () {
        closeModal('modalEliminarSucursalSIESA');
        closeModal('modalMensajeEliminarSucuralSIESA');
    }

    $scope.MaskNumero = function () {
        $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno)
    };

    /* Obtener parametros*/
    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        $scope.Codigo = $routeParams.Codigo;
        Find();
    }

}]);