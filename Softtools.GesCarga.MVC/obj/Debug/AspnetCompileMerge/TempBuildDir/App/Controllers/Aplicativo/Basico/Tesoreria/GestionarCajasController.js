﻿SofttoolsApp.controller("GestionarCajasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', '$http', 'CajasFactory', 'OficinasFactory', 'PlanUnicoCuentasFactory', 'ServicioSIESAFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, $http, CajasFactory, OficinasFactory, PlanUnicoCuentasFactory, ServicioSIESAFactory) {

    $scope.Titulo = 'GESTIONAR CAJAS';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Cajas' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CAJAS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
    $scope.ListadoOficinas = [];
    $scope.ListadoCuentasPUC = [];
    $scope.CuentaPUCValida = true
    $scope.ValidarValorBase = false
    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoAlterno: 0,
        Nombre: '',
        Estado: 0
    }

    $scope.AsignarCuentaPUC = function (CuentaPUC) {
        if (CuentaPUC != undefined || CuentaPUC != null) {
            if (angular.isObject(CuentaPUC)) {
                $scope.LongitudCuentaPUC = CuentaPUC.length;
                $scope.CuentaPUCValida = true;
            }
            else if (angular.isString(CuentaPUC)) {
                $scope.LongitudCuentaPUC = CuentaPUC.length;
                $scope.CuentaPUCValida = false;
            }
        }
    };

    $scope.ListadoEstados = [
        { Codigo: 0, Nombre: "INACTIVA" },
        { Codigo: 1, Nombre: "ACTIVA" },
    ]

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Obtener();
    }
    else {
        $scope.Modelo.Codigo = 0;
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
    }

    /*Cargar el combo de Oficinas*/
    OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinas = response.data.Datos

                    if ($scope.CodigoOficina !== undefined && $scope.CodigoOficina !== '' && $scope.CodigoOficina !== null) {
                        $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.CodigoOficina)
                    }
                    else {
                        $scope.Modelo.Oficina = $scope.ListadoOficinas[0];
                    }
                }
                else {
                    $scope.ListadoOficinas = [];
                }
            }
        }, function (response) {
        });
    /*Cargar el combo de cuentas PUC*/
    PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCuentasPUC = response.data.Datos

                    if ($scope.CodigoCuentaPUC !== undefined && $scope.CodigoCuentaPUC !== '' && $scope.CodigoCuentaPUC !== null) {
                        $scope.Modelo.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CodigoCuentaPUC)
                    }
                }
                else {
                    $scope.ListadoCuentasPUC = [];
                }
            }
        }, function (response) {
        });
    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function Obtener() {
        blockUI.start('Cargando caja código No. ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando caja código No.' + $scope.Modelo.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        CajasFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
 

                }
                else {
                    ShowError('No se logro consultar la caja código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#!ConsultarCajas';
                }
            }, function (response) {
                ShowError(response.statusText);
                document.location.href = '#!ConsultarCajas';
            });

        blockUI.stop();
    };
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------Funcion Obtener CAJAS SIESA -------------------------------------------------------------------*/
 

 
    $scope.CajasSIESA = function getData() {

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            CodigoCaja: $scope.Modelo.CodigoAlterno,
                //'CEE',
            TotalRegistros: 2
        }
        $scope.ListadoCajas = []
        ServicioSIESAFactory.ConsultarCAJAS(filtros).then(function (response) {
            console.log('llego', response)
            $scope.ListadoCajas = response.data

            
            showModal('modalListadoCajas');

            console.log('llego', $scope.ListadoCajas)
        }, function (response) {
            ShowError(response.statusText);
            
        }); 

    }

    

   
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

   $scope.ConfirmacionGuardar = function () {
        showModal('modalConfirmacionGuardar');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardar');
        if (DatosRequeridos()) {
            $scope.MaskNumero();
            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

            CajasFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó la caja "' + $scope.Modelo.Nombre + '"');
                            }
                            else {
                                ShowSuccess('Se modificó  la caja "' + $scope.Modelo.Nombre + '"');
                            }
                            location.href = '#!ConsultarCajas/' + $scope.Modelo.Codigo;
                        }
                    }
                    $scope.MaskValores();
                }, function (response) {
                    ShowError('No se pudo guardar la caja, porfavor contacte al administrador del sistema');
                });
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;

        if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
            $scope.MensajesError.push('Debe ingresar el nombre de la caja');
            continuar = false;
        }
        if ($scope.Modelo.Oficina == undefined || $scope.Modelo.Oficina == '' || $scope.Modelo.Oficina == null || $scope.Modelo.Oficina.Codigo == 0) {
            $scope.MensajesError.push('Debe seleccionar una oficina');
            continuar = false;
        }

        if ($scope.Modelo.CuentaPUC == undefined || $scope.Modelo.CuentaPUC == '' || $scope.Modelo.CuentaPUC == null || $scope.Modelo.CuentaPUC.Codigo == 0 || $scope.CuentaPUCValida == false) {
            $scope.MensajesError.push('Debe seleccionar una cuenta PUC');
            continuar = false;
        }
        return continuar;
    }


    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarCajas/' + $scope.Modelo.Codigo;
    };


    $scope.MaskMayus = function () {
        try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
    };
    $scope.MaskValores = function () {
        try { $scope.Modelo.SobregiroAutorizado = MascaraValores($scope.Modelo.SobregiroAutorizado) } catch (e) { }
        try { $scope.Modelo.SaldoActual = MascaraValores($scope.Modelo.SaldoActual) } catch (e) { }
    };
}]);