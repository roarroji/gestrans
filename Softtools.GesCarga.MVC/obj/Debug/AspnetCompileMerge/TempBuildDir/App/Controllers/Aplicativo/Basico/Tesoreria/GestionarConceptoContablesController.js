﻿SofttoolsApp.controller("GestionarConceptoContablesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ConceptoContablesFactory', 'ValorCatalogosFactory', 'PlanUnicoCuentasFactory', 'OficinasFactory', 'TipoDocumentosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, ConceptoContablesFactory, ValorCatalogosFactory, PlanUnicoCuentasFactory, OficinasFactory, TipoDocumentosFactory) {

    $scope.Titulo = 'GESTIONAR CONCEPTOS CONTABLES';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Conceptos Contables' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONCEPTOS_CONTABLES);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
    $scope.ListadoConsulta = [];
    $scope.ListadoDocumentoOrigen = [];
    $scope.ListadoTipoConcepto = [];
    $scope.ListadoTipoDocumentos = [];
    $scope.ListadoCentroCostoParametrizacion = [];
    $scope.ListadoTerceroParametrizacion = [];
    $scope.ListadoTipoCuentaConcepto = [];
    $scope.ListadoNaturalezaConceptoContable = [];
    $scope.ListadoDocumentoCruce = [];
    $scope.ListaDetalleAuxi = [];
    $scope.ListadoOficinas = [];
    $scope.MensajesErrorDetalleConcepto = [];
    $scope.ListaDetalle = [];
    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoAlterno: 0,
        Nombre: '',
        Estado: 0,
        Oficina: { Codigo: 0 },
        TipoNaturaleza: { Codigo: 100 },
    }
    $scope.Modal = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        Estado: 1
    }

    $scope.ListadoEstados = [
        { Codigo: 0, Nombre: "INACTIVO" },
        { Codigo: 1, Nombre: "ACTIVO" },
    ]

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Obtener();
    }
    else {
        $scope.Modelo.Codigo = 0;
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
    }

    /*Cargar el combo de Oficinas*/
    OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.ProcesoExitoso === true) {

                    $scope.ListadoOficinas = response.data.Datos
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: 0 })
                    if ($scope.CodigoOficina !== undefined && $scope.CodigoOficina !== '' && $scope.CodigoOficina !== null) {
                        $scope.Modelo.OficinaAplica = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.CodigoOficina)
                    }
                    else {
                        $scope.Modelo.OficinaAplica = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 0')
                    }
                }
                else {
                    $scope.ListadoOficinas = [];
                }
            }
        }, function (response) {
        });
    /*Cargar el combo de Tipo Documentos*/
    TipoDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, AplicaConceptoContables: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoDocumentos = response.data.Datos

                    if ($scope.CodigoDocumentoAplica !== undefined && $scope.CodigoDocumentoAplica !== '' && $scope.CodigoDocumentoAplica !== null) {
                        $scope.Modelo.DocumentoAplica = $linq.Enumerable().From($scope.ListadoTipoDocumentos).First('$.Codigo ==' + $scope.CodigoDocumentoAplica)
                    }
                    else {
                        $scope.Modelo.DocumentoAplica = $scope.ListadoTipoDocumentos[0];
                    }
                }
                else {
                    $scope.ListadoTipoDocumentos = [];
                }
            }
        }, function (response) {
        });
    /*Cargar el combo de Documento Origen*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoConsulta = []
                    $scope.ListadoConsulta = response.data.Datos
                    for (i = 0; i <= $scope.ListadoConsulta.length - 1; i++) {
                        var Decimal = $scope.ListadoConsulta[i].Codigo / 100
                        if (Decimal - Math.floor(Decimal) > 0) {
                            $scope.ListadoDocumentoOrigen.push($scope.ListadoConsulta[i])
                        }
                    }
                    if ($scope.CodigoDocumentoOrigen !== undefined && $scope.CodigoDocumentoOrigen !== '' && $scope.CodigoDocumentoOrigen !== null) {
                        $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentoOrigen).First('$.Codigo ==' + $scope.CodigoDocumentoOrigen)
                    }
                    else {
                        $scope.Modelo.DocumentoOrigen = $scope.ListadoDocumentoOrigen[-1]
                    }
                }
                else {
                    $scope.ListadoDocumentoOrigen = []
                }
            }
        }, function (response) {
        });
    /*Cargar el combo de Tipo Concepto Contable*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CONCEPTO_CONTABLE } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoConsulta = []
                    $scope.ListadoConsulta = response.data.Datos
                    for (i = 0; i <= $scope.ListadoConsulta.length - 1; i++) {
                        var Decimal = $scope.ListadoConsulta[i].Codigo / 100
                        if (Decimal - Math.floor(Decimal) > 0) {
                            $scope.ListadoTipoConcepto.push($scope.ListadoConsulta[i])
                        }
                    }
                    if ($scope.CodigoTipoCuenta !== undefined && $scope.CodigoTipoCuenta !== '' && $scope.CodigoTipoCuenta !== null) {
                        $scope.Modelo.TipoConceptoContable = $linq.Enumerable().From($scope.ListadoTipoConcepto).First('$.Codigo ==' + $scope.CodigoTipoCuenta)
                    }
                    else {
                        $scope.Modelo.TipoConceptoContable = $scope.ListadoTipoConcepto[0]
                    }
                }
                else {
                    $scope.ListadoTipoConcepto = []
                }
            }
        }, function (response) {
        });

    /*Cargar el combo de cuentas PUC*/
    PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCuentasPUC = response.data.Datos

                    if ($scope.CodigoCuentaPUC !== undefined && $scope.CodigoCuentaPUC !== '' && $scope.CodigoCuentaPUC !== null) {
                        $scope.Modal.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CodigoCuentaPUC)
                    }
                }
                else {
                    $scope.ListadoCuentasPUC = [];
                }
            }
        }, function (response) {
        });
    /*Cargar el combo de Documento Cruce*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_CRUCE } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoConsulta = []
                    $scope.ListadoConsulta = response.data.Datos
                    for (i = 0; i <= $scope.ListadoConsulta.length - 1; i++) {
                        var Decimal = $scope.ListadoConsulta[i].Codigo / 100
                        if (Decimal - Math.floor(Decimal) > 0) {
                            $scope.ListadoDocumentoCruce.push($scope.ListadoConsulta[i])
                        }
                    }
                    if ($scope.CodigoDocumentoCruce !== undefined && $scope.CodigoDocumentoCruce !== '' && $scope.CodigoDocumentoCruce !== null) {
                        $scope.Modal.DocumentoCruce = $linq.Enumerable().From($scope.ListadoDocumentoCruce).First('$.Codigo ==' + $scope.CodigoDocumentoCruce)
                    }
                    else {
                        $scope.Modal.DocumentoCruce = $scope.ListadoDocumentoCruce[0]
                    }
                }
                else {
                    $scope.ListadoDocumentoCruce = [];
                }
            }
        }, function (response) {
        });
    /*Cargar el combo de Naturaleza Concepto Contable*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NATURALEZA_CONCEPTO_CONTABLE } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoConsulta = []
                    $scope.ListadoConsulta = response.data.Datos
                    for (i = 0; i <= $scope.ListadoConsulta.length - 1; i++) {
                        var Decimal = $scope.ListadoConsulta[i].Codigo / 100
                        if (Decimal - Math.floor(Decimal) > 0) {
                            $scope.ListadoNaturalezaConceptoContable.push($scope.ListadoConsulta[i])
                        }
                    }
                    if ($scope.CodigoNaturalezaConceptoContable !== undefined && $scope.CodigoNaturalezaConceptoContable !== '' && $scope.CodigoNaturalezaConceptoContable !== null) {
                        $scope.Modal.NaturalezaConceptoContable = $linq.Enumerable().From($scope.ListadoNaturalezaConceptoContable).First('$.Codigo ==' + $scope.CodigoNaturalezaConceptoContable)
                    }
                    else {
                        $scope.Modal.NaturalezaConceptoContable = $scope.ListadoNaturalezaConceptoContable[0]
                    }
                }
                else {
                    $scope.ListadoNaturalezaConceptoContable = []
                }
            }
        }, function (response) {
        });
    /*Cargar el combo de Tipo Cuenta Concepto*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CUENTA_CONCEPTO } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoConsulta = []
                    $scope.ListadoConsulta = response.data.Datos
                    for (i = 0; i <= $scope.ListadoConsulta.length - 1; i++) {
                        var Decimal = $scope.ListadoConsulta[i].Codigo / 100
                        if (Decimal - Math.floor(Decimal) > 0) {
                            $scope.ListadoTipoCuentaConcepto.push($scope.ListadoConsulta[i])
                        }
                    }
                    if ($scope.CodigoTipoCuentaConcepto !== undefined && $scope.CodigoTipoCuentaConcepto !== '' && $scope.CodigoTipoCuentaConcepto !== null) {
                        $scope.Modal.TipoCuentaConcepto = $linq.Enumerable().From($scope.ListadoTipoCuentaConcepto).First('$.Codigo ==' + $scope.CodigoTipoCuentaConcepto)
                    }
                    else {
                        $scope.Modal.TipoCuentaConcepto = $scope.ListadoTipoCuentaConcepto[0]
                    }
                }
                else {
                    $scope.ListadoTipoCuentaConcepto = []
                }
            }
        }, function (response) {
        });
    /*Cargar el combo de Tercero Paramtrizacion Contable*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TERCERO_PARAMETRIZACION_CONTABLE } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoTerceroParametrizacion = []
                    $scope.ListadoTerceroParametrizacion = response.data.Datos

                    if ($scope.CodigoTerceroParametrizacion !== undefined && $scope.CodigoTerceroParametrizacion !== '' && $scope.CodigoTerceroParametrizacion !== null) {
                        $scope.Modal.TerceroParametrizacion = $linq.Enumerable().From($scope.ListadoTerceroParametrizacion).First('$.Codigo ==' + $scope.CodigoTerceroParametrizacion)
                    }
                    else {
                        $scope.Modal.TerceroParametrizacion = $scope.ListadoTerceroParametrizacion[0]
                    }
                }
                else {
                    $scope.ListadoTerceroParametrizacion = []
                }
            }
        }, function (response) {
        });
    /*Cargar el combo de centro costo Paramtrizacion Contable*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CENTRO_COSTO_PARAMETRIZACION_CONTABLE } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoCentroCostoParametrizacion = []
                    $scope.ListadoCentroCostoParametrizacion = response.data.Datos

                    if ($scope.CodigoCentroCostoParametrizacion !== undefined && $scope.CodigoCentroCostoParametrizacion !== '' && $scope.CodigoCentroCostoParametrizacion !== null) {
                        $scope.Modal.CentroCostoParametrizacion = $linq.Enumerable().From($scope.ListadoCentroCostoParametrizacion).First('$.Codigo ==' + $scope.CodigoCentroCostoParametrizacion)
                    }
                    else {
                        $scope.Modal.CentroCostoParametrizacion = $scope.ListadoCentroCostoParametrizacion[0]
                    }
                }
                else {
                    $scope.ListadoCentroCostoParametrizacion = []
                }
            }
        }, function (response) {
        });
    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function Obtener() {
        blockUI.start('Cargando concepto contable código No. ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando  concepto contable código No.' + $scope.Modelo.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        ConceptoContablesFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    $scope.Modelo.Codigo = response.data.Datos.Codigo;
                    $scope.Modelo.Fuente = response.data.Datos.Fuente
                    $scope.Modelo.FuenteAnula = response.data.Datos.FuenteAnula
                    $scope.Modelo.Observaciones = response.data.Datos.Observaciones
                    $scope.Modelo.Nombre = response.data.Datos.Nombre
                    $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno

                    response.data.Datos.Detalle.forEach(function (item) {
                        if (item.GeneraCuenta == 1) {
                            item.GeneraCuenta = true
                        } else {
                            item.GeneraCuenta = false
                        }
                        $scope.ListaDetalle.push(item)
                    })

                    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                    $scope.CodigoDocumentoAplica = response.data.Datos.DocumentoAplica
                    if ($scope.ListadoTipoDocumentos.length > 0) {
                        $scope.Modelo.DocumentoAplica = $linq.Enumerable().From($scope.ListadoTipoDocumentos).First('$.Codigo ==' + $scope.CodigoDocumentoAplica)
                    }
                    $scope.CodigoTipoCuenta = response.data.Datos.TipoConceptoContable.Codigo
                    if ($scope.ListadoTipoConcepto.length > 0) {
                        $scope.Modelo.TipoConceptoContable = $linq.Enumerable().From($scope.ListadoTipoConcepto).First('$.Codigo ==' + $scope.CodigoTipoCuenta)
                    }
                    $scope.CodigoDocumentoOrigen = response.data.Datos.DocumentoOrigen.Codigo
                    if ($scope.ListadoDocumentoOrigen.length > 0) {
                        $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentoOrigen).First('$.Codigo ==' + $scope.CodigoDocumentoOrigen)
                    }
                    $scope.CodigoOficina = response.data.Datos.OficinaAplica.Codigo
                    if ($scope.ListadoOficinas.length > 0) {
                        $scope.Modelo.OficinaAplica = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.CodigoOficina)
                    }
                }
                else {
                    ShowError('No se logro consultar el concepto contable código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#!ConsultarConceptoContables';
                }
            }, function (response) {
                ShowError(response.statusText);
                document.location.href = '#!ConsultarConceptoContables';
            });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    $scope.ConfirmacionGuardar = function () {
        showModal('modalConfirmacionGuardar');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardar');
        if (DatosRequeridos()) {
            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

            $scope.ListaDetalle.forEach(function (item) {
                if (item.GeneraCuenta == true) {
                    item.GeneraCuenta = 1
                } else {
                    item.GeneraCuenta = 0
                }

            })
            $scope.Modelo.Detalle = $scope.ListaDetalle;
            ConceptoContablesFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó el concepto contable "' + $scope.Modelo.Nombre + '"');
                            }
                            else {
                                ShowSuccess('Se modificó  el concepto contable "' + $scope.Modelo.Nombre + '"');
                            }
                            location.href = '#!ConsultarConceptoContables/' + $scope.Modelo.Codigo;
                        }
                    }
                    $scope.MaskValores();
                }, function (response) {
                    ShowError('No se pudo guardar el concepto contable, porfavor contacte al administrador del sistema');
                });
        }
    };
    $scope.ModalDetalleConcepto = function () {
        $scope.Modal.CampoAuxiliar = '';
        $scope.Modal.SufijoCodigoAnexo = '';
        $scope.Modal.CodigoAnexo = '';
        $scope.Modal.Prefijo = '';
        $scope.Modal.GeneraCuenta = false
        $scope.Modal.PorcentajeTarifa = '';
        $scope.Modal.ValorBase = '';
        $scope.Modal.CuentaPUC = '';
        showModal('modalDetalle');
    };

    $scope.GuardarDetalle = function () {
        DatosRequeridosDetalle();
        if ($scope.MensajesErrorDetalleConcepto.length == 0) {

            if ($scope.ListaDetalle.length > 0) {
                $scope.ListaDetalle.forEach(function (item) {
                    if (item.CuentaPUC.Codigo == $scope.Modal.CuentaPUC.Codigo) {
                        $scope.MensajesErrorDetalleConcepto.push('La cuenta PUC ya se encuentra ingresada, porfavor ingrese una diferente');
                    }
                })
            }
            $scope.Modal.ValorBase = MascaraNumero($scope.Modal.ValorBase)
            $scope.ListaDetalle.push(JSON.parse(JSON.stringify($scope.Modal)))
            closeModal('modalDetalle');
        }
    }

    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;
        if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
            $scope.MensajesError.push('Debe ingresar el nombre del concepto contable');
            continuar = false;
        }
        //if ($scope.Modelo.CodigoAlterno == undefined || $scope.Modelo.CodigoAlterno == '') {
        //    $scope.MensajesError.push('Debe ingresar el código alterno');
        //    continuar = false;
        //}
        if ($scope.Modelo.DocumentoOrigen == '' || $scope.Modelo.DocumentoOrigen == undefined || $scope.Modelo.DocumentoOrigen == undefined || $scope.Modelo.DocumentoOrigen.Codigo == 0) {
            $scope.MensajesError.push('Debe seleccionar el documento origen');
            continuar = false;
        }
        if ($scope.ModalEstado == '' || $scope.ModalEstado == undefined || $scope.ModalEstado == undefined) {
            $scope.MensajesError.push('Debe seleccionar un estado');
            continuar = false;
        }
        if ($scope.Modelo.TipoConceptoContable == '' || $scope.Modelo.TipoConceptoContable == undefined || $scope.Modelo.TipoConceptoContable == undefined || $scope.Modelo.TipoConceptoContable.Codigo == 0) {
            $scope.MensajesError.push('Debe seleccionar el tipo concepto ');
            continuar = false;
        }

        if ($scope.Modelo.OficinaAplica == '' || $scope.Modelo.OficinaAplica == undefined || $scope.Modelo.OficinaAplica == undefined && $scope.Modelo.OficinaAplica.Codigo != 0) {
            $scope.MensajesError.push('Debe seleccionar la oficina aplica');
            continuar = false;
        }
        if ($scope.Modelo.Fuente == '' || $scope.Modelo.Fuente == undefined || $scope.Modelo.Fuente == undefined) {
            $scope.Modelo.Fuente = ''
        }
        if ($scope.Modelo.FuenteAnula == '' || $scope.Modelo.FuenteAnula == undefined || $scope.Modelo.FuenteAnula == undefined) {
            $scope.Modelo.FuenteAnula = ''
        }
        if ($scope.Modelo.Observaciones == '' || $scope.Modelo.Observaciones == undefined || $scope.Modelo.Observaciones == undefined) {
            $scope.Modelo.Observaciones = ''
        }
        if ($scope.ListaDetalle.length == 0) {
            $scope.MensajesError.push('Debe ingresar minimo un detalle');
            continuar = false;
        }

        return continuar;
    }

    function DatosRequeridosDetalle() {
        window.scrollTo(top, top);
        $scope.MensajesErrorDetalleConcepto = [];

        if ($scope.Modal.CuentaPUC == '' || $scope.Modal.CuentaPUC == undefined || $scope.Modal.CuentaPUC == null || $scope.Modal.CuentaPUC.Codigo == 0) {
            $scope.MensajesErrorDetalleConcepto.push('Debe ingresar la cuenta PUC');
            continuar = false;
        }

        if ($scope.Modal.NaturalezaConceptoContable == '' || $scope.Modal.NaturalezaConceptoContable == undefined || $scope.Modal.NaturalezaConceptoContable.Codigo == 0) {
            $scope.MensajesErrorDetalleConcepto.push('Debe seleccionar el tipo de naturaleza');
            continuar = false;
        }

        if ($scope.Modal.TipoCuentaConcepto == '' || $scope.Modal.TipoCuentaConcepto == undefined || $scope.Modal.TipoCuentaConcepto.Codigo == 0) {
            $scope.MensajesErrorDetalleConcepto.push('Debe seleccionar el tipo de cuenta');
            continuar = false;
        }

        if ($scope.Modal.DocumentoCruce == '' || $scope.Modal.DocumentoCruce == undefined || $scope.Modal.DocumentoCruce.Codigo == 0) {
            $scope.MensajesErrorDetalleConcepto.push('Debe seleccionar el documento origen');
            continuar = false;
        }

        if ($scope.Modal.ValorBase == 0 || $scope.Modal.ValorBase == undefined || $scope.Modal.ValorBase == '') {
            $scope.Modal.ValorBase = 0
        }

        if ($scope.Modal.PorcentajeTarifa == 0 || $scope.Modal.PorcentajeTarifa == undefined || $scope.Modal.PorcentajeTarifa == '') {
            $scope.Modal.PorcentajeTarifa = 0
        }
        if ($scope.Modal.TerceroParametrizacion.Codigo == 3100) {
            if ($scope.Modal.CuentaPUC.ExigeTercero == 1) {
                $scope.MensajesErrorDetalleConcepto.push('La cuenta puc seleccionada exige un tercero');
                continuar = false;
            }
        }
        if ($scope.Modal.CampoAuxiliar == '' || $scope.Modal.CampoAuxiliar == undefined || $scope.Modal.CampoAuxiliar == null) {
            $scope.Modal.CampoAuxiliar = '';
        }
        if ($scope.Modal.Prefijo == '' || $scope.Modal.Prefijo == undefined || $scope.Modal.Prefijo == null) {
            $scope.Modal.Prefijo = '';
        }
        if ($scope.Modal.CodigoAnexo == '' || $scope.Modal.CodigoAnexo == undefined || $scope.Modal.CodigoAnexo == null) {
            $scope.Modal.CodigoAnexo = '';
        }
        if ($scope.Modal.SufijoCodigoAnexo == '' || $scope.Modal.SufijoCodigoAnexo == undefined || $scope.Modal.SufijoCodigoAnexo == null) {
            $scope.Modal.SufijoCodigoAnexo = '';
        }
    }
    $scope.ConfirmacionEliminarMovimiento = function (indice) {
        $scope.MensajeEliminar = { indice };
        $scope.VarAuxi = indice
        showModal('modalEliminarMovimiento');
    };

    $scope.EliminarMovimiento = function (indice) {
        $scope.ListaDetalle.splice($scope.VarAuxi, 1);
        closeModal('modalEliminarMovimiento');
    };

    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarConceptoContables/' + $scope.Modelo.Codigo;
    };


    $scope.MaskMayus = function () {
        try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
    };
    $scope.MaskValores = function () {
        MascaraValoresGeneral($scope);
    };
    $scope.MaskDecimales = function () {
        return MascaraDecimalesGeneral($scope)
    }

}]);