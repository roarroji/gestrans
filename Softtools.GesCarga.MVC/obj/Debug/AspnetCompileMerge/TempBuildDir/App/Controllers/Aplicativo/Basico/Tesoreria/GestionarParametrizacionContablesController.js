﻿SofttoolsApp.controller("GestionarParametrizacionContablesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'ParametrizacionContablesFactory', 'ValorCatalogosFactory', 'PlanUnicoCuentasFactory', 'TipoDocumentosFactory', 'ImpuestosFactory', 'ConceptoLiquidacionFactory', 'ConceptosFacturacionFactory', 'ConceptoGastosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, ParametrizacionContablesFactory, ValorCatalogosFactory, PlanUnicoCuentasFactory, TipoDocumentosFactory, ImpuestosFactory, ConceptoLiquidacionFactory, ConceptosFacturacionFactory, ConceptoGastosFactory) {
        console.clear();
        $scope.Titulo = 'GESTIONAR MOVIMIENTOS CONTABLES';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Moviminetos Contables' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PARAMETRIZACION_CONTABLE);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListaDetalle = [];
        $scope.ListadoDocumentoGenera = [];
        $scope.ListadoDocumentoGeneracionDetalle = [];
        $scope.ListadoCuentasPUC = [];
        $scope.ListadoDocumentoCruce = [];
        $scope.ListadoNaturalezaMovimiento = [];
        $scope.ListadoTipoParametrizacion = [];
        $scope.ListadoTerceroParametrizacion = [];
        $scope.ListadoCentroCostoParametrizacion = [];
        $scope.ListadoTipoConductor = [];
        $scope.ListadoTipoDueno = [];
        $scope.ListadoSufijoICAParametrizacion = [];
        $scope.ListadoValorParametrizacion = [];
        $scope.CuentaPUCValida = true;
        $scope.Modificar = 0;

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Provision: '',
            Estado: 0,
        }
        $scope.Modal = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: 1,
            TipoNaturaleza: { Codigo: 100 },
            Tercero_Filial: '',
            CodigoAlterno: 0,
        }

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVA" },
            { Codigo: 1, Nombre: "ACTIVA" },
        ]

        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        }

        $scope.AsignarCuentaPUC = function (CuentaPUC) {
            if (CuentaPUC != undefined || CuentaPUC != null) {
                if (angular.isObject(CuentaPUC)) {
                    $scope.LongitudCuentaPUC = CuentaPUC.length;
                    $scope.CuentaPUCValida = true;
                }
                else if (angular.isString(CuentaPUC)) {
                    $scope.LongitudCuentaPUC = CuentaPUC.length;
                    $scope.CuentaPUCValida = false;
                }
            }
        };
        /*Cargar el combo de Documento Genera */
        $scope.ListadoDocumentosGenera = TipoDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, GeneraComprobanteContable: ESTADO_ACTIVO, Sync: true }).Datos

        if ($scope.CodigoDocumentoGenera !== undefined && $scope.CodigoDocumentoGenera !== '' && $scope.CodigoDocumentoGenera !== null) {
            $scope.Modelo.TipoDocumentoGenera = $linq.Enumerable().From($scope.ListadoDocumentoGenera).First('$.Codigo ==' + $scope.CodigoDocumentoGenera)
        }
        else {
            $scope.Modelo.TipoDocumentoGenera = $scope.ListadoDocumentoGenera[0]
        }

        ResponseCatalogosDocumentoGenera = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DOCUMENTO_GENERA }, Sync : true }).Datos
           
        
        if (ResponseCatalogosDocumentoGenera.length > 0) {
            $scope.ListadoDocumentoGenera = []

            $scope.ListadoDocumentosGenera.forEach(item => {
                ResponseCatalogosDocumentoGenera.forEach(item2 => {
                    if (item.Codigo == item2.CampoAuxiliar2) {
                        $scope.ListadoDocumentoGenera.push(item2)
                    }
                });
            });

            if ($scope.CodigoDocumentoGenera !== undefined && $scope.CodigoDocumentoGenera !== '' && $scope.CodigoDocumentoGenera !== null) {
                $scope.Modelo.TipoDocumentoGenera = $linq.Enumerable().From($scope.ListadoDocumentoGenera).First('$.Codigo ==' + $scope.CodigoDocumentoGenera)
            }
            else {
                $scope.Modelo.TipoDocumentoGenera = $scope.ListadoDocumentoGenera[0]
            }



        } else {
            $scope.ListadoDocumentoGenera = []
        }
        
           
        /*Cargar el combo de Documento Genera detalle */
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_GENERACION_DETALLE_CONTABLE } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoDocumentoGeneracionDetalle = []
                        $scope.ListadoDocumentoGeneracionDetalle = response.data.Datos

                        if ($scope.CodigoDocumentoGeneraDetallle !== undefined && $scope.CodigoDocumentoGeneraDetallle !== '' && $scope.CodigoDocumentoGeneraDetallle !== null) {
                            $scope.Modelo.TipoGeneraDetalle = $linq.Enumerable().From($scope.ListadoDocumentoGeneracionDetalle).First('$.Codigo ==' + $scope.CodigoDocumentoGeneraDetallle)
                        }
                        else {
                            $scope.Modelo.TipoGeneraDetalle = $scope.ListadoDocumentoGeneracionDetalle[0]
                        }
                    }
                    else {
                        $scope.ListadoDocumentoGeneracionDetalle = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de cuentas PUC*/
        PlanUnicoCuentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCuentasPUC = [];
                        $scope.ListadoCuentasPUC = response.data.Datos
                        for (var i = 0; i < $scope.ListadoCuentasPUC.length; i++) {
                            $scope.ListadoCuentasPUC[i].NombreAlterno = $scope.ListadoCuentasPUC[i].CodigoCuenta + ' - ' + $scope.ListadoCuentasPUC[i].Nombre
                        }
                        if ($scope.CodigoCuentaPUC !== undefined && $scope.CodigoCuentaPUC !== '' && $scope.CodigoCuentaPUC !== null) {
                            $scope.Modal.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + $scope.CodigoCuentaPUC)
                        }
                    }
                    else {
                        $scope.ListadoCuentasPUC = [];
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de Documento Cruce*/
        TipoDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoDocumentoCruce = []
                        $scope.ListadoDocumentoCruce = response.data.Datos
                        if ($scope.CodigoDocumentoCruce !== undefined && $scope.CodigoDocumentoCruce !== '' && $scope.CodigoDocumentoCruce !== null) {
                            $scope.Modal.DocumentoCruce = $linq.Enumerable().From($scope.ListadoDocumentoCruce).First('$.Codigo ==' + $scope.CodigoDocumentoCruce)
                        }
                        else {
                            $scope.Modal.DocumentoCruce = $scope.ListadoDocumentoCruce[0]
                        }
                    }
                    else {
                        $scope.ListadoDocumentoCruce = [];
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de Naturaleza Movimiento*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NATURALEZA_CONCEPTO_CONTABLE } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoNaturalezaMovimiento = []
                        $scope.ListadoNaturalezaMovimiento = response.data.Datos
                        if ($scope.CodigoNaturalezaMovimiento !== undefined && $scope.CodigoNaturalezaMovimiento !== '' && $scope.CodigoNaturalezaMovimiento !== null) {
                            $scope.Modal.NaturalezaMovimiento = $linq.Enumerable().From($scope.ListadoNaturalezaMovimiento).First('$.Codigo ==' + $scope.CodigoNaturalezaMovimiento)
                        }
                        else {
                            $scope.Modal.NaturalezaMovimiento = $scope.ListadoNaturalezaMovimiento[0]
                        }
                    }
                    else {
                        $scope.ListadoNaturalezaMovimiento = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de Tipo parametrizacion*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_PARAMETRIZACION_CONTABLE } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoParametrizacion = []
                        $scope.ListadoTipoParametrizacion = response.data.Datos
                        if ($scope.CodigoTipoParametrizacion !== undefined && $scope.CodigoTipoParametrizacion !== '' && $scope.CodigoTipoParametrizacion !== null) {
                            $scope.Modal.TipoParametrizacion = $linq.Enumerable().From($scope.ListadoTipoParametrizacion).First('$.Codigo ==' + $scope.CodigoTipoParametrizacion)
                        }
                        else {
                            $scope.Modal.TipoParametrizacion = $scope.ListadoTipoParametrizacion[0]
                        }
                    }
                    else {
                        $scope.ListadoTipoParametrizacion = []
                    }
                }
            }, function (response) {
            });

        /*Cargar el combo de Tercero Paramtrizacion Contable*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TERCERO_PARAMETRIZACION_CONTABLE } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTerceroParametrizacion = []
                        $scope.ListadoTerceroParametrizacion = response.data.Datos

                        if ($scope.CodigoTerceroParametrizacion !== undefined && $scope.CodigoTerceroParametrizacion !== '' && $scope.CodigoTerceroParametrizacion !== null) {
                            $scope.Modal.TerceroParametrizacion = $linq.Enumerable().From($scope.ListadoTerceroParametrizacion).First('$.Codigo ==' + $scope.CodigoTerceroParametrizacion)
                        }
                        else {
                            $scope.Modal.TerceroParametrizacion = $scope.ListadoTerceroParametrizacion[0]
                        }
                    }
                    else {
                        $scope.ListadoTerceroParametrizacion = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de centro costo Paramtrizacion Contable*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CENTRO_COSTO_PARAMETRIZACION_CONTABLE } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCentroCostoParametrizacion = []
                        $scope.ListadoCentroCostoParametrizacion = response.data.Datos

                        if ($scope.CodigoCentroCostoParametrizacion !== undefined && $scope.CodigoCentroCostoParametrizacion !== '' && $scope.CodigoCentroCostoParametrizacion !== null) {
                            $scope.Modal.CentroCostoParametrizacion = $linq.Enumerable().From($scope.ListadoCentroCostoParametrizacion).First('$.Codigo ==' + $scope.CodigoCentroCostoParametrizacion)
                        }
                        else {
                            $scope.Modal.CentroCostoParametrizacion = $scope.ListadoCentroCostoParametrizacion[0]
                        }
                    }
                    else {
                        $scope.ListadoCentroCostoParametrizacion = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de Tipo Conductor*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CONDUCTOR } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoConductor = []
                        $scope.ListadoTipoConductor = response.data.Datos

                        if ($scope.CodigoTipoConductor !== undefined && $scope.CodigoTipoConductor !== '' && $scope.CodigoTipoConductor !== null) {
                            $scope.Modal.TipoConductor = $linq.Enumerable().From($scope.ListadoTipoConductor).First('$.Codigo ==' + $scope.CodigoTipoConductor)
                        }
                        else {
                            $scope.Modal.TipoConductor = $scope.ListadoTipoConductor[0]
                        }
                    }
                    else {
                        $scope.ListadoTipoConductor = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de Tipo dueño vehiculo*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DUEÑO_VEHICULO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoDueno = []
                        $scope.ListadoTipoDueno = response.data.Datos

                        if ($scope.CodigoTipoDueño !== undefined && $scope.CodigoTipoDueño !== '' && $scope.CodigoTipoDueño !== null) {
                            $scope.Modal.TipoDueno = $linq.Enumerable().From($scope.ListadoTipoDueno).First('$.Codigo ==' + $scope.CodigoTipoDueño)
                        }
                        else {
                            $scope.Modal.TipoDueno = $scope.ListadoTipoDueno[0]
                        }
                    }
                    else {
                        $scope.ListadoTipoDueno = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de sufijo ICA Paramtrizacion */
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SUFIJO_ICA_PARAMETRIZACION } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoSufijoICAParametrizacion = []
                        $scope.ListadoSufijoICAParametrizacion = response.data.Datos

                        if ($scope.CodigoSufijoICAParametrizacion !== undefined && $scope.CodigoSufijoICAParametrizacion !== '' && $scope.CodigoSufijoICAParametrizacion !== null) {
                            $scope.Modal.SufijoICAParametrizacion = $linq.Enumerable().From($scope.ListadoSufijoICAParametrizacion).First('$.Codigo ==' + $scope.CodigoSufijoICAParametrizacion)
                        }
                        else {
                            $scope.Modal.SufijoICAParametrizacion = $scope.ListadoSufijoICAParametrizacion[0]
                        }
                    }
                    else {
                        $scope.ListadoSufijoICAParametrizacion = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de valor Paramtrizacion*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_VALOR_PARAMETRIZACION_CONTABLE } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoValorParametrizacion = []
                        $scope.ListadoValorParametrizacion = response.data.Datos

                        if ($scope.CodigoValorParametrizacion !== undefined && $scope.CodigoValorParametrizacion !== '' && $scope.CodigoValorParametrizacion !== null) {
                            $scope.Modal.ValorParametrizacion = $linq.Enumerable().From($scope.ListadoValorParametrizacion).First('$.Codigo ==' + $scope.CodigoValorParametrizacion)
                        }
                        else {
                            $scope.Modal.ValorParametrizacion = $scope.ListadoValorParametrizacion[0]
                        }
                    }
                    else {
                        $scope.ListadoValorParametrizacion = []
                    }
                }
            }, function (response) {
            });
        /*Cargar los impuestos*/
        ImpuestosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoImpuestos = []
                        $scope.ListadoImpuestos = response.data.Datos
                    }
                    else {
                        $scope.ListadoImpuestos = []
                    }
                }
            }, function (response) {
            });
        /*Cargar los Conceptos*/
        ConceptoLiquidacionFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, ConceptoSistema: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoConceptos = []
                        $scope.ListadoConceptos = response.data.Datos
                    }
                    else {
                        $scope.ListadoConceptos = []
                    }
                }
            }, function (response) {
            });
        ConceptosFacturacionFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoConceptosFacturacion = []
                        $scope.ListadoConceptosFacturacion = response.data.Datos
                    }
                    else {
                        $scope.ListadoConceptosFacturacion = []
                    }
                }
            }, function (response) {
            });
        ConceptoGastosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoConceptosGastos = []
                        $scope.ListadoConceptosGastos = response.data.Datos
                    }
                    else {
                        $scope.ListadoConceptosGastos = []
                    }
                }
            }, function (response) {
            });

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando  movimiento contable código No. ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando  movimiento contable código No.' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            ParametrizacionContablesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Modelo.Codigo = response.data.Datos.Codigo;
                        $scope.Modelo.Fuente = response.data.Datos.Fuente
                        $scope.Modelo.FuenteAnula = response.data.Datos.FuenteAnula
                        $scope.Modelo.Observaciones = response.data.Datos.Observaciones
                        $scope.Modelo.Nombre = response.data.Datos.Nombre
                        $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno
                        $scope.ListaDetalle = response.data.Datos.Detalle
                        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                        //$scope.CodigoDocumentoGeneraDetallle = response.data.Datos.TipoGeneraDetalle.Codigo
                        //if ($scope.ListadoDocumentoGeneracionDetalle.length > 0) {
                        //    $scope.Modelo.TipoGeneraDetalle = $linq.Enumerable().From($scope.ListadoDocumentoGeneracionDetalle).First('$.Codigo ==' + $scope.CodigoDocumentoGeneraDetallle)
                        //}
                        $scope.CodigoDocumentoGenera = response.data.Datos.TipoDocumentoGenera.Codigo
                        if ($scope.ListadoDocumentoGenera.length > 0) {
                            $scope.Modelo.TipoDocumentoGenera = $linq.Enumerable().From($scope.ListadoDocumentoGenera).First('$.Codigo ==' + $scope.CodigoDocumentoGenera)
                        }
                    }
                    else {
                        ShowError('No se logro consultar el movimiento contable código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarParametrizacionContables';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarParametrizacionContables';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                //Estado
                $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

                $scope.Modelo.Detalle = $scope.ListaDetalle;
                ParametrizacionContablesFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el movimiento contable "' + $scope.Modelo.Nombre + '"');
                                }
                                else {
                                    ShowSuccess('Se modificó  el movimiento contable "' + $scope.Modelo.Nombre + '"');
                                }
                                location.href = '#!ConsultarParametrizacionContables/' + $scope.Modelo.Codigo;
                            }
                        }
                        $scope.MaskValores();
                    }, function (response) {
                        ShowError('No se pudo guardar la parametrización contable, por favor contacte al administrador del sistema');
                    });
            }
        };
        $scope.ModalDetalleParametrizacion = function () {
            $scope.Modal.Codigo = 0
            $scope.Modal.Codigo = $scope.ListaDetalle.length + 1
            $scope.Modal.Orden = $scope.Modal.Codigo
            $scope.Modificar = 0;
            $scope.Modal.CampoAuxiliar = '';
            $scope.Modal.SufijoCodigoAnexo = '';
            $scope.Modal.CodigoAnexo = '';
            $scope.Modal.Prefijo = '';
            $scope.Modal.CuentaPUC = '';
            $scope.Modal.Descripcion = '';
            $scope.Modal.DocumentoCruce = $scope.ListadoDocumentoCruce[0]
            $scope.Modal.NaturalezaMovimiento = $scope.ListadoNaturalezaMovimiento[0]
            $scope.Modal.TipoParametrizacion = $scope.ListadoTipoParametrizacion[0]
            $scope.Modal.TerceroParametrizacion = $scope.ListadoTerceroParametrizacion[0]
            $scope.Modal.CentroCostoParametrizacion = $scope.ListadoCentroCostoParametrizacion[0]
            $scope.Modal.TipoConductor = $scope.ListadoTipoConductor[0]
            $scope.Modal.TipoDueno = $scope.ListadoTipoDueno[0]
            $scope.Modal.SufijoICAParametrizacion = $scope.ListadoSufijoICAParametrizacion[0]
            $scope.Modal.ValorParametrizacion = $scope.ListadoValorParametrizacion[0]
            showModal('modalDetalle');
        };

        $scope.ModificarDetalle = function (Codigo) {
            $scope.Modificar = 1;
            $scope.CodigoDetalle = Codigo
            $scope.ListaDetalle.forEach(function (item) {
                if (item.Codigo == Codigo) {
                    $scope.Modal.Codigo = item.Codigo
                    $scope.Modal.CampoAuxiliar = item.CampoAuxiliar
                    $scope.Modal.SufijoCodigoAnexo = item.SufijoCodigoAnexo
                    $scope.Modal.CodigoAnexo = item.CodigoAnexo
                    $scope.Modal.Prefijo = item.Prefijo
                    $scope.Modal.Orden = item.Orden
                    $scope.Modal.Descripcion = item.Descripcion
                    $scope.Modal.CuentaPUC = $linq.Enumerable().From($scope.ListadoCuentasPUC).First('$.Codigo ==' + item.CuentaPUC.Codigo)
                    $scope.Modal.DocumentoCruce = $linq.Enumerable().From($scope.ListadoDocumentoCruce).First('$.Codigo ==' + item.DocumentoCruce.Codigo)
                    $scope.Modal.NaturalezaMovimiento = $linq.Enumerable().From($scope.ListadoNaturalezaMovimiento).First('$.Codigo ==' + item.NaturalezaMovimiento.Codigo)
                    $scope.Modal.TipoParametrizacion = $linq.Enumerable().From($scope.ListadoTipoParametrizacion).First('$.Codigo ==' + item.TipoParametrizacion.Codigo)
                    $scope.Modal.TerceroParametrizacion = $linq.Enumerable().From($scope.ListadoTerceroParametrizacion).First('$.Codigo ==' + item.TerceroParametrizacion.Codigo)
                    $scope.Modal.CentroCostoParametrizacion = $linq.Enumerable().From($scope.ListadoCentroCostoParametrizacion).First('$.Codigo ==' + item.CentroCostoParametrizacion.Codigo)
                    //$scope.Modal.TipoConductor = $linq.Enumerable().From($scope.ListadoTipoConductor).First('$.Codigo ==' + item.TipoConductor.Codigo)
                    //$scope.Modal.TipoDueno = $linq.Enumerable().From($scope.ListadoTipoDueno).First('$.Codigo ==' + item.TipoDueño.Codigo)
                    $scope.Modal.SufijoICAParametrizacion = $linq.Enumerable().From($scope.ListadoSufijoICAParametrizacion).First('$.Codigo ==' + item.SufijoICAParametrizacion.Codigo)
                    try {
                        $scope.Modal.ValorParametrizacion = $linq.Enumerable().From($scope.ListadoValorParametrizacion).First('$.Codigo ==' + item.ValorParametrizacion.Codigo)
                    } catch (e) {
                        try {
                            $scope.Modal.ValorParametrizacion = $linq.Enumerable().From($scope.ListadoImpuestos).First('$.Codigo ==' + item.ValorParametrizacion.Codigo)
                        } catch (e) {
                            try {
                                $scope.Modal.ValorParametrizacion = $linq.Enumerable().From($scope.ListadoConceptos).First('$.Codigo ==' + item.ValorParametrizacion.Codigo)
                            } catch (e) {
                                try {
                                    $scope.Modal.ValorParametrizacion = $linq.Enumerable().From($scope.ListadoConceptosGastos).First('$.Codigo ==' + item.ValorParametrizacion.Codigo)
                                } catch (e) {

                                }
                            }
                        }
                    }
                }

            })

            showModal('modalDetalle');
        }



        $scope.GuardarDetalle = function () {
            DatosRequeridosDetalle();
            if ($scope.MensajesErrorDetalleParametrizacion.length == 0) {
                //if ($scope.ListaDetalle.length > 0) {
                //    $scope.ListaDetalle.forEach(function (item) {
                //        if (item.CuentaPUC.Codigo == $scope.Modal.CuentaPUC.Codigo) {
                //            $scope.MensajesErrorDetalleParametrizacion.push('La cuenta PUC ya se encuentra ingresada, por favor ingrese una diferente');
                //        }
                //    })
                //}
                if ($scope.Modificar == 0) {
                    //$scope.Modal.TipoDueño = $scope.Modal.TipoDueno
                    //$scope.Modal.Codigo = $scope.Modal.CodigoAlterno
                    var Cuenta = {
                        Codigo: $scope.Modal.Codigo,
                        Orden: $scope.Modal.Codigo,
                        CampoAuxiliar: $scope.Modal.CampoAuxiliar,
                        SufijoCodigoAnexo: $scope.Modal.SufijoCodigoAnexo,
                        CodigoAnexo: $scope.Modal.CodigoAnexo,
                        Prefijo: $scope.Modal.Prefijo,

                        Orden: $scope.Modal.Orden,
                        Descripcion: $scope.Modal.Descripcion,
                        CuentaPUC: $scope.Modal.CuentaPUC,
                        DocumentoCruce: $scope.Modal.DocumentoCruce,

                        NaturalezaMovimiento: $scope.Modal.NaturalezaMovimiento,
                        TipoParametrizacion: $scope.Modal.TipoParametrizacion,
                        TerceroParametrizacion: $scope.Modal.TerceroParametrizacion,
                        CentroCostoParametrizacion: $scope.Modal.CentroCostoParametrizacion,

                        TipoConductor: $scope.Modal.TipoConductor,
                        TipoDueno: $scope.Modal.TipoDueno,
                        SufijoICAParametrizacion: $scope.Modal.SufijoICAParametrizacion,
                        ValorParametrizacion: $scope.Modal.ValorParametrizacion
                    }
                    $scope.ListaDetalle.push(JSON.parse(JSON.stringify(Cuenta)))
                }
                else {
                    $scope.ListaDetalle.forEach(function (item) {
                        if (item.Codigo == $scope.CodigoDetalle) {
                            item.CampoAuxiliar = $scope.Modal.CampoAuxiliar
                            item.SufijoCodigoAnexo = $scope.Modal.SufijoCodigoAnexo
                            item.CodigoAnexo = $scope.Modal.CodigoAnexo
                            item.Prefijo = $scope.Modal.Prefijo
                            item.Orden = $scope.Modal.Orden
                            item.Descripcion = $scope.Modal.Descripcion
                            item.CuentaPUC = $scope.Modal.CuentaPUC
                            item.DocumentoCruce = $scope.Modal.DocumentoCruce
                            item.NaturalezaMovimiento = $scope.Modal.NaturalezaMovimiento
                            item.TipoParametrizacion = $scope.Modal.TipoParametrizacion
                            item.TerceroParametrizacion = $scope.Modal.TerceroParametrizacion
                            item.CentroCostoParametrizacion = $scope.Modal.CentroCostoParametrizacion
                            item.TipoConductor = $scope.Modal.TipoConductor
                            item.TipoDueno = $scope.Modal.TipoDueno
                            item.SufijoICAParametrizacion = $scope.Modal.SufijoICAParametrizacion
                            item.ValorParametrizacion = $scope.Modal.ValorParametrizacion
                        }
                    })
                }
                closeModal('modalDetalle');
                $scope.CodigoDetalle = 0
            }
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
                $scope.MensajesError.push('Debe ingresar el nombre del movimiento contable');
                continuar = false;
            }
            if ($scope.Modelo.TipoDocumentoGenera == '' || $scope.Modelo.TipoDocumentoGenera == undefined || $scope.Modelo.TipoDocumentoGenera == undefined || $scope.Modelo.TipoDocumentoGenera.Codigo == 3600) {
                $scope.MensajesError.push('Debe seleccionar el tipo documento genera');
                continuar = false;
            }
            //if ($scope.Modelo.TipoGeneraDetalle == '' || $scope.Modelo.TipoGeneraDetalle == undefined || $scope.Modelo.TipoGeneraDetalle == undefined || $scope.Modelo.TipoGeneraDetalle.Codigo == 3700) {
            //    $scope.MensajesError.push('Debe seleccionar el tipo generación detalle contable');
            //    continuar = false;
            //}
            if ($scope.ModalEstado == '' || $scope.ModalEstado == undefined || $scope.ModalEstado == undefined) {
                $scope.MensajesError.push('Debe seleccionar un estado');
                continuar = false;
            }
            //if ($scope.Modelo.Fuente == '' || $scope.Modelo.Fuente == undefined || $scope.Modelo.Fuente == undefined) {
            //    $scope.Modelo.Fuente = ''
            //}
            //if ($scope.Modelo.FuenteAnula == '' || $scope.Modelo.FuenteAnula == undefined || $scope.Modelo.FuenteAnula == undefined) {
            //    $scope.Modelo.FuenteAnula = ''
            //}
            if ($scope.Modelo.Observaciones == '' || $scope.Modelo.Observaciones == undefined || $scope.Modelo.Observaciones == undefined) {
                $scope.Modelo.Observaciones = ''
            }
            if ($scope.ListaDetalle.length == 0) {
                $scope.MensajesError.push('Debe ingresar minimo un detalle');
                continuar = false;
            }

            return continuar;
        }

        function DatosRequeridosDetalle() {
            window.scrollTo(top, top);
            $scope.MensajesErrorDetalleParametrizacion = [];

            if ($scope.Modal.NaturalezaMovimiento == '' || $scope.Modal.NaturalezaMovimiento == undefined || $scope.Modal.NaturalezaMovimiento.Codigo == 2700) {
                $scope.MensajesErrorDetalleParametrizacion.push('Debe seleccionar la naturaleza del movimiento');
                continuar = false;
            }
            if ($scope.Modal.CuentaPUC == '' || $scope.Modal.CuentaPUC == undefined || $scope.Modal.CuentaPUC.Codigo == 0 || $scope.CuentaPUCValida == false) {
                $scope.MensajesErrorDetalleParametrizacion.push('Debe ingresar la cuenta PUC');
                continuar = false;
            }
            if ($scope.Modal.TipoParametrizacion == '' || $scope.Modal.TipoParametrizacion == undefined || $scope.Modal.TipoParametrizacion.Codigo == 3800) {
                $scope.MensajesErrorDetalleParametrizacion.push('Debe seleccionar el tipo de parametrización');
                continuar = false;
            }
            if ($scope.Modal.ValorParametrizacion == '' || $scope.Modal.ValorParametrizacion == undefined || $scope.Modal.ValorParametrizacion.Codigo == 4200) {
                $scope.MensajesErrorDetalleParametrizacion.push('Debe seleccionar el documento concepto');
                continuar = false;
            }
            if ($scope.Modal.CuentaPUC.ExigeTercero == 1) {
                if ($scope.Modal.TerceroParametrizacion == '' || $scope.Modal.TerceroParametrizacion == undefined || $scope.Modal.TerceroParametrizacion.Codigo == 3100) {
                    $scope.MensajesErrorDetalleParametrizacion.push('Debe seleccionar un tercero ');
                    continuar = false;
                }
            }
            if ($scope.Modal.Descripcion == undefined || $scope.Modal.Descripcion == null) {
                $scope.Modal.Descripcion = '';
            }
            if ($scope.Modal.CampoAuxiliar == undefined || $scope.Modal.CampoAuxiliar == null) {
                $scope.Modal.CampoAuxiliar = '';
            }
            if ($scope.Modal.Prefijo == undefined || $scope.Modal.Prefijo == null) {
                $scope.Modal.Prefijo = '';
            }
            if ($scope.Modal.CodigoAnexo == undefined || $scope.Modal.CodigoAnexo == null) {
                $scope.Modal.CodigoAnexo = '';
            }
            if ($scope.Modal.SufijoCodigoAnexo == undefined || $scope.Modal.SufijoCodigoAnexo == null) {
                $scope.Modal.SufijoCodigoAnexo = '';
            }
        }
        $scope.ConfirmacionEliminarMovimiento = function (indice) {
            $scope.MensajeEliminar = { indice };
            $scope.VarAuxi = indice
            showModal('modalEliminarMovimiento');
        };

        $scope.EliminarMovimiento = function (indice) {
            $scope.ListaDetalle.splice($scope.VarAuxi, 1);
            closeModal('modalEliminarMovimiento');
        };

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarParametrizacionContables/' + $scope.Modelo.Codigo;
        };


        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
        };

        $scope.MaskMoneda = function () {
            $scope.Modal.PorcentajeTarifa = MascaraDecimales($scope.Modal.PorcentajeTarifa)
            $scope.Modal.ValorBase = MascaraDecimales($scope.Modal.ValorBase)
        }

    }]);