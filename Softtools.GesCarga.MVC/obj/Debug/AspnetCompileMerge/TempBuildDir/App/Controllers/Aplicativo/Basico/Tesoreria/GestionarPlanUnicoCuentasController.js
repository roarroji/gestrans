﻿SofttoolsApp.controller("GestionarPlanUnicoCuentasCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'PlanUnicoCuentasFactory', 'ValorCatalogosFactory', function ($scope, $routeParams, $timeout, $linq, blockUI, PlanUnicoCuentasFactory, ValorCatalogosFactory) {

    $scope.Titulo = 'GESTIONAR PLAN ÚNICO CUENTAS';
    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Plan Único Cuentas' }, { Nombre: 'Gestionar' }];
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PLAN_UNICO_CUENTAS);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    $scope.ListadoClaseCuentaPUC = [];
    $scope.ValidarValorBase = false
    $scope.Modelo = {
        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        Codigo: 0,
        CodigoAlterno: 0,
        Nombre: '',
        Estado: 0
    }

    $scope.ListadoEstados = [
        { Codigo: 0, Nombre: "INACTIVA" },
        { Codigo: 1, Nombre: "ACTIVA" },
    ]

    /* Obtener parametros*/
    if ($routeParams.Codigo > 0) {
        $scope.Modelo.Codigo = $routeParams.Codigo;
        Obtener();
    }
    else {
        $scope.Modelo.Codigo = 0;
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
    }

    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CLASE_CUENTA_PLAN_UNICO_CUENTAS } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoClaseCuentaPUC = response.data.Datos;

                    if ($scope.CodigoCuentaPUC !== undefined && $scope.CodigoCuentaPUC !== '' || $scope.CodigoCuentaPUC !== null) {
                        $scope.Modelo.ClaseCuentaPUC = $linq.Enumerable().From($scope.ListadoClaseCuentaPUC).First('$.Codigo ==' + $scope.CodigoCuentaPUC)
                    }
                    else {
                        $scope.Modelo.ClaseCuentaPUC = $scope.ListadoClaseCuentaPUC[0]
                    }
                }
                else {
                    $scope.ListadoClaseCuentaPUC = []
                }
            }
        }, function (response) {
        });

    /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
    function Obtener() {
        blockUI.start('Cargando plan único cuentas código No. ' + $scope.Modelo.Codigo);

        $timeout(function () {
            blockUI.message('Cargando  plan único cuentas Código No.' + $scope.Modelo.Codigo);
        }, 200);

        filtros = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.Modelo.Codigo,
        };

        blockUI.delay = 1000;
        PlanUnicoCuentasFactory.Obtener(filtros).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    $scope.Modelo.Codigo = response.data.Datos.Codigo;
                    $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno;
                    $scope.Modelo.CodigoCuenta = response.data.Datos.CodigoCuenta;
                    $scope.Modelo.Nombre = response.data.Datos.Nombre;

                    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                    $scope.CodigoCuentaPUC = response.data.Datos.ClaseCuentaPUC.Codigo

                    if ($scope.ListadoClaseCuentaPUC.length > 0) {
                        $scope.Modelo.ClaseCuentaPUC = $linq.Enumerable().From($scope.ListadoClaseCuentaPUC).First('$.Codigo ==' + response.data.Datos.ClaseCuentaPUC.Codigo)
                    }

                    $scope.Modelo.ValorBase = response.data.Datos.ValorBase;

                    $scope.MaskValores();
                    if (response.data.Datos.ExigeCentroCosto == 1) {
                        $scope.Modelo.CentroCosto = true
                    }
                    else {
                        $scope.Modelo.CentroCosto = false
                    }
                    if (response.data.Datos.ExigeTercero == 1) {
                        $scope.Modelo.Tercero = true
                    }
                    else {
                        $scope.Modelo.Tercero = false
                    }
                    if (response.data.Datos.ExigeValorBase == 1) {
                        $scope.Modelo.ExigeValorBase = true
                        $scope.ValidarValor();
                    }
                    else {
                        $scope.Modelo.ExigeValorBase = false
                        $scope.ValidarValor();
                    }
                    if (response.data.Datos.ExigeDocumentoCruce == 1) {
                        $scope.Modelo.DocumentoCruce = true
                    }
                    else {
                        $scope.Modelo.DocumentoCruce = false
                    }
                    $scope.Modelo.AplicarMedioPago = response.data.Datos.AplicarMedioPago > 0 ? true : false


                }
                else {
                    ShowError('No se logro consultar el plan único cuentas código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                    document.location.href = '#!ConsultarPlanUnicoCuentas';
                }
            }, function (response) {
                ShowError(response.statusText);
                document.location.href = '#!ConsultarPlanUnicoCuentas';
            });

        blockUI.stop();
    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    $scope.ValidarValor = function () {
        if ($scope.Modelo.ExigeValorBase == true) {
            $scope.ValidarValorBase = true
        }
        else {
            $scope.ValidarValorBase = false
        }
    }

    $scope.ConfirmacionGuardar = function () {
        showModal('modalConfirmacionGuardar');
    };

    $scope.Guardar = function () {
        closeModal('modalConfirmacionGuardar');
        if (DatosRequeridos()) {
            $scope.MaskNumero();
            //Estado
            $scope.Modelo.Estado = $scope.ModalEstado.Codigo;
            $scope.Modelo.AplicarMedioPago = $scope.Modelo.AplicarMedioPago == true ? 1 : 0
            PlanUnicoCuentasFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Se guardó el plan único cuentas "' + $scope.Modelo.Nombre + '"');
                            }
                            else {
                                ShowSuccess('Se modificó el plan único cuentas "' + $scope.Modelo.Nombre + '"');
                            }
                            location.href = '#!ConsultarPlanUnicoCuentas/' + $scope.Modelo.Codigo;
                        }
                    }
                    $scope.MaskValores();
                }, function (response) {
                    if (response.statusText.includes('IX_Plan_Unico_Cuentas')) {
                        ShowError('No se pudo guardar el plan único de cuentas debido a que el codigo de cuenta ingresado ya se encuentra asociado a otra cuenta')
                    } else {
                        ShowError(response.statusText);
                    }
                });
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;

        if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
            $scope.MensajesError.push('Debe ingresar el nombre del plan único de cuentas');
            continuar = false;
        }
        if ($scope.Modelo.CodigoCuenta == undefined || $scope.Modelo.CodigoCuenta == '') {
            $scope.MensajesError.push('Debe ingresar el código cuenta del plan único de cuentas');
            continuar = false;
        }
        if ($scope.Modelo.ClaseCuentaPUC == undefined || $scope.Modelo.ClaseCuentaPUC == '' || $scope.Modelo.ClaseCuentaPUC == null) {
            $scope.MensajesError.push('Debe ingresar la clase cuenta PUC');
            continuar = false;
        }
        if ($scope.Modelo.CentroCosto == true) {
            $scope.Modelo.ExigeCentroCosto = 1
        }
        else {
            $scope.Modelo.ExigeCentroCosto = 0
        }
        if ($scope.Modelo.Tercero == true) {
            $scope.Modelo.ExigeTercero = 1
        }
        else {
            $scope.Modelo.ExigeTercero = 0
        }
        if ($scope.Modelo.ExigeValorBase == true) {
            $scope.Modelo.ExigeValorBase = 1
            if ($scope.Modelo.ValorBase == 0) {
                continuar = false;
                $scope.MensajesError.push('Debe ingresar un valor base');
            }
        }
        else {
            $scope.Modelo.ExigeValorBase = 0
        }

        if ($scope.Modelo.DocumentoCruce == true) {
            $scope.Modelo.ExigeDocumentoCruce = 1
        }
        else {
            $scope.Modelo.ExigeDocumentoCruce = 0
        }
        return continuar;
    }

    $scope.VolverMaster = function () {
        document.location.href = '#!ConsultarPlanUnicoCuentas/' + $scope.Modelo.Codigo;
    };
    $scope.MaskMayus = function () {
        try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
    };
    $scope.MaskNumero = function (option) {
        try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }

    };

    $scope.MaskValores = function () {
        try { $scope.Modelo.ValorBase = MascaraValores($scope.Modelo.ValorBase) } catch (e) { }
    };
}]);