﻿SofttoolsApp.controller("ConsultarListadoComercialCtrl", ['$scope', '$timeout', 'TarifarioVentasFactory', '$linq', 'blockUI', 'blockUIConfig', '$routeParams',
    function ($scope, $timeout, TarifarioVentasFactory, $linq, blockUI, blockUIConfig, $routeParams) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'LISTADOS COMERCIAL'
        $scope.MapaSitio = [{ Nombre: 'Comercial' }, { Nombre: 'Listados' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_LISTADO_COMERCIAL); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        //--Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.ListaComercial = [];
        for (var i = 0; i < $scope.ListadoMenuListados.length; i++) {
            if ($scope.ListadoMenuListados[i].Padre == OPCION_MENU_LISTADO_COMERCIAL) {
                $scope.ListaComercial.push({ NombreMenu: $scope.ListadoMenuListados[i].Nombre, Codigo: $scope.ListadoMenuListados[i].Codigo })
            }
        }
        $scope.ListadosComercial = $scope.ListaComercial[0];
        //--------------------------Variables-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//

        //----------------------------Informacion Requerida para funcionar---------------------------------//

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
    }]); 