﻿SofttoolsApp.controller("ConsultarInspeccionPreoperacionalCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'InspeccionPreoperacionalFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUI, InspeccionPreoperacionalFactory) {
         


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.CodigoCliente = 0
        $scope.MostrarMensajeError = false

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        $scope.OPCION_MENU = OPCION_MENU_INSPECCIONES_PREOPERACIONALES;
        $scope.MapaSitio = [{ Nombre: 'Control Tráfico' }, { Nombre: 'Documentos' }, { Nombre: 'Inspecciones' }];


        if ($routeParams.MeapCodigo !== undefined && $routeParams.MeapCodigo !== null && $routeParams.MeapCodigo !== '' && $routeParams.MeapCodigo !== 0) {
            MeapCodigo = parseInt($routeParams.MeapCodigo);
            if (MeapCodigo > 0) {
                switch (MeapCodigo) {
                    case OPCION_MENU_INSPECCIONES_PREOPERACIONALES:
                        $scope.OPCION_MENU = OPCION_MENU_INSPECCIONES_PREOPERACIONALES;
                        break;
                    case INSPECIONES_GESPHONE:
                        $scope.OPCION_MENU = INSPECIONES_GESPHONE;
                        $scope.MapaSitio = [{ Nombre: 'Inspeciones ' }, { Nombre: 'Preoperacional' }]; 
                        break;
                }
            }
        }
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + $scope.OPCION_MENU);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.DeshabilitarPermisoAnularInspecciones = false
        $scope.ListadoPermisosEspecificos = [];

        $scope.ListadoPermisos.forEach(function (item) {
            if (item.Padre == OPCION_MENU_INSPECCIONES_PREOPERACIONALES) {
                $scope.ListadoPermisosEspecificos.push(item)
            }
        });

        $scope.ListadoPermisosEspecificos.forEach(function (item) {
            if (item.Codigo == PERMISO_ANULAR_INSPECCIONES) {
                $scope.DeshabilitarPermisoAnularInspecciones = true
            }
        })

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
         

        //--Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarInspeccionPreoperacional/0/' + $scope.OPCION_MENU;
            }
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        function Find() {
            if (DatosRequeridos()) {
                blockUI.start('Buscando registros ...');
                $timeout(function () {
                    blockUI.message('Espere por favor ...');
                }, 100);
                $scope.Buscando = true;
                $scope.MensajesError = [];
                $scope.ListadoInspeccionPreoperacional = [];
                if ($scope.Buscando) {
                    if ($scope.MensajesError.length == 0) {

                        filtros = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Numero: $scope.Numero,
                            Vehiculo: { Placa: $scope.Vehiculo },
                            FechaInicial: $scope.FechaInicial,
                            FechaFinal: $scope.FechaFinal,
                            CodigoAlterno: $scope.CodigoAlterno,
                            Conductor: { NombreCompleto: $scope.Conductor },
                            Transportador: { NombreCompleto: $scope.Transportador },
                            Cliente: { NombreCompleto: $scope.Cliente },
                            FormatoInspeccion: { Nombre: $scope.FormatoInspeccion },
                            Estado: $scope.ModalEstado.Codigo,
                            Pagina: $scope.paginaActual,
                            RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                        }
                        blockUI.delay = 1000;
                        InspeccionPreoperacionalFactory.Consultar(filtros).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.length > 0) {
                                        $scope.ListadoInspeccionPreoperacional = response.data.Datos
                                        $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                        $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';
                                    }
                                    else {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }
            }
            blockUI.stop();
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        /*Validacion boton anular */
        $scope.ConfirmacionAnularInspeccion = function (numero, Anulado) {

            if ($scope.ValidarPermisos.AplicaEliminarAnular == PERMISO_ACTIVO) {
                $scope.NumeroAnulacion = numero;
                if (Anulado !== 1) {
                    showModal('modalConfirmacionAnularInspeccion');
                }
                else {
                    closeModal('modalConfirmacionAnularInspeccion');
                }
            }
        };
        /*Funcion que solicita los datos de la anulación */
        $scope.SolicitarDatosAnulacion = function () {
            $scope.FechaAnulacion = new Date();
            closeModal('modalConfirmacionAnularInspeccion');
            showModal('modalDatosAnularInspeccion');
        };
        /*---------------------------------------------------------------------Funcion Anular Inspeccion----------------------------------------------------------------------------*/
        $scope.Anular = function () {

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.NumeroAnulacion,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.CausaAnulacion,
            };

            InspeccionPreoperacionalFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se anuló la inspección No.' + entidad.Numero);
                        closeModal('modalDatosAnularInspeccion');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicial = new Date();
            $scope.FechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if (($scope.FechaInicial == null || $scope.FechaInicial == undefined || $scope.FechaInicial == '')
                && ($scope.FechaFinal == null || $scope.FechaFinal == undefined || $scope.FechaFinal == '')
                && ($scope.Numero == null || $scope.Numero == undefined || $scope.Numero == '' || $scope.Numero == 0)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.Numero !== null && $scope.Numero !== undefined && $scope.Numero !== '' && $scope.Numero !== 0)
                || ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                || ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')
            ) {
                if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                    && ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')) {
                    if ($scope.FechaFinal < $scope.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFinal - $scope.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }
            }
            if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '') && ($scope.FechaFinal == undefined || $scope.FechaFinal == null || $scope.FechaFinal == '')) {
                $scope.FechaFinal = $scope.FechaInicial
            }
            else if (($scope.FechaInicial == null || $scope.FechaInicial == undefined || $scope.FechaInicial == '') && ($scope.FechaFinal !== undefined && $scope.FechaFinal !== null && $scope.FechaFinal !== '')) {
                $scope.FechaInicial = $scope.FechaFinal
            }
            return continuar
        }

        //Desplegar Reporte
        $scope.DesplegarReporte = function (Numero) {
            console.log("Sesion: ", $scope.Sesion);
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

            $scope.FiltroArmado = '';

            //Depende del listado seleccionado se enviará el nombre por parametro
            $scope.NumeroReporte = Numero;
            $scope.NombreReporte = 'RepInspeccionPuntoGestion'
            //Arma el filtro
            $scope.ArmarFiltro();
            //Llama a la pagina ASP con el filtro por GET

            window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + 1);

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        }

        $scope.ArmarFiltro = function () {
            $scope.FiltroArmado = '';
            if ($scope.NumeroReporte !== undefined && $scope.NumeroReporte !== '' && $scope.NumeroReporte !== null) {
                $scope.FiltroArmado += '&Numero=' + $scope.NumeroReporte;
            }
            $scope.FiltroArmado += '&Prefijo=' + $scope.Sesion.Empresa.Prefijo;
        }

        /* Obtener parametros*/
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            $scope.Numero = $routeParams.Numero;
            Find();
        }

    }]);