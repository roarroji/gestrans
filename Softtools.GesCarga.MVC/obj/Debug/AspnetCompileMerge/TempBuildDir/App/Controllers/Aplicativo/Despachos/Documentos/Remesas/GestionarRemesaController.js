﻿SofttoolsApp.controller("GestionarRemesaCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'RutasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'RemesasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, RutasFactory, ValorCatalogosFactory, TercerosFactory, RemesasFactory) {
        console.clear()
        $scope.Titulo = 'GESTIONAR REMESA';
        $scope.MapaSitio = $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Documentos' }, { Nombre: 'Remesas' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_REMESAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.ctr = {
            Importar_ExportarExcel: { Visible: false }
        };
        if ($scope.Sesion.UsuarioAutenticado.ListadoControles.length > 0) {
            for (var i = 0; i < $scope.Sesion.UsuarioAutenticado.ListadoControles.length; i++) {
                var control = $scope.Sesion.UsuarioAutenticado.ListadoControles[i]
                
                if (control.Codigo == Control_Importar_Exportar_Excel_FacturaVenta) {
                    $scope.ctr.Importar_ExportarExcel.Visible = control.Visible > 0 ? true : false;
                }
            }
        }
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            Numero: '',
            Fecha: '',
            Cliente: '',
            DocumentoCliente: '',
            OrdenServicio: '',
            NombreRemitente: '',
            TipoTarifaCliente: '',
            ValorTipoTarifaCliente: '',
            Producto: '',
            Cantidad: '',
            Peso: '',
            Ruta: '',
            PlacaVehiculo: '',
            TipoVehiculo: '',
            PlacaRemolque: '',
            NombreTenedor: '',
            NombreConductor: '',
            TelefonoConductor: '',
            FleteTransportador: '',
            TotalFleteTransportador: '',
            FleteCliente: '',
            TotalFleteCliente: '',
            Anticipo: '',
            OrdenCargue: '',
            NumeroPlanilla: '',
            NumeroManifiesto: '',
            NumeroFactura: '',
            NumeroMinisterio: '',
            ValorDeclarado : ''
        }

        $scope.Calcular = function () {

        }


        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/

        if ($routeParams.Numero > 0) {
            $scope.Modelo.Numero = $routeParams.Numero;
            Obtener();
        }
        else {
            $scope.Modelo.Numero = 0;
        };

        function Obtener() {
            blockUI.start('Cargando remesa...');

            $timeout(function () {
                blockUI.message('Cargando remesa...');
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
                Obtener: 1
            };

            blockUI.delay = 1000;
            RemesasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Numero = response.data.Datos.Numero;
                        $scope.Modelo.NumeroDocumento = response.data.Datos.NumeroDocumento;
                        $scope.Modelo.Fecha = response.data.Datos.Fecha;
                        $scope.Modelo.Cliente = response.data.Datos.Cliente.NombreCompleto;
                        $scope.Modelo.DocumentoCliente = response.data.Datos.NumeroDocumentoCliente;
                        $scope.Modelo.OrdenServicio = response.data.Datos.NumeroOrdenServicio;
                        $scope.Modelo.NombreRemitente = response.data.Datos.Remitente.NombreCompleto;
                        $scope.Modelo.TipoTarifaCliente = response.data.Datos.TarifaCliente.Nombre;
                        $scope.Modelo.ValorTipoTarifaCliente = response.data.Datos.TipoTarifaCliente.Nombre;
                        $scope.Modelo.Producto = response.data.Datos.ProductoTransportado.Nombre;
                        $scope.Modelo.Cantidad = response.data.Datos.CantidadCliente;
                        $scope.Modelo.Peso = $scope.MaskMoneda(response.data.Datos.PesoCliente);
                        $scope.Modelo.Ruta = response.data.Datos.Ruta.Nombre;
                        $scope.Modelo.PlacaVehiculo = response.data.Datos.Vehiculo.Placa;
                        $scope.Modelo.TipoVehiculo = response.data.Datos.TipoVehiculo.Nombre;
                        $scope.Modelo.PlacaRemolque = response.data.Datos.Semirremolque.Placa;
                        $scope.Modelo.NombreTenedor = response.data.Datos.Tenedor.NombreCompleto;
                        $scope.Modelo.NombreConductor = response.data.Datos.Conductor.NombreCompleto;
                        $scope.Modelo.TelefonoConductor = response.data.Datos.Conductor.Telefonos;
                        $scope.Modelo.ValorDeclarado = MascaraValores(response.data.Datos.ValorDeclarado);
                        $scope.Modelo.FleteTransportador = $scope.MaskMoneda(response.data.Datos.ValorFleteTransportador);
                        $scope.Modelo.TotalFleteTransportador = $scope.MaskMoneda(response.data.Datos.TotalFleteTransportador);
                        $scope.Modelo.FleteCliente = $scope.MaskMoneda(response.data.Datos.ValorFleteCliente);
                        $scope.Modelo.TotalFleteCliente = $scope.MaskMoneda(response.data.Datos.TotalFleteCliente);
                        //$scope.Modelo.Anticipo = 
                        $scope.Modelo.NombreDestinatario = response.data.Datos.Destinatario.NombreCompleto;
                        $scope.Modelo.CiudadDestinatario = response.data.Datos.Destinatario.Ciudad.Nombre;
                        $scope.Modelo.DireccionDestinatario = response.data.Datos.DireccionDestinatario;
                        $scope.Modelo.TelefonoDestinatario = response.data.Datos.TelefonoDestinatario;
                        $scope.Modelo.OrdenCargue = response.data.Datos.OrdenCargue.NumeroDocumento;
                        $scope.Modelo.NumeroPlanilla = response.data.Datos.Planilla.NumeroDocumento;
                        $scope.Modelo.NumeroManifiesto = response.data.Datos.Manifiesto.NumeroDocumento;
                        $scope.Modelo.NumeroFactura = response.data.Datos.Factura.Numero;
                        $scope.Modelo.NumeroMinisterio = response.data.Datos.NumeroConfirmacionMinisterio;
                        $scope.ModeloOficina = response.data.Datos.Oficina.Nombre
                        $scope.ModalNumeroContenedor = response.data.Datos.NumeroContenedor
                        $scope.ModalMBLContenedor = response.data.Datos.MBLContenedor
                        $scope.ModalHBLContenedor = response.data.Datos.HBLContenedor
                        $scope.ModalDOContenedor = response.data.Datos.DOContenedor
                        $scope.ModalValorFOB = response.data.Datos.ValorFOB
                        $scope.Modelo.DT = response.data.Datos.DT
                        $scope.Modelo.Entrega = response.data.Datos.Entrega
                        $scope.Modelo.TerceroFacturarA = response.data.Datos.TerceroFacturarA
                        if (response.data.Datos.Estado == 1) {
                            $scope.ModeloEstado = 'DEFINITIVO'
                        } else {
                            $scope.ModeloEstado = 'BORRADOR'
                        }
                        if (response.data.Datos.TarifaCliente != 1) {
                            $('#btnContenedor').collapse("Hide");
                        }
                        $scope.MaskValores()
                    }
                    else {
                        ShowError('No se logro consultar la remesa número ' + $routeParams.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarRemesas';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarRemesas';
                });

            blockUI.stop();
        };

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                $scope.Modelo.Remesa.Fecha = RetornarFechaEspecificaSinTimeZone($scope.Modelo.Remesa.Fecha);
                if ($scope.Modelo.Remesa.Remitente.Codigo !== undefined && $scope.Modelo.Remesa.Remitente.Codigo > 0) {
                    try {
                        if ($scope.Modelo.Remesa.Remitente.Direccion.Direccion !== undefined) {
                            $scope.Modelo.Remesa.Remitente.Direccion = $scope.Modelo.Remesa.Remitente.Direccion.Direccion
                        }
                    } catch (e) {

                    }
                }
                if ($scope.Modelo.Remesa.Destinatario.Codigo !== undefined && $scope.Modelo.Remesa.Destinatario.Codigo > 0) {
                    try {
                        if ($scope.Modelo.Remesa.Destinatario.Direccion.Direccion !== undefined) {
                            $scope.Modelo.Remesa.Destinatario.Direccion = $scope.Modelo.Remesa.Destinatario.Direccion.Direccion;
                        }
                    } catch (e) {

                    }
                }
                if ($scope.Modelo.Remesa.ProductoTransportado.Codigo == undefined || $scope.Modelo.Remesa.ProductoTransportado.Codigo == 0) {
                    $scope.Modelo.Remesa.ProductoTransportado = { Nombre: $scope.Modelo.Remesa.ProductoTransportado, Descripcion: $scope.Modelo.Remesa.DescripcionProductoTransportado }
                }


                RemesaGuiasFactory.Guardar($scope.RemesaPaqueteria).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Numero == 0) {
                                    ShowSuccess('Se guardó la guía de paqueteria con el número ' + response.data.Datos + '');
                                }
                                else {
                                    ShowSuccess('Se modificó la guía de paqueteria con el número ' + response.data.Datos + '');
                                }
                                location.href = '#!ConsultarGuiaPaqueterias/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var modelo = $scope.Modelo

            if (modelo.Remesa.Cliente.Codigo == undefined || modelo.Remesa.Cliente.Codigo == 0 || modelo.Remesa.Cliente.Codigo == '') {
                $scope.MensajesError.push('Debe ingresar el cliente ');
                continuar = false;
            }
            if (modelo.Remesa.FormaPago.Codigo == 6100) {
                $scope.MensajesError.push('Debe ingresar la forma de pago');
                continuar = false;
            }
            else if ($scope.TarifarioSeleccionado == undefined || $scope.TarifarioSeleccionado == '') {
                $scope.MensajesError.push('Debe seleccionar los datos de la tarifa');
                continuar = false;
            }
            else if ($scope.TarifarioSeleccionado.Codigo == undefined || $scope.TarifarioSeleccionado.Codigo == '' || $scope.TarifarioSeleccionado.Codigo == 0) {
                $scope.MensajesError.push('Debe seleccionar los datos de la tarifa');
                continuar = false;
            }
            else {

                if (ValidarCampo(modelo.Remesa.ProductoTransportado) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el producto ');
                    continuar = false;
                } else if (modelo.Remesa.ProductoTransportado.Codigo == undefined || modelo.Remesa.ProductoTransportado.Codigo == 0) {
                    if (ValidarCampo(modelo.Remesa.DescripcionProductoTransportado) == OBJETO_SIN_DATOS) {
                        $scope.MensajesError.push('Debe ingresar la descripción del producto');
                        continuar = false;
                    }
                }
                if (ValidarCampo(modelo.Remesa.PesoCliente) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el peso');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.CantidadCliente) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar la cantidad');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Remesa.ValorComercialCliente) == OBJETO_SIN_DATOS) {
                    $scope.MensajesError.push('Debe ingresar el valor comercial');
                    continuar = false;
                }

                if (modelo.TipoEntregaRemesaPaqueteria.Codigo == 6600) {
                    $scope.MensajesError.push('Debe seleccionar el tipo de entrega');
                    continuar = false;
                }
                if (modelo.TipoTransporteRemsaPaqueteria.Codigo == 6200) {
                    $scope.MensajesError.push('Debe seleccionar el tipo de transporte');
                    continuar = false;
                }
                if (modelo.TipoDespachoRemesaPaqueteria.Codigo == 6300) {
                    $scope.MensajesError.push('Debe seleccionar el tipo de guía');
                    continuar = false;
                }

                if (modelo.TipoServicioRemesaPaqueteria.Codigo == 6100) {
                    $scope.MensajesError.push('Debe seleccionar el tipo de servicio');
                    continuar = false;
                }

                if (modelo.Remesa.Remitente.Codigo == 0 || modelo.Remesa.Remitente.Codigo == undefined) {
                    if (ValidarCampo(modelo.Remesa.Remitente.NumeroIdentificacion) == OBJETO_SIN_DATOS) {
                        $scope.MensajesError.push('Debe ingresar el número de identificación del remitente');
                        continuar = false;
                    }
                    if (modelo.Remesa.Remitente.TipoIdentificacion.Codigo == 100) {
                        $scope.MensajesError.push('Debe seleccionar el tipo de identificación del remitente');
                        continuar = false;
                    }
                    if (ValidarCampo(modelo.Remesa.Remitente.Nombre) == OBJETO_SIN_DATOS) {
                        $scope.MensajesError.push('Debe ingresar el nombre del remitente');
                        continuar = false;
                    }
                    if (ValidarCampo(modelo.Remesa.Remitente.Ciudad) == OBJETO_SIN_DATOS) {
                        $scope.MensajesError.push('Debe seleccionar la ciudad del remitente');
                        continuar = false;
                    }
                    if (ValidarCampo(modelo.Remesa.Remitente.Direccion) == OBJETO_SIN_DATOS) {
                        $scope.MensajesError.push('Debe seleccionar la dirección del remitente');
                        continuar = false;
                    }
                    if (ValidarCampo(modelo.Remesa.Remitente.Telefonos) == OBJETO_SIN_DATOS) {
                        $scope.MensajesError.push('Debe ingresar el teléfono del remitente');
                        continuar = false;
                    }

                }
                if (modelo.Remesa.Destinatario.Codigo == 0 || modelo.Remesa.Destinatario.Codigo == undefined) {
                    if (ValidarCampo(modelo.Remesa.Destinatario.NumeroIdentificacion) == OBJETO_SIN_DATOS) {
                        $scope.MensajesError.push('Debe ingresar el número de identificación del destinatario');
                        continuar = false;
                    }
                    if (modelo.Remesa.Destinatario.TipoIdentificacion.Codigo == 100) {
                        $scope.MensajesError.push('Debe seleccionar el tipo de identificación del destinatario');
                        continuar = false;
                    }
                    if (ValidarCampo(modelo.Remesa.Destinatario.Nombre) == OBJETO_SIN_DATOS) {
                        $scope.MensajesError.push('Debe ingresar el nombre del destinatario');
                        continuar = false;
                    }
                    if (ValidarCampo(modelo.Remesa.Destinatario.Ciudad) == OBJETO_SIN_DATOS) {
                        $scope.MensajesError.push('Debe seleccionar la ciudad del destinatario');
                        continuar = false;
                    }
                    if (ValidarCampo(modelo.Remesa.Destinatario.Direccion) == OBJETO_SIN_DATOS) {
                        $scope.MensajesError.push('Debe seleccionar la dirección del destinatario');
                        continuar = false;
                    }
                    if (ValidarCampo(modelo.Remesa.Destinatario.Telefonos) == OBJETO_SIN_DATOS) {
                        $scope.MensajesError.push('Debe ingresar el teléfono del destinatario');
                        continuar = false;
                    }
                }
                else if (modelo.Remesa.Remitente.NumeroIdentificacion == modelo.Remesa.Destinatario.NumeroIdentificacion) {
                    $scope.MensajesError.push('El remitenete no puede ser igual al destinatario');
                    continuar = false;
                }
            }
            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarRemesas/' + $scope.Modelo.NumeroDocumento;
        };

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskMoneda = function (item) {
            return MascaraValores(item)
        };

        /* Obtener parametros*/
        try {
            var Parametros = $routeParams.Codigo.split(',')
            if (Parametros.length > 1) {
                $scope.Modelo.Numero = Parametros[0];
                $scope.aplicabase = true
                Obtener();
            } else {
                if ($routeParams.Codigo > 0) {
                    $scope.Modelo.Numero = $routeParams.Codigo;
                    Obtener();
                }
                else {
                    $scope.Modelo.Numero = 0;
                }

            }
        } catch (e) {
            if ($routeParams.Codigo > 0) {
                $scope.Modelo.Numero = $routeParams.Codigo;
                Obtener();
            }
            else {
                $scope.Modelo.Numero = 0;
            }

        }

    }]);