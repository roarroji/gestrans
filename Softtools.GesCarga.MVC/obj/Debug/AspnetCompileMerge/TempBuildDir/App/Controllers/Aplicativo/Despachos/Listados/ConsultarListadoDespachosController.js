﻿SofttoolsApp.controller("ConsultarListadoDespachosCtrl", ['$scope', '$timeout', 'TercerosFactory', 'RutasFactory', 'OficinasFactory', 'TarifarioVentasFactory', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'ValorCatalogosFactory', 'EmpresasFactory', 'VehiculosFactory',
    function ($scope, $timeout, TercerosFactory, RutasFactory, OficinasFactory, TarifarioVentasFactory, $linq, blockUI, blockUIConfig, $routeParams, ValorCatalogosFactory, EmpresasFactory, VehiculosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Listados' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.DeshabilitarPDF = true;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.NombreReporte = '';
        $scope.FiltroArmado = '';
        $scope.ListadoCliente = [];
        $scope.FiltroCliente = true;
        $scope.FiltroVehiculo = false;
        $scope.FiltroTransportador = false;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaFinal = new Date()
        }

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {} }
        }

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OCION_MENU_LISTADOS_DESPACHOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        // Cargar combos de módulo y listados 
        $scope.ListaDespacho = [];
        for (var i = 0; i < $scope.ListadoMenuListados.length; i++) {
            if ($scope.ListadoMenuListados[i].Padre == OCION_MENU_LISTADOS_DESPACHOS) {
                $scope.ListaDespacho.push({ NombreMenu: $scope.ListadoMenuListados[i].Nombre, Codigo: $scope.ListadoMenuListados[i].Codigo })
            }
        }
        console.log('listado es', $scope.ListaDespacho)

        $scope.ListaEstadosRegistros = [];
        $scope.ListaEstadosRegistros.push(
            { Codigo: 0, Nombre: '(TODOS)' },
            { Codigo: 1, Nombre: 'ACTIVOS' },
            { Codigo: 2, Nombre: 'INACTIVOS' },
            { Codigo: 3, Nombre: 'ANULADOS' }
        )
        $scope.ModeloEstadoRegistro = $scope.ListaEstadosRegistros[0];
        $scope.ListaEstadosPrecintos = [];
        $scope.ListaEstadosPrecintos.push(
            { Codigo: 0, Nombre: '(TODOS)' },
            { Codigo: 3, Nombre: 'ANULADOS' },
            { Codigo: 4, Nombre: 'ASIGNADOS' },
            { Codigo: 5, Nombre: 'PENDIENTE ASIGNAR' },
        )
        $scope.ModeloEstadoRegistroPrecintos = $scope.ListaEstadosPrecintos[0];
        $scope.ModeloDocumentosDespachos = $scope.ListaDespacho[0]

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;


        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });


        /*Cargar el combo de ESTADO DE REMESAS PAQUETERIA*/

        $scope.ListadoEstadoRemesaPaqueteria = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 0, Nombre: 'BORRADOR' }, { Codigo: 1, Nombre: 'DEFINITIVO' }, { Codigo: 2, Nombre: 'ANULADO' }];
        $scope.ModeloEstadoGuia = $scope.ListadoEstadoRemesaPaqueteria[0];

        $scope.ListadoEstadoLiquidacion = [{ Codigo: 0, Nombre: '(TODOS)' }, { Codigo: 1, Nombre: 'Pendiente Aprobar' }, { Codigo: 2, Nombre: 'Aprobado' }, { Codigo: 3, Nombre: 'Pagado' }];
        $scope.ModeloEstadoLiquidaciones = $scope.ListadoEstadoLiquidacion[0];

        $scope.ListadoEstadoPlaneacion = [{ Codigo: 0, Nombre: '(TODOS)' }, { Codigo: 1, Nombre: 'PENDIENTES HOY' }, { Codigo: 2, Nombre: 'PENDIENTES ANTERIORES' }, { Codigo: 3, Nombre: 'PENDIENTES MAÑANA' }, { Codigo: 4, Nombre: 'PROYECTADO' }, { Codigo: 5, Nombre: 'DESPACHADO' }];
        $scope.ModeloEstadoPlaneacion = $scope.ListadoEstadoPlaneacion[0];
        /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)

        };

        //Consultar rutas
        RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoRutas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoRutas = response.data.Datos
                    }
                    else {
                        $scope.ListadoRutas = []
                    }
                }
            }, function (response) {
            });

        //Funcion Autocomplete Vehiculos
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos);
                }
            }
            return $scope.ListadoVehiculos;
        };


        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            OficinasFactory.Consultar({ CodigoCiudad: $scope.CodigoCiudad, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoOficinas = [];
                        $scope.HabilitarCiudad = false
                        $scope.ListadoOficinas = [
                            { Codigo: 0, Nombre: '(TODOS)' }
                        ];
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item)
                        });
                        if ($scope.ListadoOficinas.length > 0) {
                            $scope.ModeloOficinas = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==0');

                        }

                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas = [
                    { Codigo: 0, Nombre: '(TODOS)' }
                ];
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                if ($scope.ListadoOficinas.length > 0) {
                    $scope.ModeloOficinas = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==0');

                }
                //$scope.ListadoOficinas = $scope.Sesion.UsuarioAutenticado.ListadoOficinas
                //$scope.ModeloOficinas = $scope.ListadoOficinas[0]
            } else {
                ShowError('El usuario no tiene oficinas asociadas')
            }
        }
        $scope.LabelTercero = 'Cliente';
        $scope.AsignarFiltro = function (CodigoLista) {
            $scope.LabelTercero = 'Cliente';
            if (CodigoLista == CODIGO_LITADO_ORDEN_CARGUE_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LITADO_ORDEN_CARGUE_CLIENTE) {
                $scope.FiltroCliente = true;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;

            }
            if (CodigoLista == CODIGO_LITADO_REMESA_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LITADO_REMESA_CLIENTE) {
                $scope.FiltroCliente = true;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_MANIFIESTO_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_MANIFIESTO_TRANSPORTADOR) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = true;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_PLANILLA_DESPACHO_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_PLANILLA_DESPACHO_TRANSPORTADOR) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = true;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_CUMPLIDOS_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_CUMPLIDOS_TRANSPORTADOR) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = true;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_LIQUIDACION_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_LIQUIDACION_TRANSPORTADOR) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = true;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_REMESA_PENDIENTES_CUMPLIR_CLIENTE) {
                $scope.FiltroCliente = true;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_REMESA_PENDIENTES_CUMPLIR_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_REMESA_PENDIENTES_FACTURAR_CLIENTE) {
                $scope.FiltroCliente = true;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_REMESA_PENDIENTES_FACTURAR_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_TRANSPORTADOR) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = true;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.LabelTercero = 'Conductor';
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_TRANSPORTADOR) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = true;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_NOVEDADES_REMESA) {
                $scope.FiltroCliente = true;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = false;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_DISTRIBUCION_REMESA) {
                $scope.FiltroCliente = true;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = false;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_PLANEACION_CUMPLIMIENTO) {
                $scope.FiltroCliente = true;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = false;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_PRECINTO_DESPACHO_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_REMESAS_PENDIENTES_TIEMPOS) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_OPERACION_REAL) {
                $scope.FiltroCliente = true;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = false;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_TIEMPOS_REMESAS) {
                $scope.FiltroCliente = true;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = false;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_SABANA_DATOS) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = false;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_INFORME_LIQUIDACIONES) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_INFORME_GENERAL_OPERACIONES) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = false;
                $scope.FiltroVehiculo = false;
            }
            if (CodigoLista == CODIGO_LISTADO_REMESAS_GENERALES_TIPO) {
                $scope.FiltroCliente = true;
                $scope.FiltroTransportador = false;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = true;
                $scope.FiltroVehiculo = true;
            }
            if (CodigoLista == CODIGO_LISTADO_RECHAZOS_ENTURNAMIENTOS) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = true;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = false;
                $scope.FiltroVehiculo = true;
            }
            if (CodigoLista == CODIGO_LISTADO_INFORME_PARA_SEGUROS) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = true;
                $scope.ModeloCliente = '';
                $scope.ModeloTransportador = '';
                $scope.DeshabilitarPDF = false;
                $scope.FiltroVehiculo = true;
            }
        }


        $scope.DesplegarInforme = function (OpcionPDf, OpcionEXCEL) {
            var DatosRequeridos = false;
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

            switch ($scope.ModeloDocumentosDespachos.Codigo) {
                case CODIGO_LISTADO_PLANEACION_CUMPLIMIENTO:
                    DatosRequeridos = true
                    break;
                case CODIGO_LISTADO_REMESAS_PENDIENTES_TIEMPOS:
                    DatosRequeridos = $scope.DatosRequeridos2();
                    break;
                
                default:
                    DatosRequeridos = $scope.DatosRequeridos();
                    break;
            }
            //if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_PLANEACION_CUMPLIMIENTO) {
            //    DatosRequeridos = true
            //} else {
            //    $scope.DatosRequeridos();
            //}
            if (DatosRequeridos == true) {
                //Depende del listado seleccionado se enviará el nombre por parametro
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_NOVEDADES_REMESA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_NOVEDADES_REMESAS;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_DISTRIBUCION_REMESA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_DISTRIBUCION_REMESAS;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_ORDEN_CARGUE_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LITADO_ORDEN_CARGUE_OFICINA;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_ORDEN_CARGUE_CLIENTE) {
                    $scope.NombreReporte = NOMBRE_LITADO_ORDEN_CARGUE_CLIENTE;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_REMESA_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LITADO_REMESA_OFICINA;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_REMESA_CLIENTE) {
                    $scope.NombreReporte = NOMBRE_LITADO_REMESA_CLIENTE;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_MANIFIESTO_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_MANIFIESTO_OFICINA;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_MANIFIESTO_TRANSPORTADOR) {
                    $scope.NombreReporte = NOMBRE_LISTADO_MANIFIESTO_TRANSPORTADOR;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_PLANILLA_DESPACHO_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_PLANILLA_DESPACHO_OFICINA;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_PLANILLA_DESPACHO_TRANSPORTADOR) {
                    $scope.NombreReporte = NOMBRE_LISTADO_PLANILLA_DESPACHO_TRANSPORTADOR;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_CUMPLIDOS_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_CUMPLIDOS_OFICINA;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_CUMPLIDOS_TRANSPORTADOR) {
                    $scope.NombreReporte = NOMBRE_LISTADO_CUMPLIDOS_TRANSPORTADOR;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_LIQUIDACION_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_LIQUIDACION_OFICINA;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_LIQUIDACION_TRANSPORTADOR) {
                    $scope.NombreReporte = NOMBRE_LISTADO_LIQUIDACION_TRANSPORTADOR;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_REMESA_PENDIENTES_CUMPLIR_CLIENTE) {
                    $scope.NombreReporte = NOMBRE_LISTADO_REMESA_PENDIENTES_CUMPLIR_CLIENTE;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_REMESA_PENDIENTES_CUMPLIR_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_REMESA_PENDIENTES_CUMPLIR_OFICINA;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_REMESA_PENDIENTES_FACTURAR_CLIENTE) {
                    $scope.NombreReporte = NOMBRE_LISTADO_REMESA_PENDIENTES_FACTURAR_CLIENTE;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_REMESA_PENDIENTES_FACTURAR_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_REMESA_PENDIENTES_FACTURAR_OFICINA;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_TRANSPORTADOR) {
                    $scope.NombreReporte = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_TRANSPORTADOR;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_CUMPLIR_OFICINA;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_TRANSPORTADOR) {
                    $scope.NombreReporte = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_TRANSPORTADOR;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR_OFICINA;
                    $scope.ArmarFiltro();
                } if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR) {
                    $scope.NombreReporte = NOMBRE_LISTADO_PLANILLA_DESPACHO_PENDIENTES_LIQUIDAR;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_PLANEACION_CUMPLIMIENTO) {
                    $scope.NombreReporte = NOMBRE_LISTADO_PLANEACION_CUMPLIMIENTO;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_PRECINTO_DESPACHO_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_PRECINTO_DESPACHO_OFICINA;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_REMESAS_PENDIENTES_TIEMPOS) {
                    $scope.NombreReporte = NOMBRE_LISTADO_REMESAS_PENDIENTES_TIEMPOS;
                    $scope.ArmarFiltro2();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_OPERACION_REAL) {
                    $scope.NombreReporte = NOMBRE_LISTADO_OPERACION_REAL;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_TIEMPOS_REMESAS) {
                    $scope.NombreReporte = NOMBRE_LISTADO_TIEMPOS_REMESAS;

                    $scope.ArmarFiltro();
                }
                
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_SABANA_DATOS) {
                    $scope.NombreReporte = NOMBRE_LISTADO_SABANA_DATOS;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_INFORME_LIQUIDACIONES) {
                    $scope.NombreReporte = NOMBRE_LISTADO_INFORME_LIQUIDACIONES;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_INFORME_GENERAL_OPERACIONES) {
                    $scope.NombreReporte = NOMBRE_LISTADO_INFORME_GENERAL_OPERACIONES;
                    $scope.ArmarFiltro();
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_REMESAS_GENERALES_TIPO) {
                    $scope.NombreReporte = NOMBRE_LISTADO_REMESAS_GENERALES_TIPO;
                    $scope.ArmarFiltro();

                } if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_RECHAZOS_ENTURNAMIENTOS) {
                    $scope.NombreReporte = NOMBRE_LISTADO_RECHAZOS_ENTURNAMIENTOS;
                    $scope.ArmarFiltro();
                } if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_INFORME_PARA_SEGUROS) {
                    $scope.NombreReporte = NOMBRE_LISTADO_INFORME_SEGUROS;
                    $scope.ArmarFiltro();
                }
                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
                }
                if (OpcionEXCEL == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL);
                }

                //Se limpia el filtro para poder hacer mas consultas
                $scope.FiltroArmado = '';

            }
        };
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaInicial = new Date();
            $scope.ModeloFechaFinal = new Date();
        }
        $scope.DatosRequeridos = function () {
            var DatosRequeridos = true;
            $scope.MensajesError = [];
            $scope.Rango = $scope.ModeloNumeroFinal - $scope.ModeloNumeroInicial;

            if (($scope.ModeloFechaInicial == null || $scope.ModeloFechaInicial == undefined || $scope.ModeloFechaInicial == '')
                && ($scope.ModeloFechaFinal == null || $scope.ModeloFechaFinal == undefined || $scope.ModeloFechaFinal == '')
                && ($scope.ModeloNumeroInicial == null || $scope.ModeloNumeroInicial == undefined || $scope.ModeloNumeroInicial == '' || $scope.ModeloNumeroInicial == 0)
                && ($scope.ModeloNumeroFinal == null || $scope.ModeloNumeroFinal == undefined || $scope.ModeloNumeroFinal == '' || $scope.ModeloNumeroFinal == 0)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de números o fechas');
                DatosRequeridos = false

            } else if (($scope.ModeloNumeroInicial !== null && $scope.ModeloNumeroInicial !== undefined && $scope.ModeloNumeroInicial !== '' && $scope.ModeloNumeroInicial !== 0)
                || ($scope.ModeloNumeroFinal !== null && $scope.ModeloNumeroFinal !== undefined && $scope.ModeloNumeroFinal !== '' && $scope.ModeloNumeroFinal !== 0)
                || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloNumeroInicial !== null && $scope.ModeloNumeroInicial !== undefined && $scope.ModeloNumeroInicial !== '' && $scope.ModeloNumeroInicial !== 0)
                    && ($scope.ModeloNumeroFinal !== null && $scope.ModeloNumeroFinal !== undefined && $scope.ModeloNumeroFinal !== '' && $scope.ModeloNumeroFinal !== 0)) {
                    if ($scope.ModeloNumeroFinal < $scope.ModeloNumeroInicial) {
                        $scope.MensajesError.push('El número final debe ser mayor al número final');
                        DatosRequeridos = false
                    }
                    else if ($scope.Rango > RANGO_MAXIMO_CANTIDAD_DOCUMENTOS_LISTADOS) {
                        $scope.MensajesError.push('La diferencia del rango entre el numero inicial y el numero final no puede ser mayor a 2000');
                        DatosRequeridos = false
                    }
                } else {
                    if (($scope.ModeloNumeroInicial !== null && $scope.ModeloNumeroInicial !== undefined && $scope.ModeloNumeroInicial !== '' && $scope.ModeloNumeroInicial !== 0)) {
                        $scope.ModeloNumeroFinal = $scope.ModeloNumeroInicial
                    } else {
                        $scope.ModeloNumeroInicial = $scope.ModeloNumeroFinal
                    }
                }
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        DatosRequeridos = false
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        DatosRequeridos = false
                    }
                }
                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal
                    }
                }
            }
            return DatosRequeridos;
        }


        $scope.DatosRequeridos2 = function () {
            var DatosRequeridos = true;
            $scope.MensajesError = [];

            if (($scope.ModeloFechaInicial == null || $scope.ModeloFechaInicial == undefined || $scope.ModeloFechaInicial == '')
                && ($scope.ModeloFechaFinal == null || $scope.ModeloFechaFinal == undefined || $scope.ModeloFechaFinal == '')
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas');
                DatosRequeridos = false

            }
            else {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        DatosRequeridos = false
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        DatosRequeridos = false
                    }
                }
                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal
                    }
                }
            }
            return DatosRequeridos;
        }

        $scope.ArmarFiltro = function () {

            if ($scope.ModeloNumeroInicial !== undefined && $scope.ModeloNumeroInicial !== '' && $scope.ModeloNumeroInicial !== null && $scope.ModeloNumeroInicial > 0) {
                $scope.FiltroArmado += '&Numero_Documento_Inicial=' + $scope.ModeloNumeroInicial;
            }
            if ($scope.ModeloNumeroFinal !== undefined && $scope.ModeloNumeroFinal !== '' && $scope.ModeloNumeroFinal !== null && $scope.ModeloNumeroFinal > 0) {
                $scope.FiltroArmado += '&Numero_Documento_Final=' + $scope.ModeloNumeroFinal;
            }
            if ($scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '' && $scope.ModeloFechaInicial !== null) {
                var ModeloFechaInicial = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaInicial);
                $scope.FiltroArmado += '&Fecha_Inicial=' + ModeloFechaInicial;
            }
            if ($scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '' && $scope.ModeloFechaFinal !== null) {
                var ModeloFechaFinal = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaFinal);
                $scope.FiltroArmado += '&Fecha_Final=' + ModeloFechaFinal;
            }
            if ($scope.ModeloCliente !== undefined && $scope.ModeloCliente !== '' && $scope.ModeloCliente !== null) {
                $scope.FiltroArmado += '&Identificacion_Cliente=' + $scope.ModeloCliente;
            }
            if ($scope.ModeloTransportador !== undefined && $scope.ModeloTransportador !== '' && $scope.ModeloTransportador !== null) {
                $scope.FiltroArmado += '&Identificacion_Cliente=' + $scope.ModeloTransportador;
            }
            if ($scope.ModeloOficinas.Codigo !== undefined && $scope.ModeloOficinas.Codigo !== '' && $scope.ModeloOficinas.Codigo !== null && $scope.ModeloOficinas.Codigo > 0) {
                $scope.FiltroArmado += '&Codigo_Oficina=' + $scope.ModeloOficinas.Codigo;
            }
            if ($scope.ModeloRuta !== undefined && $scope.ModeloRuta !== '' && $scope.ModeloRuta !== null) {
                $scope.FiltroArmado += '&Ruta=' + $scope.ModeloRuta;
            }
            if ($scope.ModeloVehiculo !== undefined && $scope.ModeloVehiculo !== '' && $scope.ModeloVehiculo !== null && $scope.ModeloVehiculo.Codigo !== 0) {
                $scope.FiltroArmado += '&Vehiculo=' + $scope.ModeloVehiculo.Codigo;
            }
            if ($scope.ModeloEstadoGuia.Codigo != -1) {
                $scope.FiltroArmado += '&Estado=' + $scope.ModeloEstadoGuia.Codigo;
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == 400232 && $scope.ModeloEstadoLiquidaciones.Codigo > 0) {
                $scope.FiltroArmado += '&EstadoLiquidaciones=' + $scope.ModeloEstadoLiquidaciones.Codigo;
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_PRECINTO_DESPACHO_OFICINA) {
                if ($scope.ModeloEstadoRegistroPrecintos.Codigo != 0) {
                    $scope.FiltroArmado += '&Estado=' + $scope.ModeloEstadoRegistroPrecintos.Codigo;
                }
            } else {
                if ($scope.ModeloEstadoRegistro.Codigo != 0) {
                    $scope.FiltroArmado += '&Estado=' + $scope.ModeloEstadoRegistro.Codigo;
                }
            }
            if ($scope.ModeloCliente !== undefined && $scope.ModeloCliente !== '' && $scope.ModeloCliente !== null) {
                $scope.FiltroArmado += '&Cliente=' + $scope.ModeloCliente;
            }
            if ($scope.ModeloDestinatario !== undefined && $scope.ModeloDestinatario !== '' && $scope.ModeloDestinatario !== null) {
                $scope.FiltroArmado += '&Destinatario=' + $scope.ModeloDestinatario;
            }
            if ($scope.ModeloDestino !== undefined && $scope.ModeloDestino !== '' && $scope.ModeloDestino !== null) {
                $scope.FiltroArmado += '&Destino=' + $scope.ModeloDestino;
            }
            if ($scope.ModeloEstadoPlaneacion.Codigo != 0) {
                $scope.FiltroArmado += '&EstadoPlaneacion=' + $scope.ModeloEstadoPlaneacion.Codigo;
            }

        }

        $scope.ArmarFiltro2 = function () {
            if ($scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '' && $scope.ModeloFechaInicial !== null) {
                var ModeloFechaInicial = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaInicial);
                $scope.FiltroArmado += '&Fecha_Inicial=' + ModeloFechaInicial;
            }
            if ($scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '' && $scope.ModeloFechaFinal !== null) {
                var ModeloFechaFinal = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaFinal);
                $scope.FiltroArmado += '&Fecha_Final=' + ModeloFechaFinal;
            }
            if ($scope.ModeloOficinas.Codigo !== undefined && $scope.ModeloOficinas.Codigo !== '' && $scope.ModeloOficinas.Codigo !== null && $scope.ModeloOficinas.Codigo > 0) {
                $scope.FiltroArmado += '&Codigo_Oficina=' + $scope.ModeloOficinas.Codigo;
            }
        }

    }]);