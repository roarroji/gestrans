﻿SofttoolsApp.controller("GestionarFidelizacionCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUIConfig', 'blockUI', 'TercerosFactory', 'VehiculosFactory', 'ValorCatalogosFactory',
    'PuntosVehiculosFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUIConfig, blockUI,TercerosFactory, VehiculosFactory, ValorCatalogosFactory,
        PuntosVehiculosFactory) {

        $scope.Titulo = "NUEVA FIDELIZACIÓN";
        $scope.MapaSitio = [{ Nombre: 'Fidelización' }, { Nombre: 'Documentos' }, { Nombre: 'Fidelización' }, { Nombre: 'Gestionar' }];
        $scope.Master = "#!ConsultarFidelizacion";
        $scope.ActivarCategoria = false;
        //------------------------------------------------------------DECLARACION VARIABLES------------------------------------------------------------//
        var NullDate = '1/01/1900';
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.Deshabilitar = false;
        $scope.DeshabilitarEstado = false;
        $scope.DeshabilitarCategoria = false;
        $scope.MensajesError = [];
        var filtros = {};
        $scope.pref = '';
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_FIDELIZACION); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Fecha: new Date(),
            Vehiculo: "",
            Puntos: "",
        }
        $scope.CodigoVehi = CERO;
        $scope.ListadoCategorias = [];
        $scope.ListadoEstados = [
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        //------------------------------------------------------------OBTENER INFORMACION------------------------------------------------------------//
        //-- catalogo Categoria fidelizacion
        var CATA_CFTRCargado = false;
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CATEGORIA_FIDELIZACION } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCategorias.push({ Nombre: 'SELECCIONE', Codigo: -1 });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoCategorias.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo })
                        }
                        $scope.Modelo.Categoria = $linq.Enumerable().From($scope.ListadoCategorias).First('$.Codigo == -1');
                        if ($scope.ActivarCategoria == false) {
                            $scope.Modelo.Categoria = $linq.Enumerable().From($scope.ListadoCategorias).First('$.Codigo == ' + CODIGO_CATALOGO_CATEGORIA_FIDELIZACION_NINGUNO);
                        }
                        CATA_CFTRCargado = true;
                    }
                    else {
                        $scope.ListadoCategorias = [];
                        CATA_CFTRCargado = true;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
                CATA_CFTRCargado = true;
            });
        //--Validar existencia de Vehiculo en Fidelizacion
        $scope.ValidarVehiculo = function (Vehiculo) {
            if (Vehiculo != undefined && Vehiculo != null && Vehiculo != "") {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Vehiculo: { Codigo: Vehiculo.Codigo },
                    Estado: -1
                };
                PuntosVehiculosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                ShowError("Ya existe una fidelización con el vehículo " + $scope.Modelo.Vehiculo.Placa);
                                $scope.Modelo.Vehiculo = "";
                            }
                            else {
                                $scope.ObtenerInformacionTenedor();
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        $scope.Modelo.Vehiculo = "";
                    });
            }
        }
        //--Validar existencia de Vehiculo en Fidelizacion
        //--Informacion Tenedero
        $scope.ObtenerInformacionTenedor = function () {
            if ($scope.Modelo.Vehiculo.Tenedor !== undefined) {
                $scope.Modelo.Tenedor = $scope.Modelo.Vehiculo.Tenedor;
            }
            if ($scope.Modelo.Vehiculo.Conductor !== undefined) {
                $scope.Modelo.Conductor = $scope.Modelo.Vehiculo.Conductor;
            }
        }
        //------------------------------------------------------------FUNCIONES AUTOCOMPLETE------------------------------------------------------------//
        //--Vehiculos
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                }
            }
            return $scope.ListadoVehiculos
        }
        //------------------------------------------------------------MASCARAS------------------------------------------------------------//
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        //------------------------------------------------------------FUNCIONES GENERALES------------------------------------------------------------
        $scope.VolverMaster = function () {
            document.location.href = $scope.Master;
        };

        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
        };
        //--Funcion Guardar Datos
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            var PlanPuntosVehiculos = {
                CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                Codigo: $scope.CodigoVehi,
                UsuarioCrea: { Codigo: $scope.Modelo.UsuarioCrea.Codigo },
                UsuarioModifica: { Codigo: $scope.Modelo.UsuarioModifica.Codigo},
                Vehiculo: { Codigo: $scope.Modelo.Vehiculo.Codigo },
                Fecha: $scope.Modelo.Fecha,
                TotalPuntos: $scope.MaskNumeroGrid($scope.MaskValoresGrid($scope.Modelo.Puntos)),
                CATA_CFTR: { Codigo: $scope.Modelo.Categoria.Codigo },
                Estado: $scope.Modelo.Estado.Codigo
            }
            PuntosVehiculosFactory.Guardar(PlanPuntosVehiculos).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > CERO) {
                            if ($scope.CodigoVehi == CERO) {
                                ShowSuccess('Se guardó la Fidelizacion correctamente');
                            }
                            else {
                                ShowSuccess('Se modificó la Fidelizacion correctamente');
                            }
                            location.href = $scope.Master + '/' + response.data.Datos;
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        //--Funcion Guardar Datos
        //--Funcion Obtener Datos
        function Obtener() {
            blockUI.start('Cargando Vehiculo');

            $timeout(function () {
                blockUI.message('Cargando Vehiculo');
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Vehiculo: { Codigo: $scope.CodigoVehi },
            };
            blockUI.delay = 1000;

            PuntosVehiculosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        $scope.Modelo.Vehiculo = response.data.Datos.Vehiculo;
                        $scope.Modelo.Tenedor = response.data.Datos.Tenedor;
                        $scope.Modelo.Conductor = response.data.Datos.Conductor;
                        var FechaRetorno = new Date(response.data.Datos.Fecha);
                        FechaRetorno.setHours(0, 0, 0, 0);
                        var FechaNull = new Date(NullDate);
                        $scope.Modelo.Fecha = FechaRetorno.getTime() === FechaNull.getTime() ? '' : new Date(response.data.Datos.Fecha);
                        $scope.Modelo.Categoria = $linq.Enumerable().From($scope.ListadoCategorias).First('$.Codigo == ' + response.data.Datos.CATA_CFTR.Codigo);
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        $scope.Modelo.FechaActualizacion = response.data.Datos.FechaActulizacion == NullDate ? '' : response.data.Datos.FechaActulizacion;
                        $scope.Modelo.Puntos = $scope.MaskValoresGrid(response.data.Datos.TotalPuntos);
                        $scope.Deshabilitar = true;
                        $scope.Titulo = "ACTUALIZAR FIDELIZACIÓN";
                    }
                    else {
                        ShowError('No se logro consultar la Fidelizacion ' + response.data.MensajeOperacion);
                        document.location.href = $scope.Master;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = $scope.Master;
                });
            blockUI.stop();
        }
        //--Funcion Obtener Datos
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo;

            if (Modelo.Vehiculo == undefined || Modelo.Vehiculo == '' || Modelo.Vehiculo == null) {
                $scope.MensajesError.push('Debe ingresar un Vehiculo');
                continuar = false;
            }
            if (Modelo.Categoria == undefined || Modelo.Categoria == '' || Modelo.Categoria == null) {
                $scope.MensajesError.push('Debe ingresar Categoría');
                continuar = false;
            }
            else {
                if (Modelo.Categoria.Codigo == -1) {
                    $scope.MensajesError.push('Debe Seleccionar una Categoría');
                }
            }
            if (Modelo.Fecha == undefined || Modelo.Fecha == '' || Modelo.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar Fecha');
                continuar = false;
            }
            if (Modelo.Puntos == undefined || Modelo.Puntos == '' || Modelo.Puntos == null) {
                $scope.MensajesError.push('Debe ingresar Puntos');
                continuar = false;
            }
            if (Modelo.Estado == undefined || Modelo.Estado == '' || Modelo.Estado == null) {
                $scope.MensajesError.push('Debe ingresar Estado');
                continuar = false;
            }
            return continuar;
        }
        //------------------------------------------------------------PARAMETROS------------------------------------------------------------//
        try {
            if ($routeParams !== undefined && $routeParams.Codigo !== null) {
                if (parseInt($routeParams.Codigo) > CERO) {
                    $scope.CodigoVehi = parseInt($routeParams.Codigo);
                    ValEnviaObtener();
                }
            }
        }
        catch (e) {
            if ($routeParams.Codigo > CERO) {
                $scope.CodigoVehi = $routeParams.Codigo;
                ValEnviaObtener();
            }
        }
        var TimeOutCarga;
        function ValEnviaObtener() {
            if (CATA_CFTRCargado == true) {
                clearTimeout(TimeOutCarga);
                Obtener();
            }
            else {
                clearTimeout(TimeOutCarga);
                TimeOutCarga = setTimeout(ValEnviaObtener, 1000);
            }
        }
    }]);