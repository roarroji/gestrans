﻿SofttoolsApp.controller("ConsultarSolicitudesAnticipoCtrl", ['$scope', '$linq', '$timeout', 'blockUI', 'blockUIConfig','SolicitudesAnticipoFactory','$routeParams','EmpresasFactory',
    function ($scope, $linq, $timeout, blockUI, blockUIConfig, SolicitudesAnticipoFactory, $routeParams,EmpresasFactory) {
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo==' + OPCION_MENU_SOLICITUDES_ANTICIPO);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta página')
            document.location.href = '#'
            console.log(e);
        }

        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.MapaSitio = [{ Nombre: 'Flota Propia' }, { Nombre: 'Documentos' }, { Nombre: 'Solicitudes Anticipo' }];

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: 'BORRADOR' },
            { Codigo: 1, Nombre: 'DEFINITIVO' },
            { Codigo: 2, Nombre: 'ANULADO' },
            {Codigo:-1, Nombre:'(TODOS)'}
        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==-1');
        $scope.NumeroDocumento = '';
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.FechaInicial = new Date();
        $scope.FechaFinal = new Date();
        $scope.ListadoSolicitudes = [];
        $scope.totalRegistros = 0;
        $scope.totalPaginas = 0;
        $scope.pref = '';

        EmpresasFactory.Consultar({ Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.pref = response.data.Datos[0].Prefijo;
            }
        });

        $scope.NuevoDocumento = function () {
            document.location.href = '#!GestionarSolicitudAnticipo/';
        }

        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Obteniendo Documentos...");
            $timeout(function () { blockUI.message("Obteniendo Documentos..."); Find(); }, 100);
        };

        //Fin Paginación


        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0

                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Obteniendo Documentos...");
                $timeout(function () { blockUI.message("Obteniendo Documentos..."); Find(); }, 100);
                
            }
        };

        function Find() {
            
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoSolicitudes = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length == 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        NumeroDocumento: $scope.NumeroDocumento,
                        FechaInicial: $scope.FechaInicial,
                        FechaFinal: $scope.FechaFinal,
                        Estado: $scope.Estado.Codigo,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    }

                    SolicitudesAnticipoFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoSolicitudes = response.data.Datos;
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }

        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroReporte = Numero;
            $scope.NombreReporte = 'repSolicitudAnticipo';

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Nombre + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Nombre + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroReporte != undefined && $scope.NumeroReporte != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroReporte;
            }
        };

        $scope.AnularDocumento = function (Numero, NumeroDocumento) {
            $scope.Numero = Numero

            $scope.ModalErrorCompleto = ''
            $scope.NumeroDocumento = NumeroDocumento
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalAnular');
        };

        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('Debe ingresar la causa de anulación');
            }
            return continuar
        }

        $scope.Anular = function () {
            
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.ModeloCausaAnula,
                Numero: $scope.Numero

            };
            if (DatosRequeridosAnular()) {
                SolicitudesAnticipoFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos > 0) {
                                ShowSuccess('Se anuló la solicitud ' + $scope.NumeroDocumento);
                                closeModal('modalAnular');
                                Find();
                            } else {
                                var mensaje = 'No se pudo anular la solicitud ya que se encuentra relacionado con otros documentos '

                                

                                ShowError(mensaje)
                            }

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = ''
                        showModal('modalMensajeAnularSolicitudAnticipo');
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede anular la solicitud ' + $scope.NumeroDocumento;
                        $scope.ModalErrorCompleto = response.statusText
                    });
            }


        };

        $scope.CerrarModal = function () {
            closeModal('modalMensajeAnularSolicitudAnticipo');            
        }


        if ($routeParams.Numero > 0) {
            $scope.NumeroDocumento = $routeParams.Numero
            $scope.FechaFinal = '';
            $scope.FechaInicial = '';
            Find();
            $scope.NumeroDocumento = '';
            $scope.FechaFinal = new Date();
            $scope.FechaInicial = new Date();
        }



    }
]);