﻿SofttoolsApp.controller("GestionarPlanillaRecoleccionesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'RecoleccionesFactory', 'TarifarioComprasFactory', 'TarifaTransportesFactory', 'TipoTarifaTransportesFactory', 'RutasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'TipoLineaNegocioTransportesFactory', 'TarifaTransportesFactory', 'ProductoTransportadosFactory', 'CiudadesFactory', 'RemesasFactory', 'VehiculosFactory', 'PlanillaGuiasFactory', 'ImpuestosTipoDocumentosFactory', 'ImpuestosFactory', 'OficinasFactory', 'SemirremolquesFactory', 'ManifiestoFactory', 'RemesaGuiasFactory', 'ManifiestoFactory', 'ZonasFactory', 'blockUIConfig',
    function ($scope, $routeParams, $timeout, $linq, blockUI, RecoleccionesFactory, TarifarioComprasFactory, TarifaTransportesFactory, TipoTarifaTransportesFactory, RutasFactory, ValorCatalogosFactory, TercerosFactory, TipoLineaNegocioTransportesFactory, TarifaTransportesFactory, ProductoTransportadosFactory, CiudadesFactory, RemesasFactory, VehiculosFactory, PlanillaGuiasFactory, ImpuestosTipoDocumentosFactory, ImpuestosFactory, OficinasFactory, SemirremolquesFactory, ManifiestoFactory, RemesaGuiasFactory, ManifiestoFactory, ZonasFactory, blockUIConfig) {

        $scope.Titulo = 'GESTIONAR PLANILLAS RECOLECCIONES';
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Planillas Recolecciones' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PLANILLA_RECOLECCIONES); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.AplicaImpuestos = false
        console.clear();
        $scope.ListadoOriginalRecolecciones = [];
        $scope.ListadoOriginalRemesas = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Planilla: {
                Fecha: new Date(),
                FechaSalida: new Date(),
                DetalleTarifarioCompra: {},
            },
            TipoDocumento: 205,
            Remesa: { Cliente: { NombreCompleto: '' }, Ruta: {} }

        }
        $scope.Planilla = {}
        //if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        //}
        $scope.ListaAuxiliares = []
        $scope.Filtro = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CodigoOficina: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Estado: CERO,
            Remesa: {
                DetalleTarifarioVenta: {},
                Remitente: { Direccion: '' },
                Detalle: { Destinatario: { Direccion: '' }, }
            },
            EstadoRemesaPaqueteria: { Codigo: 6001 }

        }
        $scope.paginaActualRecolecciones = 1;
        $scope.cantidadRegistrosPorPagina = 10
        $scope.Filtro2 = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CodigoOficina: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Estado: CERO,
            Remesa: {
                DetalleTarifarioVenta: {},
                Remitente: { Direccion: '' },
                Detalle: { Destinatario: { Direccion: '' }, }
            },
            EstadoRemesaPaqueteria: { Codigo: 6001 }

        }
        $scope.DetallesAuxiliares = []
        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: CERO }
        ]

        CargarOficinaRecoleccion();

        $scope.ListaCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListaCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCiudades)
                }
            }
            return $scope.ListaCiudades
        }
        $scope.ListaCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListaCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCliente)
                }
            }
            return $scope.ListaCliente
        }
        $scope.ListaRemitente = [];
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_REMITENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListaRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRemitente)
                }
            }
            return $scope.ListaRemitente
        }
        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                }
            }
            return $scope.ListadoConductores
        }
        //Funciones Autocomplete
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                }
            }
            return $scope.ListadoVehiculos
        }

        $scope.ListadoRemesasFiltradas = []
        $scope.ListaRecoleccionesConsultados = [];
        $scope.ListaResultadoFiltroConsultaRecolecciones = [];
        $scope.ListadoRecoleccionesTotal = [];
        $scope.ListaRecoleccionesGrid = [];
        $scope.RecoleccionesSeleccionados = false;
        $scope.seleccionarCheckRecolecciones = true;
        /*Se cambia la ciudad recoleccion*/
        $scope.CambiarCiudadRecoleccion = function (ciudad) {
            $scope.ObjetoCiudadRecoleccion = ciudad;
            $scope.CodigoCiudadRecoleccion = ciudad.Codigo;
            //CargarOficinaRecoleccion();
            CambiarZonaRecoleccion();
        }

        //-----------------------------------------------------------------------------------Marcar y demarcar todas las recolecciones-------------------------------------------------------
        $scope.MarcarRecolecciones = function (RecoleccionesSeleccionados) {

            $scope.RecoleccionesSeleccionados = RecoleccionesSeleccionados;
            if ($scope.ListaResultadoFiltroConsultaRecolecciones.length > 0) {

                if ($scope.RecoleccionesSeleccionados == true) {
                    $scope.ListaResultadoFiltroConsultaRecolecciones.forEach(function (itemTotal) {
                        itemTotal.SeleccionadoRecolecciones = true;
                    });
                    $scope.ListaRecoleccionesGrid.forEach(function (item) {
                        item.SeleccionadoRecolecciones = true;
                    });
                } else {
                    $scope.ListaResultadoFiltroConsultaRecolecciones.forEach(function (itemTotal) {
                        itemTotal.SeleccionadoRecolecciones = false;
                    });
                    $scope.ListaRecoleccionesGrid.forEach(function (item) {
                        item.SeleccionadoRecolecciones = false;
                    });
                }
            } else {
                if ($scope.RecoleccionesSeleccionados === true) {
                    $scope.ListadoRecoleccionesTotal.forEach(function (itemTotal) {
                        itemTotal.SeleccionadoRecolecciones = true;
                    });
                    $scope.ListaRecoleccionesGrid.forEach(function (item) {
                        item.SeleccionadoRecolecciones = true;
                    });
                } else {
                    $scope.ListadoRecoleccionesTotal.forEach(function (itemTotal) {
                        itemTotal.SeleccionadoRecolecciones = false;
                    });
                    $scope.ListaRecoleccionesGrid.forEach(function (item) {
                        item.SeleccionadoRecolecciones = false;
                    });
                }
            }
            $scope.Calcular()
        }
        function CambiarZonaRecoleccion() {
            ZonasFactory.Consultar({ Ciudad: $scope.ObjetoCiudadRecoleccion, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoZonasRecoleccion = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoZonasRecoleccion.push({ Nombre: '(TODAS)', Codigo: 0 })
                            response.data.Datos.forEach(function (item) {
                                if (item.Codigo !== 0) {
                                    $scope.ListadoZonasRecoleccion.push(item);
                                }
                            });
                            $scope.ModeloZonasRecoleccion = $scope.ListadoZonasRecoleccion[0];
                        } else {
                            $scope.ListadoZonasRecoleccion = []
                        }
                    }
                }), function (response) {
                };
        };
        function CargarOficinaRecoleccion() {
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Pagina: 1, Codigo: -1, RegistrosPagina: -1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoOficinasRecoleccion = []
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoOficinasRecoleccion.push({ Nombre: '(TODAS)', Codigo: 0 })
                            response.data.Datos.forEach(function (item) {
                                if (item.Codigo !== 0) {
                                    $scope.ListadoOficinasRecoleccion.push(item);
                                }
                            });
                            //$scope.ModeloOficinasRecoleccion = $scope.ListadoOficinasRecoleccion[0];
                            //$scope.Modelo.OficinaActual = $scope.ListadoOficinasRecoleccion[0];
                            $scope.ModeloOficinasRecoleccion = $linq.Enumerable().From($scope.ListadoOficinasRecoleccion).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                            $scope.Modelo.OficinaOrigen = $linq.Enumerable().From($scope.ListadoOficinasRecoleccion).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                        } else {
                            $scope.ListadoOficinasRecoleccion = []
                        }
                    }
                }, function (response) {
                });
        }
        $scope.CargarRecolecciones = function () {
            FindRecolecciones();
        };
        function DatosRequeridosRecolecciones() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.NumeroDocumento === null || $scope.NumeroDocumento === undefined || $scope.NumeroDocumento === '' || $scope.NumeroDocumento === 0 || isNaN($scope.NumeroDocumento) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.NumeroDocumento !== null && $scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '' && $scope.NumeroDocumento !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            return continuar
        }
        function FindRecolecciones() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListaResultadoFiltroConsultaRecolecciones = [];

            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: 0,
                NumeroDocumento: $scope.NumeroDocumento,
                FechaInicio: $scope.FechaInicio,
                FechaFin: $scope.FechaFin,
                Ciudad: $scope.ModeloCiudadRecoleccion,
                Zonas: $scope.ModeloZonasRecoleccion,
                Oficinas: $scope.ModeloOficinasRecoleccion,
                NombreCliente: $scope.ModeloCliente.NombreCompleto,
                Estado: 1,
            }

            if (DatosRequeridosRecolecciones()) {
                blockUI.delay = 1000;
                RecoleccionesFactory.Consultar(Filtro).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ListaRecoleccionesGrid = [];
                            $scope.ListaRecoleccionesConsultados = [];
                            $scope.ListadoRecoleccionesTotal = [];
                            if ($scope.ListaRecoleccionesConsultados.length > 0) {
                                $scope.ListadoRecoleccionesTotal = [];
                                $scope.ListaRecoleccionesConsultados.forEach(function (itemRecolecciones) {
                                    $scope.ListadoRecoleccionesTotal.push(itemRecolecciones);
                                })

                                for (var i = 0; i < response.data.Datos.length; i++) {
                                    var existe = true;
                                    for (var j = $scope.RegistroRepetidoRecolecciones; j < $scope.ListadoRecoleccionesTotal.length; j++) {

                                        if (response.data.Datos[i].Numero == $scope.ListadoRecoleccionesTotal[j].Numero) {
                                            existe = false;
                                            $scope.RegistroRepetidoRecolecciones += 1;
                                            break;
                                        }
                                    }
                                    if (existe == true) {
                                        $scope.ListadoRecoleccionesTotal.push(response.data.Datos[i]);
                                    }
                                }
                            } else {
                                for (var i = 0; i < response.data.Datos.length; i++) {
                                    var existe = true;
                                    for (var j = $scope.RegistroRepetidoRecolecciones; j < $scope.ListadoRecoleccionesTotal.length; j++) {

                                        if (response.data.Datos[i].Numero == $scope.ListadoRecoleccionesTotal[j].Numero) {
                                            existe = false;
                                            $scope.RegistroRepetidoRecolecciones += 1;
                                            break;
                                        }
                                    }
                                    if (existe == true) {
                                        $scope.ListadoRecoleccionesTotal.push(response.data.Datos[i]);
                                    }
                                }
                            }
                            $scope.RegistroRepetidoRecolecciones = 0

                            if ($scope.ListadoRecoleccionesTotal.length > 0) {
                                $scope.ListadoRecoleccionesTotal = $linq.Enumerable().From($scope.ListadoRecoleccionesTotal).OrderBy(function (x) {
                                    return x.Numero
                                }).ToArray();
                            }
                            var i = 0;
                            for (i = 0; i <= $scope.ListadoRecoleccionesTotal.length - 1; i++) {
                                if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                    if ($scope.ListadoRecoleccionesTotal[i].Cliente.Codigo === 0) {
                                        $scope.ListadoRecoleccionesTotal[i].Cliente.Nombre = ''
                                    }
                                    $scope.ListaRecoleccionesGrid.push($scope.ListadoRecoleccionesTotal[i])
                                }
                            }

                            if ($scope.ListadoRecoleccionesTotal.length > 0) {
                                $scope.totalRegistros = $scope.ListadoRecoleccionesTotal.length;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                                $('#btncargarrecolecciones').click()
                                $('#btnfiltrosrecolecciones').click()
                            } else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActualRecolecciones = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                            $scope.ListadoOriginalRecolecciones = $scope.ListadoRecoleccionesTotal;
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                blockUI.stop();
            }
            blockUI.stop();
        };
        $scope.FiltrarRecolecciones = function () {
            $scope.totalRegistros = 0;
            $scope.ListadoRecoleccionesTotal = [];
            $scope.totalRegistros = 0;            
            $scope.ListaResultadoFiltroConsultaRecolecciones = [];
            $scope.ListaRecoleccionesGrid = [];

            if (($scope.FiltroNumeroInicio !== undefined && $scope.FiltroNumeroInicio > 0) && ($scope.FiltroNumeroFin == undefined || $scope.FiltroNumeroFin == null || $scope.FiltroNumeroFin == '' || isNaN($scope.FiltroNumeroFin))) {
                $scope.FiltroNumeroFin = $scope.FiltroNumeroInicio
            }

            for (var i = 0; i < $scope.ListadoOriginalRecolecciones.length; i++) {
                var agregarRecoleccion = true;
                var item = $scope.ListadoOriginalRecolecciones[i];
                if ($scope.FiltroNumeroInicio !== undefined && $scope.FiltroNumeroInicio > 0) {
                    if (item.NumeroDocumento < $scope.FiltroNumeroInicio) {
                        agregarRecoleccion = false;
                    }
                }
                if ($scope.FiltroNumeroFin !== undefined && $scope.FiltroNumeroFin > 0) {
                    if (item.NumeroDocumento > $scope.FiltroNumeroFin) {
                        agregarRecoleccion = false;
                    }
                }
                if ($scope.FiltroFechaInicio !== undefined && $scope.FiltroFechaInicio > 0) {
                    if (new Date(item.Fecha) < $scope.FiltroFechaInicio) {
                        agregarRecoleccion = false;
                    }
                }
                if ($scope.FiltroFechaFin !== undefined && $scope.FiltroFechaFin > 0) {
                    if (new Date(item.Fecha) > $scope.FiltroFechaFin) {
                        agregarRecoleccion = false;
                    }
                }

                if (agregarRecoleccion) {
                    $scope.ListadoRecoleccionesTotal.push(item);
                    $scope.ListaResultadoFiltroConsultaRecolecciones.push(item);
                    $scope.totalRegistros = $scope.totalRegistros + 1
                }
            }
            $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
            $scope.paginaActualRecolecciones = 2;
            $scope.PrimerPaginaRecolecciones();
           // if (DatosRequeridosFiltrarRecolecciones()) {

               /* if ($scope.ModeloCantidadTotal > 0) {
                    $scope.ValorAuxiliar = $scope.ModeloCantidadTotal;
                } else {
                    $scope.ValorAuxiliar = 0;
                }

                if ($scope.ListadoRecoleccionesTotal.length > 0) {
                    $scope.ListaRecoleccionesGrid = [];

                    if ($scope.FiltroNumeroInicio > 0 && $scope.FiltroNumeroFin == 0) {
                        $scope.FiltroNumeroFin = $scope.FiltroNumeroInicio;
                    }
                    if ($scope.FiltroNumeroInicio == 0 && $scope.FiltroNumeroFin > 0) {
                        $scope.FiltroNumeroInicio = $scope.FiltroNumeroFin;
                    }

                    var FiltroConsultaRecolecciones = '$.CodigoEmpresa == ' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;

                    if ($scope.FiltroNumeroInicio > 0 && $scope.FiltroNumeroFin > 0) {
                        FiltroConsultaRecolecciones += '&& $.NumeroDocumento >=' + $scope.FiltroNumeroInicio + ' && $.NumeroDocumento <=' + $scope.FiltroNumeroFin;
                    }
                    if (($scope.FiltroFechaInicio !== null && $scope.FiltroFechaInicio !== '' && $scope.FiltroFechaInicio !== undefined) && ($scope.FiltroFechaFin !== null || $scope.FiltroFechaFin !== undefined || $scope.FiltroFechaFin !== '')) {

                        var FechaInicialAuxiliar = Formatear_Fecha($scope.FiltroFechaInicio.toDateString(), FORMATO_FECHA_s);
                        var FechaFinalAuxiliar = Formatear_Fecha($scope.FiltroFechaFin.toDateString(), FORMATO_FECHA_s);

                        FiltroConsultaRecolecciones += ' && $.Fecha >="' + FechaInicialAuxiliar + '" && $.Fecha <= "' + FechaFinalAuxiliar + '"';
                    }

                    $scope.ListaResultadoFiltroConsultaRecolecciones = [];
                    $scope.ListaResultadoFiltroConsultaRecolecciones = $linq.Enumerable().From($scope.ListadoRecoleccionesTotal).Where(FiltroConsultaRecolecciones).ToArray();

                    if ($scope.ListaResultadoFiltroConsultaRecolecciones.length > 0) {
                        $scope.ListaRecoleccionesGrid = [];
                        $scope.paginaActual = 1;
                        var i = 0;
                        for (i = 0; i <= $scope.ListaResultadoFiltroConsultaRecolecciones.length - 1; i++) {
                            if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                $scope.ListaRecoleccionesGrid.push($scope.ListaResultadoFiltroConsultaRecolecciones[i])
                            }
                        }
                        if ($scope.ListaResultadoFiltroConsultaRecolecciones.length > 0) {
                            $scope.totalRegistros = $scope.ListaResultadoFiltroConsultaRecolecciones.length;
                            $scope.totalPaginas = Math.ceil($scope.ListaResultadoFiltroConsultaRecolecciones.length / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        } else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    } else {
                        if (($scope.FiltroFechaInicio == null || $scope.FiltroFechaInicio == '' || $scope.FiltroFechaInicio == undefined) || ($scope.FiltroFechaFin == null || $scope.FiltroFechaFin == undefined || $scope.FiltroFechaFin == '')) {
                            if ($scope.FiltroNumeroInicio == 0 && $scope.FiltroNumeroFin == 0) {
                                if ($scope.ListadoRecoleccionesTotal.length > 0) {
                                    var i = 0;
                                    for (i = 0; i <= $scope.ListadoRecoleccionesTotal.length - 1; i++) {
                                        if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                            $scope.ListaRecoleccionesGrid.push($scope.ListadoRecoleccionesTotal[i])
                                        }
                                    }
                                    if ($scope.ListadoRecoleccionesTotal.length > 0) {
                                        $scope.totalRegistros = $scope.ListadoRecoleccionesTotal.length;
                                        $scope.totalPaginas = Math.ceil($scope.ListadoRecoleccionesTotal.length / $scope.cantidadRegistrosPorPagina);
                                        $scope.Buscando = false;
                                        $scope.ResultadoSinRegistros = '';
                                    } else {
                                        $scope.totalRegistros = 0;
                                        $scope.totalPaginas = 0;
                                        $scope.paginaActual = 1;
                                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                        $scope.Buscando = false;
                                    }
                                }
                            }
                        }
                    }
                }*/
            //}
        }

        $scope.LimpiarFiltroRemesas = function () {
            $scope.totalRegistros = 0;
            var Filtro = $scope.Filtro2
            Filtro.NumeroInicial = '';
            Filtro.NumeroFinal = '';
            Filtro.FechaInicial = '';
            Filtro.FechaFinal = '';
            $scope.ListadoRemesas = [];
            $scope.ListadoRemesasFiltradas = [];
            $scope.ListadoGuias = [];
        }

        $scope.LimpiarFiltroRecolecciones = function () {
            $scope.FiltroNumeroInicio = 0;
            $scope.FiltroNumeroFin = 0;
            $scope.FiltroFechaInicio = '';
            $scope.FiltroFechaFin = '';
            $scope.ListaResultadoFiltroConsultaRecolecciones = [];
            $scope.ListaRecoleccionesGrid = [];
            $scope.ListadoRecoleccionesTotal = [];
            if ($scope.ListadoRecoleccionesTotal.length > 0) {
                $scope.ListaRecoleccionesGrid = [];
                var i = 0;
                for (i = 0; i <= $scope.ListadoRecoleccionesTotal.length - 1; i++) {
                    if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                        $scope.ListaRecoleccionesGrid.push($scope.ListadoRecoleccionesTotal[i])
                    }
                }
                if ($scope.ListadoRecoleccionesTotal.length > 0) {
                    $scope.totalRegistros = $scope.ListadoRecoleccionesTotal.length;
                    $scope.totalPaginas = Math.ceil($scope.ListadoRecoleccionesTotal.length / $scope.cantidadRegistrosPorPagina);
                    $scope.Buscando = false;
                    $scope.ResultadoSinRegistros = '';
                } else {
                    $scope.totalRegistros = 0;
                    $scope.totalPaginas = 0;
                    $scope.paginaActual = 1;
                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                    $scope.Buscando = false;
                }
            }
        }
        $scope.PrimerPaginaRecolecciones = function () {
            if ($scope.paginaActualRecolecciones > 1) {
                if ($scope.ListaResultadoFiltroConsultaRecolecciones.length > 0) {
                    $scope.paginaActualRecolecciones = 1;
                    $scope.ListaRecoleccionesGrid = [];
                    var i = 0;
                    for (i = 0; i <= $scope.ListaResultadoFiltroConsultaRecolecciones.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina) {
                            $scope.ListaRecoleccionesGrid.push($scope.ListaResultadoFiltroConsultaRecolecciones[i])
                        }
                    }
                } else {
                    if ($scope.ListadoRecoleccionesTotal.length > 0) {
                        $scope.paginaActualRecolecciones = 1;
                        $scope.ListaRecoleccionesGrid = [];
                        var i = 0;
                        for (i = 0; i <= $scope.ListadoRecoleccionesTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina) {
                                $scope.ListaRecoleccionesGrid.push($scope.ListadoRecoleccionesTotal[i])
                            }
                        }
                    }
                }
            }
        };
        $scope.SiguienteRecolecciones = function () {
            if ($scope.paginaActualRecolecciones < $scope.totalPaginas) {
                $scope.ListaRecoleccionesGrid = [];
                $scope.PaginaAuxiliar = $scope.paginaActualRecolecciones;
                $scope.paginaActualRecolecciones += 1;
                if ($scope.ListaResultadoFiltroConsultaRecolecciones.length > 0) {
                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListaResultadoFiltroConsultaRecolecciones.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActualRecolecciones) {
                            $scope.ListaRecoleccionesGrid.push($scope.ListaResultadoFiltroConsultaRecolecciones[i]);
                        }
                    }
                } else {
                    if ($scope.ListadoRecoleccionesTotal.length > 0) {
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoRecoleccionesTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActualRecolecciones) {
                                $scope.ListaRecoleccionesGrid.push($scope.ListadoRecoleccionesTotal[i]);
                            }
                        }
                    }
                }
            }
        }
        $scope.AnteriorRecolecciones = function () {
            if ($scope.paginaActualRecolecciones > 1) {
                if ($scope.ListaResultadoFiltroConsultaRecolecciones.length > 0) {
                    $scope.ListaRecoleccionesGrid = [];
                    $scope.paginaActualRecolecciones -= 1;
                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActualRecolecciones) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListaResultadoFiltroConsultaRecolecciones.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActualRecolecciones) {
                            $scope.ListaRecoleccionesGrid.push($scope.ListaResultadoFiltroConsultaRecolecciones[i]);
                        }
                    }
                } else {
                    if ($scope.ListadoRecoleccionesTotal.length > 0) {
                        $scope.ListaRecoleccionesGrid = [];
                        $scope.paginaActualRecolecciones -= 1;
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActualRecolecciones) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListadoRecoleccionesTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActualRecolecciones) {
                                $scope.ListaRecoleccionesGrid.push($scope.ListadoRecoleccionesTotal[i]);
                            }
                        }
                    }
                }
            }
        };
        $scope.UltimaPaginaRecolecciones = function () {
            if ($scope.paginaActualRecolecciones < $scope.totalPaginas) {
                if ($scope.ListaResultadoFiltroConsultaRecolecciones.length > 0) {
                    if ($scope.ListadoRecoleccionesTotal.length > 0) {
                        $scope.paginaActualRecolecciones = $scope.totalPaginas;
                        $scope.ListaRecoleccionesGrid = [];
                        var i = 0;
                        for (i = ($scope.paginaActualRecolecciones * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListaResultadoFiltroConsultaRecolecciones.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActualRecolecciones) {
                                $scope.ListaRecoleccionesGrid.push($scope.ListaResultadoFiltroConsultaRecolecciones[i])
                            }
                        }
                    }
                } else {
                    if ($scope.ListadoRecoleccionesTotal.length > 0) {
                        $scope.paginaActualRecolecciones = $scope.totalPaginas;
                        $scope.ListaRecoleccionesGrid = [];
                        var i = 0;
                        for (i = ($scope.paginaActualRecolecciones * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListadoRecoleccionesTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActualRecolecciones) {
                                $scope.ListaRecoleccionesGrid.push($scope.ListadoRecoleccionesTotal[i])
                            }
                        }
                    }
                }
            }
        }



        /*Cargar el combo de cliente*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, CadenaPerfiles: PERFIL_CLIENTE }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ NombreCompleto: '', Codigo: 0 })
                        $scope.ListaClientes = response.data.Datos;
                        $scope.ModeloCliente = $linq.Enumerable().From($scope.ListaClientes).First('$.Codigo ==0');
                        $scope.ModeloClienteGuias = $linq.Enumerable().From($scope.ListaClientes).First('$.Codigo ==0');
                    } else {
                        $scope.ListaClientes = [];
                        $scope.ModeloCliente = null;
                        $scope.ModeloClienteGuias = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.CargarDatosFunciones = function () {
            try {
                if ($scope.Modelo.Planilla.Estado.Codigo >= CERO) {
                    $scope.Modelo.Planilla.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Planilla.Estado.Codigo);
                }
            } catch (e) {
                $scope.Modelo.Planilla.Estado = $scope.ListadoEstados[CERO]
            }


            /*Cargar oficina de la ciudad*/
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoOficina = response.data.Datos;
                        $scope.ModeloOficina = $scope.ListadoOficina[$scope.ListadoOficina.length - 1];
                        try {
                            $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficina).First('$.Codigo == ' + $scope.Modelo.Planilla.Oficina.Codigo);
                        } catch (e) {
                            try {
                                $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficina).First('$.Codigo == ' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                            } catch (e) {
                                $scope.ModeloOficina = $scope.ListadoOficina[$scope.ListadoOficina.length - 1];
                            }
                        }

                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            ///*Cargar el combo de ESTADO DE RESEMAS PAQUETERIA*/

            //VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
            //    then(function (response) {
            //        if (response.data.ProcesoExitoso === true) {
            //            $scope.ListadoVehiculos = []
            //            if (response.data.Datos.length > CERO) {
            //                $scope.ListadoVehiculos = response.data.Datos;
            //                try {
            //                    if ($scope.Modelo.Planilla.Vehiculo.Codigo > CERO) {
            //                        $scope.Modelo.Planilla.Vehiculo = $linq.Enumerable().From($scope.ListadoVehiculos).First('$.Codigo ==' + $scope.Modelo.Planilla.Vehiculo.Codigo);
            //                        $scope.ObtenerInformacionTenedor()
            //                    }
            //                } catch (e) {
            //                    $scope.Modelo.Planilla.Vehiculo = ''
            //                }
            //            }
            //            else {
            //                $scope.ListadoVehiculos = [];
            //            }
            //        }
            //    }, function (response) {
            //        ShowError(response.statusText);
            //    });
            // Semirremolques
            SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaRemolque = response.data.Datos;
                            try {
                                if ($scope.Modelo.Planilla.Semirremolque.Codigo > CERO) {
                                    $scope.Modelo.Planilla.Semirremolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Codigo ==' + $scope.Modelo.Planilla.Semirremolque.Codigo);
                                }
                            } catch (e) {
                                $scope.Modelo.Planilla.Semirremolque = ''
                            }
                        }
                        else {
                            $scope.ListaRemolque = [];
                            $scope.ModeloRemolque = null;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            /*Cargar el combo de funcionarios*/
            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, CadenaPerfiles: PERFIL_EMPLEADO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.push({ NombreCompleto: '', Codigo: 0 })
                            $scope.ListaFuncionario = response.data.Datos;
                            if ($scope.CodigoFuncionario !== undefined && $scope.CodigoFuncionario !== '' && $scope.CodigoFuncionario !== 0 && $scope.CodigoFuncionario !== null) {
                                if ($scope.CodigoFuncionario > 0) {
                                    $scope.ModeloFuncionario = $linq.Enumerable().From($scope.ListaFuncionario).First('$.Codigo ==' + $scope.CodigoFuncionario);
                                } else {
                                    $scope.ModeloFuncionario = $linq.Enumerable().From($scope.ListaFuncionario).First('$.Codigo == 0');
                                }
                            } else {
                                $scope.ModeloFuncionario = $linq.Enumerable().From($scope.ListaFuncionario).First('$.Codigo ==0');
                            }
                        }
                        else {
                            $scope.ListaFuncionario = [];
                            $scope.ModeloFuncionario = null;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoRutas = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoRutas = response.data.Datos
                        }
                        else {
                            $scope.ListadoRutas = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de tipo entrega*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoEntregaRemesaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoEntregaRemesaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoEntregaRemesaPaqueteria = $scope.ListadoTipoEntregaRemesaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoEntregaRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo transporte*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_TRANSPORTE_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTransporteRemsaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoTransporteRemsaPaqueteria = $scope.ListadoTipoTransporteRemsaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo despacho*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DESPACHO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoDespachoRemesaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoDespachoRemesaPaqueteria = $scope.ListadoTipoDespachoRemesaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de tipo servicio*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SERVICIO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoServicioRemesaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoServicioRemesaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoServicioRemesaPaqueteria = $scope.ListadoTipoServicioRemesaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoServicioRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });

            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoProductoTransportados = []
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoProductoTransportados = response.data.Datos;
                        }
                        else {
                            $scope.ListadoProductoTransportados = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/



            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCliente = []
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoCliente = response.data.Datos;
                        }
                        else {
                            $scope.ListadoCliente = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_EMPLEADO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoEmpleados = []
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoEmpleados = response.data.Datos;
                            $scope.DetallesAuxiliares = []
                            if ($scope.Modelo.Planilla.DetallesAuxiliares !== undefined && $scope.Modelo.Planilla.DetallesAuxiliares !== null) {
                                $scope.DetallesAuxiliares = $scope.Modelo.Planilla.DetallesAuxiliares
                                for (var i = 0; i < $scope.DetallesAuxiliares.length; i++) {
                                    $scope.DetallesAuxiliares[i].Funcionario = $linq.Enumerable().From($scope.ListadoEmpleados).First('$.Codigo == ' + $scope.DetallesAuxiliares[i].Funcionario.Codigo);
                                }
                            }
                        }
                        else {
                            $scope.ListadoEmpleados = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCiudades = response.data.Datos;
                            if ($scope.CodigoCiudadRecoleccion != undefined && $scope.CodigoCiudadRecoleccion != '' && $scope.CodigoCiudadRecoleccion != 0 && $scope.CodigoCiudadRecoleccion != null) {
                                $scope.ModeloCiudadRecoleccion = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + $scope.CodigoCiudadRecoleccion);
                            } else {
                               // $scope.ModeloCiudadRecoleccion = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==0');
                                
                            }
                        }
                        else {
                            $scope.ListadoCiudades = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            /*Cargar el combo de TipoLineaNegocioTransportes*/
            TipoLineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoLineaNegocioTransportes = [];
                        $scope.ListadoTipoLineaNegocioTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoLineaNegocioTransportesInicial = response.data.Datos;
                            for (var i = 0; i < $scope.ListadoTipoLineaNegocioTransportesInicial.length; i++) {
                                if ($scope.ListadoTipoLineaNegocioTransportesInicial[i].LineaNegocioTransporte !== undefined && $scope.ListadoTipoLineaNegocioTransportesInicial[i].LineaNegocioTransporte !== null) {
                                    if ($scope.ListadoTipoLineaNegocioTransportesInicial[i].LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA) {
                                        $scope.ListadoTipoLineaNegocioTransportes.push($scope.ListadoTipoLineaNegocioTransportesInicial[i])
                                    }
                                }
                            }
                            $scope.ListadoTipoLineaNegocioTransportesInicial = angular.copy($scope.ListadoTipoLineaNegocioTransportes)
                            $scope.ListadoTipoLineaNegocioTransportes = []
                        }
                        else {
                            $scope.ListadoTipoLineaNegocioTransportes = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de TarifaTransportes*/
            TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTarifaTransportes = [];
                        $scope.ListadoTarifaTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTarifaTransportesInicial = response.data.Datos;
                        }
                        else {
                            $scope.ListadoTarifaTransportesInicial = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de TipoTarifaTransportes*/
            TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTarifaTransportes = [];
                        $scope.ListadoTipoTarifaTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoTarifaTransportesInicial = response.data.Datos;
                        }
                        else {
                            $scope.ListadoTipoTarifaTransportesInicial = []
                        }
                    }
                }, function (response) {
                });

        }
        $scope.CargarImpuestos = function (Ruta) {
            if (Ruta.Codigo > 0 && Ruta !== undefined && Ruta !== null && Ruta !== "") {
                if (Ruta.Codigo > 0) {
                    /*Cargar los impuestos*/
                    ImpuestosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, CodigoTipoDocumento: 205, CodigoCiudad: Ruta.CiudadOrigen.Codigo, AplicaTipoDocumento: 1 }).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.ListadoImpuestos = []
                                $scope.ListadoImpuestosFiltrado = []
                                if (response.data.Datos.length > CERO) {
                                    $scope.ListadoImpuestos = response.data.Datos;
                                    try {
                                        if ($scope.Modelo.Planilla.DetalleImpuesto !== undefined && $scope.Modelo.Planilla.DetalleImpuesto !== null && $scope.Modelo.Planilla.DetalleImpuesto !== []) {
                                            $scope.ListadoImpuestosFiltrado = $scope.Modelo.Planilla.DetalleImpuesto
                                        }
                                        else {
                                            $scope.ListadoImpuestosFiltrado = []
                                            if ($scope.ListadoImpuestos.length > 0) {
                                                for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                                                    var impuesto = {
                                                        Nombre: $scope.ListadoImpuestos[i].Nombre,
                                                        CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                                                        ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                                                        ValorBase: $scope.ListadoImpuestos[i].valor_base,
                                                        ValorImpuesto: 0
                                                    }
                                                    $scope.ListadoImpuestosFiltrado.push(impuesto)
                                                }
                                            }
                                        }
                                    } catch (e) {
                                        $scope.ListadoImpuestosFiltrado = []
                                        if ($scope.ListadoImpuestos.length > 0) {
                                            for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                                                var impuesto = {
                                                    Nombre: $scope.ListadoImpuestos[i].Nombre,
                                                    CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                                                    ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                                                    ValorBase: $scope.ListadoImpuestos[i].valor_base,
                                                    ValorImpuesto: 0
                                                }
                                                $scope.ListadoImpuestosFiltrado.push(impuesto)
                                                //$scope.Filtrar();
                                            }
                                        }
                                    }


                                }
                                else {
                                    $scope.ListadoImpuestos = [];
                                    $scope.ListadoImpuestosFiltrado = [];
                                }
                            }
                            try {
                                $scope.Calcular()

                            } catch (e) {

                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
        }

        $scope.CargarValorAnticipo = function () {
            $scope.Modelo.Planilla.ValorAnticipo = (parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador));
        }

        $scope.FiltrarTarifas = function (Ruta) {
            if (Ruta.Codigo > 0 && Ruta !== undefined && Ruta !== null && Ruta !== "") {
                if (Ruta.Codigo > 0) {




                    $scope.ListaTarifas = [];
                    $scope.ListaTarifas = [];
                    $scope.ListadoTipoTarifas = [];
                    for (var i = 0; i < $scope.ListadoTarifas.length; i++) {
                        var item = $scope.ListadoTarifas[i]
                        if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO || item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                            if (Ruta.Codigo == item.Ruta.Codigo) {
                                //if (item.TipoLineaNegocioTransportes.Codigo == TIPO_LINEA_NEGOCIO_CARGA_MASIVO_NACIONAL || item.TipoLineaNegocioTransportes.Codigo == TIPO_LINEA_NEGOCIO_CARGA_SEMIMASIVO_NACIONAL) {
                                if ($scope.ListaTarifas.length == 0) {
                                    $scope.ListaTarifas.push(item.TipoTarifaTransportes.TarifaTransporte)
                                }
                                else {
                                    var cont = 0
                                    for (var j = 0; j < $scope.ListaTarifas.length; j++) {
                                        if ($scope.ListaTarifas[j].Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                            cont++
                                        }
                                    }
                                    if (cont == 0) {
                                        $scope.ListaTarifas.push(item.TipoTarifaTransportes.TarifaTransporte)
                                    }
                                }
                                item.Codigo = item.TipoTarifaTransportes.Codigo
                                $scope.ListadoTipoTarifas.push(item)
                                //}
                            }
                        }
                    }
                    //$scope.Modelo.Planilla.TarifarioCompra.Tarifas.forEach(function (item) {

                    //});
                    if ($scope.ListaTarifas.length == 0 && $scope.ListadoTipoTarifas == 0) {
                        ShowError('La ruta ingresada no se encuentra asociada al tarifario de compras del tenedor ' + $scope.Modelo.Planilla.Vehiculo.Tenedor.NombreCompleto);
                    } else {
                        try {
                            $scope.Modelo.Planilla.TarifaTransportes = $linq.Enumerable().From($scope.ListaTarifas).First('$.Codigo ==' + $scope.Modelo.Planilla.TarifaTransportes.Codigo);
                            $scope.FiltrarTipoTarifa()
                        } catch (e) {
                            try {
                                $scope.Modelo.Planilla.TarifaTransportes = $scope.ListaTarifas[0]
                                $scope.FiltrarTipoTarifa()
                            } catch (e) {

                            }

                        }
                    }
                }
            }
        }
        $scope.FiltrarTipoTarifa = function () {
            $scope.ListadoTipoTarifa = []
            for (var i = 0; i < $scope.ListadoTipoTarifas.length; i++) {
                var item = $scope.ListadoTipoTarifas[i]
                if ($scope.Modelo.Planilla.TarifaTransportes.Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                    $scope.ListadoTipoTarifa.push(item)
                }
            }
            try {
                $scope.TipoTarifaTransportes = $linq.Enumerable().From($scope.ListadoTipoTarifa).First('$.Codigo ==' + $scope.Modelo.Planilla.TipoTarifaTransportes.Codigo);
                $scope.Modelo.Planilla.TipoTarifaTransportes = undefined
            } catch (e) {
                $scope.TipoTarifaTransportes = $scope.ListadoTipoTarifa[0]
            }
            try {
                $scope.Calcular()
            } catch (e) {

            }
        }
        /*Consultar Tarifario Tenedor*/
        $scope.ObtenerInformacionTenedor = function () {
            try {
                $scope.Modelo.Planilla.Semirremolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Codigo ==' + $scope.Modelo.Planilla.Vehiculo.Semirremolque.Codigo);

            } catch (e) {

            }
            if ($scope.Modelo.Planilla.Vehiculo.Tenedor !== undefined) {
                if ($scope.Modelo.Planilla.Vehiculo.Tenedor.Codigo > 0) {
                    $scope.Modelo.Planilla.Vehiculo.Tenedor.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                    TercerosFactory.Obtener($scope.Modelo.Planilla.Vehiculo.Tenedor).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.Codigo > 0) {
                                    if (response.data.Datos.Proveedor != undefined) {
                                        if (response.data.Datos.Proveedor.Tarifario !== undefined) {
                                            if (response.data.Datos.Proveedor.Tarifario.Codigo > 0) {
                                                $scope.Planilla.TarifarioCompra = response.data.Datos.Proveedor.Tarifario
                                                $scope.ObtenerInformacionTarifario()
                                                $scope.Modelo.Planilla.Vehiculo.Tenedor = { NombreCompleto: response.data.Datos.NombreCompleto, Codigo: response.data.Datos.Codigo }
                                            }
                                            else {
                                                ShowError('El tenedor del vehículo seleccionado no posee un tarifario de compra asignado');
                                                $scope.Planilla.TarifarioCompra = {}
                                            }
                                        }
                                        else {
                                            ShowError('Error al consultar la información del tarifario');
                                            $scope.Planilla.TarifarioCompra = {}
                                        }
                                    }
                                    else {
                                        ShowError('El tenedor del vehículo seleccionado no posee un tarifario de compra asignado');
                                        $scope.Planilla.TarifarioCompra = {}
                                    }
                                } else {
                                    ShowError('El tenedor del vehículo seleccionado no posee un tarifario de compra asignado');
                                    $scope.Planilla.TarifarioCompra = {}
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
        }
        $scope.ObtenerInformacionTarifario = function () {
            $scope.TarifarioValido = false
            $scope.ListadoTarifaTransportes = []
            $scope.ListadoTipoTarifaTransportes = []
            $scope.Planilla.DetalleTarifarioCompra = { TipoLineaNegocioTransportes: '' }
            if ($scope.Planilla.TarifarioCompra !== undefined) {
                if ($scope.Planilla.TarifarioCompra.Codigo > 0) {
                    $scope.Planilla.TarifarioCompra.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                    TarifarioComprasFactory.Obtener($scope.Planilla.TarifarioCompra).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Planilla.TarifarioCompra = response.data.Datos
                                $scope.ListadoTarifas = []
                                $scope.LustadoRutasFiltrado = []
                                for (var i = 0; i < $scope.Planilla.TarifarioCompra.Tarifas.length; i++) {
                                    if ($scope.Planilla.TarifarioCompra.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == 1 || $scope.Planilla.TarifarioCompra.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == 2) {
                                        $scope.ListadoTarifas.push($scope.Planilla.TarifarioCompra.Tarifas[i])
                                    }
                                }
                                for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                                    var aplica = 0
                                    for (var j = 0; j < $scope.ListadoTarifas.length; j++) {
                                        if ($scope.ListadoRutas[i].Codigo == $scope.ListadoTarifas[j].Ruta.Codigo) {
                                            aplica++
                                        }
                                    }
                                    if (aplica > 0) {
                                        $scope.LustadoRutasFiltrado.push($scope.ListadoRutas[i])
                                    }
                                }
                                try {
                                    if ($scope.Modelo.Planilla.Ruta.Codigo > CERO) {
                                        $scope.Modelo.Planilla.Ruta = $linq.Enumerable().From($scope.LustadoRutasFiltrado).First('$.Codigo ==' + $scope.Modelo.Planilla.Ruta.Codigo);
                                        $scope.CargarImpuestos($scope.Modelo.Planilla.Ruta)
                                        $scope.FiltrarTarifas($scope.Modelo.Planilla.Ruta)
                                    }
                                } catch (e) {
                                    $scope.Modelo.Planilla.Ruta = ''
                                }
                            }
                            else {
                                ShowError('No se logro consultar el tarifario compras');
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });

                    blockUI.stop();
                }
            }
        }


        //Calcular valores
        $scope.Calcular = function () {
            $scope.Modelo.Planilla.Cantidad = 0
            $scope.Modelo.Planilla.Peso = 0
            $scope.Modelo.Planilla.ValorRetencionFuente = 0
            $scope.Modelo.Planilla.ValorRetencionICA = 0
            $scope.Modelo.Planilla.ValorFleteCliente = 0
            $scope.Modelo.Planilla.ValorSeguroMercancia = 0
            $scope.Modelo.Planilla.ValorOtrosCobros = 0
            $scope.Modelo.Planilla.ValorTotalCredito = 0
            $scope.Modelo.Planilla.ValorTotalContado = 0
            $scope.Modelo.Planilla.ValorTotalAlcobro = 0
            $scope.Modelo.Planilla.ValorFleteTransportador = 0
            try {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    var item = $scope.ListadoRemesasFiltradas[i]
                    if (item.Seleccionado) {
                        $scope.Modelo.Planilla.Cantidad += item.Remesa.CantidadCliente
                        //$scope.Modelo.Planilla.Peso += item.Remesa.PesoCliente
                        $scope.Modelo.Planilla.Peso += item.PesoCobrar == 0 ? $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria ? item.Remesa.PesoCliente >= item.PesoVolumetrico ? item.Remesa.PesoCliente : item.PesoVolumetrico :item.Remesa.PesoCliente : item.PesoCobrar
                    }
                }
            } catch (e) {

            }

            if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== []) {
                for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                    var item = $scope.ListadoGuiaGuardadas[i]
                    $scope.Modelo.Planilla.Cantidad += item.Remesa.CantidadCliente
                    //$scope.Modelo.Planilla.Peso += item.Remesa.PesoCliente
                    if (item.PesoCobrar == undefined || item.PesoCobrar == 0) {
                        
                        if ($scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria) {
                            if (item.PesoVolumetrico != undefined) {
                                if (item.Remesa.PesoCliente >= item.Remesa.PesoVolumetricoCliente) {
                                    $scope.Modelo.Planilla.Peso += item.Remesa.PesoCobrar
                                } else {
                                    $scope.Modelo.Planilla.Peso += item.Remesa.PesoCliente
                                }
                            }
                            else {
                                $scope.Modelo.Planilla.Peso += item.Remesa.PesoCliente
                            }
                        } else {
                            $scope.Modelo.Planilla.Peso += item.Remesa.PesoCliente
                        }
                        
                    } else if (item.PesoCobrar != undefined) {
                        $scope.Modelo.Planilla.Peso += item.Remesa.PesoCobrar
                    } 

                    //$scope.Modelo.Planilla.Peso += item.PesoCobrar == 0 ? $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria ? item.Remesa.PesoCliente >= item.PesoVolumetrico ? item.Remesa.PesoCliente : item.PesoVolumetrico : item.Remesa.PesoCliente : item.PesoCobrar
                }
            }


            if ($scope.ListaResultadoFiltroConsultaRecolecciones.length > 0) {
                //$scope.ModeloCantidadTotal = $scope.ValorAuxiliar;
                $scope.ListaResultadoFiltroConsultaRecolecciones.forEach(function (itemFiltro) {
                    if (itemFiltro.SeleccionadoRecolecciones == true) {
                        $scope.Modelo.Planilla.Cantidad += parseInt(itemFiltro.Cantidad)
                        $scope.Modelo.Planilla.Peso += parseInt(itemFiltro.Peso)
                    }
                })
            } else {
                if ($scope.ListadoRecoleccionesTotal.length > 0) {
                    $scope.ListadoRecoleccionesTotal.forEach(function (itemTotal) {
                        if (itemTotal.SeleccionadoRecolecciones == true) {
                            $scope.Modelo.Planilla.Cantidad += parseInt(itemTotal.Cantidad)
                            $scope.Modelo.Planilla.Peso += parseInt(itemTotal.Peso)
                        }
                    })
                }
            }
            try {
                $scope.Modelo.Planilla.ValorFleteTransportador = $scope.TipoTarifaTransportes.ValorFlete

            } catch (e) {

            }


            if ($scope.Modelo.Planilla.ValorAnticipo !== undefined && $scope.Modelo.Planilla.ValorAnticipo !== '') {
                if ($scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorAnticipo) > $scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorFleteTransportador)) {
                    ShowError('El valor del anticipo no puede ser mayor al valor del flete del transportador')
                    $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorFleteTransportador)
                } else {
                    $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid($scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorFleteTransportador) - $scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorAnticipo))
                }
            } else {
                $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorFleteTransportador)
            }
            try {
                $scope.ListadoImpuestosFiltrado = []
                if ($scope.ListadoImpuestos.length > 0) {
                    for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                        var impuesto = {
                            Nombre: $scope.ListadoImpuestos[i].Nombre,
                            CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                            ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                            ValorBase: $scope.ListadoImpuestos[i].valor_base,
                        }
                        if ($scope.ListadoImpuestos[i].valor_base < parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))) {
                            impuesto.ValorBase = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))
                        }

                        impuesto.ValorImpuesto = impuesto.ValorTarifa * impuesto.ValorBase
                        $scope.ListadoImpuestosFiltrado.push(impuesto)
                    }
                }
            } catch (e) {

            }
            try {
                $scope.TotalImpuestos = 0
                if ($scope.ListadoImpuestosFiltrado.length > 0) {
                    for (var i = 0; i < $scope.ListadoImpuestosFiltrado.length; i++) {
                        $scope.TotalImpuestos += parseFloat($scope.ListadoImpuestosFiltrado[i].ValorImpuesto)
                    }
                }
                $scope.Modelo.Planilla.ValorPagarTransportador = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorPagarTransportador)) - $scope.TotalImpuestos
            } catch (e) {

            }

            $scope.Modelo.Planilla.ValorRetencionFuente = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorRetencionFuente)
            $scope.Modelo.Planilla.ValorRetencionICA = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorRetencionICA)
            $scope.Modelo.Planilla.ValorFleteCliente = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorFleteCliente)
            $scope.Modelo.Planilla.ValorSeguroMercancia = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorSeguroMercancia)
            $scope.Modelo.Planilla.ValorOtrosCobros = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorOtrosCobros)
            $scope.Modelo.Planilla.ValorTotalCredito = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorTotalCredito)
            $scope.Modelo.Planilla.ValorTotalContado = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorTotalContado)
            $scope.Modelo.Planilla.ValorTotalAlcobro = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorTotalAlcobro)
            $scope.Modelo.Planilla.ValorFleteTransportador = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorFleteTransportador)
            $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorPagarTransportador)
            $scope.TotalImpuestos = $scope.MaskValoresGrid($scope.TotalImpuestos)
            //CalcularImpuesto();
            $scope.MaskValores()

        }



        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        $scope.ModeloEstado = { Codigo: 0, Nombre: 'BORRADOR' };
        $scope.Modelo.FechaFinal = new Date();
        $scope.ListadoRemesas = [];
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesErrorConslta = [];
            if (DatosRequeridosConsultaRemesas()) {
                if ($scope.MensajesErrorConslta.length == 0) {
                    blockUI.delay = 1000;
                    $scope.Filtro.Pagina = 0
                    $scope.Filtro.RegistrosPagina = 0
                    //$scope.Modelo.RegistrosPagina = 10
                    $scope.Modelo.Remesa.Numeroplanillarecoleccion = 1
                    $scope.Modelo.AplicaConsultaMaster = 1;
                    $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    if ($scope.ModeloEstado !== undefined && $scope.ModeloEstado !== null) {
                        $scope.Modelo.Estado = $scope.ModeloEstado.Codigo;
                    } else {
                        $scope.Modelo.Estado = -1;
                    }

                   // var filtro = angular.copy($scope.Modelo);
                    var filtro = {
                        AplicaConsultaMaster: 1,
                        Codigo: $scope.Modelo.Codigo,
                        CodigoAlterno: $scope.Modelo.CodigoAlterno,
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Estado: $scope.Sesion.UsuarioAutenticado.ManejoConsultarRemesasBorradorEnPlanillaRecoleccion ? -1 : ESTADO_ACTIVO,
                        FechaFinal: $scope.Modelo.FechaFinal,
                        FechaInicial: $scope.Modelo.FechaInicial,
                        //Nombre: $scope.Modelo.Nombre,
                        Numero: $scope.Modelo.NumeroInicial,
                        Oficina: { Codigo: $scope.Modelo.Oficina == null ? 0: $scope.Modelo.Oficina.Codigo },
                        OficinaOrigen: { Codigo: $scope.Modelo.OficinaOrigen == null ? 0 : $scope.Modelo.OficinaOrigen.Codigo },
                        Remesa: {
                            CiudadRemitente: { Codigo: $scope.Modelo.Remesa.CiudadRemitente == undefined ? 0: $scope.Modelo.Remesa.CiudadRemitente.Codigo },
                            CiudadDestinatario: { Codigo: $scope.Modelo.Remesa.CiudadDestinatario == undefined ? 0 : $scope.Modelo.Remesa.CiudadDestinatario.Codigo },
                            Cliente: { Codigo: $scope.Modelo.Remesa.Cliente == undefined ? 0 : $scope.Modelo.Remesa.Cliente.Codigo },
                            NumeroDocumentoCliente: $scope.Modelo.Remesa.NumeroDocumentoCliente,
                            Remitente: { Codigo: $scope.Modelo.Remesa.Remitente == undefined ? 0 :  $scope.Modelo.Remesa.Remitente.Codigo },
                            Numeroplanillarecoleccion : 1
                        },
                        TipoDocumento : CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA

                    }

                    filtro.Anulado = -1;

                    RemesaGuiasFactory.Consultar(filtro).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Modelo.Codigo = 0
                                if (response.data.Datos.length > 0) {
                                    if ($scope.ListadoRemesas.length == 0) {
                                        $scope.ListadoRemesas = response.data.Datos
                                    } else {
                                        for (var j = 0; j < $scope.ListadoRemesas.length; j++) {
                                            if ($scope.ListadoRemesas[j].Seleccionado) {
                                                var cont = 0
                                                for (var i = 0; i < response.data.Datos.length; i++) {
                                                    if ($scope.ListadoRemesas[j].Remesa.Numero == response.data.Datos[i].Remesa.Numero) {
                                                        response.data.Datos[i] = $scope.ListadoRemesas[j]
                                                        cont++
                                                    }
                                                }
                                                if (cont == 0) {
                                                    response.data.Datos.push($scope.ListadoRemesas[j])
                                                }
                                            }
                                        }
                                    }
                                    $scope.ListadoRemesas = response.data.Datos
                                    $scope.ListadoRemesasFiltradas = $scope.ListadoRemesas
                                    $scope.totalRegistros = $scope.ListadoRemesas.length
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                                    $scope.PrimerPagina()
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                    $('#btncriterios').click()
                                }
                                else {
                                    datatemp = []
                                    for (var j = 0; j < $scope.ListadoRemesas.length; j++) {
                                        if ($scope.ListadoRemesas[j].Seleccionado) {

                                            datatemp.push($scope.ListadoRemesas[j])
                                        }
                                    }
                                    $scope.ListadoRemesas = datatemp
                                    $scope.ListadoRemesasFiltradas = $scope.ListadoRemesas
                                    $scope.totalRegistros = $scope.ListadoRemesas.length
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                                    $scope.PrimerPagina()
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                    $('#btncriterios').click()
                                    //$scope.totalRegistros = 0;
                                    //$scope.totalPaginas = 0;
                                    //$scope.paginaActual = 1;
                                    //$scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    //$scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });

                                $scope.ListadoOriginalRemesas = $scope.ListadoRemesas;
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.ListadoGuias = []
                $scope.paginaActual = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                }
            } else {
                $scope.ListadoGuias = $scope.ListadoRemesasFiltradas
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.ListadoGuias = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.ListadoGuias = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 10
                $scope.ListadoGuias = []
                for (var i = a - 10; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                }
            }
        }

        function DatosRequeridosConsultaRemesas() {
            $scope.MensajesErrorConslta = [];
            var modelo = $scope.Modelo
            var continuar = true;
            if ((modelo.FechaInicial == null || modelo.FechaInicial == undefined || modelo.FechaInicial == '')
                && (modelo.FechaFinal == null || modelo.FechaFinal == undefined || modelo.FechaFinal == '')
                && (modelo.NumeroInicial == null || modelo.NumeroInicial == undefined || modelo.NumeroInicial == '' || modelo.NumeroInicial == 0)
            ) {
                $scope.MensajesErrorConslta.push('Debe ingresar los filtros de números o fechas');
                continuar = false

            } else if ((modelo.FechaInicial !== null && modelo.FechaInicial !== undefined && modelo.FechaInicial !== '')
                || (modelo.FechaFinal !== null && modelo.FechaFinal !== undefined && modelo.FechaFinal !== '')
            ) {
                if ((modelo.FechaInicial !== null && modelo.FechaInicial !== undefined && modelo.FechaInicial !== '')
                    && (modelo.FechaFinal !== null && modelo.FechaFinal !== undefined && modelo.FechaFinal !== '')) {
                    if (modelo.FechaFinal < modelo.FechaInicial) {
                        $scope.MensajesErrorConslta.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if (((modelo.FechaFinal - modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > 31) {
                        $scope.MensajesErrorConslta.push('El rango de fechas no puede ser superior a 31 dias');
                        continuar = false
                    }
                }

                else {
                    if ((modelo.FechaInicial !== null && modelo.FechaInicial !== undefined && modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = modelo.FechaInicial
                    } else {
                        $scope.Modelo.FechaInicial = modelo.FechaFinal
                    }
                }
            }


            return continuar
        }
        $scope.MarcarRemesas = function (chk) {
            if (chk) {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoRemesasFiltradas[i].Seleccionado = true
                }
            } else {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoRemesasFiltradas[i].Seleccionado = false
                }
            }
        }
        $scope.Filtrar = function () {
            $scope.totalRegistros = 0;
            var Filtro = $scope.Filtro2
            var nFiltrosAplica = 0
            $scope.ListadoRemesas = [];
            $scope.ListadoRemesasFiltradas = [];
            $scope.ListadoGuias = [];

            if (($scope.Filtro.NumeroInicial !== undefined && $scope.Filtro.NumeroInicial > 0) && ($scope.Filtro.NumeroFinal == undefined || $scope.Filtro.NumeroFinal == null || $scope.Filtro.NumeroFinal == '' || isNaN($scope.Filtro.NumeroFinal))) {
                $scope.Filtro.NumeroFinal = $scope.Filtro.NumeroInicial
            }
            for (var i = 0; i < $scope.ListadoOriginalRemesas.length; i++) {
                var item = $scope.ListadoOriginalRemesas[i];
                var agregar = true;
                if (Filtro.NumeroInicial !== undefined && Filtro.NumeroInicial > 0) {
                    
                    if (item.Remesa.NumeroDocumento < Filtro.NumeroInicial) {
                        agregar = false;
                    }
                }
                if (Filtro.NumeroFinal !== undefined && Filtro.NumeroFinal > 0) {
                    
                    if (item.Remesa.NumeroDocumento > Filtro.NumeroFinal) {
                        agregar = false;
                    }
                }
                if (Filtro.FechaInicial !== undefined && Filtro.FechaInicial > 0) {
                    
                    if (new Date(item.Remesa.Fecha) < Filtro.FechaInicial) {
                        agregar = false;
                    }
                }
                if (Filtro.FechaFinal !== undefined && Filtro.FechaFinal > 0) {
                    
                    if (new Date(item.Remesa.Fecha) > Filtro.FechaFinal) {
                        agregar = false;
                    }
                }
                if (agregar) {
                    $scope.ListadoRemesas.push(item);
                    $scope.ListadoRemesasFiltradas.push(item);
                    $scope.totalRegistros = $scope.totalRegistros + 1;
                }
            }
           

            $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
            $scope.PrimerPagina();

        }
        $scope.AgregarAuxiliar = function () {
            if ($scope.ModeloFuncionario !== '' && $scope.ModeloFuncionario !== undefined && $scope.ModeloFuncionario !== null && $scope.ModeloFuncionario.Codigo !== 0 && $scope.ModeloFuncionario.Codigo !== undefined) {
                var concidencias = 0
                if ($scope.ListaAuxiliares.length > 0) {
                    for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                        if ($scope.ModeloFuncionario.Codigo === $scope.ListaAuxiliares[i].Tercero.Codigo) {
                            concidencias++
                            break;
                        }
                    }
                    if (concidencias > 0) {
                        ShowError('El funcionario ya fue ingresado')
                        $scope.ModeloFuncionario = '';
                    } else {
                        $scope.ListaAuxiliares.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto }, Modificarfuncionario: true });
                        $scope.ModeloFuncionario = '';
                    }
                } else {
                    $scope.ListaAuxiliares.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto }, Modificarfuncionario: true });
                    $scope.ModeloFuncionario = '';
                }
            } else {
                ShowError('Debe ingresar un funcionario valido');
            }
            $scope.ValidarDatosFuncionario();
        }
        $scope.ValidarDatosFuncionario = function () {
            for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                if ($scope.ListaAuxiliares[i].Horas === '' || $scope.ListaAuxiliares[i].Horas === undefined || $scope.ListaAuxiliares[i].Horas === null || $scope.ListaAuxiliares[i].Horas === 0
                    || $scope.ListaAuxiliares[i].Valor === '' || $scope.ListaAuxiliares[i].Valor === undefined || $scope.ListaAuxiliares[i].Valor === null || $scope.ListaAuxiliares[i].Valor === 0) {
                    //ShowError('Debe ingresar los detalles de horas trabajadas y valor por funcionario');
                } else {
                    $scope.ListaAuxiliares[i].Modificarfuncionario = false;
                }
            }
        }
        $scope.EliminarFuncionario = function (codigo) {
            if ($scope.ListaAuxiliares.length > 0) {
                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                    if (codigo === $scope.ListaAuxiliares[i].Codigo) {
                        $scope.ListaAuxiliares.splice(i, 1);
                    }
                }
            }
        };
        $scope.ListaImpuestos = []
        $scope.AgregarImpuestos = function () {
            if ($scope.Impuestos == '' || $scope.Impuestos == null || $scope.Impuestos == undefined) {
                ShowError('De ingresar el impuesto')
            }
            else {
                if ($scope.ListaImpuestos.length == 0) {
                    $scope.ListaImpuestos.push($scope.Impuestos)
                    $scope.Impuestos = ''
                } else {
                    var cont = 0
                    for (var i = 0; i < $scope.ListaImpuestos.length; i++) {
                        if ($scope.Impuestos.Codigo == $scope.ListaImpuestos[i].Codigo) {
                            cont++
                            break;
                        }
                    }
                    if (cont > 0) {
                        ShowError('El impuesto que intenta agregar ya se encuentra adicionado')
                    } else {
                        $scope.ListaImpuestos.push($scope.Impuestos)
                    }
                    $scope.Impuestos = ''
                }

            }
        }
        $scope.EliminarImpuesto = function (index) {
            $scope.ListaImpuestos.splice(index, 1);
        };

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarPlanillaRecolecciones/' + $scope.Modelo.Planilla.NumeroDocumento;
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            //ShowError('Opción no disponible por el momento')
            if (DatosRequeridos()) {
                //Estado
                $scope.Modelo.Planilla.Detalles = []
                if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== '') {
                    if ($scope.ListadoGuiaGuardadas.length > 0) {
                        $scope.Modelo.Planilla.Detalles = $scope.ListadoGuiaGuardadas;
                    }
                }
                if ($scope.ListadoRemesasFiltradas !== undefined && $scope.ListadoRemesasFiltradas !== null && $scope.ListadoRemesasFiltradas !== '') {
                    for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                        var guia = $scope.ListadoRemesasFiltradas[i]
                        if (guia.Seleccionado) {
                            $scope.Modelo.Planilla.Detalles.push({ Remesa: guia.Remesa })
                        }
                    }
                }
                if ($scope.DetallesAuxiliares.length > 0) {
                    $scope.Modelo.Planilla.DetallesAuxiliares = $scope.DetallesAuxiliares
                }
                $scope.Modelo.Planilla.FechaHoraSalida = new Date($scope.Modelo.Planilla.FechaSalida)
                var Horasminutos = []
                Horasminutos = $scope.Modelo.Planilla.HoraSalida.split(':')
                if (Horasminutos.length > 0) {
                    $scope.Modelo.Planilla.FechaHoraSalida.setHours(Horasminutos[0])
                    $scope.Modelo.Planilla.FechaHoraSalida.setMinutes(Horasminutos[1])
                }
                $scope.Modelo.Planilla.TarifaTransportes = { Codigo: $scope.Modelo.Planilla.TarifaTransportes.Codigo }
                $scope.Modelo.Planilla.TipoTarifaTransportes = { Codigo: $scope.TipoTarifaTransportes.Codigo }

                if ($scope.ListadoImpuestosFiltrado.length > 0) {
                    $scope.Modelo.Planilla.DetalleImpuesto = $scope.ListadoImpuestosFiltrado;
                }
                if ($scope.ListaAuxiliares.length > 0) {
                    $scope.Modelo.Planilla.DetallesAuxiliares = []
                    for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                        if (!$scope.ListaAuxiliares[i].Modificarfuncionario) {
                            $scope.Modelo.Planilla.DetallesAuxiliares.push({
                                Funcionario: $scope.ListaAuxiliares[i].Tercero
                                , NumeroHorasTrabajadas: $scope.ListaAuxiliares[i].Horas
                                , Valor: $scope.ListaAuxiliares[i].Valor
                                , Observaciones: $scope.ListaAuxiliares[i].Observaciones
                            })
                        }
                    }
                }
                try {
                    if (parseInt($scope.Modelo.Planilla.ValorAnticipo) > CERO) {
                        $scope.Modelo.CuentaPorPagar = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                            CodigoAlterno: '',
                            Fecha: $scope.Modelo.Planilla.Fecha,
                            Tercero: { Codigo: $scope.Modelo.Planilla.Vehiculo.Conductor.Codigo },
                            DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO },
                            CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO,
                            Numeracion: '',
                            CuentaPuc: { Codigo: 0 },
                            ValorTotal: $scope.Modelo.Planilla.ValorAnticipo,
                            Abono: 0,
                            Saldo: $scope.Modelo.Planilla.ValorAnticipo,
                            FechaCancelacionPago: $scope.Modelo.Planilla.Fecha,
                            Aprobado: 1,
                            UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            FechaAprobo: $scope.Modelo.Planilla.Fecha,
                            Oficina: { Codigo: $scope.ModeloOficina.Codigo },
                            Vehiculo: { Codigo: $scope.Modelo.Planilla.Vehiculo.Codigo },
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        }
                    }
                } catch (e) {

                }
                $scope.ListaPlanillaRecolecciones = [];
                $scope.ListaPlanillaRecoleccionesNoSeleccionadas = [];
                $scope.ListadoRecoleccionesTotal.forEach(function (itemDetalle) {
                    if (itemDetalle.SeleccionadoRecolecciones == true) {

                        $scope.NumeroRecoleccion = itemDetalle.Numero;

                        $scope.DetalleRecoleccion = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            NumeroPlanilla: $scope.Numero,
                            Numero: $scope.NumeroRecoleccion,
                        };

                        $scope.ListaPlanillaRecolecciones.push($scope.DetalleRecoleccion)
                    }
                    if (itemDetalle.SeleccionadoRecolecciones == false) {

                        $scope.NumeroRecoleccionNoCheck = itemDetalle.Numero;

                        var RecoleccionesNoSeleccionadas = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            NumeroPlanilla: $scope.Numero,
                            Numero: $scope.NumeroRecoleccionNoCheck,
                        }
                        $scope.ListaPlanillaRecoleccionesNoSeleccionadas.push(RecoleccionesNoSeleccionadas)
                    }
                })
                $scope.Modelo.ListadoPlanillaRecolecciones = $scope.ListaPlanillaRecolecciones
                $scope.Modelo.ListadoPlanillaRecoleccionesNoCkeck = $scope.ListaPlanillaRecoleccionesNoSeleccionadas
                $scope.Modelo.TipoDocumento = 205;
                var planilla = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Planilla: {
                        FechaHoraSalida: $scope.Modelo.Planilla.FechaHoraSalida,
                        Fecha: $scope.Modelo.Planilla.Fecha,
                        Ruta: $scope.Modelo.Planilla.Ruta,
                        Numero: $scope.CodigoPlanilla,
                        Vehiculo: {
                            Codigo: $scope.Modelo.Planilla.Vehiculo.Codigo,
                            Tenedor: { Codigo: $scope.Modelo.Planilla.Vehiculo.Tenedor.Codigo },
                            Conductor: { Codigo: $scope.Modelo.Planilla.Vehiculo.Conductor.Codigo },
                        },
                        Semirremolque: $scope.Modelo.Planilla.Semirremolque,
                        Cantidad: $scope.Modelo.Planilla.Cantidad,
                        Peso: $scope.Modelo.Planilla.Peso,
                        ValorFleteTransportador: $scope.Modelo.Planilla.ValorFleteTransportador,
                        ValorAnticipo: $scope.Modelo.Planilla.ValorAnticipo,
                        ValorImpuestos: $scope.Modelo.Planilla.ValorImpuestos,
                        ValorPagarTransportador: $scope.Modelo.Planilla.ValorPagarTransportador,
                        ValorFleteCliente: $scope.Modelo.Planilla.ValorFleteCliente,
                        ValorSeguroMercancia: $scope.Modelo.Planilla.ValorSeguroMercancia,
                        ValorOtrosCobros: $scope.Modelo.Planilla.ValorOtrosCobros,
                        ValorTotalCredito: $scope.Modelo.Planilla.ValorTotalCredito,
                        ValorTotalContado: $scope.Modelo.Planilla.ValorTotalContado,
                        ValorTotalAlcobro: $scope.Modelo.Planilla.ValorTotalAlcobro,
                        Observaciones: $scope.Modelo.Planilla.Observaciones,
                        Estado: $scope.Modelo.Planilla.Estado,
                        TarifaTransportes: $scope.Modelo.Planilla.TarifaTransportes,
                        TipoTarifaTransportes: $scope.Modelo.Planilla.TipoTarifaTransportes,
                        ValorSeguroPoliza: $scope.Modelo.Planilla.ValorSeguroPoliza,
                        AnticipoPagadoA: $scope.Modelo.Planilla.AnticipoPagadoA,
                        RemesaPadre: $scope.Modelo.Planilla.RemesaPadre,
                        RemesaMasivo: $scope.Modelo.Planilla.RemesaMasivo,
                        Detalles: $scope.Modelo.Planilla.Detalles,
                        DetallesAuxiliares: $scope.Modelo.Planilla.DetallesAuxiliares,
                        AutorizacionAnticipo: $scope.Modelo.Planilla.AutorizacionAnticipo,
                        DetalleImpuesto: $scope.Modelo.Planilla.DetalleImpuesto
                    },
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    ListadoPlanillaRecoleccionesNoCkeck: $scope.ListaPlanillaRecoleccionesNoSeleccionadas,
                    ListadoPlanillaRecolecciones: $scope.ListaPlanillaRecolecciones,
                    TipoDocumento: 205,
                    Oficina: { Codigo: $scope.ModeloOficina.Codigo },
                    CuentaPorPagar: $scope.Modelo.CuentaPorPagar
                }
                PlanillaGuiasFactory.Guardar(planilla).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > CERO) {
                                if ($scope.Modelo.Codigo == CERO) {
                                    ShowSuccess('Se guardó la planilla con el número ' + response.data.Datos + '');
                                }
                                else {
                                    ShowSuccess('Se modificó la planilla con el número ' + response.data.Datos + '');
                                }
                                location.href = '#!ConsultarPlanillaRecolecciones/' + response.data.Datos;
                                //if ($scope.Sesion.Empresa.GeneraManifiesto === 1 && $scope.Modelo.Planilla.Estado.Codigo == 1 && $scope.Modelo.Planilla.Ruta.TipoRuta.Codigo == 4401) {
                                //    $scope.Manifiesto = {};
                                //    $scope.ModeloCxP = {};

                                //    $scope.Manifiesto = {
                                //        Numero: 0,
                                //        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                //        NumeroDocumentoPlanilla: response.data.Datos,
                                //        Tipo_Documento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO },
                                //        Usuario_Crea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                //        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                                //        Tipo_Manifiesto: { Codigo: CODIGO_CATALOGO_TIPO_MANIFIESTO_PLANILLA_PAQUETERIA },
                                //        //NumeroSolicitudServicio: $scope.Numero,
                                //        //IdDetalleSolicitudServicio: $scope.CodigoDetalleSolicitud,
                                //        Valor_Anticipo: $scope.Modelo.Planilla.ValorAnticipo,
                                //        CuentaPorPagar: $scope.ModeloCxP
                                //    }

                                //    ManifiestoFactory.Guardar($scope.Manifiesto).then(function (response) {
                                //        if (response.data.ProcesoExitoso == true) {
                                //            if (response.data.Datos > CERO) {
                                //                $scope.ModalManifiesto = response.data.Datos;
                                //            } else if (response.data.Datos == -1) {
                                //                $scope.ModalManifiesto = 0;
                                //            } else {
                                //                ShowError(response.statusText);
                                //                $scope.ModalManifiesto = 0;
                                //            }
                                //        }
                                //        else {
                                //            ShowError(response.statusText);
                                //        }
                                //    }, function (response) {
                                //        ShowError(response.statusText);
                                //    });
                                //}
                                location.href = '#!ConsultarPlanillaRecolecciones/' + response.data.Datos;
                            }
                            else {
                                if (response.data.Datos == -3) {
                                    ShowError('El vehículo tiene cumplidos pendientes por realizar')
                                } else {
                                    ShowError(response.statusText);
                                }
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo
            var fecha1 = new Date(Modelo.Planilla.Fecha)
            var fecha2 = new Date(Modelo.Planilla.FechaSalida)
            var fecha3 = new Date()
            fecha1.setHours('00')
            fecha1.setMinutes('00')
            fecha1.setSeconds('00')
            fecha1.setMilliseconds('00')
            fecha2.setHours('00')
            fecha2.setMinutes('00')
            fecha2.setSeconds('00')
            fecha2.setMilliseconds('00')
            fecha3.setHours('00')
            fecha3.setMinutes('00')
            fecha3.setSeconds('00')
            fecha3.setMilliseconds('00')
            if (Modelo.Planilla.Fecha == undefined || Modelo.Planilla.Fecha == '' || Modelo.Planilla.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha ');
                continuar = false;
            }
            else if (fecha1 < fecha3) {
                $scope.MensajesError.push('La fecha ingresada no se puede ser menor a la fecha actual');
                continuar = false;
            }
            if (Modelo.Planilla.Vehiculo == undefined || Modelo.Planilla.Vehiculo == '') {
                $scope.MensajesError.push('Debe ingresar el vehículo');
                continuar = false;
            }
            if (Modelo.Planilla.FechaSalida == undefined || Modelo.Planilla.FechaSalida == '' || Modelo.Planilla.FechaSalida == null) {
                $scope.MensajesError.push('Debe ingresar la fecha de salida');
                continuar = false;
            }
            else if (fecha2 < fecha3) {
                $scope.MensajesError.push('La fecha de salida ingresada no se puede ser menor a la fecha actual');
                continuar = false;
            }
            if (Modelo.Planilla.HoraSalida == undefined || Modelo.Planilla.HoraSalida == '' || Modelo.Planilla.HoraSalida == null) {
                $scope.MensajesError.push('Debe ingresar la hora de salida');
                continuar = false;
            }
            else {
                var horas = Modelo.Planilla.HoraSalida.split(':')
                if (horas.length < 2) {
                    $scope.MensajesError.push('Debe ingresar una hora valida');
                    continuar = false;
                } else if (fecha2 == fecha3) {
                    if (parseInt(horas[0]) < fecha3.getHours()) {
                        $scope.MensajesError.push('La hora ingresada debe ser mayor a la actual');
                        continuar = false;
                    } else if (parseInt(horas[0]) == fecha3.getHours()) {
                        if (parseInt(horas[1]) < fecha3.getMinutes()) {
                            $scope.MensajesError.push('La hora ingresada debe ser mayor a la actual');
                            continuar = false;
                        } else {
                            if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                                $scope.MensajesError.push('Debe ingresar una hora valida');
                                continuar = false;
                            }
                        }

                    }

                } else {
                    if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                        $scope.MensajesError.push('Debe ingresar una hora valida');
                        continuar = false;
                    }
                }

            }
            $scope.contadorrecolecciones = 0;
            $scope.ListadoRecoleccionesTotal.forEach(function (itemDetalle) {
                if (itemDetalle.SeleccionadoRecolecciones == true) {
                    $scope.contadorrecolecciones = $scope.contadorrecolecciones + 1
                }
            })
            try {
                $scope.contadorguias = $scope.ListadoGuiaGuardadas.length

            } catch (e) {
                $scope.contadorguias = 0;

            }
            try {
                $scope.ListadoRemesasFiltradas.forEach(function (itemDetalle) {
                    if (itemDetalle.Seleccionado == true) {
                        $scope.contadorguias = $scope.contadorguias + 1
                    }
                })
            } catch (e) {

            }


            if ($scope.contadorrecolecciones === 0 && $scope.contadorguias === 0) {
                $scope.MensajesError.push('Debe seleccionar mínimo un detalle');
                continuar = false;
            }

            return continuar;
        }

        $scope.ConfirmacionGuardar = function () {
            if ($scope.DeshabilitarActualizar) {
                location.href = '#!ConsultarPlanillaRecolecciones/' + $scope.Modelo.Planilla.Numero;
            } else {
                showModal('modalConfirmacionGuardar');
            }
        };

        function Obtener() {
            blockUI.start('Cargando tarifario Compras código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando tarifario Compras Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
            };

            blockUI.delay = 1000;
            PlanillaGuiasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.esObtener = true
                        $scope.ListadoGuiaGuardadas = []
                        $scope.Modelo = response.data.Datos
                        $scope.Modelo.Remesa = {
                            Cliente: '', Ruta: {}
                        }

                        $scope.Modelo.Planilla.FechaSalida = new Date($scope.Modelo.Planilla.FechaHoraSalida)
                        $scope.Modelo.Planilla.Fecha = new Date($scope.Modelo.Planilla.Fecha)
                        $scope.Modelo.Planilla.HoraSalida = $scope.Modelo.Planilla.FechaSalida.getHours().toString()

                        if ($scope.Modelo.Planilla.FechaSalida.getMinutes().toString().length == 1) {
                            $scope.Modelo.Planilla.HoraSalida += ':0' + $scope.Modelo.Planilla.FechaSalida.getMinutes().toString()
                        } else {
                            $scope.Modelo.Planilla.HoraSalida += ':' + $scope.Modelo.Planilla.FechaSalida.getMinutes().toString()
                        }
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        if ($scope.Modelo.Planilla.Estado.Codigo == 1) {
                            $scope.DeshabilitarActualizar = true

                        }
                        $scope.Modelo.Planilla.Fecha = new Date($scope.Modelo.Planilla.Fecha)
                        ////Obtiene el detalle de impuestos aplicados
                        //if ($scope.Modelo.Planilla.DetalleImpuesto !== undefined && $scope.Modelo.Planilla.DetalleImpuesto !== null && $scope.Modelo.Planilla.DetalleImpuesto !== []) {
                        //    $scope.ListaImpuestos = $scope.Modelo.Planilla.DetalleImpuesto;
                        //    //formatea el valor de cada impuesto
                        //    for (var i = 0; i < $scope.ListaImpuestos.length; i++) {
                        //        $scope.ListaImpuestos[i].ValorImpuesto = $scope.MaskValoresGrid($scope.ListaImpuestos[i].ValorImpuesto);
                        //    }
                        //}
                        //CalcularImpuesto();
                        $scope.ListadoGuiaGuardadas = [];
                        var sumatoriapeso = 0;
                        if ($scope.Modelo.Planilla.Detalles !== undefined && $scope.Modelo.Planilla.Detalles !== null && $scope.Modelo.Planilla.Detalles !== []) {
                            $scope.ListadoGuiaGuardadas = $scope.Modelo.Planilla.Detalles
                            $scope.ListadoGuiaGuardadas.forEach(item => {
                                item.Remesa.PesoCobrar = $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria ? item.Remesa.PesoCliente >= item.Remesa.PesoVolumetricoCliente ? item.Remesa.PesoCliente : item.Remesa.PesoVolumetricoCliente : item.Remesa.PesoCliente;
                                sumatoriapeso += MascaraNumero(item.Remesa.PesoCobrar);
                            });
                            $scope.Modelo.Planilla.Peso = MascaraValores(sumatoriapeso);
                            try {
                                $('#btncriterios')[0].click()
                                $('#btnFiltros')[0].click()
                            } catch (e) {

                            }
                        }
                        $scope.ListaAuxiliares = []
                        if ($scope.Modelo.Planilla.DetallesAuxiliares !== undefined && $scope.Modelo.Planilla.DetallesAuxiliares !== null && $scope.Modelo.Planilla.DetallesAuxiliares !== []) {
                            for (var i = 0; i < $scope.Modelo.Planilla.DetallesAuxiliares.length; i++) {
                                $scope.ListaAuxiliares.push({
                                    Tercero: $scope.Modelo.Planilla.DetallesAuxiliares[i].Funcionario,
                                    Horas: $scope.Modelo.Planilla.DetallesAuxiliares[i].NumeroHorasTrabajadas,
                                    Valor: $scope.Modelo.Planilla.DetallesAuxiliares[i].Valor,
                                    Observaciones: $scope.Modelo.Planilla.DetallesAuxiliares[i].Observaciones,
                                })
                            }
                        }
                        /*-----------------------Lista recolecciones--------------------*/
                        $scope.ListaRecoleccionesConsultados = response.data.Datos.ListadoPlanillaRecolecciones;
                        $scope.ListaRecoleccionesConsultados.forEach(function (item) {
                            item.SeleccionadoRecolecciones = $scope.seleccionarCheckRecolecciones;
                        });
                        $scope.ListadoRecoleccionesTotal = $scope.ListaRecoleccionesConsultados;
                        if ($scope.ListadoRecoleccionesTotal.length > 0) {
                            $scope.ListaRecoleccionesGrid = [];
                            var i = 0;
                            for (i = 0; i <= $scope.ListadoRecoleccionesTotal.length - 1; i++) {
                                if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                    if ($scope.ListadoRecoleccionesTotal[i].Cliente.Codigo === 0) {
                                        $scope.ListadoRecoleccionesTotal[i].Cliente.Nombre = ''
                                    }
                                    $scope.ListaRecoleccionesGrid.push($scope.ListadoRecoleccionesTotal[i])
                                }
                            }

                            if ($scope.ListadoRecoleccionesTotal.length > 0) {
                                $scope.totalRegistros = $scope.ListadoRecoleccionesTotal.length;
                                $scope.totalPaginas = Math.ceil($scope.ListadoRecoleccionesTotal.length / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            } else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        } else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                        //$scope.MostrarGuiasAsociadas()
                        $scope.CargarDatosFunciones()
                        $scope.MaskValores()
                        $scope.Filtrar();
                        try {
                            var conductor = $scope.Modelo.Planilla.Vehiculo.Conductor
                            $scope.Modelo.Planilla.Vehiculo = $scope.CargarVehiculos($scope.Modelo.Planilla.Vehiculo.Codigo);
                            $scope.Modelo.Planilla.Vehiculo.Conductor = $scope.CargarTercero(conductor.Codigo)
                            $scope.ObtenerInformacionTenedor()
                        } catch (e) {
                        }

                        
                       // $scope.Calcular();
                    }
                    else {
                        ShowError('No se logro consultar el tarifario Compras código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarPlanillaRecolecciones';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarPlanillaRecolecciones';
                });

            blockUI.stop();
        };
        $scope.EliminarGuiaGuardada = function (index,NumeroRemesa) {
            $scope.ListadoGuiaGuardadas.splice(index, 1);
            PlanillaGuiasFactory.EliminarGuia({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.NumeroPlanilla,
                TipoDocumento : CODIGO_TIPO_DOCUMENTO_PLANILLA_RECOLECCION,
                Planilla: {
                    Detalles: [{ Remesa: { Numero: NumeroRemesa } }]
                },
                Sync: true
            });
        }
        $scope.EliminarRecoleccionGuardada = function (index, item) {
            if (item.SeleccionadoRecolecciones == false || item.SeleccionadoRecolecciones == 0) {
                $scope.ListaRecoleccionesGrid.splice(index, 1);
                for (var i = 0; i < $scope.ListadoRecoleccionesTotal.length; i++) {
                    if (item.Numero == $scope.ListadoRecoleccionesTotal[i].Numero) {
                        $scope.ListadoRecoleccionesTotal.splice(i, 1);
                    }
                }
            }
            PlanillaGuiasFactory.EliminarRecoleccion({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.NumeroPlanilla,
                //TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_RECOLECCION,
                ListadoPlanillaRecolecciones: [ { Numero: item.Numero }],
                Sync: true
            });
        }
        $scope.ValidarAnticipoPlanilla = function (anticipo) {
            anticipo = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorAnticipo))
            if (parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) > 0) {
                if (anticipo > ((parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador)))) {
                    ShowError('El anticipo no puede ser mayor a ' + MascaraValores((parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))))
                    $scope.Modelo.Planilla.ValorAnticipo = 0
                } else {
                    $scope.Modelo.Planilla.ValorAnticipo = MascaraValores(anticipo)
                }
            }
        }
        $('#Recolecciones').show(); $('#Guias').hide();

        $scope.MostrarConsultaRecolecciones = function () {
            $('#Recolecciones').show()
            $('#Guias').hide();
        }
        $scope.MostrarConsultaGuias = function () {
            $('#Recolecciones').hide()
            $('#Guias').show();
        }
        try {
            var Parametros = $routeParams.Codigo.split(',')
            if (Parametros.length > 1) {
                $scope.Modelo.Numero = Parametros[CERO];
                $scope.CodigoPlanilla = Parametros[CERO];
                $scope.aplicabase = true
                Obtener();
            } else {
                if ($routeParams.Codigo > CERO) {
                    $scope.Modelo.Numero = $routeParams.Codigo;
                    $scope.CodigoPlanilla = $routeParams.Codigo;
                    $scope.NumeroPlanilla = $routeParams.Codigo;
                    Obtener();
                }
                else {
                    $scope.Modelo.Numero = 0;
                    $scope.CodigoPlanilla = 0;
                    $scope.CargarDatosFunciones()
                }

            }
        } catch (e) {
            if ($routeParams.Codigo > CERO) {
                $scope.Modelo.Numero = $routeParams.Codigo;
                $scope.CodigoPlanilla = $routeParams.Codigo;
                $scope.NumeroPlanilla = $routeParams.Codigo;
                Obtener();
            }
            else {
                $scope.Modelo.Numero = 0;
                $scope.CodigoPlanilla = 0;
                $scope.CargarDatosFunciones();
                $scope.NumeroPlanilla = 0;
            }

        }

    }]);