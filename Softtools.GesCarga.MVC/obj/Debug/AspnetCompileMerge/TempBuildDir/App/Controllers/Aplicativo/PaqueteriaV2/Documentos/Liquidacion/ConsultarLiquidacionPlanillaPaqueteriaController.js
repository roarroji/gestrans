﻿SofttoolsApp.controller("ConsultarLiquidacionPlanillaPaqueteriaCtrl", ['$scope', '$timeout', 'LiquidacionesFactory', 'TercerosFactory', '$linq', 'blockUI', 'blockUIConfig',
    '$routeParams', 'OficinasFactory', 'EmpresasFactory', 'EncabezadoComprobantesContablesFactory',
    function ($scope, $timeout, LiquidacionesFactory, TercerosFactory, $linq, blockUI, blockUIConfig,
        $routeParams, OficinasFactory, EmpresasFactory, EncabezadoComprobantesContablesFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CONSULTAR LIQUIDACIONES PAQUETERÍA';
        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Documentos' }, { Nombre: 'Liquidación Planilla Paquetería' }];
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAQUETERIA.LIQUIDACION);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.MostrarMensajeError = false;
        $scope.NumeroAnular;
        $scope.NumeroDocumentoAnular;
        $scope.pref = '';
        $scope.ListadoOficinas = [];
        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.ListadoOficinas = [{ Codigo: -1, Nombre: '(TODAS)' }];
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaInicial = new Date();
            $scope.ModeloFechaFinal = new Date();
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------Init
        $scope.InitLoad = function () {
            //--Empresa
            EmpresasFactory.Consultar(
                { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }
            ).then(function (response) {
                if (response.data.ProcesoExitoso === true) {

                    $scope.pref = response.data.Datos[0].Prefijo;

                }
            });
            //--Oficinas
            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                var responseOficina = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true });
                if (responseOficina.ProcesoExitoso) {
                    responseOficina.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item);
                    });
                    $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                }
                else {
                    ShowError(responseOficina.statusText);
                }
            }
            else {
                if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 });
                    $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                        $scope.ListadoOficinas.push(item);
                    });
                    $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                }
                else {
                    ShowError('El usuario no tiene oficinas asociadas');
                }
            }
            //--Obtener Parametros
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
                if ($routeParams.Numero > 0) {
                    $scope.ModeloNumero = $routeParams.Numero;
                    PantallaBloqueo(Find);
                }
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            PantallaBloqueo(Find);
        };
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                PantallaBloqueo(Find);
            }
        };
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                PantallaBloqueo(Find);
            }
        };
        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            PantallaBloqueo(Find);
        };
        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_LIQUIDACION_PLANILLA_DESPACHO;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroDocumento;
            }

        }

        $scope.ImprimirVarios = function () {
            showModal('ModalImprimir');
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.ModeloNumero,
                FechaInicial: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaInicial,
                FechaFinal: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaFinal,
                NumeroPlanilla: $scope.ModeloPlanilla,
                NumeroManifiesto: $scope.ModeloManifiesto,
                PlacaVehiculo: $scope.ModeloPlaca,
                Conductor: { Nombre: $scope.ModeloConductor },
                Tenedor: { Nombre: $scope.ModeloTenedor },
                Oficina: $scope.ModeloOficina,
                Estado: $scope.ModeloEstado.Codigo,
                Pagina: $scope.paginaActual,
                Numero: 0,
                Aprobado: -1,
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA }
            };
            $scope.ListadoLiquidaciones2 = [];
            LiquidacionesFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Numero = 0;
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoLiquidaciones2 = response.data.Datos;

                        }

                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        $scope.Marcar = function (chk) {
            if (chk) {
                for (var i = 0; i < $scope.ListadoLiquidaciones2.length; i++) {
                    $scope.ListadoLiquidaciones2[i].Seleccionado = true
                }
            } else {
                for (var i = 0; i < $scope.ListadoLiquidaciones2.length; i++) {
                    $scope.ListadoLiquidaciones2[i].Seleccionado = false
                }
            }
        }
        $scope.ConfirmarImprimir = function () {
            ShowWarningConfirm('¿Esta seguro de imprimir los documentos seleccionados?', $scope.ImprimirDocumentos);
        };
        $scope.ImprimirDocumentos = function () {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NombreReporte = NOMBRE_REPORTE_LIQUIDACION_PLANILLA_DESPACHO;
            var Numeros = '';
            var ContadorSeleccionados = 0;
            for (var i = 0; i < $scope.ListadoLiquidaciones2.length; i++) {
                if ($scope.ListadoLiquidaciones2[i].Seleccionado) {
                    ContadorSeleccionados++;
                    Numeros += (',' + $scope.ListadoLiquidaciones2[i].Numero) //se concatenan los numeros de los documentos a imprimir en un solo reporte
                }
            }
            Numeros = Numeros.substring(1); //Se elimina el primer caracter ya que es una ,
            if (ContadorSeleccionados > 1) {
                closeModal('ModalImprimir');
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&OpcionExportarPdf=1&Masivo=1&Prefijo=' + $scope.pref + '&ListaNumeros=' + Numeros);

            } else {
                ShowError('Debe seleccionar mas de 2 documentos');
            }
        };
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        //--Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarLiquidacionPlanillaPaqueteria';
            }
        };
        //--Buscar
        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                if (DatosRequeridos()) {
                    $scope.Codigo = 0;
                    PantallaBloqueo(Find);
                }
            }
        };
        function Find() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroDocumento: $scope.ModeloNumero,
                FechaInicial: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaInicial,
                FechaFinal: $scope.ModeloNumero > 0 ? undefined : $scope.ModeloFechaFinal,
                NumeroPlanilla: $scope.ModeloPlanilla,
                NumeroManifiesto: $scope.ModeloManifiesto,
                PlacaVehiculo: $scope.ModeloPlaca,
                Conductor: { Nombre: $scope.ModeloConductor },
                Tenedor: { Nombre: $scope.ModeloTenedor },
                Oficina: $scope.ModeloOficina,
                Estado: $scope.ModeloEstado.Codigo,
                Pagina: $scope.paginaActual,
                Numero: 0,
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                Aprobado: -1,
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA },
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
            };
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoLiquidaciones = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {
                    $scope.Pagina = $scope.paginaActual;
                    $scope.RegistrosPagina = 10;
                    LiquidacionesFactory.Consultar(filtros).
                        then(function (response) {
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Numero = 0;
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoLiquidaciones = response.data.Datos;
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                            ShowError(response.statusText);
                        });
                }
            }
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.ModeloFechaInicial === null || $scope.ModeloFechaInicial === undefined || $scope.ModeloFechaInicial === '')
                && ($scope.ModeloFechaFinal === null || $scope.ModeloFechaFinal === undefined || $scope.ModeloFechaFinal === '')
                && ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0)
                || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal
                    }
                }
            }
            return continuar
        }
        //--Buscar
        //--Anular
        $scope.Anular = function (Numero, NumeroDocumento, Anulado) {
            $scope.MensajesErrorAnula = [];
            $scope.ModeloCausaAnula = '';
            $scope.NumeroAnular = Numero;
            $scope.NumeroDocumentoAnular = NumeroDocumento;
            if (Anulado === 0) {
                showModal('modalAnular')
            } else {
                ShowError('La liquidación ' + NumeroDocumento + ' ya se encuentra anulada')
            }
        }
        $scope.ConfirmaAnular = function () {
            if ($scope.ModeloCausaAnula !== undefined && $scope.ModeloCausaAnula !== null && $scope.ModeloCausaAnula !== '') {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.NumeroAnular,
                    UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CausaAnula: $scope.ModeloCausaAnula
                }
                LiquidacionesFactory.Anular(filtros).
                    then(function (response) {
                        closeModal('modalAnular')
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.ProcesoExitoso === true) {
                                ShowSuccess('Liquidación anulada');
                                Find();
                            }
                            else {
                                ShowError('Se presentó un error al anular la liquidación')
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                $scope.MensajesErrorAnula.push('Debe ingresar la causa de anulación');
            }
        }
        //--Anular
        //--Generar Movimiento
        $scope.GenerarMovimientoContable = function (NumeroDocumento) {
            EncabezadoComprobantesContablesFactory.Guardar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroDocumento: NumeroDocumento, TipoDocumentoOrigen: { Codigo: 160 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('El movimiento contable se regeneró correctamente')
                    } else {
                        ShowError('No se generó el movimiento contable, por favor verifique la parametrización ')
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        //--Mascaras
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskValoresGrid = function (valor) {
            return MascaraValores(valor);
        };
    }]);