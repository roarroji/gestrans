﻿SofttoolsApp.controller("ConsultarRemesasPaqueteriaCtrl", ['$scope', '$timeout', 'TarifarioVentasFactory', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'ValorCatalogosFactory',
    'RemesaGuiasFactory', 'EmpresasFactory', 'RecepcionGuiasFactory', 'PlanillaGuiasFactory', 'OficinasFactory', 'RemesasFactory', 'DocumentosFactory',
    function ($scope, $timeout, TarifarioVentasFactory, $linq, blockUI, blockUIConfig, $routeParams, ValorCatalogosFactory,
        RemesaGuiasFactory, EmpresasFactory, RecepcionGuiasFactory, PlanillaGuiasFactory, OficinasFactory, RemesasFactory, DocumentosFactory) {
        //--------------------------Variables-------------------------//
        $scope.Titulo = '';
        $scope.MapaSitio = '';
        var MeapCodigo = 0;
        $scope.OPCION_MENU = OPCION_MENU_PAQUETERIA.REMESAS;


        $scope.ListadoPermisosEspecificos = []
        $scope.ListadoPermisos.forEach(function (item) {
            if (item.Padre == 470102) {
                $scope.ListadoPermisosEspecificos.push(item)
            }
        });
        $('#cmdConsultarEstados').hide();
        $('#cmdCambiarEstados').hide();
        $('#cmdEntregaOficina').hide();
        $('#cmdConsultarEntrega').hide();


        $scope.PermisoConsultarEntregas = false;
        $scope.PermisoEntregaOficina = false;
        $scope.PermisoCambiarEstados = false;
        $scope.PermisoConsultarEstados = false;
        $scope.estadoAnterior = 0
        $scope.OficinaAnterior = 0
        $scope.VarNumeroGuia = 0
        $scope.VarNUmeroiDocumentoGUia =

            $scope.ListadoPermisosEspecificos.forEach(function (item) {
                if (item.Codigo == PERMISO_CONSULTAR_ESTADOS) {
                    $scope.PermisoConsultarEstados = true

                }
                if (item.Codigo == PERMISO_ENTREGAR_OFICINA) {
                    $scope.PermisoEntregaOficina = true;
                }
                if (item.Codigo == PERMISO_CONSULTAR_ENTREGA) {
                    $scope.PermisoConsultarEntregas = true;

                } if (item.Codigo == PERMISO_CAMBIAR_ESTADO_TRAZABILIDAD) {
                    $scope.PermisoCambiarEstados = true
                }

            });


        var RemesaGesphone = false;
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.pref = '';
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {} },
            Numeracion: ''
        };
        $scope.HabilitarEliminarDocumento = false;
        $scope.ListadoEstadoGuia = [
            { Codigo: -1, Nombre: '(TODOS)' }
        ];
        $scope.ListadoCambioEstado = [
            { Codigo: 0, Nombre: '(NO APLICA)' },
            { Codigo: 6005, Nombre: 'OFICINA' },
            { Codigo: 6010, Nombre: 'DESPACHADA' }
        ];
        $scope.ModeloCambioEstado = $scope.ListadoCambioEstado[0];

        $scope.ListadoOficinas = [];
        $scope.ListadoOficinasActual = [];
        $scope.ListadoEstadoRemesaPaqueteria = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 0, Nombre: 'BORRADOR' }, { Codigo: 1, Nombre: 'DEFINITIVO' }, { Codigo: 2, Nombre: 'ANULADO' }];
        $scope.ModeloEstado = $scope.ListadoEstadoRemesaPaqueteria[0];
        $scope.ListadoFormaPago = [];
        $scope.ListadoLineaNegocioPaqueteria = [];
        //--------------------------Variables-------------------------//
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CONSULTAR GUÍAS';
        $scope.MapaSitio = [{ Nombre: 'Paqueteria' }, { Nombre: 'Documentos' }, { Nombre: 'Guías' }];
        if ($routeParams.MeapCodigo !== undefined && $routeParams.MeapCodigo !== null && $routeParams.MeapCodigo !== '' && $routeParams.MeapCodigo !== 0) {
            MeapCodigo = parseInt($routeParams.MeapCodigo);
            if (MeapCodigo > 0) {
                switch (MeapCodigo) {
                    case OPCION_MENU_PAQUETERIA.REMESAS:
                        $scope.OPCION_MENU = OPCION_MENU_PAQUETERIA.REMESAS;
                        break;
                    case OPCION_MENU_PAQUETERIA.REMESAS_GESPHONE:
                        $scope.OPCION_MENU = OPCION_MENU_PAQUETERIA.REMESAS_GESPHONE;
                        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Guías' }];
                        RemesaGesphone = true;
                        $scope.Modelo.UsuarioConsulta = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
                        break;
                }
            }
        }
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + $scope.OPCION_MENU);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------Init
        $scope.InitLoad = function () {
            //--Estado Guias
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 60 }, Estado: ESTADO_DEFINITIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                if (response.data.Datos[i].Codigo != 6000) {
                                    $scope.ListadoEstadoGuia.push(response.data.Datos[i]);
                                }
                            }
                        }
                        $scope.Modelo.EstadoRemesaPaqueteria = $scope.ListadoEstadoGuia[0];
                    }
                }, function (response) {
                });
            //--Oficinas
            var responseOficina = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, Sync: true });
            if (responseOficina.ProcesoExitoso) {
                $scope.ListadoOficinas.push({ Codigo: -1, Nombre: '(TODAS)' });
                responseOficina.Datos.forEach(function (item) {
                    $scope.ListadoOficinas.push(item);
                });
                $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
            }

            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                $scope.ListadoOficinasActual = $scope.ListadoOficinas;
                $scope.Modelo.OficinaActual = $linq.Enumerable().From($scope.ListadoOficinasActual).First('$.Codigo === -1');
            }
            else {
                $scope.ListadoOficinasActual.push({
                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
                });
                $scope.Modelo.OficinaActual = $scope.ListadoOficinasActual[0];
            }
            //--Forma de pago
            var ListaFormaPago = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS },
                Sync: true
            }).Datos;
            ListaFormaPago.splice(ListaFormaPago.findIndex(item => item.Codigo === CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NA), 1);
            $scope.ListadoFormaPago.push({ Codigo: -1, Nombre: "(TODAS)" });
            $scope.ListadoFormaPago = $scope.ListadoFormaPago.concat(ListaFormaPago);
            $scope.Modelo.Remesa.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==-1');
            //--Forma Pago
            //--Catalogo Linea Negocio Paqueteria
            var listaLineaNegocioPaqueteria = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CATALOGO_LINEA_NEGOCIO_PAQUETERIA.CODIGO },
                Sync: true
            }).Datos;
            $scope.ListadoLineaNegocioPaqueteria.push({ Codigo: -1, Nombre: "(TODAS)" });
            $scope.ListadoLineaNegocioPaqueteria = $scope.ListadoLineaNegocioPaqueteria.concat(listaLineaNegocioPaqueteria);
            $scope.Modelo.LineaNegocioPaqueteria = $linq.Enumerable().From($scope.ListadoLineaNegocioPaqueteria).First('$.Codigo ==-1');
            //--Catalogo Linea Negocio Paqueteria
            //--Obtener Parametros
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== '0') {
                $scope.Modelo.NumeroInicial = $routeParams.Numero;
                $scope.Modelo.NumeroFinal = $routeParams.Numero;
                $scope.Modelo.FechaInicial = null;
                PantallaBloqueo(Find);
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            PantallaBloqueo(Find);
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            PantallaBloqueo(Find);
        };
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        //--Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarRemesasPaqueteria/0/' + $scope.OPCION_MENU;
            }
        };
        //--Funcion Buscar
        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO && DatosRequeridos()) {
                $scope.Codigo = 0;
                PantallaBloqueo(Find);
            }
        };
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                && ($scope.Modelo.NumeroInicial === null || $scope.Modelo.NumeroInicial === undefined || $scope.Modelo.NumeroInicial === '' || $scope.Modelo.NumeroInicial === 0 || isNaN($scope.Modelo.NumeroInicial) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false;

            } else if (($scope.Modelo.NumeroInicial !== null && $scope.Modelo.NumeroInicial !== undefined && $scope.Modelo.NumeroInicial !== '' && $scope.Modelo.NumeroInicial !== 0)
                || ($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')

            ) {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }

                else {
                    if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial;
                    } else {
                        $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal;
                    }
                }
            }
            return continuar;
        }
        function Find() {
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
            $scope.Modelo.Pagina = $scope.paginaActual;
            $scope.Modelo.RegistrosPagina = 10;
            $scope.Modelo.NumeroPlanilla = -1;
            $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
            if ($scope.ModeloEstado !== undefined && $scope.ModeloEstado !== null) {
                $scope.Modelo.Estado = $scope.ModeloEstado.Codigo;
            } else {
                $scope.Modelo.Estado = -1;
            }
            var filtro = angular.copy($scope.Modelo);
            filtro.Anulado = -1;
            RemesaGuiasFactory.Consultar(filtro).
                then(function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Codigo = 0;
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoGuias = response.data.Datos;
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                        var listado = [];
                        response.data.Datos.forEach(function (item) {
                            listado.push(item);
                        });
                    }
                }, function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    ShowError(response.statusText);
                });

        }
        //--Funcion Buscar
        $scope.EliminarTarifarioVentas = function (codigo, Nombre) {
            $scope.Codigo = codigo;
            $scope.Nombre = Nombre;
            $scope.ModalErrorCompleto = '';
            $scope.ModalError = '';
            $scope.MostrarMensajeError = false;
            showModal('modalEliminarTarifarioVentas');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };

            TarifarioVentasFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se elimino la linea vehículos ' + $scope.Nombre);
                        closeModal('modalEliminarTarifarioVentas');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarTarifarioVentas');
                    $scope.ModalError = 'No se puede eliminar la linea vehículos ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });


        };
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true;
        };
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        };
        //-------Función que ejecuta el boton de vista preliminar
        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroFactura = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_GUIA_PAQUETERIA;


            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroFactura != undefined && $scope.NumeroFactura != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroFactura;
            }
        };
        $scope.EntregaOficina = function (item) {


            if (item.Estado === 1 && item.Anulado === 0 && item.EstadoGuia.Codigo == 6005 || item.EstadoGuia.Codigo == 6020 || item.EstadoGuia.Codigo == 6017) {

                document.location.href = '#!EntregaOficina/' + item.Remesa.NumeroDocumento

            } else {
                ShowError('No se puede entregar la Remesa porque su estado debe corresponder al de “Oficina”');
                console.log('no enviar', item)
            }

        }
        $scope.ImprimirEtiquetas = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroFactura = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_ETIQUETAS_GUIA;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';




        };
        function DatosRequeridosAnular() {
            var continuar = true;
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false;
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación');
            }
            return continuar;
        }
        //---Anular
        $scope.AnularDocumento = function (Numero, NumeroDocumento) {
            $scope.Numero = Numero;

            $scope.ModalErrorCompleto = '';
            $scope.NumeroDocumento = NumeroDocumento;
            $scope.ModalError = '';
            $scope.MostrarMensajeError = false;
            showModal('modalAnular');
        };
        $scope.Anular = function () {
            var aplicaplanilla = false;
            var aplicaFactura = false;
            var aplicaManifiesto = false;

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnula: $scope.ModeloCausaAnula,
                Numero: $scope.Numero
            };
            if (DatosRequeridosAnular()) {
                RemesasFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado > 0) {
                                ShowSuccess('Se anuló la remesa ' + $scope.NumeroDocumento);
                                closeModal('modalAnular', 1);
                                Find();
                            } else {
                                var mensaje = 'No se pudo anular la remesa ya que se encuentra relacionado con los siguientes documentos: ';

                                var Facturas = '\nFacturas: ';
                                var Planillas = '\nPlanillas: ';
                                var Manifiestos = '\nManifiestos: ';
                                for (var i = 0; i < response.data.Datos.DocumentosRelacionados.length; i++) {
                                    if (response.data.Datos.DocumentosRelacionados[i].FacturaVentas.Numero > 0) {
                                        Facturas += response.data.Datos.DocumentosRelacionados[i].FacturaVentas.Numero + ',';
                                        aplicaFactura = true;
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Planilla.Numero > 0) {
                                        Planillas += response.data.Datos.DocumentosRelacionados[i].Planilla.Numero + ',';
                                        aplicaplanilla = true;
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Manifiesto.Numero > 0) {
                                        Manifiestos += response.data.Datos.DocumentosRelacionados[i].Manifiesto.Numero + ',';
                                        aplicaManifiesto = true;
                                    }
                                }

                                if (aplicaplanilla) {
                                    mensaje += Planillas;
                                }
                                if (aplicaFactura) {
                                    mensaje += Facturas;
                                }
                                if (aplicaManifiesto) {
                                    mensaje += Manifiestos;
                                }
                                ShowError(mensaje);
                            }

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = '';
                        showModal('modalMensajeAnularOrdenCargue');
                        result = InternalServerError(response.statusText);
                        $scope.ModalError = 'No se puede anular la orden de servicio ' + $scope.NumeroDocumento;
                        $scope.ModalErrorCompleto = response.statusText;
                    });
            }
        };
        //---Anular

        //Cambiar estados GUIAS 
        $scope.CambiarEstadosRemesas = function (item) {

            $scope.ModificarLegalizarGuias = 0;

            $scope.MensajesErrorDetalleUnidades = [];
            $scope.ListadoEstadoGuias = RemesaGuiasFactory.ConsultarEstadosRemesa({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero
                , Sync: true
            }).Datos

            $scope.MensajesErrorDetalleUnidades.push('Al cambiar la trazabilidad  eliminara la prueba de entrega');

            console.log('llego item', item)

            $scope.NumeroGuia = item.Remesa.NumeroDocumento;
            $scope.NumeroRemesa = item.Remesa.Numero;
            $scope.EstadoGuia = item.EstadoGuia.Codigo;
            $scope.EstadoDocumento = item.Estado;
            $scope.EstadoDocumentoAnulado = item.Anulado;
            $scope.OficinaActual = item.OficinaActual.Nombre;

            for (var i = $scope.ListadoEstadoGuias.length; i >= 0; i--) {

                if ($scope.ListadoEstadoGuias[i] != undefined) {
                    if ($scope.ListadoEstadoGuias[i].EstadoRemesa.Codigo == $scope.EstadoGuia) {

                    } else {
                        $scope.estadoAnterior = $scope.ListadoEstadoGuias[i].EstadoRemesa
                        $scope.Modeloplanilla = $scope.ListadoEstadoGuias[i].DocumentoPlanilla
                        $scope.OficinaAnterior = $scope.ListadoEstadoGuias[i].Oficina
                        $scope.ObtenerPlanilla($scope.Modeloplanilla);

                        break;
                    }
                }
            }

            $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==' + $scope.OficinaAnterior.Codigo);

            showModal('ModalCambiarEstadosRemesas');



            if ($scope.EstadoGuia == 6030) {

                if (item.Remesa.FormaPago.Codigo == 4903 && item.NumeroDocumentoLegalizarRecaudo > 0) {
                    $scope.MensajesErrorDetalleUnidades.push('Al cambiar la trazabilidad se eliminara el recaudo reguistrado');
                    $scope.Forma_pago = item.Remesa.FormaPago.Codigo;

                }

                if (item.Remesa.FormaPago.Codigo != 4901 && item.NumeroDocumentoLegalizarGuias > 0) {
                    $scope.ModificarLegalizarGuias = 1
                }


                if ($scope.estadoAnterior.Codigo == 6005) {

                    $scope.Modeloplanilla = ''
                    $('#cmdOficina').show();
                    $('#cmdPlanilla').hide();
                } else if ($scope.estadoAnterior.Codigo == 6010) {

                    $scope.ModeloOficina = $scope.ListadoOficinas[0];
                    $('#cmdOficina').hide();
                    $('#cmdPlanilla').show();
                } else {
                    $('#cmdOficina').hide();
                    $('#cmdPlanilla').hide();
                }
            } else {
                $('#cmdOficina').hide();
                $('#cmdPlanilla').hide();
            }
        }


        $scope.ActualizarEstadosRemesas = function () {
            if ($scope.estadoAnterior.Codigo == 6005) {

                filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    EstadoGuia: $scope.estadoAnterior,
                    OficinaActual: $scope.ModeloOficina,
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Remesa: {
                        FormaPago: {
                            Codigo: $scope.Forma_pago,
                        },
                        Numero: $scope.NumeroRemesa,
                        NumeroDocumento: $scope.NumeroGuia,

                    },
                    ModificarLegalizarGuias: $scope.ModificarLegalizarGuias
                }

            } else {

                filtro = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    EstadoGuia: $scope.estadoAnterior,
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Planilla: {
                        Codigo: $scope.Modal.Planilla.Numero,
                        Conductores: $scope.Modal.Planilla.Vehiculo.Conductor.Codigo
                    },
                    Remesa: {
                        Numero: $scope.NumeroRemesa,
                        FormaPago: {
                            Codigo: $scope.Forma_pago,
                        },
                        NumeroDocumento: $scope.NumeroGuia,
                        Vehiculo: {
                            Codigo: $scope.Modal.Planilla.Vehiculo.Codigo,
                            Placa: $scope.Modal.Planilla.Vehiculo.Placa

                        },
                        Planilla: {
                            Conductor: { Codigo: $scope.Modal.Planilla.Vehiculo.Conductor.Codigo }
                        }


                    }

                }
            }


            if (DatosRequeridosActualizarEstado()) {

                RemesaGuiasFactory.CambiarEstadoTrazabilidadGuia(filtro).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if ($scope.estadoAnterior.Codigo == 6005) {
                                ShowSuccess('Se cambio la oficina de la Guia ')

                            } else {

                                ShowSuccess('La Guia se asocio correctamente a la planilla ' + $scope.Modeloplanilla)
                            }
                            closeModal('ModalCambiarEstadosRemesas');
                            Find();
                            $scope.EstadoGuia = '';
                        } 87757
                    }, function (response) {

                        ShowError('La gua Retornada a planilla Origen');
                        closeModal('ModalCambiarEstadosRemesas');
                    });
            }

        }


        function DatosRequeridosActualizarEstado() {

            var continuar = true;
            console.log('Cambio Estado', $scope.ModeloCambioEstado)
            console.log('  Estado Guia ', $scope.EstadoGuia)

            if ($scope.ModeloCambioEstado.Codigo == $scope.EstadoGuia) {

                ShowError('La Guia ya se encuentra en estado ' + $scope.ModeloCambioEstado.Nombre)
                continuar = false;
            }

            return continuar;

        }



        $scope.ObtenerPlanilla = function (item) {
            if (item !== undefined && item !== '' && item > 0) {

                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: item,
                    TipoDocumento: 135
                };

                blockUI.delay = 1000;
                PlanillaGuiasFactory.Obtener(filtros).
                    then(function (response) {
                        console.log('llego planilla', response)
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Planilla != null) {
                                if (response.data.Datos.Planilla.Anulado == 0 && response.data.Datos.Planilla.Estado.Codigo == 1) {
                                    if (response.data.Datos.Planilla.NumeroCumplido == 0 && response.data.Datos.Planilla.NumeroLiquidacion == 0) {
                                        $scope.esObtener = true
                                        $scope.ListadoGuiaGuardadas = []
                                        $scope.Modal = response.data.Datos
                                        $scope.Modal.Planilla.FechaSalida = new Date($scope.Modal.Planilla.FechaHoraSalida)

                                        $scope.Modal.Planilla.HoraSalida = $scope.Modal.Planilla.FechaSalida.getHours().toString()


                                        if ($scope.Modal.Planilla.FechaSalida.getMinutes().toString().length == 1) {
                                            $scope.Modal.Planilla.HoraSalida += ':0' + $scope.Modal.Planilla.FechaSalida.getMinutes().toString()
                                        } else {
                                            $scope.Modal.Planilla.HoraSalida += ':' + $scope.Modal.Planilla.FechaSalida.getMinutes().toString()
                                        }
                                        $scope.Modal.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                                        $scope.Modal.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                        $scope.Modal.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                        $scope.Modal.Planilla.Fecha = new Date($scope.Modal.Planilla.Fecha)
                                        var conductor = $scope.Modal.Planilla.Vehiculo.Conductor
                                        try {
                                            $scope.Modal.Planilla.Vehiculo = $scope.CargarVehiculos($scope.Modal.Planilla.Vehiculo.Codigo);
                                            $scope.Modal.Planilla.Vehiculo.Conductor = $scope.CargarTercero(conductor.Codigo)
                                            $scope.Modal.Planilla.Vehiculo.Tenedor = $scope.CargarTercero($scope.Modal.Planilla.Vehiculo.Tenedor.Codigo)
                                        } catch (e) {
                                        }

                                    } else {
                                        ShowError('La planilla ingresada ya se encuentra liquidada');
                                        $scope.Planilla = ''
                                    }
                                } else {
                                    ShowError('La planilla ingresada se encuentra en estado borrador o fue anulada');
                                    $scope.Planilla = ''
                                }
                            } else {
                                ShowError('No se encontró una planilla válida con el número ingresado');
                                $scope.Planilla = ''
                            }
                        }
                        else {
                            ShowError('No se encontró una planilla válida con el número ingresado');
                            $scope.Planilla = ''
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        $scope.Planilla = ''
                    });
            } else {
                ShowError('Debe ingresar el número de la planilla')
            }

        }

        $scope.ConsultarEstadosRemesas = function (item) {
            $scope.ListadoEstadoGuias = [];
            $scope.ListadoPlanilla = [];
            $scope.NumeroGuia = item.Remesa.NumeroDocumento;
            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero
            };
            RemesaGuiasFactory.ConsultarEstadosRemesa(Filtro).
                then(function (response) {
                    console.log('llego', response.data.Datos)
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoEstadoGuias = response.data.Datos;
                            for (var i = 0; i < $scope.ListadoEstadoGuias.length; i++) {
                                if ($scope.ListadoEstadoGuias[i].EstadoRemesa.Codigo == 6005) {
                                    $scope.ListadoEstadoGuias[i].DocumentoPlanilla = ''
                                    $scope.ListadoEstadoGuias[i].Vehiculo = ''
                                    $scope.ListadoEstadoGuias[i].COD_vehiculo = ''
                                    $scope.ListadoEstadoGuias[i].Conductor = ''
                                    $scope.ListadoEstadoGuias[i].EstadoPlanilla = ''

                                }

                            }

                            showModal('ModalEstadosRemesas');
                        } else {
                            ShowError('No se encontro registro de estados de la remesa seleccionada');
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        //Consultar Bitacora Guias 
        $scope.ConsultarBitacoraGuias = function (item) {
            $scope.ListadoBitacoraGuias = [];
            $scope.VarNumeroGuia = item.Remesa.Numero
            $scope.VarNUmeroiDocumentoGUia = item.Remesa.NumeroDocumento
            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero
            };
            RemesaGuiasFactory.ConsultarBitacoraGuias(Filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.Numero = item.Remesa.NumeroDocumento
                            showModal('ModalBitacoraGuias');
                            $scope.ListadoBitacoraGuias = response.data.Datos;
                        } else {
                            $scope.Numero = item.Remesa.NumeroDocumento
                            //ShowError('No se encontro registro de observaciones para la remesa: ' + $scope.Numero);
                            showModal('ModalBitacoraGuias');
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        //Guardar Bitacora Guias
        $scope.GuardarDetalleBitacoraGuias = function () {
            filtros = {
                Numero: $scope.VarNumeroGuia,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Observaciones: $scope.Modelo.Observaciones
            }

            RemesaGuiasFactory.GuardarBitacoraGuias(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        //$scope.ConsultarBitacoraGuias({ Numero: $scope.VarNumeroGuia})
                        ShowSuccess('Se guardó la novedad en la guia: ' + $scope.VarNUmeroiDocumentoGUia + ' con éxito');
                        $scope.Modelo.Observaciones = undefined
                        closeModal('ModalBitacoraGuias');
                    } else {
                        ShowError(response.statusText)
                    }
                });
        }

        //--Control Entregas
        $scope.ConsultarControlEntregas = function (item) {
            $scope.DocumentoCumplido = {
                Nombre: '',
                NombreDocumento: '',
                ExtensionDocumento: ''
            };

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero
            };
            RemesaGuiasFactory.ObtenerControlEntregas(filtros).
                then(function (response) {
                    //console.log(response)
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.Remesa.NombreRecibe != null && response.data.Datos.Remesa.NumeroIdentificacionRecibe != null && response.data.Datos.Remesa.TelefonoRecibe != null) {
                            $scope.FotosEntrega = [];
                            $scope.FotosDevolucion = [];
                            //$scope.FotoControlEntrega = response.data.Datos.Remesa.Fotografia;
                            //$scope.FotoControlEntrega.FotoCargada = 'data:' + response.data.Datos.Remesa.Fotografia.TipoFoto + ';base64,' + response.data.Datos.Remesa.Fotografia.FotoBits;
                            $scope.FotosControlEntrega = response.data.Datos.Remesa.Fotografias;
                            $scope.FotosControlEntrega.forEach(foto => {
                                foto.FotoCargada = 'data:' + foto.TipoFoto + ';base64,' + foto.FotoBits;
                                if (foto.EntregaDevolucion == 1) {
                                    $scope.FotosDevolucion.push(foto);
                                } else {
                                    $scope.FotosEntrega.push(foto);
                                }
                            });
                            $scope.ControlEntregaRemesa = response.data.Datos.Remesa;


                            $scope.ControlEntregaRemesa.UsuarioEntrega = response.data.Datos.UsuarioEntrega;
                            $scope.ControlEntregaRemesa.OficinaEntrega = response.data.Datos.OficinaEntrega.Nombre;
                            $scope.ControlEntregaRemesa.MostrarEntregaOFicina = response.data.Datos.OficinaEntrega.Codigo > 0 ? true : false;
                            //console.log('mostrar',$scope.ControlEntregaRemesa.MostrarEntregaOFicina )
                            $scope.ControlEntregaRemesa.Latitud = response.data.Datos.Latitud;
                            $scope.ControlEntregaRemesa.Longitud = response.data.Datos.Longitud;
                            if (response.data.Datos.DocumentosCumplidoRemsa != undefined) {

                                for (var i = 0; i < response.data.Datos.DocumentosCumplidoRemsa.length; i++) {
                                    var Documento = response.data.Datos.DocumentosCumplidoRemsa[i];
                                    $scope.DocumentoCumplido = {
                                        Nombre: Documento.NombreDocumento + "." + Documento.ExtensionDocumento,
                                        Numero: $scope.ControlEntregaRemesa.Numero,
                                        NombreDocumento: Documento.NombreDocumento,
                                        ExtensionDocumento: Documento.ExtensionDocumento,
                                        Id: Documento.Id,
                                        Temp: false
                                    };
                                    $scope.ModificaArchivo = false;
                                    break;
                                }
                            }
                            if (response.data.Datos.DocumentosCumplidoRemsa.length > 0) {
                                $scope.HabilitarEliminarDocumento = true;
                            } else {
                                $scope.HabilitarEliminarDocumento = false;
                            }
                            $scope.ControlEntregaRemesa.FirmaImg = "data:image/png;base64," + $scope.ControlEntregaRemesa.Firma;
                            showModal('modalControlEntregas');
                        }
                        else {
                            ShowError('El documento no cuenta con un control de entregas');
                        }
                    }
                    else {
                        ShowError('El documento no cuenta con un control de entregas');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        $scope.VerFotosEntrega = function () {
            showModal('modalFotosEntregas');
        };
        $scope.VerFotosDevolucion = function () {
            showModal('modalFotosDevolucion');
        };
        //--Control Entregas
        //------Gestion Adjunto Documento Cumplido
        $scope.ModificaArchivo = false;
        var regExt = /(?:\.([^.]+))?$/;
        $scope.DocumentoCumplido = {
            Nombre: '',
            NombreDocumento: '',
            ExtensionDocumento: ''
        };
        $scope.CadenaFormatos = ".pdf";
        $scope.CargarArchivo = function (element) {
            var continuar = true;
            if (element.files.length > 0) {
                //$scope.DocumentoCumplido = angular.element(this.DocumentoCumplido)[0];

                if (element === undefined) {
                    ShowError("Archivo invalido, por favor verifique el formato del archivo  /nFormatos permitidos: 0");
                    continuar = false;
                }
                if (continuar == true) {
                    if (element.files[0].size >= 3000000) //3 MB
                    {
                        ShowError("El archivo " + element.files[0].name + " supera los " + "3 MB" + ", intente con otro archivo", false);
                        continuar = false;
                    }
                    var Formatos = $scope.CadenaFormatos.split(',');
                    var cont = 0;
                    var ExtencionArchivo = regExt.exec(element.files[0].name)[1];
                    for (var i = 0; i < Formatos.length; i++) {
                        Formatos[i] = Formatos[i].replace(' ', '');
                        //var Extencion = element.files[0].name.split('.');
                        if ('.' + (ExtencionArchivo.toUpperCase()) == Formatos[i].toUpperCase()) {
                            cont++;
                        }
                    }
                    if (cont == 0) {
                        ShowError("Archivo invalido, por favor verifique el formato del archivo.  Formatos permitidos: (" + $scope.CadenaFormatos + ")");
                        continuar = false;
                    }
                }

                if (continuar == true) {
                    if ($scope.DocumentoCumplido.Archivo != undefined && $scope.DocumentoCumplido.Archivo != '' && $scope.DocumentoCumplido.Archivo != null) {
                        $scope.DocumentoCumplido.ArchivoTemporal = element.files[0];
                        showModal('modalConfirmacionRemplazarDocumentoCumplido');
                    } else {
                        var reader = new FileReader();
                        $scope.DocumentoCumplido.ArchivoCargado = element.files[0];
                        reader.onload = AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }
            }
        };
        $scope.RemplazarDocumentoCumplido = function () {
            var reader = new FileReader();
            $scope.DocumentoCumplido.ArchivoCargado = $scope.DocumentoCumplido.ArchivoTemporal;
            reader.onload = AsignarArchivo;
            reader.readAsDataURL($scope.DocumentoCumplido.ArchivoCargado);
            closeModal('modalConfirmacionRemplazarDocumentoCumplido');
        };
        function AsignarArchivo(e) {
            $scope.$apply(function () {
                $scope.DocumentoCumplido.Numero = $scope.ControlEntregaRemesa.Numero;//--Numero Remesa
                $scope.DocumentoCumplido.Archivo = e.target.result.replace('data:' + $scope.DocumentoCumplido.ArchivoCargado.type + ';base64,', '');
                $scope.DocumentoCumplido.Tipo = $scope.DocumentoCumplido.ArchivoCargado.type;
                $scope.DocumentoCumplido.Nombre = $scope.DocumentoCumplido.ArchivoCargado.name;
                $scope.DocumentoCumplido.NombreDocumento = $scope.DocumentoCumplido.ArchivoCargado.name.substr(0, $scope.DocumentoCumplido.ArchivoCargado.name.lastIndexOf('.'));
                $scope.DocumentoCumplido.ExtensionDocumento = regExt.exec($scope.DocumentoCumplido.ArchivoCargado.name)[1];
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Cargando Documento...");
                $timeout(function () {
                    blockUI.message("Cargando Documento...");
                    InsertarDocumentoCumplido();
                }, 100);
                //Bloqueo Pantalla
            });
        }
        function InsertarDocumentoCumplido() {
            // Guardar Archivo temporal
            var Documento = {};
            Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.ControlEntregaRemesa.Numero,
                Nombre: $scope.DocumentoCumplido.NombreDocumento,
                Archivo: $scope.DocumentoCumplido.Archivo,
                Tipo: $scope.DocumentoCumplido.Tipo,
                Extension: $scope.DocumentoCumplido.ExtensionDocumento,
                CumplidoRemesa: true,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.InsertarTemporal(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {
                if (ResponseDocu.Datos > 0) {
                    $scope.DocumentoCumplido.Id = ResponseDocu.Datos;
                    $scope.DocumentoCumplido.Temp = true;
                    $scope.ModificaArchivo = true;
                    ShowSuccess('Se cargó el documento adjunto "' + $scope.DocumentoCumplido.NombreDocumento + '"');
                }
                else {
                    ShowError(ResponseDocu.MensajeOperacion);
                }
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
        }
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (item) {
            if (item.Id != undefined && item.Id != '' && item.Id != null) {
                if (item.Temp) {
                    window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=CumplidoRemesa&ID=' + item.Id + '&Temp=1' + '&ENRE_Numero=' + item.Numero);
                }
                else {
                    window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=CumplidoRemesa&ID=' + item.Id + '&ENRE_Numero=' + item.Numero + '&Temp=0');
                }
            }
            else {
                ShowError("Se debe seleccionar un archivo");
            }
        };
        $scope.GuardarArchivoCumplido = function (item) {
            if (item.Id != undefined && item.Id != '' && item.Id != null) {
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Guardando Documento...");
                $timeout(function () {
                    blockUI.message("Guardando Documento...");
                    TrasladarDocumentoCumplido(item);
                }, 100);
                //Bloqueo Pantalla
            }
        };
        $scope.EliminarArchivo = function (item) {
            Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.EliminarDocumentoCumplidoRemesa(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {

                ShowSuccess('Se eliminó el documento adjunto "' + $scope.DocumentoCumplido.NombreDocumento + '"');
                $scope.DocumentoCumplido.Nombre = '';
                $scope.HabilitarEliminarDocumento = false;
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
        }
        function TrasladarDocumentoCumplido(item) {
            var Documento = {};
            Documento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                Sync: true
            };
            var ResponseDocu = DocumentosFactory.InsertarDocumentosCumplidoRemesa(Documento);
            if (ResponseDocu.ProcesoExitoso == true) {
                if (ResponseDocu.Datos > 0) {
                    $scope.DocumentoCumplido.Id = ResponseDocu.Datos;
                    $scope.DocumentoCumplido.Temp = false;
                    $scope.ModificaArchivo = false;
                    ShowSuccess('Se guardo el documento adjunto "' + $scope.DocumentoCumplido.NombreDocumento + '"');
                }
                else {
                    ShowError(ResponseDocu.MensajeOperacion);
                }
            }
            else {
                ShowError(ResponseDocu.MensajeOperacion);
            }
            blockUI.stop();
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
        }
        //------Gestion Adjunto Documento Cumplido
        //------Mapa Google
        $scope.AbrirMapa = function (lat, lng) {
            showModal('modalMostrarMapa');
            iniciarMapaGoogle(lat, lng);
        };
        function iniciarMapaGoogle(lat, lng) {
            var marker = [];
            var map, infoWindow;
            $scope.MapaLatitud = lat != '' ? lat : 0;
            $scope.MapaLongitud = lng != '' ? lng : 0;
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    };
                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.BOUNCE,
                    title: title
                    //icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };
                markers = new window.google.maps.Marker(markerOptions);
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input 
                    $scope.MapaLatitud = this.getPosition().lat();
                    $scope.MapaLongitud = this.getPosition().lng();
                });
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                if ($scope.MapaLatitud != undefined && $scope.MapaLongitud != undefined && $scope.MapaLatitud != 0 && $scope.MapaLongitud != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.MapaLatitud, $scope.MapaLongitud), 'Posición del usuario', 'Destino');
                } else {
                    $scope.MapaLatitud = posicion.coords.latitude;
                    $scope.MapaLongitud = posicion.coords.longitude;
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }
        $scope.AsignarPosicion = function () {
            $scope.Modelo.Remesa.Latitud = $scope.MapaLatitud;
            $scope.Modelo.Remesa.Longitud = $scope.MapaLongitud;
            closeModal('modalMostrarMapa');
        };
        //------Mapa Google
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };
        //----------------------------Funciones Generales---------------------------------//
    }]);