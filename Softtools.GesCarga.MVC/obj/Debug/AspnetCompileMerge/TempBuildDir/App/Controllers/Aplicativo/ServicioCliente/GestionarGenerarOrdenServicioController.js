﻿SofttoolsApp.controller("GestionarGenerarOrdenServicioCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'SitiosTerceroClienteFactory', 'GestionDocumentalDocumentosFactory', 'OficinasFactory', 'GestionDocumentosFactory', 'TercerosFactory', 'MonedasFactory', 'TarifarioVentasFactory', 'TipoLineaNegocioTransportesFactory', 'LineaNegocioTransportesFactory', 'CiudadesFactory', 'RutasFactory', 'TipoTarifaTransportesFactory', 'TarifaTransportesFactory', 'ProductoTransportadosFactory', 'ValorCatalogosFactory', 'EncabezadoSolicitudOrdenServiciosFactory', 'ConceptoVentasFactory', 'PuertosPaisesFactory', 'PaisesFactory', 'blockUIConfig', 'UnidadEmpaqueFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, SitiosTerceroClienteFactory, GestionDocumentalDocumentosFactory, OficinasFactory, GestionDocumentosFactory, TercerosFactory, MonedasFactory, TarifarioVentasFactory, TipoLineaNegocioTransportesFactory, LineaNegocioTransportesFactory, CiudadesFactory, RutasFactory, TipoTarifaTransportesFactory, TarifaTransportesFactory, ProductoTransportadosFactory, ValorCatalogosFactory, EncabezadoSolicitudOrdenServiciosFactory, ConceptoVentasFactory, PuertosPaisesFactory, PaisesFactory, blockUIConfig, UnidadEmpaqueFactory) {

        $scope.MapaSitio = [{ Nombre: 'Proces' }, { Nombre: 'Generar Orden Servicio' }, { Nombre: 'Gestionar' }];
        $scope.Titulo = "GENERAR ORDEN SERVICIO";
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ORDEN_SERVICIO); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.MensajesErrorPoliza = [];
        $scope.ListadoLineaNegocioTarifario = [];
        $scope.MensajesErrorConceptos = [];
        $scope.ListadoPermisosEspecificos = []
        $scope.ListadoCiudades = [];
        $scope.ListadoTipoLineaNegocioTarifario = [];
        $scope.ListadoComerciales = [];
        $scope.ListaCiudadCargue = [];
        $scope.ListadoClientes = []
        $scope.ListadoNavieras = [];
        $scope.ListadoNavieras = [];
        $scope.ListaConceptosOrden = [];
        $scope.ListadoAseguradoras = [];
        $scope.ListaPolizas = [];
        $scope.ListadoPuertosPaises = [];
        $scope.ListadoModoTransporte = [];
        $scope.ListadoCliente = [];
        $scope.ListadoRutas = [];
        $scope.ListadoOficinas = [];
        $scope.ListadoPuertosDestino = [];
        $scope.ListadoPuertosOrigen = [];

        $scope.ListadoTipoTarifaTarifario = [];
        $scope.ListadoRutasTarifario = [];
        $scope.ListadoTarifaTarifario = [];
        $scope.ListadoConceptos = [];
        $scope.MensajesError = [];
        $scope.DeshabilitarLineaNegocio = false
        $scope.DeshabilitarTipoLineaNegocio = false
        $scope.DeshabilitarTarifas = true
        $scope.DeshabilitarTipoTarifas = true
        $scope.DeshabilitarRutas = true
        $scope.DeshabilitarPuertoOrigen = true
        $scope.DeshabilitarPuertoDestino = true
        $scope.HabilitarImportacionExportacion = false
        $scope.DeshabilitarSitiosClienteCargue = true
        $scope.DeshabilitarSitiosClienteDescargue = true
        $scope.DeshabilitarDocumentos = false
        $scope.HabilitarPoliza = false
        $scope.ListadoDestinatarios = [];
        $scope.ListaDetalleOrden = [];
        $scope.ListaDocumentos = []

        $scope.Modal = {}
        $scope.ModalConcepto = {}
        $scope.ModalPoliza = {}
        $scope.ModalImportacion = {}
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            TipoDocumento: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            Numero: 0,
            Fecha: new Date()
        }
        //Variables documentos
        $scope.Numero = 0;
        $scope.CodigoDocumento = 0;
        $scope.TipoImagen = '';[]
        $scope.DetalleDocumentos = [];
        //----------------------------
        $scope.ListadoEstados = [
            { Nombre: "BORRADOR", Codigo: 0 },
            { Nombre: "DEFINITIVO", Codigo: 1 }
        ]

        $scope.Estado = $scope.ListadoEstados[0]


        $scope.VisualizacionCamposProgramacion = false
        if ($scope.Sesion.UsuarioAutenticado.ManejoTarifasProgramacion) {
            $scope.VisualizacionCamposProgramacion = true
        }
        //Verificacion controles
        $scope.ctr = {
            SedeFacturacion: { Visible: true },
            Naviera: { Visible: true },
            FechaDTA: { Visible: true },
            PaisOrigen: { Visible: true },
            PuertoOrigen: { Visible: true },
            ContactoOrigen: { Visible: true },
            PaisDestino: { Visible: true },
            PuertoDestino: { Visible: true },
            ContactoDestino: { Visible: true },
            DiasLibresContenedor: { Visible: true },
            FechaCutOffDocumental: { Visible: true },
            FechaCutOffFisico: { Visible: true },
            LiberacionAutomaticaFletes: { Visible: true },
            ExoneracionPagoDeposito: { Visible: true },
            ExoneracionPagoDropOFF: { Visible: true },
            ClientePagaFleteMaritimoRuta: { Visible: true },
            ClientePagoEmisionManejo: { Visible: true },
            ClientePagaLiquidacionComodato: { Visible: true },
        }
        if ($scope.Sesion.UsuarioAutenticado.ListadoControles.length > 0) {
            for (var i = 0; i < $scope.Sesion.UsuarioAutenticado.ListadoControles.length; i++) {
                var control = $scope.Sesion.UsuarioAutenticado.ListadoControles[i]
                if (control.Codigo == Control_SedeFacturacion) {
                    $scope.ctr.SedeFacturacion.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_Naviera) {
                    $scope.ctr.Naviera.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_FechaDTA) {
                    $scope.ctr.FechaDTA.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_PaisOrigen) {
                    $scope.ctr.PaisOrigen.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_PuertoOrigen) {
                    $scope.ctr.PuertoOrigen.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_ContactoOrigen) {
                    $scope.ctr.ContactoOrigen.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_PaisDestino) {
                    $scope.ctr.PaisDestino.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_PuertoDestino) {
                    $scope.ctr.PuertoDestino.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_ContactoDestino) {
                    $scope.ctr.ContactoDestino.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_DiasLibresContenedor) {
                    $scope.ctr.DiasLibresContenedor.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_FechaCutOffDocumental) {
                    $scope.ctr.FechaCutOffDocumental.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_FechaCutOffFisico) {
                    $scope.ctr.FechaCutOffFisico.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_LiberacionAutomaticaFletes) {
                    $scope.ctr.LiberacionAutomaticaFletes.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_ExoneracionPagoDeposito) {
                    $scope.ctr.ExoneracionPagoDeposito.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_ExoneracionPagoDropOFF) {
                    $scope.ctr.ExoneracionPagoDropOFF.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_ClientePagaFleteMaritimoRuta) {
                    $scope.ctr.ClientePagaFleteMaritimoRuta.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_ClientePagoEmisionManejo) {
                    $scope.ctr.ClientePagoEmisionManejo.Visible = control.Visible > 0 ? true : false;
                }
                if (control.Codigo == Control_ClientePagaLiquidacionComodato) {
                    $scope.ctr.ClientePagaLiquidacionComodato.Visible = control.Visible > 0 ? true : false;
                }
            }
        }


        //-------------------------------------------------------------Carga De Informacion Necesaria para listas y datos-----------------------------------------------------//
        var DocumentosCarga = false;
        var LineaNegocioCarga = false;
        var OficinaCarga = false;
        var TipoLineaNegocioCarga = false;
        var PuertosPaisesCarga = false;
        var PaisesCarga = false;
        var ModoTransporteCarga = false;
        var ConceptoVentaCarga = false;
        var ClientesCarga = false;
        var ComercialesCarga = false;
        var DestinatariosCarga = false;
        var CiudadCarga = false;
        var AseguradoraCarga = false;
        var RutasCarga = false;
        var TipoTarifaCarga = false;
        var TarifaCarga = false;
        var ProductoTranportadoCarga = false;
        var ModoTransporteCarga = false;
        var TipoPolizaCarga = false;
        var HorarioPolizaCarga = false;
        var TimeOutCarga;
        /* Obtener parametros*/
        if (parseInt($routeParams.Numero) > CERO) {
            $scope.Modelo.Numero = $routeParams.Numero;
            $scope.Titulo = 'CONSULTAR SOLICITUD ORDEN SERVICIO';
            if (parseInt($routeParams.Generar) >= 1) {
                $scope.Titulo = 'GENERAR ORDEN SERVICIO';
                $scope.Deshabilitar = false;
                $scope.DeshabilitarActualizar = false
                $scope.DeshabilitarTipoLineaNegocio = false
                $scope.DeshabilitarLineaNegocio = false
                $scope.DeshabilitarPuertoDestino = false
                $scope.DeshabilitarPuertoOrigen = false
                $scope.DeshabilitarDocumentos = false
            }
            ValCargaFactorys();
        } else {
            $scope.DeshabilitarLineaNegocio = false
        }
        //-- Cargar combo de sitios cargue y descargue --//
        $scope.CargarDireccionesClienteCargue = function (cliente, ciudad) {
            if (cliente.Codigo > 0 && ciudad.Codigo > 0) {
                //showModal('ModalDirecciones')
                SitiosTerceroClienteFactory.ConsultarOrdenServicio({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Cliente: { Codigo: cliente.Codigo }, CiudadCargue: { Codigo: ciudad.Codigo } }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoSitiosCargue = [];
                                response.data.Datos.forEach(function (item) {
                                    $scope.ListadoSitiosCargue.push(item);
                                });
                                $scope.DeshabilitarSitiosClienteCargue = false
                            } else {
                                $scope.DeshabilitarSitiosClienteCargue = true
                            }
                        } else {
                            $scope.DeshabilitarSitiosClienteCargue = true
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        $scope.ListadoSitiosCargueAlterno = []
        $scope.AutocompleteDireccionesClienteCargue = function (value, cliente, ciudad) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i] = Response.Datos[i].DireccionSitio + ' (' + Response.Datos[i].SitioCliente.Nombre + ')'
                    }
                    $scope.ListadoSitiosCargueAlterno = ValidarListadoAutocompleteAlterno(Response.Datos, $scope.ListadoSitiosCargueAlterno)
                }
            }
            return $scope.ListadoSitiosCargueAlterno
        }
        $scope.ListadoSitiosCargueAlterno2 = []
        $scope.AutocompleteSitiosClienteCargue = function (value, cliente, ciudad) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                    }
                    $scope.ListadoSitiosCargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosCargueAlterno2)
                }
            }
            return $scope.ListadoSitiosCargueAlterno2
        }
        $scope.ObtenerRuta = function () {
            $scope.Modelo.Ruta = ''
            $scope.ListadoTarifaTarifario = []
            var cont = 0
            if ($scope.ListadoRutasTarifario.length > 0) {
                for (var i = 0; i < $scope.ListadoRutasTarifario.length; i++) {
                    if ($scope.ListadoRutasTarifario[i].CiudadOrigen.Codigo == $scope.Modelo.CiudadCargue.Codigo && $scope.ListadoRutasTarifario[i].CiudadDestino.Codigo == $scope.Modelo.CiudadDescargue.Codigo) {
                        $scope.Modelo.Ruta = $scope.ListadoRutasTarifario[i]
                        $scope.CargarTarifasViaje($scope.Modelo.Ruta)
                        cont++
                        break;
                    }
                }
            }
            if (cont == 0 && $scope.Modelo.CiudadCargue.Codigo > 0 && $scope.Modelo.CiudadDescargue.Codigo > 0) {
                ShowError('No se encontraron rutas en el tarifario del clinete con las condiciones especificadas')
            }
        }
        $scope.AutocompleteRutas = function () {
            $scope.ListadoAutocompleteRutas = []
            /*Cargar Autocomplete de propietario*/
            if ($scope.ListadoRutasTarifario.length > 0) {
                for (var i = 0; i < $scope.ListadoRutasTarifario.length; i++) {
                    if ($scope.ListadoRutasTarifario[i].CiudadOrigen.Codigo == $scope.Modelo.CiudadCargue.Codigo && $scope.ListadoRutasTarifario[i].CiudadDestino.Codigo == $scope.Modelo.CiudadDescargue.Codigo) {
                        $scope.ListadoAutocompleteRutas.push($scope.ListadoRutasTarifario[i])
                    }
                }
            }
            return $scope.ListadoAutocompleteRutas
        }
        $scope.ListadoSitiosDescargue = []
        $scope.AutocompleteSitiosClienteDescargue = function (value, cliente, ciudad) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                    }
                    $scope.ListadoSitiosDescargue = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosDescargue)
                }
            }
            return $scope.ListadoSitiosDescargue
        }
        //$scope.AutocompleteDireccionesClienteCargue = function (cliente, ciudad) {
        //    if (cliente.Codigo > 0 && ciudad.Codigo > 0) {
        //        //showModal('ModalDirecciones')
        //        SitiosTerceroClienteFactory.ConsultarOrdenServicio({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Cliente: { Codigo: cliente.Codigo }, CiudadCargue: { Codigo: ciudad.Codigo } }).
        //            then(function (response) {
        //                if (response.data.ProcesoExitoso === true) {
        //                    if (response.data.Datos.length > 0) {
        //                        $scope.ListadoSitiosCargue = [];
        //                        response.data.Datos.forEach(function (item) {
        //                            $scope.ListadoSitiosCargue.push(item);
        //                        });
        //                        $scope.DeshabilitarSitiosClienteCargue = false
        //                    } else {
        //                        $scope.DeshabilitarSitiosClienteCargue = true
        //                    }
        //                } else {
        //                    $scope.DeshabilitarSitiosClienteCargue = true
        //                }
        //            }, function (response) {
        //                ShowError(response.statusText);
        //            });
        //    }
        //}
        //-- Cargar combo de sitios cargue y descargue -//
        $scope.CargarDireccionesClienteDestino = function (cliente, ciudad) {
            SitiosTerceroClienteFactory.ConsultarOrdenServicio({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Cliente: { Codigo: cliente.Codigo }, CiudadCargue: { Codigo: ciudad.Codigo } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoSitiosDescargue = [];
                            response.data.Datos.forEach(function (item) {
                                $scope.ListadoSitiosDescargue.push(item);
                            });
                            $scope.DeshabilitarSitiosClienteDescargue = false
                        } else {
                            $scope.DeshabilitarSitiosClienteDescargue = true
                        }
                    } else {
                        $scope.DeshabilitarSitiosClienteDescargue = true
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        //-- Cargar campo ciudad oficina --//
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    OficinaCarga = true;
                    $scope.ListadoOficinas = response.data.Datos;
                    if ($scope.CodigoOficina > 0) {
                        try {
                            $scope.Modelo.OficinaDespacha = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + $scope.CodigoOficina);

                        } catch (e) {

                        }
                    } else {
                        $scope.Modelo.OficinaDespacha = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo de tipo vehiculo*/
        $scope.ListadoTipoVehiculo = [];
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoVehiculo = [];
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos; i++) {
                            if (response.data.Datos[i].Codigo !== 2200) {
                                $scope.ListadoTipoVehiculo.push(response.data.Datos[i])
                            }
                        }
                        $scope.ListadoTipoVehiculo = response.data.Datos;
                        if ($scope.CodigoTipoVehiculo > 0) {
                            $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == ' + $scope.CodigoTipoVehiculo);
                        }
                    }
                    else {
                        $scope.ListadoTipoVehiculo = []
                    }
                }
            }, function (response) {
            });
        //-- Cargar el combo de documentos --//
        GestionDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, EntidadDocumento: { Codigo: 4 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    DocumentosCarga = true;
                    if (response.data.Datos.length > 0) {
                        $scope.ListaDocumentos = response.data.Datos
                        $scope.ListaDocumentos.forEach(function (item) {
                            item.CodigosDocumentos = 0;
                        })
                    }
                }
            }, function (response) {
            });

        $scope.ListadoUnidadEmpaque = [];
        UnidadEmpaqueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoUnidadEmpaque = response.data.Datos;
                        if ($scope.CodigoUnidadEmpaque !== undefined && $scope.CodigoUnidadEmpaque !== '' && $scope.CodigoUnidadEmpaque !== null) {
                            $scope.Modelo.UnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo ==' + $scope.CodigoUnidadEmpaque);
                        }
                    }
                    else {
                        $scope.ListadoUnidadEmpaque = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        //-- Cargar el combo de LineaNegocioTransportes --//
        LineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    LineaNegocioCarga = true;
                    $scope.ListadoLineaNegocioCarga = [];
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo !== CODIGO_LINEA_NEGOCIO_TRANSPORTE_PAQUETERIA) {
                            $scope.ListadoLineaNegocioCarga.push(item)
                        }
                    })

                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //-- Cargar el combo de TipoLineaNegocioTransportes --//
        TipoLineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    TipoLineaNegocioCarga = true;
                    $scope.ListadoTipoLineaNegocioCarga = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoLineaNegocioCarga = response.data.Datos;
                    }
                }
            }, function (response) {
            });
        //-- Cargar el combo de PuertosPaises --//
        PuertosPaisesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    PuertosPaisesCarga = true;
                    $scope.ListadoPuertosPaises = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoPuertosPaises = response.data.Datos;
                    }
                }
            }, function (response) {
            });
        //-- Cambiar la direccion del combo cargue --//
        $scope.CargarDireccionCargue = function (direccioncargue) {
            $scope.Modelo.DireccionCargue = direccioncargue.DireccionSitio
            $scope.Modelo.TelefonoCargue = direccioncargue.Telefono
            $scope.Modelo.ContactoCargue = direccioncargue.Contacto
            $scope.Modal.DireccionCargue = direccioncargue.DireccionSitio
            $scope.Modal.TelefonoCargue = direccioncargue.Telefono
            $scope.Modal.ContactoCargue = direccioncargue.Contacto
        }
        //-- Cambiar la direccion del combo destino --//
        $scope.CargarDireccionDestino = function (direcciondestino) {
            $scope.Modelo.DireccionDestino = direcciondestino.DireccionSitio
            $scope.Modelo.TelefonoDestino = direcciondestino.Telefono
            $scope.Modelo.ContactoDestino = direcciondestino.Contacto
            $scope.Modal.DireccionDestino = direcciondestino.DireccionSitio
            $scope.Modal.TelefonoDestino = direcciondestino.Telefono
            $scope.Modal.ContactoDestino = direcciondestino.Contacto
        }

        $scope.ValidarPuertoPaisOrigen = function (Pais) {
            $scope.ListadoPuertosOrigen = [];
            if (Pais.Codigo !== undefined && Pais.Codigo !== 0 && Pais.Codigo !== null) {
                $scope.ListadoPuertosPaises.forEach(function (item) {
                    if (item.Pais.Codigo == Pais.Codigo) {
                        $scope.ListadoPuertosOrigen.push(item)
                    }
                })
                if ($scope.ListadoPuertosOrigen.length > 0) {
                    $scope.DeshabilitarPuertoOrigen = false
                    $scope.ModalImportacion.PaisOrigen = Pais
                }
                else {
                    $scope.ListadoPuertosOrigen = [];
                    $scope.DeshabilitarPuertoOrigen = true
                }
            }
        }
        $scope.ValidarPuertoPaisDestino = function (Pais) {
            $scope.ListadoPuertosDestino = [];
            if (Pais.Codigo !== undefined && Pais.Codigo !== 0 && Pais.Codigo !== null) {
                $scope.ListadoPuertosPaises.forEach(function (item) {
                    if (item.Pais.Codigo == Pais.Codigo) {
                        $scope.ListadoPuertosDestino.push(item)
                    }
                })

                if ($scope.ListadoPuertosDestino.length > 0) {
                    $scope.DeshabilitarPuertoDestino = false
                    $scope.ModalImportacion.PaisDestino = Pais
                }
                else {
                    $scope.ListadoPuertosDestino = [];
                    $scope.DeshabilitarPuertoDestino = true
                }
            }
        }
        //-- Cargar el combo de paises --//
        PaisesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    PaisesCarga = true;
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: 'Seleccione país', Codigo: 0 })
                        $scope.ListadoPaises = response.data.Datos;
                    } else {
                        $scope.ListadoPaises = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //-- Cargar el combo modo transporte --//
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NAVIERAS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ModoTransporteCarga = true;
                    $scope.ListadoNavieras = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoNavieras = response.data.Datos;
                        $scope.ModalImportacion.Naviera = $scope.ListadoNavieras[0]
                        if ($scope.CodigoNaviera > 0) {
                            $scope.ModalImportacion.Naviera = $linq.Enumerable().From($scope.ListadoNavieras).First('$.Codigo == ' + $scope.CodigoNaviera);
                        }

                    }
                    else {
                        $scope.ListadoNavieras = []
                    }
                }
            }, function (response) {
            });
        //-- Cargar el combo Tipo Cargue --//
        $scope.ListadoTipoCargue = [];
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 201 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoCargue = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoCargue = response.data.Datos;
                        $scope.Modelo.TipoCargue = $scope.ListadoTipoCargue[0]
                        if ($scope.CodigoTipoCargue > 0) {
                            $scope.Modelo.TipoCargue = $linq.Enumerable().From($scope.ListadoTipoCargue).First('$.Codigo == ' + $scope.CodigoTipoCargue);
                        }
                    }
                    else {
                        $scope.ListadoTipoCargue = []
                    }
                }
            }, function (response) {
            });
        //-- Cargar el combo tipo despacho --//
        $scope.ListadoTipoDespacho = [];
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DESPACHO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ModoTransporteCarga = true;
                    $scope.ListadoTipoDespacho = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoDespacho = response.data.Datos;
                        if ($scope.Sesion.UsuarioAutenticado.ManejoDespachoProgramacion) {
                            $scope.Modelo.TipoDespacho = $scope.ListadoTipoDespacho[1]
                        } else {
                            $scope.Modelo.TipoDespacho = $scope.ListadoTipoDespacho[0]
                        }
                        if ($scope.CodigoTipoDespacho > 0) {
                            $scope.Modelo.TipoDespacho = $linq.Enumerable().From($scope.ListadoTipoDespacho).First('$.Codigo == ' + $scope.CodigoTipoDespacho);
                        }
                    }
                    else {
                        $scope.ListadoTipoDespacho = []
                    }
                }
            }, function (response) {
            });

        $scope.ListadoNovedadesCumplimiento = []
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 193 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ModoTransporteCarga = true;
                    $scope.ListadoNovedadesCumplimiento = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoNovedadesCumplimiento = response.data.Datos;
                        $scope.Modelo.NovedadCalidad = $scope.ListadoNovedadesCumplimiento[0]
                        if ($scope.CodigoNovedadCalidad > 0) {
                            $scope.Modelo.NovedadCalidad = $linq.Enumerable().From($scope.ListadoNovedadesCumplimiento).First('$.Codigo == ' + $scope.CodigoNovedadCalidad);
                        }
                    }
                    else {
                        $scope.ListadoNovedadesCumplimiento = []
                    }
                }
            }, function (response) {
            });
        //-- Cargar el combo de ConceptoVentas --//
        ConceptoVentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ConceptoSistema: -1, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ConceptoVentaCarga = true;
                    $scope.ListadoConceptos = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoConceptos = response.data.Datos;
                    }
                }
            }, function (response) {
            });
        //-- Consulta Clientes --//
        $scope.ListadoCliente = []
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente)
                }
            }
            return $scope.ListadoCliente
        }
        $scope.ListadoAseguradora = []
        $scope.AutocompleteAseguradora = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_ASEGURADORA,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoAseguradora = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoAseguradora)
                }
            }
            return $scope.ListadoAseguradora
        }
        //-- Consulta Comerciales --//

        $scope.AutocompleteComercial = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_COMERCIAL,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoComerciales = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoComerciales)
                }
            }
            return $scope.ListadoComerciales
        }

        //-- Consulta Destinatarios --//

        $scope.AutocompleteDestinatario = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_DESTINATARIO,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoDestinatarios = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoDestinatarios)
                }
            }
            return $scope.ListadoDestinatarios
        }
        $scope.ListadoRemitente = []
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_REMITENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoRemitente)
                }
            }
            return $scope.ListadoRemitente
        }
        //-- Consulta Aserguradora--//

        $scope.AutocompleteAseguarador = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_ASEGURADORA,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoAseguradoras = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoAseguradoras)
                }
            }
            return $scope.ListadoAseguradoras
        }





        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                }
            }
            return $scope.ListadoCiudades
        }

        //-- Consultar rutas --//
        RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    RutasCarga = true;
                    $scope.ListadoRutas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoRutas = response.data.Datos
                    }
                    else {
                        $scope.ListadoRutas = []
                    }
                }
            }, function (response) {
            });
        //-- Cargar el combo de TipoTarifaTransportes --//
        TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    TipoTarifaCarga = true;
                    $scope.ListadoTipoTarifaTransportes = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoTarifaTransportes = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTipoTarifaTransportes = []
                    }
                }
            }, function (response) {
            });

        //-- Cargar el combo de TarifaTransportes --//
        TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    TarifaCarga = true;
                    $scope.ListadoTarifaTransportes = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTarifaTransportes = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTarifaTransportes = []
                    }
                }
            }, function (response) {
            });

        $scope.AutocompleteProductos = function (value) {
            $scope.ListaProductoTransportado = [];
            if (value.length > 0) {
                /*Cargar Autocomplete de propietario*/
                blockUIConfig.autoBlock = false;
                var Response = ProductoTransportadosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    ValorAutocomplete: value,
                    AplicaTarifario: -1,
                    Sync: true
                })
                $scope.ListaProductoTransportado = ValidarListadoAutocomplete(Response.Datos, $scope.ListaProductoTransportado)
            }
            return $scope.ListaProductoTransportado
        }

        //-- Productos Transportados --//
        //ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1 }).
        //    then(function (response) {
        //        if (response.data.ProcesoExitoso === true) {
        //            ProductoTranportadoCarga = true;
        //            if (response.data.Datos.length > 0) {
        //                response.data.Datos.push({ Nombre: '', Codigo: 0 })
        //                $scope.ListaProductoTransportado = response.data.Datos;
        //                try {
        //                    $scope.Modelo.Producto = $linq.Enumerable().From($scope.ListaProductoTransportado).First('$.Codigo ==' + $scope.Modelo.Producto.Codigo);

        //                } catch (e) {

        //                }
        //            }
        //            else {
        //                $scope.ListaProductoTransportado = [];
        //            }
        //        }
        //    }, function (response) {
        //        ShowError(response.statusText);
        //    });

        //--- Cargar el combo modo transporte --//
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_MODO_TRANSPORTE } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ModoTransporteCarga = true;
                    $scope.ListadoModoTransporte = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoModoTransporte = response.data.Datos;
                        $scope.Modal.ModoTransporte = $scope.ListadoModoTransporte[0]
                    }
                    else {
                        $scope.ListadoModoTransporte = []
                    }
                }
            }, function (response) {
            });

        //-- Cargar el combo Tipo poliza --//
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_POLIZA_ORDEN_SERVICIOS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    TipoPolizaCarga = true;
                    $scope.ListadoTipoPoliza = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoPoliza = response.data.Datos;
                        $scope.ModalPoliza.TipoPolizaOrdenServicio = $scope.ListadoTipoPoliza[0]
                    }
                    else {
                        $scope.ListadoTipoPoliza = []
                    }
                }
            }, function (response) {
            });


        //-- Cargar el combo Horario poliza --//
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_HORARIO_AUTORIZADO_POLIZA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    HorarioPolizaCarga = true;
                    $scope.ListadoHorarioPolizas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoHorarioPolizas = response.data.Datos;
                        $scope.ModalPoliza.HorarioAutorizado = $scope.ListadoHorarioPolizas[0]
                    }
                    else {
                        $scope.ListadoHorarioPolizas = []
                    }
                }
            }, function (response) {
            });
        //-- Cargar Productos tarifas --//
        ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, AplicaTarifario: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoProductosTransportadosTarifas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoProductosTransportadosTarifas = response.data.Datos;
                        for (var j = 0; j < $scope.ListadoProductosTransportadosTarifas.length; j++) {
                            $scope.ListadoProductosTransportadosTarifas[j] = { Codigo: $scope.ListadoProductosTransportadosTarifas[j].Codigo, Nombre: $scope.ListadoProductosTransportadosTarifas[j].Nombre }
                        }
                    }
                    else {
                        $scope.ListadoProductosTransportadosTarifas = []
                    }
                }
            }, function (response) {
            });

        $scope.ValidarPoliza = function (Cliente) {
            if ($scope.Modelo.AplicaPoliza == true) {
                $scope.HabilitarPoliza = true
            }
            else {
                $scope.HabilitarPoliza = false
            }
        }

        function ValCargaFactorys() {
            if (DocumentosCarga == true && LineaNegocioCarga == true && OficinaCarga && TipoLineaNegocioCarga == true && PuertosPaisesCarga == true &&
                PaisesCarga == true && ModoTransporteCarga == true && ConceptoVentaCarga == true && RutasCarga == true && TipoTarifaCarga == true &&
                TarifaCarga == true && ModoTransporteCarga == true && TipoPolizaCarga == true && HorarioPolizaCarga == true) {
                clearTimeout(TimeOutCarga);
                //Carga Funcion de obtner
                Obtener();
            }
            else {
                clearTimeout(TimeOutCarga);
                TimeOutCarga = setTimeout(ValCargaFactorys, 1000);
            }
        }

        //-- Consultar tarifas del cliente --//
        $scope.ValidarSaldoCliente = function () {
            var continuar = true
            if ($scope.ValidaCupoGeneral || $scope.ValidaCupoSede) {
                var ValorTotal = 0
                var valorFlete = 0
                var valorCargue = 0
                var valorDescargue = 0
                if ($scope.Modelo.TipoDespacho.Codigo == 18202) {
                    if ($scope.Modelo.LineaNegocioCarga !== undefined && $scope.Modelo.TipoLineaNegocioCarga !== undefined && $scope.Modelo.ValorTipoDespacho !== undefined && parseInt(MascaraNumero($scope.Modelo.ValorTipoDespacho)) > 0 && $scope.Modelo.Ruta !== undefined && $scope.Modelo.TarifarioVentas !== undefined) {
                        if ($scope.Modelo.TarifarioVentas.Tarifas.length > 0) {
                            var encontrotarifa = false
                            for (var i = 0; i < $scope.Modelo.TarifarioVentas.Tarifas.length; i++) {
                                var tarifa = $scope.Modelo.TarifarioVentas.Tarifas[i]
                                if (
                                    tarifa.TipoLineaNegocioTransportes.Codigo == $scope.Modelo.TipoLineaNegocioCarga.Codigo &&
                                    tarifa.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.LineaNegocioCarga.Codigo &&
                                    tarifa.TipoTarifaTransportes.TarifaTransporte.Codigo == 301 && //Peso Tonelada
                                    tarifa.Ruta.Codigo == $scope.Modelo.Ruta.Codigo
                                ) {
                                    encontrotarifa = true
                                    valorFlete = tarifa.ValorFlete
                                    break;
                                }
                            }
                            if (encontrotarifa) {
                                if ($scope.ValoresCargueDescargue.length > 0) {
                                    for (var i = 0; i < $scope.ValoresCargueDescargue.length; i++) {
                                        if ($scope.ValoresCargueDescargue[i].SitioCliente.Codigo == $scope.Modelo.SitioCargue.SitioCliente.Codigo) {
                                            valorCargue = $scope.ValoresCargueDescargue[i].ValorCargueCliente
                                        }
                                        if ($scope.ValoresCargueDescargue[i].SitioCliente.Codigo == $scope.Modelo.SitioDescargue.SitioCliente.Codigo) {
                                            valorDescargue = $scope.ValoresCargueDescargue[i].ValorDescargueCliente
                                        }
                                    }
                                }
                                valorFlete = valorFlete + valorCargue + valorDescargue
                                ValorTotal = valorFlete * (parseInt(MascaraNumero($scope.Modelo.ValorTipoDespacho)) / 1000)
                                if ($scope.ValidaCupoGeneral) {
                                    if (ValorTotal > $scope.SaldoCliente) {
                                        ShowError('El valor del cupo que esta intentado ingresar excede el saldo del cliente /n Saldo disponible $' + MascaraValores($scope.SaldoCliente) + ' Valor del despacho $' + MascaraValores(ValorTotal))
                                        $scope.Modelo.ValorTipoDespacho = 0
                                        $scope.Modelo.SaldoTipoDespacho = 0
                                        $('#SaldoOrden').focus()
                                        continuar = false
                                    }
                                }
                                if ($scope.ValidaCupoSede) {
                                    if ($scope.ListadoCupoSedes.length > 0) {
                                        for (var i = 0; i < $scope.ListadoCupoSedes.length; i++) {
                                            if ($scope.ListadoCupoSedes[i].Codigo == $scope.Modelo.SedeFacturacion.Codigo) {
                                                if (ValorTotal > $scope.ListadoCupoSedes[i].Saldo) {
                                                    ShowError('El valor del cupo que esta intentado ingresar excede el saldo de la sede del cliente /n Saldo disponible $' + MascaraValores($scope.ListadoCupoSedes[i].Saldo) + ' Valor del despacho $' + MascaraValores(ValorTotal))
                                                    $scope.Modelo.ValorTipoDespacho = 0
                                                    $scope.Modelo.SaldoTipoDespacho = 0
                                                    $('#SaldoOrden').focus()
                                                    continuar = false
                                                }
                                            }
                                            break;
                                        }
                                    } else {
                                        ShowError('No se encontro una parametrización valida del saldo de la sede seleccionada del cliente')
                                    }
                                }
                            } else {
                                ShowError('No se encontraron tarifas con las condiciones especificadas para validar el saldo del cliente')
                                continuar = false
                            }
                        }
                    }
                }
            }
            return continuar
        }
        $scope.CargarTarifarioCliente = function (Cliente) {
            $scope.ListadoSedes = []
            $scope.ValoresCargueDescargue = []
            $scope.ListadoCupoSedes = []
            if (Cliente.Codigo > 0) {
                if ($scope.Modelo.Facturar == undefined || $scope.Modelo.Facturar == '' || $scope.Modelo.Facturar == null) {
                    $scope.Modelo.Facturar = $scope.CargarTercero(Cliente.Codigo)
                }
                if ($scope.Modelo.Remitente == undefined || $scope.Modelo.Remitente == '' || $scope.Modelo.Remitente == null) {
                    $scope.Modelo.Remitente = $scope.CargarTercero(Cliente.Codigo)
                }
                TercerosFactory.Obtener(Cliente).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.Modelo.Comercial = $scope.CargarTercero(response.data.Datos.Cliente.RepresentanteComercial.Codigo)
                                try {
                                    if (response.data.Datos.TipoValidacionCupo.Codigo == 19202) {
                                        $scope.ValidaCupoGeneral = true
                                        $scope.SaldoCliente = response.data.Datos.Saldo
                                    } else if (response.data.Datos.TipoValidacionCupo.Codigo == 19203) {
                                        $scope.ValidaCupoSede = true
                                        $scope.ListadoCupoSedes = response.data.Datos.TerceroClienteCupoSedes
                                    }
                                    $scope.ValoresCargueDescargue = response.data.Datos.SitiosTerceroCliente
                                } catch (e) {

                                }
                                $scope.ListadoSedes = response.data.Datos.Direcciones
                                if ($scope.ListadoSedes.length > 0) {
                                    try {
                                        if ($scope.Modelo.SedeFacturacion.Codigo > 0) {
                                            $scope.Modelo.SedeFacturacion = $linq.Enumerable().From($scope.ListadoSedes).First('$.Codigo ==' + $scope.Modelo.SedeFacturacion.Codigo);
                                        } else {
                                            $scope.Modelo.SedeFacturacion = $scope.ListadoSedes[0]
                                        }
                                    } catch (e) {
                                        $scope.Modelo.SedeFacturacion = $scope.ListadoSedes[0]
                                    }
                                }
                                $scope.ListadoSitios = response.data.Datos.SitiosTerceroCliente
                                for (var i = 0; i < $scope.ListadoSitios.length; i++) {
                                    $scope.ListadoSitios[i].Codigo = $scope.ListadoSitios[i].SitioCliente.Codigo
                                }
                            }
                        }
                    });
                TarifarioVentasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CodigoCliente: Cliente.Codigo }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.Modelo.TarifarioVentas = response.data.Datos;
                            if ($scope.Modelo.TarifarioVentas.Tarifas.length > 0) {

                                $scope.Modelo.TarifarioVentas.Tarifas.forEach(function (item) {
                                    //Asignar lineas de negocio apartir del tarifario
                                    if ($scope.ListadoLineaNegocioTarifario.length == 0) {
                                        $scope.ListadoLineaNegocioCarga.forEach(function (itemLinea) {
                                            if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == itemLinea.Codigo) {
                                                $scope.ListadoLineaNegocioTarifario.push(itemLinea)
                                            }
                                        })
                                    }
                                    else {
                                        var con = 0
                                        $scope.ListadoLineaNegocioTarifario.forEach(function (itemLinea) {
                                            if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == itemLinea.Codigo) {
                                                con++
                                            }
                                        })

                                        if (con == 0) {
                                            var con1 = 0
                                            $scope.ListadoLineaNegocioCarga.forEach(function (itemLinea) {
                                                if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == itemLinea.Codigo) {
                                                    $scope.ListadoLineaNegocioTarifario.forEach(function (itemDetalle) {
                                                        if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == itemDetalle.Codigo) {
                                                            con1++
                                                        }
                                                    })
                                                    if (con1 == 0) {
                                                        $scope.ListadoLineaNegocioTarifario.push(itemLinea)
                                                    }
                                                }
                                            })
                                        }
                                    }
                                })

                                if ($scope.ListadoLineaNegocioTarifario.length > 0) {

                                    $scope.DeshabilitarLineaNegocio = false
                                    if ($scope.CodigoLineaNegocioCarga !== undefined && $scope.CodigoLineaNegocioCarga !== '' && $scope.CodigoLineaNegocioCarga !== null) {
                                        $scope.Modelo.LineaNegocioCarga = $linq.Enumerable().From($scope.ListadoLineaNegocioTarifario).First('$.Codigo ==' + $scope.CodigoLineaNegocioCarga);
                                        $scope.CargarTipoLineaNegocio({ Codigo: $scope.CodigoLineaNegocioCarga })
                                    }

                                }
                                else {
                                    $scope.DeshabilitarLineaNegocio = true
                                }

                            }
                            else {
                                ShowError('No se encontraron tarifas vigentes o disponibles, por favor revise la parametrización del tarifario del cliente')
                            }
                        }
                        else {
                            ShowError('No se encontraron tarifarios vigentes o disponibles, por favor revise la parametrizacion')
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        $scope.CargarTipoLineaNegocio = function (LineaNegocio) {
            $scope.ListadoTipoLineaNegocioTarifario = [];
            $scope.Modelo.TarifarioVentas.Tarifas.forEach(function (item) {
                //Asignar tipo lineas de negocio apartir del tarifario
                if ($scope.ListadoTipoLineaNegocioTarifario.length == 0) {
                    $scope.ListadoTipoLineaNegocioCarga.forEach(function (itemTipo) {
                        if (item.TipoLineaNegocioTransportes.Codigo == itemTipo.Codigo && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == LineaNegocio.Codigo) {
                            $scope.ListadoTipoLineaNegocioTarifario.push(itemTipo)
                        }
                    })
                }
                else {
                    var con = 0
                    $scope.ListadoTipoLineaNegocioTarifario.forEach(function (itemLinea) {
                        if (item.TipoLineaNegocioTransportes.Codigo == itemLinea.Codigo && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == LineaNegocio.Codigo) {
                            con++
                        }
                    })

                    if (con == 0) {
                        var con1 = 0
                        $scope.ListadoTipoLineaNegocioCarga.forEach(function (itemTipo) {
                            if (item.TipoLineaNegocioTransportes.Codigo == itemTipo.Codigo) {
                                $scope.ListadoTipoLineaNegocioTarifario.forEach(function (itemDetalle) {
                                    if (item.TipoLineaNegocioTransportes.Codigo == itemDetalle.Codigo && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == LineaNegocio.Codigo) {
                                        con1++
                                    }
                                })
                                if (con1 == 0) {
                                    if (item.TipoLineaNegocioTransportes.Codigo == itemTipo.Codigo && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == LineaNegocio.Codigo) {
                                        $scope.ListadoTipoLineaNegocioTarifario.push(itemTipo)
                                    }
                                }
                            }
                        })
                    }
                }
            })

            if ($scope.ListadoTipoLineaNegocioTarifario.length > 0) {
                $scope.DeshabilitarTipoLineaNegocio = false
                if ($scope.CodigoTipoLineaNegocioCarga !== undefined && $scope.CodigoTipoLineaNegocioCarga !== '' && $scope.CodigoTipoLineaNegocioCarga !== null) {
                    $scope.Modelo.TipoLineaNegocioCarga = $linq.Enumerable().From($scope.ListadoTipoLineaNegocioTarifario).First('$.Codigo ==' + $scope.CodigoTipoLineaNegocioCarga);
                }

                if ($scope.Modelo.TipoLineaNegocioCarga !== undefined && $scope.Modelo.TipoLineaNegocioCarga !== '' && $scope.Modelo.TipoLineaNegocioCarga !== null) {
                    if ($scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_IMPORTACION ||
                        $scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_EXTEMPORANEO ||
                        $scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_DTA ||
                        $scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_OTM) {
                        $scope.HabilitarImportacionExportacion = true;
                    }
                }

                if ($scope.Modelo.Estado == 0) {
                    $scope.Deshabilitar = false;
                }
                else if ($scope.Modelo.Estado == 1) {
                    $scope.Deshabilitar = true;
                    $scope.DeshabilitarActualizar = true
                    $scope.DeshabilitarTipoLineaNegocio = true
                    $scope.DeshabilitarLineaNegocio = true
                }
                if ($scope.Modelo.Anulado == 1) {
                    $scope.Deshabilitar = true;
                    $scope.DeshabilitarActualizar = true
                    $scope.DeshabilitarTipoLineaNegocio = true
                    $scope.DeshabilitarLineaNegocio = true
                }
                if (parseInt($routeParams.Generar) >= 1) {
                    $scope.Titulo = 'GENERAR ORDEN SERVICIO';
                    $scope.Deshabilitar = false;
                    $scope.DeshabilitarActualizar = false
                    $scope.DeshabilitarTipoLineaNegocio = false
                    $scope.DeshabilitarLineaNegocio = false
                    $scope.DeshabilitarPuertoDestino = false
                    $scope.DeshabilitarPuertoOrigen = false
                    $scope.DeshabilitarDocumentos = false
                }

            }
            else {
                $scope.DeshabilitarTipoLineaNegocio = true
            }
            try {
                if ($scope.Modelo.Ruta.Codigo > 0) {
                    $scope.CargarRutas($scope.Modelo.TipoLineaNegocioCarga, $scope.Modelo.LineaNegocioCarga)
                    $scope.CargarTarifasViaje($scope.Modelo.Ruta)

                }
            } catch (e) {

            }

        }
        console.clear()
        $scope.AdicionarDetalle = function () {
            $scope.MensajesError = [];
            $scope.MostrarModalDetalle = true;
            $scope.DireccionDestino = '';
            if ($scope.Modelo.Cliente == undefined || $scope.Modelo.Cliente == "" || $scope.Modelo.Cliente == null || $scope.Modelo.Cliente.Codigo == 0) {
                $scope.MensajesError.push("Debe ingresar un cliente")
                $scope.MostrarModalDetalle = false
            }
            if ($scope.Modelo.Destinatario == undefined || $scope.Modelo.Destinatario == "" || $scope.Modelo.Destinatario == null) {
                $scope.MensajesError.push("Debe ingresar un destinatario")
                $scope.MostrarModalDetalle = false
            }
            if ($scope.Modelo.LineaNegocioCarga == undefined || $scope.Modelo.LineaNegocioCarga == "" || $scope.Modelo.LineaNegocioCarga == null) {
                $scope.MensajesError.push("Debe ingresar una línea de negocio")
                $scope.MostrarModalDetalle = false
            }
            if ($scope.Modelo.TipoLineaNegocioCarga == undefined || $scope.Modelo.TipoLineaNegocioCarga == "" || $scope.Modelo.TipoLineaNegocioCarga == null) {
                $scope.MensajesError.push("Debe ingresar un tipo de operación")
                $scope.MostrarModalDetalle = false
            }
            if ($scope.Modelo.FechaCargue == undefined || $scope.Modelo.FechaCargue == "" || $scope.Modelo.FechaCargue == null ||
                $scope.Modelo.CiudadCargue == undefined || $scope.Modelo.CiudadCargue == "" || $scope.Modelo.CiudadCargue == null ||
                $scope.Modelo.DireccionCargue == undefined || $scope.Modelo.DireccionCargue == "" || $scope.Modelo.DireccionCargue == null ||
                $scope.Modelo.TelefonoCargue == undefined || $scope.Modelo.TelefonoCargue == "" || $scope.Modelo.TelefonoCargue == null ||
                $scope.Modelo.ContactoCargue == undefined || $scope.Modelo.ContactoCargue == "" || $scope.Modelo.ContactoCargue == null ||
                $scope.Modelo.TipoDespacho == undefined || $scope.Modelo.TipoDespacho == "" || $scope.Modelo.TipoDespacho == null
            ) {
                if ($scope.MostrarModalDetalle) {
                    ShowError('Debe ingresar la información de despacho')
                    $scope.MostrarModalDetalle = false
                } else {
                    $scope.MensajesError.push("Debe ingresar la información de despacho")
                    $scope.MostrarModalDetalle = false
                }
            }
            $scope.Modal.SitioDevolucionContenedor = {
                SitioCliente: { Nombre: '' }
            }

            if ($scope.MostrarModalDetalle) {
                if ($scope.Modelo.LineaNegocioCarga.Codigo == 2) {
                    if ($scope.ListaDetalleOrden.length == 1) {
                        $scope.MensajesError.push("Solo se permite un detalle para la línea de negocio semimasivo")
                    }
                    else {
                        limpiarModal();
                        showModal('ModalAdicionarDetalle');
                        $scope.ListadoRutasTarifario = [];

                        $scope.Modal.DocumentoCliente = $scope.Modelo.DocumentoCliente
                        $scope.Modal.DireccionCargue = $scope.Modelo.DireccionCargue
                        $scope.Modal.TelefonoCargue = $scope.Modelo.TelefonoCargue
                        $scope.Modal.ContactoCargue = $scope.Modelo.ContactoCargue
                        $scope.Modal.CiudadCargue = $scope.CargarCiudad($scope.Modelo.CiudadCargue.Codigo)
                        $scope.Modal.Destinatario = $scope.CargarTercero($scope.Modelo.Destinatario.Codigo)
                        $scope.Modal.TipoLineaNegocioCarga = $linq.Enumerable().From($scope.ListadoTipoLineaNegocioTarifario).First('$.Codigo ==' + $scope.Modelo.TipoLineaNegocioCarga.Codigo);
                        $scope.CargarRutas($scope.Modal.TipoLineaNegocioCarga, $scope.Modelo.LineaNegocioCarga)
                    }
                }
                else {
                    limpiarModal();
                    showModal('ModalAdicionarDetalle');
                    $scope.ListadoRutasTarifario = [];

                    $scope.Modal.DocumentoCliente = $scope.Modelo.DocumentoCliente
                    $scope.Modal.DireccionCargue = $scope.Modelo.DireccionCargue
                    $scope.Modal.TelefonoCargue = $scope.Modelo.TelefonoCargue
                    $scope.Modal.ContactoCargue = $scope.Modelo.ContactoCargue
                    $scope.Modal.CiudadCargue = $scope.CargarCiudad($scope.Modelo.CiudadCargue.Codigo)
                    $scope.Modal.Destinatario = $scope.CargarTercero($scope.Modelo.Destinatario.Codigo)
                    $scope.Modal.TipoLineaNegocioCarga = $linq.Enumerable().From($scope.ListadoTipoLineaNegocioTarifario).First('$.Codigo ==' + $scope.Modelo.TipoLineaNegocioCarga.Codigo);
                    $scope.CargarRutas($scope.Modal.TipoLineaNegocioCarga, $scope.Modelo.LineaNegocioCarga)
                }
            }

        }

        $scope.ModificarDetalle = function (index, itemdetalle) {
            $scope.Indice = parseInt(index);
            $scope.MensajesError = [];
            var itemdetalle = itemdetalle;
            $scope.MostrarModalModificarDetalle = true;

            if ($scope.Modelo.Cliente == undefined || $scope.Modelo.Cliente == "" || $scope.Modelo.Cliente == null || $scope.Modelo.Cliente.Codigo == 0) {
                $scope.MensajesError.push("Debe ingresar un cliente")
                $scope.MostrarModalModificarDetalle = false
            }
            if ($scope.Modelo.LineaNegocioCarga == undefined || $scope.Modelo.LineaNegocioCarga == "" || $scope.Modelo.LineaNegocioCarga == null) {
                $scope.MensajesError.push("Debe ingresar una línea de negocio")
                $scope.MostrarModalModificarDetalle = false
            }
            if ($scope.Modelo.LineaNegocioCarga.Codigo == 2) {
                //if ($scope.ListaDetalleOrden.length == 1) {
                //    $scope.MensajesError.push("Solo se permite un detalle para la línea de negocio semimasivo")
                //} else {
                showModal('ModalModificarDetalle');
                $scope.ListadoRutasTarifario = [];

                $scope.Modal.TipoLineaNegocioCarga = $linq.Enumerable().From($scope.ListadoTipoLineaNegocioTarifario).First('$.Codigo ==' + itemdetalle.TipoLineaNegocioCarga.Codigo);
                $scope.CargarRutasModificar(itemdetalle.TipoLineaNegocioCarga, $scope.Modelo.LineaNegocioCarga);
                try {
                    $scope.Modal.Ruta = $linq.Enumerable().From($scope.ListadoRutasTarifario).First('$.Codigo ==' + itemdetalle.Ruta.Codigo);
                } catch (e) {
                }
                $scope.CargarTarifasModificar(itemdetalle.Ruta);
                $scope.Modal.Tarifa = $linq.Enumerable().From($scope.ListadoTarifaTarifario).First('$.Codigo ==' + itemdetalle.Tarifa.Codigo);
                $scope.CargarTipoTarifasModificar(itemdetalle.Tarifa);
                $scope.Modal.TipoTarifa = $linq.Enumerable().From($scope.ListadoTipoTarifaTarifario).First('$.Codigo ==' + itemdetalle.TipoTarifa.Codigo);
                $scope.Modal.Destinatario = $scope.CargarTercero(itemdetalle.Destinatario.Codigo)
                $scope.Modal.Producto = $scope.CargarProducto(itemdetalle.Producto.Codigo)
                $scope.Modal.Cantidad = itemdetalle.Cantidad;
                $scope.Modal.Peso = itemdetalle.Peso;
                $scope.Modal.ValorFOB = itemdetalle.ValorFOB;
                $scope.Modal.DocumentoCliente = itemdetalle.DocumentoCliente;
                $scope.Modal.CiudadCargue = $scope.CargarCiudad(itemdetalle.CiudadCargue.Codigo)
                $scope.Modal.DireccionCargue = itemdetalle.DireccionCargue;
                $scope.Modal.TelefonoCargue = itemdetalle.TelefonoCargue;
                $scope.Modal.ContactoCargue = itemdetalle.ContactoCargue;
                $scope.Modal.CiudadDestino = $scope.CargarCiudad(itemdetalle.CiudadDestino.Codigo)
                $scope.CargarDireccionesClienteDestino($scope.Modelo.Cliente, $scope.Modal.CiudadDestino);
                $scope.Modal.DireccionDestino = itemdetalle.DireccionDestino;
                $scope.Modal.TelefonoDestino = itemdetalle.TelefonoDestino;
                $scope.Modal.ContactoDestino = itemdetalle.ContactoDestino;
                $scope.Modal.NumeroContenedor = itemdetalle.NumeroContenedor;
                $scope.Modal.MBLContenedor = itemdetalle.MBLContenedor;
                $scope.Modal.HBLContenedor = itemdetalle.HBLContenedor;
                $scope.Modal.DOContenedor = itemdetalle.DOContenedor;
                $scope.Modal.FechaDevolucionContenedor = itemdetalle.FechaDevolucionContenedor;
                $timeout(function () {
                    $scope.Modal.SitioDevolucionContenedor = $linq.Enumerable().From($scope.ListadoSitiosDescargue).First('$.SitioCliente.Codigo ==' + itemdetalle.SitioDevolucionContenedor.SitioCliente.Codigo);

                }, 2000)
                //}
            } else {
                showModal('ModalModificarDetalle');
                $scope.ListadoRutasTarifario = [];

                $scope.Modal.TipoLineaNegocioCarga = $linq.Enumerable().From($scope.ListadoTipoLineaNegocioTarifario).First('$.Codigo ==' + itemdetalle.TipoLineaNegocioCarga.Codigo);
                $scope.CargarRutasModificar(itemdetalle.TipoLineaNegocioCarga, $scope.Modelo.LineaNegocioCarga);
                try {
                    $scope.Modal.Ruta = $linq.Enumerable().From($scope.ListadoRutasTarifario).First('$.Codigo ==' + itemdetalle.Ruta.Codigo);
                } catch (e) {
                }
                $scope.CargarTarifasModificar(itemdetalle.Ruta);
                $scope.Modal.Tarifa = $linq.Enumerable().From($scope.ListadoTarifaTarifario).First('$.Codigo ==' + itemdetalle.Tarifa.Codigo);
                $scope.CargarTipoTarifasModificar(itemdetalle.Tarifa);
                $scope.Modal.TipoTarifa = $linq.Enumerable().From($scope.ListadoTipoTarifaTarifario).First('$.Codigo ==' + itemdetalle.TipoTarifa.Codigo);
                $scope.Modal.Destinatario = $scope.CargarTercero(itemdetalle.Destinatario.Codigo)
                $scope.Modal.Producto = $scope.CargarProducto(itemdetalle.Producto.Codigo);
                $scope.Modal.Cantidad = itemdetalle.Cantidad;
                $scope.Modal.Peso = itemdetalle.Peso;
                $scope.Modal.ValorFOB = itemdetalle.ValorFOB;
                $scope.Modal.DocumentoCliente = itemdetalle.DocumentoCliente;
                $scope.Modal.CiudadCargue = $scope.CargarCiudad(itemdetalle.CiudadCargue.Codigo)
                $scope.Modal.DireccionCargue = itemdetalle.DireccionCargue;
                $scope.Modal.TelefonoCargue = itemdetalle.TelefonoCargue;
                $scope.Modal.ContactoCargue = itemdetalle.ContactoCargue;
                $scope.Modal.CiudadDestino = $scope.CargarCiudad(itemdetalle.CiudadDestino.Codigo)
                $scope.CargarDireccionesClienteDestino($scope.Modelo.Cliente, $scope.Modal.CiudadDestino);
                $scope.Modal.DireccionDestino = itemdetalle.DireccionDestino;
                $scope.Modal.TelefonoDestino = itemdetalle.TelefonoDestino;
                $scope.Modal.ContactoDestino = itemdetalle.ContactoDestino;
                $scope.Modal.NumeroContenedor = itemdetalle.NumeroContenedor;
                $scope.Modal.MBLContenedor = itemdetalle.MBLContenedor;
                $scope.Modal.HBLContenedor = itemdetalle.HBLContenedor;
                $scope.Modal.DOContenedor = itemdetalle.DOContenedor;
                $scope.Modal.FechaDevolucionContenedor = itemdetalle.FechaDevolucionContenedor;
                $timeout(function () {
                    $scope.Modal.SitioDevolucionContenedor = $linq.Enumerable().From($scope.ListadoSitiosDescargue).First('$.SitioCliente.Codigo ==' + itemdetalle.SitioDevolucionContenedor.SitioCliente.Codigo);
                }, 2000)
            }
        }

        $scope.HabilitarEspoImpo = function () {
            if ($scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_IMPORTACION ||
                $scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_EXTEMPORANEO ||
                $scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_DTA ||
                $scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_OTM) {
                $scope.HabilitarImportacionExportacion = true
            }
            else {
                $scope.HabilitarImportacionExportacion = false
            }
        }

        $scope.CargarRutas = function (TipoLinea, Linea) {
            $scope.ListadoRutasTarifario = [];
            $scope.Modal.Ruta = "";
            $scope.DeshabilitarTarifas = true
            $scope.ListadoTipoTarifaTarifario = [];
            $scope.ListadoTarifaTarifario = [];
            $scope.DeshabilitarTipoTarifas = true
            $scope.Modelo.TarifarioVentas.Tarifas.forEach(function (item) {
                //Asignar rutas apartir del tarifario
                if ($scope.ListadoRutasTarifario.length == 0) {
                    $scope.ListadoRutas.forEach(function (ItemRuta) {
                        if (item.Ruta.Codigo == ItemRuta.Codigo && item.TipoLineaNegocioTransportes.Codigo == TipoLinea.Codigo && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == Linea.Codigo) {
                            $scope.ListadoRutasTarifario.push(ItemRuta)
                        }
                    })
                }
                else {
                    var con = 0
                    $scope.ListadoRutasTarifario.forEach(function (ItemRuta) {
                        if (item.Ruta.Codigo == ItemRuta.Codigo) {
                            con++
                        }
                    })

                    if (con == 0) {
                        var con1 = 0
                        $scope.ListadoRutas.forEach(function (ItemRuta) {
                            if (item.Ruta.Codigo == ItemRuta.Codigo) {
                                $scope.ListadoRutasTarifario.forEach(function (ItemRuta1) {
                                    if (item.Ruta.Codigo == ItemRuta1.Codigo && item.TipoLineaNegocioTransportes.Codigo == TipoLinea.Codigo && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == Linea.Codigo) {
                                        con1++
                                    }
                                })
                                if (con1 == 0) {
                                    if (item.Ruta.Codigo == ItemRuta.Codigo && item.TipoLineaNegocioTransportes.Codigo == TipoLinea.Codigo && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == Linea.Codigo) {
                                        $scope.ListadoRutasTarifario.push(ItemRuta)
                                    }
                                }
                            }
                        })
                    }
                }

                if ($scope.ListadoRutasTarifario.length > 0) {
                    $scope.DeshabilitarRutas = false
                }
                else {
                    $scope.DeshabilitarRutas = true
                }
            })
        }

        $scope.CargarRutasModificar = function (TipoLinea, Linea) {
            $scope.ListadoRutasTarifario = [];
            $scope.DeshabilitarTarifas = true
            $scope.ListadoTipoTarifaTarifario = [];
            $scope.ListadoTarifaTarifario = [];
            $scope.DeshabilitarTipoTarifas = true
            $scope.Modelo.TarifarioVentas.Tarifas.forEach(function (item) {
                //Asignar rutas apartir del tarifario
                if ($scope.ListadoRutasTarifario.length == 0) {
                    $scope.ListadoRutas.forEach(function (ItemRuta) {
                        if (item.Ruta.Codigo == ItemRuta.Codigo && item.TipoLineaNegocioTransportes.Codigo == TipoLinea.Codigo && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == Linea.Codigo) {
                            $scope.ListadoRutasTarifario.push(ItemRuta)
                        }
                    })
                }
                else {
                    var con = 0
                    $scope.ListadoRutasTarifario.forEach(function (ItemRuta) {
                        if (item.Ruta.Codigo == ItemRuta.Codigo) {
                            con++
                        }
                    })

                    if (con == 0) {
                        var con1 = 0
                        $scope.ListadoRutas.forEach(function (ItemRuta) {
                            if (item.Ruta.Codigo == ItemRuta.Codigo) {
                                $scope.ListadoRutasTarifario.forEach(function (ItemRuta1) {
                                    if (item.Ruta.Codigo == ItemRuta1.Codigo && item.TipoLineaNegocioTransportes.Codigo == TipoLinea.Codigo && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == Linea.Codigo) {
                                        con1++
                                    }
                                })
                                if (con1 == 0) {
                                    if (item.Ruta.Codigo == ItemRuta.Codigo && item.TipoLineaNegocioTransportes.Codigo == TipoLinea.Codigo && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == Linea.Codigo) {
                                        $scope.ListadoRutasTarifario.push(ItemRuta)
                                    }
                                }
                            }
                        })
                    }
                }

                if ($scope.ListadoRutasTarifario.length > 0) {
                    $scope.DeshabilitarRutas = false
                }
                else {
                    $scope.DeshabilitarRutas = true
                }
            })
        }

        $scope.CargarTarifasViaje = function (Ruta) {
            $scope.ListadoTarifaTarifario = [];
            if (Ruta !== undefined && Ruta !== "" && Ruta !== null) {
                $scope.Modelo.TarifarioVentas.Tarifas.forEach(function (item) {
                    //Asignar lineas de negocio apartir del tarifario
                    if ($scope.ListadoTarifaTarifario.length == 0) {
                        $scope.ListadoTarifaTransportes.forEach(function (itemTarifa) {
                            if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo && item.Ruta.Codigo == Ruta.Codigo && $scope.Modelo.TipoLineaNegocioCarga.Codigo == item.TipoTarifaTransportes.TarifaTransporte.TipoLineaNegocioTransporte.Codigo) {
                                if ($scope.ListadoTarifaTarifario.length == 0) {
                                    $scope.ListadoTarifaTarifario.push(itemTarifa)
                                } else {
                                    var cont = 0
                                    for (var i = 0; i < $scope.ListadoTarifaTarifario.length; i++) {
                                        if ($scope.ListadoTarifaTarifario[i].Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                            cont++
                                        }
                                    } if (cont == 0) {
                                        $scope.ListadoTarifaTarifario.push(itemTarifa)
                                    }
                                }

                            }
                        })
                    }
                    else {
                        var con = 0
                        $scope.ListadoTarifaTarifario.forEach(function (itemTarifa) {
                            if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo && item.Ruta.Codigo == Ruta.Codigo) {
                                con++
                            }
                        })

                        if (con == 0) {
                            var con1 = 0
                            $scope.ListadoTarifaTransportes.forEach(function (itemTarifa) {
                                if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo) {
                                    $scope.ListadoTarifaTarifario.forEach(function (itemDetalle) {
                                        if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemDetalle.Codigo && item.Ruta.Codigo == Ruta.Codigo) {
                                            con1++
                                        }
                                    })
                                    if (con1 == 0) {
                                        if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo && item.Ruta.Codigo == Ruta.Codigo) {
                                            $scope.ListadoTarifaTarifario.push(itemTarifa)
                                        }
                                    }
                                }
                            })
                        }
                    }
                    try {
                        if ($scope.Modelo.Ruta.Codigo > 0) {
                            $scope.Modelo.Tarifa = $linq.Enumerable().From($scope.ListadoTarifaTarifario).First('$.Codigo == ' + $scope.Modelo.Tarifa.Codigo);
                        }
                    } catch (e) {

                    }
                })

                $scope.Modal.ModificarCiudadDestino = $scope.CargarCiudad(Ruta.CiudadDestino.Codigo)

                if ($scope.ListadoTarifaTarifario.length > 0) {
                    $scope.DeshabilitarTarifas = false
                }
                else {
                    $scope.DeshabilitarTarifas = true
                }
            }

        }

        $scope.CargarTarifas = function (Ruta) {
            $scope.ListadoTarifaTarifario = [];
            if (Ruta !== undefined && Ruta !== "" && Ruta !== null) {
                $scope.Modelo.TarifarioVentas.Tarifas.forEach(function (item) {
                    //Asignar lineas de negocio apartir del tarifario
                    if ($scope.ListadoTarifaTarifario.length == 0) {
                        $scope.ListadoTarifaTransportes.forEach(function (itemTarifa) {
                            if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo && item.Ruta.Codigo == Ruta.Codigo && $scope.Modal.TipoLineaNegocioCarga.Codigo == item.TipoTarifaTransportes.TarifaTransporte.TipoLineaNegocioTransporte.Codigo) {
                                if ($scope.ListadoTarifaTarifario.length == 0) {
                                    $scope.ListadoTarifaTarifario.push(itemTarifa)
                                } else {
                                    var cont = 0
                                    for (var i = 0; i < $scope.ListadoTarifaTarifario.length; i++) {
                                        if ($scope.ListadoTarifaTarifario[i].Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                            cont++
                                        }
                                    } if (cont == 0) {
                                        $scope.ListadoTarifaTarifario.push(itemTarifa)
                                    }
                                }

                            }
                        })
                    }
                    else {
                        var con = 0
                        $scope.ListadoTarifaTarifario.forEach(function (itemTarifa) {
                            if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo && item.Ruta.Codigo == Ruta.Codigo) {
                                con++
                            }
                        })

                        if (con == 0) {
                            var con1 = 0
                            $scope.ListadoTarifaTransportes.forEach(function (itemTarifa) {
                                if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo) {
                                    $scope.ListadoTarifaTarifario.forEach(function (itemDetalle) {
                                        if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemDetalle.Codigo && item.Ruta.Codigo == Ruta.Codigo) {
                                            con1++
                                        }
                                    })
                                    if (con1 == 0) {
                                        if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo && item.Ruta.Codigo == Ruta.Codigo) {
                                            $scope.ListadoTarifaTarifario.push(itemTarifa)
                                        }
                                    }
                                }
                            })
                        }
                    }
                })

                $scope.Modal.ModificarCiudadDestino = $scope.CargarCiudad(Ruta.CiudadDestino.Codigo)

                if ($scope.ListadoTarifaTarifario.length > 0) {
                    $scope.DeshabilitarTarifas = false
                }
                else {
                    $scope.DeshabilitarTarifas = true
                }
            }
        }

        $scope.CargarTarifasModificar = function (Ruta) {
            $scope.ListadoTarifaTarifario = [];
            if (Ruta !== undefined && Ruta !== "" && Ruta !== null) {
                $scope.Modelo.TarifarioVentas.Tarifas.forEach(function (item) {
                    //Asignar lineas de negocio apartir del tarifario
                    if ($scope.ListadoTarifaTarifario.length == 0) {
                        $scope.ListadoTarifaTransportes.forEach(function (itemTarifa) {
                            if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo && item.Ruta.Codigo == Ruta.Codigo && $scope.Modal.TipoLineaNegocioCarga.Codigo == item.TipoTarifaTransportes.TarifaTransporte.TipoLineaNegocioTransporte.Codigo) {
                                if ($scope.ListadoTarifaTarifario.length == 0) {
                                    $scope.ListadoTarifaTarifario.push(itemTarifa)
                                } else {
                                    var cont = 0
                                    for (var i = 0; i < $scope.ListadoTarifaTarifario.length; i++) {
                                        if ($scope.ListadoTarifaTarifario[i].Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                            cont++
                                        }
                                    } if (cont == 0) {
                                        $scope.ListadoTarifaTarifario.push(itemTarifa)
                                    }
                                }

                            }
                        })
                    }
                    else {
                        var con = 0
                        $scope.ListadoTarifaTarifario.forEach(function (itemTarifa) {
                            if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo && item.Ruta.Codigo == Ruta.Codigo) {
                                con++
                            }
                        })

                        if (con == 0) {
                            var con1 = 0
                            $scope.ListadoTarifaTransportes.forEach(function (itemTarifa) {
                                if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo) {
                                    $scope.ListadoTarifaTarifario.forEach(function (itemDetalle) {
                                        if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemDetalle.Codigo && item.Ruta.Codigo == Ruta.Codigo) {
                                            con1++
                                        }
                                    })
                                    if (con1 == 0) {
                                        if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == itemTarifa.Codigo && item.Ruta.Codigo == Ruta.Codigo) {
                                            $scope.ListadoTarifaTarifario.push(itemTarifa)
                                        }
                                    }
                                }
                            })
                        }
                    }
                })

                if ($scope.ListadoTarifaTarifario.length > 0) {
                    $scope.DeshabilitarTarifas = false
                }
                else {
                    $scope.DeshabilitarTarifas = true
                }
            }
        }

        $scope.CargarTipoTarifas = function (Tarifa) {
            $scope.ListadoTipoTarifaTarifario = [];
            for (var i = 0; i < $scope.Modelo.TarifarioVentas.Tarifas.length; i++) {
                var item = $scope.Modelo.TarifarioVentas.Tarifas[i]
                if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == Tarifa.Codigo && item.TipoLineaNegocioTransportes.Codigo == $scope.Modal.TipoLineaNegocioCarga.Codigo && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.LineaNegocioCarga.Codigo
                    && item.Ruta.Codigo == $scope.Modal.Ruta.Codigo
                ) {
                    if (Tarifa.Codigo !== 6 && Tarifa.Codigo !== 300 && Tarifa.Codigo !== 301 && Tarifa.Codigo !== 302 && Tarifa.Codigo !== 5) {
                        for (var j = 0; j < $scope.ListadoTipoTarifaTransportes.length; j++) {
                            if (item.TipoTarifaTransportes.Codigo == $scope.ListadoTipoTarifaTransportes[j].Codigo && item.TipoTarifaTransportes.TarifaTransporte.Codigo == $scope.ListadoTipoTarifaTransportes[j].TarifaTransporte.Codigo) {
                                item.TipoTarifaTransportes.Nombre = $scope.ListadoTipoTarifaTransportes[j].Nombre
                            }
                        }
                        if (item.TipoTarifaTransportes.Nombre == null) {
                            item.TipoTarifaTransportes.Nombre = '(NO APLICA)'
                        }
                        $scope.ListadoTipoTarifaTarifario.push(item.TipoTarifaTransportes)
                    } else {
                        for (var j = 0; j < $scope.ListadoProductosTransportadosTarifas.length; j++) {
                            if (item.TipoTarifaTransportes.Codigo == $scope.ListadoProductosTransportadosTarifas[j].Codigo) {
                                item.TipoTarifaTransportes.Nombre = $scope.ListadoProductosTransportadosTarifas[j].Nombre
                            }
                        }
                        if (item.TipoTarifaTransportes.Nombre == null) {
                            item.TipoTarifaTransportes.Nombre = '(NO APLICA)'
                        }
                        $scope.ListadoTipoTarifaTarifario.push(item.TipoTarifaTransportes)
                    }
                }
            }

            if ($scope.ListadoTipoTarifaTarifario.length > 0) {
                $scope.DeshabilitarTipoTarifas = false
            }
            else {
                $scope.DeshabilitarTipoTarifas = true
            }

        }

        $scope.CargarTipoTarifasModificar = function (Tarifa) {
            $scope.ListadoTipoTarifaTarifario = [];
            for (var i = 0; i < $scope.Modelo.TarifarioVentas.Tarifas.length; i++) {
                var item = $scope.Modelo.TarifarioVentas.Tarifas[i]
                if (item.TipoTarifaTransportes.TarifaTransporte.Codigo == Tarifa.Codigo && item.TipoLineaNegocioTransportes.Codigo == $scope.Modal.TipoLineaNegocioCarga.Codigo && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.LineaNegocioCarga.Codigo
                    && item.Ruta.Codigo == $scope.Modal.Ruta.Codigo
                ) {
                    if (Tarifa.Codigo !== 6 && Tarifa.Codigo !== 300 && Tarifa.Codigo !== 301 && Tarifa.Codigo !== 302 && Tarifa.Codigo !== 5) {
                        for (var j = 0; j < $scope.ListadoTipoTarifaTransportes.length; j++) {
                            if (item.TipoTarifaTransportes.Codigo == $scope.ListadoTipoTarifaTransportes[j].Codigo && item.TipoTarifaTransportes.TarifaTransporte.Codigo == $scope.ListadoTipoTarifaTransportes[j].TarifaTransporte.Codigo) {
                                item.TipoTarifaTransportes.Nombre = $scope.ListadoTipoTarifaTransportes[j].Nombre
                            }
                        }
                        if (item.TipoTarifaTransportes.Nombre == null) {
                            item.TipoTarifaTransportes.Nombre = '(NO APLICA)'
                        }
                        $scope.ListadoTipoTarifaTarifario.push(item.TipoTarifaTransportes)
                    } else {
                        for (var j = 0; j < $scope.ListadoProductosTransportadosTarifas.length; j++) {
                            if (item.TipoTarifaTransportes.Codigo == $scope.ListadoProductosTransportadosTarifas[j].Codigo) {
                                item.TipoTarifaTransportes.Nombre = $scope.ListadoProductosTransportadosTarifas[j].Nombre
                            }
                        }
                        if (item.TipoTarifaTransportes.Nombre == null) {
                            item.TipoTarifaTransportes.Nombre = '(NO APLICA)'
                        }
                        $scope.ListadoTipoTarifaTarifario.push(item.TipoTarifaTransportes)
                    }
                }
            }
            if ($scope.ListadoTipoTarifaTarifario.length > 0) {
                $scope.DeshabilitarTipoTarifas = false
            }
            else {
                $scope.DeshabilitarTipoTarifas = true
            }
        }

        function limpiarModal() {
            $scope.Modal = {}
            $scope.DeshabilitarTarifas = true
            $scope.DeshabilitarTipoTarifas = true
            $scope.DeshabilitarRutas = true
            $scope.ListadoTipoTarifaTarifario = [];
            $scope.ListadoTarifaTarifario = [];
            $scope.Modal.ModoTransporte = $scope.ListadoModoTransporte[0]
        }

        $scope.GuardarDetalle = function () {
            if (DatosRequeridosDetalle()) {
                $scope.MaskNumero();
                var inserto = true;
                $scope.Modelo.TarifarioVentas.Tarifas.forEach(function (item) {
                    if (item.Ruta.Codigo == $scope.Modal.Ruta.Codigo
                        && item.TipoLineaNegocioTransportes.Codigo == $scope.Modal.TipoLineaNegocioCarga.Codigo
                        && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.LineaNegocioCarga.Codigo
                        && item.TipoTarifaTransportes.Codigo == $scope.Modal.TipoTarifa.Codigo
                        && item.TipoTarifaTransportes.TarifaTransporte.Codigo == $scope.Modal.Tarifa.Codigo) {
                        inserto = false
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_CONTENEDOR) {
                            $scope.Modal.ValorFleteCliente = parseFloat($scope.Modal.Cantidad) * parseInt(item.ValorFlete);
                            inserto = true
                        }
                        if ($scope.Modal.Tarifa.Codigo == TARIFRA_DEVOLUCION_CONTENEDOR) {
                            $scope.Modal.ValorFleteCliente = parseFloat($scope.Modal.Cantidad) * parseInt(item.ValorFlete);
                            inserto = true
                        }
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_CUPO_VEHICULO) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete);
                            inserto = true
                        }
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_CAPACIDAD_VEHICULO) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete);
                            inserto = true
                        }
                        //Peso flete
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_PESO_FLETE) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete) * parseFloat($scope.Modal.Peso);
                            inserto = true
                        }
                        //Peso Barril galon
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_GALON || $scope.Modal.Tarifa.Codigo == TARIFA_BARRIL) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete) * parseInt(MascaraNumero($scope.Modal.Cantidad));
                            inserto = true
                        }
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_DIA || $scope.Modal.Tarifa.Codigo == TARIFA_HORA) {
                            $scope.Modal.ValorFleteCliente = parseFloat($scope.Modal.Cantidad) * parseInt(item.ValorFlete);
                            inserto = true
                        }
                        //Movilización
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_MOVILIZACION) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete);
                            inserto = true
                        }


                        //Tonelada
                        if ($scope.Modal.Tarifa.Codigo == 301) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete) * (parseInt($scope.Modal.Peso) / 100);
                            inserto = true
                        }
                        //Producto
                        if ($scope.Modal.Tarifa.Codigo == 302) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete)
                            inserto = true
                        }
                        //Rango Peso Valor Kilo (SI (PE >= RI Y PE <= RS) ENTONCES  VF = FT * PE)
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_RANGO_PESO_VALOR_KILO) {
                            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_RANGO_PESOS_VALOR_KILO } }).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            for (var i = 0; i < response.data.Datos.length; i++) {
                                                if ($scope.Modal.Peso >= response.data.Datos[i].CampoAuxiliar2 && $scope.Modal.Peso <= response.data.Datos[i].CampoAuxiliar3) {
                                                    $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete) * parseFloat($scope.Modal.Peso);
                                                }
                                            }
                                            if (!inserto) {
                                                $scope.ListaDetalleOrden.push(JSON.parse(JSON.stringify($scope.Modal)));
                                            }
                                        }
                                    }
                                })
                        }
                        //Rango Peso Valor Fijo (SI (PE >= RI Y PE <= RS) ENTONCES  VF = FT)
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_RANGO_PESO_VALOR_FIJO) {
                            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_RANGO_PESOS_VALOR_FIJO } }).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            for (var i = 0; i < response.data.Datos.length; i++) {
                                                if ($scope.Modal.Peso >= response.data.Datos[i].CampoAuxiliar2 && $scope.Modal.Peso <= response.data.Datos[i].CampoAuxiliar3) {
                                                    $scope.Modal.ValorFleteCliente = item.ValorFlete;
                                                }
                                            }
                                            if (!inserto) {
                                                $scope.ListaDetalleOrden.push(JSON.parse(JSON.stringify($scope.Modal)));
                                            }
                                        }
                                    }
                                });
                        }
                    }
                });
                if (inserto) {
                    $scope.ListaDetalleOrden.push(JSON.parse(JSON.stringify($scope.Modal)))

                }
                closeModal('ModalAdicionarDetalle')
            }
        }

        function DatosRequeridosDetalle() {
            $scope.MensajesError = []
            $scope.MensajesErrorDetalle = []
            MostrarModalDetalle = true
            if ($scope.Modal.TipoLineaNegocioCarga == undefined || $scope.Modal.TipoLineaNegocioCarga == null || $scope.Modal.TipoLineaNegocioCarga == "") {
                $scope.MensajesError.push('Debe ingresar el tipo línea de negocio')
                MostrarModalDetalle = false
            }
            if ($scope.Modal.Ruta == undefined || $scope.Modal.Ruta == null || $scope.Modal.Ruta == "" || $scope.Modal.Ruta.Codigo == 0) {
                $scope.MensajesErrorDetalle.push('Debe ingresar la ruta')
                MostrarModalDetalle = false
            }
            if ($scope.Modal.Tarifa == undefined || $scope.Modal.Tarifa == null || $scope.Modal.Tarifa == "" || $scope.Modal.Tarifa.Codigo == 0) {
                $scope.MensajesErrorDetalle.push('Debe ingresar la tarifa')
                MostrarModalDetalle = false
            }
            if ($scope.Modal.TipoTarifa == undefined || $scope.Modal.TipoTarifa == null || $scope.Modal.TipoTarifa == "" || ($scope.Modal.TipoTarifa.Codigo == 0 && $scope.Modal.Tarifa.Codigo !== 5)) {
                $scope.MensajesErrorDetalle.push('Debe ingresar el tipo de tarifa')
                MostrarModalDetalle = false
            }
            if ($scope.Modal.Destinatario == undefined || $scope.Modal.Destinatario == null || $scope.Modal.Destinatario == "" || $scope.Modal.Destinatario.Codigo == 0) {
                $scope.MensajesErrorDetalle.push('Debe ingresar el destinatario')
                MostrarModalDetalle = false
            }
            if ($scope.Modal.Producto == undefined || $scope.Modal.Producto == null || $scope.Modal.Producto == "" || $scope.Modal.Producto.Codigo == 0) {
                $scope.MensajesErrorDetalle.push('Debe ingresar el producto')
                MostrarModalDetalle = false
            }
            if ($scope.Modal.Cantidad == undefined || $scope.Modal.Cantidad == null || $scope.Modal.Cantidad == "") {
                $scope.MensajesErrorDetalle.push('Debe ingresar la cantidad')
                MostrarModalDetalle = false
            }
            if ($scope.Modal.Peso == undefined || $scope.Modal.Peso == null || $scope.Modal.Peso == "") {
                $scope.MensajesErrorDetalle.push('Debe ingresar el peso')
                MostrarModalDetalle = false
            }
            if ($scope.Modal.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_IMPORTACION ||
                $scope.Modal.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_EXTEMPORANEO ||
                $scope.Modal.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_DTA ||
                $scope.Modal.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_OTM) {

                if ($scope.Modal.ValorFOB == undefined || $scope.Modal.ValorFOB == null || $scope.Modal.ValorFOB == "") {
                    $scope.MensajesErrorDetalle.push('Debe ingresar el Valor FOB')
                    MostrarModalDetalle = false
                }
            }
            if ($scope.Modal.CiudadCargue == undefined || $scope.Modal.CiudadCargue == null || $scope.Modal.CiudadCargue == "" || $scope.Modal.CiudadCargue.Codigo == 0) {
                $scope.MensajesErrorDetalle.push('Debe ingresar la ciudad de cargue')
                MostrarModalDetalle = false
            }
            if ($scope.Modal.DireccionCargue == undefined || $scope.Modal.DireccionCargue == null || $scope.Modal.DireccionCargue == "") {
                $scope.MensajesErrorDetalle.push('Debe ingresar la dirección de cargue')
                MostrarModalDetalle = false
            }
            if ($scope.Modal.CiudadDestino == undefined || $scope.Modal.CiudadDestino == null || $scope.Modal.CiudadDestino == "" || $scope.Modal.CiudadDestino.Codigo == 0) {
                $scope.MensajesErrorDetalle.push('Debe ingresar la ciudad de destino')
                MostrarModalDetalle = false
            }
            if ($scope.Modal.DireccionDestino == undefined || $scope.Modal.DireccionDestino == null || $scope.Modal.DireccionDestino == "") {
                $scope.MensajesErrorDetalle.push('Debe ingresar la dirección de destino')
                MostrarModalDetalle = false
            }
            if ($scope.Modelo.LineaNegocioCarga.Codigo == 2) {
                if ($scope.ListaDetalleOrden.length == 1) {
                    $scope.MensajesError.push("Solo se permite un detalle para la línea de negocio semimasivo")
                }
            }
            return MostrarModalDetalle
        }

        $scope.ModificarListaDetalle = function () {
            if (DatosRequeridosModificarDetalle()) {
                $scope.MaskNumero();
                var inserto = true;
                $scope.Modelo.TarifarioVentas.Tarifas.forEach(function (item) {
                    if (item.Ruta.Codigo == $scope.Modal.Ruta.Codigo
                        && item.TipoLineaNegocioTransportes.Codigo == $scope.Modal.TipoLineaNegocioCarga.Codigo
                        && item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == $scope.Modelo.LineaNegocioCarga.Codigo
                        && item.TipoTarifaTransportes.Codigo == $scope.Modal.TipoTarifa.Codigo
                        && item.TipoTarifaTransportes.TarifaTransporte.Codigo == $scope.Modal.Tarifa.Codigo) {
                        inserto = false;
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_CONTENEDOR) {
                            $scope.Modal.ValorFleteCliente = parseFloat(MascaraDecimales($scope.Modal.Cantidad)) * parseInt(item.ValorFlete);
                            inserto = true
                        }
                        if ($scope.Modal.Tarifa.Codigo == TARIFRA_DEVOLUCION_CONTENEDOR) {
                            $scope.Modal.ValorFleteCliente = parseFloat(MascaraDecimales($scope.Modal.Cantidad)) * parseInt(item.ValorFlete);
                            inserto = true
                        }
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_CUPO_VEHICULO) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete);
                            inserto = true
                        }
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_CAPACIDAD_VEHICULO) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete);
                            inserto = true
                        }
                        //Peso flete
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_PESO_FLETE) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete) * parseFloat($scope.Modal.Peso);
                            inserto = true
                        }
                        //Peso Barril galon
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_GALON || $scope.Modal.Tarifa.Codigo == TARIFA_BARRIL) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete) * parseInt(MascaraNumero($scope.Modal.Cantidad));
                            inserto = true
                        }
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_DIA || $scope.Modal.Tarifa.Codigo == TARIFA_HORA) {
                            $scope.Modal.ValorFleteCliente = parseFloat($scope.Modal.Cantidad) * parseInt(item.ValorFlete);
                            inserto = true
                        }
                        //Movilización
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_MOVILIZACION) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete);
                            inserto = true
                        }


                        //Tonelada
                        if ($scope.Modal.Tarifa.Codigo == 301) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete) * (parseInt($scope.Modal.Peso) / 100);
                            inserto = true
                        }
                        //Producto
                        if ($scope.Modal.Tarifa.Codigo == 302) {
                            $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete)
                            inserto = true
                        }
                        //Rango Peso Valor Kilo (SI (PE >= RI Y PE <= RS) ENTONCES  VF = FT * PE)
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_RANGO_PESO_VALOR_KILO) {
                            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_RANGO_PESOS_VALOR_KILO } }).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            for (var i = 0; i < response.data.Datos.length; i++) {
                                                if ($scope.Modal.Peso >= response.data.Datos[i].CampoAuxiliar2 && $scope.Modal.Peso <= response.data.Datos[i].CampoAuxiliar3) {
                                                    $scope.Modal.ValorFleteCliente = parseInt(item.ValorFlete) * parseFloat($scope.Modal.Peso);
                                                }
                                            }
                                            if (!inserto) {
                                                $scope.ListaDetalleOrden.push(JSON.parse(JSON.stringify($scope.Modal)));
                                            }
                                        }
                                    }
                                })
                        }
                        //Rango Peso Valor Fijo (SI (PE >= RI Y PE <= RS) ENTONCES  VF = FT)
                        if ($scope.Modal.Tarifa.Codigo == TARIFA_RANGO_PESO_VALOR_FIJO) {
                            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_RANGO_PESOS_VALOR_FIJO } }).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        if (response.data.Datos.length > 0) {
                                            for (var i = 0; i < response.data.Datos.length; i++) {
                                                if ($scope.Modal.Peso >= response.data.Datos[i].CampoAuxiliar2 && $scope.Modal.Peso <= response.data.Datos[i].CampoAuxiliar3) {
                                                    $scope.Modal.ValorFleteCliente = item.ValorFlete;
                                                }
                                            }
                                            if (!inserto) {
                                                $scope.ListaDetalleOrden.push(JSON.parse(JSON.stringify($scope.Modal)));
                                            }
                                        }
                                    }
                                });
                        }
                        //$scope.Modal.ValorFleteCliente = item.ValorFlete
                    }
                });
                if (inserto) {
                    $scope.ListaDetalleOrden[$scope.Indice] = JSON.parse(JSON.stringify($scope.Modal))
                }
                $scope.Buscando = false;
                $scope.ResultadoSinRegistros = '';

                closeModal('ModalModificarDetalle')
            }
        }

        function DatosRequeridosModificarDetalle() {
            $scope.MensajesError = []
            $scope.MensajesErrorModificarDetalle = []
            MostrarModalModificarDetalle = true
            if ($scope.Modal.TipoLineaNegocioCarga == undefined || $scope.Modal.TipoLineaNegocioCarga == null || $scope.Modal.TipoLineaNegocioCarga == "") {
                $scope.MensajesErrorModificarDetalle.push('Debe ingresar el tipo línea de negocio')
                MostrarModalModificarDetalle = false
            }

            if ($scope.Modal.Ruta == undefined || $scope.Modal.Ruta == null || $scope.Modal.Ruta == "" || $scope.Modal.Ruta.Codigo == 0) {
                $scope.MensajesErrorModificarDetalle.push('Debe ingresar la ruta')
                MostrarModalModificarDetalle = false
            }
            if ($scope.Modal.Tarifa == undefined || $scope.Modal.Tarifa == null || $scope.Modal.Tarifa == "" || $scope.Modal.Tarifa.Codigo == 0) {
                $scope.MensajesErrorModificarDetalle.push('Debe ingresar la tarifa')
                MostrarModalModificarDetalle = false
            }
            if ($scope.Modal.TipoTarifa == undefined || $scope.Modal.TipoTarifa == null || $scope.Modal.TipoTarifa == "" || ($scope.Modal.TipoTarifa.Codigo == 0 && $scope.Modal.Tarifa.Codigo !== 5)) {
                $scope.MensajesErrorModificarDetalle.push('Debe ingresar el tipo de tarifa')
                MostrarModalModificarDetalle = false
            }
            if ($scope.Modal.Destinatario == undefined || $scope.Modal.Destinatario == null || $scope.Modal.Destinatario == "" || $scope.Modal.Destinatario.Codigo == 0) {
                $scope.MensajesErrorModificarDetalle.push('Debe ingresar el destinatario')
                MostrarModalModificarDetalle = false
            }
            if ($scope.Modal.Producto == undefined || $scope.Modal.Producto == null || $scope.Modal.Producto == "" || $scope.Modal.Producto.Codigo == 0) {
                $scope.MensajesErrorModificarDetalle.push('Debe ingresar el producto')
                MostrarModalModificarDetalle = false
            }
            if ($scope.Modal.Cantidad == undefined || $scope.Modal.Cantidad == null || $scope.Modal.Cantidad == "") {
                $scope.MensajesErrorModificarDetalle.push('Debe ingresar la cantidad')
                MostrarModalModificarDetalle = false
            }
            if ($scope.Modal.Peso == undefined || $scope.Modal.Peso == null || $scope.Modal.Peso == "") {
                $scope.MensajesErrorModificarDetalle.push('Debe ingresar el peso')
                MostrarModalModificarDetalle = false
            }
            if ($scope.Modal.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_IMPORTACION ||
                $scope.Modal.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_EXTEMPORANEO ||
                $scope.Modal.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_DTA ||
                $scope.Modal.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_OTM) {

                if ($scope.Modal.ValorFOB == undefined || $scope.Modal.ValorFOB == null || $scope.Modal.ValorFOB == "") {
                    $scope.MensajesErrorModificarDetalle.push('Debe ingresar el Valor FOB')
                    MostrarModalModificarDetalle = false
                }
            }
            if ($scope.Modal.CiudadCargue == undefined || $scope.Modal.CiudadCargue == null || $scope.Modal.CiudadCargue == "" || $scope.Modal.CiudadCargue.Codigo == 0) {
                $scope.MensajesErrorModificarDetalle.push('Debe ingresar la ciudad de cargue')
                MostrarModalModificarDetalle = false
            }
            if ($scope.Modal.DireccionCargue == undefined || $scope.Modal.DireccionCargue == null || $scope.Modal.DireccionCargue == "") {
                $scope.MensajesErrorModificarDetalle.push('Debe ingresar la dirección de cargue')
                MostrarModalModificarDetalle = false
            }
            if ($scope.Modal.CiudadDestino == undefined || $scope.Modal.CiudadDestino == null || $scope.Modal.CiudadDestino == "" || $scope.Modal.CiudadDestino.Codigo == 0) {
                $scope.MensajesErrorModificarDetalle.push('Debe ingresar la ciudad de destino')
                MostrarModalModificarDetalle = false
            }
            if ($scope.Modal.DireccionDestino == undefined || $scope.Modal.DireccionDestino == null || $scope.Modal.DireccionDestino == "") {
                $scope.MensajesErrorModificarDetalle.push('Debe ingresar la dirección de destino')
                MostrarModalModificarDetalle = false
            }
            if ($scope.Modelo.LineaNegocioCarga.Codigo == 2) {
                if ($scope.ListaDetalleOrden.length == 1) {
                    $scope.MensajesError.push("Solo se permite un detalle para la línea de negocio semimasivo")
                }
            }
            return MostrarModalModificarDetalle
        }

        $scope.EliminarDetalle = function (index, item) {
            $scope.ListaDetalleOrden.splice(index, 1)
            $scope.Buscando = false;
            $scope.ResultadoSinRegistros = '';
        }


        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true

            if ($scope.Modelo.Fecha == undefined || $scope.Modelo.Fecha == null || $scope.Modelo.Fecha == "") {
                $scope.MensajesError.push('Debe ingresar la fecha')
                continuar = false
            }
            if ($scope.Modelo.Cliente == undefined || $scope.Modelo.Cliente == null || $scope.Modelo.Cliente == "" || $scope.Modelo.Cliente.Codigo == 0) {
                $scope.MensajesError.push('Debe ingresar un cliente')
                continuar = false
            }
            if ($scope.Modelo.LineaNegocioCarga == undefined || $scope.Modelo.LineaNegocioCarga == null || $scope.Modelo.LineaNegocioCarga == "") {
                $scope.MensajesError.push('Debe ingresar la línea de negocio')
                continuar = false
            }
            if ($scope.Modelo.TipoLineaNegocioCarga == undefined || $scope.Modelo.TipoLineaNegocioCarga == null || $scope.Modelo.TipoLineaNegocioCarga == "") {
                $scope.MensajesError.push('Debe ingresar el tipo línea de negocio')
                continuar = false
            }
            if ($scope.Modelo.Facturar == undefined || $scope.Modelo.Facturar == null || $scope.Modelo.Facturar == "") {
                $scope.MensajesError.push('Debe ingresar el facturar A')
                continuar = false
            }
            if ($scope.Modelo.Comercial == undefined || $scope.Modelo.Comercial == null || $scope.Modelo.Comercial == "") {
                $scope.MensajesError.push('Debe ingresar el comercial')
                continuar = false
            }
            if ($scope.Modelo.Destinatario == undefined || $scope.Modelo.Destinatario == null || $scope.Modelo.Destinatario == "") {
                $scope.MensajesError.push('Debe ingresar el destinatario')
                continuar = false
            }
            if (($scope.Modelo.FechaCargue == undefined || $scope.Modelo.FechaCargue == null || $scope.Modelo.FechaCargue == "") && !(($scope.Modelo.TipoDespacho.Codigo == 18202 || $scope.Modelo.TipoDespacho.Codigo == 18203) && $scope.VisualizacionCamposProgramacion)) {
                $scope.MensajesError.push('Debe ingresar la fecha de cargue')
                continuar = false
            }
            if ($scope.Modelo.CiudadCargue == undefined || $scope.Modelo.CiudadCargue == null || $scope.Modelo.CiudadCargue == "") {
                $scope.MensajesError.push('Debe ingresar la ciudad de cargue')
                continuar = false
            }
            if ($scope.Modelo.DireccionCargue == undefined || $scope.Modelo.DireccionCargue == null || $scope.Modelo.DireccionCargue == "") {
                $scope.MensajesError.push('Debe ingresar la dirección de cargue')
                continuar = false
            }
            if ($scope.Modelo.AplicaPoliza == true) {
                if ($scope.ListaPolizas.length == 0) {
                    $scope.MensajesError.push('Debe ingresar mínimo una póliza ')
                    continuar = false
                }
            }

            if ($scope.ListaDetalleOrden.length == 0 && $scope.Modelo.TipoDespacho.Codigo == 18201) {
                $scope.MensajesError.push('Debe ingresar mínimo un detalle')
                continuar = false
            } else if (($scope.Modelo.TipoDespacho.Codigo == 18202 || $scope.Modelo.TipoDespacho.Codigo == 18203) && $scope.VisualizacionCamposProgramacion) {


                if ($scope.Modelo.SitioCargue == undefined || $scope.Modelo.SitioCargue == null || $scope.Modelo.SitioCargue == "") {
                    $scope.MensajesError.push('Debe ingresar el sitio de cargue')
                    continuar = false
                }
                if ($scope.Modelo.CiudadDescargue == undefined || $scope.Modelo.CiudadDescargue == null || $scope.Modelo.CiudadDescargue == "") {
                    $scope.MensajesError.push('Debe ingresar la ciudad de descargue')
                    continuar = false
                }
                if ($scope.Modelo.SitioDescargue == undefined || $scope.Modelo.SitioDescargue == null || $scope.Modelo.SitioDescargue == "") {
                    $scope.MensajesError.push('Debe ingresar el sitio de descargue')
                    continuar = false
                }
                if ($scope.Modelo.DireccionDescargue == undefined || $scope.Modelo.DireccionDescargue == null || $scope.Modelo.DireccionDescargue == "") {
                    $scope.MensajesError.push('Debe ingresar la dirección de descargue')
                    continuar = false
                }
                if ($scope.Modelo.Ruta == undefined || $scope.Modelo.Ruta == null || $scope.Modelo.Ruta == "") {
                    $scope.MensajesError.push('Debe ingresar la Ruta')
                    continuar = false
                }
                if (($scope.Modelo.Producto == undefined || $scope.Modelo.Producto == null || $scope.Modelo.Producto == "") && $scope.Modelo.TipoDespacho.Codigo == 18202) {
                    $scope.MensajesError.push('Debe ingresar el Producto')
                    continuar = false
                }
                if (($scope.Modelo.Tarifa == undefined || $scope.Modelo.Tarifa == null || $scope.Modelo.Tarifa == "") && $scope.Modelo.TipoDespacho.Codigo == 18203) {
                    $scope.MensajesError.push('Debe ingresar la tarifa')
                    continuar = false
                }
                if ($scope.Modelo.TipoVehiculo == undefined || $scope.Modelo.TipoVehiculo == null || $scope.Modelo.TipoVehiculo == "") {
                    $scope.MensajesError.push('Debe ingresar el tipo de vehículo')
                    continuar = false
                }
            }
            if ($scope.Modelo.TipoDespacho.Codigo != 18201 && ($scope.Modelo.ValorTipoDespacho == undefined || $scope.Modelo.ValorTipoDespacho == '' || $scope.Modelo.ValorTipoDespacho == 0)) {
                $scope.MensajesError.push('Debe ingresar el cupo')
                continuar = false
            }

            if ($scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_IMPORTACION ||
                $scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_EXTEMPORANEO ||
                $scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_DTA ||
                $scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_OTM) {

                //if ($scope.ModalImportacion.Naviera == undefined || $scope.ModalImportacion.Naviera == null || $scope.ModalImportacion.Naviera == "" || $scope.ModalImportacion.Naviera.Codigo == 0) {
                //    $scope.MensajesError.push('Debe ingresar la naviera')
                //    continuar = false
                //}
                //if ($scope.ModalImportacion.ETAMotonave == undefined || $scope.ModalImportacion.ETAMotonave == null || $scope.ModalImportacion.ETAMotonave == "") {
                //    $scope.MensajesError.push('Debe ingresar la ETA motonave')
                //    continuar = false
                //}
                //if ($scope.ModalImportacion.Motonave == undefined || $scope.ModalImportacion.Motonave == null) {
                //    $scope.ModalImportacion.Motonave = ""
                //}
                //if ($scope.ModalImportacion.FechaDTA == undefined || $scope.ModalImportacion.FechaDTA == null || $scope.ModalImportacion.FechaDTA == "") {
                //    $scope.MensajesError.push('Debe ingresar la Fecha DTA')
                //    continuar = false
                //}
                //if ($scope.ModalImportacion.PaisOrigen == undefined || $scope.ModalImportacion.PaisOrigen == null || $scope.ModalImportacion.PaisOrigen == "" || $scope.ModalImportacion.PaisOrigen.Codigo == 0) {
                //    $scope.MensajesError.push('Debe ingresar el pais de origen')
                //    continuar = false
                //}
                //if ($scope.ModalImportacion.PuertoPaisOrigen == undefined || $scope.ModalImportacion.PuertoPaisOrigen == null || $scope.ModalImportacion.PuertoPaisOrigen == "" || $scope.ModalImportacion.PuertoPaisOrigen.Codigo == 0) {
                //    $scope.MensajesError.push('Debe ingresar el puerto de origen')
                //    continuar = false
                //}
                //if ($scope.ModalImportacion.ContactoOrigen == undefined || $scope.ModalImportacion.ContactoOrigen == null) {
                //    $scope.ModalImportacion.ContactoOrigen = ""
                //}
                //if ($scope.ModalImportacion.PaisDestino == undefined || $scope.ModalImportacion.PaisDestino == null || $scope.ModalImportacion.PaisDestino == "" || $scope.ModalImportacion.PaisDestino.Codigo == 0) {
                //    $scope.MensajesError.push('Debe ingresar el pais de destino')
                //    continuar = false
                //}
                //if ($scope.ModalImportacion.PuertoPaisDestino == undefined || $scope.ModalImportacion.PuertoPaisDestino == null || $scope.ModalImportacion.PuertoPaisDestino == "" || $scope.ModalImportacion.PuertoPaisDestino.Codigo == 0) {
                //    $scope.MensajesError.push('Debe ingresar el puerto de destino')
                //    continuar = false
                //}
                //if ($scope.ModalImportacion.ContactoDestino == undefined || $scope.ModalImportacion.ContactoDestino == null) {
                //    $scope.ModalImportacion.ContactoDestino = ""
                //}
                //if ($scope.ModalImportacion.FechaCutOffDocumental == undefined || $scope.ModalImportacion.FechaCutOffDocumental == null || $scope.ModalImportacion.FechaCutOffDocumental == "") {
                //    $scope.MensajesError.push('Debe ingresar la fecha CutOff Documental')
                //    continuar = false
                //}
                //if ($scope.ModalImportacion.FechaCutOffFisico == undefined || $scope.ModalImportacion.FechaCutOffFisico == null || $scope.ModalImportacion.FechaCutOffFisico == "") {
                //    $scope.MensajesError.push('Debe ingresar la fecha CutOff fisico')
                //    continuar = false
                //}
                //Datos requeridos documentos
                $scope.DetalleDocumentos = []
                for (var i = 0; i < $scope.ListaDocumentos.length; i++) {
                    var documento = $scope.ListaDocumentos[i]

                    if (documento.EntidadDocumento.Codigo == 4) {
                        if (documento.AplicaReferencia == 2) {
                            if (documento.Referencia === undefined || documento.Referencia === '') {
                                $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                        if (documento.AplicaEmisor == 2) {
                            if (documento.Emisor === undefined || documento.Emisor === '') {
                                $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Documento.Nombre + ')');
                                continuar = false;
                            }
                        }
                        if (documento.AplicaFechaEmision == 2) {
                            if (documento.FechaEmision === undefined || documento.FechaEmision === '') {
                                $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Documento.Nombre + ')');
                                continuar = false;
                                var f = new Date();
                                if (documento.FechaEmision > f) {
                                    $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                                    continuar = false;
                                }
                            }
                        }
                        if (documento.AplicaFechaVencimiento == 2) {
                            if (documento.FechaVence === undefined || documento.FechaVence === '') {
                                $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Documento.Nombre + ')');
                                continuar = false;
                            } else {
                                var f = new Date();
                                if (documento.FechaVence < f) {
                                    $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                                    continuar = false;
                                }
                            }
                        }
                        if (documento.CodigosDocumentos == 1 || documento.Referencia != undefined || documento.Emisor != undefined || documento.FechaEmision != undefined || documento.FechaVence != undefined) {
                            $scope.DetalleDocumentos.push(
                                {
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    TIDOCodigo: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
                                    CDGDCodigo: documento.Codigo,
                                    Referencia: documento.Referencia,
                                    Emisor: documento.Emisor,
                                    FechaEmision: documento.FechaEmision,
                                    FechaVence: documento.FechaVence,
                                }
                            )
                        }
                    }
                }
                //---------------------------------------------------------------------------------------------
            }
            if (!$scope.ValidarSaldoCliente()) {
                continuar = false;
            }
            return continuar
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando orden servicio');

            $timeout(function () {
                blockUI.message('Cargando orden servicio');
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_SOLICITUD_ORDEN_SERVICIO,
                ConsularDocumentos: 1,
            };

            blockUI.delay = 1000;
            EncabezadoSolicitudOrdenServiciosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        //Obtener datos documentos
                        $scope.ListaDocumentos.forEach(function (item) {
                            response.data.Datos.ListadoDocumentos.forEach(function (entidad) {

                                if (item.Codigo == entidad.CDGDCodigo && entidad.Extension != '') {
                                    item.CodigosDocumentos = 1
                                    $scope.Numero = entidad.Numero

                                    if (entidad.Referencia == 0) {
                                        item.Referencia = '';
                                    } else {
                                        item.Referencia = entidad.Referencia
                                    }
                                    if (entidad.Emisor == 0) {
                                        item.Emisor = '';
                                    } else {
                                        item.Emisor = entidad.Emisor
                                    }
                                    if (new Date(entidad.FechaEmision) < MIN_DATE) {
                                        item.FechaEmision = ''
                                    } else {
                                        item.FechaEmision = new Date(entidad.FechaEmision)
                                    }
                                    if (new Date(entidad.FechaVence) < MIN_DATE) {
                                        item.FechaVence = ''
                                    } else {
                                        item.FechaVence = new Date(entidad.FechaVence)
                                    }
                                    item.temp = false;
                                } else if (item.Codigo == entidad.CDGDCodigo) {
                                    $scope.Numero = entidad.Numero
                                    if (entidad.Referencia == 0) {
                                        item.Referencia = '';
                                    } else {
                                        item.Referencia = entidad.Referencia
                                    }
                                    if (entidad.Emisor == 0) {
                                        item.Emisor = '';
                                    } else {
                                        item.Emisor = entidad.Emisor
                                    }
                                    if (new Date(entidad.FechaEmision) < MIN_DATE) {
                                        item.FechaEmision = ''
                                    } else {
                                        item.FechaEmision = new Date(entidad.FechaEmision)
                                    }
                                    if (new Date(entidad.FechaVence) < MIN_DATE) {
                                        item.FechaVence = ''
                                    } else {
                                        item.FechaVence = new Date(entidad.FechaVence)
                                    }
                                    item.temp = false;
                                }
                            })
                        })
                        //----------------------------------------------------------------------------------
                        $scope.Modelo = response.data.Datos

                        $scope.Modelo.Fecha = RetornarFechaEspecifica(new Date(response.data.Datos.Fecha));
                        $scope.Modelo.FechaCargue = new Date(response.data.Datos.FechaCargue);
                        $scope.Modelo.FechaFinCargue = new Date(response.data.Datos.FechaFinCargue);

                        $scope.CargarDireccionesClienteCargue($scope.Modelo.Cliente, $scope.Modelo.CiudadCargue);

                        //$scope.CodigoOficina = response.data.Datos.OficinaDespacha.Codigo
                        if ($scope.ListadoOficinas.length > 0) {
                            $scope.Modelo.OficinaDespacha = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + response.data.Datos.OficinaDespacha.Codigo);
                        } else {
                            $scope.CodigoOficina = response.data.Datos.OficinaDespacha.Codigo
                        }
                        if ($scope.ListadoTipoDespacho.length > 0) {
                            $scope.Modelo.TipoDespacho = $linq.Enumerable().From($scope.ListadoTipoDespacho).First('$.Codigo == ' + response.data.Datos.TipoDespacho.Codigo);
                        } else {
                            $scope.CodigoTipoDespacho = response.data.Datos.TipoDespacho.Codigo
                        }

                        if ($scope.ListadoUnidadEmpaque.length > 0) {
                            try {
                                $scope.Modelo.UnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo == ' + response.data.Datos.UnidadEmpaque.Codigo);

                            } catch (e) {

                            }
                        } else {
                            $scope.CodigoUnidadEmpaque = response.data.Datos.UnidadEmpaque.Codigo
                        }
                        if ($scope.ListadoNovedadesCumplimiento.length > 0) {
                            try {

                                $scope.Modelo.NovedadCalidad = $linq.Enumerable().From($scope.ListadoNovedadesCumplimiento).First('$.Codigo == ' + response.data.Datos.NovedadCalidad.Codigo);
                            } catch (e) {

                            }
                        } else {
                            $scope.CodigoNovedadCalidad = response.data.Datos.NovedadCalidad.Codigo
                        }
                        if ($scope.ListadoTipoCargue.length > 0) {
                            try {

                                $scope.Modelo.TipoCargue = $linq.Enumerable().From($scope.ListadoTipoCargue).First('$.Codigo == ' + response.data.Datos.TipoCargue.Codigo);
                            } catch (e) {

                            }
                        } else {
                            $scope.CodigoTipoCargue = response.data.Datos.TipoCargue.Codigo
                        }
                        if ($scope.ListadoTipoVehiculo.length > 0) {
                            try {

                                $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo == ' + response.data.Datos.TipoVehiculo.Codigo);
                            } catch (e) {

                            }
                        } else {
                            $scope.CodigoTipoVehiculo = response.data.Datos.TipoVehiculo.Codigo
                        }

                        $scope.Modelo.Cliente = $scope.CargarTercero(response.data.Datos.Cliente.Codigo)
                        $scope.Modelo.ValorTipoDespacho = MascaraValores($scope.Modelo.ValorTipoDespacho)
                        $scope.Modelo.SaldoTipoDespacho = MascaraValores($scope.Modelo.SaldoTipoDespacho)
                        $scope.Modelo.ValorDeclarado = MascaraValores($scope.Modelo.ValorDeclarado)
                        $scope.Modelo.Facturar = $scope.CargarTercero(response.data.Datos.Facturar.Codigo)
                        $scope.Modelo.Comercial = $scope.CargarTercero(response.data.Datos.Comercial.Codigo)
                        $scope.Modelo.Destinatario = $scope.CargarTercero(response.data.Datos.Destinatario.Codigo)
                        $scope.Modelo.Remitente = $scope.CargarTercero(response.data.Datos.Remitente.Codigo)
                        $scope.Modelo.CiudadCargue = $scope.CargarCiudad(response.data.Datos.CiudadCargue.Codigo)
                        $scope.Modelo.CiudadDescargue = $scope.CargarCiudad(response.data.Datos.CiudadDescargue.Codigo)
                        $scope.Modelo.Producto = $scope.CargarProducto($scope.Modelo.Producto.Codigo)
                        if (response.data.Datos.FacturarDespachar == 1) {
                            $scope.Modelo.FacturarDespachar = true
                        }
                        else {
                            $scope.Modelo.FacturarDespachar = false
                        }
                        if (response.data.Datos.FacturarCantidadPeso == 1) {
                            $scope.Modelo.FacturarCantidadPeso = true
                        }
                        else {
                            $scope.Modelo.FacturarCantidadPeso = false
                        }
                        if (response.data.Datos.AplicaPoliza == 1) {
                            $scope.Modelo.AplicaPoliza = true
                            $scope.HabilitarPoliza = true
                        }
                        else {
                            $scope.Modelo.AplicaPoliza = false
                            $scope.HabilitarPoliza = false
                        }
                        if (response.data.Datos.FleteXCantidad == 1) {
                            $scope.Modelo.FleteXCantidad = true
                        }
                        else {
                            $scope.Modelo.FleteXCantidad = false
                        }
                        if (response.data.Datos.InformacionCompleta == 1) {
                            $scope.Modelo.InformacionCompleta = true
                        }
                        else {
                            $scope.Modelo.InformacionCompleta = false
                        }

                        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        //if (response.data.Datos.Estado == 0) {
                        //    $scope.Deshabilitar = false;
                        //}
                        //else if (response.data.Datos.Estado == 1) {

                        //}
                        //if (response.data.Datos.Anulado == 1) {
                        //    $scope.Deshabilitar = true;
                        //    $scope.DeshabilitarActualizar = true
                        //    $scope.DeshabilitarTipoLineaNegocio = true
                        //    $scope.DeshabilitarLineaNegocio = true
                        //    $scope.DeshabilitarPuertoDestino = true
                        //    $scope.DeshabilitarPuertoOrigen = true

                        //    $scope.ListadoEstados.push({ Nombre: "ANULADO", Codigo: -1 })
                        //    $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
                        //}


                        $scope.CodigoLineaNegocioCarga = response.data.Datos.LineaNegocioCarga.Codigo
                        $scope.CodigoTipoLineaNegocioCarga = response.data.Datos.TipoLineaNegocioCarga.Codigo

                        $scope.CargarTarifarioCliente(response.data.Datos.Cliente)


                        $scope.ListaDetalleOrden = $scope.Modelo.Detalle
                        for (var i = 0; i < $scope.ListaDetalleOrden.length; i++) {
                            try {
                                $scope.ListaDetalleOrden[i].SitioDevolucionContenedor.SitioCliente = JSON.parse(JSON.stringify($scope.ListaDetalleOrden[i].SitioDevolucionContenedor))
                            } catch (e) {
                            }
                        }
                        $scope.ListaConceptosOrden = $scope.Modelo.Conceptos

                        $scope.ListaPolizas = $scope.Modelo.Polizas

                        $scope.ListaPolizas.forEach(function (item) {
                            if (item.ExigeEscolta == 1) {
                                item.ExigeEscolta = "SI"
                            }
                            else {
                                item.ExigeEscolta = "NO"
                            }
                        })

                        $scope.ModalImportacion = $scope.Modelo.ImportacionExportacion

                        if ($scope.ModalImportacion !== null && $scope.ModalImportacion !== undefined && $scope.ModalImportacion !== '') {
                            $scope.ModalImportacion.ETAMotonave = new Date($scope.ModalImportacion.ETAMotonave);
                            $scope.ModalImportacion.FechaDTA = new Date($scope.ModalImportacion.FechaDTA);
                            $scope.ModalImportacion.FechaCutOffDocumental = new Date($scope.ModalImportacion.FechaCutOffDocumental);
                            $scope.ModalImportacion.FechaCutOffFisico = new Date($scope.ModalImportacion.FechaCutOffFisico);
                            $scope.ModalImportacion.ValorDeclarado = MascaraValores($scope.ModalImportacion.ValorDeclarado);
                            $scope.HabilitarImportacionExportacion = true

                            $scope.ValidarPuertoPaisOrigen($scope.ModalImportacion.PaisOrigen)

                            $scope.ValidarPuertoPaisDestino($scope.ModalImportacion.PaisDestino)

                            if ($scope.ModalImportacion.LiberacionAutomaticaFletes == 1) {
                                $scope.ModalImportacion.LiberacionAutomaticaFletes = true
                            }
                            else {
                                $scope.ModalImportacion.LiberacionAutomaticaFletes = false
                            }
                            if ($scope.ModalImportacion.ExoneracionPagoDeposito == 1) {
                                $scope.ModalImportacion.ExoneracionPagoDeposito = true
                            }
                            else {
                                $scope.ModalImportacion.ExoneracionPagoDeposito = false
                            }
                            if ($scope.ModalImportacion.ExoneracionPagoDropOFF == 1) {
                                $scope.ModalImportacion.ExoneracionPagoDropOFF = true
                            }
                            else {
                                $scope.ModalImportacion.ExoneracionPagoDropOFF = false
                            }
                            if ($scope.ModalImportacion.ClientePagaFleteMaritimoRuta == 1) {
                                $scope.ModalImportacion.ClientePagaFleteMaritimoRuta = true
                            }
                            else {
                                $scope.ModalImportacion.ClientePagaFleteMaritimoRuta = false
                            }
                            if ($scope.ModalImportacion.ClientePagoEmisionManejo == 1) {
                                $scope.ModalImportacion.ClientePagoEmisionManejo = true
                            }
                            else {
                                $scope.ModalImportacion.ClientePagoEmisionManejo = false
                            }
                            if ($scope.ModalImportacion.ClientePagaLiquidacionComodato == 1) {
                                $scope.ModalImportacion.ClientePagaLiquidacionComodato = true
                            }
                            else {
                                $scope.ModalImportacion.ClientePagaLiquidacionComodato = false
                            }
                            if ($scope.ModalImportacion.DevolucionContenedor == 1) {
                                $scope.ModalImportacion.DevolucionContenedor = true
                            }
                            else {
                                $scope.ModalImportacion.DevolucionContenedor = false
                            }
                            $scope.ModalImportacion.CiudadDevolucion = $scope.CargarCiudad($scope.ModalImportacion.CiudadDevolucion.Codigo)

                            if ($scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_IMPORTACION ||
                                $scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_EXTEMPORANEO ||
                                $scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_DTA ||
                                $scope.Modelo.TipoLineaNegocioCarga.Codigo == CODIGO_TIPO_LINEA_NEGOCIO_OTM) {
                                $scope.HabilitarImportacionExportacion = true

                                $scope.CodigoNaviera = $scope.ModalImportacion.Naviera.Codigo
                                if ($scope.ListadoNavieras.length > 0 && $scope.CodigoNaviera > 0) {
                                    $scope.ModalImportacion.Naviera = $linq.Enumerable().From($scope.ListadoNavieras).First('$.Codigo == ' + $scope.ModalImportacion.Naviera.Codigo);
                                }


                            }
                            else {
                                $scope.HabilitarImportacionExportacion = false
                            }

                        }
                        else {
                            $scope.HabilitarImportacionExportacion = false
                        }

                        if ($scope.ListaPolizas !== null && $scope.ListaPolizas !== undefined && $scope.ListaPolizas !== '' && $scope.ListaPolizas.length > 0) {
                            $scope.ListaPolizas.forEach(function (item) {
                                if (item.ExigeEscolta == 1) {
                                    item.ExigeEscolta = "SI"
                                }
                                else {
                                    item.ExigeEscolta = "NO"
                                }
                            })
                        }
                    }
                    else {
                        ShowError('No se logro consultar la orden servicio ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarOrdenservicio';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarOrdenservicio';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                $scope.Modelo.Estado = $scope.Estado.Codigo

                if ($scope.Modelo.FacturarDespachar == true) {
                    $scope.Modelo.FacturarDespachar = 1
                }
                else {
                    $scope.Modelo.FacturarDespachar = 0
                }

                if ($scope.Modelo.FacturarCantidadPeso == true) {
                    $scope.Modelo.FacturarCantidadPeso = 1
                }
                else {
                    $scope.Modelo.FacturarCantidadPeso = 0
                }

                if ($scope.Modelo.AplicaPoliza == true) {
                    $scope.Modelo.AplicaPoliza = 1
                }
                else {
                    $scope.Modelo.AplicaPoliza = 0
                }
                if ($scope.Modelo.FleteXCantidad == true) {
                    $scope.Modelo.FleteXCantidad = 1
                }
                else {
                    $scope.Modelo.FleteXCantidad = 0
                }
                if ($scope.Modelo.InformacionCompleta == true) {
                    $scope.Modelo.InformacionCompleta = 1
                }
                else {
                    $scope.Modelo.InformacionCompleta = 0
                }
                $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                $scope.Modelo.Oficina = $scope.Sesion.UsuarioAutenticado.Oficinas
                $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO
                for (var i = 0; i < $scope.ListaDetalleOrden.length; i++) {
                    try {
                        $scope.ListaDetalleOrden[i].SitioDevolucionContenedor = JSON.parse(JSON.stringify($scope.ListaDetalleOrden[i].SitioDevolucionContenedor.SitioCliente))
                    } catch (e) {
                    }
                }
                $scope.Modelo.Detalle = JSON.parse(JSON.stringify($scope.ListaDetalleOrden))

                $scope.ListaPolizas.forEach(function (item) {
                    if (item.ExigeEscolta == "SI") {
                        item.ExigeEscolta = 1
                    }
                    else {
                        item.ExigeEscolta = 0
                    }
                })
                if ($scope.ModalImportacion !== null && $scope.ModalImportacion !== undefined && $scope.ModalImportacion !== '') {
                    if ($scope.ModalImportacion.LiberacionAutomaticaFletes == true) {
                        $scope.ModalImportacion.LiberacionAutomaticaFletes = 1
                    }
                    else {
                        $scope.ModalImportacion.LiberacionAutomaticaFletes = 0
                    }
                    if ($scope.ModalImportacion.ExoneracionPagoDeposito == true) {
                        $scope.ModalImportacion.ExoneracionPagoDeposito = 1
                    }
                    else {
                        $scope.ModalImportacion.ExoneracionPagoDeposito = 0
                    }
                    if ($scope.ModalImportacion.ExoneracionPagoDropOFF == true) {
                        $scope.ModalImportacion.ExoneracionPagoDropOFF = 1
                    }
                    else {
                        $scope.ModalImportacion.ExoneracionPagoDropOFF = 0
                    }
                    if ($scope.ModalImportacion.ClientePagaFleteMaritimoRuta == true) {
                        $scope.ModalImportacion.ClientePagaFleteMaritimoRuta = 1
                    }
                    else {
                        $scope.ModalImportacion.ClientePagaFleteMaritimoRuta = 0
                    }
                    if ($scope.ModalImportacion.ClientePagoEmisionManejo == true) {
                        $scope.ModalImportacion.ClientePagoEmisionManejo = 1
                    }
                    else {
                        $scope.ModalImportacion.ClientePagoEmisionManejo = 0
                    }
                    if ($scope.ModalImportacion.ClientePagaLiquidacionComodato == true) {
                        $scope.ModalImportacion.ClientePagaLiquidacionComodato = 1
                    }
                    else {
                        $scope.ModalImportacion.ClientePagaLiquidacionComodato = 0
                    }

                    if ($scope.ModalImportacion.DevolucionContenedor == true) {
                        $scope.ModalImportacion.DevolucionContenedor = 1
                    }
                    else {
                        $scope.ModalImportacion.DevolucionContenedor = 0
                    }

                    if ($scope.Modelo.TipoLineaNegocioCarga.Codigo !== CODIGO_TIPO_LINEA_NEGOCIO_IMPORTACION &&
                        $scope.Modelo.TipoLineaNegocioCarga.Codigo !== CODIGO_TIPO_LINEA_NEGOCIO_EXTEMPORANEO &&
                        $scope.Modelo.TipoLineaNegocioCarga.Codigo !== CODIGO_TIPO_LINEA_NEGOCIO_DTA &&
                        $scope.Modelo.TipoLineaNegocioCarga.Codigo !== CODIGO_TIPO_LINEA_NEGOCIO_OTM) {
                        $scope.ModalImportacion = {}
                    }



                    $scope.Modelo.ImportacionExportacion = $scope.ModalImportacion
                }
                $scope.Modelo.Polizas = $scope.ListaPolizas
                $scope.Modelo.Conceptos = $scope.ListaConceptosOrden
                //Guardar detalle documentos
                $scope.Modelo.Documentos = $scope.DetalleDocumentos
                $scope.Modelo.SolicitudOrdenServicioNumero = $scope.Modelo.Numero;
                $scope.Modelo.Numero = CERO
                EncabezadoSolicitudOrdenServiciosFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Numero == 0) {
                                    ShowSuccess('Se guardó la orden de servicio ' + response.data.Datos);
                                    location.href = '#!ConsultarOrdenservicio/' + response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se Modificó la orden de servicio ' + $scope.Modelo.NumeroDocumento);
                                    location.href = '#!ConsultarOrdenservicio/' + $scope.Modelo.NumeroDocumento;
                                }
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            if (response.data.Datos == -1) {
                                ShowError('No se pudo guardar la orden de servicio debido a que ya existe una con el mismo documento cliente y sede');

                            } else {
                                ShowError(response.statusText);

                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        $scope.AdicionarConcepto = function () {
            $scope.MensajesErrorConceptos = [];
            $scope.ModalConcepto = {}
            $scope.ModalConcepto.ConceptoVentas = ''
            showModal('ModalAdicionarConcepto');
        }
        $scope.EliminarConcepto = function (index, item) {
            $scope.ListaConceptosOrden.splice(index, 1)
        }
        $scope.GuardarConcepto = function () {
            var con = 0
            if ($scope.ListaConceptosOrden.length > 0) {
                $scope.MensajesErrorConceptos = [];
                $scope.ListaConceptosOrden.forEach(function (item) {
                    if (item.ConceptoVentas.Codigo == $scope.ModalConcepto.ConceptoVentas.Codigo) {
                        $scope.MensajesErrorConceptos.push('El concepto ' + $scope.ModalConcepto.ConceptoVentas.Nombre + ' ya se encuentra registrado.')
                        con++
                    }
                })
            }
            if (con == 0) {
                if (DatosRequeridosConcepto()) {
                    $scope.MaskNumero()
                    $scope.ListaConceptosOrden.push($scope.ModalConcepto)
                    closeModal('ModalAdicionarConcepto')
                }
            }
        }

        function DatosRequeridosConcepto() {
            $scope.MensajesErrorConceptos = []
            var continuar = true
            if ($scope.ModalConcepto.ConceptoVentas == undefined || $scope.ModalConcepto.ConceptoVentas == null || $scope.ModalConcepto.ConceptoVentas == "" || $scope.ModalConcepto.ConceptoVentas.Codigo == 0) {
                $scope.MensajesErrorConceptos.push('Debe seleccionar un concepto')
                continuar = false
            }

            if ($scope.ModalConcepto.Descripcion == undefined || $scope.ModalConcepto.Descripcion == null || $scope.ModalConcepto.Descripcion == "") {
                $scope.MensajesErrorConceptos.push('Debe ingresar la descripción')
                continuar = false
            }
            if ($scope.ModalConcepto.Valor == undefined || $scope.ModalConcepto.Valor == null || $scope.ModalConcepto.Valor == "") {
                $scope.MensajesErrorConceptos.push('Debe ingresar el valor')
                continuar = false
            }
            return continuar
        }

        $scope.AdicionarPoliza = function () {
            $scope.MensajesErrorPoliza = [];
            $scope.ModalPoliza = {}
            $scope.ModalPoliza.TipoPolizaOrdenServicio = $scope.ListadoTipoPoliza[0]
            $scope.ModalPoliza.HorarioAutorizado = $scope.ListadoHorarioPolizas[0]
            showModal('ModalAdicionarPoliza');
        }
        $scope.EliminarPoliza = function (index, item) {
            $scope.ListaPolizas.splice(index, 1)
        }
        $scope.GuardarPoliza = function () {
            var con = 0
            if ($scope.ListaPolizas.length > 0) {

                $scope.MensajesErrorPoliza = [];
                $scope.ListaPolizas.forEach(function (item) {
                    if (item.Poliza.Codigo == $scope.ModalPoliza.Poliza.Codigo && item.TipoPolizaOrdenServicio.Codigo == $scope.ModalPoliza.TipoPolizaOrdenServicio.Codigo && item.HorarioAutorizado.Codigo == $scope.ModalPoliza.HorarioAutorizado.Codigo) {
                        $scope.MensajesErrorPoliza.push('La aseguradora ya se encuentra registrada')
                        con++
                    }
                })
            }
            if (con == 0) {
                if (DatosRequeridosPoliza()) {
                    $scope.MaskNumero()

                    if ($scope.ModalPoliza.ExigeEscolta == true) {
                        $scope.ModalPoliza.ExigeEscolta = "SI"

                    }
                    else {
                        $scope.ModalPoliza.ExigeEscolta = "NO"
                    }
                    $scope.ListaPolizas.push($scope.ModalPoliza)
                    closeModal('ModalAdicionarPoliza')
                }
            }
        }


        function DatosRequeridosPoliza() {
            $scope.MensajesErrorPoliza = []
            var continuar = true
            if ($scope.ModalPoliza.Poliza == undefined || $scope.ModalPoliza.Poliza == null || $scope.ModalConcepto.Poliza == "" || $scope.ModalPoliza.Poliza.Codigo == 0) {
                $scope.MensajesErrorPoliza.push('Debe ingresar una aseguradora')
                continuar = false
            }

            if ($scope.ModalPoliza.TipoPolizaOrdenServicio == undefined || $scope.ModalPoliza.TipoPolizaOrdenServicio == null || $scope.ModalPoliza.TipoPolizaOrdenServicio == "" || $scope.ModalPoliza.TipoPolizaOrdenServicio.Codigo == 10100) {
                $scope.MensajesErrorPoliza.push('Debe seleccionar un tipo')
                continuar = false
            }
            if ($scope.ModalPoliza.HorarioAutorizado == undefined || $scope.ModalPoliza.HorarioAutorizado == null || $scope.ModalPoliza.HorarioAutorizado == "" || $scope.ModalPoliza.HorarioAutorizado.Codigo == 10200) {
                $scope.MensajesErrorPoliza.push('Debe seleccionar un horario')
                continuar = false
            }
            if ($scope.ModalPoliza.ValorCobertura == undefined || $scope.ModalPoliza.ValorCobertura == null || $scope.ModalPoliza.ValorCobertura == "") {
                $scope.MensajesErrorPoliza.push('Debe ingresar el valor de cobertura')
                continuar = false
            }
            if ($scope.ModalPoliza.ValorDeducible == undefined || $scope.ModalPoliza.ValorDeducible == null || $scope.ModalPoliza.ValorDeducible == "") {
                $scope.MensajesErrorPoliza.push('Debe ingresar el valor de deducible')
                continuar = false
            }
            if ($scope.ModalPoliza.EdadVehiculos == undefined || $scope.ModalPoliza.EdadVehiculos == null || $scope.ModalPoliza.EdadVehiculos == "") {
                $scope.MensajesErrorPoliza.push('Debe ingresar la edad del vehículo')
                continuar = false
            }
            return continuar
        }

        /*---------------------------------------------------------------------------------------Funciones Asignar y Eliminar Documentos------------------------------------------------------------------------------------------------*/
        $scope.ElimiarDocumentoReemplazado = function (Eliminar) {
            $scope.EliminarDocumento = Eliminar.CodigosDocumentos
            $scope.LimpiarBotones = Eliminar.Codigo
        }

        $scope.DatosDocumentos = function (item) {
            $scope.CodigoDocumento = item.CodigosDocumentos;
            TIDOCodigo: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
                $scope.CDGDCodigo = item.Codigo;
            $scope.Referencia = item.Referencia;
            $scope.Emisor = item.Emisor;
            $scope.FechaEmision = item.FechaEmision;
            $scope.FechaVence = item.FechaVence;
            $scope.NombreDocumento = item.Nombre;
        }

        $scope.ValidarReemplazo = function (item) {
            $scope.valor = item;
            closeModal('modalConfirmacionReemplazarDocumento');

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TIDOCodigo: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
                Numero: $scope.Numero,
                CDGDCodigo: $scope.LimpiarBotones
            };

            GestionDocumentalDocumentosFactory.EliminarDocumentoDefinitivo(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            $scope.reader = new FileReader();
            $scope.reader.onload = $scope.ReemplazarDocumento;
            $scope.reader.readAsDataURL($scope.element.files[0]);

        };

        $scope.CargarDocumento = function (element) {

            var continuar = true;
            $scope.TipoImagen = element
            blockUI.start();
            $scope.NombreDocumento = element.files[0].name
            if (element === undefined) {
                ShowError("Solo se pueden cargar archivos en formato jpeg, png  o pdf", false);
                continuar = false;
            }
            if (continuar == true) {
                if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png' && element.files[0].type !== 'application/pdf') {
                    ShowError("Solo se pueden cargar archivos en formato jpeg, png  o pdf", false);
                    continuar = false;
                }
                if (element.files[0].type == 'application/pdf') {

                    if (element.files[0].size >= 4000000) { //4 MB
                        ShowError("El documento " + element.files[0].name + " supera los 4 MB, intente con otro documento", false);
                        continuar = false;
                    }
                }
            }
            if (continuar == true) {
                if ($scope.CodigoDocumento != undefined && $scope.CodigoDocumento != '' && $scope.CodigoDocumento != null) {
                    showModal('modalConfirmacionReemplazarDocumento');
                    document.getElementById('mensajearchivoreemplazar').innerHTML = '¿Está seguro de reemplazar el documento que se encuentra guardado?'
                    if ($scope.valor == 1) {
                        $scope.reader = new FileReader();
                        $scope.reader.onload = $scope.ReemplazarDocumento;
                        $scope.reader.readAsDataURL(element.files[0]);
                    } else {
                        $scope.element = element;
                    }
                } else {
                    var reader = new FileReader();
                    reader.onload = $scope.AsignarDocumento;
                    reader.readAsDataURL(element.files[0]);
                }
            }
            blockUI.stop();
        }

        $scope.ReemplazarDocumento = function (e) {
            $scope.$apply(function () {
                $scope.ConvertirDocumento = null
                var string = ''
                var TipoImagen = ''
                if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
                    var img = new Image()
                    img.src = e.target.result
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        TipoImagen = 'JPEG'
                        $scope.TipoImagen = TipoImagen
                        AsignarDocumentoListado()
                    }, 100)
                }
                else if ($scope.TipoImagen.files[0].type == 'image/png') {
                    var img = new Image()
                    img.src = e.target.result
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        TipoImagen = 'PNG'
                        $scope.TipoImagen = TipoImagen
                        AsignarDocumentoListado()
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
                    $scope.ConvertirDocumento = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                    TipoImagen = 'PDF'
                    $scope.TipoImagen = TipoImagen
                    AsignarDocumentoListado()
                }
            });
        }

        $scope.AsignarDocumento = function (e) {
            $scope.$apply(function () {
                $scope.ConvertirDocumento = null
                var string = ''
                var TipoImagen = ''
                if ($scope.TipoImagen.files[0].type == 'image/jpeg') {
                    var img = new Image()
                    img.src = e.target.result
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '')
                        }
                        TipoImagen = 'JPEG'
                        $scope.TipoImagen = TipoImagen
                        AsignarDocumentoListado()
                    }, 100)
                }
                else if ($scope.TipoImagen.files[0].type == 'image/png') {
                    var img = new Image()
                    img.src = e.target.result
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img)
                            $scope.ConvertirDocumento = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '')
                        }
                        TipoImagen = 'PNG'
                        $scope.TipoImagen = TipoImagen
                        AsignarDocumentoListado()
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type == 'application/pdf') {
                    $scope.ConvertirDocumento = e.target.result.replace(/data:application\/pdf;base64,/g, '')
                    TipoImagen = 'PDF'
                    $scope.TipoImagen = TipoImagen
                    AsignarDocumentoListado()
                }
            });
        }

        function AsignarDocumentoListado() {
            var documento = {
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
                TIDOCodigo: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
                CDGDCodigo: $scope.CDGDCodigo,
                Referencia: $scope.Referencia,
                Emisor: $scope.Emisor,
                FechaEmision: $scope.FechaEmision,
                FechaVence: $scope.FechaVence,
                Archivo: $scope.ConvertirDocumento,
                NombreDocumento: $scope.NombreDocumento,
                Extension: $scope.TipoImagen
            }
            // Guardar documento temporal
            GestionDocumentalDocumentosFactory.InsertarTemporal(documento).
                then(function (response) {
                    if (response.data.Datos > 0) {
                        // Se guardó correctamente el documento
                        $scope.ListaDocumentos.forEach(function (item) {
                            if (item.Codigo == $scope.CDGDCodigo) {
                                item.CodigosDocumentos = 1;
                                item.temp = true;
                            }
                        })
                    } else {
                        ShowError(response.data.MensajeError);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.ElimiarDocumento = function (EliminarDocumento) {
            $scope.LimpiarBotones = EliminarDocumento.Codigo
            showModal('modalConfirmacionEliminarDocumento');
            document.getElementById('mensajearchivo').innerHTML = '¿Está seguro de eliminar el documento?'
        };

        $scope.BorrarDocumento = function () {
            closeModal('modalConfirmacionEliminarDocumento');
            var entidad = {
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TIDOCodigo: CODIGO_TIPO_DOCUMENTO_ORDEN_SERVICIO,
                Numero: $scope.Numero,
                CDGDCodigo: $scope.LimpiarBotones
            };

            GestionDocumentalDocumentosFactory.EliminarDocumento(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaDocumentos.forEach(function (item) {
                            if (item.Codigo == $scope.LimpiarBotones) {
                                item.CodigosDocumentos = 0;
                            }
                        })
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            GestionDocumentalDocumentosFactory.EliminarDocumentoDefinitivo(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        }

        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarDocumento = function (item) {
            if (item.temp) {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Orden_Servicio&CDGD_Codigo=' + item.Codigo + '&EMPR_Codigo=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Numero=' + $scope.Numero + '&Temp=1');
            }
            else {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Orden_Servicio&CDGD_Codigo=' + item.Codigo + '&EMPR_Codigo=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&Numero=' + $scope.Numero + '&Temp=0');
            }
        };

        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarGenerarOrdenServicio/' + $scope.Modelo.NumeroDocumento;
        };

        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
            try { $scope.Modelo.NombreCorto = $scope.Modelo.NombreCorto.toUpperCase() } catch (e) { }
            //MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.TelefonoCargue = MascaraNumero($scope.Modelo.TelefonoCargue) } catch (e) { }
            try { $scope.Modal.TelefonoCargue = MascaraNumero($scope.Modal.TelefonoCargue) } catch (e) { }
            try { $scope.Modal.TelefonoDestino = MascaraNumero($scope.Modal.TelefonoDestino) } catch (e) { }
            try { $scope.ModalConcepto.Valor = MascaraNumero($scope.ModalConcepto.Valor) } catch (e) { }
            try { $scope.ModalPoliza.EdadVehiculos = MascaraNumero($scope.ModalPoliza.EdadVehiculos) } catch (e) { }
            try { $scope.ModalPoliza.ValorCobertura = MascaraNumero($scope.ModalPoliza.ValorCobertura) } catch (e) { }
            try { $scope.ModalPoliza.ValorDeducible = MascaraNumero($scope.ModalPoliza.ValorDeducible) } catch (e) { }
            try { $scope.ModalPoliza.ValorDeducible = MascaraNumero($scope.ModalPoliza.ValorDeducible) } catch (e) { }
            try { $scope.ModalImportacion.DiasLibresContenedor = MascaraNumero($scope.ModalImportacion.DiasLibresContenedor) } catch (e) { }
        };

        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };

        function RedimVertical(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasVertical');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 499, 699);
            }
        }
        function RedimHorizontal(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasHorizontal');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 699, 499);
            }
        }
        $scope.ValidarPoliza = function () {
            if ($scope.Sesion.ValorPoliza > 0) {
                if (parseInt(MascaraNumero($scope.Modelo.ValorDeclarado)) > $scope.Sesion.ValorPoliza) {
                    ShowError('El valor declarado excede el valor de la poliza de la empresa cuyo valor es de ' + MascaraValores($scope.Sesion.ValorPoliza))
                    $scope.Modelo.ValorDeclarado = 0
                }

            }
        }
    }]);