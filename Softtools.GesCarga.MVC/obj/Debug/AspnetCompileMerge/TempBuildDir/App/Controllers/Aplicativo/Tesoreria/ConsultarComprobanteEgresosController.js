﻿SofttoolsApp.controller("ConsultarComprobanteEgresosCtrl", ['$scope', '$routeParams', '$linq', 'blockUI', '$timeout', 'ValorCatalogosFactory', 'OficinasFactory', 'DocumentoComprobantesFactory', 'EmpresasFactory', 'EncabezadoComprobantesContablesFactory',
    function ($scope, $routeParams, $linq, blockUI, $timeout, ValorCatalogosFactory, OficinasFactory, DocumentoComprobantesFactory, EmpresasFactory, EncabezadoComprobantesContablesFactory) {
        /*-----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- */

        $scope.MapaSitio = [{ Nombre: 'Tesoreria' }, { Nombre: 'Documentos' }, { Nombre: 'Comprobante de Egreso' }];

        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = ''; 
        $scope.Deshabilitar = false;
        $scope.MensajesError = [];
        var filtros = {};
        $scope.ListadoDocumentosOrigen = [];
        $scope.ListadoOficinas = [];
        $scope.ListadoComprobanteEgreso = [];

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 },
            { Nombre: 'PENDIENTE APROBACIÓN', Codigo: 3 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.FechaFinal = new Date();
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.pref = '';
        $scope.totalPaginas = 0;
        $scope.FechaInicio = new Date();
        $scope.FechaFin = new Date();
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_COMPROBANTE_EGRESOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaFin = new Date();
        }
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ListadoOficinas = []

        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item)
                        });
                        $scope.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                $scope.Oficina = $scope.ListadoOficinas[0]
            } else {
                ShowError('El usuario no tiene oficinas asociadas')
            }
        }
        /*Cargar el combo de Documenteos Origen*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoDocumentosOrigen = response.data.Datos;
                    $scope.DocumentoOrigen = response.data.Datos[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarComprobanteEgresos';
            }
        };
        /*---------------------------------------------------------------------Funcion Buscar Comprobantes de egreso-----------------------------------------------------------------------------*/
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                Find()
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoComprobanteEgreso = [];



            if (DatosRequeridos()) {
                filtros = {
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.NumeroDocumento,
                    FechaInicial: $scope.NumeroDocumento > 0 ? undefined : $scope.FechaInicio,
                    FechaFinal: $scope.NumeroDocumento > 0 ? undefined : $scope.FechaFin,
                    Estado: $scope.Estado.Codigo,
                    NombreBeneficiario: $scope.Beneficiario,
                    NombreTercero: $scope.Tercero,
                    NumeroDocumentoOrigen: $scope.NumeroDocumentoOrigen,
                    DocumentoOrigen: $scope.DocumentoOrigen,
                    OficinaDestino: $scope.Oficina,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                    UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
                };
                console.log('Buscar',filtros)
                DocumentoComprobantesFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                /*se ordena el listado del Grid*/
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                                
                                $scope.ListadoComprobanteEgreso = $linq.Enumerable().From(listado).OrderBy(function (x) {
                                    return x.Numero
                                }).ToArray();
                                /*----------------------------*/
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.NumeroDocumento === null || $scope.NumeroDocumento === undefined || $scope.NumeroDocumento === '' || $scope.NumeroDocumento === 0 || isNaN($scope.NumeroDocumento) === true)
                && ($scope.NumeroDocumentoOrigen == null || $scope.NumeroDocumentoOrigen == undefined || $scope.NumeroDocumentoOrigen == '' || $scope.NumeroDocumentoOrigen == 0 || isNaN($scope.NumeroDocumentoOrigen) === true)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas, número o número documento origen');
                continuar = false

            } else if (($scope.NumeroDocumento !== null && $scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '' && $scope.NumeroDocumento !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')

            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio
                    } else {
                        $scope.FechaInicio = $scope.FechaFin
                    }
                }
            }
            return continuar
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        /*Validacion boton anular */
        $scope.ConfirmacionAnularComprobante = function (numero, Anulado, Oficina, Codigo) {

            if ($scope.ValidarPermisos.AplicaEliminarAnular == PERMISO_ACTIVO) {
                $scope.NumeroAnulacion = numero;
                $scope.OficinaAnula = Oficina
                $scope.Codigo = Codigo
                if (Anulado !== 1) {
                    showModal('modalConfirmacionAnularComprobante');
                }
                else {
                    closeModal('modalConfirmacionAnularComprobante');
                }
            }
        };
        /*Funcion que solicita los datos de la anulación */
        $scope.SolicitarDatosAnulacion = function () {
            $scope.FechaAnulacion = new Date();
            closeModal('modalConfirmacionAnularComprobante');
            showModal('modalDatosAnularComprobante');
        };
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });
        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroDocumento = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_COMPROBANTE_EGRESO;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';

        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroDocumento != undefined && $scope.NumeroDocumento != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroDocumento;
            }

        }
        /*---------------------------------------------------------------------Funcion Anular Comprobante----------------------------------------------------------------------------*/
        $scope.Anular = function () {

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                Numero: $scope.NumeroAnulacion,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnulacion: $scope.CausaAnulacion,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_COMPROBANTE_EGRESO,
                OficinaAnula: $scope.OficinaAnula
            };

            DocumentoComprobantesFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se anuló el comprobante de egreso No.' + entidad.Numero);
                        closeModal('modalDatosAnularComprobante');
                        Find();
                    }
                    else {
                        ShowError('No se puede anular el comprobante dado que esta asociado a una legalización gastos');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };
        /* Obtener parametros*/
        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            $scope.NumeroDocumento = $routeParams.Numero;
            Find();
        }
        /*---------------------------------------------------------------------------------------------------Máscaras-----------------------------------------------------------------------------*/

        $scope.MaskNumero = function () {
            try { $scope.NumeroDocumento = MascaraNumero($scope.NumeroDocumento) } catch (e) { };
            try { $scope.NumeroDocumentoOrigen = MascaraNumero($scope.NumeroDocumentoOrigen) } catch (e) { };

        };
        $scope.GenerarMovimientoContable = function (NumeroDocumento) {
            EncabezadoComprobantesContablesFactory.Guardar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroDocumento: NumeroDocumento, TipoDocumentoOrigen: { Codigo: 30 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('El movimiento contable se regeneró correctamente')
                    } else {
                        ShowError('No se generó el movimiento contable, por favor verifique la parametrización ')
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
    }]);