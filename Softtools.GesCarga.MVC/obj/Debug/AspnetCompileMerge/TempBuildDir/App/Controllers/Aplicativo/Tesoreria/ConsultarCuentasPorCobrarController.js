﻿SofttoolsApp.controller("ConsultarCuentasPorCobrarCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'blockUIConfig', 'EmpresasFactory', 'ValorCatalogosFactory',
    'TercerosFactory', 'CuentasPorCobrarFactory', 'OficinasFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUI, blockUIConfig, EmpresasFactory, ValorCatalogosFactory,
        TercerosFactory, CuentasPorCobrarFactory, OficinasFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CONSULTAR CUENTAS POR COBRAR';
        $scope.MapaSitio = [{ Nombre: 'Tesorería' }, { Nombre: 'Documentos' }, { Nombre: 'Cuentas Por Cobrar' }];
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ===' + OPCION_MENU_TESORERIA.CUENTAS_POR_COBRAR); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.Deshabilitar = false;
        $scope.MensajesError = [];
        $scope.pref = '';
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
        };

        $scope.ListadoDocumentoOrigen = [];

        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR },
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==-1');

        $scope.ListadoEstadoCuenta = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'VIGENTE', Codigo: 0 },
            { Nombre: 'CANCELADA', Codigo: 1 }
        ];
        $scope.Modelo.EstadoCuenta = $linq.Enumerable().From($scope.ListadoEstadoCuenta).First('$.Codigo ==-1');

        $scope.ListadoTerceros = [];
        $scope.ListadoOficinas = [];
        //--Observaciones
        $scope.Model = {};
        $scope.ListadoDetalleObservaciones = [];
        $scope.MensajesErrorObser = [];
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy === true) {
            $scope.FechaFinal = new Date();
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------AutoComplete
        //--Clientes
        $scope.AutocompleteTercero = function (value) {
            if (value.length > 0) {
                if (value.length >= MIN_LEN_AUTOCOMLETE) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Estado: { Codigo: ESTADO_DEFINITIVO },
                        Sync: true
                    });
                    $scope.ListadoTerceros = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTerceros);
                }
            }
            return $scope.ListadoTerceros;
        };
        //--Clientes
        //----------AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //--Documento Origen
            var ListaDocumentoOrigen = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN },
                Estado: ESTADO_ACTIVO,
                CampoAuxiliar4: 'CXC',
                Sync: true
            }).Datos;
            $scope.ListadoDocumentoOrigen.push({ Codigo: -1, Nombre: "(TODAS)" });
            $scope.ListadoDocumentoOrigen = $scope.ListadoDocumentoOrigen.concat(ListaDocumentoOrigen);
            $scope.Modelo.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentoOrigen).First('$.Codigo ==-1');
            //--Oficina
            //--Oficinas
            var responseOficina = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_DEFINITIVO, Sync: true });
            if (responseOficina.ProcesoExitoso) {
                $scope.ListadoOficinas.push({ Codigo: -1, Nombre: '(TODAS)' });
                responseOficina.Datos.forEach(function (item) {
                    $scope.ListadoOficinas.push(item);
                });
                $scope.Modelo.Oficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo === -1');
            }

            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                $scope.ListadoOficinas = $scope.ListadoOficinas;
            }
            else {
                $scope.ListadoOficinas.push({
                    Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                    Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre
                });
                $scope.Modelo.Oficina = $scope.ListadoOficinasActual[0];
            }
            //--Obtener Parametros
            if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== '0') {
                $scope.Modelo.NumeroInicial = $routeParams.Numero;
                $scope.Modelo.FechaInicial = null;
                PantallaBloqueo(Find);
            }
        };
        //----------Init
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //----FUNCION DE PAGINACIÓN Y IMPRIMIR
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            PantallaBloqueo(Find);
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                PantallaBloqueo(Find);
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            PantallaBloqueo(Find);
        };
        //----Paginacion
        //--Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarCuentasPorCobrar';
            }
        };
        //--Funcion Buscar
        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO && DatosRequeridos()) {
                $scope.Codigo = 0;
                PantallaBloqueo(Find);
            }
        };
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                && ($scope.Modelo.NumeroInicial === null || $scope.Modelo.NumeroInicial === undefined || $scope.Modelo.NumeroInicial === '' || $scope.Modelo.NumeroInicial === 0 || isNaN($scope.Modelo.NumeroInicial) === true)
                && ($scope.Modelo.NumeroDocumentoOrigen === null || $scope.Modelo.NumeroDocumentoOrigen === undefined || $scope.Modelo.NumeroDocumentoOrigen === '' || $scope.Modelo.NumeroDocumentoOrigen === 0 || isNaN($scope.Modelo.NumeroDocumentoOrigen) === true)
                && ($scope.Modelo.Tercero == undefined || $scope.Modelo.Tercero == null || $scope.Modelo.Tercero == '')
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas, número ,número documento o tercero');
                continuar = false;

            } else if (($scope.Modelo.NumeroInicial !== null && $scope.Modelo.NumeroInicial !== undefined && $scope.Modelo.NumeroInicial !== '' && $scope.Modelo.NumeroInicial !== 0)
                || ($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')

            ) {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                }

                else {
                    if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial;
                    } else {
                        $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal;
                    }
                }
            }
            return continuar;
        }
        function Find() {
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoCuentas = [];
            var filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.NumeroInicial,
                FechaInicial: $scope.Modelo.NumeroInicial > 0 ? undefined : $scope.Modelo.FechaInicial,
                FechaFinal: $scope.Modelo.NumeroInicial > 0 ? undefined : $scope.Modelo.FechaFinal,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR,
                Oficina: $scope.Modelo.Oficina,
                DocumentoOrigen: $scope.Modelo.DocumentoOrigen,
                NumeroDocumentoOrigen: $scope.Modelo.NumeroDocumentoOrigen,
                Tercero: $scope.Modelo.Tercero,
                Estado: $scope.Modelo.Estado.Codigo,
                CreadaManual: -1,
                EstadoCuenta: $scope.Modelo.EstadoCuenta.Codigo,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina
            };
            CuentasPorCobrarFactory.Consultar(filtro).
                then(function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Codigo = 0;
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCuentas = response.data.Datos;
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                        var listado = [];
                        response.data.Datos.forEach(function (item) {
                            listado.push(item);
                        });
                    }
                }, function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    ShowError(response.statusText);
                });

        }

        //--Limpiar AutoCompletes
        $scope.limpiarControl = function (Control) {
            switch (Control) {
                case "Tercero":
                    $scope.Modelo.Tercero = "";
                    break;
            }
        };
        //--Limpiar Autocompletes
        //--Gestion Observaciones
        function DatosRequeridosObs() {
            var continuar = true;
            $scope.MensajesErrorObser = [];
            if ($scope.Model.Observaciones == undefined || $scope.Model.Observaciones == '' || $scope.Model.Observaciones == null) {
                $scope.MensajesErrorObser.push("Debe digitar una observación");
                continuar = false;
            }
            return continuar;
        }
        $scope.GestionObservaciones = function (item) {
            showModal('modalObservaciones');
            $scope.ListadoDetalleObservaciones = [];
            $scope.Model = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Numero,
                Codigo: item.Codigo,
                Sync: true
            };
            var response = CuentasPorCobrarFactory.ConsultarDetalleObservaciones($scope.Model);
            if (response.ProcesoExitoso) {
                $scope.ListadoDetalleObservaciones = response.Datos.DetalleObservaciones;
            }
            else {
                ShowError(response.statusText);
            }
        };
        $scope.AgregarTmpObservaciones = function () {
            if (DatosRequeridosObs()) {
                $scope.ListadoDetalleObservaciones.push({
                    FechaCrea: new Date(),
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo, Nombre: $scope.Sesion.UsuarioAutenticado.Nombre },
                    Observaciones: $scope.Model.Observaciones,
                    Eliminar : CODIGO_UNO
                });
                $scope.Model = {
                    Numero: $scope.Model.Numero,
                    Codigo: $scope.Model.Codigo
                };
            }
        };
        $scope.RemoverObservacion = function (index) {
            $scope.ListadoDetalleObservaciones.splice(index, 1);
        };
        $scope.ConfirmarAgregarObservacion = function(){
            showModal('modalConfirmacionGuardar');
        };
        $scope.AgregarObservacion = function () {
            closeModal('modalConfirmacionGuardar');
            if ($scope.ListadoDetalleObservaciones.length > 0) {
                var cuenta = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Numero: $scope.Model.Numero,
                    Codigo: $scope.Model.Codigo,
                    Sync: true
                };
                var DetalleObs = [];
                for (var i = 0; i < $scope.ListadoDetalleObservaciones.length; i++) {
                    DetalleObs.push({
                        Observaciones: $scope.ListadoDetalleObservaciones[i].Observaciones,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    });
                }
                cuenta.DetalleObservaciones = DetalleObs;
                var response = CuentasPorCobrarFactory.GuardarDetalleObservaciones(cuenta);
                if (response.ProcesoExitoso) {
                    closeModal('modalObservaciones');
                    //ShowConfirm("Se guardaron las observaciones para la cuenta " + $scope.Model.Numero);
                    ShowSuccess("Se guardaron las observaciones para la cuenta " + $scope.Model.Numero);
                }
                else {
                    ShowError(response.statusText);
                }
            }
            else {
                ShowError("Debe agregar detalle");
            }
        };
        //--Gestion Observaciones
        //--Anular
        $scope.Anular = function (Codigo, Numero) {
            $scope.MensajesErrorAnula = [];
            $scope.CausaAnula = '';
            $scope.CodigoAnular = Codigo;
            $scope.NumeroAnular = Numero;
            showModal('modalAnular');
        };
        $scope.ConfirmaAnular = function () {
            if ($scope.CausaAnula != undefined && $scope.CausaAnula != null && $scope.CausaAnula != '') {
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.CodigoAnular,
                    UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CausaAnulacion: $scope.CausaAnula
                };
                CuentasPorCobrarFactory.Anular(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado > 0) {
                                ShowSuccess('Se anuló la cuenta por cobrar ' + $scope.NumeroAnular);
                                closeModal('modalAnular');
                                PantallaBloqueo(Find);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                $scope.MensajesErrorAnula.push('Debe ingresar la causa de anulación');
            }
        };
        //--Anular
        //--Mascaras
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };
        //----------------------------Funciones Generales---------------------------------//
    }]);
