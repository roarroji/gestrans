﻿SofttoolsApp.controller("ConsultarListadoTesoreriaCtrl", ['$scope', '$timeout', 'TercerosFactory', 'RutasFactory', 'OficinasFactory', 'TarifarioVentasFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'EmpresasFactory', 'blockUIConfig',
    function ($scope, $timeout, TercerosFactory, RutasFactory, OficinasFactory, TarifarioVentasFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, EmpresasFactory, blockUIConfig) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Listados' }, { Nombre: 'Guías Paquetería' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.NombreReporte = '';
        $scope.FiltroArmado = '';
        $scope.ListadoCliente = [];
        $scope.ListadoProveedores = [];
        $scope.FiltroCliente = true;
        $scope.FiltroTransportador = false;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ModeloCliente = [];
        $scope.ModeloTransportador = [];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {} }
        }

        $scope.ModeloOficinaDestino = {};

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OCION_MENU_LISTADOS_TESORERIA); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }


        //Listado Comprobantes:
        $scope.ListadoTiposComprobante = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CONCEPTO_CONTABLE }, Sync: true }).Datos;
        $scope.TipoComprobante = $linq.Enumerable().From($scope.ListadoTiposComprobante).First('$.Codigo==2700');

        // Cargar combos de módulo y listados 
        $scope.ListaDespacho = [];
        for (var i = 0; i < $scope.ListadoMenuListados.length; i++) {
            if ($scope.ListadoMenuListados[i].Padre == OCION_MENU_LISTADOS_TESORERIA) {
                $scope.ListaDespacho.push({ NombreMenu: $scope.ListadoMenuListados[i].Nombre, Codigo: $scope.ListadoMenuListados[i].Codigo })
            }
        }
        $scope.ListaCentrosCostos = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CENTROS_COSTOS_COMPROBANTE_EGRESO }, Sync: true }).Datos;
        $scope.ListaCentrosCostos.push({ Codigo: -1, Nombre: '(TODOS)' });

        $scope.Modelo.CentroCostos = $linq.Enumerable().From($scope.ListaCentrosCostos).First('$.Codigo == -1');
        $scope.setCentroCosto = function () {
            $scope.Modelo.CentroCostos = $linq.Enumerable().From($scope.ListaCentrosCostos).First('$.Codigo == -1');
        }
        $scope.ModeloDocumentosDespachos = $scope.ListaDespacho[0]

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;


        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });

        $scope.ListadoTerceros = []
        $scope.AutocompleteTerceros = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            //CadenaPerfiles: PERFIL_TENEDOR,
                            ValorAutocomplete: value, Sync: true
                        })
                    $scope.ListadoTerceros = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTerceros)
                }
            }
            return $scope.ListadoTerceros
        }

        $scope.AutocompleteProveedores = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            CadenaPerfiles: PERFIL_PROVEEDOR,
                            ValorAutocomplete: value, Sync: true
                        })
                    $scope.ListadoProveedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProveedores)
                }
            }
            return $scope.ListadoProveedores
        }
        $scope.ListadoDocumentosOrigenEgresos = []
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoDocumentosOrigenEgresos.push({ Codigo: 2600, Nombre: "(TODOS)" });
                    response.data.Datos.forEach(function (item) {
                        if (item.CampoAuxiliar2.match(/EGR/)) {
                            $scope.ListadoDocumentosOrigenEgresos.push(item);
                        }
                    });

                    $scope.DocumentoOrigen = $linq.Enumerable().From($scope.ListadoDocumentosOrigenEgresos).First('$.Codigo == 2600');
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.ListadoDocumentosOrigenIngresos = []
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoDocumentosOrigenIngresos.push({ Codigo: 2600, Nombre: "(TODOS)" });
                    response.data.Datos.forEach(function (item) {
                        if (item.CampoAuxiliar2.match(/IGR/)) {
                            $scope.ListadoDocumentosOrigenIngresos.push(item);
                        }
                    });

                    $scope.DocumentoOrigenIngresos = $linq.Enumerable().From($scope.ListadoDocumentosOrigenIngresos).First('$.Codigo == 2600');
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo de ESTADO DE REMESAS PAQUETERIA*/

        $scope.ListadoEstadoRemesaPaqueteria = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 0, Nombre: 'BORRADOR' }, { Codigo: 1, Nombre: 'DEFINITIVO' }, { Codigo: 2, Nombre: 'ANULADO' }];
        $scope.ModeloEstadoGuia = $scope.ListadoEstadoRemesaPaqueteria[0];


        /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true
        }
        $scope.CerrarModal = function () {
            closeModal('modalEliminarTarifarioVentas');
            closeModal('modalMensajeEliminarTarifarioVentas');
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)

        };

        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCliente = [];
                    if (response.data.Datos.length > CERO) {
                        $scope.ListadoCliente = response.data.Datos;

                    }
                    else {
                        $scope.ListadoCliente = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        $scope.AsignarListadoCliente = function (value) {
            return RetornarListaAutocomplete(value, $scope.ListadoCliente, 'NombreCompleto', 'NumeroIdentificacion');
        }


        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_TENEDOR }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoCliente = [];
                    if (response.data.Datos.length > CERO) {
                        $scope.ListadoCliente = response.data.Datos;

                    }
                    else {
                        $scope.ListadoCliente = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.AsignarListadoTransportador = function (value) {
            return RetornarListaAutocomplete(value, $scope.ListadoCliente, 'NombreCompleto', 'NumeroIdentificacion');
        }

        //Consultar rutas
        RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoRutas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoRutas = response.data.Datos
                    }
                    else {
                        $scope.ListadoRutas = []
                    }
                }
            }, function (response) {
            });




        OficinasFactory.Consultar({ CodigoCiudad: $scope.CodigoCiudad, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinas = [];
                    $scope.HabilitarCiudad = false
                    $scope.ListadoOficinas = [
                        { Codigo: 0, Nombre: '(TODOS)' }
                    ];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item)
                    });
                    if ($scope.ListadoOficinas.length > 0) {
                        $scope.ModeloOficinas = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==0');
                        $scope.ModeloOficinaDestino = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==0');
                    }

                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.AsignarFiltro = function (CodigoLista) {
            if (CodigoLista == CODIGO_LITADO_COMPROBANTE_EGRESO_TERCERO) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = true;
            }

            if (CodigoLista == CODIGO_LISTADO_COMPROBANTE_EGRESO_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = true;
            }

            if (CodigoLista == CODIGO_LITADO_COMPROBANTE_INGRESO_TERCERO) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = true;
            }

            if (CodigoLista == CODIGO_LISTADO_COMPROBANTE_INGRESO_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = true;
            }

            if (CodigoLista == CODIGO_LISTADO_COMPROBANTE_CAUSACION_OFICINA) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
            }
            
            if (CodigoLista == CODIGO_LISTADO_COMPROBANTE_CAUSACION_TERCERO) {
                $scope.FiltroCliente = false;
                $scope.FiltroTransportador = false;
            }

        }


        $scope.DesplegarInforme = function (OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.DatosRequeridos();

            if (DatosRequeridos == true) {

                //Depende del listado seleccionado se enviará el nombre por parametro

                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_COMPROBANTE_EGRESO_TERCERO) {
                    $scope.NombreReporte = NOMBRE_LITADO_COMPROBANTE_EGRESO_TERCERO
                }

                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_COMPROBANTE_EGRESO_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_COMPROBANTE_EGRESO_OFICINA
                }

                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LITADO_COMPROBANTE_INGRESO_TERCERO) {
                    $scope.NombreReporte = NOMBRE_LITADO_COMPROBANTE_INGRESO_TERCERO
                }

                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_COMPROBANTE_INGRESO_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_COMPROBANTE_INGRESO_OFICINA
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_COMPROBANTE_CAUSACION_OFICINA) {
                    $scope.NombreReporte = NOMBRE_LISTADO_COMPROBANTE_CAUSACION_OFICINA
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_COMPROBANTE_CAUSACION_TERCERO) {
                    $scope.NombreReporte = NOMBRE_LISTADO_COMPROBANTE_CAUSACION_TERCERO
                }
                if ($scope.ModeloDocumentosDespachos.Codigo == CODIGO_LISTADO_ANTICIPOS) {
                    $scope.NombreReporte = NOMBRE_LISTADO_ANTICIPOS
                }


                $scope.ArmarFiltro();

                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
                }
                if (OpcionEXCEL == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf);
                }

                //Se limpia el filtro para poder hacer mas consultas
                $scope.FiltroArmado = '';

            }
        };
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.ModeloFechaInicial = new Date();
            $scope.ModeloFechaFinal = new Date();
        }
        $scope.DatosRequeridos = function () {
            DatosRequeridos = true;
            $scope.MensajesError = [];

            if ($scope.Sesion.UsuarioAutenticado.ManejoCentroCostosComprobanteEgreso && $scope.DocumentoOrigen.Codigo == 2609) {
                if ($scope.Modelo.CentroCostos == undefined || $scope.Modelo.CentroCostos == null || $scope.Modelo.CentroCostos == '') {
                    $scope.MensajesError.push('Debe seleccionar un Centro de Operaciones');
                    DatosRequeridos = false;
                }
                
            }

            if (($scope.ModeloFechaInicial == null || $scope.ModeloFechaInicial == undefined || $scope.ModeloFechaInicial == '')
                && ($scope.ModeloFechaFinal == null || $scope.ModeloFechaFinal == undefined || $scope.ModeloFechaFinal == '')
                && ($scope.NumeroInicial == null || $scope.NumeroInicial == undefined || $scope.NumeroInicial == '' || $scope.NumeroInicial == 0)
                && ($scope.NumeroFinal == null || $scope.NumeroFinal == undefined || $scope.NumeroFinal == '' || $scope.NumeroFinal == 0)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de números o fechas');
                DatosRequeridos = false

            } else if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)
                || ($scope.NumeroFinal !== null && $scope.NumeroFinal !== undefined && $scope.NumeroFinal !== '' && $scope.NumeroFinal !== 0)
                || ($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                || ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')

            ) {
                if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')
                    && ($scope.ModeloFechaFinal !== null && $scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '')) {
                    if ($scope.ModeloFechaFinal < $scope.ModeloFechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser menor a la fecha final');
                        DatosRequeridos = false
                    } else if ((($scope.ModeloFechaFinal - $scope.ModeloFechaInicial) / (1000 * 60 * 60 * 24)) > 60) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a 60 dias');
                        DatosRequeridos = false
                    }
                }
                else {
                    if (($scope.ModeloFechaInicial !== null && $scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '')) {
                        $scope.ModeloFechaFinal = $scope.ModeloFechaInicial
                    } else {
                        $scope.ModeloFechaInicial = $scope.ModeloFechaFinal
                    }
                }
                Rango = $scope.NumeroFinal - $scope.NumeroInicial;
                if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)
                    && ($scope.NumeroFinal !== null && $scope.NumeroFinal !== undefined && $scope.NumeroFinal !== '' && $scope.NumeroFinal !== 0)) {
                    if ($scope.NumeroFinal < $scope.NumeroInicial) {
                        $scope.MensajesError.push('El número final debe ser mayor al número final');
                        Validar = false;
                    }
                    else if (Rango > RANGO_MAXIMO_CANTIDAD_DOCUMENTOS_LISTADOS) {
                        $scope.MensajesError.push('La diferencia del rango entre el numero inicial y el numero final no puede ser mayor a 2000');
                        Validar = false;
                    }
                } else {
                    if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)) {
                        $scope.NumeroFinal = $scope.NumeroInicial;
                    } else {
                        $scope.NumeroInicial = $scope.NumeroFinal;
                    }
                }
            }

            return DatosRequeridos;

        }

        $scope.ArmarFiltro = function () {
            // 

            if ($scope.Sesion.UsuarioAutenticado.ManejoCentroCostosComprobanteEgreso && $scope.DocumentoOrigen.Codigo == 2609) {
                if ($scope.Modelo.CentroCostos != undefined && $scope.Modelo.CentroCostos != null && $scope.Modelo.CentroCostos != '') {
                    if ($scope.Modelo.CentroCostos.Codigo != -1) {
                        $scope.FiltroArmado += '&CentroOperaciones=' + $scope.Modelo.CentroCostos.Codigo;
                    }
                }
            }

            if ($scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== null && $scope.NumeroInicial > 0) {
                $scope.FiltroArmado += '&Numero_Documento_Inicial=' + $scope.NumeroInicial;
            }
            if ($scope.NumeroFinal !== undefined && $scope.NumeroFinal !== '' && $scope.NumeroFinal !== null && $scope.NumeroFinal > 0) {
                $scope.FiltroArmado += '&Numero_Documento_Final=' + $scope.NumeroFinal;
            }
            if ($scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '' && $scope.ModeloFechaInicial !== null) {
                var ModeloFechaInicial = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaInicial);
                $scope.FiltroArmado += '&Fecha_Incial=' + ModeloFechaInicial;
            }
            if ($scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '' && $scope.ModeloFechaFinal !== null) {
                var ModeloFechaFinal = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaFinal);
                $scope.FiltroArmado += '&Fecha_Final=' + ModeloFechaFinal;
            }
            if ($scope.ModeloDocumentosDespachos.Codigo != 800306 && $scope.ModeloDocumentosDespachos.Codigo != 800307) {
                if ($scope.Tercero !== undefined && $scope.Tercero !== '' && $scope.Tercero !== null) {
                    $scope.FiltroArmado += '&TERC_Codigo=' + $scope.Tercero.Codigo;
                }
            } else {
                if ($scope.Proveedor !== undefined && $scope.Proveedor !== '' && $scope.Proveedor !== null) {
                    $scope.FiltroArmado += '&TERC_Codigo=' + $scope.Proveedor.Codigo;
                }
                if ($scope.TipoComprobante !== undefined && $scope.TipoComprobante !== '' && $scope.TipoComprobante !== null) {
                    if ($scope.TipoComprobante.Codigo != 2700) {
                        $scope.FiltroArmado += '&TICO_Codigo=' + $scope.TipoComprobante.Codigo;
                    }
                }
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == 800301 || $scope.ModeloDocumentosDespachos.Codigo == 800302) {
                if ($scope.DocumentoOrigen !== undefined && $scope.DocumentoOrigen !== '' && $scope.DocumentoOrigen !== null) {
                    if ($scope.DocumentoOrigen.Codigo !== 2600) {
                        $scope.FiltroArmado += '&DocumentoOrigen=' + $scope.DocumentoOrigen.Codigo;
                    }
                }
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == 800303 || $scope.ModeloDocumentosDespachos.Codigo == 800304) {
                if ($scope.DocumentoOrigenIngresos !== undefined && $scope.DocumentoOrigenIngresos !== '' && $scope.DocumentoOrigenIngresos !== null) {
                    if ($scope.DocumentoOrigenIngresos.Codigo !== 2600) {
                        $scope.FiltroArmado += '&DocumentoOrigen=' + $scope.DocumentoOrigenIngresos.Codigo;
                    }
                }
            }
            if ($scope.ModeloDocumentosDespachos.Codigo != 800306 && $scope.ModeloDocumentosDespachos.Codigo != 800307) {
                if ($scope.NumeroDocumentoOrigen !== undefined && $scope.NumeroDocumentoOrigen !== '' && $scope.NumeroDocumentoOrigen !== null && $scope.NumeroDocumentoOrigen > 0) {
                    $scope.FiltroArmado += '&NumeroDocumentoOrigen=' + $scope.NumeroDocumentoOrigen;
                }
            }
            if ($scope.ModeloOficinas.Codigo !== undefined && $scope.ModeloOficinas.Codigo !== '' && $scope.ModeloOficinas.Codigo !== null && $scope.ModeloOficinas.Codigo > 0) {
                $scope.FiltroArmado += '&Codigo_Oficina=' + $scope.ModeloOficinas.Codigo;
            }
            if ($scope.ModeloDocumentosDespachos.Codigo == 800306 || $scope.ModeloDocumentosDespachos.Codigo == 800307) {
                if ($scope.ModeloOficinaDestino.Codigo !== undefined && $scope.ModeloOficinaDestino.Codigo !== '' && $scope.ModeloOficinaDestino.Codigo !== null && $scope.ModeloOficinaDestino.Codigo > 0) {
                    $scope.FiltroArmado += '&Codigo_Oficina_Destino=' + $scope.ModeloOficinaDestino.Codigo;
                }
            }
            if ($scope.ModeloEstadoGuia.Codigo != -1) {
                $scope.FiltroArmado += '&Estado=' + $scope.ModeloEstadoGuia.Codigo;
            }
        }
    }]);