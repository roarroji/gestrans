﻿SofttoolsApp.controller("ConsultarListaDistribucionCorreosCtrl", ['$scope', '$routeParams', '$linq', '$timeout', 'EventoCorreosFactory', 'blockUI', 'TipoDocumentosFactory',
    function ($scope, $routeParams, $linq, $timeout, EventoCorreosFactory, blockUI, TipoDocumentosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Utilitarios' }, { Nombre: 'Correos' }, { Nombre: 'Lista Distribución Correos' }];
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        var filtros = {};
        $scope.Modelo = {}
        $scope.ListadoTipoDocumentos = [];

        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;


        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_LISTA_DISTRIBUCION_CORREOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        //Seleccionar evento correo----------------------------------------------------------------------------------


        EventoCorreosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaEventoCorreos = response.data.Datos;
                    for (var i = 0; i < $scope.ListaEventoCorreos.length; i++) {
                        $scope.ListaEventoCorreos[i].Nombre = $scope.ListaEventoCorreos[i].Nombre.toUpperCase()
                    }

                    $scope.ListaEventoCorreos.push({ Nombre: '(TODOS)', Codigo: 0 })
                    $scope.Modelo.NumeroEvento = $scope.ListaEventoCorreos[$scope.ListaEventoCorreos.length - 1]
                }
            }, function (response) {
                $scope.Buscando = false;
            });
        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarListaDistribucionCorreos';
            }
        };
        //-------------------------------------------------------------------
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        $scope.Buscar = function () {
            Find();
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.NumeroEvento.Codigo
            };

            blockUI.delay = 1000;
            EventoCorreosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoEventoCorreos = response.data.Datos;
                    }
                }, function (response) {
                    $scope.Buscando = false;
                });
            blockUI.stop();
        };


    }]);