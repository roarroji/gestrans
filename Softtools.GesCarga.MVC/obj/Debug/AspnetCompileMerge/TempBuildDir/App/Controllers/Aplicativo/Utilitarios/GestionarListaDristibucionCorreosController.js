﻿SofttoolsApp.controller("GestionarListaDistribucionCorreosCtrl", ['$scope', '$linq', '$routeParams', 'ListaDistribucionCorreosFactory', 'TercerosFactory', '$timeout', 'EventoCorreosFactory', 'blockUI', 'TipoDocumentosFactory',
    function ($scope, $linq, $routeParams, ListaDistribucionCorreosFactory, TercerosFactory, $timeout, EventoCorreosFactory, blockUI, TipoDocumentosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Utilitarios' }, { Nombre: 'Correos' }, { Nombre: 'Lista Distribución Correos' }, { Nombre: 'Generar' }];
        $scope.MensajesError = [];

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0
        }

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_LISTA_DISTRIBUCION_CORREOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.DesabilitarEvento = false;

        //funcion autocomplete en nombre---------------------------------------------------------------------------

        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_EMPLEADO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEmpleados = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoEmpleados = response.data.Datos;
                    }
                    else {
                        $scope.ListadoEmpleados = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //Funcion obtener correo autocomplete------------------------------------------------------------------------------

        $scope.ObtenerEmpleado = function () {
            if ($scope.Modelo.Empleado != undefined) {
                if ($scope.Modelo.Empleado.Codigo > 0) {
                    filtro = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.Modelo.Empleado.Codigo,

                    };
                    TercerosFactory.Obtener(filtro).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Modelo.Email = response.data.Datos.Correo
                            }
                            else {
                                ShowError('No se logro consulta del empleado');
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
        }

        //Funcion agregar Empleado e Email al formulario-------------------------------------------------------------

        $scope.ListadoEmpleadosCorreos = [];

        $scope.Agregar = function () {

            if ($scope.Modelo.Empleado != '' && $scope.Modelo.Empleado != undefined) {
                if ($scope.Modelo.Email != '' && $scope.Modelo.Email != undefined) {
                    var concidencias = 0;
                    var registro = 0;
                    if ($scope.ListadoEmpleadosCorreos.length > 0) {
                        for (var i = 0; i < $scope.ListadoEmpleadosCorreos.length; i++) {
                            if ($scope.Modelo.Empleado.Codigo == undefined) {
                                registro++
                                break;
                            } else if ($scope.Modelo.Empleado.Codigo == $scope.ListadoEmpleadosCorreos[i].Empleado.Codigo) {
                                concidencias++
                                break;
                            }

                        }
                        if (registro > 0) {
                            ShowError('El empleado no se encuentra registrado');
                            $scope.Modelo.Empleado = "";
                            $scope.Modelo.Email = "";
                        } else if (concidencias > 0) {
                            ShowError('El empleado ya fue ingresado')
                            $scope.Modelo.Empleado = "";
                            $scope.Modelo.Email = "";
                        }

                        else {
                            $scope.ListadoEmpleadosCorreos.push({
                                Empleado: { NombreCompleto: $scope.Modelo.Empleado.NombreCompleto, Codigo: $scope.Modelo.Empleado.Codigo },
                                Email: $scope.Modelo.Email,
                                ModificarCorreo: false,
                                OcultarBotones: true,
                                NumeroEvento: $scope.Modelo.NumeroEvento.Codigo,
                                Estado: ESTADO_ACTIVO,
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            })
                            $scope.Modelo.Empleado = "";
                            $scope.Modelo.Email = "";
                        }
                    }
                    else {
                        $scope.ListadoEmpleadosCorreos.push({
                            Empleado: { NombreCompleto: $scope.Modelo.Empleado.NombreCompleto, Codigo: $scope.Modelo.Empleado.Codigo },
                            Email: $scope.Modelo.Email,
                            ModificarCorreo: false,
                            OcultarBotones: true,
                            NumeroEvento: $scope.Modelo.NumeroEvento.Codigo,
                            Estado: ESTADO_ACTIVO,
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        })
                        $scope.Modelo.Empleado = "";
                        $scope.Modelo.Email = "";
                    }
                }
                else {
                    ShowError('El correo electronico es requerido')
                }
            }
            else {
                ShowError('El empleado es requerido')
            }

        }

        //Funcion guardar----------------------------------------------------------------------------------------------

        $scope.GuardarCorreosEmpleados = function () {
            if (DatosRequeridosCorreo()) {
                showModal('modalConfirmacionGuardar');
            }
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');

            $scope.Modelo.NumeroEvento = $scope.Modelo.NumeroEvento.Codigo;
            $scope.Modelo.Estado = $scope.Modelo.Estado.Codigo;
            $scope.Modelo.Listado = $scope.ListadoEmpleadosCorreos;
            $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
            $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
            $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;

            ListaDistribucionCorreosFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            if ($scope.Modelo.Codigo == 0) {
                                ShowSuccess('Los datos ingresados fueron guardados');
                            }
                            document.location.href = '#!ConsultarListaDistribucionCorreos';
                        }
                    }
                });

        }

        //---------------------------------Funcion Add Terceros-----------------------------------------------------------


        $scope.AddConductor = function (conductor) {
            $scope.ListaCorreosAuxiliar = [];
            if (conductor) {
                $scope.ListadoEmpleadosCorreos.push({
                    Empleado: { NombreCompleto: 'Conductor' },
                    Email: '@Conductor',
                    OcultarBotones: false,
                    NumeroEvento: $scope.Modelo.NumeroEvento.Codigo,
                    Estado: ESTADO_ACTIVO,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                });
            } else {
                $scope.ListadoEmpleadosCorreos.forEach(function (item) {
                    if (item.Email !== '@Conductor') {
                        $scope.ListaCorreosAuxiliar.push(item);
                    }
                })

                $scope.ListadoEmpleadosCorreos = $scope.ListaCorreosAuxiliar;
            }

        }

        $scope.AddTransportador = function (transportador) {
            $scope.ListaCorreosAuxiliarT = [];
            if (transportador) {
                $scope.ListadoEmpleadosCorreos.push({
                    Empleado: { NombreCompleto: 'Transportador' },
                    Email: '@Transportador',
                    OcultarBotones: false,
                    NumeroEvento: $scope.Modelo.NumeroEvento.Codigo,
                    Estado: ESTADO_ACTIVO,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                });
            } else {
                $scope.ListadoEmpleadosCorreos.forEach(function (item) {
                    if (item.Email !== '@Transportador') {
                        $scope.ListaCorreosAuxiliarT.push(item);
                    }
                })
                $scope.ListadoEmpleadosCorreos = $scope.ListaCorreosAuxiliarT;
            }
        }

        $scope.AddCliente = function (Cliente) {
            $scope.ListaCorreosAuxiliarC = [];
            if (Cliente) {
                $scope.ListadoEmpleadosCorreos.push({
                    Empleado: { NombreCompleto: 'Cliente' },
                    Email: '@Cliente',
                    OcultarBotones: false,
                    NumeroEvento: $scope.Modelo.NumeroEvento.Codigo,
                    Estado: ESTADO_ACTIVO,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                });
            } else {
                $scope.ListadoEmpleadosCorreos.forEach(function (item) {
                    if (item.Email !== '@Cliente') {
                        $scope.ListaCorreosAuxiliarC.push(item);
                    }
                })
                $scope.ListadoEmpleadosCorreos = $scope.ListaCorreosAuxiliarC;
            }
        }

        //Funcion volver a lista de distribucion correos-------------------------------------------------

        $scope.Volver = function () {
            document.location.href = '#!ConsultarListaDistribucionCorreos'
        };

        //Funcion eliminar correo---------------------------------------------------------------------------

        $scope.MensajeEliminarCorreo = function (NombreCompleto, Correo, index) {
            $scope.Empleado = NombreCompleto;
            $scope.DatosEliminar = index;
            showModal('MensajeEliminarCorreo');
        };

        $scope.EliminarCorreo = function () {
            $scope.ListadoEmpleadosCorreos.splice($scope.DatosEliminar, 1);
            closeModal('MensajeEliminarCorreo');
        }

        //--------------------------------Funcion Datos Requeridos--------------------------------------------------

        function DatosRequeridosCorreo() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.NumeroEvento == undefined || $scope.Modelo.NumeroEvento.Codigo == '' || $scope.Modelo.NumeroEvento.Codigo == 0 || $scope.Modelo.NumeroEvento.Codigo == undefined) {
                $scope.MensajesError.push('Debe ingresar el Evento Correo');
                continuar = false;
            }
            if ($scope.Modelo.Estado == undefined || $scope.Modelo.Estado == '') {
                $scope.MensajesError.push('Debe ingresar el Estado');
                continuar = false;
            }
            if ($scope.ListadoEmpleadosCorreos == undefined || $scope.ListadoEmpleadosCorreos == '') {
                $scope.MensajesError.push('Debe ingresar mínimo un detalle ');
                continuar = false;
            }

            return continuar;
        }
        //----------------------------------------Funcion Colsultar--------------------------------------------------------------------------------------------------------------------------------

        $scope.ListadoEstados = [
            { Nombre: 'ACTIVO', Codigo: ESTADO_ACTIVO },
            { Nombre: 'INACTIVO', Codigo: ESTADO_INACTIVO },
        ];
        $scope.Modelo.Estado = $scope.ListadoEstados[0];

        EventoCorreosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEventoCorreos = response.data.Datos;
                    for (var i = 0; i < $scope.ListadoEventoCorreos.length; i++) {
                        $scope.ListadoEventoCorreos[i].Nombre = $scope.ListadoEventoCorreos[i].Nombre.toUpperCase()
                    }

                    $scope.ListadoEventoCorreos.push({ Nombre: 'Seleccione un evento', Codigo: 0 })
                    if ($routeParams.Codigo > 0) {
                        $scope.DesabilitarEvento = true;
                        $scope.Modelo.NumeroEvento = $linq.Enumerable().From($scope.ListadoEventoCorreos).First('$.Codigo ==' + $routeParams.Codigo);
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.NumeroEvento.Estado);
                        ObtenerLista();
                    } else {
                        $scope.Modelo.NumeroEvento = $scope.ListadoEventoCorreos[$scope.ListadoEventoCorreos.length - 1];
                    }
                }
            }, function (response) {
                $scope.Buscando = false;
            });

        $scope.Buscar = function () {
            Find();
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroInicial: $scope.NumeroInicial,
                NumeroFinal: $scope.NumeroFinal,
                FechaInicial: $scope.FechaInicial,
                FechaFinal: $scope.FechaFinal,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                Estado: $scope.ModalEstado.Codigo,
                TipoDocumento: $scope.TipoDocu.Codigo,
            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                EventoCorreosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ListadoEventoCorreos = response.data.Datos;
                            if (response.data.Datos.length > 0) {
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = parseInt($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };
        //Funcion cargar datos en nuevo evento correo-----------------------------------------------------------------------------------------------------------------
        $scope.CargarNuevoEvento = function () {
            ObtenerLista();
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function ObtenerLista() {
            blockUI.start('Cargando lista código ' + $scope.NumeroEvento);

            $timeout(function () {
                blockUI.message('Cargando lista Código ' + $scope.NumeroEvento);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroEvento: $scope.Modelo.NumeroEvento.Codigo,
            };

            blockUI.delay = 1000;

            $scope.ListadoEmpleadosCorreos = [];
            ListaDistribucionCorreosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        //$scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos[0].Estado);

                        response.data.Datos.forEach(function (item) {

                            if (item.Email == '@Conductor') {
                                $scope.ListadoEmpleadosCorreos.push({
                                    Empleado: { NombreCompleto: 'Conductor' },
                                    Email: '@Conductor',
                                    OcultarBotones: false,
                                    NumeroEvento: $scope.Modelo.NumeroEvento.Codigo,
                                    Estado: ESTADO_ACTIVO,
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                });
                                $scope.Modelo.Conductor = true;
                            }
                            else if (item.Email == '@Transportador') {
                                $scope.ListadoEmpleadosCorreos.push({
                                    Empleado: { NombreCompleto: 'Transportador' },
                                    Email: '@Transportador',
                                    OcultarBotones: false,
                                    NumeroEvento: $scope.Modelo.NumeroEvento.Codigo,
                                    Estado: ESTADO_ACTIVO,
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                });
                                $scope.Modelo.Transportador = true;
                            }
                            else if (item.Email == '@Cliente') {
                                $scope.ListadoEmpleadosCorreos.push({
                                    Empleado: { NombreCompleto: 'Cliente' },
                                    Email: '@Cliente',
                                    OcultarBotones: false,
                                    NumeroEvento: $scope.Modelo.NumeroEvento.Codigo,
                                    Estado: ESTADO_ACTIVO,
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                });
                                $scope.Modelo.Cliente = true;
                            }
                            else {
                                $scope.ListadoEmpleadosCorreos.push({
                                    Empleado: item.Empleado,
                                    Email: item.Email,
                                    ModificarCorreo: false,
                                    OcultarBotones: true,
                                    NumeroEvento: $scope.Modelo.NumeroEvento.Codigo,
                                    Estado: ESTADO_ACTIVO,
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                });
                            }
                        });

                    } else {
                        ShowError('No se logro consultar la lista' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarListaDistribucionCorreos';
                    }
                }, function (response) {
                    ShowError(response.statusText)
                    document.location.href = '#!ConsultarListaDistribucionCorreos';
                });

            blockUI.stop();
        };

    }]);   