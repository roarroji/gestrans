﻿    SofttoolsApp.controller("ConsultarCuentasCtrl", ['$scope', '$routeParams', '$linq', 'blockUI', '$timeout', 'TercerosFactory', 'ValorCatalogosFactory', 'OficinasFactory', 'DocumentoComprobantesFactory', 'CajasFactory', 'CuentaBancariasFactory', 'PlanUnicoCuentasFactory', 'VehiculosFactory', 'ConceptoContablesFactory', 'DetalleConceptoContablesFactory', 'DocumentoCuentasFactory',
    function ($scope, $routeParams, $linq, blockUI, $timeout, TercerosFactory, ValorCatalogosFactory, OficinasFactory, DocumentoComprobantesFactory, CajasFactory, CuentaBancariasFactory, PlanUnicoCuentasFactory, VehiculosFactory, ConceptoContablesFactory, DetalleConceptoContablesFactory, DocumentoCuentasFactory) {
        /*-----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- */
        $scope.Titulo = 'CONSULTAR CUENTAS POR COBRAR';
        $scope.MapaSitio = [{ Nombre: 'Documentos' }, { Nombre: 'Cuentas' }];
        console.clear();

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + 23003); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: 0 }
        ];
        $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        $scope.cantidadRegistrosPorPagina = 10
        $scope.paginaActual = 1
        $scope.ListaCXP = [];
        $scope.ListadoCuentasPUC = [];
        $scope.ListadoOficinas = [];
        $scope.ListadoDocumentosOrigen = [];
        $scope.ListadoCuentaBancariaOficinas = [];
        $scope.ListadoConceptoContable = [];
        $scope.ListadoVehiculos = [];
        $scope.ListaFormaPago = [];
        $scope.MensajesError = [];
        $scope.ListadoCuentaBancarias = [];
        $scope.ListadoMovimiento = [];
        $scope.ListadoTodaCajaOficinas = [];
        $scope.ListadoCajaOficinas = [];
        $scope.totalRegistros = undefined
        $scope.ListaCXPSeleccionadas = [];

        $scope.ActivarEfectivo = false
        $scope.ActivarCheque = false
        $scope.ActivarConsignacion = false
        $scope.PlacaValida = true;
        $scope.DeshabilitaDocumentoOrigen = true;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            Numero: 0,
            Fecha: new Date(),
            Debito: 0,
            Credito: 0,
            Diferencia: 0,
        }
        $scope.Modal = {

        }

        try {
            $scope.Modelo.Codigo = $scope.Sesion.UsuarioAutenticado.Conductor.Codigo

        } catch (e) {
            $scope.Modelo.Codigo = 0;

        }
        $scope.ConsultarCXP = function () {
            $scope.totalRegistros = 0
            var Filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR, //CODIGO_TIPO_DOCUMENTO_CUENTA_POR_COBRAR
                Tercero: { Codigo: $scope.Modelo.Codigo },
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                DocumentoOrigen: { Codigo: 2602 }
            }

            DocumentoCuentasFactory.Consultar(Filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListaCXP = response.data.Datos
                                $scope.ListaCXP.forEach(function (item) {
                                    item.ValorPagar = MascaraValores(item.Saldo)
                                });
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                                $scope.MaskValores()
                            } else {
                                $scope.ListaCXP = [];
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                        else {
                            $scope.ListaCXP = [];
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                });
        }

        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            $scope.ConsultarCXP();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                $scope.ConsultarCXP();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                $scope.ConsultarCXP();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            $scope.ConsultarCXP();
        };
        $scope.PrimerPagina()
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    }]);