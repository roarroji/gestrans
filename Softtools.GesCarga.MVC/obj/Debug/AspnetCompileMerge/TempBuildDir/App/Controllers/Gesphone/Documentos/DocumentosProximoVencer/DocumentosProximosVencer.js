﻿SofttoolsApp.controller("DocumentosProximosVencerCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'TercerosFactory', 'VehiculosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, TercerosFactory, VehiculosFactory) {
        $scope.Titulo = 'DOCUMENTOS PRÓXIMOS A VENCER';
        $scope.MapaSitio = [{ Nombre: 'Documentos' }, { Nombre: 'Documentos próximos a vencer' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_DOCUMENTOS_PROXIMOS_VENCER);
            //--Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        }
        catch (e) {
            document.location.href = '#';
        }
        //--Obtener parametros del enrutador
        try {
            $scope.Codigo = $scope.Sesion.UsuarioAutenticado.Conductor.Codigo;
        } catch (e) {
            $scope.Codigo = 0;

        }
        //--Documents Terceros Vencer
        $scope.ListadoDocumentoProximosVencerTerceros = [];
        var RespTerDocumentosAVencer = TercerosFactory.ConsultarDocumentosProximosVencer({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.Codigo }, TipoAplicativo: CODIGO_APLICACION_GESPHONE, Sync: true });
        if (RespTerDocumentosAVencer.ProcesoExitoso === true) {
            if (RespTerDocumentosAVencer.Datos.length > 0) {
                $scope.ListadoDocumentoProximosVencerTerceros = RespTerDocumentosAVencer.Datos;
                $scope.ListadoDocumentoProximosVencerTerceros.forEach(function (item) {
                    if (item.CantidadDiasFaltantes == '0') {
                        item.CantidadDiasFaltantes = 'VENCIDO';
                    }
                });
            }
        }

        //--Documents Terceros - Vehiculo Vencer
        $scope.ListadoDocumentoProximosVencerVehiculos = [];        
        var RespTerVehiDocumentosAVencer = VehiculosFactory.ConsultarDocumentosProximosVencer({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.Codigo }, TipoAplicativo: CODIGO_APLICACION_GESPHONE, Sync: true });
        if (RespTerVehiDocumentosAVencer.ProcesoExitoso === true) {
            if (RespTerVehiDocumentosAVencer.Datos.length > 0) {
                $scope.ListadoDocumentoProximosVencerVehiculos = RespTerVehiDocumentosAVencer.Datos;
                $scope.ListadoDocumentoProximosVencerVehiculos.forEach(function (item) {
                    if (item.CantidadDiasFaltantes == '0') {
                        item.CantidadDiasFaltantes = 'VENCIDO';
                    }
                });
            }
        }
    }]);