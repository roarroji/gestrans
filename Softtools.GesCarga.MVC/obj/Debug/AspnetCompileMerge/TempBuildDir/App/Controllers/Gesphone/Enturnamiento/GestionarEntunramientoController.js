﻿SofttoolsApp.controller("GestionarEntunramientoCtrl", ['$scope', '$routeParams', '$timeout', 'CiudadesFactory', '$linq', 'blockUI', 'OficinasFactory',
    'PaisesFactory', 'ZonasFactory', 'DepartamentosFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'VehiculosFactory', 'blockUIConfig',
    'EnturnamientoFactory',
    function ($scope, $routeParams, $timeout, CiudadesFactory, $linq, blockUI, OficinasFactory,
        PaisesFactory, ZonasFactory, DepartamentosFactory, ValorCatalogosFactory, TercerosFactory, VehiculosFactory, blockUIConfig,
        EnturnamientoFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = "ENTURNAMIENTOS";
        $scope.MapaSitio = [{ Nombre: 'Turnos' }, { Nombre: 'Enturnamiento' }];
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ENTURNAMIENTO); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'Activa', Codigo: 1 },
            { Nombre: 'Inactiva', Codigo: 0 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.ListadoEstadosZonas = [
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];
        $scope.ModalEstadoZona = $linq.Enumerable().From($scope.ListadoEstadosZonas).First('$.Codigo == 1');

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            FechaDisponible: new Date()
        }
        $scope.ListaRegiones = [];
        $scope.ListadoTenedores = [];
        $scope.ListadoConductores = [];
        $scope.ListadoCiudades = [];
        $scope.CiudadDestino1 = false;
        $scope.CiudadDestino2 = false;
        //--------------------------Variables-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----Paginacion
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        //----Paginacion
        //-----AutoComplete
        //--Tenedor
        $scope.AutocompleteTenedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    $scope.ListadoTenedores = [];
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_TENEDOR,
                        ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoTenedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTenedores)
                }
            }
            return $scope.ListadoTenedores
        }
        //--Conductores
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    $scope.ListadoConductores = [];
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                }
            }
            return $scope.ListadoConductores
        }
        //--Ciudades
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    $scope.ListadoCiudades = [];
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true, CiudadOficinas: 1
                    })
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                }
            }
            return $scope.ListadoCiudades
        }
        //-----AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //--Tipo Vehiculo
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoVehiculo = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoVehiculo = response.data.Datos;
                            try {
                                $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo ==' + $scope.Modelo.Vehiculo.TipoVehiculo.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoVehiculo = $scope.ListadoTipoVehiculo[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoVehiculo = [];
                        }
                    }
                }, function (response) {
                });
            //--Tipo Causas Anula
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 191 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCausas = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCausas = response.data.Datos;
                        }
                        else {
                            $scope.ListadoCausas = []
                        }
                    }
                }, function (response) {
                });
            //--Oficinas
            OficinasFactory.ConsultarRegionesPaises({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.splice(response.data.Datos.findIndex(item => item.Codigo === 0), 1);
                            $scope.ListaRegiones.push({ Nombre: 'SELECCIONE REGIÓN', Codigo: 0 });
                            response.data.Datos.forEach(function (item) {
                                $scope.ListaRegiones.push(item);
                            });
                            if ($scope.CodigoRegion !== undefined && $scope.CodigoRegion !== null && $scope.CodigoRegion !== '') {
                                $scope.Modelo.Region = $linq.Enumerable().From($scope.ListaRegiones).First('$.Codigo ==' + $scope.CodigoRegion);
                            }
                            $scope.Modelo.RegionPaisDestino = $scope.ListaRegiones[0];
                            $scope.Modelo.RegionPaisDestino2 = $scope.ListaRegiones[0];
                        }
                        else {
                            $scope.ListaRegiones = [];
                        }
                    }
                }, function (response) {
                });
            //--Obtener parametros--//
            if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
                $scope.Codigo = $routeParams.Codigo;
                Find();
            }
            //--Validaciones Conductor
            try {
                var fecha = new Date(new Date().setSeconds(3600));
                $scope.Modelo = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    FechaDisponible: FormatoFechaISO8601(fecha)
                }
                var Response = TercerosFactory.Obtener({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Sesion.UsuarioAutenticado.Conductor.Codigo,
                    Sync: true
                })
                $scope.Modelo.Conductor = Response.Datos;

                if ($scope.Modelo.Conductor.Codigo > 0) {
                    $scope.BloqueoConductor = true
                    if ($scope.Modelo.Conductor.Estado.Codigo == 0) {
                        ShowError('El Conductor asociado al usuario actual se encuentra inactivo Causa:' + $scope.Modelo.Conductor.JustificacionBloqueo)
                    } else {
                        $scope.ListadoEnturnes = [];
                        $scope.ListadoVehiculos = [];
                        //--Cargar Autocomplete de propietario
                        try {
                            if ($scope.Modelo.Conductor.Codigo > 0) {
                                blockUIConfig.autoBlock = false;
                                var Response = VehiculosFactory.Consultar({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Conductor: { Codigo: $scope.Modelo.Conductor.Codigo },
                                    //ValorAutocomplete: value,
                                    Sync: true
                                })
                                $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                                if ($scope.ListadoVehiculos.length > 0) {
                                    $scope.Modelo.Vehiculo = $scope.ListadoVehiculos[0]
                                    try {
                                        $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo ==' + $scope.Modelo.Vehiculo.TipoVehiculo.Codigo);

                                    } catch (e) {

                                    }
                                    $scope.Modelo.Tenedor = $scope.CargarTercero($scope.Modelo.Vehiculo.Tenedor.Codigo)
                                    Find()
                                } else {
                                    ShowError('El conductor ingresado no posee vehículos asociados en el sistema')
                                }
                            }
                        } catch (e) {
                        }
                    }
                }
            } catch (e) {

            }
        };

        $scope.ConsultarOficinas = function (Ciudad) {
            if (Ciudad != undefined) {
                if (Ciudad.Codigo > 0) {
                    $scope.ListaOficinasEnturne = [];
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: Ciudad.Codigo,
                    }
                    blockUI.delay = 1000;
                    CiudadesFactory.ConsultarOficinas(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    //$scope.ListaOficinasEnturne = response.data.Datos;
                                    //$scope.Modelo.OficinaEnturna = $scope.ListaOficinasEnturne[0]
                                    for (var i = 0; i < response.data.Datos.length; i++) {
                                        if (response.data.Datos[i].AplicaEnturnamiento == CODIGO_UNO) {
                                            $scope.ListaOficinasEnturne.push(response.data.Datos[i]);
                                        }
                                    }
                                    if ($scope.ListaOficinasEnturne.length > 0) {
                                        $scope.Modelo.OficinaEnturna = $scope.ListaOficinasEnturne[0];
                                    }
                                    else {
                                        ShowError('La ciudad ingresada no tiene oficinas que aplican enturnamiento')
                                        $scope.Modelo.CiudadEnturna = ''
                                        $scope.ListaOficinasEnturne = [];
                                    }
                                } else {
                                    ShowError('La ciudad ingresada no tiene oficinas asociadas')
                                    $scope.Modelo.CiudadEnturna = ''
                                    $scope.ListaOficinasEnturne = [];
                                }
                            } else {
                                ShowError('La ciudad ingresada no tiene oficinas asociadas')
                                $scope.Modelo.CiudadEnturna = ''
                                $scope.ListaOficinasEnturne = [];
                            }
                        });
                }
            }
        }

        $scope.FiltrarCiudades = function () {
            if ($scope.Modelo.RegionPaisDestino.Codigo > 0) {
                $scope.ListaRegionCiudades = [];
                blockUIConfig.autoBlock = false;
                var Response = CiudadesFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true, Region: $scope.Modelo.RegionPaisDestino
                })
                $scope.ListaRegionCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRegionCiudades)
                $scope.Modelo.CiudadDestino1 = $scope.ListaRegionCiudades[0];
                $scope.CiudadDestino1 = true;
            }
            else {
                $scope.ListaRegionCiudades = [];
                $scope.Modelo.CiudadDestino1 = '';
                $scope.CiudadDestino1 = false;
            }
        }

        $scope.FiltrarCiudades2 = function () {
            if ($scope.Modelo.RegionPaisDestino2.Codigo > 0) {
                $scope.ListaRegionCiudades2 = [];
                blockUIConfig.autoBlock = false;
                var Response = CiudadesFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true, Region: $scope.Modelo.RegionPaisDestino2
                })
                $scope.ListaRegionCiudades2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRegionCiudades2)
                $scope.Modelo.CiudadDestino2 = $scope.ListaRegionCiudades2[0];
                $scope.CiudadDestino2 = true;
            }
            else {
                $scope.ListaRegionCiudades2 = [];
                $scope.Modelo.CiudadDestino2 = '';
                $scope.CiudadDestino2 = false;
            }
        }
        $scope.CargarVehiculos = function () {
            $scope.ListadoEnturnes = [];
            $scope.ListadoVehiculos = [];
            /*Cargar Autocomplete de propietario*/
            try {
                if ($scope.Modelo.Conductor.Codigo > 0) {
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Conductor: { Codigo: $scope.Modelo.Conductor.Codigo },
                        //ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                    if ($scope.ListadoVehiculos.length > 0) {
                        $scope.Modelo.Vehiculo = $scope.ListadoVehiculos[0]
                        $scope.AsignarDatosVehiculo()
                        Find()
                    } else {
                        ShowError('El conductor ingresado no posee vehículos asociados en el sistema')
                    }
                }
            } catch (e) {
            }
        }
        $scope.AsignarDatosVehiculo = function () {
            $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo ==' + $scope.Modelo.Vehiculo.TipoVehiculo.Codigo);
            $scope.Modelo.Tenedor = $scope.CargarTercero($scope.Modelo.Vehiculo.Tenedor.Codigo)
        }
        $scope.Enturnar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar')
            }
        }
        $scope.Guardar = function () {
            EnturnamientoFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('Gracias, Ya se encuentra enturnado');
                            closeModal('modalConfirmacionGuardar');
                            $scope.Modelo.CiudadEnturna = '';
                            $scope.Modelo.OficinaEnturna = '';
                            $scope.ListaOficinasEnturne = [];
                            $scope.Modelo.RegionPaisDestino = $scope.ListaRegiones[0];
                            $scope.Modelo.CiudadDestino1 = '';
                            $scope.ListaRegionCiudades = [];
                            $scope.Modelo.RegionPaisDestino2 = $scope.ListaRegiones[0];
                            $scope.Modelo.CiudadDestino2 = '';
                            $scope.ListaRegionCiudades2 = [];
                            $scope.FechaDisponible = FormatoFechaISO8601(new Date(new Date().setSeconds(3600)));
                            $scope.CiudadDestino1 = false;
                            $scope.CiudadDestino2 = false;
                            Find();
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true
            if ($scope.Modelo.Conductor == undefined || $scope.Modelo.Conductor == '') {
                $scope.MensajesError.push('Debe ingresar el conductor')
                continuar = false
            }
            if ($scope.Modelo.Vehiculo == undefined || $scope.Modelo.Vehiculo == '') {
                $scope.MensajesError.push('Debe ingresar el vehículo')
                continuar = false
            }
            if ($scope.Modelo.CiudadEnturna == undefined || $scope.Modelo.CiudadEnturna == '') {
                $scope.MensajesError.push('Debe ingresar la Ciudad Enturne')
                continuar = false
            }
            if ($scope.Modelo.OficinaEnturna == undefined || $scope.Modelo.OficinaEnturna == '') {
                $scope.MensajesError.push('Debe ingresar la oficina Enturne')
                continuar = false
            }
            if ($scope.Modelo.RegionPaisDestino == undefined || $scope.Modelo.RegionPaisDestino == '') {
                $scope.MensajesError.push('Debe ingresar la región destino 1')
                continuar = false
            }
            if ($scope.Modelo.CiudadDestino1 == undefined || $scope.Modelo.CiudadDestino1 == '') {
                $scope.MensajesError.push('Debe ingresar la ciudad destino 1')
                continuar = false
            }
            //if ($scope.Modelo.Ubicacion == undefined || $scope.Modelo.Ubicacion == '') {
            //    $scope.MensajesError.push('Debe ingresar la ubicación')
            //    continuar = false
            //} else {
            if ($scope.Modelo.FechaDisponible < new Date()) {
                $scope.MensajesError.push('La fecha horas de disponibilidad debe ser mayor a la actual')
                continuar = false
            }
            //}
            return continuar
        }

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarCiudades';
            }
        };
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoEnturnes = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Conductor: $scope.Modelo.Conductor,
                    }
                    blockUI.delay = 1000;
                    EnturnamientoFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoEnturnes = response.data.Datos

                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }

        $scope.ConfirmarAnular = function (item) {
            showModal('modalAnular')
            $scope.ItemAnula = item
        }
        $scope.CancelarTurno = function () {
            if ($scope.CausaAnulacion == undefined || $scope.CausaAnulacion == '') {
                ShowError('Debe ingresar el motivo de cancelación del turno')

            } else {
                closeModal('modalAnular')
                $scope.ItemAnula.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                $scope.ItemAnula.CausaAnulacion = $scope.CausaAnulacion
                $scope.ItemAnula.UsuarioAnula = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                EnturnamientoFactory.Anular($scope.ItemAnula).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('El turno se cancelo correctamente')
                            closeModal('modalAnular')
                            Find()
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }

        function localizacion(posicion) {
            $scope.Modelo.Latitud = posicion.coords.latitude
            $scope.Modelo.Longitud = posicion.coords.longitude
        }

        function errores(error) {
            MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
        }

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(localizacion, errores);
        } else {
            MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
        }

        //----------------------------------------------------------------------------------------------------
        $scope.MaskNumero = function (option) {
            try { $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno) } catch (e) { }
        };

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
    }]);