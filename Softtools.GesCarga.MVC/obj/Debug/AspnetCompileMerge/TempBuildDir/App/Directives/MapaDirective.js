﻿SofttoolsApp.directive('softtoolsMap', [function () {

    var link = function (scope, element) {
        //// cfpLoadingBar.start();
        var map, infoWindow;
        var markers = [];

        function initMap(posicion) {
            if (map === void 0) {
                var mapOptions = {
                    zoom: 15,
                    center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                    disableDefaultUI: true,
                    mapTypeId: window.google.maps.MapTypeId.ROADMAP
                }

                map = new window.google.maps.Map(element[0], mapOptions);
            }
        }

        function setMarker(map, position, title, content) {
            var marker;
            var markerOptions = {
                position: position,
                map: map,
                title: title,
                animation: google.maps.Animation.BOUNCE,
                //icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
            };

            marker = new window.google.maps.Marker(markerOptions);
            markers.push(marker);

            var infoWindowOptions = {
                content: content
            };
            infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
            infoWindow.open(map, marker);

        }

        function localizacion(posicion) {
            initMap(posicion);
            setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
        }

        function errores(error) {
            MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
        }

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(localizacion, errores);
        } else {
            MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
        }
        //// cfpLoadingBar.complete();
    };

    return {
        restrict: 'A',
        template: '<div id="gmaps"></div>',
        replace: true,
        link: link
    };
}]);