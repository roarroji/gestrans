﻿SofttoolsApp.factory('RecoleccionesFactory', ['$http', function ($http) {

    var urlService = GetUrl('Recolecciones');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ConsultarENCOE = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarENCOE';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ConsultarRecoleccionesPorPlanillar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRecoleccionesPorPlanillar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.AsociarRecoleccionesPlanilla = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/AsociarRecoleccionesPlanilla';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.GuardarEntrega = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GuardarEntregaRecoleccion';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.Cancelar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Cancelar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    return factory;
}]);