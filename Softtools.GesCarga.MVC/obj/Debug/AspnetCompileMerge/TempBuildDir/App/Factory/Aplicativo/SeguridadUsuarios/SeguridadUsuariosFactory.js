﻿SofttoolsApp.factory('SeguridadUsuariosFactory', ['$http', function ($http) {

    var urlService = GetUrl('Usuarios');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}    
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Validar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.CerrarSesion = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/CerrarSesion';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };



    return factory;
}]);