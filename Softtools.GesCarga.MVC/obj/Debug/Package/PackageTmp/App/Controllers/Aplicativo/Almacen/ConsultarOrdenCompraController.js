﻿SofttoolsApp.controller("ConsultarOrdenCompraCtrl", ['$scope', '$timeout', '$routeParams', 'blockUI', '$linq', 'OrdenCompraFactory', 'AlmacenesFactory', function ($scope, $timeout, $routeParams, blockUI, $linq, OrdenCompraFactory, AlmacenesFactory) {

    $scope.MapaSitio = [{ Nombre: 'Almacén' }, { Nombre: 'Documentos' }, { Nombre: 'Ordenes Compra' }];

    // ----------------------------- Decalaracion Variables ------------------------------------------------------//
    $scope.Buscando = false;
    $scope.ResultadoSinRegistros = '';
    $scope.MensajesError = [];
    $scope.totalRegistros = 0;
    $scope.paginaActual = 1;
    $scope.cantidadRegistrosPorPagina = 10;
    $scope.totalPaginas = 0;
    var filtros = {};
    $scope.ListadoEstados = [
        { Nombre: '(TODAS)', Codigo: -1 },
        { Nombre: 'DEFINITIVO', Codigo: 1 },
        { Nombre: 'BORRADOR', Codigo: 0 },
        { Nombre: 'ANULADO', Codigo: 2 }
    ];

    $scope.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

    // -------------------------- Constantes ---------------------------------------------------------------------//
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_ORDEN_COMPRA);
    //$scope.MapaSitio = [{ Nombre: 'Almacén' }, { Nombre: 'Documentos' }, { Nombre: 'Ordenes Compra' }];
    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar


    if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
        $scope.FechaInicial = new Date();
        $scope.FechaFinal = new Date();
    }

    /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };

    /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

    //Cargar el combo de almacenes
    AlmacenesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoAlmacenes = [];
                $scope.ListadoAlmacenes = response.data.Datos
                $scope.ListadoAlmacenes.push({ Nombre: '(Todos)', Codigo: 0 })
                $scope.Almacenes = $scope.ListadoAlmacenes[$scope.ListadoAlmacenes.length - 1];

            }
        }, function (response) {
            ShowError(response.statusText);
        });
    //---------------------------------------------------------------------Funcion Buscar Linea de Mantenimiento-----------------------------------------------------------------------------
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            Find()
        }
    };

    function Find() {
        $scope.ListadoOrdenCompra = [];
        if (DatosRequeridos()) {

            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoOrdenCompra = [];
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.NumeroInicial,
                FechaInicial: $scope.FechaInicial,
                FechaFinal: $scope.FechaFinal,
                CodigoEstadoDocumento: $scope.Estado.Codigo,
                NombreProveedor: $scope.Proveedor,
                Almacen: $scope.Almacenes,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina
            };


            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;

                OrdenCompraFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoOrdenCompra = response.data.Datos

                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        }
    };


    function DatosRequeridos() {
        $scope.MensajesError = [];
        var continuar = true;
        $scope.NumeroFinal = 0;
        if ($scope.FechaInicial !== null && $scope.FechaFinal !== null) {
            if ($scope.FechaInicial > $scope.FechaFinal) {
                $scope.MensajesError.push('La fecha inicial debe ser menor que la fecha final');
                continuar = false;
            }

        }

        if ($scope.NumeroInicial > 0 && $scope.NumeroFinal > 0) {
            if ($scope.NumeroInicial > $scope.NumeroFinal) {
                $scope.MensajesError.push('El número inicial debe ser menor que el número final');
                continuar = false;
            }
        }


        if ($scope.NumeroInicial > 0 && ($scope.NumeroFinal <= 0 || $scope.NumeroFinal == undefined || $scope.NumeroFinal == '')) {
            $scope.NumeroFinal = $scope.NumeroInicial;
        }
        
        if ($scope.FechaInicial !== null && $scope.FechaFinal !== null) {
            if ($scope.FechaInicial > $scope.FechaFinal) {
                $scope.MensajesError.push('La fecha inicial debe ser menor que la fecha final');
                continuar = false;
            }
        }
        if ($scope.FechaInicial !== null && $scope.FechaFinal == null) {
            $scope.FechaFinal = $scope.FechaInicial;
        }
        if (($scope.FechaInicial == null || $scope.FechaInicial == undefined) && ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined)) {
            $scope.MensajesError.push('Debe seleccionar la fecha inicial');
            continuar = false;
        }
        if ($scope.NumeroInicial > 0 && $scope.NumeroFinal > 0) {

        }
        else if (($scope.FechaInicial == null && $scope.FechaFinal == null) && (($scope.NumeroInicial == 0 || $scope.NumeroInicial == undefined || $scope.NumeroInicial == '') && ($scope.NumeroFinal == 0 || $scope.NumeroFinal == undefined || $scope.NumeroFinal == ''))) {
            $scope.MensajesError.push('Debe ingresar al menos una fecha o rango de fechas o un número');
            continuar = false;
        }
        return continuar;

    }

    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //Validacion boton anular
    $scope.ConfirmacionAnularOrden = function (numero, Estado) {
        //$scope.estadoanulado = Estado;
        $scope.NumeroAnulacion = numero;
        //if ($scope.estadoanulado.Codigo !== ESTADO_ANULADO) {
        showModal('modalConfirmacionAnularOrden');
        //}
        //else {
        //    closeModal('modalConfirmacionAnularOrden');
        //}
    };
    //Funcion que solicita los datos de la anulación 
    $scope.SolicitarDatosAnulacion = function () {
        $scope.FechaAnulacion = new Date();
        closeModal('modalConfirmacionAnularOrden');
        showModal('modalDatosAnularOrden');
    };
    //Funcion Anular orden compra
    $scope.Anular = function () {

        $scope.ListadoOrdenCompra.forEach(function (item) {
            if (item.Numero == $scope.NumeroAnulacion) {
                $scope.Estado = item.Estado;
            }
        });

        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Numero: $scope.NumeroAnulacion,
            UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CausaAnulacion: $scope.CausaAnulacion,
            OficinaAnula: $scope.Sesion.UsuarioAutenticado.Oficinas[0]
        };

        OrdenCompraFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se anuló la orden de compra  No.' + entidad.Numero);
                    closeModal('modalDatosAnularOrden');
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                var result = ''
                result = InternalServerError(response.statusText)
                showModal('modalMensajeEliminarOrdenCompra');
                $scope.ModalError = 'No se puede eliminar la orden de compra' + $scope.numero + ' ya que se encuentra relacionada con ' + result;
                $scope.ModalErrorCompleto = response.statusText

            });

    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarOrdenCompra';
        }
    };

    //$scope.MaskNumero = function (option) {
    //    try { $scope.Codigo = MascaraNumero($scope.Codigo) } catch (e) { }
    //};
    //$scope.urlASP = '';
    //$scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
    //$scope.DesplegarInforme = function (Numero) {
    //    if ($scope.ValidarPermisos.AplicaImprimir == PERMISO_ACTIVO) {
    //        $scope.FiltroArmado = '';

    //        //Depende del listado seleccionado se enviará el nombre por parametro
    //        $scope.Numero = Numero;
    //        $scope.NombreReporte = 'RepOrdenCompra'
    //        //Arma el filtro
    //        $scope.ArmarFiltro();
    //        //Llama a la pagina ASP con el filtro por GET

    //        window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + 1);

    //        //Se limpia el filtro para poder hacer mas consultas
    //        $scope.FiltroArmado = '';
    //    };
    //};

    //// funcion enviar parametros al proyecto ASP armando el filtro
    //$scope.ArmarFiltro = function () {
    //    $scope.FiltroArmado = '';
    //    if ($scope.Numero !== undefined && $scope.Numero !== '' && $scope.Numero !== null) {
    //        $scope.FiltroArmado += '&Numero=' + $scope.Numero;
    //    }
    //}
    //$scope.MaskNumero = function () {
    //    try { $scope.NumeroInicial = MascaraNumero($scope.NumeroInicial) } catch (e) { }
    //    try { $scope.numeroFinal = MascaraNumero($scope.numeroFinal) } catch (e) { }
    //};

    if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
        $scope.NumeroInicial = $routeParams.Numero;
        $scope.NumeroFinal = $routeParams.Numero;
        //$scope.Estado = { Codigo: 0, Nombre: '(NO Aplica)' }
        Find();
        $scope.NumeroInicial = ''
        $scope.NumeroFinal = ''
    }


}]);