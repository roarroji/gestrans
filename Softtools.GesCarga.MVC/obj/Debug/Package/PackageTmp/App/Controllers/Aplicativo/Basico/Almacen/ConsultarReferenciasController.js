﻿SofttoolsApp.controller("ConsultarReferenciasCtrl", ['$scope', '$timeout', 'ReferenciasFactory', 'GrupoReferenciasFactory', '$linq', 'blockUI', '$routeParams', function ($scope, $timeout, ReferenciasFactory,GrupoReferenciasFactory, $linq, blockUI, $routeParams) {

    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Almacén' }, { Nombre: 'Referencias' }];


    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
    
    var filtros = {};
    $scope.cantidadRegistrosPorPaginas = 10;
    $scope.paginaActual = 1;
    $scope.ModalErrorCompleto = ''
    $scope.ModalError = ''
    $scope.MostrarMensajeError = false
    $scope.ListadoEstados = [
        { Nombre: '(TODAS)', Codigo: -1},
        { Nombre: 'ACTIVA', Codigo: 1 },
        { Nombre: 'INACTIVA', Codigo: 0 }
    ];
    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
    $scope.ListadoGrupoReferencias = [];
    $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_REFERENCIAS);

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };


    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarReferencias';
        }
    };

    /*Cargar el combo de grupo referencias*/
    GrupoReferenciasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoGrupoReferencias = response.data.Datos;
                $scope.ListadoGrupoReferencias.push({Nombre: '(TODOS)', Codigo: 0})
                $scope.GrupoReferencia = $scope.ListadoGrupoReferencias[$scope.ListadoGrupoReferencias.length - 1]

            }
        }, function (response) {
            ShowError(response.statusText);
        });

    //-------------------------------------------------FUNCIONES--------------------------------------------------------
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            $scope.Codigo = 0
            Find()
        }
    };

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);
        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoReferencias = [];
        if ($scope.Buscando){
            if ($scope.MensajesError.length == 0) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoReferencia: $scope.CodigoReferencia,
                    Nombre: $scope.Nombre,
                    GrupoReferencias: $scope.GrupoReferencia,
                    Codigo: $scope.Codigo,
                    Marca: $scope.Marca,
                    Estado: $scope.ModalEstado.Codigo,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                }
                blockUI.delay = 1000;
                ReferenciasFactory.Consultar(filtros).
                       then(function (response) {
                           if (response.data.ProcesoExitoso === true) {
                               if (response.data.Datos.length > 0) {
                                   $scope.ListadoReferencias = response.data.Datos

                                   $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                   $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                   $scope.Buscando = false;
                                   $scope.ResultadoSinRegistros = '';
                               }
                               else {
                                   $scope.totalRegistros = 0;
                                   $scope.totalPaginas = 0;
                                   $scope.paginaActual = 1;
                                   $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                   $scope.Buscando = false;
                               }
                               var listado = [];
                               response.data.Datos.forEach(function (item) {
                                   listado.push(item);
                               });
                           }
                       }, function (response) {
                           ShowError(response.statusText);
                       });
            }
        }
        blockUI.stop();
    }
    /*------------------------------------------------------------------------------------Eliminar referencias-----------------------------------------------------------------------*/
    $scope.EliminarReferencias = function (codigo, Nombre, CodigoAlterno) {
        $scope.AuxCodigo = codigo
        $scope.Nombre = Nombre
        $scope.CodigoAlterno = CodigoAlterno
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        showModal('modalEliminarReferencias');
    };

    $scope.Eliminar = function () {
        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.AuxCodigo,
        };

        ReferenciasFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    var CodigoAlterno = $scope.CodigoAlterno;
                    var Nombre = $scope.Nombre;
                    ShowSuccess('Se eliminó la Referencia ' + Nombre);
                    closeModal('modalEliminarReferencias');
                    $scope.CodigoAlterno = '';
                    $scope.Nombre = '';
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                var result = ''
                result = InternalServerError(response.statusText)
                showModal('modalMensajeEliminarReferencias');
                $scope.ModalError = 'No se puede eliminar la Referencia ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                $scope.ModalErrorCompleto = response.statusText

            });

    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /* Obtener parametros*/
    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        $scope.Codigo = $routeParams.Codigo;
        Find();
    }
    $scope.CerrarModal = function () {
        closeModal('modalEliminarReferencias');
        closeModal('modalMensajeEliminarReferencias');
    }

    $scope.MaskNumero = function () {
        $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno)
        
    };

}]);