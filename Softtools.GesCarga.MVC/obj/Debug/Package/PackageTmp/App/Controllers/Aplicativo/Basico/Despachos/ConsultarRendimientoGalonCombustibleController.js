﻿SofttoolsApp.controller("ConsultarRendimientoGalonCombustibleCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'RendimientoGalonCombustibleFactory', 'ValorCatalogosFactory', 'RutasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, RendimientoGalonCombustibleFactory, ValorCatalogosFactory, RutasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Rendimiento Galón' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        $scope.Modelo = {}
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + 100214);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar


        /*Cargar el combo de tipo vehiculo*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO }, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoVehiculo = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoVehiculo = response.data.Datos;

                        $scope.Modelo.TipoVehiculo = $scope.ListadoTipoVehiculo[0]
                    }
                    else {
                        $scope.ListadoTipoVehiculo = []
                    }
                }
            }, function (response) {
            });
        RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoRutas = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoRutas = response.data.Datos

                    }
                    else {
                        $scope.ListadoRutas = []
                    }
                }
            }, function (response) {
            });
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarRendimientoGalonCombustible';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };
        function Find() {
         
            blockUI.start('Buscando registros ...'); 
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoRendimientoGalonCombustible = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        TipoVehiculo: $scope.Modelo.TipoVehiculo,
                        Ruta: $scope.Modelo.Ruta,
                        Codigo: $scope.Codigo,
                        Estado: $scope.ModalEstado.Codigo,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    }
                    blockUI.delay = 1000;
                    console.log('buscando filtros', filtros  )
                    RendimientoGalonCombustibleFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoRendimientoGalonCombustible = response.data.Datos
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        /*------------------------------------------------------------------------------------Eliminar Color Vehiculos-----------------------------------------------------------------------*/
        $scope.EliminarRendimientoGalonCombustible = function (codigo, Nombre, CodigoAlterno) {
            $scope.Codigo = codigo
            $scope.Nombre = Nombre
            ShowConfirm('¿Esta Seguro de eliminar el registro?', $scope.Eliminar)
        };

        $scope.EliminarCombustible = function (Fecha, TipoCombustible, CodigoCombus, Valor) {

            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false  
                showModal('modalEliminarCombustible');
        };



        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
            };
            RendimientoGalonCombustibleFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Codigo = 0
                        Find();

                        ShowSuccess('Se eliminó el rendimiento ' + $scope.Nombre);
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);

                });

        };


        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /* Guardar Masivo parametros*/
        $scope.GuardarMasiva = function () {
            showModal('modalConfirmacionGuardar')
        };

        $scope.Actualizar = function () {
            closeModal('modalConfirmacionGuardar')
            if (DatosRequeridos()) {
                //Estado
                RendimientoGalonCombustibleFactory.Actualizar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoVehiculo: { Codigo: $scope.Modelo.TipoVehiculo.Codigo },
                    Ruta: { Codigo: $scope.Modelo.Ruta.Codigo },
                    RendimientoGalon: $scope.Modelo.RendimientoGalon,
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo}
                }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo === 0) {
                                    ShowSuccess('Se guardó el rendimiento para la ruta "' + $scope.Modelo.Ruta.Nombre + '"');
                                    location.href = '#!ConsultarRendimientoGalonCombustible/' + response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modificó el rendimiento para la ruta "' + $scope.Modelo.Ruta.Nombre + '"');
                                    location.href = '#!ConsultarRendimientoGalonCombustible/' + $scope.Modelo.Codigo;
                                }
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError('No se pudo guardar el rendimiento galón. ' + response.statusText);
                    });
            }
        };
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if (($scope.Modelo.Ruta === undefined || $scope.Modelo.Ruta === '') && ($scope.Modelo.TipoVehiculo === undefined || $scope.Modelo.TipoVehiculo === '' || $scope.Modelo.TipoVehiculo.Codigo === 2200)) {
                $scope.MensajesError.push('Debe ingresar ruta y vehiculo valido');
                continuar = false;            
            }
           
            if ($scope.Modelo.RendimientoGalon === undefined || $scope.Modelo.RendimientoGalon === '' || $scope.Modelo.RendimientoGalon === 0) {
                $scope.MensajesError.push('Debe ingresar el rendimiento galón');
                continuar = false;
            }

            return continuar;
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /* Obtener parametros*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }

        $scope.CerrarModal = function () {
            closeModal('modalMensajeEliminarRendimientoGalonCombustible');
        }

        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };



    }]);