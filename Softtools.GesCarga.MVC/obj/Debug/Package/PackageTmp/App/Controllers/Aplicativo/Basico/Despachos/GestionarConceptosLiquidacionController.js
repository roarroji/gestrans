﻿SofttoolsApp.controller("GestionarConceptosLiquidacionCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'PlanUnicoCuentasFactory', 'OficinasFactory', 'VehiculosFactory', 'ConceptoLiquidacionFactory', 'TercerosFactory', 'ImpuestosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, PlanUnicoCuentasFactory, OficinasFactory, VehiculosFactory, ConceptoLiquidacionFactory, TercerosFactory, ImpuestosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Concepto Liquidacion' }, { Nombre: 'Gestionar' }];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONCEPTOS_LIQUIDACION);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.paginaActual = 1;
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.Codigo = 0;
        $scope.Numero = 0;
        $scope.ListaRecorridosConsultados = [];
        $scope.ListaResultadoFiltroConsulta = [];
        $scope.ListadoRecorridosTotal = [];
        $scope.ModeloFechaCrea = '';

        $scope.ListadoAuxiliar = [];
        $scope.MensajesErrorfun = [];
        $scope.MensajesErrorfil = [];
        $scope.listadoGuiasSeleccionadas = [];
        $scope.listaGuiasNoSeleccionadas = [];
        $scope.ListadoImpuestos = [];
        $scope.RecorridosSeleccionados = false;
        $scope.ListaAuxiliargrid = [];
        $scope.seleccionarCheck = true;
        $scope.registrorepetido = 0;
        $scope.ModeloFecha = new Date();
        $scope.detalleguia = '';
        $scope.Deshabilitar = false;
        




        $scope.ListadoEstados = [
            { Nombre: '(SELECCIONE ITEM)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 }
        ];
        $scope.ListadoOperacion = [
            { Nombre: '(SELECCIONE ITEM)', Codigo: -1 },
            { Nombre: 'SUMA (+)', Codigo: 1 },
            { Nombre: 'RESTA (-)', Codigo: 2 }
        ];

        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        $scope.ModeloOperacion = $linq.Enumerable().From($scope.ListadoOperacion).First('$.Codigo == -1');

        $scope.ListadoCiudades = [];
        // Se crea la propiedad modelo en el ambito
        //$scope.Modelo = {
        //    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        //    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        //    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        //    Codigo: 0,
        //    Estado: $scope.ListadoEstados[0],

        //}
        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.Codigo = $routeParams.Codigo;
            if ($scope.Codigo > 0) {
                Obtener()
            }
        }
        else {
            $scope.Codigo = 0;
        }

        $scope.PrimerPagina = function () {
            /*Verificar si la página actual ya está guardada si no lo esta procede a realizar la busqueda*/
            if ($scope.paginaActual > 1) {

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                    $scope.paginaActual = 1;
                    $scope.ListadoImpuestos = [];

                    var i = 0;
                    for (i = 0; i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina) {
                            $scope.ListadoImpuestos.push($scope.ListaResultadoFiltroConsulta[i])
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = 1;
                        $scope.ListadoImpuestos = [];

                        var i = 0;
                        for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina) {
                                $scope.ListadoImpuestos.push($scope.ListadoRecorridosTotal[i])
                            }
                        }
                    }
                }

            }


        };

        $scope.Siguiente = function () {

            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.ListadoImpuestos = [];
                $scope.PaginaAuxiliar = $scope.paginaActual;
                $scope.paginaActual += 1;

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoImpuestos.push($scope.ListaResultadoFiltroConsulta[i]);
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoImpuestos.push($scope.ListadoRecorridosTotal[i]);
                            }
                        }
                    }
                }
            }

        }

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {

                    $scope.ListadoImpuestos = [];
                    $scope.paginaActual -= 1;

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoImpuestos.push($scope.ListaResultadoFiltroConsulta[i]);
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.ListadoImpuestos = [];
                        $scope.paginaActual -= 1;

                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoImpuestos.push($scope.ListadoRecorridosTotal[i]);
                            }
                        }
                    }
                }

            }
        };

        $scope.UltimaPagina = function () {

            if ($scope.paginaActual < $scope.totalPaginas) {

                if ($scope.ListaResultadoFiltroConsulta.length > 0) {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListadoImpuestos = [];

                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListaResultadoFiltroConsulta.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoImpuestos.push($scope.ListaResultadoFiltroConsulta[i])
                            }
                        }
                    }
                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListadoImpuestos = [];

                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoImpuestos.push($scope.ListadoRecorridosTotal[i])
                            }
                        }
                    }
                }

            }
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando Concepto Liquidación Código ' + $scope.Codigo);

            $timeout(function () {
                blockUI.message('Cargando Concepto Liquidación Código ' + $scope.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                Numero: $scope.Numero,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
            };

            blockUI.delay = 1000;
            ConceptoLiquidacionFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.ModeloCodigo = response.data.Datos.Codigo;
                        $scope.ModeloCodigoAlterno = response.data.Datos.CodigoAlterno;

                        $scope.ModeloNombre = response.data.Datos.Nombre;

                        $scope.ModeloEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado);
                        $scope.ModeloOperacion = $linq.Enumerable().From($scope.ListadoOperacion).First('$.Codigo == ' + response.data.Datos.Operacion);

                        $scope.ModeloFijo = response.data.Datos.ValorFijo;
                        $scope.ModeloPorcentaje = response.data.Datos.ValorPorcentaje;
                        $scope.ModeloValorMinimo = response.data.Datos.ValorMinimo;
                        $scope.ModeloValorMaximo = response.data.Datos.ValorMaximo;
                        $scope.ModeloFechaCrea = new Date(response.data.Datos.FechaCrea);
                        $scope.ModeloConceptoSistema = response.data.Datos.ConceptoSistema;
                        if ($scope.ModeloConceptoSistema == 1) {
                            $scope.Deshabilitar=true
                        }
                        try {
                            $scope.ModeloCuenta = $linq.Enumerable().From($scope.ListaPuc).First('$.Codigo ==' + response.data.Datos.PUC.Codigo);
                        } catch (e) {
                            $scope.ModeloCuenta = response.data.Datos.PUC;
                        }
                        $scope.MaskValores();

                    }
                    else {
                        ShowError('No se logro consultar la Orden de Cargue ' + $scope.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarConceptosLiquidacion';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarConceptosLiquidacion';
                });
            blockUI.stop();

        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        // Vehiculo

        PlanUnicoCuentasFactory.Consultar({
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Estado: ESTADO_ACTIVO
        }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ CodigoNombreCuenta: '', CodigoCuenta: '', Codigo: 0 })
                        $scope.ListaPuc = response.data.Datos;
                        try {
                            $scope.ModeloCuenta = $linq.Enumerable().From($scope.ListaPuc).First('$.Codigo ==' + $scope.ModeloCuenta.Codigo);
                        } catch (e) {
                            $scope.ModeloCuenta = $scope.ListaPuc[$scope.ListaPuc.length - 1];
                        }

                    }
                    else {
                        $scope.ListaPuc = [];
                        $scope.ModeloCuenta = null;
                    }

                }
            }, function (response) {
                ShowError(response.statusText);
            });
               
        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        $scope.Guardar = function () {
            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardarConcepto');
            if (DatosRequeridos()) {
                $scope.objEnviar = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.ModeloCodigo,
                    CodigoAlterno: $scope.ModeloCodigoAlterno,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Nombre: $scope.ModeloNombre,
                    Operacion: $scope.ModeloOperacion.Codigo,
                    Estado: $scope.ModeloEstado.Codigo,
                    AplicaImpuesto: ESTADO_INACTIVO,
                    ValorFijo: $scope.ModeloFijo,
                    ValorPorcentaje: $scope.ModeloPorcentaje,
                    PUC: { Codigo: $scope.ModeloCuenta.Codigo },
                    ValorMinimo: $scope.ModeloValorMinimo,
                    ValorMaximo: $scope.ModeloValorMaximo, 
                    FechaCrea: $scope.ModeloFechaCrea
                };

                ConceptoLiquidacionFactory.Guardar($scope.objEnviar).
                    then(function (response) {
                        
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Numero == 0) {
                                    ShowSuccess('Se guardó el concepto : ' + $scope.ModeloNombre);
                                }
                                else {
                                    ShowSuccess('Se modificó el concepto : ' + $scope.ModeloNombre);
                                }
                                location.href = '#!ConsultarConceptosLiquidacion/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                            
                            ShowError("El registro con el código: " + $scope.ModeloCodigo +" ya se encuentra en el sistema");
                    });
            }
        };

        $scope.ConfirmacionGuardarConcepto = function () {
            showModal('modalConfirmacionGuardarConcepto');
        };
        console.clear()
        function DatosRequeridos() {
            window.scrollTo(top, top);
            $scope.MensajesError = [];
            var continuar = true;


            if ($scope.ModeloNombre == -1 || $scope.ModeloNombre == undefined || $scope.ModeloNombre == null || $scope.ModeloNombre == "") {  //NO APLICA
                $scope.MensajesError.push('Debe ingresar el Nombre');
                continuar = false
            }
            if ($scope.ModeloOperacion.Codigo == -1) {  //NO APLICA
                $scope.MensajesError.push('Debe Seleccionar un Tipo de operacion ');
                continuar = false
            }
            if ($scope.ModeloEstado.Codigo == -1) {  //NO APLICA
                $scope.MensajesError.push('Debe Seleccionar un Estado');
                continuar = false
            }
            if ($scope.ModeloCuenta == '' || $scope.ModeloCuenta == undefined) {
                $scope.MensajesError.push('Debe Seleccionar una cuenta PUC');
                continuar = false
            } else if ($scope.ModeloCuenta.Codigo == 0) {
                $scope.MensajesError.push('Debe Seleccionar una cuenta PUC');
                continuar = false
            }

            return continuar;
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if ($routeParams.Numero > 0) {
                document.location.href = '#!ConsultarConceptosLiquidacion/' + $scope.ModeloCodigo;
            } else {
                document.location.href = '#!ConsultarConceptosLiquidacion';
            }
        };
        $scope.AgregarAuxiliar = function () {
            showModal('ModalVerAuxiliar');
        }
        $scope.CerrarModal = function () {
            closeModal('ModalVerAuxiliar');
        }
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope);
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskDecimales = function () {
            return MascaraDecimalesGeneral($scope)
        }
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskTelefono = function () {
            return MascaraTelefono($scope);
        }
        $scope.MaskDireccion = function () {
            return MascaraDireccion($scope)
        }

    }]);