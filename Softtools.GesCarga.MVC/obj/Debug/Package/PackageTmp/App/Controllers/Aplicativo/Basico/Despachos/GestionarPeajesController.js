﻿SofttoolsApp.controller("GestionarPeajesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'PeajesFactory', 'TercerosFactory', 'blockUIConfig',
    'ValorCatalogosFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, PeajesFactory, TercerosFactory, blockUIConfig,
        ValorCatalogosFactory) {

        $scope.Titulo = 'GESTIONAR PEAJE';
        $scope.Master = "#!ConsultarPeajes";
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Peajes' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PEAJES);

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PEAJES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.Deshabilitar = true;
        $scope.ListadoEstados = [];

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            Nombre: '',
            Codigo_Alterno:'',
            Estado: 0,
            TipoPeaje: {}
        };


        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ]
        $scope.Modelo.Estado = $scope.ListadoEstados[0];


        //-------------------------------------- CATALOGOS --------------------------------------//
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_PEAJE } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoPeaje = [];
                    $scope.ListadoTipoPeaje.push({ Codigo: -1, Nombre: 'Seleccione' });
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoTipoPeaje.push(response.data.Datos[i]);
                        }
                        $scope.Modelo.TipoPeaje = $linq.Enumerable().From($scope.ListadoTipoPeaje).First('$.Codigo == -1');
                    }
                    else {
                        $scope.ListadoTipoPeaje = [];
                    }
                }
            }, function (response) {
            });
        //-------------------------------------- CATALOGOS --------------------------------------//
        //-------------------------------------- PROVEEDORES --------------------------------------//
        $scope.ListadoProveedores = [];
        $scope.AutocompleteProveedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROVEEDOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoProveedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProveedores);
                }
            }
            return $scope.ListadoProveedores;
        }
        //-------------------------------------- PROVEEDORES --------------------------------------//
        //-------------------------------------- MAPA --------------------------------------//
        $scope.iniciarMapa = function (lat, lng) {
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat
            $scope.lng = lng
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    }

                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.BOUNCE,
                    title: title,
                    //icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                };

                markers = new window.google.maps.Marker(markerOptions);
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input 
                    $scope.Modelo.Latitud = this.getPosition().lat();
                    $scope.Modelo.Longitud = this.getPosition().lng();
                });
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        }
        $scope.iniciarMapa();
        //-------------------------------------- MAPA --------------------------------------//
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando peaje código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando peaje Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            PeajesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        console.log(response.data)
                        $scope.Modelo = response.data.Datos;
                        $scope.Modelo.Proveedor = $scope.CargarTercero($scope.Modelo.Proveedor.Codigo)
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Estado);
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa;
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo };
                        $scope.Modelo.TipoPeaje = $linq.Enumerable().From($scope.ListadoTipoPeaje).First('$.Codigo ==' + response.data.Datos.TipoPeaje.Codigo);
                        $scope.Modelo.Latitud = response.data.Datos.Latitud;
                        $scope.Modelo.Longitud = response.data.Datos.Longitud;
                        $scope.iniciarMapa($scope.Modelo.Latitud, $scope.Modelo.Longitud);
                    }
                    else {
                        ShowError('No se logro consultar el peaje código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = $scope.Master;
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = $scope.Master;
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {
                $scope.Modelo.Latitud = parseFloat($scope.Modelo.Latitud)
                $scope.Modelo.Longitud = parseFloat($scope.Modelo.Longitud)
                ObjEnviar = {
                    Codigo: $scope.Modelo.Codigo,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Nombre: $scope.Modelo.Nombre,
                    Codigo_Alterno: $scope.Modelo.Codigo_Alterno,
                    Proveedor: { Codigo: $scope.Modelo.Proveedor.Codigo },
                    Contacto: $scope.Modelo.Contacto,
                    Telefono: $scope.Modelo.Telefono,
                    Celular: $scope.Modelo.Celular,
                    Ubicacion: $scope.Modelo.Ubicacion,
                    Estado: $scope.Modelo.Estado.Codigo,
                    Longitud: $scope.Modelo.Longitud,
                    Latitud: $scope.Modelo.Latitud,
                    TipoPeaje: { Codigo: $scope.Modelo.TipoPeaje.Codigo },
                    UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                }
                PeajesFactory.Guardar(ObjEnviar).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el peaje "' + $scope.Modelo.Nombre + '"');
                                }
                                else {
                                    ShowSuccess('Se modificó el peaje "' + $scope.Modelo.Nombre + '"');
                                }
                                location.href = $scope.Master + '/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        function DatosRequeridos() {
            $scope.MensajesError = [];
            window.scrollTo(top, top);
            var modelo = $scope.Modelo
            var continuar = true;

            var PeajeExiste = PeajesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Nombre: $scope.Modelo.Nombre, Estado: 1, Sync: true })  
            if ($scope.Modelo.Codigo == 0) {
                if (PeajeExiste.Datos.length > 0) {
                    $scope.MensajesError.push('El peaje ' + $scope.Modelo.Nombre + ' ya se encuentra creado ');
                    continuar = false;
                }
            }
          


            if (ValidarCampo(modelo.Nombre) == 1) {
                $scope.MensajesError.push('Debe ingresar el nombre');
                continuar = false;
            }


            if ($scope.Modelo.TipoPeaje.Codigo == -1) {
                $scope.MensajesError.push('Debe seleccionar tipo peaje');
                continuar = false;
            } 
            if ($scope.Modelo.Proveedor == undefined || $scope.Modelo.Proveedor == null || $scope.Modelo.Proveedor == '' || $scope.Modelo.Proveedor.Codigo == '0' ) {
                $scope.MensajesError.push('Debe seleccionar un proveedor');
                continuar = false;
            }
            if (ValidarCampo(modelo.Ubicacion) == 1) {
                $scope.MensajesError.push('Debe ingresar la ubicación');
                continuar = false;
            }

            return continuar;
        }

        //-------------------------------------- PARAMETROS --------------------------------------//
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
        }
        //-------------------------------------- PARAMETROS --------------------------------------//
        $scope.VolverMaster = function () {
            document.location.href = $scope.Master;
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MasckDecimal = function (option) {
            return MascaraDecimales(option)
        }
    }]);