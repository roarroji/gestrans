﻿SofttoolsApp.controller("GestionarSitiosCargueDescargueCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'EmpresasFactory', 'SitiosCargueDescargueFactory', 'PaisesFactory', 'CiudadesFactory', 'ValorCatalogosFactory', 'blockUIConfig', 'ZonasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, EmpresasFactory, SitiosCargueDescargueFactory, PaisesFactory, CiudadesFactory, ValorCatalogosFactory, blockUIConfig, ZonasFactory) {

        $scope.Titulo = 'GESTIONAR SITIOS CARGUE DESCARGUE';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Sitios Cargue Descargue' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_SITIOS_CARGUE_DESCARGUE);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.ListadoZonas = [];
        $scope.ListadoPaises = [];
        $scope.ListadoCiudades = [];
        $scope.ListadoSitios = [];
        $scope.CargarPaisUsuario = 0;
        $scope.Deshabilitar = true;
        $scope.BloquearZona = true;

        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVO" },
            { Codigo: 1, Nombre: "ACTIVO" }
        ];

        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.ModeloCodigo = $routeParams.Codigo;
            Obtener();
        }
        else {
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
            $scope.ModeloCodigo = 0;
        }

        $scope.iniciarMapa = function (lat, lng) {
            var marker = [];
            var map, infoWindow;
            $scope.lat = lat;
            $scope.lng = lng;
            function initMap(posicion) {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude),
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    };
                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                }
            }

            function setMarker(map, position, title, content) {
                var markers;
                var markerOptions = {
                    position: position,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.BOUNCE,
                    title: title
                };

                markers = new window.google.maps.Marker(markerOptions);
                markers.addListener('dragend', function (event) {
                    //escribimos las coordenadas de la posicion actual del marcador dentro los input 
                    $scope.ModeloLatitud = this.getPosition().lat();
                    $scope.ModeloLongitud = this.getPosition().lng();
                });
                marker.push(markers);

                var infoWindowOptions = {
                    content: content
                };
                infoWindow = new window.google.maps.InfoWindow(infoWindowOptions);
                infoWindow.open(map, markers);

            }

            function localizacion(posicion) {
                initMap(posicion);
                if ($scope.lat != undefined && $scope.lng != undefined && $scope.lat != 0 && $scope.lng != 0) {
                    setMarker(map, new window.google.maps.LatLng($scope.lat, $scope.lng), 'Posición del usuario', 'Me encuentro aquí');
                } else {
                    setMarker(map, new window.google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude), 'Posición actual del usuario', 'Me encuentro aquí');
                }
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }

        };

        $scope.iniciarMapa();

        /*---------------------------------------------------------------------------------------COMBOS Y AUTOCOMPLETE------------------------------------------------------------------------------------*/
        /*Cargar pais usuario*/
        EmpresasFactory.Obtener({ Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.CargarPaisUsuario = response.data.Datos.Pais.Codigo;
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de paises*/
        PaisesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoPaises = response.data.Datos;
                        if ($scope.CodigoPais !== undefined && $scope.CodigoPais !== '' && $scope.CodigoPais !== null) {
                            $scope.ModeloPais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + $scope.CodigoPais);
                        } else {
                            $scope.ModeloPais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + $scope.CargarPaisUsuario);
                        }
                    } else {
                        $scope.ListadoPaises = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de ciudades*/
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades)
                }
            }
            return $scope.ListadoCiudades;
        };

        /*Cargar el combo de tipo sitios*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SITIO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: 'Seleccione tipo sitio', Codigo: 0 })
                        $scope.ListadoSitios = response.data.Datos
                        if ($scope.CodigoSitio !== undefined && $scope.CodigoSitio !== '' && $scope.CodigoSitio !== null) {
                            $scope.ModeloTipoSitio = $linq.Enumerable().From($scope.ListadoSitios).First('$.Codigo ==' + $scope.CodigoSitio);
                        } else {
                            $scope.ModeloTipoSitio = $linq.Enumerable().From($scope.ListadoSitios).First('$.Codigo == 0');
                        }
                    }
                    else {
                        $scope.ListadoSitios = []
                    }
                }
            }, function (response) {
            });

        /*Se cambia la zona de la ciudad*/
        $scope.CambiarZonasCiudad = function (ciudad) {
            if (ciudad !== '') {
                if (ciudad.Codigo !== undefined) {
                    $scope.ObjetoCiudad = ciudad;
                    $scope.BloquearZona = false;
                    CambiarZonaCiudadDestinatario();
                } else {
                    $scope.Distribucion.Destinatario.Ciudad = '';
                    $scope.ListadoZonas = [];
                    $scope.BloquearZona = true;
                }
            } else {
                $scope.Distribucion.Destinatario.Ciudad = '';
                $scope.ListadoZonas = [];
                $scope.BloquearZona = true;
            }
        };

        /*Cargar combo lista zonas ciudades*/
        function CambiarZonaCiudadDestinatario() {
            ZonasFactory.Consultar({ Ciudad: $scope.ObjetoCiudad, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoZonas = [];
                        $scope.ListadoZonas.push({ Nombre: '(NO APLICA)', Codigo: 0 });
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                if (item.Codigo !== 0) {
                                    $scope.ListadoZonas.push(item);
                                }
                            });
                            if ($scope.CodigoZona !== undefined && $scope.CodigoZona !== '' && $scope.CodigoZona !== null) {
                                $scope.ModeloZona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo ==' + $scope.CodigoZona);
                            } else {
                                $scope.ModeloZona = $scope.ListadoZonas[0];
                            }
                        } else {
                            $scope.ModeloZona = $scope.ListadoZonas[0];
                        }
                    }
                }), function (response) {
                };
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando sitio cargue/descargue código ' + $scope.ModeloCodigo);

            $timeout(function () {
                blockUI.message('Cargando sitio cargue/descargue Código ' + $scope.ModeloCodigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.ModeloCodigo,
            };

            blockUI.delay = 1000;
            SitiosCargueDescargueFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.CambiarZonasCiudad(response.data.Datos.Ciudad);

                        $scope.ModeloCodigo = response.data.Datos.Codigo;
                        $scope.ModeloNombre = response.data.Datos.Nombre;

                        $scope.CodigoSitio = response.data.Datos.CodigoTipoSitio
                        if ($scope.ListadoSitios.length > 0 && $scope.CodigoSitio > 0) {
                            $scope.ModeloTipoSitio = $linq.Enumerable().From($scope.ListadoSitios).First('$.Codigo == ' + response.data.Datos.CodigoTipoSitio);
                        }

                        $scope.CodigoPais = response.data.Datos.Pais.Codigo
                        if ($scope.ListadoPaises.length > 0 && $scope.CodigoPais > 0) {
                            $scope.ModeloPais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo == ' + response.data.Datos.Pais.Codigo);
                        }

                        $scope.CodigoPais = response.data.Datos.Pais.Codigo
                        if ($scope.ListadoPaises.length > 0 && $scope.CodigoPais > 0) {
                            $scope.ModeloPais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo == ' + response.data.Datos.Pais.Codigo);
                        }

                        $scope.CodigoZona = response.data.Datos.Zona.Codigo;
                        if ($scope.ListadoZonas.length > 0 && $scope.CodigoZona > 0) {
                            $scope.ModeloZona = $linq.Enumerable().From($scope.ListadoZonas).First('$.Codigo == ' + response.data.Datos.Zona.Codigo);
                        }

                        $scope.ModeloCiudad = $scope.CargarCiudad(response.data.Datos.Ciudad.Codigo);

                        $scope.ModeloDireccion = response.data.Datos.Direccion;
                        $scope.ModeloTelefono = response.data.Datos.Telefono;
                        $scope.ModeloContacto = response.data.Datos.Contacto;
                        $scope.ModeloLatitud = response.data.Datos.Latitud;
                        $scope.ModeloLongitud = response.data.Datos.Longitud;
                        $scope.ModeloCargue = MascaraValores(response.data.Datos.ValorCargue);
                        $scope.ModeloDescargue = MascaraValores(response.data.Datos.ValorDescargue);
                        $scope.iniciarMapa($scope.ModeloLatitud, $scope.ModeloLongitud)
                        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                    }
                    else {
                        ShowError('No se logro consultar el sitio cargue/descargue código ' + $scope.ModeloCodigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarSitiosCargueDescargue';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarSitiosCargueDescargue';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            if (DatosRequeridos()) {
                showModal('modalConfirmacionGuardar');
            }
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');

            $scope.ModeloLatitud = parseFloat($scope.ModeloLatitud)
            $scope.ModeloLongitud = parseFloat($scope.ModeloLongitud)

            $scope.DatosGuardar = {

                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.ModeloCodigo,
                Nombre: $scope.ModeloNombre,
                CodigoTipoSitio: $scope.ModeloTipoSitio.Codigo,
                Pais: $scope.ModeloPais,
                Ciudad: $scope.ModeloCiudad,
                Zona: $scope.ModeloZona,
                Direccion: $scope.ModeloDireccion,
                CodigoPostal: $scope.ModeloCodigoPostal,
                Telefono: $scope.ModeloTelefono,
                Contacto: $scope.ModeloContacto,
                ValorCargue: MascaraNumero($scope.ModeloCargue),
                ValorDescargue: MascaraNumero($scope.ModeloDescargue),
                Estado: $scope.ModalEstado.Codigo,
                Longitud: $scope.ModeloLongitud,
                Latitud: $scope.ModeloLatitud,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },

            };

            SitiosCargueDescargueFactory.Guardar($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            if ($scope.ModeloCodigo === 0) {
                                ShowSuccess('Se guardó el sitio de ' + $scope.ModeloTipoSitio.Nombre + ' "' + $scope.ModeloNombre + '"');
                                location.href = '#!ConsultarSitiosCargueDescargue/' + response.data.Datos;
                            } else {
                                ShowSuccess('Se modificó el sitio de ' + $scope.ModeloTipoSitio.Nombre + ' "' + $scope.ModeloNombre + '"');
                                location.href = '#!ConsultarSitiosCargueDescargue/' + $scope.ModeloCodigo;
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }
                    else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.ModeloNombre === undefined || $scope.ModeloNombre === '' || $scope.ModeloNombre === null) {
                $scope.MensajesError.push('Debe ingresar el nombre del sitio');
                continuar = false;
            }
            if ($scope.ModeloTipoSitio === undefined || $scope.ModeloTipoSitio === '' || $scope.ModeloTipoSitio === null || $scope.ModeloTipoSitio.Codigo === 0) {
                $scope.MensajesError.push('Debe ingresar el tipo de sitio');
                continuar = false;
            }
            if ($scope.ModeloPais === undefined || $scope.ModeloPais === '' || $scope.ModeloPais === null || $scope.ModeloPais.Codigo === 0) {
                $scope.MensajesError.push('Debe ingresar el país');
                continuar = false;
            }
            if ($scope.ModeloCiudad === undefined || $scope.ModeloCiudad === '' || $scope.ModeloCiudad === null || $scope.ModeloCiudad.Codigo === 0) {
                $scope.MensajesError.push('Debe ingresar la ciudad');
                continuar = false;
            }
            if ($scope.ModeloDireccion === undefined || $scope.ModeloDireccion === '' || $scope.ModeloDireccion === null) {
                $scope.MensajesError.push('Debe ingresar la direccíon');
                continuar = false;
            }
            if ($scope.ModeloTelefono === undefined || $scope.ModeloTelefono === '' || $scope.ModeloTelefono === null) {
                $scope.MensajesError.push('Debe ingresar el teléfono');
                continuar = false;
            }

            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarSitiosCargueDescargue/' + $scope.ModeloCodigo;
        };

        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskDireccion = function (option) {
            try { $scope.ModeloDireccion = MascaraDireccion($scope.ModeloDireccion) } catch (e) { }
        };
        $scope.MaskTelefono = function (option) {
            try { $scope.ModeloTelefono = MascaraTelefono($scope.ModeloTelefono) } catch (e) { }
        };
        $scope.MasckDecimal = function (option) {
            return MascaraDecimales(option)
        }




    }]);