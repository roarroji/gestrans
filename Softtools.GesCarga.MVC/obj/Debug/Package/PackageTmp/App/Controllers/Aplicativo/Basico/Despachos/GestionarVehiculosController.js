﻿SofttoolsApp.controller("GestionarVehiculosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'FotosVehiculosFactory', 'VehiculosFactory', 'TercerosFactory', 'ValorCatalogosFactory', 'ColorVehiculosFactory', 'MarcaVehiculosFactory', 'LineaVehiculosFactory', 'SemirremolquesFactory', 'GestionDocumentosFactory', 'DocumentosFactory', 'blockUIConfig',
    function ($scope, $routeParams, $timeout, $linq, blockUI, FotosVehiculosFactory, VehiculosFactory, TercerosFactory, ValorCatalogosFactory, ColorVehiculosFactory, MarcaVehiculosFactory, LineaVehiculosFactory, SemirremolquesFactory, GestionDocumentosFactory, DocumentosFactory, blockUIConfig) {

        $scope.Titulo = 'GESTIONAR VEHICULOS';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Despachos' }, { Nombre: 'Vehiculos' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_VEHICULOS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }


        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        $scope.ListadoCiudadesDestino = []
        $scope.ListadoCiudadesOrigen = []
        $scope.ListadoEstados = [];
        $scope.DeshabilitarEstado = false;
        $scope.ListadoTiposCombustible = '';

        $scope.DesabilitarFechaUltimoCargue = false;

        $scope.MensajesError = [];

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: '',
            Nombre: '',
            Estado: 0
        }

        $scope.DeshabilitarProveedor = false;
        $scope.Modelo.ProveedorGPS = ''
        $scope.DeshabilitarUsuario = false;
        $scope.Modelo.UsuarioGPS = ''
        $scope.DeshabilitarClave = false;
        $scope.Modelo.ClaveGPS = ''
        $scope.DeshabilitarTelefono = false;
        $scope.Modelo.TelefonoGPS = ''
        $scope.list = {
            Fecha: new Date(),
            Fotos: []
        };

        $scope.ListadoNovedades = [];
        $scope.ListadoCompletoNovedades = [];
        $scope.ListaNovedadesVehiculo = [];
        $scope.ModalNovedades = {};
        $scope.NumeroPaginaNovedades = 1;
        $scope.CantidadRegistrosPorPaginaNovedades = 15;
        $scope.ListadoFiltradoNovedadesVehiculo = [];
        $scope.totalPaginasNovedades = 0;


        var ResponseNovedadesVehiculo = ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_NOVEDADES_VEHICULOS }, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
        if (ResponseNovedadesVehiculo != undefined) {
            $scope.ListadoCompletoNovedades = ResponseNovedadesVehiculo;
        }

        // Limpia la lista temporal de las fotos por usuario
        FotosVehiculosFactory.LimpiarTemporalUsuario({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                } else {
                }
            }, function (response) {
                ShowErr;
            });

        $scope.GPS = {};
        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
        }
        else {
            $scope.Modelo.Codigo = 0;
        }
        $scope.CargarDatosFunciones = function () {
            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
            var ResponseListadoTiposCombustible = ValorCatalogosFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_COMBUSTIBLE },
                Sync: true
            }).Datos;
            if (ResponseListadoTiposCombustible != undefined) {
                $scope.ListadoTiposCombustible = ResponseListadoTiposCombustible
                $scope.Modelo.TipoCombustible = ResponseListadoTiposCombustible[0]
            }

            /*Cargar el combo de tipo dueño*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DUEÑO_VEHICULO }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoDueno = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoDueno = response.data.Datos;
                            try {
                                $scope.Modelo.TipoDueno = $linq.Enumerable().From($scope.ListadoTipoDueno).First('$.Codigo ==' + $scope.Modelo.TipoDueno.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoDueno = $scope.ListadoTipoDueno[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoDueno = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo vehiculo*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoVehiculo = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoVehiculo = response.data.Datos;
                            try {
                                $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo ==' + $scope.Modelo.TipoVehiculo.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoVehiculo = $scope.ListadoTipoVehiculo[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoVehiculo = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo carroceria*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CARROCERIA }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoCarroceria = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoCarroceria = response.data.Datos;
                            try {
                                $scope.Modelo.TipoCarroceria = $linq.Enumerable().From($scope.ListadoTipoCarroceria).First('$.Codigo ==' + $scope.Modelo.TipoCarroceria.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoCarroceria = $scope.ListadoTipoCarroceria[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoCarroceria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de causa inactividad*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CAUSA_INACTIVIDAD_VEHICULOS }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCausa = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCausa = response.data.Datos;
                            try {
                                $scope.Modelo.CausaInactividad = $linq.Enumerable().From($scope.ListadoCausa).First('$.Codigo ==' + $scope.Modelo.CausaInactividad.Codigo);
                            } catch (e) {
                                $scope.Modelo.CausaInactividad = $scope.ListadoCausa[0]
                            }
                            $scope.AsignarJustificacionBloqueo($scope.Modelo.Estado.Codigo)
                        }
                        else {
                            $scope.ListadoCausa = []
                        }
                    }
                }, function (response) {
                });
            $scope.ObtuvoCausa = true
            /*Cargar el combo de estado*/
            $scope.AsignarJustificacionBloqueo = function (cod) {
                if (cod == 0) {//Bloqueado
                    $('#JustificacionBloqueo').show()
                    $('#CausaInactividad').show()

                    if ($scope.Modelo.Codigo > 0 && $scope.ObtuvoCausa) {
                        $scope.ObtuvoCausa = false
                    } else {
                        $scope.Modelo.CausaInactividad = $scope.ListadoCausa[0]
                    }
                } else {
                    $('#JustificacionBloqueo').hide()
                    $('#CausaInactividad').hide()
                    $scope.Modelo.CausaInactividad = $scope.ListadoCausa[0]
                    $scope.Modelo.JustificacionBloqueo = ''
                }

            }
            $scope.ListadoEstados = [
                { Codigo: 1, Nombre: "ACTIVO" },
                { Codigo: 0, Nombre: "INACTIVO" }
            ]
            /*Cargar Autocomplete de marca*/
            MarcaVehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoMarcas = []
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoMarcas = response.data.Datos;
                            try {
                                if ($scope.Modelo.Marca.Codigo > 0) {
                                    $scope.Modelo.Marca = $linq.Enumerable().From($scope.ListadoMarcas).First('$.Codigo ==' + $scope.Modelo.Marca.Codigo);
                                    $scope.AsignarLineaVehiculo()
                                }
                            } catch (e) {
                                $scope.Modelo.Marca = ''
                            }
                        }
                        else {
                            $scope.ListadoMarcas = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            $("#linea").prop('disabled', true);
            $scope.AsignarLineaVehiculo = function () {
                try {
                    if ($scope.Modelo.Marca.Codigo > 0) {
                        $("#linea").prop('disabled', false);
                        FiltroMarca = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Marca: $scope.Modelo.Marca, Estado: 1 }
                        LineaVehiculosFactory.Consultar(FiltroMarca).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    $scope.ListadoLineas = []
                                    if (response.data.Datos.length > 0) {
                                        $scope.ListadoLineas = response.data.Datos;
                                        try {
                                            if ($scope.Modelo.Linea.Codigo > 0) {
                                                $scope.Modelo.Linea = $linq.Enumerable().From($scope.ListadoLineas).First('$.Codigo ==' + $scope.Modelo.Linea.Codigo);
                                            }
                                        } catch (e) {
                                            $scope.Modelo.Linea = ''
                                        }
                                    }
                                    else {
                                        $scope.ListadoLineas = [];
                                        ShowError('La marca seleccionada no tiene asociadas lineas de vehículos')
                                        $("#linea").prop('disabled', true);
                                        $scope.ListadoLineas = []
                                    }
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                                $("#linea").prop('disabled', true);
                                $scope.ListadoLineas = []
                            });
                    } else {
                        $("#linea").prop('disabled', true);
                        $scope.ListadoLineas = []
                    }
                } catch (e) {
                    $("#linea").prop('disabled', true);
                    $scope.ListadoLineas = []
                }

            }
            /*Cargar Autocomplete de semirremolques*/
            SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoSemirremolques = []
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoSemirremolques = response.data.Datos;
                            try {
                                if ($scope.Modelo.Semirremolque.Codigo > 0) {
                                    $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListadoSemirremolques).First('$.Codigo ==' + $scope.Modelo.Semirremolque.Codigo);
                                }
                            } catch (e) {
                                $scope.Modelo.Semirremolque = ''
                            }
                        }
                        else {
                            $scope.ListadoSemirremolques = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            //Elimina todos los archivos temporales asociados a este usuario
            DocumentosFactory.EliminarDocumento({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo } }).
                then(function (response) {
                }, function (response) {
                    ShowError(response.statusText);
                });
            //Carga los documentos
            GestionDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, EntidadDocumento: { Codigo: 1 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDocumentos = []
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                var item = response.data.Datos[i]
                                if (item.Habilitado == 1) {
                                    if (item.TipoArchivoDocumento.Codigo == 4301) {
                                        $scope.ItemFoto = item
                                    }
                                    else {
                                        $scope.ListadoDocumentos.push(item)
                                    }
                                }
                            }
                            try {
                                var esImagen = false
                                if ($scope.Modelo.Documentos.length > 0) {
                                    for (var j = 0; j < $scope.ListadoDocumentos.length; j++) {
                                        $scope.ListadoDocumentos[j]
                                        for (var k = 0; k < $scope.Modelo.Documentos.length; k++) {
                                            var doc2 = $scope.Modelo.Documentos[k]
                                            if ($scope.ListadoDocumentos[j].Codigo == doc2.Codigo) {
                                                $scope.ListadoDocumentos[j].Referencia = doc2.Referencia
                                                if (doc2.Codigo == 102) {
                                                    $scope.ListadoDocumentos[j].Emisor = $scope.CargarTercero($scope.Modelo.AseguradoraSOAT.Codigo)
                                                    //$scope.ListadoDocumentos[j].Emisor = doc2.Emisor
                                                } else {
                                                    $scope.ListadoDocumentos[j].Emisor = doc2.Emisor
                                                }
                                                try {
                                                    if (new Date(doc2.FechaEmision) < MIN_DATE) {
                                                        $scope.ListadoDocumentos[j].FechaEmision = ''
                                                    } else {
                                                        $scope.ListadoDocumentos[j].FechaEmision = new Date(doc2.FechaEmision)
                                                    }

                                                } catch (e) {
                                                    $scope.ListadoDocumentos[j].FechaEmision = ''
                                                }
                                                try {
                                                    if (new Date(doc2.FechaVence) < MIN_DATE) {
                                                        $scope.ListadoDocumentos[j].FechaVence = ''
                                                    } else {
                                                        $scope.ListadoDocumentos[j].FechaVence = new Date(doc2.FechaVence)
                                                    }
                                                } catch (e) {
                                                    $scope.ListadoDocumentos[j].FechaVence = ''
                                                }
                                                $scope.ListadoDocumentos[j].AplicaInactivacion = doc2.AplicaInactivacion == 1 ? true : false;
                                                $scope.ListadoDocumentos[j].ValorDocumento = doc2.ValorDocumento
                                            }
                                        }
                                    }
                                    $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListadoSemirremolques).First('$.Codigo ==' + $scope.Modelo.Semirremolque.Codigo);
                                }
                            } catch (e) {
                            }
                            $scope.ListadoDocumentos.forEach(item => {
                                if ((item.Documento.Codigo == 3 || item.Documento.Codigo == 5 || item.Documento.Codigo == 6) && $scope.Sesion.UsuarioAutenticado.ObligatoriedadPolizasYTecnicoMecanica) {
                                    item.AplicaInactivacion = true
                                }
                            })
                        }
                        else {
                            $scope.Documentos = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            /*Cargar el combo capacidad vehículos*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CAPACIDAD_VEHICULO }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCapacidadVehiculo = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCapacidadVehiculo = response.data.Datos;
                            try {
                                $scope.Modelo.CapacidadVehiculo = $linq.Enumerable().From($scope.ListadoCapacidadVehiculo).First('$.Codigo ==' + $scope.Modelo.CapacidadVehiculo.Codigo);
                            } catch (e) {
                                $scope.Modelo.CapacidadVehiculo = $scope.ListadoCapacidadVehiculo[0]
                            }
                        }
                        else {
                            $scope.ListadoCapacidadVehiculo = []
                        }
                    }
                }, function (response) {
                });
            try {
                $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Estado.Codigo);
            } catch (e) {
                $scope.Modelo.Estado = $scope.ListadoEstados[0]
            }
        }

        $scope.CargarVehiculoEstudio = function () {
            BloqueoPantalla.start('Cargando vehículo ...');
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroAutorizacion: $scope.NumeroEstudioSeguridad
            };
            if ($scope.DatosRequeridosEstudio()) {
                VehiculosFactory.ObtenerVehiculoEstudioSeguridad(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado === 0) {
                                if ($scope.Modelo.Placa == response.data.Datos.Placa) {

                                    $scope.Modelo.Modelo = response.data.Datos.Modelo;
                                    $scope.Modelo.ModeloRepotenciado = response.data.Datos.ModeloRepotenciado;

                                    $scope.Modelo.TelefonoGPS = response.data.Datos.TelefonoEmpresaGPS;
                                    $scope.Modelo.UsuarioGPS = response.data.Datos.UsuarioGPS;
                                    $scope.Modelo.ClaveGPS = response.data.Datos.ClaveGPS;

                                    try {
                                        try {
                                            for (var k = 0; k < $scope.ListadoDocumentos.length; k++) {
                                                var item1 = $scope.ListadoDocumentos[k]
                                                for (var l = 0; l < response.data.Datos.ListadoDocumentos.length; l++) {
                                                    var item2 = response.data.Datos.ListadoDocumentos[l]
                                                    if ((item2.EOESCodigo == 1 || item2.EOESCodigo == 8 || item2.EOESCodigo == 15) && item1.Codigo == item2.CodigoConfiguracion) {
                                                        item1.Referencia = item2.Referencia
                                                        item1.Emisor = item2.Emisor
                                                        item1.FechaEmision = item2.FechaEmision
                                                        item1.FechaVence = item2.FechaVence
                                                    }
                                                }
                                            }
                                        } catch (e) {

                                        }
                                    } catch (e) {

                                    }
                                    for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                                        //soat
                                        if ($scope.ListadoDocumentos[i].Documento.Codigo == 2) {
                                            $scope.ListadoDocumentos[i].Referencia = response.data.Datos.NumeroSeguro;
                                            //$scope.ListadoDocumentos[i].Emisor = response.data.Datos.Aseguradora.NombreCompleto;
                                            $scope.ListadoDocumentos[i].FechaEmision = new Date(response.data.Datos.FechaEmisionSeguro);
                                            $scope.ListadoDocumentos[i].FechaVence = new Date(response.data.Datos.FechaVenceSeguro);
                                        }
                                        //tecnomecanica
                                        if ($scope.ListadoDocumentos[i].Documento.Codigo == 3) {
                                            $scope.ListadoDocumentos[i].Referencia = response.data.Datos.NumeroTecnomecanica;
                                            $scope.ListadoDocumentos[i].FechaEmision = new Date(response.data.Datos.FechaEmisionTecnomecanica);
                                            $scope.ListadoDocumentos[i].FechaVence = new Date(response.data.Datos.FechaVenceTecnomecanica);
                                        }
                                    }

                                    var CodigoProp = response.data.Datos.CodigoPropietario;
                                    var CodigoTene = response.data.Datos.CodigoTenedor;
                                    var CodigoCond = response.data.Datos.CodigoConductor;

                                    if (CodigoProp > 0 && CodigoProp !== undefined) {
                                        $scope.Modelo.Propietario = $scope.CargarTercero(CodigoProp);
                                    }
                                    if (CodigoTene > 0 && CodigoTene !== undefined) {
                                        $scope.Modelo.Tenedor = $scope.CargarTercero(CodigoTene);
                                    }
                                    if (CodigoCond > 0 && CodigoCond !== undefined) {
                                        $scope.Modelo.Conductor = $scope.CargarTercero(CodigoCond);
                                    }

                                    var CodigoColo = response.data.Datos.ColorVehiculo
                                    var CodigoTipo = response.data.Datos.TipoVehiculo
                                    var CodigoMarc = response.data.Datos.MarcaVehiculo

                                    if (CodigoColo > 0 && CodigoColo != undefined) {
                                        $scope.Modelo.Color = $scope.CargarColor(CodigoColo);
                                    }
                                    if (CodigoTipo > 0 && CodigoTipo != undefined) {
                                        $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo ==' + CodigoTipo);
                                    }
                                    if (CodigoMarc > 0 && CodigoMarc != undefined) {
                                        $scope.Modelo.Marca = $linq.Enumerable().From($scope.ListadoMarcas).First('$.Codigo ==' + CodigoMarc);
                                    }

                                    ////Afiliadora;
                                    //var Response = TercerosFactory.Consultar({
                                    //    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    //    CadenaPerfiles: PERFIL_EMPRESA_AFILIADORA,
                                    //    ValorAutocomplete: response.data.Datos.EmpresaAfiliadora,
                                    //    Sync: true
                                    //})
                                    //if (Response.Datos.length > CERO) {
                                    //    var CodigoAfiliador = Response.Datos[0].Codigo;
                                    //    $scope.Modelo.Afiliador = $scope.CargarTercero(CodigoAfiliador);
                                    //}

                                    ////Proveedor
                                    //var Response2 = TercerosFactory.Consultar({
                                    //    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    //    CadenaPerfiles: PERFIL_PROPIETARIO,
                                    //    ValorAutocomplete: response.data.Datos.NombreEmpresaGPS,
                                    //    Sync: true
                                    //});
                                    //if (Response2.Datos.length > CERO) {
                                    //    var CodigoProveedorGPS = Response2.Datos[0].Codigo;
                                    //    Modelo.ProveedorGPS = $scope.CargarTercero(CodigoProveedorGPS)
                                    //}
                                    if (response.data.Datos.PlacaSemirremolque !== '') {
                                        $scope.ListadoSemirremolques.forEach(function (item) {
                                            if (item.Placa === response.data.Datos.PlacaSemirremolque) {
                                                $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListadoSemirremolques).First('$.Placa == "' + response.data.Datos.PlacaSemirremolque + '"');
                                            }
                                        });
                                    }
                                    ShowSuccess('Información cargada correctamente, por favor diligenciar los campos obligatorios');
                                } else {
                                    ShowError('La placa ' + $scope.Modelo.Placa + ' no corresponde al No. autorización ingresado');
                                }
                            } else {
                                ShowError('El estudio de seguridad autorizado se encuentra anulado');
                            }
                            BloqueoPantalla.stop();
                        } else {
                            ShowError(response.data.MensajeOperacion); +
                                BloqueoPantalla.stop();
                        }
                    }, function (response) {
                        ShowError(response.data.MensajeOperacion);
                        BloqueoPantalla.stop();
                    });
            }
            BloqueoPantalla.stop();
        };

        $scope.DatosRequeridosEstudio = function () {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Modelo.Placa === undefined || $scope.Modelo.Placa === '' || $scope.Modelo.Placa === null) {
                $scope.MensajesError.push('Debe ingresar la placa autorizada en el estudio de seguiridad');
                continuar = false;
            }
            if ($scope.NumeroEstudioSeguridad === undefined || $scope.NumeroEstudioSeguridad === '' || $scope.NumeroEstudioSeguridad === null) {
                $scope.MensajesError.push('Debe ingresar el número de autorización del estudio de seguiridad');
                continuar = false;
            }
            return continuar
        };

        //Funciones Autocomplete
        $scope.ListadoPropietarios = [];
        $scope.AutocompletePropietario = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_PROPIETARIO,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoPropietarios = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoPropietarios)
                }
            }
            return $scope.ListadoPropietarios
        }
        $scope.ListadoTenedores = [];

        $scope.AutocompleteTenedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            CadenaPerfiles: PERFIL_TENEDOR,
                            ValorAutocomplete: value, Sync: true
                        })
                    $scope.ListadoTenedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTenedores)
                }
            }
            return $scope.ListadoTenedores
        }

        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                }
            }
            return $scope.ListadoConductores
        }

        $scope.ListadoAseguradoras = [];
        $scope.AutocompleteAseguradora = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_ASEGURADORA, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoAseguradoras = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoAseguradoras)
                }
            }
            return $scope.ListadoAseguradoras
        }
        $scope.ListadoAfiliadores = [];
        $scope.AutocompleteAfilidor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_EMPRESA_AFILIADORA,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoAfiliadores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoAfiliadores)
                }
            }
            return $scope.ListadoAfiliadores
        }
        $scope.ListadoProveedores = [];
        $scope.AutocompleteProveedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_PROVEEDOR_GPS, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoProveedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoProveedores)
                }
            }
            return $scope.ListadoProveedores
        }
        $scope.AutocompleteColores = function (value) {
            $scope.ListadoColores = [];
            if (value.length > 0) {
                /*Cargar Autocomplete de propietario*/
                blockUIConfig.autoBlock = false;
                var Response = ColorVehiculosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    ValorAutocomplete: value,
                    Sync: true
                })
                $scope.ListadoColores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoColores)
            }
            return $scope.ListadoColores
        }

        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR VEHÍCULO';
            $scope.Deshabilitar = true;
            ObtenerVehiculo();
        } else {
            $scope.CargarDatosFunciones()
        }
        function ObtenerVehiculo() {
            blockUI.start('Cargando vehículo Código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando vehículo Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            VehiculosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        var intCodigoTipoCombustible = response.data.Datos.TipoCombustible.Codigo
                        $scope.Modelo = response.data.Datos
                        $scope.DesactivarPlaca = true
                        if (ValidarFecha(response.data.Datos.FechaActualizacionKilometraje) == 0) {
                            $scope.Modelo.FechaActualizacionKilometraje = new Date(response.data.Datos.FechaActualizacionKilometraje)
                        }

                        if (ValidarFecha(response.data.Datos.FechaUltimoCargue) == 0) {
                            $scope.Modelo.FechaUltimoCargue = new Date(response.data.Datos.FechaUltimoCargue)
                            $scope.DesabilitarFechaUltimoCargue = true;
                        }
                        try {
                            if (response.data.Datos.ProveedorGPS.Codigo > 0) {
                                $scope.GPS.Tiene = true
                                $scope.Modelo.ProveedorGPS = $scope.CargarTercero(response.data.Datos.ProveedorGPS.Codigo)
                                $scope.CamposGPS()
                            }
                        } catch (e) {
                        }

                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        //CargarTerceros
                        $scope.Modelo.Propietario = $scope.CargarTercero($scope.Modelo.Propietario.Codigo)
                        $scope.Modelo.TenedorAnterior = $scope.CargarTercero($scope.Modelo.Tenedor.Codigo)
                        $scope.Modelo.Tenedor = $scope.CargarTercero($scope.Modelo.Tenedor.Codigo)
                        $scope.Modelo.Conductor = $scope.CargarTercero($scope.Modelo.Conductor.Codigo)
                        $scope.Modelo.Afiliador = $scope.CargarTercero($scope.Modelo.Afiliador.Codigo)
                        $scope.Modelo.Color = $scope.CargarColor($scope.Modelo.Color.Codigo)

                        $scope.ListadoEstados = [
                            { Codigo: 1, Nombre: "ACTIVO" },
                            { Codigo: 0, Nombre: "INACTIVO" },
                        ];

                        $scope.ListaNovedadesVehiculo = response.data.Datos.ListaNovedades;
                        $scope.ListaNovedadesVehiculo.forEach(item => {
                            item.Hora = new Date(item.Fecha).getHours();
                            item.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + item.Estado);
                        });
                        Paginar();
                        $scope.CargarDatosFunciones()


                        $scope.Modelo.TipoCombustible = $linq.Enumerable().From($scope.ListadoTiposCombustible).First('$.Codigo==' + intCodigoTipoCombustible)



                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + $scope.Modelo.Estado.Codigo);

                        blockUI.delay = 1000;
                    }
                    else {
                        ShowError('No se logro consultar el vehículo No.' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarVehiculos';
                    }
                }, function (response) {
                    ShowError('No se logro consultar el vehículo No.' + $scope.Modelo.Codigo + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!ConsultarVehiculos';
                });
            blockUI.stop();
            $timeout(function () {
                var consultarFotos = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoVehiculo: $scope.Modelo.Codigo
                };

                FotosVehiculosFactory.Consultar(consultarFotos).
                    then(function (response) {
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                var item = response.data.Datos[i];
                                item.EliminarTemporal = 0;
                                $scope.list.Fotos.push(item);
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }, 400);


        };
        $scope.VerificarExistencia = function () {
            if ($scope.Modelo.Placa.length == 6) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Placa: $scope.Modelo.Placa,
                };

                blockUI.delay = 1000;
                VehiculosFactory.Obtener(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                ShowError('La placa ingresada ya se encuentra registrada con otro vehículo')
                                $scope.Modelo.Placa = ''
                            }
                        }

                    }, function (response) {

                    });

                blockUI.stop();
            } else {
                ShowError('Por favor ingrese una placa valida')
            }

        }
        $scope.VerificarAsociacionSemirremolque = function () {
            //if ($scope.Modelo.Semirremolque !== undefined) {
            //    if ($scope.Modelo.Semirremolque.Placa !== undefined) {
            //        filtros = {
            //            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            //            Conductor: {},
            //            Tenedor: {},
            //            Semirremolque: $scope.Modelo.Semirremolque,
            //        };
            //        blockUI.delay = 1000;
            //        VehiculosFactory.Consultar(filtros).
            //            then(function (response) {
            //                if (response.data.ProcesoExitoso === true) {
            //                    if (response.data.Datos.length > 0) {
            //                        var cont = 0
            //                        for (var i = 0; i < response.data.Datos.length; i++) {
            //                            var item = response.data.Datos[i]
            //                            if (item.Placa != $scope.Modelo.Placa) {
            //                                cont++
            //                            }
            //                        }
            //                        if (cont > 0) {
            //                            ShowError('El semirremolque ya se encuentra asociado a otro vehículo')
            //                            $scope.Modelo.Semirremolque = ''
            //                        }
            //                    }
            //                }

            //            }, function (response) {

            //            });

            //        blockUI.stop();
            //    }

            //}
        }
        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarVehiculos';
        };
        //    $scope.MaskMayus = function () {
        //        try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
        //    };
        //    $scope.MaskNumero = function (option) {
        //        try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
        //    };
        //-------------------------------------------TABS----------------------------------------------
        $('#terceros').show(); $('#caracteristicas').hide(); $('#documentos').hide(); $('#gps').hide();
        $('#estado').hide(); $('#foto').hide();

        $scope.MostrarTerceros = function () {
            $('#terceros').show()
            $('#caracteristicas').hide();
            $('#documentos').hide()
            $('#gps').hide()
            $('#estado').hide()
            $('#foto').hide()
            $('#novedades').hide()
        }

        $scope.MostrarCaracteristicas = function () {
            $('#terceros').hide()
            $('#caracteristicas').show();
            $('#documentos').hide()
            $('#gps').hide()
            $('#estado').hide()
            $('#foto').hide()
            $('#novedades').hide()
        }

        $scope.MostrarDocumentos = function () {
            $('#terceros').hide()
            $('#caracteristicas').hide();
            $('#documentos').show()
            $('#gps').hide()
            $('#estado').hide()
            $('#foto').hide()
            $('#novedades').hide()
        }

        $scope.MostrarGPS = function () {
            $('#terceros').hide()
            $('#caracteristicas').hide();
            $('#documentos').hide()
            $('#gps').show()
            $('#estado').hide()
            $('#foto').hide()
            $('#novedades').hide()
        }

        $scope.MostrarEstado = function () {
            $('#terceros').hide()
            $('#caracteristicas').hide();
            $('#documentos').hide()
            $('#gps').hide()
            $('#estado').show()
            $('#foto').hide()
            $('#novedades').hide()
        }

        $scope.MostrarFoto = function () {
            $('#terceros').hide()
            $('#caracteristicas').hide();
            $('#documentos').hide()
            $('#gps').hide()
            $('#estado').hide()
            $('#foto').show()
            $('#novedades').hide()
        }

        $scope.MostrarNovedades = function () {
            $('#terceros').hide()
            $('#caracteristicas').hide();
            $('#documentos').hide()
            $('#gps').hide()
            $('#estado').hide()
            $('#foto').hide()
            $('#novedades').show()
        }

        $scope.CamposGPS = function () {
            if ($scope.GPS.Tiene == true) {
                $scope.DeshabilitarProveedor = false;
                $scope.DeshabilitarUsuario = false;
                $scope.DeshabilitarClave = false;
                $scope.DeshabilitarTelefono = false;
            }
            else {
                $scope.DeshabilitarProveedor = true;
                $scope.DeshabilitarUsuario = true;
                $scope.DeshabilitarClave = true;
                $scope.DeshabilitarTelefono = true;
            }
        };
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };
        function DatosRequeridosVehiculos() {

            $scope.MensajesError = [];
            window.scrollTo(top, top);
            var modelo = $scope.Modelo
            var continuar = true;
            //console.log('estado actual ', modelo.Conductor.Estado.Codigo)

            if (ValidarCampo(modelo.Placa) == 1) {
                $scope.MensajesError.push('Debe ingresar la placa');
                continuar = false;
            }
            if (ValidarCampo(modelo.Propietario) == 1) {
                $scope.MensajesError.push('Debe ingresar un propietario');
                continuar = false;
            } else if (ValidarCampo(modelo.Propietario, undefined, true) == 2) {
                $scope.MensajesError.push('Debe ingresar un propietario valido');
                continuar = false;
            }
            if (ValidarCampo(modelo.Tenedor) == 1) {
                $scope.MensajesError.push('Debe ingresar un tenedor');
                continuar = false;
            } else if (ValidarCampo(modelo.Tenedor, undefined, true) == 2) {
                $scope.MensajesError.push('Debe ingresar un tenedor valido');
                continuar = false;
            }
            if (ValidarCampo(modelo.Conductor) == 1) {
                $scope.MensajesError.push('Debe ingresar un conductor');
                continuar = false;

            } else if (modelo.Conductor.Estado.Codigo == 0) {
                $scope.MensajesError.push('Debe ingresar un conductor Activo');
                continuar = false;

            } else if (ValidarCampo(modelo.Conductor, undefined, true) == 2) {
                $scope.MensajesError.push('Debe ingresar un conductor valido');
                continuar = false;
            }
            if (ValidarCampo(modelo.Afiliador) == 1) {
                $scope.MensajesError.push('Debe ingresar un afiliador');
                continuar = false;
            } else if (ValidarCampo(modelo.Afiliador, undefined, true) == 2) {
                $scope.MensajesError.push('Debe ingresar un afiliador valido');
                continuar = false;
            }
            if (modelo.TipoDueno.Codigo == 2100)  /*No aplica*/ {
                $scope.MensajesError.push('Debe seleccionar el tipo de dueño');
                continuar = false;
            }
            if (modelo.TipoVehiculo.Codigo == 2200)  /*No aplica*/ {
                $scope.MensajesError.push('Debe seleccionar el tipo de vehículo');
                continuar = false;
            }
            if (ValidarCampo(modelo.TarjetaPropiedad) == 1) {
                $scope.MensajesError.push('Debe ingresar la tarjeta de propiedad');
                continuar = false;
            }
            if (modelo.TipoCarroceria.Codigo == 2300)  /*No aplica*/ {
                $scope.MensajesError.push('Debe seleccionar el tipo de carrocería');
                continuar = false;
            }
            if (ValidarCampo(modelo.NumeroMotor) == 1) {
                $scope.MensajesError.push('Debe ingresar el número de motor');
                continuar = false;
            }
            if (ValidarCampo(modelo.Modelo) == 1) {
                $scope.MensajesError.push('Debe ingresar el modelo');
                continuar = false;
            }
            if (ValidarCampo(modelo.Chasis) == 1) {
                $scope.MensajesError.push('Debe ingresar el número de serie/chasis');
                continuar = false;
            }
            if (ValidarCampo(modelo.Color) == 1) {
                $scope.MensajesError.push('Debe ingresar un color');
                continuar = false;
            } else if (ValidarCampo(modelo.Color, undefined, true) == 2) {
                $scope.MensajesError.push('Debe ingresar un color valido');
                continuar = false;
            }
            if (ValidarCampo(modelo.Marca) == 1) {
                $scope.MensajesError.push('Debe ingresar una marca');
                continuar = false;
            } else if (ValidarCampo(modelo.Marca, undefined, true) == 2) {
                $scope.MensajesError.push('Debe ingresar una marca valida');
                continuar = false;
            }
            if (ValidarCampo(modelo.Linea) == 1) {
                $scope.MensajesError.push('Debe ingresar una línea');
                continuar = false;
            } else if (ValidarCampo(modelo.Linea, undefined, true) == 2) {
                $scope.MensajesError.push('Debe ingresar una línea valida');
                continuar = false;
            }
            if (ValidarCampo(modelo.PesoBruto) == 1) {
                $scope.MensajesError.push('Debe ingresar el peso bruto');
                continuar = false;
            }

            if (ValidarCampo(modelo.Capacidad) == 1) {
                $scope.MensajesError.push('Debe ingresar la capacidad');
                continuar = false;
            }
            if (ValidarCampo(modelo.NumeroEjes) == 1) {
                $scope.MensajesError.push('Debe ingresar el número de ejes');
                continuar = false;
            }
            if (ValidarCampo(modelo.Cilindraje) == 1) {
                $scope.MensajesError.push('Debe ingresar el clilindraje');
                continuar = false;
            }
            if ($scope.GPS.Tiene == true) {
                if (ValidarCampo(modelo.ProveedorGPS, undefined, true) == 1) {
                    $scope.MensajesError.push('Debe ingresar un proveedor de GPS');
                    continuar = false;
                }
                if (ValidarCampo(modelo.UsuarioGPS, undefined, true) == 1) {
                    $scope.MensajesError.push('Debe ingresar un usuario Gps');
                    continuar = false;
                }
                if (ValidarCampo(modelo.ClaveGPS, undefined, true) == 1) {
                    $scope.MensajesError.push('Debe ingresar la Clave GPS');
                    continuar = false;
                }

            }
            if (modelo.Estado.Codigo == 0)  /*Bloqueado*/ {
                if (ValidarCampo(modelo.JustificacionBloqueo) == 1) {
                    $scope.MensajesError.push('Debe ingresar justificación del bloqueo');
                    continuar = false;
                }
                if (modelo.CausaInactividad.Codigo == 2400)  /*No aplica*/ {
                    $scope.MensajesError.push('Debe seleccionar causa de inactividad del vehiculo');
                    continuar = false;
                }
            }

            $scope.Modelo.Documentos = []
            //Documentos
            for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                var Cont = 0
                var documento = $scope.ListadoDocumentos[i]
                if (documento.TipoArchivoDocumento.Codigo !== 4301) {

                    if (ValidarCampo(documento.Referencia) == 1) {
                        if (documento.AplicaReferencia == CAMPO_DOCUMENTO_OBLIGATORIO && $scope.Modelo.Estado.Codigo == 1) {
                            $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        }
                    }
                    if (ValidarCampo(documento.Emisor) == 1) {
                        if (documento.AplicaEmisor == CAMPO_DOCUMENTO_OBLIGATORIO && $scope.Modelo.Estado.Codigo == 1) {
                            $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        }
                    }
                    if (ValidarFecha(documento.FechaEmision) == 1) {
                        if (documento.AplicaFechaEmision == CAMPO_DOCUMENTO_OBLIGATORIO && $scope.Modelo.Estado.Codigo == 1) {
                            $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        } else {
                            documento.FechaEmision = ''
                        }
                    } else {
                        var f = new Date();
                        if (documento.FechaEmision > f) {
                            $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        }
                    }
                    if (ValidarFecha(documento.FechaVence) == 1) {
                        if (documento.AplicaInactivacion) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        }
                        else if (documento.AplicaFechaVencimiento == CAMPO_DOCUMENTO_OBLIGATORIO && $scope.Modelo.Estado.Codigo == 1) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        } else {
                            documento.FechaVence = ''
                        }
                    } else {
                        if ($scope.Modelo.Estado.Codigo == 1) {
                            if (documento.AplicaInactivacion) {
                                var f = new Date();
                                if (documento.FechaVence < f) {
                                    $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                                    continuar = false;
                                    DocumentoValido = false;
                                }
                            }
                            else if (documento.AplicaFechaVencimiento != CAMPO_DOCUMENTO_NO_APLICA) {
                                var f = new Date();
                                if (documento.FechaVence < f) {
                                    $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                                    continuar = false;
                                    DocumentoValido = false;
                                }
                            } else {
                                documento.FechaVence = ''
                            }
                        }
                    }
                    if (documento.Codigo == 102) {
                        if (documento.Emisor == undefined) {
                            $scope.MensajesError.push('Debe ingresar la información del SOAT');
                            continuar = false;

                        } else {

                            $scope.Modelo.Documentos.push(
                                {
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Vehiculo: true,
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    Configuracion: { Codigo: documento.Codigo },
                                    Referencia: documento.Referencia,
                                    Emisor: documento.Emisor.NombreCompleto,
                                    FechaEmision: documento.FechaEmision,
                                    FechaVence: documento.FechaVence,
                                    EliminaDocumento: documento.ValorDocumento,
                                    AplicaInactivacion: documento.AplicaInactivacion == true ? 1 : 0
                                }
                            )
                            try {
                                $scope.Modelo.AseguradoraSOAT = { Codigo: documento.Emisor.Codigo }
                            } catch (e) {
                            }
                            $scope.Modelo.ReferenciaSOAT = documento.Referencia
                            $scope.Modelo.FechaVencimientoSOAT = documento.FechaVence
                        }
                    } else {
                        $scope.Modelo.Documentos.push(
                            {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Vehiculo: true,
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                Configuracion: { Codigo: documento.Codigo },
                                Referencia: documento.Referencia,
                                Emisor: documento.Emisor,
                                FechaEmision: documento.FechaEmision,
                                FechaVence: documento.FechaVence,
                                EliminaDocumento: documento.ValorDocumento,
                                AplicaInactivacion: documento.AplicaInactivacion == true ? 1 : 0
                            }
                        )
                    }

                } else if (documento.AplicaInactivacion == true || documento.AplicaInactivacion == 1) {
                    if (ValidarCampo(documento.Referencia) == 1) {
                        if (documento.AplicaReferencia == CAMPO_DOCUMENTO_OBLIGATORIO && $scope.Modelo.Estado.Codigo == 1) {
                            $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        }
                    }
                    if (ValidarCampo(documento.Emisor) == 1) {
                        if (documento.AplicaEmisor == CAMPO_DOCUMENTO_OBLIGATORIO && $scope.Modelo.Estado.Codigo == 1) {
                            $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        }
                    }
                    if (ValidarFecha(documento.FechaEmision) == 1) {
                        if (documento.AplicaFechaEmision == CAMPO_DOCUMENTO_OBLIGATORIO && $scope.Modelo.Estado.Codigo == 1) {
                            $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        } else {
                            documento.FechaEmision = ''
                        }
                    } else {
                        var f = new Date();
                        if (documento.FechaEmision > f) {
                            $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                            continuar = false;
                            DocumentoValido = false;
                        }
                    }
                    if (ValidarFecha(documento.FechaVence) == 1) {

                        $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Documento.Nombre + ')');
                        continuar = false;
                        DocumentoValido = false;

                    }
                    if (documento.Codigo == 102) {
                        if (documento.Emisor == undefined) {
                            $scope.MensajesError.push('Debe ingresar la información del SOAT');
                            continuar = false;

                        } else {

                            $scope.Modelo.Documentos.push(
                                {
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Vehiculo: true,
                                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                    Configuracion: { Codigo: documento.Codigo },
                                    Referencia: documento.Referencia,
                                    Emisor: documento.Emisor.NombreCompleto,
                                    FechaEmision: documento.FechaEmision,
                                    FechaVence: documento.FechaVence,
                                    EliminaDocumento: documento.ValorDocumento,
                                    AplicaInactivacion: documento.AplicaInactivacion == true ? 1 : 0
                                }
                            )
                            try {
                                $scope.Modelo.AseguradoraSOAT = { Codigo: documento.Emisor.Codigo }
                            } catch (e) {
                            }
                            $scope.Modelo.ReferenciaSOAT = documento.Referencia
                            $scope.Modelo.FechaVencimientoSOAT = documento.FechaVence
                        }
                    } else {
                        $scope.Modelo.Documentos.push(
                            {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Vehiculo: true,
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                Configuracion: { Codigo: documento.Codigo },
                                Referencia: documento.Referencia,
                                Emisor: documento.Emisor,
                                FechaEmision: documento.FechaEmision,
                                FechaVence: documento.FechaVence,
                                EliminaDocumento: documento.ValorDocumento,
                                AplicaInactivacion: documento.AplicaInactivacion == true ? 1 : 0
                            }
                        )
                    }
                }
            }
            $scope.Modelo.Documentos.push(
                {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Vehiculo: true,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Configuracion: { Codigo: $scope.ItemFoto.Codigo },
                    Referencia: $scope.ItemFoto.Referencia,
                    Emisor: $scope.ItemFoto.Emisor,
                    FechaEmision: $scope.ItemFoto.FechaEmision,
                    FechaVence: $scope.ItemFoto.FechaVence,
                    EliminaDocumento: $scope.ItemFoto.ValorDocumento,
                    AplicaInactivacion: documento.AplicaInactivacion == true ? 1 : 0
                }
            )
            return continuar


        }
        function ValidarCampo(objeto, minlength, Esobjeto) {
            var resultado = 0
            if (objeto == undefined || objeto == '' || objeto == null || objeto == 0 || objeto == '0') {
                resultado = 1
            } else {
                if (resultado == 0) {
                    if (minlength !== undefined) {
                        if (objeto.length < minlength) {
                            resultado = 3
                        }
                        else {
                            resultado = 0
                        }
                    }
                } if (resultado == 0) {
                    if (Esobjeto) {
                        if (objeto.Codigo == undefined || objeto.Codigo == '' || objeto.Codigo == null || objeto.Codigo == 0) {
                            resultado = 2
                        }
                    }
                }
            }
            return resultado
        }
        function ValidarFecha(objeto, MayorActual, MenorActual) {
            var resultado = 0
            var now = new Date()
            if (objeto == undefined || objeto == '' || objeto == null) {
                resultado = 1
            } else {
                if (resultado == 0) {
                    if (MayorActual) {
                        if (objeto > now) {
                            resultado = 2
                        }
                    }
                    else if (MenorActual) {
                        if (objeto < now) {
                            resultado = 3
                        }
                    }
                }
            }
            return resultado
        }
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridosVehiculos()) {

                $scope.Modelo.Propietario = { Codigo: $scope.Modelo.Propietario.Codigo, NombreCompleto: $scope.Modelo.Propietario.NombreCompleto }
                $scope.Modelo.Tenedor = { Codigo: $scope.Modelo.Tenedor.Codigo, NombreCompleto: $scope.Modelo.Tenedor.NombreCompleto }
                $scope.Modelo.Conductor = { Codigo: $scope.Modelo.Conductor.Codigo, NombreCompleto: $scope.Modelo.Conductor.NombreCompleto }
                $scope.Modelo.Afiliador = { Codigo: $scope.Modelo.Afiliador.Codigo, NombreCompleto: $scope.Modelo.Afiliador.NombreCompleto }
                try {
                    $scope.Modelo.ProveedorGPS = { Codigo: $scope.Modelo.ProveedorGPS.Codigo, NombreCompleto: $scope.Modelo.ProveedorGPS.NombreCompleto }
                } catch (e) {
                }
                $scope.Modelo.DetalleFotos = $scope.list.Fotos;
                var Entidad = $scope.Modelo;

                Entidad.Propietario = { Codigo: $scope.Modelo.Propietario.Codigo }
                Entidad.Tenedor = { Codigo: $scope.Modelo.Tenedor.Codigo }
                Entidad.Conductor = { Codigo: $scope.Modelo.Conductor.Codigo }
                Entidad.Afiliador = { Codigo: $scope.Modelo.Afiliador.Codigo }
                Entidad.ProveedorGPS = { Codigo: $scope.Modelo.ProveedorGPS.Codigo }
                Entidad.ListaNovedades = {};
                Entidad.TipoCombustible = { Codigo: $scope.Modelo.TipoCombustible == undefined ? 22901 : $scope.Modelo.TipoCombustible.Codigo };
                VehiculosFactory.Guardar(Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if (response.data.Datos == 1) {
                                    ShowError('La placa que intenta ingresar ya se encuentra registrada');
                                } else {
                                    ShowSuccess('Se guardó el vehículo de placas ' + $scope.Modelo.Placa);
                                    if ($scope.Sesion.UsuarioAutenticado.ManejoPorcentajesPropietariosVehiculo) {
                                        var Entidad = {
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            Codigo: response.data.Datos,
                                            Propietarios: [{
                                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                Vehiculo: { Codigo: response.data.Datos },
                                                Propietario: { Codigo: $scope.Modelo.Tenedor.Codigo },
                                                PorcentajeParticipacion: 100
                                            }],
                                            Sync: true

                                        }

                                        var Novedad = {
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            Codigo: response.data.Datos,
                                            Estado: { Codigo: 1 },
                                            Novedad: { Codigo: 22321 },
                                            JustificacionNovedad: 'Asignacion de  100% propietario',
                                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                            ,
                                            Sync: true

                                        }

                                        if ($scope.Modelo.TenedorAnterior != undefined) {
                                            if ($scope.Modelo.TenedorAnterior.Codigo != $scope.Modelo.Tenedor.Codigo) {
                                                VehiculosFactory.GuardarPropietarios(Entidad)
                                                VehiculosFactory.GuardarNovedad(Novedad)
                                            }

                                        } else {
                                            VehiculosFactory.GuardarPropietarios(Entidad)
                                            VehiculosFactory.GuardarNovedad(Novedad)
                                        }


                                    }
                                    location.href = '#!ConsultarVehiculos/' + response.data.Datos
                                }
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        //--------------------------------------------------------------------------------------------Funciones fotos-------------------------------------------------------------
        $scope.CargarImagen = function (element) {
            var continuar = true;
            $scope.TipoImagen = element
            blockUI.start();
            $scope.NombreDocumento = element.files[0].name;
            if ($scope.list.Fotos.length < 3) {
                if (element === undefined) {
                    ShowError("Solo se pueden cargar archivos en formato jpeg, png  o pdf", false);
                    continuar = false;
                }
                if (continuar === true) {
                    if (element.files[0].type !== 'image/jpeg' && element.files[0].type !== 'image/png' && element.files[0].type !== 'application/pdf') {
                        ShowError("Solo se pueden cargar archivos en formato jpeg, png  o pdf", false);
                        continuar = false;
                    }
                    if (element.files[0].type === 'application/pdf') {

                        if (element.files[0].size >= 4000000) { //4 MB
                            ShowError("El documento " + element.files[0].name + " supera los 4 MB, intente con otro documento", false);
                            continuar = false;
                        }
                    }
                }
                if (continuar === true) {
                    var reader = new FileReader();
                    reader.onload = $scope.AsignarImagenListado;
                    reader.readAsDataURL(element.files[0]);
                }
            }
            else {
                ShowError("Ha superado el maximo de imagenes permitidas", false);
            }
            blockUI.stop();
        };

        $scope.AsignarImagenListado = function (e) {
            $scope.$apply(function () {
                $scope.Convertirfoto = null;
                var string = '';
                var TipoImagen = '';
                if ($scope.TipoImagen.files[0].type === 'image/jpeg') {
                    var img = new Image();
                    img.src = e.target.result;
                    //Se detecta primero la orientacion de la imagen luego se redimenciona para bajar la resolucion a un tamaño estandar de 500x700 o 700x500 segun su orientacion
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img);
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '');
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img);
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/jpeg").replace(/data:image\/jpeg;base64,/g, '');
                        }
                        TipoImagen = 'JPEG';
                        $scope.TipoImagen = TipoImagen;
                        AsignarFotoListado();
                    }, 100);
                }
                else if ($scope.TipoImagen.files[0].type === 'image/png') {
                    var img = new Image();
                    img.src = e.target.result;
                    //la imagen es vertical
                    $timeout(function () {
                        if (img.height > img.width) {
                            RedimVertical(img);
                            $scope.Convertirfoto = document.getElementById('CanvasVertical').toDataURL("image/png").replace(/data:image\/png;base64,/g, '');
                        }
                        //la imagen es horizontal
                        else {
                            RedimHorizontal(img);
                            $scope.Convertirfoto = document.getElementById('CanvasHorizontal').toDataURL("image/png").replace(/data:image\/png;base64,/g, '');
                        }
                        TipoImagen = 'PNG';
                        $scope.TipoImagen = TipoImagen;
                        AsignarFotoListado();
                    }, 400)
                }
                else if ($scope.TipoImagen.files[0].type === 'application/pdf') {
                    $scope.Convertirfoto = e.target.result.replace(/data:application\/pdf;base64,/g, '');
                    TipoImagen = 'PDF';
                    $scope.TipoImagen = TipoImagen;
                    AsignarFotoListado();
                }
            });
        };

        function AsignarFotoListado() {
            var foto = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CodigoVehiculo: $scope.Modelo.Codigo,
                NombreDocumento: $scope.NombreDocumento,
                ExtencionDocumento: $scope.TipoImagen,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Documento: $scope.Convertirfoto
            };
            // Guardar Foto temporal
            FotosVehiculosFactory.InsertarTemporal(foto).
                then(function (response) {
                    if (response.data.Datos > 0) {
                        // Se guardó correctamente la foto
                        foto.Codigo = response.data.Datos;
                        foto.EliminarTemporal = 1;
                        $scope.list.Fotos.push(foto);
                    } else {
                        ShowError(response.data.MensajeError);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.EliminarFoto = function (index, Codigo, Temp) {
            var fotoEliminar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: Codigo,
                EliminarTemporal: Temp
            };
            fotoEliminar.Foto = null;
            FotosVehiculosFactory.EliminarFoto(fotoEliminar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.list.Fotos.splice(index, 1);
                    } else {
                        ShowError(response.data.MensajeError);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        $scope.CargarArchivo = function (element) {
            var continuar = true;
            if (element.files.length > 0) {
                $scope.PDF = element
                $scope.TipoDocumento = element
                blockUI.start();
                if (angular.element(this.item)[0] == undefined) {
                    $scope.Documento = angular.element(this.ItemFoto)[0];
                } else {
                    angular.element(this.item)[0].Temporal = true
                    $scope.Documento = angular.element(this.item)[0];
                }
                if (element === undefined) {
                    ShowError("Archivo invalido, por favor verifique el formato del archivo  /nFormatos permitidos:" + $scope.Documento.TipoArchivoDocumento.CampoAuxiliar2);
                    continuar = false;
                }
                if (continuar == true) {
                    if ($scope.Documento.TipoArchivoDocumento.Codigo !== 4300) {
                        var Formatos = $scope.Documento.TipoArchivoDocumento.CampoAuxiliar2.split(',')
                        var cont = 0
                        for (var i = 0; i < Formatos.length; i++) {
                            Formatos[i] = Formatos[i].replace(' ', '')
                            var Extencion = element.files[0].name.split('.')
                            //if (element.files[0].type == Formatos[i]) {
                            if ('.' + (Extencion[1].toUpperCase()) == Formatos[i].toUpperCase()) {
                                cont++
                            }
                        }
                        if (cont == 0) {
                            ShowError("Archivo invalido, por favor verifique el formato del archivo  Formatos permitidos:" + $scope.Documento.TipoArchivoDocumento.CampoAuxiliar2);
                            continuar = false;
                        }
                    }
                    else if (element.files[0].size >= $scope.Documento.Size) //8 MB
                    {
                        ShowError("El archivo " + element.files[0].name + " supera los " + $scope.Documento.NombreTamano + ", intente con otro archivo", false);
                        continuar = false;
                    }
                }
                if (continuar == true) {
                    if ($scope.Documento.Archivo != undefined && $scope.Documento.Archivo != '' && $scope.Documento.Archivo != null) {
                        $scope.Documento.ArchivoTemporal = element.files[0]
                        showModal('modalConfirmacionRemplazarDocumento');
                    } else {
                        var reader = new FileReader();
                        $scope.Documento.ArchivoCargado = element.files[0]
                        reader.onload = $scope.AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }

                blockUI.stop();
            }
        }
        $scope.RemplazarDocumento = function () {
            var reader = new FileReader();
            $scope.Documento.ArchivoCargado = $scope.Documento.ArchivoTemporal
            reader.onload = $scope.AsignarArchivo;
            reader.readAsDataURL($scope.Documento.ArchivoCargado);
            closeModal('modalConfirmacionRemplazarDocumento');
        }
        $scope.AsignarArchivo = function (e) {
            $scope.$apply(function () {
                //imagenes, tienen un proceso aparte ya que se deben redimencionar para que no ocupen mucho espacio en la BD
                if ($scope.Documento.ArchivoCargado.type.includes("image")) {
                    var img = new Image()
                    img.src = e.target.result
                    $timeout(function () {
                        $('#Foto').show()
                        $('#FotoCargada').hide()
                        if (img.height > img.width) {
                            RedimencionarImagen('CanvasVertical', img, 699, 499)
                            $scope.Documento.Archivo = document.getElementById('CanvasVertical').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')

                        }
                        //la imagen es horizontal
                        else {
                            RedimencionarImagen('CanvasHorizontal', img, 499, 699)
                            $scope.Documento.Archivo = document.getElementById('CanvasHorizontal').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')
                        }

                        $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type
                        var Extencion = $scope.Documento.ArchivoCargado.name.split('.')
                        $scope.Documento.NombreDocumento = Extencion[0]
                        $scope.Documento.ExtensionDocumento = Extencion[1]
                        if ($scope.Documento.TipoArchivoDocumento.Codigo == 4301) {
                            RedimencionarImagen('Foto', img, 200, 200)
                            $scope.ItemFoto.NombreDocumento = Extencion[0]
                            $scope.ItemFoto.ValorDocumento = 1
                            $scope.ItemFoto.temp = true
                        }
                        $scope.InsertarDocumento()
                    }, 100)
                }
                //Resto de archivos
                else {
                    $scope.Documento.Archivo = e.target.result.replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')
                    $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type
                    var Extencion = $scope.Documento.ArchivoCargado.name.split('.')
                    $scope.Documento.NombreDocumento = Extencion[0]
                    $scope.Documento.ExtensionDocumento = Extencion[1]
                    $scope.InsertarDocumento()
                }
            });
        }
        $scope.InsertarDocumento = function () {
            $timeout(function () {
                // Guardar Archivo temporal
                var Documento = {
                    CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                    Configuracion: { Codigo: $scope.Documento.Codigo },
                    Archivo: $scope.Documento.Archivo,
                    Nombre: $scope.Documento.NombreDocumento,
                    Extension: $scope.Documento.ExtensionDocumento,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Tipo: $scope.Documento.Tipo
                }
                DocumentosFactory.InsertarTemporal(Documento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            // Se guardó correctamente el pdf
                            if (response.data.Datos) {
                                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                                    var item = $scope.ListadoDocumentos[i]
                                    if (item.Codigo == $scope.Documento.Codigo) {
                                        item.ValorDocumento = 1
                                        item.temp = true
                                    }
                                }
                                ShowSuccess('Se cargó el documento adjunto "' + $scope.Documento.NombreDocumento + '"');
                                $scope.Documento = ''
                            }
                        } else {
                            ShowError(response.data.MensajeError);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }, 200)
        }
        $scope.ElimiarDocumento = function (EliminarDocumento) {
            $scope.EliminarDocumento = EliminarDocumento
            showModal('modalConfirmacionEliminarDocumento');
            document.getElementById('mensajearchivo').innerHTML = '¿Está seguro de eliminar el documento?'

        };
        $scope.BorrarDocumento = function () {
            closeModal('modalConfirmacionEliminarDocumento');
            if ($scope.EliminarDocumento.temp) {
                $scope.EliminarDocumento.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                DocumentosFactory.EliminarDocumento($scope.EliminarDocumento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ListadoDocumentos.forEach(function (item) {
                                if (item.Codigo == $scope.EliminarDocumento.Codigo) {
                                    item.ValorDocumento = 0
                                }
                            })
                            if ($scope.EliminarDocumento.TipoArchivoDocumento.Codigo == 4301) {
                                var img = new Image()
                                RedimencionarImagen('Foto', img, 200, 200)
                                $scope.ItemFoto.ValorDocumento = 0
                                $('#Foto').hide()
                                $scope.FotoCargada = ''
                                $('#FotoCargada').hide()
                            }
                        }
                        else {
                            MensajeError("Error", response.data.MensajeError, false);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                    if ($scope.EliminarDocumento.Codigo == $scope.ListadoDocumentos[i].Codigo) {
                        $scope.ListadoDocumentos[i].ValorDocumento = 0
                    }
                }
                if ($scope.EliminarDocumento.TipoArchivoDocumento.Codigo == 4301) {
                    var img = new Image()
                    RedimencionarImagen('Foto', img, 200, 200)
                    $scope.ItemFoto.ValorDocumento = 0
                    $('#Foto').hide()
                    $scope.FotoCargada = ''
                    $('#FotoCargada').hide()
                }

            }
        }

        function RedimVertical(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasVertical');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 499, 699);
            }
        }
        function RedimHorizontal(img) {
            //Se carga el contexto vans donde se redimencionara la imagen
            var ctx = cargaContextoCanvas('CanvasHorizontal');
            if (ctx) {
                // dentro del canvaz se dibija la imagen que se recive por parametro
                ctx.drawImage(img, 0, 0, 699, 499);
            }
        }
        //-----------------------------------------------------------------------------------------------------
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (item) {
            if (item.temp) {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Vehiculos&Codigo=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&CDGD_Codigo=' + item.Codigo + '&Temp=1');
            }
            else {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Vehiculos&Codigo=' + $scope.Modelo.Codigo + '&CDGD_Codigo=' + item.Codigo + '&Temp=0');
            }
        };

        //Novedades:

        $scope.PrimerPaginaNovedades = function () {
            $scope.NumeroPaginaNovedades = 1;
            Paginar();
        }
        $scope.AnteriorNovedades = function () {
            if ($scope.NumeroPaginaNovedades > 1) {
                $scope.NumeroPaginaNovedades--;
                Paginar();
            }

        }
        $scope.SiguienteNovedades = function () {
            if ($scope.NumeroPaginaNovedades < (Math.ceil($scope.ListaNovedadesVehiculo.length / $scope.CantidadRegistrosPorPaginaNovedades))) {
                $scope.NumeroPaginaNovedades++;
                Paginar();
            }
        }
        $scope.UltimaPaginaNovedades = function () {
            $scope.NumeroPaginaNovedades = Math.ceil($scope.ListaNovedadesVehiculo.length / $scope.CantidadRegistrosPorPaginaNovedades);
            Paginar();
        }



        function Paginar() {
            $scope.ListadoFiltradoNovedadesVehiculo = [];
            var ListaNovedadesOrdenado = $scope.ListaNovedadesVehiculo.sort((a, b) => { return (new Date(b.Fecha) - new Date(a.Fecha)) })
            $scope.totalPaginasNovedades = Math.ceil(ListaNovedadesOrdenado.length / $scope.CantidadRegistrosPorPaginaNovedades);
            var RegistroInicio = ($scope.NumeroPaginaNovedades * $scope.CantidadRegistrosPorPaginaNovedades) - $scope.CantidadRegistrosPorPaginaNovedades;
            var RegistroFin = ($scope.NumeroPaginaNovedades * $scope.CantidadRegistrosPorPaginaNovedades) - 1
            if (ListaNovedadesOrdenado.length > 0) {
                for (var i = 0; i < ListaNovedadesOrdenado.length; i++) {
                    if (i >= RegistroInicio && i <= RegistroFin) {
                        $scope.ListadoFiltradoNovedadesVehiculo.push(ListaNovedadesOrdenado[i])
                    }
                }
            }

            console.log($scope.ListadoFiltradoNovedadesVehiculo)

        }

        $scope.ValidarEstadoModalNovedades = function () {
            $scope.ListadoNovedades = [];
            $scope.ListadoCompletoNovedades.forEach(itemNovedad => {
                if ($scope.ModalNovedades.Estado.Codigo == ESTADO_ACTIVO) {
                    if (itemNovedad.CampoAuxiliar4 == ESTADO_ACTIVO) {
                        $scope.ListadoNovedades.push(itemNovedad);
                    }
                } else {
                    if (itemNovedad.CampoAuxiliar4 == ESTADO_INACTIVO) {
                        $scope.ListadoNovedades.push(itemNovedad);
                    }
                }
                $scope.ModalNovedades.Novedad = $scope.ListadoNovedades[0];
                $scope.AsignarJustificacion();
            });

        }

        $scope.AsignarJustificacion = function () {
            if ($scope.ModalNovedades.Novedad != undefined) {
                $scope.ModalNovedades.Justificacion = $scope.ModalNovedades.Novedad.Nombre
            }

        }

        $scope.AgregarNovedadVehiculo = function () {
            $scope.ListadoNovedades = [];
            $scope.ModalNovedades.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==1');
            $scope.ListadoCompletoNovedades.forEach(itemNovedad => {
                if ($scope.ModalNovedades.Estado.Codigo == ESTADO_ACTIVO) {
                    if (itemNovedad.CampoAuxiliar4 == ESTADO_ACTIVO) {
                        $scope.ListadoNovedades.push(itemNovedad);
                    }
                } else {
                    if (itemNovedad.CampoAuxiliar4 == ESTADO_INACTIVO) {
                        $scope.ListadoNovedades.push(itemNovedad);
                    }
                }

            });


            if ($scope.ListadoNovedades.length > 0) {
                $scope.ModalNovedades.Novedad = $scope.ListadoNovedades[0];
            }
            $scope.ModalNovedades.Justificacion = '';
            showModal('modalAgregarNovedad');

        }

        $scope.GuardarNovedad = function () {
            var Novedad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
                Estado: $scope.ModalNovedades.Estado,
                Novedad: { Codigo: $scope.ModalNovedades.Novedad.Codigo },
                JustificacionNovedad: $scope.ModalNovedades.Justificacion,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
            }

            console.log('guardadno', Novedad)
            VehiculosFactory.GuardarNovedad(Novedad).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        $scope.ListaNovedadesVehiculo.push({
                            Fecha: new Date(),
                            Hora: new Date().getHours(),
                            Responsable: { NombreCompleto: $scope.Sesion.UsuarioAutenticado.Empleado.Nombre },
                            Estado: Novedad.Estado,
                            Novedad: $scope.ModalNovedades.Novedad,
                            JustificacionNovedad: Novedad.JustificacionNovedad

                        });

                        $scope.ListaNovedadesVehiculo.forEach(item => {
                            item.Fecha = new Date(item.Fecha)
                        });

                        $scope.Modelo.JustificacionBloqueo = Novedad.JustificacionNovedad;
                        if (Novedad.Estado.Codigo == ESTADO_ACTIVO) {
                            $('#JustificacionBloqueo').hide();
                            $('#CausaInactividad').hide();
                            $scope.Modelo.CausaInactividad = '';
                        } else {
                            $('#JustificacionBloqueo').show();
                            $('#CausaInactividad').show();
                        }
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo==' + Novedad.Estado.Codigo);
                        if (Novedad.Estado.Codigo == ESTADO_INACTIVO) {
                            $scope.Modelo.CausaInactividad = $linq.Enumerable().From($scope.ListadoCausa).First('$.Codigo==' + $scope.ModalNovedades.Novedad.CampoAuxiliar2);
                        }
                        Paginar();

                        ShowSuccess('Se guardó la novedad con éxito');

                    } else {
                        ShowError(response.statusText)
                    }
                });

            closeModal('modalAgregarNovedad');
        }

        $scope.DesplegarExcel = function (OpcionPDf, OpcionEXCEL, OpcionListado) {

            // $scope.OpcionLista = OpcionListado;

            //Depende del listado seleccionado se enviará el nombre por parametro
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NombreReporte = NOMBRE_LISTADO_EXCEL_NOVEDADES_VEHICULO;




            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf === 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&VEHI_Codigo=' + $scope.Modelo.Codigo + '&OpcionExportarPdf=' + OpcionPDf);
            }
            if (OpcionEXCEL === 1) {
                window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + '&VEHI_Codigo=' + $scope.Modelo.Codigo + '&OpcionExportarExcel=' + OpcionEXCEL);
            }



        };

        //Fin Novedades

        if ($scope.Sesion.UsuarioAutenticado.ManejoEstadosNovedades) {
            $scope.DeshabilitarEstado = true;
        }


        $scope.BorrarFoto = function () {
            $scope.Modelo.Foto = null;
        };
        $scope.MaskPlaca = function () {
            try { $scope.Modelo.Placa = MascaraPlaca($scope.Modelo.Placa) } catch (e) { }
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function (option) {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);
        };
    }]);