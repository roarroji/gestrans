﻿SofttoolsApp.controller("ConsultarCiudadesCtrl", ['$scope', '$routeParams', '$timeout', 'CiudadesFactory', '$linq', 'blockUI', 'OficinasFactory', 'PaisesFactory', 'ZonasFactory', 'DepartamentosFactory', 'OficinasFactory',
    function ($scope, $routeParams, $timeout, CiudadesFactory, $linq, blockUI, OficinasFactory, PaisesFactory, ZonasFactory, DepartamentosFactory, OficinasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Ciudades' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'Activa', Codigo: 1 },
            { Nombre: 'Inactiva', Codigo: 0 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.ListadoEstadosZonas = [
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];
        $scope.ModalEstadoZona = $linq.Enumerable().From($scope.ListadoEstadosZonas).First('$.Codigo == 1');
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CIUDADES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinas = [];
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo != 0) {
                            $scope.ListadoOficinas.push(item);
                        }
                    });
                    $scope.Oficinas = response.data.Datos[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo de paises*/
        PaisesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '', Codigo: 0 })
                        $scope.ListadoPaises = response.data.Datos;
                        $scope.Pais = $scope.ListadoPaises[$scope.ListadoPaises.length - 1];
                    } else {
                        $scope.ListadoPaises = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        OficinasFactory.ConsultarRegionesPaises({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListaRegiones = response.data.Datos;

                        if ($scope.CodigoRegion !== undefined && $scope.CodigoRegion !== null && $scope.CodigoRegion !== '') {
                            $scope.Modelo.Region = $linq.Enumerable().From($scope.ListaRegiones).First('$.Codigo ==' + $scope.CodigoRegion);
                        }
                        //$scope.Modelo.RegionPaisDestino = $scope.ListaRegiones[0]
                        //$scope.Modelo.RegionPaisDestino2 = $scope.ListaRegiones[0]


                    }
                    else {
                        $scope.ListaRegiones = [];
                    }
                }
            }, function (response) {
            });
        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarCiudades';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoCiudades = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.Codigo,
                        CodigoAlterno: $scope.CodigoAlterno,
                        Nombre: $scope.Nombre,
                        Departamento: { Nombre: $scope.Departamento },
                        Paises: $scope.Pais,
                        Estado: $scope.ModalEstado.Codigo,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    }
                    blockUI.delay = 1000;
                    CiudadesFactory.Consultar(filtros).
                        then(function (response) {
                            console.log(response);
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    response.data.Datos.forEach(function (item) {
                                        var registro = item;
                                        $scope.ListadoCiudades.push(registro);
                                    });

                                    $scope.ListadoCiudades.forEach(function (item) {
                                        $scope.codigo = item.Codigo;
                                    });
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
            console.log('llego',$scope.ListadoCiudades)
        }
        /*------------------------------------------------------------------------------------Eliminar Ciudades-----------------------------------------------------------------------*/
        $scope.EliminarCiudades = function (codigo, Nombre, CodigoAlterno) {
            $scope.AuxiCodigo = codigo
            $scope.AuxiNombre = Nombre
            $scope.AuxiCodigoAlterno = CodigoAlterno
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarCiudades');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.AuxiCodigo,
            };

            CiudadesFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó la ciudad ' + $scope.AuxiCodigoAlterno + ' - ' + $scope.AuxiNombre);
                        closeModal('modalEliminarCiudades');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarCiudades');
                    $scope.ModalError = 'No se puede eliminar la ciudad ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };
        /* Obtener parametros*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }

        $scope.CerrarModal = function () {
            closeModal('modalEliminarCiudades');
            closeModal('modalMensajeEliminarCiudades');
            closeModal('modalMensajeEliminarZonas');
            closeModal('ModalZonas');
            $scope.ModeloZona = ''
            Find();
        }

        /*----------------------------------------------------------------------------FUNCIONES ZONAS CIUDADES----------------------------------------------------------------------------------------------*/
        $scope.ModalZonas = function (codigo, nombre) {
            showModal('ModalZonas');
            $scope.CodigoCiudad = codigo;
            $scope.NombreCiudad = nombre;
            $scope.ModeloZona = '';
        }

        $scope.ConfirmacionGuardarZonas = function () {
            showModal('modalConfirmacionGuardarZonas', 1);
        };

        $scope.BuscarZonas = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = $scope.CodigoCiudad
                Find()
                FindZonas()
            }
        };

        $scope.EliminarZonas = function (codigo, nombre) {
            $scope.Codigo
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: -1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            var contador = 0
                            response.data.Datos.forEach(function (item) {
                                if (item.CodigoZona === $scope.Codigo) {
                                    contador = contador + 1
                                }
                            });
                            if (contador > 0) {
                                ShowError('No se puede eliminar la zona porque tiene una oficina asociada')
                            } else {
                                $scope.CodigoZona = codigo
                                $scope.NombreZona = nombre
                                $scope.ModalErrorCompleto = ''
                                $scope.ModalError = ''
                                $scope.MostrarMensajeError = false;
                                showModal('modalEliminarZonas', 1);
                            }
                        }
                    }
                }, function (response) {
                });
        };

        $scope.ConfirmarEliminarZonas = function () {

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.CodigoZona,
                Ciudad: { Codigo: $scope.CodigoCiudad },
            };
            if ($scope.CodigoZona > 0) {
                ZonasFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            for (var i = 0; i < $scope.ListadoZonas.length; i++) {
                                if ($scope.ListadoZonas[i].Nombre === $scope.NombreZona) {
                                    $scope.ListadoZonas.splice(i, 1);
                                    i = $scope.ListadoZonas.length;
                                }
                            }
                            ShowSuccess('Se eliminó la zona ');
                            closeModal('modalEliminarZonas', 1);
                        }
                    }, function (response) {
                        var result = ''
                        showModal('modalMensajeEliminarZonas', 1);
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede eliminar la zona ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                        $scope.ModalErrorCompleto = response.statusText
                    });
            } else {
                for (var i = 0; i < $scope.ListadoZonas.length; i++) {
                    if ($scope.ListadoZonas[i].Nombre === $scope.NombreZona) {

                        $scope.ListadoZonas.splice(i, 1);
                        i = $scope.ListadoZonas.length;
                        ShowSuccess('Se eliminó la zona ');
                        closeModal('modalEliminarZonas', 1);
                    }
                }
            }
        };


        $scope.Guardar = function () {

            closeModal('modalConfirmacionGuardarZonas');

            $scope.ListadoZonas.forEach(function (item) {
                if (item.Codigo > 0) {
                    item.Estado = item.Estado
                } else {
                    item.Estado = item.Estado.Codigo
                }
            });

            $scope.DatosGuardar = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                ListaZonas: $scope.ListadoZonas,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            };

            ZonasFactory.Guardar($scope.DatosGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('Se guardaron las zonas ');
                            closeModal('ModalZonas');
                            FindZonas();
                            Find()
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError('No hay zonas que guardar ');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        function FindZonas() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.BuscandoZonas = true;
            $scope.MensajesError = [];
            DatosRequeridos();

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Ciudad: { Codigo: $scope.CodigoCiudad },
            };

            if ($scope.MensajesError.length === 0) {
                $scope.ListadoZonas = [];
                blockUI.delay = 1000;
                ZonasFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                response.data.Datos.forEach(function (item) {
                                    $scope.ListadoZonas.push(item);
                                });

                                $scope.totalRegistrosZonasZonas = response.data.Datos[0].TotalRegistros;
                                $scope.BuscandoZonas = true;
                                $scope.ResultadoSinRegistros = '';
                            } else {
                                $scope.ListadoZonas = [];
                                $scope.totalRegistrosZonas = 0;
                                $scope.ResultadoSinRegistrosZonas = 'no hay datos para mostrar';
                                $scope.BuscandoZonas = false;
                            }
                        }
                    }, function (response) {
                        $scope.BuscandoZonas = false;
                    });
            } else {
                $scope.BuscandoZonas = false;
            }
            blockUI.stop();
        };

        function DatosRequeridos() {
            if (filtros.Codigo === undefined) {
                filtros.Codigo = 0;
            }
        }

        $scope.AdicionarZonas = function () {
            $scope.MensajesErrorZonas = [];
            var continuar = true;
            if ($scope.ModeloZona === undefined || $scope.ModeloZona === '' || $scope.ModeloZona === null) {
                continuar = false;
                $scope.MensajesErrorZonas.push('Debe ingresar el nombre de la zona')
            }
            //if ($scope.ModalEstadoZona.Nombre === undefined || $scope.ModalEstadoZona.Nombre === '' || $scope.ModalEstadoZona.Nombre === null || $scope.ModalEstadoZona.Codigo === 0) {
            //    continuar = false;
            //    $scope.MensajesErrorZonas.push('Debe ingresar el estado')
            //}
            if (continuar === true) {
                NuevasZonas = {
                    Codigo: 0,
                    Ciudad: { Codigo: $scope.CodigoCiudad },
                    Nombre: $scope.ModeloZona,
                    Estado: $scope.ModalEstadoZona,
                    editado: true
                }
                $scope.ListadoZonas.push(JSON.parse(JSON.stringify(NuevasZonas)))
                $scope.ModeloZona = ''
            }
        }

        $scope.CambiarEstado = function (item) {
            if (item.Estado.Codigo === 0 || item.Estado === 0) {
                item.Estado.Codigo = 1
                item.Estado = 1
            } else {
                item.Estado.Codigo = 0
                item.Estado = 0
            }
            item.editable = false;
        }

        //----------------------------------------------------------------------------------------------------
        $scope.MaskNumero = function (option) {
            try { $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno) } catch (e) { }
        };

        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };

        $scope.RegionCiudades = function (Ciudad) {
            $scope.Ciudad = Ciudad
            showModal('modalRegionesCiudades')
            $scope.ListadoGridORegiones = []
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: Ciudad.Codigo,
            }
            blockUI.delay = 1000;
            CiudadesFactory.ConsultarRegiones(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoGridORegiones = [];
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                $scope.ListadoGridORegiones.push(item);
                            });
                        }
                    }
                    //$scope.ListadoGridORegiones.forEach(function (item) {
                    //    var ind = $scope.ListaRegiones.indexOf({ Codigo: item.Codigo })
                    //    if (ind > -1) {
                    //        $scope.ListaRegiones.splice(ind, 1)
                    //        $scope.Oficinas = $scope.ListaRegiones[0];
                    //    }
                    //});

                });
        }
        //#region ADICIONAR A LISTA
        $scope.AdicionarRegion = function (opcion) {
            if (opcion > 0) {
                for (var i = 0; i < $scope.ListaRegiones.length; i++) {
                    var Region = $scope.ListaRegiones[i]
                    $scope.RegionAgregada = 0;
                    $scope.ListadoGridORegiones.forEach(function (item) {
                        if (item.Codigo == Region.Codigo) {
                            $scope.RegionAgregada = 1;
                        }
                    });
                    if ($scope.RegionAgregada == 0) {
                        $scope.ListadoGridORegiones.push({ Codigo: Region.Codigo, Nombre: Region.Nombre });
                    }
                }
            } else {
                $scope.RegionAgregada = 0;
                $scope.ListadoGridORegiones.forEach(function (item) {
                    if (item.Codigo == $scope.Region.Codigo) {
                        $scope.RegionAgregada = 1;
                    }
                });
                if ($scope.RegionAgregada == 0) {
                    $scope.ListadoGridORegiones.push($scope.Region);
                }
                else {
                    ShowError("La región ya se encuentra adicionada");
                }
            }
            $scope.Region = ''
        }
        $scope.GuardarRegiones = function () {

            entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Ciudad.Codigo,
                Regiones: $scope.ListadoGridORegiones
            }
            CiudadesFactory.GuardarRegiones(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('regiones asociadas correctamente');
                            closeModal('modalRegionesCiudades')
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                });
        }


        $scope.OficinaCiudades = function (Ciudad) {
            $scope.Ciudad = Ciudad
            showModal('modalOficinaCiudades')
            $scope.ListadoGridOficinas = []
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: Ciudad.Codigo,
            }
            blockUI.delay = 1000;
            CiudadesFactory.ConsultarOficinas(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoGridOficinas = [];
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                var registro = item;
                                $scope.ListadoGridOficinas.push(registro);
                            });
                        }
                    }
                    //$scope.ListadoGridOficinas.forEach(function (item) {
                    //    var ind = $scope.ListadoOficinas.indexOf({ Codigo: item.Codigo })
                    //    if (ind > -1) {
                    //        $scope.ListadoOficinas.splice(ind, 1)
                    //        $scope.Oficinas = $scope.ListadoOficinas[0];
                    //    }
                    //});

                });
        }

        //#region ADICIONAR A LISTA
        $scope.AdicionarOficina = function (opcion) {
            if (opcion > 0) {
                for (var i = 0; i < $scope.ListadoOficinas.length; i++) {
                    var Oficina = $scope.ListadoOficinas[i]
                    $scope.OficinaAgregada = 0;
                    $scope.ListadoGridOficinas.forEach(function (item) {
                        if (item.Codigo == Oficina.Codigo) {
                            $scope.OficinaAgregada = 1;
                        }
                    });
                    if ($scope.OficinaAgregada == 0) {
                        $scope.ListadoGridOficinas.push({ Codigo: Oficina.Codigo, Nombre: Oficina.Nombre });
                    }
                }
            } else {
                $scope.OficinaAgregada = 0;
                $scope.ListadoGridOficinas.forEach(function (item) {
                    if (item.Codigo == $scope.Oficinas.Codigo) {
                        $scope.OficinaAgregada = 1;
                    }
                });
                if ($scope.OficinaAgregada == 0) {
                    $scope.ListadoGridOficinas.push($scope.Oficinas);
                }
                else {
                    ShowError("La oficina ya se encuentra adicionada");
                }
            }
        }
        $scope.GuardarOficina = function () {
            $scope.listaOficinasGuardar = [];
            $scope.ListadoGridOficinas.forEach(function (itmOficinas) {
                $scope.listaOficinasGuardar.push({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: itmOficinas.Codigo,
                });
            })
            entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Ciudad.Codigo,
                Oficinas: $scope.listaOficinasGuardar
            }
            CiudadesFactory.GuardarOficinas(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('Oficinas asociadas correctamente');
                            closeModal('modalOficinaCiudades')
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                });
        }
    }]);