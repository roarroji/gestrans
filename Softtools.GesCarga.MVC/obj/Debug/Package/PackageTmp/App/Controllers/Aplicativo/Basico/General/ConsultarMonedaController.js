﻿SofttoolsApp.controller("ConsultarMonedaCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'MonedasFactory', 'TRMFactory', 'blockUI',
    function ($scope, $routeParams, $timeout, $linq, MonedasFactory, TRMFactory, blockUI) {
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Moneda - TRM' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        var filtros = {};
        var TRM = {};
        $scope.Modelo = {};
        $scope.ListadoEstados = [];
        $scope.MensajesErrorTRM = [];
        $scope.ListadoTRM = [];
        $scope.MostrarMensajeError = false
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.varLocal = 1;
        $scope.OpcionModifica = 0;
        $scope.Valor_Moneda_Local = 0;
        $scope.Local = 0;
        $scope.CodLocal = 0;
        $scope.ModeloFecha = new Date();
        $scope.ListadoAuxiliar = [];

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OCION_MENU_MONEDA);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'Activa', Codigo: 1 },
            { Nombre: 'Inactiva', Codigo: 0 }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');

        if ($routeParams.Codigo !== undefined) {
            Find();
        }
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        $scope.PrimerPaginaTRM = function () {
            /*Verificar si la página actual ya está guardada si no lo esta procede a realizar la busqueda*/
            if ($scope.paginaActual > 1) {

                if ($scope.ListadoAuxiliar.length > 0) {

                    $scope.paginaActual = 1;
                    $scope.ListadoTRM = [];

                    var i = 0;
                    for (i = 0; i <= $scope.ListadoAuxiliar.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina) {
                            $scope.ListadoTRM.push($scope.ListadoAuxiliar[i])
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = 1;
                        $scope.ListadoTRM = [];

                        var i = 0;
                        for (i = 0; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina) {
                                $scope.ListadoTRM.push($scope.ListadoRecorridosTotal[i])
                            }
                        }
                    }
                }

            }


        };

        $scope.SiguienteTRM = function () {

            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.ListadoTRM = [];
                $scope.PaginaAuxiliar = $scope.paginaActual;
                $scope.paginaActual += 1;

                if ($scope.ListadoAuxiliar.length > 0) {

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoAuxiliar.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoTRM.push($scope.ListadoAuxiliar[i]);
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoTRM.push($scope.ListadoRecorridosTotal[i]);
                            }
                        }
                    }
                }
            }

        }

        $scope.AnteriorTRM = function () {
            if ($scope.paginaActual > 1) {

                if ($scope.ListadoAuxiliar.length > 0) {

                    $scope.ListadoTRM = [];
                    $scope.paginaActual -= 1;

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListadoAuxiliar.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoTRM.push($scope.ListadoAuxiliar[i]);
                        }
                    }

                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.ListadoTRM = [];
                        $scope.paginaActual -= 1;

                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoTRM.push($scope.ListadoRecorridosTotal[i]);
                            }
                        }
                    }
                }

            }
        };

        $scope.UltimaPaginaTRM = function () {

            if ($scope.paginaActual < $scope.totalPaginas) {

                if ($scope.ListadoAuxiliar.length > 0) {
               // if ($scope.ListadoRecorridosTotal.length > 0) {
                    $scope.paginaActual = $scope.totalPaginas;
                    $scope.ListadoTRM = [];

                    var i = 0;
                    for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListadoAuxiliar.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListadoTRM.push($scope.ListadoAuxiliar[i])
                        }
                    }
                  //  }
                } else {
                    if ($scope.ListadoRecorridosTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListadoTRM = [];

                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListadoRecorridosTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListadoTRM.push($scope.ListadoRecorridosTotal[i])
                            }
                        }
                    }
                }

            }
        }


        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/

        /*Metodo buscar*/
        $scope.Buscar = function () {

            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }

        };

        $scope.BuscarTRM = function () {

            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = $scope.MoneCodigo
                FindTRM()
            }

        };
    /*-------------------------*/

        //$scope.TempAdicionados = [];
        $scope.AdicionarTRM = function (NombreMoneda) {
            $scope.ListadoAuxiliar = $scope.ListaOriginal;

            $scope.MensajesErrorTRM = [];
            var continuar = true;
            if ($scope.ModeloFecha == undefined) {
                continuar = false;
                $scope.MensajesErrorTRM.push('Ingrese una Fecha')
            } else {

                if ($scope.ListadoAuxiliar.length > 0) {
                    $scope.ListadoAuxiliar.forEach(function (item) {
                        var Fecha = new Date(item.Fecha)
                        Fecha = Fecha.toString()
                        if ($scope.ModeloFecha.toString() == Fecha.toString()) {
                            Fechafin = Formatear_Fecha_Dia_Mes_Ano(new Date(item.Fecha));
                            continuar = false;
                            $scope.MensajesErrorTRM.push('Ya se ingresó un valor de TRM para el día ' + Fechafin)
                        }
                    });

                } else {
                    continuar = true;
                }

            }

            if ($scope.ModeloValor == undefined || $scope.ModeloValor == '' || $scope.ModeloValor == null || isNaN($scope.ModeloValor)) {
                continuar = false;
                $scope.MensajesErrorTRM.push('Debe Ingresar Un valor ')

            }
            if (continuar == true) {
                console.log("scopeModeloFecha: ",$scope.ModeloFecha)
                NuevoTRM = {
                    Codigo: $scope.MoneCodigo,
                    Nombre_Corto: NombreMoneda,
                   // Fecha: $scope.ModeloFecha,

                    Fecha: new Date($scope.ModeloFecha.getTime() - $scope.ModeloFecha.getTimezoneOffset() * 60000).toISOString(),
                    
                    Valor_Moneda_Local: $scope.ModeloValor,
                    editado: true
                }
                console.log("FechaISO: ", NuevoTRM.Fecha);
                $scope.ListadoAuxiliar.push(NuevoTRM);
                $scope.ListaOriginal = $scope.ListadoAuxiliar;
                $scope.totalRegistros = ($scope.ListadoAuxiliar.length);
              
                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                $scope.ListadoAuxiliar = $scope.ListadoAuxiliar.sort((a, b) => new Date(b.Fecha) - new Date(a.Fecha));
                
                $scope.ListadoTRM = [];
                $scope.PaginaAuxiliar = $scope.paginaActual -1;
                $scope.paginaTemp = $scope.paginaActual ;

                var i = 0;
                for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoAuxiliar.length - 1; i++) {
                    if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaTemp) {
                        $scope.ListadoTRM.push($scope.ListadoAuxiliar[i]);

                    }
                }
               
            }


        }

        /*-------------------------------------------------------------------------------------------Funcion Buscar----------------------------------------------------------------*/
        $scope.ModalTRM = function (Codigo, nombre) {
            showModal('ModalTRM');
            $scope.MensajesErrorTRM = [];
            $scope.MoneCodigo = Codigo;
            $scope.NombreMoneda = nombre;
            $scope.Modelo.Valor = 0;
            $scope.Modelo.Fecha = '';

        }

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarMoneda/';
            }
        };



        $scope.filtrotabla = function (Fecha) {
         

            $scope.ListadoAuxiliar = $scope.ListaOriginal;
                
            
            if (Fecha != null) {
                var FiltroFecha = new Date(Fecha.getTime() - Fecha.getTimezoneOffset() * 60000)
                FiltroFecha = FiltroFecha.toISOString();
                console.log("fecha a filtrar: ", FiltroFecha, "formato Date(): ", new Date(Fecha.getTime() - Fecha.getTimezoneOffset() * 60000), "original: ", Fecha);
                console.log($scope.ModeloValor);
                $scope.listadofiltro = [];
                $scope.ListadoAuxiliar.forEach(function (item) {
                    var fechaItem = item.Fecha.toString();
                    var arrayFechaItem = fechaItem.split("T");
                    var arrayFiltroFecha = FiltroFecha.split("T");
                    if (arrayFechaItem[0] == arrayFiltroFecha[0]) {
                        if ($scope.ModeloValor == undefined || isNaN($scope.ModeloValor) || $scope.ModeloValor == '') {
                            $scope.listadofiltro.push(item)
                        } else {
                            if (item.Valor_Moneda_Local == $scope.ModeloValor) {
                                $scope.listadofiltro.push(item)
                            }
                        }
                    }
                })
                if ($scope.listadofiltro.length > 0) {
                    $scope.ListadoTRM = $scope.listadofiltro;
                } else {
                    if ($scope.ModeloValor == undefined || isNaN($scope.ModeloValor) || $scope.ModeloValor == '') {
                        ShowError('La Fecha ingresada no se ha sido registrada ')
                        console.log("modeloValor: ", $scope.ModeloValor, "listaAuxiliar: ", $scope.ListadoAuxiliar);

                    } else {
                        ShowError('La Fecha con este valor ingresado no ha sido registrado ')
                    }
                    $scope.ListadoTRM = [];
                    if ($scope.totalRegistros > 10) {
                        for (var i = 0; i < 10; i++) {
                            $scope.ListadoTRM.push($scope.ListadoAuxiliar[i])
                        }
                    } else {
                        $scope.ListadoTRM = $scope.ListadoAuxiliar
                    }
                }
            } else {       


                if ($scope.ModeloValor == undefined || isNaN($scope.ModeloValor) || $scope.ModeloValor == '') {
                    $scope.ListadoTRM = $scope.ListadoAuxiliar
                    $scope.ListadoTRM = [];
                    $scope.PaginaAuxiliar = $scope.paginaActual - 1;
                    $scope.paginaTemp = $scope.paginaActual;

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoAuxiliar.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaTemp) {
                            $scope.ListadoTRM.push($scope.ListadoAuxiliar[i]);
                        }
                    }
                } else {
                    $scope.ListaTemporal = [];
                    for (var k = 0; k < $scope.ListadoAuxiliar.length; k++) {
                        if ($scope.ListadoAuxiliar[k].Valor_Moneda_Local == $scope.ModeloValor) {
                            $scope.ListaTemporal.push($scope.ListadoAuxiliar[k]);
                        }
                    }
                    $scope.ListadoAuxiliar = $scope.ListaTemporal;


                    $scope.ListadoTRM = [];
                    $scope.PaginaAuxiliar = $scope.paginaActual - 1;
                    $scope.paginaTemp = $scope.paginaActual;

                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListadoAuxiliar.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaTemp) {
                            $scope.ListadoTRM.push($scope.ListadoAuxiliar[i]);

                        }
                    }
                }
            }

           
                

        };




        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            $scope.ListadoMoneda = [];

            filtros = {
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo,
                Nombre: $scope.Modelo.Nombre,
                NombreCorto: $scope.Modelo.NombreCorto,
                Estado: $scope.Modelo.Estado.Codigo

            };

            if ($scope.MensajesError.length == 0) {
                blockUI.delay = 1000;
                MonedasFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoMoneda = []
                                response.data.Datos.forEach(function (item) {
                                    if (item.Codigo > 0) {

                                        $scope.ListadoMoneda.push(item);
                                    }
                                    if (item.Local == 1) {
                                        $scope.Local = item.Local;
                                        $scope.CodLocal = item.Codigo;

                                    }

                                });

                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.Local = 0;
                                $scope.CodLocal = 0;
                                $scope.ListadoMoneda = "";
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'no hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };

        function FindTRM() {
            $scope.ListaOriginal = [];
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];

            DatosRequeridos();

            filtros = {
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.MoneCodigo,
                Nombre: $scope.Nombre,

            };

            if ($scope.MensajesError.length == 0) {
                $scope.ListadoTRM = [];
                blockUI.delay = 1000;
                TRMFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            var Temporal = []
                            if (response.data.Datos.length > 0) {
                                response.data.Datos.forEach(function (item) {


                                    Temporal.push(item);
                                });
                                
                                $scope.ListadoAuxiliar = Temporal;
                                $scope.ListaOriginal = Temporal;
                                $scope.totalRegistros = ($scope.ListadoAuxiliar.length);
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = true;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.ListadoTRM = [];
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'no hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                        if ($scope.totalRegistros > 10) {
                            for (var i = 0; i < 10; i++) {
                                $scope.ListadoTRM.push($scope.ListadoAuxiliar[i])
                            }
                        } else {
                            $scope.ListadoTRM = $scope.ListadoAuxiliar
                        }
                       // $scope.ListadoTRM = $scope.ListadoTRM.sort((a, b) => a.Fecha - b.Fecha);
                    }, function (response) {
                        $scope.Buscando = false;
                    });
            }
            else {
                $scope.Buscando = false;
            }
            blockUI.stop();
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        function DatosRequeridos() {
            if (filtros.Codigo == undefined) {
                filtros.Codigo = 0;
            }
        }
        /*------------------------------------------------------------------------------------Eliminar Oficinas-----------------------------------------------------------------------*/
        $scope.EliminarMoneda = function (codigo, Nombre) {
            $scope.Codigo = codigo
            $scope.Codigo = codigo
            $scope.Nombre = Nombre
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarMoneda');
        };
        $scope.EliminarTRM = function (codigo, fecha) {
            $scope.Codigo = codigo
            $scope.fecha = fecha
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false;
            showModal('modalEliminarTRM');
        };


        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Codigo
            };

            MonedasFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó la moneda : ' + $scope.Nombre);
                        closeModal('modalEliminarMoneda');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    showModal('modalMensajeEliminarMoneda');
                    result = InternalServerError(response.statusText)
                    $scope.ModalError = 'No se puede eliminar la moneda ' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };


        $scope.ConfirmarEliminarTRM = function () {
            $scope.ListadoAuxiliar = $scope.ListaOriginal;
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.MoneCodigo,
                Fecha: $scope.fecha
            };

            TRMFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se Borró el TRM ');
                        closeModal('modalEliminarTRM');
                        Find();
                        FindTRM();
                    }
                    else {
                        for (var i = 0; i < $scope.ListadoAuxiliar.length; i++) {
                            if ($scope.ListadoAuxiliar[i].Fecha == $scope.fecha) {

                                $scope.ListadoAuxiliar.splice(i, 1);

                                i = $scope.ListadoAuxiliar.length;
                               
                            }
                        }
                        $scope.ListaOriginal = $scope.ListadoAuxiliar;

                        for (var i = 0; i < $scope.ListadoTRM.length; i++) {
                            if ($scope.ListadoTRM[i].Fecha == $scope.fecha) {

                                $scope.ListadoTRM.splice(i, 1);
                               
                                i = $scope.ListadoTRM.length;
                                ShowSuccess('Se Borró el  TRM ');
                                closeModal('modalEliminarTRM');
                            }
                        }



                    }
                }, function (response) {
                    var result = ''
                    showModal('modalMensajeEliminarTRM');
                    result = InternalServerError(response.statusText)
                    $scope.ModalError = 'no se puede Eliminar el TRM' + $scope.Nombre + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };


        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /* Obtener parametros*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }

        $scope.CerrarModal = function () {
            closeModal('modalEliminarMoneda');
            closeModal('modalMensajeEliminarMoneda');
            closeModal('modalMensajeEliminarTRM');
            closeModal('ModalTRM');
            $scope.MoneCodigo = 0;
            $scope.NombreMoneda = ''
        }


        $scope.ModifcarTRM = function (Fecha, Valor_Moneda_Local) {
            $scope.Modelo.Fecha = new Date(Fecha);
            $scope.Modelo.Valor = Valor_Moneda_Local;

        }

        $scope.ConfirmacionGuardarTRM = function () {
            showModal('modalConfirmacionGuardarTRM');

        };

        $scope.ValidarModificacion = function (Item) {

            if (Item.Valor_Moneda_Local == 0 || Item.Valor_Moneda_Local == '' || Item.Valor_Moneda_Local == undefined || isNaN(Item.Valor_Moneda_Local)) {

                $scope.MensajesErrorTRM.push('No puede quedar el valor de la moneda vacia');


            } else {
                if (Item.Fecha == 0 || Item.Fecha == '' || Item.Fecha == undefined) {
                    $scope.MensajesErrorTRM.push('No puede quedar la fecha vacia');

                }
                else {


                    Item.editable = false;
                }
            };
        }




        $scope.Guardar = function () {
           
           /* if ($scope.TempAdicionados.length > 0) {
                for (var n = 0; n < $scope.TempAdicionados.length; n++) {
                    $scope.ListadoAuxiliar.push($scope.TempAdicionados[n]);
                }
            }*/
            $scope.ListadoAuxiliar = $scope.ListaOriginal;
            console.log("$scope.ListadoTRM: ", $scope.ListadoTRM);
            var lista = [];
            var listaModifica = [];

            /*Verificar si la página actual ya está guardada antes de proceder a guardar*/
            closeModal('modalConfirmacionGuardarTRM');
            var indicador = 0
            $scope.ListadoAuxiliar.forEach(function (item) {
                if (item.editado != undefined && item.editado == true) {
                    listaModifica.push({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: $scope.MoneCodigo,
                        //Fecha: RetornarFechaEspecificaSinTimeZone(item.Fecha),
                        Fecha: new Date(item.Fecha),
                        Valor_Moneda_Local: item.Valor_Moneda_Local,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                    });

                }

            });

            console.log("listaModificada: ", listaModifica);
            TRMFactory.Guardar({ Lista: listaModifica }).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('Se guardó la información de TRM ');
                            FindTRM();
                            closeModal('ModalTRM');
                            MensajesErrorTRM = [];
                            $scope.ModeloFecha = '';
                            $scope.ModeloValor = '';
                            Find()
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);

                        }
                    }
                    else {
                        ShowInfo('No hay TRM que guardar ');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

           // $scope.TempAdicionados = [];

        }
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);

        };


    }]);