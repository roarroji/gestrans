﻿SofttoolsApp.controller("GestionarPaisesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'PaisesFactory', 'MonedasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, PaisesFactory, MonedasFactory) {

        $scope.Titulo = 'GESTIONAR PAÍSES';
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'General' }, { Nombre: 'Países' }, { Nombre: 'Gestionar' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PAISES);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0
        }

        $scope.ListadoMonedas = []
        $scope.ListadoEstados = [
            { Codigo: 0, Nombre: "INACTIVO" },
            { Codigo: 1, Nombre: "ACTIVO" },
        ]

        /* Obtener parametros*/
        if ($routeParams.Codigo > 0) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
            Obtener();
        }
        else {
            $scope.Modelo.Codigo = 0;
            $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == 1');
        }
        MonedasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: '(TODAS)', Codigo: 0 })
                        $scope.ListadoMonedas = response.data.Datos;

                        if ($scope.Moneda > 0) {
                            $scope.Modelo.Moneda = $linq.Enumerable().From($scope.ListadoMonedas).First('$.Codigo ==' + $scope.Moneda);
                        } else {
                            $scope.Modelo.Moneda = $scope.ListadoMonedas[$scope.ListadoMonedas.length - 1];
                        }
                    }
                    else {
                        $scope.ListadoMonedas = [];
                        $scope.Modelo.Moneda = null;
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });


        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando país código No. ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando país Código No.' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            PaisesFactory.Obtener(filtros).
               then(function (response) {
                   if (response.data.ProcesoExitoso === true) {

                       $scope.Modelo.Codigo = response.data.Datos.Codigo;
                       $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno;
                       $scope.Modelo.Nombre = response.data.Datos.Nombre;
                       $scope.Modelo.NombreCorto = response.data.Datos.NombreCorto;
                       $scope.Moneda = response.data.Datos.Moneda.Codigo;
                       if ($scope.ListadoMonedas.length > 0 && $scope.Moneda > 0) {
                           $scope.Modelo.Moneda = $linq.Enumerable().From($scope.ListadoMonedas).First('$.Codigo ==' + response.data.Datos.Moneda.Codigo);
                       }

                       $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado)

                   }
                   else {
                       ShowError('No se logro consultar el país código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                       document.location.href = '#!ConsultarPaises';
                   }
               }, function (response) {
                   ShowError(response.statusText);
                   document.location.href = '#!ConsultarPaises';
               });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            if (DatosRequeridos()) {

                //Estado
                $scope.Modelo.Estado = $scope.ModalEstado.Codigo;

                PaisesFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó el país "' + $scope.Modelo.Nombre + '"');
                                }
                                else {
                                    ShowSuccess('Se Modificó el país "' + $scope.Modelo.Nombre + '"');
                                }
                                location.href = '#!ConsultarPaises/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };


        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if ($scope.Modelo.CodigoAlterno == undefined || $scope.Modelo.CodigoAlterno == null) {
                $scope.Modelo.CodigoAlterno = ''
            }
            if ($scope.Modelo.Nombre == undefined || $scope.Modelo.Nombre == '') {
                $scope.MensajesError.push('Debe ingresar el nombre del país');
                continuar = false;
            }
            if ($scope.Modelo.Moneda == undefined || $scope.Modelo.Moneda == '' || $scope.Modelo.Moneda == null || $scope.Modelo.Moneda.Codigo == 0) {
                $scope.MensajesError.push('Debe ingresar la moneda del país');
                continuar = false;
            }
            return continuar;
        }

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarPaises/' + $scope.Modelo.Codigo;
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            try { $scope.Modelo.CodigoAlterno = MascaraNumero($scope.Modelo.CodigoAlterno) } catch (e) { }
        };
    }]);