﻿SofttoolsApp.controller("GestionarLineaMantenimientoEspecialCtrl", ['$scope', '$routeParams', 'blockUI', '$timeout', 'ValorCatalogosFactory', '$linq', 'LineaMantenimientoFactory',
    function ($scope, $routeParams, blockUI, $timeout, ValorCatalogosFactory, $linq, LineaMantenimientoFactory) {
        $scope.ListadoDetallePlanMantenimiento = [];
        $scope.MensajesError = [];
        $scope.MensajesErrorDetalle = [];
        $scope.ListadoEstados = [];
        $scope.ModificarDetalle = 0;
        $scope.Operacion = 'Adicionar';
        $scope.ValidarvalorManoObra = false
        $scope.ValidarValorRepuestos = false
        $scope.ValidarSubtotal = false
        $scope.CodigoAlterno = '';

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_LINEA_MANTENIMIENTO);

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        // Se crea la propiedad modelo en el ambito
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Mantenimiento' }, { Nombre: 'Lineas' }];

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: 0,
            Estado: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },

        };
        $scope.ModeloFecha = new Date();

        /* Obtener parametros del enrutador*/
        if ($routeParams.Codigo !== undefined) { 
            $scope.Modelo.Codigo = $routeParams.Codigo;
        }
        else {
            $scope.Modelo.Codigo = 0;
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR LÍNEA';
            Obtener();
        }

        //var ResponseListadoEstados = ValorCatalogosFactory.Consultar({
        //    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        //    Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_REGISTROS }, Sync: true
        //}).Datos;
         
        //$scope.ListadoEstados = [];
        //ResponseListadoEstados.forEach(function (item) {
        //    if (item.Codigo != ESTADO_REGISTRO_NO_APLICA) {
        //        $scope.ListadoEstados.push(item);
            
        //    }
        //});

        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ]
        $scope.Modelo.Estado = $scope.ListadoEstados[0];

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando linea ' + $scope.Modelo.Codigo); 
            $timeout(function () {
                blockUI.message('Cargando linea  ' + $scope.Modelo.Codigo);
            }, 200); 
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            }; 
            blockUI.delay = 1000;
            LineaMantenimientoFactory.Obtener(filtros).
                then(function (response) { 
                    if (response.data.ProcesoExitoso === true) { 
                        $scope.Nombre = response.data.Datos.Nombre
                        $scope.CodigoAlterno = response.data.Datos.CodigoAlterno
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado.Codigo); 
                    }
                    else {
                        ShowError('No se logro consultar la linea  No.' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarLineaMantenimiento';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    //ShowError('No se logro consultar el plan de mantenimiento No.' + $scope.Modelo.Codigo + '. Por favor contacte el administrador del sistema.');
                        document.location.href = '#!ConsultarLineaMantenimiento';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if (parseInt($scope.Modelo.Codigo) > 0) {
                document.location.href = '#!ConsultarLineaMantenimiento/' + $scope.Modelo.Codigo;
            }
            else {
                document.location.href = '#!ConsultarLineaMantenimiento';
            }

        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
       
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para guardar /modificar
        //Guardar:
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar'); 
            $scope.Modelo.Nombre = $scope.Nombre
            $scope.Modelo.CodigoAlterno = $scope.CodigoAlterno;
             if (DatosRequeridos()) {
                LineaMantenimientoFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    ShowSuccess('Se guardó la línea ' + $scope.Modelo.Nombre);
                                    location.href = '#!ConsultarLineaMantenimiento/' + response.data.Datos;
                                }
                                else {
                                    ShowSuccess('Se modificó la línea ' + $scope.Modelo.Nombre);
                                    location.href = '#!ConsultarLineaMantenimiento/' + response.data.Datos;
                                }

                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }; 
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Valida los datos requridos 
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Nombre == '' || $scope.Nombre == 0 || $scope.Nombre == undefined) {
                $scope.MensajesError.push('Debe ingresar el nombre de la linea');
                continuar = false;
            } 
            return continuar

        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Asignar Movimiento Al Comprobante*/
        $scope.GuardarDetalleMantenimiento = function () {
            $scope.MensajesErrorDetalleMantenimiento = [];
            if (DatosRequeridosPlanMantenimiento()) {
                CargarListadoDetallePlanMantenimiento()
            }
        }

        $scope.MaskMayus = function () {
            try { $scope.Nombre = $scope.Nombre.toUpperCase() } catch (e) { }
        };

    }]);