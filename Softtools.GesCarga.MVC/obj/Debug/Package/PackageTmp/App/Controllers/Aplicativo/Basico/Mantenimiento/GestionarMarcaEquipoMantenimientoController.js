﻿SofttoolsApp.controller("GestionarMarcaEquipoMantenimientoCtrl", ['$scope', '$routeParams', 'blockUI', '$timeout', 'ValorCatalogosFactory', '$linq', 'MarcaEquipoMantenimientoFactory',
    function ($scope, $routeParams, blockUI, $timeout, ValorCatalogosFactory, $linq, MarcaEquipoMantenimientoFactory) {
        $scope.MensajesError = [];
        $scope.MensajesErrorDetalle = [];
        $scope.ListadoEstados = [];
        $scope.ModificarDetalle = 0;
        $scope.Operacion = 'Adicionar';


        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_MARCA_EQUIPO_MANTENIMIENTO);
        $scope.ListadoDocumentos = [{
            idRow: 1,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Referencia: '',
            Documento: '',
            Extencion: ''
        }]
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        // Se crea la propiedad modelo en el ambito
        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Mantenimiento' }, { Nombre: 'Marca Equipos' }];
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: 0,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
        };
        

        if ($routeParams.Codigo !== undefined) {
            $scope.Modelo.Codigo = $routeParams.Codigo;
        
        }
        else {
            $scope.Modelo.Codigo = 0;
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*Cargar el combo de estados*/

        $scope.ListadoEstados = [
            { Codigo: 1, Nombre: "ACTIVO" },
            { Codigo: 0, Nombre: "INACTIVO" },
        ]
        $scope.Modelo.Estado = $scope.ListadoEstados[0];

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            $scope.Titulo = 'CONSULTAR MARCA EQUIPO';
            Obtener();
        }

        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando marca equipo  ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando marca equipo ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            MarcaEquipoMantenimientoFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Nombre = response.data.Datos.Nombre
                        $scope.Modelo.CodigoAlterno = response.data.Datos.CodigoAlterno
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == ' + response.data.Datos.Estado.Codigo);
                    }
                    else {
                        ShowError('No se logro consultar la marca de equipo de mantenimiento No.' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarMarcaEquiposMantenimiento';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    //ShowError('No se logro consultar el plan de mantenimiento No.' + $scope.Modelo.Codigo + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#!ConsultarMarcaEquiposMantenimiento';
                });

            blockUI.stop();
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            if (parseInt($scope.Modelo.Codigo) > 0) {
                document.location.href = '#!ConsultarMarcaEquiposMantenimiento/' + $scope.Modelo.Codigo;
            }
            else {
                document.location.href = '#!ConsultarMarcaEquiposMantenimiento';
            }
        };

        // Metodo para guardar /modificar
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.Guardar = function () {
            if (DatosRequeridos()) {
                MarcaEquipoMantenimientoFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                if ($scope.Modelo.Codigo == 0) {
                                    blockUI.stop();
                                    ShowSuccess('Se guardó la marca equipo ' + $scope.Modelo.Nombre);
                                    location.href = '#!ConsultarMarcaEquiposMantenimiento/' + response.data.Datos;
                                }
                                else {
                                    blockUI.stop();
                                    ShowSuccess('Se modificó la marca equipo ' + $scope.Modelo.Nombre);
                                    location.href = '#!ConsultarMarcaEquiposMantenimiento/' + $scope.Modelo.Codigo;
                                }
                                closeModal('modalConfirmacionGuardar');
                                closeModal('modalInsertarMarcaEquipoMantenimiento');

                            }
                            else {
                                blockUI.stop();
                                ShowError(response.data.MensajeOperacion);
                                location.href = '#!ConsultarMarcaEquiposMantenimiento';
                            }
                        }
                        else {
                            blockUI.stop();
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                blockUI.stop();

            }
            else {
                closeModal('modalConfirmacionGuardar', 1)
            }
        }
        // Valida los datos requridos 
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;

            if ($scope.Modelo.Nombre == '' || $scope.Modelo.Nombre == undefined) {
                $scope.MensajesError.push('Debe ingresar el nombre del la marca ');
                continuar = false;
            }
            if ($scope.Modelo.Estado == '' || $scope.Modelo.Estado == undefined) {
                $scope.MensajesError.push('Debe ingresar el estado del la marca');
                continuar = false;
            }
            else if ($scope.Modelo.Estado.Codigo == 1292) {
                $scope.MensajesError.push('Debe ingresar el estado del la marca');
                continuar = false;
            }

            return continuar

        }

        $scope.MaskMayus = function () {
            try { $scope.Modelo.Nombre = $scope.Modelo.Nombre.toUpperCase() } catch (e) { }
        };
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        }
    }]);