﻿SofttoolsApp.controller("ConsultarConceptoContablesCtrl", ['$scope', '$timeout', 'ConceptoContablesFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'OficinasFactory', function ($scope, $timeout, ConceptoContablesFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, OficinasFactory) {

    $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Conceptos Contables' }];


    // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

    var filtros = {};
    $scope.cantidadRegistrosPorPaginas = 10;
    $scope.paginaActual = 1;
    $scope.ModalErrorCompleto = ''
    $scope.ModalError = ''
    $scope.MostrarMensajeError = false
    $scope.ListadoEstados = [
        { Nombre: '(TODOS)', Codigo: -1 },
        { Nombre: 'ACTIVO', Codigo: 1 },
        { Nombre: 'INACTIVO', Codigo: 0 }
    ];

    $scope.Modelo = {}
    $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
    $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
    $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


    try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CONCEPTOS_CONTABLES);
    } catch (e) {
        ShowError('No tiene permiso para visualizar esta pagina')
        document.location.href = '#'
    }

    /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
    $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

    $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
    $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
    $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
    $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
    $scope.PrimerPagina = function () {
        $scope.paginaActual = 1;
        Find();
    };

    $scope.Siguiente = function () {
        if ($scope.paginaActual < $scope.totalPaginas) {
            $scope.paginaActual = $scope.paginaActual + 1;
            Find();
        }
    };

    $scope.Anterior = function () {
        if ($scope.paginaActual > 1) {
            $scope.paginaActual = $scope.paginaActual - 1;
            Find();
        }
    };

    $scope.UltimaPagina = function () {
        $scope.paginaActual = $scope.totalPaginas;
        Find();
    };

    /*Cargar el combo de Oficinas*/
    OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.ListadoOficinas = [];
                $scope.ListadoOficinas = response.data.Datos
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: 0 })
                $scope.Modelo.OficinaAplica = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==0');
            }
            else {
                $scope.ListadoOficinas = [];
            }
        }, function (response) {
        });
    /*Cargar el combo de Documento Origen*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DOCUMENTO_ORIGEN } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoDocumentoOrigen = []
                    $scope.ListadoDocumentoOrigen = response.data.Datos
                    $scope.Modelo.DocumentoOrigen = $scope.ListadoDocumentoOrigen[0]
                }
                else {
                    $scope.ListadoDocumentoOrigen = []
                }
            }
        }, function (response) {
        });
    /*Cargar el combo de Tipo Concepto Contable*/
    ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CONCEPTO_CONTABLE } }).
        then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                if (response.data.Datos.length > 0) {
                    $scope.ListadoTipoConcepto = []
                    $scope.ListadoTipoConcepto = response.data.Datos
                    $scope.Modelo.TipoConceptoContable = $scope.ListadoTipoConcepto[0]
                }
                else {
                    $scope.ListadoTipoConcepto = []
                }
            }
        }, function (response) {
        });

    //Funcion Nuevo Documento
    $scope.NuevoDocumento = function () {
        if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
            document.location.href = '#!GestionarConceptoContables';
        }
    };
    //-------------------------------------------------FUNCIONES--------------------------------------------------------
    $scope.Buscar = function () {
        if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
            $scope.Codigo = 0
            Find()
        }
    };

    function Find() {
        blockUI.start('Buscando registros ...');

        $timeout(function () {
            blockUI.message('Espere por favor ...');
        }, 100);
        $scope.Buscando = true;
        $scope.MensajesError = [];
        $scope.ListadoConceptosContables = [];
        if ($scope.Buscando) {
            if ($scope.MensajesError.length == 0) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    CodigoAlterno: $scope.CodigoAlterno,
                    OficinaAplica: $scope.Modelo.OficinaAplica,
                    DocumentoOrigen: $scope.Modelo.DocumentoOrigen,
                    TipoConceptoContable: $scope.Modelo.TipoConceptoContable,
                    Nombre: $scope.Nombre,
                    Codigo: $scope.Codigo,
                    Estado: $scope.ModalEstado.Codigo,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                }
                blockUI.delay = 1000;
                ConceptoContablesFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                response.data.Datos.forEach(function (item) {
                                    var registro = item;
                                    $scope.ListadoConceptosContables.push(registro);
                                });

                                $scope.ListadoConceptosContables.forEach(function (item) {
                                    $scope.codigo = item.Codigo;
                                });
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                            var listado = [];
                            response.data.Datos.forEach(function (item) {
                                listado.push(item);
                            });
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        blockUI.stop();
    }
    /*------------------------------------------------------------------------------------Eliminar Concepto Contables-----------------------------------------------------------------------*/
    $scope.EliminarConceptoContables = function (codigo, Nombre) {
        $scope.CodigoConceptoContables = codigo
        $scope.NombreConceptoContables = Nombre
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        showModal('modalEliminarConceptoContables');
    };

    $scope.Eliminar = function () {
        var entidad = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            Codigo: $scope.CodigoConceptoContables,
        };

        ConceptoContablesFactory.Anular(entidad).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    ShowSuccess('Se eliminó el concepto contable ' + $scope.NombreConceptoContables);
                    closeModal('modalEliminarConceptoContables');
                    Find();
                }
                else {
                    ShowError(response.data.MensajeOperacion);
                }
            }, function (response) {
                var result = ''
                result = InternalServerError(response.statusText)
                showModal('modalMensajeEliminarConceptoContables');
                $scope.ModalError = 'No se puede eliminar el concepto contable ' + $scope.NombreConceptoContables + ' ya que se encuentra relacionada con ' + result;
                $scope.ModalErrorCompleto = response.statusText

            });

    };
    /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    /* Obtener parametros*/
    if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
        $scope.Codigo = $routeParams.Codigo;
        Find();
    }
    $scope.CerrarModal = function () {
        closeModal('modalEliminarConceptoContables');
        closeModal('modalMensajeEliminarConceptoContables');
    }

    $scope.MaskNumero = function () {
        $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno)
    };

}]);