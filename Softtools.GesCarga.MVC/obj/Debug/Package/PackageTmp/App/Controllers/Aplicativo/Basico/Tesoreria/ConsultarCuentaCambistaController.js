﻿SofttoolsApp.controller("ConsultarCuentaCambistaCtrl", ['$scope', '$timeout', 'CuentaBancariasFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'OficinasFactory',
    function ($scope, $timeout, CuentaBancariasFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, OficinasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Básico' }, { Nombre: 'Tesoreria' }, { Nombre: 'Cuentas Cambistas' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.NumeroCuenta = ''
        $scope.modalNombre = '';
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.ListadoTipoCuentaBancaria = []
        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;


        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + 100308);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        $scope.TipoCuentaBancaria = { Codigo: 403 }

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        //#region CONSULTAR

        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: CODIGO_UNO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinas = [];
                    response.data.Datos.forEach(function (item) {
                        if (item.Codigo != 0) {
                            $scope.ListadoOficinas.push(item);
                        }
                    });
                    $scope.Oficinas = response.data.Datos[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.OficinaCuenta = function (Cuenta) {

            $scope.CodigoCuentaTemp = Cuenta.Codigo;
            $scope.modalNombre = Cuenta.Nombre;
            $scope.NumeroCuenta = Cuenta.NumeroCuenta
            $scope.Oficinas = $scope.ListadoOficinas[0];
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CuentaBancaria: Cuenta,
            }
            blockUI.delay = 1000;
            CuentaBancariasFactory.ConsultarCuentaOficinas(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoGridOficinas = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoGridOficinas = response.data.Datos
                        }
                    }
                    $scope.ListadoGridOficinas.forEach(function (item) {
                        var ind = $scope.ListadoOficinas.indexOf({ Codigo: item.Codigo })
                        if (ind > -1) {
                            $scope.ListadoOficinas.splice(ind, 1)
                            $scope.Oficinas = $scope.ListadoOficinas[0];
                        }
                    });

                });
            showModal('modalOficinaCuenta');
        }
        //#region GUARDAR
        $scope.GuardarOficina = function () {
            $scope.listaOficinasGuardar = [];
            $scope.ListadoGridOficinas.forEach(function (itmOficinas) {
                parametros =
                    {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Oficina: { Codigo: itmOficinas.Oficina.Codigo },
                        CuentaBancaria: { Codigo: $scope.CodigoCuentaTemp },
                    };
                $scope.listaOficinasGuardar.push(parametros);
            })
            CuentaBancariasFactory.GuardarCuentaOficinas($scope.listaOficinasGuardar).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('Oficinas modificadas correctamente');
                            closeModal('modalOficinaCuenta')
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                });
        }
        $scope.AdicionarOficina = function () {
            $scope.ListadoTempOficina = [];
            $scope.OficinaAgregada = 0;
            $scope.ListadoTempOficina.Codigo = $scope.Oficinas.Codigo;
            $scope.ListadoTempOficina.Nombre = $scope.Oficinas.Nombre;
            $scope.ListadoGridOficinas.forEach(function (item) {
                if (item.Oficina.Codigo == $scope.ListadoTempOficina.Codigo) {
                    $scope.OficinaAgregada = 1;
                }
            });
            if ($scope.OficinaAgregada == 0) {
                $scope.ListadoGridOficinas.push({ Oficina: { Codigo: $scope.ListadoTempOficina.Codigo, Nombre: $scope.ListadoTempOficina.Nombre } });
            }
            else {
                ShowError("La oficina ya se encuentra adicionada");
            }
        }
        $scope.CerrarModalOficinaCuenta = function () {
            closeModal('modalOficinaCuenta');
        }
        //#region ELIMINAR
        $scope.EliminarOficina = function (indexOficina) {
            $scope.ListadoGridOficinas.splice(indexOficina, 1);
        }

        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarCuentasCambista';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoCuentaBancarias = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        NumeroCuenta: $scope.NumeroCuenta,
                        Nombre: $scope.Nombre,
                        Banco: { Nombre: $scope.NombreBanco },
                        TipoCuentaBancaria: $scope.TipoCuentaBancaria,
                        Codigo: $scope.Codigo,
                        Estado: $scope.ModalEstado.Codigo,
                        Pagina: $scope.paginaActual,
                        RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                    }
                    blockUI.delay = 1000;
                    CuentaBancariasFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    response.data.Datos.forEach(function (item) {
                                        var registro = item;
                                        $scope.ListadoCuentaBancarias.push(registro);
                                    });

                                    $scope.ListadoCuentaBancarias.forEach(function (item) {
                                        $scope.codigo = item.Codigo;
                                    });
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        /*------------------------------------------------------------------------------------Eliminar Cuenta Bancarias-----------------------------------------------------------------------*/
        $scope.EliminarCuentaBancarias = function (codigo, Nombre) {
            $scope.CodigoCuentaBancarias = codigo
            $scope.NombreCuentaBancarias = Nombre
            $scope.ModalErrorCompleto = ''
            $scope.ModalError = ''
            $scope.MostrarMensajeError = false
            showModal('modalEliminarCuentaBancarias');
        };

        $scope.Eliminar = function () {
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.CodigoCuentaBancarias,
            };

            CuentaBancariasFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó la cuenta bancaria ' + $scope.NombreCuentaBancarias);
                        closeModal('modalEliminarCuentaBancarias');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    var result = ''
                    result = InternalServerError(response.statusText)
                    showModal('modalMensajeEliminarCuentaBancarias');
                    $scope.ModalError = 'No se puede eliminar la cuenta bancaria ' + $scope.NombreCuentaBancarias + ' ya que se encuentra relacionada con ' + result;
                    $scope.ModalErrorCompleto = response.statusText

                });

        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /* Obtener parametros*/
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            $scope.Codigo = $routeParams.Codigo;
            Find();
        }

        $scope.CerrarModal = function () {
            closeModal('modalEliminarCuentaBancarias');
            closeModal('modalMensajeEliminarCuentaBancarias');
        }

        $scope.MaskNumero = function () {
            $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno)
        };

    }]);