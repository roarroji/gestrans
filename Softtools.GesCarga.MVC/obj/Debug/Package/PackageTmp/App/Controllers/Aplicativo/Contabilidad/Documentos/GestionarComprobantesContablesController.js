﻿SofttoolsApp.controller("GestionarComprobantesContablesCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'EncabezadoComprobantesContablesFactory', 'ValorCatalogosFactory', 'PlanUnicoCuentasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, EncabezadoComprobantesContablesFactory, ValorCatalogosFactory, PlanUnicoCuentasFactory) {

        // Se crea la propiedad modelo en el ambito
        $scope.MapaSitio = [{ Nombre: 'Contabilidad' }, { Nombre: 'Documentos' }, { Nombre: 'Comprobante Contable' }];
        $scope.Modelo = {
            Numero: 0
        };
        $scope.ListadoTipoDocumentoOrigen = []
        $scope.ListadoEstadosInterfaz = []
        $scope.ListadoComprobantes = [];

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try {
        $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_COMPROBANTE_CONTABLE);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar

        /* Obtener parametros*/
        if ($routeParams.Numero !== undefined) {
            $scope.Modelo.Numero = $routeParams.Numero;
            if ($scope.Numero > 0) {
                Obtener()
            }
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarComprobantesContables/' + $scope.Modelo.Numero;
        };
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        /*Cargar el combo de Tipo documento origen*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 26 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ Nombre: 'Seleccione tipo documento', Codigo: 0 })
                        $scope.ListadoTipoDocumentoOrigen = response.data.Datos
                        if ($scope.CodigoDocumentoOrigen !== undefined && $scope.CodigoDocumentoOrigen !== '' && $scope.CodigoDocumentoOrigen !== null) {
                            $scope.ModalEstadoInterfaz = $linq.Enumerable().From($scope.ListadoTipoDocumentoOrigen).First('$.Codigo ==' + $scope.CodigoDocumentoOrigen);
                        } else {
                            $scope.ModalEstadoInterfaz = $linq.Enumerable().From($scope.ListadoTipoDocumentoOrigen).First('$.Codigo == 0');
                        }
                    } else {
                        $scope.ListadoTipoDocumentoOrigen = []
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de estados interfaz*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 120 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoEstadosInterfaz = response.data.Datos
                        if ($scope.CodigoEstadoInterfaz !== undefined && $scope.CodigoEstadoInterfaz !== '' && $scope.CodigoEstadoInterfaz !== null) {
                            $scope.ModalEstadoInterfaz = $linq.Enumerable().From($scope.ListadoEstadosInterfaz).First('$.Codigo ==' + $scope.CodigoEstadoInterfaz);
                        }
                    } else {
                        $scope.ListadoEstadosInterfaz = []
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Numero > 0) {
            $scope.Titulo = 'CONSULTAR COMPROBANTE CONTABLE';
            Obtener();
        }
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function Obtener() {
            blockUI.start('Cargando comprobante número ' + $scope.Modelo.Numero);

            $timeout(function () {
                blockUI.message('Cargando comprobante número ' + $scope.Modelo.Numero);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
            };

            blockUI.delay = 1000;
            EncabezadoComprobantesContablesFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {

                        $scope.Modelo.Numero = response.data.Datos.Numero;
                        $scope.Modelo.Fecha = new Date(response.data.Datos.Fecha);
                        $scope.Modelo.NumeroDocumento = response.data.Datos.NumeroDocumento;

                        $scope.CodigoDocumentoOrigen = response.data.Datos.DocumentoOrigen.Codigo
                        if ($scope.ListadoTipoDocumentoOrigen.length > 0 && $scope.CodigoDocumentoOrigen > 0) {
                            $scope.ModalTipoDocumentoOrigen = $linq.Enumerable().From($scope.ListadoTipoDocumentoOrigen).First('$.Codigo == ' + response.data.Datos.DocumentoOrigen.Codigo);
                        }

                        $scope.CodigoEstadoInterfaz = response.data.Datos.EstadoInterfazContable.Codigo
                        if ($scope.ListadoEstadosInterfaz.length > 0 && $scope.CodigoEstadoInterfaz > 0) {
                            $scope.ModalEstadoInterfaz = $linq.Enumerable().From($scope.ListadoEstadosInterfaz).First('$.Codigo == ' + response.data.Datos.EstadoInterfazContable.Codigo);
                        }

                        $scope.ListadoComprobantes = response.data.Datos.DetalleCuentas;
                    }
                    else {
                        ShowError('No se logro consultar el Comprobante No.' + $scope.Modelo.Numero + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarComprobantesContables';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarComprobantesContables';
                });

            blockUI.stop();
        };


    }]);