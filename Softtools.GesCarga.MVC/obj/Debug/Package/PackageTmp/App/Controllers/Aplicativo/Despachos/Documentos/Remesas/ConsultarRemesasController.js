﻿SofttoolsApp.controller("ConsultarRemesasCtrl", ['$scope', '$timeout', 'ProductoTransportadosFactory', 'ZonasFactory', 'CiudadesFactory', 'UnidadEmpaqueFactory', 'TercerosFactory', 'RemesasFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'OficinasFactory', 'EmpresasFactory', 'DetalleDistribucionRemesasFactory', 'SitiosCargueDescargueFactory', 'SitiosTerceroClienteFactory', 'blockUIConfig',
    function ($scope, $timeout, ProductoTransportadosFactory, ZonasFactory, CiudadesFactory, UnidadEmpaqueFactory, TercerosFactory, RemesasFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, OficinasFactory, EmpresasFactory, DetalleDistribucionRemesasFactory, SitiosCargueDescargueFactory, SitiosTerceroClienteFactory, blockUIConfig) {

        $scope.MapaSitio = [{ Nombre: 'Despachos' }, { Nombre: 'Documentos' }, { Nombre: 'Remesas' }];
        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MensajesError = [];
        $scope.ListaDistribucion = [];
        $scope.MensajesErrorModal = [];
        $scope.ListadoTipoIdentificacion = [];
        $scope.MostrarMensajeError = false;
        $scope.BloquearZona = true;
        $scope.pref = '';
        $scope.numeroremesa = 0;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.ListadoOficinas = [];
        $scope.ListadoTipoRemesas = [];
        $scope.ListadoEstados = [];
        $scope.ListaProductoTransportados = [];
        $scope.ModeloTipoRemesa = [];
        $scope.ModeloOficina = [];
        $scope.SumaCantidades = 0;
        $scope.SumaPesos = 0;
        $scope.Distribucion = {
            Destinatario: {
                Codigo: 0
            },
            NumeroRemesa: 0,
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
        };
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_REMESAS);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;



        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        //Cargar combo de unidad de empaque
        UnidadEmpaqueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoUnidadEmpaque = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoUnidadEmpaque = response.data.Datos;
                        $scope.Distribucion.UnidadEmpaque = $scope.ListadoUnidadEmpaque[$scope.ListadoUnidadEmpaque.length];
                    } else {
                        $scope.ListadoUnidadEmpaque = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        //Cargar combo de estado distribucion
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 114 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoEstadosDistribucion = '';
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoEstadosDistribucion = response.data.Datos;
                        $scope.Distribucion.EstadoDistribucionRemesas = $linq.Enumerable().From($scope.ListadoEstadosDistribucion).First('$.Codigo ==' + ESTADO_DISTRIBUCION_REGISTRADA);
                    } else {
                        $scope.ListadoEstadosDistribucion = '';
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Cargar el combo de ciudad*/
        CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaCiudades = [];
                    if (response.data.Datos.length > 0) {
                        response.data.Datos.push({ NombreCompleto: '', Codigo: 0 });
                        $scope.ListaCiudades = response.data.Datos;
                        $scope.Distribucion.Destinatario.Ciudad = $scope.ListaCiudades[$scope.ListaCiudades.length];
                    } else {
                        $scope.ListaCiudades = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        $scope.ListadoSitiosCargueAlterno2 = []
        $scope.AutocompleteSitiosClienteCargue = function (value, cliente, ciudad) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                        Response.Datos[i].Nombre = Response.Datos[i].SitioCliente.Nombre
                    }
                    $scope.ListadoSitiosCargueAlterno2 = ValidarListadoAutocompleteAlterno(Response.Datos, $scope.ListadoSitiosCargueAlterno2)
                }
            }
            return $scope.ListadoSitiosCargueAlterno2
        }
        //funcion validar peso
        $scope.ValidarPeso = function (peso) {
            if (peso !== '' && peso !== '0' && peso !== null && peso !== undefined) {
                var pesokg = MascaraNumero(peso);
                if (pesokg > $scope.NumeroPesoPendiente) {
                    ShowWarning('', 'El peso ingresado supera el peso pendiente distribución');
                    $scope.Distribucion.Peso = ''
                }
            }
        };

        //Funciones validar identificacion destinatario-------------------------------------------------------
        $scope.VerificarDestinatario = function (identificacion) {
            $scope.ValidarIdentificacion = '';
            $scope.ValidarIdentificacion = identificacion;
            if ($scope.ValidarIdentificacion !== '') {
                if ($scope.ValidarIdentificacion === undefined || $scope.ValidarIdentificacion === null || $scope.ValidarIdentificacion === 0 || isNaN($scope.ValidarIdentificacion) === true) {
                    ShowInfo('El destinatario no se encuentra registrado, por favor diligenciar todos los campos.');
                    $scope.Distribucion.Destinatario.Codigo = 0;
                    $scope.Distribucion.Destinatario.NombreCompleto = '';
                    $scope.Distribucion.Destinatario.Ciudad = '';
                    $scope.Distribucion.ZonaDestinatario = '';
                    $scope.Distribucion.Destinatario.Barrio = '';
                    $scope.Distribucion.Destinatario.Direccion = '';
                    $scope.Distribucion.Destinatario.CodigoPostal = '';
                    $scope.Distribucion.Destinatario.Telefonos = '';
                    $scope.BloquearZona = true;
                } else {
                    TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, NumeroIdentificacion: $scope.ValidarIdentificacion }).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.ListadoZonasDestinatario = [];
                                if (response.data.Datos.length > 0) {
                                    if (response.data.Datos[0].Estado.Codigo === 1) {
                                        item = response.data.Datos[0];
                                        $scope.Distribucion.Destinatario.Codigo = item.Codigo;
                                        $scope.Distribucion.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + item.TipoIdentificacion.Codigo);
                                        $scope.Distribucion.Destinatario.NumeroIdentificacion = item.NumeroIdentificacion;
                                        $scope.Distribucion.Destinatario.NombreCompleto = item.NombreCompleto;
                                        $scope.Distribucion.Destinatario.Ciudad = item.Ciudad;
                                        $scope.CambiarZonasCiudad($scope.Distribucion.Destinatario.Ciudad);
                                        $scope.Distribucion.Destinatario.Barrio = item.Barrio;
                                        $scope.Distribucion.Destinatario.Direccion = item.Direccion;
                                        $scope.Distribucion.Destinatario.CodigoPostal = item.CodigoPostal;
                                        $scope.Distribucion.Destinatario.Telefonos = item.Telefonos;
                                        $scope.BloquearZona = false;
                                    } else {
                                        ShowInfo('El destinatario ingresado se encuentra inactivo en el sistema.');
                                        $scope.Distribucion.Destinatario.Codigo = 0;
                                        $scope.Distribucion.Destinatario.NumeroIdentificacion = '';
                                        $scope.Distribucion.Destinatario.NombreCompleto = '';
                                        $scope.Distribucion.Destinatario.Ciudad = '';
                                        $scope.Distribucion.ZonaDestinatario = '';
                                        $scope.Distribucion.Destinatario.Barrio = '';
                                        $scope.Distribucion.Destinatario.Direccion = '';
                                        $scope.Distribucion.Destinatario.CodigoPostal = '';
                                        $scope.Distribucion.Destinatario.Telefonos = '';
                                        $scope.BloquearZona = true;
                                    }

                                } else {
                                    ShowInfo('El destinatario no se encuentra registrado, por favor diligenciar todos los campos.');
                                    $scope.Distribucion.Destinatario.Codigo = 0;
                                    $scope.Distribucion.Destinatario.NombreCompleto = '';
                                    $scope.Distribucion.Destinatario.Ciudad = '';
                                    $scope.Distribucion.ZonaDestinatario = '';
                                    $scope.Distribucion.Destinatario.Barrio = '';
                                    $scope.Distribucion.Destinatario.Direccion = '';
                                    $scope.Distribucion.Destinatario.CodigoPostal = '';
                                    $scope.Distribucion.Destinatario.Telefonos = '';
                                    $scope.BloquearZona = true;
                                }
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
        }
        /*Se cambia la zona de la ciudad destinatario*/
        $scope.CambiarZonasCiudad = function (ciudad) {
            if (ciudad !== '') {
                if (ciudad.Codigo !== undefined) {
                    $scope.ObjetoCiudadRecoleccion = ciudad;
                    $scope.BloquearZona = false;
                    CambiarZonaCiudadDestinatario();
                } else {
                    $scope.Distribucion.Destinatario.Ciudad = '';
                    $scope.ListadoZonasDestinatario = [];
                    $scope.BloquearZona = true;
                }
            } else {
                $scope.Distribucion.Destinatario.Ciudad = '';
                $scope.ListadoZonasDestinatario = [];
                $scope.BloquearZona = true;
            }
        }
        /*Cargar combo lista zonas ciudades*/
        function CambiarZonaCiudadDestinatario() {
            ZonasFactory.Consultar({ Ciudad: $scope.ObjetoCiudadRecoleccion, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoZonasDestinatario = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoZonasDestinatario.push({ Nombre: '(NO APLICA)', Codigo: 0 });
                            response.data.Datos.forEach(function (item) {
                                if (item.Codigo !== 0) {
                                    $scope.ListadoZonasDestinatario.push(item);
                                }
                            });
                            $scope.Distribucion.ZonaDestinatario = $scope.ListadoZonasDestinatario[0];
                        } else {
                            $scope.ListadoZonasDestinatario.push({ Nombre: '(NO APLICA)', Codigo: 0 });
                            $scope.Distribucion.ZonaDestinatario = $scope.ListadoZonasDestinatario[0];
                        }
                    }
                }), function (response) {
                };
        };

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'BORRADOR', Codigo: CERO },
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'ANULADO', Codigo: 2 }
        ];

        $scope.ModeloEstado = $scope.ListadoEstados[CERO]

        $scope.ListadoOficinas = []
        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinas.push(item)
                        });
                        $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == -1');
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                $scope.ModeloOficina = $scope.ListadoOficinas[0]
            } else {
                ShowError('El usuario no tiene oficinas asociadas')
            }
        }

        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 91 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoRemesas = response.data.Datos;
                    $scope.ListadoTipoRemesas.push({ Codigo: 0, Nombre: '(TODOS)' });
                    $scope.ModeloTipoRemesa = $scope.ListadoTipoRemesas[2];
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });

        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };


        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarRemesa';
            }
        };
        $scope.CopiarDocumento = function (Codigo) {
            if ($scope.ValidarPermisos.AplicaActualizar === PERMISO_ACTIVO) {
                document.location.href = '#!GestionarRemesa/' + Numero.toString() + ',1';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                if (DatosRequeridosConsulta()) {
                    $scope.Codigo = 0;
                    Find();
                }
            }
        };

        function Find() {
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioCrea: $scope.Sesion.UsuarioAutenticado.Codigo,
                UsuarioModifica: $scope.Sesion.UsuarioAutenticado.Codigo,
                NumeroDocumento: $scope.ModeloNumero,
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS },
                FechaInicial: $scope.ModeloNumero > 0 ? undefined : $scope.FechaInicio,
                FechaFinal: $scope.ModeloNumero > 0 ? undefined : $scope.FechaFin,
                NumeroDocumentoOrdenServicio: $scope.ModeloOrdenServicio,
                Manifiesto: { NumeroDocumento: $scope.ModeloManifiesto },
                FiltroCliente: $scope.ModeloCliente,
                NumeroDocumentoCliente: $scope.ModeloDocumentoCliente,
                Vehiculo: { Placa: $scope.ModeloPlaca },
                FiltroConductor: $scope.ModeloConductor,
                FiltroTenedor: $scope.ModeloTenedor,
                TipoRemesa: { Codigo: $scope.ModeloTipoRemesa.Codigo },
                Oficina: { Codigo: $scope.ModeloOficina.Codigo },
                Estado: $scope.ModeloEstado.Codigo,
                NumeroDocumentoPlanillaDespacho: $scope.ModeloPlanilla,
                NumeroContenedor: $scope.ModalNumeroContenedor,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPaginas,
                UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas ? 0 : $scope.Sesion.UsuarioAutenticado.Codigo },
            };
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Buscando registros..."); }, 100);

            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoRemesas = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {
                    $scope.Pagina = $scope.paginaActual;
                    $scope.RegistrosPagina = 10;
                    RemesasFactory.Consultar(filtros).
                        then(function (response) {
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);

                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoRemesas = response.data.Datos
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                            ShowError(response.statusText);
                        });
                }
            }
        }
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
        }
        function DatosRequeridosConsulta() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.ModeloNumero === null || $scope.ModeloNumero === undefined || $scope.ModeloNumero === '' || $scope.ModeloNumero === 0 || isNaN($scope.ModeloNumero) === true)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false
            } else if (($scope.ModeloNumero !== null && $scope.ModeloNumero !== undefined && $scope.ModeloNumero !== '' && $scope.ModeloNumero !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')
            ) {
                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {
                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser menor a la fecha final');
                        continuar = false
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }
                else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio;
                    } else {
                        $scope.FechaInicio = $scope.FechaFin;
                    }
                }
            }
            return continuar;
        }

        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            if ($scope.DeshabilitarImprimir == false) {
                $scope.urlASP = '';
                $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
                $scope.NumeroDocumento = Numero;
                if ($scope.pref == 'ENCOE') {
                    $scope.NombreReporte = NOMBRE_REPORTE_GUIA_PAQUETERIA
                } else {
                    $scope.NombreReporte = NOMBRE_REPORTE_REME;
                }

                //Arma el filtro
                $scope.ArmarFiltro();

                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf === 1) {
                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
                }
                if (OpcionEXCEL === 1) {
                    window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL + '&USUA_Imprime=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&Prefijo=' + $scope.pref);
                }

                //Se limpia el filtro para poder hacer mas consultas
                $scope.FiltroArmado = '';
            }
        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroDocumento;
            }
        };

        /*--------------------------------------------------FUNCIONES ADICIONAR DISTRIBUCION REMESAS------------------------------------------------*/
        //funcion que muestra la modal distribucion 
        $scope.AbrirDistribucion = function (item) {
            if ($scope.DeshabilitarActualizar == false) {
                /*Cargar el combo de tipo identificaciones*/
                $scope.MensajesErrorModal = [];
                $scope.ListadoTipoIdentificacion = [];
                ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoTipoIdentificacion = response.data.Datos;
                                $scope.Distribucion.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + CODIGO_NO_APLICA_TIPO_IDENTIFICACION);
                            }
                            else {
                                $scope.ListadoTipoIdentificacion = [];
                            }
                        }
                    }, function (response) {
                    });

                $scope.NumeroDocumentoRemesa = item.NumeroDocumento;
                $scope.Distribucion.NumeroRemesa = item.Numero;
                $scope.ResultadoSinRegistrosDistribucion = '';

                //funcion que consulta grid lista distribución remesas
                $scope.ListaDistribucion = [];
                var filtroDistribucion = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroRemesa: $scope.Distribucion.NumeroRemesa
                };
                DetalleDistribucionRemesasFactory.Consultar(filtroDistribucion).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.SumaCantidades = 0;
                            $scope.SumaPesos = 0;
                            if (response.data.Datos.length > 0) {
                                $scope.ListaDistribucion = response.data.Datos;
                                $scope.ListaDistribucion.forEach(function (item) {
                                    if (item.UsuarioModifica.Codigo !== 0) {
                                        item.DistribucionRecidida = true;
                                    }
                                    $scope.SumaCantidades = $scope.SumaCantidades + item.Cantidad;
                                    $scope.SumaPesos = $scope.SumaPesos + item.Peso;
                                });
                                CalcularPorDistribuir();
                            } else {
                                $scope.ResultadoSinRegistrosDistribucion = 'No hay distribuciones';
                            }
                            CalcularPorDistribuir();
                        }
                    }, function (response) {
                    });
                showModal('modalDistribuciones');
                $('#NuevaDistribucion').hide(500);
                $('#BotonNuevo').show(500);
                $scope.CargarDatosRemesa(1);
            }
        };

        $scope.CargarTipoIdentificacion = function () {
            $scope.Distribucion.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + CODIGO_NO_APLICA_TIPO_IDENTIFICACION);
        };

        //funcion que consulta detalles de la remesa
        $scope.CargarDatosRemesa = function (valor) {
            $scope.Distribucion.FechaDocumentoCliente = new Date();
            $scope.Valor = valor;
            /*Cargar autocomplete de productos transportados*/
            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, AplicaTarifario: -1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaProductoTransportados = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListaProductoTransportados = response.data.Datos;
                            if ($scope.CodigoProductoTransportados !== undefined && $scope.CodigoProductoTransportados !== '' && $scope.CodigoProductoTransportados !== 0 && $scope.CodigoProductoTransportados !== null) {
                                $scope.Distribucion.Producto = $linq.Enumerable().From($scope.ListaProductoTransportados).First('$.Codigo ==' + $scope.CodigoProductoTransportados);
                            } else {
                                $scope.Distribucion.Producto = $scope.ListaProductoTransportados[$scope.ListaProductoTransportados.length];
                            }
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            //Filtros enviados
            var remesafiltros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Distribucion.NumeroRemesa,
                Obtener: 1
            };
            RemesasFactory.Obtener(remesafiltros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.CodigoProductoTransportados = response.data.Datos.ProductoTransportado.Codigo;
                        $scope.Distribucion.Destinatario.Codigo = response.data.Datos.Destinatario.Codigo;
                        $scope.Distribucion.Destinatario.NumeroIdentificacion = response.data.Datos.Destinatario.NumeroIdentificacion;
                        $scope.Distribucion.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.Destinatario.TipoIdentificacion.Codigo);
                        $scope.Distribucion.Destinatario.NombreCompleto = response.data.Datos.Destinatario.NombreCompleto;
                        $scope.Distribucion.Destinatario.Ciudad = $linq.Enumerable().From($scope.ListaCiudades).First('$.Codigo ==' + response.data.Datos.Destinatario.Ciudad.Codigo);
                        $scope.CambiarZonasCiudad(response.data.Datos.Destinatario.Ciudad);
                        $scope.Distribucion.Destinatario.Direccion = response.data.Datos.DireccionDestinatario;
                        $scope.Distribucion.Destinatario.Telefonos = response.data.Datos.TelefonoDestinatario;
                        if ($scope.ListaProductoTransportados.length > 0 && $scope.CodigoProductoTransportados > 0) {
                            $scope.Distribucion.Producto = $linq.Enumerable().From($scope.ListaProductoTransportados).First('$.Codigo == ' + $scope.CodigoProductoTransportados);
                        }
                        if ($scope.Valor === 1) {
                            $scope.NombreProductoTransportado = response.data.Datos.ProductoTransportado.Nombre;
                            $scope.NumeroCantidad = response.data.Datos.CantidadCliente;
                            $scope.NumeroPeso = response.data.Datos.PesoCliente;
                        } else {
                            $scope.Distribucion.UnidadEmpaque = $linq.Enumerable().From($scope.ListadoUnidadEmpaque).First('$.Codigo == ' + $scope.Distribucion.Producto.UnidadEmpaque);
                            if ($scope.NumeroCantidadPendiente === 0 && $scope.NumeroPesoPendiente === 0) {
                                ShowError('Ya se encuentra totalmente distribuida la remesa');
                            } else {
                                $('#NuevaDistribucion').show(500);
                                $('#BotonNuevo').hide(500);
                            }
                        }
                        CalcularPorDistribuir();
                    } else {
                        ShowError(response.statusText);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        //funcion que inserta distribucion remesa
        $scope.InsertarDistribucion = function () {
            if (DatosRequeridos()) {
                $scope.ResultadoSinRegistrosDistribucion = 'No hay distribuciones';
                $scope.Distribucion.FechaDocumentoCliente = RetornarFechaEspecificaSinTimeZone($scope.Distribucion.FechaDocumentoCliente);
                DetalleDistribucionRemesasFactory.Guardar($scope.Distribucion).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('Distribución adicionada satisfactoriamente');
                            Find();
                            //funcion que consulta nueva novedad
                            $scope.ListaDistribucion = [];
                            var filtroDistribucion = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                NumeroRemesa: $scope.Distribucion.NumeroRemesa
                            };
                            DetalleDistribucionRemesasFactory.Consultar(filtroDistribucion).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso === true) {
                                        $scope.SumaCantidades = 0;
                                        $scope.SumaPesos = 0;
                                        if (response.data.Datos.length > 0) {
                                            $scope.ListaDistribucion = response.data.Datos;
                                            $scope.ListaDistribucion.forEach(function (item) {
                                                $scope.SumaCantidades = $scope.SumaCantidades + item.Cantidad;
                                                $scope.SumaPesos = $scope.SumaPesos + item.Peso;
                                            });
                                            CalcularPorDistribuir();
                                        } else {
                                            $scope.NumeroCantidadPendiente = $scope.NumeroCantidad;
                                            $scope.NumeroPesoPendiente = $scope.NumeroPeso;
                                            $scope.Distribucion.Cantidad = MascaraValores($scope.NumeroCantidad);
                                            $scope.Distribucion.Peso = MascaraValores($scope.NumeroPeso);
                                            $scope.ResultadoSinRegistrosDistribucion = 'No hay distribuciones';
                                        }
                                        CalcularPorDistribuir();
                                    }
                                }, function (response) {
                                });
                            showModal('modalDistribuciones');
                            $('#NuevaDistribucion').hide(500); $('#BotonNuevo').show(500);
                        }
                    }, function (response) {
                    });
            }
        }

        function DatosRequeridos() {
            $scope.MensajesErrorModal = [];
            var continuar = true;

            if ($scope.Distribucion.Producto === undefined || $scope.Distribucion.Producto === '' || $scope.Distribucion.Producto === null || $scope.Distribucion.Producto.Codigo === 0) {
                $scope.MensajesErrorModal.push('Debe ingresar el nombre del producto transportado');
                continuar = false;
            }
            if ($scope.Distribucion.UnidadEmpaque === undefined || $scope.Distribucion.UnidadEmpaque === '' || $scope.Distribucion.UnidadEmpaque === null || $scope.Distribucion.UnidadEmpaque.Codigo === 0) {
                $scope.MensajesErrorModal.push('Debe ingresar la unidad de empaque');
                continuar = false;
            }
            if ($scope.Distribucion.Cantidad === undefined || $scope.Distribucion.Cantidad === '' || $scope.Distribucion.Cantidad === null || $scope.Distribucion.Cantidad.Codigo === 0) {
                $scope.MensajesErrorModal.push('Debe ingresar la cantidad');
                continuar = false;
            }
            $scope.SumaCantidades = 0;
            $scope.ListaDistribucion.forEach(function (item) {
                $scope.SumaCantidades = $scope.SumaCantidades + item.Cantidad;
            });
            $scope.SumaCantidades = parseInt($scope.SumaCantidades) + parseInt($scope.Distribucion.Cantidad);
            if ($scope.SumaCantidades > $scope.NumeroCantidad) {
                $scope.MensajesErrorModal.push('La cantidad ingresada, supera la cantidad de la remesa');
                continuar = false;
            }
            if ($scope.Distribucion.Peso === undefined || $scope.Distribucion.Peso === '' || $scope.Distribucion.Peso === null || $scope.Distribucion.Peso.Codigo === 0) {
                $scope.MensajesErrorModal.push('Debe ingresar el peso');
                continuar = false;
            }
            if ($scope.Distribucion.Destinatario.TipoIdentificacion === undefined || $scope.Distribucion.Destinatario.TipoIdentificacion === '' || $scope.Distribucion.Destinatario.TipoIdentificacion === null || $scope.Distribucion.Destinatario.TipoIdentificacion.Codigo === 100) {
                $scope.MensajesErrorModal.push('Debe ingresar el tipo de identificación del destinatario');
                continuar = false;
            }
            if ($scope.Distribucion.Destinatario.NumeroIdentificacion === undefined || $scope.Distribucion.Destinatario.NumeroIdentificacion === '' || $scope.Distribucion.Destinatario.NumeroIdentificacion === null || $scope.Distribucion.Destinatario.NumeroIdentificacion === 0) {
                $scope.MensajesErrorModal.push('Debe ingresar el número de identificación del destinatario');
                continuar = false;
            }
            if ($scope.Distribucion.Destinatario.NombreCompleto === undefined || $scope.Distribucion.Destinatario.NombreCompleto === '' || $scope.Distribucion.Destinatario.NombreCompleto === null) {
                $scope.MensajesErrorModal.push('Debe ingresar el nombre del destinatario');
                continuar = false;
            }
            if ($scope.Distribucion.Destinatario.Ciudad === undefined || $scope.Distribucion.Destinatario.Ciudad === '' || $scope.Distribucion.Destinatario.Ciudad === null || $scope.Distribucion.Destinatario.Ciudad.Codigo === 0) {
                $scope.MensajesErrorModal.push('Debe ingresar la ciudad del destinatario');
                continuar = false;
            }
            if ($scope.Distribucion.Destinatario.Direccion === undefined || $scope.Distribucion.Destinatario.Direccion === '' || $scope.Distribucion.Destinatario.Direccion === null) {
                $scope.MensajesErrorModal.push('Debe ingresar la dirección del destinatario');
                continuar = false;
            }
            return continuar;
        }

        //funcion eliminar distribución remesa
        $scope.EliminarNovedadesDespachos = function (codigo, ciudad) {
            $scope.CodigoDistribucionRemesa = codigo;
            $scope.NombreCiudadDistribucionRemesa = ciudad;
            $scope.ModalErrorCompleto = '';
            $scope.ModalError = '';
            $scope.MostrarMensajeError = false;
            showModal('modalEliminarDistribucionRemesas');
        };

        $scope.Eliminar = function () {
            $scope.ResultadoSinRegistrosDistribucion = '';
            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: $scope.Distribucion.NumeroRemesa,
                Codigo: $scope.CodigoDistribucionRemesa
            };

            DetalleDistribucionRemesasFactory.Anular(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se eliminó la distribución para la ciudad ' + $scope.NombreCiudadDistribucionRemesa);
                        closeModal('modalEliminarDistribucionRemesas', 1);
                        Find();
                        //funcion que consulta nueva novedad
                        $scope.ListaDistribucion = [];
                        var filtroDistribucion = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            NumeroRemesa: $scope.Distribucion.NumeroRemesa
                        };
                        DetalleDistribucionRemesasFactory.Consultar(filtroDistribucion).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    $scope.SumaCantidades = 0;
                                    $scope.SumaPesos = 0;
                                    if (response.data.Datos.length > 0) {
                                        $scope.ListaDistribucion = response.data.Datos;
                                        $scope.ListaDistribucion.forEach(function (item) {
                                            $scope.SumaCantidades = $scope.SumaCantidades + item.Cantidad;
                                            $scope.SumaPesos = $scope.SumaPesos + item.Peso;
                                        });
                                        CalcularPorDistribuir();
                                    } else {
                                        $scope.NumeroCantidadPendiente = $scope.NumeroCantidad;
                                        $scope.NumeroPesoPendiente = $scope.NumeroPeso;
                                        $scope.Distribucion.Cantidad = MascaraValores($scope.NumeroCantidad);
                                        $scope.Distribucion.Peso = MascaraValores($scope.NumeroPeso);
                                        $scope.ResultadoSinRegistrosDistribucion = 'No hay distribuciones';
                                    }
                                    CalcularPorDistribuir();
                                }
                            }, function (response) {
                            });
                        showModal('modalDistribuciones');
                        $('#NuevaDistribucion').hide(500); $('#BotonNuevo').show(500);
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                });
        };

        //----------------------------------------------------------------------
        $scope.CerrarModal = function () {
            closeModal('modalEliminarDistribucionRemesas', 1);
        };
        $scope.MaskTelefono = function (option) {
            try { $scope.Distribucion.Destinatario.Telefonos = MascaraTelefono($scope.Distribucion.Destinatario.Telefonos) } catch (e) { }
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };

        function DatosRequeridosAnular() {
            var continuar = true
            $scope.MensajesErrorAnular = []

            if ($scope.ModeloCausaAnula == undefined || $scope.ModeloCausaAnula == null || $scope.ModeloCausaAnula == "") {
                continuar = false
                $scope.MensajesErrorAnular.push('debe ingresar la causa de anulación')
            }
            return continuar
        }

        $scope.AnularDocumento = function (Numero, NumeroDocumento) {
            if ($scope.DeshabilitarEliminarAnular == false) {
                $scope.Numero = Numero
                $scope.ModalErrorCompleto = ''
                $scope.NumeroDocumento = NumeroDocumento
                $scope.ModalError = ''
                $scope.MostrarMensajeError = false
                showModal('modalAnular');
            }
        };

        $scope.Anular = function () {
            var aplicaplanilla = false
            var aplicaFactura = false
            var aplicaManifiesto = false

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioAnula: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                CausaAnula: $scope.ModeloCausaAnula,
                Numero: $scope.Numero,

            };
            if (DatosRequeridosAnular()) {
                RemesasFactory.Anular(entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado > 0) {
                                ShowSuccess('Se anuló la remesa ' + $scope.NumeroDocumento);
                                closeModal('modalAnular', 1);
                                Find();
                            } else {
                                var mensaje = 'No se pudo anular la remesa ya que se encuentra relacionado con los siguientes documentos: '

                                var Facturas = '\nFacturas: '
                                var Planillas = '\nPlanillas: '
                                var Manifiestos = '\nManifiestos: '
                                for (var i = 0; i < response.data.Datos.DocumentosRelacionados.length; i++) {
                                    if (response.data.Datos.DocumentosRelacionados[i].FacturaVentas.Numero > 0) {
                                        Facturas += response.data.Datos.DocumentosRelacionados[i].FacturaVentas.Numero + ','
                                        aplicaFactura = true
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Planilla.Numero > 0) {
                                        Planillas += response.data.Datos.DocumentosRelacionados[i].Planilla.Numero + ','
                                        aplicaplanilla = true
                                    }
                                    if (response.data.Datos.DocumentosRelacionados[i].Manifiesto.Numero > 0) {
                                        Manifiestos += response.data.Datos.DocumentosRelacionados[i].Manifiesto.Numero + ','
                                        aplicaManifiesto = true
                                    }
                                }

                                if (aplicaplanilla) {
                                    mensaje += Planillas
                                }
                                if (aplicaFactura) {
                                    mensaje += Facturas
                                }
                                if (aplicaManifiesto) {
                                    mensaje += Manifiestos
                                }
                                ShowError(mensaje)
                            }

                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }, function (response) {
                        var result = ''
                        showModal('modalMensajeAnularOrdenCargue');
                        result = InternalServerError(response.statusText)
                        $scope.ModalError = 'No se puede anular la orden de servicio ' + $scope.NumeroDocumento;
                        $scope.ModalErrorCompleto = response.statusText
                    });
            }
        };

   
       

        $scope.AbrirEstadosDistribucion = function (item) {
            /*Cargar el combo de tipo identificaciones*/
            $scope.MensajesErrorModal = []
            $scope.itemRemesaDistribucion = item
            $scope.NumeroDocumentoRemesa = item.NumeroDocumento;
            $scope.NombreProductoTransportado = item.ProductoTransportado.Nombre;
            $scope.NumeroCantidad = item.CantidadCliente;
            $scope.NumeroPeso = item.PesoCliente;
            $scope.Distribucion.NumeroRemesa = item.Numero;
            $scope.ResultadoSinRegistrosDistribucion = '';
            console.log('estados distribuccion', item)


            //funcion que consulta grid lista distribución remesas
            $scope.ListaDistribucion = [];
            var filtroDistribucion = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: $scope.Distribucion.NumeroRemesa,
            };
            DetalleDistribucionRemesasFactory.Consultar(filtroDistribucion).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                if (item.Observaciones !== '') {
                                    var str = item.Observaciones;
                                    item.Observaciones = str.substring(0, 10);
                                    item.ObservacionCompleta = str.substring(0, 250);
                                    item.OcultarVerMas = true;
                                } else {
                                    item.OcultarVerMas = false;
                                }
                                $scope.ListaDistribucion.push(item);
                            });
                        } else {
                            $scope.DeshabilitarBotonNuevaDistribucion = true;
                            $scope.ResultadoSinRegistrosDistribucion = 'No hay distribuciones';
                        }
                    }
                }, function (response) {
                });
            showModal('modalEstadosDistribuciones');
            console.log('listado', $scope.ListaDistribucion)
        };

      

        $scope.ConsultarControlEntregas = function (item) {
            console.log('llego item', item)
            BloqueoPantalla.start('Buscando registros ...');
            $timeout(function () {
                BloqueoPantalla.message('Espere por favor ...');
            }, 100);
       
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroRemesa: item.NumeroRemesa,
                Codigo: item.Codigo
            };
            $scope.Recibe = [];
            

            $scope.ListadoTipoIdentificacion = [];
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {

                            $scope.ListadoTipoIdentificacion = response.data.Datos;
                            $scope.Distribucion.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + CODIGO_NO_APLICA_TIPO_IDENTIFICACION);
                        }
                        else {
                            $scope.ListadoTipoIdentificacion = [];
                        }
                    }
                }, function (response) {
                });


            DetalleDistribucionRemesasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        console.log('resultado entrega', response.data)
                        $scope.DeshabilitarDatosRecibe = true

                        $scope.Destinatario = response.data.Datos.Destinatario

                        $scope.Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.Destinatario.TipoIdentificacion.Codigo); 
                        $scope.Recibe.Cantidad = response.data.Datos.CantidadRecibe
                        $scope.Recibe.Peso = response.data.Datos.PesoRecibe 
                        $scope.Recibe.TipoIdentificacionRecibe = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.CodigoTipoIdentificacionRecibe);

                        $scope.Recibe.NumeroIdentificacion = response.data.Datos.NumeroIdentificacionRecibe
                        $scope.Recibe.NombreCompleto = response.data.Datos.NombreRecibe
                        $scope.Recibe.Telefonos = response.data.Datos.TelefonoRecibe
                        $scope.Recibe.Observaciones = response.data.Datos.ObservacionesRecibe         
                        
                        if (response.data.Datos.Fotografia.FotoBits !== undefined && response.data.Datos.Fotografia.FotoBits !== null && response.data.Datos.Fotografia.FotoBits !== '') {
                            $scope.Foto = [];
                            $scope.Foto.push({
                                NombreFoto: response.data.Datos.Fotografia.NombreFoto,
                                TipoFoto: response.data.Datos.Fotografia.TipoFoto,
                                ExtensionFoto: response.data.Datos.Fotografia.ExtensionFoto,
                                FotoBits: response.data.Datos.Fotografia.FotoBits,
                                ValorDocumento: 1,
                                FotoCargada: 'data:' + response.data.Datos.Fotografia.TipoFoto + ';base64,' + response.data.Datos.Fotografia.FotoBits
                            });
                        } else {
                            $scope.Foto = [];
                        }
                        if ($scope.Foto.length > 0) {
                            $scope.FotoAgregada = 1
                        } else {
                            $scope.FotoAgregada = 0
                        }

                        $scope.Recibe.FirmaImg = "data:image/png;base64," + response.data.Datos.Firma;

                    }
                    else {
                        ShowError('No se logró consultar la distribución ' + response.data.MensajeOperacion); 
                        closeModal('modalControlEntregas', 1);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    showModal('modalControlEntregas');
                });
            BloqueoPantalla.stop();

            showModal('modalControlEntregas');
           

            
        };


        function CalcularPorDistribuir() {
            try {
                if ($scope.SumaCantidades > 0) {
                    $scope.NumeroCantidadPendiente = $scope.NumeroCantidad - $scope.SumaCantidades;
                    $scope.Distribucion.Cantidad = MascaraValores($scope.NumeroCantidad - $scope.SumaCantidades);
                } else {
                    $scope.NumeroCantidadPendiente = $scope.NumeroCantidad;
                    $scope.Distribucion.Cantidad = MascaraValores($scope.NumeroCantidad);
                }
                if ($scope.SumaPesos > 0) {
                    $scope.NumeroPesoPendiente = $scope.NumeroPeso - $scope.SumaPesos;
                    $scope.Distribucion.Peso = MascaraValores($scope.NumeroPeso - $scope.SumaPesos);
                } else {
                    $scope.NumeroPesoPendiente = $scope.NumeroPeso;
                    $scope.Distribucion.Peso = MascaraValores($scope.NumeroPeso);
                }
            } catch (e) {
                try {
                    $scope.NumeroCantidadPendiente = $scope.NumeroCantidad;
                    $scope.NumeroPesoPendiente = $scope.NumeroPeso;
                } catch (e) {
                }
            }
        }

        $scope.LimpiarFirmaRecibe = function () {
            try {
                var canvas = document.getElementById('FirmaRecibe');
                var context = canvas.getContext('2d')
                context.clearRect(0, 0, canvas.width, canvas.height);
            } catch (e) {

            }

        }

        if ($routeParams.Numero !== undefined && $routeParams.Numero !== null && $routeParams.Numero !== '' && $routeParams.Numero !== 0) {
            if ($routeParams.Numero > 0) {
                $scope.ModeloNumero = $routeParams.Numero;
                Find();
            }
        }

    }]);