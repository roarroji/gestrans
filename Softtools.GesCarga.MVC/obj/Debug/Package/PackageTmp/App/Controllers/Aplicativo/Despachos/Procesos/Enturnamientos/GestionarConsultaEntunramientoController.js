﻿SofttoolsApp.controller("GestionarConsultaEntunramientoCtrl", ['$scope', '$routeParams', '$timeout', 'CiudadesFactory', '$linq', 'blockUI', 'OficinasFactory',
    'PaisesFactory', 'ZonasFactory', 'DepartamentosFactory', 'EnturnamientoFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'blockUIConfig', 'VehiculosFactory',
    function ($scope, $routeParams, $timeout, CiudadesFactory, $linq, blockUI, OficinasFactory,
        PaisesFactory, ZonasFactory, DepartamentosFactory, EnturnamientoFactory, ValorCatalogosFactory, TercerosFactory, blockUIConfig, VehiculosFactory) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = "ENTURNAMIENTOS";
        $scope.MapaSitio = [{ Nombre: 'Despacho' }, { Nombre: 'Procesos' }, { Nombre: 'Enturnamiento' }];
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + 400150);
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.ListadoEstados = [
            { Nombre: '(TODAS)', Codigo: -1 },
            { Nombre: 'Activa', Codigo: 1 },
            { Nombre: 'Inactiva', Codigo: 0 }
        ];
        $scope.ModalEstado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        $scope.ListadoEstadosZonas = [
            { Nombre: 'ACTIVA', Codigo: 1 },
            { Nombre: 'INACTIVA', Codigo: 0 }
        ];
        $scope.ModalEstadoZona = $linq.Enumerable().From($scope.ListadoEstadosZonas).First('$.Codigo == 1');

        $scope.ListadoPermisosEspecificos = []
        $scope.ListadoPermisos.forEach(function (item) {
            if (item.Padre == 400150) {
                $scope.ListadoPermisosEspecificos.push(item)
            }
        });
        $scope.PermisoVisualizarOficinas = false

        $scope.ListadoPermisosEspecificos.forEach(function (item) {
            if (item.Codigo == PERMISO_VISUALIZAR_OFICINAS_ENTURNAMIENTO) {
                $scope.PermisoVisualizarOficinas = true
            }
        });

        $scope.Modelo = {
            Vehiculo: '',
            Conductor: '',
            Tenedor: '',
            TipoVehiculo: '',
            CiudadEnturna: '',
            OficinaEnturna: '',
            RegionPaisDestino: '',
            CiudadDestino1: '',
            RegionPaisDestino2: '',
            CiudadDestino2: '',
            FechaDisponible: '',
            Ubicacion: '',
            Observaciones: ''
        };
        $scope.ListaRegiones = [];
        $scope.ListadoOficinas = [];
        $scope.ListaPlaca = [];
        $scope.ListadoTenedores = [];
        $scope.ListadoConductores = [];
        $scope.ListadoCiudades = [];
        //--------------------------Variables-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----Paginacion
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };
        //----Paginacion

        //$scope.ConsultarOficinasEnturne = function (Ciudad) {
        //    if (Ciudad != undefined) {
        //        if (Ciudad.Codigo > 0) {
        //            $scope.ListadoOficinas = [];
        //            filtros = {
        //                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
        //                Codigo: Ciudad.Codigo,
        //            }
        //            blockUI.delay = 1000;
        //            CiudadesFactory.ConsultarOficinas(filtros).
        //                then(function (response) {
        //                    if (response.data.ProcesoExitoso === true) {
        //                        if (response.data.Datos.length > 0) {
        //                            $scope.ListadoOficinas = response.data.Datos;
        //                            //$scope.Modelo.OficinaEnturna = $scope.ListadoOficinas[0]
        //                        } else {
        //                            ShowError('La ciudad ingresada no tiene oficinas asociadas')
        //                            //$scope.Modelo.CiudadEnturna = ''
        //                            $scope.ListadoOficinas = [];
        //                        }
        //                    } else {
        //                        ShowError('La ciudad ingresada no tiene oficinas asociadas')
        //                        //$scope.Modelo.CiudadEnturna = ''
        //                        $scope.ListadoOficinas = [];
        //                    }
        //                });
        //        }
        //    }
        //}
        //-----AutoComplete
        //--Vehiculos
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 2) == 0 || value.length == 1) {
                    $scope.ListaPlaca = [];
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,                      
                        Sync: true
                    });
                    $scope.ListaPlaca = $linq.Enumerable().From(Response.Datos).Where('$.Estado.Codigo == ' + ESTADO_ACTIVO).ToArray();
                    $scope.ListaPlaca = ValidarListadoAutocomplete($scope.ListaPlaca, $scope.ListaPlaca);
                }
            }
            return $scope.ListaPlaca;
        };
        //--Vehiculos
        //--Tenedor
        $scope.AutocompleteTenedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    $scope.ListadoTenedores = [];
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_TENEDOR,
                        ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoTenedores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTenedores);
                }
            }
            return $scope.ListadoTenedores;
        };
        //--Tenedor
        //--Conductores
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    $scope.ListadoConductores = [];
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores);
                }
            }
            return $scope.ListadoConductores;
        };
        //--Conductores
        //--Ciudades
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    $scope.ListadoCiudades = [];
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true, CiudadOficinas: 1
                    });
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades);
                }
            }
            return $scope.ListadoCiudades;
        };
        //--Ciudades
        //-----AutoComplete
        //----------Init
        $scope.InitLoad = function () {
            //--Oficinas
            if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
                OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                    then(function (response) {
                        $scope.ListadoOficinas.push({ Nombre: '(TODAS)', Codigo: -1 })
                        if (response.data.ProcesoExitoso === true) {
                            response.data.Datos.forEach(function (item) {
                                $scope.ListadoOficinas.push(item)
                            });

                            $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo ==-1');
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                    $scope.ListadoOficinas = $scope.Sesion.UsuarioAutenticado.ListadoOficinas
                    $scope.ModeloOficina = $scope.ListadoOficinas[0]
                } else {
                    ShowError('El usuario no tiene oficinas asociadas')
                }
            }
            //--Oficinas
            //--Tipo Vehiculo
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_VEHICULO }, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoVehiculo = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoVehiculo = response.data.Datos;
                            $scope.ModeloTipoVehiculo = $scope.ListadoTipoVehiculo[0];
                        }
                        else {
                            $scope.ListadoTipoVehiculo = [];
                        }
                    }
                }, function (response) {
                });
            //--Tipo Vehiculo
            //--Tipo Causa Anulacion Enturne
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 191 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCausas = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCausas = response.data.Datos;
                        }
                        else {
                            $scope.ListadoCausas = []
                        }
                    }
                }, function (response) {
                });
            //--Tipo Causa Anulacion Enturne
            //--Regiones Paises
            OficinasFactory.ConsultarRegionesPaises({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.splice(response.data.Datos.findIndex(item => item.Codigo === 0), 1);
                            $scope.ListaRegiones.push({ Nombre: 'SELECCIONE REGIÓN', Codigo: 0 });
                            response.data.Datos.forEach(function (item) {
                                $scope.ListaRegiones.push(item);
                            });
                            $scope.Modelo.RegionPaisDestino = $scope.ListaRegiones[0];
                            $scope.Modelo.RegionPaisDestino2 = $scope.ListaRegiones[0];
                        }
                        else {
                            $scope.ListaRegiones = [];
                        }
                    }
                }, function (response) {
                });
            //--Regiones Paises
        };
        //----------Init
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoEnturnes = [];
            $scope.ListadoSolicitudes = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === 0) {
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CiudadEnturna: $scope.ModeloCiudadEnturna,
                        OficinaEnturna: $scope.ModeloOficina,
                        Conductor: $scope.ModeloConductor,
                        Vehiculo: $scope.ModeloVehiculo,
                        TipoVehiculo: $scope.ModeloTipoVehiculo
                    }
                    blockUI.delay = 1000;
                    EnturnamientoFactory.Consultar(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    $scope.ListadoEnturnes = response.data.Datos
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        //--Nuevo Turno
        $scope.NuevoEnturnamiento = function () {
            var fecha = new Date(new Date().setSeconds(3600));
            showModal('ModalEnturnamiento');
            $scope.Modelo = {
                Vehiculo: '',
                Conductor: '',
                Tenedor: '',
                TipoVehiculo: '',
                OficinaEnturna: '',
                CiudadEnturna: '',
                CiudadDestino1: '',
                CiudadDestino2: '',
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                FechaDisponible: FormatoFechaISO8601(fecha),
                Observaciones: ''
            };
            $scope.MensajesError2 = [];
            $scope.Modelo.RegionPaisDestino = $scope.ListaRegiones[0];
            $scope.Modelo.RegionPaisDestino2 = $scope.ListaRegiones[0];
            $scope.ListaRegionCiudades = [];
            $scope.ListaRegionCiudades2 = [];
            $scope.ListaOficinasEnturne = [];

            function localizacion(posicion) {
                $scope.Modelo.Latitud = posicion.coords.latitude;
                $scope.Modelo.Longitud = posicion.coords.longitude;
            }

            function errores(error) {
                MensajeInfo("Info", "Ha ocurrido un error al intentar obtener la información de las cordenadas GPS. Error:" + error, false);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(localizacion, errores);
            } else {
                MensajeInfo("Info", "Tu dispositivo no soporta o no tiene habilitada la 'Geolocalización'", false);
            }
        };
        //--Nuevo Turno
        $scope.ConsultarOficinas = function (Ciudad) {
            if (Ciudad != undefined) {
                if (Ciudad.Codigo > 0) {
                    $scope.ListaOficinasEnturne = [];
                    filtros = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: Ciudad.Codigo,
                    }
                    blockUI.delay = 1000;
                    CiudadesFactory.ConsultarOficinas(filtros).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > 0) {
                                    //$scope.ListaOficinasEnturne = response.data.Datos;
                                    //$scope.Modelo.OficinaEnturna = $scope.ListaOficinasEnturne[0];
                                    for (var i = 0; i < response.data.Datos.length; i++) {
                                        if (response.data.Datos[i].AplicaEnturnamiento == CODIGO_UNO) {
                                            $scope.ListaOficinasEnturne.push(response.data.Datos[i]);
                                        }
                                    }
                                    if ($scope.ListaOficinasEnturne.length > 0) {
                                        $scope.Modelo.OficinaEnturna = $scope.ListaOficinasEnturne[0];
                                    }
                                    else {
                                        ShowError('La ciudad ingresada no tiene oficinas que aplican enturnamiento')
                                        $scope.Modelo.CiudadEnturna = ''
                                        $scope.ListaOficinasEnturne = [];
                                    }
                                }
                                else {
                                    ShowError('La ciudad ingresada no tiene oficinas asociadas')
                                    $scope.Modelo.CiudadEnturna = ''
                                    $scope.ListaOficinasEnturne = [];
                                }
                            }
                            else {
                                ShowError('La ciudad ingresada no tiene oficinas asociadas')
                                $scope.Modelo.CiudadEnturna = ''
                                $scope.ListaOficinasEnturne = [];
                            }
                        });
                }
            }
        }
        $scope.FiltrarCiudades = function () {
            if ($scope.Modelo.RegionPaisDestino.Codigo > 0) {
                $scope.ListaRegionCiudades = [];
                blockUIConfig.autoBlock = false;
                var Response = CiudadesFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true, Region: $scope.Modelo.RegionPaisDestino
                });
                $scope.ListaRegionCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRegionCiudades);
                $scope.Modelo.CiudadDestino1 = $scope.ListaRegionCiudades[0];
            }
            else {
                $scope.ListaRegionCiudades = [];
                $scope.Modelo.CiudadDestino1 = '';
            }
        };

        $scope.FiltrarCiudades2 = function () {
            if ($scope.Modelo.RegionPaisDestino2.Codigo > 0) {
                $scope.ListaRegionCiudades2 = [];
                blockUIConfig.autoBlock = false;
                var Response = CiudadesFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true, Region: $scope.Modelo.RegionPaisDestino2
                });
                $scope.ListaRegionCiudades2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRegionCiudades2);
                $scope.Modelo.CiudadDestino2 = $scope.ListaRegionCiudades2[0];
            }
            else {
                $scope.ListaRegionCiudades2 = [];
                $scope.Modelo.CiudadDestino2 = $scope.ListaRegionCiudades2[0];
            }
        };
        $scope.ConsultarVehiculos = function () {
            //$scope.ListadoEnturnes = [];
            $scope.ListadoVehiculos = [];
            /*Cargar Autocomplete de propietario*/
            try {
                if ($scope.Modelo.Conductor.Codigo > 0) {
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Conductor: { Codigo: $scope.Modelo.Conductor.Codigo },
                        //ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                    if ($scope.ListadoVehiculos.length > 0) {
                        $scope.Modelo.Vehiculo = $scope.ListadoVehiculos[0]
                        $scope.AsignarDatosVehiculo()
                    } else {
                        ShowError('El conductor ingresado no posee vehículos asociados en el sistema')
                    }
                }
            } catch (e) {
            }
        }
        $scope.AsignarDatosVehiculo = function () {
            if ($scope.Modelo.Vehiculo.Codigo != undefined && $scope.Modelo.Vehiculo.Codigo != null && $scope.Modelo.Vehiculo.Codigo != '') {
                var tmpVehiculo = VehiculosFactory.Obtener({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: $scope.Modelo.Vehiculo.Codigo,
                    Sync: true
                }).Datos;
                var tmpConductro = TercerosFactory.Obtener({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Codigo: tmpVehiculo.Conductor.Codigo,
                    Sync: true
                }).Datos;

                var VehiculoValido = true;
                if (tmpVehiculo.Estado == 0) {
                    VehiculoValido = false;
                    ShowError('El vehiculo inactivo Causa:' + tmpVehiculo.JustificacionBloqueo);
                }
                else {
                    var ResponsListaNegra = VehiculosFactory.ConsultarEstadoListaNegra({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Codigo: tmpVehiculo.Codigo,
                        Sync: true
                    });
                    if (ResponsListaNegra.ProcesoExitoso == true) {
                        VehiculoValido = false;
                        ShowError("El vehículo se encuentra en lista negra");
                    }
                }

                if (VehiculoValido == true) {
                    if (tmpConductro.Estado.Codigo == 0) {
                        VehiculoValido = false;
                        ShowError("El conductor del vehículo " + tmpVehiculo.Placa + " de nombre '" + tmpConductro.NombreCompleto + "' esta inactivo por motivo '" + tmpConductro.JustificacionBloqueo + "'");
                    }
                }

                if (VehiculoValido == true) {
                    $scope.Modelo.TipoVehiculo = $linq.Enumerable().From($scope.ListadoTipoVehiculo).First('$.Codigo ==' + $scope.Modelo.Vehiculo.TipoVehiculo.Codigo);
                    $scope.Modelo.Tenedor = $scope.CargarTercero($scope.Modelo.Vehiculo.Tenedor.Codigo);
                    $scope.Modelo.Conductor = $scope.CargarTercero($scope.Modelo.Vehiculo.Conductor.Codigo);
                }
                else {
                    $scope.Modelo.Vehiculo = '';
                }
            }
        };
        $scope.Enturnar = function () {
            if (DatosRequeridos()) {
                $scope.Guardar();
            }
        };
        $scope.Guardar = function () {
            EnturnamientoFactory.Guardar($scope.Modelo).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('El enturnamiento se solicitó correctamente')
                            closeModal('ModalEnturnamiento')
                            Find()
                        }
                        else {
                            ShowError(response.statusText)
                        }
                    } else {
                        ShowError('El enturnamiento no se logro guardar correctamente, por favor verifique que el conductor no este enturnado actualmente')

                    }
                }, function (response) {
                    ShowError(response.statusText)
                });

        }
        function DatosRequeridos() {
            $scope.MensajesError2 = []
            var continuar = true
            if ($scope.Modelo.Conductor == undefined || $scope.Modelo.Conductor == '') {
                $scope.MensajesError2.push('Debe ingresar el conductor')
                continuar = false
            }
            if ($scope.Modelo.Vehiculo == undefined || $scope.Modelo.Vehiculo == '') {
                $scope.MensajesError2.push('Debe ingresar el vehículo')
                continuar = false
            }
            if ($scope.Modelo.CiudadEnturna == undefined || $scope.Modelo.CiudadEnturna == '') {
                $scope.MensajesError2.push('Debe ingresar la Ciudad Enturne')
                continuar = false
            }
            if ($scope.Modelo.OficinaEnturna == undefined || $scope.Modelo.OficinaEnturna == '') {
                $scope.MensajesError2.push('Debe ingresar la oficina Enturne')
                continuar = false
            }
            if ($scope.Modelo.RegionPaisDestino == undefined || $scope.Modelo.RegionPaisDestino == '') {
                $scope.MensajesError2.push('Debe ingresar la región destino 1')
                continuar = false
            }
            if ($scope.Modelo.CiudadDestino1 == undefined || $scope.Modelo.CiudadDestino1 == '') {
                $scope.MensajesError2.push('Debe ingresar la ciudad destino 1')
                continuar = false
            }
            if ($scope.Modelo.FechaDisponible == undefined || $scope.Modelo.FechaDisponible == '') {
                $scope.MensajesError2.push('Debe ingresar la fecha hora de disponibilidad')
                continuar = false
            } else {
                if ($scope.Modelo.FechaDisponible < new Date()) {
                    $scope.MensajesError2.push('La fecha horas de disponibilidad debe ser mayor a la actual')
                    continuar = false
                }
            }
            return continuar
        }
        $scope.CerrarModal = function () {
            closeModal('ModalEnturnamiento')

        }
        $scope.ConfirmarAnular = function (item) {
            if ($scope.DeshabilitarEliminarAnular == false) {
                showModal('modalAnular')
                $scope.ItemAnula = item
            }
        }
        $scope.CancelarTurno = function () {
            if ($scope.CausaAnulacion == undefined || $scope.CausaAnulacion == '') {
                ShowError('Debe ingresar el motivo de cancelación del turno')
            } else {
                closeModal('modalAnular')
                $scope.ItemAnula.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                $scope.ItemAnula.CausaAnulacion = $scope.CausaAnulacion
                $scope.ItemAnula.UsuarioAnula = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                EnturnamientoFactory.Anular($scope.ItemAnula).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            ShowSuccess('El turno se cancelo correctamente')
                            closeModal('modalAnular')
                            Find()
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        $scope.AsignarTurno = function (item) {
            item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            item.OficinaEnturna = $scope.ModeloOficina
            item.Numero = item.ID
            item.AsignarTurno = 1
            EnturnamientoFactory.Guardar(item).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('La asignación de turnos se realizó correctamente')
                            Find()
                        }
                        else {
                            ShowError(response.statusText)
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        $scope.Desenturnar = function (item) {
            item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            item.OficinaEnturna = $scope.ModeloOficina
            item.Numero = item.ID
            item.Desenturnar = 1
            EnturnamientoFactory.Guardar(item).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('El vehículo se desenturno correctamente')
                            Find()
                        }
                        else {
                            ShowError(response.statusText)
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }
        $scope.CerrarTurno = function (item) {
            item.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
            item.OficinaEnturna = $scope.ModeloOficina
            item.Numero = item.ID
            item.CerrarTurno = 1
            EnturnamientoFactory.Guardar(item).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos > 0) {
                            ShowSuccess('El turno se cerro correctamente')
                            Find()
                        }
                        else {
                            ShowError(response.statusText)
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        }

        $scope.CambiarTurno = function (item) {
            showModal('modalCambioTurno')
            $scope.ItemCambio = item
            $scope.ListadoTurnosDisponibles = []
            for (var i = 0; i < $scope.ListadoEnturnes.length; i++) {
                if ($scope.ListadoEnturnes[i].Turno !== item.Turno) {
                    $scope.ListadoTurnosDisponibles.push($scope.ListadoEnturnes[i].Turno)
                }
            }
        }
        $scope.GuardarCambiarTurno = function () {
            if ($scope.Turno > 0) {
                closeModal('modalCambioTurno')

                $scope.ItemCambio.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                $scope.ItemCambio.OficinaEnturna = $scope.ModeloOficina
                $scope.ItemCambio.Numero = $scope.ItemCambio.ID
                $scope.ItemCambio.TurnoCambio = $scope.Turno
                $scope.ItemCambio.CambioTurno = 1
                EnturnamientoFactory.Guardar($scope.ItemCambio).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos > 0) {
                                ShowSuccess('El turno se cambio correctamente')
                                Find()
                            }
                            else {
                                ShowError(response.statusText)
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            else {
                ShowError('Por favor seleccione el turno')
            }
        }
        //--Mascaras
        $scope.MaskNumero = function (option) {
            try { $scope.CodigoAlterno = MascaraNumero($scope.CodigoAlterno) } catch (e) { }
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        //--Mascaras
        //----------------------------Informacion Requerida para funcionar---------------------------------//
    }]);