﻿SofttoolsApp.controller("GestionarNotaFacturasCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'OficinasFactory', 'FacturasFactory', 'blockUI', 'blockUIConfig', 'TercerosFactory', 'ConceptoNotasFacturasFactory',
    'FacturasFactory', 'FacturasOtrosConceptosFactory', 'NotasFacturasFactory',
    function ($scope, $timeout, $linq, $routeParams, OficinasFactory, FacturasFactory, blockUI, blockUIConfig, TercerosFactory, ConceptoNotasFacturasFactory,
        FacturasFactory, FacturasOtrosConceptosFactory, NotasFacturasFactory) {
        $scope.Titulo = 'GESTIONAR NOTA FACTURA';
        $scope.MapaSitio = [{ Nombre: 'Facturación' }, { Nombre: 'Documentos' }, { Nombre: 'Notas Factura' }, { Nombre: 'Gestionar' }];
        $scope.Master = "#!ConsultarNotaFacturas";
        // -----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- //
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_NOTA_CREDITO_FACTURAS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        //-- Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.MensajesErrorConslta = [];
        var TMPCodigoCliente = 0;
        var TMPNumeroDocumentoFactura = 0;
        var TMPTipoDocumento = 0;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            TipoDocumento: CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            OficinaNota: {
                Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
                Nombre: $scope.Sesion.UsuarioAutenticado.Oficinas.Nombre,
            },
            Numero: CERO,
            Fecha: '',
            Factura: {
                NumeroDocumento: '',
                ValorFactura: '',
            },
            Estado: CERO,
            Anulado: CERO,
        }
        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: ESTADO_DEFINITIVO },
            { Nombre: 'BORRADOR', Codigo: ESTADO_BORRADOR }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + ESTADO_BORRADOR);

        $scope.ListadoTipoNotas = [
            { Nombre: 'CRÉDITO', Codigo: CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO },
            { Nombre: 'DÉBITO', Codigo: CODIGO_TIPO_DOCUMENTO_NOTA_DEBITO }
        ];
        $scope.Modelo.TipoDocumento = $scope.ListadoTipoNotas[CERO];

        //-- Listados
        $scope.ListadoConceptoNota = [];
        $scope.ListadoCliente = [];
        $scope.ListadoTerceros = [];
        $scope.ListadoConceptoNotas = [];
        $scope.DeshabilitarFactura = true;
        $scope.DeshabilitarValorNota = false;
        // -----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- //
        // ----------------------------Informacion Requerida para funcionar--------------------------------- //
        var CodigoAnulacionNota = 0;
        $scope.ObtenerConceptosNotas = function () {
            if ($scope.Modelo.TipoDocumento != null && $scope.Modelo.TipoDocumento != undefined && TMPTipoDocumento != $scope.Modelo.TipoDocumento.Codigo) {
                var ResponseConceptosNotas = ConceptoNotasFacturasFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Estado: ESTADO_DEFINITIVO,
                    TipoDocumento: $scope.Modelo.TipoDocumento.Codigo,
                    Sync: true
                });
                if (ResponseConceptosNotas.ProcesoExitoso) {
                    $scope.ListadoConceptoNotas = [];
                    if (ResponseConceptosNotas.Datos.length > CERO) {
                        $scope.ListadoConceptoNotas = ResponseConceptosNotas.Datos;
                        TMPTipoDocumento = $scope.Modelo.TipoDocumento.Codigo;
                        //$scope.Modelo.ConceptoNotas = '';
                        for (var i = 0; i < $scope.ListadoConceptoNotas.length; i++) {
                            if ($scope.ListadoConceptoNotas[i].TipoDocumento == CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO && $scope.ListadoConceptoNotas[i].Nombre.indexOf(CONCEPTO_TIPO_NOTA_ANULACION) > -1) {
                                CodigoAnulacionNota = $scope.ListadoConceptoNotas[i].Codigo;
                                break;
                            }
                        }
                    }
                    else {
                        $scope.ListadoConceptoNotas = [];
                    }
                    $scope.DeshabilitarValorNota = false;
                }
            }
        }
        $scope.ObtenerConceptosNotas();
        //--Carga oficinas
        $scope.ListadoOficinas = [];
        if ($scope.Sesion.UsuarioAutenticado.AplicaConsultaOtrasOficinas) {
            $scope.ListadoOficinas = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
            try {
                $scope.Modelo.Factura.OficinaFactura = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
            } catch (e) {
                console.log("error: ", e);
            }
        } else {
            if ($scope.Sesion.UsuarioAutenticado.ListadoOficinas.length > 0) {
                $scope.Sesion.UsuarioAutenticado.ListadoOficinas.forEach(function (item) {
                    $scope.ListadoOficinas.push(item)
                });
                try {
                    $scope.Modelo.Factura.OficinaFactura = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                } catch (e) {
                    console.log("error: ", e);
                }
            } else {
                ShowError('El usuario no tiene oficinas asociadas');
            }
        }
        //----------------------------Informacion Requerida para funcionar--------------------------------- //
        //-- Validacion Facturas --//
        $scope.AsignarFactura = function (Factura_Numero_Documento) {
            if (Factura_Numero_Documento > 0) {
                if (Factura_Numero_Documento != TMPNumeroDocumentoFactura) {
                    var filtro = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        NumeroDocumento: Factura_Numero_Documento,
                        Estado: ESTADO_DEFINITIVO,
                        OficinaFactura: { Codigo: $scope.Modelo.Factura.OficinaFactura.Codigo },
                        Anulado: ESTADO_NO_ANULADO,
                        Sync: true
                    };
                    //-- Factura Ventas --//
                    var existeRegistro = false;
                    var ResponseFactura = FacturasFactory.Obtener(filtro);
                    if (ResponseFactura.ProcesoExitoso) {
                        if (ResponseFactura.Datos.Numero > CERO) {
                            existeRegistro = true;
                            $scope.Modelo.Factura = ResponseFactura.Datos;
                            $scope.Modelo.FacturarA = $scope.CargarTercero(ResponseFactura.Datos.FacturarA.Codigo)
                            $scope.Modelo.Clientes = $scope.CargarTercero(ResponseFactura.Datos.Cliente.Codigo);
                            $scope.Modelo.Factura.OficinaFactura = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + ResponseFactura.Datos.OficinaFactura.Codigo);
                            TMPNumeroDocumentoFactura = $scope.Modelo.NumeroDocumento;
                            $scope.MaskValores();
                            $scope.AsignarConceptoNotas();
                        }
                        else {
                            var ResponseFacturaOtros = FacturasOtrosConceptosFactory.Obtener(filtro);
                            if (ResponseFacturaOtros.ProcesoExitoso) {
                                if (ResponseFacturaOtros.Datos.Numero > CERO) {
                                    existeRegistro = true;
                                    $scope.Modelo.Factura = ResponseFacturaOtros.Datos;
                                    $scope.Modelo.FacturarA = $scope.CargarTercero(ResponseFacturaOtros.Datos.FacturarA.Codigo)
                                    $scope.Modelo.Clientes = $scope.CargarTercero(ResponseFactura.Datos.Cliente.Codigo);
                                    $scope.Modelo.Factura.OficinaFactura = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + ResponseFactura.Datos.OficinaFactura.Codigo);
                                    TMPNumeroDocumentoFactura = $scope.Modelo.NumeroDocumento;
                                    $scope.MaskValores();
                                    $scope.AsignarConceptoNotas();
                                }
                            }
                        }
                    }
                    if (!existeRegistro) {
                        $scope.Modelo.Factura.NumeroDocumento = '';
                        $scope.Modelo.Factura.ValorFactura = '';
                        TMPNumeroDocumentoFactura = 0;
                        ShowInfo("No se encontró una factura con este número, revise si el estado está en definitivo sin anular");
                    }
                }
            }
            else {
                $scope.Modelo.Factura.NumeroDocumento = '';
                $scope.Modelo.Factura.ValorFactura = '';
                TMPNumeroDocumentoFactura = 0;
            }
        }
        //-- Validacion Facturas --//
        //-- Asignar Cliente --//
        $scope.AsignarCliente = function (Cliente) {
            if (Cliente !== undefined && Cliente !== null && Cliente !== '') {
                //valida si el dato ingresado hace parte del objeto de la lista
                if (angular.isObject(Cliente)) {
                    if (TMPCodigoCliente !== Cliente.Codigo) {
                        $scope.Modelo.Clientes = angular.copy(Cliente);
                        TMPCodigoCliente = Cliente.Codigo;
                        $scope.DeshabilitarFactura = false;
                        //Inicializa Listas Cuando Cambia de usuario
                        $scope.Modelo.Factura.NumeroDocumento = '';
                        $scope.Modelo.Factura.ValorFactura = '';
                    }
                }
            }
            else {
                //Inicializa Listas Cuando Cambia de usuario
                $scope.DeshabilitarFactura = true;
                $scope.Modelo.Factura.NumeroDocumento = '';
                $scope.Modelo.Factura.ValorFactura = '';
            }
        };
        //-- Asignar Cliente --//
        //-------------------------------------------------- Proceso Guardado --------------------------------------------------//
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            var NotaFactura = {
                Numero: $scope.Modelo.Numero,
                CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                TipoDocumento: $scope.Modelo.TipoDocumento.Codigo,
                Factura: { Numero: $scope.Modelo.Factura.Numero, CataTipoFactura: $scope.Modelo.Factura.CataTipoFactura },
                Fecha: $scope.Modelo.Fecha,
                OficinaNota: { Codigo: $scope.Modelo.OficinaNota.Codigo },
                Clientes: { Codigo: $scope.Modelo.Clientes.Codigo },
                FacturarA: { Codigo: $scope.Modelo.FacturarA.Codigo },
                ConceptoNotas: { Codigo: $scope.Modelo.ConceptoNotas.Codigo },
                Observaciones: $scope.Modelo.Observaciones,
                ValorNota: $scope.Modelo.ValorNota,
                Estado: $scope.Modelo.Estado.Codigo,
                Anulado: $scope.Modelo.Anulado,
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            }
            NotasFacturasFactory.Guardar(NotaFactura).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        if (response.data.Datos > CERO) {
                            $scope.Modelo.NumeroDocumento = response.data.Datos;
                            if ($scope.Modelo.Numero == CERO) {
                                ShowSuccess('Se guardó la nota número ' + response.data.Datos + ' correctamente');
                            }
                            else {
                                ShowSuccess('Se modificó la nota número ' + response.data.Datos + ' correctamente');
                            }
                            location.href = $scope.Master + '/' + response.data.Numero;
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                        }
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        $scope.ConfirmacionGuardar = function () {
            if (!$scope.DeshabilitarActualizar) {
                if (DatosRequeridos()) {
                    showModal('modalConfirmacionGuardar');
                }
            }
        };
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo;
            if (Modelo.Fecha == undefined || Modelo.Fecha == '' || Modelo.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha');
                continuar = false;
            }
            if (Modelo.Factura.ValorFactura == undefined || Modelo.Factura.ValorFactura == '' || Modelo.Factura.ValorFactura == null) {
                $scope.MensajesError.push('Debe ingresar Número de factura');
                continuar = false;
            }
            if (Modelo.TipoDocumento == undefined || Modelo.TipoDocumento == '' || Modelo.TipoDocumento == null) {
                $scope.MensajesError.push('Debe ingresar un tipo nota');
                continuar = false;
            }
            if (Modelo.ConceptoNotas == undefined || Modelo.ConceptoNotas == '' || Modelo.ConceptoNotas == null) {
                $scope.MensajesError.push('Seleccione un concepto de nota');
                continuar = false;
            }
            if (Modelo.ValorNota == undefined || Modelo.ValorNota == '' || Modelo.ValorNota == null) {
                $scope.MensajesError.push('Debe ingresar el valor de la nota');
                continuar = false;
            }
            if (Modelo.TipoDocumento.Codigo == CODIGO_TIPO_DOCUMENTO_NOTA_CREDITO) {
                var valorNota = parseInt(MascaraDecimales(Modelo.ValorNota));
                var valorfactura = parseInt(MascaraDecimales(Modelo.Factura.ValorFactura));
                if (valorNota > valorfactura) {
                    $scope.MensajesError.push('El valor de la nota, no puede superar el valor de la factura');
                    continuar = false;
                }
            }

            return continuar;
        }
        $scope.AsignarConceptoNotas = function () {
            if ($scope.Modelo.ConceptoNotas != null && $scope.Modelo.ConceptoNotas != '' && $scope.Modelo.ConceptoNotas != undefined) {
                if ($scope.Modelo.ConceptoNotas.Codigo == CodigoAnulacionNota) {
                    $scope.Modelo.ValorNota = $scope.Modelo.Factura.ValorFactura;
                    $scope.DeshabilitarValorNota = true;
                }
                else {
                    $scope.Modelo.ValorNota = '';
                    $scope.DeshabilitarValorNota = false;
                }
            }
        }
        //-------------------------------------------------- Proceso Guardado --------------------------------------------------//
        //-------------------------------------------------- Proceso Modificar --------------------------------------------------//
        function Obtener() {
            blockUI.start('Cargando Nota No. ' + $scope.Modelo.Numero);
            $timeout(function () {
                blockUI.message('Cargando Nota No. ' + $scope.Modelo.Numero);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
                OpcNotEle: 0,
            };

            NotasFacturasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso == true) {
                        $scope.Modelo = response.data.Datos;
                        $scope.Modelo.Fecha = new Date(response.data.Datos.Fecha);
                        TMPCodigoCliente = response.data.Datos.Clientes.Codigo;
                        TMPNumeroDocumentoFactura = response.data.Datos.Factura.NumeroDocumento;
                        $scope.Modelo.Clientes = $scope.CargarTercero(response.data.Datos.Clientes.Codigo);
                        $scope.Modelo.FacturarA = $scope.CargarTercero(response.data.Datos.FacturarA.Codigo);
                        $scope.Modelo.TipoDocumento = $linq.Enumerable().From($scope.ListadoTipoNotas).First('$.Codigo ==' + response.data.Datos.TipoDocumento);
                        $scope.ObtenerConceptosNotas();
                        $scope.DeshabilitarTipoDocumento = true;

                        if ($scope.ListadoConceptoNotas !== undefined && $scope.ListadoConceptoNotas !== null) {
                            if ($scope.ListadoConceptoNotas.length > 0 && response.data.Datos.ConceptoNotas.Codigo > 0) {
                                $scope.Modelo.ConceptoNotas = $linq.Enumerable().From($scope.ListadoConceptoNotas).First('$.Codigo ==' + response.data.Datos.ConceptoNotas.Codigo);
                            }
                        }


                        if (response.data.Datos.Anulado == ESTADO_ANULADO) {
                            $scope.ListadoEstados.push({ Nombre: 'ANULADO', Codigo: 2 });
                            $scope.Modelo.Estado = $scope.ListadoEstados[2];
                            $scope.Deshabilitar = true;
                            $scope.DeshabilitarFactura = true;
                            $scope.DeshabilitarValorNota = true;
                        } else {
                            if (response.data.Datos.Estado == ESTADO_DEFINITIVO) {
                                $scope.Deshabilitar = true;
                                $scope.DeshabilitarFactura = true;
                                $scope.DeshabilitarValorNota = true;
                            } else {
                                $scope.Deshabilitar = false;
                                $scope.DeshabilitarFactura = false;
                                $scope.DeshabilitarValorNota = false;
                            }
                        }
                        if (CodigoAnulacionNota == $scope.Modelo.ConceptoNotas.Codigo) {
                            $scope.DeshabilitarValorNota = true;
                        }
                        //$scope.AsignarConceptoNotas();
                        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + response.data.Datos.Estado);
                        $scope.MaskValores();
                        blockUI.stop();
                    }
                    else {
                        ShowError(response.statusText);
                        location.href = $scope.Master;
                        blockUI.stop();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    location.href = $scope.Master;
                    blockUI.stop();
                });
        }
        //-------------------------------------------------- Proceso Modificar --------------------------------------------------//
        //-- Obtener parametros --//
        if ($routeParams.Numero > 0) {
            $scope.Modelo.Numero = $routeParams.Numero;
            $scope.Titulo = 'CONSULTAR NOTA FACTURA';
            Obtener();
        }
        else {
            $scope.Modelo.Fecha = new Date();
        }
        //-- Mascaras
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        //-- Mascaras
        //-- Volver Master
        $scope.VolverMaster = function () {
            if ($scope.Modelo.Numero == 0) {
                document.location.href = $scope.Master
            }
            else {
                document.location.href = $scope.Master + "/" + $scope.Modelo.Numero;
            }
        }
    }]);