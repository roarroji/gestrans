﻿SofttoolsApp.controller("MarcarRemesasFacturadasCtrl", ['$scope', '$timeout', '$linq', 'blockUI', 'blockUIConfig', 'RemesasFactory', 'TercerosFactory',
    function ($scope, $timeout, $linq, blockUI, blockUIConfig, RemesasFactory, TercerosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Facturación' }, { Nombre: 'Procesos' }, { Nombre: 'Marcar Remesas Facturadas' }];
        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.ListadoRemesas = [];
        $scope.ListadoRemesasSeleccionadas = [];
        $scope.MostrarMensajeError = false;
        $scope.FechaInicio = new Date();
        $scope.FechaFin = new Date();
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_MARCAR_REMESAS_FACTURADAS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*------------------------------------------------------------------ FUNCION DE PAGINACIÓN --------------------------------------------------------------------------------- */
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.ListadoRemesas = [];
                $scope.paginaActual = 1;
                for (var i = 0; i < 10; i++) {
                    $scope.ListadoRemesas.push($scope.ListadoRemesasFiltradas[i]);
                }
            } else {
                $scope.ListadoRemesas = $scope.ListadoRemesasFiltradas;
            }
        };
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1;
                var a = $scope.paginaActual * 10;
                if (a < $scope.totalRegistros) {
                    $scope.ListadoRemesas = [];
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoRemesas.push($scope.ListadoRemesasFiltradas[i]);
                    }
                }
            } else {
                $scope.PrimerPagina();
            }
        };
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1;
                var a = $scope.paginaActual * 10;
                if (a < $scope.totalRegistros) {
                    $scope.ListadoRemesas = [];
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoRemesas.push($scope.ListadoRemesasFiltradas[i]);
                    }
                } else {
                    $scope.UltimaPagina();
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina();
            }
        };
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas;
                var a = $scope.paginaActual * 10;
                $scope.ListadoRemesas = [];
                for (var i = a - 10; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoRemesas.push($scope.ListadoRemesasFiltradas[i]);
                }
            }
        };

        //Cargar combo de clientes
        $scope.ListadoCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de clientes*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, ValorAutocomplete: value, Sync: true });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };

        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        //Seleccionar todas las remesas
        $scope.SeleccionarTodo = function (seleccion) {
            if (seleccion) {
                for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                    $scope.ListadoRemesas[i].Seleccionado = true;
                }
                for (var j = 0; j < $scope.ListadoRemesasFiltradas.length; j++) {
                    $scope.ListadoRemesasFiltradas[j].Seleccionado = true;
                }
                $scope.SeleccionRemesas = true;
            } else {
                for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                    $scope.ListadoRemesas[i].Seleccionado = false;
                }
                for (var j = 0; j < $scope.ListadoRemesasFiltradas.length; j++) {
                    $scope.ListadoRemesasFiltradas[j].Seleccionado = false;
                }
                $scope.SeleccionRemesas = false;
            }
        };
        
        // Marcar Remesas Facturadas
        $scope.MarcarRemesasFacturadas = function () {
            $scope.NumerosRemesas = '';
            $scope.ListadoRemesasFiltradas.forEach(function (item) {
                if (item.Seleccionado === true) {
                    $scope.NumerosRemesas = $scope.NumerosRemesas + item.Numero + ',';
                }
            });
            if ($scope.NumerosRemesas !== '') {
                showModal('modalConfirmacionmarcarremesasfacturadas');
            } else {
                ShowError('No se ha seleccionado ninguna remesa para marcar');
            }
        };

        $scope.ConfirmacionMarcar = function () {

            var entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumerosRemesas: $scope.NumerosRemesas
            };

            RemesasFactory.MarcarRemesasFacturadas(entidad).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        ShowSuccess('Se marcaron las remesas seleccionadas como facturadas');
                        closeModal('modalConfirmacionmarcarremesasfacturadas');
                        Find();
                    }
                    else {
                        ShowError(response.data.MensajeOperacion);
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };

        // Buscar remesas
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                if (DatosRequeridos()) {
                    Find();
                }
            }
        };

        function Find() {
            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            if ($scope.Buscando) {
                if ($scope.MensajesError.length === CERO) {
                    blockUI.delay = 1000;
                    $scope.ListadoRemesas = [];
                    $scope.ListadoRemesasSeleccionadas = [];

                    var TraerRemesas = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        NumeroDocumento: $scope.NumeroRemesa,
                        FechaInicial: $scope.FechaInicio,
                        FechaFinal: $scope.FechaFin,
                        Cliente: $scope.Modelo.Cliente == undefined ? CERO : $scope.Modelo.Cliente
                    };

                    RemesasFactory.ConsultarRemesasPendientesFacturar(TraerRemesas).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.length > CERO) {
                                    $scope.ListadoRemesasFiltradas = response.data.Datos;
                                    $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                    $scope.PrimerPagina();
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                }
            }
            blockUI.stop();
        }
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicio = new Date();
            $scope.FechaFin = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.FechaInicio === null || $scope.FechaInicio === undefined || $scope.FechaInicio === '')
                && ($scope.FechaFin === null || $scope.FechaFin === undefined || $scope.FechaFin === '')
                && ($scope.NumeroRemesa === null || $scope.NumeroRemesa === undefined || $scope.NumeroRemesa === '' || $scope.NumeroRemesa === 0 || isNaN($scope.NumeroRemesa) === true)) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número remesa');
                continuar = false;

            } else if (($scope.NumeroDocumento !== null && $scope.NumeroDocumento !== undefined && $scope.NumeroDocumento !== '' && $scope.NumeroDocumento !== 0)
                || ($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                || ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {

                if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')
                    && ($scope.FechaFin !== null && $scope.FechaFin !== undefined && $scope.FechaFin !== '')) {

                    if ($scope.FechaFin < $scope.FechaInicio) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    } else if ((($scope.FechaFin - $scope.FechaInicio) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }

                } else {
                    if (($scope.FechaInicio !== null && $scope.FechaInicio !== undefined && $scope.FechaInicio !== '')) {
                        $scope.FechaFin = $scope.FechaInicio;
                    } else {
                        $scope.FechaInicio = $scope.FechaFin;
                    }
                }
            }
            return continuar;
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };

    }]);