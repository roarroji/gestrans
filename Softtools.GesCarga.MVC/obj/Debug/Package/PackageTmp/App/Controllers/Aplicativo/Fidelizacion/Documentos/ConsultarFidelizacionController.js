﻿SofttoolsApp.controller("ConsultarFidelizacionCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'blockUIConfig', 'TercerosFactory', 'VehiculosFactory', 'ValorCatalogosFactory',
    "PuntosVehiculosFactory",
    function ($scope, $timeout, $linq, $routeParams, blockUI, blockUIConfig, TercerosFactory, VehiculosFactory, ValorCatalogosFactory,
        PuntosVehiculosFactory) {

        $scope.Titulo = "CONSULTAR FIDELIZACIÓN";
        $scope.MapaSitio = [{ Nombre: 'Fidelización' }, { Nombre: 'Documentos' }, { Nombre: 'Fidelización' }];
        $scope.ActivarCategoria = false;
        //------------------------------------------------------------DECLARACION VARIABLES------------------------------------------------------------//
        var NullDate = '1/01/1900';
        $scope.Buscando = false;
        $scope.ResultadoSinRegistros = '';
        $scope.Deshabilitar = false;
        $scope.MensajesError = [];
        var filtros = {};
        $scope.pref = '';
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.totalPaginas = 0;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_FIDELIZACION); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Vehiculo: "",
            Tenedor: "",
            Conductor:""
        }

        $scope.ListadoCategorias = [];
        $scope.Codigo = CERO;

        $scope.ListadoEstados = [
            { Nombre: '(TODOS)', Codigo: -1 },
            { Nombre: 'ACTIVO', Codigo: 1 },
            { Nombre: 'INACTIVO', Codigo: 0 }
        ];
        $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo == -1');
        //------------------------------------------------------------PAGINACION------------------------------------------------------------//
        $scope.PrimerPagina = function () {
            $scope.paginaActual = 1;
            Find();
        };

        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual = $scope.paginaActual + 1;
                Find();
            }
        };

        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual = $scope.paginaActual - 1;
                Find();
            }
        };

        $scope.UltimaPagina = function () {
            $scope.paginaActual = $scope.totalPaginas;
            Find();
        };

        $scope.Imprimir = function () {
            Print();
        };
        $scope.ImprimirReporte = function (numero) {
            $scope.NumeroFactura = numero;
            PrintReporte();
        };
        //------------------------------------------------------------OBTENER INFORMACION------------------------------------------------------------//
        //-- catalogo Categoria fidelizacion
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CATEGORIA_FIDELIZACION } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCategorias.push({ Nombre: '(TODOS)', Codigo: -1 });
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoCategorias.push({ Nombre: response.data.Datos[i].Nombre, Codigo: response.data.Datos[i].Codigo })
                        }
                        $scope.Modelo.Categoria = $linq.Enumerable().From($scope.ListadoCategorias).First('$.Codigo == -1');
                    }
                    else {
                        $scope.ListadoCategorias = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //--Informacion Tenedero
        $scope.ObtenerInformacionTenedor = function () {
            if ($scope.Modelo.Vehiculo.Tenedor !== undefined) {
                $scope.Modelo.Tenedor = $scope.Modelo.Vehiculo.Tenedor;
            }
            if ($scope.Modelo.Vehiculo.Conductor !== undefined) {
                $scope.Modelo.Conductor = $scope.Modelo.Vehiculo.Conductor;
            }
        }
        //------------------------------------------------------------FUNCIONES AUTOCOMPLETE------------------------------------------------------------//
        //--Vehiculos
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                }
            }
            return $scope.ListadoVehiculos
        }
        //--Conductores
        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores);
                }
            }
            return $scope.ListadoConductores;
        }
        //--Tenedores
        $scope.ListadoTenedor = [];
        $scope.AutocompleteTenedor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_TENEDOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoTenedor = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoTenedor);
                }
            }
            return $scope.ListadoTenedor;
        }
        //------------------------------------------------------------FUNCIONES GENERALES------------------------------------------------------------//
        //--Nuevo Registro
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarFidelizacion';
            }
        };
        //--Buscar
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0;
                Find();
            }
        };

        function Find() {
            $scope.ListaFidelizacion = [];
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);

            $scope.Buscando = true;
            var CodigoVehiculo = $scope.Modelo.Vehiculo.Codigo == undefined ? 0 : $scope.Modelo.Vehiculo.Codigo;
            var CodigoTenedor = $scope.Modelo.Tenedor.Codigo == undefined ? 0 : $scope.Modelo.Tenedor.Codigo;
            var CodigoConductor = $scope.Modelo.Conductor.Codigo == undefined ? 0 : $scope.Modelo.Conductor.Codigo;

            var filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Vehiculo: { Codigo: CodigoVehiculo },
                Tenedor: { Codigo: CodigoTenedor },
                Conductor: { Codigo: CodigoConductor },
                CATA_CFTR: { Codigo: $scope.Modelo.Categoria.Codigo },
                Estado: $scope.Modelo.Estado.Codigo,
                Pagina: $scope.paginaActual,
                RegistrosPagina: $scope.cantidadRegistrosPorPagina,
            };
            blockUI.delay = 1000;
            PuntosVehiculosFactory.Consultar(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaFidelizacion = response.data.Datos;
                            for (var i = 0; i < $scope.ListaFidelizacion.length; i++) {
                                if ($scope.ListaFidelizacion[i].FechaActulizacion == NullDate) {
                                    $scope.ListaFidelizacion[i].FechaActulizacion = '';
                                }
                            }
                            /*----------------------------*/
                            $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                        else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            blockUI.stop();
        }

        //------------------------------------------------------------PARAMETROS------------------------------------------------------------//
        if ($routeParams !== undefined && $routeParams.Codigo !== null) {
            $scope.ListaFidelizacion = [];
            if (parseInt($routeParams.Codigo) > CERO) {
                $scope.Codigo = parseInt($routeParams.Codigo);
                //-- Filtro de busqueda en Get 
                var filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Vehiculo: { Codigo: $scope.Codigo },
                    Estado: $scope.Modelo.Estado.Codigo,
                    Pagina: $scope.paginaActual,
                    RegistrosPagina: $scope.cantidadRegistrosPorPagina
                }
                PuntosVehiculosFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.ListaFidelizacion = response.data.Datos;
                                /*----------------------------*/
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
                //-- Filtro de busqueda en Get 
            }
        }
        //------------------------------------------------------------REDIRECCIONAMIENTOS------------------------------------------------------------//
        $scope.ConsultarFidelizacionCausales = function (VehiCodigo) {
            document.location.href = "#!ConsultarCausalesFidelizacion/0/" + VehiCodigo;
        };
        $scope.ConsultarFidelizacionDespachos = function (VehiCodigo) {
            document.location.href = "#!ConsultarDespachosFidelizacion/0/" + VehiCodigo;
        };
    }]);