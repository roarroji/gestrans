﻿SofttoolsApp.controller("ConsultarListadoFidelizacionCtrl", ['$scope', '$timeout', 'TercerosFactory', 'RutasFactory', 'OficinasFactory', 'TarifarioVentasFactory', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'ValorCatalogosFactory', 'EmpresasFactory', 'VehiculosFactory',
    function ($scope, $timeout, TercerosFactory, RutasFactory, OficinasFactory, TarifarioVentasFactory, $linq, blockUI, blockUIConfig, $routeParams, ValorCatalogosFactory, EmpresasFactory, VehiculosFactory) {

        $scope.MapaSitio = [{ Nombre: 'Fidelización' }, { Nombre: 'Listados' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.DeshabilitarPDF = true;
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.NombreReporte = '';
        $scope.FiltroArmado = '';
        $scope.FiltroCliente = true;
        $scope.FiltroTransportador = false;
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.MostrarEstado = true;
        $scope.MostrarFecha = false;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            
        }

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCON_MENU_LISTADOS_FIDELIZACION); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        // Cargar combos de módulo y listados 
        $scope.ListaFidelizaciones = [];
        for (var i = 0; i < $scope.ListadoMenuListados.length; i++) {
            if ($scope.ListadoMenuListados[i].Padre == OPCON_MENU_LISTADOS_FIDELIZACION) {
                $scope.ListaFidelizaciones.push({ NombreMenu: $scope.ListadoMenuListados[i].Nombre, Codigo: $scope.ListadoMenuListados[i].Codigo })
            }
        }

        $scope.ModeloListadosFidelizacion = $scope.ListaFidelizaciones[0];

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;


        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });


        /*Cargar el combo de Estados*/

        $scope.ListadoEstado = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 1, Nombre: 'ACTIVOS' }, { Codigo: 0, Nombre: 'INACTIVOS' }];
        $scope.ModeloEstado = $scope.ListadoEstado[0];


        /*------------------------------------------------------------------------------------Eliminar Linea Vehiculos-----------------------------------------------------------------------*/

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.MostrarMensajeErrorModal = function () {
            $scope.MostrarMensajeErrorCompleto = true;
        }

        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope);
        };

        //--Vehiculos
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, ValorAutocomplete: value, Sync: true });
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos);
                }
            }
            return $scope.ListadoVehiculos;
        }

        $scope.AsignarFiltro = function (CodigoLista) {
            if (CodigoLista == CODIGO_LISTADO_INFORME_FIDELIZACION) {
                $scope.ModeloVehiculo = '';
                $scope.ModeloFechaInicial = '';
                $scope.ModeloFechaFinal = '';
                $scope.ModeloEstado = $scope.ListadoEstado[0];
                $scope.MostrarEstado = true;
                $scope.MostrarFecha = false;
            }
            if (CodigoLista == CODIGO_LISTADO_FIDELIZACION_CAUSAL_ANULADAS) {
                $scope.ModeloVehiculo = '';
                $scope.ModeloFechaInicial = '';
                $scope.ModeloFechaFinal = '';
                $scope.ModeloEstado = $scope.ListadoEstado[0];
                $scope.MostrarEstado = false;
                $scope.MostrarFecha = true;
            }

        }


        $scope.DesplegarInforme = function (OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.DatosRequeridos();

            if (DatosRequeridos == true) {

                //Depende del listado seleccionado se enviará el nombre por parametro

                if ($scope.ModeloListadosFidelizacion.Codigo == CODIGO_LISTADO_INFORME_FIDELIZACION) {
                    $scope.NombreReporte = NOMBRE_LISTADO_INFORME_FIDELIZACION;
                }
                if ($scope.ModeloListadosFidelizacion.Codigo == CODIGO_LISTADO_FIDELIZACION_CAUSAL_ANULADAS) {
                    $scope.NombreReporte = NOMBRE_LISTADO_FIDELIZACION_CAUSAL_ANULADAS;
                }
                

                $scope.ArmarFiltro();

                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
                }
                if (OpcionEXCEL == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf);
                }

                //Se limpia el filtro para poder hacer mas consultas
                $scope.FiltroArmado = '';

            }
        };

        $scope.DatosRequeridos = function () {
            DatosRequeridos = true;
            $scope.MensajesError = [];

            if ($scope.ModeloVehiculo == null || $scope.ModeloVehiculo == undefined || $scope.ModeloVehiculo == '') {
                $scope.ModeloVehiculo = null;
            }
            if ($scope.ModeloFechaInicial == null || $scope.ModeloFechaInicial == undefined || $scope.ModeloFechaInicial == '') {
                $scope.ModeloFechaInicial = null;
            }
            if ($scope.ModeloFechaFinal == null || $scope.ModeloFechaFinal == undefined || $scope.ModeloFechaFinal == '') {
                $scope.ModeloFechaFinal = null;
            }

            if ($scope.ModeloFechaInicial == null && $scope.ModeloFechaFinal !== null) {
                $scope.ModeloFechaInicial = $scope.ModeloFechaFinal;
            }
            if ($scope.ModeloFechaFinal == null && $scope.ModeloFechaInicial !== null) {
                $scope.ModeloFechaFinal = $scope.ModeloFechaInicial;
            }

            if ($scope.ModeloFechaInicial > $scope.ModeloFechaFinal ) {
                DatosRequeridos = false;
                $scope.MensajesError.push('La fecha inicial no puede ser superior a la fecha final');
            }

            if ($scope.ModeloEstado.Codigo == -1 ) {
                if ($scope.ModeloVehiculo == null && $scope.ModeloFechaInicial == null && $scope.ModeloFechaFinal == null) {
                    DatosRequeridos = false;
                    $scope.MensajesError.push('Debe ingresar algún criterio de búsqueda');
                }
            }

            return DatosRequeridos;

        }

        $scope.ArmarFiltro = function () {
           
            if ($scope.ModeloFechaInicial !== undefined && $scope.ModeloFechaInicial !== '' && $scope.ModeloFechaInicial !== null && $scope.MostrarFecha == true) {
                var ModeloFechaInicial = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaInicial);
                $scope.FiltroArmado += '&Fecha_Inicial=' + ModeloFechaInicial;
            }
            if ($scope.ModeloFechaFinal !== undefined && $scope.ModeloFechaFinal !== '' && $scope.ModeloFechaFinal !== null && $scope.MostrarFecha == true) {
                var ModeloFechaFinal = Formatear_Fecha_Mes_Dia_Ano($scope.ModeloFechaFinal);
                $scope.FiltroArmado += '&Fecha_Final=' + ModeloFechaFinal;
            }
            if ($scope.ModeloVehiculo !== undefined && $scope.ModeloVehiculo !== '' && $scope.ModeloVehiculo !== null) {
                if ($scope.ModeloVehiculo.Codigo > 0) {
                    $scope.FiltroArmado += '&Vehiculo=' + $scope.ModeloVehiculo.Codigo;
                }
            }
            if ($scope.ModeloEstado.Codigo != -1) {
                $scope.FiltroArmado += '&Estado=' + $scope.ModeloEstado.Codigo;
            }
        }
    }]);