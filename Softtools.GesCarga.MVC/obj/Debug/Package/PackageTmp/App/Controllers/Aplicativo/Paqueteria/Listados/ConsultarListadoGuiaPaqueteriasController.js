﻿SofttoolsApp.controller("ConsultarListadoGuiaPaqueteriasCtrl", ['$scope', '$timeout', 'TarifarioVentasFactory', '$linq', 'blockUI', 'blockUIConfig', '$routeParams', 'EmpresasFactory',
    'ValorCatalogosFactory', 'RutasFactory', 'TercerosFactory', 'OficinasFactory',
    function ($scope, $timeout, TarifarioVentasFactory, $linq, blockUI, blockUIConfig, $routeParams, EmpresasFactory,
        ValorCatalogosFactory, RutasFactory, TercerosFactory, OficinasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Listados' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.NombreReporte = '';
        $scope.FiltroArmado = '';

        //--Seccion Variables Mostrar Formulario
        $scope.MostrarRuta = true;
        $scope.MostrarReex = true;
        $scope.MostrarFormaPago = true;
        $scope.MostrarCliente = true;
        //--Seccion Variables Mostrar Formulario

        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OCION_MENU_LISTADOS_PAQUETERIA); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        // Cargar combos de módulo y listados 
        $scope.ListaListados = [];
        for (var i = 0; i < $scope.ListadoMenuListados.length; i++) {
            if ($scope.ListadoMenuListados[i].Padre == OCION_MENU_LISTADOS_PAQUETERIA) {
                $scope.ListaListados.push({
                    NombreMenu: $scope.ListadoMenuListados[i].Nombre,
                    Codigo: $scope.ListadoMenuListados[i].Codigo,
                    NombreListado: $scope.ListadoMenuListados[i].UrlPagina
                });
            }
        }

        $scope.ListadosPaqueteria = $scope.ListaListados[0];
        MostrarFiltroListado();

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        $scope.ListadoReexpedicion = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 0, Nombre: 'NO' }, { Codigo: 1, Nombre: 'SI' }];
        $scope.Reexpedicion = $scope.ListadoReexpedicion[0];
        $scope.ListadoEstados = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 0, Nombre: 'BORRADOR' }, { Codigo: 1, Nombre: 'DEFINITIVO' }, { Codigo: 2, Nombre: 'ANULADO' }];
        $scope.EstadoListado = $scope.ListadoEstados[0];
        $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO;
        $scope.PendienteCumplir = null;
        $scope.PendienteFacturar = null;

        //-- cargar Prefijo Empresa --//
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {
                $scope.pref = response.data.Datos[0].Prefijo;
                //console.log("Empresa Prefijo: ", $scope.pref);
            }
        });

        //--Rutas
        $scope.ListadoRutas = RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true }).Datos;
        //--Forma Paog
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoFormaPago = [];
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            $scope.ListadoFormaPago.push(response.data.Datos[i]);
                        }
                        $scope.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NO_APLICA);
                    }
                    else {
                        $scope.ListadoFormaPago = [];
                    }
                }
            }, function (response) {
            });
        //---- Clientes
        $scope.ListadoCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //---- Oficinas
        OficinasFactory.Consultar({ CodigoCiudad: $scope.CodigoCiudad, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoOficinas = [];
                    $scope.HabilitarCiudad = false;
                    $scope.ListadoOficinas = [{ Codigo: 0, Nombre: '(TODOS)' }];
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinas.push(item);
                    });
                    if ($scope.ListadoOficinas.length > 0) {
                        $scope.Oficinas = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 0');
                        $scope.OficinaActual = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 0');
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        //---DESPLEGAR INFORME LISTADOS ASP
        $scope.DesplegarInforme = function (OpcionPDf, OpcionEXCEL) {
            $scope.FiltroArmado = '';
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            var DatosRequeridos = true;

            switch ($scope.ListadosPaqueteria.Codigo) {
                case CODIGO_LISTADO_PLANILLAS_RECOGIDA:
                    $scope.NombreReporte = NOMBRE_LITADO_PLANILLAS_RECOGIDA;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_PLANILLA_RECOLECCION;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_PLANILLAS_DESPACHOS_PAQUETERIA:
                    if ($scope.ListadosPaqueteria.NombreListado == NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA)
                        $scope.NombreReporte = NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA;
                    else
                        $scope.NombreReporte = NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA + "_" + $scope.pref;

                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_PLANILLAS_DESPACHOS_PAQUETERIA_TENEDOR:
                    $scope.NombreReporte = NOMBRE_LITADO_PLANILLAS_DESPACHOS_PAQUETERIA_TENEDOR;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_PLANILLAS_ENTREGAS:
                    $scope.NombreReporte = NOMBRE_LITADO_PLANILLAS_ENTREGAS;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_PLANILLA_ENTREGA;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_REMESAS_PAQUETERIA:
                    $scope.NombreReporte = NOMBRE_LISTADO_REMESAS_PAQUETERIA;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.ArmarFiltro();
                    break;

                case CODIGO_LISTADO_INVENTARIO_REMESAS_OFICINA:
                    $scope.NombreReporte = NOMBRE_LISTADO_INVENTARIO_REMESAS_OFICINA;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_INVENTARIO_REMESAS_CLIENTE:
                    $scope.NombreReporte = NOMBRE_LISTADO_INVENTARIO_REMESAS_CLIENTE;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_INVENTARIO_REMESAS_OFICINA_POR_DESTINO:
                    $scope.NombreReporte = NOMBRE_LISTADO_INVENTARIO_REMESAS_OFICINA_POR_DESTINO;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.ArmarFiltro();
                    break;

                case CODIGO_LISTADO_COMISIONES_POR_OFICINA:
                    $scope.NombreReporte = NOMBRE_LISTADO_COMISIONES_POR_OFICINA;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_REMESAS_PENDIENTES_CUMPLIR_PAQUETERIA:
                    $scope.NombreReporte = NOMBRE_LISTADO_REMESAS_PENDIENTES_CUMPLIR_PAQUETERIA;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.PendienteCumplir = 0;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_REMESAS_PENDIENTES_FACTURAR_PAQUETERIA:
                    $scope.NombreReporte = NOMBRE_LISTADO_REMESAS_PENDIENTES_FACTURAR_PAQUETERIA;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.PendienteFacturar = 0;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_NOVEDADES_REMESAS_PAQUETERIA:
                    $scope.NombreReporte = NOMBRE_LISTADO_NOVEDADES_REMESAS_PAQUETERIA;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_VENTAS_OFICINA_PAQUETERIA:
                    $scope.NombreReporte = NOMBRE_LISTADO_VENTAS_OFICINA_PAQUETERIA;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_PUNTOS_SIN_ENTREGAR:
                    $scope.NombreReporte = NOMBRE_LISTADO_PUNTOS_SIN_ENTREGAR;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_INFORME_REEXPEDICION:
                    $scope.NombreReporte = NOMBRE_LISTADO_INFORME_REEXPEDICION;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_INFORME_REEXPEDICION_POR_OFICINA:
                    $scope.NombreReporte = NOMBRE_LISTADO_INFORME_REEXPEDICION_OFICINA;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.ArmarFiltro();
                    break;
                case CODIGO_LISTADO_SAC_REMESAS_PAQUETERIA:
                    $scope.NombreReporte = NOMBRE_LISTADO_SAC_REMESAS_PAQUETERIA;
                    DatosRequeridos = DatosRequeridosPlanillas();
                    $scope.TipoDocuento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;
                    $scope.ArmarFiltro();
                    break;
            }

            if (DatosRequeridos == true) {
                
                //Llama a la pagina ASP con el filtro por GET
                if (OpcionPDf == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf);
                }
                if (OpcionEXCEL == 1) {
                    window.open($scope.urlASP + '/Listados/Listados.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionEXCEL);
                }
            }

        };

        $scope.LimpiarFormFiltros = function () {
            $scope.Ruta = '';
            $scope.TipoDocuento = '';
            $scope.Cliente = '';
            $scope.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NO_APLICA);
            $scope.Reexpedicion = $scope.ListadoReexpedicion[0];
            $scope.PendienteCumplir = null;
            $scope.PendienteFacturar = null;
            $scope.OficinaActual = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == 0');
            if ($scope.ListadosPaqueteria.Codigo == CODIGO_LISTADO_INVENTARIO_REMESAS_OFICINA || $scope.ListadosPaqueteria.Codigo == CODIGO_LISTADO_INVENTARIO_REMESAS_OFICINA_POR_DESTINO || $scope.ListadosPaqueteria.Codigo == CODIGO_LISTADO_INVENTARIO_REMESAS_CLIENTE) {
                $scope.mostrarOficinaActual = true;
            } else {
                $scope.mostrarOficinaActual = false;
            }
            $scope.Planilla = '';
            $scope.Placa = '';
            MostrarFiltroListado();
        };
        function MostrarFiltroListado() {
            switch ($scope.ListadosPaqueteria.Codigo) {
                case CODIGO_LISTADO_PLANILLAS_RECOGIDA:
                    $scope.MostrarRuta = true;
                    $scope.MostrarReex = false;
                    $scope.MostrarFormaPago = false;
                    $scope.MostrarCliente = false;
                    $scope.MostrarPlanilla = false;
                    $scope.MostrarPlaca = false;
                    $scope.MostrarNumeroInicial = true;
                    $scope.MostrarNumeroFinal = true;
                    $scope.MostrarEstado = true;
                    $scope.MostrarOficina = true;
                    break;
                case CODIGO_LISTADO_PLANILLAS_DESPACHOS_PAQUETERIA:
                    $scope.MostrarRuta = true;
                    $scope.MostrarReex = false;
                    $scope.MostrarFormaPago = false;
                    $scope.MostrarCliente = false;
                    $scope.MostrarPlanilla = false;
                    $scope.MostrarPlaca = false;
                    $scope.MostrarNumeroInicial = true;
                    $scope.MostrarNumeroFinal = true;
                    $scope.MostrarEstado = true;
                    $scope.MostrarOficina = true;
                    break;
                case CODIGO_LISTADO_PLANILLAS_DESPACHOS_PAQUETERIA_TENEDOR:
                    $scope.MostrarRuta = true;
                    $scope.MostrarReex = false;
                    $scope.MostrarFormaPago = false;
                    $scope.MostrarCliente = false;
                    $scope.MostrarPlanilla = false;
                    $scope.MostrarPlaca = false;
                    $scope.MostrarNumeroInicial = true;
                    $scope.MostrarNumeroFinal = true;
                    $scope.MostrarEstado = true;
                    $scope.MostrarOficina = true;
                    break;
                case CODIGO_LISTADO_PLANILLAS_ENTREGAS:
                    $scope.MostrarRuta = true;
                    $scope.MostrarReex = false;
                    $scope.MostrarFormaPago = false;
                    $scope.MostrarCliente = false;
                    $scope.MostrarPlanilla = false;
                    $scope.MostrarPlaca = false;
                    $scope.MostrarNumeroInicial = true;
                    $scope.MostrarNumeroFinal = true;
                    $scope.MostrarEstado = true;
                    $scope.MostrarOficina = true;
                    break;
                case CODIGO_LISTADO_REMESAS_PAQUETERIA:
                case CODIGO_LISTADO_REMESAS_PENDIENTES_CUMPLIR_PAQUETERIA:
                case CODIGO_LISTADO_REMESAS_PENDIENTES_FACTURAR_PAQUETERIA:
                case CODIGO_LISTADO_COMISIONES_POR_OFICINA:
                case CODIGO_LISTADO_INVENTARIO_REMESAS_OFICINA:
                case CODIGO_LISTADO_INVENTARIO_REMESAS_CLIENTE:
                case CODIGO_LISTADO_INVENTARIO_REMESAS_OFICINA_POR_DESTINO:
                    $scope.MostrarRuta = false;
                    $scope.MostrarReex = true;
                    $scope.MostrarFormaPago = true;
                    $scope.MostrarCliente = true;
                    $scope.MostrarPlanilla = false;
                    $scope.MostrarPlaca = false;
                    $scope.MostrarNumeroInicial = true;
                    $scope.MostrarNumeroFinal = true;
                    $scope.MostrarEstado = true;
                    $scope.MostrarOficina = true;
                    break;
                case CODIGO_LISTADO_NOVEDADES_REMESAS_PAQUETERIA:
                    $scope.MostrarRuta = false;
                    $scope.MostrarReex = false;
                    $scope.MostrarFormaPago = false;
                    $scope.MostrarCliente = true;
                    $scope.MostrarPlanilla = false;
                    $scope.MostrarPlaca = false;
                    $scope.MostrarNumeroInicial = true;
                    $scope.MostrarNumeroFinal = true;
                    $scope.MostrarEstado = true;
                    $scope.MostrarOficina = true;
                    break;
                case CODIGO_LISTADO_VENTAS_OFICINA_PAQUETERIA:
                    $scope.MostrarRuta = false;
                    $scope.MostrarReex = false;
                    $scope.MostrarFormaPago = true;
                    $scope.MostrarCliente = true;
                    $scope.MostrarPlanilla = false;
                    $scope.MostrarPlaca = false;
                    $scope.MostrarNumeroInicial = true;
                    $scope.MostrarNumeroFinal = true;
                    $scope.MostrarEstado = true;
                    $scope.MostrarOficina = true;
                    break;
                case CODIGO_LISTADO_PUNTOS_SIN_ENTREGAR:
                    $scope.MostrarRuta = false;
                    $scope.MostrarReex = false;
                    $scope.MostrarFormaPago = true;
                    $scope.MostrarCliente = false;
                    $scope.MostrarPlanilla = false;
                    $scope.MostrarPlaca = false;
                    $scope.MostrarNumeroInicial = true;
                    $scope.MostrarNumeroFinal = true;
                    $scope.MostrarEstado = true;
                    $scope.MostrarOficina = true;
                    break;
                case CODIGO_LISTADO_INFORME_REEXPEDICION:
                    $scope.MostrarRuta = false;
                    $scope.MostrarReex = false;
                    $scope.MostrarFormaPago = true;
                    $scope.MostrarCliente = false;
                    $scope.MostrarPlanilla = true;
                    $scope.MostrarPlaca = true;
                    $scope.MostrarNumeroInicial = false;
                    $scope.MostrarNumeroFinal = false;
                    $scope.MostrarEstado = false;
                    $scope.MostrarOficina = false;
                    break;
                case CODIGO_LISTADO_INFORME_REEXPEDICION_POR_OFICINA:
                    $scope.MostrarRuta = false;
                    $scope.MostrarReex = false;
                    $scope.MostrarFormaPago = true;
                    $scope.MostrarCliente = false;
                    $scope.MostrarPlanilla = false;
                    $scope.MostrarPlaca = false;
                    $scope.MostrarNumeroInicial = false;
                    $scope.MostrarNumeroFinal = false;
                    $scope.MostrarEstado = false;
                    $scope.MostrarOficina = true;
                    break;
                case CODIGO_LISTADO_SAC_REMESAS_PAQUETERIA:
                    $scope.MostrarRuta = false;
                    $scope.MostrarReex = false;
                    $scope.MostrarFormaPago = false;
                    $scope.MostrarCliente = true;
                    $scope.MostrarPlanilla = false;
                    $scope.MostrarPlaca = false;
                    $scope.MostrarNumeroInicial = true;
                    $scope.MostrarNumeroFinal = true;
                    $scope.MostrarEstado = true;
                    $scope.MostrarOficina = true;
                    break;
            }
        }

        //--------------------------Validacion y Filtros Planillas--------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.FechaInicial = new Date();
            $scope.FechaFinal = new Date();
        }
        function DatosRequeridosPlanillas(){
            DatosRequeridos = true;
            $scope.MensajesError = [];

            if (($scope.FechaInicial == null || $scope.FechaInicial == undefined || $scope.FechaInicial == '')
                && ($scope.FechaFinal == null || $scope.FechaFinal == undefined || $scope.FechaFinal == '')
                && ($scope.NumeroInicial == null || $scope.NumeroInicial == undefined || $scope.NumeroInicial == '' || $scope.NumeroInicial == 0)
                && ($scope.NumeroFinal == null || $scope.NumeroFinal == undefined || $scope.NumeroFinal == '' || $scope.NumeroFinal == 0)
            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de números o fechas');
                DatosRequeridos = false;

            }
            else if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)
                || ($scope.NumeroFinal !== null && $scope.NumeroFinal !== undefined && $scope.NumeroFinal !== '' && $scope.NumeroFinal !== 0)
                || ($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                || ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')

            ) {
                DatosRequeridos = ValidarFechas();
                if (DatosRequeridos) {
                    DatosRequeridos = ValidarNumeros();
                }
                
            }
            return DatosRequeridos;
        }

        $scope.ArmarFiltro = function () {
            // Filtros Listado Nominaciones
            // 
            if ($scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== null && $scope.NumeroInicial > 0) {
                $scope.FiltroArmado += '&Numero_Documento_Inicial=' + $scope.NumeroInicial;
            }
            if ($scope.NumeroFinal !== undefined && $scope.NumeroFinal !== '' && $scope.NumeroFinal !== null && $scope.NumeroFinal > 0) {
                $scope.FiltroArmado += '&Numero_Documento_Final=' + $scope.NumeroFinal;
            }
            if ($scope.FechaInicial !== undefined && $scope.FechaInicial !== '' && $scope.FechaInicial !== null) {
                var FechaInicial = Formatear_Fecha_Mes_Dia_Ano($scope.FechaInicial);
                $scope.FiltroArmado += '&Fecha_Incial=' + FechaInicial;
            }
            if ($scope.FechaFinal !== undefined && $scope.FechaFinal !== '' && $scope.FechaFinal !== null) {
                var FechaFinal = Formatear_Fecha_Mes_Dia_Ano($scope.FechaFinal);
                $scope.FiltroArmado += '&Fecha_Final=' + FechaFinal;
            }

            if ($scope.Ruta !== undefined && $scope.Ruta !== '' && $scope.Ruta !== null) {
                $scope.FiltroArmado += '&Ruta=' + $scope.Ruta.Codigo;
            }
            if ($scope.EstadoListado.Codigo != -1) {
                $scope.FiltroArmado += '&Estado=' + $scope.EstadoListado.Codigo;
            }
            if ($scope.TipoDocuento != undefined) {
                $scope.FiltroArmado += '&TipoDocumento=' + $scope.TipoDocuento;
            }
            if ($scope.FormaPago != undefined && $scope.FormaPago !== '' && $scope.FormaPago !== null && $scope.FormaPago.Codigo != CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NO_APLICA) {
                $scope.FiltroArmado += '&FormaPago=' + $scope.FormaPago.Codigo;
            }
            if ($scope.Reexpedicion != undefined && $scope.Reexpedicion !== '' && $scope.Reexpedicion !== null && $scope.Reexpedicion.Codigo != -1) {
                $scope.FiltroArmado += '&Reexpedicion=' + $scope.Reexpedicion.Codigo;
            }
            if ($scope.PendienteCumplir != null) {
                $scope.FiltroArmado += '&PendienteCumplir=' + $scope.PendienteCumplir;
            }
            if ($scope.PendienteFacturar != null) {
                $scope.FiltroArmado += '&PendienteFacturar=' + $scope.PendienteFacturar;
            }
            if ($scope.Cliente != undefined && $scope.Cliente !== '' && $scope.Cliente !== null && $scope.Cliente.Codigo !== CERO) {
                $scope.FiltroArmado += '&Cliente=' + $scope.Cliente.Codigo;
            }
            if ($scope.Oficinas != undefined && $scope.Oficinas !== '' && $scope.Oficinas !== null && $scope.Oficinas.Codigo !== CERO) {
                $scope.FiltroArmado += '&Oficina=' + $scope.Oficinas.Codigo;
            }
            if ($scope.Planilla != undefined && $scope.Planilla !== '' && $scope.Planilla !== null ) {
                $scope.FiltroArmado += '&Planilla=' + $scope.Planilla;
            }
            if ($scope.Placa != undefined && $scope.Placa !== '' && $scope.Placa !== null ) {
                $scope.FiltroArmado += '&Placa=' + $scope.Placa;
            }
            if ($scope.OficinaActual != undefined && $scope.OficinaActual !== '' && $scope.OficinaActual !== null && $scope.OficinaActual.Codigo !== CERO) {
                $scope.FiltroArmado += '&OficinaActual=' + $scope.OficinaActual.Codigo;
            }
        };
        //--------------------------Validacion y Filtros Planillas--------------------------//
        //--------------------------Validaciones Generales--------------------------//
        function ValidarFechas() {
            var Validar = true;
            if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')
                && ($scope.FechaFinal !== null && $scope.FechaFinal !== undefined && $scope.FechaFinal !== '')) {
                if ($scope.FechaFinal < $scope.FechaInicial) {
                    $scope.MensajesError.push('La fecha final debe ser mayor a la fecha inicial');
                    Validar = false;
                } else if ((($scope.FechaFinal - $scope.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                    $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                    Validar = false;
                }
            }
            else {
                if (($scope.FechaInicial !== null && $scope.FechaInicial !== undefined && $scope.FechaInicial !== '')) {
                    $scope.FechaFinal = $scope.FechaInicial;
                }
                else {
                    $scope.FechaInicial = $scope.FechaFinal;
                }
            }
            return Validar;
        }

        function ValidarNumeros() {
            var Validar = true;
            var Rango = 0;
            Rango = $scope.NumeroFinal - $scope.NumeroInicial;
            if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)
                && ($scope.NumeroFinal !== null && $scope.NumeroFinal !== undefined && $scope.NumeroFinal !== '' && $scope.NumeroFinal !== 0)) {
                if ($scope.NumeroFinal < $scope.NumeroInicial) {
                    $scope.MensajesError.push('El número final debe ser mayor al número final');
                    Validar = false;
                }
                else if (Rango > RANGO_MAXIMO_CANTIDAD_DOCUMENTOS_LISTADOS) {
                    $scope.MensajesError.push('La diferencia del rango entre el numero inicial y el numero final no puede ser mayor a 2000');
                    Validar = false;
                }
            } else {
                if (($scope.NumeroInicial !== null && $scope.NumeroInicial !== undefined && $scope.NumeroInicial !== '' && $scope.NumeroInicial !== 0)) {
                    $scope.NumeroFinal = $scope.NumeroInicial;
                } else {
                    $scope.NumeroInicial = $scope.NumeroFinal;
                }
            }
            return Validar;
        }
        //--------------------------Validaciones Generales--------------------------//
    }]); 