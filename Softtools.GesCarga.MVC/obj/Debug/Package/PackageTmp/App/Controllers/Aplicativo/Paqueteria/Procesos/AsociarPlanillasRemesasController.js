﻿SofttoolsApp.controller("AsociarPlanillasRemesasCtrl", ['$scope', '$timeout', 'TarifarioVentasFactory', '$linq', 'blockUI', '$routeParams', 'ValorCatalogosFactory', 'RemesaGuiasFactory', 'EmpresasFactory', 'OficinasFactory', 'RemesasFactory', 'PlanillaGuiasFactory', 'SemirremolquesFactory', 'RutasFactory',
    function ($scope, $timeout, TarifarioVentasFactory, $linq, blockUI, $routeParams, ValorCatalogosFactory, RemesaGuiasFactory, EmpresasFactory, OficinasFactory, RemesasFactory, PlanillaGuiasFactory, SemirremolquesFactory, RutasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Paqueteria' }, { Nombre: 'Procesos' }, { Nombre: 'Adicionar Remesas Planilla Despacho' }];


        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 

        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = ''
        $scope.ModalError = ''
        $scope.MostrarMensajeError = false
        $scope.pref = '';
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Remesa: { Cliente: {}, Ruta: {} }
        }

        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GUIAS_PAQUETERIA); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        $scope.ListadoGuiasGeneral = []
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        /*-----------------------------------------------------------------FUNCION DE PAGINACIÓN Y IMPRIMIR --------------------------------------------------------------------------------- */
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.ListadoGuias = []
                $scope.paginaActual = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ListadoGuias.push($scope.ListadoGuiasGeneral[i])
                }
            } else {
                for (var i = 0; i < $scope.ListadoGuiasGeneral.length; i++) {
                    $scope.ListadoGuias.push($scope.ListadoGuiasGeneral[i])
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.ListadoGuias = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoGuias.push($scope.ListadoGuiasGeneral[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.ListadoGuias = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoGuias.push($scope.ListadoGuiasGeneral[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 10
                $scope.ListadoGuias = []
                for (var i = a - 10; i < $scope.ListadoGuiasGeneral.length; i++) {
                    $scope.ListadoGuias.push($scope.ListadoGuiasGeneral[i])
                }
            } else {
                for (var i = 0; i < $scope.ListadoGuiasGeneral.length; i++) {
                    $scope.ListadoGuias.push($scope.ListadoGuiasGeneral[i])
                }
            }
        }
        $scope.ListadoEstadoGuia = [
            { Codigo: -1, Nombre: '(TODOS)' }
        ]
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 60 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != 6000) {
                                $scope.ListadoEstadoGuia.push(response.data.Datos[i])
                            }
                        }
                    }
                    $scope.Modelo.EstadoRemesaPaqueteria = $scope.ListadoEstadoGuia[0]
                }
            }, function (response) {
            });

        $scope.ListadoOficinasActual = [
            { Codigo: 0, Nombre: '(TODAS)' }
        ];
        OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    response.data.Datos.forEach(function (item) {
                        $scope.ListadoOficinasActual.push(item);
                    })
                    $scope.Modelo.OficinaActual = $scope.ListadoOficinasActual[0];
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        //Funcion Nuevo Documento
        $scope.NuevoDocumento = function () {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarGuiaPaqueterias';
            }
        };
        $scope.CopiarDocumento = function (Codigo) {
            if ($scope.ValidarPermisos.AplicaActualizar == PERMISO_ACTIVO) {
                document.location.href = '#!GestionarGuiaPaqueterias/' + Codigo.toString() + ',1';
            }
        };
        //-------------------------------------------------FUNCIONES--------------------------------------------------------
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };
        if ($routeParams.Codigo !== undefined && $routeParams.Codigo !== null && $routeParams.Codigo !== '' && $routeParams.Codigo !== 0) {
            if ($routeParams.Codigo > 0) {
                $scope.Modelo.NumeroInicial = $routeParams.Codigo;
                $scope.Modelo.NumeroFinal = $routeParams.Codigo;
                Find();
            }
        }
        /*Cargar el combo de ESTADO DE REMESAS PAQUETERIA*/

        $scope.ListadoEstadoRemesaPaqueteria = [{ Codigo: -1, Nombre: '(TODOS)' }, { Codigo: 0, Nombre: 'BORRADOR' }, { Codigo: 1, Nombre: 'DEFINITIVO' }, { Codigo: 2, Nombre: 'ANULADO' }];
        $scope.ModeloEstado = $scope.ListadoEstadoRemesaPaqueteria[2];
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        }
        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                && ($scope.Modelo.NumeroInicial === null || $scope.Modelo.NumeroInicial === undefined || $scope.Modelo.NumeroInicial === '' || $scope.Modelo.NumeroInicial === 0 || isNaN($scope.Modelo.NumeroInicial) === true)

            ) {
                $scope.MensajesError.push('Debe ingresar los filtros de fechas o número');
                continuar = false

            } else if (($scope.Modelo.NumeroInicial !== null && $scope.Modelo.NumeroInicial !== undefined && $scope.Modelo.NumeroInicial !== '' && $scope.Modelo.NumeroInicial !== 0)
                || ($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')

            ) {
                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial
                    } else {
                        $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal
                    }
                }
            }
            return continuar
        }
        function Find() {

            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoGuias = []; $scope.ListadoGuiasGeneral = [];
            if (DatosRequeridos()) {

                blockUI.delay = 1000;
                $scope.Modelo.Pagina = $scope.paginaActual
                $scope.Modelo.RegistrosPagina = 0
                $scope.Modelo.Remesa = { NumeroplanillaDespacho: 1 }


                $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;

                if ($scope.ModeloEstado !== undefined && $scope.ModeloEstado !== null) {
                    $scope.Modelo.Estado = $scope.ModeloEstado.Codigo;
                } else {
                    $scope.Modelo.Estado = -1;
                }
                $scope.Modelo.AsociarRemesasPlanilla = 1

                var filtro = angular.copy($scope.Modelo);
                filtro.Anulado = -1;

                RemesaGuiasFactory.Consultar(filtro).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.Modelo.Codigo = 0
                            if (response.data.Datos.length > 0) {
                                $scope.ListadoGuiasGeneral = response.data.Datos
                                $scope.totalRegistros = response.data.Datos[0].TotalRegistros;
                                $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPaginas);
                                $scope.Buscando = false;
                                $scope.ResultadoSinRegistros = '';
                                $scope.PrimerPagina()
                            }
                            else {
                                $scope.totalRegistros = 0;
                                $scope.totalPaginas = 0;
                                $scope.paginaActual = 1;
                                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                $scope.Buscando = false;
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
            blockUI.stop();
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.DesplegarAsociacionPlanilla = function (item) {
            showModal('ModalAsociacionPlanilla')
            $scope.Modal = {
                Planilla: {}
            }
            //$scope.Remesa = item
            PlanillaGuiasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, TipoDocumento: 130, ConsultaPLanillasenRuta: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Codigo = 0
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoPlanilla = response.data.Datos
                        }
                        else {
                            ShowError('No se encontraron planillas disponibles ')
                            closeModal('ModalAsociacionPlanilla')
                        }
                        var listado = [];
                    }
                }, function (response) {
                    ShowError('No se encontraron planillas disponibles ')
                    closeModal('ModalAsociacionPlanilla')
                });
        }
        $scope.AosicarRemesasPlanillas = function (item) {
            var countmarcadas = 0
            if ($scope.ListadoGuiasGeneral.length > 0) {
                for (var i = 0; i < $scope.ListadoGuiasGeneral.length; i++) {
                    if ($scope.ListadoGuiasGeneral[i].Marcado) {
                        countmarcadas++
                    }
                }
                if (countmarcadas > 0) {
                    showModal('ModalAsociacionPlanilla')
                    $scope.Planilla = ''
                    $scope.Modal = {
                        Planilla: {}
                    }
                } else {
                    ShowError('Debe seleccionar al menos una remesa')
                }
            } else {
                ShowError('Debe seleccionar al menos una remesa')
            }

            //$scope.Remesa = item
        }
        $scope.ObtenerPlanilla = function (item) {
            if (item !== undefined && item !== '' && item > 0) {

                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: item,
                    TipoDocumento: 130
                };

                blockUI.delay = 1000;
                PlanillaGuiasFactory.Obtener(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Planilla.Anulado == 0 && response.data.Datos.Planilla.Estado.Codigo == 1) {
                                if (response.data.Datos.Planilla.NumeroCumplido == 0 && response.data.Datos.Planilla.NumeroLiquidacion == 0) {

                                    $scope.esObtener = true
                                    $scope.ListadoGuiaGuardadas = []
                                    $scope.Modal = response.data.Datos
                                    $scope.Modal.Planilla.FechaSalida = new Date($scope.Modal.Planilla.FechaHoraSalida)

                                    $scope.Modal.Planilla.HoraSalida = $scope.Modal.Planilla.FechaSalida.getHours().toString()

                                    if ($scope.Modal.Planilla.FechaSalida.getMinutes().toString().length == 1) {
                                        $scope.Modal.Planilla.HoraSalida += ':0' + $scope.Modal.Planilla.FechaSalida.getMinutes().toString()
                                    } else {
                                        $scope.Modal.Planilla.HoraSalida += ':' + $scope.Modal.Planilla.FechaSalida.getMinutes().toString()
                                    }
                                    $scope.Modal.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                                    $scope.Modal.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                    $scope.Modal.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                                    $scope.Modal.Planilla.Fecha = new Date($scope.Modal.Planilla.Fecha)
                                    var conductor = $scope.Modal.Planilla.Vehiculo.Conductor
                                    try {
                                        $scope.Modal.Planilla.Vehiculo = $scope.CargarVehiculos($scope.Modal.Planilla.Vehiculo.Codigo);
                                        $scope.Modal.Planilla.Vehiculo.Conductor = $scope.CargarTercero(conductor.Codigo)
                                        $scope.Modal.Planilla.Vehiculo.Tenedor = $scope.CargarTercero($scope.Modal.Planilla.Vehiculo.Tenedor.Codigo)
                                    } catch (e) {
                                    }
                                    SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
                                        then(function (response) {
                                            if (response.data.ProcesoExitoso === true) {
                                                if (response.data.Datos.length > 0) {
                                                    $scope.ListaRemolque = response.data.Datos;
                                                    try {
                                                        if ($scope.Modal.Planilla.Semirremolque.Codigo > CERO) {
                                                            $scope.Modal.Planilla.Semirremolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Codigo ==' + $scope.Modal.Planilla.Semirremolque.Codigo);
                                                        }
                                                    } catch (e) {
                                                    }
                                                }
                                                else {
                                                    $scope.ListaRemolque = [];
                                                    $scope.ModeloRemolque = null;
                                                }
                                            }
                                        }, function (response) {
                                            ShowError(response.statusText);
                                        });
                                    RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                                        then(function (response) {
                                            if (response.data.ProcesoExitoso === true) {
                                                $scope.ListadoRutas = [];
                                                if (response.data.Datos.length > 0) {
                                                    $scope.Modal.Planilla.Ruta = $linq.Enumerable().From(response.data.Datos).First('$.Codigo ==' + $scope.Modal.Planilla.Ruta.Codigo);
                                                }
                                                else {
                                                    $scope.ListadoRutas = []
                                                }
                                            }
                                        }, function (response) {
                                        });
                                } else {
                                    ShowError('La planilla ingresada ya se encuentra liquidada');
                                    $scope.Planilla = ''
                                }
                            } else {
                                ShowError('La planilla ingresada se encuentra en estado borrador o fue anulada');
                                $scope.Planilla = ''
                            }
                        }
                        else {
                            ShowError('No se encontro una planilla valida con el número ingresado');
                            $scope.Planilla = ''
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                        $scope.Planilla = ''
                    });
            } else {
                ShowError('Debe ingresar el número de la planilla')
            }

        }

        $scope.AsociarGuiasPlanilla = function () {
            var Entidad = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,
                RemesaPaqueteria: [],
                NumeroPlanilla: $scope.Modal.Planilla.Numero
            }
            var filtro = angular.copy($scope.Modelo);
            filtro.Anulado = -1;
            filtro.Sync = true;
            $scope.ListaResultadoGuias = [];
            try {
                $scope.ListaResultadoGuias = RemesaGuiasFactory.Consultar(filtro).Datos
            } catch (e) {
                console.log("errorConsulta: ", e);
            }
            var Coincidencias = 0;
            var guardar = true;
            for (var i = 0; i < $scope.ListadoGuiasGeneral.length; i++) {
                if ($scope.ListadoGuiasGeneral[i].Marcado) {
                    Coincidencias = 0;
                    $scope.ListaResultadoGuias.forEach(guia => {
                        if ($scope.ListadoGuiasGeneral[i].Remesa.Numero == guia.Remesa.Numero) {
                            Entidad.RemesaPaqueteria.push({
                                Numero: $scope.ListadoGuiasGeneral[i].Remesa.Numero,
                                NumeroDocumento: $scope.ListadoGuiasGeneral[i].Remesa.NumeroDocumento,

                            });
                            Coincidencias++;
                        }

                    });
                    if (Coincidencias == 0) {
                        $scope.ListadoGuiasGeneral[i].backGround = 'yellow';
                        guardar = false;
                    }
                }
            }
            $scope.PrimerPagina();
            if (guardar) {
                RemesaGuiasFactory.AsociarGuiaPlanilla(Entidad).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            closeModal('ModalAsociacionPlanilla')
                            ShowSuccess('Las remesas se asociaron correctamente a la planilla')
                            Find()
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                ShowError('las remesas seleccionadas ya se encuentran asociadas, por favor recargue la página');
            }
        }
        $scope.AsociarPlanilla = function () {
            if ($scope.Modal.Planilla !== undefined) {
                if ($scope.Modal.Planilla.Numero > 0) {
                    ShowWarningConfirm('¿Esta seguro de asociar las remesas a la planilla seleccionada?', $scope.AsociarGuiasPlanilla)
                } else {
                    ShowError('Debe ingresar la planilla')
                }

            } else {
                ShowError('Debe ingresar la planilla')
            }
        }
        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListadoGuiasGeneral.length; i++) {
                    $scope.ListadoGuiasGeneral[i].Marcado = true
                }
            }
            else {
                for (var i = 0; i < $scope.ListadoGuiasGeneral.length; i++) {
                    $scope.ListadoGuiasGeneral[i].Marcado = false
                }
            }
        }
    }]);