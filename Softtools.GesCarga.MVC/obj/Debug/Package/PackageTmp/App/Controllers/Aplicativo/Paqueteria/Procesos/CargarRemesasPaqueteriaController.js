﻿SofttoolsApp.controller("CargarRemesasPaqueteriaCtrl", ['$scope', '$timeout', '$linq', 'TercerosFactory', 'blockUI', 'RemesaGuiasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'ProductoTransportadosFactory', 'CiudadesFactory', 'SitiosTerceroClienteFactory', 'blockUIConfig', 'OficinasFactory', 'ZonasFactory',
    function ($scope, $timeout, $linq, TercerosFactory, blockUI, RemesaGuiasFactory, ValorCatalogosFactory, TercerosFactory, ProductoTransportadosFactory, CiudadesFactory, SitiosTerceroClienteFactory, blockUIConfig, OficinasFactory, ZonasFactory) {
        console.clear()



        // ----------------------------- Decalaracion Variables ------------------------------------------------------// 
        $scope.Buscando = false;
        $scope.continuar = false
        $scope.VerErrores = false;
        $scope.DeshabilitarCliente = false;
        $scope.ResultadoSinRegistros = '';
        $scope.MensajesError = [];
        $scope.ListadoEstados = [];
        $scope.ListadoRutasPuntosGestion = [];
        $scope.ListaHorariosEntrega = [];
        $scope.OficinaUsuario = OficinasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo, Sync: true }).Datos;
        $scope.Modelo = {
            Fecha: new Date(),
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas,

            Codigo: 0,
            CodigoAlterno: 0,
            Nombre: '',
            Estado: 0,
            Remesa: {
                TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS },
                Numero: 0,
                TipoRemesaDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },
                //TipoRemesa: {Codigo: }
                DetalleTarifaVenta: {},
                ProductoTransportado: '',
                Fecha: new Date(),
                Remitente: { Direccion: '' },
                Destinatario: { Direccion: '' },
            },
        }
        $scope.paginaActual = 1;
        $scope.cantidadRegistrosPorPagina = 20;
        $scope.MapaSitio = [{ Nombre: 'Paqueteria' }, { Nombre: 'Procesos' }, { Nombre: 'Cargue Remesas' }];

        $scope.AutocompleteSitiosClienteDescargue2 = function (value, cliente, ciudad) {
            $scope.ListadoSitiosDescargueAlterno2 = []
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Cliente: { Codigo: cliente.Codigo },
                        CiudadCargue: { Codigo: ciudad.Codigo },
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    for (var i = 0; i < Response.Datos.length; i++) {
                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                    }
                    $scope.ListadoSitiosDescargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoSitiosDescargueAlterno2)
                }
            }
            return $scope.ListadoSitiosDescargueAlterno2
        }

        $scope.AutocompleteZonasCiudades = function (value,ciudad) {
            $scope.ListadoZonasCiudades = []
            //if (value.length > 0) {
               // if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = ZonasFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,                        
                        Ciudad: { Codigo: ciudad.Codigo },                        
                        Sync: true
                    })
                    //for (var i = 0; i < Response.Datos.length; i++) {
                    //    Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                    //}
                    $scope.ListadoZonasCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoZonasCiudades)
               // }
            //}
            return $scope.ListadoZonasCiudades
        }
        /*-------------------------------------------------------------------------------------------Cargar Combos----------------------------------------------------------------*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoFormaPago = [];
                    if (response.data.Datos.length > 0) {
                        for (var i = 0; i < response.data.Datos.length; i++) {
                            if (response.data.Datos[i].Codigo != CODIGO_CATALOGO_FORMA_PAGO_VENTAS_NO_APLICA) {
                                $scope.ListadoFormaPago.push(response.data.Datos[i])
                            }
                        }
                    }
                    else {
                        $scope.ListadoFormaPago = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de tipo entrega*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoEntregaRemesaPaqueteria = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoEntregaRemesaPaqueteria = response.data.Datos;
                    }
                }
            }, function (response) {
            });
        /*Cargar Autocomplete de CLIENTES*/
        TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaClientes = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListaClientes = response.data.Datos;
                    }
                    else {
                        $scope.ListaClientes = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Autocomplete de productos*/
        ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, AplicaTarifario: -1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoProductoTransportados = []
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoProductoTransportados = response.data.Datos;
                    }
                    else {
                        $scope.ListadoProductoTransportados = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });

        /*Cargar el combo de tipo identificaciones*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListadoTipoIdentificacion = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoTipoIdentificacion = response.data.Datos;
                    }
                    else {
                        $scope.ListadoTipoIdentificacion = []
                    }
                }
            }, function (response) {
            });
        /*Cargar el combo de líneas negocio paquetería*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 216 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaLineasNegocioPaqueteria = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaLineasNegocioPaqueteria = response.data.Datos;
                    }
                    else {
                        $scope.ListaLineasNegocioPaqueteria = []
                    }
                }
            }, function (response) {
            });
        

        /*Cargar el combo de Horarios Entrega*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 217 } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaHorariosEntrega = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaHorariosEntrega = response.data.Datos;
                    }
                    else {
                        $scope.ListaHorariosEntrega = []
                    }
                }
            }, function (response) {
            });
        $scope.ListaRecogerOficinaDestino = [];
        $scope.ListaRecogerOficinaDestino = [
            { Codigo: 0, Nombre: 'NO' },
            { Codigo: 1, Nombre: 'SI' }
        ];
        /*Ciudades*/
        CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    if (response.data.Datos.length > 0) {
                        $scope.ListadoCiudades = response.data.Datos;
                    }
                    else {
                        $scope.ListadoCiudades = [];
                    }
                }
            }, function (response) {
                ShowError(response.statusText);
            });
        /*Estado GUÍA*/
        ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_ESTADO_REMESA_PAQUETERIA } }).
            then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    $scope.ListaEstadosGuia = [];
                    if (response.data.Datos.length > 0) {
                        $scope.ListaEstadosGuia = response.data.Datos;

                        $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                        $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                        $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                        if ($scope.CodigoEstadoGuia !== undefined && $scope.CodigoEstadoGuia !== null) {
                            $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + $scope.CodigoEstadoGuia);

                        } else {
                            if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_PROPIA) {
                                $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_OFICINA_ORIGEN);

                                $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                    if ($scope.ListaOficinas.length > 0) {
                                        $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                    }
                                }
                                $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                            }
                            if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_CLIENTE) {
                                $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_RECOGIDA);


                                $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                    if ($scope.ListaOficinas.length > 0) {
                                        $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                    }
                                }

                                $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;

                            }
                            if ($scope.Sesion.UsuarioAutenticado.Oficinas.CodigoTipoOficina == TIPO_OFICINA_RECEPTORIA) {
                                $scope.Modelo.EstadoGuia = $linq.Enumerable().From($scope.ListaEstadosGuia).First('$.Codigo ==' + ESTADO_GUIA_RECOGIDA);

                                $scope.Modelo.OficinaOrigen = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                $scope.Modelo.OficinaDestino = $scope.Sesion.UsuarioAutenticado.Oficinas;
                                if ($scope.ListaOficinas !== undefined && $scope.ListaOficinas !== null) {
                                    if ($scope.ListaOficinas.length > 0) {
                                        $scope.Modelo.OficinaDestino = $linq.Enumerable().From($scope.ListaOficinas).First('$.Codigo ==' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                                    }
                                }
                                $scope.Modelo.OficinaActual = $scope.Sesion.UsuarioAutenticado.Oficinas;
                            }
                        }


                    }
                    else {
                        $scope.ListaEstadosGuia = []
                    }
                }
            }, function (response) {
            });
        /*--------------------------------------------------------------------------------Funciones paginación --------------------------------------------------------------------*/
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 20) {
                $scope.DataActual = []
                $scope.paginaActual = 1
                for (var i = 0; i < 20; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 20
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 20
                if (a < $scope.totalRegistros) {
                    $scope.DataActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataActual.push($scope.DataArchivo[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 20 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 20
                $scope.DataActual = []
                for (var i = a - 20; i < $scope.DataArchivo.length; i++) {
                    $scope.DataActual.push($scope.DataArchivo[i])
                }
            }
        }

        //Paginacion MOdal de errores

        $scope.PrimerPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 20) {
                $scope.DataErrorActual = []
                $scope.paginaActualError = 1
                for (var i = 0; i < 20; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }
        $scope.AnteriorModalError = function () {
            if ($scope.paginaActualError > 1) {
                $scope.paginaActualError -= 1
                var a = $scope.paginaActualError * 20
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteModalError = function () {
            if ($scope.paginaActualError < $scope.totalPaginasErrores) {
                $scope.paginaActualError += 1
                var a = $scope.paginaActualError * 20
                if (a < $scope.totalRegistrosErrores) {
                    $scope.DataErrorActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    $scope.UltimaPaginaModalError()

                }
            } else if ($scope.paginaActualError == $scope.totalPaginasErrores) {
                $scope.UltimaPaginaModalError()
            }
        }
        $scope.UltimaPaginaModalError = function () {
            if ($scope.totalRegistrosErrores > 20 && $scope.totalPaginasErrores > 1) {
                $scope.paginaActualError = $scope.totalPaginasErrores
                var a = $scope.paginaActualError * 20
                $scope.DataErrorActual = []
                for (var i = a - 20; i < $scope.ListaErrores.length; i++) {
                    $scope.DataErrorActual.push($scope.ListaErrores[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 20) {
                $scope.ProgramacionesNoGuardadasActual = []
                $scope.paginaActualNoGuardadas = 1
                for (var i = 0; i < 20; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }
        $scope.AnteriorNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas -= 1
                var a = $scope.paginaActualNoGuardadas * 20
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardados = function () {
            if ($scope.paginaActualNoGuardadas < $scope.totalPaginasNoGuardadas) {
                $scope.paginaActualNoGuardadas += 1
                var a = $scope.paginaActualNoGuardadas * 20
                if (a < $scope.totalRegistrosNoGuardadas) {
                    $scope.ProgramacionesNoGuardadasActual = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardados()

                }
            } else if ($scope.paginaActualNoGuardadas == $scope.totalPaginasNoGuardadas) {
                $scope.UltimaPaginaNoGuardados()
            }
        }
        $scope.UltimaPaginaNoGuardados = function () {
            if ($scope.totalRegistrosNoGuardadas > 20 && $scope.totalPaginasNoGuardadas > 1) {
                $scope.paginaActualNoGuardadas = $scope.totalPaginasNoGuardadas
                var a = $scope.paginaActualNoGuardadas * 20
                $scope.ProgramacionesNoGuardadasActual = []
                for (var i = a - 20; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadasActual.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
        }


        $scope.PrimerPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 20) {
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                $scope.paginaActualNoGuardadasRemplazo = 1
                for (var i = 0; i < 20; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }
        $scope.AnteriorNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo -= 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 20
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.SiguienteNoGuardadosRemplazo = function () {
            if ($scope.paginaActualNoGuardadasRemplazo < $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.paginaActualNoGuardadasRemplazo += 1
                var a = $scope.paginaActualNoGuardadasRemplazo * 20
                if (a < $scope.totalRegistrosNoGuardadasRemplazo) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo = []
                    for (var i = a - 20; i < a; i++) {
                        $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                    }
                }
                else {
                    $scope.UltimaPaginaNoGuardadosRemplazo()

                }
            } else if ($scope.paginaActualNoGuardadasRemplazo == $scope.totalPaginasNoGuardadasRemplazo) {
                $scope.UltimaPaginaNoGuardadosRemplazo()
            }
        }
        $scope.UltimaPaginaNoGuardadosRemplazo = function () {
            if ($scope.totalRegistrosNoGuardadasRemplazo > 20 && $scope.totalPaginasNoGuardadasRemplazo > 1) {
                $scope.paginaActualNoGuardadasRemplazo = $scope.totalPaginasNoGuardadasRemplazo
                var a = $scope.paginaActualNoGuardadasRemplazo * 20
                $scope.ProgramacionesNoGuardadasActualRemplazo = []
                for (var i = a - 20; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasActualRemplazo.push($scope.ProgramacionesNoGuardadasRemplazo[i])
                }
            }
        }


        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ListaErrores.length; i++) {
                    $scope.ListaErrores[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardaras = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = true
                }
            }
            else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                    $scope.ProgramacionesNoGuardadas[i].Omitido = false
                }
            }
        }
        $scope.MarcadDesmarcarTodoNoGuardadasRemplazo = function (item) {
            if ($scope.Option.name == 'Omitido') {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Omitido'
                }
            } else {
                for (var i = 0; i < $scope.ProgramacionesNoGuardadasRemplazo.length; i++) {
                    $scope.ProgramacionesNoGuardadasRemplazo[i].Option = 'Remplazado'
                }
            }

        }
        $scope.LimpiarData = function () {
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            $scope.DataVerificada = []
            $scope.DataActual = []
            $scope.DataErrorActual = []
            $scope.DataArchivo = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            $scope.VerErrores = false

        }

        $scope.Archivo = '';
        $scope.ModalConfirmacionNuevoProceso = function () {
            showModal('modalConfirmacionCancelarProceso');
        }
        $scope.CerrarModalNuevoProceso = function (e) {
            var fileVal = document.getElementById("fileModal");
            fileVal.value = '';
            fileVal = document.getElementById("filePrincipal");
            fileVal.value = '';
            closeModal('modalConfirmacionCancelarProceso');


        }
        /*-----------------------------------------------------------------------funciones de lectura y apertura de archivos----------------------------------------------------------------------------*/

        $scope.ListaProgramaciones = []
        $scope.DataArchivo = []
        var X = XLSX;
        function to_json(workbook) {
            var result = {};
            workbook.SheetNames.forEach(function (sheetName) {
                var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            return result;
        }
        function fixdata(data) {
            var o = "", l = 0, w = 10240;
            for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
            o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
            return o;
        }
        function Eval(item) {
            if (item == '0' || item == undefined) {
                return true
            } else {
                return false
            }
        }
        function AsignarValoresTabla() {
            $scope.NumeroCargue = 0

            if ($scope.DataArchivo.Valores.length > 0) {
                var DataTemporal = []
                for (var i = 0; i < $scope.DataArchivo.Valores.length; i++) {
                    var item = $scope.DataArchivo.Valores[i]

                    if (!Eval(item.Tipo_Entrega) || !Eval(item.Ciudad_Remitente) || !Eval(item.Ciudad_Destinatario)
                    ) {
                        DataTemporal.push(item)
                    }
                }
                $scope.DataArchivo = DataTemporal
                $scope.totalRegistros = ($scope.DataArchivo.length)
                $scope.paginaActual = 1
                $scope.totalPaginas = Math.ceil($scope.totalRegistros / 20);
                blockUI.start();
                blockUI.message('Configurando campos, Esto puede tardar algunos minutos, por favor espere...');
                $timeout(function () {
                    try {
                        var data = []
                        for (var i = 0; i < $scope.DataArchivo.length; i++) {
                            if ($scope.DataArchivo[i].Tipo_Entrega !== undefined && $scope.DataArchivo[i].Tipo_Entrega !== '' && $scope.DataArchivo[i].Tipo_Entrega !== '0') {
                                try { $scope.DataArchivo[i].TipoEntregaRemesaPaqueteria = $linq.Enumerable().From($scope.ListadoTipoEntregaRemesaPaqueteria).First('$.Codigo ==' + $scope.DataArchivo[i].Tipo_Entrega); } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].No_PreImpreso !== undefined && $scope.DataArchivo[i].No_PreImpreso !== '' && $scope.DataArchivo[i].No_PreImpreso !== '0') {
                                try { $scope.DataArchivo[i].No_PreImpreso = $scope.DataArchivo[i].No_PreImpreso } catch (e) { }
                            }
                            $scope.DataArchivo[i].Remitente = {}
                            if ($scope.DataArchivo[i].Cliente !== undefined && $scope.DataArchivo[i].Cliente !== '' && $scope.DataArchivo[i].Cliente !== '0') {
                                try { $scope.DataArchivo[i].Cliente = $linq.Enumerable().From($scope.ListaClientes).First('$.Codigo ==' + $scope.DataArchivo[i].Cliente); $scope.DataArchivo[i].validartarifario = true; } catch (e) { }
                            } else {
                                $scope.DataArchivo[i].validartarifario = false;
                            }
                            if ($scope.DataArchivo[i].Tipo_Identificacion_Remitente !== undefined && $scope.DataArchivo[i].Tipo_Identificacion_Remitente !== '' && $scope.DataArchivo[i].Tipo_Identificacion_Remitente !== '0') {
                                try { $scope.DataArchivo[i].Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.DataArchivo[i].Tipo_Identificacion_Remitente); } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Idnt_Remitente !== undefined && $scope.DataArchivo[i].Idnt_Remitente !== '' && $scope.DataArchivo[i].Idnt_Remitente !== '0') {
                                try { $scope.DataArchivo[i].Remitente.NumeroIdentificacion = MascaraNumero($scope.DataArchivo[i].Idnt_Remitente) } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Nombre_Remitente !== undefined && $scope.DataArchivo[i].Nombre_Remitente !== '' && $scope.DataArchivo[i].Nombre_Remitente !== '0') {
                                try { $scope.DataArchivo[i].Remitente.Nombre = $scope.DataArchivo[i].Nombre_Remitente.toUpperCase() } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Ciudad_Remitente !== undefined && $scope.DataArchivo[i].Ciudad_Remitente !== '' && $scope.DataArchivo[i].Ciudad_Remitente !== '0') {
                                try { $scope.DataArchivo[i].Remitente.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + $scope.DataArchivo[i].Ciudad_Remitente); } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Barrio_Remitente !== undefined && $scope.DataArchivo[i].Barrio_Remitente !== '' && $scope.DataArchivo[i].Barrio_Remitente !== '0') {
                                try { $scope.DataArchivo[i].BarrioRemitente = $scope.DataArchivo[i].Barrio_Remitente.toUpperCase() } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Direccion_Remitente !== undefined && $scope.DataArchivo[i].Direccion_Remitente !== '' && $scope.DataArchivo[i].Direccion_Remitente !== '0') {
                                try { $scope.DataArchivo[i].Remitente.Direccion = $scope.DataArchivo[i].Direccion_Remitente.toUpperCase() } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Codigo_Postal_Remitente !== undefined && $scope.DataArchivo[i].Codigo_Postal_Remitente !== '' && $scope.DataArchivo[i].Codigo_Postal_Remitente !== '0') {
                                try { $scope.DataArchivo[i].CodigoPostalRemitente = $scope.DataArchivo[i].Codigo_Postal_Remitente.toUpperCase() } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Telefono_Remitente !== undefined && $scope.DataArchivo[i].Telefono_Remitente !== '' && $scope.DataArchivo[i].Telefono_Remitente !== '0') {
                                try { $scope.DataArchivo[i].Remitente.Telefonos = MascaraNumero($scope.DataArchivo[i].Telefono_Remitente) } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Observaciones_Remitente !== undefined && $scope.DataArchivo[i].Observaciones_Remitente !== '' && $scope.DataArchivo[i].Observaciones_Remitente !== '0') {
                                try { $scope.DataArchivo[i].ObservacionesRemitente = $scope.DataArchivo[i].Observaciones_Remitente.toUpperCase() } catch (e) { }
                            }
                            $scope.DataArchivo[i].Destinatario = {}
                            if ($scope.DataArchivo[i].Tipo_Identificacion_Destinatario !== undefined && $scope.DataArchivo[i].Tipo_Identificacion_Destinatario !== '' && $scope.DataArchivo[i].Tipo_Identificacion_Destinatario !== '0') {
                                try { $scope.DataArchivo[i].Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.DataArchivo[i].Tipo_Identificacion_Destinatario); } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Idnt_Destinatario !== undefined && $scope.DataArchivo[i].Idnt_Destinatario !== '' && $scope.DataArchivo[i].Idnt_Destinatario !== '0') {
                                try { $scope.DataArchivo[i].Destinatario.NumeroIdentificacion = MascaraNumero($scope.DataArchivo[i].Idnt_Destinatario) } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Nombre_Destinatario !== undefined && $scope.DataArchivo[i].Nombre_Destinatario !== '' && $scope.DataArchivo[i].Nombre_Destinatario !== '0') {
                                try { $scope.DataArchivo[i].Destinatario.Nombre = $scope.DataArchivo[i].Nombre_Destinatario.toUpperCase() } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Ciudad_Destinatario !== undefined && $scope.DataArchivo[i].Ciudad_Destinatario !== '' && $scope.DataArchivo[i].Ciudad_Destinatario !== '0') {
                                try { $scope.DataArchivo[i].Destinatario.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + $scope.DataArchivo[i].Ciudad_Destinatario); } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Zona_Destinatario !== undefined && $scope.DataArchivo[i].Zona_Destinatario !== '' && $scope.DataArchivo[i].Zona_Destinatario !== '0') {
                                try { $scope.DataArchivo[i].ZonaDestinatario = ZonasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: parseInt($scope.DataArchivo[i].Zona_Destinatario), Sync:true }).Datos[0] } catch (e) { console.log(e);}
                            }
                            if ($scope.DataArchivo[i].Latitud !== undefined && $scope.DataArchivo[i].Latitud !== '' && $scope.DataArchivo[i].Latitud !== '0') {
                                try { $scope.DataArchivo[i].Latitud = parseFloat($scope.DataArchivo[i].Latitud); } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Longitud !== undefined && $scope.DataArchivo[i].Longitud !== '' && $scope.DataArchivo[i].Longitud !== '0') {
                                try { $scope.DataArchivo[i].Longitud = parseFloat($scope.DataArchivo[i].Longitud); } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Horario_Entrega !== undefined && $scope.DataArchivo[i].Horario_Entrega !== '' && $scope.DataArchivo[i].Horario_Entrega !== '0') {
                                try { $scope.DataArchivo[i].Horario_Entrega = $linq.Enumerable().From($scope.ListaHorariosEntrega).First('$.Codigo ==' + parseInt($scope.DataArchivo[i].Horario_Entrega)); } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Barrio_Destinatario !== undefined && $scope.DataArchivo[i].Barrio_Destinatario !== '' && $scope.DataArchivo[i].Barrio_Destinatario !== '0') {
                                try { $scope.DataArchivo[i].BarrioDestinatario = $scope.DataArchivo[i].Barrio_Destinatario.toUpperCase() } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Direccion_Destinatario !== undefined && $scope.DataArchivo[i].Direccion_Destinatario !== '' && $scope.DataArchivo[i].Direccion_Destinatario !== '0') {
                                try { $scope.DataArchivo[i].Destinatario.Direccion = $scope.DataArchivo[i].Direccion_Destinatario.toUpperCase() } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Codigo_Postal_Destinatario !== undefined && $scope.DataArchivo[i].Codigo_Postal_Destinatario !== '' && $scope.DataArchivo[i].Codigo_Postal_Destinatario !== '0') {
                                try { $scope.DataArchivo[i].CodigoPostalDestinatario = $scope.DataArchivo[i].Codigo_Postal_Destinatario.toUpperCase() } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Telefono_Destinatario !== undefined && $scope.DataArchivo[i].Telefono_Destinatario !== '' && $scope.DataArchivo[i].Telefono_Destinatario !== '0') {
                                try { $scope.DataArchivo[i].Destinatario.Telefonos = MascaraNumero($scope.DataArchivo[i].Telefono_Destinatario) } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Observaciones_Destinatario !== undefined && $scope.DataArchivo[i].Observaciones_Destinatario !== '' && $scope.DataArchivo[i].Observaciones_Destinatario !== '0') {
                                try { $scope.DataArchivo[i].ObservacionesDestinatario = $scope.DataArchivo[i].Observaciones_Destinatario.toUpperCase() } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Forma_Pago !== undefined && $scope.DataArchivo[i].Forma_Pago !== '' && $scope.DataArchivo[i].Forma_Pago !== '0') {
                                try { $scope.DataArchivo[i].FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.DataArchivo[i].Forma_Pago); } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Peso !== undefined && $scope.DataArchivo[i].Peso !== '' && $scope.DataArchivo[i].Peso !== '0') {
                               
                                try { $scope.DataArchivo[i].PesoCliente = parseFloat($scope.DataArchivo[i].Peso) } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Unidades !== undefined && $scope.DataArchivo[i].Unidades !== '' && $scope.DataArchivo[i].Unidades !== '0') {
                                try { $scope.DataArchivo[i].CantidadCliente = parseFloat($scope.DataArchivo[i].Unidades) } catch (e) { }
                            }

                            if ($scope.DataArchivo[i].Largo !== undefined && $scope.DataArchivo[i].Largo !== '' && $scope.DataArchivo[i].Largo !== '0') {
                                try { $scope.DataArchivo[i].Largo = parseFloat($scope.DataArchivo[i].Largo) } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Alto !== undefined && $scope.DataArchivo[i].Alto !== '' && $scope.DataArchivo[i].Alto !== '0') {
                                try { $scope.DataArchivo[i].Alto = parseFloat($scope.DataArchivo[i].Alto) } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Ancho !== undefined && $scope.DataArchivo[i].Ancho !== '' && $scope.DataArchivo[i].Ancho !== '0') {
                                try { $scope.DataArchivo[i].Ancho = parseFloat($scope.DataArchivo[i].Ancho) } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Peso_Volumetrico !== undefined && $scope.DataArchivo[i].Peso_Volumetrico !== '' && $scope.DataArchivo[i].Peso_Volumetrico !== '0') {
                                try { $scope.DataArchivo[i].Peso_Volumetrico = parseFloat($scope.DataArchivo[i].Peso_Volumetrico) } catch (e) { }
                            }

                            if ($scope.DataArchivo[i].Linea_Negocio_Paqueteria !== undefined && $scope.DataArchivo[i].Linea_Negocio_Paqueteria !== '' && $scope.DataArchivo[i].Linea_Negocio_Paqueteria !== '0') {
                                try { $scope.DataArchivo[i].Linea_Negocio_Paqueteria = $linq.Enumerable().From($scope.ListaLineasNegocioPaqueteria).First('$.Codigo ==' + $scope.DataArchivo[i].Linea_Negocio_Paqueteria) } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Recoger_Oficina_Destino !== undefined && $scope.DataArchivo[i].Recoger_Oficina_Destino !== '') {
                                try { $scope.DataArchivo[i].Recoger_Oficina_Destino = $linq.Enumerable().From($scope.ListaRecogerOficinaDestino).First('$.Codigo ==' + $scope.DataArchivo[i].Recoger_Oficina_Destino) } catch (e) { }
                            } else {
                                try { $scope.DataArchivo[i].Recoger_Oficina_Destino = $linq.Enumerable().From($scope.ListaRecogerOficinaDestino).First('$.Codigo == 0') } catch (e) { }
                            }

                            if ($scope.DataArchivo[i].Valor_Comercial !== undefined && $scope.DataArchivo[i].Valor_Comercial !== '' && $scope.DataArchivo[i].Valor_Comercial !== '0') {
                                try { $scope.DataArchivo[i].ValorComercialCliente = MascaraValores($scope.DataArchivo[i].Valor_Comercial) } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Producto !== undefined && $scope.DataArchivo[i].Producto !== '' && $scope.DataArchivo[i].Producto !== '0') {
                                try { $scope.DataArchivo[i].ProductoTransportado = $linq.Enumerable().From($scope.ListadoProductoTransportados).First('$.Codigo ==' + $scope.DataArchivo[i].Producto); } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Descripcion !== undefined && $scope.DataArchivo[i].Descripcion !== '' && $scope.DataArchivo[i].Descripcion !== '0') {
                                try { $scope.DataArchivo[i].DescripcionProductoTransportado = MascaraMayus($scope.DataArchivo[i].Descripcion) } catch (e) { }
                            }


                            if ($scope.DataArchivo[i].Documento_Cliente !== undefined && $scope.DataArchivo[i].Documento_Cliente !== '' && $scope.DataArchivo[i].Documento_Cliente !== '0') {
                                try { $scope.DataArchivo[i].NumeroDocumentoCliente = MascaraMayus($scope.DataArchivo[i].Documento_Cliente) } catch (e) { }
                            }
                            if ($scope.DataArchivo[i].Centro_Costo !== undefined && $scope.DataArchivo[i].Centro_Costo !== '' && $scope.DataArchivo[i].Centro_Costo !== '0') {
                                try { $scope.DataArchivo[i].CentroCosto = MascaraMayus($scope.DataArchivo[i].Centro_Costo) } catch (e) { }
                            }
                            try {
                                if ($scope.DataArchivo[i].Linea_Servicio !== undefined && $scope.DataArchivo[i].Linea_Servicio !== '' && $scope.DataArchivo[i].Linea_Servicio !== '0') {
                                    try { $scope.DataArchivo[i].LineaSerivicioCliente = $scope.DataArchivo[i].Linea_Servicio } catch (e) { }
                                }
                            } catch (e) {

                            }
                            try {
                                if ($scope.DataArchivo[i].Sitio_Entrega !== undefined && $scope.DataArchivo[i].Sitio_Entrega !== '' && $scope.DataArchivo[i].Sitio_Entrega !== '0') {
                                    // try { $scope.DataArchivo[i].SitioDescargue = parseInt($scope.DataArchivo[i].Sitio_Entrega) } catch (e) { }
                                    try { $scope.DataArchivo[i].SitioDescargue = SitiosTerceroClienteFactory.ConsultarOrdenServicio({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: parseInt($scope.DataArchivo[i].Sitio_Entrega), Sync: true }).Datos[0].SitioCliente} catch (e) { console.log("error: " + e) }
                                    try { $scope.DataArchivo[i].SitioDescargue.DireccionSitio = SitiosTerceroClienteFactory.ConsultarOrdenServicio({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: parseInt($scope.DataArchivo[i].Sitio_Entrega), Sync: true }).Datos[0].DireccionSitio } catch (e) { }
                                    try { $scope.DataArchivo[i].SitioDescargue.Telefono = SitiosTerceroClienteFactory.ConsultarOrdenServicio({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: parseInt($scope.DataArchivo[i].Sitio_Entrega), Sync: true }).Datos[0].Telefono } catch (e) { }
                                }
                            } catch (e) {

                            }
                            try {
                                if ($scope.DataArchivo[i].Fecha_Entrega !== undefined && $scope.DataArchivo[i].Fecha_Entrega !== '' && $scope.DataArchivo[i].Fecha_Entrega !== '0') {
                                    try { $scope.DataArchivo[i].FechaEntrega = new Date($scope.DataArchivo[i].Fecha_Entrega) } catch (e) { }
                                }
                            } catch (e) {

                            }
                            $scope.DataArchivo[i].Pos = i;

                            $scope.TC = {
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                Codigo: $scope.DataArchivo[i].validartarifario == true ? $scope.DataArchivo[i].Cliente.Codigo : 0,
                                CodigoLineaNegocio: CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA,   
                                CodigoCiudadOrigen: $scope.DataArchivo[i].Ciudad_Remitente,
                                CodigoCiudadDestino: $scope.DataArchivo[i].Ciudad_Destinatario,
                                TarifarioProveedores: false,
                                TarifarioClientes: true,
                                Sync : true
                            }
                            
                            //var RTarifa = TercerosFactory.Consultar($scope.TC);
                            //for (var h = 0; h < RTarifa.Datos.length; h++) {
                            //    var InsertaT = true;
                            //    if ($scope.DataArchivo[i].ListaTarifaCarga != undefined && $scope.DataArchivo[i].ListaTarifaCarga != null) {
                            //        for (var j = 0; j < $scope.DataArchivo[i].ListaTarifaCarga.length; j++) {

                            //            if ($scope.DataArchivo[i].ListaTarifaCarga[j].Codigo == RTarifa.Datos[h].TarifaCarga.Codigo) {
                            //                InsertaT = false;
                            //            }
                            //        }
                                    
                            //    } else {
                            //        $scope.DataArchivo[i].ListaTarifaCarga = [];
                            //        $scope.DataArchivo[i].ListaTarifaCarga.push(RTarifa.Datos[h].TarifaCarga)
                            //        InsertaT = false;
                            //    }
                            //    if (InsertaT == true) {
                            //        $scope.DataArchivo[i].ListaTarifaCarga.push(RTarifa.Datos[h].TarifaCarga)
                            //    }
                            //}
                            $scope.DataArchivo[i].ListaTarifaCarga = [
                                { Codigo : 200, Nombre: 'Rango Pesos Valor Kilo' },
                                { Codigo : 201, Nombre: 'Rango Pesos Valor Fijo' }
                            ];
                        }
                        if ($scope.totalRegistros > 20) {
                            for (var i = 0; i < 20; i++) {
                                $scope.DataActual.push($scope.DataArchivo[i])
                            }
                        } else {
                            $scope.DataActual = $scope.DataArchivo
                        }
                        if ($scope.DataArchivo.length > 0) {
                            //ShowSuccess('La lectura del archivo se realizó correctamente')
                            //                            blockUI.start();
                            //$scope.ValidarRemtientes(0)
                            blockUI.stop();
                        } else {
                            ShowError('El archivo seleccionado no contiene datos para validar, por favor intente con otro archivo')
                            blockUI.stop()
                            $timeout(blockUI.stop(), 1000)

                        }
                    } catch (e) {
                        ShowError('Error en la lectura del archivo por favor intente nuevamente')
                        blockUI.stop()
                        $timeout(blockUI.stop(), 1000)
                    }


                }, 1000)
            }

        }
        var conterroresprevalidar = 0
        $scope.ValidarRemtientes = function (inicio) {
            if (inicio + 1 <= $scope.DataArchivo.length) {
                $timeout(function () {
                    blockUI.message('Validando remitentes ' + (inicio + 1) + ' de ' + $scope.DataArchivo.length);
                }, 100);
                if ($scope.DataArchivo[inicio].Cliente.Codigo !== null && $scope.DataArchivo[inicio].Cliente.Codigo != undefined && $scope.DataArchivo[inicio].Cliente.Codigo > 0) {
                    try {
                        var Filtro = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Codigo: $scope.DataArchivo[inicio].Cliente.Codigo,
                        }
                    } catch (e) {
                        var Filtro = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Codigo: 0,
                            NumeroIdentificacion: $scope.DataArchivo[inicio].Remitente.NumeroIdentificacion
                        }
                    }
                    //Remitente
                    TercerosFactory.Obtener(Filtro).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.Codigo > 0) {
                                    $scope.DataArchivo[inicio].Remitente.Codigo = response.data.Datos.Codigo
                                    $scope.DataArchivo[inicio].Remitente.NumeroIdentificacion = response.data.Datos.NumeroIdentificacion
                                    $scope.DataArchivo[inicio].Remitente.Nombre = response.data.Datos.NombreCompleto
                                    $scope.DataArchivo[inicio].Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.TipoIdentificacion.Codigo);
                                    $scope.DataArchivo[inicio].CodigoPostalRemitente = response.data.Datos.CodigoPostal;
                                    $scope.DataArchivo[inicio].BarrioRemitente = response.data.Datos.Barrio;
                                    $scope.DataArchivo[inicio].Cliente = $scope.CargarTercero(response.data.Datos.Codigo);
                                    if ($scope.DataArchivo[inicio].Remitente.Direccion == undefined || $scope.DataArchivo[inicio].Remitente.Direccion == '') {
                                        $scope.DataArchivo[inicio].Remitente.Direccion = response.data.Datos.Direccion
                                    }
                                    if ($scope.DataArchivo[inicio].Remitente.Telefonos == undefined || $scope.DataArchivo[inicio].Remitente.Telefonos == '') {
                                        $scope.DataArchivo[inicio].Remitente.Telefonos = response.data.Datos.Telefonos.replace(';', ' - ')
                                    }
                                    if ($scope.DataArchivo[inicio].Remitente.Ciudad == undefined || $scope.DataArchivo[inicio].Remitente.Ciudad == '') {
                                        $scope.DataArchivo[inicio].Remitente.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + response.data.Datos.Ciudad.Codigo);
                                    }
                                    if ($scope.DataArchivo[inicio].Cliente !== undefined && $scope.DataArchivo[inicio].Cliente !== '' && $scope.DataArchivo[inicio].Cliente !== null) {
                                        if (($scope.DataArchivo[inicio].Destinatario.NumeroIdentificacion == undefined || $scope.DataArchivo[inicio].Destinatario.NumeroIdentificacion == '') && $scope.DataArchivo[inicio].Cliente.Codigo > 0) {
                                            $scope.DataArchivo[inicio].Destinatario.Codigo = response.data.Datos.Codigo
                                            $scope.DataArchivo[inicio].Destinatario.NumeroIdentificacion = response.data.Datos.NumeroIdentificacion
                                            $scope.DataArchivo[inicio].Destinatario.Nombre = response.data.Datos.NombreCompleto
                                            $scope.DataArchivo[inicio].Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.TipoIdentificacion.Codigo);
                                            if ($scope.DataArchivo[inicio].Destinatario.Direccion == undefined || $scope.DataArchivo[inicio].Destinatario.Direccion == '') {
                                                $scope.DataArchivo[inicio].Destinatario.Direccion = response.data.Datos.Direccion
                                            }
                                            if ($scope.DataArchivo[inicio].Destinatario.Telefonos == undefined || $scope.DataArchivo[inicio].Destinatario.Telefonos == '') {
                                                $scope.DataArchivo[inicio].Destinatario.Telefonos = response.data.Datos.Telefonos.replace(';', ' - ')
                                            }
                                            if ($scope.DataArchivo[inicio].Destinatario.Ciudad == undefined || $scope.DataArchivo[inicio].Destinatario.Ciudad == '') {
                                                $scope.DataArchivo[inicio].Destinatario.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + response.data.Datos.Ciudad.Codigo);
                                            }
                                        }
                                    }
                                    try {
                                        $scope.DataArchivo[inicio].ListaLineaSerivicioCliente = response.data.Datos.LineaServicio
                                        $scope.DataArchivo[inicio].LineaSerivicioCliente = $linq.Enumerable().From($scope.DataArchivo[inicio].ListaLineaSerivicioCliente).First('$.Codigo ==' + $scope.DataArchivo[inicio].LineaSerivicioCliente);
                                    } catch (e) {

                                    }



                                }
                            }
                            $scope.ValidarRemtientes(inicio + 1)
                        }, function (response) {
                            $scope.ValidarRemtientes(inicio + 1)
                        });
                } else {
                    $scope.ValidarRemtientes(inicio + 1)
                }
            }
            else {
                $scope.ValidarDestinatarios(0)
            }
        }
        $scope.ValidarDestinatarios = function (inicio) {
            if (inicio + 1 <= $scope.DataArchivo.length) {

                $timeout(function () {
                    blockUI.message('Validando destinatarios ' + (inicio + 1) + ' de ' + $scope.DataArchivo.length);
                }, 100);
                if (!Eval($scope.DataArchivo[inicio].Destinatario.NumeroIdentificacion)) {
                    $scope.DataArchivo[inicio].Destinatario.Codigo = 0
                    var Filtro = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        NumeroIdentificacion: $scope.DataArchivo[inicio].Destinatario.NumeroIdentificacion
                    }
                    //destinatario
                    TercerosFactory.Obtener(Filtro).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                if (response.data.Datos.Codigo > 0) {
                                    $scope.DataArchivo[inicio].Destinatario.Codigo = response.data.Datos.Codigo
                                    $scope.DataArchivo[inicio].Destinatario.NumeroIdentificacion = response.data.Datos.NumeroIdentificacion
                                    $scope.DataArchivo[inicio].Destinatario.Nombre = response.data.Datos.NombreCompleto
                                    $scope.DataArchivo[inicio].Destinatario.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.TipoIdentificacion.Codigo);
                                    if ($scope.DataArchivo[inicio].Destinatario.Direccion == undefined || $scope.DataArchivo[inicio].Destinatario.Direccion == '') {
                                        $scope.DataArchivo[inicio].Destinatario.Direccion = response.data.Datos.Direccion
                                    }
                                    if ($scope.DataArchivo[inicio].Destinatario.Telefonos == undefined || $scope.DataArchivo[inicio].Destinatario.Telefonos == '') {
                                        $scope.DataArchivo[inicio].Destinatario.Telefonos = response.data.Datos.Telefonos.replace(';', ' - ')
                                    }
                                    if ($scope.DataArchivo[inicio].Destinatario.Ciudad == undefined || $scope.DataArchivo[inicio].Destinatario.Ciudad == '') {
                                        $scope.DataArchivo[inicio].Destinatario.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + response.data.Datos.Ciudad.Codigo);
                                    }
                                    try {
                                        blockUIConfig.autoBlock = false;
                                        var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            Cliente: { Codigo: $scope.DataArchivo[inicio].Destinatario.Codigo },
                                            //CiudadCargue: { Codigo: ciudad.Codigo },
                                            Codigo: $scope.DataArchivo[inicio].SitioDescargue.Codigo,
                                            Sync: true
                                        })
                                        for (var i = 0; i < Response.Datos.length; i++) {
                                            Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                                        }
                                        try {
                                            if (Response.Datos.lenght > 0) {
                                                $scope.DataArchivo[inicio].SitioDescargue = Response.Datos[0]
                                            }
                                            $scope.DataArchivo[inicio].Destinatario.Direccion = $scope.DataArchivo[inicio].SitioDescargue.DireccionSitio
                                            $scope.DataArchivo[inicio].Destinatario.Telefonos = $scope.DataArchivo[inicio].SitioDescargue.Telefono
                                        } catch (e) {

                                        }


                                        $scope.DataArchivo[inicio].ListadoSitiosDescargueAlterno2 = []
                                        $scope.DataArchivo[inicio].AutocompleteSitiosClienteDescargue = function (value, cliente, ciudad) {
                                            if (value.length > 0) {
                                                if ((value.length % 3) == 0 || value.length == 2) {
                                                    /*Cargar Autocomplete de propietario*/
                                                    blockUIConfig.autoBlock = false;
                                                    var Response = SitiosTerceroClienteFactory.ConsultarOrdenServicio({
                                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                                        Cliente: { Codigo: cliente.Codigo },
                                                        CiudadCargue: { Codigo: ciudad.Codigo },
                                                        ValorAutocomplete: value,
                                                        Sync: true
                                                    })
                                                    for (var i = 0; i < Response.Datos.length; i++) {
                                                        Response.Datos[i].Codigo = Response.Datos[i].SitioCliente.Codigo
                                                    }
                                                    $scope.DataArchivo[inicio].ListadoSitiosDescargueAlterno2 = ValidarListadoAutocomplete(Response.Datos, $scope.DataArchivo[inicio].ListadoSitiosDescargueAlterno2)
                                                }
                                            }
                                            return $scope.DataArchivo[inicio].ListadoSitiosDescargueAlterno2
                                        }
                                    } catch (e) {

                                    }
                                }
                            }
                            $scope.ValidarDestinatarios(inicio + 1)
                        }, function (response) {
                            $scope.ValidarDestinatarios(inicio + 1)
                        });
                } else {
                    $scope.ValidarDestinatarios(inicio + 1)
                }

            }
            else {
                $timeout(function () {
                    blockUI.stop();
                }, 1000);
                if (conterroresprevalidar > 0) {
                    ShowError('Se encontraron errores al validar los datos cargados')
                } else {
                    ShowSuccess('Los datos se verificaron correctamente')
                }
            }
        }
        function process_wb(wb) {
            $scope.DataArchivo = []
            $scope.ListaErrores = []
            $scope.DataActual = []
            $scope.DataArchivo = to_json(wb);
            if ($scope.DataArchivo.Valores != undefined) {
                $('#TablaCarge').show();
                $('#btnValida').show();

                $scope.DataArchivo = { Valores: $scope.DataArchivo.Valores }
                AsignarValoresTabla()


            }
            else {
                ShowError('El arhivo cargado no corresponde al formato de carge masivo de las programaciones')
            }
            blockUI.stop()

        }

        $scope.handleFile = function (e) {
            //e.files[0].name = e.files[0].name + "1";
            var files = e.files;
            var f = files[0];
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = $scope.CargarDocumento
                reader.readAsArrayBuffer(f);
            }
        }

        $scope.CargarDocumento = function (e) {
            $scope.$apply(function () {
                var data = e.target.result;
                $scope.arr = fixdata(data);
                blockUI.start();
                $timeout(function () {
                    blockUI.message('Subiendo Archivo, Por Favor Espere...');
                }, 1000);
                $timeout(function () {
                    try {
                        $scope.wb = X.read(btoa($scope.arr), { type: 'base64' });
                        process_wb($scope.wb);
                    } catch (e) {
                        ShowError('El arhivo seleccionado no corresponde a una hoja de cálculo válida, por favor intente con otro archivo')
                        blockUI.stop()

                    }
                }, 1500);
            });

        }

        function restrow(item) {
            item.stRow = ''
            item.stGeneral = ''
            item.msGeneral = ''
            item.stTipoEntregaRemesaPaqueteria = ''
            item.msTipoEntregaRemesaPaqueteria = ''
            item.stPreImpreso = ''
            item.msPreImpreso = ''
            item.stCliente = ''
            item.msCliente = ''
            item.stTipoIdentificacionRemitente = ''
            item.msTipoIdentificacionRemitente = ''
            item.stNumeroIdentificacionRemitente = ''
            item.msNumeroIdentificacionRemitente = ''
            item.stNombreRemitente = ''
            item.msNombreRemitente = ''
            item.stCiudadRemitente = ''
            item.msCiudadRemitente = ''
            item.stDireccionRemitente = ''
            item.msDireccionRemitente = ''
            item.stTelefonosRemitente = ''
            item.msTelefonosRemitente = ''
            item.stTipoIdentificacionDestinatario = ''
            item.msTipoIdentificacionDestinatario = ''
            item.stNumeroIdentificacionDestinatario = ''
            item.msNumeroIdentificacionDestinatario = ''
            item.stNombreDestinatario = ''
            item.msNombreDestinatario = ''
            item.stCiudadDestinatario = ''
            item.msCiudadDestinatario = ''
            item.stZonaCiudad = ''
            item.msZonaCiudad = ''
            item.stLatitud = ''
            item.msLatitud = ''
            item.stLongitud = ''
            item.msLongitud = ''
            item.stHorario_Entrega = ''
            item.msHorario_Entrega = ''
            item.stDireccionDestinatario = ''
            item.msDireccionDestinatario = ''
            item.stTelefonosDestinatario = ''
            item.msTelefonosDestinatario = ''
            item.stFormaPago = ''
            item.msFormaPago = ''
            item.stPesoCliente = ''
            item.msPesoCliente = ''
            item.stCantidadCliente = ''
            item.msCantidadCliente = ''
            item.stLargo = ''
            item.msLargo = ''
            item.stAlto = ''
            item.msAlto = ''
            item.stAncho = ''
            item.msAncho = ''
            item.stPeso_Volumetrico = ''
            item.msPeso_Volumetrico = ''
            item.stLinea_Negocio_Paqueteria = ''
            item.msLinea_Negocio_Paqueteria = ''
            item.stRecoger_Oficina_Destino = ''
            item.msRecoger_Oficina_Destino = ''
            item.stValorComercialCliente = ''
            item.msValorComercialCliente = ''
            item.stProductoTransportado = ''
            item.msProductoTransportado = ''
            item.stFechaEntrega = ''
            item.msFechaEntrega = ''
            item.stSitioEntrega = ''
            item.msSitioEntrega = ''
        }

        $scope.ValidarDocumento = function () {
            $scope.ListaErrores = []
            $scope.MensajesError = []
            /*
                        blockUI.start();
                        $timeout(function() {
                            blockUI.message('Validando Archivo');
                        }, 1000);*/
            var contadorgeneral = 0
            var contadorRegistros = 0
            $scope.DataVerificada = []
            $scope.DataVerificadatmp = []
            for (var i = 0; i < $scope.DataArchivo.length; i++) {
                var item = $scope.DataArchivo[i]
                item.ID = i
                //resetea todos los estilos de la fila que se va a evaluar
                restrow(item);
                var errores = 0
                //Condicion para saber si aplican las validaciones (Si el registro no tiene una ruta seleccionada no validara el resto de campos)
                if (item.Omitido !== true) {
                    contadorRegistros++
                    if (item.TipoEntregaRemesaPaqueteria == undefined || item.TipoEntregaRemesaPaqueteria == '') {
                        item.stTipoEntregaRemesaPaqueteria = 'background:red'
                        item.msTipoEntregaRemesaPaqueteria = 'Ingrese el tipo entrega'
                        errores++
                    }
                    //Remitente
                    if (item.Remitente.TipoIdentificacion == undefined || item.Remitente.TipoIdentificacion == '') {
                        item.stTipoIdentificacionRemitente = 'background:red'
                        item.msTipoIdentificacionRemitente = 'Ingrese el tipo de identificación'
                        errores++
                    } else {
                        if (item.Remitente.TipoIdentificacion.Codigo == 100) {
                            item.stTipoIdentificacionRemitente = 'background:red'
                            item.msTipoIdentificacionRemitente = 'Ingrese el tipo de identificación'
                            errores++
                        }
                    }
                    if (item.Remitente.NumeroIdentificacion == undefined || item.Remitente.NumeroIdentificacion == '' || item.Remitente.NumeroIdentificacion == null || isNaN(item.Remitente.NumeroIdentificacion)) {
                        item.stNumeroIdentificacionRemitente = 'background:red'
                        item.msNumeroIdentificacionRemitente = 'Ingrese el número de identificación'
                        errores++
                    }
                    if (item.Remitente.Nombre == undefined || item.Remitente.Nombre == '' || item.Remitente.Nombre == null) {
                        item.stNombreRemitente = 'background:red'
                        item.msNombreRemitente = 'Ingrese el nombre'
                        errores++
                    }
                    if (item.Remitente.Ciudad == undefined || item.Remitente.Ciudad == '' || item.Remitente.Ciudad == null) {
                        item.stCiudadRemitente = 'background:red'
                        item.msCiudadRemitente = 'Ingrese la ciudad'
                        errores++
                    }
                    if (item.Remitente.Direccion == undefined || item.Remitente.Direccion == '' || item.Remitente.Direccion == null) {
                        item.stDireccionRemitente = 'background:red'
                        item.msDireccionRemitente = 'Ingrese la dirección'
                        errores++
                    }
                    if (item.Remitente.Telefonos == undefined || item.Remitente.Telefonos == '' || item.Remitente.Telefonos == null) {
                        item.stTelefonosRemitente = 'background:red'
                        item.msTelefonosRemitente = 'Ingrese el teléfono'
                        errores++
                    }

                    //Destinatario
                    if (item.Destinatario.TipoIdentificacion == undefined || item.Destinatario.TipoIdentificacion == '') {
                        item.stTipoIdentificacionDestinatario = 'background:red'
                        item.msTipoIdentificacionDestinatario = 'Ingrese el tipo de identificación'
                        errores++
                    } else {
                        if (item.Destinatario.TipoIdentificacion.Codigo == 100) {
                            item.stTipoIdentificacionDestinatario = 'background:red'
                            item.msTipoIdentificacionDestinatario = 'Ingrese el tipo de identificación'
                            errores++
                        }
                    }
                    if (item.Destinatario.NumeroIdentificacion == undefined || item.Destinatario.NumeroIdentificacion == '' || item.Destinatario.NumeroIdentificacion == null || isNaN(item.Destinatario.NumeroIdentificacion)) {
                        item.stNumeroIdentificacionDestinatario = 'background:red'
                        item.msNumeroIdentificacionDestinatario = 'Ingrese el número de identificación'
                        errores++
                    }
                    if (item.Destinatario.Nombre == undefined || item.Destinatario.Nombre == '' || item.Destinatario.Nombre == null) {
                        item.stNombreDestinatario = 'background:red'
                        item.msNombreDestinatario = 'Ingrese el nombre'
                        errores++
                    }
                    if (item.Destinatario.Ciudad == undefined || item.Destinatario.Ciudad == '' || item.Destinatario.Ciudad == null) {
                        item.stCiudadDestinatario = 'background:red'
                        item.msCiudadDestinatario = 'Ingrese la ciudad'
                        errores++
                    }
                    /*if (item.SitioDescargue == undefined || item.SitioDescargue == '' || item.SitioDescargue == null) {
                        item.stSitioEntrega = 'background:red'
                        item.msSitioEntrega = 'Ingrese el sitio de entrega'
                        errores++
                    }*/
                    if (item.Destinatario.Direccion == undefined || item.Destinatario.Direccion == '' || item.Destinatario.Direccion == null) {
                        item.stDireccionDestinatario = 'background:red'
                        item.msDireccionDestinatario = 'Ingrese la dirección'
                        errores++
                    } else {
                        if (item.SitioDescargue == undefined || item.SitioDescargue == '' || item.SitioDescargue == null) {

                        }
                    }
                    if (item.Destinatario.Telefonos == undefined || item.Destinatario.Telefonos == '' || item.Destinatario.Telefonos == null) {
                        item.stTelefonosDestinatario = 'background:red'
                        item.msTelefonosDestinatario = 'Ingrese el teléfono'
                        errores++
                    }
                    try {
                        if (item.Remitente.Ciudad.Codigo > 0 && item.Destinatario.Ciudad.Codigo > 0) {
                            if (item.Remitente.Ciudad.Codigo !== item.Destinatario.Ciudad.Codigo) {
                                item.TipoRemesa = CODIGO_TIPO_REMESA_NACIONAL;
                            } else {
                                item.TipoRemesa = CODIGO_TIPO_REMESA_URBANA;
                            }
                        }
                    } catch (e) {

                    }

                    //Valores
                    if (item.FormaPago == undefined || item.FormaPago == '') {
                        item.stFormaPago = 'background:red'
                        item.msFormaPago = 'Ingrese la forma de pago'
                        errores++
                    }
                    if ($scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria && (item.Peso_Volumetrico == undefined && (item.PesoCliente == undefined || item.PesoCliente == null || item.PesoCliente == '' || isNaN(item.PesoCliente)) || item.Peso_Volumetrico == '' && (item.PesoCliente == undefined || item.PesoCliente == null || item.PesoCliente == '' || isNaN(item.PesoCliente)) || isNaN(item.Peso_Volumetrico) && (item.PesoCliente == undefined || item.PesoCliente == null || item.PesoCliente == '' || isNaN(item.PesoCliente)))) {
                        item.stPeso_Volumetrico = 'background:red'
                        item.msPeso_Volumetrico = 'Ingrese el Peso Volumétrico'
                        errores++
                    }
                    if (item.PesoCliente == undefined || item.PesoCliente == '' || isNaN(item.PesoCliente)) {
                        if (item.Alto == undefined || item.Alto == '' || isNaN(item.Alto)) {
                            item.stAlto = 'background:red'
                            item.msAlto = 'Ingrese el alto'
                            errores++
                        }
                        if (item.Largo == undefined || item.Largo == '' || isNaN(item.Largo)) {
                            item.stLargo = 'background:red'
                            item.msLargo = 'Ingrese el largo'
                            errores++
                        }
                        if (item.Ancho == undefined || item.Ancho == '' || isNaN(item.Ancho)) {
                            item.stAncho = 'background:red'
                            item.msAncho = 'Ingrese el ancho'
                            errores++
                        }
                        if (item.Peso_Volumetrico == undefined || item.Peso_Volumetrico == '' || isNaN(item.Peso_Volumetrico)) {
                            item.stPeso_Volumetrico = 'background:red'
                            item.msPeso_Volumetrico = 'Ingrese el Peso Volumétrico'
                            errores++
                        }
                        //item.stPesoCliente = 'background:red'
                        //item.msPesoCliente = 'Ingrese el peso'
                        //errores++
                    }
                    if (item.CantidadCliente == undefined || item.CantidadCliente == '' || isNaN(item.CantidadCliente)) {
                        item.stCantidadCliente = 'background:red'
                        item.msCantidadCliente = 'Ingrese la cantidad'
                        errores++
                    }

                    if (item.Linea_Negocio_Paqueteria == undefined || item.Linea_Negocio_Paqueteria == '' || isNaN(item.Linea_Negocio_Paqueteria.Codigo)) {
                        item.stLinea_Negocio_Paqueteria = 'background:red'
                        item.msLinea_Negocio_Paqueteria = 'Ingrese la línea negocio paquetería'
                        errores++
                    }

                    if (item.Recoger_Oficina_Destino == undefined || item.Recoger_Oficina_Destino == '' || isNaN(item.Recoger_Oficina_Destino.Codigo)) {
                        item.stRecoger_Oficina_Destino = 'background:red'
                        item.msRecoger_Oficina_Destino = 'Ingrese una opción para recoger oficina destino'
                        errores++
                    }

                    if (item.ValorComercialCliente == undefined || item.ValorComercialCliente == '') {
                        item.stValorComercialCliente = 'background:red'
                        item.msValorComercialCliente = 'Ingrese el valor'
                        errores++
                    }
                    if (item.ProductoTransportado == undefined || item.ProductoTransportado == '') {
                        item.stProductoTransportado = 'background:red'
                        item.msProductoTransportado = 'Ingrese el producto'
                        errores++
                    }
                    if (item.FechaEntrega == undefined || item.FechaEntrega == '') {
                        item.stFechaEntrega = 'background:red'
                        item.msFechaEntrega = 'Ingrese la Fecha Entrega'
                        errores++
                    } else {
                        if (item.FechaEntrega <= new Date()) {
                            item.stFechaEntrega = 'background:red'
                            item.msFechaEntrega = 'La fecha de entrega debe ser mayor a la fecha actual'
                            errores++
                        }
                    }
                    if (errores > 0) {
                        item.stRow = 'background:yellow'
                        $scope.ListaErrores.push(item)
                        contadorgeneral++
                    }
                    else {
                        item.stRow = ''
                        $scope.DataVerificadatmp.push(item)
                    }
                }
                else {
                    restrow(item);
                }
            }
            //blockUI.stop();

            if (contadorgeneral > 0) {
                ShowError('Se encontraron inconsistencias en los datos ingresados, por favor corrija los datos marcados o seleccione omitirlos.');
                $scope.paginaActualError = 1
                $scope.DataErrorActual = []
                $scope.totalRegistrosErrores = $scope.ListaErrores.length
                $scope.totalPaginasErrores = Math.ceil($scope.totalRegistrosErrores / 20);
                if ($scope.totalRegistrosErrores > 20) {
                    for (var i = 0; i < 20; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
                else {
                    for (var i = 0; i < $scope.ListaErrores.length; i++) {
                        $scope.DataErrorActual.push($scope.ListaErrores[i])
                    }
                }
            }
            else if (contadorRegistros == 0) {
                ShowError('No se encontraron registros para validar, por favor revise que la casilla de omitir no se encuentre marcada en al menos un registro')
            } else {
                blockUI.start();
                $timeout(function () {
                    blockUI.message('Validando tarifas');
                }, 1000);
                $scope.ValidarTarifas(0)
                //showModal('modalConfirmacionGuardarPorgramacion')
                //$scope.MSJ = 'Los datos se verificaron correctamente. \n  ¿Desea continuar con el cargue de la información?'
            }

        }

        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardarPorgramacion')
            for (var i = 0; i < $scope.DataVerificada.length; i++) {
                var item = $scope.DataVerificada[i]
                var ObtjetoRemesa = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA },
                    TipoRemesa: { Codigo: item.TipoRemesa },
                    NumeroDocumentoCliente: item.NumeroDocumentoCliente,
                    FechaDocumentoCliente: MIN_DATE,
                    Fecha: new Date(),
                    Ruta: { Codigo: 0 },
                    ProductoTransportado: item.ProductoTransportado,
                    FormaPago: item.FormaPago,
                    Cliente: { Codigo: item.Cliente.Codigo },
                    Remitente: item.Remitente,
                    CiudadRemitente: { Codigo: item.Remitente.Ciudad.Codigo },
                    DireccionRemitente: item.Remitente.Direccion,
                    TelefonoRemitente: item.Remitente.Telefonos,
                    Observaciones: item.ObservacionesRemitente,
                    CantidadCliente: item.CantidadCliente,
                    PesoCliente: item.PesoCliente,
                    PesoVolumetricoCliente: item.PesoVolumetricoCliente,
                    ValorComisionOficina: Math.round((item.ValorFleteCliente * $scope.OficinaUsuario.Porcentaje) / 100),
                    ValorFleteCliente: item.ValorFleteCliente,
                    ValorManejoCliente: item.ValorManejoCliente,
                    ValorSeguroCliente: Math.round(item.ValorSeguroCliente),
                    ValorDescuentoCliente: 0,
                    TotalFleteCliente: item.TotalFleteCliente,
                    ValorComercialCliente: item.ValorComercialCliente,
                    CantidadTransportador: item.CantidadTransportador,
                    PesoTransportador: item.PesoTransportador,
                    ValorFleteTransportador: 0,
                    TotalFleteTransportador: item.TotalFleteTransportador,
                    Destinatario: item.Destinatario,
                    CiudadDestinatario: { Codigo: item.Destinatario.Ciudad.Codigo },
                    DireccionDestinatario: item.Destinatario.Direccion,
                    TelefonoDestinatario: item.Destinatario.Telefonos,
                    Estado: 1,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                    TarifarioVenta: { Codigo: item.NumeroTarifarioVenta },
                    NumeroOrdenServicio: 0,
                    BarrioRemitente: item.BarrioRemitente,
                    CodigoPostalRemitente: item.CodigoPostalRemitente,
                    BarrioDestinatario: item.BarrioDestinatario,
                    CodigoPostalDestinatario: item.CodigoPostalDestinatario,
                    DetalleTarifaVenta: { Codigo: item.TarifaCarga != undefined ? item.TarifaCarga.Codigo : 0 },
                    Numeracion: item.No_PreImpreso
                }

                $scope.AplicaReexpedicion = 0;
                if ($scope.ListaReexpedicionOficinas !== undefined && $scope.ListaReexpedicionOficinas !== null) {
                    if ($scope.ListaReexpedicionOficinas.length > 0) {
                        $scope.AplicaReexpedicion = 1;
                    }
                }

                var RemesaPaqueteria = {
                    CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                    Remesa: ObtjetoRemesa,
                    TransportadorExterno: { Codigo: 0 },
                    NumeroGuiaExterna: '',
                    //TipoTransporteRemsaPaqueteria: $scope.Modelo.TipoTransporteRemsaPaqueteria,
                    //TipoDespachoRemesaPaqueteria: $scope.Modelo.TipoDespachoRemesaPaqueteria,
                    //TemperaturaProductoRemesaPaqueteria: $scope.Modelo.TemperaturaProductoRemesaPaqueteria,
                    //TipoServicioRemesaPaqueteria: $scope.Modelo.TipoServicioRemesaPaqueteria,
                    TipoEntregaRemesaPaqueteria: item.TipoEntregaRemesaPaqueteria,
                    //TipoInterfazRemesaPaqueteria: $scope.Modelo.TipoInterfazRemesaPaqueteria,
                    //FechaInterfazTracking: $scope.Modelo.FechaInterfaz, /// ----> fecha interfaz
                    //FechaInterfazWMS: $scope.Modelo.FechaInterfaz, /////
                    UsuarioCrea: $scope.Modelo.UsuarioCrea,
                    Oficina: $scope.Modelo.Oficina,
                    EstadoRemesaPaqueteria: $scope.Modelo.EstadoGuia,
                    ObservacionesDestinatario: item.ObservacionesDestinatario,
                    ObservacionesRemitente: item.ObservacionesRemitente,
                    OficinaOrigen: { Codigo: $scope.Modelo.OficinaOrigen.Codigo },
                    OficinaDestino: { Codigo: $scope.Modelo.OficinaDestino.Codigo },
                    OficinaActual: { Codigo: $scope.Modelo.OficinaActual.Codigo },
                    DesripcionMercancia: item.DescripcionProductoTransportado,
                    PesoCobrar: $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria ? item.Peso_Volumetrico > item.PesoCliente ? item.Peso_Volumetrico : item.PesoCliente : item.PesoCliente,
                    Largo: item.Largo,
                    Alto: item.Alto,
                    Ancho: item.Ancho,
                    PesoVolumetrico: item.Peso_Volumetrico,

                    LineaNegocioPaqueteria: { Codigo: item.Linea_Negocio_Paqueteria.Codigo },
                    RecogerOficinaDestino: item.Recoger_Oficina_Destino != undefined ? item.Recoger_Oficina_Destino.Codigo : 0,
                    ManejaDetalleUnidades: 0,

                    CentroCosto: item.CentroCosto,
                    //SitioCargue: { Codigo: $scope.Modelo.Remesa.Remitente.SitioCargue.Codigo },
                    SitioDescargue: { Codigo: item.SitioDescargue != undefined ? item.SitioDescargue.Codigo : 0 },
                    FechaEntrega: item.FechaEntrega,
                    CodigoZona: item.ZonaDestinatario == undefined ? 0 : item.ZonaDestinatario.Codigo,
                    Latitud: item.Latitud,
                    Longitud: item.Longitud,
                    HorarioEntregaRemesa: { Codigo: item.Horario_Entrega == undefined ? 0 : item.Horario_Entrega.Codigo },
                    FechaInterfazTracking: new Date()
                }
                try {
                    if (item.LineaSerivicioCliente !== undefined && item.LineaSerivicioCliente !== '' && item.LineaSerivicioCliente !== null) {
                        RemesaPaqueteria.LineaSerivicioCliente = { Codigo: item.LineaSerivicioCliente.Codigo }
                    }
                } catch (e) {

                }
                try {
                    if (item.SitioDescargue !== undefined && item.SitioDescargue !== '' && item.SitioDescargue !== null) {
                        RemesaPaqueteria.SitioDescargue = { Codigo: item.SitioDescargue.Codigo }
                    }
                } catch (e) {

                }
                item.Remesa = RemesaPaqueteria
            }
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        $scope.GuardarErrados = function () {
            //closeModal('ModalErrores')
            data = []
            for (var i = 0; i < $scope.ProgramacionesNoGuardadas.length; i++) {
                if (!$scope.ProgramacionesNoGuardadas[i].Omitido && $scope.ProgramacionesNoGuardadas[i].Option != 'Omitido') {
                    if ($scope.ProgramacionesNoGuardadas[i].Option == 'Remplazado') {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 1
                    } else {
                        $scope.ProgramacionesNoGuardadas[i].Remplaza = 0
                    }
                    data.push($scope.ProgramacionesNoGuardadas[i])
                }
            }
            $scope.DataVerificada = data
            $scope.ProgramacionesNoGuardadas = []
            $scope.ProgramacionesGuardadas = []
            Guardaros = 0
            contGuard = 0
            contGuardGeneral = 0
            contGeneral = $scope.DataVerificada.length
            GuardarCompleto(0)
        }
        var contGuard = 0
        var contGuardGeneral = 0
        var contGeneral = 0
        var Guardaros = 0
        function GuardarCompleto(inicio, opt) {
            contGuard++
            if (contGuard > $scope.DataVerificada.length) {

                ShowSuccess('Se guardaron ' + contGuardGeneral + ' de ' + contGeneral + ' remesas')
                closeModal('modalConfirmacionRemplazarProgramacion');
                closeModal('ModalErrores');
                $scope.LimpiarData()

            } else {

                blockUI.start();
                $timeout(function () {
                    blockUI.message('Guardando Remesa ' + (inicio + 1) + ' de ' + $scope.DataVerificada.length);
                }, 1000);
                RemesaGuiasFactory.Guardar($scope.DataVerificada[inicio].Remesa).then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        Guardaros++
                        contGuardGeneral++
                        GuardarCompleto(contGuard)
                        blockUI.stop();
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    $scope.ProgramacionesNoGuardadas.push($scope.DataVerificada[inicio])
                    GuardarCompleto(contGuard)
                    blockUI.stop();
                })
            }
        }

        /*---------------------------------------------------------------------Funcion validar datos requeridos-----------------------------------------------------------------------------*/
        function DatosRequeridos() {

        }
        $scope.MaskNumero = function () {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (o) {
            return MascaraNumero(o)
        }
        $scope.MaskValoresGrid = function (o) {
            return MascaraValores(o)
        }
        $scope.MaskMayusGrid = function (o) {
            return MascaraMayus(o)
        }
        $scope.MaskplacaGrid = function (o) {
            return MascaraPlaca(o)
        }
        $scope.MaskplacaSemiremolqueGrid = function (o) {
            return MascaraPlacaSemirremolque(o)
        }

        $scope.GenerarPlanilla = function () {
            var entidad = { CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }
            blockUI.start();
            $timeout(function () {
                blockUI.message('Generando Plantilla..');
            }, 1000);
            RemesaGuiasFactory.GenerarPlanitilla(entidad).then(function (response) {
                if (response.data.ProcesoExitoso === true) {
                    var pdfAsDataUri = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + response.data.Datos.Planitlla;
                    var link = document.createElement('a');
                    link.href = pdfAsDataUri;
                    link.download = "Plantilla_Cargue_Masivo_Remesas.xlsx";
                    link.click();
                    //window.open(pdfAsDataUri);
                    blockUI.stop();
                    ShowSuccess('La plantilla se generó correctamente')
                }
            }, function (response) {
                blockUI.stop();
                ShowError(response.statusText)
            })
        }
        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);

        $scope.ObtenerRemitente = function (item, Obtener, optvalidar, Inicio) {
            $scope.RemitenteNoRegistrado = false

            if (item.Remitente.NumeroIdentificacion !== undefined && item.Remitente.NumeroIdentificacion !== null && item.Remitente.NumeroIdentificacion !== '') {

                if (Obtener) {
                    item.Cliente = { NombreCompleto: '' }
                } else {
                    item.Remitente.NumeroIdentificacion = ''
                }
                try {
                    if (item.Cliente.Codigo > 0) {
                        var FiltroRemitente = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Codigo: item.Cliente.Codigo,
                        }
                    } else {
                        var FiltroRemitente = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            NumeroIdentificacion: item.Remitente.NumeroIdentificacion
                        }
                    }

                } catch (e) {
                    var FiltroRemitente = {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        NumeroIdentificacion: item.Remitente.NumeroIdentificacion
                    }
                }
                TercerosFactory.Obtener(FiltroRemitente).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                item.Remitente.Codigo = response.data.Datos.Codigo
                                item.Remitente.NumeroIdentificacion = response.data.Datos.NumeroIdentificacion
                                item.Remitente.Nombre = response.data.Datos.NombreCompleto
                                item.Remitente.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.TipoIdentificacion.Codigo);
                                item.CodigoPostalRemitente = response.data.Datos.CodigoPostal;
                                item.BarrioRemitente = response.data.Datos.Barrio;
                                item.Cliente = $scope.CargarTercero(response.data.Datos.Codigo);
                                if (item.Remitente.Direccion == undefined || item.Remitente.Direccion == '') {
                                    item.Remitente.Direccion = response.data.Datos.Direccion
                                }
                                if (item.Remitente.Telefonos == undefined || item.Remitente.Telefonos == '') {
                                    item.Remitente.Telefonos = response.data.Datos.Telefonos.replace(';', ' - ')
                                }
                                if (item.Remitente.Ciudad == undefined || item.Remitente.Ciudad == '') {
                                    item.Remitente.Ciudad = $linq.Enumerable().From($scope.ListadoCiudades).First('$.Codigo ==' + response.data.Datos.Ciudad.Codigo);
                                }
                                try { item.Cliente = $linq.Enumerable().From($scope.ListaClientes).First('$.Codigo ==' + response.data.Datos.Codigo); } catch (e) { }

                                $scope.ObtenerTarifarioCliente(item, optvalidar, Inicio)

                            }
                            else {
                                item.Remitente.Nombre = ''
                                item.Remitente.TipoIdentificacion = $scope.ListadoTipoIdentificacion[1]
                                item.Remitente.Codigo = 0
                                if (optvalidar) {
                                    $scope.ValidarTarifas(Inicio + 1)
                                    item.stRow = 'background:yellow'
                                    item.stGeneral = 'background:red'
                                    item.msGeneral = 'No se encontrarón tarifas validas'
                                    $scope.ListaErrores.push(item)
                                }
                            }
                        }
                        else {
                            if (optvalidar) {
                                $scope.ValidarTarifas(Inicio + 1)
                                item.stGeneral = 'background:red'
                                item.stRow = 'background:yellow'
                                item.msGeneral = 'No se encontrarón tarifas validas'
                                $scope.ListaErrores.push(item)
                            }
                        }
                    }, function (response) {
                        if (optvalidar) {
                            $scope.ValidarTarifas(Inicio + 1)
                            item.stGeneral = 'background:red'
                            item.stRow = 'background:yellow'
                            item.msGeneral = 'No se vencontrarón tarifas validas'
                            $scope.ListaErrores.push(item)
                        }
                    });

            }


        }
        $scope.ObtenerTarifarioCliente = function (item, optvalidar, Inicio) {
            item.ListaAuxTipoTarifas = [];
           // item.ListaTarifaCarga = [];
            item.ListaTipoTarifaCargaVenta = [];
            item.TarifarioInvalido = false

            var TipoLineaNegocioAux = 0
            //if (item.validartarifario == true) {
            if (item.Remitente.Ciudad.Codigo == item.Destinatario.Ciudad.Codigo) {
                TipoLineaNegocioAux = CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA;
                CodigoTipoRemesa = CODIGO_TIPO_REMESA_URBANA;
            } else {
                TipoLineaNegocioAux = CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL;
                CodigoTipoRemesa = CODIGO_TIPO_REMESA_NACIONAL;
            }

            $scope.CodigoCliente = 0;
            if (item.Cliente !== undefined && item.Cliente !== null && item.Cliente !== '') {
                $scope.CodigoCliente = item.Cliente.Codigo;
            }

            $scope.TarifaCliente = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: item.validartarifario == true ? $scope.CodigoCliente : 0,
                CodigoCiudadOrigen: item.Remitente.Ciudad.Codigo,
                CodigoCiudadDestino: item.Destinatario.Ciudad.Codigo,
                CodigoLineaNegocio: CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA,
                CodigoTipoLineaNegocio: TipoLineaNegocioAux,
                CodigoFormaPago: item.FormaPago.Codigo,
                TarifarioProveedores: false,
                TarifarioClientes: true
            }
            //TercerosFactory.Consultar($scope.TarifaCliente).then(function (response) {
            //    if (response.data.ProcesoExitoso === true) {
            //        if (response.data.Datos.length > 0) {


            //            for (var i = 0; i < response.data.Datos.length; i++) {
            //                var InsertaTarifa = true;
            //                for (var j = 0; j < item.ListaTarifaCarga.length; j++) {
            //                    if (item.ListaTarifaCarga[j].Codigo == response.data.Datos[i].TarifaCarga.Codigo) {
            //                        InsertaTarifa = false;
            //                    }
            //                }
            //                if (InsertaTarifa == true) {
            //                    item.ListaTarifaCarga.push(response.data.Datos[i].TarifaCarga)
            //                }
            //                if (item.CodigoTarifaCarga !== undefined && item.CodigoTarifaCarga !== null) {
            //                    if (item.CodigoTarifaCarga > 0) {
            //                        item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo ==' + item.CodigoTarifaCarga);
            //                        $scope.CargarTipoTarifa(item, item.CodigoTarifaCarga);
            //                    } else {
            //                        try {
            //                            item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo ==' + 302);

            //                        } catch (e) {
            //                            item.TarifaCarga = item.ListaTarifaCarga[0];
            //                        }
            //                    }
            //                } else {
            //                    try {
            //                        item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo ==' + 302);

            //                    } catch (e) {
            //                       // item.TarifaCarga = item.ListaTarifaCarga[0];
            //                    }
            //                    $scope.CargarTipoTarifa(item, 0)
            //                }


            //                item.ListaAuxTipoTarifas.push({
            //                    CodigoDetalleTarifa: response.data.Datos[i].CodigoDetalleTarifa,
            //                    Codigo: response.data.Datos[i].TipoTarifaCarga.Codigo,
            //                    CodigoTarifa: response.data.Datos[i].TarifaCarga.Codigo,
            //                    Nombre: response.data.Datos[i].TipoTarifaCarga.Nombre,
            //                    ValorFlete: response.data.Datos[i].ValorFlete,
            //                    ValorManejo: response.data.Datos[i].ValorManejo,
            //                    ValorSeguro: response.data.Datos[i].ValorSeguro
            //                })

            //                if (i == 0) {
            //                    item.NumeroTarifarioVenta = response.data.Datos[i].NumeroTarifarioVenta;
            //                }
            //            }
            //            //$scope.CargarTipoTarifa(item, item.TarifaCarga.Codigo)
            //            $scope.CargarTipoTarifa(item, 0)
            //            $scope.Calcular(item)
            //            if (optvalidar) {
            //                $scope.ValidarTarifas(Inicio + 1)
            //            }
            //        }
            //        else {
            //            item.TarifarioInvalido = true
            //            if (optvalidar) {
            //                $scope.ListaErrores.push(item)
            //                $scope.ValidarTarifas(Inicio + 1)
            //                item.stRow = 'background:yellow'
            //                item.stGeneral = 'background:red'
            //                item.msGeneral = item.validartarifario == true ? 'No se encontrarón tarifas validas' : 'No se encontraron tarifas validas en el tarifario base'
            //            }
            //        }
            //    } else {
            //        item.TarifarioInvalido = true
            //        if (optvalidar) {
            //            $scope.ListaErrores.push(item)
            //            $scope.ValidarTarifas(Inicio + 1)
            //            item.stRow = 'background:yellow'
            //            item.stGeneral = 'background:red'
            //            item.msGeneral = item.msGeneral = item.validartarifario == true ? 'No se encontrarón tarifas validas' : 'No se encontraron tarifas validas en el tarifario base'
            //        }
            //    }
            //}, function (response) {
            //    blockUI.stop();
            //    if (optvalidar) {
            //        $scope.ListaErrores.push(item)
            //        $scope.ValidarTarifas(Inicio + 1)
            //        item.stRow = 'background:yellow'
            //        item.stGeneral = 'background:red'
            //        item.msGeneral = item.msGeneral = item.validartarifario == true ? 'No se encontrarón tarifas validas' : 'No se encontraron tarifas validas en el tarifario base'
            //    }
            //});
            $scope.TarifaCliente.Sync = true;
            var ResponseTarifa = TercerosFactory.Consultar($scope.TarifaCliente);
            if (ResponseTarifa.ProcesoExitoso === true) {
                if (ResponseTarifa.Datos.length > 0) {


                    for (var i = 0; i < ResponseTarifa.Datos.length; i++) {
                            //var InsertaTarifa = true;
                            //for (var j = 0; j < item.ListaTarifaCarga.length; j++) {
                            //    if (item.ListaTarifaCarga[j].Codigo == ResponseTarifa.Datos[i].TarifaCarga.Codigo) {
                            //        InsertaTarifa = false;
                            //    }
                            //}
                            //if (InsertaTarifa == true) {
                            //    item.ListaTarifaCarga.push(ResponseTarifa.Datos[i].TarifaCarga)
                            //}
                            if (item.CodigoTarifaCarga !== undefined && item.CodigoTarifaCarga !== null) {
                                if (item.CodigoTarifaCarga > 0) {
                                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo ==' + item.CodigoTarifaCarga);
                                    $scope.CargarTipoTarifa(item, item.CodigoTarifaCarga);
                                } else {
                                    try {
                                        item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo ==' + 302);

                                    } catch (e) {
                                        item.TarifaCarga = item.ListaTarifaCarga[0];
                                    }
                                }
                            } else {
                                try {
                                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo ==' + 302);

                                } catch (e) {
                                    // item.TarifaCarga = item.ListaTarifaCarga[0];
                                }
                                $scope.CargarTipoTarifa(item, 0)
                            }


                            item.ListaAuxTipoTarifas.push({
                                CodigoDetalleTarifa: ResponseTarifa.Datos[i].CodigoDetalleTarifa,
                                Codigo: ResponseTarifa.Datos[i].TipoTarifaCarga.Codigo,
                                CodigoTarifa: ResponseTarifa.Datos[i].TarifaCarga.Codigo,
                                Nombre: ResponseTarifa.Datos[i].TipoTarifaCarga.Nombre,
                                ValorFlete: ResponseTarifa.Datos[i].ValorFlete,
                                ValorManejo: ResponseTarifa.Datos[i].ValorManejo,
                                ValorSeguro: ResponseTarifa.Datos[i].ValorSeguro,
                                PesoMinimo: ResponseTarifa.Datos[i].ValorCatalogoTipoTarifa.CampoAuxiliar2,
                                PesoMaximo: ResponseTarifa.Datos[i].ValorCatalogoTipoTarifa.CampoAuxiliar3
                            })

                            if (i == 0) {
                                item.NumeroTarifarioVenta = ResponseTarifa.Datos[i].NumeroTarifarioVenta;
                            }
                        }
                        //$scope.CargarTipoTarifa(item, item.TarifaCarga.Codigo)
                        $scope.CargarTipoTarifa(item, 0)
                        $scope.Calcular(item)
                        if (optvalidar) {
                            $scope.ValidarTarifas(Inicio + 1)
                        }
                    }
                    else {
                        item.TarifarioInvalido = true
                        if (optvalidar) {
                            $scope.ListaErrores.push(item)
                            $scope.ValidarTarifas(Inicio + 1)
                            item.stRow = 'background:yellow'
                            item.stGeneral = 'background:red'
                            item.msGeneral = item.validartarifario == true ? 'No se encontrarón tarifas validas' : 'No se encontraron tarifas validas en el tarifario base'
                        }
                    }
                } else {
                    item.TarifarioInvalido = true
                    if (optvalidar) {
                        $scope.ListaErrores.push(item)
                        $scope.ValidarTarifas(Inicio + 1)
                        item.stRow = 'background:yellow'
                        item.stGeneral = 'background:red'
                        item.msGeneral = item.msGeneral = item.validartarifario == true ? 'No se encontrarón tarifas validas' : 'No se encontraron tarifas validas en el tarifario base'
                    }
                }
            //}, function (response) {
            //    blockUI.stop();
            //    if (optvalidar) {
            //        $scope.ListaErrores.push(item)
            //        $scope.ValidarTarifas(Inicio + 1)
            //        item.stRow = 'background:yellow'
            //        item.stGeneral = 'background:red'
            //        item.msGeneral = item.msGeneral = item.validartarifario == true ? 'No se encontrarón tarifas validas' : 'No se encontraron tarifas validas en el tarifario base'
            //    }
            //});
            /* } else {
 
                 if (item.Remitente.Ciudad.Codigo == item.Destinatario.Ciudad.Codigo) {
                     TipoLineaNegocioAux = CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_URBANA;
                     CodigoTipoRemesa = CODIGO_TIPO_REMESA_URBANA;
                 } else {
                     TipoLineaNegocioAux = CODIGO_TIPO_LINEA_NEGOCIO_CARGA_PAQUETERIA_NACIONAL;
                     CodigoTipoRemesa = CODIGO_TIPO_REMESA_NACIONAL;
                 }
 
                 $scope.TarifaCliente = {
                     CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                     
                     CodigoCiudadOrigen: item.Remitente.Ciudad.Codigo,
                     CodigoCiudadDestino: item.Destinatario.Ciudad.Codigo,
                     CodigoLineaNegocio: CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA,
                     CodigoTipoLineaNegocio: TipoLineaNegocioAux,
                     CodigoFormaPago: item.FormaPago.Codigo,
                     TarifarioProveedores: false,
                     TarifarioClientes: true
                 }
 
                 TercerosFactory.Consultar($scope.TarifaCliente).then(function (response) {
                     if (response.data.ProcesoExitoso === true) {
                         if (response.data.Datos.length > 0) {
 
                             item.ListaTarifaCarga = response.data.Datos;
                             item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo ==' + item.CodigoTarifaCarga);
                             $scope.DataVerificada.push(item)
                             $scope.ValidarTarifas(Inicio + 1)
                         }
                         else {
                             item.TarifarioInvalido = true
                             if (optvalidar) {
                                 $scope.ListaErrores.push(item)
                                 $scope.ValidarTarifas(Inicio + 1)
                                 item.stRow = 'background:yellow'
                                 item.stGeneral = 'background:red'
                                 item.msGeneral = 'No se encontraron tarifas válidas en el tarifario base'
                             }
                         }
                     } else {
                         item.TarifarioInvalido = true
                         if (optvalidar) {
                             $scope.ListaErrores.push(item)
                             $scope.ValidarTarifas(Inicio + 1)
                             item.stRow = 'background:yellow'
                             item.stGeneral = 'background:red'
                             item.msGeneral = 'No se encontraron tarifas válidas en el tarifario base'
                         }
                     }
                 });
                 
             }*/


        }
        $scope.CargarTipoTarifa = function (item, CodigoTarifaCarga) {

            //item.ListaTipoTarifaCargaVenta = [];
            //item.ListaAuxTipoTarifas.forEach(function (item2) {
            //    if (CodigoTarifaCarga == item2.CodigoTarifa) {
            //        item.ListaTipoTarifaCargaVenta.push(item2);
            //    }
            //});
            item.ListaTipoTarifaCargaVenta = item.ListaAuxTipoTarifas;

        }
        //Calcular Flete
        $scope.Calcular = function (item) {
            item.ValorFleteCliente = 0
            item.ValorManejoCliente = 0
            item.ValorSeguroCliente = 0
            if (item.PesoCliente == null || item.PesoCliente == '' || item.PesoCliente == undefined || isNaN(item.PesoCliente)) {
                item.PesoCliente = 0;
            }
            var PesoCalcular = $scope.Sesion.UsuarioAutenticado.ManejoPesoVolumetricoRemesaPauqeteria ? item.Peso_Volumetrico : item.Peso_Volumetrico > item.PesoCliente ? item.Peso_Volumetrico : item.PesoCliente;
            var Calculo = false
            
            //Pendiente calcular fletes y consultar tarifas
            try {

                if (item.TarifaCarga != undefined && item.TarifaCarga != null && item.TarifaCarga != '') {
                    for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                        if (item.TarifaCarga.Codigo == item.ListaTipoTarifaCargaVenta[i].CodigoTarifa && (PesoCalcular >= item.ListaTipoTarifaCargaVenta[i].PesoMinimo && PesoCalcular <= item.ListaTipoTarifaCargaVenta[i].PesoMaximo)) {
                            item.ValorFleteCliente = item.TarifaCarga.Codigo == TARIFA_RANGO_PESO_VALOR_KILO ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                            item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                            item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                            item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);                            
                            Calculo = true
                        }
                        $scope.DataArchivo[item.Pos].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                    }
                } else {
                    for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                        if (PesoCalcular >= item.ListaTipoTarifaCargaVenta[i].PesoMinimo && PesoCalcular <= item.ListaTipoTarifaCargaVenta[i].PesoMaximo) {
                            item.ValorFleteCliente = item.ListaTipoTarifaCargaVenta[i].CodigoTarifa == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                            item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                            item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                            item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                            $scope.DataArchivo[item.Pos].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                            Calculo = true
                        }
                    }
                }
                    
                //if (PesoCalcular > 0 && PesoCalcular <= 5) {
                //    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[0], item.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[1], item.ListaTipoTarifaCargaVenta)) {
                        
                //        if (item.TarifaCarga != undefined && item.TarifaCarga != null && item.TarifaCarga != '') {
                //            for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //                if (item.TarifaCarga.Codigo == item.ListaTipoTarifaCargaVenta[i].CodigoTarifa && (item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[0] || item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[1])) {
                //                    item.ValorFleteCliente = item.TarifaCarga.Codigo == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                    item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                    item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                    $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                    Calculo = true
                //                }
                //            }
                //        } else {
                //            for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //                if (item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[0] || item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_1_5_KILOS[1]) {
                //                    item.ValorFleteCliente = item.ListaTipoTarifaCargaVenta[i].CodigoTarifa == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                    item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                    item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                    $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                    Calculo = true
                //                }
                //            }
                //        }
                //    }

                //}
                //if (PesoCalcular > 5 && PesoCalcular <= 10) {
                //    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[0], item.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[1], item.ListaTipoTarifaCargaVenta)) {
                       
                //        if (item.TarifaCarga != undefined && item.TarifaCarga != null && item.TarifaCarga != '') {
                //            for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //                if (item.TarifaCarga.Codigo == item.ListaTipoTarifaCargaVenta[i].CodigoTarifa && (item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[0] || item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[1])) {
                //                    item.ValorFleteCliente = item.TarifaCarga.Codigo == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                    item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                    item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                    $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                    Calculo = true
                //                }
                //            }
                //        } else {
                //            for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //                if (item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[0] || item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_6_10_KILOS[1]) {
                //                    item.ValorFleteCliente = item.ListaTipoTarifaCargaVenta[i].CodigoTarifa == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                    item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                    item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                    $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                    Calculo = true
                //                }
                //            }
                //        }
                //    }
                //}
                //if (PesoCalcular > 10 && PesoCalcular <= 20) {
                //    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[0], item.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[1], item.ListaTipoTarifaCargaVenta)) {
                       
                //        if (item.TarifaCarga != undefined && item.TarifaCarga != null && item.TarifaCarga != '') {
                //            for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //                if (item.TarifaCarga.Codigo == item.ListaTipoTarifaCargaVenta[i].CodigoTarifa && (item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[0] || item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[1])) {
                //                    item.ValorFleteCliente = item.TarifaCarga.Codigo == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                    item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                    item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                    $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                    Calculo = true
                //                }
                //            }
                //        } else {
                //            for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //                if (item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[0] || item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_11_20_KILOS[1]) {
                //                    item.ValorFleteCliente = item.ListaTipoTarifaCargaVenta[i].CodigoTarifa == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                    item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                    item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                    $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                    Calculo = true
                //                }
                //            }
                //        }
                //    }
                //}
                //if (PesoCalcular > 20 && PesoCalcular <= 30) {
                //    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[0], item.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[1], item.ListaTipoTarifaCargaVenta)) {
                        
                //        if (item.TarifaCarga != undefined && item.TarifaCarga != null && item.TarifaCarga != '') {
                //            for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //                if (item.TarifaCarga.Codigo == item.ListaTipoTarifaCargaVenta[i].CodigoTarifa && (item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[0] || item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[1])) {
                //                    item.ValorFleteCliente = item.TarifaCarga.Codigo == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                    item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                    item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                    $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                    Calculo = true
                //                }
                //            }
                //        } else {
                //            for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //                if (item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[0] || item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_21_30_KILOS[1]) {
                //                    item.ValorFleteCliente = item.ListaTipoTarifaCargaVenta[i].CodigoTarifa == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                    item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                    item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                    $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                    Calculo = true
                //                }
                //            }
                //        }
                //    }
                //}
                //if (PesoCalcular >= 5 && PesoCalcular <= 30) {
                //    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[0], item.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[1], item.ListaTipoTarifaCargaVenta)) {
                      
                //        if (item.TarifaCarga != undefined && item.TarifaCarga != null && item.TarifaCarga != '') {
                //            for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //                if (item.TarifaCarga.Codigo == item.ListaTipoTarifaCargaVenta[i].CodigoTarifa && (item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[0] || item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[1])) {
                //                    item.ValorFleteCliente = item.TarifaCarga.Codigo == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                    item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                    item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                    $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                    Calculo = true
                //                }
                //            }
                //        } else {
                //            for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //                if (item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[0] || item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_5_30_KILOS[1]) {
                //                    item.ValorFleteCliente = item.ListaTipoTarifaCargaVenta[i].CodigoTarifa == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                    item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                    item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                    $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                    Calculo = true
                //                }
                //            }
                //        }
                //    }
                //}
                //if (PesoCalcular > 30 && PesoCalcular <= 499) {
                //    if (ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[0], item.ListaTipoTarifaCargaVenta) || ObjetoEnListado(CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[1], item.ListaTipoTarifaCargaVenta)) {
                       

                //        if (item.TarifaCarga != undefined && item.TarifaCarga != null && item.TarifaCarga != '') {
                //            for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //                if (item.TarifaCarga.Codigo == item.ListaTipoTarifaCargaVenta[i].CodigoTarifa && (item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[0] || item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[1])) {
                //                    item.ValorFleteCliente = item.TarifaCarga.Codigo == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                    item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                    item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                    $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                    Calculo = true
                //                }
                //            }
                //        } else {
                //            for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //                if (item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[0] || item.ListaTipoTarifaCargaVenta[i].Codigo == CODIGO_CATALOGO_TIPO_TARIFA_PAQUETERIA_30_MAS_KILOS[1]) {
                //                    item.ValorFleteCliente = item.ListaTipoTarifaCargaVenta[i].CodigoTarifa == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                    item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                    item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                    item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                    $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                    Calculo = true
                //                }
                //            }
                //        }
                //    }
                //}
                //if (PesoCalcular >= 500  && PesoCalcular < 1000) {                                     

                //    if (item.TarifaCarga != undefined && item.TarifaCarga != null && item.TarifaCarga != '') {
                //        for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //            if (item.TarifaCarga.Codigo == item.ListaTipoTarifaCargaVenta[i].CodigoTarifa && (item.ListaTipoTarifaCargaVenta[i].Codigo == 71)) {
                //                item.ValorFleteCliente = item.TarifaCarga.Codigo == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                Calculo = true
                //            }
                //        }
                //    } else {
                //        for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //            if (item.ListaTipoTarifaCargaVenta[i].Codigo == 71) {
                //                item.ValorFleteCliente = item.ListaTipoTarifaCargaVenta[i].CodigoTarifa == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                Calculo = true
                //            }
                //        }
                //    }
                    
                //}
                //if (PesoCalcular > 1000) {

                //    if (item.TarifaCarga != undefined && item.TarifaCarga != null && item.TarifaCarga != '') {
                //        for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //            if (item.TarifaCarga.Codigo == item.ListaTipoTarifaCargaVenta[i].CodigoTarifa && (item.ListaTipoTarifaCargaVenta[i].Codigo == 72)) {
                //                item.ValorFleteCliente = item.TarifaCarga.Codigo == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.TarifaCarga.Codigo);
                //                Calculo = true
                //            }
                //        }
                //    } else {
                //        for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //            if (item.ListaTipoTarifaCargaVenta[i].Codigo == 72) {
                //                item.ValorFleteCliente = item.ListaTipoTarifaCargaVenta[i].CodigoTarifa == 200 ? PesoCalcular * item.ListaTipoTarifaCargaVenta[i].ValorFlete : item.ListaTipoTarifaCargaVenta[i].ValorFlete;
                //                item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //                item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //                item.TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                $scope.DataArchivo[i].TarifaCarga = $linq.Enumerable().From(item.ListaTarifaCarga).First('$.Codigo==' + item.ListaTipoTarifaCargaVenta[i].CodigoTarifa);
                //                Calculo = true
                //            }
                //        }
                //    }

                //}
                //if (item.TarifaCarga.Codigo == 302) {
                //    for (var i = 0; i < item.ListaTipoTarifaCargaVenta.length; i++) {
                //        if (item.TarifaCarga.Codigo == item.ListaTipoTarifaCargaVenta[i].CodigoTarifa && (item.ListaTipoTarifaCargaVenta[i].Codigo == item.ProductoTransportado.Codigo)) {
                //            item.ValorFleteCliente = item.CantidadCliente * item.ListaTipoTarifaCargaVenta[i].ValorFlete
                //            item.ValorManejoCliente = item.ListaTipoTarifaCargaVenta[i].ValorManejo;
                //            item.ValorSeguroCliente = item.ListaTipoTarifaCargaVenta[i].ValorSeguro;
                //            Calculo = true
                //        }
                //    }
                //}
            } catch (e) {
                console.log(e);
            }
            if (Calculo == false) {
                item.msPesoCliente = 'No hay tarifa disponible para el peso ingresado'
                item.msGeneral = 'No hay tarifa disponible para el peso ingresado'
                item.stGeneral = 'background:red'
                item.stRow = 'background:yellow'
               // item.PesoCliente = 0
                $scope.ListaErrores.push(item)
            }
            else {
                $scope.DataVerificada.push(item)
            }
            item.TotalFleteCliente = item.ValorFleteCliente + item.ValorManejoCliente + item.ValorSeguroCliente;
            item.TotalFleteCliente = MascaraValores(item.TotalFleteCliente)
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

        $scope.ValidarTarifas = function (Inicio) {
            if (Inicio + 1 <= $scope.DataVerificadatmp.length) {
                $timeout(function () {
                    blockUI.message('Validando Tarifa ' + (Inicio + 1) + ' de ' + $scope.DataArchivo.length + ' Por favor espere');
                }, 100);
                $scope.ObtenerRemitente($scope.DataVerificadatmp[Inicio], false, true, Inicio)
            }
            else {
                $timeout(function () {
                    blockUI.stop();
                }, 1000);
                if ($scope.ListaErrores.length > 0) {
                    $scope.paginaActualError = 1
                    $scope.DataErrorActual = []
                    $scope.totalRegistrosErrores = $scope.ListaErrores.length
                    $scope.totalPaginasErrores = Math.ceil($scope.totalRegistrosErrores / 20);
                    if ($scope.totalRegistrosErrores > 20) {
                        for (var i = 0; i < 20; i++) {
                            $scope.DataErrorActual.push($scope.ListaErrores[i])
                        }
                    }
                    else {
                        for (var i = 0; i < $scope.ListaErrores.length; i++) {
                            $scope.DataErrorActual.push($scope.ListaErrores[i])
                        }
                    }
                    ShowError('Se encontraron errores al validar los datos cargados')
                } else {
                    showModal('modalConfirmacionGuardarPorgramacion')
                    $scope.MSJ = 'Los datos se verificaron correctamente. \n  ¿Desea continuar con el cargue de la información?'
                }
            }
        }
    }]);

