﻿SofttoolsApp.controller("PlanificacionDespachosCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'blockUIConfig', 'PlanificacionDespachosFactory', 'TercerosFactory',
    'CiudadesFactory', 'EnturnamientoFactory', 'OficinasFactory', 'SemirremolquesFactory', 'RutasFactory', 'TarifarioComprasFactory', 'ImpuestosFactory', 'PlanillaGuiasFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUI, blockUIConfig, PlanificacionDespachosFactory,
        TercerosFactory, CiudadesFactory, EnturnamientoFactory, OficinasFactory, SemirremolquesFactory, RutasFactory, TarifarioComprasFactory, ImpuestosFactory, PlanillaGuiasFactory) {

        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Procesos' }, { Nombre: 'Planificación Despachos' }];
        $scope.Titulo = 'PLANIFICACIÓN DESPACHOS';
        //-----------------------------------------------------------------DECLARACION VARIABLES---------------------------------------------------------------------------------//
        $scope.Buscando = false;
        $scope.Deshabilitar = false;
        $scope.DeshabilitarTipoPlanilla = false;
        $scope.MensajesError = [];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.MostrarResumenRemesas = false;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ===' + OPCION_MENU_PLANIFICACION_DESPACHOS_PAQUETERIA); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        //--Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados--//
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        var filtros = {};
        $scope.DetalleRemesas = [];
        $scope.DetalleRemesasPag = [];
        $scope.VehiculosDisponibles = [];
        $scope.PlanillaDespacho = [];
        $scope.IndicePlanilla = 0;

        $scope.FechaInicial = new Date();
        $scope.FechaFinal = new Date();

        $scope.CiudadOrigen = $scope.CargarCiudad($scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);

        $scope.ListadoOficinas = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Sync: true }).Datos;
        $scope.OficinaDespacha = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
        //------------------------------AutoCompletes------------------------------//
        //---- Clientes
        $scope.ListadoCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) === 0 || value.length === 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //---- Remitente
        $scope.ListadoRemitente = [];
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) === 0 || value.length === 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_REMITENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoREmitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoREmitente);
                }
            }
            return $scope.ListadoREmitente;
        };
        //---- Ciudades
        $scope.AutocompleteCiudades = function (value) {
            $scope.ListadoCiudades = [];
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades);
                }
            }
            return $scope.ListadoCiudades;
        };
        //----Semiremolques
        $scope.ListaSemirremolque = [];
        $scope.AutoCompleteSemiRemolque = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = SemirremolquesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListaSemirremolque = ValidarListadoAutocomplete(Response.Datos, $scope.ListaSemirremolque);
                }
            }
            return $scope.ListaSemirremolque;
        };
        //----Semiremolques
        //----Rutas
        $scope.ListadoRutas = RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true }).Datos;
        //-Filtro Rutas
        $scope.AutoCompleteRutasFiltradas = function (value, ListaRutas) {
            if (value.length > 0) {
                var tmparr = [];
                for (var i = 0; i < ListaRutas.length; i++) {
                    var NombreRuta = ListaRutas[i].Nombre;
                    if (NombreRuta.likeFind(value + "%")) {
                        tmparr.push(ListaRutas[i]);
                    }
                }
            }
            return tmparr;
        };
        //-Filtro Rutas
        //----Rutas
        //----Tipo Documento Planilla
        $scope.ListadoTipoDocumentoPlanilla = [
            { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO, Nombre: "PLANILLA DESPACHOS" },
            { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_ENTREGA, Nombre: "PLANILLA ENTREGAS" }
        ];
        $scope.TipoPlanilla = $scope.ListadoTipoDocumentoPlanilla[0];
        //----Tipo Documento Planilla
        //------------------------------AutoCompletes------------------------------//
        //------------------------------Cargar Informacion Resumen Remesas -----------------//
        $scope.CargarResumenRemesas = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                CargarResumenGeneralRemesas();
            }
        };
        function CargarResumenGeneralRemesas() {
            $scope.RemesasAgencia = [];
            $scope.RemesasReex = [];
            $scope.DetalleRemesas = [];
            $scope.VehiculosDisponibles = [];
            $scope.PlanillaDespacho = [];
            $scope.IndicePlanilla = 0;
            $scope.MostrarResumenRemesas = true;
            $scope.DeshabilitarGenerarPlanificacion = false;
            DeshabilitarTipoPlanilla = false;
            blockUI.start('Buscando registros ...');
            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentoPlanilla: $scope.TipoPlanilla.Codigo,
                FechaInicial: $scope.FechaInicial,
                FechaFinal: $scope.FechaFinal,
                Oficina: { Codigo: $scope.OficinaDespacha.Codigo },
                Cliente: { Codigo: $scope.Cliente === undefined ? '' : $scope.Cliente.Codigo },
                Remitente: { Codigo: $scope.Remitente === undefined ? '' : $scope.Remitente.Codigo },
                CiudadOrigen: { Codigo: $scope.CiudadOrigen === undefined ? '' : $scope.CiudadOrigen.Codigo },
                CiudadDestino: { Codigo: $scope.CiudadDestino === undefined ? '' : $scope.CiudadDestino.Codigo },
                Sync: true
            };
            blockUI.delay = 1000;
            $scope.ResultadoSinRegistrosResumenRemesas = '';
            $scope.ResultadoSinRegistrosVehiculos = '';
            var ResponConsulta = PlanificacionDespachosFactory.Consultar(filtros);
            if (ResponConsulta.ProcesoExitoso === true) {
                if (ResponConsulta.Datos.length > 0) {
                    for (var i = 0; i < ResponConsulta.Datos.length; i++) {
                        if (ResponConsulta.Datos[i].Reexpedicion == 0) {
                            $scope.RemesasAgencia.push(ResponConsulta.Datos[i]);
                        }
                        //else {
                        //    $scope.RemesasReex.push(ResponConsulta.Datos[i]);
                        //}
                    }
                }
                else {
                    $scope.ResultadoSinRegistrosResumenRemesas = 'No hay datos para mostrar';
                }
            }
            else {
                ShowError(ResponConsulta.statusText);
            }
            ConsultarFlotaDisponible();
            blockUI.stop();
        }
        function ConsultarFlotaDisponible() {
            filtrosEnturnamiento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                OficinaEnturna: $scope.OficinaDespacha,
                Sync: true
            };
            var response = EnturnamientoFactory.Consultar(filtrosEnturnamiento);
            if (response.ProcesoExitoso == true) {
                if (response.Datos.length > 0) {
                    $scope.VehiculosDisponibles = response.Datos;
                    $scope.VehiculosDisponibles.forEach(vehiculo => {
                        vehiculo.Seleccion = true;
                    });
                }
                else {
                    $scope.ResultadoSinRegistrosVehiculos = 'No hay vehiculos para mostrar';
                }
            }
            else {
                ShowError(ResponConsulta.statusText);
            }
        }
        //------------------------------Cargar Informacion Resumen Remesas -----------------//
        //------------------------------Cargar Detalle Remesas -----------------//
        $scope.CheckRemesas = function (Seleccionado, ResumenRemesa) {
            if (Seleccionado == true) {
                //--Cargar Remesas
                blockUI.start('Buscando registros ...');
                $timeout(function () {
                    blockUI.message('Espere por favor ...');
                }, 100);
                $scope.Buscando = true;
                var FiltroRemesasGen = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumentoPlanilla: $scope.TipoPlanilla.Codigo,
                    FechaInicial: $scope.FechaInicial,
                    FechaFinal: $scope.FechaFinal,
                    Oficina: { Codigo: $scope.OficinaDespacha.Codigo },
                    Cliente: { Codigo: filtros.Cliente === undefined ? '' : filtros.Cliente.Codigo },
                    Remitente: { Codigo: filtros.Remitente === undefined ? '' : filtros.Remitente.Codigo },
                    CiudadOrigen: { Codigo: ResumenRemesa.CiudadOrigen.Codigo },
                    CiudadDestino: { Codigo: ResumenRemesa.CiudadDestino.Codigo },
                    Reexpedicion: ResumenRemesa.Reexpedicion,
                    Sync: true
                };
                blockUI.delay = 1000;
                var ResponConsulta = PlanificacionDespachosFactory.ConsultarDetalleRemesasResumen(FiltroRemesasGen);
                if (ResponConsulta.ProcesoExitoso === true) {
                    var existe = false;
                    if ($scope.DetalleRemesas.length > 0) {
                        for (var i = 0; i < ResponConsulta.Datos.length; i++) {
                            for (var j = 0; j < $scope.DetalleRemesas.length; j++) {
                                if ($scope.DetalleRemesas[j].Remesa.Numero === ResponConsulta.Datos[i].Remesa.Numero) {
                                    existe = true;
                                }
                            }
                            if (!existe) {
                                $scope.DetalleRemesas.push(ResponConsulta.Datos[i]);
                            }
                            existe = false;
                        }
                    }
                    else {
                        for (var i = 0; i < ResponConsulta.Datos.length; i++) {
                            $scope.DetalleRemesas.push(ResponConsulta.Datos[i]);
                        }
                    }
                }
                else {
                    ShowError(ResponConsulta.statusText);
                }
                blockUI.stop();
            }
            else {
                var tmpDetalle = [];
                var tmpDetalleAgencia = [];
                var tmpDetalleReex = [];
                for (var i = 0; i < $scope.DetalleRemesas.length; i++) {
                    if ($scope.DetalleRemesas[i].Reexpedicion == 0) {
                        tmpDetalleAgencia.push($scope.DetalleRemesas[i]);
                    }
                    else {
                        tmpDetalleReex.push($scope.DetalleRemesas[i]);
                    }
                }
                if (ResumenRemesa.Reexpedicion == 0) {
                    tmpDetalleAgencia = BorrarElementosArray(tmpDetalleAgencia, ResumenRemesa.CiudadOrigen.Codigo, ResumenRemesa.CiudadDestino.Codigo);
                }
                if (ResumenRemesa.Reexpedicion == 1) {
                    tmpDetalleReex = BorrarElementosArray(tmpDetalleReex, ResumenRemesa.CiudadOrigen.Codigo, ResumenRemesa.CiudadDestino.Codigo);
                }
                $scope.DetalleRemesas = tmpDetalle.concat(tmpDetalleAgencia, tmpDetalleReex);
            }
            ResetPaginacion();
        };
        function BorrarElementosArray(arr, origen, destino) {
            var i = arr.length;
            while (i--) {
                //if (arr[i].Remesa.CiudadDestinatario.Codigo === value && (arr[i].Seleccion == false || arr[i].Seleccion == undefined) ) {
                if (arr[i].Remesa.CiudadRemitente.Codigo === origen && arr[i].Remesa.CiudadDestinatario.Codigo === destino) {
                    arr.splice(i, 1);
                }
            }
            return arr;
        }
        $scope.SlcTodo = function (Seleccion) {
            for (var i = 0; i < $scope.DetalleRemesas.length; i++) {
                $scope.DetalleRemesas[i].Seleccion = Seleccion;
            }
        };
        //------------------------------Cargar Detalle Remesas -----------------//
        //--Paguinacion Detalle
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.DetalleRemesasPag = [];
                $scope.paginaActual = 1;
                for (var i = 0; i < 10; i++) {
                    $scope.DetalleRemesasPag.push($scope.DetalleRemesas[i]);
                }
            } else {
                $scope.DetalleRemesasPag = $scope.DetalleRemesas;
            }
        };
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1;
                var a = $scope.paginaActual * 10;
                if (a < $scope.totalRegistros) {
                    $scope.DetalleRemesasPag = [];
                    for (var i = a - 10; i < a; i++) {
                        $scope.DetalleRemesasPag.push($scope.DetalleRemesas[i]);
                    }
                }
            }
            else {
                $scope.PrimerPagina();
            }
        };
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1;
                var a = $scope.paginaActual * 10;
                if (a < $scope.totalRegistros) {
                    $scope.DetalleRemesasPag = [];
                    for (var i = a - 10; i < a; i++) {
                        $scope.DetalleRemesasPag.push($scope.DetalleRemesas[i]);
                    }
                } else {
                    $scope.UltimaPagina();
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina();
            }
        };
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas;
                var a = $scope.paginaActual * 10;
                $scope.DetalleRemesasPag = [];
                for (var i = a - 10; i < $scope.DetalleRemesas.length; i++) {
                    $scope.DetalleRemesasPag.push($scope.DetalleRemesas[i]);
                }
            }
        }
        function ResetPaginacion() {
            if ($scope.DetalleRemesas.length > CERO) {
                $scope.totalRegistros = $scope.DetalleRemesas.length;
                $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                $scope.PrimerPagina();
            }
        }
        //-----------------------------Planificacion Despachos--------------------------//
        var VehiculosDespacho = [];
        var RemesasDespacho = [];
        $scope.GenerarPlanifiacionDespacho = function () {
            $scope.PlanillaDespacho = [];
            $scope.IndicePlanilla = 0;
            if (DatosRequeridosGenerarPlanifiacion()) {
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Generando Planificación...");
                $timeout(function () {
                    blockUI.message("Generando Planificación...");
                    GenerarPlanificacionPlanillasDespacho();
                }, 100);
                //Bloqueo Pantalla
            }
        };
        function GenerarPlanificacionPlanillasDespacho() {
            var i = 0;
            var j = 0;
            //--Genera Vehiculos Despacho
            VehiculosDespacho = [];
            for (i = 0; i < $scope.VehiculosDisponibles.length; i++) {
                if ($scope.VehiculosDisponibles[i].Seleccion == true) {
                    VehiculosDespacho.push($scope.VehiculosDisponibles[i]);
                }
            }
            VehiculosDespacho.forEach(vehiculo => { vehiculo.Disponible = 1; });
            //--Genera Vehiculos Despacho
            //--Genera Remesas Despacho
            RemesasDespacho = [];
            for (i = 0; i < $scope.DetalleRemesas.length; i++) {
                if ($scope.DetalleRemesas[i].Seleccion == true) {
                    RemesasDespacho.push($scope.DetalleRemesas[i]);
                }
            }
            RemesasDespacho.forEach(remesa => { remesa.Asignada = 0; });
            //--Genera Remesas Despacho
            //--Agrega Remesas a Vehiculos
            $scope.PlanillaDespacho = [];
            var ProcesoPlanilla = true;
            while (ProcesoPlanilla) {
                var Vehi = RetornarIdVehiculoViable();
                if (Vehi >= 0) {
                    var iDVehi = Vehi.Id;
                    var Ruta = Vehi.Ruta;
                    var PesoDisponible = VehiculosDespacho[iDVehi].Capacidad;
                    var TotalCargado = 0;
                    var ObjPlanilla = {
                        Vehiculo: '',
                        Remesas: [],
                        Semirremolque: ''
                    };
                    for (i = 0; i < RemesasDespacho.length; i++) {
                        if (RemesasDespacho[i].Asignada == 0) {
                            if ((PesoDisponible - RemesasDespacho[i].Remesa.PesoCliente) >= 0) {
                                RemesasDespacho[i].Asignada = 1;
                                ObjPlanilla.Vehiculo = {
                                    Codigo: VehiculosDespacho[iDVehi].Vehiculo.Codigo,
                                    Placa: VehiculosDespacho[iDVehi].Vehiculo.Placa,
                                    Conductor: VehiculosDespacho[iDVehi].Conductor,
                                    Capacidad: VehiculosDespacho[iDVehi].Capacidad,
                                    CapacidadM3: VehiculosDespacho[iDVehi].CapacidadM3,
                                    Tenedor: VehiculosDespacho[iDVehi].Tenedor,
                                    TipoVehiculo: VehiculosDespacho[iDVehi].TipoVehiculo,
                                };
                                ObjPlanilla.Remesas.push(RemesasDespacho[i]);
                                ObjPlanilla.Semirremolque = VehiculosDespacho[iDVehi].Vehiculo.Semirremolque;
                                PesoDisponible -= RemesasDespacho[i].Remesa.PesoCliente;
                                TotalCargado += RemesasDespacho[i].Remesa.PesoCliente;
                            }
                        }
                    }
                    if (ObjPlanilla.Vehiculo != '') {
                        ObjPlanilla.PesoTotal = TotalCargado;
                        //--Cargar Informacion Vehiculo, Tarifarios y Rutas
                        ObjPlanilla.Vehiculo.Tenedor = ObtenerTenedor(ObjPlanilla.Vehiculo.Tenedor.Codigo);
                        var ObjTarifario = ObtenerInformacionTarifario(ObjPlanilla.Vehiculo.Tenedor.Proveedor.Tarifario.Codigo);
                        if (ObjTarifario != null) {
                            ObjPlanilla.Tarifario = ObjTarifario.Tarifario;
                            ObjPlanilla.ListadoTarifas = ObjTarifario.Tarifas;
                            ObjPlanilla.ListadoRutas = ObjTarifario.Rutas;
                        }
                        if (ObjPlanilla.Semirremolque != undefined) {
                            if (ObjPlanilla.Semirremolque.Codigo > 0) {
                                ObjPlanilla.Semirremolque = $scope.CargarSemirremolqueCodigo(ObjPlanilla.Semirremolque.Codigo);
                            }
                        }
                        //--Cargar Informacion Vehiculo, Tarifarios y Rutas
                        $scope.PlanillaDespacho.push(ObjPlanilla);
                        VehiculosDespacho[iDVehi].Disponible = 0;
                    }
                }
                else {
                    ProcesoPlanilla = false;
                }
            }
            //--Agrega Remesas a Vehiculos
            //--Totalizar para Visualizaciones
            for (i = 0; i < $scope.PlanillaDespacho.length; i++) {
                $scope.PlanillaDespacho[i].Indice = i;
                $scope.PlanillaDespacho[i].Guias = $scope.PlanillaDespacho[i].Remesas.length;
                var Remesas = $scope.PlanillaDespacho[i].Remesas;
                var SumUnidades = 0;
                var TotalFleteCliente = 0;
                var FleteCliente = 0;
                var VolumenTotal = 0;
                for (j = 0; j < Remesas.length; j++) {
                    SumUnidades += Remesas[j].Remesa.CantidadCliente;
                    TotalFleteCliente += Remesas[j].Remesa.TotalFleteCliente;
                    FleteCliente += Remesas[j].Remesa.ValorFleteCliente;
                    VolumenTotal += Remesas[j].VolumenM3;
                }
                $scope.PlanillaDespacho[i].CantidadTotal = SumUnidades;
                $scope.PlanillaDespacho[i].FleteCliente = FleteCliente;
                $scope.PlanillaDespacho[i].VolumenTotal = VolumenTotal;
            }
            //--Totalizar para Visualizaciones
            blockUI.stop();
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);

        }

        $scope.ObtenerTarifasCompra = function (planilla) {
            if (planilla.Ruta !== undefined && planilla.Ruta !== null && planilla.Ruta !== '') {
                if (planilla.Vehiculo.Tenedor.Proveedor.Tarifario.Codigo > 0) {
                    var ListaTarifas = [];
                    var ListaTipoTarifas = [];
                    for (var i = 0; i < planilla.ListadoTarifas.length; i++) {
                        var item = planilla.ListadoTarifas[i];
                        if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO ||
                            item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                            if (planilla.Ruta.Codigo == item.Ruta.Codigo) {
                                var TipoTarifaAux = {
                                    Codigo: item.TipoTarifaTransportes.Codigo,
                                    Nombre: item.TipoTarifaTransportes.Nombre,
                                    CodigoTarifa: item.TipoTarifaTransportes.TarifaTransporte.Codigo,
                                    ValorFlete: item.ValorFlete
                                };
                                var Existe = false;
                                for (var j = 0; j < ListaTarifas.length; j++) {
                                    if (ListaTarifas[j].Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                        Existe = true;
                                        break;
                                    }
                                }
                                if (Existe == false) {
                                    ListaTarifas.push(item.TipoTarifaTransportes.TarifaTransporte);
                                }
                                ListaTipoTarifas.push(TipoTarifaAux);
                            }
                        }
                    }
                    planilla.Tarifa = ListaTarifas[0];
                    planilla.ListadoTarifasFiltradas = ListaTarifas;
                    planilla.ListadoTipoTarifas = ListaTipoTarifas;
                    GestionarImpuestos(planilla);
                    $scope.CambioTarifa(planilla);
                }
            }
        };
        //--Calculos Vehiculo Viable por capacidad y total kg de remesas
        function RetornarIdVehiculoViable() {
            var IdRetorno = -1;
            var PesoRemesas = 0;
            var ArrDiferencial = [];
            var i = 0;

            for (i = 0; i < RemesasDespacho.length; i++) {
                if (RemesasDespacho[i].Asignada == 0) {
                    PesoRemesas += RemesasDespacho[i].Remesa.PesoCliente;
                }
            }

            if (PesoRemesas > 0) {
                var ExisteDisponible = false;
                //--Calcula dependendiendo Si existe un vehiculo para llevar el total
                for (i = 0; i < VehiculosDespacho.length; i++) {
                    if (VehiculosDespacho[i].Disponible == 1) {
                        if (VehiculosDespacho[i].Capacidad - PesoRemesas > 0) {
                            ExisteDisponible = true;
                            ArrDiferencial.push({
                                Diferencial: VehiculosDespacho[i].Capacidad - PesoRemesas,
                                Id: i
                            });
                        }
                    }
                }
                //--Calcula dependendiendo Si existe un vehiculo para llevar el total
                //--Calcula del que mas pueda llevar
                if (ExisteDisponible == false) {
                    for (i = 0; i < VehiculosDespacho.length; i++) {
                        if (VehiculosDespacho[i].Disponible == 1) {
                            ExisteDisponible = true;
                            ArrDiferencial.push({
                                Diferencial: Math.abs(VehiculosDespacho[i].Capacidad - PesoRemesas),
                                Id: i
                            });
                        }
                    }
                }
                //--Calcula del que mas pueda llevar

                if (ExisteDisponible) {
                    ArrDiferencial = OrdenarArreglo(ArrDiferencial);
                    IdRetorno = ArrDiferencial[0].Id;
                }

            }
            return IdRetorno;
        }
        //--Calculos Vehiculo Viable por capacidad y total kg de remesas
        //--Ordena Arreglo de menor a mayor
        function OrdenarArreglo(Arr) {
            var Tmp;
            for (var i = 1; i < Arr.length; i++) {
                for (var j = 0; j < Arr.length - 1; j++) {
                    if (Arr[j].Diferencial > Arr[j + 1].Diferencial) {
                        Tmp = Arr[j];
                        Arr[j] = Arr[j + 1];
                        Arr[j + 1] = Tmp;
                    }
                }
            }
            return Arr;
        }
        //--Ordena Arreglo de menor a mayor
        //--Validacion para proceso de planificacion
        function DatosRequeridosGenerarPlanifiacion() {
            $scope.MensajesErrorPlanifacion = [];
            var continuar = true;

            if ($scope.VehiculosDisponibles.length == 0) {
                $scope.MensajesErrorPlanifacion.push('Debe haber al menos un vehiculo marcado');
                continuar = false;
            }
            else {
                var CountVehiculo = 0;
                for (var i = 0; i < $scope.VehiculosDisponibles.length; i++) {
                    if ($scope.VehiculosDisponibles[i].Seleccion == true) {
                        CountVehiculo += 1;
                    }
                }
                if (CountVehiculo == 0) {
                    $scope.MensajesErrorPlanifacion.push('Debe haber al menos un vehiculo marcado');
                    continuar = false;
                }
            }

            if ($scope.DetalleRemesas.length == 0) {
                $scope.MensajesErrorPlanifacion.push('Debe haber al menos una remesa marcada');
                continuar = false;
            }
            else {
                var CountRemesa = 0;
                for (var i = 0; i < $scope.DetalleRemesas.length; i++) {
                    if ($scope.DetalleRemesas[i].Seleccion == true) {
                        CountRemesa += 1;
                    }
                }
                if (CountRemesa == 0) {
                    $scope.MensajesErrorPlanifacion.push('Debe haber al menos una remesa marcada');
                    continuar = false;
                }
            }

            if ($scope.VehiculosDisponibles.length > 0 && $scope.DetalleRemesas.length > 0) {
                var CapacidadVehi = 0
                for (var i = 0; i < $scope.VehiculosDisponibles.length; i++) {
                    if ($scope.VehiculosDisponibles[i].Seleccion == true) {
                        CapacidadVehi += $scope.VehiculosDisponibles[i].Capacidad;
                    }
                }
                var PesoRemesas = 0;
                for (var i = 0; i < $scope.DetalleRemesas.length; i++) {
                    if ($scope.DetalleRemesas[i].Seleccion == true) {
                        PesoRemesas += $scope.DetalleRemesas[i].Remesa.PesoCliente;
                    }
                }
                if (PesoRemesas > CapacidadVehi) {
                    $scope.MensajesErrorPlanifacion.push('El peso total de las remesas seleccionadas, excede la capacidad de la flota disponible, ' + MascaraValores(PesoRemesas) + ' kg');
                    continuar = false;
                }
            }

            return continuar;
        }
        //--Validacion para proceso de planificacion
        function ObtenerTenedor(Codigo) {
            var Tenedor = null;
            if (Codigo > 0) {
                var Response = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, Sync: true });
                if (Response.ProcesoExitoso === true) {
                    Tenedor = Response.Datos;
                }
            }
            return Tenedor;
        }
        //--Obtiene Tarifario, Tarifas y Rutas Filtradas
        function ObtenerInformacionTarifario(Codigo) {
            var objTarifario = null;
            var Tarifas = [];
            var RutasFiltradas = [];
            if (Codigo > 0) {
                var Response = TarifarioComprasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, Sync: true });
                if (Response.ProcesoExitoso === true) {
                    if (Response.Datos.Estado = ESTADO_ACTIVO) {
                        var Tarifario = Response.Datos;
                        for (var i = 0; i < Tarifario.Tarifas.length; i++) {
                            if (Tarifario.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO ||
                                Tarifario.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                                Tarifas.push(Tarifario.Tarifas[i]);
                            }
                        }
                        for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                            var aplica = false;
                            for (var j = 0; j < Tarifas.length; j++) {
                                if ($scope.ListadoRutas[i].Codigo == Tarifas[j].Ruta.Codigo) {
                                    aplica = true;
                                    break;
                                }
                            }
                            if (aplica > 0) {
                                RutasFiltradas.push($scope.ListadoRutas[i]);
                            }
                        }
                    }
                    objTarifario = {
                        Tarifario: Tarifario,
                        Tarifas: Tarifas,
                        Rutas: RutasFiltradas,
                    };
                }
            }
            return objTarifario;
        }
        //--Obtiene Tarifario, Tarifas y Rutas Filtradas
        //--Gestion de cambios de tarifa y tipo tarifa
        $scope.CambioTarifa = function (Planilla) {
            var ListaTipoTarifaFiltradas = [];
            for (var i = 0; i < Planilla.ListadoTipoTarifas.length; i++) {
                if (Planilla.ListadoTipoTarifas[i].CodigoTarifa == Planilla.Tarifa.Codigo) {
                    ListaTipoTarifaFiltradas.push(Planilla.ListadoTipoTarifas[i]);
                }
            }
            Planilla.ListadoTipoTarifasFiltradas = ListaTipoTarifaFiltradas;
            Planilla.TipoTarifa = Planilla.ListadoTipoTarifasFiltradas[0];
            Planilla.ValorFleteTransportador = MascaraValores(Planilla.ListadoTipoTarifasFiltradas[0].ValorFlete);
            //--Impuestos
            CalcularImpuestos(Planilla);
            //--Impuestos
        };
        $scope.CambioTipoTarifa = function (Planilla) {
            Planilla.ValorFleteTransportador = MascaraValores(Planilla.TipoTarifa.ValorFlete);
            //--Impuestos
            CalcularImpuestos(Planilla);
            //--Impuestos
        };
        //--Gestion de cambios de tarifa y tipo tarifa
        //--Gestion de Impuestos
        function GestionarImpuestos(Planilla) {
            var Impuestos = [];
            if (Planilla.Ruta !== undefined && Planilla.Ruta !== null && Planilla.Ruta !== '') {
                var response = ImpuestosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Estado: ESTADO_DEFINITIVO,
                    CodigoTipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO,
                    CodigoCiudad: Planilla.Ruta.CiudadOrigen.Codigo,
                    AplicaTipoDocumento: 1,
                    Sync: true
                });
                if (response.ProcesoExitoso == true) {
                    if (response.Datos.length > 0) {
                        Impuestos = response.Datos;
                        Impuestos.forEach(impuesto => { impuesto.ValorImpuesto = 0; });
                        Planilla.ListadoImpuestos = Impuestos;
                    }
                }
            }
        }
        function CalcularImpuestos(Planilla) {
            //--Impuestos
            var tmpImpuestosFiltrados = [];
            for (var i = 0; i < Planilla.ListadoImpuestos.length; i++) {
                var impuesto = {
                    Nombre: Planilla.ListadoImpuestos[i].Nombre,
                    Codigo: Planilla.ListadoImpuestos[i].Codigo,
                    Valor_tarifa: Planilla.ListadoImpuestos[i].Valor_tarifa,
                    valor_base: Planilla.ListadoImpuestos[i].valor_base,
                    Operacion: Planilla.ListadoImpuestos[i].Operacion,
                    ValorImpuesto: 0
                };
                if (Planilla.ListadoImpuestos[i].valor_base < parseInt(MascaraNumero(Planilla.ValorFleteTransportador))) {
                    impuesto.valor_base = parseInt(MascaraNumero(Planilla.ValorFleteTransportador));
                }
                impuesto.ValorImpuesto = Math.round(impuesto.Valor_tarifa * impuesto.valor_base);
                tmpImpuestosFiltrados.push(impuesto);
            }
            Planilla.ImpuestosFiltrados = tmpImpuestosFiltrados;
            Planilla.ValorImpuesto = 0;
            Planilla.TotalFleteTransportador = 0;
            for (var i = 0; i < Planilla.ImpuestosFiltrados.length; i++) {
                switch (Planilla.ImpuestosFiltrados[i].Operacion) {
                    case 1:
                        Planilla.ValorImpuesto += Planilla.ImpuestosFiltrados[i].ValorImpuesto;
                        break;
                    case 2:
                        Planilla.ValorImpuesto -= Planilla.ImpuestosFiltrados[i].ValorImpuesto;
                        break;
                    default:
                        Planilla.ValorImpuesto -= Planilla.ImpuestosFiltrados[i].ValorImpuesto;
                        break;
                }
            }
            Planilla.TotalFleteTransportador = MascaraValores(RevertirMV(Planilla.ValorFleteTransportador) + Planilla.ValorImpuesto - RevertirMV(Planilla.Anticipo));
            Planilla.ValorImpuesto = MascaraValores(Planilla.ValorImpuesto);
            //--Impuestos
        }
        //--Gestion de Impuestos
        //--Proceso Guardar Planilla Despachos
        $scope.ConfirmacionGenerar = function (IndicePlanilla) {
            $scope.IndicePlanilla = IndicePlanilla;
            if (DatosRequeridosGenerarPlanilla()) {
                showModal('modalConfirmacionGenerar');
            }
            else {
                showModal('modalErrorPlanillaDespacho');
            }
        };
        $scope.IniciarProcesoGenerarPlanilla = function () {
            closeModal('modalConfirmacionGenerar');
            //Bloqueo Pantalla
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            var strDocu = $scope.TipoPlanilla.Codigo == CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO ? 'Despacho' : 'Entregas';
            blockUI.start("Generando Planilla " + strDocu + "...");
            $timeout(function () {
                blockUI.message("Generando Planilla " + strDocu + "...");
                GenerarPlanillasDespacho();
            }, 100);
            //Bloqueo Pantalla
        };
        function GenerarPlanillasDespacho() {
            var i = 0;
            var Planilla = $scope.PlanillaDespacho[$scope.IndicePlanilla];
            //--Genera Anticipo
            if (Planilla.Anticipo > 0) {
                var CuentaPorPagar = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                    CodigoAlterno: '',
                    Fecha: new Date(Planilla.FechaSalida),
                    Tercero: { Codigo: Planilla.Vehiculo.Conductor.Codigo },
                    DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO },
                    CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO,
                    Numeracion: '',
                    CuentaPuc: { Codigo: 0 },
                    ValorTotal: Planilla.Anticipo,
                    Abono: 0,
                    Saldo: Planilla.Anticipo,
                    FechaCancelacionPago: new Date(Planilla.FechaSalida),
                    Aprobado: 1,
                    UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    FechaAprobo: new Date(Planilla.FechaSalida),
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                    Vehiculo: { Codigo: Planilla.Vehiculo.Codigo },
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                };
            }
            //--Genera Anticipo
            //--Detalle De Remesas
            var ListadoRemesaDetalle = [];
            for (i = 0; i < Planilla.Remesas.length; i++) {
                var remesa = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Remesa: {
                        Numero: Planilla.Remesas[i].Remesa.Numero,
                        NumeroDocumento: Planilla.Remesas[i].Remesa.NumeroDocumento
                    }
                };
                ListadoRemesaDetalle.push(remesa);
            }
            //--Detalle De Remesas
            //--Detalle Impuestos
            var ListadoDetalleImpuestos = [];
            for (i = 0; i < Planilla.ImpuestosFiltrados.length; i++) {
                var impuesto = {
                    CodigoImpuesto: Planilla.ImpuestosFiltrados[i].Codigo,
                    ValorTarifa: Planilla.ImpuestosFiltrados[i].Valor_tarifa,
                    ValorBase: Planilla.ImpuestosFiltrados[i].valor_base,
                    ValorImpuesto: Planilla.ImpuestosFiltrados[i].ValorImpuesto
                };
                ListadoDetalleImpuestos.push(impuesto);
            }
            //--Detalle Impuestos
            //--Genera Fecha Salida con hora
            var FechaHoraSalida = new Date(Planilla.FechaSalida);
            var Horasminutos = [];
            Horasminutos = Planilla.HoraSalida.split(':');
            if (Horasminutos.length > 0) {
                FechaHoraSalida.setHours(Horasminutos[0]);
                FechaHoraSalida.setMinutes(Horasminutos[1]);
            }
            //--Genera Fecha Salida con hora
            //--Genera Obj Planilla
            var PlanillaDespachos = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: $scope.TipoPlanilla.Codigo,
                Planilla: {
                    Fecha: new Date(Planilla.FechaSalida),
                    FechaHoraSalida: FechaHoraSalida,
                    Ruta: { Codigo: Planilla.Ruta.Codigo },
                    Vehiculo: {
                        Codigo: Planilla.Vehiculo.Codigo,
                        Tenedor: { Codigo: Planilla.Vehiculo.Tenedor.Codigo },
                        Conductor: { Codigo: Planilla.Vehiculo.Conductor.Codigo }
                    },
                    Semirremolque: { Codigo: Planilla.Semirremolque != '' && Planilla.Semirremolque != null && Planilla.Semirremolque != undefined ? Planilla.Semirremolque.Codigo : 0 },
                    Cantidad: Planilla.CantidadTotal,
                    Peso: Planilla.PesoTotal,
                    ValorFleteTransportador: Planilla.ValorFleteTransportador,
                    ValorAnticipo: Planilla.Anticipo,
                    ValorImpuestos: Planilla.ValorImpuesto,
                    ValorPagarTransportador: Planilla.TotalFleteTransportador,
                    ValorFleteCliente: Planilla.TotalFleteCliente,
                    Estado: { Codigo: ESTADO_BORRADOR },
                    TarifaTransportes: { Codigo: Planilla.Tarifa.Codigo },
                    TipoTarifaTransportes: { Codigo: Planilla.TipoTarifa.Codigo },
                    ValorBruto: Planilla.FleteCliente,
                    Detalles: ListadoRemesaDetalle,
                    DetalleImpuesto: ListadoDetalleImpuestos
                },
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                CuentaPorPagar: CuentaPorPagar,
                Sync: true
            };
            //--Genera Obj Planilla
            //console.log(PlanillaDespachos);
            //--Guarda Planilla y Manifiesto
            try {
                var Response = PlanillaGuiasFactory.Guardar(PlanillaDespachos);
                if (Response.ProcesoExitoso == true) {
                    if (Response.Datos > CERO) {
                        var strDocu = $scope.TipoPlanilla.Codigo == CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO ? 'despacho' : 'entregas';
                        ShowSuccess('Se guardó la planilla ' + strDocu + ' con el número ' + Response.Datos);
                        //--Deshabilita Checks de Vehiculos Resumen y Detalle Remesas
                        for (i = 0; i < $scope.VehiculosDisponibles.length; i++) {
                            if ($scope.VehiculosDisponibles[i].Seleccion === true) {
                                $scope.VehiculosDisponibles[i].Deshabilitar = true;
                            }
                        }
                        for (i = 0; i < $scope.RemesasAgencia.length; i++) {
                            if ($scope.RemesasAgencia[i].Seleccion === true) {
                                $scope.RemesasAgencia[i].Deshabilitar = true;
                            }
                        }
                        for (i = 0; i < $scope.DetalleRemesas.length; i++) {
                            if ($scope.DetalleRemesas[i].Seleccion === true) {
                                $scope.DetalleRemesas[i].Deshabilitar = true;
                            }
                        }
                        var tmpRemesas = [];
                        for (i = 0; i < $scope.DetalleRemesas.length; i++) {
                            var Existe = false;
                            for (var j = 0; j < Planilla.Remesas.length; j++) {
                                if ($scope.DetalleRemesas[i].Remesa.Numero === Planilla.Remesas[j].Remesa.Numero) {
                                    Existe = true;
                                    break;
                                }
                            }
                            if (Existe === false) {
                                tmpRemesas.push($scope.DetalleRemesas[i]);
                            }
                        }
                        $scope.DetalleRemesas = tmpRemesas;
                        ResetPaginacion();

                        var TMPPlanilla = [];
                        for (i = 0; i < $scope.PlanillaDespacho.length; i++) {
                            if (i !== $scope.IndicePlanilla) {
                                TMPPlanilla.push($scope.PlanillaDespacho[i]);
                            }
                        }
                        $scope.PlanillaDespacho = TMPPlanilla;
                        for (i = 0; i < $scope.PlanillaDespacho.length; i++) {
                            $scope.PlanillaDespacho[i].Indice = i;
                        }
                        $scope.DeshabilitarGenerarPlanificacion = true;
                        $scope.DeshabilitarTipoPlanilla = true;
                        if ($scope.PlanillaDespacho.length === 0) {
                            $scope.CargarResumenRemesas();
                        }
                        //--Deshabilita Checks de Vehiculos Resumen y Detalle Remesas
                        blockUI.stop();
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                    }
                    else {
                        ShowError(Response.statusText);
                        blockUI.stop();
                        $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                    }
                }
                else {
                    ShowError(Response.statusText);
                    blockUI.stop();
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
                }
            }
            catch (e) {
                ShowError(Response.statusText);
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 1000);
            }

            //--Guarda Planilla y Manifiesto
        }
        function DatosRequeridosGenerarPlanilla() {
            $scope.MensajesErrorGenerarPlanilla = [];
            var continuar = true;
            var Planilla = $scope.PlanillaDespacho[$scope.IndicePlanilla];
            var FechaSalida = new Date(Planilla.FechaSalida).withoutTime();
            var FechaActual = new Date().withoutTime();

            if (Planilla.Ruta == undefined || Planilla.Ruta == '' || Planilla.Ruta == null) {
                $scope.MensajesErrorGenerarPlanilla.push('Debe ingresar ruta');
                continuar = false;
            }
            if (Planilla.Tarifa == undefined || Planilla.Tarifa == '' || Planilla.Tarifa == null) {
                $scope.MensajesErrorGenerarPlanilla.push('Debe ingresar tarifa');
                continuar = false;
            }
            if (Planilla.TipoTarifa == undefined || Planilla.TipoTarifa == '' || Planilla.TipoTarifa == null) {
                $scope.MensajesErrorGenerarPlanilla.push('Debe ingresar tipo tarifa');
                continuar = false;
            }
            if (Planilla.FechaSalida == undefined || Planilla.FechaSalida == '' || Planilla.FechaSalida == null) {
                $scope.MensajesErrorGenerarPlanilla.push('Debe ingresar fecha salida');
                continuar = false;
            }
            else if (FechaSalida < FechaActual) {
                $scope.MensajesErrorGenerarPlanilla.push('La fecha de salida ingresada no se puede ser menor a la fecha actual');
                continuar = false;
            }
            if (Planilla.HoraSalida == undefined || Planilla.HoraSalida == '' || Planilla.HoraSalida == null) {
                $scope.MensajesErrorGenerarPlanilla.push('Debe ingresar hora salida');
                continuar = false;
            }
            else {
                var horas = Planilla.HoraSalida.split(':');
                if (horas.length < 2) {
                    $scope.MensajesErrorGenerarPlanilla.push('Debe ingresar una hora valida');
                    continuar = false;
                }
                else if (FechaSalida.getTime() == FechaActual.getTime()) {
                    if (parseInt(horas[0]) < new Date().getHours()) {
                        $scope.MensajesErrorGenerarPlanilla.push('La hora ingresada debe ser mayor a la actual');
                        continuar = false;
                    }
                    else if (parseInt(horas[0]) == new Date().getHours()) {
                        if (parseInt(horas[1]) < new Date().getMinutes()) {
                            $scope.MensajesErrorGenerarPlanilla.push('La hora ingresada debe ser mayor a la actual');
                            continuar = false;
                        }
                        else {
                            if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                                $scope.MensajesError.push('Debe ingresar una hora valida');
                                continuar = false;
                            }
                        }
                    }
                }
                else {
                    if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                        $scope.MensajesError.push('Debe ingresar una hora valida');
                        continuar = false;
                    }
                }
            }

            if (Planilla.Anticipo !== undefined && Planilla.Anticipo !== '') {
                if (RevertirMV(Planilla.Anticipo) > RevertirMV(Planilla.ValorFleteTransportador)) {
                    $scope.MensajesErrorGenerarPlanilla.push('El valor del anticipo no puede ser mayor al valor del flete del transportador');
                    $scope.PlanillaDespacho[$scope.IndicePlanilla].Anticipo = 0;
                    continuar = false;
                }
            }
            return continuar;
        }
        //--Proceso Guardar Planilla Despachos
        //-----------------------------Planificacion Despachos--------------------------//
        //-----------------------------Ruta Mapa Despachos--------------------------//

        $scope.VerRutaMapa = function (resumen) {
            var marker = [];
            var map, infoWindow;
            var Coordenadas = [];
            var DirectionsServices = new google.maps.DirectionsService();
            var DirectionsDisplay = new google.maps.DirectionsRenderer();

            var DireccionOrigen = ObtenerDireccionOficina(resumen.CiudadOrigen.Codigo);
            var DireccionDestino = ObtenerDireccionOficina(resumen.CiudadDestino.Codigo);
            ObtenerCoordenadasDireccion(DireccionOrigen);
            ObtenerCoordenadasDireccion(DireccionDestino);

            function initMap() {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: Coordenadas[0],
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    };
                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                    DirectionsDisplay.setMap(map);

                    DirectionsServices.route({
                        origin: Coordenadas[0],
                        destination: Coordenadas[1],
                        travelMode: google.maps.TravelMode.DRIVING
                    }, (response, status) => {
                            if (status === google.maps.DirectionsStatus.OK) {
                                DirectionsDisplay.setDirections(response);
                                resumen.Distancia = response.routes[0].legs[0].distance.text;
                            }
                            else {
                                ShowError('No se pudo generar la ruta: ' + status);
                            }
                    });
                }
                showModal("modalMostrarRutaMapa");
            }

            function ObtenerCoordenadasDireccion(direccion) {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'address': direccion }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        Coordenadas.push({ lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() });
                        if (Coordenadas.length >= 2) {
                            initMap();
                        }
                    }
                });
            }
        };
        function ObtenerDireccionOficina(CodigoCiudad) {
            var direccion = null;
            var ResponsOficina = OficinasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CodigoCiudad: CodigoCiudad,
                Estado: ESTADO_DEFINITIVO,
                Sync: true
            });
            if (ResponsOficina.ProcesoExitoso == true) {
                if (ResponsOficina.Datos.length > 0) {
                    direccion = ResponsOficina.Datos[0].Direccion;
                }
            }
            return direccion;
        }
        $scope.LimpiarMapa = function () {
            $("#gmaps").html("");
        };
        //-----------------------------Ruta Mapa Despachos--------------------------//
        //----Mascaras
        $scope.MascaraHora = function (Planilla) {
            Planilla.HoraSalida = MascaraHora(Planilla.HoraSalida);
        };
        $scope.MascaraAnticipo = function (Planilla) {
            Planilla.Anticipo = MascaraValores(Planilla.Anticipo);
            Planilla.TotalFleteTransportador = MascaraValores(RevertirMV(Planilla.ValorFleteTransportador) + RevertirMV(Planilla.ValorImpuesto) - RevertirMV(Planilla.Anticipo));
        };
        //----Mascaras
    }]);
