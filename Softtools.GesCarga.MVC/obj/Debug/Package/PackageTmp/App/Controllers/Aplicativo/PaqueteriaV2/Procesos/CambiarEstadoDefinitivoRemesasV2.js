﻿SofttoolsApp.controller("CambiarEstadoDefinitivoRemesasV2Ctrl", ['$scope', '$timeout', '$linq', 'blockUI', '$routeParams', 'RemesaGuiasFactory', 'EmpresasFactory','DocumentosFactory',
    'OficinasFactory', 'blockUIConfig',
    function ($scope, $timeout, $linq, blockUI, $routeParams, RemesaGuiasFactory, EmpresasFactory, DocumentosFactory,
        OficinasFactory, blockUIConfig) {
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        $scope.Titulo = 'CAMBIAR ESTADO  GUÍAS'; 
        $scope.MapaSitio = [{ Nombre: 'Paqueteria' }, { Nombre: 'Procesos' }, { Nombre: 'Cambiar Estado Guías' }];
        try {
            $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_CAMBIAR_ESTADO_GUIAS);
            $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

            $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
            $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
            $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
            $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        //--------------------------Permisos, Titulo, Mapa-------------------------//
        //--------------------------Variables-------------------------//
        var filtros = {};
        $scope.cantidadRegistrosPorPaginas = 10;
        $scope.paginaActual = 1;
        $scope.ModalErrorCompleto = '';
        $scope.ModalError = '';
        $scope.MostrarMensajeError = false;
        $scope.ArcchivoTemporal = true;
        $scope.pref = '';
        $scope.ListaDocumentos = []
        $scope.CadenaFormatos = ".BMP, .GIF, .JPG, .JPEG, .TIF, .TIFF, .PNG, .doc, .docx, .docm, .dotx, .dotm, .dot, .xps, .txt, .xml, .odt, .xlsx, .xlsm, .xlsb, .xltx, .xltm, .xls, .xlt, .xml, .xlam, .xla, .xlw, .xlr, .pdf"

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: $scope.Sesion.UsuarioAutenticado.Oficinas
        };
        $scope.ListaDocuemntos = [];
        $scope.ListadoGuias = [];
        $scope.ListadoOficinasCrea = [
            { Codigo: 0, Nombre: '(TODAS)' }
        ];
        $scope.ListadoOficinasActual = [
            { Codigo: 0, Nombre: '(TODAS)' }
        ]; 
        $scope.ListadoEstadoRemesaPaqueteria = [ { Codigo: 0, Nombre: 'BORRADOR' }, { Codigo: 1, Nombre: 'DEFINITIVO' } ];
        $scope.ModeloEstado = $scope.ListadoEstadoRemesaPaqueteria[1];

        $scope.PagRemesas = {
            Buscando: false,
            totalRegistros: 0,
            totalPaginas: 0,
            paginaActual: 1,
            ResultadoSinRegistros: ''
        };
        //--------------------------Variables-------------------------//
        //--------------------------Validaciones Proceso-------------------------//
        if ($scope.Sesion.UsuarioAutenticado.ManejoFechaFinalDefaultHoy == true) {
            $scope.Modelo.FechaInicial = new Date();
            $scope.Modelo.FechaFinal = new Date();
        }
        //--------------------------Validaciones Proceso-------------------------//
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------Init
        $scope.InitLoad = function () {
            //--Oficinas
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoOficinasCrea.push(item);
                            $scope.ListadoOficinasActual.push(item);
                        });
                        $scope.Modelo.OficinaCrea = $scope.ListadoOficinasCrea[0];
                        $scope.Modelo.OficinaActual = $scope.ListadoOficinasActual[0];
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        //----------------------------Informacion Requerida para funcionar---------------------------------//
        //----------------------------Funciones Generales---------------------------------//
        //----FUNCION DE PAGINACIÓN
        function ResetPaginacion(obj) {
            obj.totalRegistros = obj.array.length;
            obj.totalPaginas = Math.ceil(obj.totalRegistros / 10);
            $scope.PrimerPagina(obj);
        }
        $scope.PrimerPagina = function (obj) {
            if (obj.totalRegistros > 10) {
                obj.PagArray = [];
                obj.paginaActual = 1;
                for (var i = 0; i < 10; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            } else {
                obj.PagArray = obj.array;
            }
        };
        $scope.Anterior = function (obj) {
            if (obj.paginaActual > 1) {
                obj.paginaActual -= 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                }
            }
            else {
                $scope.PrimerPagina(obj);
            }
        };
        $scope.Siguiente = function (obj) {
            if (obj.paginaActual < obj.totalPaginas) {
                obj.paginaActual += 1;
                var a = obj.paginaActual * 10;
                if (a < obj.totalRegistros) {
                    obj.PagArray = [];
                    for (var i = a - 10; i < a; i++) {
                        obj.PagArray.push(obj.array[i]);
                    }
                } else {
                    $scope.UltimaPagina(obj);
                }
            } else if (obj.paginaActual == obj.totalPaginas) {
                $scope.UltimaPagina(obj);
            }
        };
        $scope.UltimaPagina = function (obj) {
            if (obj.totalRegistros > 10 && obj.totalPaginas > 1) {
                obj.paginaActual = obj.totalPaginas;
                var a = obj.paginaActual * 10;
                obj.PagArray = [];
                for (var i = a - 10; i < obj.array.length; i++) {
                    obj.PagArray.push(obj.array[i]);
                }
            }
        };
        //----------------------------Funciones Generales---------------------------------//
        function PantallaBloqueo(fun) {
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            blockUI.start("Buscando registros...");
            $timeout(function () { blockUI.message("Espere por favor..."); fun(); }, 100);
        }
        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO && DatosRequeridos()) {
                PantallaBloqueo(Find);
               
            }
        };

        function Find() {
            $scope.Buscando = true;
            $scope.MensajesError = [];
            $scope.ListadoGuias = [];
 

            var filtro = { 
                Codigo: 0,
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Estado: $scope.ModeloEstado.Codigo, 
                FechaInicial: $scope.Modelo.FechaInicial,
                FechaFinal: $scope.Modelo.FechaFinal,
                NumeroPlanilla: -1, 
                Numeracion: $scope.Modelo.Preimpreso,
                NumeroInicial: $scope.Modelo.NumeroRemesa,
                Oficina: { Codigo: $scope.Modelo.OficinaCrea.Codigo },
                OficinaActual: { Codigo: $scope.Modelo.OficinaActual.Codigo },
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA, 
            };   
            RemesaGuiasFactory.Consultar(filtro).
                then(function (response) {  
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoGuias = response.data.Datos;
                            $scope.PagRemesas.array = $scope.ListadoGuias;
                            $scope.PagRemesas.ResultadoSinRegistros = '';
                            ResetPaginacion($scope.PagRemesas);

                            console.log('llegaron', $scope.ListadoGuias)

                        }
                        else {
                            $scope.PagRemesas.totalRegistros = 0;
                            $scope.PagRemesas.totalPaginas = 0;
                            $scope.PagRemesas.paginaActual = 1;
                            $scope.PagRemesas.array = [];
                            ResetPaginacion($scope.PagRemesas);
                            $scope.PagRemesas.ResultadoSinRegistros = 'No hay datos para mostrar';
                        }
                    }
                }, function (response) {
                    $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; if (blockUI.state().blocking == true) { blockUI.reset(); } }, 500);
                    ShowError(response.statusText);
                });
        }

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            if (($scope.Modelo.FechaInicial === null || $scope.Modelo.FechaInicial === undefined || $scope.Modelo.FechaInicial === '')
                && ($scope.Modelo.FechaFinal === null || $scope.Modelo.FechaFinal === undefined || $scope.Modelo.FechaFinal === '')
                && ($scope.Modelo.NumeroRemesa === null || $scope.Modelo.NumeroRemesa === undefined || $scope.Modelo.NumeroRemesa === '' || $scope.Modelo.NumeroRemesa === 0 || isNaN($scope.Modelo.NumeroRemesa))
                && ($scope.Modelo.Preimpreso === null || $scope.Modelo.Preimpreso === undefined || $scope.Modelo.Preimpreso === '' || $scope.Modelo.Preimpreso === 0)) {
                $scope.MensajesError.push('Debe ingresar los filtros de fecha, número o Preimpreso');
                continuar = false;

            } else if (($scope.Modelo.NumeroRemesa !== null && $scope.Modelo.NumeroRemesa !== undefined && $scope.Modelo.NumeroRemesa !== '' && $scope.Modelo.NumeroRemesa !== 0)
                || ($scope.Modelo.Preimpreso !== null || $scope.Modelo.Preimpreso !== undefined || $scope.Modelo.Preimpreso !== '' || $scope.Modelo.Preimpreso !== 0)
                || ($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                || ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {


                if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')
                    && ($scope.Modelo.FechaFinal !== null && $scope.Modelo.FechaFinal !== undefined && $scope.Modelo.FechaFinal !== '')) {
                    if ($scope.Modelo.FechaFinal < $scope.Modelo.FechaInicial) {
                        $scope.MensajesError.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false;
                    }

                    else if ((($scope.Modelo.FechaFinal - $scope.Modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesError.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false;
                    }
                } else {
                    if (($scope.Modelo.FechaInicial !== null && $scope.Modelo.FechaInicial !== undefined && $scope.Modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = $scope.Modelo.FechaInicial;
                    }
                    else {
                        $scope.Modelo.FechaInicial = $scope.Modelo.FechaFinal;
                    }
                }
                if (($scope.Modelo.Preimpreso !== null && $scope.Modelo.Preimpreso !== undefined && $scope.Modelo.Preimpreso !== '' && $scope.Modelo.Preimpreso !== 0 && isNaN($scope.Modelo.Preimpreso) === true)) {
                    $scope.Modelo.Preimpreso = $scope.Modelo.Preimpreso;
                }
            }
            return continuar;
        }

        // Marcar o Desmarcar
        $scope.MarcadDesmarcarTodo = function (item) {
            if (item) {
                for (var i = 0; i < $scope.ListadoGuias.length; i++) {
                    $scope.ListadoGuias[i].Marcado = true;
                }
            }
            else {
                for (var i = 0; i < $scope.ListadoGuias.length; i++) {
                    $scope.ListadoGuias[i].Marcado = false;
                }
            }
        };

        $scope.CambiarEstado = function (item) {
            var countmarcadas = 0;
            if ($scope.ListadoGuias.length > 0) { 

                for (var i = 0; i < $scope.ListadoGuias.length; i++) {
                    console.log('iterando')
                    if ($scope.ListadoGuias[i].Marcado) {
                        console.log('marcada', $scope.ListadoGuias[i].Marcado)
                        countmarcadas++;
                    }
                }
                if (countmarcadas > 0) {
                    showModal('modalConfirmacionCarbiarEstado');
                }
                else {
                    ShowError('Debe seleccionar al menos una Guía');
                }
            }
            else {
                ShowError('Debe seleccionar al menos una Guía');
            }
        };

        $scope.CambiarEstadoRemesas = function () {

            closeModal('modalConfirmacionCarbiarEstado');
            blockUI.start();
            blockUI.message('Procesando Remesas...');

            var EstadoCambio = [];

            if ($scope.ModeloEstado.Codigo == 0) {
                EstadoCambio = {
                    Codigo: 1,
                    Nombre: 'DEFINITIVO'
                }
            } else {
                EstadoCambio = {
                    Codigo: 0,
                    Nombre: 'BORRADOR'
                }
            }

            $timeout(function () {
                var Entidad = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    RemesaPaqueteria: [],
                    Estado: EstadoCambio.Codigo
                };
                for (var obj = 0; obj < $scope.ListadoGuias.length; obj++) {
                    console.log('enviando', $scope.ListadoGuias[obj])
                    if ($scope.ListadoGuias[obj].Marcado) {
                        if ($scope.ListadoGuias[obj].Factura.NumeroDocumento == 0) {
                            Entidad.RemesaPaqueteria.push({
                                Numero: $scope.ListadoGuias[obj].Remesa.Numero,
                                NumeroDocumento: $scope.ListadoGuias[obj].Remesa.NumeroDocumento
                            });

                            filtros = {
                                Numero: $scope.ListadoGuias[obj].Remesa.Numero,
                                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                Observaciones: 'Cambio Estado Guia'
                            }

                            RemesaGuiasFactory.GuardarBitacoraGuias(filtros).
                                then(function (response) {
                                    if (response.data.ProcesoExitoso == true) {
                                        console.log('RENESA CAMBIADA ', $scope.ListadoGuias[obj].Remesa)
                                    } else {
                                        ShowError(response.statusText)
                                    }
                                });

                        } else { 
                            if ($scope.ListaDocumentos.length > 0 ) {
                                for (var i = 0; i < $scope.ListaDocumentos.length; i++) {
                                    if ($scope.ListadoGuias[obj].Remesa.Numero == $scope.ListaDocumentos[i].Numero && $scope.ListadoGuias[obj].Marcado) {

                                        filtros = {
                                            Numero: $scope.ListadoGuias[obj].Remesa.Numero,
                                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                            Observaciones: 'Cambio Estado Guia'
                                        }

                                        RemesaGuiasFactory.GuardarBitacoraGuias(filtros).
                                            then(function (response) {
                                                if (response.data.ProcesoExitoso == true) {
                                                    console.log('RENESA CAMBIADA ' ,$scope.ListadoGuias[obj].Remesa)
                                                } else {
                                                    ShowError(response.statusText)
                                                }
                                            });

                                        Entidad.RemesaPaqueteria.push({
                                            Numero: $scope.ListadoGuias[obj].Remesa.Numero,
                                            NumeroDocumento: $scope.ListadoGuias[obj].Remesa.NumeroDocumento
                                        });

                                        $scope.ListadoGuias[obj].Inserto = '';
                                        $scope.ListadoGuias[obj].Inserto = DocumentosFactory.InsertarDocumentoRemesa({
                                            CodigoEmpresa: $scope.ListaDocumentos[i].CodigoEmpresa,
                                            Codigo: $scope.ListaDocumentos[i].Codigo,
                                            Numero: $scope.ListaDocumentos[i].Numero,
                                            TipoDocumento: $scope.ListaDocumentos[i].TipoDocumento,
                                            Temporal: $scope.ListaDocumentos[i].Temporal,
                                            Sync: true
                                        }).Datos

                                    }  

                                }
                            } else {
                                ShowError('Debe Ingresar un documento para la remesa ' + $scope.ListadoGuias[obj].Remesa.NumeroDocumento);
                            }
                            

                        }
                            

                    }
                }


           


                    RemesaGuiasFactory.CambiarEstadoGuia(Entidad).
                        then(function (response) {
                            console.log('finproceso', response)
                            if (response.data.ProcesoExitoso === true) {
                                ShowSuccess('El estado de las Guias  se modificó correctamente');
                                $scope.ListaDocumentos = [];
                                Find();
                            }
                            Find();
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                    blockUI.stop();
                    $timeout(blockUI.stop(), 1000);
                }, 1000);
            
        };

        $scope.SeleccionarObjeto = function (item) { 
            $scope.DocumentoSeleccionado = item;
        };

        $scope.CargarArchivo = function (element) {
            $scope.IdPosiction = parseInt(element.id, 10);
            var continuar = true;
            if (element.files.length > 0) {
                $scope.PDF = element;
                $scope.TipoDocumento = element;
                if (angular.element(this.item)[0] == undefined) {
                    $scope.Documento = angular.element(this.ItemFoto)[0];
                } else {
                    angular.element(this.item)[0].Temporal = true
                    $scope.Documento = angular.element(this.item)[0];
                }
                if (element === undefined) {
                    ShowError("Archivo invalido, por favor verifique el formato del archivo  /nFormatos permitidos: 0");
                    continuar = false;
                }
                if (continuar == true) {
                    if (element.files[0].size >= 3000000) { //3 MB
                        ShowError("El archivo " + element.files[0].name + " supera los " + "3 MB" + ", intente con otro archivo", false);
                        continuar = false;
                    }
                    var Formatos = $scope.CadenaFormatos.split(',');
                    var cont = 0;
                    for (var i = 0; i < Formatos.length; i++) {
                        Formatos[i] = Formatos[i].replace(' ', '');
                        var Extencion = element.files[0].name.split('.');
                        //if (element.files[0].type == Formatos[i]) {
                        if ('.' + (Extencion[Extencion.length - 1].toUpperCase()) == Formatos[i].toUpperCase()) {
                            cont++;
                        }
                    }
                    if (cont == 0) {
                        ShowError("Archivo invalido, por favor verifique el formato del archivo.  Formatos permitidos:" + $scope.CadenaFormatos);
                        continuar = false;
                    }
                }
                if (continuar == true) {
                    if ($scope.Documento.Archivo != undefined && $scope.Documento.Archivo != '' && $scope.Documento.Archivo != null) {
                        $scope.Documento.ArchivoTemporal = element.files[0];
                        showModal('modalConfirmacionRemplazarDocumentoRemesa');
                    } else {
                        var reader = new FileReader();
                        $scope.Documento.ArchivoCargado = element.files[0];
                        reader.onload = $scope.AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }
            }
        };


        $scope.RemplazarDocumentoRemesa = function () {
            var reader = new FileReader();
            $scope.Documento.ArchivoCargado = $scope.Documento.ArchivoTemporal; 
            reader.readAsDataURL($scope.Documento.ArchivoCargado);
            reader.onload = $scope.AsignarArchivo;
            for (var i = 0; i < $scope.ListaDocumentos.length; i++) {
                if ($scope.ListaDocumentos[i].Numero == $scope.DocumentoSeleccionado.Remesa.Numero) { 
                    var i = $scope.ListaDocumentos.indexOf($scope.ListaDocumentos[i]);
                    $scope.ListaDocumentos.splice(i, 1); 
                } 
            }

            console.log('documentos', $scope.ListaDocumentos)

            closeModal('modalConfirmacionRemplazarDocumentoRemesa');
        };


        $scope.AsignarArchivo = function (e) {
            $scope.$apply(function () {
                //imagenes, tienen un proceso aparte ya que se deben redimencionar para que no ocupen mucho espacio en la BD
                if ($scope.Documento.ArchivoCargado.type.includes("image")) {
                    var img = new Image();
                    img.src = e.target.result;
                    $timeout(function () {
                        $('#Foto' + $scope.IdPosiction).show();
                        $('#FotoCargada' + $scope.IdPosiction).hide();
                        if (img.height > img.width) {
                            RedimencionarImagen('CanvasVertical', img, 699, 499);
                            $scope.Documento.Archivo = document.getElementById('CanvasVertical').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                        }
                        //la imagen es horizontal
                        else {
                            RedimencionarImagen('CanvasHorizontal', img, 499, 699);
                            $scope.Documento.Archivo = document.getElementById('CanvasHorizontal').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                        }
                        $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type;
                        var Extencion = $scope.Documento.ArchivoCargado.name.split('.');
                        var tmpNombre = $scope.Documento.ArchivoCargado.name.split('.').slice(0, -1).join('.');
                        $scope.Documento.NombreDocumento = tmpNombre;
                        $scope.Documento.ExtensionDocumento = Extencion[Extencion.length - 1];
                        $scope.InsertarDocumento();
                    }, 100);
                }
                //Resto de archivos
                else {
                    $scope.Documento.Archivo = e.target.result.replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '');
                    $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type;
                    var Extencion = $scope.Documento.ArchivoCargado.name.split('.');
                    var tmpNombre = $scope.Documento.ArchivoCargado.name.split('.').slice(0, -1).join('.');
                    $scope.Documento.NombreDocumento = tmpNombre;
                    $scope.Documento.ExtensionDocumento = Extencion[Extencion.length - 1];
                    $scope.InsertarDocumento();
                }
            });
        };

    

        $scope.InsertarDocumento = function () {
            $timeout(function () {

                // Guardar Archivo temporal
                var Documento = {};
                Documento = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Codigo: $scope.DocumentoSeleccionado.Id,
                    Numero: $scope.DocumentoSeleccionado.Remesa.Numero,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                    Descripcion: $scope.Documento.Descripcion,
                    Nombre: $scope.Documento.NombreDocumento,
                    Extension: $scope.Documento.ExtensionDocumento,
                    Tipo: $scope.Documento.Tipo,
                    Archivo: $scope.Documento.Archivo,
                    Temporal: true
                };
                DocumentosFactory.InsertarDocumentoRemesa(Documento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            // Se guardó correctamente el pdf
                            if (response.data.Datos) { 
                                $scope.ListaDocumentos.push({
                                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                    Codigo: response.data.Datos,
                                    Numero: $scope.DocumentoSeleccionado.Remesa.Numero,
                                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA,
                                    Temporal: false
                                }); 
                                console.log('listado', $scope.ListaDocuemntos)
                                ShowSuccess('Se cargó el documento adjunto "' + $scope.Documento.NombreDocumento + '"');
                                $scope.Documento = '';
                            }
                        } else {
                            ShowError(response.data.MensajeError);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }, 200);
        };
        //----------------------------Funciones Generales---------------------------------//
    }]);