﻿SofttoolsApp.controller("PlanificacionDespachosPaqueteriaCtrl", ['$scope', '$timeout', '$linq', '$routeParams', 'blockUI', 'blockUIConfig', 'PlanificacionDespachosFactory', 'TercerosFactory',
    'CiudadesFactory', 'EnturnamientoFactory', 'OficinasFactory', 'SemirremolquesFactory', 'RutasFactory', 'TarifarioComprasFactory', 'ImpuestosFactory', 'PlanillaPaqueteriaFactory',
    function ($scope, $timeout, $linq, $routeParams, blockUI, blockUIConfig, PlanificacionDespachosFactory, TercerosFactory,
        CiudadesFactory, EnturnamientoFactory, OficinasFactory, SemirremolquesFactory, RutasFactory, TarifarioComprasFactory, ImpuestosFactory, PlanillaPaqueteriaFactory) {

        $scope.MapaSitio = [{ Nombre: 'Paquetería' }, { Nombre: 'Procesos' }, { Nombre: 'Planificación Despachos' }];
        $scope.Titulo = 'PLANIFICACIÓN DESPACHOS';
        //-----------------------------------------------------------------DECLARACION VARIABLES---------------------------------------------------------------------------------//
        $scope.Buscando = false;
        $scope.Deshabilitar = false;
        $scope.DeshabilitarTipoPlanilla = false;
        $scope.MensajesError = [];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;

        $scope.MostrarResumenRemesas = false;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ===' + OPCION_MENU_PAQUETERIA.PLANIFICACION_DESPACHOS); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina');
            document.location.href = '#';
        }
        //--Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados--//
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);

        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;

        var filtros = {};
        $scope.DetalleRemesas = [];
        $scope.VehiculosDisponibles = [];
        $scope.PlanillaDespacho = [];
        $scope.IndicePlanilla = 0;

        $scope.FechaInicial = new Date();
        $scope.FechaFinal = new Date();

        $scope.CiudadOrigen = $scope.CargarCiudad($scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);

        $scope.ListadoOficinas = OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, Sync: true }).Datos;
        $scope.OficinaDespacha = $linq.Enumerable().From($scope.ListadoOficinas).First('$.Codigo == ' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
        //------------------------------AutoCompletes------------------------------//
        //---- Clientes
        $scope.ListadoCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) === 0 || value.length === 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_CLIENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCliente);
                }
            }
            return $scope.ListadoCliente;
        };
        //---- Remitente
        $scope.ListadoRemitente = [];
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) === 0 || value.length === 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        CadenaPerfiles: PERFIL_REMITENTE,
                        ValorAutocomplete: value,
                        Sync: true
                    });
                    $scope.ListadoREmitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoREmitente);
                }
            }
            return $scope.ListadoREmitente;
        };
        //---- Ciudades
        $scope.AutocompleteCiudades = function (value) {
            $scope.ListadoCiudades = [];
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades);
                }
            }
            return $scope.ListadoCiudades;
        };
        //----Semiremolques
        $scope.ListaSemirremolque = [];
        $scope.AutoCompleteSemiRemolque = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    blockUIConfig.autoBlock = false;
                    var Response = SemirremolquesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListaSemirremolque = ValidarListadoAutocomplete(Response.Datos, $scope.ListaSemirremolque);
                }
            }
            return $scope.ListaSemirremolque;
        };
        //----Semiremolques
        //----Rutas
        $scope.ListadoRutas = RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true }).Datos;
        //-Filtro Rutas
        $scope.AutoCompleteRutasFiltradas = function (value, ListaRutas) {
            if (value.length > 0) {
                var tmparr = [];
                for (var i = 0; i < ListaRutas.length; i++) {
                    var NombreRuta = ListaRutas[i].Nombre;
                    if (NombreRuta.likeFind(value + "%")) {
                        tmparr.push(ListaRutas[i]);
                    }
                }
            }
            return tmparr;
        };
        //-Filtro Rutas
        //----Rutas
        //----Tipo Documento Planilla
        $scope.ListadoTipoDocumentoPlanilla = [
            { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO, Nombre: "PLANILLA DESPACHOS" },
            { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_ENTREGA, Nombre: "PLANILLA ENTREGAS" }
        ];
        $scope.TipoPlanilla = $scope.ListadoTipoDocumentoPlanilla[0];
        //----Tipo Documento Planilla
        //------------------------------AutoCompletes------------------------------//
        //------------------------------Cargar Informacion Resumen Remesas -----------------//
        $scope.CargarResumenRemesas = function () {
            if ($scope.ValidarPermisos.AplicaConsultar === PERMISO_ACTIVO) {
                //Bloqueo Pantalla
                blockUIConfig.autoBlock = true;
                blockUIConfig.delay = 0;
                blockUI.start("Generando Planificación...");
                $timeout(function () {
                    blockUI.message("Generando Planificación...");
                    CargarResumenGeneralRemesas();
                }, 100);
                //Bloqueo Pantalla
            }
        };
        function CargarResumenGeneralRemesas() {
            $scope.RemesasAgencia = [];
            $scope.DetalleRemesas = [];
            $scope.VehiculosDisponibles = [];
            $scope.PlanillaDespacho = [];
            $scope.IndicePlanilla = 0;
            DeshabilitarTipoPlanilla = false;
            var continuar = true;
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentoPlanilla: $scope.TipoPlanilla.Codigo,
                FechaInicial: $scope.FechaInicial,
                FechaFinal: $scope.FechaFinal,
                Oficina: { Codigo: $scope.OficinaDespacha.Codigo },
                Cliente: { Codigo: $scope.Cliente === undefined ? '' : $scope.Cliente.Codigo },
                Remitente: { Codigo: $scope.Remitente === undefined ? '' : $scope.Remitente.Codigo },
                CiudadOrigen: { Codigo: $scope.CiudadOrigen === undefined ? '' : $scope.CiudadOrigen.Codigo },
                CiudadDestino: { Codigo: $scope.CiudadDestino === undefined ? '' : $scope.CiudadDestino.Codigo },
                Sync: true
            };
            $scope.ResultadoSinRegistrosResumenRemesas = '';
            $scope.ResultadoSinRegistrosVehiculos = '';
            var ResponConsulta = PlanificacionDespachosFactory.Consultar(filtros);
            if (ResponConsulta.ProcesoExitoso === true) {
                if (ResponConsulta.Datos.length > 0) {
                    var ResumenRemesas = OrdenarArregloObj(ResponConsulta.Datos, 2, 'Peso');
                    $scope.CoordenadasOficinas = [];
                    //ObtenerCoordenadasRutas();
                    for (var i = 0; i < ResumenRemesas.length; i++) {
                        var TMPremesas = [];
                        $scope.RemesasAgencia.push(ResumenRemesas[i]);
                        TMPremesas = ObtenerDetalleRemesas(ResumenRemesas[i]);
                        $scope.DetalleRemesas = $scope.DetalleRemesas.concat(TMPremesas);
                        //console.log("Coordenadas: ", $scope.CoordenadasOficinas);
                    }
                }
                else {
                    $scope.ResultadoSinRegistrosResumenRemesas = 'No hay datos para mostrar';
                    continuar = false;
                }
            }
            else {
                ShowError(ResponConsulta.statusText);
                continuar = false;
            }
            ConsultarFlotaDisponible();
            if (continuar) {
                GenerarPlanificacionDespachos();
            }
            else {
                blockUI.stop();
                $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
            }
        }
        var Consec = 0;
        function ObtenerCoordenadasRutas() {
            if ($scope.RemesasAgencia.length > 0) {
                var DireccionOrigen = ObtenerDireccionOficina($scope.RemesasAgencia[Consec].CiudadOrigen.Codigo);
                var DireccionDestino = ObtenerDireccionOficina($scope.RemesasAgencia[Consec].CiudadDestino.Codigo);
                Promise.all([
                    ObtenerCoordenadasDireccion(DireccionOrigen, $scope.RemesasAgencia[Consec].CiudadOrigen.Codigo),
                    ObtenerCoordenadasDireccion(DireccionDestino, $scope.RemesasAgencia[Consec].CiudadDestino.Codigo)
                ]).then(function (results) {
                    $scope.CoordenadasOficinas.push({
                        Origen: {
                            Codigo: results[0].CodigoOrigen,
                            Coordenadas: results[0].Coordenadas
                        },
                        Destino: {
                            Codigo: results[1].CodigoOrigen,
                            Coordenadas: results[0].Coordenadas
                        }
                    });
                });

                if (Consec < $scope.RemesasAgencia.length) {
                    Consec += 1;
                    $timeout(ObtenerCoordenadasRutas, 100);
                }
                else {
                    console.log("coordenadas: ", $scope.CoordenadasOficinas);
                }

            }
            else {
                $timeout(ObtenerCoordenadasRutas, 100);
            }
        }

        function ActualizarResumenGeneralRemesas() {
            $scope.RemesasAgencia = [];
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentoPlanilla: $scope.TipoPlanilla.Codigo,
                FechaInicial: $scope.FechaInicial,
                FechaFinal: $scope.FechaFinal,
                Oficina: { Codigo: $scope.OficinaDespacha.Codigo },
                Cliente: { Codigo: $scope.Cliente === undefined ? '' : $scope.Cliente.Codigo },
                Remitente: { Codigo: $scope.Remitente === undefined ? '' : $scope.Remitente.Codigo },
                CiudadOrigen: { Codigo: $scope.CiudadOrigen === undefined ? '' : $scope.CiudadOrigen.Codigo },
                CiudadDestino: { Codigo: $scope.CiudadDestino === undefined ? '' : $scope.CiudadDestino.Codigo },
                Sync: true
            };
            $scope.ResultadoSinRegistrosResumenRemesas = '';
            $scope.ResultadoSinRegistrosVehiculos = '';
            var ResponConsulta = PlanificacionDespachosFactory.Consultar(filtros);
            if (ResponConsulta.ProcesoExitoso === true) {
                if (ResponConsulta.Datos.length > 0) {
                    var ResumenRemesas = OrdenarArregloObj(ResponConsulta.Datos, 2, 'Peso');
                    for (var i = 0; i < ResumenRemesas.length; i++) {
                        $scope.RemesasAgencia.push(ResumenRemesas[i]);
                    }
                }
                else {
                    $scope.ResultadoSinRegistrosResumenRemesas = 'No hay datos para mostrar';
                    continuar = false;
                }
            }
            else {
                ShowError(ResponConsulta.statusText);
                continuar = false;
            }
            ConsultarFlotaDisponible();
            blockUI.stop();
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
        }

        function ConsultarFlotaDisponible() {
            filtrosEnturnamiento = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                OficinaEnturna: $scope.OficinaDespacha,
                EstadoTurno: CODIGO_CATALOGOS_ESTADO_ENTURNAMIENTO_DESPACHO.ETURNADO,
                Sync: true
            };
            var response = EnturnamientoFactory.Consultar(filtrosEnturnamiento);
            if (response.ProcesoExitoso == true) {
                if (response.Datos.length > 0) {
                    $scope.VehiculosDisponibles = OrdenarArregloObj(response.Datos, 2, 'Capacidad');
                }
                else {
                    $scope.ResultadoSinRegistrosVehiculos = 'No hay vehiculos para mostrar';
                }
            }
            else {
                ShowError(ResponConsulta.statusText);
            }
        }
        function ObtenerDetalleRemesas(ResumenRemesa) {
            var TMPRemesas = [];
            var FiltroRemesasGen = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumentoPlanilla: $scope.TipoPlanilla.Codigo,
                FechaInicial: $scope.FechaInicial,
                FechaFinal: $scope.FechaFinal,
                Oficina: { Codigo: $scope.OficinaDespacha.Codigo },
                Cliente: { Codigo: filtros.Cliente === undefined ? '' : filtros.Cliente.Codigo },
                Remitente: { Codigo: filtros.Remitente === undefined ? '' : filtros.Remitente.Codigo },
                CiudadOrigen: { Codigo: ResumenRemesa.CiudadOrigen.Codigo },
                CiudadDestino: { Codigo: ResumenRemesa.CiudadDestino.Codigo },
                Reexpedicion: ResumenRemesa.Reexpedicion,
                Sync: true
            };
            var ResponConsulta = PlanificacionDespachosFactory.ConsultarDetalleRemesasResumen(FiltroRemesasGen);
            if (ResponConsulta.ProcesoExitoso === true) {
                var existe = false;
                if ($scope.DetalleRemesas.length > 0) {
                    for (var i = 0; i < ResponConsulta.Datos.length; i++) {
                        for (var j = 0; j < $scope.DetalleRemesas.length; j++) {
                            if ($scope.DetalleRemesas[j].Remesa.Numero === ResponConsulta.Datos[i].Remesa.Numero) {
                                existe = true;
                            }
                        }
                        if (!existe) {
                            TMPRemesas.push(ResponConsulta.Datos[i]);
                        }
                        existe = false;
                    }
                }
                else {
                    for (var i = 0; i < ResponConsulta.Datos.length; i++) {
                        TMPRemesas.push(ResponConsulta.Datos[i]);
                    }
                }
            }
            return TMPRemesas;
        }
        //------------------------------Cargar Informacion Resumen Remesas -----------------//
        //-----------------------------Planificacion Despachos--------------------------//
        function GenerarPlanificacionDespachos() {
            //console.log("Coordenadas Oficinas: ", $scope.CoordenadasOficinas);
            $scope.VehiculosDisponibles.forEach(vehiculo => { vehiculo.Asignado = 0; });
            $scope.DetalleRemesas.forEach(remesa => { remesa.Asignada = 0; });
            var ProcesoPlanilla = true;
            while (ProcesoPlanilla == true) {
                var Vehi = RetornarIdVehiculoViable();
                var IdVehi = Vehi.Id;
                var Ruta = Vehi.Ruta;
                if (IdVehi >= 0) {
                    var PesoDisponible = $scope.VehiculosDisponibles[IdVehi].Capacidad;
                    var TotalCargado = 0;
                    var ObjPlanilla = {
                        Vehiculo: '',
                        Remesas: [],
                        Semirremolque: ''
                    };
                    for (var j = 0; j < $scope.DetalleRemesas.length; j++) {
                        if ($scope.DetalleRemesas[j].Asignada == 0
                            && $scope.DetalleRemesas[j].Remesa.CiudadRemitente.Codigo == Ruta.Origen
                            && $scope.DetalleRemesas[j].Remesa.CiudadDestinatario.Codigo == Ruta.Destino) {
                            if ((PesoDisponible - $scope.DetalleRemesas[j].Remesa.PesoCliente) >= 0) {
                                $scope.DetalleRemesas[j].Asignada = 1;
                                ObjPlanilla.Vehiculo = {
                                    IdTurno: $scope.VehiculosDisponibles[IdVehi].ID,
                                    Codigo: $scope.VehiculosDisponibles[IdVehi].Vehiculo.Codigo,
                                    Placa: $scope.VehiculosDisponibles[IdVehi].Vehiculo.Placa,
                                    Conductor: $scope.VehiculosDisponibles[IdVehi].Conductor,
                                    Capacidad: $scope.VehiculosDisponibles[IdVehi].Capacidad,
                                    CapacidadM3: $scope.VehiculosDisponibles[IdVehi].CapacidadM3,
                                    Tenedor: $scope.VehiculosDisponibles[IdVehi].Tenedor,
                                    TipoVehiculo: $scope.VehiculosDisponibles[IdVehi].TipoVehiculo
                                };
                                ObjPlanilla.RemesaOrigenDestino = $scope.DetalleRemesas[j].Remesa.CiudadRemitente.Nombre + " - " + $scope.DetalleRemesas[j].Remesa.CiudadDestinatario.Nombre;
                                ObjPlanilla.CiudadOrigen = $scope.DetalleRemesas[j].Remesa.CiudadRemitente.Codigo;
                                ObjPlanilla.CiudadDestino = $scope.DetalleRemesas[j].Remesa.CiudadDestinatario.Codigo;
                                ObjPlanilla.Remesas.push($scope.DetalleRemesas[j]);
                                ObjPlanilla.Semirremolque = $scope.VehiculosDisponibles[IdVehi].Vehiculo.Semirremolque;
                                PesoDisponible -= $scope.DetalleRemesas[j].Remesa.PesoCliente;
                                TotalCargado += $scope.DetalleRemesas[j].Remesa.PesoCliente;
                            }
                        }
                    }
                    if (ObjPlanilla.Vehiculo != '') {
                        ObjPlanilla.PesoTotal = TotalCargado;
                        //--Cargar Informacion Vehiculo, Tarifarios y Rutas
                        ObjPlanilla.Vehiculo.Tenedor = ObtenerTenedor(ObjPlanilla.Vehiculo.Tenedor.Codigo);
                        var ObjTarifario = ObtenerInformacionTarifario(ObjPlanilla.Vehiculo.Tenedor.Proveedor.Tarifario.Codigo);
                        if (ObjTarifario != null) {
                            ObjPlanilla.Tarifario = ObjTarifario.Tarifario;
                            ObjPlanilla.ListadoTarifas = ObjTarifario.Tarifas;
                            ObjPlanilla.ListadoRutas = ObjTarifario.Rutas;
                        }
                        if (ObjPlanilla.Semirremolque != undefined) {
                            if (ObjPlanilla.Semirremolque.Codigo > 0) {
                                ObjPlanilla.Semirremolque = $scope.CargarSemirremolqueCodigo(ObjPlanilla.Semirremolque.Codigo);
                            }
                        }
                        //--Cargar Informacion Vehiculo, Tarifarios y Rutas
                        $scope.PlanillaDespacho.push(ObjPlanilla);
                        $scope.VehiculosDisponibles[IdVehi].Asignado = 1;
                    }
                }
                else {
                    ProcesoPlanilla = false;
                }
            }
            //--Totalizar para Visualizaciones
            for (i = 0; i < $scope.PlanillaDespacho.length; i++) {
                $scope.PlanillaDespacho[i].Indice = i;
                $scope.PlanillaDespacho[i].Guias = $scope.PlanillaDespacho[i].Remesas.length;
                var Remesas = $scope.PlanillaDespacho[i].Remesas;
                var SumUnidades = 0;
                var TotalFleteCliente = 0;
                var FleteCliente = 0;
                var VolumenTotal = 0;
                for (j = 0; j < Remesas.length; j++) {
                    SumUnidades += Remesas[j].Remesa.CantidadCliente;
                    TotalFleteCliente += Remesas[j].Remesa.TotalFleteCliente;
                    FleteCliente += Remesas[j].Remesa.ValorFleteCliente;
                    VolumenTotal += Remesas[j].VolumenM3;
                }
                $scope.PlanillaDespacho[i].FechaSalida = new Date();
                $scope.PlanillaDespacho[i].CantidadTotal = SumUnidades;
                $scope.PlanillaDespacho[i].FleteCliente = FleteCliente;
                $scope.PlanillaDespacho[i].VolumenTotal = VolumenTotal;
            }
            //--Totalizar para Visualizaciones
            //for (var i = 0; i < $scope.VehiculosDisponibles.length; i++) {
            //    var PesoDisponible = $scope.VehiculosDisponibles[i].Capacidad;
            //    var TotalCargado = 0;
            //    var Ruta = {
            //        Origen: '',
            //        Destino: ''
            //    };
            //    var ObjPlanilla = {
            //        Vehiculo: '',
            //        Remesas: [],
            //        Semirremolque: ''
            //    };
            //    //--Obtiene la Primera Ruta Disponible para llenar
            //    for (var k = 0; k < $scope.DetalleRemesas.length; k++) {
            //        if ($scope.DetalleRemesas[k].Asignada == 0) {
            //            Ruta = { Origen: $scope.DetalleRemesas[k].Remesa.CiudadRemitente.Codigo, Destino: $scope.DetalleRemesas[k].Remesa.CiudadDestinatario.Codigo };
            //            break;
            //        }
            //    }
            //    //--Obtiene la Primera Ruta Disponible para llenar

            //    for (var j = 0; j < $scope.DetalleRemesas.length; j++) {
            //        if ($scope.DetalleRemesas[j].Asignada == 0
            //            && $scope.DetalleRemesas[j].Remesa.CiudadRemitente.Codigo == Ruta.Origen
            //            && $scope.DetalleRemesas[j].Remesa.CiudadDestinatario.Codigo == Ruta.Destino) {
            //            if ((PesoDisponible - $scope.DetalleRemesas[j].Remesa.PesoCliente) >= 0) {
            //                $scope.DetalleRemesas[j].Asignada = 1;
            //                ObjPlanilla.Vehiculo = {
            //                    IdTurno: $scope.VehiculosDisponibles[i].ID,
            //                    Codigo: $scope.VehiculosDisponibles[i].Vehiculo.Codigo,
            //                    Placa: $scope.VehiculosDisponibles[i].Vehiculo.Placa,
            //                    Conductor: $scope.VehiculosDisponibles[i].Conductor,
            //                    Capacidad: $scope.VehiculosDisponibles[i].Capacidad,
            //                    CapacidadM3: $scope.VehiculosDisponibles[i].CapacidadM3,
            //                    Tenedor: $scope.VehiculosDisponibles[i].Tenedor,
            //                    TipoVehiculo: $scope.VehiculosDisponibles[i].TipoVehiculo
            //                };
            //                ObjPlanilla.RemesaOrigenDestino = $scope.DetalleRemesas[j].Remesa.CiudadRemitente.Nombre + " - " + $scope.DetalleRemesas[j].Remesa.CiudadDestinatario.Nombre;
            //                ObjPlanilla.CiudadOrigen = $scope.DetalleRemesas[j].Remesa.CiudadRemitente.Codigo;
            //                ObjPlanilla.CiudadDestino = $scope.DetalleRemesas[j].Remesa.CiudadDestinatario.Codigo;
            //                ObjPlanilla.Remesas.push($scope.DetalleRemesas[j]);
            //                ObjPlanilla.Semirremolque = $scope.VehiculosDisponibles[i].Vehiculo.Semirremolque;
            //                PesoDisponible -= $scope.DetalleRemesas[j].Remesa.PesoCliente;
            //                TotalCargado += $scope.DetalleRemesas[j].Remesa.PesoCliente;
            //            }
            //        }
            //    }
            //    if (ObjPlanilla.Vehiculo != '') {
            //        ObjPlanilla.PesoTotal = TotalCargado;
            //        //--Cargar Informacion Vehiculo, Tarifarios y Rutas
            //        ObjPlanilla.Vehiculo.Tenedor = ObtenerTenedor(ObjPlanilla.Vehiculo.Tenedor.Codigo);
            //        var ObjTarifario = ObtenerInformacionTarifario(ObjPlanilla.Vehiculo.Tenedor.Proveedor.Tarifario.Codigo);
            //        if (ObjTarifario != null) {
            //            ObjPlanilla.Tarifario = ObjTarifario.Tarifario;
            //            ObjPlanilla.ListadoTarifas = ObjTarifario.Tarifas;
            //            ObjPlanilla.ListadoRutas = ObjTarifario.Rutas;
            //        }
            //        if (ObjPlanilla.Semirremolque != undefined) {
            //            if (ObjPlanilla.Semirremolque.Codigo > 0) {
            //                ObjPlanilla.Semirremolque = $scope.CargarSemirremolqueCodigo(ObjPlanilla.Semirremolque.Codigo);
            //            }
            //        }
            //        //--Cargar Informacion Vehiculo, Tarifarios y Rutas
            //        $scope.PlanillaDespacho.push(ObjPlanilla);
            //        $scope.VehiculosDisponibles[i].Disponible = 0;
            //    }
            //}
            ////--Totalizar para Visualizaciones
            //for (i = 0; i < $scope.PlanillaDespacho.length; i++) {
            //    $scope.PlanillaDespacho[i].Indice = i;
            //    $scope.PlanillaDespacho[i].Guias = $scope.PlanillaDespacho[i].Remesas.length;
            //    var Remesas = $scope.PlanillaDespacho[i].Remesas;
            //    var SumUnidades = 0;
            //    var TotalFleteCliente = 0;
            //    var FleteCliente = 0;
            //    var VolumenTotal = 0;
            //    for (j = 0; j < Remesas.length; j++) {
            //        SumUnidades += Remesas[j].Remesa.CantidadCliente;
            //        TotalFleteCliente += Remesas[j].Remesa.TotalFleteCliente;
            //        FleteCliente += Remesas[j].Remesa.ValorFleteCliente;
            //        VolumenTotal += Remesas[j].VolumenM3;
            //    }
            //    $scope.PlanillaDespacho[i].CantidadTotal = SumUnidades;
            //    $scope.PlanillaDespacho[i].FleteCliente = FleteCliente;
            //    $scope.PlanillaDespacho[i].VolumenTotal = VolumenTotal;
            //}
            //--Totalizar para Visualizaciones
            blockUI.stop();
            $timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
        }
        //--Calculos Vehiculo Viable por capacidad y total kg de remesas
        function RetornarIdVehiculoViable() {
            var IdRetorno = -1;
            var PesoRemesas = 0;
            var ArrDiferencial = [];
            var i, j, k = 0;
            var Ruta = {
                Origen: '',
                Destino: ''
            };

            //--Obtiene la Primera Ruta Disponible para llenar
            for (k = 0; k < $scope.DetalleRemesas.length; k++) {
                if ($scope.DetalleRemesas[k].Asignada == 0) {
                    Ruta = { Origen: $scope.DetalleRemesas[k].Remesa.CiudadRemitente.Codigo, Destino: $scope.DetalleRemesas[k].Remesa.CiudadDestinatario.Codigo };
                    break;
                }
            }
            //--Obtiene la Primera Ruta Disponible para llenar

            for (i = 0; i < $scope.DetalleRemesas.length; i++) {
                if ($scope.DetalleRemesas[i].Asignada == 0 &&
                    $scope.DetalleRemesas[i].Remesa.CiudadRemitente.Codigo == Ruta.Origen &&
                    $scope.DetalleRemesas[i].Remesa.CiudadDestinatario.Codigo == Ruta.Destino) {
                    PesoRemesas += $scope.DetalleRemesas[i].Remesa.PesoCliente;
                }
            }

            if (PesoRemesas > 0) {
                var ExisteDisponible = false;
                //--Calcula dependendiendo Si existe un vehiculo para llevar el total
                for (i = 0; i < $scope.VehiculosDisponibles.length; i++) {
                    if ($scope.VehiculosDisponibles[i].Asignado == 0) {
                        if ($scope.VehiculosDisponibles[i].Capacidad - PesoRemesas > 0) {
                            ExisteDisponible = true;
                            ArrDiferencial.push({
                                Diferencial: $scope.VehiculosDisponibles[i].Capacidad - PesoRemesas,
                                Id: i
                            });
                        }
                    }
                }
                //--Calcula dependendiendo Si existe un vehiculo para llevar el total
                //--Calcula del que mas pueda llevar
                if (ExisteDisponible == false) {
                    for (i = 0; i < $scope.VehiculosDisponibles.length; i++) {
                        if ($scope.VehiculosDisponibles[i].Asignado == 0) {
                            ExisteDisponible = true;
                            ArrDiferencial.push({
                                Diferencial: Math.abs($scope.VehiculosDisponibles[i].Capacidad - PesoRemesas),
                                Id: i
                            });
                        }
                    }
                }
                //--Calcula del que mas pueda llevar
                if (ExisteDisponible) {
                    ArrDiferencial = OrdenarArregloObj(ArrDiferencial, 1, 'Diferencial');
                    IdRetorno = { Id: ArrDiferencial[0].Id, Ruta: Ruta };
                }
            }
            return IdRetorno;
        }
        //--Calculos Vehiculo Viable por capacidad y total kg de remesas
        //--Obtiene Tarifas A partir de la Ruta
        $scope.ObtenerTarifasCompra = function (planilla) {
            if (planilla.Ruta !== undefined && planilla.Ruta !== null && planilla.Ruta !== '') {
                if (planilla.Vehiculo.Tenedor.Proveedor.Tarifario.Codigo > 0) {
                    var ListaTarifas = [];
                    var ListaTipoTarifas = [];
                    for (var i = 0; i < planilla.ListadoTarifas.length; i++) {
                        var item = planilla.ListadoTarifas[i];
                        if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO ||
                            item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                            if (planilla.Ruta.Codigo == item.Ruta.Codigo) {
                                var TipoTarifaAux = {
                                    Codigo: item.TipoTarifaTransportes.Codigo,
                                    Nombre: item.TipoTarifaTransportes.Nombre,
                                    CodigoTarifa: item.TipoTarifaTransportes.TarifaTransporte.Codigo,
                                    ValorFlete: item.ValorFlete
                                };
                                var Existe = false;
                                for (var j = 0; j < ListaTarifas.length; j++) {
                                    if (ListaTarifas[j].Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                        Existe = true;
                                        break;
                                    }
                                }
                                if (Existe == false) {
                                    ListaTarifas.push(item.TipoTarifaTransportes.TarifaTransporte);
                                }
                                ListaTipoTarifas.push(TipoTarifaAux);
                            }
                        }
                    }
                    planilla.Tarifa = ListaTarifas[0];
                    planilla.ListadoTarifasFiltradas = ListaTarifas;
                    planilla.ListadoTipoTarifas = ListaTipoTarifas;
                    GestionarImpuestos(planilla);
                    $scope.CambioTarifa(planilla);
                }
            }
        };
        //--Obtiene Tarifas A partir de la Ruta
        //--Obtiene Tenedor
        function ObtenerTenedor(Codigo) {
            var Tenedor = null;
            if (Codigo > 0) {
                var Response = TercerosFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, Sync: true });
                if (Response.ProcesoExitoso === true) {
                    Tenedor = Response.Datos;
                }
            }
            return Tenedor;
        }
        //--Obtiene Tenedor
        //--Obtiene Tarifario, Tarifas y Rutas Filtradas
        function ObtenerInformacionTarifario(Codigo) {
            var objTarifario = null;
            var Tarifas = [];
            var RutasFiltradas = [];
            if (Codigo > 0) {
                var Response = TarifarioComprasFactory.Obtener({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Codigo: Codigo, Sync: true });
                if (Response.ProcesoExitoso === true) {
                    if (Response.Datos.Estado = ESTADO_ACTIVO) {
                        var Tarifario = Response.Datos;
                        for (var i = 0; i < Tarifario.Tarifas.length; i++) {
                            if (Tarifario.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO ||
                                Tarifario.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                                Tarifas.push(Tarifario.Tarifas[i]);
                            }
                        }
                        for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                            var aplica = false;
                            for (var j = 0; j < Tarifas.length; j++) {
                                if ($scope.ListadoRutas[i].Codigo == Tarifas[j].Ruta.Codigo) {
                                    aplica = true;
                                    break;
                                }
                            }
                            if (aplica > 0) {
                                RutasFiltradas.push($scope.ListadoRutas[i]);
                            }
                        }
                    }
                    objTarifario = {
                        Tarifario: Tarifario,
                        Tarifas: Tarifas,
                        Rutas: RutasFiltradas
                    };
                }
            }
            return objTarifario;
        }
        //--Obtiene Tarifario, Tarifas y Rutas Filtradas
        //--Gestion de cambios de tarifa y tipo tarifa
        $scope.CambioTarifa = function (Planilla) {
            var ListaTipoTarifaFiltradas = [];
            for (var i = 0; i < Planilla.ListadoTipoTarifas.length; i++) {
                if (Planilla.ListadoTipoTarifas[i].CodigoTarifa == Planilla.Tarifa.Codigo) {
                    ListaTipoTarifaFiltradas.push(Planilla.ListadoTipoTarifas[i]);
                }
            }
            Planilla.ListadoTipoTarifasFiltradas = ListaTipoTarifaFiltradas;
            Planilla.TipoTarifa = Planilla.ListadoTipoTarifasFiltradas[0];
            Planilla.ValorFleteTransportador = MascaraValores(Planilla.ListadoTipoTarifasFiltradas[0].ValorFlete);
            //--Impuestos
            CalcularImpuestos(Planilla);
            //--Impuestos
        };
        $scope.CambioTipoTarifa = function (Planilla) {
            Planilla.ValorFleteTransportador = MascaraValores(Planilla.TipoTarifa.ValorFlete);
            //--Impuestos
            CalcularImpuestos(Planilla);
            //--Impuestos
        };
        //--Gestion de cambios de tarifa y tipo tarifa
        //--Gestion de Impuestos
        function GestionarImpuestos(Planilla) {
            var Impuestos = [];
            if (Planilla.Ruta !== undefined && Planilla.Ruta !== null && Planilla.Ruta !== '') {
                var response = ImpuestosFactory.Consultar({
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    Estado: ESTADO_DEFINITIVO,
                    CodigoTipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO,
                    CodigoCiudad: Planilla.Ruta.CiudadOrigen.Codigo,
                    AplicaTipoDocumento: 1,
                    Sync: true
                });
                if (response.ProcesoExitoso == true) {
                    if (response.Datos.length > 0) {
                        Impuestos = response.Datos;
                        Impuestos.forEach(impuesto => { impuesto.ValorImpuesto = 0; });
                        Planilla.ListadoImpuestos = Impuestos;
                    }
                }
            }
        }
        function CalcularImpuestos(Planilla) {
            //--Impuestos
            var tmpImpuestosFiltrados = [];
            for (var i = 0; i < Planilla.ListadoImpuestos.length; i++) {
                var impuesto = {
                    Nombre: Planilla.ListadoImpuestos[i].Nombre,
                    Codigo: Planilla.ListadoImpuestos[i].Codigo,
                    Valor_tarifa: Planilla.ListadoImpuestos[i].Valor_tarifa,
                    valor_base: Planilla.ListadoImpuestos[i].valor_base,
                    Operacion: Planilla.ListadoImpuestos[i].Operacion,
                    ValorImpuesto: 0
                };
                if (Planilla.ListadoImpuestos[i].valor_base < parseInt(MascaraNumero(Planilla.ValorFleteTransportador))) {
                    impuesto.valor_base = parseInt(MascaraNumero(Planilla.ValorFleteTransportador));
                }
                impuesto.ValorImpuesto = Math.round(impuesto.Valor_tarifa * impuesto.valor_base);
                tmpImpuestosFiltrados.push(impuesto);
            }
            Planilla.ImpuestosFiltrados = tmpImpuestosFiltrados;
            Planilla.ValorImpuesto = 0;
            Planilla.TotalFleteTransportador = 0;
            for (var i = 0; i < Planilla.ImpuestosFiltrados.length; i++) {
                switch (Planilla.ImpuestosFiltrados[i].Operacion) {
                    case 1:
                        Planilla.ValorImpuesto += Planilla.ImpuestosFiltrados[i].ValorImpuesto;
                        break;
                    case 2:
                        Planilla.ValorImpuesto -= Planilla.ImpuestosFiltrados[i].ValorImpuesto;
                        break;
                    default:
                        Planilla.ValorImpuesto -= Planilla.ImpuestosFiltrados[i].ValorImpuesto;
                        break;
                }
            }
            Planilla.TotalFleteTransportador = MascaraValores(RevertirMV(Planilla.ValorFleteTransportador) + Planilla.ValorImpuesto - RevertirMV(Planilla.Anticipo));
            Planilla.ValorImpuesto = MascaraValores(Planilla.ValorImpuesto);
            //--Impuestos
        }
        //--Gestion de Impuestos
        //--Proceso Guardar Planilla Despachos
        $scope.ConfirmacionGenerar = function (IndicePlanilla) {
            $scope.IndicePlanilla = IndicePlanilla;
            if (DatosRequeridosGenerarPlanilla()) {
                showModal('modalConfirmacionGenerar');
            }
            else {
                showModal('modalErrorPlanillaDespacho');
            }
        };
        $scope.IniciarProcesoGenerarPlanilla = function () {
            closeModal('modalConfirmacionGenerar');
            //Bloqueo Pantalla
            blockUIConfig.autoBlock = true;
            blockUIConfig.delay = 0;
            var strDocu = $scope.TipoPlanilla.Codigo == CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO ? 'Despacho' : 'Entregas';
            blockUI.start("Generando Planilla " + strDocu + "...");
            $timeout(function () {
                blockUI.message("Generando Planilla " + strDocu + "...");
                GenerarPlanillasDespacho();
            }, 100);
            //Bloqueo Pantalla
        };
        function GenerarPlanillasDespacho() {
            var i = 0;
            var Planilla = $scope.PlanillaDespacho[$scope.IndicePlanilla];
            //--Genera Anticipo
            if (Planilla.Anticipo > 0) {
                var CuentaPorPagar = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                    CodigoAlterno: '',
                    Fecha: new Date(Planilla.FechaSalida),
                    Tercero: { Codigo: Planilla.Vehiculo.Conductor.Codigo },
                    DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA },
                    CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO_PAQUETERIA,
                    Numeracion: '',
                    CuentaPuc: { Codigo: 0 },
                    ValorTotal: Planilla.Anticipo,
                    Abono: 0,
                    Saldo: Planilla.Anticipo,
                    FechaCancelacionPago: new Date(Planilla.FechaSalida),
                    Aprobado: 1,
                    UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    FechaAprobo: new Date(Planilla.FechaSalida),
                    Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                    Vehiculo: { Codigo: Planilla.Vehiculo.Codigo },
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                };
            }
            //--Genera Anticipo
            //--Detalle De Remesas
            var ListadoRemesaDetalle = [];
            for (i = 0; i < Planilla.Remesas.length; i++) {
                var remesa = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                    Remesa: {
                        Numero: Planilla.Remesas[i].Remesa.Numero,
                        NumeroDocumento: Planilla.Remesas[i].Remesa.NumeroDocumento
                    }
                };
                ListadoRemesaDetalle.push(remesa);
            }
            //--Detalle De Remesas
            //--Detalle Impuestos
            var ListadoDetalleImpuestos = [];
            for (i = 0; i < Planilla.ImpuestosFiltrados.length; i++) {
                var impuesto = {
                    CodigoImpuesto: Planilla.ImpuestosFiltrados[i].Codigo,
                    ValorTarifa: Planilla.ImpuestosFiltrados[i].Valor_tarifa,
                    ValorBase: Planilla.ImpuestosFiltrados[i].valor_base,
                    ValorImpuesto: Planilla.ImpuestosFiltrados[i].ValorImpuesto
                };
                ListadoDetalleImpuestos.push(impuesto);
            }
            //--Detalle Impuestos
            //--Genera Fecha Salida con hora
            var FechaHoraSalida = new Date(Planilla.FechaSalida);
            var Horasminutos = [];
            Horasminutos = Planilla.HoraSalida.split(':');
            if (Horasminutos.length > 0) {
                FechaHoraSalida.setHours(Horasminutos[0]);
                FechaHoraSalida.setMinutes(Horasminutos[1]);
            }
            //--Genera Fecha Salida con hora
            //--Genera Obj Planilla
            var PlanillaDespachos = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                TipoDocumento: CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA,
                Planilla: {
                    Fecha: new Date(Planilla.FechaSalida),
                    FechaHoraSalida: FechaHoraSalida,
                    Ruta: { Codigo: Planilla.Ruta.Codigo },
                    Vehiculo: {Codigo: Planilla.Vehiculo.Codigo},
                    Tenedor: { Codigo: Planilla.Vehiculo.Tenedor.Codigo },
                    Conductor: { Codigo: Planilla.Vehiculo.Conductor.Codigo },
                    Semirremolque: { Codigo: Planilla.Semirremolque != '' && Planilla.Semirremolque != null && Planilla.Semirremolque != undefined ? Planilla.Semirremolque.Codigo : 0 },
                    Cantidad: Planilla.CantidadTotal,
                    Peso: Planilla.PesoTotal,
                    ValorFleteTransportador: Planilla.ValorFleteTransportador,
                    ValorAnticipo: Planilla.Anticipo,
                    ValorImpuestos: Planilla.ValorImpuesto,
                    ValorPagarTransportador: Planilla.TotalFleteTransportador,
                    ValorFleteCliente: Planilla.TotalFleteCliente,
                    Estado: { Codigo: ESTADO_BORRADOR },
                    TarifaTransportes: { Codigo: Planilla.Tarifa.Codigo },
                    TipoTarifaTransportes: { Codigo: Planilla.TipoTarifa.Codigo },
                    ValorBruto: Planilla.FleteCliente,
                    Detalles: ListadoRemesaDetalle,
                    DetalleImpuesto: ListadoDetalleImpuestos
                },
                UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                CuentaPorPagar: CuentaPorPagar,
                Sync: true
            };
            //--Genera Obj Planilla
            //console.log(PlanillaDespachos);
            //--Guarda Planilla y Manifiesto
            try {
                var Response = PlanillaPaqueteriaFactory.Guardar(PlanillaDespachos);
                if (Response.ProcesoExitoso == true) {
                    if (Response.Datos > CERO) {
                        ShowSuccess('Se guardó la planilla con el número ' + Response.Datos);

                        var TMPPlanilla = [];
                        for (i = 0; i < $scope.PlanillaDespacho.length; i++) {
                            if (i !== $scope.IndicePlanilla) {
                                TMPPlanilla.push($scope.PlanillaDespacho[i]);
                            }
                        }
                        $scope.PlanillaDespacho = TMPPlanilla;
                        for (i = 0; i < $scope.PlanillaDespacho.length; i++) {
                            $scope.PlanillaDespacho[i].Indice = i;
                        }

                        filtrosEnturnamiento = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            Numero: Planilla.Vehiculo.IdTurno,
                            ID: Planilla.Vehiculo.IdTurno,
                            CerrarTurno: ESTADO_ACTIVO,
                            Sync: true
                        };
                        var response = EnturnamientoFactory.Guardar(filtrosEnturnamiento);
                        $timeout(function () {
                            blockUI.message("Actualizando Resumen...");
                            ActualizarResumenGeneralRemesas();
                        }, 100);
                    }
                    else {
                        ShowError(Response.statusText);
                    }
                }
                else {
                    ShowError(Response.statusText);
                }
            }
            catch (e) {
                ShowError(e.message);
            }
            //blockUI.stop();
            //$timeout(function () { blockUI.stop(); blockUIConfig.autoBlock = false; }, 100);
            //--Guarda Planilla y Manifiesto
        }
        function DatosRequeridosGenerarPlanilla() {
            $scope.MensajesErrorGenerarPlanilla = [];
            var continuar = true;
            var Planilla = $scope.PlanillaDespacho[$scope.IndicePlanilla];
            var FechaSalida = new Date(Planilla.FechaSalida).withoutTime();
            var FechaActual = new Date().withoutTime();

            if (Planilla.Ruta == undefined || Planilla.Ruta == '' || Planilla.Ruta == null) {
                $scope.MensajesErrorGenerarPlanilla.push('Debe ingresar ruta');
                continuar = false;
            }
            if (Planilla.Tarifa == undefined || Planilla.Tarifa == '' || Planilla.Tarifa == null) {
                $scope.MensajesErrorGenerarPlanilla.push('Debe ingresar tarifa');
                continuar = false;
            }
            if (Planilla.TipoTarifa == undefined || Planilla.TipoTarifa == '' || Planilla.TipoTarifa == null) {
                $scope.MensajesErrorGenerarPlanilla.push('Debe ingresar tipo tarifa');
                continuar = false;
            }
            if (Planilla.FechaSalida == undefined || Planilla.FechaSalida == '' || Planilla.FechaSalida == null) {
                $scope.MensajesErrorGenerarPlanilla.push('Debe ingresar fecha salida');
                continuar = false;
            }
            else if (FechaSalida < FechaActual) {
                $scope.MensajesErrorGenerarPlanilla.push('La fecha de salida ingresada no se puede ser menor a la fecha actual');
                continuar = false;
            }
            if (Planilla.HoraSalida == undefined || Planilla.HoraSalida == '' || Planilla.HoraSalida == null) {
                $scope.MensajesErrorGenerarPlanilla.push('Debe ingresar hora salida');
                continuar = false;
            }
            else {
                var horas = Planilla.HoraSalida.split(':');
                if (horas.length < 2) {
                    $scope.MensajesErrorGenerarPlanilla.push('Debe ingresar una hora valida');
                    continuar = false;
                }
                else if (FechaSalida.getTime() == FechaActual.getTime()) {
                    if (parseInt(horas[0]) < new Date().getHours()) {
                        $scope.MensajesErrorGenerarPlanilla.push('La hora ingresada debe ser mayor a la actual');
                        continuar = false;
                    }
                    else if (parseInt(horas[0]) == new Date().getHours()) {
                        if (parseInt(horas[1]) < new Date().getMinutes()) {
                            $scope.MensajesErrorGenerarPlanilla.push('La hora ingresada debe ser mayor a la actual');
                            continuar = false;
                        }
                        else {
                            if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                                $scope.MensajesError.push('Debe ingresar una hora valida');
                                continuar = false;
                            }
                        }
                    }
                }
                else {
                    if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                        $scope.MensajesError.push('Debe ingresar una hora valida');
                        continuar = false;
                    }
                }
            }

            if (Planilla.Anticipo !== undefined && Planilla.Anticipo !== '') {
                if (RevertirMV(Planilla.Anticipo) > RevertirMV(Planilla.ValorFleteTransportador)) {
                    $scope.MensajesErrorGenerarPlanilla.push('El valor del anticipo no puede ser mayor al valor del flete del transportador');
                    $scope.PlanillaDespacho[$scope.IndicePlanilla].Anticipo = 0;
                    continuar = false;
                }
            }
            return continuar;
        }
        //--Proceso Guardar Planilla Despachos
        //-----------------------------Planificacion Despachos--------------------------//
        //-----------------------------Ruta Mapa Despachos--------------------------//
        $scope.VerRutaMapa = function (indicePlanilla) {
            var DirectionsServices = new google.maps.DirectionsService();
            var DirectionsDisplay = new google.maps.DirectionsRenderer();
            var Planilla = $scope.PlanillaDespacho[indicePlanilla];
            var map;
            var Coordenadas = [];
            var DireccionOrigen = ObtenerDireccionOficina(Planilla.CiudadOrigen);
            var DireccionDestino = ObtenerDireccionOficina(Planilla.CiudadDestino);
            Promise.all([
                ObtenerCoordenadasDireccion(DireccionOrigen),
                ObtenerCoordenadasDireccion(DireccionDestino)
            ]).then(function (results) {
                showModal("modalMostrarRutaMapa");
                Coordenadas = results;
                initMap();
            });

            function initMap() {
                if (map === void 0) {
                    var mapOptions = {
                        zoom: 15,
                        center: Coordenadas[0].Coordenadas,
                        disableDefaultUI: true,
                        mapTypeId: window.google.maps.MapTypeId.ROADMAP
                    };
                    map = new window.google.maps.Map(document.getElementById('gmaps'), mapOptions);
                    DirectionsDisplay.setMap(map);
                    var PromiseRoute = ObtenerRutaMapa(DirectionsServices, Coordenadas[0].Coordenadas, Coordenadas[1].Coordenadas);
                    PromiseRoute.then(function (result) {
                        DirectionsDisplay.setDirections(result);
                        //--Centrar Mapa en ruta
                        DirectionsDisplay.setOptions({ preserveViewport: true });
                        var LatLngList = [];
                        LatLngList.push(new google.maps.LatLng(Coordenadas[0].Coordenadas.lat, Coordenadas[0].Coordenadas.lng));
                        LatLngList.push(new google.maps.LatLng(Coordenadas[1].Coordenadas.lat, Coordenadas[1].Coordenadas.lng));
                        var bounds = new google.maps.LatLngBounds();
                        for (var i = 0, LtLgLen = LatLngList.length; i < LtLgLen; i++) {
                            bounds.extend(LatLngList[i]);
                        }
                        map.setCenter(bounds.getCenter());
                        map.fitBounds(bounds);
                        //--Centrar Mapa en ruta

                        Planilla.Distancia = result.routes[0].legs[0].distance.text;
                        Planilla.Tiempo = result.routes[0].legs[0].duration.text;
                    });
                }
            }
        };
        function ObtenerCoordenadasDireccion(direccion, CodigoOrigen) {
            return new Promise(function (resolve, reject) {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'address': direccion }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var coordenadas = {
                            CodigoOrigen: CodigoOrigen != undefined ? CodigoOrigen : 0,
                            Coordenadas: { lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() }
                        };
                        resolve(coordenadas);
                    }
                    else {
                        reject(new Error(status));
                    }
                });
            });
        }
        function ObtenerRutaMapa(DirectionsServices, origen, destino) {
            return new Promise(function (resolve, reject) {
                DirectionsServices.route({
                    origin: origen,
                    destination: destino,
                    travelMode: google.maps.TravelMode.DRIVING
                }, (response, status) => {
                    if (status === google.maps.DirectionsStatus.OK) {
                        resolve(response);
                    }
                    else {
                        reject(new Error(status));
                    }
                });
            });
        }
        function ObtenerDireccionOficina(CodigoCiudad) {
            var direccion = null;
            var ResponsOficina = OficinasFactory.Consultar({
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                CodigoCiudad: CodigoCiudad,
                Estado: ESTADO_DEFINITIVO,
                Sync: true
            });
            if (ResponsOficina.ProcesoExitoso == true) {
                if (ResponsOficina.Datos.length > 0) {
                    direccion = ResponsOficina.Datos[0].Direccion;
                }
            }
            return direccion;
        }
        $scope.LimpiarMapa = function () {
            $("#gmaps").html("");
        };
        //-----------------------------Ruta Mapa Despachos--------------------------//
        //----Mascaras
        $scope.MascaraHora = function (Planilla) {
            Planilla.HoraSalida = MascaraHora(Planilla.HoraSalida);
        };
        $scope.MascaraAnticipo = function (Planilla) {
            Planilla.Anticipo = MascaraValores(Planilla.Anticipo);
            Planilla.TotalFleteTransportador = MascaraValores(RevertirMV(Planilla.ValorFleteTransportador) + RevertirMV(Planilla.ValorImpuesto) - RevertirMV(Planilla.Anticipo));
        };
        //----Mascaras
        //-------------------Utilitarios-----------------------------//
        //--Ordenamiento Arreglo 
        /// <summary>
        ///   Ordena Arreglo de Objetos, tener en cuenta, solo funciona en primer nivel de anidacion
        /// </summary>
        ///
        /// <param name="Arr">Array que se desea Ordenar</param>
        /// <param name="Tipo">Tipo De ordenaniemto (1 menor a Mayor, 2 Mayor a Menor)</param>
        /// <param name="Criterio">Criterio de comparacion para ordenamiento</param>
        ///
        function OrdenarArregloObj(Arr, Tipo, Criterio) {
            var Tmp;
            var Array = angular.copy(Arr);
            var Actual;
            var Sig;
            if (Tipo == 1) {
                //--Ordena de Menor a Mayor
                for (var i = 1; i < Array.length; i++) {
                    for (var j = 0; j < Array.length - 1; j++) {
                        Actual = Array[j][Criterio] != undefined ? Array[j][Criterio] : Array[j];
                        Sig = Array[j + 1][Criterio] != undefined ? Array[j + 1][Criterio] : Array[j + 1];
                        if (Actual > Sig) {
                            Tmp = Array[j];
                            Array[j] = Array[j + 1];
                            Array[j + 1] = Tmp;
                        }
                    }
                }
            }
            if (Tipo == 2) {
                //--Ordena de Mayor a Menor
                for (var i = 1; i < Array.length; i++) {
                    for (var j = 0; j < Array.length - 1; j++) {
                        Actual = Array[j][Criterio] != undefined ? Array[j][Criterio] : Array[j];
                        Sig = Array[j + 1][Criterio] != undefined ? Array[j + 1][Criterio] : Array[j + 1];
                        if (Actual < Sig) {
                            Tmp = Array[j];
                            Array[j] = Array[j + 1];
                            Array[j + 1] = Tmp;
                        }
                    }
                }
            }
            return Array;
        }
        //--Ordenamiento Arreglo 
        //-------------------Utilitarios-----------------------------//
    }]);