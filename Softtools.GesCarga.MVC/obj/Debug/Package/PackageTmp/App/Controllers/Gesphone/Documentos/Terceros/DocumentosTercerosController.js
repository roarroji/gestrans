﻿SofttoolsApp.controller("DocumentosTercerosCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'SitiosCargueDescargueFactory', 'CiudadesFactory', 'TercerosFactory', 'ValorCatalogosFactory', 'BancosFactory', 'DocumentosFactory', 'GestionDocumentosFactory', 'TarifarioVentasFactory', 'TarifarioComprasFactory', 'ImpuestosFactory', 'EventoCorreosFactory', 'PaisesFactory', 'blockUIConfig', 'ZonasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, SitiosCargueDescargueFactory, CiudadesFactory, TercerosFactory, ValorCatalogosFactory, BancosFactory, DocumentosFactory, GestionDocumentosFactory, TarifarioVentasFactory, TarifarioComprasFactory, ImpuestosFactory, EventoCorreosFactory, PaisesFactory, blockUIConfig, ZonasFactory) {
        console.clear();
        $scope.Titulo = 'CONSULTAR CONDUCTOR';
        $scope.MapaSitio = [{ Nombre: 'Documentos' }, { Nombre: 'Conductor' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_GESPHONE_VENCIMIENTOS.CONDUCTOR); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }
        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta;
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular;
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir;
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar;
        $scope.MIN_DATE = MIN_DATE
        $scope.ACT_DATE = new Date()
        $scope.ListaCorreos = [];
        $scope.ListaSitios = [];
        $scope.ListadoFotosEnviar = [];
        $scope.perfilConductor = false;
        $scope.ListadoDirecciones = [{ Direccion: '' }];
        $scope.SitiosTerceroCliente = [];
        $scope.IdPosiction = 0;
        $scope.Documento = {};
        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            CodigoAlterno: 0,
            CodigoContable: 0,
            DigitoChequeo: 0,
            Codigo: 0,
            Cliente: { Codigo: 0, Impuestos: [] },
            Conductor: { Codigo: 0 },
            Empleado: { Codigo: 0 },
            Proveedor: { Codigo: 0 },
            Observaciones: '',
            SitiosTerceroCliente: [{}],
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Estado: 1
        };
        $scope.ListaResultadoFiltroConsultaSitios = [];
        $scope.SitiosTerceroClienteTotal = [];
        $scope.ListaSitiosGrid = [];
        $scope.totalRegistros = 0;
        $scope.paginaActual = 1;
        $scope.totalPaginas = 0;
        $scope.cantidadRegistrosPorPagina = 10;
        $scope.ListadoZonas = [];
        $scope.BloquearZona = true;

        /* Obtener parametros del enrutador*/
        try {
            $scope.Modelo.Codigo = $scope.Sesion.UsuarioAutenticado.Conductor.Codigo

        } catch (e) {
            $scope.Modelo.Codigo = 0;

        }

        /*-----------------------------------------------------------------DECLARACION VARIABLES--------------------------------------------------------------------------------- */
        $scope.ListaPerfiles = [];
        $scope.ListadoPaises = [];
        $scope.MensajesErrorRecorrido = [];
        $scope.ListadoTipoDocumentoVehiculos = [];
        $scope.ListadoTipoDocumentoVehiculosVerificados = [];
        $scope.DeshabilitarEstado = false;
        $scope.ItemFoto = {};
        $scope.Modelo.Foto = '';
        $('#FotoCargada').hide();
        $scope.ListadoCiudades = [];
        $scope.AsignarJustificacionBloqueo = function (cod) {
            if (cod == 0) {
                $('#JustificacionBloqueo').show();
            } else {
                $('#JustificacionBloqueo').hide();
                $scope.Modelo.JustificacionBloqueo = '';
            }
        };

        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    });
                    $scope.ListadoCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoCiudades);
                }
            }
            return $scope.ListadoCiudades;
        };
        $scope.CargarDatosFunciones = function () {
            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

            $scope.ListadoEstados = [
                { Codigo: 1, Nombre: "ACTIVO" },
                { Codigo: 0, Nombre: "INACTIVO" },
            ];
            try {
                $scope.Modelo.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Estado.Codigo);
                $scope.AsignarJustificacionBloqueo($scope.Modelo.Estado.Codigo);
            } catch (e) {
                $scope.Modelo.Estado = $scope.ListadoEstados[0];
            }

            /* Cargar combo de sitios cargue y descargue*/
            SitiosCargueDescargueFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaSitios = response.data.Datos;
                            try {
                                for (var i = 0; i < $scope.Modelo.SitiosTerceroCliente.length; i++) {
                                    try {
                                        $scope.Modelo.SitiosTerceroCliente[i].SitioCliente = $linq.Enumerable().From($scope.ListaSitios).First('$.Codigo ==' + $scope.Modelo.SitiosTerceroCliente[i].SitioCliente.Codigo);
                                    } catch (e) { }
                                }
                            } catch (e) { }
                        } else {
                            $scope.ListaSitios = []
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            /*Cargar el combo de tipo naturalezas*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_NATURALEZA_TERCERO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoNaturalezas = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoNaturalezas = response.data.Datos;
                            try {
                                $scope.Modelo.TipoNaturaleza = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + $scope.Modelo.TipoNaturaleza.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoNaturaleza = $scope.ListadoNaturalezas[0]
                            }
                        }
                        else {
                            $scope.ListadoNaturalezas = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo anticipo*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 187 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoAnticipo = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoAnticipo = response.data.Datos;
                            try {
                                $scope.Modelo.TipoAnticipo = $linq.Enumerable().From($scope.ListadoTipoAnticipo).First('$.Codigo ==' + $scope.Modelo.TipoAnticipo.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoAnticipo = $scope.ListadoTipoAnticipo[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoAnticipo = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo identificaciones*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_IDENTIFICACION_TERCERO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoIdentificacionGeneral = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoIdentificacionGeneral = response.data.Datos;
                            $scope.AsignarTipoIdentificacion()
                        }
                        else {
                            $scope.ListadoTipoIdentificacion = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo naturalezas*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: 192 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoValidacionCupo = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoValidacionCupo = response.data.Datos;
                            try {
                                $scope.Modelo.TipoValidacionCupo = $linq.Enumerable().From($scope.ListadoValidacionCupo).First('$.Codigo ==' + $scope.Modelo.TipoValidacionCupo.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoValidacionCupo = $scope.ListadoValidacionCupo[0]
                            }
                        }
                        else {
                            $scope.ListadoValidacionCupo = []
                        }
                    }
                }, function (response) {
                });
            //Combo lista evento correos----------------------------------------------------------------------

            EventoCorreosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListaEventoCorreos = [];
                        $scope.ListaEventoCorreos = response.data.Datos;
                        for (var i = 0; i < $scope.ListaEventoCorreos.length; i++) {
                            $scope.ListaEventoCorreos[i].Nombre = $scope.ListaEventoCorreos[i].Nombre.toUpperCase()
                        }
                        $scope.ListaEventoCorreos.push({ Nombre: 'Seleccione un Evento', Codigo: 0 })
                        $scope.EventoCorreo = $scope.ListaEventoCorreos[$scope.ListaEventoCorreos.length - 1]
                    }
                }, function (response) {
                    $scope.Buscando = false;
                });

            //Funcion agregar Email al formulario-------------------------------------------------------------

            $scope.AgregarCorreo = function () {
                if ($scope.EventoCorreo != '' && $scope.EventoCorreo != undefined && $scope.EventoCorreo.Codigo != 0) {
                    if ($scope.Email != '' && $scope.Email != undefined) {
                        var concidencias = 0
                        if ($scope.ListaCorreos.length > 0) {
                            for (var i = 0; i < $scope.ListaCorreos.length; i++) {
                                if ($scope.Email == $scope.ListaCorreos[i].Email && $scope.EventoCorreo.Codigo == $scope.ListaCorreos[i].EventoCorreo.Codigo) {
                                    concidencias++
                                    break;
                                }
                            }
                            if (concidencias > 0) {
                                ShowError('El correo electronico ya fue ingresado')
                                $scope.Email = '';
                            }

                            else {
                                $scope.ListaCorreos.push({ Email: $scope.Email, EventoCorreo: $scope.EventoCorreo, ModificarCorreo: false });
                                $scope.Email = '';
                                $scope.AgruparListado()

                                $scope.ListaCorreosAgrupado.forEach(function (Recorrido) {
                                    Recorrido.Data.forEach(function (Detalle) {
                                        if ($scope.EventoCorreo.Codigo == Detalle.EventoCorreo.Codigo) {
                                            Recorrido.items = false;
                                        } else {
                                            Recorrido.items = true;
                                        }
                                    })
                                })

                            }
                        } else {
                            $scope.ListaCorreos.push({ Email: $scope.Email, EventoCorreo: $scope.EventoCorreo, ModificarCorreo: false });
                            $scope.Email = '';
                            $scope.AgruparListado()

                            $scope.ListaCorreosAgrupado.forEach(function (Recorrido) {
                                Recorrido.Data.forEach(function (Detalle) {
                                    if ($scope.EventoCorreo.Codigo == Detalle.EventoCorreo.Codigo) {
                                        Recorrido.items = false;
                                    } else {
                                        Recorrido.items = true;
                                    }
                                })
                            })

                        }
                    } else {
                        ShowError('Debe ingresar el correo electronico');
                    }
                } else {
                    ShowError('Debe ingresar el evento correos');
                }
            }

            //Funcion eliminar correo---------------------------------------------------------------------------

            $scope.EliminarCorreo = function (evento, email) {

                if ($scope.ListaCorreos.length > 0) {
                    for (var i = 0; i < $scope.ListaCorreos.length; i++) {
                        if (email == $scope.ListaCorreos[i].Email) {
                            if (evento == $scope.ListaCorreos[i].EventoCorreo.Codigo) {
                                $scope.ListaCorreos.splice(i, 1);
                                $scope.AgruparListado();
                            }
                        }
                    }
                }
            };


            $scope.AsignarTipoIdentificacion = function () {
                if ($scope.Modelo.TipoNaturaleza != undefined) {
                    if ($scope.Modelo.TipoNaturaleza.Codigo === 501) {
                        $scope.ListadoTipoIdentificacion = []
                        for (var i = 0; i < $scope.ListadoTipoIdentificacionGeneral.length; i++) {
                            if ($scope.ListadoTipoIdentificacionGeneral[i].Codigo !== 102) {
                                $scope.ListadoTipoIdentificacion.push($scope.ListadoTipoIdentificacionGeneral[i])
                            }
                        }
                        try {
                            $scope.Modelo.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.Modelo.TipoIdentificacion.Codigo);
                        } catch (e) {
                            $scope.Modelo.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                        }
                        $scope.Juridica = false
                        $scope.Modelo.RazonSocial = ''
                        $scope.Modelo.RepresentanteLegal = ''
                        $scope.Natural = true
                    } else if ($scope.Modelo.TipoNaturaleza.Codigo === 502) {
                        $scope.ListadoTipoIdentificacion = []
                        for (var i = 0; i < $scope.ListadoTipoIdentificacionGeneral.length; i++) {
                            if ($scope.ListadoTipoIdentificacionGeneral[i].Codigo === 102) {
                                $scope.ListadoTipoIdentificacion.push($scope.ListadoTipoIdentificacionGeneral[i])
                            }
                        }
                        try {
                            $scope.Modelo.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + $scope.Modelo.TipoIdentificacion.Codigo);
                        } catch (e) {
                            $scope.Modelo.TipoIdentificacion = $scope.ListadoTipoIdentificacion[0]
                        }
                        $scope.Juridica = true
                        $scope.Natural = false
                        $scope.Modelo.Nombre = ''
                        $scope.Modelo.PrimeroApellido = ''
                        $scope.Modelo.SegundoApellido = ''
                        $scope.Modelo.Sexo = $scope.ListadoSexos[0]
                        $scope.Modelo.CiudadExpedicionIdentificacion = ''
                        $scope.Modelo.CiudadNacimiento = ''
                    } else {
                        $scope.Juridica = false
                        $scope.Natural = false
                        $scope.Modelo.RazonSocial = ''
                        $scope.Modelo.RepresentanteLegal = ''
                        $scope.Modelo.Nombre = ''
                        $scope.Modelo.PrimeroApellido = ''
                        $scope.Modelo.SegundoApellido = ''
                        $scope.Modelo.Sexo = $scope.ListadoSexos[0]
                        $scope.Modelo.CiudadExpedicionIdentificacion = ''
                        $scope.Modelo.CiudadNacimiento = ''
                    }

                }

            }
            /*Cargar el combo de Sexos*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_SEXO_TERCERO_PERSONA_NATURAL } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoSexos = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoSexos = response.data.Datos;
                            try {
                                $scope.Modelo.Sexo = $linq.Enumerable().From($scope.ListadoSexos).First('$.Codigo ==' + $scope.Modelo.Sexo.Codigo);
                            } catch (e) {
                                $scope.Modelo.Sexo = $scope.ListadoSexos[0]
                            }
                        }
                        else {
                            $scope.ListadoSexos = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de forma pago */
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_PAGO_VENTAS } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoFormaPago = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoFormaPago = response.data.Datos;
                            try {
                                $scope.Modelo.Cliente.FormaPago = $linq.Enumerable().From($scope.ListadoFormaPago).First('$.Codigo ==' + $scope.Modelo.Cliente.FormaPago.Codigo);
                                $scope.AsignarFormaPago($scope.Modelo.Cliente.FormaPago.Codigo)
                            } catch (e) {
                                try {
                                    $scope.Modelo.Cliente.FormaPago = $scope.ListadoFormaPago[0]
                                } catch (e) {
                                }
                            }
                        }
                        else {
                            $scope.ListadoFormaPago = []
                        }
                    }
                }, function (response) {
                });
            $('#DiasPlazo').hide()
            $scope.AsignarFormaPago = function (cod) {
                if (cod == CODIGO_CATALOGO_FORMA_PAGO_VENTAS_CREDITO) {
                    $('#DiasPlazo').show();
                } else {
                    $('#DiasPlazo').hide();
                    $scope.Modelo.Cliente.DiasPlazo = 0
                }

            }
            /*Cargar el combo de forma pago */
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_FORMA_COBRO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoFormaCobro = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoFormaCobro = response.data.Datos;
                            try {
                                $scope.Modelo.Proveedor.FormaCobro = $linq.Enumerable().From($scope.ListadoFormaCobro).First('$.Codigo ==' + $scope.Modelo.Proveedor.FormaCobro.Codigo);
                                $scope.AsignarFormaCobro($scope.Modelo.Proveedor.FormaCobro.Codigo)
                            } catch (e) {
                                try {
                                    $scope.Modelo.Proveedor.FormaCobro = $scope.ListadoFormaCobro[0]
                                } catch (e) {
                                }
                            }
                        }
                        else {
                            $scope.ListadoFormaCobro = []
                        }
                    }
                }, function (response) {
                });
            $('#DiasPlazo2').hide()
            $scope.AsignarFormaCobro = function (cod) {
                if (cod == 8801) {
                    $('#DiasPlazo2').show()
                } else {
                    $('#DiasPlazo2').hide()
                    $scope.Modelo.Proveedor.DiasPlazo = 0
                }

            }
            //Tipo Documento Terceros
            //$scope.ListadoTipoDocumentoVehiculos = [];
            //$scope.CargarComboTipoDocumento = function () {
            //    for (var i = 0; i < $scope.ListadoCatalogosCombos.length; i++) {
            //        if ($scope.ListadoCatalogosCombos[i].Catalogo.Codigo == 146) {
            //            $scope.ListadoTipoDocumentoVehiculos.push($scope.ListadoCatalogosCombos[i])
            //        }
            //    }
            //}
            //$scope.CargarComboTipoDocumento();
            /*Cargar el combo de Tipo cta bancarias*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CUENTA_BANCARIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoBancos = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoBancos = response.data.Datos;
                            try {
                                $scope.Modelo.TipoBanco = $linq.Enumerable().From($scope.ListadoTipoBancos).First('$.Codigo ==' + $scope.Modelo.TipoBanco.Codigo);
                            } catch (e) {
                                $scope.Modelo.TipoBanco = $scope.ListadoTipoBancos[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoBancos = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar los check de perfiles*/

            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_PERFIL_TERCEROS } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.perfilTerceros = response.data.Datos;

                            $scope.perfilTerceros.splice(0, 1)
                            try {
                                for (var j = 0; j < $scope.perfilTerceros.length; j++) {
                                    for (var i = 0; i < $scope.Modelo.Perfiles.length; i++) {
                                        if ($scope.Modelo.Perfiles[i].Codigo == $scope.perfilTerceros[j].Codigo) {
                                            $scope.perfilTerceros[j].Estado = true
                                        }
                                    }
                                }
                                $scope.verificacionPerfiles()
                            } catch (e) {
                                for (var i = 0; i < $scope.perfilTerceros.length; i++) {
                                    var perfil = $scope.perfilTerceros[i]
                                    perfil.Estado = false
                                }
                            }
                            $scope.perfilTerceros.Asc = true
                            OrderBy('Nombre', undefined, $scope.perfilTerceros);

                        }
                        else {
                            $scope.perfilTerceros = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de Tipo Sangres*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DE_SANGRE } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoSangres = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoSangres = response.data.Datos;
                            try {
                                $scope.Modelo.Conductor.TipoSangre = $linq.Enumerable().From($scope.ListadoTipoSangres).First('$.Codigo ==' + $scope.Modelo.Conductor.TipoSangre.Codigo);
                            } catch (e) {
                                $scope.Modelo.Conductor.TipoSangre = $scope.ListadoTipoSangres[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoSangres = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de Tipo Contrato*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CONTRATO_CONDUCTOR } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoContrato = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoContrato = response.data.Datos;
                            try {
                                $scope.Modelo.Conductor.TipoContrato = $linq.Enumerable().From($scope.ListadoTipoContrato).First('$.Codigo ==' + $scope.Modelo.Conductor.TipoContrato.Codigo);
                            } catch (e) {
                                $scope.Modelo.Conductor.TipoContrato = $scope.ListadoTipoContrato[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoContrato = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de Tipo Contrato empleados*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_CONTRATO_EMPLEADO } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoContratoEmpleados = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoContratoEmpleados = response.data.Datos;
                            try {
                                $scope.Modelo.Empleado.TipoContrato = $linq.Enumerable().From($scope.ListadoTipoContratoEmpleados).First('$.Codigo ==' + $scope.Modelo.Empleado.TipoContrato.Codigo);
                            } catch (e) {
                                $scope.Modelo.Empleado.TipoContrato = $scope.ListadoTipoContratoEmpleados[0]
                            }
                        }
                        else {
                            $scope.ListadoTipoContrato = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de Categoria Licencia*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CATEGORIAS_LICENCIAS_DE_CONDUCCION } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCategorias = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCategorias = response.data.Datos;
                            try {
                                $scope.Modelo.Conductor.CategoriaLicencia = $linq.Enumerable().From($scope.ListadoCategorias).First('$.Codigo ==' + $scope.Modelo.Conductor.CategoriaLicencia.Codigo);
                            } catch (e) {
                                $scope.Modelo.Conductor.CategoriaLicencia = $scope.ListadoCategorias[0]
                            }
                        }
                        else {
                            $scope.ListadoCategorias = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de departamento empleado*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_DEPARTAMENTO_EMPLEADOS } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDepartamentosEmpleado = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoDepartamentosEmpleado = response.data.Datos;
                            try {
                                $scope.Modelo.Empleado.Departamento = $linq.Enumerable().From($scope.ListadoDepartamentosEmpleado).First('$.Codigo ==' + $scope.Modelo.Empleado.Departamento.Codigo);
                            } catch (e) {
                                $scope.Modelo.Empleado.Departamento = $scope.ListadoDepartamentosEmpleado[0]
                            }
                        }
                        else {
                            $scope.ListadoDepartamentosEmpleado = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de cargos*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_CARGOS } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCargos = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCargos = response.data.Datos;
                            try {
                                $scope.Modelo.Empleado.Cargo = $linq.Enumerable().From($scope.ListadoCargos).First('$.Codigo ==' + $scope.Modelo.Empleado.Cargo.Codigo);
                            } catch (e) {
                                $scope.Modelo.Empleado.Cargo = $scope.ListadoCargos[0]
                            }
                        }
                        else {
                            $scope.ListadoCargos = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de Bancos*/
            BancosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoBancos = [];
                        response.data.Datos.forEach(function (item) {
                            $scope.ListadoBancos.push(item);
                        });
                        try {
                            $scope.Modelo.Cliente.Impuestos
                            $scope.Modelo.Banco = $linq.Enumerable().From($scope.ListadoBancos).First('$.Codigo ==' + $scope.Modelo.Banco.Codigo);
                        } catch (e) {
                            $scope.Modelo.Banco = $scope.ListadoBancos[0]
                        }
                    }
                }, function (response) {
                });



            /*Cargar Autocomplete de Grupos empresariales*/
            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_COMERCIAL, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoRepresentanteComerciales = []
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoRepresentanteComerciales = response.data.Datos;
                            try {
                                if ($scope.Modelo.Cliente.RepresentanteComercial.Codigo > 0) {
                                    $scope.Modelo.Cliente.RepresentanteComercial = $linq.Enumerable().From($scope.ListadoRepresentanteComerciales).First('$.Codigo ==' + $scope.Modelo.Cliente.RepresentanteComercial.Codigo);
                                }
                            } catch (e) {
                                $scope.Modelo.RepresentanteComercial = ''
                            }
                        }
                        else {
                            $scope.ListadoGrupoEmpresariales = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            /*Cargar el combo de paises*/
            PaisesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.push({ Nombre: 'Seleccione país', Codigo: 0 })
                            $scope.ListadoPaises = response.data.Datos;
                            if ($scope.CodigoPais > 0) {
                                $scope.Modelo.Pais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + $scope.CodigoPais);
                            }
                            else {
                                $scope.Modelo.Pais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + $scope.Sesion.Empresa.Pais.Codigo);
                            }
                        } else {
                            $scope.ListadoPaises = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            //Elimina todos los archivos temporales asociados a este usuario
            DocumentosFactory.EliminarDocumento({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo } }).
                then(function (response) {
                }, function (response) {
                    ShowError(response.statusText);
                });
            //Carga los documentos

            GestionDocumentosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, EntidadDocumento: { Codigo: 2 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoDocumentos = []
                        if (response.data.Datos.length > 0) {
                            for (var i = 0; i < response.data.Datos.length; i++) {
                                var item = response.data.Datos[i]
                                if (item.Habilitado == 1) {
                                    if (item.Documento.Codigo == 1) {

                                        $scope.ListadoFotos = [];

                                        for (var f = 0; f < item.Cantidad_repeticiones; f++) {
                                            var temp = angular.copy(item);
                                            temp.IDNew = f;
                                            $scope.ListadoFotos.push(temp);

                                        }
                                        //$scope.ItemFoto = item
                                    }
                                    else {
                                        $scope.ListadoDocumentos.push(item)
                                    }
                                }

                            }
                            try {
                                var esImagen = false
                                if ($scope.Modelo.Documentos.length > 0) {
                                    for (var j = 0; j < $scope.ListadoDocumentos.length; j++) {
                                        $scope.ListadoDocumentos[j]
                                        for (var k = 0; k < $scope.Modelo.Documentos.length; k++) {
                                            var doc2 = $scope.Modelo.Documentos[k]
                                            if ($scope.ListadoDocumentos[j].Codigo == doc2.Codigo) {
                                                $scope.ListadoDocumentos[j].Referencia = doc2.Referencia
                                                $scope.ListadoDocumentos[j].Emisor = doc2.Emisor
                                                try {
                                                    if (new Date(doc2.FechaEmision) < MIN_DATE) {
                                                        $scope.ListadoDocumentos[j].FechaEmision = ''
                                                    } else {
                                                        $scope.ListadoDocumentos[j].FechaEmision = new Date(doc2.FechaEmision)
                                                    }

                                                } catch (e) {
                                                    $scope.ListadoDocumentos[j].FechaEmision = ''
                                                }
                                                try {
                                                    if (new Date(doc2.FechaVence) < MIN_DATE) {
                                                        $scope.ListadoDocumentos[j].FechaVence = ''
                                                    } else {
                                                        $scope.ListadoDocumentos[j].FechaVence = new Date(doc2.FechaVence)
                                                    }
                                                } catch (e) {
                                                    $scope.ListadoDocumentos[j].FechaVence = ''
                                                }
                                                $scope.ListadoDocumentos[j].ValorDocumento = doc2.ValorDocumento

                                            } else {
                                                if (!esImagen) {
                                                    if (doc2.Codigo == $scope.ItemFoto.Codigo) {
                                                        esImagen = true
                                                        doc2.Terceros = true
                                                        doc2.CodigoTerceros = $scope.Modelo.Codigo
                                                        $scope.ItemFoto.ValorDocumento = doc2.ValorDocumento
                                                        DocumentosFactory.Obtener(doc2).
                                                            then(function (response) {
                                                                if (response.data.ProcesoExitoso === true) {
                                                                    if (response.data.Datos) {
                                                                        doc2.Archivo = response.data.Datos.Archivo
                                                                        doc2.Extencion = response.data.Datos.Extencion
                                                                        doc2.Tipo = response.data.Datos.Tipo
                                                                        $scope.FotoCargada = 'data:' + doc2.Tipo + ';base64,' + doc2.Archivo
                                                                        $('#Foto').hide()
                                                                        $('#FotoCargada').show()
                                                                    }
                                                                } else {
                                                                    ShowError(response.data.MensajeError);
                                                                }
                                                            }, function (response) {
                                                                ShowError(response.statusText);
                                                            });

                                                    }
                                                }

                                            }
                                        }
                                    }
                                    $scope.Modelo.Semirremolque = $linq.Enumerable().From($scope.ListadoSemirremolques).First('$.Codigo ==' + $scope.Modelo.Semirremolque.Codigo);
                                }
                            } catch (e) {
                            }
                        }
                        else {
                            $scope.Documentos = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });


            TarifarioVentasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
                then(function (response) {
                    $scope.ListadoTarifarios = [];
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTarifarios = response.data.Datos
                        try {
                            $scope.Modelo.Cliente.Tarifario = $linq.Enumerable().From($scope.ListadoTarifarios).First('$.Codigo ==' + $scope.Modelo.Cliente.Tarifario.Codigo);
                        } catch (e) {
                            $scope.Modelo.Cliente.Tarifario = $scope.ListadoTarifarios[0]
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            TarifarioComprasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
                then(function (response) {
                    $scope.ListadoTarifariosCompra = [];
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTarifariosCompra = response.data.Datos
                        try {
                            $scope.Modelo.Proveedor.Tarifario = $linq.Enumerable().From($scope.ListadoTarifariosCompra).First('$.Codigo ==' + $scope.Modelo.Proveedor.Tarifario.Codigo);
                        } catch (e) {
                            $scope.Modelo.Proveedor.Tarifario = $scope.ListadoTarifariosCompra[0]
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            /*Cargar los impuestos*/
            ImpuestosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoImpuestos = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoImpuestos = response.data.Datos;
                            $scope.ListadoImpuestosVenta = []
                            $scope.ListadoImpuestosCompra = []
                            for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                                if ($scope.ListadoImpuestos[i].Tipo_Impuesto.Codigo == CODIGO_TIMPO_IMPUESTO_COMPRA) {
                                    $scope.ListadoImpuestosCompra.push($scope.ListadoImpuestos[i])
                                }
                                if ($scope.ListadoImpuestos[i].Tipo_Impuesto.Codigo == CODIGO_TIMPO_IMPUESTO_VENTA) {
                                    $scope.ListadoImpuestosVenta.push($scope.ListadoImpuestos[i])
                                }
                            }
                            //try {
                            //    $scope.Modelo.TipoNaturaleza = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + $scope.Modelo.TipoNaturaleza.Codigo);
                            //} catch (e) {
                            //    $scope.Modelo.TipoNaturaleza = $scope.ListadoNaturalezas[0]
                            //}
                        }
                        else {
                            $scope.ListadoImpuestos = []
                        }
                    }
                }, function (response) {
                });

        }


        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        if ($scope.Modelo.Codigo > 0) {
            // Consultar los datos del tercero correspondientes al número que llego como parametro del enrutador
            //$scope.Titulo = 'CONSULTAR TERCERO';
            $scope.Deshabilitar = true;
            ObtenerTercero();
        } else {
            $scope.CargarDatosFunciones()
        }
        /*----------------------------------------------------------------------------------Funcion Obtener datos-------------------------------------------------------------------*/
        function ObtenerTercero() {
            BloqueoPantalla.start('Cargando...');
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Codigo: $scope.Modelo.Codigo,
            };

            blockUI.delay = 1000;
            TercerosFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.Modelo.Documentos = [];
                        $scope.Modelo = response.data.Datos;
                        try { $scope.Modelo.Ciudad = $scope.CargarCiudad(response.data.Datos.Ciudad.Codigo) } catch (e) { }
                        try { $scope.Modelo.CiudadExpedicionIdentificacion = $scope.CargarCiudad(response.data.Datos.CiudadExpedicionIdentificacion.Codigo) } catch (e) { }
                        try { $scope.Modelo.CiudadNacimiento = $scope.CargarCiudad(response.data.Datos.CiudadNacimiento.Codigo) } catch (e) { }

                        var telefonos = response.data.Datos.Telefonos.split(';')
                        $scope.Telefono = telefonos[0]
                        if (telefonos[1] !== undefined) {
                            $scope.Telefono2 = telefonos[1]
                        }
                        $scope.CodigoPais = response.data.Datos.Pais.Codigo
                        if ($scope.CodigoPais > 0 && $scope.ListadoPaises.length > 0) {
                            $scope.Modelo.Pais = $linq.Enumerable().From($scope.ListadoPaises).First('$.Codigo ==' + $scope.CodigoPais);
                        }

                        $scope.SitiosTerceroCliente = [];
                        try {
                            for (var i = 0; i < response.data.Datos.SitiosTerceroCliente.length; i++) {
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.Ciudad = response.data.Datos.SitiosTerceroCliente[i].Ciudad;
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.Zona = response.data.Datos.SitiosTerceroCliente[i].Zona;
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.ValorCargueCliente = MascaraValores(response.data.Datos.SitiosTerceroCliente[i].ValorCargueCliente);
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.ValorDescargueCliente = MascaraValores(response.data.Datos.SitiosTerceroCliente[i].ValorDescargueCliente);
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.ValorCargueTransportador = MascaraValores(response.data.Datos.SitiosTerceroCliente[i].ValorCargueTransportador);
                                response.data.Datos.SitiosTerceroCliente[i].SitioCliente.ValorDescargueTransportador = MascaraValores(response.data.Datos.SitiosTerceroCliente[i].ValorDescargueTransportador);
                                $scope.SitiosTerceroCliente.push(response.data.Datos.SitiosTerceroCliente[i].SitioCliente);
                            }
                        } catch (e) {
                        }

                        $scope.SitiosTerceroClienteTotal = [];
                        $scope.ListaSitiosGrid = [];

                        if ($scope.SitiosTerceroCliente.length > 0) {
                            $scope.SitiosTerceroCliente.forEach(function (itemSitios) {
                                $scope.SitiosTerceroClienteTotal.push(itemSitios);
                            });
                            for (var i = 0; i <= $scope.SitiosTerceroClienteTotal.length - 1; i++) {
                                if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                    $scope.ListaSitiosGrid.push($scope.SitiosTerceroClienteTotal[i]);
                                }
                            }
                        }

                        if ($scope.SitiosTerceroClienteTotal.length > 0) {
                            $scope.totalRegistros = $scope.SitiosTerceroClienteTotal.length;
                            $scope.totalPaginas = Math.ceil($scope.totalRegistros / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        } else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }

                        //var q = 0;
                        //var x = 0;
                        //if (response.data.Datos.ListadoFOTOS != undefined) {
                        //    for (q = 0; q < $scope.ListadoFotos.length; q++) {

                        //        for (var x = 0; x < response.data.Datos.ListadoFOTOS.length; x++) {

                        //            if (response.data.Datos.ListadoFOTOS[x].Numero == $scope.ListadoFotos[q].IDNew) {

                        //                $scope.Documento = response.data.Datos.ListadoFOTOS[x]
                        //                $scope.ListadoFotos[q].id = response.data.Datos.ListadoFOTOS[x].Id

                        //                $scope.IdPosiction = response.data.Datos.ListadoFOTOS[x].Id
                        //                reader.onload = $scope.AsignarArchivo($scope.Documento);
                        //                reader.readAsDataURL(element.files[0]);
                        //            }
                        //        }
                        //    }
                        //}

                        if (response.data.Datos.Conductor !== undefined && response.data.Datos.Conductor !== null) {
                            if (response.data.Datos.Conductor.Propio == 1) {
                                response.data.Datos.Conductor.Propio = true
                            }
                            if (ValidarFecha(response.data.Datos.Conductor.FechaVencimiento) == 0) {
                                $scope.Modelo.Conductor.FechaVencimiento = new Date(response.data.Datos.Conductor.FechaVencimiento)
                            }
                            if (ValidarFecha(response.data.Datos.Conductor.FechaUltimoViaje) == 0) {
                                $scope.Modelo.Conductor.FechaUltimoViaje = new Date(response.data.Datos.Conductor.FechaUltimoViaje)
                            }
                        } else {
                            $scope.Modelo.Conductor = { Codigo: 0 }

                        }
                        if (response.data.Datos.Empleado !== undefined && response.data.Datos.Empleado !== null) {
                            if (ValidarFecha(response.data.Datos.Empleado.FechaVinculacion) == 0) {
                                $scope.Modelo.Empleado.FechaVinculacion = new Date(response.data.Datos.Empleado.FechaVinculacion)
                                if ($scope.Modelo.Empleado.FechaVinculacion < MIN_DATE) {
                                    $scope.Modelo.Empleado.FechaVinculacion = '';
                                }

                            }
                            if (ValidarFecha(response.data.Datos.Empleado.FechaFinalizacion) == 0) {

                                $scope.Modelo.Empleado.FechaFinalizacion = new Date(response.data.Datos.Empleado.FechaFinalizacion)
                                if ($scope.Modelo.Empleado.FechaFinalizacion < MIN_DATE) {
                                    $scope.Modelo.Empleado.FechaFinalizacion = ''
                                }
                            }
                        } else {
                            $scope.Modelo.Empleado = { Codigo: 0 }
                        }
                        if (response.data.Datos.Cliente == undefined || response.data.Datos.Cliente == null) {
                            $scope.Modelo.Cliente = { Codigo: 0 }
                        }

                        if (response.data.Datos.Direcciones.length > 0) {
                            $scope.DireccionesTemp = response.data.Datos.Direcciones

                        } else {
                            $scope.ListadoDirecciones = [{ Direccion: '' }]
                        }
                        try {
                            $scope.ListadoDirecciones = []
                            if ($scope.DireccionesTemp != undefined) {
                                for (var i = 0; i < $scope.DireccionesTemp.length; i++) {
                                    var ciud
                                    try {
                                        try { ciud = $scope.CargarCiudad(response.data.Datos.Direcciones[i].Ciudad.Codigo) } catch (e) { }
                                    } catch (e) {
                                        ciud = ''
                                    }
                                    $scope.ListadoDirecciones.push({ Direccion: $scope.DireccionesTemp[i].Direccion, Ciudad: ciud, Telefonos: $scope.DireccionesTemp[i].Telefonos, Barrio: $scope.DireccionesTemp[i].Barrio, CodigoPostal: $scope.DireccionesTemp[i].CodigoPostal })
                                }
                            }
                            else {
                                $scope.ListadoDirecciones = [{ Direccion: '' }]
                            }

                        } catch (e) {
                        };

                        $scope.AsignarNombre()
                        if (response.data.Datos.Beneficiario !== undefined && response.data.Datos.Beneficiario !== null) {
                            if (response.data.Datos.Beneficiario.Codigo > 0) {
                                var beneficiario = $scope.CargarTercero(response.data.Datos.Beneficiario.Codigo)
                                $scope.ModalIdentificacionBeneficiario = beneficiario.NumeroIdentificacion
                                $scope.ModalNombreBeneficiario = beneficiario.NombreCompleto
                                $scope.Modelo.Beneficiario = { Codigo: beneficiario.Codigo }
                            }
                        }
                        try {
                            $scope.EventoCorreo = $scope.Modelo.ListadoCorreos[0].EventoCorreo.Nombre;
                            $scope.EventoCorreo = '';
                            $scope.ListaCorreos = $scope.Modelo.ListadoCorreos;
                            $scope.AgruparListado();
                        } catch (e) {
                        }
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $timeout(function () {
                            $scope.CargarDatosFunciones()
                        }, 200);

                        blockUI.delay = 1000;

                    }
                    else {
                        ShowError('No se logro consultar el tercero No.' + $scope.Modelo.NumeroIdentificacion + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#consultarTerceros';
                    }
                    BloqueoPantalla.stop();
                }, function (response) {
                    ShowError('No se logro consultar el tercero No.' + $scope.Modelo.NumeroIdentificacion + '. Por favor contacte el administrador del sistema.');
                    document.location.href = '#consultarTerceros';
                    BloqueoPantalla.stop();
                });
        }

        $scope.CargarTerceroEstudio = function () {
            BloqueoPantalla.start('Cargando tercero ...');
            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                NumeroAutorizacion: $scope.NumeroEstudioSeguridad
            };
            if ($scope.DatosRequeridosEstudio()) {
                TercerosFactory.ObtenerTerceroEstudioSeguridad(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Anulado === 0) {
                                var sinAsignar = true;
                                for (var i = 0; i < $scope.perfilTerceros.length; i++) {
                                    if ($scope.Modelo.NumeroIdentificacion == response.data.Datos.Propietario.NumeroIdentificacion) {
                                        if ($scope.perfilTerceros[i].Estado === true && $scope.perfilTerceros[i].Codigo === 1408) {
                                            if (response.data.Datos.Propietario.TipoNaturaleza.Codigo !== 0 || response.data.Datos.Propietario.TipoNaturaleza.Codigo !== undefined) {
                                                $scope.Modelo.TipoNaturaleza = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + response.data.Datos.Propietario.TipoNaturaleza.Codigo);
                                                $scope.AsignarTipoIdentificacion();
                                                $scope.Modelo.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.Propietario.TipoIdentificacion.Codigo);
                                            }
                                            $scope.Modelo.DigitoChequeo = response.data.Datos.Propietario.DigitoChequeo;
                                            $scope.Modelo.Nombre = response.data.Datos.Propietario.Nombre;
                                            $scope.Modelo.PrimeroApellido = response.data.Datos.Propietario.PrimeroApellido;
                                            $scope.Modelo.SegundoApellido = response.data.Datos.Propietario.SegundoApellido;
                                            $scope.Modelo.CiudadExpedicionIdentificacion = $scope.CargarCiudad(response.data.Datos.Propietario.CiudadExpedicionIdent.Codigo);
                                            $scope.Modelo.Ciudad = $scope.CargarCiudad(response.data.Datos.Propietario.Ciudad.Codigo);
                                            $scope.Modelo.Direccion = response.data.Datos.Propietario.Direccion;
                                            $scope.Telefono = response.data.Datos.Propietario.Telefonos;
                                            $scope.Modelo.Celular = response.data.Datos.Propietario.Celular;
                                            sinAsignar = false;
                                        }
                                    } else if ($scope.Modelo.NumeroIdentificacion == response.data.Datos.PropietarioRemolque.NumeroIdentificacion) {
                                        if ($scope.perfilTerceros[i].Estado === true && $scope.perfilTerceros[i].Codigo === 1408) {
                                            if (response.data.Datos.PropietarioRemolque.TipoNaturaleza.Codigo !== 0 || response.data.Datos.PropietarioRemolque.TipoNaturaleza.Codigo !== undefined) {
                                                $scope.Modelo.TipoNaturaleza = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + response.data.Datos.PropietarioRemolque.TipoNaturaleza.Codigo);
                                                $scope.AsignarTipoIdentificacion();
                                                $scope.Modelo.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.PropietarioRemolque.TipoIdentificacion.Codigo);
                                            }
                                            $scope.Modelo.DigitoChequeo = response.data.Datos.PropietarioRemolque.DigitoChequeo;
                                            $scope.Modelo.Nombre = response.data.Datos.PropietarioRemolque.Nombre;
                                            $scope.Modelo.PrimeroApellido = response.data.Datos.PropietarioRemolque.PrimeroApellido;
                                            $scope.Modelo.SegundoApellido = response.data.Datos.PropietarioRemolque.SegundoApellido;
                                            $scope.Modelo.CiudadExpedicionIdentificacion = $scope.CargarCiudad(response.data.Datos.PropietarioRemolque.CiudadExpedicionIdent.Codigo);
                                            $scope.Modelo.Ciudad = $scope.CargarCiudad(response.data.Datos.PropietarioRemolque.Ciudad.Codigo);
                                            $scope.Modelo.Direccion = response.data.Datos.PropietarioRemolque.Direccion;
                                            $scope.Telefono = response.data.Datos.PropietarioRemolque.Telefonos;
                                            $scope.Modelo.Celular = response.data.Datos.PropietarioRemolque.Celular;
                                            sinAsignar = false;
                                        }
                                    } else if ($scope.Modelo.NumeroIdentificacion == response.data.Datos.Conductor.NumeroIdentificacion) {
                                        if ($scope.perfilTerceros[i].Estado === true && $scope.perfilTerceros[i].Codigo === 1403) {
                                            if (response.data.Datos.Conductor.TipoNaturaleza.Codigo !== 0 || response.data.Datos.Conductor.TipoNaturaleza.Codigo !== undefined) {
                                                $scope.Modelo.TipoNaturaleza = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + response.data.Datos.Conductor.TipoNaturaleza.Codigo);
                                                $scope.AsignarTipoIdentificacion();
                                                $scope.Modelo.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.Conductor.TipoIdentificacion.Codigo);
                                            }
                                            $scope.Modelo.DigitoChequeo = response.data.Datos.Conductor.DigitoChequeo;
                                            $scope.Modelo.Nombre = response.data.Datos.Conductor.Nombre;
                                            $scope.Modelo.PrimeroApellido = response.data.Datos.Conductor.PrimeroApellido;
                                            $scope.Modelo.SegundoApellido = response.data.Datos.Conductor.SegundoApellido;
                                            $scope.Modelo.CiudadExpedicionIdentificacion = $scope.CargarCiudad(response.data.Datos.Conductor.CiudadExpedicionIdent.Codigo);
                                            $scope.Modelo.Ciudad = $scope.CargarCiudad(response.data.Datos.Conductor.Ciudad.Codigo);
                                            $scope.Modelo.Direccion = response.data.Datos.Conductor.Direccion;
                                            $scope.Telefono = response.data.Datos.Conductor.Telefonos;
                                            $scope.Modelo.Celular = response.data.Datos.Conductor.Celular;
                                            sinAsignar = false;
                                        }
                                    } else if ($scope.Modelo.NumeroIdentificacion == response.data.Datos.Tenedor.NumeroIdentificacion) {
                                        if ($scope.perfilTerceros[i].Estado === true && $scope.perfilTerceros[i].Codigo === 1412) {
                                            if (response.data.Datos.Tenedor.TipoNaturaleza.Codigo !== 0 || response.data.Datos.Tenedor.TipoNaturaleza.Codigo !== undefined) {
                                                $scope.Modelo.TipoNaturaleza = $linq.Enumerable().From($scope.ListadoNaturalezas).First('$.Codigo ==' + response.data.Datos.Tenedor.TipoNaturaleza.Codigo);
                                                $scope.AsignarTipoIdentificacion();
                                                $scope.Modelo.TipoIdentificacion = $linq.Enumerable().From($scope.ListadoTipoIdentificacion).First('$.Codigo ==' + response.data.Datos.Tenedor.TipoIdentificacion.Codigo);
                                            }
                                            $scope.Modelo.DigitoChequeo = response.data.Datos.Tenedor.DigitoChequeo;
                                            $scope.Modelo.Nombre = response.data.Datos.Tenedor.Nombre;
                                            $scope.Modelo.PrimeroApellido = response.data.Datos.Tenedor.PrimeroApellido;
                                            $scope.Modelo.SegundoApellido = response.data.Datos.Tenedor.SegundoApellido;
                                            $scope.Modelo.CiudadExpedicionIdentificacion = $scope.CargarCiudad(response.data.Datos.Tenedor.CiudadExpedicionIdent.Codigo);
                                            $scope.Modelo.Ciudad = $scope.CargarCiudad(response.data.Datos.Tenedor.Ciudad.Codigo);
                                            $scope.Modelo.Direccion = response.data.Datos.Tenedor.Direccion;
                                            $scope.Telefono = response.data.Datos.Tenedor.Telefonos;
                                            $scope.Modelo.Celular = response.data.Datos.Tenedor.Celular;
                                            sinAsignar = false;
                                        }
                                    }
                                }
                                if (sinAsignar === true) {
                                    ShowError('Los perfiles seleccionados no corresponden al No. identificación ' + $scope.Modelo.NumeroIdentificacion + ' ni al No. autorización ingresado');
                                    $scope.Modelo.NumeroIdentificacion = '';
                                    $scope.Modelo.DigitoChequeo = '';
                                    $scope.Modelo.Nombre = '';
                                    $scope.Modelo.PrimeroApellido = '';
                                    $scope.Modelo.SegundoApellido = '';
                                    $scope.Modelo.CiudadExpedicionIdentificacion = '';
                                    $scope.Modelo.Ciudad = '';
                                    $scope.Modelo.Direccion = '';
                                    $scope.Telefono = '';
                                    $scope.Modelo.Celular = '';
                                } else {
                                    ShowSuccess('Información cargada correctamente, por favor selecionar los perfiles requeridos y diligenciar los campos obligatorios');
                                }
                            } else {
                                ShowError('El estudio de seguridad autorizado se encuentra anulado');
                            }
                            BloqueoPantalla.stop();
                        } else {
                            ShowError(response.data.MensajeOperacion);
                            BloqueoPantalla.stop();
                        }
                        BloqueoPantalla.stop();
                    }, function (response) {
                        ShowError(response.data.MensajeOperacion);
                        BloqueoPantalla.stop();
                    });
            }
            BloqueoPantalla.stop();
        };
        $scope.DatosRequeridosEstudio = function () {
            $scope.MensajesError = [];
            var continuar = true;
            var PerfilSeleccionado = 0;
            for (var i = 0; i < $scope.perfilTerceros.length; i++) {
                if ($scope.perfilTerceros[i].Estado) {
                    PerfilSeleccionado++;
                }
            }
            if (PerfilSeleccionado === 0) {
                $scope.MensajesError.push('Debe Seleccionar por lo menos un perfil');
                continuar = false;
            }
            if ($scope.Modelo.TipoNaturaleza === undefined || $scope.Modelo.TipoNaturaleza === '' || $scope.Modelo.TipoNaturaleza === null || $scope.Modelo.TipoNaturaleza.Codigo === 500) {
                $scope.MensajesError.push('Debe ingresar un tipo naturaleza');
                continuar = false;
            }
            if ($scope.Modelo.TipoIdentificacion === undefined || $scope.Modelo.TipoIdentificacion === '' || $scope.Modelo.TipoIdentificacion === null || $scope.Modelo.TipoIdentificacion.Codigo === 100) {
                $scope.MensajesError.push('Debe ingresar un tipo identificación');
                continuar = false;
            }
            if ($scope.Modelo.NumeroIdentificacion === undefined || $scope.Modelo.NumeroIdentificacion === '' || $scope.Modelo.NumeroIdentificacion === null || $scope.Modelo.NumeroIdentificacion === 0) {
                $scope.MensajesError.push('Debe ingresar el número de identificación');
                continuar = false;
            }
            if ($scope.NumeroEstudioSeguridad === undefined || $scope.NumeroEstudioSeguridad === '' || $scope.NumeroEstudioSeguridad === null || $scope.NumeroEstudioSeguridad === 0) {
                $scope.MensajesError.push('Debe ingresar el número de autorización del estudio de seguiridad');
                continuar = false;
            }
            return continuar;
        };

        /*Se cambia la zona de la ciudad*/
        $scope.CambiarZonasCiudad = function (ciudad) {
            if (ciudad !== '') {
                if (ciudad.Codigo !== undefined) {
                    $scope.ObjetoCiudad = ciudad;
                    $scope.BloquearZona = false;
                    CambiarZonaCiudadDestinatario();
                } else {
                    $scope.FiltroZona = '';
                    $scope.ListadoZonas = [];
                    $scope.BloquearZona = true;
                }
            } else {
                $scope.FiltroZona = '';
                $scope.ListadoZonas = [];
                $scope.BloquearZona = true;
            }
        };

        /*Cargar combo lista zonas ciudades*/
        function CambiarZonaCiudadDestinatario() {
            ZonasFactory.Consultar({ Ciudad: $scope.ObjetoCiudad, CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoZonas = [];
                        $scope.ListadoZonas.push({ Nombre: '(NO APLICA)', Codigo: 0 });
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.forEach(function (item) {
                                if (item.Codigo !== 0) {
                                    $scope.ListadoZonas.push(item);
                                }
                            });
                            $scope.FiltroZona = $scope.ListadoZonas[0];
                        } else {
                            $scope.FiltroZona = $scope.ListadoZonas[0];
                        }
                    }
                }), function (response) {
                };
        }

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        //--------------------------------------------------------------------------------Funciones paginación sitios-----------------------------------------------------------------        
        $scope.PrimerPagina = function () {
            if ($scope.paginaActual > 1) {
                if ($scope.ListaResultadoFiltroConsultaSitios.length > 0) {
                    $scope.paginaActual = 1;
                    $scope.ListaSitiosGrid = [];
                    var i = 0;
                    for (i = 0; i <= $scope.ListaResultadoFiltroConsultaSitios.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina) {
                            $scope.ListaSitiosGrid.push($scope.ListaResultadoFiltroConsultaSitios[i]);
                        }
                    }
                } else {
                    if ($scope.SitiosTerceroClienteTotal.length > 0) {
                        $scope.paginaActual = 1;
                        $scope.ListaSitiosGrid = [];
                        var i = 0;
                        for (i = 0; i <= $scope.SitiosTerceroClienteTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina) {
                                $scope.ListaSitiosGrid.push($scope.SitiosTerceroClienteTotal[i]);
                            }
                        }
                    }
                }
            }
        };
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.ListaSitiosGrid = [];
                $scope.PaginaAuxiliar = $scope.paginaActual;
                $scope.paginaActual += 1;
                if ($scope.ListaResultadoFiltroConsultaSitios.length > 0) {
                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.ListaResultadoFiltroConsultaSitios.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListaSitiosGrid.push($scope.ListaResultadoFiltroConsultaSitios[i]);
                        }
                    }
                } else {
                    if ($scope.SitiosTerceroClienteTotal.length > 0) {
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.PaginaAuxiliar); i <= $scope.SitiosTerceroClienteTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListaSitiosGrid.push($scope.SitiosTerceroClienteTotal[i]);
                            }
                        }
                    }
                }
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                if ($scope.ListaResultadoFiltroConsultaSitios.length > 0) {
                    $scope.ListaSitiosGrid = [];
                    $scope.paginaActual -= 1;
                    var i = 0;
                    for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.ListaResultadoFiltroConsultaSitios.length - 1; i++) {
                        if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                            $scope.ListaSitiosGrid.push($scope.ListaResultadoFiltroConsultaSitios[i]);
                        }
                    }
                } else {
                    if ($scope.SitiosTerceroClienteTotal.length > 0) {
                        $scope.ListaSitiosGrid = [];
                        $scope.paginaActual -= 1;
                        var i = 0;
                        for (i = ($scope.cantidadRegistrosPorPagina * $scope.paginaActual) - ($scope.cantidadRegistrosPorPagina); i <= $scope.SitiosTerceroClienteTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListaSitiosGrid.push($scope.SitiosTerceroClienteTotal[i]);
                            }
                        }
                    }
                }
            }
        };
        $scope.UltimaPagina = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                if ($scope.ListaResultadoFiltroConsultaSitios.length > 0) {
                    if ($scope.SitiosTerceroClienteTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListaSitiosGrid = [];
                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.ListaResultadoFiltroConsultaSitios.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListaSitiosGrid.push($scope.ListaResultadoFiltroConsultaSitios[i]);
                            }
                        }
                    }
                } else {
                    if ($scope.SitiosTerceroClienteTotal.length > 0) {
                        $scope.paginaActual = $scope.totalPaginas;
                        $scope.ListaSitiosGrid = [];
                        var i = 0;
                        for (i = ($scope.paginaActual * $scope.cantidadRegistrosPorPagina) - $scope.cantidadRegistrosPorPagina; i <= $scope.SitiosTerceroClienteTotal.length - 1; i++) {
                            if (i < $scope.cantidadRegistrosPorPagina * $scope.paginaActual) {
                                $scope.ListaSitiosGrid.push($scope.SitiosTerceroClienteTotal[i]);
                            }
                        }
                    }
                }
            }
        }

        ////-----------------------------------------------------------------------------------------------Funcion Filtrar recoleciones--------------------------------------------------------
        $scope.FiltrarSitios = function () {

            if ($scope.SitiosTerceroClienteTotal.length > 0) {
                if (($scope.FiltroCiudad !== '' && $scope.FiltroCiudad !== null && $scope.FiltroCiudad !== undefined)
                    || ($scope.FiltroSitioCliente !== undefined && $scope.FiltroSitioCliente !== '' && $scope.FiltroSitioCliente !== null)) {

                    $scope.ListaSitiosGrid = [];
                    var FiltroConsulta = '';

                    if ($scope.FiltroCiudad !== '' && $scope.FiltroCiudad !== null && $scope.FiltroCiudad !== undefined) {
                        FiltroConsulta += '$.Ciudad.Codigo == ' + $scope.FiltroCiudad.Codigo;
                    }
                    if ($scope.FiltroZona !== '' && $scope.FiltroZona !== null && $scope.FiltroZona !== undefined) {
                        if ($scope.FiltroZona.Codigo !== 0) {
                            FiltroConsulta += ' && $.Zona.Codigo == ' + $scope.FiltroZona.Codigo;
                        }
                    }
                    if ($scope.FiltroCiudad !== '' && $scope.FiltroCiudad !== null && $scope.FiltroCiudad !== undefined) {
                        if ($scope.FiltroSitioCliente !== '' && $scope.FiltroSitioCliente !== null && $scope.FiltroSitioCliente !== undefined) {
                            FiltroConsulta += ' && $.Codigo == ' + $scope.FiltroSitioCliente.Codigo;
                        }
                    } else {
                        if ($scope.FiltroSitioCliente !== '' && $scope.FiltroSitioCliente !== null && $scope.FiltroSitioCliente !== undefined) {
                            FiltroConsulta += '$.Codigo == ' + $scope.FiltroSitioCliente.Codigo;
                        }
                    }

                    $scope.ListaResultadoFiltroConsultaSitios = [];
                    $scope.ListaResultadoFiltroConsultaSitios = $linq.Enumerable().From($scope.SitiosTerceroClienteTotal).Where(FiltroConsulta).ToArray();

                    if ($scope.ListaResultadoFiltroConsultaSitios.length > 0) {
                        $scope.ListaSitiosGrid = [];
                        $scope.paginaActual = 1;
                        var i = 0;
                        for (i = 0; i <= $scope.ListaResultadoFiltroConsultaSitios.length - 1; i++) {
                            if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                                $scope.ListaSitiosGrid.push($scope.ListaResultadoFiltroConsultaSitios[i]);
                            }
                        }
                        if ($scope.ListaResultadoFiltroConsultaSitios.length > 0) {
                            $scope.totalRegistros = $scope.ListaResultadoFiltroConsultaSitios.length;
                            $scope.totalPaginas = Math.ceil($scope.ListaResultadoFiltroConsultaSitios.length / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        }
                    } else {
                        $scope.totalRegistros = 0;
                        $scope.totalPaginas = 0;
                        $scope.paginaActual = 1;
                        $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                        $scope.Buscando = false;
                    }
                } else {
                    $scope.LimpiarFiltroTarifas();
                }
            }
        };

        ////---------------------------------------------------------------------------------------------Limpiar filtro recolecciones----------------------------------------------------------
        $scope.LimpiarFiltroTarifas = function () {
            $scope.FiltroCiudad = '';
            $scope.FiltroZona = '';
            $scope.FiltroSitioCliente = '';
            $scope.ListadoZonas = [];
            $scope.BloquearZona = true;
            $scope.ListaResultadoFiltroConsultaSitios = [];

            if ($scope.SitiosTerceroClienteTotal.length > 0) {
                $scope.ListaSitiosGrid = [];
                var i = 0;
                for (i = 0; i <= $scope.SitiosTerceroClienteTotal.length - 1; i++) {
                    if (i <= $scope.cantidadRegistrosPorPagina - 1) {
                        $scope.ListaSitiosGrid.push($scope.SitiosTerceroClienteTotal[i]);
                    }
                }
                if ($scope.SitiosTerceroClienteTotal.length > 0) {
                    $scope.totalRegistros = $scope.SitiosTerceroClienteTotal.length;
                    $scope.totalPaginas = Math.ceil($scope.SitiosTerceroClienteTotal.length / $scope.cantidadRegistrosPorPagina);
                    $scope.Buscando = false;
                    $scope.ResultadoSinRegistros = '';
                } else {
                    $scope.totalRegistros = 0;
                    $scope.totalPaginas = 0;
                    $scope.paginaActual = 1;
                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                    $scope.Buscando = false;
                }
            }
        };

        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        $scope.ConfirmacionGuardar = function () {
            showModal('modalConfirmacionGuardar');
        };
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');

            if (DatosRequeridosTercero()) {
                if ($scope.Telefono2 !== undefined && $scope.Telefono2 !== '') {
                    if ($scope.Telefono !== undefined) {
                        $scope.Modelo.Telefonos = $scope.Telefono.toString() + ';' + $scope.Telefono2.toString()
                    } else {
                        $scope.Modelo.Telefonos = $scope.Telefono2.toString()
                    }
                } else {
                    if ($scope.Telefono !== undefined) {
                        $scope.Modelo.Telefonos = $scope.Telefono.toString()
                    }
                }

                if ($scope.SitiosTerceroClienteTotal.length > 0) {
                    $scope.Modelo.SitiosTerceroCliente = $scope.SitiosTerceroClienteTotal;
                }

                $scope.Modelo.CadenaPerfiles = ''
                for (var i = 0; i < $scope.perfilTerceros.length; i++) {
                    var Perfil = $scope.perfilTerceros[i]
                    if (Perfil.Estado) {
                        if ($scope.Modelo.CadenaPerfiles == undefined || $scope.Modelo.CadenaPerfiles == '') {
                            $scope.Modelo.CadenaPerfiles = Perfil.Codigo.toString() + ','
                        } else {
                            $scope.Modelo.CadenaPerfiles = $scope.Modelo.CadenaPerfiles.toString() + Perfil.Codigo.toString() + ','
                        }
                    }
                }
                if ($scope.Modelo.Conductor.Propio) {
                    $scope.Modelo.Conductor.Propio = 1
                }
                $scope.Modelo.ListadoFOTOS = $scope.ListadoFotosEnviar;
                BloqueoPantalla.start('Guardando...');
                TercerosFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > 0) {
                                ShowSuccess('Se guardó el tercero ' + $scope.Modelo.RazonSocial + $scope.Modelo.Nombre + ' ' + $scope.Modelo.PrimeroApellido);
                                location.href = '#!ConsultarTerceros/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.data.MensajeOperacion);
                                $scope.cargarLisatdoTerceros();
                            }
                        }
                        else {
                            ShowError(response.data.MensajeOperacion);
                            $scope.cargarLisatdoTerceros();
                        }
                        BloqueoPantalla.stop();
                    }, function (response) {
                        ShowError(response.statusText);
                        $scope.cargarLisatdoTerceros();
                        BloqueoPantalla.stop();
                    });
            }
        };

        //Funciones del listado sitios cargue y descargue--------------------------------------------------------------
        $scope.ValidarSitio = function (index) {
            var cont = 0;
            for (var i = 0; i < $scope.Modelo.SitiosTerceroCliente.length; i++) {
                if (i !== index) {
                    var item = $scope.Modelo.SitiosTerceroCliente[i]
                    if (item.Codigo == $scope.Modelo.SitiosTerceroCliente[index].SitioCliente.Codigo) {
                        cont++;
                        break;
                    }
                }
            }
            if (cont > 0) {
                ShowError('El sitio ya está asociada');
                $scope.Modelo.SitiosTerceroCliente[index].SitioCliente = undefined;
            }
        };

        //Funcion agrupar listados por evento--------------------------------------------------------------
        $scope.AgruparListado = function () {
            $scope.ListaCorreosAgrupado = []
            $scope.ListaCorreosAgrupado = AgruparListados($scope.ListaCorreos, 'EventoCorreo');
        };

        $scope.ListTitem = function (item) {
            if (item.items) {
                item.items = false;
            } else {
                item.items = true;
            }
        };

        /*-----------------------------------------------------------Funcion validar datos requeridos para poder guardar/modificar el tercero -------------------------------------------------------------------*/
        function DatosRequeridosTercero() {
            $scope.MensajesError = [];
            window.scrollTo(top, top);
            var modelo = $scope.Modelo
            var continuar = true;
            var listadoDirrecionesConfirmadas = []
            $scope.MensajesError = []

            if ($scope.ListadoDirecciones.length > 0) {
                for (var i = 0; i < $scope.ListadoDirecciones.length; i++) {
                    if ($scope.ListadoDirecciones[i].Direccion != undefined && $scope.ListadoDirecciones[i].Direccion != '') {
                        listadoDirrecionesConfirmadas.push($scope.ListadoDirecciones[i])
                    }
                }
                $scope.Modelo.Direcciones = listadoDirrecionesConfirmadas

            }
            if (modelo.TipoNaturaleza.Codigo == 500)  /*No aplica*/ {
                $scope.MensajesError.push('Debe seleccionar el tipo de naturaleza');
                continuar = false;
            }
            else if (modelo.TipoNaturaleza.Codigo == 501)  /*Natural*/ {
                if (modelo.TipoIdentificacion.Codigo == 100)  /*No aplica*/ {
                    $scope.MensajesError.push('Debe seleccionar el tipo de identificación');
                    continuar = false;
                }
                if (ValidarCampo(modelo.Nombre) == 1) {
                    $scope.MensajesError.push('Debe ingresar el nombre');
                    continuar = false;
                }
                if (ValidarCampo(modelo.PrimeroApellido) == 1) {
                    $scope.MensajesError.push('Debe ingresar el primer apellido');
                    continuar = false;
                }
                if (modelo.Sexo.Codigo == 600)  /*No aplica*/ {
                    $scope.MensajesError.push('Debe seleccionar el sexo');
                    continuar = false;
                }
                if (ValidarCampo(modelo.CiudadExpedicionIdentificacion) == 1) {
                    $scope.MensajesError.push('Debe ingresar la ciudad de expedición de la identificación');
                    continuar = false;
                } else if (ValidarCampo(modelo.CiudadExpedicionIdentificacion, undefined, true) == 2) {
                    $scope.MensajesError.push('Debe ingresar una ciudad de expedición de la identificación válida');
                    continuar = false;
                }
                if (ValidarCampo(modelo.CiudadNacimiento) == 1) {
                    $scope.MensajesError.push('Debe ingresar la ciudad de nacimiento');
                    continuar = false;
                } else if (ValidarCampo(modelo.CiudadNacimiento, undefined, true) == 2) {
                    $scope.MensajesError.push('Debe ingresar una ciudad de expedición nacimiento válida');
                    continuar = false;
                }
            }
            else if (modelo.TipoNaturaleza.Codigo == 502)  /*Juridica*/ {
                if (modelo.TipoIdentificacion.Codigo == 100)  /*No aplica*/ {
                    $scope.MensajesError.push('Debe seleccionar el tipo de identificación');
                    continuar = false;
                }
                if (modelo.DigitoChequeo == undefined || modelo.DigitoChequeo.toString() == '' || modelo.DigitoChequeo == null || isNaN(modelo.DigitoChequeo)) {
                    $scope.MensajesError.push('Debe ingresar el digito de chequeo');
                    continuar = false;
                } else if (ValidarCampo(modelo.NumeroIdentificacion) == 0) {
                    if (!$scope.ValidarDigitoChequeo(modelo.NumeroIdentificacion, modelo.DigitoChequeo)) {
                        $scope.MensajesError.push('Debe ingresar un digito de chequeo válido');
                        continuar = false;
                    }
                }
                if (ValidarCampo(modelo.RazonSocial) == 1) {
                    $scope.MensajesError.push('Debe ingresar la razón social');
                    continuar = false;
                }
                if (ValidarCampo(modelo.RepresentanteLegal) == 1) {
                    $scope.MensajesError.push('Debe ingresar el representante legal');
                    continuar = false;
                }
            }
            if (ValidarCampo(modelo.NumeroIdentificacion) == 1) {
                $scope.MensajesError.push('Debe ingresar el número de identificación');
                continuar = false;
            }
            else if (ValidarCampo(modelo.NumeroIdentificacion, 5) == 3) {
                $scope.MensajesError.push('El número de identificacion debe tener al menos 5 digitos');
                continuar = false;
            }
            if (ValidarCampo(modelo.Ciudad) == 1) {
                $scope.MensajesError.push('Debe ingresar la ciudad');
                continuar = false;
            } else if (ValidarCampo(modelo.Ciudad, undefined, true) == 2) {
                $scope.MensajesError.push('Debe ingresar una ciudad válida');
                continuar = false;
            }
            if (ValidarCampo(modelo.Direccion) == 1) {
                $scope.MensajesError.push('Debe ingresar la dirección');
                continuar = false;
            }
            if (ValidarCampo($scope.Telefono) == 1) {
                $scope.MensajesError.push('Debe ingresar el teléfono 1');
                continuar = false;
            }
            else if (ValidarCampo($scope.Telefono, 7) == 3) {
                $scope.MensajesError.push('El número de teléfono debe tener al menos 7 digitos');
                continuar = false;
            }
            if (ValidarCampo($scope.Telefono2) != 1) {
                if (ValidarCampo($scope.Telefono2, 7) == 3) {
                    $scope.MensajesError.push('El número de teléfono 2 debe tener al menos 7 digitos');
                    continuar = false;
                }
            }
            if ($scope.perfilTerceros[3].Estado != true) {
                if (ValidarCampo(modelo.Correo) == 1) {
                    $scope.MensajesError.push('Debe ingresar el e-mail');
                    continuar = false;
                }
            }

            /*Valida el Proveedor*/
            if ($scope.btnproveedor) {
                var Proveedor = modelo.Proveedor
                if (Proveedor !== null && Proveedor !== undefined && Proveedor !== '') {
                    //if ($scope.Modelo.Proveedor.Tarifario == undefined || $scope.Modelo.Proveedor.Tarifario == null || $scope.Modelo.Proveedor.Tarifario == '') {
                    //    $scope.MensajesError.push('Debe ingresar un tarifario de compra');
                    //    continuar = false;
                    //}
                    if ($scope.Modelo.Proveedor.FormaCobro == undefined || $scope.Modelo.Proveedor.FormaCobro == null || $scope.Modelo.Proveedor.FormaCobro == '') {
                        $scope.MensajesError.push('Debe seleccionar la forma de cobro');
                        continuar = false;
                    }
                    if (Proveedor.FormaCobro.Codigo == 8801) {//Credito
                        if (ValidarCampo(Proveedor.DiasPlazo) == 1) {
                            $scope.MensajesError.push('Debe ingresar los dias de plazo del proveedor');
                            continuar = false;
                        }
                    }
                }
            }
            $scope.Modelo.Documentos = [];
            var cont = 0
            for (var i = 0; i < $scope.perfilTerceros.length; i++) {
                if ($scope.perfilTerceros[i].Estado) {
                    cont++
                    /*Valida el cliente*/
                    if ($scope.perfilTerceros[i].Codigo == 1401) {
                        var Cliente = modelo.Cliente
                        if (ValidarCampo(Cliente.RepresentanteComercial) == 1) {
                            $scope.MensajesError.push('Debe ingresar el representante comercial');
                            continuar = false;
                        } else if (ValidarCampo(Cliente.RepresentanteComercial, undefined, true) == 2) {
                            $scope.MensajesError.push('Debe ingresar un representante comercial valido');
                            continuar = false;
                        } else {
                            $scope.Modelo.Cliente.RepresentanteComercial = { Codigo: Cliente.RepresentanteComercial.Codigo, NombreCompleto: Cliente.RepresentanteComercial.NombreCompleto }
                        }

                        if (Cliente.FormaPago.Codigo == 1503) {//Credito
                            if (ValidarCampo(Cliente.DiasPlazo) == 1) {
                                $scope.MensajesError.push('Debe ingresar los dias de plazo del cliente');
                                continuar = false;
                            }
                        }
                    }
                    /*Valida el Conductor*/
                    else if ($scope.perfilTerceros[i].Codigo == 1403) {
                        var Conductor = modelo.Conductor

                        if (ValidarCampo(Conductor.NumeroLicencia) == 1) {
                            $scope.MensajesError.push('Debe ingresar el número de licencia');
                            continuar = false;
                        }

                        if (ValidarFecha(Conductor.FechaVencimiento) == 1) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vencimiento');
                            continuar = false;
                        } else if (ValidarFecha(Conductor.FechaVencimiento, false, true) == 3) {
                            $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual');
                            continuar = false;
                        }
                        if (Conductor.CategoriaLicencia.Codigo == 1800) {
                            $scope.MensajesError.push('Debe seleccionar la categoria de la licencia');
                            continuar = false;
                        }
                        if (Conductor.TipoSangre.Codigo == 1900) {
                            $scope.MensajesError.push('Debe seleccionar el tipo de sangre');
                            continuar = false;
                        }
                        for (var f = 0; f < $scope.ListadoDocumentos.length; f++) {
                            if ($scope.ListadoDocumentos[f].Documento.Codigo == CODIGO_DOCUMENTO_TERCERO_CERTIFICACION_BANCARIA
                                || $scope.ListadoDocumentos[f].Documento.Codigo == CODIGO_DOCUMENTO_TERCERO_CARNE_DE_VACUNACION
                                || $scope.ListadoDocumentos[f].Documento.Codigo == CODIGO_DOCUMENTO_TERCERO_CURSO_MECANICA_BASICA
                                || $scope.ListadoDocumentos[f].Documento.Codigo == CODIGO_DOCUMENTO_TERCERO_CURSO_MANEJO_DEFENSIVO
                                || $scope.ListadoDocumentos[f].Documento.Codigo == CODIGO_DOCUMENTO_TERCERO_CURSO_MANEJO_DE_EXTINTORES
                                || $scope.ListadoDocumentos[f].Documento.Codigo == CODIGO_DOCUMENTO_TERCERO_CONTROL_DE_INCENDIOS
                                || $scope.ListadoDocumentos[f].Documento.Codigo == CODIGO_DOCUMENTO_TERCERO_CURSO_DE_PRIMEROS_AUXILIOS
                                || $scope.ListadoDocumentos[f].Documento.Codigo == CODIGO_DOCUMENTO_TERCERO_CURSO_MANEJO_SUSTANCIAS_PELIGROSAS) {

                                if ($scope.ListadoDocumentos[f].AplicaReferencia == 2) {
                                    if ($scope.ListadoDocumentos[f].Referencia == '' || $scope.ListadoDocumentos[f].Referencia == null || isNaN($scope.ListadoDocumentos[f].Referencia)) {
                                        $scope.MensajesError.push('Debe ingresar una referencia para ' + $scope.ListadoDocumentos[f].Documento.Nombre);
                                        continuar = false;
                                    }

                                }
                                if ($scope.ListadoDocumentos[f].Documento.Codigo != CODIGO_DOCUMENTO_TERCERO_CARNE_DE_VACUNACION) {
                                    if ($scope.ListadoDocumentos[f].AplicaFechaEmision == 2) {
                                        if ($scope.ListadoDocumentos[f].FechaEmision == '' || $scope.ListadoDocumentos[f].FechaEmision == null || isNaN($scope.ListadoDocumentos[f].FechaEmision)) {
                                            $scope.MensajesError.push('Debe ingresar una Fecha Emision para ' + $scope.ListadoDocumentos[f].Documento.Nombre);
                                            continuar = false;
                                        }

                                    }
                                    if ($scope.ListadoDocumentos[f].AplicaFechaVencimiento == 2) {
                                        if ($scope.ListadoDocumentos[f].FechaVence == '' || $scope.ListadoDocumentos[f].FechaVence == null || isNaN($scope.ListadoDocumentos[f].FechaVence)) {
                                            $scope.MensajesError.push('Debe ingresar una Feche Vence para ' + $scope.ListadoDocumentos[f].Documento.Nombre);
                                            continuar = false;
                                        }
                                    }
                                }

                            }
                            var documento = $scope.ListadoDocumentos[f]
                            if (documento.TipoArchivoDocumento.Codigo !== 4301) {

                                if (ValidarCampo(documento.Referencia) == 1) {
                                    if (documento.AplicaReferencia == CAMPO_DOCUMENTO_OBLIGATORIO) {
                                        $scope.MensajesError.push('Debe ingresar la referencia del documento (' + documento.Documento.Nombre + ')');
                                        continuar = false;
                                        DocumentoValido = false;
                                    }
                                }
                                if (ValidarCampo(documento.Emisor) == 1) {
                                    if (documento.AplicaEmisor == CAMPO_DOCUMENTO_OBLIGATORIO) {
                                        $scope.MensajesError.push('Debe ingresar el emisor del documento (' + documento.Documento.Nombre + ')');
                                        continuar = false;
                                        DocumentoValido = false;
                                    }
                                }
                                if (ValidarFecha(documento.FechaEmision) == 1) {
                                    if (documento.AplicaFechaEmision == CAMPO_DOCUMENTO_OBLIGATORIO) {
                                        $scope.MensajesError.push('Debe ingresar la fecha de emisión del documento  (' + documento.Documento.Nombre + ')');
                                        continuar = false;
                                        DocumentoValido = false;
                                    } else {
                                        documento.FechaEmision = ''
                                    }
                                } else {
                                    var f = new Date();
                                    if (documento.FechaEmision > f) {
                                        $scope.MensajesError.push('La fecha de emisión debe ser menor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                                        continuar = false;
                                        DocumentoValido = false;
                                    }
                                }
                                if (ValidarFecha(documento.FechaVence) == 1) {
                                    if (documento.AplicaFechaVencimiento == CAMPO_DOCUMENTO_OBLIGATORIO) {
                                        $scope.MensajesError.push('Debe ingresar la fecha de vencimiento del documento  (' + documento.Documento.Nombre + ')');
                                        continuar = false;
                                        DocumentoValido = false;
                                    } else {
                                        documento.FechaVence = ''
                                    }
                                } else {
                                    var f = new Date();
                                    if (documento.FechaVence < f) {
                                        $scope.MensajesError.push('La fecha de vencimiento debe ser mayor a la fecha actual del documento  (' + documento.Documento.Nombre + ')');
                                        continuar = false;
                                        DocumentoValido = false;
                                    }
                                }



                            }

                            //if (ValidarFecha(Conductor.FechaUltimoViaje) == 1) {
                            //    $scope.MensajesError.push('Debe ingresar la fecha del último viaje');
                            //    continuar = false;
                            //} else if (ValidarFecha(Conductor.FechaUltimoViaje, true) == 2) {
                            //    $scope.MensajesError.push('La fecha del último viaje debe ser menor a la fecha actual');
                            //    continuar = false;
                            //}
                        }
                    }
                    /*Valida el empleado*/
                    else if ($scope.perfilTerceros[i].Codigo == 1405) {
                        var Empleado = modelo.Empleado
                        if (ValidarFecha(Empleado.FechaVinculacion) == 1) {
                            $scope.MensajesError.push('Debe ingresar la fecha de vinculación');
                            continuar = false;
                        } else if (ValidarFecha(Empleado.FechaVinculacion, true) == 2) {
                            $scope.MensajesError.push('La fecha de vinculación debe ser menor a la fecha actual');
                            continuar = false;
                        }
                        if (ValidarFecha(Empleado.FechaFinalizacion) == 1) {

                        } else if (ValidarFecha(Empleado.FechaFinalizacion, false, true) == 3) {
                            $scope.MensajesError.push('La fecha de finalización debe ser mayor a la fecha actual');
                            continuar = false;
                        }
                        if (Empleado.Departamento.Codigo == 8700) {
                            $scope.MensajesError.push('Debe seleccionar el Departamento');
                            continuar = false;
                        }
                        if (Empleado.Cargo.Codigo == 2000) {
                            $scope.MensajesError.push('Debe seleccionar el cargo');
                            continuar = false;
                        }
                    }

                }
            }
            if (cont == 0) {
                $scope.MensajesError.push('Debe seleccionar al menos 1 perfil');
                continuar = false;
            }
            if (modelo.Banco.Codigo > 0 || modelo.TipoBanco.Codigo > 400 || (modelo.CuentaBancaria !== undefined && modelo.CuentaBancaria !== '' && modelo.CuentaBancaria !== 0 && modelo.CuentaBancaria !== '0') || (modelo.TitularCuentaBancaria !== undefined && modelo.TitularCuentaBancaria !== '')) {
                if (modelo.Banco.Codigo == 0) {
                    $scope.MensajesError.push('Debe ingresar el banco');
                    continuar = false;
                }
                //if (modelo.TipoBanco.Codigo == 400) {
                //    $scope.MensajesError.push('Debe ingresar el tipo de cuenta bancaria');
                //    continuar = false;
                //}
                //if (ValidarCampo(modelo.CuentaBancaria) == 1) {
                //    $scope.MensajesError.push('Debe ingresar la cuenta bancaria');
                //    continuar = false;
                //}
                //if (ValidarCampo(modelo.TitularCuentaBancaria) == 1) {
                //    $scope.MensajesError.push('Debe ingresar el titular de la cuenta');
                //    continuar = false;
                //}
            }
            if (modelo.Estado.Codigo == 0) {
                if (ValidarCampo(modelo.JustificacionBloqueo) == 1) {
                    $scope.MensajesError.push('Debe ingresar la justificación de la inactividad');
                    continuar = false;
                }
            }
            $scope.Modelo.Documentos = []
            ////Documentos
            for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                var Cont = 0
                var documento = $scope.ListadoDocumentos[i]


                $scope.Modelo.Documentos.push(
                    {
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        Terceros: true,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Configuracion: { Codigo: documento.Codigo },
                        Referencia: documento.Referencia,
                        Emisor: documento.Emisor,
                        FechaEmision: documento.FechaEmision,
                        FechaVence: documento.FechaVence,
                        EliminaDocumento: documento.ValorDocumento
                    }
                )
            }

            var x = 0;
            $scope.ListadoFotosEnviar = [];
            for (var x = 0; x < $scope.ListadoFotos.length; x++) {

                var foto = $scope.ListadoFotos[x];
                if (foto.Archivo != undefined) {


                    $scope.ListadoFotosEnviar.push(
                        {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            Terceros: true,
                            Numero: x,
                            Nombre: foto.ArchivoCargado.name,
                            Archivo: foto.Archivo,
                            Extension: foto.ExtensionDocumento,
                            Tipo: foto.ArchivoCargado.type
                        }
                    )
                }
            }


            //$scope.Modelo.Documentos.push(
            //    {
            //        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            //        Terceros: true,
            //        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            //        Configuracion: { Codigo: $scope.ItemFoto.Codigo },
            //        Referencia: $scope.ItemFoto.Referencia,
            //        Emisor: $scope.ItemFoto.Emisor,
            //        FechaEmision: $scope.ItemFoto.FechaEmision,
            //        FechaVence: $scope.ItemFoto.FechaVence,
            //        EliminaDocumento: $scope.ItemFoto.ValorDocumento
            //    }
            //)
            if ($scope.ListaCorreos.length > 0) {
                $scope.Modelo.ListadoCorreos = $scope.ListaCorreos;
            }
            return continuar;
        }

        function ValidarCampo(objeto, minlength, Esobjeto) {
            var resultado = 0
            if (objeto == undefined || objeto == '' || objeto == null || objeto == 0 || objeto == '0') {
                resultado = 1
            } else {
                if (resultado == 0) {
                    if (minlength !== undefined) {
                        if (objeto.length < minlength) {
                            resultado = 3
                        }
                        else {
                            resultado = 0
                        }
                    }
                } if (resultado == 0) {
                    if (Esobjeto) {
                        if (objeto.Codigo == undefined || objeto.Codigo == '' || objeto.Codigo == null || objeto.Codigo == 0) {
                            resultado = 2
                        }
                    }
                }
            }
            return resultado
        }
        function ValidarFecha(objeto, MayorActual, MenorActual) {
            var resultado = 0
            var now = new Date()
            if (objeto == undefined || objeto == '' || objeto == null) {
                resultado = 1
            } else {
                if (resultado == 0) {
                    if (MayorActual) {
                        if (objeto > now) {
                            resultado = 2
                        }
                    }
                    else if (MenorActual) {
                        if (objeto < now) {
                            resultado = 3
                        }
                    }
                }
            }
            return resultado
        }
        /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
        // Metodo para volver a la página de consulta
        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarTerceros';
        };



        // Metodo para verificar el perfil seleccionado
        var ConditionConuctor = false
        var ConditionEmpleado = false
        //Inicializacion tabs
        $scope.btncliente = false;
        $scope.btnempleado = false;
        $scope.btnconductor = false;
        $scope.btnproveedor = false;
        $scope.btnpasajero = false;

        //inicializacion de divisiones
        $('#infoBasica').show(); $('#Perfiles').hide(); $('#Localizacion').hide(); $('#cuentabanco').hide();
        $('#impuestos').hide(); $('#pasajero').hide(); $('#cliente').hide(); $('#conductor').hide(); $('#empleado').hide(); $('#listacorreos').hide();
        $('#seguridad').hide(); $('#foto').hide(); $('#estado').hide(); $('#Infovisible').hide(); $('#documentos').hide(); $('#proveedor').hide()

        $scope.MostrarInfobasica = function () {
            $('#infoBasica').show()
            $('#Infovisible').hide();
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
        }
        $scope.MostrarPerfiles = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').show()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
        }
        $scope.MostrarLocalizacion = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').show()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#documentos').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#proveedor').hide()
        }
        $scope.MostrarCuenta = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').show()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
        }
        $scope.MostrarImpuestos = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').show()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
        }
        $scope.MostrarPasajeros = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').show()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
        }
        $scope.MostrarCliente = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').show()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
        }
        $scope.MostrarConductor = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').show()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()

        }
        $scope.MostrarEmpleado = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').show()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
        }
        $scope.MostrarSeguridad = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').show()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
        }
        $scope.MostrarFoto = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').show()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
        }
        $scope.MostrarEstado = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').show()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').hide()
        }
        $scope.MostrarDocumentos = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').show()
            $('#proveedor').hide()

        }
        $scope.MostrarProveedor = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').hide()
            $('#documentos').hide()
            $('#proveedor').show()
        }
        $scope.MostrarListaCorreos = function () {
            $('#Infovisible').show();
            $('#infoBasica').hide()
            $('#Perfiles').hide()
            $('#Localizacion').hide()
            $('#cuentabanco').hide()
            $('#impuestos').hide()
            $('#pasajero').hide()
            $('#cliente').hide()
            $('#conductor').hide()
            $('#empleado').hide()
            $('#seguridad').hide()
            $('#foto').hide()
            $('#estado').hide()
            $('#listacorreos').show()
            $('#documentos').hide()
            $('#proveedor').hide()
        }


        $scope.AsignarNombre = function () {
            $scope.NombreCompleto = $scope.Modelo.RazonSocial + $scope.Modelo.Nombre + ' ' + $scope.Modelo.PrimeroApellido + ' ' + $scope.Modelo.SegundoApellido
        }
        $scope.CargarNombreBeneficiario = function (ModalIdentificacionBeneficiario) {
            if (ModalIdentificacionBeneficiario !== undefined && ModalIdentificacionBeneficiario !== null && ModalIdentificacionBeneficiario !== '') {

                filtrosBeneficiario = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: $scope.ModalIdentificacionBeneficiario,
                };

                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosBeneficiario).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ModalNombreBeneficiario = response.data.Datos.NombreCompleto
                            $scope.Beneficiario.Codigo = response.data.Datos.Codigo
                        }
                        else {
                            ShowError('No existe el tercero con identificación no.' + $scope.ModalIdentificacionBeneficiario + '. ' + response.data.MensajeOperacion);
                            $scope.ModalIdentificacionBeneficiario = ''
                            $scope.ModalNombreBeneficiario = ''
                        }
                    }, function (response) {
                        ShowError('No existe el tercero con identificación no.' + $scope.ModalIdentificacionBeneficiario + '. ' + response.data.MensajeOperacion);
                        $scope.ModalIdentificacionBeneficiario = ''
                        $scope.ModalNombreBeneficiario = ''
                    });
            }
        };

        $scope.verificarExistencia = function () {
            if ($scope.Modelo.NumeroIdentificacion !== undefined && $scope.Modelo.NumeroIdentificacion !== null && $scope.Modelo.NumeroIdentificacion !== '' && $scope.Modelo.NumeroIdentificacion !== 0) {

                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: $scope.Modelo.NumeroIdentificacion,
                };

                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                ShowError('El tercero con identificación no.' + $scope.Modelo.NumeroIdentificacion + ' ya existe con el nombre de ' + response.data.Datos.NombreCompleto);
                                $scope.Modelo.NumeroIdentificacion = "";
                            }
                        }
                    });
            }
        };
        $scope.ObtenerBeneficiario = function () {
            if ($scope.ModalIdentificacionBeneficiario !== undefined && $scope.ModalIdentificacionBeneficiario !== null && $scope.ModalIdentificacionBeneficiario !== '' && $scope.ModalIdentificacionBeneficiario !== 0) {
                filtrosTercero = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroIdentificacion: $scope.ModalIdentificacionBeneficiario,
                };
                blockUI.delay = 1000;
                TercerosFactory.Obtener(filtrosTercero).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.Codigo > 0) {
                                $scope.ModalNombreBeneficiario = response.data.Datos.NombreCompleto
                                $scope.Modelo.Beneficiario = { Codigo: response.data.Datos.Codigo }
                            } else {
                                $scope.ModalNombreBeneficiario = ''
                                $scope.ModalIdentificacionBeneficiario = "";
                                ShowError('No se encontro ningún beneficiario asociado al número de identificación ingresado')
                            }
                        }
                    });
            } else {
                $scope.ModalNombreBeneficiario = ''
                $scope.ModalIdentificacionBeneficiario = "";
                $scope.Modelo.Beneficiario = { Codigo: 0 }
            }
        };
        $scope.ValidarDigitoChequeo = function (numeroIdentificacion, digitoChequeo) {
            var valido = true;

            var digitoValidoChequeo = 0;
            var sumaDigito = 0;
            var j = 0;
            var tamano = 0;
            var strNumeroIdentificacion = numeroIdentificacion.toString();

            var peso = [3, 7, 13, 17, 19, 23, 29, 37, 41, 43, 47, 53, 59, 67, 71];

            if (strNumeroIdentificacion.length <= 15) {
                tamano = strNumeroIdentificacion.length;
                for (var i = strNumeroIdentificacion.length; i > 0; i--) {
                    tamano = tamano - 1;
                    Aux = strNumeroIdentificacion.substring(tamano, i);
                    sumaDigito = sumaDigito + (Aux * peso[j]);
                    j = j + 1;
                }

                digitoValidoChequeo = sumaDigito % 11;

                if (digitoValidoChequeo > 1) {
                    digitoValidoChequeo = 11 - digitoValidoChequeo;
                }

                if (digitoValidoChequeo == digitoChequeo) {
                    continuar = true;
                } else {
                    continuar = false;
                }

            } else {
                continuar = false;
            }

            return continuar;
        }
        var archivo


        $scope.Tercero = { codigo: $scope.Modelo.Codigo };

        Array.prototype.unique = function (a) {
            return function () { return this.filter(a) }
        }(function (a, b, c) {
            return c.indexOf(a, b + 1) < 0
        });

        $scope.CargarArchivo = function (element) {
            $scope.IdPosiction = parseInt(element.id, 10)
            var continuar = true;
            if (element.files.length > 0) {
                $scope.PDF = element
                $scope.TipoDocumento = element
                blockUI.start();
                if (angular.element(this.item)[0] == undefined) {
                    $scope.Documento = angular.element(this.ItemFoto)[0];
                } else {
                    angular.element(this.item)[0].Temporal = true
                    $scope.Documento = angular.element(this.item)[0];
                }
                if (element === undefined) {
                    ShowError("Archivo invalido, poar favor verifique el formato del archivo  /nFormatos permitidos:" + $scope.Documento.TipoArchivoDocumento.CampoAuxiliar2);
                    continuar = false;
                }
                if (continuar == true) {
                    if ($scope.Documento.TipoArchivoDocumento.Codigo !== 4300) {
                        var Formatos = $scope.Documento.TipoArchivoDocumento.CampoAuxiliar2.split(',')
                        var cont = 0
                        for (var i = 0; i < Formatos.length; i++) {
                            Formatos[i] = Formatos[i].replace(' ', '')
                            var Extencion = element.files[0].name.split('.')
                            //if (element.files[0].type == Formatos[i]) {
                            if ('.' + (Extencion[1].toUpperCase()) == Formatos[i].toUpperCase()) {
                                cont++
                            }


                        }
                        if (cont == 0) {
                            ShowError("Archivo invalido, poar favor verifique el formato del archivo  Formatos permitidos:" + $scope.Documento.TipoArchivoDocumento.CampoAuxiliar2);
                            continuar = false;
                        }
                    }
                    else if (element.files[0].size >= $scope.Documento.Size) //8 MB
                    {
                        ShowError("El archivo " + element.files[0].name + " supera los " + $scope.Documento.NombreTamano + ", intente con otro archivo", false);
                        continuar = false;
                    }
                }
                if (continuar == true) {
                    if ($scope.Documento.Archivo != undefined && $scope.Documento.Archivo != '' && $scope.Documento.Archivo != null) {
                        $scope.Documento.ArchivoTemporal = element.files[0]
                        showModal('modalConfirmacionRemplazarDocumento');
                    } else {
                        var reader = new FileReader();
                        $scope.Documento.ArchivoCargado = element.files[0]
                        reader.onload = $scope.AsignarArchivo;
                        reader.readAsDataURL(element.files[0]);
                    }
                }

                blockUI.stop();
            }
        }

        $scope.RemplazarDocumento = function () {
            var reader = new FileReader();
            $scope.Documento.ArchivoCargado = $scope.Documento.ArchivoTemporal
            reader.onload = $scope.AsignarArchivo;
            reader.readAsDataURL($scope.Documento.ArchivoCargado);
            closeModal('modalConfirmacionRemplazarDocumento');
        }


        $scope.AsignarArchivo = function (e) {
            $scope.$apply(function () {
                //imagenes, tienen un proceso aparte ya que se deben redimencionar para que no ocupen mucho espacio en la BD
                if ($scope.Documento.ArchivoCargado.type.includes("image")) {
                    var img = new Image()
                    img.src = e.target.result
                    $timeout(function () {
                        $('#Foto' + $scope.IdPosiction).show()
                        $('#FotoCargada' + $scope.IdPosiction).hide()
                        if (img.height > img.width) {
                            RedimencionarImagen('CanvasVertical', img, 699, 499)
                            $scope.Documento.Archivo = document.getElementById('CanvasVertical').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')

                        }
                        //la imagen es horizontal
                        else {
                            RedimencionarImagen('CanvasHorizontal', img, 499, 699)
                            $scope.Documento.Archivo = document.getElementById('CanvasHorizontal').toDataURL($scope.Documento.ArchivoCargado.type).replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')
                        }
                        $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type
                        var Extencion = $scope.Documento.ArchivoCargado.name.split('.')
                        $scope.Documento.NombreDocumento = Extencion[0]
                        $scope.Documento.ExtensionDocumento = Extencion[1]
                        if ($scope.Documento.TipoArchivoDocumento.Codigo == 4301) {
                            RedimencionarImagen('Foto' + $scope.IdPosiction, img, 200, 200)
                            $scope.ItemFoto.NombreDocumento = Extencion[0]
                            $scope.ItemFoto.ValorDocumento = 1
                            $scope.ItemFoto.temp = true
                        }
                        $scope.InsertarDocumento()
                    }, 100)
                }
                //Resto de archivos
                else {
                    $scope.Documento.Archivo = e.target.result.replace('data:' + $scope.Documento.ArchivoCargado.type + ';base64,', '')
                    $scope.Documento.Tipo = $scope.Documento.ArchivoCargado.type
                    var Extencion = $scope.Documento.ArchivoCargado.name.split('.')
                    $scope.Documento.NombreDocumento = Extencion[0]
                    $scope.Documento.ExtensionDocumento = Extencion[1]
                    $scope.InsertarDocumento()
                }
            });
        }

        $scope.InsertarDocumento = function () {
            $timeout(function () {
                // Guardar Archivo temporal
                var Documento = {};
                if ($scope.Documento.Codigo == 201) {
                    Documento = {
                        CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                        Configuracion: { Codigo: $scope.IdPosiction },
                        Archivo: $scope.Documento.Archivo,
                        Nombre: $scope.Documento.NombreDocumento,
                        Extension: $scope.Documento.ExtensionDocumento,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Tipo: $scope.Documento.Tipo
                    }
                } else {
                    Documento = {
                        CodigoEmpresa: $scope.Modelo.CodigoEmpresa,
                        Configuracion: { Codigo: $scope.Documento.Codigo },
                        Archivo: $scope.Documento.Archivo,
                        Nombre: $scope.Documento.NombreDocumento,
                        Extension: $scope.Documento.ExtensionDocumento,
                        UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                        Tipo: $scope.Documento.Tipo
                    }
                }


                DocumentosFactory.InsertarTemporal(Documento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            // Se guardó correctamente el pdf
                            if (response.data.Datos) {
                                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                                    var item = $scope.ListadoDocumentos[i]
                                    if (item.Codigo == $scope.Documento.Codigo) {
                                        item.ValorDocumento = 1
                                        item.temp = true
                                    }
                                }
                                ShowSuccess('Se cargó el documento adjunto "' + $scope.Documento.NombreDocumento + '"');
                                $scope.Documento = '';

                            }
                        } else {
                            ShowError(response.data.MensajeError);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }, 200)
        }

        $scope.ElimiarDocumento = function (EliminarDocumento) {
            $scope.EliminarDocumento = EliminarDocumento
            //showModal('modalConfirmacionEliminarDocumento');
            $scope.IdPosiction = $scope.EliminarDocumento.IDNew;
            $scope.BorrarDocumento();
        };

        $scope.BorrarDocumento = function () {
            //closeModal('modalConfirmacionEliminarDocumento');
            $scope.ItemFoto.NombreDocumento = ''
            if ($scope.EliminarDocumento.temp) {
                $scope.EliminarDocumento.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                DocumentosFactory.EliminarDocumento($scope.EliminarDocumento).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            $scope.ListadoDocumentos.forEach(function (item) {
                                if (item.Codigo == $scope.EliminarDocumento.Codigo) {
                                    item.ValorDocumento = 0
                                }
                            })
                            if ($scope.EliminarDocumento.TipoArchivoDocumento.Codigo == 4301) {
                                var img = new Image()
                                RedimencionarImagen('Foto' + $scope.IdPosiction, img, 200, 200)
                                $scope.ItemFoto.ValorDocumento = 0
                                $('#Foto' + $scope.IdPosiction).hide()
                                $scope.FotoCargada = ''
                                $('#FotoCargada' + $scope.IdPosiction).hide()
                            }
                        }
                        else {
                            MensajeError("Error", response.data.MensajeError, false);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            } else {
                for (var i = 0; i < $scope.ListadoDocumentos.length; i++) {
                    if ($scope.EliminarDocumento.Codigo == $scope.ListadoDocumentos[i].Codigo) {
                        $scope.ListadoDocumentos[i].ValorDocumento = 0
                    }
                }
                if ($scope.EliminarDocumento.TipoArchivoDocumento.Codigo == 4301) {
                    var img = new Image()
                    RedimencionarImagen('Foto' + $scope.IdPosiction, img, 200, 200)
                    $scope.ItemFoto.ValorDocumento = 0;
                    $scope.ItemFoto.NombreDocumento = '';
                    $('#Foto' + $scope.IdPosiction).hide()
                    $scope.FotoCargada = ''
                    $('#FotoCargada' + $scope.IdPosiction).hide()
                }

            }
        }

        $scope.urlASP = '';
        $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
        $scope.DesplegarArchivo = function (item) {
            if (item.temp) {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Terceros&Codigo=' + $scope.Sesion.UsuarioAutenticado.Codigo + '&CDGD_Codigo=' + item.Codigo + '&Temp=1');
            }
            else {
                window.open($scope.urlASP + '/DescargueDocumentos/DescargueDocumentos.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombDoc=Terceros&Codigo=' + $scope.Modelo.Codigo + '&CDGD_Codigo=' + item.Codigo + '&Temp=0');
            }


        };

        $scope.verificacionPerfiles = function () {
            $scope.ContPerfiltes = 0
            var prov = {}
            var tene = {}
            $scope.perfilTerceros.forEach(function (perfil) {

                if (perfil.Codigo == 1401)  /*Cliente*/ {
                    if (perfil.Estado == true) {
                        $scope.btncliente = true;
                    } else {
                        $scope.btncliente = false;
                    }
                }

                if (perfil.Codigo == 1405)  /*Empleado*/ {
                    if (perfil.Estado == true) {
                        $scope.btnempleado = true;
                        ConditionEmpleado = true;
                    } else {
                        $scope.btnempleado = false;
                        ConditionEmpleado = false;
                    }
                }
                if (perfil.Codigo == 1403) /*conductor*/ {
                    if (perfil.Estado == true) {
                        $scope.btnconductor = true;
                        ConditionConuctor = true;
                    }
                    else {
                        $scope.btnconductor = false;
                        ConditionConuctor = false;
                    }
                }
                if (perfil.Codigo == 1409)  /*Proveedor*/ {
                    prov = perfil
                }
                if (perfil.Codigo == 1412)  /*tenedor*/ {
                    tene = perfil

                }
                if (ConditionConuctor == true || ConditionEmpleado == true) {
                    $scope.btnseguridad = true;
                } else {
                    $scope.btnseguridad = false;
                }

                if (prov.Estado || tene.Estado) {
                    $scope.btnproveedor = true;
                    $scope.DivProveedor = true;
                } else {
                    $scope.btnproveedor = false;
                    $scope.DivProveedor = false;
                }
                if (perfil.Estado == true) {
                    $scope.ContPerfiltes++
                }
            }
            )

        };
        $scope.QuitarDireccion = function (index) {
            $scope.ListadoDirecciones.splice(index, 1)
        }
        $scope.AgregarDireccion = function () {
            try {
                if ($scope.ListadoDirecciones[$scope.ListadoDirecciones.length - 1].Direccion == '') {
                    ShowError('El registro anterior se encuentra vacio, para agregar un nuevo resgistro complete el anterior')
                } else {
                    if ($scope.ListadoDirecciones.length >= 10) {
                        ShowError('No se pueden agregar mas direcciones, el numero maximo de registros permitidos es 10')
                    } else {
                        $scope.ListadoDirecciones.push({ Direccion: '' })
                    }
                }
            } catch (e) {
                $scope.ListadoDirecciones.push({ Direccion: '' })
            }
        }
        $scope.AgregarImpuestoVenta = function () {
            var INT = 0
            if ($scope.Modelo.Cliente.Impuestos == undefined || $scope.Modelo.Cliente.Impuestos == '' || $scope.Modelo.Cliente.Impuestos == null) {
                $scope.Modelo.Cliente.Impuestos = []
                $scope.Modelo.Cliente.Impuestos.push($scope.ImpuestoVenta)
                $scope.ImpuestoVenta = ''
            }
            else {
                if ($scope.ImpuestoVenta == undefined || $scope.ImpuestoVenta == '' || $scope.ImpuestoVenta == null) {
                    ShowError('Debe seleccionar el impuesto')
                } else {
                    var cont = 0
                    for (var i = 0; i < $scope.Modelo.Cliente.Impuestos.length; i++) {
                        var item = $scope.Modelo.Cliente.Impuestos[i]
                        if (item.Codigo == $scope.ImpuestoVenta.Codigo) {
                            cont++
                            break;
                        }
                    }
                    if (cont > 0) {
                        ShowError('El impuesto ya se encuentra ingresado')
                    } else {
                        $scope.Modelo.Cliente.Impuestos.push($scope.ImpuestoVenta)
                    }
                }
                $scope.ImpuestoVenta = ''
            }
        };

        $scope.AgregarSitio = function () {
            if ($scope.SitiosTerceroCliente.length <= CERO) {
                $scope.SitiosTerceroCliente = [];
                $scope.SitiosTerceroCliente.push($scope.SitioCliente);
                $scope.SitioCliente = '';
            }
            else {
                if ($scope.SitioCliente == undefined || $scope.SitioCliente == '' || $scope.SitioCliente == null) {
                    ShowError('Debe seleccionar un sitio');
                } else {
                    var cont = 0;
                    for (var i = 0; i < $scope.SitiosTerceroCliente.length; i++) {
                        var item = $scope.SitiosTerceroCliente[i].Codigo;
                        if (item == $scope.SitioCliente.Codigo) {
                            cont++;
                            break;
                        }
                    }
                    if (cont > 0) {
                        ShowError('El sitio ingresado ya se encuentra ingresado');
                    } else {
                        $scope.SitiosTerceroClienteTotal.push($scope.SitioCliente);
                        $scope.ListaSitiosGrid.push($scope.SitioCliente);

                        if ($scope.SitiosTerceroClienteTotal.length > 0) {
                            $scope.totalRegistros = $scope.SitiosTerceroClienteTotal.length;
                            $scope.totalPaginas = Math.ceil($scope.SitiosTerceroClienteTotal.length / $scope.cantidadRegistrosPorPagina);
                            $scope.Buscando = false;
                            $scope.ResultadoSinRegistros = '';
                        } else {
                            $scope.totalRegistros = 0;
                            $scope.totalPaginas = 0;
                            $scope.paginaActual = 1;
                            $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                            $scope.Buscando = false;
                        }
                    }
                }
                $scope.SitioCliente = '';
            }
        };

        $scope.EliminarSitio = function (codigo, nombre) {
            showModal('modalConfirmacionEliminar');
            $scope.tempCodigo = codigo;
            $scope.tempNombre = nombre;
        };

        $scope.ConfirmarEliminar = function () {
            closeModal('modalConfirmacionEliminar');

            for (var i = 0; i < $scope.SitiosTerceroClienteTotal.length; i++) {
                var posicion = i;
                var item = $scope.SitiosTerceroClienteTotal[i].Codigo;
                if (item === $scope.tempCodigo) {
                    $scope.SitiosTerceroClienteTotal.splice(posicion, 1);
                }
            }

            for (var j = 0; j < $scope.ListaSitiosGrid.length; j++) {
                var posicion2 = j;
                var item2 = $scope.ListaSitiosGrid[j].Codigo;
                if (item2 === $scope.tempCodigo) {
                    $scope.ListaSitiosGrid.splice(posicion2, 1);
                }
            }

            if ($scope.SitiosTerceroClienteTotal.length > 0) {
                $scope.totalRegistros = $scope.SitiosTerceroClienteTotal.length;
                $scope.totalPaginas = Math.ceil($scope.SitiosTerceroClienteTotal.length / $scope.cantidadRegistrosPorPagina);
                $scope.Buscando = false;
                $scope.ResultadoSinRegistros = '';
            } else {
                $scope.totalRegistros = 0;
                $scope.totalPaginas = 0;
                $scope.paginaActual = 1;
                $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                $scope.Buscando = false;
            }
        };

        $scope.EliminarImpuestoVenta = function (index) {
            $scope.Modelo.Cliente.Impuestos.splice(index, 1);
        };
        $scope.AgregarImpuestoCompra = function () {
            if ($scope.Modelo.Proveedor.Impuestos == undefined || $scope.Modelo.Proveedor.Impuestos == '' || $scope.Modelo.Proveedor.Impuestos == null) {
                $scope.Modelo.Proveedor.Impuestos = []
                $scope.Modelo.Proveedor.Impuestos.push($scope.ImpuestoCompra)
                $scope.ImpuestoCompra = ''
            }
            else {
                if ($scope.ImpuestoCompra == undefined || $scope.ImpuestoCompra == '' || $scope.ImpuestoCompra == null) {
                    ShowError('Debe seleccionar el impuesto')
                } else {
                    var cont = 0
                    for (var i = 0; i < $scope.Modelo.Proveedor.Impuestos.length; i++) {
                        var item = $scope.Modelo.Proveedor.Impuestos[i]
                        if (item.Codigo == $scope.ImpuestoCompra.Codigo) {
                            cont++
                            break;
                        }
                    }
                    if (cont > 0) {
                        ShowError('El impuesto ya se encuentra ingresado')
                    } else {
                        $scope.Modelo.Proveedor.Impuestos.push($scope.ImpuestoCompra)
                    }
                }
                $scope.ImpuestoCompra = ''
            }
        }
        $scope.EliminarImpuestoCompra = function (index) {
            $scope.Modelo.Proveedor.Impuestos.splice(index, 1)
        }
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskMayus = function (option) {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskMinus = function () {
            try { $scope.Modelo.Correo = $scope.Modelo.Correo.toLowerCase() } catch (e) { }
        }
        $scope.MaskDireccion = function (option) {
            try { $scope.Modelo.Direccion = MascaraDireccion($scope.Modelo.Direccion) } catch (e) { }
        };
        $scope.MaskTelefono = function (option) {
            try { $scope.Modelo.Telefono = MascaraTelefono($scope.Modelo.Telefono) } catch (e) { }
            try { $scope.Modelo.Fax = MascaraTelefono($scope.Modelo.Fax) } catch (e) { }
        };
        $scope.MaskCelular = function (option) {
            try { $scope.Modelo.Celular = MascaraCelular($scope.Modelo.Celular) } catch (e) { }
        };
        $scope.MaskSelect = function (option, select) {
            try { MascaraNaturalezaTercero(option, select) } catch (e) { }
        };
        $scope.MaskValores = function () {
            MascaraValoresGeneral($scope);
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item);
        };

    }]);