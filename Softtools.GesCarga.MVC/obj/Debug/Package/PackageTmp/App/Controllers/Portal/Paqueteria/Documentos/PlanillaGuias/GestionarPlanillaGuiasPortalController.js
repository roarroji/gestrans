﻿SofttoolsApp.controller("GestionarPlanillasPaqueteriaPortalCtrl", ['$scope', '$routeParams', '$timeout', '$linq', 'blockUI', 'TarifarioComprasFactory', 'TarifaTransportesFactory', 'TipoTarifaTransportesFactory', 'RutasFactory', 'ValorCatalogosFactory', 'TercerosFactory', 'TipoLineaNegocioTransportesFactory', 'TarifaTransportesFactory', 'ProductoTransportadosFactory', 'CiudadesFactory', 'RemesasFactory', 'VehiculosFactory', 'PlanillaGuiasFactory', 'ImpuestosTipoDocumentosFactory', 'ImpuestosFactory', 'OficinasFactory', 'SemirremolquesFactory', 'ManifiestoFactory', 'RemesaGuiasFactory', 'ManifiestoFactory', 'blockUIConfig', 'EmpresasFactory',
    function ($scope, $routeParams, $timeout, $linq, blockUI, TarifarioComprasFactory, TarifaTransportesFactory, TipoTarifaTransportesFactory, RutasFactory, ValorCatalogosFactory, TercerosFactory, TipoLineaNegocioTransportesFactory, TarifaTransportesFactory, ProductoTransportadosFactory, CiudadesFactory, RemesasFactory, VehiculosFactory, PlanillaGuiasFactory, ImpuestosTipoDocumentosFactory, ImpuestosFactory, OficinasFactory, SemirremolquesFactory, ManifiestoFactory, RemesaGuiasFactory, ManifiestoFactory, blockUIConfig, EmpresasFactory) {
        console.clear()
        $scope.Titulo = 'GESTIONAR PLANILLA DESPACHOS';
        $scope.MapaSitio = [{ Nombre: 'Paqueteria' }, { Nombre: 'Planilla Despachos' }];
        $scope.PERMISO_INACTIVO = PERMISO_INACTIVO;
        $scope.PERMISO_ACTIVO = PERMISO_ACTIVO;
        try { $scope.ValidarPermisos = $linq.Enumerable().From($scope.ListadoMenu).First('$.Codigo ==' + OPCION_MENU_PLANILLA_DESPACHOS_PAQUETERIA); } catch (e) {
            ShowError('No tiene permiso para visualizar esta pagina')
            document.location.href = '#'
        }

        /*Se ejecuta la funcion validar permisos la cual retorna si los permisos estan habilitados o deshabilitados*/
        $scope.Permisos = ValidarPermisos($scope.ValidarPermisos);
        $scope.DeshabilitarConsulta = $scope.Permisos.DeshabilitarConsulta
        $scope.DeshabilitarEliminarAnular = $scope.Permisos.DeshabilitarEliminarAnular
        $scope.DeshabilitarImprimir = $scope.Permisos.DeshabilitarImprimir
        $scope.DeshabilitarActualizar = $scope.Permisos.DeshabilitarActualizar
        if ($scope.Sesion.UsuarioAutenticado.ManejoEngancheRemesasMasivoPaqueteria) {
            $scope.RemesaPadre = true
        }
        $scope.AplicaImpuestos = false

        $scope.Modelo = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Planilla: {
                Fecha: new Date(),
                FechaSalida: new Date(),
                DetalleTarifarioCompra: {},
            },
            TipoDocumento: 130,
            Remesa: { Cliente: { NombreCompleto: '' }, Ruta: {} }
        }
        $scope.Planilla = {}

        $scope.ListaAuxiliares = []
        $scope.Filtro = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CodigoOficina: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Estado: CERO,
            Remesa: {
                DetalleTarifarioVenta: {},
                Remitente: { Direccion: '' },
                Detalle: { Destinatario: { Direccion: '' }, }
            },
            EstadoRemesaPaqueteria: { Codigo: 6001 }

        }

        $scope.Filtro2 = {
            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            UsuarioModifica: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
            CodigoOficina: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo,
            Codigo: CERO,
            CodigoAlterno: CERO,
            Nombre: '',
            Estado: CERO,
            Remesa: {
                DetalleTarifarioVenta: {},
                Remitente: { Direccion: '' },
                Detalle: { Destinatario: { Direccion: '' }, }
            },
            EstadoRemesaPaqueteria: { Codigo: 6001 }

        }
        $scope.DetallesAuxiliares = []
        $scope.ListadoEstados = [
            { Nombre: 'DEFINITIVO', Codigo: 1 },
            { Nombre: 'BORRADOR', Codigo: CERO }
        ]

        $scope.ListaCiudades = []
        $scope.AutocompleteCiudades = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = CiudadesFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, ValorAutocomplete: value, Sync: true
                    })
                    $scope.ListaCiudades = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCiudades)
                }
            }
            return $scope.ListaCiudades
        }
        $scope.ListaCliente = [];
        $scope.AutocompleteCliente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListaCliente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaCliente)
                }
            }
            return $scope.ListaCliente
        }
        $scope.ListaRemitente = [];
        $scope.AutocompleteRemitente = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_REMITENTE, ValorAutocomplete: value, Sync: true })
                    $scope.ListaRemitente = ValidarListadoAutocomplete(Response.Datos, $scope.ListaRemitente)
                }
            }
            return $scope.ListaRemitente
        }
        $scope.ListadoConductores = [];
        $scope.AutocompleteConductor = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CONDUCTOR, ValorAutocomplete: value, Sync: true })
                    $scope.ListadoConductores = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoConductores)
                }
            }
            return $scope.ListadoConductores
        }
        //Funciones Autocomplete
        $scope.ListadoVehiculos = [];
        $scope.AutocompleteVehiculos = function (value) {
            if (value.length > 0) {
                if ((value.length % 3) == 0 || value.length == 2) {
                    /*Cargar Autocomplete de propietario*/
                    blockUIConfig.autoBlock = false;
                    var Response = VehiculosFactory.Consultar({
                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                        ValorAutocomplete: value,
                        Sync: true
                    })
                    $scope.ListadoVehiculos = ValidarListadoAutocomplete(Response.Datos, $scope.ListadoVehiculos)
                }
            }
            return $scope.ListadoVehiculos
        }
        //$scope.Modelo.Conductor = $scope.CargarTercero($scope.Modelo.Conductor.Codigo)
        $scope.CargarDatosFunciones = function () {
            try {
                if ($scope.Modelo.Planilla.Estado.Codigo >= CERO) {
                    $scope.Modelo.Planilla.Estado = $linq.Enumerable().From($scope.ListadoEstados).First('$.Codigo ==' + $scope.Modelo.Planilla.Estado.Codigo);
                }
            } catch (e) {
                $scope.Modelo.Planilla.Estado = $scope.ListadoEstados[CERO]
            }


            /*Cargar oficina de la ciudad*/
            OficinasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoOficina = response.data.Datos;
                        $scope.ModeloOficina = $scope.ListadoOficina[$scope.ListadoOficina.length - 1];
                        try {
                            $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficina).First('$.Codigo == ' + $scope.Modelo.Planilla.Oficina.Codigo);
                        } catch (e) {
                            try {
                                $scope.ModeloOficina = $linq.Enumerable().From($scope.ListadoOficina).First('$.Codigo == ' + $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo);
                            } catch (e) {
                                $scope.ModeloOficina = $scope.ListadoOficina[$scope.ListadoOficina.length - 1];
                            }
                        }

                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            /*Cargar el combo de ESTADO DE RESEMAS PAQUETERIA*/

            //VehiculosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
            //    then(function (response) {
            //        if (response.data.ProcesoExitoso === true) {
            //            $scope.ListadoVehiculos = []
            //            if (response.data.Datos.length > CERO) {
            //                $scope.ListadoVehiculos = response.data.Datos;
            //                try {
            //                    if ($scope.Modelo.Planilla.Vehiculo.Codigo > CERO) {
            //                        $scope.Modelo.Planilla.Vehiculo = $linq.Enumerable().From($scope.ListadoVehiculos).First('$.Codigo ==' + $scope.Modelo.Planilla.Vehiculo.Codigo);
            //                        $scope.ObtenerInformacionTenedor()
            //                    }
            //                } catch (e) {
            //                    $scope.Modelo.Planilla.Vehiculo = ''
            //                }
            //            }
            //            else {
            //                $scope.ListadoVehiculos = [];
            //            }
            //        }
            //    }, function (response) {
            //        ShowError(response.statusText);
            //    });
            // Semirremolques
            SemirremolquesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: { Codigo: 1 } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListaRemolque = response.data.Datos;
                            try {
                                if ($scope.Modelo.Planilla.Semirremolque.Codigo > CERO) {
                                    $scope.Modelo.Planilla.Semirremolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Codigo ==' + $scope.Modelo.Planilla.Semirremolque.Codigo);
                                }
                            } catch (e) {
                                $scope.Modelo.Planilla.Semirremolque = ''
                            }
                        }
                        else {
                            $scope.ListaRemolque = [];
                            $scope.ModeloRemolque = null;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            /*Cargar el combo de funcionarios*/
            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: ESTADO_ACTIVO, CadenaPerfiles: PERFIL_EMPLEADO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            response.data.Datos.push({ NombreCompleto: '', Codigo: 0 })
                            $scope.ListaFuncionario = response.data.Datos;
                            if ($scope.CodigoFuncionario !== undefined && $scope.CodigoFuncionario !== '' && $scope.CodigoFuncionario !== 0 && $scope.CodigoFuncionario !== null) {
                                if ($scope.CodigoFuncionario > 0) {
                                    $scope.ModeloFuncionario = $linq.Enumerable().From($scope.ListaFuncionario).First('$.Codigo ==' + $scope.CodigoFuncionario);
                                } else {
                                    $scope.ModeloFuncionario = $linq.Enumerable().From($scope.ListaFuncionario).First('$.Codigo ==0');
                                }
                            } else {
                                $scope.ModeloFuncionario = $linq.Enumerable().From($scope.ListaFuncionario).First('$.Codigo ==0');
                            }
                        }
                        else {
                            $scope.ListaFuncionario = [];
                            $scope.ModeloFuncionario = null;
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            //RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
            //    then(function (response) {
            //        if (response.data.ProcesoExitoso === true) {
            //            $scope.ListadoRutas = [];
            //            if (response.data.Datos.length > 0) {
            //                $scope.ListadoRutas = response.data.Datos
            //            }
            //            else {
            //                $scope.ListadoRutas = []
            //            }
            //        }
            //    }, function (response) {
            //    });
            $scope.ListadoRutas = RutasFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, Sync: true }).Datos

            /*Cargar el combo de tipo entrega*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_ENTREGA_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoEntregaRemesaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoEntregaRemesaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoEntregaRemesaPaqueteria = $scope.ListadoTipoEntregaRemesaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoEntregaRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo transporte*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_TRANSPORTE_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTransporteRemsaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoTransporteRemsaPaqueteria = $scope.ListadoTipoTransporteRemsaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoTransporteRemsaPaqueteria = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de tipo despacho*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_DESPACHO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoDespachoRemesaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoDespachoRemesaPaqueteria = $scope.ListadoTipoDespachoRemesaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoDespachoRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });

            /*Cargar el combo de tipo servicio*/
            ValorCatalogosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Catalogo: { Codigo: CODIGO_CATALOGO_TIPO_SERVICIO_REMESA_PAQUETERIA } }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoServicioRemesaPaqueteria = [];
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoTipoServicioRemesaPaqueteria = response.data.Datos;

                            $scope.Filtro.TipoServicioRemesaPaqueteria = $scope.ListadoTipoServicioRemesaPaqueteria[CERO]

                        }
                        else {
                            $scope.ListadoTipoServicioRemesaPaqueteria = []
                        }
                    }
                }, function (response) {
                });

            ProductoTransportadosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoProductoTransportados = []
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoProductoTransportados = response.data.Datos;
                        }
                        else {
                            $scope.ListadoProductoTransportados = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });

            /*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/



            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_CLIENTE }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoCliente = []
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoCliente = response.data.Datos;
                        }
                        else {
                            $scope.ListadoCliente = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            TercerosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, CadenaPerfiles: PERFIL_EMPLEADO }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoEmpleados = []
                        if (response.data.Datos.length > CERO) {
                            $scope.ListadoEmpleados = response.data.Datos;
                            $scope.DetallesAuxiliares = []
                            if ($scope.Modelo.Planilla.DetallesAuxiliares !== undefined && $scope.Modelo.Planilla.DetallesAuxiliares !== null) {
                                $scope.DetallesAuxiliares = $scope.Modelo.Planilla.DetallesAuxiliares
                                for (var i = 0; i < $scope.DetallesAuxiliares.length; i++) {
                                    $scope.DetallesAuxiliares[i].Funcionario = $linq.Enumerable().From($scope.ListadoEmpleados).First('$.Codigo == ' + $scope.DetallesAuxiliares[i].Funcionario.Codigo);
                                }
                            }
                        }
                        else {
                            $scope.ListadoEmpleados = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            CiudadesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoCiudades = response.data.Datos;
                        }
                        else {
                            $scope.ListadoCiudades = [];
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
            /*Cargar el combo de TipoLineaNegocioTransportes*/
            TipoLineaNegocioTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoLineaNegocioTransportes = [];
                        $scope.ListadoTipoLineaNegocioTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoLineaNegocioTransportesInicial = response.data.Datos;
                            for (var i = 0; i < $scope.ListadoTipoLineaNegocioTransportesInicial.length; i++) {
                                if ($scope.ListadoTipoLineaNegocioTransportesInicial[i].LineaNegocioTransporte !== undefined && $scope.ListadoTipoLineaNegocioTransportesInicial[i].LineaNegocioTransporte !== null) {
                                    if ($scope.ListadoTipoLineaNegocioTransportesInicial[i].LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANPSORTE_CARGA_PAQUETERIA) {
                                        $scope.ListadoTipoLineaNegocioTransportes.push($scope.ListadoTipoLineaNegocioTransportesInicial[i])
                                    }
                                }
                            }
                            $scope.ListadoTipoLineaNegocioTransportesInicial = angular.copy($scope.ListadoTipoLineaNegocioTransportes)
                            $scope.ListadoTipoLineaNegocioTransportes = []
                        }
                        else {
                            $scope.ListadoTipoLineaNegocioTransportes = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de TarifaTransportes*/
            TarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTarifaTransportes = [];
                        $scope.ListadoTarifaTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTarifaTransportesInicial = response.data.Datos;
                        }
                        else {
                            $scope.ListadoTarifaTransportesInicial = []
                        }
                    }
                }, function (response) {
                });
            /*Cargar el combo de TipoTarifaTransportes*/
            TipoTarifaTransportesFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1 }).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.ListadoTipoTarifaTransportes = [];
                        $scope.ListadoTipoTarifaTransportesInicial = [];
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoTipoTarifaTransportesInicial = response.data.Datos;
                        }
                        else {
                            $scope.ListadoTipoTarifaTransportesInicial = []
                        }
                    }
                }, function (response) {
                });

        }
        $scope.CargarImpuestos = function (Ruta) {
            if (Ruta.Codigo > 0 && Ruta !== undefined && Ruta !== null && Ruta !== "") {
                if (Ruta.Codigo > 0) {
                    /*Cargar los impuestos*/
                    ImpuestosFactory.Consultar({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Estado: 1, CodigoTipoDocumento: 130, CodigoCiudad: Ruta.CiudadOrigen.Codigo, AplicaTipoDocumento: 1 }).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.ListadoImpuestos = []
                                $scope.ListadoImpuestosFiltrado = []
                                if (response.data.Datos.length > CERO) {
                                    $scope.ListadoImpuestos = response.data.Datos;
                                    try {
                                        if ($scope.Modelo.Planilla.DetalleImpuesto !== undefined && $scope.Modelo.Planilla.DetalleImpuesto !== null && $scope.Modelo.Planilla.DetalleImpuesto !== []) {
                                            $scope.ListadoImpuestosFiltrado = $scope.Modelo.Planilla.DetalleImpuesto
                                        }
                                        else {
                                            $scope.ListadoImpuestosFiltrado = []
                                            if ($scope.ListadoImpuestos.length > 0) {
                                                for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                                                    var impuesto = {
                                                        Nombre: $scope.ListadoImpuestos[i].Nombre,
                                                        CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                                                        ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                                                        ValorBase: $scope.ListadoImpuestos[i].valor_base,
                                                        ValorImpuesto: 0
                                                    }
                                                    $scope.ListadoImpuestosFiltrado.push(impuesto)
                                                }
                                            }
                                        }
                                    } catch (e) {
                                        $scope.ListadoImpuestosFiltrado = []
                                        if ($scope.ListadoImpuestos.length > 0) {
                                            for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                                                var impuesto = {
                                                    Nombre: $scope.ListadoImpuestos[i].Nombre,
                                                    CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                                                    ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                                                    ValorBase: $scope.ListadoImpuestos[i].valor_base,
                                                    ValorImpuesto: 0
                                                }
                                                $scope.ListadoImpuestosFiltrado.push(impuesto)
                                                //$scope.Calcular()
                                            }
                                        }
                                    }


                                }
                                else {
                                    $scope.ListadoImpuestos = [];
                                    $scope.ListadoImpuestosFiltrado = [];
                                }
                            }
                            try {
                                $scope.Calcular()

                            } catch (e) {

                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
        }

        $scope.FiltrarTarifas = function (Ruta) {
            if (Ruta.Codigo > 0 && Ruta !== undefined && Ruta !== null && Ruta !== "") {
                if (Ruta.Codigo > 0) {




                    $scope.ListaTarifas = [];
                    $scope.ListaTarifas = [];
                    $scope.ListadoTipoTarifas = [];
                    for (var i = 0; i < $scope.ListadoTarifas.length; i++) {
                        var item = $scope.ListadoTarifas[i]
                        if (item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_MASIVO || item.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == CODIGO_LINEA_NEGOCIO_TRANSPORTE_SEMIMASIVO) {
                            if (Ruta.Codigo == item.Ruta.Codigo) {
                                //if (item.TipoLineaNegocioTransportes.Codigo == TIPO_LINEA_NEGOCIO_CARGA_MASIVO_NACIONAL || item.TipoLineaNegocioTransportes.Codigo == TIPO_LINEA_NEGOCIO_CARGA_SEMIMASIVO_NACIONAL) {
                                if ($scope.ListaTarifas.length == 0) {
                                    $scope.ListaTarifas.push(item.TipoTarifaTransportes.TarifaTransporte)
                                }
                                else {
                                    var cont = 0
                                    for (var j = 0; j < $scope.ListaTarifas.length; j++) {
                                        if ($scope.ListaTarifas[j].Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                                            cont++
                                        }
                                    }
                                    if (cont == 0) {
                                        $scope.ListaTarifas.push(item.TipoTarifaTransportes.TarifaTransporte)
                                    }
                                }
                                item.Codigo = item.TipoTarifaTransportes.Codigo
                                $scope.ListadoTipoTarifas.push(item)
                                //}
                            }
                        }
                    }
                    //$scope.Modelo.Planilla.TarifarioCompra.Tarifas.forEach(function (item) {

                    //});
                    if ($scope.ListaTarifas.length == 0 && $scope.ListadoTipoTarifas == 0) {
                        ShowError('La ruta ingresada no se encuentra asociada al tarifario de compras del tenedor ' + $scope.Modelo.Planilla.Vehiculo.Tenedor.NombreCompleto);
                    } else {
                        try {
                            $scope.Modelo.Planilla.TarifaTransportes = $linq.Enumerable().From($scope.ListaTarifas).First('$.Codigo ==' + $scope.Modelo.Planilla.TarifaTransportes.Codigo);
                            $scope.FiltrarTipoTarifa()
                        } catch (e) {
                            try {
                                $scope.Modelo.Planilla.TarifaTransportes = $scope.ListaTarifas[0]
                                $scope.FiltrarTipoTarifa()
                            } catch (e) {

                            }

                        }
                    }
                }
            }
        }
        $scope.FiltrarTipoTarifa = function () {
            $scope.ListadoTipoTarifa = []
            for (var i = 0; i < $scope.ListadoTipoTarifas.length; i++) {
                var item = $scope.ListadoTipoTarifas[i]
                if ($scope.Modelo.Planilla.TarifaTransportes.Codigo == item.TipoTarifaTransportes.TarifaTransporte.Codigo) {
                    $scope.ListadoTipoTarifa.push(item)
                }
            }
            try {
                $scope.TipoTarifaTransportes = $linq.Enumerable().From($scope.ListadoTipoTarifa).First('$.Codigo ==' + $scope.Modelo.Planilla.TipoTarifaTransportes.Codigo);
                $scope.Modelo.Planilla.TipoTarifaTransportes = undefined
            } catch (e) {
                $scope.TipoTarifaTransportes = $scope.ListadoTipoTarifa[0]
            }
            try {
                $scope.Calcular()
            } catch (e) {

            }
        }
        /*Consultar Tarifario Tenedor*/
        $scope.ObtenerInformacionTenedor = function () {
            try {
                $scope.Modelo.Planilla.Semirremolque = $linq.Enumerable().From($scope.ListaRemolque).First('$.Codigo ==' + $scope.Modelo.Planilla.Vehiculo.Semirremolque.Codigo);

            } catch (e) {

            }
            if ($scope.Sesion.UsuarioAutenticado.ManejoPorcentajeAfiliado) {
                $scope.LustadoRutasFiltrado = []
                for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                    $scope.LustadoRutasFiltrado.push($scope.ListadoRutas[i])
                }
                try {
                    $scope.Modelo.Planilla.Peso = $scope.pesotemp
                    $scope.Modelo.Planilla.Ruta = $linq.Enumerable().From($scope.LustadoRutasFiltrado).First('$.Codigo ==' + $scope.Modelo.Planilla.Ruta.Codigo);
                    $scope.CargarImpuestos($scope.Modelo.Planilla.Ruta)
                } catch (e) {

                }
            } else {
                if ($scope.Modelo.Planilla.Vehiculo.Tenedor !== undefined) {
                    if ($scope.Modelo.Planilla.Vehiculo.Tenedor.Codigo > 0) {
                        $scope.Modelo.Planilla.Vehiculo.Tenedor.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                        TercerosFactory.Obtener($scope.Modelo.Planilla.Vehiculo.Tenedor).
                            then(function (response) {
                                if (response.data.ProcesoExitoso === true) {
                                    if (response.data.Datos.Codigo > 0) {
                                        if (response.data.Datos.Proveedor != undefined) {
                                            if (response.data.Datos.Proveedor.Tarifario !== undefined) {
                                                if (response.data.Datos.Proveedor.Tarifario.Codigo > 0) {
                                                    $scope.Planilla.TarifarioCompra = response.data.Datos.Proveedor.Tarifario
                                                    $scope.ObtenerInformacionTarifario()
                                                    $scope.Modelo.Planilla.Vehiculo.Tenedor = { NombreCompleto: response.data.Datos.NombreCompleto, Codigo: response.data.Datos.Codigo }
                                                }
                                                else {
                                                    ShowError('El tenedor del vehículo seleccionado no posee un tarifario de compra asignado');
                                                    $scope.Planilla.TarifarioCompra = {}
                                                }
                                            }
                                            else {
                                                ShowError('Error al consultar la información del tarifario');
                                                $scope.Planilla.TarifarioCompra = {}
                                            }
                                        }
                                        else {
                                            ShowError('Error al consultar la información del tenedor, por favor verifique la parametrización del tercero');
                                            $scope.Planilla.TarifarioCompra = {}
                                        }
                                    } else {
                                        ShowError('Error al consultar la información del tenedor, por favor verifique la parametrización del tercero');
                                        $scope.Planilla.TarifarioCompra = {}
                                    }
                                }
                            }, function (response) {
                                ShowError(response.statusText);
                            });
                    }
                }
            }
        }
        $scope.ObtenerInformacionTarifario = function () {
            $scope.TarifarioValido = false
            $scope.ListadoTarifaTransportes = []
            $scope.ListadoTipoTarifaTransportes = []
            $scope.Planilla.DetalleTarifarioCompra = { TipoLineaNegocioTransportes: '' }
            if ($scope.Planilla.TarifarioCompra !== undefined) {
                if ($scope.Planilla.TarifarioCompra.Codigo > 0) {
                    $scope.Planilla.TarifarioCompra.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                    TarifarioComprasFactory.Obtener($scope.Planilla.TarifarioCompra).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Planilla.TarifarioCompra = response.data.Datos
                                $scope.ListadoTarifas = []
                                $scope.LustadoRutasFiltrado = []
                                for (var i = 0; i < $scope.Planilla.TarifarioCompra.Tarifas.length; i++) {
                                    if ($scope.Planilla.TarifarioCompra.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == 1 || $scope.Planilla.TarifarioCompra.Tarifas[i].TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo == 2) {
                                        $scope.ListadoTarifas.push($scope.Planilla.TarifarioCompra.Tarifas[i])
                                    }
                                }
                                for (var i = 0; i < $scope.ListadoRutas.length; i++) {
                                    var aplica = 0
                                    for (var j = 0; j < $scope.ListadoTarifas.length; j++) {
                                        if ($scope.ListadoRutas[i].Codigo == $scope.ListadoTarifas[j].Ruta.Codigo) {
                                            aplica++
                                        }
                                    }
                                    if (aplica > 0) {
                                        $scope.LustadoRutasFiltrado.push($scope.ListadoRutas[i])
                                    }
                                }
                                try {
                                    if ($scope.Modelo.Planilla.Ruta.Codigo > CERO) {
                                        $scope.Modelo.Planilla.Ruta = $linq.Enumerable().From($scope.LustadoRutasFiltrado).First('$.Codigo ==' + $scope.Modelo.Planilla.Ruta.Codigo);
                                        $scope.CargarImpuestos($scope.Modelo.Planilla.Ruta)
                                        $scope.FiltrarTarifas($scope.Modelo.Planilla.Ruta)
                                    }
                                } catch (e) {
                                    $scope.Modelo.Planilla.Ruta = ''
                                }
                            }
                            else {
                                ShowError('No se logro consultar el tarifario compras');
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });

                    blockUI.stop();
                }
            }
        }


        //Calcular valores
        $scope.Calcular = function () {
            $scope.Modelo.Planilla.Cantidad = 0
            $scope.Modelo.Planilla.Peso = 0
            $scope.Modelo.Planilla.ValorRetencionFuente = 0
            $scope.Modelo.Planilla.ValorRetencionICA = 0
            $scope.Modelo.Planilla.ValorFleteCliente = 0
            $scope.Modelo.Planilla.ValorSeguroMercancia = 0
            $scope.Modelo.Planilla.ValorOtrosCobros = 0
            $scope.Modelo.Planilla.ValorTotalCredito = 0
            $scope.Modelo.Planilla.ValorTotalContado = 0
            $scope.Modelo.Planilla.ValorTotalAlcobro = 0
            $scope.Modelo.Planilla.ValorFleteTransportador = 0
            $scope.Contado = { Cantidad: 0, FormaPago: 'Contado', ValorCliente: 0, ValorTransportador: 0 }
            $scope.Credito = { Cantidad: 0, FormaPago: 'Crédito', ValorCliente: 0, ValorTransportador: 0 }
            $scope.ContraEntrega = { Cantidad: 0, FormaPago: 'Contra Entrega	', ValorCliente: 0, ValorTransportador: 0 }
            $scope.ListadoTotalesRemesas = []
            try {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    var item = $scope.ListadoRemesasFiltradas[i]
                    if (item.Seleccionado) {
                        if (item.Remesa.FormaPago.Codigo == 4901) {
                            $scope.Credito.Cantidad += 1
                            $scope.Credito.ValorCliente += item.Remesa.TotalFleteCliente
                            $scope.Credito.ValorTransportador += item.Remesa.ValorFleteTransportador
                        }
                        if (item.Remesa.FormaPago.Codigo == 4902) {
                            $scope.Contado.Cantidad += 1
                            $scope.Contado.ValorCliente += item.Remesa.TotalFleteCliente
                            $scope.Contado.ValorTransportador += item.Remesa.ValorFleteTransportador
                        }
                        if (item.Remesa.FormaPago.Codigo == 4903) {
                            $scope.ContraEntrega.Cantidad += 1
                            $scope.ContraEntrega.ValorCliente += item.Remesa.TotalFleteCliente
                            $scope.ContraEntrega.ValorTransportador += item.Remesa.ValorFleteTransportador
                        }
                        $scope.Modelo.Planilla.Cantidad += item.Remesa.CantidadCliente
                        $scope.Modelo.Planilla.Peso += item.Remesa.PesoCliente

                    }
                }
            } catch (e) {

            }

            if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== []) {
                for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                    var item = $scope.ListadoGuiaGuardadas[i]
                    if (item.Remesa.FormaPago.Codigo == 4901) {
                        $scope.Credito.Cantidad += 1
                        $scope.Credito.ValorCliente += item.Remesa.TotalFleteCliente
                        $scope.Credito.ValorTransportador += item.Remesa.ValorFleteTransportador
                    }
                    if (item.Remesa.FormaPago.Codigo == 4902) {
                        $scope.Contado.Cantidad += 1
                        $scope.Contado.ValorCliente += item.Remesa.TotalFleteCliente
                        $scope.Contado.ValorTransportador += item.Remesa.ValorFleteTransportador
                    }
                    if (item.Remesa.FormaPago.Codigo == 4903) {
                        $scope.ContraEntrega.Cantidad += 1
                        $scope.ContraEntrega.ValorCliente += item.Remesa.TotalFleteCliente
                        $scope.ContraEntrega.ValorTransportador += item.Remesa.ValorFleteTransportador
                    }
                    $scope.Modelo.Planilla.Cantidad += item.Remesa.CantidadCliente
                    $scope.Modelo.Planilla.Peso += item.Remesa.PesoCliente
                }
            }


            $scope.ListadoTotalesRemesas.push($scope.Credito)
            $scope.ListadoTotalesRemesas.push($scope.Contado)
            $scope.ListadoTotalesRemesas.push($scope.ContraEntrega)
            $scope.Modelo.Planilla.ValorFleteTransportador = 0
            if ($scope.Sesion.UsuarioAutenticado.ManejoPorcentajeAfiliado) {
                try {
                    for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                        var item = $scope.ListadoRemesasFiltradas[i]
                        if (item.Seleccionado) {
                            $scope.Modelo.Planilla.ValorFleteTransportador += item.Remesa.ValorFleteTransportador
                        }
                    }
                } catch (e) {
                }
                if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== []) {
                    for (var i = 0; i < $scope.ListadoGuiaGuardadas.length; i++) {
                        var item = $scope.ListadoGuiaGuardadas[i]
                        $scope.Modelo.Planilla.ValorFleteTransportador += item.Remesa.ValorFleteTransportador
                    }
                }
            }
            else {
                try {
                    $scope.Modelo.Planilla.ValorFleteTransportador = $scope.TipoTarifaTransportes.ValorFlete

                } catch (e) {

                }
            }


            if ($scope.Modelo.Planilla.ValorAnticipo !== undefined && $scope.Modelo.Planilla.ValorAnticipo !== '') {
                if ($scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorAnticipo) > $scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorFleteTransportador)) {
                    ShowError('El valor del anticipo no puede ser mayor al valor del flete del transportador')
                    $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid(Math.round($scope.Modelo.Planilla.ValorFleteTransportador))
                } else {
                    $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid($scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorFleteTransportador) - $scope.MaskNumeroGrid($scope.Modelo.Planilla.ValorAnticipo))
                }
            } else {
                $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid(Math.round($scope.Modelo.Planilla.ValorFleteTransportador))
            }
            try {
                $scope.ListadoImpuestosFiltrado = []
                if ($scope.ListadoImpuestos.length > 0) {
                    for (var i = 0; i < $scope.ListadoImpuestos.length; i++) {
                        var impuesto = {
                            Nombre: $scope.ListadoImpuestos[i].Nombre,
                            CodigoImpuesto: $scope.ListadoImpuestos[i].Codigo,
                            ValorTarifa: $scope.ListadoImpuestos[i].Valor_tarifa,
                            ValorBase: $scope.ListadoImpuestos[i].valor_base,
                        }
                        if ($scope.ListadoImpuestos[i].valor_base < parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))) {
                            impuesto.ValorBase = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))
                        }

                        impuesto.ValorImpuesto = impuesto.ValorTarifa * impuesto.ValorBase
                        $scope.ListadoImpuestosFiltrado.push(impuesto)
                    }
                }
            } catch (e) {

            }
            try {
                $scope.TotalImpuestos = 0
                if ($scope.ListadoImpuestosFiltrado.length > 0) {
                    for (var i = 0; i < $scope.ListadoImpuestosFiltrado.length; i++) {
                        $scope.TotalImpuestos += parseFloat($scope.ListadoImpuestosFiltrado[i].ValorImpuesto)
                    }
                }
                $scope.Modelo.Planilla.ValorPagarTransportador = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorPagarTransportador)) - $scope.TotalImpuestos
            } catch (e) {

            }

            $scope.Modelo.Planilla.ValorRetencionFuente = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorRetencionFuente)
            $scope.Modelo.Planilla.ValorRetencionICA = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorRetencionICA)
            $scope.Modelo.Planilla.ValorFleteCliente = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorFleteCliente)
            $scope.Modelo.Planilla.ValorSeguroMercancia = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorSeguroMercancia)
            $scope.Modelo.Planilla.ValorOtrosCobros = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorOtrosCobros)
            $scope.Modelo.Planilla.ValorTotalCredito = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorTotalCredito)
            $scope.Modelo.Planilla.ValorTotalContado = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorTotalContado)
            $scope.Modelo.Planilla.ValorTotalAlcobro = $scope.MaskValoresGrid($scope.Modelo.Planilla.ValorTotalAlcobro)
            $scope.Modelo.Planilla.ValorFleteTransportador = $scope.MaskValoresGrid(Math.round($scope.Modelo.Planilla.ValorFleteTransportador))
            $scope.Modelo.Planilla.ValorPagarTransportador = $scope.MaskValoresGrid(Math.round($scope.Modelo.Planilla.ValorPagarTransportador))
            $scope.TotalImpuestos = $scope.MaskValoresGrid(Math.round($scope.TotalImpuestos))
            //CalcularImpuesto();
            $scope.MaskValores()

        }



        $scope.Buscar = function () {
            if ($scope.ValidarPermisos.AplicaConsultar == PERMISO_ACTIVO) {
                $scope.Codigo = 0
                Find()
            }
        };

        $scope.ModeloEstado = { Codigo: 1, Nombre: 'DEFINITIVO' };
        $scope.Modelo.FechaFinal = new Date();
        $scope.ListadoRemesas = [];
        function Find() {
            blockUI.start('Buscando registros ...');

            $timeout(function () {
                blockUI.message('Espere por favor ...');
            }, 100);
            $scope.Buscando = true;
            $scope.MensajesErrorConslta = [];
            if (DatosRequeridosConsultaRemesas()) {
                if ($scope.MensajesErrorConslta.length == 0) {

                    blockUI.delay = 1000;
                    $scope.Filtro.Pagina = 0
                    $scope.Filtro.RegistrosPagina = 0
                    //$scope.Modelo.RegistrosPagina = 10
                    $scope.Modelo.NumeroPlanilla = 0


                    $scope.Modelo.TipoDocumento = CODIGO_TIPO_DOCUMENTO_REMESA_PAQUETERIA;

                    if ($scope.ModeloEstado !== undefined && $scope.ModeloEstado !== null) {
                        $scope.Modelo.Estado = $scope.ModeloEstado.Codigo;
                    } else {
                        $scope.Modelo.Estado = -1;
                    }

                    var filtro = angular.copy($scope.Modelo);
                    filtro.Anulado = -1;

                    RemesaGuiasFactory.Consultar(filtro).
                        then(function (response) {
                            if (response.data.ProcesoExitoso === true) {
                                $scope.Modelo.Codigo = 0
                                if (response.data.Datos.length > 0) {
                                    if ($scope.ListadoRemesas.length == 0) {
                                        $scope.ListadoRemesas = response.data.Datos
                                    } else {
                                        for (var i = 0; i < response.data.Datos.length; i++) {
                                            var cont = 0
                                            for (var j = 0; j < $scope.ListadoRemesas.length; j++) {
                                                if ($scope.ListadoRemesas[j].Remesa.Numero == response.data.Datos[i].Remesa.Numero) {
                                                    cont++
                                                }
                                            }
                                            if (cont == 0) {
                                                $scope.ListadoRemesas.push(response.data.Datos[i])
                                            }
                                        }
                                    }
                                    $scope.ListadoRemesasFiltradas = $scope.ListadoRemesas
                                    $scope.totalRegistros = $scope.ListadoRemesas.length;
                                    $scope.totalPaginas = Math.ceil($scope.totalRegistros / 10);
                                    $scope.PrimerPagina()
                                    $scope.Buscando = false;
                                    $scope.ResultadoSinRegistros = '';
                                    $('#btncriterios').click()
                                }
                                else {
                                    $scope.totalRegistros = 0;
                                    $scope.totalPaginas = 0;
                                    $scope.paginaActual = 1;
                                    $scope.ResultadoSinRegistros = 'No hay datos para mostrar';
                                    $scope.Buscando = false;
                                }
                                var listado = [];
                                response.data.Datos.forEach(function (item) {
                                    listado.push(item);
                                });
                            }
                        }, function (response) {
                            ShowError(response.statusText);
                        });
                }
            }
            blockUI.stop();
        }
        //Paginacion
        $scope.PrimerPagina = function () {
            if ($scope.totalRegistros > 10) {
                $scope.ListadoGuias = []
                $scope.paginaActual = 1
                for (var i = 0; i < 10; i++) {
                    $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                }
            } else {
                $scope.ListadoGuias = $scope.ListadoRemesasFiltradas
            }
        }
        $scope.Anterior = function () {
            if ($scope.paginaActual > 1) {
                $scope.paginaActual -= 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.ListadoGuias = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                    }
                }
            }
            else {
                $scope.PrimerPagina()
            }
        }
        $scope.Siguiente = function () {
            if ($scope.paginaActual < $scope.totalPaginas) {
                $scope.paginaActual += 1
                var a = $scope.paginaActual * 10
                if (a < $scope.totalRegistros) {
                    $scope.ListadoGuias = []
                    for (var i = a - 10; i < a; i++) {
                        $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                    }
                } else {
                    $scope.UltimaPagina()
                }
            } else if ($scope.paginaActual == $scope.totalPaginas) {
                $scope.UltimaPagina()
            }
        }
        $scope.UltimaPagina = function () {
            if ($scope.totalRegistros > 10 && $scope.totalPaginas > 1) {
                $scope.paginaActual = $scope.totalPaginas
                var a = $scope.paginaActual * 10
                $scope.ListadoGuias = []
                for (var i = a - 10; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoGuias.push($scope.ListadoRemesasFiltradas[i])
                }
            }
        }

        function DatosRequeridosConsultaRemesas() {
            $scope.MensajesErrorConslta = [];
            var modelo = $scope.Modelo
            var continuar = true;
            if ((modelo.FechaInicial == null || modelo.FechaInicial == undefined || modelo.FechaInicial == '')
                && (modelo.FechaFinal == null || modelo.FechaFinal == undefined || modelo.FechaFinal == '')
                && (modelo.NumeroInicial == null || modelo.NumeroInicial == undefined || modelo.NumeroInicial == '' || modelo.NumeroInicial == 0)
                && (modelo.NumeroFinal == null || modelo.NumeroFinal == undefined || modelo.NumeroFinal == '' || modelo.NumeroFinal == 0)
            ) {
                $scope.MensajesErrorConslta.push('Debe ingresar los filtros de números o fechas');
                continuar = false

            } else if ((modelo.NumeroInicial !== null && modelo.NumeroInicial !== undefined && modelo.NumeroInicial !== '' && modelo.NumeroInicial !== 0)
                || (modelo.NumeroFinal !== null && modelo.NumeroFinal !== undefined && modelo.NumeroFinal !== '' && modelo.NumeroFinal !== 0)
                || (modelo.FechaInicial !== null && modelo.FechaInicial !== undefined && modelo.FechaInicial !== '')
                || (modelo.FechaFinal !== null && modelo.FechaFinal !== undefined && modelo.FechaFinal !== '')

            ) {
                if ((modelo.NumeroInicial !== null && modelo.NumeroInicial !== undefined && modelo.NumeroInicial !== '' && modelo.NumeroInicial !== 0)
                    && (modelo.NumeroFinal !== null && modelo.NumeroFinal !== undefined && modelo.NumeroFinal !== '' && modelo.NumeroFinal !== 0)) {
                    if (modelo.NumeroFinal < modelo.NumeroInicial) {
                        $scope.MensajesErrorConslta.push('El número final debe ser mayor al número final');
                        continuar = false
                    }
                } else {
                    if ((modelo.NumeroInicial !== null && modelo.NumeroInicial !== undefined && modelo.NumeroInicial !== '' && modelo.NumeroInicial !== 0)) {
                        $scope.Modelo.NumeroFinal = modelo.NumeroInicial
                    } else {
                        $scope.Modelo.NumeroInicial = modelo.NumeroFinal
                    }
                }
                if ((modelo.FechaInicial !== null && modelo.FechaInicial !== undefined && modelo.FechaInicial !== '')
                    && (modelo.FechaFinal !== null && modelo.FechaFinal !== undefined && modelo.FechaFinal !== '')) {
                    if (modelo.FechaFinal < modelo.FechaInicial) {
                        $scope.MensajesErrorConslta.push('La fecha inicial debe ser mayor a la fecha final');
                        continuar = false
                    } else if (((modelo.FechaFinal - modelo.FechaInicial) / (1000 * 60 * 60 * 24)) > CANTIDAD_DIAS_FLITRO) {
                        $scope.MensajesErrorConslta.push('El rango de fechas no puede ser superior a ' + CANTIDAD_DIAS_FLITRO + ' dias');
                        continuar = false
                    }
                }

                else {
                    if ((modelo.FechaInicial !== null && modelo.FechaInicial !== undefined && modelo.FechaInicial !== '')) {
                        $scope.Modelo.FechaFinal = modelo.FechaInicial
                    } else {
                        $scope.Modelo.FechaInicial = modelo.FechaFinal
                    }
                }
            }


            return continuar
        }
        $scope.MarcarRemesas = function (chk) {
            if (chk) {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoRemesasFiltradas[i].Seleccionado = true
                }
            } else {
                for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                    $scope.ListadoRemesasFiltradas[i].Seleccionado = false
                }
            }
        }
        $scope.Filtrar = function () {

            var Filtro = $scope.Filtro2
            var nFiltrosAplica = 0
            if (Filtro.NumeroInicial !== undefined && Filtro.NumeroInicial > 0) {
                nFiltrosAplica++
            }
            if (Filtro.NumeroFinal !== undefined && Filtro.NumeroFinal > 0) {
                nFiltrosAplica++
            }
            if (Filtro.FechaInicial !== undefined && Filtro.FechaInicial > 0) {
                nFiltrosAplica++
            }
            if (Filtro.FechaFinal !== undefined && Filtro.FechaFinal > 0) {
                nFiltrosAplica++
            }
            $scope.ListadoRemesasFiltradas = []
            for (var i = 0; i < $scope.ListadoRemesas.length; i++) {
                var cont = 0
                var item = $scope.ListadoRemesas[i]
                if (Filtro.NumeroInicial !== undefined && Filtro.NumeroInicial > 0) {
                    if (item.Remesa.NumeroDocumento >= Filtro.NumeroInicial) {
                        cont++
                    }
                }
                if (Filtro.NumeroFinal !== undefined && Filtro.NumeroFinal > 0) {
                    if (item.Remesa.NumeroDocumento <= Filtro.NumeroFinal) {
                        cont++
                    }
                }
                if (Filtro.FechaInicial !== undefined && Filtro.FechaInicial > 0) {
                    if (new Date(item.Remesa.Fecha) >= Filtro.FechaInicial) {
                        cont++
                    }
                }
                if (Filtro.FechaFinal !== undefined && Filtro.FechaFinal > 0) {
                    if (new Date(item.Remesa.Fecha) <= Filtro.FechaFinal) {
                        cont++
                    }
                }
                if (cont == nFiltrosAplica) {
                    $scope.ListadoRemesasFiltradas.push(item)
                }
            }
            $scope.PrimerPagina();

        }
        $scope.AgregarAuxiliar = function () {
            if ($scope.ModeloFuncionario !== '' && $scope.ModeloFuncionario !== undefined && $scope.ModeloFuncionario !== null && $scope.ModeloFuncionario.Codigo !== 0 && $scope.ModeloFuncionario.Codigo !== undefined) {
                var concidencias = 0
                if ($scope.ListaAuxiliares.length > 0) {
                    for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                        if ($scope.ModeloFuncionario.Codigo === $scope.ListaAuxiliares[i].Tercero.Codigo) {
                            concidencias++
                            break;
                        }
                    }
                    if (concidencias > 0) {
                        ShowError('El funcionario ya fue ingresado')
                        $scope.ModeloFuncionario = '';
                    } else {
                        $scope.ListaAuxiliares.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto }, Modificarfuncionario: true });
                        $scope.ModeloFuncionario = '';
                    }
                } else {
                    $scope.ListaAuxiliares.push({ CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, Tercero: { Codigo: $scope.ModeloFuncionario.Codigo, Nombre: $scope.ModeloFuncionario.NombreCompleto }, Modificarfuncionario: true });
                    $scope.ModeloFuncionario = '';
                }
            } else {
                ShowError('Debe ingresar un funcionario valido');
            }
            $scope.ValidarDatosFuncionario();
        }
        $scope.ValidarDatosFuncionario = function () {
            for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                if ($scope.ListaAuxiliares[i].Horas === '' || $scope.ListaAuxiliares[i].Horas === undefined || $scope.ListaAuxiliares[i].Horas === null || $scope.ListaAuxiliares[i].Horas === 0
                    || $scope.ListaAuxiliares[i].Valor === '' || $scope.ListaAuxiliares[i].Valor === undefined || $scope.ListaAuxiliares[i].Valor === null || $scope.ListaAuxiliares[i].Valor === 0) {
                    //ShowError('Debe ingresar los detalles de horas trabajadas y valor por funcionario');
                } else {
                    $scope.ListaAuxiliares[i].Modificarfuncionario = false;
                }
            }
        }
        $scope.EliminarFuncionario = function (codigo) {
            if ($scope.ListaAuxiliares.length > 0) {
                for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                    if (codigo === $scope.ListaAuxiliares[i].Codigo) {
                        $scope.ListaAuxiliares.splice(i, 1);
                    }
                }
            }
        };
        $scope.ListaImpuestos = []
        $scope.AgregarImpuestos = function () {
            if ($scope.Impuestos == '' || $scope.Impuestos == null || $scope.Impuestos == undefined) {
                ShowError('De ingresar el impuesto')
            }
            else {
                if ($scope.ListaImpuestos.length == 0) {
                    $scope.ListaImpuestos.push($scope.Impuestos)
                    $scope.Impuestos = ''
                } else {
                    var cont = 0
                    for (var i = 0; i < $scope.ListaImpuestos.length; i++) {
                        if ($scope.Impuestos.Codigo == $scope.ListaImpuestos[i].Codigo) {
                            cont++
                            break;
                        }
                    }
                    if (cont > 0) {
                        ShowError('El impuesto que intenta agregar ya se encuentra adicionado')
                    } else {
                        $scope.ListaImpuestos.push($scope.Impuestos)
                    }
                    $scope.Impuestos = ''
                }

            }
        }
        $scope.EliminarImpuesto = function (index) {
            $scope.ListaImpuestos.splice(index, 1);
        };

        $scope.VolverMaster = function () {
            document.location.href = '#!ConsultarPlanillasPaqueteriaPortal/' + $scope.Modelo.Planilla.NumeroDocumento;
        };
        $scope.MaskMayus = function () {
            MascaraMayusGeneral($scope)
        };
        $scope.MaskNumero = function (option) {
            MascaraNumeroGeneral($scope)
        };
        $scope.MaskNumeroGrid = function (item) {
            return MascaraNumero(item)
        };
        $scope.MaskMoneda = function (option) {
            MascaraMonedaGeneral($scope)
        };
        $scope.MaskMonedaGrid = function (item) {
            return MascaraMoneda(item)
        };
        $scope.MaskValores = function (option) {
            MascaraValoresGeneral($scope)
        };
        $scope.MaskValoresGrid = function (item) {
            return MascaraValores(item)
        };
        $scope.MaskHora = function () {
            MascaraHorasGeneral($scope)
        };
        $scope.MaskHoraGrid = function (item) {
            return MascaraHora(item)
        };
        $scope.Guardar = function () {
            closeModal('modalConfirmacionGuardar');
            //ShowError('Opción no disponible por el momento')
            if (DatosRequeridos()) {
                //Estado
                $scope.Modelo.Planilla.Detalles = []
                if ($scope.ListadoGuiaGuardadas !== undefined && $scope.ListadoGuiaGuardadas !== null && $scope.ListadoGuiaGuardadas !== '') {
                    if ($scope.ListadoGuiaGuardadas.length > 0) {
                        $scope.Modelo.Planilla.Detalles = $scope.ListadoGuiaGuardadas;
                    }
                }
                if ($scope.ListadoRemesasFiltradas !== undefined && $scope.ListadoRemesasFiltradas !== null && $scope.ListadoRemesasFiltradas !== '') {
                    for (var i = 0; i < $scope.ListadoRemesasFiltradas.length; i++) {
                        var guia = $scope.ListadoRemesasFiltradas[i]
                        if (guia.Seleccionado) {
                            $scope.Modelo.Planilla.Detalles.push({ Remesa: guia.Remesa })
                        }
                    }
                }
                if ($scope.DetallesAuxiliares.length > 0) {
                    $scope.Modelo.Planilla.DetallesAuxiliares = $scope.DetallesAuxiliares
                }
                $scope.Modelo.Planilla.FechaHoraSalida = new Date($scope.Modelo.Planilla.FechaSalida)
                var Horasminutos = []
                Horasminutos = $scope.Modelo.Planilla.HoraSalida.split(':')
                if (Horasminutos.length > 0) {
                    $scope.Modelo.Planilla.FechaHoraSalida.setHours(Horasminutos[0])
                    $scope.Modelo.Planilla.FechaHoraSalida.setMinutes(Horasminutos[1])
                }
                $scope.Modelo.Planilla.TarifaTransportes = { Codigo: $scope.Sesion.UsuarioAutenticado.ManejoPorcentajeAfiliado ? 0 : $scope.Modelo.Planilla.TarifaTransportes.Codigo }
                $scope.Modelo.Planilla.TipoTarifaTransportes = { Codigo: $scope.Sesion.UsuarioAutenticado.ManejoPorcentajeAfiliado ? 0 : $scope.TipoTarifaTransportes.Codigo }
                //$scope.Modelo.Planilla.TarifaTransportes = { Codigo: $scope.Modelo.Planilla.TarifaTransportes.Codigo }
                //$scope.Modelo.Planilla.TipoTarifaTransportes = { Codigo: $scope.TipoTarifaTransportes.Codigo }
                $scope.Modelo.Planilla.ValorContraEntrega = $scope.ContraEntrega.ValorCliente
                if ($scope.ListadoImpuestosFiltrado.length > 0) {
                    $scope.Modelo.Planilla.DetalleImpuesto = $scope.ListadoImpuestosFiltrado;
                }
                if ($scope.ListaAuxiliares.length > 0) {
                    $scope.Modelo.Planilla.DetallesAuxiliares = []
                    for (var i = 0; i < $scope.ListaAuxiliares.length; i++) {
                        if (!$scope.ListaAuxiliares[i].Modificarfuncionario) {
                            $scope.Modelo.Planilla.DetallesAuxiliares.push({
                                Funcionario: $scope.ListaAuxiliares[i].Tercero
                                , NumeroHorasTrabajadas: $scope.ListaAuxiliares[i].Horas
                                , Valor: $scope.ListaAuxiliares[i].Valor
                                , Observaciones: $scope.ListaAuxiliares[i].Observaciones
                            })
                        }
                    }
                }
                try {
                    if (parseInt($scope.Modelo.Planilla.ValorAnticipo) > CERO) {
                        $scope.Modelo.CuentaPorPagar = {
                            CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                            TipoDocumento: CODIGO_TIPO_DOCUMENTO_CUENTA_POR_PAGAR,
                            CodigoAlterno: '',
                            Fecha: $scope.Modelo.Planilla.Fecha,
                            Tercero: { Codigo: $scope.Modelo.Planilla.Vehiculo.Conductor.Codigo },
                            DocumentoOrigen: { Codigo: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO },
                            CodigoDocumentoOrigen: CODIGO_DOCUMENTO_ORIGEN_ANTICIPO,
                            Numeracion: '',
                            CuentaPuc: { Codigo: 0 },
                            ValorTotal: $scope.Modelo.Planilla.ValorAnticipo,
                            Abono: 0,
                            Saldo: $scope.Modelo.Planilla.ValorAnticipo,
                            FechaCancelacionPago: $scope.Modelo.Planilla.Fecha,
                            Aprobado: 1,
                            UsuarioAprobo: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                            FechaAprobo: $scope.Modelo.Planilla.Fecha,
                            Oficina: { Codigo: $scope.ModeloOficina.Codigo },
                            Vehiculo: { Codigo: $scope.Modelo.Planilla.Vehiculo.Codigo },
                            UsuarioCrea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        }
                    }
                } catch (e) {

                }
                try {
                    $scope.Modelo.Planilla.Vehiculo.Conductor = { Codigo: $scope.Modelo.Planilla.Vehiculo.Conductor.Codigo, Nombre: $scope.Modelo.Planilla.Vehiculo.Conductor.Nombre, NombreCompleto: $scope.Modelo.Planilla.Vehiculo.Conductor.NombreCompleto }
                } catch (e) {

                }
                $scope.Modelo.TipoDocumento = 130;
                PlanillaGuiasFactory.Guardar($scope.Modelo).
                    then(function (response) {
                        if (response.data.ProcesoExitoso == true) {
                            if (response.data.Datos > CERO) {
                                if ($scope.Modelo.Codigo == CERO) {
                                    ShowSuccess('Se guardó la planilla con el número ' + response.data.Datos + '');
                                }
                                else {
                                    ShowSuccess('Se modificó la planilla con el número ' + response.data.Datos + '');
                                }
                                location.href = '#!ConsultarPlanillasPaqueteriaPortal/' + response.data.Datos;
                                if ($scope.Sesion.Empresa.GeneraManifiesto === 1 && $scope.Modelo.Planilla.Estado.Codigo == 1 && $scope.Modelo.Planilla.Ruta.TipoRuta.Codigo == 4401 && $scope.Sesion.UsuarioAutenticado.ManejoGenerarManifiestoPaqueteria) {
                                    $scope.Manifiesto = {
                                        Numero: 0,
                                        CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                                        NumeroDocumentoPlanilla: response.data.Datos,
                                        Tipo_Documento: { Codigo: CODIGO_TIPO_DOCUMENTO_PLANILLA_DESPACHO },
                                        Usuario_Crea: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                                        Oficina: { Codigo: $scope.Sesion.UsuarioAutenticado.Oficinas.Codigo },
                                        Tipo_Manifiesto: { Codigo: CODIGO_CATALOGO_TIPO_MANIFIESTO_PLANILLA_PAQUETERIA },
                                        Valor_Anticipo: $scope.Modelo.Planilla.ValorAnticipo,
                                    }

                                    ManifiestoFactory.Guardar($scope.Manifiesto).then(function (response) {
                                        if (response.data.ProcesoExitoso == true) {
                                            if (response.data.Datos > CERO) {
                                                $scope.ModalManifiesto = response.data.Datos;
                                            } else if (response.data.Datos == -1) {
                                                $scope.ModalManifiesto = 0;
                                            } else {
                                                ShowError(response.statusText);
                                                $scope.ModalManifiesto = 0;
                                            }
                                        }
                                        else {
                                            ShowError(response.statusText);
                                        }
                                    }, function (response) {
                                        ShowError(response.statusText);
                                    });
                                }
                                location.href = '#!ConsultarPlanillasPaqueteriaPortal/' + response.data.Datos;
                            }
                            else {
                                ShowError(response.statusText);
                            }
                        }
                        else {
                            ShowError(response.statusText);
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        };

        function DatosRequeridos() {
            $scope.MensajesError = [];
            var continuar = true;
            var Modelo = $scope.Modelo
            var fecha1 = new Date(Modelo.Planilla.Fecha)
            var fecha2 = new Date(Modelo.Planilla.FechaSalida)
            var fecha3 = new Date()
            fecha1.setHours('00')
            fecha1.setMinutes('00')
            fecha1.setSeconds('00')
            fecha1.setMilliseconds('00')
            fecha2.setHours('00')
            fecha2.setMinutes('00')
            fecha2.setSeconds('00')
            fecha2.setMilliseconds('00')
            fecha3.setHours('00')
            fecha3.setMinutes('00')
            fecha3.setSeconds('00')
            fecha3.setMilliseconds('00')
            if (Modelo.Planilla.Fecha == undefined || Modelo.Planilla.Fecha == '' || Modelo.Planilla.Fecha == null) {
                $scope.MensajesError.push('Debe ingresar la fecha ');
                continuar = false;
            }
            else if (fecha1 < fecha3) {
                $scope.MensajesError.push('La fecha ingresada no se puede ser menor a la fecha actual');
                continuar = false;
            }
            if (Modelo.Planilla.Vehiculo == undefined || Modelo.Planilla.Vehiculo == '') {
                $scope.MensajesError.push('Debe ingresar el vehículo');
                continuar = false;
            }
            if (Modelo.Planilla.FechaSalida == undefined || Modelo.Planilla.FechaSalida == '' || Modelo.Planilla.FechaSalida == null) {
                $scope.MensajesError.push('Debe ingresar la fecha de salida');
                continuar = false;
            }
            else if (fecha2 < fecha3) {
                $scope.MensajesError.push('La fecha de salida ingresada no se puede ser menor a la fecha actual');
                continuar = false;
            }
            if (Modelo.Planilla.HoraSalida == undefined || Modelo.Planilla.HoraSalida == '' || Modelo.Planilla.HoraSalida == null) {
                $scope.MensajesError.push('Debe ingresar la hora de salida');
                continuar = false;
            }
            else {
                var horas = Modelo.Planilla.HoraSalida.split(':')
                if (horas.length < 2) {
                    $scope.MensajesError.push('Debe ingresar una hora valida');
                    continuar = false;
                } else if (fecha2 == fecha3) {
                    if (parseInt(horas[0]) < fecha3.getHours()) {
                        $scope.MensajesError.push('La hora ingresada debe ser mayor a la actual');
                        continuar = false;
                    } else if (parseInt(horas[0]) == fecha3.getHours()) {
                        if (parseInt(horas[1]) < fecha3.getMinutes()) {
                            $scope.MensajesError.push('La hora ingresada debe ser mayor a la actual');
                            continuar = false;
                        } else {
                            if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                                $scope.MensajesError.push('Debe ingresar una hora valida');
                                continuar = false;
                            }
                        }

                    }

                } else {
                    if (parseInt(horas[0]) > 23 || parseInt(horas[0]) < 0 || parseInt(horas[1]) > 59 || parseInt(horas[1]) < 0) {
                        $scope.MensajesError.push('Debe ingresar una hora valida');
                        continuar = false;
                    }
                }

            }
            try {
                $scope.contadorguias = $scope.ListadoGuiaGuardadas.length

            } catch (e) {
                $scope.contadorguias = 0;

            }
            try {
                $scope.ListadoRemesasFiltradas.forEach(function (itemDetalle) {
                    if (itemDetalle.Seleccionado == true) {
                        $scope.contadorguias = $scope.contadorguias + 1
                    }
                })
            } catch (e) {
            }

            if ($scope.contadorrecolecciones === 0 && $scope.contadorguias === 0) {
                $scope.MensajesError.push('Debe seleccionar mínimo un detalle');
                continuar = false;
            }


            return continuar;
        }

        $scope.ConfirmacionGuardar = function () {
            if ($scope.DeshabilitarActualizar) {
                location.href = '#!ConsultarPlanillasPaqueteriaPortal/' + $scope.Modelo.Planilla.Numero;
            } else {
                showModal('modalConfirmacionGuardar');
            }
        };

        function Obtener() {
            blockUI.start('Cargando tarifario Compras código ' + $scope.Modelo.Codigo);

            $timeout(function () {
                blockUI.message('Cargando tarifario Compras Código ' + $scope.Modelo.Codigo);
            }, 200);

            filtros = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: $scope.Modelo.Numero,
                Cliente: { Codigo: $scope.Sesion.UsuarioAutenticado.Cliente.Codigo }
            };

            blockUI.delay = 1000;
            PlanillaGuiasFactory.Obtener(filtros).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        $scope.esObtener = true
                        $scope.ListadoGuiaGuardadas = []
                        $scope.Modelo = response.data.Datos
                        $scope.Modelo.Remesa = { Cliente: {}, Ruta: {} }
                        $scope.Modelo.Planilla.FechaSalida = new Date($scope.Modelo.Planilla.FechaHoraSalida)

                        $scope.Modelo.Planilla.HoraSalida = $scope.Modelo.Planilla.FechaSalida.getHours().toString()

                        if ($scope.Modelo.Planilla.FechaSalida.getMinutes().toString().length == 1) {
                            $scope.Modelo.Planilla.HoraSalida += ':0' + $scope.Modelo.Planilla.FechaSalida.getMinutes().toString()
                        } else {
                            $scope.Modelo.Planilla.HoraSalida += ':' + $scope.Modelo.Planilla.FechaSalida.getMinutes().toString()
                        }
                        $scope.Modelo.CodigoEmpresa = $scope.Sesion.UsuarioAutenticado.CodigoEmpresa
                        $scope.Modelo.UsuarioCrea = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        $scope.Modelo.UsuarioModifica = { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo }
                        if ($scope.Modelo.Planilla.Estado.Codigo == 1) {
                            $scope.DeshabilitarActualizar = true

                        }
                        $scope.Modelo.Planilla.Fecha = new Date($scope.Modelo.Planilla.Fecha)
                        ////Obtiene el detalle de impuestos aplicados
                        //if ($scope.Modelo.Planilla.DetalleImpuesto !== undefined && $scope.Modelo.Planilla.DetalleImpuesto !== null && $scope.Modelo.Planilla.DetalleImpuesto !== []) {
                        //    $scope.ListaImpuestos = $scope.Modelo.Planilla.DetalleImpuesto;
                        //    //formatea el valor de cada impuesto
                        //    for (var i = 0; i < $scope.ListaImpuestos.length; i++) {
                        //        $scope.ListaImpuestos[i].ValorImpuesto = $scope.MaskValoresGrid($scope.ListaImpuestos[i].ValorImpuesto);
                        //    }
                        //}
                        //CalcularImpuesto();
                        $scope.ListadoGuiaGuardadas = []
                        if ($scope.Modelo.Planilla.Detalles !== undefined && $scope.Modelo.Planilla.Detalles !== null && $scope.Modelo.Planilla.Detalles !== []) {
                            $scope.ListadoGuiaGuardadas = $scope.Modelo.Planilla.Detalles
                            //$('#btncriterios')[0].click()
                            //$('#btnFiltros')[0].click()
                        }
                        $scope.ListaAuxiliares = []
                        if ($scope.Modelo.Planilla.DetallesAuxiliares !== undefined && $scope.Modelo.Planilla.DetallesAuxiliares !== null && $scope.Modelo.Planilla.DetallesAuxiliares !== []) {
                            for (var i = 0; i < $scope.Modelo.Planilla.DetallesAuxiliares.length; i++) {
                                $scope.ListaAuxiliares.push({
                                    Tercero: $scope.Modelo.Planilla.DetallesAuxiliares[i].Funcionario,
                                    Horas: $scope.Modelo.Planilla.DetallesAuxiliares[i].NumeroHorasTrabajadas,
                                    Valor: $scope.Modelo.Planilla.DetallesAuxiliares[i].Valor,
                                    Observaciones: $scope.Modelo.Planilla.DetallesAuxiliares[i].Observaciones,
                                })
                            }
                        }
                        var conductor = $scope.Modelo.Planilla.Vehiculo.Conductor

                        //$scope.MostrarGuiasAsociadas()
                        $scope.CargarDatosFunciones()
                        $scope.MaskValores()
                        try {
                            $scope.Modelo.Planilla.Vehiculo = $scope.CargarVehiculos($scope.Modelo.Planilla.Vehiculo.Codigo);
                            $scope.Modelo.Planilla.Vehiculo.Conductor = $scope.CargarTercero(conductor.Codigo)
                            $scope.ObtenerInformacionTenedor()
                        } catch (e) {
                        }

                    }
                    else {
                        ShowError('No se logro consultar el tarifario Compras código ' + $scope.Modelo.Codigo + '. ' + response.data.MensajeOperacion);
                        document.location.href = '#!ConsultarPlanillasPaqueteriaPortal';
                    }
                }, function (response) {
                    ShowError(response.statusText);
                    document.location.href = '#!ConsultarPlanillasPaqueteriaPortal';
                });

            blockUI.stop();
        };
        $scope.EliminarGuiaGuardada = function (index) {
            $scope.ListadoGuiaGuardadas.splice(index, 1)
        }

        $scope.ValidarAnticipoPlanilla = function (anticipo) {
            anticipo = parseInt(MascaraNumero($scope.Modelo.Planilla.ValorAnticipo))
            if (parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) > 0) {
                if (anticipo > ((parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador)))) {
                    ShowError('El anticipo no puede ser mayor a ' + ((parseFloat($scope.Modelo.Planilla.Ruta.PorcentajeAnticipo) / 100) * parseInt(MascaraNumero($scope.Modelo.Planilla.ValorFleteTransportador))))
                    $scope.Modelo.Planilla.ValorAnticipo = 0
                } else {
                    $scope.Modelo.Planilla.ValorAnticipo = MascaraValores(anticipo)
                }
            }
        }
        try {
            var Parametros = $routeParams.Numero.split(',')
            if (Parametros.length > 1) {
                $scope.Modelo.Numero = Parametros[CERO];
                $scope.aplicabase = true
                Obtener();
            } else {
                if ($routeParams.Numero > CERO) {
                    $scope.Modelo.Numero = $routeParams.Numero;
                    Obtener();
                }
                else {
                    $scope.Modelo.Numero = 0;
                    $scope.CargarDatosFunciones()
                }

            }
        } catch (e) {
            if ($routeParams.Numero > CERO) {
                $scope.Modelo.Numero = $routeParams.Numero;
                Obtener();
            }
            else {
                $scope.Modelo.Numero = 0;
                $scope.CargarDatosFunciones()
            }

        }
        $scope.ValidarRemesa = function (Numero) {
            $scope.Modelo.Planilla.RemesaMasivo = null
            if (Numero > 0) {
                filtros = {
                    CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                    NumeroDocumento: Numero,
                    TipoDocumento: { Codigo: CODIGO_TIPO_DOCUMENTO_REMESAS },
                    Estado: 1,
                    Pagina: 1,
                    RegistrosPagina: 1,
                    //UsuarioConsulta: { Codigo: $scope.Sesion.UsuarioAutenticado.Codigo },
                };
                RemesasFactory.Consultar(filtros).
                    then(function (response) {
                        if (response.data.ProcesoExitoso === true) {
                            if (response.data.Datos.length > 0) {
                                $scope.Modelo.Planilla.RemesaMasivo = response.data.Datos[0].Numero
                            }
                            else {
                                ShowError('El número de remesa ingresado no corresponde a un remesa de masivo o se encuentra anulada')
                                $scope.Modelo.Planilla.RemesaPadre = ''
                                $scope.Modelo.Planilla.RemesaMasivo = null
                            }
                        }
                    }, function (response) {
                        ShowError(response.statusText);
                    });
            }
        }
        $scope.ConsultarEstadosRemesas = function (item) {
            $scope.ListadoEstadoGuias = []
            var Filtro = {
                CodigoEmpresa: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa,
                Numero: item.Remesa.Numero
            }
            RemesaGuiasFactory.ConsultarEstadosRemesa(Filtro).
                then(function (response) {
                    if (response.data.ProcesoExitoso === true) {
                        if (response.data.Datos.length > 0) {
                            $scope.ListadoEstadoGuias = response.data.Datos
                            showModal('ModalEstadosRemesas')
                        } else {
                            ShowError('No se encontro registro de estados de la remesa seleccionada')
                        }
                    }
                }, function (response) {
                    ShowError(response.statusText);
                });
        };
        EmpresasFactory.Consultar(
            { Codigo: $scope.Sesion.UsuarioAutenticado.CodigoEmpresa, }
        ).then(function (response) {
            if (response.data.ProcesoExitoso === true) {

                $scope.pref = response.data.Datos[0].Prefijo;

            }
        });
        //Función que ejecuta el boton de vista preliminar
        $scope.DesplegarInforme = function (Numero, OpcionPDf, OpcionEXCEL) {
            $scope.urlASP = '';
            $scope.urlASP = ObtenerURLProyectoASP($scope.urlASP);
            $scope.NumeroFactura = Numero;
            $scope.NombreReporte = NOMBRE_REPORTE_GUIA;

            //Arma el filtro
            $scope.ArmarFiltro();

            //Llama a la pagina ASP con el filtro por GET
            if (OpcionPDf == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarPdf=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }
            if (OpcionEXCEL == 1) {
                window.open($scope.urlASP + '/Reportes/Reportes.aspx?Empresa=' + $scope.Sesion.UsuarioAutenticado.CodigoEmpresa + '&NombRepo=' + $scope.NombreReporte + $scope.FiltroArmado + '&OpcionExportarExcel=' + OpcionPDf + '&Prefijo=' + $scope.pref);
            }

            //Se limpia el filtro para poder hacer mas consultas
            $scope.FiltroArmado = '';
        };
        $scope.ArmarFiltro = function () {
            if ($scope.NumeroFactura != undefined && $scope.NumeroFactura != '') {
                $scope.FiltroArmado = '&Numero=' + $scope.NumeroFactura;
            }

        }
    }]);