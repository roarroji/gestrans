﻿SofttoolsApp.factory('DetalleSeguimientoVehiculosFactory', ['$http', function ($http) {

    var urlService = GetUrl('DetalleSeguimientoVehiculos');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarRemesas = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarRemesas';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarNovedadesSitiosReporte = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarNovedadesSitiosReporte';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarPuestoControlRutas = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarPuestoControlRutas';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarMasterRemesa = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarMasterRemesa';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.ConsultarPuntosControl = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarPuntosControl';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    }; factory.CambiarRutaSeguimiento = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/CambiarRutaSeguimiento';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.EnviarCorreoCliente = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/EnviarCorreoCliente';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    //Enviar Email Seguimiento Vehicular
    factory.EnviarCorreoSeguimiento = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/EnviarCorreoSeguimiento';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.EnviarCorreoAvanceVehiculosCliente = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/EnviarCorreoAvanceVehiculosCliente';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.EnviarDocumentosConductor = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/EnviarDocumentosConductor';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    return factory;
}]);