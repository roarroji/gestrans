﻿SofttoolsApp.factory('NotasFacturasFactory', ['$http', function ($http) {

    var urlService = GetUrl('NotasFacturas');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }
        
    };
    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.ObtenerDatosNotaElectronicaSaphety = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerDatosNotaElectronicaSaphety';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.GuardarNotaElectronica = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/GuardarNotaElectronica';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    factory.ObtenerDocumentoReporteSaphety = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ObtenerDocumentoReporteSaphety';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    return factory;
}]);