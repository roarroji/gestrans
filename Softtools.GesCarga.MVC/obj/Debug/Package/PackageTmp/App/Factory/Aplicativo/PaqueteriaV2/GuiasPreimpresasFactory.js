﻿SofttoolsApp.factory('GuiasPreimpresasFactory', ['$http', function ($http) {

    var urlService = GetUrl('GuiasPreimpresas');
    var factory = {};
    var request = {
        method: '',
        url: '',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        data: {}
    }

    factory.Consultar = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Consultar';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Obtener = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/Obtener';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.Anular = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Anular';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.Guardar = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/Guardar';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.GuardarPrecintos = function (entidad) {
        request.method = 'POST';
        request.url = urlService + '/GuardarPrecintos';
        request.data = entidad; if (entidad.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ConsultarPrecinto = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ConsultarUnicoPrecinto';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };

    factory.LiberarPrecinto = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/LiberarPrecinto';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }

    };
    factory.ValidarPrecintoPlanillaPaqueteria = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ValidarPrecintoPlanillaPaqueteria';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };
    
    factory.ActualizarResponsable = function (filtro) {
        request.method = 'POST';
        request.url = urlService + '/ActualizarResponsable';
        request.data = filtro; if (filtro.Sync) { return ajaxRequest(request); } else { return $http(request); }
    };

    return factory;

}]);