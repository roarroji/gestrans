﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Almacen
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Fachada.Almacen
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Almacen
    Public NotInheritable Class LogicaOrdenCompra
        Inherits LogicaBase(Of OrdenCompra)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of OrdenCompra)
        ReadOnly _persistenciaDetalle As IPersistenciaBase(Of DetalleOrdenCompra)

        Sub New(persistencia As IPersistenciaBase(Of OrdenCompra), persistenciaDetalle As IPersistenciaBase(Of DetalleOrdenCompra))
            _persistencia = persistencia
            _persistenciaDetalle = persistenciaDetalle
        End Sub

        Public Overrides Function Consultar(filtro As OrdenCompra) As Respuesta(Of IEnumerable(Of OrdenCompra))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of OrdenCompra))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of OrdenCompra))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As OrdenCompra) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As OrdenCompra) As Respuesta(Of OrdenCompra)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of OrdenCompra)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            If consulta.Numero = 0 Then
                Return New Respuesta(Of OrdenCompra)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            If consulta.Numero > 0 Then
                consulta.Detalles = _persistenciaDetalle.Consultar(New DetalleOrdenCompra With {.CodigoEmpresa = consulta.CodigoEmpresa, .Numero = consulta.Numero})
            End If


            Return New Respuesta(Of OrdenCompra)(consulta)
        End Function

        Public Function Anular(entidad As OrdenCompra) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaOrdenCompra).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function
    End Class
End Namespace
