﻿Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Fachada.Basico.Despachos
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Basico.Despachos
    Public NotInheritable Class LogicaRutas
        Inherits LogicaBase(Of Rutas)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Rutas)

        Sub New(persistencia As IPersistenciaBase(Of Rutas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Rutas) As Respuesta(Of IEnumerable(Of Rutas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Rutas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Rutas))(consulta)
        End Function


        Public Function ConsultarTrayectos(filtro As Rutas) As Respuesta(Of IEnumerable(Of TrayectoRuta))
            Dim consulta = CType(_persistencia, PersistenciaRutas).ConsultarTrayectos(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of TrayectoRuta))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of TrayectoRuta))(consulta)
        End Function
        Public Overrides Function Guardar(entidad As Rutas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Rutas) As Respuesta(Of Rutas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Rutas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Rutas)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As Rutas) As Respuesta(Of Rutas)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of Rutas) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of Rutas) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As Rutas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaRutas).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Function InsertarPuestosControl(entidad As Rutas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaRutas).InsertarPuestosControl(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionInserto)}
        End Function

        Public Function InsertarLegalizacionGastosRuta(entidad As Rutas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Inserto As Boolean = False

            Inserto = CType(_persistencia, PersistenciaRutas).InsertarLegalizacionGastosRuta(entidad)

            If Not Inserto Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionInserto)}
        End Function

        Public Function InsertarPeaje(entidad As Rutas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Inserto As Boolean = False

            Inserto = CType(_persistencia, PersistenciaRutas).InsertarPeaje(entidad)

            If Not Inserto Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionInserto)}
        End Function
        Public Function GenerarPlanitilla(entidad As Rutas) As Respuesta(Of Rutas)
            Dim mensajeGuardo = "Genero Archivo"
            Dim url As New Rutas
            url = CType(_persistencia, PersistenciaRutas).GenerarPlanitilla(entidad)
            Return New Respuesta(Of Rutas)(url)
        End Function

        Public Function InsertarTrayectos(entidad As Rutas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Inserto As Boolean = False

            Inserto = CType(_persistencia, PersistenciaRutas).InsertarTrayectos(entidad)

            If Not Inserto Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionInserto)}
        End Function
    End Class

End Namespace