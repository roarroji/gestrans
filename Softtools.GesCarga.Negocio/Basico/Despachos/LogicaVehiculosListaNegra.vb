﻿Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Fachada.Basico.Despachos
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Basico.Despachos
    Public NotInheritable Class LogicaVehiculosListaNegra
        Inherits LogicaBase(Of Vehiculos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>  
        ReadOnly _persistencia As IPersistenciaBase(Of Vehiculos)

        Sub New(persistencia As IPersistenciaBase(Of Vehiculos))
            _persistencia = persistencia
        End Sub

        Private Function DatosRequeridos(entidad As Vehiculos) As Respuesta(Of Vehiculos)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of Vehiculos) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of Vehiculos) With {.ProcesoExitoso = True}

        End Function



        Public Overrides Function Consultar(filtro As Vehiculos) As Respuesta(Of IEnumerable(Of Vehiculos))

            Throw New NotImplementedException()




        End Function

        Public Overrides Function Guardar(entidad As Vehiculos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.CodigoListaNegra > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}
        End Function

        Public Overrides Function Obtener(filtro As Vehiculos) As Respuesta(Of Vehiculos)

            Throw New NotImplementedException()




        End Function

        Public Function Anular(entidad As Vehiculos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaVehiculosListaNegra).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

    End Class
End Namespace
