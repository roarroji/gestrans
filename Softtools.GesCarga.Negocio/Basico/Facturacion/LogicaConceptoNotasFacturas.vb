﻿Imports Softtools.GesCarga.Entidades.Basico.Facturacion
Imports Softtools.GesCarga.Fachada.Basico.Facturacion
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Namespace Basico.Facturacion
    Public NotInheritable Class LogicaConceptoNotasFacturas
        Inherits LogicaBase(Of ConceptoNotasFacturas)

        ReadOnly _persistencia As IPersistenciaBase(Of ConceptoNotasFacturas)

        Sub New(persistencia As IPersistenciaBase(Of ConceptoNotasFacturas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As ConceptoNotasFacturas) As Respuesta(Of IEnumerable(Of ConceptoNotasFacturas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of ConceptoNotasFacturas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of ConceptoNotasFacturas))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As ConceptoNotasFacturas) As Respuesta(Of ConceptoNotasFacturas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of ConceptoNotasFacturas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of ConceptoNotasFacturas)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As ConceptoNotasFacturas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function


        Public Function Anular(entidad As ConceptoNotasFacturas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaConceptoNotasFacturas).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

        Private Function DatosRequeridos(entidad As ConceptoNotasFacturas) As Respuesta(Of ConceptoNotasFacturas)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of ConceptoNotasFacturas) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of ConceptoNotasFacturas) With {.ProcesoExitoso = True}

        End Function
    End Class
End Namespace
