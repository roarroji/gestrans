﻿Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Fachada.Basico.General
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Basico.General
    Public NotInheritable Class LogicaOficinas
        Inherits LogicaBase(Of Oficinas)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Oficinas)

        Sub New(persistencia As IPersistenciaBase(Of Oficinas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Oficinas) As Respuesta(Of IEnumerable(Of Oficinas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Oficinas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Oficinas))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Oficinas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Oficinas) As Respuesta(Of Oficinas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Oficinas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Oficinas)(consulta)
        End Function

        Public Function Anular(entidad As Oficinas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaOficinas).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

        Public Function ConsultarRegionesPaises(filtro As RegionesPaises) As Respuesta(Of IEnumerable(Of RegionesPaises))
            Dim consulta = CType(_persistencia, PersistenciaOficinas).ConsultarRegionesPaises(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of RegionesPaises))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RegionesPaises))(consulta)
        End Function

        Public Function ConsultarConTipoDocumento(filtro As Oficinas) As Respuesta(Of IEnumerable(Of TipoDocumentos))

            Return New Respuesta(Of IEnumerable(Of TipoDocumentos))
        End Function

        Public Function InsertarConsecutivos(entidad As Oficinas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Inserto As Boolean = False

            Inserto = CType(_persistencia, PersistenciaOficinas).InsertarConsecutivos(entidad)

            If Not Inserto Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionInserto)}
        End Function

    End Class

End Namespace