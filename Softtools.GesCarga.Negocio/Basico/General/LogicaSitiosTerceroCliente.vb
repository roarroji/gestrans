﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.Basico.Operacion
Imports Softtools.GesCarga.Fachada.Basico.General

Namespace Basico.General
    Public NotInheritable Class LogicaSitiosTerceroCliente
        Inherits LogicaBase(Of SitiosTerceroCliente)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of SitiosTerceroCliente)

        Sub New(persistencia As IPersistenciaBase(Of SitiosTerceroCliente))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As SitiosTerceroCliente) As Respuesta(Of IEnumerable(Of SitiosTerceroCliente))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of SitiosTerceroCliente))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of SitiosTerceroCliente))(consulta)
        End Function

        Public Function ConsultarOrdenServicio(filtro As SitiosTerceroCliente) As Respuesta(Of IEnumerable(Of SitiosTerceroCliente))
            Dim consulta = CType(_persistencia, PersistenciaSitiosTerceroCliente).ConsultarOrdenServicio(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of SitiosTerceroCliente))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of SitiosTerceroCliente))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As SitiosTerceroCliente) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = _persistencia.Insertar(entidad)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As SitiosTerceroCliente) As Respuesta(Of SitiosTerceroCliente)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of SitiosTerceroCliente)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of SitiosTerceroCliente)(consulta)
        End Function

    End Class

End Namespace