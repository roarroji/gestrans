﻿Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Fachada.Basico.General
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.Paqueteria

Namespace Basico.General
    Public NotInheritable Class LogicaZonas
        Inherits LogicaBase(Of Zonas)


        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Zonas)

        Sub New(persistencia As IPersistenciaBase(Of Zonas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Zonas) As Respuesta(Of IEnumerable(Of Zonas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Zonas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Zonas))(consulta)
        End Function

        Public Function ConsultarZonificacionGuias(filtro As Zonas) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim consulta = CType(_persistencia, PersistenciaZonas).ConsultarZonificacionGuias(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(CType(consulta, IEnumerable(Of RemesaPaqueteria)))
        End Function
        Public Function InsertarZonificacionGuias(entidad As Zonas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaZonas).InsertarZonificacionGuias(entidad)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Guardar(entidad As Zonas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = _persistencia.Insertar(entidad)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Zonas) As Respuesta(Of Zonas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Zonas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Zonas)(consulta)
        End Function

        Public Function Anular(entidad As Zonas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaZonas).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

    End Class

End Namespace