﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Fachada.Mantenimiento
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Mantenimiento
    Public NotInheritable Class LogicaDocumentoEquipoMantenimiento
        Inherits LogicaBase(Of EquipoMantenimientoDocumento)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of EquipoMantenimientoDocumento)

        Sub New(persistencia As IPersistenciaBase(Of EquipoMantenimientoDocumento))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As EquipoMantenimientoDocumento) As Respuesta(Of IEnumerable(Of EquipoMantenimientoDocumento))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EquipoMantenimientoDocumento))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EquipoMantenimientoDocumento))(consulta)
        End Function


        Public Overrides Function Guardar(entidad As EquipoMantenimientoDocumento) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If
            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}
        End Function

        Public Overrides Function Obtener(filtro As EquipoMantenimientoDocumento) As Respuesta(Of EquipoMantenimientoDocumento)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EquipoMantenimientoDocumento)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EquipoMantenimientoDocumento)(consulta)
        End Function
        Public Function InsertarTemporal(entidad As EquipoMantenimientoDocumento) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long
            valor = CType(_persistencia, PersistenciaDocumentoEquipoMantenimiento).InsertarTemporal(entidad)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function
        Public Function EliminarDocumento(entidad As EquipoMantenimientoDocumento) As Respuesta(Of Boolean)
            Dim resultado As Boolean = False
            Dim mensaje As String = String.Empty

            resultado = CType(_persistencia, PersistenciaDocumentoEquipoMantenimiento).EliminarDocumento(entidad)

            If Not resultado Then
                mensaje = String.Format("La foto no se logro eliminar, Contacte con el administrador del sistema")
            End If

            Return New Respuesta(Of Boolean)(resultado, mensaje)
        End Function
    End Class
End Namespace
