﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Mantenimiento


    Public NotInheritable Class LogicaEstadoEquipoMantenimiento
        Inherits LogicaBase(Of EstadoEquipoMantenimiento)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of EstadoEquipoMantenimiento)

        Sub New(persistencia As IPersistenciaBase(Of EstadoEquipoMantenimiento))
            _persistencia = persistencia

        End Sub

        Public Overrides Function Consultar(filtro As EstadoEquipoMantenimiento) As Respuesta(Of IEnumerable(Of EstadoEquipoMantenimiento))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EstadoEquipoMantenimiento))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EstadoEquipoMantenimiento))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As EstadoEquipoMantenimiento) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If


            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As EstadoEquipoMantenimiento) As Respuesta(Of EstadoEquipoMantenimiento)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EstadoEquipoMantenimiento)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EstadoEquipoMantenimiento)(consulta)
        End Function
        Private Function DatosRequeridos(entidad As EstadoEquipoMantenimiento) As Respuesta(Of EstadoEquipoMantenimiento)
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
