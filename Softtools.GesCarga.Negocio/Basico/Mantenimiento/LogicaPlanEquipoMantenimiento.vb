﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Fachada.Mantenimiento
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.MantenimientosPendientes

Namespace Mantenimiento


    Public NotInheritable Class LogicaPlanEquipoMantenimiento
        Inherits LogicaBase(Of PlanEquipoMantenimiento)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of PlanEquipoMantenimiento)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos del detalle.
        ''' </summary>
        ReadOnly _persistenciaDetalle As IPersistenciaBase(Of DetallePlanEquipoMantenimiento)

        Sub New(persistencia As IPersistenciaBase(Of PlanEquipoMantenimiento), persistenciaDetalle As IPersistenciaBase(Of DetallePlanEquipoMantenimiento))
            _persistencia = persistencia
            _persistenciaDetalle = persistenciaDetalle
        End Sub

        Public Overrides Function Consultar(filtro As PlanEquipoMantenimiento) As Respuesta(Of IEnumerable(Of PlanEquipoMantenimiento))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanEquipoMantenimiento))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanEquipoMantenimiento))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As PlanEquipoMantenimiento) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If


            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As PlanEquipoMantenimiento) As Respuesta(Of PlanEquipoMantenimiento)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of PlanEquipoMantenimiento)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If
            If consulta.Codigo > 0 Then
                consulta.Detalles = _persistenciaDetalle.Consultar(New DetallePlanEquipoMantenimiento With {.CodigoEmpresa = consulta.CodigoEmpresa, .Codigo = consulta.Codigo})
            End If

            Return New Respuesta(Of PlanEquipoMantenimiento)(consulta)
        End Function

        Public Function Anular(entidad As PlanEquipoMantenimiento) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False


            anulo = CType(_persistencia, PersistenciaPlanEquipoMantenimiento).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function
        Public Function InsertarEquipos(entidad As IEnumerable(Of EquipoPlanEquipoMantenimiento)) As Respuesta(Of Boolean)
            Dim mensajeGuardo = "el registro se guardó correctamente"
            Dim Guardo As Long


            Guardo = CType(_persistencia, PersistenciaPlanEquipoMantenimiento).InsertarEquipos(entidad)

            If Guardo < 1 Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = mensajeGuardo}
        End Function
        Public Function ConsultarEquipos(filtro As PlanEquipoMantenimiento) As Respuesta(Of IEnumerable(Of EquipoPlanEquipoMantenimiento))
            Dim consulta = CType(_persistencia, PersistenciaPlanEquipoMantenimiento).ConsultarEquipos(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EquipoPlanEquipoMantenimiento))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EquipoPlanEquipoMantenimiento))(consulta)
        End Function
        Public Function ConsultarMantenimientosPendientes(filtro As MantenimientosPendientes) As Respuesta(Of IEnumerable(Of MantenimientosPendientes))
            Dim consulta = CType(_persistencia, PersistenciaPlanEquipoMantenimiento).ConsultarMantenimientosPendientes(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of MantenimientosPendientes))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of MantenimientosPendientes))(consulta)
        End Function


        Private Function DatosRequeridos(entidad As PlanEquipoMantenimiento) As Respuesta(Of PlanEquipoMantenimiento)
            Throw New NotImplementedException()
        End Function

        Public Function CambiarEstado(entidad As PlanEquipoMantenimiento) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Modifico As Boolean = False


            Modifico = CType(_persistencia, PersistenciaPlanEquipoMantenimiento).CambiarEstado(entidad)

            If Not Modifico Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionModifico)}
        End Function
    End Class
End Namespace
