﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Basico
    Public NotInheritable Class LogicaSistemas
        Inherits LogicaBase(Of Sistemas)


        ReadOnly _persistencia As IPersistenciaBase(Of Sistemas)
        ReadOnly _persistencia2 As IPersistenciaBase(Of Sistemas)

        Sub New(persistencia As IPersistenciaBase(Of Sistemas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Sistemas) As Respuesta(Of IEnumerable(Of Sistemas))
            Dim consulta = _persistencia.Consultar(filtro)
            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Sistemas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If
            Return New Respuesta(Of IEnumerable(Of Sistemas))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Sistemas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If


            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If
            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}
        End Function

        Public Overrides Function Obtener(filtro As Sistemas) As Respuesta(Of Sistemas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Sistemas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If
            Return New Respuesta(Of Sistemas)(consulta)
        End Function
    End Class
End Namespace
