﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Contabilidad.Documentos
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Contabilidad
    Public NotInheritable Class LogicaintegracionSIESA
        Inherits LogicaBase(Of integracionSIESA)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of integracionSIESA)

        Public Sub New(capaPersistenciaDetalleSeguimientoVehiculos As IPersistenciaBase(Of integracionSIESA))
            _persistencia = capaPersistenciaDetalleSeguimientoVehiculos
        End Sub

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos de la notificacion de correo.
        ''' </summary>

        Public Overrides Function Consultar(filtro As integracionSIESA) As Respuesta(Of IEnumerable(Of integracionSIESA))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of integracionSIESA))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of integracionSIESA))(consulta)
        End Function


        Public Function Anular(entidad As integracionSIESA) As Respuesta(Of Boolean)
        End Function
        Public Overrides Function Obtener(filtro As integracionSIESA) As Respuesta(Of integracionSIESA)

        End Function

        Public Overrides Function Guardar(entidad As integracionSIESA) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente

            Dim valor As Long

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function



    End Class

End Namespace