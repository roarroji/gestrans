﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.ControlViajes
Imports Softtools.GesCarga.Fachada.ControlViajes
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace ControlViajes

    Public NotInheritable Class LogicaDocumentoInspeccionPreoperacional
        Inherits LogicaBase(Of DocumentoInspeccionPreoperacional)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of DocumentoInspeccionPreoperacional)

        Sub New(persistencia As IPersistenciaBase(Of DocumentoInspeccionPreoperacional))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As DocumentoInspeccionPreoperacional) As Respuesta(Of IEnumerable(Of DocumentoInspeccionPreoperacional))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DocumentoInspeccionPreoperacional))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DocumentoInspeccionPreoperacional))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As DocumentoInspeccionPreoperacional) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DocumentoInspeccionPreoperacional) As Respuesta(Of DocumentoInspeccionPreoperacional)
            Throw New NotImplementedException()
        End Function
        Private Function DatosRequeridos(entidad As DocumentoInspeccionPreoperacional) As Respuesta(Of DocumentoInspeccionPreoperacional)
            Throw New NotImplementedException()

        End Function
        Public Function InsertarTemporal(entidad As DocumentoInspeccionPreoperacional) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long
            valor = CType(_persistencia, PersistenciaDocumentoInspeccionPreoperacional).InsertarTemporal(entidad)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function
        Public Function EliminarFoto(entidad As DocumentoInspeccionPreoperacional) As Respuesta(Of Boolean)
            Dim resultado As Boolean = False
            Dim mensaje As String = String.Empty

            resultado = CType(_persistencia, PersistenciaDocumentoInspeccionPreoperacional).EliminarFoto(entidad)

            If Not resultado Then
                mensaje = String.Format("La foto no se logro eliminar, Contacte con el administrador del sistema")
            End If

            Return New Respuesta(Of Boolean)(resultado, mensaje)
        End Function
        Public Function LimpiarTemporalUsuario(entidad As DocumentoInspeccionPreoperacional) As Respuesta(Of Boolean)
            Dim resultado As Boolean = False
            Dim mensaje As String = String.Empty

            resultado = CType(_persistencia, PersistenciaDocumentoInspeccionPreoperacional).LimpiarTemporalUsuario(entidad)

            If Not resultado Then
                mensaje = String.Format("")
            End If

            Return New Respuesta(Of Boolean)(resultado, mensaje)
        End Function

    End Class

End Namespace