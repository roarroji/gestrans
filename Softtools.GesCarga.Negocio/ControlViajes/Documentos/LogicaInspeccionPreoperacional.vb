﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.ControlViajes
Imports Softtools.GesCarga.Fachada.ControlViajes
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace ControlViajes

    Public NotInheritable Class LogicaInspeccionPreoperacional
        Inherits LogicaBase(Of InspeccionPreoperacional)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of InspeccionPreoperacional)
        '''' <summary>
        '''' Variable de entorno para el manejo de la persistencia de datos de la notificacion de correo.
        '''' </summary>
        'ReadOnly _persistenciaBandejaSalidaCorreos As IPersistenciaBase(Of BandejaSalidaCorreos)

        Sub New(persistencia As IPersistenciaBase(Of InspeccionPreoperacional))
            _persistencia = persistencia
            '_persistenciaBandejaSalidaCorreos = persistenciaBandejaSalidaCorreos
        End Sub

        Public Overrides Function Consultar(filtro As InspeccionPreoperacional) As Respuesta(Of IEnumerable(Of InspeccionPreoperacional))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of InspeccionPreoperacional))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of InspeccionPreoperacional))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As InspeccionPreoperacional) As Respuesta(Of Long)
            Dim Codigo As Integer = 0
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then

                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If

            End If

            'If Not IsNothing(entidad.EventoCorreo) Then
            '    Codigo = EncolarCorreo(entidad)
            'End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(Codigo), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As InspeccionPreoperacional) As Respuesta(Of InspeccionPreoperacional)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of InspeccionPreoperacional)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of InspeccionPreoperacional)(consulta)
        End Function


        Public Function ObtenerDocumentoActivo(filtro As InspeccionPreoperacional) As Respuesta(Of InspeccionPreoperacional)
            Dim consulta = CType(_persistencia, PersistenciaInspeccionPreoperacional).ObtenerDocumentoActivo(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of InspeccionPreoperacional)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of InspeccionPreoperacional)(consulta)
        End Function

        Public Function ObtenerUltimaInspeccion(filtro As InspeccionPreoperacional) As Respuesta(Of InspeccionPreoperacional)
            Dim consulta = CType(_persistencia, PersistenciaInspeccionPreoperacional).ObtenerUltimaInspeccion(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of InspeccionPreoperacional)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of InspeccionPreoperacional)(consulta)
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As InspeccionPreoperacional) As Respuesta(Of InspeccionPreoperacional)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of InspeccionPreoperacional) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of InspeccionPreoperacional) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As InspeccionPreoperacional) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaInspeccionPreoperacional).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

        '#Region "Envio Correos"
        '        Private Function PrepararCorreo(entidad As InspeccionPreoperacional) As BandejaSalidaCorreos
        '            Dim TextoCorreo As New BandejaSalidaCorreos With {
        '                .CodigoEmpresa = entidad.CodigoEmpresa,
        '                .TipoDocumento = 180,'New ValorCatalogo With {.Codigo = entidad.TipoDocumento},
        '                .NumeroDocumeto = entidad.Numero,
        '                .CuentaCorreoDe = "supportlogistic@soscontingencias.com.co",
        '                .CuentaCorreoCopia = String.Empty,
        '                .CodigoUsuarioCrea = entidad.UsuarioCrea.Codigo,
        '                .Estado = 0
        '            }
        '            Dim Con = 0
        '            Dim CantidadCorreos = entidad.EventoCorreo.ListaDistrubicionCorreos.Count

        '            For Each CuentaCorreos In entidad.EventoCorreo.ListaDistrubicionCorreos
        '                If Con < (CantidadCorreos - 1) Then
        '                    If CuentaCorreos.Email <> "" Then
        '                        TextoCorreo.CuentaCorreoPara += CuentaCorreos.Email + ","
        '                        Con = Con + 1
        '                    Else
        '                        Con = Con + 1
        '                        TextoCorreo.CuentaCorreoPara += CuentaCorreos.Email
        '                    End If
        '                Else
        '                    TextoCorreo.CuentaCorreoPara += CuentaCorreos.Email
        '                End If
        '            Next
        '            TextoCorreo.Contenido += vbNewLine
        '            TextoCorreo.Contenido += vbNewLine
        '            TextoCorreo.Contenido = entidad.EventoCorreo.EncabezadoCorreo
        '            TextoCorreo.Contenido += vbNewLine
        '            TextoCorreo.Contenido += vbNewLine
        '            TextoCorreo.Contenido += entidad.EventoCorreo.MensajeCorreo
        '            TextoCorreo.Contenido += vbNewLine
        '            TextoCorreo.Contenido += vbNewLine

        '            TextoCorreo.AsuntoCorreo = Replace(Replace(Replace(entidad.EventoCorreo.AsuntoCorreo, EventoCorreos.CAMPO_TEXTO_CORREO_NUMERO_VIAJE, entidad.NumeroViaje),
        '                                             EventoCorreos.CAMPO_TEXTO_CORREO_PLACA, entidad.Vehiculo.Placa), EventoCorreos.CAMPO_TEXTO_CORREO_FECHA, entidad.FechaInicioHora)
        '            TextoCorreo.Contenido += vbNewLine
        '            TextoCorreo.Contenido += vbNewLine
        '            TextoCorreo.Contenido += entidad.EventoCorreo.MensajeFirma
        '            TextoCorreo.Contenido += vbNewLine
        '            TextoCorreo.Contenido += vbNewLine

        '            Return TextoCorreo
        '        End Function

        '        Public Function EncolarCorreo(Inspeccion As InspeccionPreoperacional)
        '            Dim Codigo As Integer = 0
        '            Try

        '                Dim bandejaSalida As New LogicaBandejaSalidaCorreos(_persistenciaBandejaSalidaCorreos)
        '                Dim correo = PrepararCorreo(Inspeccion)

        '                Codigo = _persistenciaBandejaSalidaCorreos.Insertar(correo)

        '            Catch ex As Exception
        '                Debug.WriteLine(ex.Message.ToString())
        '                Codigo = 0
        '            End Try
        '            Return Codigo

        '        End Function

        '#End Region

    End Class

End Namespace