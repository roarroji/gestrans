﻿Imports Softtools.GesCarga.Entidades.Despachos
Imports Softtools.GesCarga.Fachada.Basico.General
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Despachos
    Public NotInheritable Class LogicaManifiestos
        Inherits LogicaBase(Of Manifiesto)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Manifiesto)

        Sub New(persistencia As IPersistenciaBase(Of Manifiesto))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Manifiesto) As Respuesta(Of IEnumerable(Of Manifiesto))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Manifiesto))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Manifiesto))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Manifiesto) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Manifiesto) As Respuesta(Of Manifiesto)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Manifiesto)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Manifiesto)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        ''' 
        Public Function Anular(entidad As Manifiesto) As Respuesta(Of Manifiesto)
            Return New Respuesta(Of Manifiesto)(CType(_persistencia, PersistenciaManifiestos).Anular(entidad))
        End Function
    End Class

End Namespace