﻿Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Fachada.Despachos
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Despachos
    Public NotInheritable Class LogicaRemesas
        Inherits LogicaBase(Of Remesas)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of Remesas)

        Sub New(persistencia As IPersistenciaBase(Of Remesas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Remesas) As Respuesta(Of IEnumerable(Of Remesas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Remesas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Remesas))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Remesas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Numero, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Remesas) As Respuesta(Of Remesas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Remesas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Remesas)(consulta)
        End Function

        Private Function DatosRequeridos(entidad As Remesas) As Respuesta(Of Remesas)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of Remesas) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of Remesas) With {.ProcesoExitoso = True}

        End Function

        Public Function Anular(entidad As Remesas) As Respuesta(Of Remesas)
            Return New Respuesta(Of Remesas)(CType(_persistencia, PersistenciaRemesas).Anular(entidad))
        End Function

        Public Function ConsultarRemesasPendientesFacturar(filtro As Remesas) As Respuesta(Of IEnumerable(Of Remesas))
            Dim consulta = CType(_persistencia, PersistenciaRemesas).ConsultarRemesasPendientesFacturar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Remesas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Remesas))(consulta)
        End Function

        Public Function ActualizarRemesasExcel(filtro As Remesas) As Respuesta(Of Long)
            Dim consulta = CType(_persistencia, PersistenciaRemesas).ActualizarRemesasExcel(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Long)() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Long)(consulta)
        End Function

        Public Function ActualizarDistribuido(entidad As Remesas) As Respuesta(Of Remesas)
            Return New Respuesta(Of Remesas)(CType(_persistencia, PersistenciaRemesas).ActualizarDistribuido(entidad))
        End Function

        Public Function MarcarRemesasFacturadas(entidad As Remesas) As Respuesta(Of Remesas)
            Return New Respuesta(Of Remesas)(CType(_persistencia, PersistenciaRemesas).MarcarRemesasFacturadas(entidad))
        End Function

        Public Function CumplirGuias(entidad As Remesas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Cumplio As Boolean = False

            Cumplio = CType(_persistencia, PersistenciaRemesas).CumplirRemesas(entidad)

            If Not Cumplio Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionCumplidoGuias)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad, Recursos.OperacionCumplido)}
        End Function

        Public Function ConsultarNumerosRemesas(filtro As Remesas) As Respuesta(Of IEnumerable(Of Remesas))
            Dim consulta = CType(_persistencia, PersistenciaRemesas).ConsultarNumerosRemesas(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Remesas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Remesas))(consulta)
        End Function

        Public Function ObtenerPrecintosAleatorios(filtro As Remesas) As Respuesta(Of IEnumerable(Of DetallePrecintos))
            Dim consulta = CType(_persistencia, PersistenciaRemesas).ObtenerPrecintosAleatorios(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetallePrecintos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetallePrecintos))(consulta)
        End Function

        Public Function ConsultarRemesasPlanillaDespachos(filtro As Remesas) As Respuesta(Of IEnumerable(Of Remesas))
            Dim consulta = CType(_persistencia, PersistenciaRemesas).ConsultarRemesasPlanillaDespachos(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Remesas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Remesas))(consulta)
        End Function
        Public Function ConsultarCumplidoRemesas(filtro As Remesas) As Respuesta(Of IEnumerable(Of Remesas))
            Dim consulta = CType(_persistencia, PersistenciaRemesas).ConsultarCumplidoRemesas(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Remesas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Remesas))(consulta)
        End Function
    End Class

End Namespace