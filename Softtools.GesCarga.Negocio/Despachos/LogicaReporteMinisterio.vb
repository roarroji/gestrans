﻿Imports Softtools.GesCarga.Entidades.Despachos.Procesos
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Fachada.Despachos

Namespace Despachos
    Public NotInheritable Class LogicaReporteMinisterio
        Inherits LogicaBase(Of ReporteMinisterio)

        ReadOnly _persistencia As IPersistenciaBase(Of ReporteMinisterio)

        Sub New(persistencia As IPersistenciaBase(Of ReporteMinisterio))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As ReporteMinisterio) As Respuesta(Of IEnumerable(Of ReporteMinisterio))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of ReporteMinisterio))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of ReporteMinisterio))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As ReporteMinisterio) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.NumeroDocumento = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.NumeroDocumento > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As ReporteMinisterio) As Respuesta(Of ReporteMinisterio)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of ReporteMinisterio)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of ReporteMinisterio)(consulta)
        End Function

        Public Function ReportarRNDC(entidad As ReporteMinisterio) As Respuesta(Of ReporteMinisterio)
            Return New Respuesta(Of ReporteMinisterio)(CType(_persistencia, PersistenciaReporteMinisterio).ReportarRNDC(entidad))
        End Function

        Public Function Anular(entidad As ReporteMinisterio) As Respuesta(Of Boolean)
            Throw New NotImplementedException
        End Function

        Public Function ConsultarRemesasSinManifiesto(filtro As ReporteMinisterio) As Respuesta(Of IEnumerable(Of ReporteMinisterio))
            Dim consulta = CType(_persistencia, PersistenciaReporteMinisterio).ConsultarRemesasSinManifiesto(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of ReporteMinisterio))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of ReporteMinisterio))(consulta)
        End Function

    End Class

End Namespace