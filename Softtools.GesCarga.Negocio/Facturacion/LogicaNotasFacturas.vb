﻿Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Fachada.Facturacion
Namespace Facturacion
    Public NotInheritable Class LogicaNotasFacturas
        Inherits LogicaBase(Of NotasFacturas)

        ReadOnly _persistencia As IPersistenciaBase(Of NotasFacturas)

        Sub New(persistencia As IPersistenciaBase(Of NotasFacturas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As NotasFacturas) As Respuesta(Of IEnumerable(Of NotasFacturas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of NotasFacturas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of NotasFacturas))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As NotasFacturas) As Respuesta(Of Long)

            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(entidad.MensajeSQL)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Numero, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}


        End Function

        Private Function DatosRequeridos(entidad As NotasFacturas) As Respuesta(Of NotasFacturas)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of NotasFacturas) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of NotasFacturas) With {.ProcesoExitoso = True}

        End Function

        Public Overrides Function Obtener(filtro As NotasFacturas) As Respuesta(Of NotasFacturas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of NotasFacturas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of NotasFacturas)(consulta)
        End Function

        Public Function Anular(entidad As NotasFacturas) As Respuesta(Of Boolean)
            Dim anulo As Boolean = False
            Dim mensajeAnulacionFallida = Recursos.FalloOperacionAnularRegistro
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            anulo = CType(_persistencia, PersistenciaNotasFacturas).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeAnulacionFallida, entidad.Numero, Recursos.OperacionAnulo)}
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Public Function ObtenerDatosNotaElectronicaSaphety(filtro As NotasFacturas) As Respuesta(Of NotasFacturas)
            Dim consulta As NotasFacturas
            consulta = New PersistenciaNotasFacturas().ObtenerDatosNotaElectronicaSaphety(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of NotasFacturas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If
            Return New Respuesta(Of NotasFacturas)(consulta)
        End Function

        Public Function GuardarNotaElectronica(entidad As NotasFacturas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long
            valor = New PersistenciaNotasFacturas().GuardarNotaElectronica(entidad)

            If valor.Equals(0) Or Len(Trim(entidad.MensajeSQL)) > 0 Then
                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If

            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Function ObtenerDocumentoReporteSaphety(filtro As NotasFacturas) As Respuesta(Of NotasFacturas)
            Dim consulta As NotasFacturas
            consulta = New PersistenciaNotasFacturas().ObtenerDocumentoReporteSaphety(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of NotasFacturas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of NotasFacturas)(consulta)
        End Function

    End Class
End Namespace

