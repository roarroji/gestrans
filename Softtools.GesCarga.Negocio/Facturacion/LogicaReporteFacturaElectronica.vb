﻿Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Fachada.Facturacion

Namespace Facturacion
    Public NotInheritable Class LogicaReporteFacturaElectronica
        Inherits LogicaBase(Of ReporteFacturaElectronica)

        ReadOnly _persistencia As IPersistenciaBase(Of ReporteFacturaElectronica)
        Sub New(persistencia As IPersistenciaBase(Of ReporteFacturaElectronica))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As ReporteFacturaElectronica) As Respuesta(Of IEnumerable(Of ReporteFacturaElectronica))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of ReporteFacturaElectronica))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of ReporteFacturaElectronica))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As ReporteFacturaElectronica) As Respuesta(Of ReporteFacturaElectronica)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Guardar(entidad As ReporteFacturaElectronica) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function

        Public Function ReportarDocumentosElectronicos(filtro As ReporteFacturaElectronica) As Respuesta(Of ReporteFacturaElectronica)
            Dim consulta As ReporteFacturaElectronica
            consulta = New PersistenciaReporteFacturaElectronica().ReportarDocumentosElectronicos(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of ReporteFacturaElectronica)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If
            Return New Respuesta(Of ReporteFacturaElectronica)(consulta)
        End Function

        Public Function ReportarDocumentosSaphetyServicioWindows(filtro As ReporteFacturaElectronica) As Respuesta(Of ReporteFacturaElectronica)
            Dim consulta As ReporteFacturaElectronica
            consulta = New PersistenciaReporteFacturaElectronica().ReportarDocumentosSaphetyServicioWindows(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of ReporteFacturaElectronica)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If
            Return New Respuesta(Of ReporteFacturaElectronica)(consulta)
        End Function

    End Class
End Namespace
