﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Fidelizacion.Documentos
Imports Softtools.GesCarga.Fachada.Fidelizacion.Documentos
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Fidelizacion.Documentos
    Public NotInheritable Class LogicaDetalleCausalesPuntosVehiculos
        Inherits LogicaBase(Of DetalleCausalesPuntosVehiculos)

        ReadOnly _persistencia As IPersistenciaBase(Of DetalleCausalesPuntosVehiculos)

        Public Sub New(capaPersistenciaDetalleCausalesPuntosVehiculos As IPersistenciaBase(Of DetalleCausalesPuntosVehiculos))
            _persistencia = capaPersistenciaDetalleCausalesPuntosVehiculos
        End Sub

        Public Overrides Function Consultar(filtro As DetalleCausalesPuntosVehiculos) As Respuesta(Of IEnumerable(Of DetalleCausalesPuntosVehiculos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleCausalesPuntosVehiculos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleCausalesPuntosVehiculos))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As DetalleCausalesPuntosVehiculos) As Respuesta(Of DetalleCausalesPuntosVehiculos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of DetalleCausalesPuntosVehiculos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of DetalleCausalesPuntosVehiculos)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As DetalleCausalesPuntosVehiculos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Function Anular(entidad As DetalleCausalesPuntosVehiculos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaDetalleCausalesPuntosVehiculos).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function
    End Class
End Namespace