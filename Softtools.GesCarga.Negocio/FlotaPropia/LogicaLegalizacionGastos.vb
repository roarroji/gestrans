﻿Imports Softtools.GesCarga.Fachada.FlotaPropia
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.FlotaPropia


Namespace Despachos
    Public NotInheritable Class LogicaLegalizacionGastos
        Inherits LogicaBase(Of LegalizacionGastos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of LegalizacionGastos)

        Sub New(persistencia As IPersistenciaBase(Of LegalizacionGastos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As LegalizacionGastos) As Respuesta(Of IEnumerable(Of LegalizacionGastos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of LegalizacionGastos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of LegalizacionGastos))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As LegalizacionGastos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Numero, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As LegalizacionGastos) As Respuesta(Of LegalizacionGastos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of LegalizacionGastos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of LegalizacionGastos)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>

        Public Function Anular(entidad As LegalizacionGastos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaLegalizacionGastos).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Public Function GuardarFacturaTerpel(entidad As DetalleLegalizacionGastos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaLegalizacionGastos).GuardarFacturaTerpel(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(anulo)
            End If

            Return New Respuesta(Of Boolean)(anulo)
        End Function


    End Class
End Namespace