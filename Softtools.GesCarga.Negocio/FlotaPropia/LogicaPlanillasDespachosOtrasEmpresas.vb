﻿Imports Softtools.GesCarga.Fachada.FlotaPropia
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.FlotaPropia


Namespace FlotaPropia

    Public NotInheritable Class LogicaPlanillasDespachosOtrasEmpresas
        Inherits LogicaBase(Of PlanillaDespachosOtrasEmpresas)

        ReadOnly _persistencia As IPersistenciaBase(Of PlanillaDespachosOtrasEmpresas)

        Sub New(persistencia As IPersistenciaBase(Of PlanillaDespachosOtrasEmpresas))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As PlanillaDespachosOtrasEmpresas) As Respuesta(Of IEnumerable(Of PlanillaDespachosOtrasEmpresas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanillaDespachosOtrasEmpresas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanillaDespachosOtrasEmpresas))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As PlanillaDespachosOtrasEmpresas) As Respuesta(Of PlanillaDespachosOtrasEmpresas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of PlanillaDespachosOtrasEmpresas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of PlanillaDespachosOtrasEmpresas)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As PlanillaDespachosOtrasEmpresas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long


            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Numero, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}
        End Function

        Private Function DatosRequeridos(entidad As PlanillaDespachosOtrasEmpresas) As Respuesta(Of PlanillaDespachosOtrasEmpresas)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of PlanillaDespachosOtrasEmpresas) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of PlanillaDespachosOtrasEmpresas) With {.ProcesoExitoso = True}

        End Function


        Public Function Anular(entidad As PlanillaDespachosOtrasEmpresas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaPlanillasDespachosOtrasEmpresas).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function
    End Class
End Namespace

