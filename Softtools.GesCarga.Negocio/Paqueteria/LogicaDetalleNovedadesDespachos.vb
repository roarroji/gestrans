﻿
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Imports Softtools.GesCarga.Fachada.Despachos.Paqueteria

Namespace Despachos.Paqueteria
    Public NotInheritable Class LogicaDetalleNovedadesDespachos
        Inherits LogicaBase(Of DetalleNovedadesDespachos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of DetalleNovedadesDespachos)

        Sub New(persistencia As IPersistenciaBase(Of DetalleNovedadesDespachos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As DetalleNovedadesDespachos) As Respuesta(Of IEnumerable(Of DetalleNovedadesDespachos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleNovedadesDespachos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleNovedadesDespachos))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As DetalleNovedadesDespachos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Or Len(Trim(entidad.MensajeSQL)) > 0 Then
                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As DetalleNovedadesDespachos) As Respuesta(Of DetalleNovedadesDespachos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of DetalleNovedadesDespachos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of DetalleNovedadesDespachos)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As DetalleNovedadesDespachos) As Respuesta(Of DetalleNovedadesDespachos)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of DetalleNovedadesDespachos) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of DetalleNovedadesDespachos) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As DetalleNovedadesDespachos) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaDetalleNovedadesDespachos).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

    End Class

End Namespace