﻿Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Fachada.Basico.Despachos
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Basico.Despachos
    Public NotInheritable Class LogicaEtiquetasPreimpresas
        Inherits LogicaBase(Of EtiquetaPreimpresa)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of EtiquetaPreimpresa)
        ReadOnly _persistencia2 As IPersistenciaBase(Of DetallePrecintos)
        Sub New(persistencia As IPersistenciaBase(Of EtiquetaPreimpresa))
            _persistencia = persistencia

        End Sub

        Public Overrides Function Consultar(filtro As EtiquetaPreimpresa) As Respuesta(Of IEnumerable(Of EtiquetaPreimpresa))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EtiquetaPreimpresa))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EtiquetaPreimpresa))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As EtiquetaPreimpresa) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Function GuardarPrecintos(entidad As EtiquetaPreimpresa) As Respuesta(Of EtiquetaPreimpresa)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As EtiquetaPreimpresa

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of EtiquetaPreimpresa) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = CType(_persistencia, PersistenciaEtiquetasPreimpresas).InsertarPrecintos(entidad)
            Else
                valor = CType(_persistencia, PersistenciaEtiquetasPreimpresas).ModificarPrecintos(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of EtiquetaPreimpresa)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of EtiquetaPreimpresa)(valor)
        End Function

        Public Overrides Function Obtener(filtro As EtiquetaPreimpresa) As Respuesta(Of EtiquetaPreimpresa)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EtiquetaPreimpresa)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EtiquetaPreimpresa)(consulta)
        End Function
        Public Function ConsultarUnicoPrecinto(filtro As DetalleEtiquetaPreimpresa) As Respuesta(Of IEnumerable(Of DetalleEtiquetaPreimpresa))

            Dim consulta = CType(_persistencia, PersistenciaEtiquetasPreimpresas).ConsultarUnicoPrecinto(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleEtiquetaPreimpresa))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleEtiquetaPreimpresa))(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As EtiquetaPreimpresa) As Respuesta(Of EtiquetaPreimpresa)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of EtiquetaPreimpresa) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of EtiquetaPreimpresa) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As EtiquetaPreimpresa) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaEtiquetasPreimpresas).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Function LiberarPrecinto(entidad As EtiquetaPreimpresa) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaEtiquetasPreimpresas).LiberarPrecinto(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, "liberó"), .Datos = anulo}
        End Function

        Public Function ValidarPrecintoPlanillaPaqueteria(entidad As EtiquetaPreimpresa) As Respuesta(Of EtiquetaPreimpresa)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valPrecinto As EtiquetaPreimpresa

            valPrecinto = CType(_persistencia, PersistenciaEtiquetasPreimpresas).ValidarPrecintoPlanillaPaqueteria(entidad)

            If IsNothing(valPrecinto) Then
                Return New Respuesta(Of EtiquetaPreimpresa)(Recursos.NoSeEncontroRegistro)
            End If

            Return New Respuesta(Of EtiquetaPreimpresa)(valPrecinto) With {.ProcesoExitoso = True}
        End Function



        Public Function ActualizarResponsable(entidad As EtiquetaPreimpresa) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valPrecinto As Long = 0

            valPrecinto = CType(_persistencia, PersistenciaEtiquetasPreimpresas).ActualizarResponsable(entidad)

            If valPrecinto <= 0 Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valPrecinto) With {.ProcesoExitoso = True}
        End Function


    End Class

End Namespace