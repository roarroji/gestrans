﻿Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Fachada.Basico.Despachos
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Basico.Despachos
    Public NotInheritable Class LogicaGuiasPreimpresas
        Inherits LogicaBase(Of GuiaPreimpresa)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of GuiaPreimpresa)
        ReadOnly _persistencia2 As IPersistenciaBase(Of DetallePrecintos)
        Sub New(persistencia As IPersistenciaBase(Of GuiaPreimpresa))
            _persistencia = persistencia

        End Sub

        Public Overrides Function Consultar(filtro As GuiaPreimpresa) As Respuesta(Of IEnumerable(Of GuiaPreimpresa))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of GuiaPreimpresa))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of GuiaPreimpresa))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As GuiaPreimpresa) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Function GuardarPrecintos(entidad As GuiaPreimpresa) As Respuesta(Of GuiaPreimpresa)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As GuiaPreimpresa

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of GuiaPreimpresa) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = CType(_persistencia, PersistenciaGuiasPreimpresas).InsertarPrecintos(entidad)
            Else
                valor = CType(_persistencia, PersistenciaGuiasPreimpresas).ModificarPrecintos(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of GuiaPreimpresa)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of GuiaPreimpresa)(valor)
        End Function

        Public Overrides Function Obtener(filtro As GuiaPreimpresa) As Respuesta(Of GuiaPreimpresa)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of GuiaPreimpresa)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of GuiaPreimpresa)(consulta)
        End Function
        Public Function ConsultarUnicoPrecinto(filtro As DetalleGuiaPreimpresa) As Respuesta(Of IEnumerable(Of DetalleGuiaPreimpresa))

            Dim consulta = CType(_persistencia, PersistenciaGuiasPreimpresas).ConsultarUnicoPrecinto(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleGuiaPreimpresa))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleGuiaPreimpresa))(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As GuiaPreimpresa) As Respuesta(Of GuiaPreimpresa)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of GuiaPreimpresa) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of GuiaPreimpresa) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As GuiaPreimpresa) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaGuiasPreimpresas).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Function LiberarPrecinto(entidad As GuiaPreimpresa) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaGuiasPreimpresas).LiberarPrecinto(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, "liberó"), .Datos = anulo}
        End Function

        Public Function ValidarPrecintoPlanillaPaqueteria(entidad As GuiaPreimpresa) As Respuesta(Of GuiaPreimpresa)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valPrecinto As GuiaPreimpresa

            valPrecinto = CType(_persistencia, PersistenciaGuiasPreimpresas).ValidarPrecintoPlanillaPaqueteria(entidad)

            If IsNothing(valPrecinto) Then
                Return New Respuesta(Of GuiaPreimpresa)(Recursos.NoSeEncontroRegistro)
            End If

            Return New Respuesta(Of GuiaPreimpresa)(valPrecinto) With {.ProcesoExitoso = True}
        End Function



        Public Function ActualizarResponsable(entidad As GuiaPreimpresa) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valPrecinto As Long = 0

            valPrecinto = CType(_persistencia, PersistenciaGuiasPreimpresas).ActualizarResponsable(entidad)

            If valPrecinto <= 0 Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valPrecinto) With {.ProcesoExitoso = True}
        End Function


    End Class

End Namespace