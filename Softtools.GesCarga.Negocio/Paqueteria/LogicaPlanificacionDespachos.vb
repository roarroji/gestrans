﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Fachada.Paqueteria

Namespace Paqueteria
    Public NotInheritable Class LogicaPlanificacionDespachos
        Inherits LogicaBase(Of PlanificacionDespachos)

        ReadOnly _persistencia As IPersistenciaBase(Of PlanificacionDespachos)
        Sub New(persistencia As IPersistenciaBase(Of PlanificacionDespachos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As PlanificacionDespachos) As Respuesta(Of IEnumerable(Of PlanificacionDespachos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanificacionDespachos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanificacionDespachos))(consulta)
        End Function

        Public Function ConsultarDetalleRemesasResumen(filtro As PlanificacionDespachos) As Respuesta(Of IEnumerable(Of PlanificacionDespachos))
            Dim consulta As IEnumerable(Of PlanificacionDespachos)
            consulta = New PersistenciaPlanificacionDespachos().ConsultarDetalleRemesasResumen(filtro)
            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanificacionDespachos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanificacionDespachos))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As PlanificacionDespachos) As Respuesta(Of PlanificacionDespachos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of PlanificacionDespachos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of PlanificacionDespachos)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As PlanificacionDespachos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Remesa.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then

                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If

            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Private Function DatosRequeridos(entidad As PlanificacionDespachos) As Respuesta(Of PlanificacionDespachos)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of PlanificacionDespachos) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of PlanificacionDespachos) With {.ProcesoExitoso = True}

        End Function
    End Class

End Namespace