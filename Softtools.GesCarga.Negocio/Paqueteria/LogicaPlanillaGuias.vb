﻿
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Fachada.Paqueteria

Namespace Paqueteria
    Public NotInheritable Class LogicaPlanillaGuias
        Inherits LogicaBase(Of PlanillaGuias)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of PlanillaGuias)

        Sub New(persistencia As IPersistenciaBase(Of PlanillaGuias))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As PlanillaGuias) As Respuesta(Of IEnumerable(Of PlanillaGuias))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanillaGuias))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanillaGuias))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As PlanillaGuias) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Planilla.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As PlanillaGuias) As Respuesta(Of PlanillaGuias)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of PlanillaGuias)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of PlanillaGuias)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As PlanillaGuias) As Respuesta(Of PlanillaGuias)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of PlanillaGuias) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of PlanillaGuias) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As PlanillaGuias) As Respuesta(Of PlanillaGuias)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo = CType(_persistencia, PersistenciaPlanillaGuias).Anular(entidad)

            If IsNothing(anulo) Then
                Return New Respuesta(Of PlanillaGuias) With {.ProcesoExitoso = False, .MensajeOperacion = Recursos.FalloOperacionAnularRegistro}
            End If
            Return New Respuesta(Of PlanillaGuias)(anulo)
        End Function

        Public Function EliminarGuia(entidad As PlanillaGuias) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim BorrarGuia As Boolean = False

            BorrarGuia = CType(_persistencia, PersistenciaPlanillaGuias).EliminarGuia(entidad)

            If Not BorrarGuia Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function
        Public Function EliminarRecoleccion(entidad As PlanillaGuias) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim BorrarGuia As Boolean = False

            BorrarGuia = CType(_persistencia, PersistenciaPlanillaGuias).EliminarRecoleccion(entidad)

            If Not BorrarGuia Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function
    End Class

End Namespace