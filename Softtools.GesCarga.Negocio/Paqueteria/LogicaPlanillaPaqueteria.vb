﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Fachada.Paqueteria

Namespace Paqueteria
    Public NotInheritable Class LogicaPlanillaPaqueteria
        Inherits LogicaBase(Of PlanillaPaqueteria)

        ReadOnly _persistencia As IPersistenciaBase(Of PlanillaPaqueteria)

        Sub New(persistencia As IPersistenciaBase(Of PlanillaPaqueteria))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As PlanillaPaqueteria) As Respuesta(Of IEnumerable(Of PlanillaPaqueteria))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanillaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanillaPaqueteria))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As PlanillaPaqueteria) As Respuesta(Of PlanillaPaqueteria)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of PlanillaPaqueteria)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of PlanillaPaqueteria)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As PlanillaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Planilla.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Private Function DatosRequeridos(entidad As PlanillaPaqueteria) As Respuesta(Of PlanillaPaqueteria)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of PlanillaPaqueteria) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of PlanillaPaqueteria) With {.ProcesoExitoso = True}

        End Function

        Public Function Anular(entidad As PlanillaPaqueteria) As Respuesta(Of PlanillaPaqueteria)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo = CType(_persistencia, PersistenciaPlanillaPaqueteria).Anular(entidad)

            If IsNothing(anulo) Then
                Return New Respuesta(Of PlanillaPaqueteria) With {.ProcesoExitoso = False, .MensajeOperacion = Recursos.FalloOperacionAnularRegistro}
            End If
            Return New Respuesta(Of PlanillaPaqueteria)(anulo)
        End Function

        Public Function EliminarGuia(entidad As PlanillaPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim BorrarGuia As Boolean = False

            BorrarGuia = CType(_persistencia, PersistenciaPlanillaPaqueteria).EliminarGuia(entidad)

            If Not BorrarGuia Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Public Function EliminarRecoleccion(entidad As PlanillaPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim BorrarGuia As Boolean = False

            BorrarGuia = CType(_persistencia, PersistenciaPlanillaPaqueteria).EliminarRecoleccion(entidad)

            If Not BorrarGuia Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Public Function ConsultarPlanillasPendientesRecepcionar(filtro As PlanillaPaqueteria) As Respuesta(Of IEnumerable(Of PlanillaPaqueteria))
            Dim consulta = CType(_persistencia, PersistenciaPlanillaPaqueteria).ConsultarPlanillasPendientesRecepcionar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanillaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanillaPaqueteria))(consulta)
        End Function

    End Class
End Namespace

