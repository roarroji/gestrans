﻿
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Fachada.Paqueteria
Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Paqueteria
    Public NotInheritable Class LogicaRemesaPaqueteria
        Inherits LogicaBase(Of RemesaPaqueteria)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of RemesaPaqueteria)

        Sub New(persistencia As IPersistenciaBase(Of RemesaPaqueteria))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(consulta)
        End Function

        Public Function ConsultarGuiasPlanillaEntrega(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))

            Dim consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarGuiasPlanillaEntrega(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(consulta)
        End Function


        Public Function ConsultarGuiasEtiquetasPreimpresas(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleGuiaPreimpresa))

            Dim consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarGuiasEtiquetasPreimpresas(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleGuiaPreimpresa))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleGuiaPreimpresa))(consulta)
        End Function
        Public Overrides Function Guardar(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Remesa.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then

                If Len(Trim(entidad.MensajeSQL)) > 0 Then
                    Return New Respuesta(Of Long)(entidad.MensajeSQL)
                Else
                    Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
                End If

            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Remesa.Numero, .MensajeOperacion = IIf(entidad.Remesa.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of RemesaPaqueteria)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of RemesaPaqueteria)(consulta)
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="entidad"></param>
        ''' <returns></returns>
        Private Function DatosRequeridos(entidad As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of RemesaPaqueteria) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of RemesaPaqueteria) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaRemesaPaqueteria).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Remesa.Numero, Recursos.OperacionAnulo)}
        End Function
        Public Function CumplirGuias(entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Cumplio As Boolean = False

            Cumplio = CType(_persistencia, PersistenciaRemesaPaqueteria).CumplirGuias(entidad)

            If Not Cumplio Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionCumplidoGuias)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad, Recursos.OperacionCumplido)}
        End Function
        Public Function GenerarPlanitilla(entidad As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Dim mensajeGuardo = "Genero Archivo"
            Dim url As New RemesaPaqueteria

            url = CType(_persistencia, PersistenciaRemesaPaqueteria).GenerarPlanitilla(entidad)

            Return New Respuesta(Of RemesaPaqueteria)(url)
        End Function
        Public Function GuardarEntrega(entidad As Remesas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Numero As Long = 0

            Numero = CType(_persistencia, PersistenciaRemesaPaqueteria).GuardarEntrega(entidad)

            Return New Respuesta(Of Long)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad, Recursos.OperacionModifico), .Datos = Numero}
        End Function
        Public Function GuardarBitacoraGuias(entidad As DetalleEstadosRemesa) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim Numero As Long = 0

            Numero = CType(_persistencia, PersistenciaRemesaPaqueteria).GuardarBitacoraGuias(entidad)

            Return New Respuesta(Of Long)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad, Recursos.OperacionModifico), .Datos = Numero}
        End Function

        Public Function ConsultarIndicadores(entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarIndicadores(entidad)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function
        Public Function AsociarGuiaPlanilla(entidad As RemesaPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaRemesaPaqueteria).AsociarGuiaPlanilla(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function
        Public Function ConsultarEstadosRemesa(entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarEstadosRemesa(entidad)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))(Consulta)

        End Function
        Public Function ConsultarBitacoraGuias(entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarBitacoraGuias(entidad)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleEstadosRemesa))(Consulta)

        End Function

        Public Function ObtenerControlEntregas(entidad As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ObtenerControlEntregas(entidad)
            If IsNothing(Consulta) Then
                Return New Respuesta(Of RemesaPaqueteria)() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of RemesaPaqueteria)(Consulta)
        End Function

        Public Function ConsultarRemesasPendientesControlEntregas(entidad As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasPendientesControlEntregas(entidad)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function

        Public Function ConsultarRemesasPorPlanillar(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasPorPlanillar(filtro)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function
        Public Function ConsultarRemesasRecogerOficina(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasRecogerOficina(filtro)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function

        Public Function ConsultarRemesasRetornoContado(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasRetornoContado(filtro)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function

        Public Function ValidarPreimpreso(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaRemesaPaqueteria).ValidarPreimpreso(entidad)

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.NoSeEncontroRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function

        Public Function CambiarEstadoRemesasPaqueteria(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaRemesaPaqueteria).CambiarEstadoRemesasPaqueteria(entidad)

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.NoSeEncontroRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function
        Public Function CambiarEstadoGuiaPaqueteria(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaRemesaPaqueteria).CambiarEstadoGuiaPaqueteria(entidad)

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.NoSeEncontroRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function
        Public Function CambiarEstadoTrazabilidadGuia(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaRemesaPaqueteria).CambiarEstadoTrazabilidadGuia(entidad)

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.NoSeEncontroRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function

        Public Function ConsultarRemesasPendientesRecibirOficina(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasPendientesRecibirOficina(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(consulta)
        End Function

        Public Function RecibirRemesaPaqueteriaOficinaDestino(entidad As RemesaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = CType(_persistencia, PersistenciaRemesaPaqueteria).RecibirRemesaPaqueteriaOficinaDestino(entidad)

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function

        Public Function ConsultarRemesasPorLegalizar(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasPorLegalizar(filtro)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function

        Public Function ConsultarRemesasPorLegalizarRecaudo(filtro As RemesaPaqueteria) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
            Dim Consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ConsultarRemesasPorLegalizarRecaudo(filtro)

            If IsNothing(Consulta) Then
                Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of RemesaPaqueteria))(Consulta)

        End Function

        Public Function ActualizarSecuencia(filtro As RemesaPaqueteria) As Respuesta(Of RemesaPaqueteria)
            Dim consulta = CType(_persistencia, PersistenciaRemesaPaqueteria).ActualizarSecuencia(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of RemesaPaqueteria)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of RemesaPaqueteria)(consulta)
        End Function

    End Class

End Namespace