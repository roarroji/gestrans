﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Fachada.Paqueteria

Namespace Paqueteria
    Public NotInheritable Class LogicaValidacionPlanillaPaqueteria
        Inherits LogicaBase(Of EncabezadoValidacionPlanillaPaqueteria)

        ReadOnly _persistencia As IPersistenciaBase(Of EncabezadoValidacionPlanillaPaqueteria)

        Sub New(persistencia As IPersistenciaBase(Of EncabezadoValidacionPlanillaPaqueteria))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As EncabezadoValidacionPlanillaPaqueteria) As Respuesta(Of IEnumerable(Of EncabezadoValidacionPlanillaPaqueteria))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EncabezadoValidacionPlanillaPaqueteria))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EncabezadoValidacionPlanillaPaqueteria))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As EncabezadoValidacionPlanillaPaqueteria) As Respuesta(Of EncabezadoValidacionPlanillaPaqueteria)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EncabezadoValidacionPlanillaPaqueteria)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EncabezadoValidacionPlanillaPaqueteria)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As EncabezadoValidacionPlanillaPaqueteria) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Private Function DatosRequeridos(entidad As EncabezadoValidacionPlanillaPaqueteria) As Respuesta(Of EncabezadoValidacionPlanillaPaqueteria)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of EncabezadoValidacionPlanillaPaqueteria) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of EncabezadoValidacionPlanillaPaqueteria) With {.ProcesoExitoso = True}

        End Function

        Public Function Anular(entidad As EncabezadoValidacionPlanillaPaqueteria) As Respuesta(Of EncabezadoValidacionPlanillaPaqueteria)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo = CType(_persistencia, PersistenciaValidacionPlanillaPaqueteria).Anular(entidad)

            If IsNothing(anulo) Then
                Return New Respuesta(Of EncabezadoValidacionPlanillaPaqueteria) With {.ProcesoExitoso = False, .MensajeOperacion = Recursos.FalloOperacionAnularRegistro}
            End If
            Return New Respuesta(Of EncabezadoValidacionPlanillaPaqueteria)(anulo)
        End Function

        Public Function EliminarGuia(entidad As PlanillaPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim BorrarGuia As Boolean = False

            BorrarGuia = CType(_persistencia, PersistenciaPlanillaPaqueteria).EliminarGuia(entidad)

            If Not BorrarGuia Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

        Public Function EliminarRecoleccion(entidad As PlanillaPaqueteria) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim BorrarGuia As Boolean = False

            BorrarGuia = CType(_persistencia, PersistenciaPlanillaPaqueteria).EliminarRecoleccion(entidad)

            If Not BorrarGuia Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function

    End Class
End Namespace

