﻿Imports Softtools.GesCarga.Fachada.Proveedores
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.Proveedores


Namespace Proveedores

    Public NotInheritable Class LogicaPlanillasDespachosProveedores
        Inherits LogicaBase(Of PlanillasDespachosProveedores)

        ReadOnly _persistencia As IPersistenciaBase(Of PlanillasDespachosProveedores)

        Sub New(persistencia As IPersistenciaBase(Of PlanillasDespachosProveedores))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As PlanillasDespachosProveedores) As Respuesta(Of IEnumerable(Of PlanillasDespachosProveedores))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PlanillasDespachosProveedores))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PlanillasDespachosProveedores))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As PlanillasDespachosProveedores) As Respuesta(Of PlanillasDespachosProveedores)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of PlanillasDespachosProveedores)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of PlanillasDespachosProveedores)(consulta)
        End Function

        Public Overrides Function Guardar(entidad As PlanillasDespachosProveedores) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long


            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .Numero = entidad.Numero, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}
        End Function

        Private Function DatosRequeridos(entidad As PlanillasDespachosProveedores) As Respuesta(Of PlanillasDespachosProveedores)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of PlanillasDespachosProveedores) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of PlanillasDespachosProveedores) With {.ProcesoExitoso = True}

        End Function


        Public Function Anular(entidad As PlanillasDespachosProveedores) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaPlanillasDespachosProveedores).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Numero, Recursos.OperacionAnulo)}
        End Function
    End Class
End Namespace

