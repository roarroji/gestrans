﻿Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Negocio.WS_ENCOE_SIESA_GENERICTRANSFER

Public NotInheritable Class LogicaServiciosENCOE

    ' Retorna la Lista Cajas o una Caja
    Dim objGenericTrasfer As WSUNOEE
    Dim strXml As String
    Sub New()
        objGenericTrasfer = New WSUNOEE()
    End Sub

    Public Function Consultar_Cajas_SIESA(strCodCajSIESA As ServiciosENCOE) As List(Of ServiciosENCOE)

        Dim strRespuestaXML As DataSet
        Dim lista As New List(Of ServiciosENCOE)


        strXml = "<?xml version='1.0' encoding='utf-8'?> <Consulta><NombreConexion>pruebas nomina 2015</NombreConexion><IdCia>1</IdCia><IdProveedor>IYS</IdProveedor><IdConsulta>INTEGRACION_MAESTROS_CAJAS</IdConsulta><Usuario>generict</Usuario><Clave>generic2021</Clave><Parametros><caja>" & strCodCajSIESA.CodigoCaja & "</caja></Parametros></Consulta>"
        strRespuestaXML = objGenericTrasfer.EjecutarConsultaXML(strXml)

        Dim data = strRespuestaXML.Tables(0).DataSet().Tables(0)

        For Each item In data.Rows
            lista.Add(New ServiciosENCOE() With {.CodigoCaja = Convert.ToString(item("id_caja")), .CentroOperacion = Convert.ToString(item("centro_operacion")), .Descripcion = Convert.ToString(item("descripcion"))})
        Next
        Return lista
    End Function



    Private Function Consultar_Lista_Precios_Cliente(strIdentClie As String) As String
        Try
            'Dim strRespuestaXML As String

            '' Invocar WEB SERVICE CONSULTA SELENE método ListaPrecios
            'Me.objSelene = New WebServicePlexaSoapClient
            'strRespuestaXML = objSelene.ListaPrecio(strIdentClie)

            ' Asignar la Respuesta a la Lista


        Catch ex As Exception

        End Try

        Return 1


    End Function



End Class





