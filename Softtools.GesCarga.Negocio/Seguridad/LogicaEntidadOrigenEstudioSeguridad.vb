﻿Imports Softtools.GesCarga.Entidades.Seguridad
Imports Softtools.GesCarga.Fachada.Seguridad
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Fachada.Basico.General

Namespace Seguridad
    Public NotInheritable Class LogicaEntidadOrigenEstudioSeguridad
        Inherits LogicaBase(Of EntidadOrigenEstudioSeguridad)

        Private Const GrupoActivo As Byte = 1 'Grupo Logueado
        Private Const CuentaActiva As Byte = 1
        Private Const GrupoExterno As Byte = 1
        Private Const NumeroMaximoIntentosIngresoGrupo As Byte = 3
        Private Const GrupoAdministrador As String = "ADMIN"
        Private Const GrupoDeslogueo As String = "LOGOUT"

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As PersistenciaEntidadOrigenEstudioSeguridad

        ''' <summary>
        '''  Variable de entorno para el manejo de la persistencia de datos de empresa
        ''' </summary>
        Private _persistenciaEmpresa As PersistenciaEmpresas

        Sub New(persistencia As PersistenciaEntidadOrigenEstudioSeguridad)
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As EntidadOrigenEstudioSeguridad) As Respuesta(Of IEnumerable(Of EntidadOrigenEstudioSeguridad))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EntidadOrigenEstudioSeguridad))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EntidadOrigenEstudioSeguridad))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As EntidadOrigenEstudioSeguridad) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As EntidadOrigenEstudioSeguridad) As Respuesta(Of EntidadOrigenEstudioSeguridad)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EntidadOrigenEstudioSeguridad)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EntidadOrigenEstudioSeguridad)(consulta)
        End Function

    End Class
End Namespace
