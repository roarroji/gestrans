﻿Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Fachada.SeguridadUsuarios
Imports Softtools.GesCarga.Fachada.Basico.General
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Seguridad
    Public NotInheritable Class LogicaEstudioSeguridad
        Inherits LogicaBase(Of EstudioSeguridad)

        Private Const GrupoActivo As Byte = 1 'Grupo Logueado
        Private Const CuentaActiva As Byte = 1
        Private Const GrupoExterno As Byte = 1
        Private Const NumeroMaximoIntentosIngresoGrupo As Byte = 3
        Private Const GrupoAdministrador As String = "ADMIN"
        Private Const GrupoDeslogueo As String = "LOGOUT"

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As PersistenciaEstudioSeguridad

        ''' <summary>
        '''  Variable de entorno para el manejo de la persistencia de datos de empresa
        ''' </summary>
        Private _persistenciaEmpresa As PersistenciaEmpresas

        Sub New(persistencia As PersistenciaEstudioSeguridad)
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As EstudioSeguridad) As Respuesta(Of IEnumerable(Of EstudioSeguridad))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EstudioSeguridad))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EstudioSeguridad))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As EstudioSeguridad) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Numero = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As EstudioSeguridad) As Respuesta(Of EstudioSeguridad)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EstudioSeguridad)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EstudioSeguridad)(consulta)
        End Function
        Public Function Anular(entidad As EstudioSeguridad) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaEstudioSeguridad).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Function Autorizar(entidad As EstudioSeguridad) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim autorizo As Boolean = False

            autorizo = CType(_persistencia, PersistenciaEstudioSeguridad).Autorizar(entidad)

            If Not autorizo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
        Public Function Rechazar(entidad As EstudioSeguridad) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim rechazo As Boolean = False

            rechazo = CType(_persistencia, PersistenciaEstudioSeguridad).Rechazar(entidad)

            If Not rechazo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function

    End Class
End Namespace
