﻿Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Fachada.SeguridadUsuarios

Namespace Seguridad

    Public NotInheritable Class LogicaEstudioSeguridadDocumentos
        Inherits LogicaBase(Of EstudioSeguridadDocumentos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of EstudioSeguridadDocumentos)

        Sub New(persistencia As IPersistenciaBase(Of EstudioSeguridadDocumentos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As EstudioSeguridadDocumentos) As Respuesta(Of IEnumerable(Of EstudioSeguridadDocumentos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EstudioSeguridadDocumentos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EstudioSeguridadDocumentos))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As EstudioSeguridadDocumentos) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As EstudioSeguridadDocumentos) As Respuesta(Of EstudioSeguridadDocumentos)
            Throw New NotImplementedException()
        End Function
        Private Function DatosRequeridos(entidad As EstudioSeguridadDocumentos) As Respuesta(Of EstudioSeguridadDocumentos)
            Throw New NotImplementedException()

        End Function
        Public Function InsertarTemporal(entidad As EstudioSeguridadDocumentos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long
            valor = CType(_persistencia, PersistenciaEstudioSeguridadDocumentos).InsertarTemporal(entidad)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function
        Public Function EliminarDocumento(entidad As EstudioSeguridadDocumentos) As Respuesta(Of Boolean)
            Dim resultado As Boolean = False
            Dim mensaje As String = String.Empty

            resultado = CType(_persistencia, PersistenciaEstudioSeguridadDocumentos).EliminarDocumento(entidad)

            If Not resultado Then
                mensaje = String.Format("El documento no se logro eliminar, Contacte con el administrador del sistema")
            End If

            Return New Respuesta(Of Boolean)(resultado, mensaje)
        End Function

        Public Function EliminarDocumentoDefinitivo(entidad As EstudioSeguridadDocumentos) As Respuesta(Of Boolean)
            Dim resultado As Boolean = False
            Dim mensaje As String = String.Empty

            resultado = CType(_persistencia, PersistenciaEstudioSeguridadDocumentos).EliminarDocumentoDefinitivo(entidad)

            If Not resultado Then
                mensaje = String.Format("El documento no se logro eliminar, Contacte con el administrador del sistema")
            End If

            Return New Respuesta(Of Boolean)(resultado, mensaje)
        End Function

        Public Function LimpiarDocumentoTemporalUsuario(entidad As EstudioSeguridadDocumentos) As Respuesta(Of Boolean)
            Dim resultado As Boolean = False
            Dim mensaje As String = String.Empty

            resultado = CType(_persistencia, PersistenciaEstudioSeguridadDocumentos).LimpiarDocumentoTemporalUsuario(entidad)

            If Not resultado Then
                mensaje = String.Format("El documento no se logro eliminar, Contacte con el administrador del sistema")
            End If

            Return New Respuesta(Of Boolean)(resultado, mensaje)
        End Function


    End Class

End Namespace
