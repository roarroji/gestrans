﻿Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Fachada.SeguridadUsuarios
Imports Softtools.GesCarga.Fachada.Basico.General
Imports Softtools.GesCarga.Mensajes.My.Resources
Imports Softtools.GesCarga.Entidades.Utilitarios
Namespace Seguridad
    Public NotInheritable Class LogicaPermisoGrupoUsuarios
        Inherits LogicaBase(Of PermisoGrupoUsuarios)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As PersistenciaPermisoGrupoUsuarios

        ''' <summary>
        '''  Variable de entorno para el manejo de la persistencia de datos de empresa
        ''' </summary>
        Private _persistenciaEmpresa As PersistenciaEmpresas

        Sub New(persistencia As PersistenciaPermisoGrupoUsuarios)
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As PermisoGrupoUsuarios) As Respuesta(Of IEnumerable(Of PermisoGrupoUsuarios))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of PermisoGrupoUsuarios))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of PermisoGrupoUsuarios))(consulta)
        End Function

        Public Function ConsultarModulo(codigoEmpresa As Short, Codigo As Long) As Respuesta(Of IEnumerable(Of ModuloAplicaciones))
            Dim modulo = _persistencia.ConsultarModulo(codigoEmpresa, Codigo)

            If IsNothing(modulo) Then
                Return New Respuesta(Of IEnumerable(Of ModuloAplicaciones))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of ModuloAplicaciones))(modulo)

        End Function

        Public Overrides Function Guardar(entidad As PermisoGrupoUsuarios) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Menu.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Menu.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}


        End Function

        Public Overrides Function Obtener(filtro As PermisoGrupoUsuarios) As Respuesta(Of PermisoGrupoUsuarios)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of PermisoGrupoUsuarios)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of PermisoGrupoUsuarios)(consulta)
        End Function

        Public Function GenerarPlanitilla(entidad As PermisoGrupoUsuarios) As Respuesta(Of PermisoGrupoUsuarios)
            Dim mensajeGuardo = "Genero Archivo"
            Dim url As New PermisoGrupoUsuarios
            url = CType(_persistencia, PersistenciaPermisoGrupoUsuarios).GenerarPlanitilla(entidad)
            Return New Respuesta(Of PermisoGrupoUsuarios)(url)
        End Function

        Public Function GuardarAsigancionPerfilesNotificaciones(entidad As NotificacionGrupoUsuarios) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long
            valor = CType(_persistencia, PersistenciaPermisoGrupoUsuarios).GuardarAsigancionPerfilesNotificaciones(entidad)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Notificacion.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}
        End Function

        Public Function ConsultarAsigancionPerfilesNotificaciones(filtro As NotificacionGrupoUsuarios) As Respuesta(Of IEnumerable(Of DetalleGrupoUsuariosNotificaciones))
            Dim consulta = CType(_persistencia, PersistenciaPermisoGrupoUsuarios).ConsultarAsigancionPerfilesNotificaciones(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleGrupoUsuariosNotificaciones))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleGrupoUsuariosNotificaciones))(consulta)
        End Function

    End Class
End Namespace
