﻿Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Fachada.SeguridadUsuarios
Imports Softtools.GesCarga.Fachada.Basico.General
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Aplicativo.SeguridadUsuarios
    Public NotInheritable Class LogicaUsuarios
        Inherits LogicaBase(Of Usuarios)

        Private Const UsuarioActivo As Byte = 1 'Usuario Logueado
        Private Const CuentaActiva As Byte = 1
        Private Const UsuarioExterno As Byte = 1
        Private Const NumeroMaximoIntentosIngresoUsuario As Byte = 3
        Private Const UsuarioAdministrador As String = "ADMIN"
        Private Const UsuarioDeslogueo As String = "LOGOUT"
        Private HoraPermisoUsuario As String = ""
        Private Const Aplicativo As Integer = 201
        Private Const Portal As Integer = 202
        Private Const Gesphone As Integer = 203

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As PersistenciaUsuarios

        ''' <summary>
        '''  Variable de entorno para el manejo de la persistencia de datos de empresa
        ''' </summary>
        Private _persistenciaEmpresa As PersistenciaEmpresas

        Sub New(persistencia As PersistenciaUsuarios)
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As Usuarios) As Respuesta(Of IEnumerable(Of Usuarios))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of Usuarios))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of Usuarios))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As Usuarios) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long


            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            ElseIf valor.Equals(-1) Then
                Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As Usuarios) As Respuesta(Of Usuarios)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of Usuarios)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Usuarios)(consulta)
        End Function

        Public Function Validar(codigoEmpresa As Short, CodigoUsuario As String, clave As String, Token As Short) As Respuesta(Of Usuarios)
            Dim usuario = _persistencia.Validar(codigoEmpresa, CodigoUsuario)

            If IsNothing(usuario) Then
                Return New Respuesta(Of Usuarios) With {.MensajeOperacion = Recursos.UsuarioNoExiste, .ProcesoExitoso = False}
            End If


            If usuario.CodigoUsuario <> "ADMIN" Then
                If usuario.Codigo = 0 Then
                    Return New Respuesta(Of Usuarios) With {.MensajeOperacion = Recursos.UsuarioNoExiste, .ProcesoExitoso = False}
                End If
            End If
            If usuario.CodigoUsuario = "SYSTEM" Then
                Return New Respuesta(Of Usuarios) With {.MensajeOperacion = Recursos.UsuarioNoExiste, .ProcesoExitoso = False}
            End If

            Dim resultadoClave = ValidarClave(usuario, clave)

            If Not resultadoClave.ProcesoExitoso Then
                Return resultadoClave
            End If

            If Not usuario.Habilitado = CuentaActiva Then
                Return New Respuesta(Of Usuarios) With {.MensajeOperacion = Recursos.UsuarioSinHabilitar, .ProcesoExitoso = False}
            End If
            If usuario.AplicacionUsuario = Aplicativo Then
                If usuario.NumeroMaximoAplicativo <= usuario.UsuariosLogeadosAplicativo Then
                    Return New Respuesta(Of Usuarios) With {.MensajeOperacion = Recursos.UsuarioNumeroMaximoAplicativo, .ProcesoExitoso = False}
                End If
            End If
            If usuario.AplicacionUsuario = Portal Then
                If usuario.NumeroMaximoPortal <= usuario.UsuariosLogeadosPortal Then
                    Return New Respuesta(Of Usuarios) With {.MensajeOperacion = Recursos.UsuarioNumeroMaximoPortal, .ProcesoExitoso = False}
                End If
            End If
            If usuario.AplicacionUsuario = Gesphone Then
                If usuario.NumeroMaximoGesphone <= usuario.UsuariosLogeadosGesphone Then
                    Return New Respuesta(Of Usuarios) With {.MensajeOperacion = Recursos.UsuarioNumeroMaximoGesphone, .ProcesoExitoso = False}
                End If
            End If

            usuario.GruposUsuarios = _persistencia.ConsultarUsuarioGrupoUsuarios(usuario.CodigoEmpresa, usuario.Codigo)
            usuario.ConsultaModulo = New PersistenciaModuloAplicaciones().ConsultarModuloPorUsuario(usuario.CodigoEmpresa, usuario.Codigo)
            usuario.ConsultaMenu = New PersistenciaMenuAplicaciones().ConsultarMenuPorUsuario(usuario.CodigoEmpresa, usuario.Codigo)
            usuario.Oficinas = New PersistenciaOficinas().ConsultarPorUsuario(usuario.CodigoEmpresa, usuario.Codigo)

            If Token > 0 Then
                Dim UsuarioLogeado = _persistencia.GuardarIngresoUsuario(codigoEmpresa, usuario.Codigo)
            End If

            Return New Respuesta(Of Usuarios)(usuario)

        End Function

        Private Function ValidarClave(usuario As Usuarios, clave As String) As Respuesta(Of Usuarios)

            If Not usuario.Clave.Trim().Equals(clave.Trim()) Then
                Dim intentosIngreso = _persistencia.ActualizarIntentosIngreso(usuario.CodigoEmpresa, usuario.Codigo)
                If intentosIngreso = NumeroMaximoIntentosIngresoUsuario Then
                    _persistencia.DeshabilitarUsuario(usuario.CodigoEmpresa, usuario.Codigo)
                    Return New Respuesta(Of Usuarios) With {.MensajeOperacion = Recursos.UsuarioInactivoPorCantidadIntentos, .ProcesoExitoso = False}
                End If

                Return New Respuesta(Of Usuarios) With {.MensajeOperacion = Recursos.ClaveInvalida, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of Usuarios)(True)
        End Function


        Private Function ObtenerNumeroDia() As Short
            Return Convert.ToInt16(IIf(Date.Now.DayOfWeek = DayOfWeek.Sunday, 7, Date.Now.DayOfWeek))
        End Function

        Private Function ObtenerHora(horaMilitar As String) As DateTime
            Dim arrhora = Split(horaMilitar, ":")

            If arrhora(0).Equals("24") Then
                arrhora(0) = "23"
                arrhora(1) = "59"
            End If

            Dim fecha = New DateTime(Date.Now.Year, Date.Now.Month, Date.Now.Day, arrhora(0), arrhora(1), 0)
            Return fecha
        End Function

        Public Function ConsultarUsuarioGrupoSeguridades(filtro As Usuarios) As Respuesta(Of IEnumerable(Of UsuarioGrupoUsuarios))
            Dim consulta = _persistencia.ConsultarUsuarioGrupoUsuarios(filtro.CodigoEmpresa, filtro.Codigo)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of UsuarioGrupoUsuarios))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of UsuarioGrupoUsuarios))(consulta)
        End Function
        Public Function CerrarSesion(codigoEmpresa As Short, Codigo As Integer) As Respuesta(Of Usuarios)
            Dim usuario = _persistencia.CerrarSesion(codigoEmpresa, Codigo)
            If IsNothing(usuario) Then
                Return New Respuesta(Of Usuarios)(String.Format("", Codigo))
            End If
            Return New Respuesta(Of Usuarios)(usuario)
        End Function

        Public Function GuardarUsuarioGrupoSeguridades(detalle As IEnumerable(Of GrupoUsuarios)) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = _persistencia.InsertarUsuarioGrupoSeguridades(detalle)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}

        End Function





        Public Function ConsultarUsuarioOficinas(filtro As UsuarioOficinas) As Respuesta(Of IEnumerable(Of UsuarioOficinas))
            Dim consulta = _persistencia.ConsultarUsuarioOficinas(filtro.CodigoEmpresa, filtro.Codigo)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of UsuarioOficinas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of UsuarioOficinas))(consulta)
        End Function


        Public Function GuardarUsuarioOficinas(detalle As IEnumerable(Of UsuarioOficinas)) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            valor = _persistencia.InsertarUsuarioOficina(detalle)

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If
            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True}
        End Function

        Public Function RegistrarActividad(entidad As Usuarios) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaUsuarios).RegistrarActividad(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionInserto)}
        End Function

    End Class
End Namespace
