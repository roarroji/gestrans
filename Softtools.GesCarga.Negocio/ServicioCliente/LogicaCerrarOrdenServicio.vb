﻿Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Fachada.ServicioCliente
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace ServicioCliente
    Public NotInheritable Class LogicaCerrarOrdenServicio
        Inherits LogicaBase(Of CerrarOrdenServicio)

        ReadOnly _persistencia As IPersistenciaBase(Of CerrarOrdenServicio)

        Sub New(persistencia As IPersistenciaBase(Of CerrarOrdenServicio))
            _persistencia = persistencia
        End Sub


        Public Overrides Function Consultar(filtro As CerrarOrdenServicio) As Respuesta(Of IEnumerable(Of CerrarOrdenServicio))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of CerrarOrdenServicio))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of CerrarOrdenServicio))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As CerrarOrdenServicio) As Respuesta(Of CerrarOrdenServicio)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Guardar(entidad As CerrarOrdenServicio) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function

        Public Function CerrarOrdenServicio(entidad As CerrarOrdenServicio) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim cerrar As Boolean = False

            cerrar = CType(_persistencia, PersistenciaCerrarOrdenServicio).CerrarOrdenServicio(entidad)

            If Not cerrar Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.OrdenServicio.Numero, Recursos.OperacionAnulo)}
        End Function
    End Class
End Namespace

