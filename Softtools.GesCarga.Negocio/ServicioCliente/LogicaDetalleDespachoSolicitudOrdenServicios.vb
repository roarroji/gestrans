﻿Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Fachada.ServicioCliente
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace ServicioCliente
    Public NotInheritable Class LogicaDetalleDespachoSolicitudOrdenServicios
        Inherits LogicaBase(Of DetalleDespachoSolicitudOrdenServicios)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of DetalleDespachoSolicitudOrdenServicios)

        Sub New(persistencia As IPersistenciaBase(Of DetalleDespachoSolicitudOrdenServicios))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As DetalleDespachoSolicitudOrdenServicios) As Respuesta(Of IEnumerable(Of DetalleDespachoSolicitudOrdenServicios))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleDespachoSolicitudOrdenServicios))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleDespachoSolicitudOrdenServicios))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As DetalleDespachoSolicitudOrdenServicios) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As DetalleDespachoSolicitudOrdenServicios) As Respuesta(Of DetalleDespachoSolicitudOrdenServicios)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of DetalleDespachoSolicitudOrdenServicios)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of DetalleDespachoSolicitudOrdenServicios)(consulta)
        End Function

        Private Function DatosRequeridos(entidad As DetalleDespachoSolicitudOrdenServicios) As Respuesta(Of DetalleDespachoSolicitudOrdenServicios)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of DetalleDespachoSolicitudOrdenServicios) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of DetalleDespachoSolicitudOrdenServicios) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As DetalleDespachoSolicitudOrdenServicios) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaDetalleDespachoSolicitudOrdenServicios).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
    End Class

End Namespace