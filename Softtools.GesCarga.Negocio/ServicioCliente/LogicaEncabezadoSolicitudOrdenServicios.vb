﻿Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Fachada.ServicioCliente
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace ServicioCliente
    Public NotInheritable Class LogicaEncabezadoSolicitudOrdenServicios
        Inherits LogicaBase(Of EncabezadoSolicitudOrdenServicios)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of EncabezadoSolicitudOrdenServicios)


        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos del detalle.
        ''' </summary>
        ReadOnly _persistenciaDetalle As IPersistenciaBase(Of DetalleSolicitudOrdenServicios)
        ReadOnly _persistenciaDetalleDespacho As IPersistenciaBase(Of DetalleDespachoSolicitudOrdenServicios)

        Sub New(persistencia As IPersistenciaBase(Of EncabezadoSolicitudOrdenServicios), persistenciaDetalle As IPersistenciaBase(Of DetalleSolicitudOrdenServicios), persistenciaDetalleDespacho As IPersistenciaBase(Of DetalleDespachoSolicitudOrdenServicios))
            _persistencia = persistencia
            _persistenciaDetalle = persistenciaDetalle
            _persistenciaDetalleDespacho = persistenciaDetalleDespacho
        End Sub

        Public Overrides Function Consultar(filtro As EncabezadoSolicitudOrdenServicios) As Respuesta(Of IEnumerable(Of EncabezadoSolicitudOrdenServicios))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EncabezadoSolicitudOrdenServicios))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EncabezadoSolicitudOrdenServicios))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As EncabezadoSolicitudOrdenServicios) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Numero.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor <= 0 Then
                Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = False, .MensajeOperacion = Recursos.FalloOperacionGuardarRegistro}
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Numero > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As EncabezadoSolicitudOrdenServicios) As Respuesta(Of EncabezadoSolicitudOrdenServicios)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EncabezadoSolicitudOrdenServicios)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If
            If consulta.Numero > 0 Then
                If filtro.DesdeDespachoOrdenServicio Then
                    consulta.DetalleDespacho = _persistenciaDetalleDespacho.Consultar(New DetalleDespachoSolicitudOrdenServicios With {.CodigoEmpresa = consulta.CodigoEmpresa, .Numero = consulta.Numero, .Codigo = filtro.Codigo, .Pagina = filtro.Pagina, .RegistrosPagina = filtro.RegistrosPagina})
                Else
                    consulta.Detalle = _persistenciaDetalle.Consultar(New DetalleSolicitudOrdenServicios With {.CodigoEmpresa = consulta.CodigoEmpresa, .Numero = consulta.Numero})
                End If

            End If

            Return New Respuesta(Of EncabezadoSolicitudOrdenServicios)(consulta)
        End Function

        Private Function DatosRequeridos(entidad As EncabezadoSolicitudOrdenServicios) As Respuesta(Of EncabezadoSolicitudOrdenServicios)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of EncabezadoSolicitudOrdenServicios) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of EncabezadoSolicitudOrdenServicios) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As EncabezadoSolicitudOrdenServicios) As Respuesta(Of EncabezadoSolicitudOrdenServicios)
            Return New Respuesta(Of EncabezadoSolicitudOrdenServicios)(CType(_persistencia, PersistenciaEncabezadoSolicitudOrdenServicios).Anular(entidad))
        End Function

        Public Function ObtenerInformacionControlCupo(filtro As EncabezadoSolicitudOrdenServicios) As Respuesta(Of EncabezadoSolicitudOrdenServicios)
            Dim consulta As EncabezadoSolicitudOrdenServicios
            consulta = New PersistenciaEncabezadoSolicitudOrdenServicios().ObtenerInformacionControlCupo(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EncabezadoSolicitudOrdenServicios)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EncabezadoSolicitudOrdenServicios)(consulta)
        End Function

        Public Function ObtenerOrdenServicioProgramacion(filtro As EncabezadoSolicitudOrdenServicios) As Respuesta(Of EncabezadoSolicitudOrdenServicios)
            Dim consulta As EncabezadoSolicitudOrdenServicios
            consulta = New PersistenciaEncabezadoSolicitudOrdenServicios().ObtenerOrdenServicioProgramacion(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EncabezadoSolicitudOrdenServicios)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of EncabezadoSolicitudOrdenServicios)(consulta)
        End Function

    End Class

End Namespace