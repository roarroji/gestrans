﻿Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Fachada.Tesoreria
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources

Namespace Tesoreria
    Public NotInheritable Class LogicaEncabezadoDocumentoCuentas
        Inherits LogicaBase(Of EncabezadoDocumentoCuentas)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of EncabezadoDocumentoCuentas)


        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos del detalle.
        ''' </summary>
        ReadOnly _persistenciaDetalle As IPersistenciaBase(Of DetalleDocumentoCuentaComprobantes)

        Sub New(persistencia As IPersistenciaBase(Of EncabezadoDocumentoCuentas), persistenciaDetalle As IPersistenciaBase(Of DetalleDocumentoCuentaComprobantes))
            _persistencia = persistencia
            _persistenciaDetalle = persistenciaDetalle
        End Sub

        Public Overrides Function Consultar(filtro As EncabezadoDocumentoCuentas) As Respuesta(Of IEnumerable(Of EncabezadoDocumentoCuentas))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EncabezadoDocumentoCuentas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EncabezadoDocumentoCuentas))(consulta)
        End Function


        Public Function ConsultarCuentasconEgresos(filtro As EncabezadoDocumentoCuentas) As Respuesta(Of IEnumerable(Of EncabezadoDocumentoCuentas))
            Dim consulta = CType(_persistencia, PersistenciaEncabezadoDocumentoCuentas).ConsultarCuentasconEgresos(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of EncabezadoDocumentoCuentas))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of EncabezadoDocumentoCuentas))(consulta)
        End Function


        Public Function ConsultarDetalleCruceDocumentos(filtro As EncabezadoDocumentoCuentas) As Respuesta(Of IEnumerable(Of DetalleCruceDocumentoCuenta))
            Dim consulta = CType(_persistencia, PersistenciaEncabezadoDocumentoCuentas).ConsultarDetalleCruceDocumentos(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of DetalleCruceDocumentoCuenta))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of DetalleCruceDocumentoCuenta))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As EncabezadoDocumentoCuentas) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Codigo.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As EncabezadoDocumentoCuentas) As Respuesta(Of EncabezadoDocumentoCuentas)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of EncabezadoDocumentoCuentas)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If
            If consulta.Codigo > 0 Then
                consulta.Detalle = _persistenciaDetalle.Consultar(New DetalleDocumentoCuentaComprobantes With {.CodigoEmpresa = consulta.CodigoEmpresa, .Codigo = consulta.Codigo})
            End If

            Return New Respuesta(Of EncabezadoDocumentoCuentas)(consulta)
        End Function

        Private Function DatosRequeridos(entidad As EncabezadoDocumentoCuentas) As Respuesta(Of EncabezadoDocumentoCuentas)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of EncabezadoDocumentoCuentas) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of EncabezadoDocumentoCuentas) With {.ProcesoExitoso = True}

        End Function
        Public Function Anular(entidad As EncabezadoDocumentoCuentas) As Respuesta(Of Boolean)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim anulo As Boolean = False

            anulo = CType(_persistencia, PersistenciaEncabezadoDocumentoCuentas).Anular(entidad)

            If Not anulo Then
                Return New Respuesta(Of Boolean)(Recursos.FalloOperacionAnularRegistro)
            End If

            Return New Respuesta(Of Boolean)(True) With {.ProcesoExitoso = True, .MensajeOperacion = String.Format(mensajeGuardo, entidad.Codigo, Recursos.OperacionAnulo)}
        End Function
    End Class


End Namespace