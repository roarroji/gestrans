﻿Imports Softtools.GesCarga.Entidades.Utilitarios
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources


Namespace Utilitarios

    Public NotInheritable Class LogicaBandejaSalidaCorreos
        Inherits LogicaBase(Of BandejaSalidaCorreos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of BandejaSalidaCorreos)

        Sub New(persistencia As IPersistenciaBase(Of BandejaSalidaCorreos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As BandejaSalidaCorreos) As Respuesta(Of IEnumerable(Of BandejaSalidaCorreos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of BandejaSalidaCorreos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of BandejaSalidaCorreos))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As BandejaSalidaCorreos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim validarDatos = DatosRequeridos(entidad)
            Dim valor As Long

            If Not validarDatos.ProcesoExitoso Then
                Return New Respuesta(Of Long) With {.MensajeOperacion = validarDatos.MensajeOperacion, .ProcesoExitoso = validarDatos.ProcesoExitoso}
            End If

            If entidad.Id.Equals(0) Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor.Equals(0) Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Id.Equals(0), String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}
        End Function

        Public Overrides Function Obtener(filtro As BandejaSalidaCorreos) As Respuesta(Of BandejaSalidaCorreos)
            Throw New NotImplementedException()
        End Function

        Private Function DatosRequeridos(entidad As BandejaSalidaCorreos) As Respuesta(Of BandejaSalidaCorreos)
            Dim strError As String = String.Empty
            Dim bolProcesos As Boolean = True

            If IsNothing(entidad) Then
                bolProcesos = False
                strError += String.Format(Recursos.DR_GENERICO_RegistroSinInformacion, 1)
            End If

            If Not bolProcesos Then
                Return New Respuesta(Of BandejaSalidaCorreos) With {.ProcesoExitoso = False, .MensajeOperacion = strError}
            End If

            Return New Respuesta(Of BandejaSalidaCorreos) With {.ProcesoExitoso = True}

        End Function

    End Class

End Namespace

