﻿Imports Softtools.GesCarga.Entidades.Utilitarios
Imports Softtools.GesCarga.Fachada.Utilitarios
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources


Namespace Utilitarios
    Public NotInheritable Class LogicaConfiguracionServidorCorreos
        Inherits LogicaBase(Of ConfiguracionServidorCorreos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As PersistenciaConfiguracionServidorCorreos

        Sub New(persistencia As IPersistenciaBase(Of ConfiguracionServidorCorreos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As ConfiguracionServidorCorreos) As Respuesta(Of IEnumerable(Of ConfiguracionServidorCorreos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of ConfiguracionServidorCorreos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of ConfiguracionServidorCorreos))(consulta)
        End Function

        Public Overrides Function Guardar(entidad As ConfiguracionServidorCorreos) As Respuesta(Of Long)
            Dim mensajeGuardo = Recursos.RegistroGuardadoSatisfactoriamente
            Dim valor As Long

            If entidad.Codigo = 0 Then
                valor = _persistencia.Insertar(entidad)
            Else
                valor = _persistencia.Modificar(entidad)
            End If

            If valor = 0 Then
                Return New Respuesta(Of Long)(Recursos.FalloOperacionGuardarRegistro)
            End If

            Return New Respuesta(Of Long)(valor) With {.ProcesoExitoso = True, .MensajeOperacion = IIf(entidad.Codigo > 0, String.Format(mensajeGuardo, valor, Recursos.OperacionInserto), String.Format(mensajeGuardo, valor, Recursos.OperacionModifico))}

        End Function

        Public Overrides Function Obtener(filtro As ConfiguracionServidorCorreos) As Respuesta(Of ConfiguracionServidorCorreos)
            Dim consulta = _persistencia.Obtener(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of ConfiguracionServidorCorreos)() With {.MensajeOperacion = Recursos.NoSeEncontroRegistro, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of ConfiguracionServidorCorreos)(consulta)
        End Function

        Public Function EnviarCorreoPrueba(filtro As ConfiguracionServidorCorreos) As Respuesta(Of String)

            Return New Respuesta(Of String)(_persistencia.EnviarCorreoPrueba(filtro))
        End Function
    End Class
End Namespace
