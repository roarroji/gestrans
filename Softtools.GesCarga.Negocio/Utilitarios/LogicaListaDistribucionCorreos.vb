﻿Imports Softtools.GesCarga.Entidades.Utilitarios
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.IFachada
Imports Softtools.GesCarga.Mensajes.My.Resources


Namespace Utilitarios

    Public NotInheritable Class LogicaListadosAuditoriaDocumentos
        Inherits LogicaBase(Of ListadosAuditoriaDocumentos)

        ''' <summary>
        ''' Variable de entorno para el manejo de la persistencia de datos.
        ''' </summary>
        ReadOnly _persistencia As IPersistenciaBase(Of ListadosAuditoriaDocumentos)

        Sub New(persistencia As IPersistenciaBase(Of ListadosAuditoriaDocumentos))
            _persistencia = persistencia
        End Sub

        Public Overrides Function Consultar(filtro As ListadosAuditoriaDocumentos) As Respuesta(Of IEnumerable(Of ListadosAuditoriaDocumentos))
            Dim consulta = _persistencia.Consultar(filtro)

            If IsNothing(consulta) Then
                Return New Respuesta(Of IEnumerable(Of ListadosAuditoriaDocumentos))() With {.MensajeOperacion = Recursos.SinRegistros, .ProcesoExitoso = False}
            End If

            Return New Respuesta(Of IEnumerable(Of ListadosAuditoriaDocumentos))(consulta)
        End Function

        Public Overrides Function Obtener(filtro As ListadosAuditoriaDocumentos) As Respuesta(Of ListadosAuditoriaDocumentos)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Guardar(entidad As ListadosAuditoriaDocumentos) As Respuesta(Of Long)
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
