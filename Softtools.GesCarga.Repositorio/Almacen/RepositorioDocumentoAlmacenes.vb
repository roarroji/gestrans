﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Almacen
Imports System.Transactions


Namespace Almacen
    Public NotInheritable Class RepositorioDocumentoAlmacenes
        Inherits RepositorioBase(Of DocumentoAlmacenes)
        Public Overrides Function Consultar(filtro As DocumentoAlmacenes) As IEnumerable(Of DocumentoAlmacenes)
            Dim lista As New List(Of DocumentoAlmacenes)
            Dim item As DocumentoAlmacenes

            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If Not IsNothing(filtro.NombreAlmacen) And filtro.NombreAlmacen <> "" Then
                    conexion.AgregarParametroSQL("@par_Almacen", filtro.NombreAlmacen)
                End If
                If Not IsNothing(filtro.NumeroDocumento) Then
                    If filtro.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroDocumento)
                    End If
                End If
                If (filtro.FechaInicial > Date.MinValue) And (filtro.FechaFinal >= filtro.FechaInicial) Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.DateTime)
                End If
                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado.Codigo >= 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.NumeroOrdenCompra) Then
                    If filtro.NumeroOrdenCompra > 0 Then
                        conexion.AgregarParametroSQL("@par_Orden_Compra", filtro.NumeroOrdenCompra)
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If Not IsNothing(filtro.TipoDocumento) Then
                    If filtro.TipoDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_Tipo_Documento", filtro.TipoDocumento)
                    End If
                End If
                If filtro.ConsultaKardex > 0 Then
                    If Not IsNothing(filtro.NombreReferencia) And filtro.NombreReferencia <> "" Then
                        conexion.AgregarParametroSQL("@par_Nombre_Referencia", filtro.NombreReferencia)
                    End If
                    If Not IsNothing(filtro.Referencia) And filtro.Referencia <> "" Then
                        conexion.AgregarParametroSQL("@par_Referencia", filtro.Referencia)
                    End If
                    resultado = conexion.ExecuteReaderStoreProcedure("sp_consultar_kardex_almacenes")
                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("sp_consultar_encabezado_documento_almacenes")
                End If
                While resultado.Read
                    item = New DocumentoAlmacenes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DocumentoAlmacenes) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo ", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha ", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_ALMA_Codigo", entidad.Almacen.Codigo)
                    conexion.AgregarParametroSQL("@par_EOCA_Numero", entidad.NumeroOrdenCompra)
                    conexion.AgregarParametroSQL("@par_Numero_Remision ", entidad.NumeroRemision)
                    conexion.AgregarParametroSQL("@par_Nombre_Recibe ", entidad.NombreRecibe)
                    conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Valor_Total ", entidad.ValorTotal)
                    conexion.AgregarParametroSQL("@par_ALMA_Traslado", entidad.Almacentraslado)
                    conexion.AgregarParametroSQL("@par_ENDA_Traslado ", entidad.ENDATraslado)
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado.Codigo)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo ", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Responsable ", entidad.Responsable)
                    conexion.AgregarParametroSQL("@par_Nombre_Recibe_Salida ", entidad.NombreResibeSalida)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_encabezado_documento_almacenes]")


                    While resultado.Read
                        entidad.NumeroDocumento = Convert.ToInt64(resultado.Item("Numero_Documento").ToString())
                        entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                    End While

                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If
                    For Each detalle In entidad.Detalles
                        detalle.Numero = entidad.Numero
                        detalle.Almacen = entidad.Almacen
                        detalle.TipoDocumento = entidad.TipoDocumento
                        detalle.NumeroOrdenCompra = entidad.NumeroOrdenCompra
                        detalle.Estado = entidad.Estado.Codigo

                        If Not InsertarDetalle(detalle, conexion, entidad.MensajeSQL) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto And insertoRecorrido Then
                    transaccion.Complete()
                Else
                    entidad.NumeroDocumento = Cero
                End If

            End Using
            Return entidad.NumeroDocumento

        End Function
        Public Function InsertarDetalle(entidad As DetalleDocumentoAlmacenes, ByRef contextoConexion As Conexion.DataBaseFactory, ByRef MensajeInsercion As String) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ENDA_Numero", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_REAL_Codigo", entidad.ReferenciaAlmacen.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Valor_Unitario", entidad.ValorUnitario)
            contextoConexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad)
            contextoConexion.AgregarParametroSQL("@par_Valor_Total", entidad.ValorTotal)
            contextoConexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
            contextoConexion.AgregarParametroSQL("@par_EOCA_Numero", entidad.NumeroOrdenCompra)
            contextoConexion.AgregarParametroSQL("@par_ALMA_Codigo", entidad.Almacen.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoDocumento)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_detalle_documento_almacenes]")

            While resultado.Read
                inserto = IIf(resultado.Item("ENDA_Numero") > Cero, True, False)
                If Not inserto Then
                    MensajeInsercion = resultado.Item("Mensaje")
                End If
            End While


            resultado.Close()

            Return inserto
        End Function
        Public Overrides Function Modificar(entidad As DocumentoAlmacenes) As Long
            Dim modifico As Boolean = True
            Dim modificoorden As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo ", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Numero ", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Fecha ", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_ALMA_Codigo", entidad.Almacen.Codigo)
                    conexion.AgregarParametroSQL("@par_EOCA_Numero", entidad.NumeroOrdenCompra)
                    conexion.AgregarParametroSQL("@par_Numero_Remision ", entidad.NumeroRemision)
                    conexion.AgregarParametroSQL("@par_Nombre_Recibe ", entidad.NombreRecibe)
                    conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Valor_Total ", entidad.ValorTotal)
                    conexion.AgregarParametroSQL("@par_ALMA_Traslado", entidad.Almacentraslado)
                    conexion.AgregarParametroSQL("@par_ENDA_Traslado ", entidad.ENDATraslado)
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado.Codigo)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica ", entidad.UsuarioModifica.Codigo)
                    conexion.AgregarParametroSQL("@par_Responsable ", entidad.Responsable)
                    conexion.AgregarParametroSQL("@par_Nombre_Recibe_Salida ", entidad.NombreResibeSalida)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_modificar_encabezado_documento_almacenes")

                    While resultado.Read
                        entidad.NumeroDocumento = Convert.ToInt64(resultado.Item("Numero_Documento").ToString())
                        entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                    End While

                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If
                    For Each detalle In entidad.Detalles
                        detalle.Numero = entidad.Numero
                        detalle.Almacen = entidad.Almacen
                        detalle.TipoDocumento = entidad.TipoDocumento
                        detalle.NumeroOrdenCompra = entidad.NumeroOrdenCompra
                        detalle.Estado = entidad.Estado.Codigo
                        If Not InsertarDetalle(detalle, conexion, entidad.MensajeSQL) Then
                            modificoorden = False
                            modifico = False
                            Exit For
                        End If
                    Next

                End Using

                If modifico And modificoorden Then
                    transaccion.Complete()
                Else
                    entidad.NumeroDocumento = Cero
                End If

            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As DocumentoAlmacenes) As DocumentoAlmacenes
            Dim item As New DocumentoAlmacenes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                If filtro.TipoDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_obtener_encabezado_documento_almacenes")

                While resultado.Read
                    item = New DocumentoAlmacenes(resultado)
                End While

                If item.Numero > 0 Then
                    Dim DetalleDocumentoAlmacenes As New DetalleDocumentoAlmacenes With {.CodigoEmpresa = item.CodigoEmpresa, .Numero = item.Numero, .TipoDocumento = item.TipoDocumento}
                    item.Detalles = New RepositorioDetalleDocumentoAlmacenes().Consultar(DetalleDocumentoAlmacenes)
                End If

            End Using

            Return item
        End Function
        Public Function Anular(entidad As DocumentoAlmacenes) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anulacion", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo_Anula", entidad.Oficina.Codigo)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_anular_encabezado_documento_almacenes]")

                While resultado.Read
                    anulo = IIf(Convert.ToInt16(resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class
End Namespace
