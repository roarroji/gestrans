﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Almacen

Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleUbicacionAlmacenes"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleUbicacionAlmacenes
        Inherits RepositorioBase(Of DetalleUbicacionAlmacenes)

        Public Overrides Function Consultar(filtro As DetalleUbicacionAlmacenes) As IEnumerable(Of DetalleUbicacionAlmacenes)
            Dim lista As New List(Of DetalleUbicacionAlmacenes)
            Dim item As DetalleUbicacionAlmacenes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_detalle_ubicacion_referencia_almacenes]")

                While resultado.Read
                    item = New DetalleUbicacionAlmacenes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleUbicacionAlmacenes) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleUbicacionAlmacenes) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleUbicacionAlmacenes) As DetalleUbicacionAlmacenes
            Throw New NotImplementedException()
        End Function

    End Class

End Namespace