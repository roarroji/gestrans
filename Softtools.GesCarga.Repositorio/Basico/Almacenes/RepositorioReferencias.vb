﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Almacen

Namespace Basico.Almacen

    ''' <summary>
    ''' Clase <see cref="RepositorioReferencias"/>
    ''' </summary>
    Public NotInheritable Class RepositorioReferencias
        Inherits RepositorioBase(Of Referencias)

        Public Overrides Function Consultar(filtro As Referencias) As IEnumerable(Of Referencias)
            Dim lista As New List(Of Referencias)
            Dim item As Referencias

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.Nombre) Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.CodigoReferencia) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Referencia", filtro.CodigoReferencia)
                End If
                If Not String.IsNullOrWhiteSpace(filtro.Marca) Then
                    conexion.AgregarParametroSQL("@par_Marca", filtro.Marca)
                End If
                If Not IsNothing(filtro.GrupoReferencias) Then
                    If filtro.GrupoReferencias.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Grupo_Referencia", filtro.GrupoReferencias.Codigo)
                    End If
                End If
                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_referencia_almacenes]")

                While resultado.Read
                    item = New Referencias(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Referencias) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Codigo_Referencia", entidad.CodigoReferencia)
                conexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
                conexion.AgregarParametroSQL("@par_UMRA_Codigo", entidad.UnidadMedida.Codigo)
                conexion.AgregarParametroSQL("@par_GRRA_Codigo", entidad.GrupoReferencias.Codigo)
                conexion.AgregarParametroSQL("@par_UERA_Codigo", entidad.UnidadEmpaque.Codigo)
                conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPUC.Codigo)
                conexion.AgregarParametroSQL("@par_Marca", entidad.Marca)
                conexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
                conexion.AgregarParametroSQL("@par_Porcentaje_Iva", entidad.PorcentajeIva, SqlDbType.Decimal)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_referencia_almacenes")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As Referencias) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Codigo_Referencia", entidad.CodigoReferencia)
                conexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
                conexion.AgregarParametroSQL("@par_UMRA_Codigo", entidad.UnidadMedida.Codigo)
                conexion.AgregarParametroSQL("@par_GRRA_Codigo", entidad.GrupoReferencias.Codigo)
                conexion.AgregarParametroSQL("@par_UERA_Codigo", entidad.UnidadEmpaque.Codigo)
                conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPUC.Codigo)
                conexion.AgregarParametroSQL("@par_Marca", entidad.Marca)
                conexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
                conexion.AgregarParametroSQL("@par_Porcentaje_Iva", entidad.PorcentajeIva, SqlDbType.Decimal)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_referencia_almacenes")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As Referencias) As Referencias
            Dim item As New Referencias
            

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If filtro.Referencia > 0 Then
                    If Not IsNothing(filtro.Referencia) Then
                        If Not String.IsNullOrWhiteSpace(filtro.Referencia) Then
                            conexion.AgregarParametroSQL("@par_Referencia", filtro.Referencia)
                        End If
                    End If
                    If Not IsNothing(filtro.Nombre) Then
                        If Not String.IsNullOrWhiteSpace(filtro.Nombre) Then
                            conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                        End If
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_referencia_almacenes]")

                While resultado.Read
                    item = New Referencias(resultado)
                End While

            End Using
            Return item
        End Function

        Public Function Anular(entidad As Referencias) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_referencia_almacenes]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function


    End Class

End Namespace