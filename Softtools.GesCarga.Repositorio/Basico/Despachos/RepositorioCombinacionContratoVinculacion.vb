﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class RepositorioCombinacionContratoVinculacion
        Inherits RepositorioBase(Of CombinacionContratoVinculacion)

        Public Overrides Function Consultar(filtro As CombinacionContratoVinculacion) As IEnumerable(Of CombinacionContratoVinculacion)
            Dim lista As New List(Of CombinacionContratoVinculacion)
            Dim item As CombinacionContratoVinculacion
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Cliente", filtro.Cliente.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Remitente) Then
                    If filtro.Remitente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Remitente", filtro.Remitente.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Tenedor) Then
                    If filtro.Tenedor.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Tenedor", filtro.Tenedor.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Producto) Then
                    If filtro.Producto.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_PRTR_Codigo", filtro.Producto.Codigo)
                    End If
                End If
                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_combinacion_contrato_vinculacion_temporal")
                While resultado.Read
                    item = New CombinacionContratoVinculacion(resultado)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As CombinacionContratoVinculacion) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                If Not IsNothing(entidad.Cliente) Then
                    If entidad.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Cliente", entidad.Cliente.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.Remitente) Then
                    If entidad.Remitente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Remitente", entidad.Remitente.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.Tenedor) Then
                    If entidad.Tenedor.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Tenedor", entidad.Tenedor.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.Producto) Then
                    If entidad.Producto.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Producto.Codigo)
                    End If
                End If

                If Not IsNothing(entidad.TipoCobro) Then
                    If entidad.TipoCobro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CATA_TCOB_Codigo", entidad.TipoCobro.Codigo)
                    End If
                End If
                conexion.AgregarParametroSQL("@par_Valor", entidad.Valor, SqlDbType.Decimal)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_combinacion_contrato_vinculacion_temporal")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()
                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If
            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As CombinacionContratoVinculacion) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                If Not IsNothing(entidad.Cliente) Then
                    If entidad.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Cliente", entidad.Cliente.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.Remitente) Then
                    If entidad.Remitente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Remitente", entidad.Remitente.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.Tenedor) Then
                    If entidad.Tenedor.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Tenedor", entidad.Tenedor.Codigo)
                    End If
                End If
                If Not IsNothing(entidad.Producto) Then
                    If entidad.Producto.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Producto.Codigo)
                    End If
                End If

                If Not IsNothing(entidad.TipoCobro) Then
                    If entidad.TipoCobro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CATA_TCOB_Codigo", entidad.TipoCobro.Codigo)
                    End If
                End If
                conexion.AgregarParametroSQL("@par_Valor", entidad.Valor, SqlDbType.Decimal)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_combinacion_contrato_vinculacion_temporal")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()
                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If
            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As CombinacionContratoVinculacion) As CombinacionContratoVinculacion
            Dim item As New CombinacionContratoVinculacion

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_combinacion_contrato_vinculacion_temporal")
                While resultado.Read
                    item = New CombinacionContratoVinculacion(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As CombinacionContratoVinculacion) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_combinacion_contrato_vinculacion_temporal")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class
End Namespace