﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Imports Softtools.GesCarga.Repositorio.Basico.General


Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioConceptoLiquidacion"/>
    ''' </summary>

    Public NotInheritable Class RepositorioConceptoLiquidacion
        Inherits RepositorioBase(Of ConceptoLiquidacionPlanillaDespacho)
        Dim inserto As Boolean = True
        Public Overrides Function Consultar(filtro As ConceptoLiquidacionPlanillaDespacho) As IEnumerable(Of ConceptoLiquidacionPlanillaDespacho)
            Dim lista As New List(Of ConceptoLiquidacionPlanillaDespacho)
            Dim item As ConceptoLiquidacionPlanillaDespacho

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                If Len(filtro.CodigoAlterno) > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If
                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If
                If Len(filtro.Nombre) > 0 Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If
                If filtro.ConceptoSistema >= 0 Then
                    conexion.AgregarParametroSQL("@par_Concepto_Sistema", filtro.ConceptoSistema)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_conceptos_liquidacion")

                While resultado.Read
                    item = New ConceptoLiquidacionPlanillaDespacho(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ConceptoLiquidacionPlanillaDespacho) As Long

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                Dim resultado As IDataReader
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Valor_Fijo", entidad.ValorFijo)
                conexion.AgregarParametroSQL("@par_Valor_Porcentaje", entidad.ValorPorcentaje, SqlDbType.Decimal)
                conexion.AgregarParametroSQL("@par_PUC", entidad.PUC.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Operacion ", entidad.Operacion)
                conexion.AgregarParametroSQL("@par_ValorMinimo", entidad.ValorMinimo)
                conexion.AgregarParametroSQL("@par_ValorMaximo", entidad.ValorMaximo)
                conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)

                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_conceptos_liquidacion")



                While resultado.Read
                    entidad.Codigo = resultado.Item("Numero").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(0) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo


        End Function

        Public Function LimpiarImpuestosAsociados(entidad As ConceptoLiquidacionPlanillaDespacho) As Long

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_limpiar_impuestos_conceptos")


            End Using
            Return entidad.Codigo
        End Function

        Public Function AsignarImpuestos(entidad As ImpuestoConceptoLiquidacionPlanillaDespacho, Codigo As Integer) As Boolean

            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Impuesto", entidad.Codigo)


                resultado = conexion.ExecuteReaderStoreProcedure("gsp_asignar_impuestos_conceptos")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Numero").ToString()
                End While

                If entidad.Codigo > Cero Then
                    inserto = True
                Else
                    inserto = False
                End If

            End Using
            Return inserto
        End Function


        Public Function GuardarImpuestos(entidad As ConceptoLiquidacionPlanillaDespacho) As Long

            LimpiarImpuestosAsociados(entidad)
            If Not IsNothing(entidad.ListadoImpuestos) Then
                For Each impuesto In entidad.ListadoImpuestos
                    inserto = AsignarImpuestos(impuesto, entidad.Codigo)
                Next
            End If
            If inserto = True Then
                entidad.Codigo = 1
            End If



            If entidad.Codigo.Equals(Cero) Then
                Return Cero
            End If



            Return entidad.Codigo
        End Function
        Public Overrides Function Modificar(entidad As ConceptoLiquidacionPlanillaDespacho) As Long

            Dim resultado As IDataReader
            If entidad.AplicaImpuesto = Cero Then
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Valor_Fijo", entidad.ValorFijo)
                    conexion.AgregarParametroSQL("@par_Valor_Porcentaje", entidad.ValorPorcentaje, SqlDbType.Decimal)
                    conexion.AgregarParametroSQL("@par_PUC", entidad.PUC.Codigo)

                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Operacion ", entidad.Operacion)
                    conexion.AgregarParametroSQL("@par_ValorMinimo", entidad.ValorMinimo)
                    conexion.AgregarParametroSQL("@par_ValorMaximo", entidad.ValorMaximo)
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica ", entidad.UsuarioModifica.Codigo)

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_conceptos_liquidacion")

                    entidad.Codigo = Cero

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()
                End Using
            Else
                LimpiarImpuestosAsociados(entidad)
                If Not IsNothing(entidad.ListadoImpuestos) Then
                    For Each listaguiasnoselecc In entidad.ListadoImpuestos
                        inserto = AsignarImpuestos(listaguiasnoselecc, entidad.Codigo)
                    Next
                End If
                If inserto = True Then
                    entidad.Codigo = 1
                End If
            End If


            If entidad.Codigo.Equals(Cero) Then
                Return Cero
            End If



            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As ConceptoLiquidacionPlanillaDespacho) As ConceptoLiquidacionPlanillaDespacho
            Dim item As New ConceptoLiquidacionPlanillaDespacho
            Dim RepoImpuesto As New RepositorioImpuestos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_conceptos_liquidacion]")

                While resultado.Read
                    item = New ConceptoLiquidacionPlanillaDespacho(resultado)
                End While

                If filtro.Codigo > 0 Then
                    item.ListadoImpuestos = Obtener_Detalle(filtro)
                End If



            End Using


            Return item
        End Function
        Public Function Obtener_Detalle(filtro As ConceptoLiquidacionPlanillaDespacho) As IEnumerable(Of ImpuestoConceptoLiquidacionPlanillaDespacho)
            Dim lista As New List(Of ImpuestoConceptoLiquidacionPlanillaDespacho)
            Dim item As ImpuestoConceptoLiquidacionPlanillaDespacho

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_impuestos_conceptos_liquidacion")

                While resultado.Read
                    item = New ImpuestoConceptoLiquidacionPlanillaDespacho(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista

        End Function
        Public Function Anular(entidad As ConceptoLiquidacionPlanillaDespacho) As Boolean
            Dim anulo As Boolean = False
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                If entidad.AplicaImpuesto = Cero Then
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                    LimpiarImpuestosAsociados(entidad)

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_concepto_liquidacion]")

                Else

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Codigo_Impuesto", entidad.CodigoImpuesto)


                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_impuestos_conceptos]")

                End If


                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class

End Namespace