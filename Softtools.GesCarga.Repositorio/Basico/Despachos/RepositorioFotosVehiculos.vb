﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioFotosVehiculos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioFotosVehiculos
        Inherits RepositorioBase(Of FotosVehiculos)

        Public Overrides Function Consultar(filtro As FotosVehiculos) As IEnumerable(Of FotosVehiculos)
            Dim lista As New List(Of FotosVehiculos)
            Dim item As FotosVehiculos

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.CodigoVehiculo >= Cero Then
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", filtro.CodigoVehiculo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_fotos_vehiculos]")

                While resultado.Read
                    item = New FotosVehiculos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As FotosVehiculos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As FotosVehiculos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As FotosVehiculos) As FotosVehiculos
            Throw New NotImplementedException()
        End Function

        Public Function Anular(entidad As FotosVehiculos) As Boolean
            Throw New NotImplementedException()
        End Function

        Public Function InsertarTemporal(entidad As FotosVehiculos) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.CodigoVehiculo)
                    conexion.AgregarParametroSQL("@par_Nombre_Documento", entidad.NombreDocumento)
                    conexion.AgregarParametroSQL("@par_Extencion_Documento", entidad.ExtencionDocumento)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Documento", entidad.Documento, SqlDbType.Image)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_t_fotos_vehiculos]")

                    While resultado.Read
                        entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                    End While


                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        inserto = False
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using
            Return entidad.Codigo
        End Function

        Public Function EliminarTemporal(entidad As FotosVehiculos) As Boolean
            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Eliminar_Temporal", entidad.EliminarTemporal)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_t_fotos_vehiculos]")

                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using
            Return inserto
        End Function

        Public Function LimpiarTemporalUsuario(entidad As FotosVehiculos) As Boolean
            Dim inserto As Boolean = False

            Using contextoConexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                contextoConexion.CreateConnection()
                contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                contextoConexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)
                Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_limpiar_t_fotos_vehiculos]")

                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
                End While
            End Using
            Return inserto
        End Function

        Public Function TrasladarFotos(entidad As FotosVehiculos, ByRef contextoConexion As DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CreateConnection()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.CodigoVehiculo)
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_transladar_fotos_vehiculos]")

            While resultado.Read
                inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
            End While
            resultado.Close()

            Return inserto
        End Function

    End Class

End Namespace