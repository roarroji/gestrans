﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class RepositorioLegalizacionGastosRuta
        Inherits RepositorioBase(Of LegalizacionGastosRuta)

        'Public Overrides Function Insertar(entidad As LegalizacionGastosRuta) As Long

        '    Dim inserto As Long

        '    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

        '        conexion.CreateConnection()

        '        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
        '        conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
        '        conexion.AgregarParametroSQL("@par_CATA_TICO_Codigo", entidad.TipoCombustible.Codigo)
        '        conexion.AgregarParametroSQL("@par_Valor", entidad.Valor, SqlDbType.Money)
        '        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

        '        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_valor_combustibles")


        '        While resultado.Read
        '            inserto = resultado.Item("Retorno").ToString()
        '        End While

        '        resultado.Close()
        '        conexion.CloseConnection()

        '    End Using
        '    Return inserto


        'End Function

        'Public Overrides Function Modificar(entidad As LegalizacionGastosRuta) As Long
        '    Throw New NotImplementedException()
        'End Function 
        Public Overrides Function Insertar(entidad As LegalizacionGastosRuta) As Long
            Dim inserto As Boolean = False
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo.Codigo)
                conexion.AgregarParametroSQL("@par_CLGC_Codigo", entidad.Concepto.Codigo)
                conexion.AgregarParametroSQL("@par_Valor", entidad.Valor)
                conexion.AgregarParametroSQL("@par_Aplica_Anticipo", entidad.AplicaAnticipo)
                conexion.AgregarParametroSQL("@par_USUA_Crea", entidad.UsuarioCrea.Codigo)


                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_legalizacion_gastos_ruta]")
                While resultado.Read
                    inserto = IIf((resultado.Item("RUTA_Codigo")) > 0, True, False)
                End While

            End Using

            Return inserto
        End Function

        Public Overrides Function Modificar(entidad As LegalizacionGastosRuta) As Long
            Dim inserto As Boolean = False
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo.Codigo)
                conexion.AgregarParametroSQL("@par_CLGC_Codigo", entidad.Concepto.Codigo)
                conexion.AgregarParametroSQL("@par_Valor", entidad.Valor)
                conexion.AgregarParametroSQL("@par_Aplica_Anticipo", entidad.AplicaAnticipo)
                conexion.AgregarParametroSQL("@par_USUA_Modigica", entidad.UsuarioModifica.Codigo)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_Modificar_legalizacion_gastos_ruta]")
                While resultado.Read
                    inserto = IIf((resultado.Item("RUTA_Codigo")) > 0, True, False)
                End While

            End Using

            Return inserto
        End Function

        Public Overrides Function Obtener(filtro As LegalizacionGastosRuta) As LegalizacionGastosRuta
            Throw New NotImplementedException()
        End Function


        Public Overrides Function Consultar(filtro As LegalizacionGastosRuta) As IEnumerable(Of LegalizacionGastosRuta)
            Dim listaGatosRuta As New List(Of LegalizacionGastosRuta)
            Dim GastosRuta As LegalizacionGastosRuta

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Ruta) Then
                    If filtro.Ruta.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Ruta.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Concepto) Then
                    If filtro.Concepto.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_codigo_Concep", filtro.Concepto.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.TipoVehiculo) Then
                    If filtro.TipoVehiculo.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_codigo_tipoVehiculo", filtro.TipoVehiculo.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_legalizacion_gastos_ruta")

                While resultado.Read
                    GastosRuta = New LegalizacionGastosRuta(resultado)
                    listaGatosRuta.Add(GastosRuta)
                End While

            End Using


            Return listaGatosRuta

        End Function

        'Public Overrides Function Obtener(filtro As RepositorioGastosRutas) As RepositorioGastosRutas
        '    Dim item As ValorCombustible

        '    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
        '        conexion.CreateConnection()

        '        conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
        '        conexion.AgregarParametroSQL("@par_CATA_TICO_Codigo", filtro.TipoCombustible.Codigo)

        '        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_valor_combustible")

        '        While resultado.Read

        '            item = New ValorCombustible(resultado)

        '        End While

        '    End Using

        '    Return item

        'End Function

        'Public Function Anular(entidad As ValorCombustible) As Boolean
        '    Dim item As Boolean

        '    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
        '        conexion.CreateConnection()

        '        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
        '        conexion.AgregarParametroSQL("@par_CATA_TICO_Codigo", entidad.TipoCombustible.Codigo)
        '        conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
        '        conexion.AgregarParametroSQL("@par_Valor", entidad.Valor, SqlDbType.Money)

        '        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_Valor_Combustibles]")

        '        While resultado.Read

        '            item = IIf(resultado.Item("Numero") > 0, True, False)

        '        End While

        '    End Using

        '    Return item

        'End Function

        Public Function Anular(entidad As LegalizacionGastosRuta) As Boolean
            Dim item As Boolean

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo.Codigo)
                conexion.AgregarParametroSQL("@par_CLGC_Codigo", entidad.Concepto.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_gastos_ruta]")

                While resultado.Read

                    item = IIf(resultado.Item("Numero") > 0, True, False)

                End While

            End Using

            Return item

        End Function



    End Class


End Namespace


