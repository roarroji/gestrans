﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports System.Transactions

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioNovedadesDespachos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioNovedadesDespachos
        Inherits RepositorioBase(Of NovedadesDespachos)

        Public Overrides Function Consultar(filtro As NovedadesDespachos) As IEnumerable(Of NovedadesDespachos)
            Dim lista As New List(Of NovedadesDespachos)
            Dim item As NovedadesDespachos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > Cero Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                If Not IsNothing(filtro.CodigoAlterno) Then
                    If Len(filtro.CodigoAlterno) > Cero Then
                        conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                    End If
                End If
                If Not IsNothing(filtro.Nombre) Then
                    If Len(filtro.Nombre) > Cero Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If
                End If
                If filtro.Estado >= Cero Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_novedades_despachos]")

                While resultado.Read
                    item = New NovedadesDespachos(resultado)
                    If item.Codigo > 0 Then
                        item.Conceptos = ConsultarListaConceptos(item.CodigoEmpresa, item.Codigo)
                    End If
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As NovedadesDespachos) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_novedades_despachos")
                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While


                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    Else
                        Call BorrarListaConceptos(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                        If Not IsNothing(entidad.Conceptos) Then

                            entidad.Conceptos.NODECodigo = entidad.Codigo

                            If Not InsertarlistaConceptos(entidad.Conceptos, conexion) Then

                            End If

                        End If
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As NovedadesDespachos) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_novedades_despachos")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                Else
                    Call BorrarListaConceptos(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                    If Not IsNothing(entidad.Conceptos) Then

                        entidad.Conceptos.NODECodigo = entidad.Codigo

                        If Not InsertarlistaConceptos(entidad.Conceptos, conexion) Then

                        End If

                    End If
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As NovedadesDespachos) As NovedadesDespachos
            Dim item As New NovedadesDespachos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > Cero Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_novedades_despachos]")

                While resultado.Read
                    item = New NovedadesDespachos(resultado)
                End While
                resultado.Close()

                If item.Codigo > 0 Then
                    item.Conceptos = ConsultarListaConceptos(item.CodigoEmpresa, item.Codigo)
                End If

            End Using

            Return item
        End Function

        Public Function Anular(entidad As NovedadesDespachos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                Call BorrarListaConceptos(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_novedades_despachos]")


                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > Cero, True, False)
                End While
                resultado.Close()



            End Using

            Return anulo
        End Function

        Public Function BorrarListaConceptos(CodiEmpresa As Short, Cod As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean
            Try
                conexion.CleanParameters()

            Catch ex As Exception

            End Try

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_Codigo", Cod)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_lista_Conceptos]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarlistaConceptos(entidad As NovedadesConceptos, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_NODE_Codigo", entidad.NODECodigo)
            conexion.AgregarParametroSQL("@par_COVE_Codigo", entidad.COVECodigo)
            conexion.AgregarParametroSQL("@par_CLPD_Codigo", entidad.CLPDCodigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_lista_Conceptos]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function ConsultarListaConceptos(CodiEmpresa As Short, Cod As Long) As NovedadesConceptos
            Dim item As NovedadesConceptos
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", Cod)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_Conceptos]")

                While resultado.Read
                    item = New NovedadesConceptos(resultado)
                End While
                resultado.Close()
            End Using

            Return item

        End Function

    End Class

End Namespace