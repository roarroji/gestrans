﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports System.Data.OleDb
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.Despachos
    Public NotInheritable Class RepositorioPeajes
        Inherits RepositorioBase(Of Peajes)

        Public Overrides Function Consultar(filtro As Peajes) As IEnumerable(Of Peajes)
            Dim lista As New List(Of Peajes)
            Dim item As Peajes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not IsNothing(filtro.Nombre) Then
                    If Len(filtro.Nombre) > 0 Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If
                End If

                If Not IsNothing(filtro.Proveedor) Then
                    If filtro.Proveedor.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_PROV_Codigo", filtro.Proveedor.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.TipoPeaje) Then
                    If filtro.TipoPeaje.Codigo <> -1 And filtro.TipoPeaje.Codigo <> 0 Then
                        conexion.AgregarParametroSQL("@par_TIPE_Codigo", filtro.TipoPeaje.Codigo)
                    End If
                End If



                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_peajes")


                While resultado.Read
                    item = New Peajes(resultado)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Peajes) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.Codigo_Alterno)

                If Not IsNothing(entidad.Proveedor) Then
                    conexion.AgregarParametroSQL("@par_PROV_Codigo", entidad.Proveedor.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Contacto", entidad.Contacto)
                conexion.AgregarParametroSQL("@par_Telefono", entidad.Telefono)
                If Not IsNothing(entidad.Celular) Then
                    conexion.AgregarParametroSQL("@par_Celular", entidad.Celular)
                End If

                conexion.AgregarParametroSQL("@par_Ubicacion", entidad.Ubicacion)
                conexion.AgregarParametroSQL("@par_Tipo_Peaje", entidad.TipoPeaje.Codigo)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                If Not IsNothing(entidad.Longitud) Then
                    conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Float)
                End If
                If Not IsNothing(entidad.Latitud) Then
                    conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Float)
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_peajes")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As Peajes) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.Codigo_Alterno)
                If Not IsNothing(entidad.Proveedor) Then
                    conexion.AgregarParametroSQL("@par_PROV_Codigo", entidad.Proveedor.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Contacto", entidad.Contacto)
                conexion.AgregarParametroSQL("@par_Telefono", entidad.Telefono)
                If Not IsNothing(entidad.Celular) Then
                    conexion.AgregarParametroSQL("@par_Celular", entidad.Celular)
                End If
                conexion.AgregarParametroSQL("@par_Ubicacion", entidad.Ubicacion)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_Tipo_Peaje", entidad.TipoPeaje.Codigo)
                If Not IsNothing(entidad.Longitud) Then
                    conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Float)
                End If
                If Not IsNothing(entidad.Latitud) Then
                    conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Float)
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_peajes")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As Peajes) As Peajes
            Dim item As New Peajes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_peajes")

                While resultado.Read
                    item = New Peajes(resultado)
                End While

                If filtro.ConsultarTarifasPeajes Then
                    Dim listaTarifasPeajes As New List(Of TarifasPeajes)
                    Dim TarifasPeajes As TarifasPeajes
                    resultado.Close()
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_tarifas_peajes")
                    While resultado.Read
                        TarifasPeajes = New TarifasPeajes(resultado)
                        listaTarifasPeajes.Add(TarifasPeajes)
                    End While
                    item.TarifasPeajes = listaTarifasPeajes
                End If

            End Using

            Return item
        End Function

        Public Function Anular(entidad As Peajes) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_peajes")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function InsertarTarifasPeajes(entidad As Peajes) As Boolean
            Dim inserto As Boolean = True

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_PEAJ_Codigo", entidad.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_tarifas_peajes")
                resultado.Close()

                If entidad.TarifasPeajes.Count > 0 Then

                    For Each detalle In entidad.TarifasPeajes
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_PEAJ_Codigo", entidad.Codigo)
                        conexion.AgregarParametroSQL("@par_CATA_CAPE_Codigo", detalle.Categoria.Codigo)
                        conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", detalle.TipoVehiculo.Codigo)
                        conexion.AgregarParametroSQL("@par_Valor", detalle.Valor)
                        conexion.AgregarParametroSQL("@par_Valor_Remolque_Un_Eje", detalle.ValorRemolque1Eje)
                        conexion.AgregarParametroSQL("@par_Valor_Remolque_Dos_Ejes", detalle.ValorRemolque2Ejes)
                        conexion.AgregarParametroSQL("@par_Valor_Remolque_Tres_Ejes", detalle.ValorRemolque3Ejes)
                        conexion.AgregarParametroSQL("@par_Valor_Remolque_Cuatro_Ejes", detalle.ValorRemolque4Ejes)
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_tarifas_peajes")
                        While resultado.Read
                            inserto = IIf((resultado.Item("Codigo")) > 0, True, False)
                        End While
                        resultado.Close()
                        If Not inserto Then
                            Exit For
                        End If
                    Next
                End If


            End Using

            Return inserto
        End Function

        Public Function GenerarPlanitilla(filtro As Peajes) As Peajes

            Dim fullPath As String
            fullPath = System.AppDomain.CurrentDomain.BaseDirectory()
            System.IO.File.Copy(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Peajes.xlsx", fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Peajes_Generado.xlsx", True)
            Dim cn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Peajes_Generado.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES'"



            Dim Proveedores As IEnumerable(Of Terceros)
            Dim Proveedor As New Terceros
            Dim ObjProveedor As New RepositorioTerceros
            Proveedor.CodigoEmpresa = filtro.CodigoEmpresa
            Proveedor.Sync = True
            Proveedor.CadenaPerfiles = 1409
            'Proveedor.CodigoLineaNegocio = filtro.CodigoLineaNegocio
            'Proveedor.CodigoTipoLineaNegocio = filtro.CodigoTipoLineaNegocio
            'Proveedor.Estado = 1
            Proveedores = ObjProveedor.Consultar(Proveedor)

            Dim TiposPeaje As IEnumerable(Of ValorCatalogos)
            Dim TipoPeaje As New ValorCatalogos
            Dim ObjTipoPeaje As New RepositorioValorCatalogos
            TipoPeaje.CodigoEmpresa = filtro.CodigoEmpresa
            TipoPeaje.Estado = 1
            '' .Codigo = 205 = al Codigo del catalogo 
            TipoPeaje.Catalogo = New Catalogos With {.Codigo = 205}
            TiposPeaje = ObjTipoPeaje.Consultar(TipoPeaje)


            Using cnn As New OleDbConnection(cn)
                cnn.Open()
                TiposPeaje = TiposPeaje.OrderBy(Function(a) a.Nombre)
                For i = 1 To TiposPeaje.Count
                    Dim row = i + 2
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        ''''INSERT INTO [Nombre de la pestaña de excel]''
                        cmd.CommandText = "INSERT INTO [Tipo_Peaje$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = TiposPeaje(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", TiposPeaje(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    End Using
                Next

                Proveedores = Proveedores.OrderBy(Function(a) a.Nombre)
                For i = 1 To Proveedores.Count
                    Dim row = i + 2
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        ''''INSERT INTO [Nombre de la pestaña de excel]''
                        cmd.CommandText = "INSERT INTO [Proveedor$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Proveedores(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Proveedores(i - 1).NombreCompleto)
                        cmd.ExecuteNonQuery()
                    End Using

                Next

                ''''' insert de la otra pestaña 

                cnn.Close()
            End Using
            Dim RTA As New Peajes
            Dim Archivo() As Byte = IO.File.ReadAllBytes(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Peajes_Generado.xlsx")
            RTA.Planitlla = Archivo
            Return RTA

        End Function


    End Class
End Namespace

