﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioPeajesRutas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioPeajesRutas
        Inherits RepositorioBase(Of PeajesRutas)

        Public Overrides Function Consultar(filtro As PeajesRutas) As IEnumerable(Of PeajesRutas)
            Dim lista As New List(Of PeajesRutas)
            Dim item As PeajesRutas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Ruta) Then
                    If filtro.Ruta.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Ruta_Codigo", filtro.Ruta.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Peaje) Then
                    If filtro.Peaje.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Peaje_Codigo", filtro.Peaje.Codigo)
                    End If
                End If
                If filtro.Estado >= -1 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_peajes_ruta")

                While resultado.Read
                    item = New PeajesRutas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As PeajesRutas) As Long

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_PEAJ_Codigo", entidad.Peaje.Codigo)
                conexion.AgregarParametroSQL("@par_Orden", entidad.Orden)
                conexion.AgregarParametroSQL("@par_Tiempo_Estimado", entidad.DuracionHoras, SqlDbType.Real)

                Dim resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_peajes_ruta]")
                While resultado.Read
                    entidad.Codigo = resultado.Item("Numero").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As PeajesRutas) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                If Not IsNothing(entidad.TipoRuta) Then
                End If
                conexion.AgregarParametroSQL("@par_Duracion_Horas", entidad.DuracionHoras, SqlDbType.Money)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_rutas")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As PeajesRutas) As PeajesRutas
            Dim item As New PeajesRutas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_rutas]")


                While resultado.Read
                    item = New PeajesRutas(resultado)
                End While


            End Using

            Return item
        End Function
        Public Function Anular(entidad As PeajesRutas) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_rutas]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
        Public Function EliminarPeajesRutas(entidad As PeajesRutas) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                conexion.AgregarParametroSQL("@par_Peaje_Codigo", entidad.Peaje.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_peajes_ruta")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class

End Namespace