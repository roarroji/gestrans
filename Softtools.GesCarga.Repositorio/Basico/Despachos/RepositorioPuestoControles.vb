﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioPuestoControles"/>
    ''' </summary>
    Public NotInheritable Class RepositorioPuestoControles
        Inherits RepositorioBase(Of PuestoControles)

        Public Overrides Function Consultar(filtro As PuestoControles) As IEnumerable(Of PuestoControles)
            Dim lista As New List(Of PuestoControles)
            Dim item As PuestoControles

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If
                If Not IsNothing(filtro.Nombre) Then
                    If Len(filtro.Nombre) > 0 Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If
                End If
                If Not IsNothing(filtro.Proveedor) Then
                    If filtro.Proveedor.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Proveedor", filtro.Proveedor.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.TipoPuestoControl) Then
                    If filtro.TipoPuestoControl.Codigo <> -1 And filtro.TipoPuestoControl.Codigo <> 0 Then
                        conexion.AgregarParametroSQL("@par_Tipo_Puesto", filtro.TipoPuestoControl.Codigo)
                    End If
                End If
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If


                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_puesto_controles]")

                While resultado.Read
                    item = New PuestoControles(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As PuestoControles) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                If Not IsNothing(entidad.Proveedor) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Proveedor", entidad.Proveedor.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Contacto", entidad.Contacto)
                conexion.AgregarParametroSQL("@par_Telefono", entidad.Telefono)
                conexion.AgregarParametroSQL("@par_Celular", entidad.Celular)
                conexion.AgregarParametroSQL("@par_Ubicacion", entidad.Ubicacion)
                conexion.AgregarParametroSQL("@par_Tipo_puesto_control", entidad.TipoPuestoControl.Codigo)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                If Not IsNothing(entidad.Longitud) Then
                    conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Float)
                End If
                If Not IsNothing(entidad.Latitud) Then
                    conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Float)
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_puesto_controles")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As PuestoControles) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                If Not IsNothing(entidad.Proveedor) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Proveedor", entidad.Proveedor.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Contacto", entidad.Contacto)
                conexion.AgregarParametroSQL("@par_Telefono", entidad.Telefono)
                conexion.AgregarParametroSQL("@par_Celular", entidad.Celular)
                conexion.AgregarParametroSQL("@par_Ubicacion", entidad.Ubicacion)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_Tipo_puesto_control", entidad.TipoPuestoControl.Codigo)
                If Not IsNothing(entidad.Longitud) Then
                    conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Float)
                End If
                If Not IsNothing(entidad.Latitud) Then
                    conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Float)
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_puesto_controles")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As PuestoControles) As PuestoControles
            Dim item As New PuestoControles

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_puesto_controles]")

                While resultado.Read
                    item = New PuestoControles(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As PuestoControles) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_Puesto_Controles]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace