﻿Imports System.Data.OleDb
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Repositorio.Basico.General

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioRutas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioRutas
        Inherits RepositorioBase(Of Rutas)

        Public Overrides Function Consultar(filtro As Rutas) As IEnumerable(Of Rutas)
            Dim lista As New List(Of Rutas)
            Dim item As Rutas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If
                If Not IsNothing(filtro.NombreCiudadDestino) Then
                    conexion.AgregarParametroSQL("@par_Ciudad_Destino", filtro.NombreCiudadDestino)
                End If
                If Not IsNothing(filtro.NombreCiudadOrigen) Then
                    conexion.AgregarParametroSQL("@par_Ciudad_Origen", filtro.NombreCiudadOrigen)
                End If
                If Not IsNothing(filtro.Nombre) Then
                    If Len(filtro.Nombre) > 0 Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If
                End If
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                If Not IsNothing(filtro.TipoRuta) Then
                    If filtro.TipoRuta.Codigo <> 4400 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Tipo_Ruta", filtro.TipoRuta.Codigo)
                    End If
                End If
                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_rutas]")

                While resultado.Read
                    item = New Rutas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function


        Public Function ConsultarTrayectos(filtro As Rutas) As IEnumerable(Of TrayectoRuta)
            Dim lista As New List(Of TrayectoRuta)
            Dim item As TrayectoRuta

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_trayectos_ruta")

                While resultado.Read
                    item = New TrayectoRuta(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Rutas) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Origen", entidad.CiudadOrigen.Codigo)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino", entidad.CiudadDestino.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                If Not IsNothing(entidad.TipoRuta) Then
                    conexion.AgregarParametroSQL("@par_CATA_TIRU_Codigo", entidad.TipoRuta.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Duracion_Horas", entidad.DuracionHoras, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Porcentaje_Anticipo", entidad.PorcentajeAnticipo, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Kilometros", entidad.Kilometros, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Plan_Puntos", entidad.PlanPuntos, SqlDbType.Money)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_rutas")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As Rutas) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Origen", entidad.CiudadOrigen.Codigo)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino", entidad.CiudadDestino.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                If Not IsNothing(entidad.TipoRuta) Then
                    conexion.AgregarParametroSQL("@par_CATA_TIRU_Codigo", entidad.TipoRuta.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Duracion_Horas", entidad.DuracionHoras, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Porcentaje_Anticipo", entidad.PorcentajeAnticipo, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Kilometros", entidad.Kilometros, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Plan_Puntos", entidad.PlanPuntos, SqlDbType.Money)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_rutas")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As Rutas) As Rutas
            Dim item As New Rutas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_rutas]")


                While resultado.Read
                    item = New Rutas(resultado)
                End While
                If filtro.ConsultaPuestosControl Then
                    Dim lista As New List(Of PuestoControles)
                    Dim Puesto As PuestoControles
                    resultado.Close()
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_puestos_controles_rutas]")
                    While resultado.Read
                        Puesto = New PuestoControles(resultado)
                        lista.Add(Puesto)
                    End While
                    item.PuestosControl = lista
                End If

                If filtro.ConsultaLegalizacionGastosRuta Then
                    Dim listaGatosRuta As New List(Of LegalizacionGastosRuta)
                    Dim GastosRuta As LegalizacionGastosRuta
                    resultado.Close()
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_legalizacion_gastos_ruta]")
                    While resultado.Read
                        GastosRuta = New LegalizacionGastosRuta(resultado)
                        listaGatosRuta.Add(GastosRuta)
                    End While
                    item.LegalizacionGastosRuta = listaGatosRuta
                End If

                If filtro.ConsultarPeajes Then
                    Dim listaPeajeRutas As New List(Of PeajeRutas)
                    Dim PeajeRutas As PeajeRutas
                    resultado.Close()
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_peaje_rutas]")
                    While resultado.Read
                        PeajeRutas = New PeajeRutas(resultado)
                        listaPeajeRutas.Add(PeajeRutas)
                    End While
                    item.PeajeRutas = listaPeajeRutas
                End If

                item.Trayectos = ConsultarTrayectos(filtro)

            End Using

            Return item
        End Function
        Public Function Anular(entidad As Rutas) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_rutas]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function InsertarPuestosControl(entidad As Rutas) As Boolean
            Dim inserto As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_puesto_control_rutas]")
                resultado.Close()

                For Each detalle In entidad.PuestosControl
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_PUCO_Codigo", detalle.Codigo)
                    conexion.AgregarParametroSQL("@par_Orden", detalle.Orden)
                    conexion.AgregarParametroSQL("@par_Tiempo_Estimado", detalle.TiempoEstimado, SqlDbType.DateTime)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_puesto_control_rutas]")
                    While resultado.Read
                        inserto = IIf((resultado.Item("Numero")) > 0, True, False)
                    End While
                    resultado.Close()
                    If Not inserto Then
                        Exit For
                    End If
                Next

            End Using

            Return inserto
        End Function

        Public Function InsertarLegalizacionGastosRuta(entidad As Rutas) As Boolean
            Dim inserto As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_legalizacion_gastos_ruta]")
                resultado.Close()

                For Each detalle In entidad.LegalizacionGastosRuta
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", detalle.TipoVehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_CLGC_Codigo", detalle.Concepto.Codigo)
                    conexion.AgregarParametroSQL("@par_Valor", detalle.Valor)
                    conexion.AgregarParametroSQL("@par_Aplica_Anticipo", detalle.AplicaAnticipo)
                    conexion.AgregarParametroSQL("@par_USUA_Crea", entidad.UsuarioCrea.Codigo)
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_legalizacion_gastos_ruta]")
                    While resultado.Read
                        inserto = IIf((resultado.Item("RUTA_Codigo")) > 0, True, False)
                    End While
                    resultado.Close()
                    If Not inserto Then
                        Exit For
                    End If
                Next

            End Using

            Return inserto
        End Function

        Public Function InsertarPeaje(entidad As Rutas) As Boolean
            Dim inserto As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_peajes_ruta]")
                resultado.Close()

                If entidad.PeajeRutas.Count > 0 Then



                    For Each detalle In entidad.PeajeRutas
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Codigo)
                        conexion.AgregarParametroSQL("@par_PEAJ_Codigo", detalle.Peaje.Codigo)
                        conexion.AgregarParametroSQL("@par_Orden", detalle.Orden)
                        conexion.AgregarParametroSQL("@par_Tiempo_Estimado", detalle.TiempoEstimado, SqlDbType.Real)
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_peajes_ruta]")
                        While resultado.Read
                            inserto = IIf((resultado.Item("Numero")) > 0, True, False)
                        End While
                        resultado.Close()
                        If Not inserto Then
                            Exit For
                        End If
                    Next
                Else
                    inserto = True
                End If

            End Using

            Return inserto
        End Function

        Public Function GenerarPlanitilla(filtro As Rutas) As Rutas

            Dim fullPath As String
            fullPath = System.AppDomain.CurrentDomain.BaseDirectory()
            System.IO.File.Copy(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Rutas.xlsx", fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Rutas_Generado.xlsx", True)
            Dim cn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Rutas_Generado.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES'"
            Dim Ciudades As IEnumerable(Of Ciudades)
            Dim Ciudad As New Ciudades
            Dim ObjCiudad As New RepositorioCiudades
            Ciudad.CodigoEmpresa = filtro.CodigoEmpresa
            Ciudad.Estado = 1
            Ciudades = ObjCiudad.Consultar(Ciudad)

            Using cnn As New OleDbConnection(cn)
                cnn.Open()
                Ciudades = Ciudades.OrderBy(Function(a) a.Nombre)
                For i = 1 To Ciudades.Count
                    Dim row = i + 2
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        cmd.CommandText = "INSERT INTO [Ciudad$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Ciudades(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Ciudades(i - 1).NombreCompleto)
                        cmd.ExecuteNonQuery()
                    End Using
                Next
                cnn.Close()
            End Using
            Dim RTA As New Rutas
            Dim Archivo() As Byte = IO.File.ReadAllBytes(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Rutas_Generado.xlsx")
            RTA.Planitlla = Archivo
            Return RTA
        End Function


        Public Function InsertarTrayectos(entidad As Rutas) As Boolean
            Dim inserto As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_trayectos_ruta")
                resultado.Close()

                If entidad.Trayectos.Count > 0 Then



                    For Each detalle In entidad.Trayectos
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Codigo)
                        conexion.AgregarParametroSQL("@par_RUTA_Codigo_Trayecto", detalle.RutaTrayecto.Codigo)
                        conexion.AgregarParametroSQL("@par_Orden", detalle.Orden)
                        conexion.AgregarParametroSQL("@par_Porcentaje_Flete", detalle.PorcentajeFlete, SqlDbType.Decimal)
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_trayectos_ruta")
                        While resultado.Read
                            inserto = IIf((resultado.Item("Numero")) > 0, True, False)
                        End While
                        resultado.Close()
                        If Not inserto Then
                            Exit For
                        End If
                    Next
                Else
                    inserto = True
                End If

            End Using

            Return inserto
        End Function
    End Class

End Namespace