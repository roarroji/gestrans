﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Basico.Despachos
    Public NotInheritable Class RepositorioValorCombustible
        Inherits RepositorioBase(Of ValorCombustible)

        Public Overrides Function Insertar(entidad As ValorCombustible) As Long

            Dim inserto As Long

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                conexion.AgregarParametroSQL("@par_CATA_TICO_Codigo", entidad.TipoCombustible.Codigo)
                conexion.AgregarParametroSQL("@par_Valor", entidad.Valor, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_valor_combustibles")


                While resultado.Read
                    inserto = resultado.Item("Retorno").ToString()
                End While

                resultado.Close()
                conexion.CloseConnection()

            End Using
            Return inserto


        End Function

        Public Overrides Function Modificar(entidad As ValorCombustible) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Consultar(filtro As ValorCombustible) As IEnumerable(Of ValorCombustible)

            Dim lista As New List(Of ValorCombustible)
            Dim item As ValorCombustible

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Fecha_ini", filtro.FechaInicio, SqlDbType.Date)
                conexion.AgregarParametroSQL("@par_Fecha_fin", filtro.FechaFin, SqlDbType.Date)

                If filtro.TipoCombustible.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_CATA_TICO_Codigo", filtro.TipoCombustible.Codigo)
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_Valor_Combustibles")


                While resultado.Read
                    item = New ValorCombustible(resultado)
                    lista.Add(item)
                End While

                resultado.Close()
                conexion.CloseConnection()


            End Using
            Return lista

        End Function

        Public Overrides Function Obtener(filtro As ValorCombustible) As ValorCombustible
            Dim item As ValorCombustible

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CATA_TICO_Codigo", filtro.TipoCombustible.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_valor_combustible")

                While resultado.Read

                    item = New ValorCombustible(resultado)

                End While

            End Using

            Return item

        End Function

        Public Function Anular(entidad As ValorCombustible) As Boolean
            Dim item As Boolean

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_CATA_TICO_Codigo", entidad.TipoCombustible.Codigo)
                conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                conexion.AgregarParametroSQL("@par_Valor", entidad.Valor, SqlDbType.Money)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_Valor_Combustibles]")

                While resultado.Read

                    item = IIf(resultado.Item("Numero") > 0, True, False)

                End While

            End Using

            Return item

        End Function

    End Class


End Namespace


