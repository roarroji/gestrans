﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.FlotaPropia

Imports Softtools.GesCarga.Repositorio.Basico.General


Namespace Basico.FlotaPropia

    ''' <summary>
    ''' Clase <see cref="RepositorioConceptoGastos"/>
    ''' </summary>

    Public NotInheritable Class RepositorioConceptoGastos
        Inherits RepositorioBase(Of ConceptoGastos)
        Dim inserto As Boolean = True
        Public Overrides Function Consultar(filtro As ConceptoGastos) As IEnumerable(Of ConceptoGastos)
            Dim lista As New List(Of ConceptoGastos)
            Dim item As ConceptoGastos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Codigo) Then
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If
                If Not IsNothing(filtro.Nombre) Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If

                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado >= 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If filtro.ConceptoSistema > 0 Then
                    conexion.AgregarParametroSQL("@par_Concepto_Sistema", filtro.ConceptoSistema)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_conceptos_legalizacion_gastos]")



                While resultado.Read
                    item = New ConceptoGastos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista

        End Function

        Public Overrides Function Insertar(entidad As ConceptoGastos) As Long



            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                Dim resultado As IDataReader

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Operacion ", entidad.Operacion)
                If Not IsNothing(entidad.CuentaPUC) Then
                    conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPUC.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Valor_Fijo ", entidad.ValorFijo, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Valor_Porcentaje ", entidad.Porcentaje)
                conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_conceptos_legalizacion_gastos")



                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(0) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As ConceptoGastos) As Long

            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Operacion ", entidad.Operacion)
                If Not IsNothing(entidad.CuentaPUC) Then
                    conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPUC.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Valor_Fijo ", entidad.ValorFijo, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Valor_Porcentaje ", entidad.Porcentaje, SqlDbType.Decimal)
                conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica ", entidad.UsuarioModifica.Codigo)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_conceptos_legalizacion_gastos")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While
                resultado.Close()
            End Using
            If entidad.Codigo.Equals(Cero) Then
                Return Cero
            End If
            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As ConceptoGastos) As ConceptoGastos
            Dim item As New ConceptoGastos
            Dim RepoImpuesto As New RepositorioImpuestos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_conceptos_legalizacion_gastos]")

                While resultado.Read
                    item = New ConceptoGastos(resultado)
                End While



            End Using


            Return item
        End Function


        Public Function Anular(entidad As ConceptoGastos) As Boolean
            Dim anulo As Boolean = False
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)


                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_concepto_legalizacion_gastos]")



                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class

End Namespace