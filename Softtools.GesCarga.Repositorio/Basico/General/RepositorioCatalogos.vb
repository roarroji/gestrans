﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioCatalogos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioCatalogos
        Inherits RepositorioBase(Of Catalogos)

        Public Overrides Function Consultar(filtro As Catalogos) As IEnumerable(Of Catalogos)
            Dim lista As New List(Of Catalogos)
            Dim item As Catalogos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_catalogos]")

                While resultado.Read
                    item = New Catalogos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Catalogos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As Catalogos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As Catalogos) As Catalogos
            Dim item As New Catalogos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_empresas]")

                While resultado.Read
                    item = New Catalogos(resultado)
                End While

            End Using

            Return item
        End Function
    End Class

End Namespace