﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports System.Transactions


Namespace Basico.General

    Public NotInheritable Class RepositorioCodigosDaneCiudades
        Inherits RepositorioBase(Of CodigoDaneCiudades)

        Public Overrides Function Insertar(entidad As CodigoDaneCiudades) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Ciudad", entidad.Ciudad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Departamento", entidad.Departamento.Codigo)
                    conexion.AgregarParametroSQL("@par_CodigoDivision", entidad.CodigoDivision)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_codigo_municipio_dane")
                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While


                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As CodigoDaneCiudades) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Obtenido", entidad.Obtenido)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Ciudad", entidad.Ciudad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Departamento", entidad.Departamento.Codigo)
                conexion.AgregarParametroSQL("@par_CodigoDivision", entidad.CodigoDivision)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_codigo_municipio_dane")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Consultar(filtro As CodigoDaneCiudades) As IEnumerable(Of CodigoDaneCiudades)
            Dim lista As New List(Of CodigoDaneCiudades)
            Dim item As CodigoDaneCiudades

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)



                If Not IsNothing(filtro.Nombre) Then
                    If Len(filtro.Nombre) > Cero Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If
                End If

                If Not IsNothing(filtro.Departamento) Then
                    If filtro.Departamento.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Departamento", filtro.Departamento.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Ciudad) Then
                    If filtro.Ciudad.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Ciudad", filtro.Ciudad.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Codigo) Then
                    If filtro.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If Not IsNothing(filtro.Master) Then
                    If filtro.Master > Cero Then
                        conexion.AgregarParametroSQL("@par_Master", filtro.Master)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_codigos_municipios_dane")

                While resultado.Read
                    item = New CodigoDaneCiudades(resultado)
                    lista.Add(item)
                End While


            End Using

            Return lista
        End Function

        Public Overrides Function Obtener(filtro As CodigoDaneCiudades) As CodigoDaneCiudades
            Dim item As New CodigoDaneCiudades

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > Cero Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_codigo_municipio_dane")

                While resultado.Read
                    item = New CodigoDaneCiudades(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(filtro As CodigoDaneCiudades) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_codigo_municipio_dane")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class
End Namespace
