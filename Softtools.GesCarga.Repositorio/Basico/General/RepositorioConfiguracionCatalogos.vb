﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.General

    Public NotInheritable Class RepositorioConfiguracionCatalogos
        Inherits RepositorioBase(Of ConfiguracionCatalogos)
        Public Overrides Function Consultar(filtro As ConfiguracionCatalogos) As IEnumerable(Of ConfiguracionCatalogos)
            Dim lista As New List(Of ConfiguracionCatalogos)
            Dim item As ConfiguracionCatalogos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.CodigoCatalogo > 0 Then
                    conexion.AgregarParametroSQL("@par_CATA_Codigo", filtro.CodigoCatalogo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_configuracion_catalogos]")

                While resultado.Read
                    item = New ConfiguracionCatalogos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ConfiguracionCatalogos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ConfiguracionCatalogos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As ConfiguracionCatalogos) As ConfiguracionCatalogos
            Dim item As New ConfiguracionCatalogos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.CodigoCatalogo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.CodigoCatalogo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_configuracion_catalogos]")

                While resultado.Read
                    item = New ConfiguracionCatalogos(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As ConfiguracionCatalogos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_anular_encabezado_planilla_especial_pasajeros]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class
End Namespace
