﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioGestionDocumentos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioGestionDocumentos
        Inherits RepositorioBase(Of ConfiguracionGestionDocumentos)

        Public Overrides Function Consultar(filtro As ConfiguracionGestionDocumentos) As IEnumerable(Of ConfiguracionGestionDocumentos)
            Dim lista As New List(Of ConfiguracionGestionDocumentos)
            Dim item As ConfiguracionGestionDocumentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.EntidadDocumento) Then
                    conexion.AgregarParametroSQL("@par_EDGD_Codigo", filtro.EntidadDocumento.Codigo)
                End If
                If Not IsNothing(filtro.Documento) Then
                    conexion.AgregarParametroSQL("@par_DOGD_Codigo", filtro.Documento.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_gestion_documentos]")

                While resultado.Read
                    item = New ConfiguracionGestionDocumentos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ConfiguracionGestionDocumentos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ConfiguracionGestionDocumentos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As ConfiguracionGestionDocumentos) As ConfiguracionGestionDocumentos
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As ConfiguracionGestionDocumentos) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace