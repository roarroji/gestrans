﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports System.Transactions

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioImpuestos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioImpuestos
        Inherits RepositorioBase(Of Impuestos)
        Dim inserto As Boolean = True
        Public Overrides Function Consultar(filtro As Impuestos) As IEnumerable(Of Impuestos)
            Dim lista As New List(Of Impuestos)
            Dim item As Impuestos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > Cero Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                If Not IsNothing(filtro.CodigoAlterno) Then
                    If filtro.CodigoAlterno <> String.Empty Then
                        conexion.AgregarParametroSQL("@par_CodigoAlterno", filtro.CodigoAlterno)
                    End If
                End If


                If filtro.Nombre <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If

                If Not IsNothing(filtro.Tipo_Impuesto) Then
                    If filtro.Tipo_Impuesto.Codigo <> 0 Then
                        conexion.AgregarParametroSQL("@par_CATA_TIIM_Codigo", filtro.Tipo_Impuesto.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Tipo_Recaudo) Then
                    If filtro.Tipo_Recaudo.Codigo Then
                        conexion.AgregarParametroSQL("@par_CATA_TRAI_Codigo", filtro.Tipo_Recaudo.Codigo)
                    End If

                End If

                If filtro.Estado >= Cero Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado As IDataReader
                If filtro.AplicaTipoDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.CodigoTipoDocumento)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.CodigoCiudad)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_impuestos_Tipo_documento")
                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_impuestos")
                End If

                While resultado.Read
                    item = New Impuestos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Impuestos) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    If Not IsNothing(entidad.CodigoAlterno) Then
                        conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    End If

                    conexion.AgregarParametroSQL("@par_Label", entidad.Label)
                    conexion.AgregarParametroSQL("@par_CATA_TRAI_Codigo", entidad.Tipo_Recaudo.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TIIM_Codigo", entidad.Tipo_Impuesto.Codigo)
                    conexion.AgregarParametroSQL("@par_Operacion", entidad.Operacion)
                    conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.PUC.Codigo)
                    conexion.AgregarParametroSQL("@par_Valor_Tarifa", entidad.Valor_tarifa, SqlDbType.Decimal)
                    conexion.AgregarParametroSQL("@par_Valor_Base", entidad.valor_base)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.Codigo_USUA_Crea)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_impuestos")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While
                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As Impuestos) As Long

            If entidad.AplicaDetalle = Cero Then
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Label", entidad.Label)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)

                    conexion.AgregarParametroSQL("@par_CATA_TRAI_Codigo", entidad.Tipo_Recaudo.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TIIM_Codigo", entidad.Tipo_Impuesto.Codigo)
                    conexion.AgregarParametroSQL("@par_Operacion", entidad.Operacion)
                    conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.PUC.Codigo)
                    conexion.AgregarParametroSQL("@par_Valor_Tarifa", entidad.Valor_tarifa, SqlDbType.Decimal)
                    conexion.AgregarParametroSQL("@par_Valor_Base", entidad.valor_base)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modi", entidad.Codigo_USUA_Modi)
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Modificar_Impuestos")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()
                End Using
            Else
                If entidad.AplicaDetalle = 2 Then
                    If Not IsNothing(entidad.ListadoCiudades) Then
                        For Each listadoimpuestos In entidad.ListadoCiudades
                            inserto = AsignarImpuestosCiudades(listadoimpuestos, entidad)
                        Next
                    End If
                End If

                If entidad.AplicaDetalle = 3 Then
                    If Not IsNothing(entidad.ListadoDepartamentos) Then
                        For Each listadoimpuestosdepartamento In entidad.ListadoDepartamentos
                            inserto = AsignarImpuestosDepartamento(listadoimpuestosdepartamento, entidad)
                        Next
                    End If
                End If

                If inserto = True Then
                    entidad.Codigo = 1
                End If
            End If



            If entidad.Codigo.Equals(Cero) Then
                Return Cero
            End If



            Return entidad.Codigo
        End Function
        Public Function AsignarImpuestosCiudades(entidad As impuestosCiudades, Impuesto As Impuestos) As Boolean

            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Impuesto.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", Impuesto.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Ciudad", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_valor_tarifa", entidad.ValorTarifa, SqlDbType.Decimal)
                conexion.AgregarParametroSQL("@par_valor_base", entidad.ValorBase, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Codigo_Cuenta_PUC", entidad.CuentaPUC.Codigo)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_usuario", Impuesto.Codigo_USUA_Crea)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_asignar_impuesto_ciudades")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Numero").ToString()
                End While

                If entidad.Codigo > Cero Then
                    inserto = True
                Else
                    inserto = False
                End If

            End Using
            Return inserto
        End Function
        Public Function AsignarImpuestosDepartamento(entidad As ImpuestosDepartamento, Impuesto As Impuestos) As Boolean

            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", Impuesto.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", Impuesto.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Departamento", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_valor_tarifa", entidad.ValorTarifa, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_valor_base", entidad.ValorBase, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Codigo_Cuenta_PUC", entidad.CuentaPUC.Codigo)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_usuario", Impuesto.Codigo_USUA_Crea)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_asignar_impuesto_departamento")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Numero").ToString()
                End While

                If entidad.Codigo > Cero Then
                    inserto = True
                Else
                    inserto = False
                End If

            End Using
            Return inserto
        End Function

        Public Overrides Function Obtener(filtro As Impuestos) As Impuestos

            Dim item As New Impuestos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Obtener_Impuestos")

                While resultado.Read
                    item = New Impuestos(resultado)
                End While

                If filtro.Codigo > 0 Then
                    item.ListadoCiudades = Obtener_Impuesto_Ciudades(filtro)
                End If
                If filtro.Codigo > 0 Then
                    item.ListadoDepartamentos = Obtener_Impuesto_Departamento(filtro)
                End If

            End Using

            Return item

        End Function

        Public Function Obtener_Impuesto_Ciudades(filtro As Impuestos) As IEnumerable(Of impuestosCiudades)
            Dim LISTA As New List(Of impuestosCiudades)
            Dim ITEM As impuestosCiudades
            Using Conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory
                Conexion.CreateConnection()

                Conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                Conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                Conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                Conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)

                Dim resultado As IDataReader = Conexion.ExecuteReaderStoreProcedure("gsp_consultar_impuestos_ciudades")

                While resultado.Read
                    ITEM = New impuestosCiudades(resultado)
                    LISTA.Add(ITEM)
                End While

            End Using
            Return LISTA
        End Function
        Public Function Obtener_Impuesto_Departamento(filtro As Impuestos) As IEnumerable(Of ImpuestosDepartamento)
            Dim LISTA As New List(Of ImpuestosDepartamento)
            Dim ITEM As ImpuestosDepartamento
            Using Conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory
                Conexion.CreateConnection()

                Conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                Conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                Conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                Conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)

                Dim resultado As IDataReader = Conexion.ExecuteReaderStoreProcedure("gsp_consultar_impuestos_departamento")

                While resultado.Read
                    ITEM = New ImpuestosDepartamento(resultado)
                    LISTA.Add(ITEM)
                End While

            End Using
            Return LISTA
        End Function
        Public Function Anular(entidad As Impuestos) As Boolean
            Dim anulo As Boolean = False
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                If entidad.AplicaDetalle = 2 Then
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Codigo_Ciudad", entidad.CodigoCiudad)

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_impuesto_Ciudad")

                End If
                If entidad.AplicaDetalle = 3 Then
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Codigo_Departamento", entidad.CodigoDepartamento)

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_impuesto_departamento")
                Else
                    If entidad.AplicaDetalle = 0 Then
                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_Eliminar_Impuestos")
                    End If


                End If

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function LimpiarImpuestosCiudades(entidad As Impuestos) As Long

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_limpiar_impuestos_ciudades")


            End Using
            Return entidad.Codigo
        End Function

        Public Function LimpiarImpuestosDepartamentos(entidad As Impuestos) As Long

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_limpiar_impuestos_departamentos")


            End Using
            Return entidad.Codigo
        End Function
    End Class

End Namespace