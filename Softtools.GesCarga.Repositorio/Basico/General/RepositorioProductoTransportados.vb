﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports System.Transactions
Imports System.Data.OleDb

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioProductoTransportados"/>
    ''' </summary>
    Public NotInheritable Class RepositorioProductoTransportados
        Inherits RepositorioBase(Of ProductoTransportados)

        Public Overrides Function Consultar(filtro As ProductoTransportados) As IEnumerable(Of ProductoTransportados)
            Dim lista As New List(Of ProductoTransportados)
            Dim item As ProductoTransportados

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.ValorAutocomplete) Or filtro.Sync = True Then
                    conexion.AgregarParametroSQL("@par_Filtro", filtro.ValorAutocomplete)
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                    If filtro.AplicaTarifario > -1 Then
                        conexion.AgregarParametroSQL("@par_AplicaTarifario", filtro.AplicaTarifario)
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_producto_transportados_Autocomplete")
                    While resultado.Read
                        item = New ProductoTransportados(resultado)
                        lista.Add(item)
                    End While

                Else
                    If filtro.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If

                    If Not IsNothing(filtro.CodigoAlterno) Then
                        If filtro.CodigoAlterno <> "" And filtro.CodigoAlterno <> "0" Then
                            conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                        End If
                    End If

                    If Not IsNothing(filtro.Nombre) Then
                        If filtro.Nombre <> "" Then
                            conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                        End If
                    End If

                        If filtro.Estado >= Cero Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If
                    If Not IsNothing(filtro.AplicaTarifario) Then
                        If filtro.AplicaTarifario > 0 Then
                            conexion.AgregarParametroSQL("@par_Aplica_Tarifario", filtro.AplicaTarifario)
                        End If
                    End If

                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > Cero Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If
                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > Cero Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_producto_transportados]")

                    While resultado.Read
                        item = New ProductoTransportados(resultado)
                        lista.Add(item)
                    End While
                End If

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ProductoTransportados) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
                    conexion.AgregarParametroSQL("@par_UMPT_Codigo", entidad.UnidadMedida)
                    conexion.AgregarParametroSQL("@par_UEPT_Codigo", entidad.UnidadEmpaque)
                    conexion.AgregarParametroSQL("@par_CATA_LIPT_Codigo", entidad.LineaProducto)
                    conexion.AgregarParametroSQL("@par_CATA_NAPT_Codigo", entidad.NaturalezaProducto)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_PRMI_Codigo", entidad.CodigoProductoMinisterio)
                    conexion.AgregarParametroSQL("@par_AplicaTarifario", entidad.AplicaTarifario)
                    conexion.AgregarParametroSQL("@par_Peso", entidad.Peso, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@CATA_NRPR_Codigo", entidad.NivelRiesgo)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_producto_transportados")


                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As ProductoTransportados) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
                conexion.AgregarParametroSQL("@par_UMPT_Codigo", entidad.UnidadMedida)
                conexion.AgregarParametroSQL("@par_UEPT_Codigo", entidad.UnidadEmpaque)
                conexion.AgregarParametroSQL("@par_CATA_LIPT_Codigo", entidad.LineaProducto)
                conexion.AgregarParametroSQL("@par_CATA_NAPT_Codigo", entidad.NaturalezaProducto)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_PRMI_Codigo", entidad.CodigoProductoMinisterio)
                conexion.AgregarParametroSQL("@par_AplicaTarifario", entidad.AplicaTarifario)
                conexion.AgregarParametroSQL("@par_Peso", entidad.Peso, SqlDbType.Money)

                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@CATA_NRPR_Codigo", entidad.NivelRiesgo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_producto_transportados")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As ProductoTransportados) As ProductoTransportados
            Dim item As New ProductoTransportados

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > Cero Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_producto_transportados]")

                While resultado.Read
                    item = New ProductoTransportados(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As ProductoTransportados) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_producto_transportados]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function


        Public Function GenerarPlantilla(filtro As ProductoTransportados) As ProductoTransportados

            Dim fullPath As String
            fullPath = System.AppDomain.CurrentDomain.BaseDirectory()
            System.IO.File.Copy(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Productos_Transportados.xlsx", fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Productos_Transportados_Generado.xlsx", True)
            Dim cn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Productos_Transportados_Generado.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES'"
            Dim ProductosMin As IEnumerable(Of ProductosMinisterioTransporte)
            Dim Producto As New ProductosMinisterioTransporte
            Dim ObjProducto As New RepositorioProductosMinisterioTransporte
            Producto.CodigoEmpresa = filtro.CodigoEmpresa
            Producto.Estado = 1
            ProductosMin = ObjProducto.Consultar(Producto)

            Dim UnidadesEmpaque As IEnumerable(Of UnidadEmpaqueProductosTransportados)
            Dim UEmpaque As New UnidadEmpaqueProductosTransportados
            Dim ObjUEmpaque As New RepositorioUnidadEmpaqueProductosTransportados
            UEmpaque.CodigoEmpresa = filtro.CodigoEmpresa
            UEmpaque.Estado = 1
            UnidadesEmpaque = ObjUEmpaque.Consultar(UEmpaque)

            Dim UnidadesMedida As IEnumerable(Of UnidadMedida)
            Dim UMedida As New UnidadMedida
            Dim ObjUMedida As New RepositorioUnidadMedida
            UMedida.CodigoEmpresa = filtro.CodigoEmpresa
            UnidadesMedida = ObjUMedida.Consultar(UMedida)

            Dim NaturalezasProducto As IEnumerable(Of ValorCatalogos)
            Dim NProducto As New ValorCatalogos
            Dim ObjNProducto As New RepositorioValorCatalogos
            NProducto.CodigoEmpresa = filtro.CodigoEmpresa
            NProducto.Catalogo = New Catalogos With {.Codigo = 94}
            NaturalezasProducto = ObjNProducto.Consultar(NProducto)

            Dim LineasProductosTransportados As IEnumerable(Of ValorCatalogos)
            Dim LineaProducto As New ValorCatalogos
            Dim ObjLineaProducto As New RepositorioValorCatalogos
            LineaProducto.CodigoEmpresa = filtro.CodigoEmpresa
            LineaProducto.Catalogo = New Catalogos With {.Codigo = 93}
            LineasProductosTransportados = ObjLineaProducto.Consultar(LineaProducto)

            Using cnn As New OleDbConnection(cn)
                cnn.Open()
                ProductosMin = ProductosMin.OrderBy(Function(a) a.Nombre)
                For i = 1 To ProductosMin.Count
                    ' For i = 1 To 10
                    Dim row = i + 2
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        ' cmd.CommandText = "INSERT INTO [ProductosMinisterio$A1:B5463] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.CommandText = "INSERT INTO [ProductoMinisterio$] (Codigo,Nombre) values(@Cod,@nom)"
                        ' cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = ProductosMin(i - 1).CodigoAlterno
                        'cmd.Parameters.AddWithValue("@Cod", ProductosMin(i - 1).CodigoAlterno)
                        ' cmd.Parameters.AddWithValue("@nom", ProductosMin(i - 1).Nombre)
                        'cmd.Parameters.AddWithValue("@desc", ProductosMin(i - 1).Descripcion)
                        cmd.Parameters.Add("@Cod", OleDbType.VarChar, 255).Value = ProductosMin(i - 1).CodigoAlterno
                        cmd.Parameters.Add("@nom", OleDbType.VarChar, 255).Value = ProductosMin(i - 1).Nombre

                        ' cmd.Parameters.Add("@Cod", OleDbType.VarChar, 255).Value = "1"
                        ' cmd.Parameters.Add("@nom", OleDbType.VarChar, 255).Value = "dssfdsfs"

                        cmd.ExecuteNonQuery()
                    End Using
                Next

                UnidadesEmpaque = UnidadesEmpaque.OrderBy(Function(a) a.Nombre)
                For i = 1 To UnidadesEmpaque.Count
                    Dim rowue = i + 2
                    Using cmdue As OleDbCommand = cnn.CreateCommand()
                        cmdue.CommandText = "INSERT INTO [UnidadesEmpaque$] (Codigo,Nombre) values(@Cod,@nom)"

                        cmdue.Parameters.Add("@Cod", OleDbType.Integer, 6).Value = UnidadesEmpaque(i - 1).Codigo
                        cmdue.Parameters.Add("@nom", OleDbType.VarChar, 255).Value = UnidadesEmpaque(i - 1).Nombre

                        cmdue.ExecuteNonQuery()
                    End Using
                Next

                UnidadesMedida = UnidadesMedida.OrderBy(Function(a) a.Nombre)
                For i = 1 To UnidadesMedida.Count
                    Dim rowum = i + 2
                    Using cmdum As OleDbCommand = cnn.CreateCommand()
                        cmdum.CommandText = "INSERT INTO [UnidadesMedida$] (Codigo,Nombre) values(@Cod,@nom)"

                        cmdum.Parameters.Add("@Cod", OleDbType.Integer, 6).Value = UnidadesMedida(i - 1).Codigo
                        cmdum.Parameters.Add("@nom", OleDbType.VarChar, 255).Value = UnidadesMedida(i - 1).Nombre

                        cmdum.ExecuteNonQuery()
                    End Using
                Next

                NaturalezasProducto = NaturalezasProducto.OrderBy(Function(a) a.Nombre)
                For i = 1 To NaturalezasProducto.Count
                    Dim rownp = i + 2
                    Using cmdnp As OleDbCommand = cnn.CreateCommand()
                        cmdnp.CommandText = "INSERT INTO [NaturalezaProducto$] (Codigo,Nombre) values(@Cod,@nom)"

                        cmdnp.Parameters.Add("@Cod", OleDbType.Integer, 6).Value = NaturalezasProducto(i - 1).Codigo
                        cmdnp.Parameters.Add("@nom", OleDbType.VarChar, 255).Value = NaturalezasProducto(i - 1).Nombre

                        cmdnp.ExecuteNonQuery()
                    End Using
                Next

                LineasProductosTransportados = LineasProductosTransportados.OrderBy(Function(a) a.Nombre)
                For i = 1 To LineasProductosTransportados.Count
                    Dim rowlp = i + 2
                    Using cmdlp As OleDbCommand = cnn.CreateCommand()
                        cmdlp.CommandText = "INSERT INTO [LineaProductosTransportados$] (Codigo,Nombre) values(@Cod,@nom)"

                        cmdlp.Parameters.Add("@Cod", OleDbType.Integer, 6).Value = LineasProductosTransportados(i - 1).Codigo
                        cmdlp.Parameters.Add("@nom", OleDbType.VarChar, 255).Value = LineasProductosTransportados(i - 1).Nombre

                        cmdlp.ExecuteNonQuery()
                    End Using
                Next

                cnn.Close()
                    End Using
                    Dim RTA As New ProductoTransportados
            Dim Archivo() As Byte = IO.File.ReadAllBytes(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Productos_Transportados_Generado.xlsx")
            RTA.Plantilla = Archivo
            Return RTA
        End Function

    End Class

End Namespace