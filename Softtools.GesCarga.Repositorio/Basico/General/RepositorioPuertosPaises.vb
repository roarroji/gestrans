﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioPuertosPaises"/>
    ''' </summary>
    Public NotInheritable Class RepositorioPuertosPaises
        Inherits RepositorioBase(Of PuertosPaises)

        Public Overrides Function Consultar(filtro As PuertosPaises) As IEnumerable(Of PuertosPaises)
            Dim lista As New List(Of PuertosPaises)
            Dim item As PuertosPaises

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                If Not IsNothing(filtro.Nombre) Then
                    If Len(filtro.Nombre) > 0 Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If
                End If
                If Not IsNothing(filtro.Pais) Then
                    If Len(filtro.Pais.Nombre) > 0 Then
                        conexion.AgregarParametroSQL("@par_Nombre_Pais", filtro.Pais.Nombre)
                    End If
                End If
                If filtro.Estado > -1 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_puertos_paises]")

                While resultado.Read
                    item = New PuertosPaises(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As PuertosPaises) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Contacto", entidad.Contacto)
                conexion.AgregarParametroSQL("@par_PAIS_Codigo", entidad.Pais.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre_Ciudad", entidad.NombreCiudad)
                conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                conexion.AgregarParametroSQL("@par_Telefono", entidad.Telefono)
                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_puertos_paises")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As PuertosPaises) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Contacto", entidad.Contacto)
                conexion.AgregarParametroSQL("@par_PAIS_Codigo", entidad.Pais.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre_Ciudad", entidad.NombreCiudad)
                conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                conexion.AgregarParametroSQL("@par_Telefono", entidad.Telefono)
                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Modifica", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_puertos_paises")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As PuertosPaises) As PuertosPaises
            Dim item As New PuertosPaises

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_puertos_paises]")

                While resultado.Read
                    item = New PuertosPaises(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function Anular(entidad As PuertosPaises) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_puertos_paises]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class

End Namespace