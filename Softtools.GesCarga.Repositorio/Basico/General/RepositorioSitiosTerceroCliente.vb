﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Operacion

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioSitiosTerceroCliente"/>
    ''' </summary>
    Public NotInheritable Class RepositorioSitiosTerceroCliente
        Inherits RepositorioBase(Of SitiosTerceroCliente)

        Public Overrides Function Consultar(filtro As SitiosTerceroCliente) As IEnumerable(Of SitiosTerceroCliente)
            Dim lista As New List(Of SitiosTerceroCliente)
            Dim item As SitiosTerceroCliente

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.Cliente.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_terceros_sitios_cargue_descargue]")

                While resultado.Read
                    item = New SitiosTerceroCliente(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ConsultarOrdenServicio(filtro As SitiosTerceroCliente) As IEnumerable(Of SitiosTerceroCliente)
            Dim lista As New List(Of SitiosTerceroCliente)
            Dim item As SitiosTerceroCliente

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.ValorAutocomplete) Or filtro.Sync = True Then
                    conexion.AgregarParametroSQL("@par_Filtro", filtro.ValorAutocomplete)
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                    If Not IsNothing(filtro.Cliente) Then
                        If filtro.Cliente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.Cliente.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.CiudadCargue) Then
                        If filtro.CiudadCargue.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.CiudadCargue.Codigo)
                        End If
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_orden_servicio_sitios_cargue_descargue_autocomplete")
                    While resultado.Read
                        item = New SitiosTerceroCliente(resultado)
                        lista.Add(item)
                    End While

                Else
                    If Not IsNothing(filtro.Cliente) Then
                        If filtro.Cliente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.Cliente.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.CiudadCargue) Then
                        If filtro.CiudadCargue.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.CiudadCargue.Codigo)
                        End If
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_orden_servicio_sitios_cargue_descargue]")

                    While resultado.Read
                        item = New SitiosTerceroCliente(resultado)
                        lista.Add(item)
                    End While
                End If

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As SitiosTerceroCliente) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As SitiosTerceroCliente) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As SitiosTerceroCliente) As SitiosTerceroCliente
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace