﻿Imports System.Data.OleDb
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Basico.Operacion
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Repositorio.Basico.Tesoreria
Imports Softtools.GesCarga.Repositorio.Comercial.Documentos

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioTerceros"/>
    ''' </summary>
    Public NotInheritable Class RepositorioTerceros
        Inherits RepositorioBase(Of Terceros)

        Public Overrides Function Consultar(filtro As Terceros) As IEnumerable(Of Terceros)
            Dim lista As New List(Of Terceros)
            Dim item As Terceros

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.TarifarioProveedores Then

                    conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.Codigo)
                    conexion.AgregarParametroSQL("@par_LNTC_Codigo", filtro.CodigoLineaNegocio)
                    conexion.AgregarParametroSQL("@par_TLNC_Codigo", filtro.CodigoTipoLineaNegocio)
                    If Not IsNothing(filtro.TarifaCarga) Then
                        conexion.AgregarParametroSQL("@par_TATC_Codigo", filtro.TarifaCarga.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_RUTA_Codigo", filtro.CodigoRuta)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_tarifario_proveedores")
                    While resultado.Read
                        item = New Terceros(resultado, False, filtro.TarifarioProveedores, False)
                        lista.Add(item)
                    End While

                ElseIf filtro.TarifarioClientes Then

                    'Si no envía el cliente consulta el tarifario base
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_LNTC_Codigo", filtro.CodigoLineaNegocio)
                    If filtro.CodigoTipoLineaNegocio > 0 Then
                        conexion.AgregarParametroSQL("@par_TLNC_Codigo", filtro.CodigoTipoLineaNegocio)

                    End If
                    If filtro.CodigoCiudadOrigen > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Origen", filtro.CodigoCiudadOrigen)
                    End If
                    If filtro.CodigoCiudadDestino > 0 Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino", filtro.CodigoCiudadDestino)
                    End If
                    If filtro.CodigoFormaPago > 0 Then
                        conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", filtro.CodigoFormaPago)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_tarifario_clientes")
                    While resultado.Read
                        item = New Terceros(resultado, False, False, filtro.TarifarioClientes)
                        lista.Add(item)
                    End While
                    'Autocompletes 
                ElseIf Not IsNothing(filtro.ValorAutocomplete) Or filtro.Sync = True Then
                    conexion.AgregarParametroSQL("@par_Filtro", filtro.ValorAutocomplete)
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                    If Not IsNothing(filtro.CadenaPerfiles) Then
                        If Integer.Parse(filtro.CadenaPerfiles) <> 1400 Then
                            conexion.AgregarParametroSQL("@par_Perfil_Tercero", Integer.Parse(filtro.CadenaPerfiles))
                        End If
                    End If
                    Dim resultado As IDataReader
                    If filtro.ConsultaConductoreslegalizacion > 0 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_terceros_Autocomplete_legalizacion_gastos_varios_conductores")
                    Else
                        If Not IsNothing(filtro.Estado) Then
                            conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                        End If

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_terceros_Autocomplete")
                    End If
                    While resultado.Read
                        item = New Terceros(resultado)
                        lista.Add(item)
                    End While

                Else
                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If
                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > 0 Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                    If Not String.IsNullOrWhiteSpace(filtro.NumeroIdentificacion) Then
                        If filtro.NumeroIdentificacion <> 0 Then
                            conexion.AgregarParametroSQL("@par_Numero_Identificacion", filtro.NumeroIdentificacion)
                        End If
                    End If
                    If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                        If filtro.CodigoAlterno <> 0 Then
                            conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                        End If
                    End If
                    If Not String.IsNullOrWhiteSpace(filtro.NombreTercero) Then
                        conexion.AgregarParametroSQL("@par_Nombre_Tercero", filtro.NombreTercero)
                    End If
                    If Not String.IsNullOrWhiteSpace(filtro.NombreCiudad) Then
                        conexion.AgregarParametroSQL("@par_Nombre_Ciudad", filtro.NombreCiudad)
                    End If
                    If Not IsNothing(filtro.Estado) Then
                        If filtro.Estado.Codigo > -1 Then 'NO APLICA
                            conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.CadenaPerfiles) Then
                        If Integer.Parse(filtro.CadenaPerfiles) <> 1400 Then
                            conexion.AgregarParametroSQL("@par_Perfil_Tercero", Integer.Parse(filtro.CadenaPerfiles))
                        End If
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_terceros")
                    While resultado.Read
                        item = New Terceros(resultado)
                        lista.Add(item)
                    End While

                End If

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Terceros) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                Dim item = New Terceros
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Filtro", entidad.NumeroIdentificacion)
                Dim resultadoConsulta As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_tercero_x_identificacion")
                While resultadoConsulta.Read
                    item = New Terceros(resultadoConsulta)
                End While
                resultadoConsulta.Close()
                If item.Codigo > 0 Then
                    conexion.CleanParameters()
                    conexion.CloseConnection()
                    entidad.Codigo = item.Codigo
                    Return Modificar(entidad)
                Else
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno ", entidad.CodigoAlterno) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Codigo_Contable ", entidad.CodigoContable) 'VARCHAR
                    If Not IsNothing(entidad.TipoNaturaleza) Then
                        conexion.AgregarParametroSQL("@par_CATA_TINT_Codigo ", entidad.TipoNaturaleza.Codigo) 'NUMERIC
                    End If
                    If Not IsNothing(entidad.Pais) Then
                        conexion.AgregarParametroSQL("@par_PAIS_Codigo ", entidad.Pais.Codigo) 'NUMERIC
                    End If
                    If Not IsNothing(entidad.TipoIdentificacion) Then
                        conexion.AgregarParametroSQL("@par_CATA_TIID_Codigo ", entidad.TipoIdentificacion.Codigo) 'NUMERIC
                    End If
                    conexion.AgregarParametroSQL("@par_Numero_Identificacion ", entidad.NumeroIdentificacion) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Digito_Chequeo ", entidad.DigitoChequeo) 'SMALLINT
                    conexion.AgregarParametroSQL("@par_Razon_Social ", entidad.RazonSocial) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Representante_Legal ", entidad.RepresentanteLegal) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Nombre ", entidad.Nombre) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Apellido1 ", entidad.PrimeroApellido) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Apellido2 ", entidad.SegundoApellido) 'VARCHAR
                    If Not IsNothing(entidad.Sexo) Then
                        conexion.AgregarParametroSQL("@par_CATA_SETE_Codigo ", entidad.Sexo.Codigo) 'NUMERIC
                    End If
                    If Not IsNothing(entidad.CiudadExpedicionIdentificacion) Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Identificacion ", entidad.CiudadExpedicionIdentificacion.Codigo) 'NUMERIC
                    End If
                    If Not IsNothing(entidad.CiudadNacimiento) Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Nacimiento ", entidad.CiudadNacimiento.Codigo) 'NUMERIC
                    End If
                    If Not IsNothing(entidad.Ciudad) Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Direccion ", entidad.Ciudad.Codigo) 'NUMERIC
                    End If
                    conexion.AgregarParametroSQL("@par_Direccion ", entidad.Direccion) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Barrio ", entidad.Barrio) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Codigo_Postal ", entidad.CodigoPostal) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Telefonos ", entidad.Telefonos) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Celulares ", entidad.Celular) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Emails ", entidad.Correo) 'VARCHAR

                    If Len(entidad.CorreoFacturacion) > 0 Then
                        conexion.AgregarParametroSQL("@par_Correo_Factura_Electronica", entidad.CorreoFacturacion)
                    End If

                    If Not IsNothing(entidad.Banco) Then
                        conexion.AgregarParametroSQL("@par_BANC_Codigo ", entidad.Banco.Codigo) 'NUMERIC
                    End If
                    If Not IsNothing(entidad.TipoBanco) Then
                        conexion.AgregarParametroSQL("@par_CATA_TICB_Codigo ", entidad.TipoBanco.Codigo) 'NUMERIC
                    End If
                    conexion.AgregarParametroSQL("@par_Numero_Cuenta_Bancaria ", entidad.CuentaBancaria) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Titular_Cuenta_Bancaria ", entidad.TitularCuentaBancaria) 'VARCHAR
                    If Not IsNothing(entidad.Beneficiario) Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Beneficiario ", entidad.Beneficiario.Codigo) 'NUMERIC
                    End If
                    conexion.AgregarParametroSQL("@par_Foto ", entidad.Foto, SqlDbType.Image) 'IMAGE
                    conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones) 'VARCHAR

                    conexion.AgregarParametroSQL("@par_Justificacion_Bloqueo ", entidad.JustificacionBloqueo) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Reporto_Contabilidad ", entidad.ReportoContabilidad) 'SMALLINT
                    conexion.AgregarParametroSQL("@par_Codigo_Retorno_Contabilidad ", entidad.CodigoRetornoContabilidad) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Mensaje_Retorno_Contabilidad ", entidad.MensajeRetornoContabilidad) 'VARCHAR
                    If Not IsNothing(entidad.Estado) Then
                        If entidad.Estado.Codigo > 0 Then 'NO APLICA
                            conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                        End If
                    End If 'SMALLINT
                    If Not IsNothing(entidad.TipoAnticipo) Then
                        conexion.AgregarParametroSQL("@par_CATA_TIAN_Codigo ", entidad.TipoAnticipo.Codigo) 'NUMERIC
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuaCodigoCrea) 'SMALLINT
                    conexion.AgregarParametroSQL("@par_Perfiles", entidad.CadenaPerfiles) 'VARCHAR


                    conexion.AgregarParametroSQL("@Codigo ", entidad.Codigo) 'NUMERIC
                    '- -Datos Conductor
                    If Not IsNothing(entidad.Conductor) Then
                        If Not IsNothing(entidad.Conductor.TipoContrato) Then
                            conexion.AgregarParametroSQL("@par_CATA_TICO_Codigo ", entidad.Conductor.TipoContrato.Codigo) 'NUMERIC
                        End If
                        If Not IsNothing(entidad.Conductor.TipoSangre) Then
                            conexion.AgregarParametroSQL("@par_CATA_TISA_Codigo ", entidad.Conductor.TipoSangre.Codigo) 'NUMERIC
                        End If
                        If Not IsNothing(entidad.Conductor.CategoriaLicencia) Then
                            conexion.AgregarParametroSQL("@par_CATA_CALC_Codigo ", entidad.Conductor.CategoriaLicencia.Codigo) 'NUMERIC
                        End If
                        conexion.AgregarParametroSQL("@par_Numero_Licencia ", entidad.Conductor.NumeroLicencia) 'VARCHAR
                        If entidad.Conductor.FechaVencimiento > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Vencimiento_Licencia ", entidad.Conductor.FechaVencimiento, SqlDbType.DateTime) 'DATE
                        End If
                        If Not IsNothing(entidad.Conductor.ViajesConductor) Then
                            conexion.AgregarParametroSQL("@par_CATA_CCCV_Codigo", entidad.Conductor.ViajesConductor.Codigo) 'NUMERIC
                        End If


                        conexion.AgregarParametroSQL("@par_Conductor_Propio ", entidad.Conductor.Propio) 'SMALLINT
                        conexion.AgregarParametroSQL("@par_Conductor_Afiliado ", entidad.Conductor.Afiliado) 'SMALLINT
                        conexion.AgregarParametroSQL("@par_BloqueadoAltoRiesgo", entidad.Conductor.BloqueadoAltoRiesgo) 'SMALLINT

                        If Not IsNothing(entidad.Conductor.FechaUltimoViaje) Then
                            If entidad.Conductor.FechaUltimoViaje > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Ultimo_Viaje ", entidad.Conductor.FechaUltimoViaje, SqlDbType.DateTime) 'DATE
                            End If
                        End If
                        conexion.AgregarParametroSQL("@par_Referencias_Personales", entidad.Conductor.ReferenciasPersonales) 'SMALLINT


                    End If
                    '--Datos Empleado
                    If Not IsNothing(entidad.Empleado) Then


                        If Not IsNothing(entidad.Empleado.FechaVinculacion) Then
                            If entidad.Empleado.FechaVinculacion > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Vinculacion ", entidad.Empleado.FechaVinculacion, SqlDbType.DateTime) 'DATE
                            End If
                        End If
                        If Not IsNothing(entidad.Empleado.FechaFinalizacion) Then
                            If entidad.Empleado.FechaFinalizacion > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Finalizacion ", entidad.Empleado.FechaFinalizacion, SqlDbType.DateTime) 'DATE
                            End If
                        End If
                        If Not IsNothing(entidad.Empleado.TipoContrato) Then
                            conexion.AgregarParametroSQL("@par_CATA_TICE_Codigo ", entidad.Empleado.TipoContrato.Codigo) 'SMALLINT
                        End If
                        If Not IsNothing(entidad.Empleado.Cargo) Then
                            conexion.AgregarParametroSQL("@par_CATA_CARG_Codigo ", entidad.Empleado.Cargo.Codigo) 'SMALLINT
                        End If
                        conexion.AgregarParametroSQL("@par_Salario ", entidad.Empleado.Salario) 'MONEY
                        conexion.AgregarParametroSQL("@par_Valor_Auxilio_Transporte ", entidad.Empleado.ValorAuxilioTransporte) 'MONEY
                        conexion.AgregarParametroSQL("@par_Valor_Seguridad_Social ", entidad.Empleado.ValorSeguridadSocial) 'MONEY
                        conexion.AgregarParametroSQL("@par_Valor_Aporte_Parafiscales ", entidad.Empleado.ValorAporteParafiscales) 'MONEY
                        conexion.AgregarParametroSQL("@par_Porcentaje_Comision ", entidad.Empleado.PorcentajeComision) 'NUMERIC
                        conexion.AgregarParametroSQL("@par_Valor_Seguro_Vida ", entidad.Empleado.ValorSeguroVida) 'MONEY
                        conexion.AgregarParametroSQL("@par_Valor_Provision_Prestaciones_Sociales ", entidad.Empleado.ValorProvisionPrestacionesSociales) 'MONEY
                        conexion.AgregarParametroSQL("@par_Empleado_Externo ", entidad.Empleado.Externo) 'SMALLINT
                        If Not IsNothing(entidad.Empleado.Departamento) Then
                            conexion.AgregarParametroSQL("@par_CATA_DEEM_Codigo ", entidad.Empleado.Departamento.Codigo) 'SMALLINT
                        End If
                    End If
                    '- -Datos Cliente
                    If Not IsNothing(entidad.Cliente) Then
                        If Not IsNothing(entidad.Cliente.FormaPago) Then
                            conexion.AgregarParametroSQL("@par_CATA_FPCL_Codigo ", entidad.Cliente.FormaPago.Codigo) 'NUMERIC
                        End If

                        If Not IsNothing(entidad.Cliente.BloquearDespachos) Then
                            conexion.AgregarParametroSQL("@par_BloquearDespachos", entidad.Cliente.BloquearDespachos) 'NUMERIC
                        End If

                        If Not IsNothing(entidad.Cliente.RepresentanteComercial) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Comercial ", entidad.Cliente.RepresentanteComercial.Codigo) 'NUMERIC
                        End If
                        conexion.AgregarParametroSQL("@par_Dias_Plazo_Pago ", entidad.Cliente.DiasPlazo) 'NUMERIC
                        If Not IsNothing(entidad.Cliente.Tarifario) Then
                            conexion.AgregarParametroSQL("@par_ETCV_Numero ", entidad.Cliente.Tarifario.Codigo) 'NUMERIC
                        End If
                        If Not IsNothing(entidad.TipoValidacionCupo) Then
                            conexion.AgregarParametroSQL("@par_CATA_TIVC_Codigo", entidad.TipoValidacionCupo.Codigo) 'NUMERIC
                        End If

                        conexion.AgregarParametroSQL("@par_Cupo", entidad.Cupo) 'NUMER
                        conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo) 'NUMERIC

                        If entidad.Cliente.MargenUtilidad > 0 Then
                            conexion.AgregarParametroSQL("@par_Margen_Utilidad", entidad.Cliente.MargenUtilidad, SqlDbType.Decimal)
                        End If

                        If entidad.Cliente.ManejaCondicionesPesoCumplido > 0 Then
                            conexion.AgregarParametroSQL("@par_Maneja_Condiciones_Peso_Cumplido", SI_APLICA)
                        End If

                        If entidad.Cliente.CondicionesComercialesTarifas > 0 Then
                            conexion.AgregarParametroSQL("@par_Maneja_Condiciones_Comerciales_Tarifas", SI_APLICA)
                        End If

                        If entidad.Cliente.GestionDocumentos > 0 Then
                            conexion.AgregarParametroSQL("@par_Maneja_Gestion_Documentos_Cliente", SI_APLICA)
                        End If

                        If Not IsNothing(entidad.Cliente.AnalistaCartera) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Analista_Cartera", entidad.Cliente.AnalistaCartera.Codigo)
                        End If

                        If entidad.Cliente.DiasCierreFacturacion > 0 Then
                            conexion.AgregarParametroSQL("@par_Dia_Cierre_Facturacion", entidad.Cliente.DiasCierreFacturacion)
                        End If

                        If Not IsNothing(entidad.Cliente.ModalidadFacturacionPeso) Then
                            If entidad.Cliente.ModalidadFacturacionPeso.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_CATA_MFPC_Codigo", entidad.Cliente.ModalidadFacturacionPeso.Codigo)
                            End If
                        End If



                    End If
                    '- -Datos Proveedor
                    If Not IsNothing(entidad.Proveedor) Then
                        If Not IsNothing(entidad.Proveedor.Tarifario) Then
                            conexion.AgregarParametroSQL("@par_ETCC_Numero ", entidad.Proveedor.Tarifario.Codigo) 'NUMERIC
                        End If
                        If Not IsNothing(entidad.Proveedor.FormaCobro) Then
                            conexion.AgregarParametroSQL("@par_CATA_FOCO_Codigo ", entidad.Proveedor.FormaCobro.Codigo) 'NUMERIC
                        End If
                        conexion.AgregarParametroSQL("@par_Dias_Plazo_Cobro ", entidad.Proveedor.DiasPlazo) 'NUMERIC
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_terceros")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()
                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    Else
                        Call BorrarSitiosTerceroCliente(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                        If Not IsNothing(entidad.SitiosTerceroCliente) Then
                            For Each SitiosTerceroCliente In entidad.SitiosTerceroCliente
                                SitiosTerceroCliente.CodigoEmpresa = entidad.CodigoEmpresa
                                SitiosTerceroCliente.Codigo = entidad.Codigo
                                If Not IsNothing(SitiosTerceroCliente.SitioCliente) Then
                                    If Not InsertarSitiosTerceroCliente(SitiosTerceroCliente, conexion) Then
                                        Exit For
                                    End If
                                End If
                            Next
                        End If
                        'Call BorrarDirecciones(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                        If Not IsNothing(entidad.Direcciones) Then
                            For Each Direccion In entidad.Direcciones
                                Direccion.CodigoEmpresa = entidad.CodigoEmpresa
                                Direccion.Tercero = New Tercero With {.Codigo = entidad.Codigo}
                                If Not InsertarDirecciones(Direccion, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If
                        Call BorrarImpuestos(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                        If Not IsNothing(entidad.Cliente) Then


                            Dim CodiEmpresa As Short
                            CodiEmpresa = entidad.CodigoEmpresa
                            Dim CodTercero As Double
                            CodTercero = entidad.Codigo
                            Dim CadenaFormasPago As String
                            CadenaFormasPago = entidad.CadenaFormasPago

                            If CadenaFormasPago.Length > 0 Then
                                InsertarMediosPagos(CodiEmpresa, CodTercero, CadenaFormasPago)
                            End If

                            If Not IsNothing(entidad.Cliente.Impuestos) Then
                                If entidad.Cliente.Impuestos.Count > 0 Then
                                    For Each Impuesto In entidad.Cliente.Impuestos
                                        Impuesto.CodigoEmpresa = entidad.CodigoEmpresa
                                        Impuesto.CodigoTercero = entidad.Codigo
                                        If Not InsertarImpuestosTerceros(Impuesto, conexion) Then
                                            Exit For
                                        End If

                                    Next
                                End If
                            End If
                        End If
                        If Not IsNothing(entidad.Proveedor) Then
                            If Not IsNothing(entidad.Proveedor.Impuestos) Then
                                If entidad.Proveedor.Impuestos.Count > 0 Then
                                    For Each Impuesto In entidad.Proveedor.Impuestos
                                        Impuesto.CodigoEmpresa = entidad.CodigoEmpresa
                                        Impuesto.CodigoTercero = entidad.Codigo
                                        If Not InsertarImpuestosTerceros(Impuesto, conexion) Then
                                            Exit For
                                        End If

                                    Next
                                End If
                            End If
                        End If
                        Call BorrarListasCorreos(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                        If Not IsNothing(entidad.ListadoCorreos) Then
                            For Each ListadoCorreos In entidad.ListadoCorreos

                                ListadoCorreos.CodigoEmpresa = entidad.CodigoEmpresa
                                ListadoCorreos.Tercero = New Tercero With {.Codigo = entidad.Codigo}

                                If Not Insertarlistascorreos(ListadoCorreos, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If
                        If Not IsNothing(entidad.LineaServicio) Then
                            For Each Linea In entidad.LineaServicio
                                Linea.CodigoEmpresa = entidad.CodigoEmpresa
                                If Not InsertarlineasServicio(Linea, entidad.Codigo, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If

                        If Not IsNothing(entidad.TerceroClienteCupoSedes) Then
                            Call BorrarClienteCupoSede(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                            For Each CupoOficina In entidad.TerceroClienteCupoSedes
                                CupoOficina.CodigoEmpresa = entidad.CodigoEmpresa
                                CupoOficina.Tercero = New Tercero With {.Codigo = entidad.Codigo}
                                If Not InsertarClienteCupoSede(CupoOficina, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If

                        If Not IsNothing(entidad.CondicionesPeso) Then
                            If entidad.CondicionesPeso.Count() > 0 Then
                                Call BorrarCondicionesPeso(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                            End If
                            For Each TercCondicinesPeso In entidad.CondicionesPeso
                                TercCondicinesPeso.CodigoEmpresa = entidad.CodigoEmpresa
                                TercCondicinesPeso.Tercero = New Tercero With {.Codigo = entidad.Codigo}
                                TercCondicinesPeso.UsuarioCodigoCrea = entidad.UsuaCodigoCrea
                                If Not InsertarCondicionesPeso(TercCondicinesPeso, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If
                        If Not IsNothing(entidad.Cliente) Then
                            If Not IsNothing(entidad.Cliente.CondicionesComerciales) Then
                                If entidad.Cliente.CondicionesComerciales.Count() > 0 Then
                                    Call BorrarCondicionesComercialesTercero(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                                End If
                                For Each TercCondicionComercial In entidad.Cliente.CondicionesComerciales
                                    If Not InsertarCondicionComercialTerceroCliente(entidad, TercCondicionComercial, conexion) Then
                                        Exit For
                                    End If
                                Next
                            End If

                            If Not IsNothing(entidad.Cliente.ListaGestionDocumentos) Then
                                If entidad.Cliente.ListaGestionDocumentos.Count() > 0 Then
                                    Call BorrarGestionDocumentosCliente(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                                End If
                                For Each GestionDocumento In entidad.Cliente.ListaGestionDocumentos
                                    If Not InsertarGestionDocumentosCliente(entidad, GestionDocumento, conexion) Then
                                        Exit For
                                    End If
                                Next
                            End If
                        End If

                        conexion.CloseConnection()
                        Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                        If Not IsNothing(entidad.Documentos) Then
                            For Each detalle In entidad.Documentos
                                detalle.CodigoTerceros = entidad.Codigo
                                detalle.UsuarioCrea = New Usuarios With {.Codigo = entidad.UsuaCodigoCrea}
                                If Not Documento.InsertarDocumentoTercero(detalle) Then
                                    Exit For
                                End If

                            Next
                        End If
                        If Not IsNothing(entidad.ListadoFOTOS) Then
                            For Each foto In entidad.ListadoFOTOS
                                foto.CodigoTerceros = entidad.Codigo
                                foto.UsuarioCrea = New Usuarios With {.Codigo = entidad.UsuaCodigoCrea}
                                If Not Documento.InsertarFotosTercero(foto) Then

                                End If
                            Next
                        End If
                    End If
                    Dim Correos As New Repositorio.Utilitarios.RepositorioCorreos
                    Correos.GenerarCorreo(entidad)
                    Return entidad.Codigo
                End If
            End Using
        End Function

        Public Function InsertarMediosPagos(CodiEmpresa As Short, CodTercero As Long,
                                            CadenaFormasPago As String) As Long

            Dim inserto As Short
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)
                conexion.AgregarParametroSQL("@par_FormasPago", CadenaFormasPago) 'VARCHAR

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_InserUpdate_Formas_Pago")

                While resultado.Read
                    inserto = resultado.Item("Numero")
                End While
            End Using
            Return inserto
        End Function


        Public Overrides Function Modificar(entidad As Terceros) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT
                conexion.AgregarParametroSQL("@par_Codigo_Alterno ", entidad.CodigoAlterno) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Codigo_Contable ", entidad.CodigoContable) 'VARCHAR
                If Not IsNothing(entidad.Pais) Then
                    conexion.AgregarParametroSQL("@par_PAIS_Codigo ", entidad.Pais.Codigo) 'VARCHAR
                End If
                If Not IsNothing(entidad.TipoNaturaleza) Then
                    conexion.AgregarParametroSQL("@par_CATA_TINT_Codigo ", entidad.TipoNaturaleza.Codigo) 'NUMERIC
                End If
                If Not IsNothing(entidad.TipoIdentificacion) Then
                    conexion.AgregarParametroSQL("@par_CATA_TIID_Codigo ", entidad.TipoIdentificacion.Codigo) 'NUMERIC
                End If
                conexion.AgregarParametroSQL("@par_Numero_Identificacion ", entidad.NumeroIdentificacion) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Digito_Chequeo ", entidad.DigitoChequeo) 'SMALLINT
                conexion.AgregarParametroSQL("@par_Razon_Social ", entidad.RazonSocial) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Representante_Legal ", entidad.RepresentanteLegal) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Nombre ", entidad.Nombre) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Apellido1 ", entidad.PrimeroApellido) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Apellido2 ", entidad.SegundoApellido) 'VARCHAR
                If Not IsNothing(entidad.Sexo) Then
                    conexion.AgregarParametroSQL("@par_CATA_SETE_Codigo ", entidad.Sexo.Codigo) 'NUMERIC
                End If
                If Not IsNothing(entidad.CiudadExpedicionIdentificacion) Then
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Identificacion ", entidad.CiudadExpedicionIdentificacion.Codigo) 'NUMERIC
                End If
                If Not IsNothing(entidad.CiudadNacimiento) Then
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Nacimiento ", entidad.CiudadNacimiento.Codigo) 'NUMERIC
                End If
                If Not IsNothing(entidad.Ciudad) Then
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Direccion ", entidad.Ciudad.Codigo) 'NUMERIC
                End If
                conexion.AgregarParametroSQL("@par_Direccion ", entidad.Direccion) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Barrio ", entidad.Barrio) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Codigo_Postal ", entidad.CodigoPostal) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Telefonos ", entidad.Telefonos) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Celulares ", entidad.Celular) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Emails ", entidad.Correo) 'VARCHAR
                If Len(entidad.CorreoFacturacion) > 0 Then
                    conexion.AgregarParametroSQL("@par_Correo_Factura_Electronica", entidad.CorreoFacturacion)
                End If

                If Not IsNothing(entidad.Banco) Then
                    conexion.AgregarParametroSQL("@par_BANC_Codigo ", entidad.Banco.Codigo) 'NUMERIC
                End If
                If Not IsNothing(entidad.TipoBanco) Then
                    conexion.AgregarParametroSQL("@par_CATA_TICB_Codigo ", entidad.TipoBanco.Codigo) 'NUMERIC
                End If
                conexion.AgregarParametroSQL("@par_Numero_Cuenta_Bancaria ", entidad.CuentaBancaria) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Titular_Cuenta_Bancaria ", entidad.TitularCuentaBancaria) 'VARCHAR
                If Not IsNothing(entidad.Beneficiario) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Beneficiario ", entidad.Beneficiario.Codigo) 'NUMERIC
                End If
                conexion.AgregarParametroSQL("@par_Foto ", entidad.Foto, SqlDbType.Image) 'IMAGE
                conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Justificacion_Bloqueo ", entidad.JustificacionBloqueo) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Reporto_Contabilidad ", entidad.ReportoContabilidad) 'SMALLINT
                conexion.AgregarParametroSQL("@par_Codigo_Retorno_Contabilidad ", entidad.CodigoRetornoContabilidad) 'VARCHAR
                conexion.AgregarParametroSQL("@par_Mensaje_Retorno_Contabilidad ", entidad.MensajeRetornoContabilidad) 'VARCHAR
                If Not IsNothing(entidad.Estado) Then
                    If entidad.Estado.Codigo > 0 Then 'NO APLICA
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                    End If
                End If 'SMALLINT
                If Not IsNothing(entidad.TipoAnticipo) Then
                    conexion.AgregarParametroSQL("@par_CATA_TIAN_Codigo ", entidad.TipoAnticipo.Codigo) 'NUMERIC
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuaCodigoCrea) 'SMALLINT
                conexion.AgregarParametroSQL("@par_Perfiles  ", entidad.CadenaPerfiles) 'VARCHAR


                conexion.AgregarParametroSQL("@Codigo ", entidad.Codigo) 'NUMERIC
                '- -Datos Conductor
                If Not IsNothing(entidad.Conductor) Then
                    If Not IsNothing(entidad.Conductor.TipoContrato) Then
                        conexion.AgregarParametroSQL("@par_CATA_TICO_Codigo ", entidad.Conductor.TipoContrato.Codigo) 'NUMERIC
                    End If
                    If Not IsNothing(entidad.Conductor.TipoSangre) Then
                        conexion.AgregarParametroSQL("@par_CATA_TISA_Codigo ", entidad.Conductor.TipoSangre.Codigo) 'NUMERIC
                    End If
                    If Not IsNothing(entidad.Conductor.CategoriaLicencia) Then
                        conexion.AgregarParametroSQL("@par_CATA_CALC_Codigo ", entidad.Conductor.CategoriaLicencia.Codigo) 'NUMERIC
                    End If
                    If Not IsNothing(entidad.Conductor.ViajesConductor) Then
                        conexion.AgregarParametroSQL("@par_CATA_CCCV_Codigo ", entidad.Conductor.ViajesConductor.Codigo) 'NUMERIC
                    End If

                    conexion.AgregarParametroSQL("@par_Numero_Licencia ", entidad.Conductor.NumeroLicencia) 'VARCHAR
                    conexion.AgregarParametroSQL("@par_Fecha_Vencimiento_Licencia ", entidad.Conductor.FechaVencimiento, SqlDbType.Date) 'DATE
                    conexion.AgregarParametroSQL("@par_Conductor_Propio", entidad.Conductor.Propio) 'SMALLINT
                    conexion.AgregarParametroSQL("@par_Conductor_Afiliado", entidad.Conductor.Afiliado) 'SMALLINT
                    conexion.AgregarParametroSQL("@par_BloqueadoAltoRiesgo", entidad.Conductor.BloqueadoAltoRiesgo) 'SMALLINT
                    If entidad.Conductor.FechaUltimoViaje > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Ultimo_Viaje ", entidad.Conductor.FechaUltimoViaje, SqlDbType.Date) 'DATE
                    End If
                    conexion.AgregarParametroSQL("@par_Referencias_Personales", entidad.Conductor.ReferenciasPersonales) 'SMALLINT
                End If
                '--Datos Empleado
                If Not IsNothing(entidad.Empleado) Then
                    conexion.AgregarParametroSQL("@par_Fecha_Vinculacion ", entidad.Empleado.FechaVinculacion, SqlDbType.Date) 'DATE
                    conexion.AgregarParametroSQL("@par_Fecha_Finalizacion ", entidad.Empleado.FechaFinalizacion, SqlDbType.Date) 'DATE
                    If Not IsNothing(entidad.Empleado.TipoContrato) Then
                        conexion.AgregarParametroSQL("@par_CATA_TICE_Codigo ", entidad.Empleado.TipoContrato.Codigo) 'SMALLINT
                    End If
                    If Not IsNothing(entidad.Empleado.Cargo) Then
                        conexion.AgregarParametroSQL("@par_CATA_CARG_Codigo ", entidad.Empleado.Cargo.Codigo) 'SMALLINT
                    End If
                    conexion.AgregarParametroSQL("@par_Salario ", entidad.Empleado.Salario) 'MONEY
                    conexion.AgregarParametroSQL("@par_Valor_Auxilio_Transporte ", entidad.Empleado.ValorAuxilioTransporte) 'MONEY
                    conexion.AgregarParametroSQL("@par_Valor_Seguridad_Social ", entidad.Empleado.ValorSeguridadSocial) 'MONEY
                    conexion.AgregarParametroSQL("@par_Valor_Aporte_Parafiscales ", entidad.Empleado.ValorAporteParafiscales) 'MONEY
                    conexion.AgregarParametroSQL("@par_Porcentaje_Comision ", entidad.Empleado.PorcentajeComision) 'NUMERIC
                    conexion.AgregarParametroSQL("@par_Valor_Seguro_Vida ", entidad.Empleado.ValorSeguroVida) 'MONEY
                    conexion.AgregarParametroSQL("@par_Valor_Provision_Prestaciones_Sociales ", entidad.Empleado.ValorProvisionPrestacionesSociales) 'MONEY
                    conexion.AgregarParametroSQL("@par_Empleado_Externo ", entidad.Empleado.Externo) 'SMALLINT
                    If Not IsNothing(entidad.Empleado.Departamento) Then
                        conexion.AgregarParametroSQL("@par_CATA_DEEM_Codigo ", entidad.Empleado.Departamento.Codigo) 'SMALLINT
                    End If
                End If
                '- -Datos Cliente
                If Not IsNothing(entidad.Cliente) Then

                    If Not IsNothing(entidad.Cliente.FormaPago) Then
                        conexion.AgregarParametroSQL("@par_CATA_FPCL_Codigo", entidad.Cliente.FormaPago.Codigo) 'NUMERIC
                        'conexion.AgregarParametroSQL("@CATA_FPVE_Codigo", entidad.Cliente.FormaPago.Codigo) 'NUMERIC
                    End If


                    If Not IsNothing(entidad.Cliente.BloquearDespachos) Then
                        conexion.AgregarParametroSQL("@par_BloquearDespachos", entidad.Cliente.BloquearDespachos) 'NUMERIC
                    End If

                    If Not IsNothing(entidad.Cliente.RepresentanteComercial) Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Comercial ", entidad.Cliente.RepresentanteComercial.Codigo) 'NUMERIC
                    End If
                    conexion.AgregarParametroSQL("@par_Dias_Plazo_Pago ", entidad.Cliente.DiasPlazo) 'NUMERIC
                    If Not IsNothing(entidad.Cliente.Tarifario) Then
                        conexion.AgregarParametroSQL("@par_ETCV_Numero ", entidad.Cliente.Tarifario.Codigo) 'NUMERIC
                    End If
                    If Not IsNothing(entidad.TipoValidacionCupo) Then
                        conexion.AgregarParametroSQL("@par_CATA_TIVC_Codigo", entidad.TipoValidacionCupo.Codigo) 'NUMERIC
                    End If
                    conexion.AgregarParametroSQL("@par_Cupo", entidad.Cupo) 'NUMER
                    conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo) 'NUMERIC

                    If entidad.Cliente.MargenUtilidad > 0 Then
                        conexion.AgregarParametroSQL("@par_Margen_Utilidad", entidad.Cliente.MargenUtilidad, SqlDbType.Decimal)
                    End If

                    If entidad.Cliente.ManejaCondicionesPesoCumplido > 0 Then
                        conexion.AgregarParametroSQL("@par_Maneja_Condiciones_Peso_Cumplido", SI_APLICA)
                    End If

                    If entidad.Cliente.CondicionesComercialesTarifas > 0 Then
                        conexion.AgregarParametroSQL("@par_Maneja_Condiciones_Comerciales_Tarifas", SI_APLICA)
                    End If

                    If entidad.Cliente.GestionDocumentos > 0 Then
                        conexion.AgregarParametroSQL("@par_Maneja_Gestion_Documentos_Cliente", SI_APLICA)
                    End If

                    If Not IsNothing(entidad.Cliente.AnalistaCartera) Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Analista_Cartera", entidad.Cliente.AnalistaCartera.Codigo)
                    End If

                    If entidad.Cliente.DiasCierreFacturacion > 0 Then
                        conexion.AgregarParametroSQL("@par_Dia_Cierre_Facturacion", entidad.Cliente.DiasCierreFacturacion)
                    End If

                    If Not IsNothing(entidad.Cliente.ModalidadFacturacionPeso) Then
                        If entidad.Cliente.ModalidadFacturacionPeso.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_CATA_MFPC_Codigo", entidad.Cliente.ModalidadFacturacionPeso.Codigo)
                        End If
                    End If



                End If
                '- -Datos Proveedor
                If Not IsNothing(entidad.Proveedor) Then
                    If Not IsNothing(entidad.Proveedor.Tarifario) Then
                        conexion.AgregarParametroSQL("@par_ETCC_Numero ", entidad.Proveedor.Tarifario.Codigo) 'NUMERIC
                    End If
                    If Not IsNothing(entidad.Proveedor.FormaCobro) Then
                        conexion.AgregarParametroSQL("@par_CATA_FOCO_Codigo ", entidad.Proveedor.FormaCobro.Codigo) 'NUMERIC
                    End If
                    conexion.AgregarParametroSQL("@par_Dias_Plazo_Cobro ", entidad.Proveedor.DiasPlazo) 'NUMERIC

                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_terceros")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()
                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                Else
                    Call BorrarSitiosTerceroCliente(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                    If Not IsNothing(entidad.SitiosTerceroCliente) Then
                        For Each SitioTerceroCliente In entidad.SitiosTerceroCliente
                            SitioTerceroCliente.CodigoEmpresa = entidad.CodigoEmpresa
                            SitioTerceroCliente.SitioCliente = New Entidades.Basico.Despachos.SitiosCargueDescargue With {.Codigo = SitioTerceroCliente.Codigo}
                            SitioTerceroCliente.Codigo = entidad.Codigo
                            If Not IsNothing(SitioTerceroCliente.SitioCliente) Then
                                If Not InsertarSitiosTerceroCliente(SitioTerceroCliente, conexion) Then
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                    If Not IsNothing(entidad.SitiosTerceroClienteModifica) Then
                        For Each SitioTerceroCliente In entidad.SitiosTerceroClienteModifica
                            SitioTerceroCliente.CodigoEmpresa = entidad.CodigoEmpresa
                            SitioTerceroCliente.SitioCliente = New Entidades.Basico.Despachos.SitiosCargueDescargue With {.Codigo = SitioTerceroCliente.Codigo}
                            SitioTerceroCliente.Codigo = entidad.Codigo
                            If Not IsNothing(SitioTerceroCliente.SitioCliente) Then
                                If Not ModificarSitiosTerceroCliente(SitioTerceroCliente, conexion) Then
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                    'Call BorrarDirecciones(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                    If Not IsNothing(entidad.Direcciones) Then
                        For Each Direccion In entidad.Direcciones
                            Direccion.CodigoEmpresa = entidad.CodigoEmpresa
                            Direccion.Tercero = New Tercero With {.Codigo = entidad.Codigo}
                            If Not InsertarDirecciones(Direccion, conexion) Then
                                Exit For
                            End If
                        Next
                    End If
                    Call BorrarImpuestos(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                    If Not IsNothing(entidad.Cliente) Then
                        Dim CodiEmpresa As Short
                        CodiEmpresa = entidad.CodigoEmpresa
                        Dim CodTercero As Double
                        CodTercero = entidad.Codigo
                        Dim CadenaFormasPago As String
                        CadenaFormasPago = entidad.CadenaFormasPago

                        If CadenaFormasPago.Length > 0 Then
                            InsertarMediosPagos(CodiEmpresa, CodTercero, CadenaFormasPago)
                        End If



                        If Not IsNothing(entidad.Cliente.Impuestos) Then
                            If entidad.Cliente.Impuestos.Count > 0 Then
                                For Each Impuesto In entidad.Cliente.Impuestos
                                    Impuesto.CodigoEmpresa = entidad.CodigoEmpresa
                                    Impuesto.CodigoTercero = entidad.Codigo
                                    If Not InsertarImpuestosTerceros(Impuesto, conexion) Then
                                        Exit For
                                    End If

                                Next
                            End If
                        End If
                    End If
                    If Not IsNothing(entidad.Proveedor) Then
                        If Not IsNothing(entidad.Proveedor.Impuestos) Then
                            If entidad.Proveedor.Impuestos.Count > 0 Then
                                For Each Impuesto In entidad.Proveedor.Impuestos
                                    Impuesto.CodigoEmpresa = entidad.CodigoEmpresa
                                    Impuesto.CodigoTercero = entidad.Codigo
                                    If Not InsertarImpuestosTerceros(Impuesto, conexion) Then
                                        Exit For
                                    End If

                                Next
                            End If
                        End If
                    End If
                    Call BorrarListasCorreos(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                    If Not IsNothing(entidad.ListadoCorreos) Then
                        For Each ListadoCorreos In entidad.ListadoCorreos

                            ListadoCorreos.CodigoEmpresa = entidad.CodigoEmpresa
                            ListadoCorreos.Tercero = New Tercero With {.Codigo = entidad.Codigo}

                            If Not Insertarlistascorreos(ListadoCorreos, conexion) Then
                                Exit For
                            End If
                        Next
                    End If
                    If Not IsNothing(entidad.LineaServicio) Then
                        For Each Linea In entidad.LineaServicio
                            Linea.CodigoEmpresa = entidad.CodigoEmpresa
                            If Not InsertarlineasServicio(Linea, entidad.Codigo, conexion) Then
                                Exit For
                            End If
                        Next
                    End If

                    If Not IsNothing(entidad.TerceroClienteCupoSedes) Then
                        Call BorrarClienteCupoSede(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                        For Each CupoOficina In entidad.TerceroClienteCupoSedes
                            CupoOficina.CodigoEmpresa = entidad.CodigoEmpresa
                            CupoOficina.Tercero = New Tercero With {.Codigo = entidad.Codigo}
                            If Not InsertarClienteCupoSede(CupoOficina, conexion) Then
                                Exit For
                            End If
                        Next
                    End If

                    If Not IsNothing(entidad.CondicionesPeso) Then
                        If entidad.CondicionesPeso.Count() > 0 Then
                            Call BorrarCondicionesPeso(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                        End If
                        For Each TercCondicinesPeso In entidad.CondicionesPeso
                            TercCondicinesPeso.CodigoEmpresa = entidad.CodigoEmpresa
                            TercCondicinesPeso.Tercero = New Tercero With {.Codigo = entidad.Codigo}
                            TercCondicinesPeso.UsuarioCodigoCrea = entidad.UsuaCodigoCrea
                            If Not InsertarCondicionesPeso(TercCondicinesPeso, conexion) Then
                                Exit For
                            End If
                        Next
                    End If

                    If Not IsNothing(entidad.Cliente) Then
                        If Not IsNothing(entidad.Cliente.CondicionesComerciales) Then
                            If entidad.Cliente.CondicionesComerciales.Count() > 0 Then
                                Call BorrarCondicionesComercialesTercero(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                            End If
                            For Each TercCondicionComercial In entidad.Cliente.CondicionesComerciales
                                If Not InsertarCondicionComercialTerceroCliente(entidad, TercCondicionComercial, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If

                        If Not IsNothing(entidad.Cliente.ListaGestionDocumentos) Then
                            If entidad.Cliente.ListaGestionDocumentos.Count() > 0 Then
                                Call BorrarGestionDocumentosCliente(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                            End If
                            For Each GestionDocumento In entidad.Cliente.ListaGestionDocumentos
                                If Not InsertarGestionDocumentosCliente(entidad, GestionDocumento, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If
                    End If

                    conexion.CloseConnection()
                    Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                    If Not IsNothing(entidad.Documentos) Then
                        For Each detalle In entidad.Documentos
                            detalle.CodigoTerceros = entidad.Codigo
                            If Not Documento.InsertarDocumentoTercero(detalle) Then
                                Exit For
                            End If

                        Next
                    End If
                    Call Documento.EliminarFotos(entidad.CodigoEmpresa, entidad.Codigo)
                    If Not IsNothing(entidad.ListadoFOTOS) Then
                        For Each foto In entidad.ListadoFOTOS
                            foto.CodigoTerceros = entidad.Codigo
                            If Not Documento.InsertarFotosTercero(foto) Then

                            End If
                        Next
                    End If
                End If

            End Using

            Return entidad.Codigo
        End Function


        Public Function GuardarNovedad(entidad As Terceros) As Long
            Dim inserto As Short
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_NOTE_Codigo", entidad.Novedad.Codigo)
                conexion.AgregarParametroSQL("@par_Justificacion", entidad.JustificacionNovedad)
                conexion.AgregarParametroSQL("@par_Responsable", entidad.UsuarioConsulta.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_novedad_tercero")

                While resultado.Read
                    inserto = resultado.Item("Numero")
                End While
            End Using
            Return inserto
        End Function
        Public Overrides Function Obtener(filtro As Terceros) As Terceros
            Dim item As New Terceros
            Dim ListaPerfiles As New List(Of Perfil)
            Dim ListaFormaPago As New List(Of FormasPago)
            Dim Perfil As New Perfil
            Dim FormaPago As New FormasPago

            Dim Doc As New Documentos
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                ElseIf Not IsNothing(filtro.NumeroIdentificacion) Then
                    conexion.AgregarParametroSQL("@par_Identificacion", filtro.NumeroIdentificacion)
                End If

                Dim Reader As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_terceros]")

                While Reader.Read
                    item = New Terceros(Reader)
                End While
                Reader.Close()
                If filtro.SoloCondicionesPesoCumplido > 0 Then
                    If item.Codigo > 0 Then
                        item.CondicionesPeso = ConsultarCondicionesPeso(item.CodigoEmpresa, item.Codigo, conexion, Reader)
                    End If
                Else
                    ''Consulta Los perfiles del tercero
                    Reader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_perfiles]")
                    While Reader.Read
                        Perfil = New Perfil(Reader)
                        ListaPerfiles.Add(Perfil)
                    End While


                    item.Perfiles = ListaPerfiles
                    Reader.Close()
                    If item.Perfiles.Count > 0 Then
                        For Each lista In item.Perfiles
                            'Cliente
                            If lista.Codigo = 1401 Then

                                Reader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_Formas_Pago]")
                                While Reader.Read
                                    FormaPago = New FormasPago(Reader)
                                    ListaFormaPago.Add(FormaPago)
                                End While
                                item.FormasPago = ListaFormaPago
                                Reader.Close()

                                Reader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_cliente]")
                                While Reader.Read
                                    item.Cliente = New Cliente(Reader)
                                End While
                                Reader.Close()
                                'Conductor
                            ElseIf lista.Codigo = 1403 Then
                                Reader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_conductor]")
                                While Reader.Read
                                    item.Conductor = New Conductor(Reader)
                                End While
                                Reader.Close()
                                'Empleado
                            ElseIf lista.Codigo = 1405 Then
                                Reader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_empleado]")
                                While Reader.Read
                                    item.Empleado = New Empleado(Reader)
                                End While
                                Reader.Close()
                            ElseIf lista.Codigo = 1409 Or lista.Codigo = 1412 Then
                                Reader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_proveedor]")
                                While Reader.Read
                                    item.Proveedor = New Proveedor(Reader)
                                End While
                                Reader.Close()
                            End If
                        Next
                    End If
                    If item.Codigo > 0 Then
                        item.Direcciones = ConsultarDirecciones(item.CodigoEmpresa, item.Codigo, conexion, Reader)
                    End If
                    If item.Codigo > 0 Then
                        item.ListadoCorreos = ConsultarListasCorreos(item.CodigoEmpresa, item.Codigo, conexion, Reader)
                    End If
                    If item.Codigo > 0 Then
                        item.SitiosTerceroCliente = ConsultarSitiosTerceroCliente(item.CodigoEmpresa, item.Codigo, conexion, Reader)
                    End If
                    If item.Codigo > 0 Then
                        item.TerceroClienteCupoSedes = ConsultarClienteCupoSedes(item.CodigoEmpresa, item.Codigo, conexion, Reader)
                    End If
                    If item.Codigo > 0 Then
                        item.LineaServicio = ConsultarLineasServicio(item.CodigoEmpresa, item.Codigo, conexion, Reader)
                    End If
                    If item.Codigo > 0 Then
                        item.CondicionesPeso = ConsultarCondicionesPeso(item.CodigoEmpresa, item.Codigo, conexion, Reader)
                    End If
                    If item.Codigo > 0 Then
                        item.ListaNovedades = ConsultarNovedadesTercero(item.CodigoEmpresa, item.Codigo, conexion, Reader)
                    End If
                    If Not IsNothing(item.Cliente) Then
                        If item.Codigo > 0 Then
                            item.Cliente.CondicionesComerciales = ConsultarCondicionesComercialesTercero(item.CodigoEmpresa, item.Codigo, conexion, Reader)
                        End If

                        If item.Codigo > 0 Then
                            item.Cliente.ListaGestionDocumentos = ConsultarGestionDocumentosTercero(item.CodigoEmpresa, item.Codigo, conexion, Reader)
                        End If
                    End If
                    Reader.Close()
                    conexion.CleanParameters()
                    If Not IsNothing(item.Cliente) Then
                        item.Cliente.Impuestos = ConsultarImpuestos(item.CodigoEmpresa, item.Codigo, 902, conexion, Reader)
                    End If
                    Reader.Close()
                    conexion.CleanParameters()
                    If Not IsNothing(item.Proveedor) Then
                        item.Proveedor.Impuestos = ConsultarImpuestos(item.CodigoEmpresa, item.Codigo, 901, conexion, Reader)
                    End If
                    Reader.Close()
                    conexion.CloseConnection()
                    Dim Documento = New Repositorio.Basico.General.RepositorioDocumentos
                    Doc.CodigoEmpresa = filtro.CodigoEmpresa
                    Doc.Terceros = True
                    Doc.CodigoTerceros = item.Codigo
                    item.Documentos = Documento.Consultar(Doc)
                    item.ListadoFOTOS = Documento.ConsultarFotos(Doc)
                End If

            End Using

            Return item
        End Function

        Public Function ConsultarAutocomplete(filtro As Terceros) As IEnumerable(Of Terceros)
            Dim lista As New List(Of Terceros)
            Dim item As Terceros

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.CadenaPerfiles) And Integer.Parse(filtro.CadenaPerfiles) <> 1400 Then
                    conexion.AgregarParametroSQL("@par_Perfil_Tercero", Integer.Parse(filtro.CadenaPerfiles))
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_terceros_autocomplete]")
                While resultado.Read
                    item = New Terceros(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista

        End Function

        Public Function InsertarDirecciones(entidad As TerceroDirecciones, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
            conexion.AgregarParametroSQL("@par_Telefonos", entidad.Telefonos)
            conexion.AgregarParametroSQL("@par_CIUD_Codigo_Direccion", entidad.Ciudad.Codigo)
            conexion.AgregarParametroSQL("@par_Barrio", entidad.Barrio)
            conexion.AgregarParametroSQL("@par_Codigo_Postal", entidad.CodigoPostal)
            conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)


            If Not IsNothing(entidad.Nombre) Then
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
            End If
            If Not IsNothing(entidad.CodigoAlterno) Then
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
            End If

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_direcciones_terceros]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarDirecciones(entidad As TerceroDirecciones) As Boolean
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
                conexion.AgregarParametroSQL("@par_Direccion", entidad.Direccion)
                conexion.AgregarParametroSQL("@par_Telefonos", entidad.Telefonos)
                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Direccion", entidad.Ciudad.Codigo)
                conexion.AgregarParametroSQL("@par_Barrio", entidad.Barrio)
                conexion.AgregarParametroSQL("@par_Codigo_Postal", entidad.CodigoPostal)

                If Not IsNothing(entidad.Nombre) Then
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                End If
                If Not IsNothing(entidad.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_direcciones_terceros]")

                If resultado.RecordsAffected > 0 Then
                    resultado.Close()
                End If

                resultado.Close()
                Return True
            End Using
        End Function

        Public Function ConsultarDirecciones(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of TerceroDirecciones)
            Dim lista As New List(Of TerceroDirecciones)
            Dim item As TerceroDirecciones
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_direcciones_terceros]")

            While resultado.Read
                item = New TerceroDirecciones(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function BorrarDirecciones(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_direcciones_terceros]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarClienteCupoSede(entidad As TerceroClienteCupoSedes, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_TEDI_Codigo", entidad.Codigo)
            conexion.AgregarParametroSQL("@par_Cupo", entidad.Cupo)
            conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_tercero_cliente_cupo_sede")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarCondicionesPeso(entidad As TerceroCondicionesPesoCumplido, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Producto.Codigo)
            conexion.AgregarParametroSQL("@par_UMPT_Codigo", entidad.UnidadEmpaque.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_BCPC_Codigo", entidad.BaseCalculo.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Unidad", entidad.ValorUnidad)
            conexion.AgregarParametroSQL("@par_CATA_TCPC_Codigo", entidad.TipoCalculo.Codigo)
            conexion.AgregarParametroSQL("@par_Tolerancia", entidad.Tolerancia, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_CATA_CCPC_Codigo", entidad.CondicionCobro.Codigo)
            conexion.AgregarParametroSQL("@par_Usuario_Crea_Codigo", entidad.UsuarioCodigoCrea)


            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_tercero_condicion_peso_cumplido")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarCondicionComercialTerceroCliente(entidad As Terceros, Condicion As CondicionComercialTarifaTercero, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Codigo)
            conexion.AgregarParametroSQL("@par_LNTC_Codigo", Condicion.LineaNegocio.Codigo)
            conexion.AgregarParametroSQL("@par_TLNC_Codigo", Condicion.TipoLineaNegocio.Codigo)
            conexion.AgregarParametroSQL("@par_TATC_Codigo", Condicion.Tarifa.Codigo)
            conexion.AgregarParametroSQL("@par_TTTC_Codigo", Condicion.TipoTarifa.Codigo)
            conexion.AgregarParametroSQL("@par_Flete_Minimo", Condicion.FleteMinimo, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Manejo_Minimo", Condicion.ManejoMinimo, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Porcentaje_Valor_Declarado", Condicion.PorcentajeValorDeclarado, SqlDbType.Decimal)
            conexion.AgregarParametroSQL("@par_Porcentaje_Descuento_Flete", Condicion.PorcentajeFlete, SqlDbType.Decimal)
            conexion.AgregarParametroSQL("@par_Porcentaje_Descuento_Volumen", Condicion.PorcentajeVolumen, SqlDbType.Decimal)


            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_tercero_condicion_comercial")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarGestionDocumentosCliente(entidad As Terceros, Condicion As GestionDocumentoCliente, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Codigo)
            conexion.AgregarParametroSQL("@par_TDGC_Codigo", Condicion.TipoDocumento.Codigo)
            conexion.AgregarParametroSQL("@par_TGCD_Codigo", Condicion.TipoGestion.Codigo)
            conexion.AgregarParametroSQL("@par_Genera_Cobro", Condicion.GeneraCobro)
            conexion.AgregarParametroSQL("@par_Estado", Condicion.Estado.Codigo)



            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_gestion_documento_cliente")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function BorrarClienteCupoSede(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_tercero_cliente_cupo_sede")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function BorrarCondicionesPeso(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_tercero_cliente_condiciones_peso")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function BorrarCondicionesComercialesTercero(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_condiciones_comerciales_tercero_cliente")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function BorrarGestionDocumentosCliente(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_gestion_documentos_cliente")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function ConsultarClienteCupoSedes(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of TerceroClienteCupoSedes)
            Dim lista As New List(Of TerceroClienteCupoSedes)
            Dim item As TerceroClienteCupoSedes
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_tercero_cliente_cupo_sede")

            While resultado.Read
                item = New TerceroClienteCupoSedes(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function ConsultarListasCorreos(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of ListasCorreos)
            Dim lista As New List(Of ListasCorreos)
            Dim item As ListasCorreos
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_listas_correos_terceros]")

            While resultado.Read
                item = New ListasCorreos(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function ConsultarLineasServicio(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of ValorCatalogos)
            Dim lista As New List(Of ValorCatalogos)
            Dim item As ValorCatalogos
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lineas_Servicio_terceros]")

            While resultado.Read
                item = New ValorCatalogos(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function ConsultarCondicionesPeso(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of TerceroCondicionesPesoCumplido)
            Dim lista As New List(Of TerceroCondicionesPesoCumplido)
            Dim item As TerceroCondicionesPesoCumplido
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_condiciones_peso_cumplido]")

            While resultado.Read
                item = New TerceroCondicionesPesoCumplido(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function ConsultarNovedadesTercero(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of NovedadTercero)
            Dim lista As New List(Of NovedadTercero)
            Dim item As NovedadTercero
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_novedades_tercero")

            While resultado.Read
                item = New NovedadTercero(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function ConsultarCondicionesComercialesTercero(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of CondicionComercialTarifaTercero)
            Dim lista As New List(Of CondicionComercialTarifaTercero)
            Dim item As CondicionComercialTarifaTercero
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_condiciones_comerciales_tercero]")

            While resultado.Read
                item = New CondicionComercialTarifaTercero(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function ConsultarGestionDocumentosTercero(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of GestionDocumentoCliente)
            Dim lista As New List(Of GestionDocumentoCliente)
            Dim item As GestionDocumentoCliente
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_gestion_documentos_tercero]")

            While resultado.Read
                item = New GestionDocumentoCliente(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function Insertarlistascorreos(entidad As ListasCorreos, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_EVCO_Codigo", entidad.EventoCorreo.Codigo)
            conexion.AgregarParametroSQL("@par_TEDI_Codigo", entidad.TerceroDireccion.Codigo)
            conexion.AgregarParametroSQL("@par_Email", entidad.Email)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_listas_correos_terceros]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarlineasServicio(entidad As ValorCatalogos, Codigo As Integer, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", Codigo)
            conexion.AgregarParametroSQL("@par_CATA_LISC_Codigo", entidad.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_lineas_Servicio_terceros]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function BorrarListasCorreos(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_listas_correos_terceros]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function InsertarImpuestosTerceros(entidad As ImpuestosTerceros, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.CodigoTercero)
            conexion.AgregarParametroSQL("@par_ENIM_Codigo", entidad.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_impuestos_terceros]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function ConsultarImpuestos(CodiEmpresa As Short, CodTercero As Long, CodTtipoImpuesto As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of ImpuestosTerceros)
            Dim lista As New List(Of ImpuestosTerceros)
            Dim item As ImpuestosTerceros
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)
            conexion.AgregarParametroSQL("@par_TIIM_Codigo", CodTtipoImpuesto)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_impuestos_terceros]")

            While resultado.Read
                item = New ImpuestosTerceros(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function BorrarImpuestos(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_impuestos_terceros]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function ConsultarTerceroGeneral(filtro As Tercero) As IEnumerable(Of Tercero)
            Dim lista As New List(Of Tercero)
            Dim item As Tercero

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If Not String.IsNullOrWhiteSpace(filtro.Nombre) Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If

                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado > -1 Then 'NO APLICA
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_terceros_general")
                While resultado.Read
                    item = New Tercero(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function InsertarSitiosTerceroCliente(entidad As SitiosTerceroCliente, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Codigo)
            conexion.AgregarParametroSQL("@par_SICD_Codigo", entidad.SitioCliente.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Cargue_Cliente", entidad.ValorCargueCliente, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Valor_Descargue_Cliente", entidad.ValorDescargueCliente, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Valor_Cargue_Transportador", entidad.ValorCargueTransportador, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Valor_Descargue_Transportador", entidad.ValorDescargueTransportador, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_terceros_sitios_cargue_descargue]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function
        Public Function ModificarSitiosTerceroCliente(entidad As SitiosTerceroCliente, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Codigo)
            conexion.AgregarParametroSQL("@par_SICD_Codigo", entidad.SitioCliente.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Cargue_Cliente", entidad.ValorCargueCliente, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Valor_Descargue_Cliente", entidad.ValorDescargueCliente, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Valor_Cargue_Transportador", entidad.ValorCargueTransportador, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Valor_Descargue_Transportador", entidad.ValorDescargueTransportador, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_modificar_terceros_sitios_cargue_descargue]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function ConsultarSitiosTerceroCliente(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of SitiosTerceroCliente)
            Dim lista As New List(Of SitiosTerceroCliente)
            Dim item As SitiosTerceroCliente
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_terceros_sitios_cargue_descargue]")

            While resultado.Read
                item = New SitiosTerceroCliente(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function BorrarSitiosTerceroCliente(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_terceros_sitios_cargue_descargue]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function ConsultarTerceroEstudioSeguridad(filtro As EstudioSeguridad) As EstudioSeguridad

            Dim item As New Terceros
            Dim objEstudio As New EstudioSeguridad
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero_Autorizacion", filtro.NumeroAutorizacion)
                    Dim Reader As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[Obtener_tercero_estudio_seguridad]")
                    While Reader.Read
                        objEstudio = New EstudioSeguridad(Reader)
                    End While
                    Reader.Close()
                    If objEstudio.Numero > 0 Then
                        Dim lista As New List(Of EstudioSeguridadDocumentos)
                        Dim Seccion As EstudioSeguridadDocumentos
                        conexionDocumentos.CreateConnection()
                        conexionDocumentos.CleanParameters()
                        conexionDocumentos.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                        If objEstudio.Numero > 0 Then
                            conexionDocumentos.AgregarParametroSQL("@par_Numero", objEstudio.Numero)
                        End If

                        Reader = conexionDocumentos.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_estudio_seguridad_documentos]")
                        While Reader.Read
                            Seccion = New EstudioSeguridadDocumentos(Reader)
                            lista.Add(Seccion)
                        End While
                        objEstudio.ListadoDocumentos = lista
                    End If

                End Using
            End Using

            Return objEstudio

        End Function

        Public Function ConsultarDocumentosProximosVencer(entidad As TercerosDocumentos) As IEnumerable(Of TercerosDocumentos)
            Dim item As New TercerosDocumentos
            Dim lista As New List(Of TercerosDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                If Not IsNothing(entidad.Tercero) Then
                    If entidad.Tercero.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
                    End If
                End If
                If entidad.TipoAplicativo > 0 Then
                    conexion.AgregarParametroSQL("@par_CATA_APLI", entidad.TipoAplicativo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_listado_propietarios_documentos_proximos_vencer]")

                While resultado.Read
                    item = New TercerosDocumentos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
            End Using
            Return lista
        End Function

        Public Function ConsultarDocumentos(entidad As TercerosDocumentos) As IEnumerable(Of TercerosDocumentos)
            Dim item As New TercerosDocumentos
            Dim lista As New List(Of TercerosDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                If Not IsNothing(entidad.Tercero) Then
                    If entidad.Tercero.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
                    End If
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_documentos_tercero")

                While resultado.Read
                    item = New TercerosDocumentos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
            End Using
            Return lista
        End Function
        Public Function GenerarPlanitilla(filtro As Terceros) As Terceros
            If filtro.PlantillaDocumento > 0 Then
                Dim fullPath As String
                fullPath = System.AppDomain.CurrentDomain.BaseDirectory()
                System.IO.File.Copy(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Documentos_Terceros.xlsx", fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Documentos_Terceros_Generado.xlsx", True)
                Dim cn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Documentos_Terceros_Generado.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES'"
                Dim Documentos As IEnumerable(Of ConfiguracionGestionDocumentos)
                Dim Documento As New ConfiguracionGestionDocumentos
                Dim ObjDocumento As New RepositorioGestionDocumentos
                Documento.CodigoEmpresa = filtro.CodigoEmpresa
                Documento.Estado = 1
                Documento.EntidadDocumento = New EntidadGestionDocumentos With {.Codigo = 2}
                Documentos = ObjDocumento.Consultar(Documento)
                Using cnn As New OleDbConnection(cn)
                    cnn.Open()
                    Documentos = Documentos.OrderBy(Function(a) a.Documento.Nombre)
                    For i = 1 To Documentos.Count
                        Dim row = i + 2
                        If Documentos(i - 1).Codigo <> 201 Then
                            Using cmd As OleDbCommand = cnn.CreateCommand()
                                cmd.CommandText = "INSERT INTO [Documentos$] (Codigo,Nombre) values(@Cod,@nom)"
                                cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Documentos(i - 1).Codigo
                                cmd.Parameters.AddWithValue("@nom", Documentos(i - 1).Documento.Nombre)
                                cmd.ExecuteNonQuery()
                            End Using
                        End If
                    Next

                    cnn.Close()
                End Using
                Dim RTA As New Terceros
                Dim Archivo() As Byte = IO.File.ReadAllBytes(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Documentos_Terceros_Generado.xlsx")
                RTA.Planitlla = Archivo
                Return RTA
            Else
                Dim fullPath As String
                fullPath = System.AppDomain.CurrentDomain.BaseDirectory()
                System.IO.File.Copy(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Terceros.xlsx", fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Terceros_Generado.xlsx", True)
                Dim cn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Terceros_Generado.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES'"

                Dim Ciudades As IEnumerable(Of Ciudades)
                Dim Ciudad As New Ciudades
                Dim ObjCiudad As New RepositorioCiudades
                Ciudad.CodigoEmpresa = filtro.CodigoEmpresa
                Ciudad.Estado = 1
                Ciudades = ObjCiudad.Consultar(Ciudad)


                Dim Bancos As IEnumerable(Of Bancos)
                Dim Banco As New Bancos
                Dim ObjBanco As New RepositorioBancos
                Banco.CodigoEmpresa = filtro.CodigoEmpresa
                Banco.Estado = 1
                Bancos = ObjBanco.Consultar(Banco)


                Dim Tarifarioventas As IEnumerable(Of TarifarioVentas)
                Dim Tarifarioventa As New TarifarioVentas
                Dim ObjTarifarioventa As New RepositorioTarifarioVentas
                Tarifarioventa.CodigoEmpresa = filtro.CodigoEmpresa
                Tarifarioventa.Estado = New Estado With {.Codigo = 1}
                Tarifarioventas = ObjTarifarioventa.Consultar(Tarifarioventa)

                Dim Tarifariocompras As IEnumerable(Of TarifarioCompras)
                Dim Tarifariocompra As New TarifarioCompras
                Dim ObjTarifariocompra As New RepositorioTarifarioCompras
                Tarifariocompra.CodigoEmpresa = filtro.CodigoEmpresa
                Tarifariocompra.Estado = New Estado With {.Codigo = 1}
                Tarifariocompras = ObjTarifariocompra.Consultar(Tarifariocompra)

                Dim Catalogos As IEnumerable(Of ValorCatalogos)
                Dim Catalogo As New ValorCatalogos
                Dim ObjValorCatalogos As New RepositorioValorCatalogos
                Catalogo.CodigoEmpresa = filtro.CodigoEmpresa
                Catalogo.Catalogo = New Catalogos With {.Codigo = 87}
                Catalogos = ObjValorCatalogos.Consultar(Catalogo)

                Using cnn As New OleDbConnection(cn)
                    cnn.Open()

                    Ciudades = Ciudades.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Ciudades.Count
                        Dim row = i + 2
                        Using cmd As OleDbCommand = cnn.CreateCommand()
                            cmd.CommandText = "INSERT INTO [Ciudad$] (Codigo,Nombre) values(@Cod,@nom)"
                            cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Ciudades(i - 1).Codigo
                            cmd.Parameters.AddWithValue("@nom", Ciudades(i - 1).Nombre)
                            cmd.ExecuteNonQuery()
                        End Using
                    Next
                    Bancos = Bancos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Bancos.Count
                        Dim row = i + 2
                        Using cmd As OleDbCommand = cnn.CreateCommand()
                            cmd.CommandText = "INSERT INTO [Bancos$] (Codigo,Nombre) values(@Cod,@nom)"
                            cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Bancos(i - 1).Codigo
                            cmd.Parameters.AddWithValue("@nom", Bancos(i - 1).Nombre)
                            cmd.ExecuteNonQuery()
                        End Using
                    Next
                    Tarifarioventas = Tarifarioventas.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Tarifarioventas.Count
                        Dim row = i + 2
                        Using cmd As OleDbCommand = cnn.CreateCommand()
                            cmd.CommandText = "INSERT INTO [TarifarioVenta$] (Codigo,Nombre) values(@Cod,@nom)"
                            cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Tarifarioventas(i - 1).Codigo
                            cmd.Parameters.AddWithValue("@nom", Tarifarioventas(i - 1).Nombre)
                            cmd.ExecuteNonQuery()
                        End Using
                    Next
                    Tarifariocompras = Tarifariocompras.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Tarifariocompras.Count
                        Dim row = i + 2
                        Using cmd As OleDbCommand = cnn.CreateCommand()
                            cmd.CommandText = "INSERT INTO [TarifarioCompra$] (Codigo,Nombre) values(@Cod,@nom)"
                            cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Tarifariocompras(i - 1).Codigo
                            cmd.Parameters.AddWithValue("@nom", Tarifariocompras(i - 1).Nombre)
                            cmd.ExecuteNonQuery()
                        End Using
                    Next
                    Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Catalogos.Count
                        Dim row = i + 2
                        Using cmd As OleDbCommand = cnn.CreateCommand()
                            cmd.CommandText = "INSERT INTO [Departamento$] (Codigo,Nombre) values(@Cod,@nom)"
                            cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                            cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                            cmd.ExecuteNonQuery()
                        End Using
                    Next
                    Catalogo.CodigoEmpresa = filtro.CodigoEmpresa
                    Catalogo.Catalogo = New Catalogos With {.Codigo = 20}
                    Catalogos = ObjValorCatalogos.Consultar(Catalogo)
                    Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Catalogos.Count
                        Dim row = i + 2
                        Using cmd As OleDbCommand = cnn.CreateCommand()
                            cmd.CommandText = "INSERT INTO [Cargo$] (Codigo,Nombre) values(@Cod,@nom)"
                            cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                            cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                            cmd.ExecuteNonQuery()
                        End Using
                    Next
                    cnn.Close()
                End Using
                Dim RTA As New Terceros
                Dim Archivo() As Byte = IO.File.ReadAllBytes(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Terceros_Generado.xlsx")
                RTA.Planitlla = Archivo
                Return RTA
            End If
        End Function

        Public Function ActualizarEmailFacturaElectronica(entidad As Terceros) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Crea_Codigo", entidad.UsuaCodigoCrea)
                If Len(entidad.CorreoFacturacion) > 0 Then
                    conexion.AgregarParametroSQL("@par_Correo_Factura_Electronica", entidad.CorreoFacturacion)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_tercero_correo_factura_electronica")
                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While
                resultado.Close()
            End Using
            Return entidad.Codigo
        End Function

        Public Function ConsultarAuditoria(filtro As Tercero) As IEnumerable(Of Tercero)

            Dim lista As New List(Of Tercero)
            Dim item As Tercero

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If


                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then 'NO APLICA
                    conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.Codigo)
                End If



                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Consultar_auditoria_terceros")
                While resultado.Read
                    item = New Tercero(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

    End Class
End Namespace