﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioTipoDocumentos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioTipoDocumentos
        Inherits RepositorioBase(Of TipoDocumentos)
        Dim inserto As Boolean = True
        Public Overrides Function Consultar(filtro As TipoDocumentos) As IEnumerable(Of TipoDocumentos)
            Dim lista As New List(Of TipoDocumentos)
            Dim item As TipoDocumentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Nombre) Then
                    If filtro.Nombre <> "" Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If
                End If

                If Not IsNothing(filtro.Codigo) Then
                    If filtro.Codigo <> 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                End If
                If filtro.CierreContable > 0 Then
                    conexion.AgregarParametroSQL("@par_Cierre_Contable", filtro.CierreContable)
                End If

                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado >= 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If Not IsNothing(filtro.TipoNumeracion) Then
                    If filtro.TipoNumeracion <> 1100 And filtro.TipoNumeracion <> 0 Then
                        conexion.AgregarParametroSQL("@par_TIGN", filtro.TipoNumeracion)
                    End If
                End If

                If filtro.GeneraComprobanteContable > 0 Then
                    conexion.AgregarParametroSQL("@par_Genera_Comprobante_Contable", filtro.GeneraComprobanteContable)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_tipo_documentos")

                While resultado.Read
                    item = New TipoDocumentos(resultado)
                    lista.Add(item)
                End While

            End Using


            Return lista
        End Function

        Public Overrides Function Insertar(entidad As TipoDocumentos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As TipoDocumentos) As Long

            If entidad.AplicaDetalle = Cero Then
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                    conexion.AgregarParametroSQL("@par_CATA_TIGN", entidad.TipoNumeracion)
                    conexion.AgregarParametroSQL("@par_Consecutivo", entidad.Consecutivo)
                    conexion.AgregarParametroSQL("@par_Consecutivo_Hasta", entidad.ConsecutivoFin)
                    conexion.AgregarParametroSQL("@par_Controlar_Consecutivo_Hasta", entidad.ControladorConsecutivo)
                    conexion.AgregarParametroSQL("@par_Numero_Documentos_Faltantes", entidad.DocumentosFaltantesAviso)
                    conexion.AgregarParametroSQL("@par_Genera_Comprobante_Contable", entidad.GeneraComprobanteContable)
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_Nota_Inicio", entidad.NotaInicio)
                    conexion.AgregarParametroSQL("@par_Nota_Fin", entidad.NotaFin)

                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica ", entidad.UsuarioModifica.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_tipo_documentos")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Numero").ToString()
                    End While

                    resultado.Close()
                End Using
            Else
                If entidad.AplicaDetalle = 2 Then
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                        conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_Listado_Impuestos_Documentos")
                    End Using


                    If Not IsNothing(entidad.ListadoImpuestos) Then
                        For Each listadoimpuestos In entidad.ListadoImpuestos
                            inserto = AsignarImpuestos(listadoimpuestos, entidad)
                        Next
                    End If
                End If

                If inserto = True Then
                    entidad.Codigo = 1
                End If
            End If

            If entidad.Codigo.Equals(Cero) Then
                Return Cero
            End If



            Return entidad.Codigo

        End Function

        Public Function AsignarImpuestos(entidad As ImpuestoTipoDocumentos, TipoDocumentos As TipoDocumentos) As Boolean

            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", TipoDocumentos.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", TipoDocumentos.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Impuesto", entidad.CodigoImpuesto)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_asignar_impuestos_tipo_documentos")

                While resultado.Read
                    entidad.CodigoImpuesto = resultado.Item("Numero").ToString()
                End While

                If entidad.CodigoImpuesto > Cero Then
                    inserto = True
                Else
                    inserto = False
                End If

            End Using
            Return inserto
        End Function

        Public Overrides Function Obtener(filtro As TipoDocumentos) As TipoDocumentos

            Dim item As New TipoDocumentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_tipo_documentos")

                While resultado.Read
                    item = New TipoDocumentos(resultado)
                End While

                conexion.CloseConnection()

                If filtro.Codigo > 0 Then
                    item.ListadoImpuestos = Obtener_Impuesto(filtro)
                End If


            End Using

            Return item

        End Function
        Public Function Obtener_Impuesto(filtro As TipoDocumentos) As IEnumerable(Of ImpuestoTipoDocumentos)
            Dim LISTA As New List(Of ImpuestoTipoDocumentos)
            Dim ITEM As ImpuestoTipoDocumentos
            Using Conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory
                Conexion.CreateConnection()

                Conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                Conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)


                Dim resultado As IDataReader = Conexion.ExecuteReaderStoreProcedure("gsp_consultar_impuestos_tipo_documentos")

                While resultado.Read
                    ITEM = New ImpuestoTipoDocumentos(resultado)
                    LISTA.Add(ITEM)
                End While
                Conexion.CloseConnection()
            End Using
            Return LISTA
        End Function
        Public Function Anular(entidad As TipoDocumentos) As Boolean

            Dim anulo As Boolean = False
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                If entidad.AplicaDetalle = 0 Then
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    resultado = conexion.ExecuteReaderStoreProcedure("")

                End If
                If entidad.AplicaDetalle = 1 Then
                    conexion.CreateConnection()
                    resultado = conexion.ExecuteReaderStoreProcedure("")
                Else
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Codigo_Impuesto", entidad.CodigoImpuesto)

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_impuestos_tipodocumento")
                End If
                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > Cero, True, False)
                End While

            End Using

            Return anulo

        End Function
    End Class

End Namespace