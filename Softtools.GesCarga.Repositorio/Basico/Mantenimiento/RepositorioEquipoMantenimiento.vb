﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Mantenimiento

Namespace Mantenimiento
    Public NotInheritable Class RepositorioEquipoMantenimiento
        Inherits RepositorioBase(Of EquipoMantenimiento)
        Public Overrides Function Consultar(filtro As EquipoMantenimiento) As IEnumerable(Of EquipoMantenimiento)
            Dim lista As New List(Of EquipoMantenimiento)
            Dim item As EquipoMantenimiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Nombre) And filtro.Nombre <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre.ToString)
                End If
                If Not IsNothing(filtro.EstadoDocumento) Then
                    If Not IsNothing(filtro.EstadoDocumento.Codigo) Or filtro.EstadoDocumento.Codigo = vbNull Then
                        If filtro.EstadoDocumento.Codigo <> 8697 And filtro.EstadoDocumento.Codigo <> -1 Then 'NO APLICA
                            conexion.AgregarParametroSQL("@par_estado", filtro.EstadoDocumento.Codigo)
                        End If
                    End If
                End If
                If Not IsNothing(filtro.EstadoEquipoMantenimiento) Then
                    If Not IsNothing(filtro.EstadoEquipoMantenimiento.Codigo) Or filtro.EstadoEquipoMantenimiento.Codigo = vbNull Then
                        If filtro.EstadoEquipoMantenimiento.Codigo <> -1 Then
                            conexion.AgregarParametroSQL("@par_estado_Mantenimiento", filtro.EstadoEquipoMantenimiento.Codigo)
                        End If
                    End If
                End If
                If Not IsNothing(filtro.Codigo) Then
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If Not IsNothing(filtro.CodigoAlterno) Then
                    If filtro.CodigoAlterno <> "" Then
                        conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_consultar_equipo_mantenimientos")

                While resultado.Read
                    item = New EquipoMantenimiento(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As EquipoMantenimiento) As Long
            Dim inserto As Boolean = True
            Dim InsertoDocumento As Boolean = True
            Dim TransladoDocumento As RepositorioDocumentoEquipoMantenimiento
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()

                'Dim insertoDocumento As Boolean = True
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.Codigo_Alterno)
                    conexion.AgregarParametroSQL("@par_UNFM_Codigo", entidad.UnidadReferenciaMantenimiento.Codigo)
                    conexion.AgregarParametroSQL("@par_Valor_Uso", entidad.Valor_Uso)
                    conexion.AgregarParametroSQL("@par_ESEM_Codigo", entidad.EstadoEquipoMantenimiento.Codigo)
                    conexion.AgregarParametroSQL("@par_TEMA_Codigo", entidad.TipoEquipoMantenimiento.Codigo)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_MAEM_Codigo", entidad.MarcaEquipoMantenimiento.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Propietario", entidad.Propietario.Codigo)
                    conexion.AgregarParametroSQL("@par_Serie", entidad.Serie)
                    conexion.AgregarParametroSQL("@par_Modelo", entidad.Modelo)
                    conexion.AgregarParametroSQL("@par_Cpacidad_Carge", entidad.CapacidadCargue) 'Capacidad de carge'
                    conexion.AgregarParametroSQL("@par_Ancho", entidad.Ancho) 'Ancho'
                    conexion.AgregarParametroSQL("@par_Largo", entidad.Largo) 'Largo'
                    conexion.AgregarParametroSQL("@par_Alto", entidad.Alto) 'Alto'
                    conexion.AgregarParametroSQL("@par_CATA_ESDO_Codigo", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_insertar_Equipo_Mantemiento")

                    While resultado.Read
                        entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                    End While
                    resultado.Close()
                    If InsertoDocumento Then
                        TransladoDocumento = New RepositorioDocumentoEquipoMantenimiento
                        For Each detalle In entidad.DocumentosEquipoMantenimiento
                            detalle.Codigo = entidad.Codigo
                            detalle.ID_Documento = InsertarDocumento(detalle, conexionDocumentos)
                            If Not TransladoDocumento.TrasladarDocumento(detalle, conexionDocumentos) Then
                                inserto = False
                                Exit For
                            End If

                        Next
                    End If
                    InsertoDocumento = False
                End Using
            End Using
            Return entidad.Codigo
        End Function

        Public Function InsertarDocumento(entidad As EquipoMantenimientoDocumento, ByRef contextoConexion As Conexion.DataBaseFactory) As Integer
            Dim inserto As Integer = 0

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_Codio", entidad.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
            contextoConexion.AgregarParametroSQL("@par_Extencion", entidad.Extencion)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_insertar_equipo_documentos]")

            While resultado.Read
                inserto = resultado.Item("Codigo")
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Overrides Function Modificar(entidad As EquipoMantenimiento) As Long
            Dim inserto As Boolean = True
            Dim InsertoDocumento As Boolean = True
            Dim TransladoDocumento As RepositorioDocumentoEquipoMantenimiento
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                'Dim insertoDocumento As Boolean = True
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.Codigo_Alterno)
                    conexion.AgregarParametroSQL("@par_UNFM_Codigo", entidad.UnidadReferenciaMantenimiento.Codigo)
                    conexion.AgregarParametroSQL("@par_Valor_Uso", entidad.Valor_Uso)
                    conexion.AgregarParametroSQL("@par_ESEM_Codigo", entidad.EstadoEquipoMantenimiento.Codigo)
                    conexion.AgregarParametroSQL("@par_TEMA_Codigo", entidad.TipoEquipoMantenimiento.Codigo)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_MAEM_Codigo", entidad.MarcaEquipoMantenimiento.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Propietario", entidad.Propietario.Codigo)
                    conexion.AgregarParametroSQL("@par_Serie", entidad.Serie)
                    conexion.AgregarParametroSQL("@par_Modelo", entidad.Modelo) 'pendiente por definir
                    conexion.AgregarParametroSQL("@par_Cpacidad_Carge", entidad.CapacidadCargue) 'Capacidad de carge'
                    conexion.AgregarParametroSQL("@par_Ancho", entidad.Ancho) 'Ancho'
                    conexion.AgregarParametroSQL("@par_Largo", entidad.Largo) 'Largo'
                    conexion.AgregarParametroSQL("@par_Alto", entidad.Alto) 'Alto'
                    conexion.AgregarParametroSQL("@par_CATA_ESDO_Codigo", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_modificar_Equipo_Mantemiento")

                    While resultado.Read
                        entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                    End While
                    resultado.Close()
                    'Inserta y/o modifica los documentos ya sea el caso'
                    If InsertoDocumento Then
                        TransladoDocumento = New RepositorioDocumentoEquipoMantenimiento
                        For Each detalle In entidad.DocumentosEquipoMantenimiento
                            detalle.Codigo = entidad.Codigo
                            If detalle.ID_Documento > 0 And detalle.ID_Documento_temporal > 0 Then
                                If Not TransladoDocumento.TrasladarDocumento(detalle, conexionDocumentos) Then
                                    If Not ModificarDocumento(detalle, conexionDocumentos) Then
                                        inserto = False
                                        Exit For
                                    End If
                                End If
                            ElseIf detalle.ID_Documento > 0 Then
                                If Not ModificarDocumento(detalle, conexionDocumentos) Then
                                    inserto = False
                                    Exit For
                                End If
                            Else
                                detalle.ID_Documento = InsertarDocumento(detalle, conexionDocumentos)
                                If Not TransladoDocumento.TrasladarDocumento(detalle, conexionDocumentos) Then
                                    inserto = False
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                    'Elimina documentos que ya no estan asociados al equipo'
                    For Each detalle In entidad.ID_Documentos_Eliminar
                        If Not EliminarDocumento(detalle, entidad.CodigoEmpresa, conexionDocumentos) Then
                            inserto = False
                            Exit For
                        End If
                    Next
                    InsertoDocumento = False
                End Using
            End Using
            Return entidad.Codigo
        End Function

        Public Function ModificarDocumento(entidad As EquipoMantenimientoDocumento, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@ID_Documento", entidad.ID_Documento)
            contextoConexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
            contextoConexion.AgregarParametroSQL("@par_Extencion", entidad.Extencion)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_modificar_equipo_documentos]")

            While resultado.Read
                inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
            End While

            resultado.Close()

            Return inserto
        End Function
        Public Function EliminarDocumento(ID As Integer, CodigoEmpresa As Integer, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@ID_Documento", ID)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_eliminar_equipo_documentos]")

            While resultado.Read
                inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Overrides Function Obtener(filtro As EquipoMantenimiento) As EquipoMantenimiento
            Dim item As New EquipoMantenimiento
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_obtener_Equipo_mantenimientos")

                    While resultado.Read
                        item = New EquipoMantenimiento(resultado)
                    End While
                    resultado.Close()
                    conexion.CloseConnection()
                    If item.Codigo > Cero Then
                        item.DocumentosEquipoMantenimiento = ObtenerDocumentos(item.Codigo, filtro.CodigoEmpresa, conexionDocumentos)
                    End If
                End Using
            End Using
            Return item
        End Function
        Public Function ObtenerDocumentos(codigo As Integer, codigoEmpresa As Integer, ByRef contextoConexion As Conexion.DataBaseFactory) As IEnumerable(Of EquipoMantenimientoDocumento)
            Dim lista As New List(Of EquipoMantenimientoDocumento)
            Dim item As EquipoMantenimientoDocumento


            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_Codigo", codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_obtener_equipo_documentos]")

            While resultado.Read
                item = New EquipoMantenimientoDocumento(resultado)
                lista.Add(item)
            End While

            resultado.Close()

            Return lista
        End Function
    End Class
End Namespace
