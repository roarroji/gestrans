﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.ServicioCliente

Namespace Basico.ServicioCliente

    ''' <summary>
    ''' Clase <see cref="RepositorioConceptoVentas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioConceptoVentas
        Inherits RepositorioBase(Of ConceptoVentas)

        Public Overrides Function Consultar(filtro As ConceptoVentas) As IEnumerable(Of ConceptoVentas)
            Dim lista As New List(Of ConceptoVentas)
            Dim item As ConceptoVentas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                If filtro.ConceptoSistema >= 0 Then
                    conexion.AgregarParametroSQL("@par_Concepto_Sistema", filtro.ConceptoSistema)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_conceptos_ventas]")

                While resultado.Read
                    item = New ConceptoVentas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ConceptoVentas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ConceptoVentas) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As ConceptoVentas) As ConceptoVentas
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace