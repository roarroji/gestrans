﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.ServicioCliente

Namespace Basico.ServicioCliente

    ''' <summary>
    ''' Clase <see cref="RepositorioLineaNegocioTransporteCarga"/>
    ''' </summary>
    Public NotInheritable Class RepositorioLineaNegocioTransporteCarga
        Inherits RepositorioBase(Of LineaNegocioTransporteCarga)

        Public Overrides Function Consultar(filtro As LineaNegocioTransporteCarga) As IEnumerable(Of LineaNegocioTransporteCarga)
            Dim lista As New List(Of LineaNegocioTransporteCarga)
            Dim item As LineaNegocioTransporteCarga

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If filtro.Estado > -1 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_linea_negocio_transporte_carga]")

                While resultado.Read
                    item = New LineaNegocioTransporteCarga(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As LineaNegocioTransporteCarga) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Modificar(entidad As LineaNegocioTransporteCarga) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As LineaNegocioTransporteCarga) As LineaNegocioTransporteCarga
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace