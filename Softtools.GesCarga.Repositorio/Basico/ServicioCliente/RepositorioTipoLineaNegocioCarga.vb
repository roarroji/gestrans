﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.ServicioCliente

Namespace Basico.ServicioCliente

    ''' <summary>
    ''' Clase <see cref="RepositorioTipoLineaNegocioCarga"/>
    ''' </summary>
    Public NotInheritable Class RepositorioTipoLineaNegocioCarga
        Inherits RepositorioBase(Of TipoLineaNegocioCarga)

        Public Overrides Function Consultar(filtro As TipoLineaNegocioCarga) As IEnumerable(Of TipoLineaNegocioCarga)
            Dim lista As New List(Of TipoLineaNegocioCarga)
            Dim item As TipoLineaNegocioCarga

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If filtro.Estado > -1 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If
                If Not IsNothing(filtro.LineaNegocioTransporteCarga) Then
                    If filtro.LineaNegocioTransporteCarga.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_LNTC_Codigo", filtro.LineaNegocioTransporteCarga.Codigo)
                    End If
                End If



                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_tipo_linea_negocio_carga]")

                While resultado.Read
                    item = New TipoLineaNegocioCarga(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As TipoLineaNegocioCarga) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Modificar(entidad As TipoLineaNegocioCarga) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As TipoLineaNegocioCarga) As TipoLineaNegocioCarga
            Dim item As New TipoLineaNegocioCarga

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_empresas]")

                While resultado.Read
                    item = New TipoLineaNegocioCarga(resultado)
                End While

            End Using

            Return item
        End Function
    End Class

End Namespace