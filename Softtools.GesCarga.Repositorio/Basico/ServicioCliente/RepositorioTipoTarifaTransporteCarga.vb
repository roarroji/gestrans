﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.ServicioCliente

Namespace Basico.ServicioCliente

    ''' <summary>
    ''' Clase <see cref="RepositorioTipoTarifaTransporteCarga"/>
    ''' </summary>
    Public NotInheritable Class RepositorioTipoTarifaTransporteCarga
        Inherits RepositorioBase(Of TipoTarifaTransporteCarga)

        Public Overrides Function Consultar(filtro As TipoTarifaTransporteCarga) As IEnumerable(Of TipoTarifaTransporteCarga)
            Dim lista As New List(Of TipoTarifaTransporteCarga)
            Dim item As TipoTarifaTransporteCarga

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TATC_Codigo", filtro.TarifaTransporteCarga.Codigo)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_tipo_tarifa_transporte_carga]")

                While resultado.Read
                    item = New TipoTarifaTransporteCarga(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As TipoTarifaTransporteCarga) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Modificar(entidad As TipoTarifaTransporteCarga) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As TipoTarifaTransporteCarga) As TipoTarifaTransporteCarga
            Dim item As New TipoTarifaTransporteCarga

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_empresas]")

                While resultado.Read
                    item = New TipoTarifaTransporteCarga(resultado)
                End While

            End Using

            Return item
        End Function
    End Class

End Namespace