﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="RepositorioBancos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioBancos
        Inherits RepositorioBase(Of Bancos)

        ''' <summary>
        ''' Metodo para consultar un listado de Bancos
        ''' </summary>
        ''' <param name="filtro">filtros de busqueda</param>
        ''' <returns>Listado de Bancos</returns>
        Public Overrides Function Consultar(filtro As Bancos) As IEnumerable(Of Bancos)
            Dim lista As New List(Of Bancos)
            Dim item As Bancos
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                If filtro.sLista = 1 Then

                    resultado = conexion.ExecuteReaderStoreProcedure("dbo.gsp_retornar_lista_bancos")

                Else


                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If

                    If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                        conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                    End If

                    If Not String.IsNullOrWhiteSpace(filtro.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If

                    If filtro.Estado >= 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If

                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If
                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > 0 Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_bancos")

                End If

                While resultado.Read
                    item = New Bancos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        ''' <summary>
        ''' Metodo para insertar una nueva empresa en el repositorio de datos
        ''' </summary>
        ''' <param name="entidad">Objeto para insertar</param>
        ''' <returns>Codigo unico de la nueva empresa</returns>
        Public Overrides Function Insertar(entidad As Bancos) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_bancos")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        ''' <summary>
        ''' Metodo para modificar la informacion de una empresa existente en el repositorio de datos
        ''' </summary>
        ''' <param name="entidad">Objeto para modificar</param>
        ''' <returns>Codigo unico de la empresa modificada</returns>
        Public Overrides Function Modificar(entidad As Bancos) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_bancos")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        ''' <summary>
        ''' Metodo para obtener la informacion de una empresa
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Overrides Function Obtener(filtro As Bancos) As Bancos
            Dim item As New Bancos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("dbo.gsp_obtener_bancos")

                While resultado.Read
                    item = New Bancos(resultado)
                End While

            End Using

            Return item
        End Function

    End Class

End Namespace