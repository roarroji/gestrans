﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico

    ''' <summary>
    ''' Clase <see cref="RepositorioCuentaBancariaOficinas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioCuentaBancariaOficinas
        Inherits RepositorioBase(Of CuentaBancariaOficinas)
        Public Overrides Function Consultar(filtro As CuentaBancariaOficinas) As IEnumerable(Of CuentaBancariaOficinas)
            Dim lista As New List(Of CuentaBancariaOficinas)
            Dim item As CuentaBancariaOficinas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Oficina) Then
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                End If

                If filtro.CodigoEstado > 0 Then
                    conexion.AgregarParametroSQL("@par_CATA_ESCB_Codigo", filtro.CodigoEstado)
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_cuenta_bancaria_oficinas]")

                While resultado.Read
                    item = New CuentaBancariaOficinas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As CuentaBancariaOficinas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As CuentaBancariaOficinas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As CuentaBancariaOficinas) As CuentaBancariaOficinas
            Throw New NotImplementedException()
        End Function



        Public Function ConsultarCuentaOficinas(codigoEmpresa As Short, codigoCuenta As Short) As IEnumerable(Of CuentaBancariaOficinas)
            Dim lista As New List(Of CuentaBancariaOficinas)
            Dim item As CuentaBancariaOficinas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_CUBA_Codigo", codigoCuenta)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_consultar_cuenta_oficinas")

                While resultado.Read
                    item = New CuentaBancariaOficinas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function InsertarCuentaOficinas(detalles As IEnumerable(Of CuentaBancariaOficinas)) As Long
            Dim lonResultado As Long = 0
            If detalles.Count <> 0 Then
                Call EliminarCuentaOficinas(detalles(0).CodigoEmpresa, detalles(0).CuentaBancaria.Codigo)
            End If

            For Each CuentaOficinas In detalles

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", CuentaOficinas.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_CUBA_Codigo", CuentaOficinas.CuentaBancaria.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", CuentaOficinas.Oficina.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_insertar_cuenta_oficinas")
                    While resultado.Read
                        CuentaOficinas.CuentaBancaria.Codigo = Convert.ToInt64(resultado.Item("CUBA_Codigo").ToString())
                    End While

                    resultado.Close()

                    If CuentaOficinas.CuentaBancaria.Codigo.Equals(Cero) Then
                        lonResultado = Cero
                        Exit For
                    End If

                    lonResultado = CuentaOficinas.CuentaBancaria.Codigo
                End Using
            Next

            Return lonResultado
        End Function

        Public Sub EliminarCuentaOficinas(codigoEmpresa As Short, codigoCuenta As Short)
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                    conexion.AgregarParametroSQL("@par_CUBA_Codigo", codigoCuenta)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_eliminar_cuenta_oficinas")
                End Using
                transaccion.Complete()
            End Using
        End Sub
    End Class
End Namespace