﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="RepositorioCuentaBancarias"/>
    ''' </summary>
    Public NotInheritable Class RepositorioCuentaBancarias
        Inherits RepositorioBase(Of CuentaBancarias)

        Public Overrides Function Consultar(filtro As CuentaBancarias) As IEnumerable(Of CuentaBancarias)
            Dim lista As New List(Of CuentaBancarias)
            Dim item As CuentaBancarias

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.NumeroCuenta) Then
                    conexion.AgregarParametroSQL("@par_Numero_Cuenta", filtro.NumeroCuenta)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.Nombre) Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If

                If Not IsNothing(filtro.TipoCuentaBancaria) Then
                    If filtro.TipoCuentaBancaria.Codigo > 400 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Tipo_Cuenta", filtro.TipoCuentaBancaria.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Banco) Then
                    If Not String.IsNullOrWhiteSpace(filtro.Banco.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Nombre_Banco", filtro.Banco.Nombre)
                    End If
                End If

                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_cuenta_bancarias]")

                While resultado.Read
                    item = New CuentaBancarias(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As CuentaBancarias) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                If Not IsNothing(entidad.Banco) Then
                    conexion.AgregarParametroSQL("@par_BANC_Codigo", entidad.Banco.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Numero_Cuenta", entidad.NumeroCuenta)
                conexion.AgregarParametroSQL("@par_CATA_TICB_Codigo", entidad.TipoCuentaBancaria.Codigo)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
                If Not IsNothing(entidad.CuentaPUC) Then
                    conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPUC.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Sobregiro_Autorizado", entidad.SobregiroAutorizado)
                conexion.AgregarParametroSQL("@par_Saldo_Actual", entidad.SaldoActual)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_cuenta_bancarias")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
        Public Overrides Function Modificar(entidad As CuentaBancarias) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                If Not IsNothing(entidad.Banco) Then
                    conexion.AgregarParametroSQL("@par_BANC_Codigo", entidad.Banco.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Numero_Cuenta", entidad.NumeroCuenta)
                conexion.AgregarParametroSQL("@par_CATA_TICB_Codigo", entidad.TipoCuentaBancaria.Codigo)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
                If Not IsNothing(entidad.CuentaPUC) Then
                    conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPUC.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Sobregiro_Autorizado", entidad.SobregiroAutorizado)
                conexion.AgregarParametroSQL("@par_Saldo_Actual", entidad.SaldoActual)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_cuenta_bancarias")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
        Public Overrides Function Obtener(filtro As CuentaBancarias) As CuentaBancarias
            Dim item As New CuentaBancarias

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("dbo.gsp_obtener_cuenta_bancarias")

                While resultado.Read
                    item = New CuentaBancarias(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function CuentaBancariaoficinas(codigoEmpresa As Short, Codigo As Short) As IEnumerable(Of CuentaBancariaOficinas)

            Dim lista As New List(Of CuentaBancariaOficinas)
            Dim item As CuentaBancariaOficinas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo", Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("dbo.gsp_consultar_cuenta_bancaria_oficinas")


                While resultado.Read
                    item = New CuentaBancariaOficinas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ConsultarCuentaOficinas(codigoEmpresa As Short, codigoCuenta As Short) As IEnumerable(Of CuentaBancariaOficinas)
            Dim lista As New List(Of CuentaBancariaOficinas)
            Dim item As CuentaBancariaOficinas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_CUBA_Codigo", codigoCuenta)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_consultar_cuenta_oficinas")

                While resultado.Read
                    item = New CuentaBancariaOficinas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function InsertarCuentaOficinas(detalles As IEnumerable(Of CuentaBancariaOficinas)) As Long
            Dim lonResultado As Long = 0
            If detalles.Count <> 0 Then
                Call EliminarCuentaOficinas(detalles(0).CodigoEmpresa, detalles(0).CuentaBancaria.Codigo)
            End If

            For Each CuentaOficinas In detalles

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", CuentaOficinas.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_CUBA_Codigo", CuentaOficinas.CuentaBancaria.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", CuentaOficinas.Oficina.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_insertar_cuenta_oficinas")
                    While resultado.Read
                        CuentaOficinas.CuentaBancaria.Codigo = Convert.ToInt64(resultado.Item("CUBA_Codigo").ToString())
                    End While

                    resultado.Close()

                    If CuentaOficinas.CuentaBancaria.Codigo.Equals(Cero) Then
                        lonResultado = Cero
                        Exit For
                    End If

                    lonResultado = CuentaOficinas.CuentaBancaria.Codigo
                End Using
            Next

            Return lonResultado
        End Function

        Public Sub EliminarCuentaOficinas(codigoEmpresa As Short, codigoCuenta As Short)
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                    conexion.AgregarParametroSQL("@par_CUBA_Codigo", codigoCuenta)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_eliminar_cuenta_oficinas")
                End Using
                transaccion.Complete()
            End Using
        End Sub


        Public Function Anular(entidad As CuentaBancarias) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_cuenta_bancarias]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace