﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria


Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleConceptoContables"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleConceptoContables
        Inherits RepositorioBase(Of DetalleConceptoContables)

        Public Overrides Function Consultar(filtro As DetalleConceptoContables) As IEnumerable(Of DetalleConceptoContables)
            Dim lista As New List(Of DetalleConceptoContables)
            Dim item As DetalleConceptoContables

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_ECCO_Codigo", filtro.Codigo)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_concepto_contables]")

                While resultado.Read
                    item = New DetalleConceptoContables(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As DetalleConceptoContables) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Modificar(entidad As DetalleConceptoContables) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As DetalleConceptoContables) As DetalleConceptoContables
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DetalleConceptoContables) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace