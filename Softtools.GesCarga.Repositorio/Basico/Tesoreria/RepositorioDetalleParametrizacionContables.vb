﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria


Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleParametrizacionContables"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleParametrizacionContables
        Inherits RepositorioBase(Of DetalleParametrizacionContables)

        Public Overrides Function Consultar(filtro As DetalleParametrizacionContables) As IEnumerable(Of DetalleParametrizacionContables)
            Dim lista As New List(Of DetalleParametrizacionContables)
            Dim item As DetalleParametrizacionContables

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_parametrizacion_contables]")

                While resultado.Read
                    item = New DetalleParametrizacionContables(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As DetalleParametrizacionContables) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Modificar(entidad As DetalleParametrizacionContables) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As DetalleParametrizacionContables) As DetalleParametrizacionContables
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DetalleParametrizacionContables) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace