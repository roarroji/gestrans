﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports System.Transactions

Namespace Basico.Tesoreria

    ''' <summary>
    ''' Clase <see cref="RepositorioEncabezadoConceptoContables"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEncabezadoConceptoContables
        Inherits RepositorioBase(Of EncabezadoConceptoContables)

        Public Overrides Function Consultar(filtro As EncabezadoConceptoContables) As IEnumerable(Of EncabezadoConceptoContables)
            Dim lista As New List(Of EncabezadoConceptoContables)
            Dim item As EncabezadoConceptoContables

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.CodigoAlterno) Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.Nombre) Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If
                If Not IsNothing(filtro.OficinaAplica) Then
                    If filtro.OficinaAplica.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFAP_Codigo", filtro.OficinaAplica.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.DocumentoOrigen) Then
                    If filtro.DocumentoOrigen.Codigo <> 2600 Then
                        conexion.AgregarParametroSQL("@par_DOOR_Codigo", filtro.DocumentoOrigen.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.TipoConceptoContable) Then
                    If filtro.TipoConceptoContable.Codigo <> 2700 Then
                        conexion.AgregarParametroSQL("@par_TICC_Codigo", filtro.TipoConceptoContable.Codigo)
                    End If
                End If
                If filtro.Estado >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_concepto_contables]")

                While resultado.Read
                    item = New EncabezadoConceptoContables(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As EncabezadoConceptoContables) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.DocumentoAplica)
                    conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TICC_Codigo", entidad.TipoConceptoContable.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TINA_Codigo", entidad.TipoNaturaleza.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo_Aplica", entidad.OficinaAplica.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Fuente", entidad.Fuente)
                    conexion.AgregarParametroSQL("@par_Fuente_Anulacion", entidad.FuenteAnula)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_concepto_contables")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While


                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        detalle.CodigoConceptoComtable = entidad.Codigo

                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
        Public Function InsertarDetalle(entidad As DetalleConceptoContables, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ECCO_Codigo", entidad.CodigoConceptoComtable)
            contextoConexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPUC.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_NACC_Codigo", entidad.NaturalezaConceptoContable.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_TCUC_Codigo", entidad.TipoCuentaConcepto.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_DOCR_Codigo", entidad.DocumentoCruce.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_TEPC_Codigo", entidad.TerceroParametrizacion.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CATA_CCPC_Codigo", entidad.CentroCostoParametrizacion.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Valor_Base", entidad.ValorBase, SqlDbType.Decimal)
            contextoConexion.AgregarParametroSQL("@par_Porcentaje_Tarifa", entidad.PorcentajeTarifa, SqlDbType.Decimal)
            contextoConexion.AgregarParametroSQL("@par_Genera_Cuenta", entidad.GeneraCuenta)
            contextoConexion.AgregarParametroSQL("@par_Prefijo", entidad.Prefijo)
            contextoConexion.AgregarParametroSQL("@par_Codigo_Anexo", entidad.CodigoAnexo)
            contextoConexion.AgregarParametroSQL("@par_Sufijo_Codigo_Anexo", entidad.SufijoCodigoAnexo)
            contextoConexion.AgregarParametroSQL("@par_Campo_Auxiliar", entidad.CampoAuxiliar)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_concepto_contables]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
            End While

            resultado.Close()

            Return inserto
        End Function
        Public Overrides Function Modificar(entidad As EncabezadoConceptoContables) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.DocumentoAplica)
                    conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TICC_Codigo", entidad.TipoConceptoContable.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TINA_Codigo", entidad.TipoNaturaleza.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo_Aplica", entidad.OficinaAplica.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Fuente", entidad.Fuente)
                    conexion.AgregarParametroSQL("@par_Fuente_Anulacion", entidad.FuenteAnula)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_concepto_contables")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While
                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        detalle.CodigoConceptoComtable = entidad.Codigo

                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoRecorrido = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
        Public Overrides Function Obtener(filtro As EncabezadoConceptoContables) As EncabezadoConceptoContables
            Dim item As New EncabezadoConceptoContables

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("dbo.gsp_obtener_encabezado_Concepto_contables")

                While resultado.Read
                    item = New EncabezadoConceptoContables(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As EncabezadoConceptoContables) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_encabezado_concepto_contables]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace