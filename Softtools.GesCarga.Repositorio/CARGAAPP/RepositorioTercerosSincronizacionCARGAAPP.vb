﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades

Public Class RepositorioTercerosSincronizacionCARGAAPP
    Inherits RepositorioBase(Of TercerosSincronizacionCARGAAPP)

    Public Overrides Function Insertar(entidad As TercerosSincronizacionCARGAAPP) As Long
        Throw New NotImplementedException()
    End Function

    Public Overrides Function Modificar(entidad As TercerosSincronizacionCARGAAPP) As Long
        Throw New NotImplementedException()
    End Function

    Public Function ModificarVehiculos(entidad As VehiculosModificacionCARGAAPP) As Long
        Dim item As New VehiculosModificacionCARGAAPP
        Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

            conexion.CreateConnection()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", 13)
            If Not IsNothing(entidad.placa) Then
                conexion.AgregarParametroSQL("@par_placa", entidad.placa)
            End If
            If Not IsNothing(entidad.placaTrailer) Then
                If entidad.placaTrailer <> "" Then
                    conexion.AgregarParametroSQL("@par_placaTrailer", entidad.placaTrailer)
                End If
            End If
            If Not IsNothing(entidad.codigoTipoCombustible) Then
                If entidad.codigoTipoCombustible <> 0 Then
                    conexion.AgregarParametroSQL("@par_codigoTipoCombustible", entidad.codigoTipoCombustible)
                End If
            End If
            If Not IsNothing(entidad.codigoAlternoEmpresaGPS) Then
                If entidad.codigoAlternoEmpresaGPS <> "" Then
                    conexion.AgregarParametroSQL("@par_codigoAlternoEmpresaGPS", entidad.codigoAlternoEmpresaGPS)
                End If
            End If
            If Not IsNothing(entidad.usuarioGPS) Then
                If entidad.usuarioGPS <> "" Then
                    conexion.AgregarParametroSQL("@par_usuarioGPS", entidad.usuarioGPS)
                End If
            End If
            If Not IsNothing(entidad.passwordGPS) Then
                If entidad.passwordGPS <> "" Then
                    conexion.AgregarParametroSQL("@par_passwordGPS", entidad.passwordGPS)
                End If
            End If
            If Not IsNothing(entidad.tipoVehiculo) Then
                If entidad.tipoVehiculo <> 0 Then
                    conexion.AgregarParametroSQL("@par_tipoVehiculo", entidad.tipoVehiculo)
                End If
            End If
            If Not IsNothing(entidad.modelo) Then
                If entidad.modelo <> 0 Then
                    conexion.AgregarParametroSQL("@par_modelo", entidad.modelo)
                End If
            End If
            If Not IsNothing(entidad.activo) Then
                conexion.AgregarParametroSQL("@par_activo", IIf(entidad.activo = True, 1, 0))

            End If

            Dim Reader As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_vehiculos_sincronizacion_CARGAAPP")

            While Reader.Read
                item = New VehiculosModificacionCARGAAPP(Reader)
            End While
            Reader.Close()

        End Using

        Return item.modelo
    End Function
    Public Function ModificarTerceros(entidad As TercerosModificacionCARGAAPP) As Long
        Dim item As New TercerosModificacionCARGAAPP
        Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

            conexion.CreateConnection()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", 13)
            If Not IsNothing(entidad.identificacion) Then
                conexion.AgregarParametroSQL("@par_Identificacion", entidad.identificacion)
            End If
            If Not IsNothing(entidad.nombres) Then
                If entidad.nombres <> "" Then
                    conexion.AgregarParametroSQL("@par_nombre", entidad.nombres)
                End If
            End If
            If Not IsNothing(entidad.apellidos) Then
                If entidad.apellidos <> "" Then
                    conexion.AgregarParametroSQL("@par_apellidos", entidad.apellidos)
                End If
            End If
            If Not IsNothing(entidad.direccion) Then
                If entidad.direccion <> "" Then
                    conexion.AgregarParametroSQL("@par_direccion", entidad.direccion)
                End If
            End If
            If Not IsNothing(entidad.ciudad) Then
                If entidad.ciudad <> "" Then
                    conexion.AgregarParametroSQL("@par_ciudad", entidad.ciudad)
                End If
            End If
            If Not IsNothing(entidad.email) Then
                If entidad.email <> "" Then
                    conexion.AgregarParametroSQL("@par_email", entidad.email)
                End If
            End If
            If Not IsNothing(entidad.celular) Then
                If entidad.celular <> "" Then
                    conexion.AgregarParametroSQL("@par_celular", entidad.celular)
                End If
            End If
            If Not IsNothing(entidad.codigoNovedad) Then
                If entidad.codigoNovedad <> 0 Then
                    conexion.AgregarParametroSQL("@par_codigoNovedad", entidad.codigoNovedad)
                End If
            End If
            If Not IsNothing(entidad.activo) Then
                conexion.AgregarParametroSQL("@par_activo", IIf(entidad.activo = True, 1, 0))

            End If

            Dim Reader As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_terceros_sincronizacion_CARGAAPP")

            While Reader.Read
                item = New TercerosModificacionCARGAAPP(Reader)
            End While
            Reader.Close()

        End Using

        Return item.identificacion
    End Function
    Public Overrides Function Consultar(filtro As TercerosSincronizacionCARGAAPP) As IEnumerable(Of TercerosSincronizacionCARGAAPP)
        Throw New NotImplementedException()
    End Function

    Public Overrides Function Obtener(filtro As TercerosSincronizacionCARGAAPP) As TercerosSincronizacionCARGAAPP
        Dim item As New TercerosSincronizacionCARGAAPP
        Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

            conexion.CreateConnection()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", 13)
            If Not IsNothing(filtro.identificacion) Then
                conexion.AgregarParametroSQL("@par_Identificacion", filtro.identificacion)
            End If
            If Not IsNothing(filtro.nombres) Then
                If filtro.nombres <> "" Then
                    conexion.AgregarParametroSQL("@par_nombre", filtro.nombres)
                End If
            End If

            Dim Reader As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_terceros_sincronizacion_CARGAAPP")

            While Reader.Read
                item = New TercerosSincronizacionCARGAAPP(Reader)
            End While
            Reader.Close()

        End Using

        Return item
    End Function
End Class
