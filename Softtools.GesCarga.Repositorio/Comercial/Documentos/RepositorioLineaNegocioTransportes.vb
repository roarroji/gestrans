﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Comercial.Documentos

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref="RepositorioLineaNegocioTransportes"/>
    ''' </summary>
    Public NotInheritable Class RepositorioLineaNegocioTransportes
        Inherits RepositorioBase(Of LineaNegocioTransportes)

        Public Overrides Function Consultar(filtro As LineaNegocioTransportes) As IEnumerable(Of LineaNegocioTransportes)
            Dim lista As New List(Of LineaNegocioTransportes)
            Dim item As LineaNegocioTransportes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_linea_negocio_transportes]")

                While resultado.Read
                    item = New LineaNegocioTransportes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As LineaNegocioTransportes) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As LineaNegocioTransportes) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As LineaNegocioTransportes) As LineaNegocioTransportes
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As LineaNegocioTransportes) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace