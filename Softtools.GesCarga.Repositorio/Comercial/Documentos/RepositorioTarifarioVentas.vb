﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Repositorio.Basico.Despachos
Imports Softtools.GesCarga.Repositorio.Basico.General
Imports System.Data.OleDb
Imports System.Transactions

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref="RepositorioTarifarioVentas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioTarifarioVentas
        Inherits RepositorioBase(Of TarifarioVentas)
        Const TIPO_CONSULTA_ORDEN_DE_SERVICIO As Integer = 1
        Public Overrides Function Consultar(filtro As TarifarioVentas) As IEnumerable(Of TarifarioVentas)
            Dim lista As New List(Of TarifarioVentas)
            Dim item As TarifarioVentas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado.Codigo > -1 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                    End If
                End If
                conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                If Not IsNothing(filtro.FechaInicio) Then
                    If filtro.FechaInicio > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia_Inicio", filtro.FechaInicio, SqlDbType.DateTime) '	datetime = null,
                    End If
                End If
                If Not IsNothing(filtro.FechaFin) Then
                    If filtro.FechaFin > Date.MinValue And filtro.FechaFin >= filtro.FechaInicio Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia_Fin", filtro.FechaFin, SqlDbType.DateTime) '	datetime = null,
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_tarifario_ventas]")

                While resultado.Read
                    item = New TarifarioVentas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As TarifarioVentas) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", 60) 'NUMERIC = 0,
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre) 'VARCHAR ,
                    conexion.AgregarParametroSQL("@par_Tarifario_Base", entidad.TarifarioBase) 'VARCHAR ,
                    If entidad.FechaInicio > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia_Inicio", entidad.FechaInicio, SqlDbType.DateTime) '	datetime = null,
                    End If
                    If entidad.FechaFin > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia_Fin", entidad.FechaFin, SqlDbType.DateTime) '	datetime = null,
                    End If
                    If Not IsNothing(entidad.Estado) Then
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo) 'numeric = null,
                    End If
                    If Not IsNothing(entidad.UsuarioCrea) Then
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo) 'smallint = null,
                    End If
                    'Datos paqueteria'
                    If Not IsNothing(entidad.Paqueteria) Then
                        conexion.AgregarParametroSQL("@par_Aplica_Paqueteria", 1) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Minimo_Kilo_Cobro", entidad.Paqueteria.MinimoKgCobro, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Maximo_Kilo_Cobro", entidad.Paqueteria.MaximoKgCobro, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Valor_Kilo_Adicional", entidad.Paqueteria.ValorKgAdicional, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Valor_Manejo", entidad.Paqueteria.ValorManejo, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Porcentaje_Manejo", entidad.Paqueteria.PorcentajeManejo, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Minimo_Valor_Manejo", entidad.Paqueteria.MinimoValorManejo, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Valor_Seguro", entidad.Paqueteria.ValorSeguro, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Porcentaje_Seguro", entidad.Paqueteria.PorcentajeSeguro, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Minimo_Valor_Seguro", entidad.Paqueteria.MinimoValorSeguro, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Minimo_Valor_Unidad", entidad.Paqueteria.MinimoValorUnidad, SqlDbType.Money) 'VARCHAR ,
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_tarifario_ventas")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()
                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    Else
                        For Each detalle In entidad.Tarifas
                            detalle.NumeroTarifario = entidad.Codigo
                            detalle.UsuarioCrea = entidad.UsuarioCrea
                            detalle.CodigoEmpresa = entidad.CodigoEmpresa
                            If Not InsertarDetalle(detalle, conexion) Then
                                inserto = False
                                Exit For
                            End If '
                        Next

                    End If
                End Using
                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If
            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As TarifarioVentas) As Long
            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                If entidad.OpcionCargue = 0 Then
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Codigo) 'NUMERIC = 0,
                    conexion.AgregarParametroSQL("@par_Tarifario_Base", entidad.TarifarioBase) 'VARCHAR ,

                    If Not IsNothing(entidad.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre) '	varchar = null,
                    End If
                    If entidad.FechaInicio > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia_Inicio", entidad.FechaInicio, SqlDbType.DateTime) '	datetime = null,
                    End If
                    If entidad.FechaFin > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia_Fin", entidad.FechaFin, SqlDbType.DateTime) '	datetime = null,
                    End If
                    If Not IsNothing(entidad.Estado) Then
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo) 'numeric = null,
                    End If
                    If Not IsNothing(entidad.UsuarioCrea) Then
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo) 'smallint = null,
                    End If
                    'Datos paqueteria'
                    If Not IsNothing(entidad.Paqueteria) Then
                        conexion.AgregarParametroSQL("@par_Aplica_Paqueteria", 1) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Minimo_Kilo_Cobro", entidad.Paqueteria.MinimoKgCobro, SqlDbType.Decimal) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Maximo_Kilo_Cobro", entidad.Paqueteria.MaximoKgCobro, SqlDbType.Decimal) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Valor_Kilo_Adicional", entidad.Paqueteria.ValorKgAdicional, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Valor_Manejo", entidad.Paqueteria.ValorManejo, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Porcentaje_Manejo", entidad.Paqueteria.PorcentajeManejo, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Minimo_Valor_Manejo", entidad.Paqueteria.MinimoValorManejo, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Valor_Seguro", entidad.Paqueteria.ValorSeguro, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Porcentaje_Seguro", entidad.Paqueteria.PorcentajeSeguro, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Minimo_Valor_Seguro", entidad.Paqueteria.MinimoValorSeguro, SqlDbType.Money) 'VARCHAR ,
                        conexion.AgregarParametroSQL("@par_Minimo_Valor_Unidad", entidad.Paqueteria.MinimoValorUnidad, SqlDbType.Money) 'VARCHAR ,
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_tarifario_ventas")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While
                    resultado.Close()
                End If

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                Else

                    If Not IsNothing(entidad.TarifasEliminar) Then
                        For Each itemEliminar In entidad.TarifasEliminar
                            itemEliminar.NumeroTarifario = entidad.Codigo
                            itemEliminar.CodigoEmpresa = entidad.CodigoEmpresa
                            EliminarDetalle(itemEliminar, conexion)

                        Next
                    End If

                    For Each detalle In entidad.Tarifas
                        detalle.NumeroTarifario = entidad.Codigo
                        detalle.UsuarioCrea = entidad.UsuarioCrea
                        detalle.CodigoEmpresa = entidad.CodigoEmpresa
                        If Not InsertarDetalle(detalle, conexion) Then
                            inserto = False
                            Exit For
                        End If '
                    Next

                End If
            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As TarifarioVentas) As TarifarioVentas
            Dim resultado As IDataReader
            Dim item As New TarifarioVentas
            Dim ListaDetalle As New List(Of DetalleTarifarioVentas)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo = 0 And IsNothing(filtro.Codigo) Then
                    Return item
                Else
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", filtro.Codigo)
                    End If

                    If Not IsNothing(filtro.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                    End If

                    If filtro.CodigoCliente > 0 Then
                        conexion.AgregarParametroSQL("@par_Cliente", filtro.CodigoCliente)
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_encabezado_tarifario_ventas_cliente]")
                    Else
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_encabezado_tarifario_ventas]")
                    End If


                    While resultado.Read
                        item = New TarifarioVentas(resultado)
                    End While
                    conexion.CloseConnection()
                    resultado.Close()
                    If filtro.TipoConsulta = TIPO_CONSULTA_ORDEN_DE_SERVICIO Then
                        Dim Detalle As DetalleTarifarioVentas
                        'ListaDetalle As New List(Of DetalleTarifarioVentas)
                        conexion.CleanParameters()
                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ETCV_Numero", item.Codigo)
                        If filtro.CodigoCliente > 0 Then
                            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_tarifario_ventas_cliente]")
                        Else
                            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_tarifario_ventas]")
                        End If

                        While resultado.Read
                            Detalle = New DetalleTarifarioVentas(resultado)
                            ListaDetalle.Add(Detalle)
                        End While
                    End If
                    If item.Codigo > 0 Then
                        filtro.Codigo = item.Codigo
                    End If
                    conexion.CloseConnection()
                        resultado.Close()
                        'Dim Paqueteria As DetalleTarifarioVentas
                        conexion.CleanParameters()
                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ETCV_Numero", filtro.Codigo)
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_tarifario_paqueteria_ventas]")

                        While resultado.Read
                            item.Paqueteria = New DetallePaquteriaVentas(resultado)
                        End While

                    item.Tarifas = ListaDetalle
                End If

            End Using

            Return item

        End Function

        Public Function Anular(entidad As TarifarioVentas) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_usuario_anula", entidad.UsuarioAnula)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_Anular_Tarifario_Ventas]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function InsertarDetalle(entidad As DetalleTarifarioVentas, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
            contextoConexion.AgregarParametroSQL("@par_ETCV_Numero", entidad.NumeroTarifario)
            contextoConexion.AgregarParametroSQL("@par_LNTC_Codigo", entidad.TipoLineaNegocioTransportes.LineaNegocioTransporte.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TLNC_Codigo", entidad.TipoLineaNegocioTransportes.Codigo)
            If Not IsNothing(entidad.Ruta) Then
                contextoConexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
            Else
                contextoConexion.AgregarParametroSQL("@par_RUTA_Codigo", 0)
            End If
            If Not IsNothing(entidad.CiudadOrigen) Then
                contextoConexion.AgregarParametroSQL("@par_CIUD_Codigo_Origen", entidad.CiudadOrigen.Codigo)
            End If
            If Not IsNothing(entidad.CiudadDestino) Then
                contextoConexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino", entidad.CiudadDestino.Codigo)
            End If
            If Not IsNothing(entidad.TipoTarifaTransportes.TarifaTransporte) Then
                contextoConexion.AgregarParametroSQL("@par_TATC_Codigo", entidad.TipoTarifaTransportes.TarifaTransporte.Codigo)
            End If
            contextoConexion.AgregarParametroSQL("@par_TTTC_Codigo", entidad.TipoTarifaTransportes.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Reexpedicion", entidad.Reexpedicion)
            contextoConexion.AgregarParametroSQL("@par_Porcentaje_Afiliado", entidad.PorcentajeAfiliado, SqlDbType.Decimal)
            contextoConexion.AgregarParametroSQL("@par_Valor_Flete", entidad.ValorFlete, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Valor_Escolta", entidad.ValorEscolta, SqlDbType.Money)
            If Not IsNothing(entidad.FormaPagoVentas) Then
                contextoConexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", entidad.FormaPagoVentas.Codigo)
            End If
            'contextoConexion.AgregarParametroSQL("@par_Valor_Kilo_Adicional", entidad.ValorKgAdicional, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Valor_Otros1", 0)
            contextoConexion.AgregarParametroSQL("@par_Valor_Otros2", 0)
            contextoConexion.AgregarParametroSQL("@par_Valor_Otros3", 0)
            contextoConexion.AgregarParametroSQL("@par_Valor_Otros4", 0)
            contextoConexion.AgregarParametroSQL("@par_Valor_Cargue", entidad.ValorCargue, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Valor_Descargue", entidad.ValorDescargue, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Valor_Flete_Transportador", entidad.ValorFleteTransportador, SqlDbType.Money)
            If entidad.FechaInicioVigencia > Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_Fecha_Vigencia_Inicio", entidad.FechaInicioVigencia, SqlDbType.DateTime) '	datetime = null,
            End If
            If entidad.FechaFinVigencia > Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_Fecha_Vigencia_Fin", entidad.FechaFinVigencia, SqlDbType.DateTime) '	datetime = null,
            End If
            contextoConexion.AgregarParametroSQL("@par_Porcentaje_Seguro", entidad.PorcentajeSeguro, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Estado", entidad.Estado.Codigo)
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_tarifario_ventas]")

            While resultado.Read
                inserto = IIf(resultado.Item("Codigo") > Cero, True, False)
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Function EliminarDetalle(entidad As DetalleTarifarioVentas, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
            contextoConexion.AgregarParametroSQL("@par_ETCV_Numero", entidad.NumeroTarifario)


            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_detalle_tarifario_ventas]")

            While resultado.Read
                inserto = IIf(resultado.Item("Codigo") > Cero, True, False)
            End While

            resultado.Close()

            Return inserto
        End Function
        Public Function GenerarPlanitilla(filtro As TarifarioVentas) As TarifarioVentas
            Dim fullPath As String
            fullPath = System.AppDomain.CurrentDomain.BaseDirectory()
            If filtro.LineaNegocioTransporte.Codigo = 3 Then
                System.IO.File.Copy(fullPath + "Controllers\Comercial\Plantillas\Plantilla_Cargue_Masivo_Tarifario_Venta_Paqueteria.xlsx", fullPath + "Controllers\Comercial\Plantillas\Plantilla_Cargue_Masivo_Tarifario_Venta_Generado.xlsx", True)
            Else
                System.IO.File.Copy(fullPath + "Controllers\Comercial\Plantillas\Plantilla_Cargue_Masivo_Tarifario_Venta_Masivo_Semimasivo.xlsx", fullPath + "Controllers\Comercial\Plantillas\Plantilla_Cargue_Masivo_Tarifario_Venta_Generado.xlsx", True)

            End If
            Dim cn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullPath + "Controllers\Comercial\Plantillas\Plantilla_Cargue_Masivo_Tarifario_Venta_Generado.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES'"

            filtro.TipoLineaNegocioTransportes.LineaNegocioTransporte = filtro.LineaNegocioTransporte


            Dim Ciudades As IEnumerable(Of Ciudades)
            Dim Ciudad As New Ciudades
            Dim ObjCiudad As New RepositorioCiudades
            Ciudad.CodigoEmpresa = filtro.CodigoEmpresa
            Ciudad.Estado = 1
            Ciudades = ObjCiudad.Consultar(Ciudad)


            'Dim LineaNegocioTransportes As IEnumerable(Of LineaNegocioTransportes)
            'Dim LineaNegocioTransporte As New LineaNegocioTransportes
            'Dim ObjLineaNegocioTransportes As New RepositorioLineaNegocioTransportes
            'LineaNegocioTransporte.CodigoEmpresa = filtro.CodigoEmpresa
            'LineaNegocioTransporte.Estado = 1
            'LineaNegocioTransportes = ObjLineaNegocioTransportes.Consultar(LineaNegocioTransporte)


            'Dim TipoLineaNegocioTransportes As IEnumerable(Of TipoLineaNegocioTransportes)
            'Dim TipoLineaNegocioTransporte As New TipoLineaNegocioTransportes
            'Dim ObjTipoLineaNegocioTransportes As New RepositorioTipoLineaNegocioTransportes
            'TipoLineaNegocioTransporte.CodigoEmpresa = filtro.CodigoEmpresa
            'TipoLineaNegocioTransporte.Estado = 1
            'TipoLineaNegocioTransporte.OpcionPlantilla = 1

            'TipoLineaNegocioTransportes = ObjTipoLineaNegocioTransportes.Consultar(TipoLineaNegocioTransporte)

            Dim Rutas As IEnumerable(Of Rutas)
            Dim Ruta As New Rutas
            Dim ObjRuta As New RepositorioRutas
            Ruta.CodigoEmpresa = filtro.CodigoEmpresa
            Ruta.Estado = 1
            Rutas = ObjRuta.Consultar(Ruta)

            Dim TarifaTransportes As IEnumerable(Of TarifaTransportes)
            Dim TarifaTransporte As New TarifaTransportes
            Dim ObjTarifaTransportes As New RepositorioTarifaTransportes
            TarifaTransporte.CodigoEmpresa = filtro.CodigoEmpresa
            TarifaTransporte.Estado = 1
            TarifaTransporte.TipoLineaNegocioTransporte = filtro.TipoLineaNegocioTransportes
            TarifaTransporte.OpcionPlantilla = 1
            TarifaTransportes = ObjTarifaTransportes.Consultar(TarifaTransporte)

            Dim TipoTarifaTransportes As IEnumerable(Of TipoTarifaTransportes)
            Dim TipoTarifaTransporte As New TipoTarifaTransportes
            Dim ObjTipoTarifaTransportes As New RepositorioTipoTarifaTransportes
            TipoTarifaTransporte.CodigoEmpresa = filtro.CodigoEmpresa
            TipoTarifaTransporte.Estado = 1
            TipoTarifaTransporte.TipoLineaNegocioTransporte = filtro.TipoLineaNegocioTransportes
            TipoTarifaTransporte.OpcionPlantilla = 1
            TipoTarifaTransportes = ObjTipoTarifaTransportes.Consultar(TipoTarifaTransporte)

            Dim Catalogos As IEnumerable(Of ValorCatalogos)
            Dim Catalogo As New ValorCatalogos
            Dim ObjValorCatalogos As New RepositorioValorCatalogos
            Catalogo.CodigoEmpresa = filtro.CodigoEmpresa
            Catalogo.Catalogo = New Catalogos With {.Codigo = 49}
            Catalogos = ObjValorCatalogos.Consultar(Catalogo)



            Using cnn As New OleDbConnection(cn)
                cnn.Open()

                Ciudades = Ciudades.OrderBy(Function(a) a.Nombre)
                For i = 1 To Ciudades.Count
                    Dim row = i + 2
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        cmd.CommandText = "INSERT INTO [Ciudades$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Ciudades(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Ciudades(i - 1).NombreCompleto)
                        cmd.ExecuteNonQuery()
                    End Using
                Next
                filtro.TipoLineaNegocioTransportes.LineaNegocioTransporte = filtro.LineaNegocioTransporte

                Using cmd As OleDbCommand = cnn.CreateCommand()
                    cmd.CommandText = "INSERT INTO [Lineas_Negocio$] (Codigo,Nombre) values(@Cod,@nom)"
                    cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = filtro.LineaNegocioTransporte.Codigo
                    cmd.Parameters.AddWithValue("@nom", filtro.LineaNegocioTransporte.Nombre)
                    cmd.ExecuteNonQuery()
                End Using
                Using cmd As OleDbCommand = cnn.CreateCommand()
                    cmd.CommandText = "INSERT INTO [Tipo_Linea_Negocio$] (Codigo,Nombre) values(@Cod,@nom)"
                    cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = filtro.TipoLineaNegocioTransportes.Codigo
                    cmd.Parameters.AddWithValue("@nom", filtro.TipoLineaNegocioTransportes.Nombre)
                    cmd.ExecuteNonQuery()
                End Using
                Rutas = Rutas.OrderBy(Function(a) a.Nombre)
                For i = 1 To Rutas.Count
                    Dim row = i + 2
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        cmd.CommandText = "INSERT INTO [Rutas$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Rutas(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", Rutas(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    End Using
                Next

                TarifaTransportes = TarifaTransportes.OrderBy(Function(a) a.Nombre)
                For i = 1 To TarifaTransportes.Count
                    Dim row = i + 2
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        cmd.CommandText = "INSERT INTO [Tarifa$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = TarifaTransportes(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", TarifaTransportes(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    End Using
                Next
                TipoTarifaTransportes = TipoTarifaTransportes.OrderBy(Function(a) a.Nombre)
                For i = 1 To TipoTarifaTransportes.Count
                    Dim row = i + 2
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        cmd.CommandText = "INSERT INTO [Tipo_Tarifa$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.AddWithValue("@nom", TipoTarifaTransportes(i - 1).Codigo)
                        cmd.Parameters.AddWithValue("@Cod", TipoTarifaTransportes(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    End Using
                Next


                'se crea una tabla que tenga el cruce entre tarifas y tipo tarifas para poder tener las listas de tipos tarifa de acuerdo a la tarifa seleccionada en cada registro de la plantilla:

                'se crea el encabezado:

                Dim strTATT As String = ""
                strTATT = "CREATE TABLE [Tarifas_Tipo_Tarifas$] ( ID INT, "
                Dim intIDTarifaTipoTarifa As Integer = 0
                Dim intUltimoIDTarifaTipoTarifa As Integer = 1

                Using cmd As OleDbCommand = cnn.CreateCommand()

                    For Each Tarifa In TarifaTransportes

                        strTATT += "[" + Tarifa.Nombre + "] char(250),"

                    Next

                    strTATT = strTATT.Remove(strTATT.Length - 1)
                    strTATT += ")"

                    cmd.Parameters.Clear()
                    cmd.CommandText = strTATT
                    cmd.ExecuteNonQuery()

                    'se inserta el detalle por cada Tarifa:

                    Dim ListaTiposTarifa As New List(Of TipoTarifaTransportes)

                    For Each TipoTarifa In TipoTarifaTransportes
                        ListaTiposTarifa.Add(TipoTarifa)
                    Next

                    For Each Tarifa In TarifaTransportes

                        For Each TipoTarifa In TipoTarifaTransportes
                            If TipoTarifa.TarifaTransporte.Codigo = Tarifa.Codigo Then
                                cmd.Parameters.Clear()

                                'se busca (de arriba a abajo) la primera celda que este vacía o sea valor null, en la columna de la tarifa correspondiente(Tarifa in Tarifa Transportes), si en la fila de la celda encontrada la columna ID no tiene valor, se procede a realizar un INSERT, de lo contrario, un UPDATE:
                                cmd.CommandText = "SELECT MIN(ID) From [Tarifas_Tipo_Tarifas$] Where [" + Tarifa.Nombre + "] Is NULL Or [" + Tarifa.Nombre + "] = '' "
                                Dim reader As OleDbDataReader = cmd.ExecuteReader()

                                intIDTarifaTipoTarifa = 0
                                While reader.Read
                                    If Not IsDBNull(reader.GetValue(0)) Then
                                        intIDTarifaTipoTarifa = reader.GetValue(0)
                                    End If
                                End While

                                reader.Close()


                                If intIDTarifaTipoTarifa = 0 Then
                                    cmd.CommandText = "INSERT INTO [Tarifas_Tipo_Tarifas$] (ID, [" + Tarifa.Nombre + "]) values(@Cod,@nom)"
                                    cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = intUltimoIDTarifaTipoTarifa
                                    cmd.Parameters.AddWithValue("@nom", TipoTarifa.Nombre)
                                    cmd.ExecuteNonQuery()
                                    intUltimoIDTarifaTipoTarifa = intUltimoIDTarifaTipoTarifa + 1
                                Else
                                    cmd.CommandText = "UPDATE [Tarifas_Tipo_Tarifas$] SET [" + Tarifa.Nombre + "] = @nom WHERE ID =" + intIDTarifaTipoTarifa.ToString()
                                    cmd.Parameters.AddWithValue("@nom", TipoTarifa.Nombre)
                                    cmd.ExecuteNonQuery()
                                End If

                                ListaTiposTarifa.Remove(TipoTarifa)
                            End If
                        Next

                    Next

                End Using


                Catalogos = Catalogos.OrderBy(Function(a) a.Nombre)
                    For i = 1 To Catalogos.Count
                        Dim row = i + 2
                        Using cmd As OleDbCommand = cnn.CreateCommand()
                            cmd.CommandText = "INSERT INTO [Forma_Pago$] (Codigo,Nombre) values(@Cod,@nom)"
                            cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Catalogos(i - 1).Codigo
                            cmd.Parameters.AddWithValue("@nom", Catalogos(i - 1).Nombre)
                            cmd.ExecuteNonQuery()
                        End Using
                    Next

                    cnn.Close()
                End Using
                Dim RTA As New TarifarioVentas
            Dim Archivo() As Byte = IO.File.ReadAllBytes(fullPath + "Controllers\Comercial\Plantillas\Plantilla_Cargue_Masivo_Tarifario_Venta_Generado.xlsx")
            RTA.Planitlla = Archivo
            Return RTA
        End Function

        Public Function ConsultarDetalleTarifarioVentas(filtro As DetalleTarifarioVentas) As IEnumerable(Of DetalleTarifarioVentas)
            Dim resultado As IDataReader
            Dim Detalle As DetalleTarifarioVentas
            Dim ListaDetalle As New List(Of DetalleTarifarioVentas)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ETCV_Numero", filtro.NumeroTarifario)
                If Not IsNothing(filtro.LineaNegocioTransportes) Then
                    If filtro.LineaNegocioTransportes.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_LNTC_Codigo", filtro.LineaNegocioTransportes.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.TipoLineaNegocioTransportes) Then
                    If filtro.TipoLineaNegocioTransportes.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TLNC_Codigo", filtro.TipoLineaNegocioTransportes.Codigo)
                    End If
                End If
                    If Not IsNothing(filtro.Ruta) Then
                    If filtro.Ruta.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Ruta_Codigo", filtro.Ruta.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.CiudadOrigen) Then
                    If filtro.CiudadOrigen.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIOR_Codigo", filtro.CiudadOrigen.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.CiudadDestino) Then
                    If filtro.CiudadDestino.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CIDE_Codigo", filtro.CiudadDestino.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.TarifaTransporteCarga) Then
                    If filtro.TarifaTransporteCarga.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TATC_Codigo", filtro.TarifaTransporteCarga.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.TipoTarifaTransporteCarga) Then
                    If filtro.TipoTarifaTransporteCarga.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TTTC_Codigo", filtro.TipoTarifaTransporteCarga.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.FormaPagoVentas) Then
                    If filtro.FormaPagoVentas.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", filtro.FormaPagoVentas.Codigo)
                    End If
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_tarifario_ventas]")
                While resultado.Read
                    Detalle = New DetalleTarifarioVentas(resultado)
                    ListaDetalle.Add(Detalle)
                End While

                conexion.CloseConnection()
                resultado.Close()
            End Using
            Return ListaDetalle
        End Function

        Public Function ObtenerDetalleTarifarioPaqueteria(filtro As TarifarioVentas) As TarifarioVentas
            Dim resultado As IDataReader
            Dim Detalle As TarifarioVentas = New TarifarioVentas()
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ETCV_Numero", filtro.Codigo)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_tarifario_paqueteria_ventas]")
                While resultado.Read
                    Detalle.Paqueteria = New DetallePaquteriaVentas(resultado)
                End While
                conexion.CloseConnection()
                resultado.Close()
            End Using
            Return Detalle
        End Function
    End Class

End Namespace