﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Comercial.Documentos

Namespace Comercial.Documentos

    ''' <summary>
    ''' Clase <see cref="RepositorioTipoLineaNegocioTransportes"/>
    ''' </summary>
    Public NotInheritable Class RepositorioTipoLineaNegocioTransportes
        Inherits RepositorioBase(Of TipoLineaNegocioTransportes)

        Public Overrides Function Consultar(filtro As TipoLineaNegocioTransportes) As IEnumerable(Of TipoLineaNegocioTransportes)
            Dim lista As New List(Of TipoLineaNegocioTransportes)
            Dim item As TipoLineaNegocioTransportes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                Dim resultado As IDataReader
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                If filtro.OpcionPlantilla > 0 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_tipo_linea_negocio_transportes_Plantilla]")
                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_tipo_linea_negocio_transportes]")
                End If

                While resultado.Read
                    item = New TipoLineaNegocioTransportes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As TipoLineaNegocioTransportes) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As TipoLineaNegocioTransportes) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As TipoLineaNegocioTransportes) As TipoLineaNegocioTransportes
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As TipoLineaNegocioTransportes) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace