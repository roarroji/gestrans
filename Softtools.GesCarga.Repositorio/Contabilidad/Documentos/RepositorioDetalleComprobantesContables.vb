﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Contabilidad.Documentos

Namespace Contabilidad

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleComprobantesContables"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleComprobantesContables
        Inherits RepositorioBase(Of DetalleComprobantesContables)

        Public Overrides Function Consultar(filtro As DetalleComprobantesContables) As IEnumerable(Of DetalleComprobantesContables)
            Dim lista As New List(Of DetalleComprobantesContables)
            Dim item As New DetalleComprobantesContables

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENCC_Numero", filtro.NumeroComprobante)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_detalle_comprobante_contables]")

                While resultado.Read
                    item = New DetalleComprobantesContables(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleComprobantesContables) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleComprobantesContables) As Long

            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleComprobantesContables) As DetalleComprobantesContables
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace