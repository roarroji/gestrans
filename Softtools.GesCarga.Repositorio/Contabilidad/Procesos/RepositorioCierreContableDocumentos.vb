﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Contabilidad.Procesos
Imports System.Transactions

Namespace Contabilidad

    ''' <summary>
    ''' Clase <see cref="RepositorioCierreContableDocumentos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioCierreContableDocumentos
        Inherits RepositorioBase(Of CierreContableDocumentos)

        Public Overrides Function Consultar(filtro As CierreContableDocumentos) As IEnumerable(Of CierreContableDocumentos)
            Dim lista As New List(Of CierreContableDocumentos)
            Dim item As CierreContableDocumentos
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Ano > 0 Then
                    conexion.AgregarParametroSQL("@par_Ano", filtro.Ano)
                End If

                If Not IsNothing(filtro.Mes) Then
                    If filtro.Mes.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Mes", filtro.Mes.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.TipoDocumentos) Then
                    If filtro.TipoDocumentos.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Tipo_Documento", filtro.TipoDocumentos.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.EstadoCierre) Then
                    If filtro.EstadoCierre.Codigo > -1 Then
                        conexion.AgregarParametroSQL("@par_EstadoCierre", filtro.EstadoCierre.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_cierre_contable]")

                While resultado.Read
                    item = New CierreContableDocumentos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As CierreContableDocumentos) As Long
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    If Not IsNothing(entidad.ListadoDocumentosCierre) Then
                        For Each entidadDetalle In entidad.ListadoDocumentosCierre

                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidadDetalle.CodigoEmpresa)
                            conexion.AgregarParametroSQL("@par_Ano", entidadDetalle.Ano)
                            conexion.AgregarParametroSQL("@par_Mes", entidadDetalle.Mes.Codigo)
                            conexion.AgregarParametroSQL("@par_Tipo_Documento", entidadDetalle.TipoDocumentos.Codigo)
                            conexion.AgregarParametroSQL("@par_Estado_Cierre", entidadDetalle.EstadoCierre.Codigo)
                            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidadDetalle.UsuarioCrea.Codigo)

                            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_cierre_contable")

                            While resultado.Read
                                entidad.Codigo = resultado.Item("Codigo").ToString()
                            End While

                            resultado.Close()

                        Next
                    End If
                End Using

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As CierreContableDocumentos) As Long
            Dim modifico As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    If Not IsNothing(entidad.ListadoDocumentosCierre) Then
                        For Each entidadDetalle In entidad.ListadoDocumentosCierre

                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidadDetalle.CodigoEmpresa)
                            conexion.AgregarParametroSQL("@par_Ano", entidadDetalle.Ano)
                            conexion.AgregarParametroSQL("@par_Mes", entidadDetalle.Mes.Codigo)
                            conexion.AgregarParametroSQL("@par_Tipo_Documento", entidadDetalle.TipoDocumentos.Codigo)
                            conexion.AgregarParametroSQL("@par_Estado_Cierre", entidadDetalle.EstadoCierre.Codigo)
                            conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidadDetalle.UsuarioCrea.Codigo)

                            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_cierre_contable")

                            While resultado.Read
                                entidad.Codigo = resultado.Item("Codigo").ToString()
                            End While

                            resultado.Close()

                            If entidad.Codigo.Equals(Cero) Then
                                Return Cero
                            End If

                        Next
                    End If
                End Using

                If modifico Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As CierreContableDocumentos) As CierreContableDocumentos
            Throw New NotImplementedException()
            Dim item As New CierreContableDocumentos
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Fecha <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha", filtro.Fecha, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.TipoDocumentos) Then
                    If filtro.TipoDocumentos.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Tipo_Documento", filtro.TipoDocumentos.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_obtener_cierre_contable_documento]")

                While resultado.Read
                    item = New CierreContableDocumentos(resultado)
                End While

            End Using

            Return item
        End Function
    End Class
End Namespace