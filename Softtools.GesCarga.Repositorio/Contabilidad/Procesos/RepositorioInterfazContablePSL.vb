﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Contabilidad.Procesos

Namespace Contabilidad
    Public NotInheritable Class RepositorioInterfazContablePSL
        Inherits RepositorioBase(Of InterfazContablePSL)

        Public Overrides Function Consultar(filtro As InterfazContablePSL) As IEnumerable(Of InterfazContablePSL)
            Dim lista As New List(Of InterfazContablePSL)
            Dim item As InterfazContablePSL
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.CATATipoDocumento.Codigo) Then
                    If filtro.CATATipoDocumento.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.CATATipoDocumento.Codigo)
                    End If
                End If

                If filtro.FechaInicial <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If filtro.NumeroDocumentoOrigen > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento_Origen", filtro.NumeroDocumentoOrigen)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_interfaz_contable_psl")

                While resultado.Read
                    item = New InterfazContablePSL(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As InterfazContablePSL) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As InterfazContablePSL) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As InterfazContablePSL) As InterfazContablePSL
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace

