﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Contabilidad.Documentos

Namespace Contabilidad
    Public NotInheritable Class RepositoriointegracionSIESA
        Inherits RepositorioBase(Of integracionSIESA)

        Public Overrides Function Consultar(filtro As integracionSIESA) As IEnumerable(Of integracionSIESA)
            Dim lista As New List(Of integracionSIESA)
            Dim item As integracionSIESA
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If filtro.FechaInicial <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.DateTime)
                End If

                If filtro.FechaFinal <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.DateTime)
                End If

                If Not IsNothing(filtro.NumeroDocumento) And filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                If (filtro.Estado = 0 Or filtro.Estado = 1) And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)
                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If

                If filtro.InterfazContable > -1 Then
                    conexion.AgregarParametroSQL("@par_Estado_Interfaz", filtro.InterfazContable)
                End If

                'conexion.AgregarParametroSQL("@par_Estado_Interfaz", filtro.EstadoInterfazContable.Codigo)

                conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)


                If Not IsNothing(filtro.DocumentoOrigen) Then


                    If filtro.DocumentoOrigen.Codigo = 23601 Then


                        conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", filtro.DocumentoOrigen.CampoAuxiliar2)
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_Comprobantes_interfaz_siesa]")

                    ElseIf filtro.DocumentoOrigen.Codigo = 23603 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_facturas_interfaz_siesa]")
                    ElseIf filtro.DocumentoOrigen.Codigo = 23604 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_Gastos_Conductor_interfaz_siesa]")
                    ElseIf filtro.DocumentoOrigen.Codigo = 23607 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_Legalizacion_Recaudo_interfaz_siesa]")

                    ElseIf filtro.DocumentoOrigen.Codigo = 23608 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_liquidacion_planilla_interfaz_siesa]")

                    ElseIf filtro.DocumentoOrigen.Codigo = 23609 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_Factura_Ventas_interfaz_siesa]")
                    ElseIf filtro.DocumentoOrigen.Codigo = 23610 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_Preanticipos_interfaz_siesa]")


                    End If
                End If




                While resultado.Read
                    item = New integracionSIESA(resultado)
                    lista.Add(item)
                End While



            End Using

            Return lista
        End Function



        Public Overrides Function Insertar(entidad As integracionSIESA) As Long
            Throw New NotImplementedException()

        End Function



        Public Overrides Function Modificar(entidad As integracionSIESA) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@PAR_EMPR_CODIGO", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@PAR_NUMERO", entidad.Numero)
                conexion.AgregarParametroSQL("@PAR_TIPODOCU", entidad.TipoDocumentoOrigen.Codigo)

                If entidad.Numero > 0 Then
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_Inicializar_Intentos_interfaz_siesa]")
                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()

                End While

            End Using

            Return entidad.Codigo
        End Function



        Public Overrides Function Obtener(filtro As integracionSIESA) As integracionSIESA
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace

