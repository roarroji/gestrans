﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Despachos

Namespace Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioEncabezadoSeguimientoVehiculos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEncabezadoSeguimientoVehiculos
        Inherits RepositorioBase(Of EncabezadoSeguimientoVehiculos)

        Public Overrides Function Consultar(filtro As EncabezadoSeguimientoVehiculos) As IEnumerable(Of EncabezadoSeguimientoVehiculos)
            Dim lista As New List(Of EncabezadoSeguimientoVehiculos)
            Dim item As EncabezadoSeguimientoVehiculos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.NumeroDocumento) Then
                    If filtro.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                    End If
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_seguimientos_abiertos]")

                While resultado.Read
                    item = New EncabezadoSeguimientoVehiculos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As EncabezadoSeguimientoVehiculos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As EncabezadoSeguimientoVehiculos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As EncabezadoSeguimientoVehiculos) As EncabezadoSeguimientoVehiculos
            Dim item As New EncabezadoSeguimientoVehiculos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.NumeroDocumento) Then
                    If filtro.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_seguimientos_abiertos]")

                While resultado.Read
                    item = New EncabezadoSeguimientoVehiculos(resultado)
                End While

            End Using

            Return item
        End Function
    End Class

End Namespace