﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.ControlViajes
Imports Softtools.GesCarga.Entidades.Despachos
Imports Softtools.GesCarga.Repositorio.ControlViajes
Imports System.Transactions

Namespace Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioEnturnamiento"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEnturnamiento
        Inherits RepositorioBase(Of Enturnamiento)

        Public Overrides Function Consultar(filtro As Enturnamiento) As IEnumerable(Of Enturnamiento)

            Dim lista As New List(Of Enturnamiento)
            Dim item As Enturnamiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()


                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not IsNothing(filtro.OficinaEnturna) Then
                    If filtro.OficinaEnturna.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.OficinaEnturna.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.CiudadEnturna) Then
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo", filtro.CiudadEnturna.Codigo)
                End If
                If Not IsNothing(filtro.Conductor) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", filtro.Conductor.Codigo)
                End If
                If Not IsNothing(filtro.Vehiculo) Then
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", filtro.Vehiculo.Codigo)
                End If
                If Not IsNothing(filtro.TipoVehiculo) Then
                    If filtro.TipoVehiculo.Codigo > 0 And filtro.TipoVehiculo.Codigo <> 2200 Then
                        conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", filtro.TipoVehiculo.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.EstadoTurno) Then
                    If filtro.EstadoTurno.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CATA_ESED_Codigo", filtro.EstadoTurno.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_Enturnamiento_Despachos")

                While resultado.Read
                    item = New Enturnamiento(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As Enturnamiento) As Long


            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)


                    conexion.AgregarParametroSQL("@par_Fecha", entidad.FechaDisponible, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Tenedor", entidad.Tenedor.Codigo)
                    conexion.AgregarParametroSQL("@par_Ubicacion", entidad.Ubicacion)
                    conexion.AgregarParametroSQL("@par_Latitud", entidad.Latitud, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Longitud", entidad.Longitud, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Enturna", entidad.CiudadEnturna.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo_Enturna", entidad.OficinaEnturna.Codigo)
                    conexion.AgregarParametroSQL("@par_REPA_Codigo_Destino1", entidad.RegionPaisDestino.Codigo)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino1", entidad.CiudadDestino1.Codigo)
                    If Not IsNothing(entidad.RegionPaisDestino2) Then
                        conexion.AgregarParametroSQL("@par_REPA_Codigo_Destino2", entidad.RegionPaisDestino2.Codigo)
                    End If
                    If Not IsNothing(entidad.CiudadDestino2) Then
                        conexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino2", entidad.CiudadDestino2.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_Fecha_Hora_Disponible", entidad.FechaDisponible, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_Enturnamiento_Despachos")

                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While
                    resultado.Close()
                    If entidad.Codigo > 0 Then
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ID", entidad.Codigo)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.OficinaEnturna.Codigo)
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_asignar_turno_enturnamiento_despachos")
                    End If
                End Using

                If entidad.Codigo > 0 Then
                    transaccion.Complete()
                End If

            End Using

            Return entidad.Codigo
        End Function

        Private Function ActualizarDespachoSolicitudServicio(entidad As Enturnamiento, contextoConexion As Conexion.DataBaseFactory) As Boolean

            Dim Actualizo As Boolean

            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumento)
            contextoConexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("gsp_actualizar_documento_solicitud_Servicios")

            While resultado.Read
                Actualizo = IIf(resultado.Item("Cantidad") > Cero, True, False)
            End While

            resultado.Close()

            Return Actualizo

        End Function

        Public Overrides Function Modificar(entidad As Enturnamiento) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    Dim resultado As IDataReader
                    conexion.CreateConnection()
                    If entidad.AsignarTurno > 0 Then

                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ID", entidad.ID)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.OficinaEnturna.Codigo)
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_asignar_turno_enturnamiento_despachos")
                    End If
                    If entidad.Desenturnar > 0 Then

                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ID", entidad.ID)
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_desenturnar_enturnamiento_despachos")
                    End If
                    If entidad.CerrarTurno > 0 Then

                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ID", entidad.ID)
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_cerrar_turno_enturnamiento_despachos")
                    End If
                    If entidad.CambioTurno > 0 Then
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ID", entidad.ID)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.OficinaEnturna.Codigo)
                        conexion.AgregarParametroSQL("@par_Turno", entidad.TurnoCambio)
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_cerrar_turno_enturnamiento_despachos")
                    End If
                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While
                    resultado.Close()
                End Using


                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If
                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If
            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As Enturnamiento) As Enturnamiento
            Dim item As New Enturnamiento

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Obtener_orden_cargue")

                While resultado.Read
                    item = New Enturnamiento(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As Enturnamiento) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                If entidad.Autorizacion > 0 Then
                    Dim Autorizacion = New Autorizaciones
                    Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                    Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18404}
                    Autorizacion.UsuarioSolicita = entidad.UsuarioAnula
                    Autorizacion.ID_Enturne = entidad.ID
                    Autorizacion.Vehiculo = entidad.Vehiculo
                    Autorizacion.Observaciones = entidad.CausaAnulacion
                    Dim repAutorizacion = New RepositorioAutorizaciones()
                    anulo = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)
                Else
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                    If Not IsNothing(entidad.Novedad) Then
                        conexion.AgregarParametroSQL("@par_CATA_NOED_Codigo", entidad.Novedad.Codigo)
                    End If
                    conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)
                    conexion.AgregarParametroSQL("@par_ID", entidad.ID)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_Enturnamiento_Despachos")

                    While resultado.Read
                        anulo = IIf((resultado.Item("Codigo")) > Cero, True, False)
                    End While
                End If


            End Using

            Return anulo
        End Function

    End Class
End Namespace