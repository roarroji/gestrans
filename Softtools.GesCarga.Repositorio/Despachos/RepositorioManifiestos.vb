﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Despachos
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports System.Transactions

Namespace Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioManifiesto"/>
    ''' </summary>
    Public Class RepositorioManifiesto
        Inherits RepositorioBase(Of Manifiesto)

        Public Overrides Function Consultar(filtro As Manifiesto) As IEnumerable(Of Manifiesto)
            Dim lista As New List(Of Manifiesto)
            Dim item As Manifiesto

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo_Planilla", filtro.Planilla.TipoDocumento)
                If filtro.NumeroDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Vehiculo) Then
                    If filtro.Vehiculo.Placa <> String.Empty Then
                        conexion.AgregarParametroSQL("@par_Placa", filtro.Vehiculo.Placa)
                    End If

                End If
                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo <> Cero Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Conductor) Then
                    If filtro.Conductor.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Conductor", filtro.Conductor.Codigo)
                    End If
                    If Not IsNothing(filtro.Conductor.Nombre) Then
                        conexion.AgregarParametroSQL("@par_Conductor", filtro.Conductor.Nombre)
                    End If
                End If

                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado.Codigo >= 0 And filtro.Estado.Codigo <> 2 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                        conexion.AgregarParametroSQL("@par_Anulado", 0)

                    ElseIf filtro.Estado.Codigo = 2 Then
                        conexion.AgregarParametroSQL("@par_Anulado", 1)
                    End If
                End If

                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Tipo_Manifiesto) Then
                    If filtro.Tipo_Manifiesto.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_CATA_TIMA_Codigo", filtro.Tipo_Manifiesto.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_manifiestos")
                While resultado.Read
                    item = New Manifiesto(resultado)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Manifiesto) As Long
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero_Planilla", entidad.NumeroPlanilla)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.Tipo_Documento.Codigo)
                    conexion.AgregarParametroSQL("@par_Numero_Documento_Planilla", entidad.NumeroDocumentoPlanilla)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.Usuario_Crea.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_CATA_TIMA_Codigo", entidad.Tipo_Manifiesto.Codigo)
                    conexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.NumeroSolicitudServicio)
                    conexion.AgregarParametroSQL("@par_Aceptacion_Electronica", entidad.AceptacionElectronica)

                    If Not IsNothing(entidad.CantidadViajesDia) Then
                        If entidad.CantidadViajesDia > 0 Then
                            conexion.AgregarParametroSQL("@par_CantidadViajesDia", entidad.CantidadViajesDia)
                        End If
                    End If
                    'Si el manifiesto es generado desde el despacho de la SS,
                    'entonces se envía como parámetro el detalle del despacho de la solicitud.
                    If entidad.IdDetalleSolicitudServicio > Cero Then
                        conexion.AgregarParametroSQL("@par_ID", entidad.IdDetalleSolicitudServicio)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_generar_manifiesto")
                    While resultado.Read
                        entidad.Numero = resultado.Item("Manifiesto").ToString()
                    End While
                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If
            End Using
            Return entidad.Numero
        End Function

        Public Overrides Function Modificar(entidad As Manifiesto) As Long
            If Not IsNothing(entidad.QR_Manifiesto) Then
                entidad.Numero = Generar_QR_Manifiesto(entidad)
            End If
            Return entidad.Numero
        End Function

        Public Overrides Function Obtener(filtro As Manifiesto) As Manifiesto
            Dim item As New Manifiesto

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_manifiesto")

                While resultado.Read
                    item = New Manifiesto(resultado)
                End While

                If item.Numero > 0 Then
                    item.ListaDetalleManifiesto = Obtener_Detalle(filtro)
                End If

            End Using
            Return item
        End Function

        Public Function Obtener_Detalle(filtro As Manifiesto) As IEnumerable(Of DetalleManifiesto)
            Dim lista As New List(Of DetalleManifiesto)
            Dim item As DetalleManifiesto

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENMC_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_Manifiesto")

                While resultado.Read
                    item = New DetalleManifiesto(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function

        Public Function Anular(entidad As Manifiesto) As Manifiesto
            Dim item As New Manifiesto
            Dim itemDocumento As EncabezadoDocumentos
            Dim lista As New List(Of EncabezadoDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_manifiesto_carga")

                While resultado.Read
                    itemDocumento = New EncabezadoDocumentos With {
                     .Cumplido = New Cumplido With {.Numero = Read(resultado, "ECPD_Numero_Documento")},
                     .Liquidacion = New Liquidacion With {.Numero = Read(resultado, "ELPD_Numero_Documento")}
                     }
                    lista.Add(itemDocumento)
                    item.Anulado = Read(resultado, "Anulo")

                End While
                item.DocumentosRelacionados = lista

            End Using

            Return item
        End Function

        Public Function Generar_CxP_Anticipo(entidad As EncabezadoDocumentoCuentas, conexion As DataBaseFactory) As Boolean
            Generar_CxP_Anticipo = False

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
            conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
            conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
            conexion.AgregarParametroSQL("@par_Codigo_Documento_Origen", entidad.CodigoDocumentoOrigen)
            conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.Numero)
            conexion.AgregarParametroSQL("@par_Fecha_Documento_Origen", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Fecha_Vence_Documento_Origen", DateAdd(DateInterval.Day, 30, entidad.Fecha), SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPuc.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Total", entidad.ValorTotal)
            conexion.AgregarParametroSQL("@par_Abono", entidad.Abono)
            conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo)
            conexion.AgregarParametroSQL("@par_Fecha_Cancelacion_Pago", entidad.FechaCancelacionPago, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Aprobado", entidad.Aprobado)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Aprobo", entidad.UsuarioAprobo.Codigo)
            conexion.AgregarParametroSQL("@par_Fecha_Aprobo", entidad.FechaAprobo, SqlDbType.DateTime)
            conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_documento_cuentas")

            While resultado.Read
                Generar_CxP_Anticipo = IIf(resultado.Item("Codigo") > 0, True, False)
            End While
            resultado.Close()

            Return Generar_CxP_Anticipo
        End Function
        Public Function Generar_QR_Manifiesto(entidad As Manifiesto) As Long

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@PAR_EMPR_CODIGO", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@PAR_NUMERO_MANIFIESTO", entidad.Numero)
                    conexion.AgregarParametroSQL("@PAR_QR_MANIFIESTO", entidad.QR_Manifiesto, SqlDbType.VarBinary)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("GSP_CREAR_QR_MANIFIESTO")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                End Using
                If entidad.Numero > Cero Then
                    transaccion.Complete()
                End If
            End Using

            Return entidad.Numero
        End Function

    End Class


End Namespace