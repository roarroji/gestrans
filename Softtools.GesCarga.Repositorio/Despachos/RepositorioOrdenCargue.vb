﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Despachos.OrdenCargue
Imports System.Transactions

Namespace Despachos.Masivo

    ''' <summary>
    ''' Clase <see cref="RepositorioOrdenCargue"/>
    ''' </summary>
    Public NotInheritable Class RepositorioOrdenCargue
        Inherits RepositorioBase(Of OrdenCargue)

        Public Overrides Function Consultar(filtro As OrdenCargue) As IEnumerable(Of OrdenCargue)

            Dim lista As New List(Of OrdenCargue)
            Dim item As OrdenCargue

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()


                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                If filtro.NumeroPlanilla > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero_Planilla", filtro.NumeroPlanilla)
                End If
                If filtro.NumeroManifiesto > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero_Manifiesto", filtro.NumeroManifiesto)
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If


                If Not IsNothing(filtro.Vehiculo) Then
                    If filtro.Vehiculo.Placa <> String.Empty Then
                        conexion.AgregarParametroSQL("@par_Placa", filtro.Vehiculo.Placa)
                    End If
                End If
                If Not IsNothing(filtro.TerceroCliente) Then
                    If Not IsNothing(filtro.TerceroCliente.Nombre) And filtro.TerceroCliente.Nombre <> String.Empty Then
                        conexion.AgregarParametroSQL("@par_Nombre_Cliente", filtro.TerceroCliente.Nombre)
                    End If
                End If

                If filtro.Estado >= 0 And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If

                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If Not IsNothing(filtro.UsuarioConsulta.Codigo) > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If


                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_orden_cargue")

                While resultado.Read
                    item = New OrdenCargue(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As OrdenCargue) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento.Codigo)

                    conexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.NumeroSolicitudServicio)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.TerceroCliente.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Remitente", entidad.TerceroRemitente.Codigo)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.TerceroConductor.Codigo)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Cargue", entidad.CiudadCargue.Codigo)
                    If entidad.FechaCargue > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Cargue", entidad.FechaCargue, SqlDbType.DateTime)
                    End If
                    If Not IsNothing(entidad.SitioCargue) Then
                        If entidad.SitioCargue.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.SitioCargue.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.DireccionCargue) Then
                        conexion.AgregarParametroSQL("@par_Direccion_Cargue", entidad.DireccionCargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Direccion_Cargue", VACIO)
                    End If

                    If Not IsNothing(entidad.TelefonosCargue) Then
                        conexion.AgregarParametroSQL("@par_Telefonos_Cargue", entidad.TelefonosCargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Telefonos_Cargue", VACIO)
                    End If

                    If Not IsNothing(entidad.ContactoCargue) Then
                        conexion.AgregarParametroSQL("@par_Contacto_Cargue", entidad.ContactoCargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Contacto_Cargue", VACIO)
                    End If

                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    Else
                        conexion.AgregarParametroSQL("@par_Observaciones", VACIO)
                    End If

                    conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                    conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Producto.Codigo)
                    conexion.AgregarParametroSQL("@par_Cantidad_Cliente", entidad.CantidadCliente, SqlDbType.Decimal)
                    conexion.AgregarParametroSQL("@par_Peso_Cliente", entidad.PesoCliente, SqlDbType.Decimal)
                    conexion.AgregarParametroSQL("@par_Valor_Anticipo", entidad.ValorAnticipo)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Descargue", entidad.CiudadDescargue.Codigo)
                    If entidad.FechaDescargue > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Descargue", entidad.FechaDescargue, SqlDbType.DateTime)
                    End If
                    If Not IsNothing(entidad.SitioDescargue) Then
                        If entidad.SitioDescargue.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.SitioDescargue.Codigo)
                        End If
                    End If

                    If Not IsNothing(entidad.DireccionDescargue) Then
                        conexion.AgregarParametroSQL("@par_Direccion_Descargue", entidad.DireccionDescargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Direccion_Descargue", VACIO)
                    End If

                    If Not IsNothing(entidad.TelefonoDescargue) Then
                        conexion.AgregarParametroSQL("@par_Telefono_Descargue", entidad.TelefonoDescargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Telefono_Descargue", VACIO)
                    End If

                    If Not IsNothing(entidad.ContactoDescargue) Then
                        conexion.AgregarParametroSQL("@par_Contacto_Descargue", entidad.ContactoDescargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Contacto_Descargue", VACIO)
                    End If


                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.CodigoUsuarioCrea.Codigo)

                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)

                    If Not IsNothing(entidad.Semirremolque) Then
                        If entidad.Semirremolque.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo)
                        End If
                    End If

                    conexion.AgregarParametroSQL("@par_ID_Solicitud", entidad.IDDetalleSolicitud)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_orden_cargue")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()
                    If entidad.Numero > 0 Then

                        If entidad.DesdeDespacharSolicitud Then
                            If Not ActualizarDespachoSolicitudServicio(entidad, conexion) Then
                                Return -1
                            End If
                        End If
                        If entidad.NumeroDocumento.Equals(Cero) Then
                            Return Cero
                        End If
                        If Not IsNothing(entidad.ListaPrecintos) Then
                            If entidad.ListaPrecintos.Count > 0 Then
                                For Each precinto In entidad.ListaPrecintos
                                    inserto = ModificarPrecintos(precinto, entidad.Numero, entidad.CodigoEmpresa, conexion)
                                Next
                            End If
                        End If
                    Else
                        transaccion.Dispose()
                        Return entidad.Numero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.NumeroDocumento = Cero
                End If

            End Using

            Return entidad.NumeroDocumento
        End Function


        Public Function ModificarPrecintos(entidad As DetallePrecintos, NumeroOrden As Integer, CodigoEmpresa As Integer, conexion As Conexion.DataBaseFactory) As Boolean
            Dim Actualizo As Boolean

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ID", entidad.IdPreci)
            conexion.AgregarParametroSQL("@par_ENOC_Numero", NumeroOrden)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_precintos_orden_cargue")

            While resultado.Read
                Actualizo = IIf(resultado.Item("Cantidad") > Cero, True, False)
            End While
            resultado.Close()
            Return Actualizo
        End Function

        Private Function ActualizarDespachoSolicitudServicio(entidad As OrdenCargue, contextoConexion As Conexion.DataBaseFactory) As Boolean

            Dim Actualizo As Boolean

            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ESOS_Codigo", entidad.NumeroSolicitudServicio)
            contextoConexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumento)
            contextoConexion.AgregarParametroSQL("@par_ID", entidad.IDDetalleSolicitud)
            contextoConexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("gsp_actualizar_documento_solicitud_Servicios")

            While resultado.Read
                Actualizo = IIf(resultado.Item("Cantidad") > Cero, True, False)
            End While

            resultado.Close()

            Return Actualizo

        End Function

        Public Overrides Function Modificar(entidad As OrdenCargue) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()


                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.NumeroSolicitudServicio)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.TerceroCliente.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Remitente", entidad.TerceroRemitente.Codigo)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.TerceroConductor.Codigo)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Cargue", entidad.CiudadCargue.Codigo)
                    If entidad.FechaCargue > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Cargue", entidad.FechaCargue, SqlDbType.DateTime)
                    End If
                    If Not IsNothing(entidad.SitioCargue) Then
                        If entidad.SitioCargue.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.SitioCargue.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.DireccionCargue) Then
                        conexion.AgregarParametroSQL("@par_Direccion_Cargue", entidad.DireccionCargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Direccion_Cargue", VACIO)
                    End If

                    If Not IsNothing(entidad.TelefonosCargue) Then
                        conexion.AgregarParametroSQL("@par_Telefonos_Cargue", entidad.TelefonosCargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Telefonos_Cargue", VACIO)
                    End If

                    If Not IsNothing(entidad.ContactoCargue) Then
                        conexion.AgregarParametroSQL("@par_Contacto_Cargue", entidad.ContactoCargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Contacto_Cargue", VACIO)
                    End If
                    If Not IsNothing(entidad.Semirremolque) Then
                        If entidad.Semirremolque.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    Else
                        conexion.AgregarParametroSQL("@par_Observaciones", VACIO)
                    End If


                    conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                    conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Producto.Codigo)
                    conexion.AgregarParametroSQL("@par_Cantidad_Cliente", entidad.CantidadCliente)
                    conexion.AgregarParametroSQL("@par_Peso_Cliente", entidad.PesoCliente, SqlDbType.Decimal)
                    conexion.AgregarParametroSQL("@par_Valor_Anticipo", entidad.ValorAnticipo)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Descargue", entidad.CiudadDescargue.Codigo)
                    If entidad.FechaDescargue > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Descargue", entidad.FechaDescargue, SqlDbType.DateTime)
                    End If
                    If Not IsNothing(entidad.SitioDescargue) Then
                        If entidad.SitioDescargue.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.SitioDescargue.Codigo)
                        End If
                    End If

                    If Not IsNothing(entidad.DireccionDescargue) Then
                        conexion.AgregarParametroSQL("@par_Direccion_Descargue", entidad.DireccionDescargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Direccion_Descargue", VACIO)
                    End If

                    If Not IsNothing(entidad.TelefonoDescargue) Then
                        conexion.AgregarParametroSQL("@par_Telefono_Descargue", entidad.TelefonoDescargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Telefono_Descargue", VACIO)
                    End If

                    If Not IsNothing(entidad.ContactoDescargue) Then
                        conexion.AgregarParametroSQL("@par_Contacto_Descargue", entidad.ContactoDescargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Contacto_Descargue", VACIO)
                    End If


                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.CodigoUsuarioModifica.Codigo)

                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Modificar_orden_cargue")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                    End While
                    resultado.Close()
                End Using


                If entidad.NumeroDocumento.Equals(Cero) Then
                    Return Cero
                End If
                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.NumeroDocumento = Cero
                End If
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As OrdenCargue) As OrdenCargue
            Dim item As New OrdenCargue

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Obtener_orden_cargue")

                While resultado.Read
                    item = New OrdenCargue(resultado)
                End While

            End Using

            item.ListaPrecintos = ObtenerPrecintos(filtro)

            Return item
        End Function

        Public Function ObtenerPrecintos(entidad As OrdenCargue) As IEnumerable(Of DetallePrecintos)
            Dim item As New DetallePrecintos
            Dim lista As New List(Of DetallePrecintos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENOC_Codigo", entidad.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_precintos_orden_cargue")

                While resultado.Read
                    item = New DetallePrecintos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Function Anular(entidad As OrdenCargue) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.CodigoUsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnula)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_CATALOGO", entidad.ConceptoAnulacion)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Anular_orden_cargue")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero_Documento")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function ActualizarCargueDescargue(entidad As OrdenCargue) As Long
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    'Cargue
                    If entidad.FechaCargue > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Cargue", entidad.FechaCargue, SqlDbType.DateTime)
                    End If
                    If Not IsNothing(entidad.SitioCargue) Then
                        If entidad.SitioCargue.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.SitioCargue.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.DireccionCargue) Then
                        conexion.AgregarParametroSQL("@par_Direccion_Cargue", entidad.DireccionCargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Direccion_Cargue", VACIO)
                    End If

                    If Not IsNothing(entidad.TelefonosCargue) Then
                        conexion.AgregarParametroSQL("@par_Telefonos_Cargue", entidad.TelefonosCargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Telefonos_Cargue", VACIO)
                    End If

                    If Not IsNothing(entidad.ContactoCargue) Then
                        conexion.AgregarParametroSQL("@par_Contacto_Cargue", entidad.ContactoCargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Contacto_Cargue", VACIO)
                    End If
                    'Descargue
                    If entidad.FechaDescargue > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Descargue", entidad.FechaDescargue, SqlDbType.DateTime)
                    End If
                    If Not IsNothing(entidad.SitioDescargue) Then
                        If entidad.SitioDescargue.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.SitioDescargue.Codigo)
                        End If
                    End If

                    If Not IsNothing(entidad.DireccionDescargue) Then
                        conexion.AgregarParametroSQL("@par_Direccion_Descargue", entidad.DireccionDescargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Direccion_Descargue", VACIO)
                    End If

                    If Not IsNothing(entidad.TelefonoDescargue) Then
                        conexion.AgregarParametroSQL("@par_Telefono_Descargue", entidad.TelefonoDescargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Telefono_Descargue", VACIO)
                    End If

                    If Not IsNothing(entidad.ContactoDescargue) Then
                        conexion.AgregarParametroSQL("@par_Contacto_Descargue", entidad.ContactoDescargue)
                    Else
                        conexion.AgregarParametroSQL("@par_Contacto_Descargue", VACIO)
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.CodigoUsuarioModifica.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_cargue_descargue_orden_cargue")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                    End While
                    resultado.Close()
                End Using
                If entidad.NumeroDocumento.Equals(Cero) Then
                    entidad.NumeroDocumento = Cero
                    transaccion.Dispose()
                Else
                    transaccion.Complete()
                End If
            End Using
            Return entidad.NumeroDocumento
        End Function
    End Class
End Namespace