﻿Imports System.Net
Imports System.Text
Imports System.Drawing
Imports System.Web.Script.Serialization
Imports System.IO

Imports QRCodeEncoderLibrary
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Entidades.Basico.Facturacion
Imports QRCodeSharedLibrary

Namespace Facturacion.FacturaElectronicaSaphety
    Public Class clsFacturaElectronicaSaphety
#Region "Constantes"
        Dim URL_ASP As String

        Dim REPLACE_VIRTUAL_OPERATOR As String = "{virtualOperator}"
        Dim REPLACE_CUFE As String = "{CUFE}"
        Dim GEN_URL_SAPHETY As String = "https://api-factura-electronica-co-qa.saphety.com/"
        Dim GEN_URL_SAPHETY_PRODUCCION As String = "https://api-factura-electronica-co.saphety.com/"
        Dim API_GET_TOKEN As String = "v2/auth/gettoken"
        Dim API_GET_CUFE As String = "v2/" & REPLACE_VIRTUAL_OPERATOR & "/outbounddocuments/cufe"
        Dim API_POST_HABILITA_FACTURA As String = "v2/" & REPLACE_VIRTUAL_OPERATOR & "/qualificationoutbounddocuments/salesInvoiceAsync"
        Dim API_POST_EMISION_FACTURA As String = "v2/" & REPLACE_VIRTUAL_OPERATOR & "/outbounddocuments/salesInvoice"
        Dim API_GET_CUDE As String = "v2/" & REPLACE_VIRTUAL_OPERATOR & "/outbounddocuments/cude"
        Dim API_POST_HABILITA_NOTA_CREDITO As String = "v2/" & REPLACE_VIRTUAL_OPERATOR & "/qualificationoutbounddocuments/creditNoteAsync"
        Dim API_POST_EMISION_NOTA_CREDITO As String = "v2/" & REPLACE_VIRTUAL_OPERATOR & "/outbounddocuments/creditNote"
        Dim API_POST_HABILITA_NOTA_DEBITO As String = "v2/" & REPLACE_VIRTUAL_OPERATOR & "/qualificationoutbounddocuments/debitNoteAsync"
        Dim API_POST_EMISION_NOTA_DEBITO As String = "v2/" & REPLACE_VIRTUAL_OPERATOR & "/outbounddocuments/debitNote"
        Dim TIPO_REPORTE_FACTURA_GENERA As String = "Electronic"

        Dim AMBIENTE_HABILITACION_DIAN_FACTURA_ELECTRONICA As String = "https://catalogo-vpfe-hab.dian.gov.co/document/searchqr?documentkey=" & REPLACE_CUFE
        Dim AMBIENTE_PRODUCCION_DIAN_FACTURA_ELECTRONICA As String = "https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey=" & REPLACE_CUFE

        Dim NOMBRE_REPORTE_FACT As String = "RepFact"
        Dim NOMBRE_REPORTE_NOTA_CREDITO As String = "RepNotCre"
        Dim NOMBRE_IMPUESTO_IVA As String = "IVA"
        'Dim NOMBRE_RETENCION_FUENTE As String = "RETEFUENTE"
        Dim NOMBRE_RETENCION_FUENTE As String = "RETERENTA" ' Anexo Tecnico 1.8 18-AGO-2021 Cambia el nombre del impuesto
        Dim NOMBRE_RETENCION_ICA As String = "RETEICA"

#End Region

#Region "Variables"
        Public Token As TokenSaphety
        Public CUFE As String = ""
        Public CUDE As String = ""
        Public subtotal As Double = 0
        Public descuento As Double = 0

        'QRCODE
        Private Encoder As QRCodeEncoder
        Private QRCodeImage As Bitmap
        Public QRbytes As Byte()
#End Region

#Region "Declaracion Enums"
        Public Enum TIPO_DOCUMENTO_FACTURA_ELECTRONICA
            FACTURA = 170
            NOTA_CREDITO = 190
            NOTA_DEBITO = 195
        End Enum
        Public Enum PROVEEDOR_FACTURA_ELECTRONICA
            SAPHETY = 21101
        End Enum
        Public Enum AMBIENTE_FACTURA_ELECTRONICA
            PRUEBAS = 20701
            HABILITACION = 20702
            PRODUCCION = 20703
        End Enum
        Public Enum CODGIO_IMPUESTO
            IVA = 31
            RETE_FUENTE = 30
            RETE_ICA = 32
        End Enum
#End Region

#Region "Constructor"
        Sub New(Empresa As Empresas, urlasp As String)
            URL_ASP = urlasp
            'Genera El token de acceso            
            Dim resultToken As JObject = GET_TOKEN(Empresa)
            Token = New TokenSaphety()
            If Convert.ToBoolean(resultToken("IsValid")) = True Then
                Token.IsValid = True
                Token.access_token = resultToken("ResultData")("access_token")
                Token.token_type = resultToken("ResultData")("token_type")
                Token.expires = resultToken("ResultData")("expires")
            End If
        End Sub
#End Region

#Region "Funciones"
        Public Function GenerarEnvioFacturaElectronica(Empresa As Empresas, NumeroFactura As Integer) As Boolean
            Dim Factura As Facturas = New Facturas()
            Dim GuardaFactura As Facturas = New Facturas()
            Dim RespuestaGuarda As Long = 0

            Dim RespEnvioPruebas As JObject = Nothing
            Dim RespEnvioHabilitacion As JObject = Nothing
            Dim RespEnvioProduccion As JObject = Nothing
            Dim objArchivo As JObject = Nothing
            Dim TextErrorField As String = ""
            Dim CodeError As String = ""
            Dim DescriptionError As String = ""
            Dim ProcesoExito As Boolean = True
            Dim ArchivoRespuesta As FacturaElectronica = New FacturaElectronica()

            Factura.CodigoEmpresa = Empresa.Codigo
            Factura.Numero = NumeroFactura
            Factura = New RepositorioFacturas().ObtenerDatosFacturaElectronicaSaphety(Factura)

            If Token.IsValid Then
                'Subtotales y Descuentos
                subtotal = 0
                descuento = 0
                If Not IsNothing(Factura.Remesas) Then
                    For i = 0 To Factura.Remesas.Count - 1
                        subtotal += Factura.Remesas(i).Remesas.TotalFleteCliente
                    Next
                End If
                If Not IsNothing(Factura.OtrosConceptos) Then
                    For i = 0 To Factura.OtrosConceptos.Count - 1
                        If Factura.OtrosConceptos(i).ConceptosVentas.Operacion <> 2 Then
                            subtotal += Factura.OtrosConceptos(i).Valor_Concepto
                        Else
                            descuento += Factura.OtrosConceptos(i).Valor_Concepto
                        End If
                    Next
                End If

                'Creacion de Cufe
                Dim resultCufe As JObject = GET_CUFE(Empresa, Factura)
                If Convert.ToBoolean(resultCufe("IsValid")) = True Then
                    CUFE = resultCufe("ResultData")
                End If

                'Generacion Codigo QR
                Dim UrlQRCodeAmbiente As String
                If Empresa.EmpresaFacturaElectronica.Ambiente.Codigo = AMBIENTE_FACTURA_ELECTRONICA.PRUEBAS Or
                    Empresa.EmpresaFacturaElectronica.Ambiente.Codigo = AMBIENTE_FACTURA_ELECTRONICA.HABILITACION Then
                    UrlQRCodeAmbiente = AMBIENTE_HABILITACION_DIAN_FACTURA_ELECTRONICA.Replace(REPLACE_CUFE, CUFE)
                Else
                    UrlQRCodeAmbiente = AMBIENTE_PRODUCCION_DIAN_FACTURA_ELECTRONICA.Replace(REPLACE_CUFE, CUFE)
                End If
                Dim FechaCreacion As Date = Convert.ToDateTime(Factura.Fecha)

                Dim strtxtQr As String = "NumFac: " & (Factura.Prefijo & Factura.NumeroDocumento).ToString() & vbNewLine
                strtxtQr += "FecFac: " & String.Format("{0:yyyy-MM-dd}", FechaCreacion) & vbNewLine
                strtxtQr += "HorFac: " & "00:00:00" & "-05:00" & vbNewLine
                strtxtQr += "NitFac: " & Factura.Empresas.NumeroIdentificacion & vbNewLine
                strtxtQr += "DocAdq: " & IIf(Factura.FacturarA.Codigo > 0, Factura.FacturarA.NumeroIdentificacion, Factura.Cliente.NumeroIdentificacion) & vbNewLine
                strtxtQr += "ValFac: " & (subtotal - Factura.ValorTotalIva).ToString() & vbNewLine
                strtxtQr += "ValIva: " & Factura.ValorTotalIva.ToString() & vbNewLine
                strtxtQr += "ValOtroIm: 0" & vbNewLine
                strtxtQr += "ValTolFac: " & (subtotal + Factura.ValorTotalIva - descuento).ToString() & vbNewLine
                strtxtQr += "CUFE:" & CUFE & vbNewLine
                strtxtQr += "QRCode :" & UrlQRCodeAmbiente
                QRbytes = ImageToByte(EncodeQRIMG(strtxtQr))

                'Guarda Factura para reporte
                GuardaFactura.CodigoEmpresa = Factura.CodigoEmpresa
                GuardaFactura.Numero = Factura.Numero
                GuardaFactura.NumeroDocumento = Factura.NumeroDocumento
                GuardaFactura.FacturaElectronica = New FacturaElectronica With {.CUFE = CUFE, .FechaEnvio = Factura.FechaCreacion, .QRcode = QRbytes}
                RespuestaGuarda = New RepositorioFacturas().GuardarFacturaElectronica(GuardaFactura)

                If RespuestaGuarda > 0 Then
                    'Obtiene PDF Generado
                    Dim geturl As String = "Empresa=" & Factura.CodigoEmpresa & "&"
                    geturl += "NombRepo=" & NOMBRE_REPORTE_FACT & "&"
                    geturl += "Numero=" & Factura.Numero & "&"
                    geturl += "OpcionExportarPdf=1&"
                    geturl += "Prefijo=" & Empresa.Prefijo & "&"
                    geturl += "TipoExportar=" & TIPO_REPORTE_FACTURA_GENERA & "&"
                    geturl += "TipoFactura=" & Factura.CataTipoFactura
                    Dim urlReporte As String = URL_ASP & "Reportes/Reportes.aspx?"
                    Dim result = RequestGet(urlReporte & geturl)
                End If

                Dim DocumentoFactura As Facturas = New Facturas With {.CodigoEmpresa = Factura.CodigoEmpresa, .Numero = Factura.Numero}
                DocumentoFactura = New RepositorioFacturas().ObtenerDocumentoReporteSaphety(DocumentoFactura)
                If DocumentoFactura.Numero > 0 Then
                    Factura.FacturaElectronica.Archivo = DocumentoFactura.FacturaElectronica.Archivo
                End If
                '--Genera Obj Json
                Dim JsonFactura As DocumentoFacturaJson = GenerarJsonFactura(Empresa, Factura)
                '--Genera Obj Json
                '--Envio de factura electronica
                If Not IsNothing(JsonFactura) Then
                    Select Case Empresa.EmpresaFacturaElectronica.Ambiente.Codigo
                        Case AMBIENTE_FACTURA_ELECTRONICA.PRUEBAS
                            RespEnvioPruebas = Emision_Factura_Saphety(Empresa, JsonFactura)
                            If Convert.ToBoolean(RespEnvioPruebas("IsValid")) = False Then
                                ProcesoExito = False
                                TextErrorField = If(RespEnvioPruebas("Errors")(0)("Field") <> "null", RespEnvioPruebas("Errors")(0)("Field"), "")
                                CodeError = If(RespEnvioPruebas("Errors")(0)("Code") <> "null", RespEnvioPruebas("Errors")(0)("Code"), "")
                                DescriptionError = If(RespEnvioPruebas("Errors")(0)("Description") <> "", RespEnvioPruebas("Errors")(0)("Description"), "")
                            Else
                                ArchivoRespuesta.Archivo = RespEnvioPruebas("ResultData")("Content")
                                ArchivoRespuesta.TipoArchivo = RespEnvioPruebas("ResultData")("ContentType")
                                ArchivoRespuesta.DecripcionArchivo = "UBL Documento"
                            End If
                        Case AMBIENTE_FACTURA_ELECTRONICA.HABILITACION
                            RespEnvioHabilitacion = Emision_Factura_Saphety(Empresa, JsonFactura)
                            If Convert.ToBoolean(RespEnvioHabilitacion("IsValid")) = False Then
                                ProcesoExito = False
                                TextErrorField = If(RespEnvioHabilitacion("Errors")(0)("Field") <> "null", RespEnvioHabilitacion("Errors")(0)("Field"), "")
                                CodeError = If(RespEnvioHabilitacion("Errors")(0)("Code") <> "null", RespEnvioHabilitacion("Errors")(0)("Code"), "")
                                DescriptionError = If(RespEnvioHabilitacion("Errors")(0)("Description") <> "", RespEnvioHabilitacion("Errors")(0)("Description"), "")
                            Else
                                ArchivoRespuesta.Archivo = RespEnvioHabilitacion("ResultData")("Content")
                                ArchivoRespuesta.TipoArchivo = RespEnvioHabilitacion("ResultData")("ContentType")
                                ArchivoRespuesta.DecripcionArchivo = "UBL Documento"
                            End If
                        Case AMBIENTE_FACTURA_ELECTRONICA.PRODUCCION
                            RespEnvioProduccion = Emision_Factura_Saphety(Empresa, JsonFactura)
                            If Convert.ToBoolean(RespEnvioProduccion("IsValid")) = False Then
                                ProcesoExito = False
                                TextErrorField = If(RespEnvioProduccion("Errors")(0)("Field") <> "null", RespEnvioProduccion("Errors")(0)("Field"), "")
                                CodeError = If(RespEnvioProduccion("Errors")(0)("Code") <> "null", RespEnvioProduccion("Errors")(0)("Code"), "")
                                DescriptionError = If(RespEnvioProduccion("Errors")(0)("Description") <> "", RespEnvioProduccion("Errors")(0)("Description"), "")
                            Else
                                ArchivoRespuesta.Archivo = RespEnvioProduccion("ResultData")("Content")
                                ArchivoRespuesta.TipoArchivo = RespEnvioProduccion("ResultData")("ContentType")
                                ArchivoRespuesta.DecripcionArchivo = "UBL Documento"
                            End If
                    End Select
                End If
                '--Envio de factura electronica
                '--Guarda Informacion Final de factura enviada
                GuardaFactura.CodigoEmpresa = Factura.CodigoEmpresa
                GuardaFactura.Numero = Factura.Numero
                GuardaFactura.NumeroDocumento = Factura.NumeroDocumento
                GuardaFactura.EstadoFAEL = If(ProcesoExito = True, 1, 0)
                If ProcesoExito = True Then
                    ArchivoRespuesta.ID_Pruebas = If(Not IsNothing(RespEnvioPruebas), RespEnvioPruebas("ResultData")("Id"), "")
                    ArchivoRespuesta.ID_Habilitacion = If(Not IsNothing(RespEnvioHabilitacion), RespEnvioHabilitacion("ResultData")("Id"), "")
                    ArchivoRespuesta.ID_Emision = If(Not IsNothing(RespEnvioProduccion), RespEnvioProduccion("ResultData")("Id"), "")
                End If
                ArchivoRespuesta.CodigoError = CodeError
                ArchivoRespuesta.MensajeError = TextErrorField & " " & CodeError & " " & DescriptionError
                GuardaFactura.FacturaElectronica = ArchivoRespuesta

            Else
                ProcesoExito = False
                GuardaFactura.CodigoEmpresa = Factura.CodigoEmpresa
                GuardaFactura.Numero = Factura.Numero
                GuardaFactura.NumeroDocumento = Factura.NumeroDocumento
                GuardaFactura.EstadoFAEL = 0
                GuardaFactura.FacturaElectronica = New FacturaElectronica With {
                    .CodigoError = "0",
                    .MensajeError = "No se pudo Generar el Token de acceso a Saphety"
                }
            End If
            RespuestaGuarda = New RepositorioFacturas().GuardarFacturaElectronica(GuardaFactura)
            '--Guarda Informacion Final de factura enviada
            Return ProcesoExito
        End Function

        Public Function GET_TOKEN(Empresa As Empresas) As JObject
            Dim AccesoToken = New Dictionary(Of String, Object)
            AccesoToken.Add("username", Empresa.EmpresaFacturaElectronica.Usuario)
            AccesoToken.Add("password", Empresa.EmpresaFacturaElectronica.Clave)
            AccesoToken.Add("virtual_operator", Empresa.EmpresaFacturaElectronica.OperadorVirtual)
            Dim urlToken As String = ObtenerUrlApiSaphety(Empresa.EmpresaFacturaElectronica.Ambiente.Codigo) & API_GET_TOKEN
            Dim objResult As JObject = RequestPost(urlToken, AccesoToken)
            Return objResult
        End Function

        Public Function GET_CUFE(Empresa As Empresas, Factura As Facturas) As JObject
            Dim CUFE = New Dictionary(Of String, Object)
            CUFE.Add("IssueDate", Factura.FechaCreacion)
            CUFE.Add("CustomerDocumentNumber", IIf(Factura.FacturarA.Codigo > 0, Factura.FacturarA.NumeroIdentificacion, Factura.Cliente.NumeroIdentificacion))
            CUFE.Add("IssuerNitNumber", Factura.Empresas.NumeroIdentificacion)
            CUFE.Add("SerieTechnicalKey", Factura.OficinaFactura.ClaveTecnicaFactura)
            CUFE.Add("DocumentNumber", Factura.Prefijo & Factura.NumeroDocumento)
            CUFE.Add("TotalIva", Factura.ValorTotalIva)
            CUFE.Add("TotalInc", "0")
            CUFE.Add("TotalIca", "0")
            CUFE.Add("TotalGrossAmount", subtotal)
            CUFE.Add("TotalPayableAmount", (subtotal + Factura.ValorTotalIva - descuento))
            CUFE.Add("SupplierTimeZoneCode", "America/Bogota")

            Dim urlCufe As String = ObtenerUrlApiSaphety(Empresa.EmpresaFacturaElectronica.Ambiente.Codigo) & API_GET_CUFE.Replace(REPLACE_VIRTUAL_OPERATOR, Empresa.EmpresaFacturaElectronica.OperadorVirtual)
            Dim objResult As JObject = RequestPost(urlCufe, CUFE, Token)
            Return objResult
        End Function

        Public Function GenerarJsonFactura(Empresa As Empresas, Factura As Facturas) As DocumentoFacturaJson
            Dim jsonFactura As DocumentoFacturaJson = New DocumentoFacturaJson()
            Dim indLinea As Integer = 1
            'Valida Destinatario
            Dim Destinatario As Terceros
            If Factura.FacturarA.Codigo > 0 Then
                Destinatario = Factura.FacturarA
            Else
                Destinatario = Factura.Cliente
            End If
            'Valida Destinatario
            '----------Totales de retención de impuestos
            'obtener Ids Impuestos
            Dim listadoIdImpuestos As List(Of Integer) = New List(Of Integer)
            For i = 0 To Factura.DetalleImpuestosFacturas.Count - 1
                listadoIdImpuestos.Add(Factura.DetalleImpuestosFacturas(i).ImpuestoFacturas.Codigo)
            Next
            ' Agrupa Impuestos
            Dim Tmpimpuestos As List(Of Integer) = New List(Of Integer)
            For i = 0 To listadoIdImpuestos.Count - 1
                If Tmpimpuestos.Contains(listadoIdImpuestos(i)) = False Then
                    Tmpimpuestos.Add(listadoIdImpuestos(i))
                End If
            Next
            'Genera Listado Objeto Concepto
            Dim ListadoObjImpuesto As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))
            For i = 0 To Tmpimpuestos.Count - 1
                Dim nombreImpuesto As String = ""
                Select Case Tmpimpuestos(i)
                    Case CODGIO_IMPUESTO.IVA
                        nombreImpuesto = NOMBRE_IMPUESTO_IVA
                    Case CODGIO_IMPUESTO.RETE_FUENTE
                        nombreImpuesto = NOMBRE_RETENCION_FUENTE
                    Case CODGIO_IMPUESTO.RETE_ICA
                        nombreImpuesto = NOMBRE_RETENCION_ICA
                End Select
                Dim impuesto = New Dictionary(Of String, Object)
                impuesto.Add("Codigo", Tmpimpuestos(i))
                impuesto.Add("Nombre", nombreImpuesto)
                impuesto.Add("Valor_impuesto", 0)
                impuesto.Add("Valor_base", 0)
                impuesto.Add("Valor_tarifa", 0)
                ListadoObjImpuesto.Add(impuesto)
            Next
            'Suma Valores impuestos
            For i = 0 To ListadoObjImpuesto.Count - 1
                For j = 0 To Factura.DetalleImpuestosFacturas.Count - 1
                    If ListadoObjImpuesto(i)("Codigo") = Factura.DetalleImpuestosFacturas(j).ImpuestoFacturas.Codigo Then
                        ListadoObjImpuesto(i)("Valor_impuesto") += Factura.DetalleImpuestosFacturas(j).Valor_impuesto
                        ListadoObjImpuesto(i)("Valor_base") += Factura.DetalleImpuestosFacturas(j).Valor_base
                        ListadoObjImpuesto(i)("Valor_tarifa") = (FormatNumber(CDbl(Factura.DetalleImpuestosFacturas(j).Valor_tarifa * 100), 3)).ToString().Replace(",", ".")
                    End If
                Next
            Next

            For i = 0 To ListadoObjImpuesto.Count - 1
                Dim impuesto As TaxSubTotal
                Dim retencion As WithholdingTaxSubTotal
                Select Case ListadoObjImpuesto(i)("Codigo")
                    Case CODGIO_IMPUESTO.IVA
                        impuesto = New TaxSubTotal With {
                            .TaxCategory = ListadoObjImpuesto(i)("Nombre"),
                            .TaxPercentage = ListadoObjImpuesto(i)("Valor_tarifa"),
                            .TaxableAmount = ListadoObjImpuesto(i)("Valor_base"),
                            .TaxAmount = ListadoObjImpuesto(i)("Valor_impuesto")
                        }
                        jsonFactura.TaxSubTotals.Add(impuesto)
                        jsonFactura.TaxTotals.Add(New TaxTotal With {.TaxCategory = ListadoObjImpuesto(i)("Nombre"), .TaxAmount = ListadoObjImpuesto(i)("Valor_impuesto")})
                    Case CODGIO_IMPUESTO.RETE_FUENTE, CODGIO_IMPUESTO.RETE_ICA
                        retencion = New WithholdingTaxSubTotal With {
                           .WithholdingTaxCategory = ListadoObjImpuesto(i)("Nombre"),
                           .TaxPercentage = ListadoObjImpuesto(i)("Valor_tarifa"),
                           .TaxableAmount = ListadoObjImpuesto(i)("Valor_base"),
                           .TaxAmount = ListadoObjImpuesto(i)("Valor_impuesto")
                       }
                        jsonFactura.WithholdingTaxSubTotals.Add(retencion)
                        jsonFactura.WithholdingTaxTotals.Add(New WithholdingTaxTotal With {.WithholdingTaxCategory = ListadoObjImpuesto(i)("Nombre"), .TaxAmount = ListadoObjImpuesto(i)("Valor_impuesto")})
                End Select
            Next

            If jsonFactura.TaxSubTotals.Count = 0 Then
                jsonFactura.TaxSubTotals.Add(New TaxSubTotal With {
                    .TaxCategory = NOMBRE_IMPUESTO_IVA,
                    .TaxPercentage = "0",
                    .TaxableAmount = "0",
                    .TaxAmount = "0"
                })
            End If

            If jsonFactura.TaxTotals.Count = 0 Then
                jsonFactura.TaxTotals.Add(New TaxTotal With {.TaxCategory = NOMBRE_IMPUESTO_IVA, .TaxAmount = "0"})
            End If
            '----------Totales de retención de impuestos
            jsonFactura.DeliveryDate = Factura.FechaCreacion
            Dim tmpFechaVence = DateTimeOffset.Parse(Factura.FechaVence)
            Dim FechaVence As Date = tmpFechaVence.DateTime
            jsonFactura.PaymentMeans.Add(New PaymentMean With {
                .Code = "1",'--No se especifica un medio de pago
                .Mean = Factura.FormaPago.Codigo.ToString(),
                .DueDate = String.Format("{0:yyyy-MM-dd}", FechaVence)
            })
            '--Informacion de emisor
            jsonFactura.IssuerParty = New IssuerParty()
            jsonFactura.IssuerParty.DocumentContacts.Add(New DocumentContact With {
                .Name = Factura.Empresas.RazonSocial,
                .Telephone = Factura.Empresas.Telefono,
                .Email = Factura.Empresas.Email,
                .Type = "SellerContact"
            })
            'jsonFactura.IssuerParty.DocumentContacts.Add(New DocumentContact With {
            '    .Name = "SOFTTOOLS SAS",
            '    .Telephone = (7446586).ToString(),
            '    .Email = "gerencia@softtools.co",
            '    .Type = "SellerContact"
            '})
            jsonFactura.IssuerParty.Identification = New Identification With {
                .DocumentNumber = Factura.Empresas.NumeroIdentificacion,
                .DocumentType = Factura.Empresas.TipoNumeroIdentificacion,
                .CountryCode = "CO",
                .CheckDigit = Factura.Empresas.DigitoChequeo.ToString()
            }
            'jsonFactura.IssuerParty.Identification = New Identification With {
            '    .DocumentNumber = "830031653",
            '    .DocumentType = Factura.Empresas.TipoNumeroIdentificacion,
            '    .CountryCode = "CO",
            '    .CheckDigit = "3"
            '}
            '--Informacion de emisor
            '--Informacion de destinatario
            jsonFactura.CustomerParty = New CustomerParty()
            jsonFactura.CustomerParty.DocumentContacts.Add(New DocumentContact With {
                .Name = Destinatario.NombreCompleto,
                .Telephone = Destinatario.Telefonos,
                .Email = Destinatario.Correo,
                .Type = "BuyerContact"
            })
            jsonFactura.CustomerParty.LegalType = Destinatario.TipoNaturaleza.Nombre
            jsonFactura.CustomerParty.Identification = New Identification With {
                .DocumentNumber = Destinatario.NumeroIdentificacion,
                .DocumentType = Destinatario.TipoIdentificacion.Nombre,
                .CountryCode = "CO"
            }
            If Destinatario.TipoIdentificacion.Nombre.ToUpper() = "NIT" Then
                jsonFactura.CustomerParty.Identification.CheckDigit = Destinatario.DigitoChequeo.ToString()
            End If
            jsonFactura.CustomerParty.Name = Destinatario.NombreCompleto
            jsonFactura.CustomerParty.Email = Destinatario.Correo
            jsonFactura.CustomerParty.Address = New Address With {
                .DepartmentCode = Destinatario.Departamentos.CodigoAlterno.ToString(),
                .CityCode = Destinatario.Ciudad.CodigoAlterno.ToString(),
                .AddressLine = Destinatario.Direccion,
                .PostalCode = Destinatario.Ciudad.CodigoPostal.ToString(),
                .Country = "CO"
            }
            'jsonFactura.CustomerParty.TaxScheme = "49"
            jsonFactura.CustomerParty.TaxScheme = "ZZ" ' Anexo Tecnico 1.8 18-AGO-2021
            jsonFactura.CustomerParty.Person = New Person With {
                .FirstName = Destinatario.Nombre.ToString(),
                .MiddleName = "",
                .FamilyName = Destinatario.PrimeroApellido + " " + Destinatario.SegundoApellido
            }
            'jsonFactura.CustomerParty.ResponsabilityTypes = New List(Of String) From {"O-06", "O-07", "O-09", "O-14", "O-48", "O-99", "A-29"}
            jsonFactura.CustomerParty.ResponsabilityTypes = New List(Of String) From {"R-99-PN"}
            '--Informacion de destinatario
            jsonFactura.Currency = "COP"
            '-----Lineas
            '--Remesas
            Dim Lineas = New List(Of Line)
            If Not IsNothing(Factura.Remesas) Then
                For i = 0 To Factura.Remesas.Count - 1
                    Dim detalle = New Line()
                    detalle.Number = indLinea.ToString()
                    detalle.Quantity = "1"
                    detalle.QuantityUnitOfMeasure = "NAR" '--Unidad de medida estandar
                    detalle.TaxSubTotals.Add(New TaxSubTotal With {
                        .TaxCategory = NOMBRE_IMPUESTO_IVA,
                        .TaxPercentage = "0",
                        .TaxableAmount = "0",
                        .TaxAmount = "0"
                    })
                    detalle.TaxTotals.Add(New TaxTotal With {.TaxCategory = NOMBRE_IMPUESTO_IVA, .TaxAmount = "0"})
                    detalle.UnitPrice = Factura.Remesas(i).Remesas.TotalFleteCliente.ToString()
                    detalle.GrossAmount = Factura.Remesas(i).Remesas.TotalFleteCliente.ToString()
                    detalle.NetAmount = Factura.Remesas(i).Remesas.TotalFleteCliente.ToString()
                    detalle.Item = New Item With {.Description = (If(Factura.Remesas(i).Remesas.MBLContenedor = "", "MBL", Factura.Remesas(i).Remesas.MBLContenedor)) & " - " & Factura.Remesas(i).Remesas.Ruta.Nombre}
                    Lineas.Add(detalle)
                    indLinea += 1
                Next
            End If
            '--Remesas
            '--Otros Conceptos
            Dim SUMtaxableAmount As Integer = 0
            If Not IsNothing(Factura.OtrosConceptos) Then
                For i = 0 To Factura.OtrosConceptos.Count - 1
                    If Factura.OtrosConceptos(i).ConceptosVentas.Operacion <> 2 Then
                        Dim detalle = New Line()
                        detalle.Number = indLinea.ToString()
                        detalle.Quantity = "1"
                        detalle.QuantityUnitOfMeasure = "NAR" '--Unidad de medida en el estandar
                        detalle.UnitPrice = Factura.OtrosConceptos(i).Valor_Concepto.ToString()
                        detalle.GrossAmount = Factura.OtrosConceptos(i).Valor_Concepto.ToString()
                        detalle.NetAmount = Factura.OtrosConceptos(i).Valor_Concepto.ToString()
                        Dim NumeroRemesa As String = ""
                        If Factura.OtrosConceptos(i).Remesas.NumeroDocumento > 0 Then
                            NumeroRemesa = " Remesa " & Factura.OtrosConceptos(i).Remesas.NumeroDocumento.ToString()
                        End If
                        detalle.Item = New Item With {
                            .Description = Factura.OtrosConceptos(i).ConceptosVentas.Nombre & NumeroRemesa
                        }
                        Dim DetalleImpuestosConceptos = Factura.OtrosConceptos(i).DetalleImpuestoConceptosFacturas
                        Dim objSubIVA = New TaxSubTotal With {.TaxCategory = "", .TaxPercentage = "", .TaxableAmount = "0", .TaxAmount = "0"}
                        Dim objSubRETEFUENTE = New WithholdingTaxSubTotal With {.WithholdingTaxCategory = "", .TaxPercentage = "", .TaxableAmount = "0", .TaxAmount = "0"}
                        Dim objSubRETEICA = New WithholdingTaxSubTotal With {.WithholdingTaxCategory = "", .TaxPercentage = "", .TaxableAmount = "0", .TaxAmount = "0"}

                        Dim objIVA = New TaxTotal With {.TaxCategory = "", .TaxAmount = "0"}
                        Dim objRETEFUENTE = New WithholdingTaxTotal With {.WithholdingTaxCategory = "0", .TaxAmount = "0"}
                        Dim objRETEICA = New WithholdingTaxTotal With {.WithholdingTaxCategory = "0", .TaxAmount = "0"}

                        For j = 0 To DetalleImpuestosConceptos.Count - 1
                            Select Case DetalleImpuestosConceptos(j).ENIM_Codigo
                                Case CODGIO_IMPUESTO.IVA
                                    objSubIVA.TaxCategory = NOMBRE_IMPUESTO_IVA
                                    objSubIVA.TaxPercentage = (FormatNumber(CDbl(DetalleImpuestosConceptos(j).Valor_tarifa * 100), 3)).ToString().Replace(",", ".")
                                    objSubIVA.TaxableAmount += DetalleImpuestosConceptos(j).Valor_base
                                    objSubIVA.TaxAmount += DetalleImpuestosConceptos(j).Valor_impuesto

                                    objIVA.TaxCategory = NOMBRE_IMPUESTO_IVA
                                    objIVA.TaxAmount += DetalleImpuestosConceptos(j).Valor_impuesto
                                Case CODGIO_IMPUESTO.RETE_FUENTE
                                    objSubRETEFUENTE.WithholdingTaxCategory = NOMBRE_RETENCION_FUENTE
                                    objSubRETEFUENTE.TaxPercentage = (FormatNumber(CDbl(DetalleImpuestosConceptos(j).Valor_tarifa * 100), 3)).ToString().Replace(",", ".")
                                    objSubRETEFUENTE.TaxableAmount += DetalleImpuestosConceptos(j).Valor_base
                                    objSubRETEFUENTE.TaxAmount += DetalleImpuestosConceptos(j).Valor_impuesto

                                    objRETEFUENTE.WithholdingTaxCategory = NOMBRE_RETENCION_FUENTE
                                    objRETEFUENTE.TaxAmount += DetalleImpuestosConceptos(j).Valor_impuesto
                                Case CODGIO_IMPUESTO.RETE_ICA
                                    objSubRETEICA.WithholdingTaxCategory = NOMBRE_RETENCION_ICA
                                    objSubRETEICA.TaxPercentage = (FormatNumber(CDbl(DetalleImpuestosConceptos(j).Valor_tarifa * 100), 3)).ToString().Replace(",", ".")
                                    objSubRETEICA.TaxableAmount += DetalleImpuestosConceptos(j).Valor_base
                                    objSubRETEICA.TaxAmount += DetalleImpuestosConceptos(j).Valor_impuesto

                                    objRETEICA.WithholdingTaxCategory = NOMBRE_RETENCION_ICA
                                    objRETEICA.TaxAmount += DetalleImpuestosConceptos(j).Valor_impuesto
                            End Select
                        Next

                        If objSubIVA.TaxAmount > 0 Then
                            SUMtaxableAmount += objSubIVA.TaxableAmount
                            objSubIVA.TaxableAmount = objSubIVA.TaxableAmount.ToString()
                            objSubIVA.TaxAmount = objSubIVA.TaxAmount.ToString()
                            objIVA.TaxAmount = objIVA.TaxAmount.ToString()
                            detalle.TaxSubTotals.Add(objSubIVA)
                            detalle.TaxTotals.Add(objIVA)
                        Else
                            detalle.TaxSubTotals.Add(New TaxSubTotal With {.TaxCategory = NOMBRE_IMPUESTO_IVA, .TaxPercentage = "0", .TaxableAmount = "0", .TaxAmount = "0"})
                            detalle.TaxTotals.Add(New TaxTotal With {.TaxCategory = NOMBRE_IMPUESTO_IVA, .TaxAmount = "0"})
                        End If

                        If objSubRETEFUENTE.TaxAmount > 0 Then
                            objSubRETEFUENTE.TaxableAmount = objSubRETEFUENTE.TaxableAmount.ToString()
                            objSubRETEFUENTE.TaxAmount = objSubRETEFUENTE.TaxAmount.ToString()
                            objRETEFUENTE.TaxAmount = objRETEFUENTE.TaxAmount.ToString()
                            detalle.WithholdingTaxSubTotals.Add(objSubRETEFUENTE)
                            detalle.WithholdingTaxTotals.Add(objRETEFUENTE)
                        End If

                        If objSubRETEICA.TaxAmount > 0 Then
                            objSubRETEICA.TaxableAmount = objSubRETEICA.TaxableAmount.ToString()
                            objSubRETEICA.TaxAmount = objSubRETEICA.TaxAmount.ToString()
                            objRETEICA.TaxAmount = objRETEICA.TaxAmount.ToString()
                            detalle.WithholdingTaxSubTotals.Add(objSubRETEICA)
                            detalle.WithholdingTaxTotals.Add(objRETEICA)
                        End If
                        Lineas.Add(detalle)
                        indLinea += 1
                    End If
                Next
            End If
            '--Otros Conceptos
            jsonFactura.Lines = Lineas
            '-----Lineas
            '-----Totales
            jsonFactura.Total = New Total With {
                .GrossAmount = subtotal.ToString(),
                .TotalBillableAmount = (subtotal + Factura.ValorTotalIva).ToString(),
                .PayableAmount = (subtotal + Factura.ValorTotalIva - descuento).ToString(),
                .TaxableAmount = SUMtaxableAmount.ToString()
            }
            jsonFactura.Total.AllowancesTotalAmount = descuento.ToString()
            '-----Totales
            '--Fecha Emision
            jsonFactura.IssueDate = Factura.FechaCreacion.ToString()
            '--Fecha Emision
            '--Fecha Vencimiento
            jsonFactura.DueDate = Factura.FechaVence.ToString()
            '--Fecha Vencimiento
            '--Prefijo
            jsonFactura.SeriePrefix = Factura.Prefijo
            '--Prefijo
            '--Numero Factura
            jsonFactura.SerieNumber = Factura.NumeroDocumento.ToString()
            '--Numero Factura
            '--Tipo Operacion
            jsonFactura.OperationType = "10" 'Standard
            '--Tipo Operacion
            '--Modo Envio
            jsonFactura.IssueMode = TIPO_REPORTE_FACTURA_GENERA
            '--Modo Envio
            '--Numero de documento
            Dim currentTime As DateTime = Factura.FechaCreacion
            Dim dt1970 As DateTime = New DateTime(1970, 1, 1)
            Dim span As TimeSpan = currentTime - dt1970
            jsonFactura.CorrelationDocumentId = "FV" + Factura.Prefijo + Factura.NumeroDocumento.ToString() + "-" + span.TotalMilliseconds.ToString()
            '--Numero de documento
            '--llave externa
            jsonFactura.SerieExternalKey = Factura.OficinaFactura.ClaveExternaFactura
            '---llave externa
            '--Descuentos
            Dim Descuentos = New List(Of AllowanceCharge)
            Dim AcuDesc As Integer = 1
            For i = 0 To Factura.OtrosConceptos.Count - 1
                Dim porcentaje As Double = (Factura.OtrosConceptos(i).Valor_Concepto * 100) / subtotal
                If Factura.OtrosConceptos(i).ConceptosVentas.Operacion = 2 Then
                    Dim NumeroRemesa As String = ""
                    If Factura.OtrosConceptos(i).Remesas.NumeroDocumento > 0 Then
                        NumeroRemesa = " Remesa " & Factura.OtrosConceptos(i).Remesas.NumeroDocumento.ToString()
                    End If

                    Dim detalle = New AllowanceCharge With {
                        .ChargeIndicator = "false",'---indica que es un descuento no un cargo
                        .BaseAmount = subtotal.ToString(),
                        .ReasonCode = "00", ' Anexo Tecnico 1.8 18-AGO-2021 se cambio el valor 11 por 00
                        .Reason = Factura.OtrosConceptos(i).ConceptosVentas.Nombre & NumeroRemesa,
                        .Amount = Factura.OtrosConceptos(i).Valor_Concepto.ToString(),
                        .Percentage = (FormatNumber(porcentaje, 3)).ToString.Replace(",", "."),
                        .SequenceIndicator = AcuDesc.ToString()
                    }
                    Descuentos.Add(detalle)
                    AcuDesc += 1
                End If
            Next
            If Descuentos.Count > 0 Then
                jsonFactura.AllowanceCharges = Descuentos
            End If
            '--Descuentos
            '--Pdf Reporte
            jsonFactura.PdfData = New PdfData With {.Pdf = Factura.FacturaElectronica.Archivo}
            '--Pdf Reporte
            '----Seccion serializado
            Return jsonFactura
        End Function

        Public Function Emision_Factura_Saphety(Empresa As Empresas, JsonFacturas As DocumentoFacturaJson) As JObject
            Dim urlEmision As String = ObtenerUrlApiSaphety(Empresa.EmpresaFacturaElectronica.Ambiente.Codigo) & API_POST_EMISION_FACTURA.Replace(REPLACE_VIRTUAL_OPERATOR, Empresa.EmpresaFacturaElectronica.OperadorVirtual)
            Dim objResult As JObject = RequestPost(urlEmision, JsonFacturas, Token)
            Return objResult
        End Function

        Public Function Emision_Nota_Saphety(Empresa As Empresas, JsonNota As DocumentoNotaJson, TipoNota As Integer) As JObject
            Dim urlEmision As String
            If TipoNota = TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_CREDITO Then
                urlEmision = ObtenerUrlApiSaphety(Empresa.EmpresaFacturaElectronica.Ambiente.Codigo) & API_POST_EMISION_NOTA_CREDITO.Replace(REPLACE_VIRTUAL_OPERATOR, Empresa.EmpresaFacturaElectronica.OperadorVirtual)
            ElseIf TipoNota = TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_DEBITO Then
                urlEmision = ObtenerUrlApiSaphety(Empresa.EmpresaFacturaElectronica.Ambiente.Codigo) & API_POST_EMISION_NOTA_DEBITO.Replace(REPLACE_VIRTUAL_OPERATOR, Empresa.EmpresaFacturaElectronica.OperadorVirtual)
            End If
            Dim objResult As JObject = RequestPostNota(urlEmision, JsonNota, Token)
            Return objResult
        End Function

        Public Function Habilitar_Factura_Saphety(Empresa As Empresas, JsonFacturas As DocumentoFacturaJson) As JObject
            Dim urlEmision As String = ObtenerUrlApiSaphety(Empresa.EmpresaFacturaElectronica.Ambiente.Codigo) & API_POST_HABILITA_FACTURA.Replace(REPLACE_VIRTUAL_OPERATOR, Empresa.EmpresaFacturaElectronica.OperadorVirtual)
            Dim objResult As JObject = RequestPost(urlEmision, JsonFacturas, Token)
            Return objResult
        End Function
#End Region

#Region "utilitarios"
        Private Function RequestPost(ByVal URL As String, ByVal JSonData As Dictionary(Of String, Object), Optional Token As TokenSaphety = Nothing) As JObject
            'Se crea el Objeto que realizará la solicitud REST
            Dim webClient As New WebClient()
            Try
                'Headers
                webClient.Headers("content-type") = "application/json"
                webClient.Headers("Accept") = "application / json"
                If Not IsNothing(Token) Then
                    webClient.Headers("Authorization") = Token.token_type & " " & Token.access_token
                End If
                'Se convierte el dictionary que representa los datos en un String
                Dim strJSON As String = JsonConvert.SerializeObject(JSonData, Formatting.Indented)
                'El String con el JSON se convierte a un arreglo de Bytes
                Dim reqString() As Byte = Encoding.Default.GetBytes(strJSON)
                'Se realiza la solicitud por el verbo POST enviando el JSON representado en Bytes
                Dim resByte As Byte() = webClient.UploadData(URL, "post", reqString)
                'Se convierte la respuesta que es un arreglo de Bytes a un String
                Dim resString As String = Encoding.Default.GetString(resByte)
                'Se libera el Objeto de la conexión
                webClient.Dispose()
                'Se retorna la respuestaString en formato JObject
                Return JsonConvert.DeserializeObject(Of JObject)(resString)
            Catch ex As Exception
                'Se imprime en consola el error generado
                Console.WriteLine(ex.Message)
                Return JsonConvert.DeserializeObject(Of JObject)("{}")
            End Try
            'Se envía una respuesta vacia en caso de no pederse realizar la solicitud
            Return JsonConvert.DeserializeObject(Of JObject)("{}")

        End Function

        Private Function RequestPost(ByVal URL As String, ByVal JSonData As DocumentoFacturaJson, Optional Token As TokenSaphety = Nothing) As JObject
            'Se crea el Objeto que realizará la solicitud REST
            Dim webClient As New WebClient()
            Try
                'Headers
                webClient.Headers("content-type") = "application/json"
                webClient.Headers("Accept") = "application / json"
                If Not IsNothing(Token) Then
                    webClient.Headers("Authorization") = Token.token_type & " " & Token.access_token
                End If
                Dim JavaScriptSerializer = New JavaScriptSerializer()
                Dim strJSON As String = JavaScriptSerializer.Serialize(JSonData)
                Dim reqString() As Byte = Encoding.UTF8.GetBytes(strJSON)
                Dim resByte As Byte() = webClient.UploadData(URL, "post", reqString)
                Dim resString As String = Encoding.Default.GetString(resByte)
                webClient.Dispose()
                Return JsonConvert.DeserializeObject(Of JObject)(resString)
            Catch ex As WebException
                Dim resError As String = "{}"
                If ex.Response IsNot Nothing Then
                    '' can use ex.Response.Status, .StatusDescription
                    If ex.Response.ContentLength <> 0 Then
                        Using stream = ex.Response.GetResponseStream()
                            Using reader = New StreamReader(stream)
                                resError = reader.ReadToEnd()
                            End Using
                        End Using
                    End If
                End If
                Return JsonConvert.DeserializeObject(Of JObject)(resError)
            End Try
            Return JsonConvert.DeserializeObject(Of JObject)("{}")

        End Function

        Private Function RequestPostNota(ByVal URL As String, ByVal JSonData As DocumentoNotaJson, Optional Token As TokenSaphety = Nothing) As JObject
            'Se crea el Objeto que realizará la solicitud REST
            Dim webClient As New WebClient()
            Try
                'Headers
                webClient.Headers("content-type") = "application/json"
                webClient.Headers("Accept") = "application / json"
                If Not IsNothing(Token) Then
                    webClient.Headers("Authorization") = Token.token_type & " " & Token.access_token
                End If
                Dim JavaScriptSerializer = New JavaScriptSerializer()
                Dim strJSON As String = JavaScriptSerializer.Serialize(JSonData)
                Dim reqString() As Byte = Encoding.UTF8.GetBytes(strJSON)
                Dim resByte As Byte() = webClient.UploadData(URL, "post", reqString)
                Dim resString As String = Encoding.Default.GetString(resByte)
                webClient.Dispose()
                Return JsonConvert.DeserializeObject(Of JObject)(resString)
            Catch ex As WebException
                Dim resError As String = "{}"
                If ex.Response IsNot Nothing Then
                    '' can use ex.Response.Status, .StatusDescription
                    If ex.Response.ContentLength <> 0 Then
                        Using stream = ex.Response.GetResponseStream()
                            Using reader = New StreamReader(stream)
                                resError = reader.ReadToEnd()
                            End Using
                        End Using
                    End If
                End If
                Return JsonConvert.DeserializeObject(Of JObject)(resError)
            End Try
            Return JsonConvert.DeserializeObject(Of JObject)("{}")

        End Function

        Private Function RequestGet(ByVal URL As String) As String
            Dim webClient As New System.Net.WebClient
            Dim result As String = webClient.DownloadString(URL)
            Return result
        End Function

        Public Function ObtenerUrlApiSaphety(Ambiente)
            Dim url As String = ""
            Select Case Ambiente
                Case AMBIENTE_FACTURA_ELECTRONICA.PRUEBAS, AMBIENTE_FACTURA_ELECTRONICA.HABILITACION
                    url = GEN_URL_SAPHETY
                Case AMBIENTE_FACTURA_ELECTRONICA.PRODUCCION
                    url = GEN_URL_SAPHETY_PRODUCCION
            End Select
            Return url
        End Function

        Public Shared Function ImageToByte(ByVal img As System.Drawing.Image) As Byte()
            Dim converter As ImageConverter = New ImageConverter
            Return CType(converter.ConvertTo(img, GetType(System.Byte())), Byte())
        End Function

        Public Function EncodeQRIMG(ByVal Data As String) As Bitmap
            Try
                Me.Encoder = New QRCodeEncoder
                'Dim QRCodeImage As Bitmap
                Dim ModuleSize As Integer = 4
                Dim QuietZone As Integer = 16
                Dim EciValue As Integer = -1
                Me.Encoder.ErrorCorrection = ErrorCorrection.M
                Me.Encoder.ModuleSize = ModuleSize
                Me.Encoder.QuietZone = QuietZone
                Me.Encoder.ECIAssignValue = EciValue
                ' single segment
                ' encode data
                Me.Encoder.Encode(Data)
                ' create bitmap
                Me.QRCodeImage = Me.Encoder.CreateQRCodeBitmap
                Return Me.QRCodeImage
            Catch Ex As Exception
                Return Nothing
            End Try
        End Function

        Public Function GenerarEnvioNotaElectronica(Empresa As Empresas, NumeroNota As Integer, TipoNota As Integer) As Boolean
            Dim Nota As NotasFacturas = New NotasFacturas()
            Dim RespuestaGuarda As Long = 0

            Dim RespEnvioPruebas As JObject = Nothing
            Dim RespEnvioHabilitacion As JObject = Nothing
            Dim RespEnvioProduccion As JObject = Nothing
            Dim TextErrorField As String = ""
            Dim CodeError As String = ""
            Dim DescriptionError As String = ""
            Dim ProcesoExito As Boolean = True
            Dim ArchivoRespuesta As NotasFacturas = New NotasFacturas()

            Nota.CodigoEmpresa = Empresa.Codigo
            Nota.TipoDocumento = TipoNota
            Nota.Numero = NumeroNota

            Nota = New RepositorioNotasFacturas().ObtenerDatosNotaElectronicaSaphety(Nota)
            If Token.IsValid Then
                'Creacion de Cude
                Dim resultCude As JObject = GET_CUDE(Empresa, Nota)
                If Convert.ToBoolean(resultCude("IsValid")) = True Then
                    CUDE = resultCude("ResultData")
                End If

                'Guarda Factura para reporte
                Nota.CUDE = CUDE
                RespuestaGuarda = New RepositorioNotasFacturas().GuardarNotaElectronica(Nota)

                If RespuestaGuarda > 0 Then
                    'Obtiene PDF Generado
                    Dim geturl As String = "Empresa=" & Nota.CodigoEmpresa & "&"
                    geturl += "NombRepo=" & NOMBRE_REPORTE_NOTA_CREDITO & "&"
                    geturl += "Numero=" & Nota.Numero & "&"
                    geturl += "OpcionExportarPdf=1&"
                    geturl += "Prefijo=" & Empresa.Prefijo & "&"
                    geturl += "TipoExportar=" & TIPO_REPORTE_FACTURA_GENERA & "&"

                    Dim urlReporte As String = URL_ASP & "Reportes/Reportes.aspx?"
                    Dim result = RequestGet(urlReporte & geturl)
                End If

                Dim DocumentoNota As NotasFacturas = New NotasFacturas With {.CodigoEmpresa = Nota.CodigoEmpresa, .Numero = Nota.Numero}
                DocumentoNota = New RepositorioNotasFacturas().ObtenerDocumentoReporteSaphety(DocumentoNota)
                If DocumentoNota.Numero > 0 Then
                    Nota.Archivo = DocumentoNota.Archivo
                End If

                '--Genera Obj Json
                Dim JsonNota As DocumentoNotaJson = GenerarJsonNota(Empresa, Nota)

                '--Envio de Nota electronica
                If Not IsNothing(JsonNota) Then
                    Select Case Empresa.EmpresaFacturaElectronica.Ambiente.Codigo
                        Case AMBIENTE_FACTURA_ELECTRONICA.PRUEBAS
                            RespEnvioPruebas = Emision_Nota_Saphety(Empresa, JsonNota, TipoNota)
                            If Convert.ToBoolean(RespEnvioPruebas("IsValid")) = False Then
                                ProcesoExito = False
                                TextErrorField = If(RespEnvioPruebas("Errors")(0)("Field") <> "null", RespEnvioPruebas("Errors")(0)("Field"), "")
                                CodeError = If(RespEnvioPruebas("Errors")(0)("Code") <> "null", RespEnvioPruebas("Errors")(0)("Code"), "")
                                DescriptionError = If(RespEnvioPruebas("Errors")(0)("Description") <> "", RespEnvioPruebas("Errors")(0)("Description"), "")
                            Else
                                ArchivoRespuesta.Archivo = RespEnvioPruebas("ResultData")("Content")
                                ArchivoRespuesta.TipoArchivo = RespEnvioPruebas("ResultData")("ContentType")
                                ArchivoRespuesta.DecripcionArchivo = "UBL Documento"
                            End If
                        Case AMBIENTE_FACTURA_ELECTRONICA.HABILITACION
                            RespEnvioHabilitacion = Emision_Nota_Saphety(Empresa, JsonNota, TipoNota)
                            If Convert.ToBoolean(RespEnvioHabilitacion("IsValid")) = False Then
                                ProcesoExito = False
                                TextErrorField = If(RespEnvioHabilitacion("Errors")(0)("Field") <> "null", RespEnvioHabilitacion("Errors")(0)("Field"), "")
                                CodeError = If(RespEnvioHabilitacion("Errors")(0)("Code") <> "null", RespEnvioHabilitacion("Errors")(0)("Code"), "")
                                DescriptionError = If(RespEnvioHabilitacion("Errors")(0)("Description") <> "", RespEnvioHabilitacion("Errors")(0)("Description"), "")
                            Else
                                ArchivoRespuesta.Archivo = RespEnvioHabilitacion("ResultData")("Content")
                                ArchivoRespuesta.TipoArchivo = RespEnvioHabilitacion("ResultData")("ContentType")
                                ArchivoRespuesta.DecripcionArchivo = "UBL Documento"
                            End If
                        Case AMBIENTE_FACTURA_ELECTRONICA.PRODUCCION
                            RespEnvioProduccion = Emision_Nota_Saphety(Empresa, JsonNota, TipoNota)
                            If Convert.ToBoolean(RespEnvioProduccion("IsValid")) = False Then
                                ProcesoExito = False
                                TextErrorField = If(RespEnvioProduccion("Errors")(0)("Field") <> "null", RespEnvioProduccion("Errors")(0)("Field"), "")
                                CodeError = If(RespEnvioProduccion("Errors")(0)("Code") <> "null", RespEnvioProduccion("Errors")(0)("Code"), "")
                                DescriptionError = If(RespEnvioProduccion("Errors")(0)("Description") <> "", RespEnvioProduccion("Errors")(0)("Description"), "")
                            Else
                                ArchivoRespuesta.Archivo = RespEnvioProduccion("ResultData")("Content")
                                ArchivoRespuesta.TipoArchivo = RespEnvioProduccion("ResultData")("ContentType")
                                ArchivoRespuesta.DecripcionArchivo = "UBL Documento"
                            End If
                    End Select
                End If

                Nota.Archivo = ArchivoRespuesta.Archivo
                Nota.TipoArchivo = ArchivoRespuesta.TipoArchivo
                Nota.DecripcionArchivo = ArchivoRespuesta.DecripcionArchivo
                Nota.NotaElectronica = If(ProcesoExito = True, 1, 0)
            Else
                ProcesoExito = False
                Nota.Archivo = ArchivoRespuesta.Archivo
                Nota.TipoArchivo = ArchivoRespuesta.TipoArchivo
                Nota.DecripcionArchivo = ArchivoRespuesta.DecripcionArchivo
                Nota.NotaElectronica = 0
            End If
            RespuestaGuarda = New RepositorioNotasFacturas().GuardarNotaElectronica(Nota)

            Return ProcesoExito
        End Function

        Public Function GenerarJsonNota(Empresa As Empresas, Nota As NotasFacturas) As DocumentoNotaJson
            'Valida Destinatario
            Dim Destinatario As Terceros
            If Nota.FacturarA.Codigo > 0 Then
                Destinatario = Nota.FacturarA
            Else
                Destinatario = Nota.Clientes
            End If
            'Valida Destinatario
            Dim jsonNota As DocumentoNotaJson = New DocumentoNotaJson()
            Dim PrefixCorrelationId As String = "NC"
            jsonNota.SerieNumber = Nota.NumeroDocumento
            If Nota.TipoDocumento = TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_CREDITO Then
                PrefixCorrelationId = "NC"
                jsonNota.SerieExternalKey = Empresa.EmpresaFacturaElectronica.ClaveExternaNotaCredito
                jsonNota.SeriePrefix = Empresa.EmpresaFacturaElectronica.PrefijoNotaCredito
                If Nota.ConceptoNotas.Nombre.Contains("ANULA") Then
                    jsonNota.ReasonCredit = "2"
                Else
                    jsonNota.ReasonCredit = "6"
                End If
            ElseIf Nota.TipoDocumento = TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_DEBITO Then
                PrefixCorrelationId = "ND"
                jsonNota.SerieExternalKey = Empresa.EmpresaFacturaElectronica.ClaveExternaNotaDebito
                jsonNota.SeriePrefix = Empresa.EmpresaFacturaElectronica.PrefijoNotaDebito
                jsonNota.ReasonDebit = "4"
            End If

            jsonNota.DeliveryDate = Nota.FechaCreacion.ToString
            jsonNota.IssueDate = Nota.FechaCreacion.ToString
            jsonNota.DueDate = Nota.FechaVence.ToString
            jsonNota.Currency = "COP"
            jsonNota.CorrelationDocumentId = PrefixCorrelationId + Nota.NumeroDocumento.ToString()
            jsonNota.PaymentMeans.Add(New PaymentMean With {.Code = "1", .Mean = "1", .DueDate = String.Format("{0:yyyy-MM-dd}", DateTimeOffset.Parse(Nota.FechaVence).DateTime)})
            jsonNota.IssuerParty = New IssuerParty()
            jsonNota.IssuerParty.DocumentContacts.Add(New DocumentContact With {.Name = Nota.Empresas.RazonSocial, .Telephone = Nota.Empresas.Telefono, .Email = Nota.Empresas.Email, .Type = "SellerContact"})
            jsonNota.IssuerParty.Identification = New Identification With {.DocumentNumber = Nota.Empresas.NumeroIdentificacion, .DocumentType = Nota.Empresas.TipoNumeroIdentificacion, .CountryCode = "CO", .CheckDigit = Nota.Empresas.DigitoChequeo}
            jsonNota.CustomerParty = New CustomerParty()
            jsonNota.CustomerParty.DocumentContacts.Add(New DocumentContact With {.Name = Destinatario.NombreCompleto, .Telephone = Destinatario.Telefonos, .Email = Destinatario.Correo, .Type = "BuyerContact"})
            jsonNota.CustomerParty.LegalType = Destinatario.TipoNaturaleza.Nombre
            jsonNota.CustomerParty.Identification = New Identification With {
                .DocumentNumber = Destinatario.NumeroIdentificacion,
                .DocumentType = Destinatario.TipoIdentificacion.Nombre,
                .CountryCode = "CO"
            }
            If Destinatario.TipoIdentificacion.Nombre.ToUpper() = "NIT" Then
                jsonNota.CustomerParty.Identification.CheckDigit = Destinatario.DigitoChequeo.ToString()
            End If
            jsonNota.CustomerParty.Name = Destinatario.NombreCompleto
            jsonNota.CustomerParty.Email = Destinatario.Correo
            jsonNota.CustomerParty.Address = New Address With {.DepartmentCode = Destinatario.Departamentos.CodigoAlterno.ToString, .CityCode = Destinatario.Ciudad.CodigoAlterno.ToString, .AddressLine = Destinatario.Direccion, .PostalCode = Destinatario.Ciudad.CodigoPostal.ToString, .Country = "CO"}
            jsonNota.CustomerParty.TaxScheme = "49"
            jsonNota.CustomerParty.Person = New Person With {
                .FirstName = Destinatario.Nombre.ToString(),
                .MiddleName = "",
                .FamilyName = Destinatario.PrimeroApellido + " " + Destinatario.SegundoApellido
            }
            'jsonNota.CustomerParty.ResponsabilityTypes = New List(Of String) From {"O-06", "O-07", "O-09", "O-14", "O-48", "O-99", "A-29"}
            jsonNota.CustomerParty.ResponsabilityTypes = New List(Of String) From {"R-99-PN"}
            Dim Lineas As List(Of Line) = New List(Of Line)
            Dim Linea = New Line()
            Linea.Number = "1"
            Linea.Quantity = "1"
            Linea.QuantityUnitOfMeasure = "NAR"  'Unidad de medida en el estandar
            Linea.TaxSubTotals.Add(New TaxSubTotal With {
                                  .TaxCategory = "IVA",
                                  .TaxPercentage = "0",
                                  .TaxableAmount = "0",
                                  .TaxAmount = "0"
                               })
            Linea.TaxTotals.Add(New TaxTotal With {
                                  .TaxAmount = "0",
                                  .TaxCategory = "IVA"
                               })
            Linea.UnitPrice = Nota.ValorNota.ToString
            Linea.GrossAmount = Nota.ValorNota.ToString
            Linea.NetAmount = Nota.ValorNota.ToString
            Linea.Item = New Item With {.Description = Nota.ConceptoNotas.Nombre}
            Lineas.Add(Linea)
            jsonNota.Lines = Lineas
            jsonNota.TaxSubTotals.Add(New TaxSubTotal With {.TaxCategory = "IVA", .TaxPercentage = "0", .TaxableAmount = "0", .TaxAmount = "0"})
            jsonNota.TaxTotals.Add(New TaxTotal With {.TaxAmount = "0", .TaxCategory = "IVA"})
            jsonNota.Total = New Total With {.GrossAmount = Nota.ValorNota.ToString, .TotalBillableAmount = Nota.ValorNota.ToString, .PayableAmount = Nota.ValorNota.ToString, .TaxableAmount = "0"}
            jsonNota.DocumentReferences.Add(New DocumentReference With {.DocumentReferred = Nota.Factura.Prefijo + Nota.NumeroDocumento.ToString, .IssueDate = Nota.Factura.FacturaElectronica.FechaCreacion.ToString, .Type = "InvoiceReference", .DocumentReferredCUFE = Nota.Factura.FacturaElectronica.CUFE})
            jsonNota.PdfData = New PdfData With {.Pdf = Nota.Archivo}

            Return jsonNota
        End Function


        Public Function GET_CUDE(Empresa As Empresas, Nota As NotasFacturas) As JObject
            Dim CUDE = New Dictionary(Of String, Object)
            CUDE.Add("IssueDate", Nota.FechaCreacion)
            CUDE.Add("CustomerDocumentNumber", IIf(Nota.FacturarA.Codigo > 0, Nota.FacturarA.NumeroIdentificacion, Nota.Clientes.NumeroIdentificacion))
            CUDE.Add("IssuerNitNumber", Nota.Empresas.NumeroIdentificacion)
            If Nota.TipoDocumento = TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_CREDITO Then
                CUDE.Add("DocumentNumber", Empresa.EmpresaFacturaElectronica.PrefijoNotaCredito & Nota.NumeroDocumento)
            ElseIf Nota.TipoDocumento = TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_DEBITO Then
                CUDE.Add("DocumentNumber", Empresa.EmpresaFacturaElectronica.PrefijoNotaDebito & Nota.NumeroDocumento)
            End If
            CUDE.Add("TotalIva", "0")
            CUDE.Add("TotalInc", "0")
            CUDE.Add("TotalIca", "0")
            CUDE.Add("TotalGrossAmount", Nota.ValorNota.ToString)
            CUDE.Add("TotalPayableAmount", Nota.ValorNota.ToString)
            CUDE.Add("SupplierTimeZoneCode", "America/Bogota")

            Dim urlCude As String = ObtenerUrlApiSaphety(Empresa.EmpresaFacturaElectronica.Ambiente.Codigo) & API_GET_CUDE.Replace(REPLACE_VIRTUAL_OPERATOR, Empresa.EmpresaFacturaElectronica.OperadorVirtual)
            Dim objResult As JObject = RequestPost(urlCude, CUDE, Token)




            Return objResult
        End Function
#End Region
    End Class
End Namespace