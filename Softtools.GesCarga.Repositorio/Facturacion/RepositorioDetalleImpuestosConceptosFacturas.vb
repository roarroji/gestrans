﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Facturacion

Namespace Facturacion
    Public NotInheritable Class RepositorioDetalleImpuestosConceptosFacturas
        Inherits RepositorioBase(Of DetalleImpuestosConceptosFacturas)

        Public Overrides Function Consultar(filtro As DetalleImpuestosConceptosFacturas) As IEnumerable(Of DetalleImpuestosConceptosFacturas)
            Dim lista As New List(Of DetalleImpuestosConceptosFacturas)
            Dim item As DetalleImpuestosConceptosFacturas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENFA_Numero", filtro.NumeroEncabezado)
                conexion.AgregarParametroSQL("@par_DECF_Codigo", filtro.DECF_Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_impuestos_conceptos_facturas")

                While resultado.Read
                    item = New DetalleImpuestosConceptosFacturas(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista

        End Function

        Public Overrides Function Insertar(entidad As DetalleImpuestosConceptosFacturas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleImpuestosConceptosFacturas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleImpuestosConceptosFacturas) As DetalleImpuestosConceptosFacturas
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace

