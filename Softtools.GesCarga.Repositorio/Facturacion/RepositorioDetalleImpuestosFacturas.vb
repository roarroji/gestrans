﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Facturacion
Namespace Facturacion
    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleImpuestosFacturas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleImpuestosFacturas
        Inherits RepositorioBase(Of DetalleImpuestosFacturas)
        Public Overrides Function Consultar(filtro As DetalleImpuestosFacturas) As IEnumerable(Of DetalleImpuestosFacturas)
            Dim lista As New List(Of DetalleImpuestosFacturas)
            Dim item As DetalleImpuestosFacturas
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENFA_Numero", filtro.NumeroEncabezado)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_impuestos_facturas]")

                While resultado.Read
                    item = New DetalleImpuestosFacturas(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleImpuestosFacturas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleImpuestosFacturas) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleImpuestosFacturas) As DetalleImpuestosFacturas
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace


