﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Repositorio.Contabilidad

Namespace Facturacion

    ''' <summary>
    ''' Clase <see cref="RepositorioFacturas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioFacturas
        Inherits RepositorioBase(Of Facturas)
        Const TIDO_Factura As Integer = 170

        Public Overrides Function Consultar(filtro As Facturas) As IEnumerable(Of Facturas)
            Dim lista As New List(Of Facturas)
            Dim item As Facturas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If filtro.NumeroDocumento > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                If filtro.CataTipoFactura > Cero Then
                    conexion.AgregarParametroSQL("@par_CATA_TIFA_Codigo", filtro.CataTipoFactura)
                End If

                If filtro.FechaInicial <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TERC_Cliente", filtro.Cliente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.FacturarA) Then
                    If filtro.FacturarA.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TERC_Facturar", filtro.FacturarA.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.OficinaFactura) Then
                    If filtro.OficinaFactura.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_OFIC_Facturar", filtro.OficinaFactura.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.FormaPago) Then
                    If filtro.FormaPago.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", filtro.FormaPago.Codigo)
                    End If
                End If

                If (filtro.Estado = Cero Or filtro.Estado = 1) And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", NO_APLICA)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", SI_APLICA)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_facturas]")

                While resultado.Read
                    item = New Facturas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As Facturas) As Long

            Dim inserto As Boolean = True
            Dim insertoDetalle As Boolean = True


            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.CleanParameters()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Numero_Preimpreso", entidad.NumeroPreImpreso)
                    conexion.AgregarParametroSQL("@par_CATA_TIFA_Codigo", entidad.CataTipoFactura)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_OFIC_Factura", entidad.OficinaFactura.Codigo)

                    conexion.AgregarParametroSQL("@par_TERC_Cliente", entidad.Cliente.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Facturar", entidad.FacturarA.Codigo)
                    If Not IsNothing(entidad.SedeFacturacion) Then
                        If entidad.SedeFacturacion.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TEDI_Codigo", entidad.SedeFacturacion.Codigo)
                        End If
                    End If
                    conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", entidad.FormaPago.Codigo)
                    conexion.AgregarParametroSQL("@par_Dias_Plazo", entidad.DiasPlazo)

                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    Else
                        conexion.AgregarParametroSQL("@par_Observaciones", VACIO)
                    End If


                    conexion.AgregarParametroSQL("@par_Valor_Remesas", entidad.ValorRemesas)
                    conexion.AgregarParametroSQL("@par_Valor_Otros_Conceptos", entidad.ValorOtrosConceptos)
                    conexion.AgregarParametroSQL("@par_Valor_Descuentos", entidad.ValorDescuentos)
                    conexion.AgregarParametroSQL("@par_Subtotal", entidad.Subtotal)
                    conexion.AgregarParametroSQL("@par_Valor_Impuestos", entidad.ValorImpuestos)

                    conexion.AgregarParametroSQL("@par_Valor_Factura", entidad.ValorFactura)
                    conexion.AgregarParametroSQL("@par_Valor_TRM", entidad.ValorTRM)
                    conexion.AgregarParametroSQL("@par_MONE_Codigo_Alterna", entidad.Moneda.Codigo)
                    conexion.AgregarParametroSQL("@par_Valor_Moneda_Alterna", entidad.ValorMonedaAlterna)
                    conexion.AgregarParametroSQL("@par_Valor_Anticipo", entidad.ValorAnticipo)

                    conexion.AgregarParametroSQL("@par_Resolucion_Facturacion", entidad.ResolucionFacturacion)
                    conexion.AgregarParametroSQL("@par_Control_Impresion", entidad.ControlImpresion)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Anulado", entidad.Anulado)

                    If Not IsNothing(entidad.NumeroOrdenCompra) Then
                        conexion.AgregarParametroSQL("@par_Numero_Orden_Compra", entidad.NumeroOrdenCompra)

                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_facturas")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While

                    resultado.Close()

                    If entidad.Numero > Cero Then

                        For Each Remesa In entidad.Remesas
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                            conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero", Remesa.Remesas.Numero)
                            conexion.AgregarParametroSQL("@par_ENOS_Numero", Remesa.Remesas.NumeroSemillaOrdenServicio)
                            conexion.AgregarParametroSQL("@par_Peso", Remesa.Remesas.PesoCliente, SqlDbType.Money)
                            conexion.AgregarParametroSQL("@par_Valor_Flete", Remesa.Remesas.ValorFleteCliente, SqlDbType.Money)
                            conexion.AgregarParametroSQL("@par_Total_Flete", Remesa.Remesas.TotalFleteCliente, SqlDbType.Money)
                            conexion.AgregarParametroSQL("@par_DT", Remesa.Remesas.DT)
                            conexion.AgregarParametroSQL("@par_Entrega", Remesa.Remesas.Entrega)
                            conexion.AgregarParametroSQL("@par_Modifica", Remesa.Modifica)
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_remesas_facturas")

                            While resultado.Read
                                If Not (resultado.Item("ENFA_Numero") > 0) Then
                                    inserto = False
                                    Exit For
                                End If
                            End While

                            resultado.Close()

                        Next

                        If inserto <> False Then
                            If Not IsNothing(entidad.OtrosConceptos) Then
                                For Each OtroConcepto In entidad.OtrosConceptos
                                    conexion.CleanParameters()
                                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                                    conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                                    conexion.AgregarParametroSQL("@par_DEND_ID", OtroConcepto.DetalleNODE.Codigo)
                                    conexion.AgregarParametroSQL("@par_COVE_Codigo", OtroConcepto.ConceptosVentas.Codigo)
                                    conexion.AgregarParametroSQL("@par_Descripcion", OtroConcepto.Descripcion)
                                    conexion.AgregarParametroSQL("@par_Valor_Concepto", OtroConcepto.Valor_Concepto, SqlDbType.Money)
                                    conexion.AgregarParametroSQL("@par_Valor_Impuestos", OtroConcepto.Valor_Impuestos, SqlDbType.Money)
                                    conexion.AgregarParametroSQL("@par_ENRE_Numero", OtroConcepto.Remesas.Numero)
                                    conexion.AgregarParametroSQL("@par_ENOS_Numero", OtroConcepto.NumeroOrdenServicio)
                                    conexion.AgregarParametroSQL("@par_EMOE_Numero", OtroConcepto.Emoe_numero)

                                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_Conceptos_facturas")

                                    While resultado.Read
                                        If Not (resultado.Item("ENFA_Numero") > 0) Then
                                            inserto = False
                                            Exit For
                                        Else
                                            OtroConcepto.DECF_Codigo = resultado.Item("Codigo")
                                        End If
                                    End While
                                    resultado.Close()

                                    For Each ImpuestosOtroConcepto In OtroConcepto.DetalleImpuestoConceptosFacturas
                                        conexion.CleanParameters()
                                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                                        conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                                        conexion.AgregarParametroSQL("@par_DECF_Codigo", OtroConcepto.DECF_Codigo)
                                        conexion.AgregarParametroSQL("@par_ENIM_Codigo", ImpuestosOtroConcepto.ENIM_Codigo)
                                        conexion.AgregarParametroSQL("@par_Valor_tarifa", ImpuestosOtroConcepto.Valor_tarifa, SqlDbType.Decimal)
                                        conexion.AgregarParametroSQL("@par_Valor_base", ImpuestosOtroConcepto.Valor_base, SqlDbType.Money)
                                        conexion.AgregarParametroSQL("@par_Valor_impuesto", ImpuestosOtroConcepto.Valor_impuesto, SqlDbType.Money)

                                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_Impuestos_Conceptos_facturas")

                                        While resultado.Read
                                            If Not (resultado.Item("ENFA_Numero") > 0) Then
                                                inserto = False
                                                Exit For
                                            End If
                                        End While
                                        resultado.Close()

                                    Next
                                    If inserto = False Then
                                        Exit For
                                    End If
                                Next
                            End If
                        End If

                        If inserto <> False Then
                            For Each DetalleImpuestosFacturas In entidad.DetalleImpuestosFacturas
                                conexion.CleanParameters()
                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                                conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                                conexion.AgregarParametroSQL("@par_ENIM_Codigo", DetalleImpuestosFacturas.ImpuestoFacturas.Codigo)
                                conexion.AgregarParametroSQL("@par_CATA_TVFI_Codigo", DetalleImpuestosFacturas.CATA_TVFI_Codigo)
                                conexion.AgregarParametroSQL("@par_Valor_tarifa", DetalleImpuestosFacturas.Valor_tarifa, SqlDbType.Decimal)
                                conexion.AgregarParametroSQL("@par_Valor_base", DetalleImpuestosFacturas.Valor_base, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Valor_impuesto", DetalleImpuestosFacturas.Valor_impuesto, SqlDbType.Money)

                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_Impuestos_facturas")

                                While resultado.Read
                                    If Not (resultado.Item("ENFA_Numero") > 0) Then
                                        inserto = False
                                        Exit For
                                    End If
                                End While
                                resultado.Close()

                            Next
                        End If

                        If entidad.Estado = 1 And inserto <> False Then
                            entidad.CuentaPorCobrar.CodigoDocumentoOrigen = entidad.Numero
                            entidad.CuentaPorCobrar.NumeroDocumento = entidad.NumeroDocumento
                            entidad.CuentaPorCobrar.Observaciones = "CUENTA POR COBRAR FACTURA No. " & entidad.NumeroDocumento.ToString() & " VALOR $ " & entidad.ValorFactura
                            inserto = Generar_CxC_Factura(entidad.CuentaPorCobrar, TIDO_Factura, conexion)
                            Dim rep As New RepositorioEncabezadoComprobantesContables
                            Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Numero, TIDO_Factura, conexion, resultado)
                        End If
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.NumeroDocumento = Cero
                End If

            End Using


            Return entidad.NumeroDocumento

        End Function

        Public Function InsertarFacturaRemesasPaqueteria(entidad As Facturas) As Long

            Dim inserto As Boolean = True

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.CleanParameters()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                conexion.AgregarParametroSQL("@par_Numero_Preimpreso", entidad.NumeroPreImpreso)
                conexion.AgregarParametroSQL("@par_CATA_TIFA_Codigo", entidad.CataTipoFactura)
                conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                conexion.AgregarParametroSQL("@par_OFIC_Factura", entidad.OficinaFactura.Codigo)

                conexion.AgregarParametroSQL("@par_TERC_Cliente", entidad.Cliente.Codigo)
                conexion.AgregarParametroSQL("@par_TERC_Facturar", entidad.FacturarA.Codigo)
                conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", entidad.FormaPago.Codigo)
                conexion.AgregarParametroSQL("@par_Dias_Plazo", entidad.DiasPlazo)

                If Not IsNothing(entidad.Observaciones) Then
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                Else
                    conexion.AgregarParametroSQL("@par_Observaciones", VACIO)
                End If


                conexion.AgregarParametroSQL("@par_Valor_Remesas", entidad.ValorRemesas)
                conexion.AgregarParametroSQL("@par_Valor_Otros_Conceptos", entidad.ValorOtrosConceptos)
                conexion.AgregarParametroSQL("@par_Valor_Descuentos", entidad.ValorDescuentos)
                conexion.AgregarParametroSQL("@par_Subtotal", entidad.Subtotal)
                conexion.AgregarParametroSQL("@par_Valor_Impuestos", entidad.ValorImpuestos)

                conexion.AgregarParametroSQL("@par_Valor_Factura", entidad.ValorFactura)
                conexion.AgregarParametroSQL("@par_Valor_TRM", entidad.ValorTRM)
                conexion.AgregarParametroSQL("@par_MONE_Codigo_Alterna", entidad.Moneda.Codigo)
                conexion.AgregarParametroSQL("@par_Valor_Moneda_Alterna", entidad.ValorMonedaAlterna)
                conexion.AgregarParametroSQL("@par_Valor_Anticipo", entidad.ValorAnticipo)

                conexion.AgregarParametroSQL("@par_Resolucion_Facturacion", entidad.ResolucionFacturacion)
                conexion.AgregarParametroSQL("@par_Control_Impresion", entidad.ControlImpresion)
                conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_Anulado", entidad.Anulado)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_facturas")

                While resultado.Read
                    entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                    entidad.Numero = resultado.Item("Numero").ToString()
                End While

                resultado.Close()

                If entidad.Numero > Cero Then

                    For Each Remesa In entidad.Remesas
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                        conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", Remesa.Remesas.Numero)
                        conexion.AgregarParametroSQL("@par_ENOS_Numero", Remesa.Remesas.NumeroSemillaOrdenServicio)
                        conexion.AgregarParametroSQL("@par_Peso", Remesa.Remesas.PesoCliente)
                        conexion.AgregarParametroSQL("@par_Valor_Flete", Remesa.Remesas.ValorFleteCliente, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Total_Flete", Remesa.Remesas.TotalFleteCliente, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Modifica", Remesa.Modifica)
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_remesas_facturas")

                        While resultado.Read
                            If Not (resultado.Item("ENFA_Numero") > 0) Then
                                inserto = False
                                Exit For
                            End If
                        End While

                        resultado.Close()

                    Next

                    'If inserto <> False Then
                    '    For Each DetalleImpuestosFacturas In entidad.DetalleImpuestosFacturas
                    '        conexion.CleanParameters()
                    '        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                    '        conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                    '        conexion.AgregarParametroSQL("@par_ENIM_Codigo", DetalleImpuestosFacturas.ImpuestoFacturas.Codigo)
                    '        conexion.AgregarParametroSQL("@par_CATA_TVFI_Codigo", DetalleImpuestosFacturas.CATA_TVFI_Codigo)
                    '        conexion.AgregarParametroSQL("@par_Valor_tarifa", DetalleImpuestosFacturas.Valor_tarifa, SqlDbType.Decimal)
                    '        conexion.AgregarParametroSQL("@par_Valor_base", DetalleImpuestosFacturas.Valor_base, SqlDbType.Money)
                    '        conexion.AgregarParametroSQL("@par_Valor_impuesto", DetalleImpuestosFacturas.Valor_impuesto, SqlDbType.Money)

                    '        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_Impuestos_facturas")

                    '        While resultado.Read
                    '            If Not (resultado.Item("ENFA_Numero") > 0) Then
                    '                inserto = False
                    '                Exit For
                    '            End If
                    '        End While
                    '        resultado.Close()

                    '    Next
                    'End If

                    'If entidad.Estado = 1 And inserto <> False Then
                    '    entidad.CuentaPorCobrar.CodigoDocumentoOrigen = entidad.Numero
                    '    entidad.CuentaPorCobrar.NumeroDocumento = entidad.NumeroDocumento
                    '    entidad.CuentaPorCobrar.Observaciones = "CUENTA POR COBRAR FACTURA No. " & entidad.NumeroDocumento.ToString() & " VALOR $ " & entidad.ValorFactura
                    '    inserto = Generar_CxC_Factura(entidad.CuentaPorCobrar, TIDO_Factura, conexion)
                    '    Dim rep As New RepositorioEncabezadoComprobantesContables
                    '    Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Numero, TIDO_Factura, conexion, resultado)
                    'End If
                End If


            End Using

            If inserto = False Then
                Return 0
            End If

            Return entidad.NumeroDocumento

        End Function

        Public Overrides Function Modificar(entidad As Facturas) As Long

            Dim Modifico As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_OFIC_Factura", entidad.OficinaFactura.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Cliente", entidad.Cliente.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Facturar", entidad.FacturarA.Codigo)
                    If Not IsNothing(entidad.SedeFacturacion) Then
                        If entidad.SedeFacturacion.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TEDI_Codigo", entidad.SedeFacturacion.Codigo)
                        End If
                    End If
                    conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", entidad.FormaPago.Codigo)
                    conexion.AgregarParametroSQL("@par_Dias_Plazo", entidad.DiasPlazo)

                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    End If

                    conexion.AgregarParametroSQL("@par_Valor_Remesas", entidad.ValorRemesas)
                    conexion.AgregarParametroSQL("@par_Valor_Otros_Conceptos", entidad.ValorOtrosConceptos)
                    conexion.AgregarParametroSQL("@par_Valor_Descuentos", entidad.ValorDescuentos)
                    conexion.AgregarParametroSQL("@par_Subtotal", entidad.Subtotal)
                    conexion.AgregarParametroSQL("@par_Valor_Impuestos", entidad.ValorImpuestos)

                    conexion.AgregarParametroSQL("@par_Valor_Factura", entidad.ValorFactura)
                    conexion.AgregarParametroSQL("@par_Valor_TRM", entidad.ValorTRM)
                    conexion.AgregarParametroSQL("@par_MONE_Codigo_Alterna", entidad.Moneda.Codigo)
                    conexion.AgregarParametroSQL("@par_Valor_Moneda_Alterna", entidad.ValorMonedaAlterna)
                    conexion.AgregarParametroSQL("@par_Valor_Anticipo", entidad.ValorAnticipo)

                    conexion.AgregarParametroSQL("@par_Resolucion_Facturacion", entidad.ResolucionFacturacion)
                    conexion.AgregarParametroSQL("@par_Control_Impresion", entidad.ControlImpresion)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)

                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)

                    If Not IsNothing(entidad.NumeroOrdenCompra) Then
                        conexion.AgregarParametroSQL("@par_Numero_Orden_Compra", entidad.NumeroOrdenCompra)

                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_facturas")

                    While resultado.Read
                        entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                        entidad.NumeroDocumento = Convert.ToInt64(Read(resultado, "Numero_Documento"))
                    End While

                    resultado.Close()

                    If entidad.Numero > Cero Then

                        For Each Remesa In entidad.Remesas
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                            conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero", Remesa.Remesas.Numero)
                            conexion.AgregarParametroSQL("@par_ENOS_Numero", Remesa.Remesas.NumeroOrdenServicio)
                            conexion.AgregarParametroSQL("@par_Peso", Remesa.Remesas.PesoCliente, SqlDbType.Money)
                            conexion.AgregarParametroSQL("@par_Valor_Flete", Remesa.Remesas.ValorFleteCliente, SqlDbType.Money)
                            conexion.AgregarParametroSQL("@par_Total_Flete", Remesa.Remesas.TotalFleteCliente, SqlDbType.Money)
                            conexion.AgregarParametroSQL("@par_DT", Remesa.Remesas.DT)
                            conexion.AgregarParametroSQL("@par_Entrega", Remesa.Remesas.Entrega)
                            conexion.AgregarParametroSQL("@par_Modifica", Remesa.Modifica)
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_remesas_facturas")

                            While resultado.Read
                                If Not (resultado.Item("ENFA_Numero") > 0) Then
                                    Modifico = False
                                    Exit For
                                End If
                            End While

                            resultado.Close()

                        Next

                        If Modifico <> False Then
                            If Not IsNothing(entidad.OtrosConceptos) Then
                                For Each OtroConcepto In entidad.OtrosConceptos
                                    conexion.CleanParameters()
                                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                                    conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                                    conexion.AgregarParametroSQL("@par_DEND_ID", OtroConcepto.DetalleNODE.Codigo)
                                    conexion.AgregarParametroSQL("@par_COVE_Codigo", OtroConcepto.ConceptosVentas.Codigo)
                                    conexion.AgregarParametroSQL("@par_Descripcion", OtroConcepto.Descripcion)
                                    conexion.AgregarParametroSQL("@par_Valor_Concepto", OtroConcepto.Valor_Concepto, SqlDbType.Money)
                                    conexion.AgregarParametroSQL("@par_Valor_Impuestos", OtroConcepto.Valor_Impuestos, SqlDbType.Money)
                                    conexion.AgregarParametroSQL("@par_ENRE_Numero", OtroConcepto.Remesas.Numero)
                                    conexion.AgregarParametroSQL("@par_ENOS_Numero", OtroConcepto.NumeroOrdenServicio)
                                    conexion.AgregarParametroSQL("@par_EMOE_Numero", OtroConcepto.Emoe_numero)

                                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_Conceptos_facturas")

                                    While resultado.Read
                                        If Not (resultado.Item("ENFA_Numero") > 0) Then
                                            Modifico = False
                                            Exit For
                                        Else
                                            OtroConcepto.DECF_Codigo = resultado.Item("Codigo")
                                        End If
                                    End While
                                    resultado.Close()

                                    For Each ImpuestosOtroConcepto In OtroConcepto.DetalleImpuestoConceptosFacturas
                                        conexion.CleanParameters()
                                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                                        conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                                        conexion.AgregarParametroSQL("@par_DECF_Codigo", OtroConcepto.DECF_Codigo)
                                        conexion.AgregarParametroSQL("@par_ENIM_Codigo", ImpuestosOtroConcepto.ENIM_Codigo)
                                        conexion.AgregarParametroSQL("@par_Valor_tarifa", ImpuestosOtroConcepto.Valor_tarifa, SqlDbType.Decimal)
                                        conexion.AgregarParametroSQL("@par_Valor_base", ImpuestosOtroConcepto.Valor_base, SqlDbType.Money)
                                        conexion.AgregarParametroSQL("@par_Valor_impuesto", ImpuestosOtroConcepto.Valor_impuesto, SqlDbType.Money)

                                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_Impuestos_Conceptos_facturas")

                                        While resultado.Read
                                            If Not (resultado.Item("ENFA_Numero") > 0) Then
                                                Modifico = False
                                                Exit For
                                            End If
                                        End While
                                        resultado.Close()

                                    Next
                                    If Modifico = False Then
                                        Exit For
                                    End If
                                Next
                            End If
                        End If

                        If Modifico <> False Then
                            For Each DetalleImpuestosFacturas In entidad.DetalleImpuestosFacturas
                                conexion.CleanParameters()
                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                                conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                                conexion.AgregarParametroSQL("@par_ENIM_Codigo", DetalleImpuestosFacturas.ImpuestoFacturas.Codigo)
                                conexion.AgregarParametroSQL("@par_CATA_TVFI_Codigo", DetalleImpuestosFacturas.CATA_TVFI_Codigo)
                                conexion.AgregarParametroSQL("@par_Valor_tarifa", DetalleImpuestosFacturas.Valor_tarifa, SqlDbType.Decimal)
                                conexion.AgregarParametroSQL("@par_Valor_base", DetalleImpuestosFacturas.Valor_base, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Valor_impuesto", DetalleImpuestosFacturas.Valor_impuesto, SqlDbType.Money)

                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_Impuestos_facturas")

                                While resultado.Read
                                    If Not (resultado.Item("ENFA_Numero") > 0) Then
                                        Modifico = False
                                        Exit For
                                    End If
                                End While
                                resultado.Close()

                            Next
                        End If

                        If entidad.Estado = 1 And Modifico <> False Then

                            entidad.CuentaPorCobrar.CodigoDocumentoOrigen = entidad.Numero
                            entidad.CuentaPorCobrar.NumeroDocumento = entidad.NumeroDocumento
                            entidad.CuentaPorCobrar.Observaciones = "CUENTA POR COBRAR FACTURA No. " & entidad.NumeroDocumento.ToString() & " VALOR $ " & entidad.ValorFactura
                            Modifico = Generar_CxC_Factura(entidad.CuentaPorCobrar, TIDO_Factura, conexion)
                            Dim rep As New RepositorioEncabezadoComprobantesContables
                            Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Numero, TIDO_Factura, conexion, resultado)
                        End If
                    End If

                End Using

                If Modifico Then
                    transaccion.Complete()
                Else
                    entidad.NumeroDocumento = Cero
                End If

            End Using

            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As Facturas) As Facturas
            Dim item As New Facturas
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroDocumento", filtro.NumeroDocumento)
                End If

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TERC_Cliente", filtro.Cliente.Codigo)
                    End If
                End If

                If filtro.Estado > -1 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                If Not IsNothing(filtro.OficinaFactura) Then
                    If filtro.OficinaFactura.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.OficinaFactura.Codigo)
                    End If
                End If

                If filtro.Anulado > -1 Then
                    conexion.AgregarParametroSQL("@par_Anulado", filtro.Anulado)
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_facturas")

                While resultado.Read
                    item = New Facturas(resultado)
                End While

                If item.Numero > 0 Then
                    Dim DetalleFacturas As New DetalleFacturas With {.CodigoEmpresa = item.CodigoEmpresa, .NumeroEncabezado = item.Numero}
                    item.Remesas = New RepositorioDetalleFacturas().Consultar(DetalleFacturas)

                    Dim DetalleConceptosFacturas As New DetalleConceptosFacturas With {.CodigoEmpresa = item.CodigoEmpresa, .NumeroEncabezado = item.Numero}
                    item.OtrosConceptos = New RepositorioDetalleConceptosFacturas().Consultar(DetalleConceptosFacturas)

                    Dim DetalleImpuestosFacturas As New DetalleImpuestosFacturas With {.CodigoEmpresa = item.CodigoEmpresa, .NumeroEncabezado = item.Numero}
                    item.DetalleImpuestosFacturas = New RepositorioDetalleImpuestosFacturas().Consultar(DetalleImpuestosFacturas)
                End If


            End Using

            Return item

        End Function

        Public Function Anular(entidad As Facturas) As Boolean
            Dim anulo As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_encabezado_factura_ventas")

                While resultado.Read
                    anulo = IIf(Convert.ToInt32(resultado.Item("Numero")) > 0, True, False)
                End While

            End Using
            Return anulo
        End Function

        Public Function Generar_CxC_Factura(entidad As EncabezadoDocumentoCuentas, TidoOrigen As Integer, conexion As DataBaseFactory) As Boolean
            Generar_CxC_Factura = False

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo_Origen", TidoOrigen)
            conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
            conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
            conexion.AgregarParametroSQL("@par_Codigo_Documento_Origen", entidad.CodigoDocumentoOrigen)
            conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.NumeroDocumento)
            conexion.AgregarParametroSQL("@par_Fecha_Documento_Origen", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Fecha_Vence_Documento_Origen", DateAdd(DateInterval.Day, 30, entidad.Fecha), SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPuc.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Total", entidad.ValorTotal)
            conexion.AgregarParametroSQL("@par_Abono", entidad.Abono)
            conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo)
            conexion.AgregarParametroSQL("@par_Fecha_Cancelacion_Pago", entidad.FechaCancelacionPago, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Aprobado", entidad.Aprobado)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Aprobo", entidad.UsuarioAprobo.Codigo)
            conexion.AgregarParametroSQL("@par_Fecha_Aprobo", entidad.FechaAprobo, SqlDbType.DateTime)
            conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
            conexion.AgregarParametroSQL("@par_AplicaPSL", Cero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_documento_cuentas")

            While resultado.Read
                Generar_CxC_Factura = IIf(resultado.Item("Codigo") > 0, True, False)
            End While
            resultado.Close()

            Return Generar_CxC_Factura
        End Function

        Public Function ObtenerDatosFacturaElectronicaSaphety(filtro As Facturas) As Facturas
            Dim item As New Facturas
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_factura_electronica_Saphety")

                While resultado.Read
                    item = New Facturas(resultado)

                    Dim DetalleFacturas As New DetalleFacturas With {.CodigoEmpresa = item.CodigoEmpresa, .NumeroEncabezado = item.Numero}
                    item.Remesas = New RepositorioDetalleFacturas().Consultar(DetalleFacturas)

                    Dim DetalleConceptosFacturas As New DetalleConceptosFacturas With {.CodigoEmpresa = item.CodigoEmpresa, .NumeroEncabezado = item.Numero}
                    item.OtrosConceptos = New RepositorioDetalleConceptosFacturas().Consultar(DetalleConceptosFacturas)

                    Dim DetalleImpuestosFacturas As New DetalleImpuestosFacturas With {.CodigoEmpresa = item.CodigoEmpresa, .NumeroEncabezado = item.Numero}
                    item.DetalleImpuestosFacturas = New RepositorioDetalleImpuestosFacturas().Consultar(DetalleImpuestosFacturas)

                End While

            End Using
            Return item
        End Function

        Public Function GuardarFacturaElectronica(entidad As Facturas) As Long
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                    If Not IsNothing(entidad.NumeroDocumento) Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumento)
                    End If
                    If Not IsNothing(entidad.FacturaElectronica.CUFE) Then
                        conexion.AgregarParametroSQL("@par_CUFE", entidad.FacturaElectronica.CUFE)
                    End If
                    If entidad.FacturaElectronica.FechaEnvio <> Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Envio", entidad.FacturaElectronica.FechaEnvio, SqlDbType.Date)
                    End If
                    If Not IsNothing(entidad.FacturaElectronica.ID_Pruebas) Then
                        conexion.AgregarParametroSQL("@par_ID_Pruebas", entidad.FacturaElectronica.ID_Pruebas)
                    End If
                    If Not IsNothing(entidad.FacturaElectronica.ID_Habilitacion) Then
                        conexion.AgregarParametroSQL("@par_ID_Habilitacion", entidad.FacturaElectronica.ID_Habilitacion)
                    End If
                    If Not IsNothing(entidad.FacturaElectronica.ID_Emision) Then
                        conexion.AgregarParametroSQL("@par_ID_Emision", entidad.FacturaElectronica.ID_Emision)
                    End If
                    If Not IsNothing(entidad.FacturaElectronica.QRcode) Then
                        conexion.AgregarParametroSQL("@par_QR_Code", entidad.FacturaElectronica.QRcode, SqlDbType.VarBinary)
                    End If
                    If entidad.EstadoFAEL > 0 Then
                        conexion.AgregarParametroSQL("@par_Estado_Fael", SI_APLICA)
                    End If
                    If Len(entidad.FacturaElectronica.CodigoError) > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo_Error", entidad.FacturaElectronica.CodigoError)
                    End If
                    If Len(entidad.FacturaElectronica.MensajeError) > 0 Then
                        conexion.AgregarParametroSQL("@par_Mensaje_Error", entidad.FacturaElectronica.MensajeError)
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Guardar_Factura_Electronica_Saphety")

                    While resultado.Read
                        entidad.Numero = resultado.Item("ENFA_Numero")
                    End While
                    resultado.Close()
                End Using

                If entidad.Numero <> 0 Then
                    transaccion.Complete()
                End If
            End Using

            If Len(entidad.FacturaElectronica.Archivo) > 0 Then
                Dim RespuestaGuardado As Boolean = GuardarArhivoRespuestaSaphety(entidad)
            End If


            Return entidad.Numero
        End Function

        Public Function ObtenerDocumentoReporteSaphety(filtro As Facturas) As Facturas
            Dim item As New Facturas
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_documento_factura_saphety")

                While resultado.Read
                    item = New Facturas(resultado)
                End While

            End Using
            Return item
        End Function

        Public Function GuardarArhivoRespuestaSaphety(entidad As Facturas) As Boolean
            Dim guardar As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENFA_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_Tipo_Docu", entidad.FacturaElectronica.TipoArchivo)
                conexion.AgregarParametroSQL("@par_Archivo", entidad.FacturaElectronica.Archivo)
                conexion.AgregarParametroSQL("@par_Descrip", entidad.FacturaElectronica.DecripcionArchivo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_Documento_Factura")
                While resultado.Read
                    guardar = True
                End While
                resultado.Close()

            End Using
            Return guardar
        End Function
    End Class

End Namespace