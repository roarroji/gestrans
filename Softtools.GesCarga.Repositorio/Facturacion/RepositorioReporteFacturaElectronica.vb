﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Repositorio.Facturacion.FacturaElectronicaSaphety
Imports Softtools.GesCarga.Repositorio.Facturacion.FacturaElectronicaSaphety.clsFacturaElectronicaSaphety
Imports Softtools.GesCarga.Repositorio.Basico.General


Namespace Facturacion
    Public NotInheritable Class RepositorioReporteFacturaElectronica
        Inherits RepositorioBase(Of ReporteFacturaElectronica)

        Public Overrides Function Consultar(filtro As ReporteFacturaElectronica) As IEnumerable(Of ReporteFacturaElectronica)
            Dim lista As New List(Of ReporteFacturaElectronica)
            Dim item As ReporteFacturaElectronica
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.FechaInicial <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TERC_Cliente", filtro.Cliente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.FormaPago) Then
                    If filtro.FormaPago.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", filtro.FormaPago.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > Cero Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > Cero Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If

                Dim resultado As IDataReader
                If filtro.TipoDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Tipo_Documeno", filtro.TipoDocumento)

                    Select Case filtro.TipoDocumento
                        Case TIPO_DOCUMENTO_FACTURA_ELECTRONICA.FACTURA
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_facturas_pendientes_reporte_electronico")
                        Case TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_CREDITO, TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_DEBITO
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_notas_pendientes_reporte_electronico")
                    End Select
                End If

                While resultado.Read
                    item = New ReporteFacturaElectronica(resultado)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ReporteFacturaElectronica) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ReporteFacturaElectronica) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As ReporteFacturaElectronica) As ReporteFacturaElectronica
            Throw New NotImplementedException()
        End Function

        Public Function ReportarDocumentosElectronicos(entidad As ReporteFacturaElectronica) As ReporteFacturaElectronica
            Dim Reporte As ReporteFacturaElectronica = New ReporteFacturaElectronica()
            Reporte.CodigoEmpresa = entidad.CodigoEmpresa
            'Obtiene Informacion de Factura Electronica
            Dim Empresa As New Empresas()
            Empresa.Codigo = entidad.CodigoEmpresa
            Empresa = New RepositorioEmpresas().Obtener(Empresa)
            If (Empresa.EmpresaFacturaElectronica.Proveedor.Codigo > 0) Then
                Select Case Empresa.EmpresaFacturaElectronica.Proveedor.Codigo
                    Case PROVEEDOR_FACTURA_ELECTRONICA.SAPHETY
                        Dim FAELSaphety As clsFacturaElectronicaSaphety = New clsFacturaElectronicaSaphety(Empresa, URL_ASP)
                        Select Case entidad.TipoDocumento
                            Case TIPO_DOCUMENTO_FACTURA_ELECTRONICA.FACTURA
                                For i = 0 To entidad.ListaDocumentos.Count - 1
                                    If (FAELSaphety.GenerarEnvioFacturaElectronica(Empresa, entidad.ListaDocumentos(i)) = True) Then
                                        Reporte.Numero += 1
                                    End If
                                Next
                                'Envios Notas Electrónicas:
                            Case TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_CREDITO
                                For i = 0 To entidad.ListaDocumentos.Count - 1
                                    If (FAELSaphety.GenerarEnvioNotaElectronica(Empresa, entidad.ListaDocumentos(i), TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_CREDITO) = True) Then
                                        Reporte.Numero += 1
                                    End If
                                Next

                            Case TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_DEBITO
                                For i = 0 To entidad.ListaDocumentos.Count - 1
                                    If (FAELSaphety.GenerarEnvioNotaElectronica(Empresa, entidad.ListaDocumentos(i), TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_DEBITO) = True) Then
                                        Reporte.Numero += 1
                                    End If
                                Next
                        End Select
                End Select
            End If
            Return Reporte
        End Function

        Public Function ReportarDocumentosSaphetyServicioWindows(filtro As ReporteFacturaElectronica) As ReporteFacturaElectronica
            Dim item As ReporteFacturaElectronica
            Dim resultado As IDataReader
            Dim ListDocu As New List(Of Integer)
            Dim RepoFact = New RepositorioReporteFacturaElectronica()
            REM: Reporte Factura
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                item = New ReporteFacturaElectronica()
                item.CodigoEmpresa = filtro.CodigoEmpresa
                item.TipoDocumento = TIPO_DOCUMENTO_FACTURA_ELECTRONICA.FACTURA
                ListDocu = New List(Of Integer)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_facturas_pendientes_reporte_electronico_sw")
                While resultado.Read
                    ListDocu.Add(resultado.Item("Numero"))
                End While
                item.ListaDocumentos = ListDocu
                resultado.Close()
            End Using
            'Inicia el Proceso del reporte de la factura
            Dim ResultFactura As New ReporteFacturaElectronica
            If item.ListaDocumentos.Count > 0 Then
                ResultFactura = RepoFact.ReportarDocumentosElectronicos(item)
            End If

            REM: Reporte Nota Debito
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_DEBITO, SqlDbType.Int)
                item = New ReporteFacturaElectronica()
                item.CodigoEmpresa = filtro.CodigoEmpresa
                item.TipoDocumento = TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_DEBITO
                ListDocu = New List(Of Integer)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_notas_pendientes_reporte_electronico_sw")
                While resultado.Read
                    ListDocu.Add(resultado.Item("Numero"))
                End While
                item.ListaDocumentos = ListDocu
                resultado.Close()
            End Using
            Dim ResultNotaDebito As New ReporteFacturaElectronica
            If item.ListaDocumentos.Count > 0 Then
                ResultNotaDebito = RepoFact.ReportarDocumentosElectronicos(item)
            End If

            REM: Reporte Nota Credito
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_CREDITO, SqlDbType.Int)
                item = New ReporteFacturaElectronica()
                item.CodigoEmpresa = filtro.CodigoEmpresa
                item.TipoDocumento = TIPO_DOCUMENTO_FACTURA_ELECTRONICA.NOTA_CREDITO
                ListDocu = New List(Of Integer)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_notas_pendientes_reporte_electronico_sw")
                While resultado.Read
                    ListDocu.Add(resultado.Item("Numero"))
                End While
                item.ListaDocumentos = ListDocu
                resultado.Close()
            End Using
            Dim ResultNotaCredito As New ReporteFacturaElectronica
            If item.ListaDocumentos.Count > 0 Then
                ResultNotaCredito = RepoFact.ReportarDocumentosElectronicos(item)
            End If

            Dim result As ReporteFacturaElectronica = New ReporteFacturaElectronica With {
                .TotalFacturas = ResultFactura.Numero,
                .TotalNotasDebito = ResultNotaDebito.Numero,
                .TotalNotasCredito = ResultNotaCredito.Numero
            }
            Return result
        End Function
    End Class
End Namespace
