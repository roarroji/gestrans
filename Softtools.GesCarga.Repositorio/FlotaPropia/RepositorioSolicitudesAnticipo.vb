﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.FlotaPropia
Imports System.Transactions


Namespace FlotaPropia
    Public Class RepositorioSolicitudesAnticipo
        Inherits RepositorioBase(Of EncabezadoSolicitudAnticipo)

        Public Overrides Function Insertar(entidad As EncabezadoSolicitudAnticipo) As Long
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_solicitud_anticipo")
                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString
                    End While
                    resultado.Close()

                    For Each detalle In entidad.Solicitudes
                        detalle.CodigoEmpresa = entidad.CodigoEmpresa
                        detalle.Solicitud = New EncabezadoSolicitudAnticipo With {.Numero = entidad.Numero}
                        If Not (InsertarDetalle(detalle, conexion)) Then
                            transaccion.Dispose()
                            inserto = False
                            Exit For
                        End If
                    Next

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                    entidad.NumeroDocumento = Cero
                End If
            End Using

            Return entidad.NumeroDocumento
        End Function

        Public Function InsertarDetalle(entidad As DetalleSolicitudAnticipo, conexion As DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENSA_Codigo", entidad.Solicitud.Numero)
            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
            conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_TSAN_Codigo", entidad.TipoSolicitud.Codigo)
            conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Planilla)
            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
            conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
            conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.SitioCargue.Codigo)
            conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.SitioDescargue.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Sugerido", entidad.ValorSugerido, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Valor_Registrado", entidad.ValorRegistrado, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_solicitud_anticipo")

            While resultado.Read
                If Not (resultado.Item("Numero") > 0) Then
                    inserto = False
                End If
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Function EliminarDetalles(entidad As EncabezadoSolicitudAnticipo, conexion As DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENSA_Codigo", entidad.Numero)


            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_detalle_solicitud_anticipo")

            While resultado.Read
                If Not (resultado.Item("Numero") > 0) Then
                    inserto = False
                End If
            End While

            resultado.Close()

            Return inserto
        End Function


        Public Overrides Function Modificar(entidad As EncabezadoSolicitudAnticipo) As Long
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_solicitud_anticipo")
                    While resultado.Read

                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString
                    End While
                    resultado.Close()

                    EliminarDetalles(entidad, conexion)

                    For Each detalle In entidad.Solicitudes
                        detalle.CodigoEmpresa = entidad.CodigoEmpresa
                        detalle.Solicitud = New EncabezadoSolicitudAnticipo With {.Numero = entidad.Numero}
                        If Not (InsertarDetalle(detalle, conexion)) Then
                            transaccion.Dispose()
                            inserto = False
                            Exit For
                        End If
                    Next

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                    entidad.NumeroDocumento = Cero
                End If
            End Using

            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Consultar(filtro As EncabezadoSolicitudAnticipo) As IEnumerable(Of EncabezadoSolicitudAnticipo)
            Dim lista As New List(Of EncabezadoSolicitudAnticipo)
            Dim item As EncabezadoSolicitudAnticipo


            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If (filtro.Estado = 0 Or filtro.Estado = 1) Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)

                ElseIf filtro.Estado = 2 Then

                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_solicitudes_anticipo")


                While resultado.Read
                    item = New EncabezadoSolicitudAnticipo(resultado)
                    lista.Add(item)
                End While


                conexion.CloseConnection()

            End Using


            Return lista
        End Function

        Public Overrides Function Obtener(filtro As EncabezadoSolicitudAnticipo) As EncabezadoSolicitudAnticipo
            Dim item As New EncabezadoSolicitudAnticipo


            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Imprime", filtro.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_solicitud_anticipo")


                While resultado.Read
                    filtro.Numero = resultado.Item("Numero")
                    item = New EncabezadoSolicitudAnticipo(resultado)
                End While


                If item.Numero > 0 Then


                    Dim detalle As DetalleSolicitudAnticipo
                    Dim Listadetalles As New List(Of DetalleSolicitudAnticipo)
                    conexion.CleanParameters()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENSA_Numero", filtro.Numero)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_solicitud_anticipo")
                    While resultado.Read
                        detalle = New DetalleSolicitudAnticipo(resultado)
                        Listadetalles.Add(detalle)
                    End While
                    If Listadetalles.Count() > 0 Then
                        item.Solicitudes = Listadetalles
                    End If

                End If




                conexion.CloseConnection()
                resultado.Close()



            End Using

            Return item
        End Function

        Public Function Anular(entidad As EncabezadoSolicitudAnticipo) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_encabezado_solicitud_anticipo")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function ConsultarListaDetalles(filtro As DetalleSolicitudAnticipo) As IEnumerable(Of DetalleSolicitudAnticipo)
            Dim lista As New List(Of DetalleSolicitudAnticipo)
            Dim item As DetalleSolicitudAnticipo


            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Solicitud) Then
                    If filtro.Solicitud.Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_ENSA_Numero", filtro.Solicitud.Numero)
                    End If
                End If
                If Not IsNothing(filtro.Vehiculo) Then
                    If filtro.Vehiculo.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_VEHI_Codigo", filtro.Vehiculo.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.TipoSolicitud) Then
                    If filtro.TipoSolicitud.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_CATA_TSAN_Codigo", filtro.TipoSolicitud.Codigo)
                    End If
                End If
                If filtro.Planilla > 0 Then
                    conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Planilla)
                End If
                If (filtro.EstadoEncabezado = 0 Or filtro.EstadoEncabezado = 1) Then
                    conexion.AgregarParametroSQL("@par_EstadoEncabezado", filtro.EstadoEncabezado)
                    conexion.AgregarParametroSQL("@par_AnuladoEncabezado", 0)

                ElseIf filtro.EstadoEncabezado = 2 Then
                    conexion.AgregarParametroSQL("@par_AnuladoEncabezado", 1)
                End If

                If Not IsNothing(filtro.Conductor) Then
                    If filtro.Conductor.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", filtro.Conductor.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Ruta) Then
                    If filtro.Ruta.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_RUTA_Codigo", filtro.Ruta.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.SitioCargue) Then
                    If filtro.SitioCargue.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", filtro.SitioCargue.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.SitioDescargue) Then
                    If filtro.SitioDescargue.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", filtro.SitioDescargue.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Semirremolque) Then
                    If filtro.Semirremolque.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_SEMI_Codigo", filtro.Semirremolque.Codigo)
                    End If
                End If
                If filtro.EstadoERP >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado_ERP", filtro.EstadoERP)
                End If
                Dim resultado As IDataReader

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_lista_detalles_solicitudes_anticipo")


                While resultado.Read
                    item = New DetalleSolicitudAnticipo(resultado)
                    lista.Add(item)
                End While


                conexion.CloseConnection()

            End Using


            Return lista
        End Function
    End Class
End Namespace
