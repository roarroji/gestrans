﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Gesphone

Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleCheckListCargueDescargues"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleCheckListCargueDescargues
        Inherits RepositorioBase(Of DetalleCheckListCargueDescargues)

        Public Overrides Function Consultar(filtro As DetalleCheckListCargueDescargues) As IEnumerable(Of DetalleCheckListCargueDescargues)
            Dim lista As New List(Of DetalleCheckListCargueDescargues)
            Dim item As DetalleCheckListCargueDescargues

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_ECCD_Numero", filtro.NumeroDocumento)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_check_cargue_descargues]")

                While resultado.Read
                    item = New DetalleCheckListCargueDescargues(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleCheckListCargueDescargues) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleCheckListCargueDescargues) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleCheckListCargueDescargues) As DetalleCheckListCargueDescargues
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DetalleCheckListCargueDescargues) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace