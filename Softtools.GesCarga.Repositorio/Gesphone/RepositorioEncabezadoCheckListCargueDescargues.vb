﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Gesphone
Imports System.Transactions

Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="RepositorioEncabezadoCheckListCargueDescargues"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEncabezadoCheckListCargueDescargues
        Inherits RepositorioBase(Of EncabezadoCheckListCargueDescargues)

        Public Overrides Function Consultar(filtro As EncabezadoCheckListCargueDescargues) As IEnumerable(Of EncabezadoCheckListCargueDescargues)
            Dim lista As New List(Of EncabezadoCheckListCargueDescargues)
            Dim item As EncabezadoCheckListCargueDescargues

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If filtro.TipoDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Tipo_Documento", filtro.TipoDocumento)
                End If

                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroDocumento", filtro.NumeroDocumento)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.Placa) Then
                    conexion.AgregarParametroSQL("@par_Placa", filtro.Placa)
                End If
                If (filtro.FechaInicial > Date.MinValue) And (filtro.FechaFinal >= filtro.FechaInicial) Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.DateTime)
                End If

                If filtro.Estado >= 0 And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)

                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_check_cargue_descargues]")

                While resultado.Read
                    item = New EncabezadoCheckListCargueDescargues(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As EncabezadoCheckListCargueDescargues) As Long
            Dim inserto As Boolean = True
            Dim insertoDetalle As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semiremolque.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha_Hora_Llegada_Cargue", entidad.FechaLlegadaCargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Hora_Inicio_Cargue", entidad.FechaInicioCargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Hora_Fin_Cargue", entidad.FechaFinCargue, SqlDbType.DateTime)

                    If (entidad.FechaLlegadaDescargue > Date.MinValue) Then
                        conexion.AgregarParametroSQL("@par_Fecha_Hora_Llegada_Descargue", entidad.FechaLlegadaDescargue, SqlDbType.DateTime)
                    End If
                    If (entidad.FechaInicioDescargue > Date.MinValue) Then
                        conexion.AgregarParametroSQL("@par_Fecha_Hora_Inicio_Descargue", entidad.FechaInicioDescargue, SqlDbType.DateTime)
                    End If
                    If (entidad.FechaFinDescargue > Date.MinValue) Then
                        conexion.AgregarParametroSQL("@par_Fecha_Hora_Fin_Descargue", entidad.FechaFinDescargue, SqlDbType.DateTime)
                    End If

                    If (entidad.FechaLlegadaDescargue > Date.MinValue) Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia", entidad.FechaVigencia, SqlDbType.DateTime)
                    End If

                    If (entidad.FechaLlegadaDescargue > Date.MinValue) Then
                        conexion.AgregarParametroSQL("@par_Fecha_Programacion", entidad.FechaProgramacion, SqlDbType.DateTime)
                    End If
                    conexion.AgregarParametroSQL("@par_Numero_Operarios", entidad.NumeroOperarios)
                    conexion.AgregarParametroSQL("@par_Sitio_Cargue_Descargue", entidad.SitioCargueDescargue)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Responsable", entidad.Responsable.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_ECCD_Numero_Cargue", entidad.NumeroCargue)
                    conexion.AgregarParametroSQL("@par_TIDO_Documento_Soporte", entidad.TipoDocumentoSoporte)
                    conexion.AgregarParametroSQL("@par_Numero_Documento_Soporte", entidad.NumeroDocumentoSoporte)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha_Solicitud_Operacion", entidad.FechaSolicitudOperacion, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Cargue", entidad.CiudadCargue.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha_Programa_Cargue", entidad.FechaProgramacionCargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Contacto_Cliente", entidad.ContactoCliente)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_check_cargue_descargues")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While

                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        detalle.NumeroDocumento = entidad.Numero
                        detalle.TipoDocumento = entidad.TipoDocumento
                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoDetalle = False
                            inserto = False
                            Exit For
                        End If
                    Next


                End Using
                If entidad.Numero > Cero Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using
            Return entidad.Numero
        End Function
        Public Function InsertarDetalle(entidad As DetalleCheckListCargueDescargues, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ECCD_Numero", entidad.NumeroDocumento)
            contextoConexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
            contextoConexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
            contextoConexion.AgregarParametroSQL("@par_Orden_Formulario", entidad.OrdenFormulario)
            contextoConexion.AgregarParametroSQL("@par_CATA_TPCL_Codigo", entidad.TipoPregunta.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Valor", entidad.Valor)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_check_cargue_decargues]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("ID").ToString())
            End While

            resultado.Close()

            Return inserto
        End Function
        Public Overrides Function Modificar(entidad As EncabezadoCheckListCargueDescargues) As Long
            Dim inserto As Boolean = True
            Dim insertoDetalle As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semiremolque.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha_Hora_Llegada_Cargue", entidad.FechaLlegadaCargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Hora_Inicio_Cargue", entidad.FechaInicioCargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Fecha_Hora_Fin_Cargue", entidad.FechaFinCargue, SqlDbType.DateTime)

                    If (entidad.FechaLlegadaDescargue > Date.MinValue) Then
                        conexion.AgregarParametroSQL("@par_Fecha_Hora_Llegada_Descargue", entidad.FechaLlegadaDescargue, SqlDbType.DateTime)
                    End If
                    If (entidad.FechaInicioDescargue > Date.MinValue) Then
                        conexion.AgregarParametroSQL("@par_Fecha_Hora_Inicio_Descargue", entidad.FechaInicioDescargue, SqlDbType.DateTime)
                    End If
                    If (entidad.FechaFinDescargue > Date.MinValue) Then
                        conexion.AgregarParametroSQL("@par_Fecha_Hora_Fin_Descargue", entidad.FechaFinDescargue, SqlDbType.DateTime)
                    End If

                    If (entidad.FechaLlegadaDescargue > Date.MinValue) Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vigencia", entidad.FechaVigencia, SqlDbType.DateTime)
                    End If

                    If (entidad.FechaLlegadaDescargue > Date.MinValue) Then
                        conexion.AgregarParametroSQL("@par_Fecha_Programacion", entidad.FechaProgramacion, SqlDbType.DateTime)
                    End If
                    conexion.AgregarParametroSQL("@par_Numero_Operarios", entidad.NumeroOperarios)
                    conexion.AgregarParametroSQL("@par_Sitio_Cargue_Descargue", entidad.SitioCargueDescargue)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Responsable", entidad.Responsable.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)
                    conexion.AgregarParametroSQL("@par_ECCD_Numero_Cargue", entidad.NumeroCargue)
                    conexion.AgregarParametroSQL("@par_TIDO_Documento_Soporte", entidad.TipoDocumentoSoporte)
                    conexion.AgregarParametroSQL("@par_Numero_Documento_Soporte", entidad.NumeroDocumentoSoporte)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha_Solicitud_Operacion", entidad.FechaSolicitudOperacion, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_CIUD_Codigo_Cargue", entidad.CiudadCargue.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha_Programa_Cargue", entidad.FechaProgramacionCargue, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_Contacto_Cliente", entidad.ContactoCliente)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_check_cargue_descargues")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        detalle.NumeroDocumento = entidad.Numero
                        detalle.TipoDocumento = entidad.TipoDocumento

                        If Not InsertarDetalle(detalle, conexion) Then
                            insertoDetalle = False
                            inserto = False
                            Exit For
                        End If
                    Next

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using

            Return entidad.Numero
        End Function

        Public Overrides Function Obtener(filtro As EncabezadoCheckListCargueDescargues) As EncabezadoCheckListCargueDescargues
            Dim item As New EncabezadoCheckListCargueDescargues

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_encabezado_check_cargue_descargues]")

                While resultado.Read
                    item = New EncabezadoCheckListCargueDescargues(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As EncabezadoCheckListCargueDescargues) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_anular_encabezado_check_cargue_descargues]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
    End Class

End Namespace