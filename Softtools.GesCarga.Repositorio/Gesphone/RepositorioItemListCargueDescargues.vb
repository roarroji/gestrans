﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Gesphone

Namespace Gesphone

    ''' <summary>
    ''' Clase <see cref="RepositorioItemListCargueDescargues"/>
    ''' </summary>
    Public NotInheritable Class RepositorioItemListCargueDescargues
        Inherits RepositorioBase(Of ItemListCargueDescargues)

        Public Overrides Function Consultar(filtro As ItemListCargueDescargues) As IEnumerable(Of ItemListCargueDescargues)
            Dim lista As New List(Of ItemListCargueDescargues)
            Dim item As ItemListCargueDescargues

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.CodigoChecklist) Then
                    conexion.AgregarParametroSQL("@par_CLCD_Codigo", filtro.CodigoChecklist)
                End If
                If filtro.Estado >= Cero Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_item_list_cargue_descargues]")

                While resultado.Read
                    item = New ItemListCargueDescargues(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ItemListCargueDescargues) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As ItemListCargueDescargues) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As ItemListCargueDescargues) As ItemListCargueDescargues
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As ItemListCargueDescargues) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace