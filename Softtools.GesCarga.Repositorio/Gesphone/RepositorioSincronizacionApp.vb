﻿Imports System.Reflection
Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Entidades.Despachos.Planilla
Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Entidades.Gesphone
Imports Softtools.GesCarga.Repositorio.Paqueteria

Namespace Gesphone

    ''' <summary>
    ''' 
    ''' </summary>
    Public Class RepositorioSincronizacionApp
        Inherits RepositorioBase(Of DataBaseOffLine)

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="codigoEmpresa"></param>
        ''' <returns></returns>
        Public Function ObtenerEncabezadoTarifario(codigoEmpresa As Integer, codigoTercero As Integer) As SyncEncabezadoTarifarioVenta

            Dim item As New SyncEncabezadoTarifarioVenta

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", codigoTercero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_tarifario_base_ventas_sincronizacion]")

                item = DataReaderMapToObject(Of SyncEncabezadoTarifarioVenta)(resultado)

                conexion.CloseConnection()
                resultado.Close()

                If (IsNothing(item)) Then
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo", 0)

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_tarifario_base_ventas_sincronizacion]")

                    item = DataReaderMapToObject(Of SyncEncabezadoTarifarioVenta)(resultado)

                    conexion.CloseConnection()
                    resultado.Close()
                End If

                conexion.CleanParameters()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_ETCV_Numero", item.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_tarifario_paqueteria_ventas]")

                While resultado.Read
                    item.Paqueteria = New DetallePaquteriaVentas(resultado)
                End While

                conexion.CloseConnection()
                resultado.Close()

                conexion.CleanParameters()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", codigoTercero)

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_condiciones_comerciales_tercero]")
                Dim listaCondificiones As New List(Of CondicionComercialTarifaTercero)
                Dim itemCondicion As CondicionComercialTarifaTercero

                While resultado.Read
                    itemCondicion = New CondicionComercialTarifaTercero(resultado)
                    listaCondificiones.Add(itemCondicion)
                End While

                item.CondicionesComerciales = listaCondificiones

            End Using

            Return item
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="codigoEmpresa"></param>
        ''' <returns></returns>
        Public Function ObtenerDetallePlanilla(codigoEmpresa As Integer, numeroPlanilla As Integer) As List(Of SyncDetallePlanilla)

            Dim lista As New List(Of SyncDetallePlanilla)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", numeroPlanilla)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_planilla_despachos_sincronizacion]")

                lista = DataReaderMapToList(Of SyncDetallePlanilla)(resultado)

            End Using

            For Each item In lista
                Dim listaEtiquetas = New RepositorioRemesaPaqueteria().ConsultarDetalleEtiquetasPreimpresas(codigoEmpresa, item.NumeroRemesa)

                If (IsNothing(listaEtiquetas) = False And listaEtiquetas.Count > 0) Then
                    item.Etiquetas = listaEtiquetas
                End If
            Next

            Return lista
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="filtro"></param>
        ''' <returns></returns>
        Public Overloads Function Consultar(filtro As FiltroSyncApp) As IEnumerable(Of SyncDetalleTarifarioVenta)
            Dim lista As New List(Of SyncDetalleTarifarioVenta)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ETCV_Numero", filtro.NumeroTarifario)
                conexion.AgregarParametroSQL("@par_SincronizarTodo", filtro.SincronizarTodo)
                conexion.AgregarParametroSQL("@par_CIUD_Origen", filtro.CodigoCiudadOrigen)
                conexion.AgregarParametroSQL("@par_CIUD_Destino", filtro.CodigoCiudadDestino)
                conexion.AgregarParametroSQL("@par_LNTC_Codigo", filtro.CodigoLineaNegocio)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_tarifario_base_ventas_sincronizacion]")

                lista = DataReaderMapToList(Of SyncDetalleTarifarioVenta)(resultado)

            End Using

            Return lista
        End Function

        Public Overloads Function ConsultarDetalleAxiliares(codigoEmpresa As Integer, numeroPlanilla As Long) As IEnumerable(Of DetalleAuxiliaresPlanillas)

            Dim DetalleAuxuliares As DetalleAuxiliaresPlanillas
            Dim lista As New List(Of DetalleAuxiliaresPlanillas)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", numeroPlanilla)

                Dim resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_Auxiliares_planilla_guia_paqueteria]")

                While resultado.Read
                    DetalleAuxuliares = New DetalleAuxiliaresPlanillas(resultado)
                    lista.Add(DetalleAuxuliares)
                End While
            End Using

            Return lista
        End Function

        Public Overloads Function ConsultarDetalleAsignacionEtiquetas(codigoEmpresa As Short, codigoOficina As Short, codigoResponsable As Long) As IEnumerable(Of SyncAsignacionEtiquetas)

            Dim DetalleAuxiliar As SyncAsignacionEtiquetas
            Dim lista As New List(Of SyncAsignacionEtiquetas)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.CleanParameters()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo", codigoOficina)
                conexion.AgregarParametroSQL("@par_TERC_Codigo_Responsable", codigoResponsable)

                Dim resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_etiquetas_preimpresas_sincronizacion]")

                While resultado.Read
                    DetalleAuxiliar = New SyncAsignacionEtiquetas(resultado)
                    lista.Add(DetalleAuxiliar)
                End While
            End Using

            Return lista
        End Function


        Public Function ConsultarTerceros(filtro As Terceros) As IEnumerable(Of Terceros)
            Dim lista As New List(Of Terceros)
            Dim item As Terceros

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Sync = True Then
                    conexion.AgregarParametroSQL("@par_Filtro", filtro.ValorAutocomplete)
                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If
                    If Not IsNothing(filtro.CadenaPerfiles) Then
                        If Integer.Parse(filtro.CadenaPerfiles) <> 1400 Then
                            conexion.AgregarParametroSQL("@par_Perfil_Tercero", Integer.Parse(filtro.CadenaPerfiles))
                        End If
                    End If

                    Dim resultado As IDataReader

                    If Not IsNothing(filtro.Estado) Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado.Codigo)
                    End If

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_terceros_Autocomplete_gesphone")

                    While resultado.Read
                        item = New Terceros(resultado)
                        lista.Add(item)
                    End While

                    resultado.Close()

                    If Not IsNothing(filtro.CadenaPerfiles) Then
                        If Integer.Parse(filtro.CadenaPerfiles) = 1401 Then '' Clientes

                        End If
                    End If

                    If filtro.CadenaPerfiles = 1401 Then
                        For Each tercero In lista

                            conexion.CleanParameters()

                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", tercero.CodigoEmpresa)
                            conexion.AgregarParametroSQL("@par_Codigo", tercero.Codigo)
                            conexion.AgregarParametroSQL("@par_Identificacion", tercero.NumeroIdentificacion)

                            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_cliente]")
                            While resultado.Read
                                tercero.Cliente = New Cliente(resultado)
                            End While
                            resultado.Close()
                        Next
                    End If

                End If

            End Using

            Return lista
        End Function

        Public Function ConsultarGestionDocumentosTercero(CodiEmpresa As Short, CodTercero As Long) As IEnumerable(Of GestionDocumentoCliente)

            Dim lista As New List(Of GestionDocumentoCliente)
            Dim item As GestionDocumentoCliente

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
                conexion.AgregarParametroSQL("@par_TERC_Codigo", CodTercero)

                Dim resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_gestion_documentos_tercero]")

                While resultado.Read
                    item = New GestionDocumentoCliente(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista

        End Function

        Public Function ConsultarFormasPagoTercero(CodiEmpresa As Short, CodTercero As Long) As IEnumerable(Of FormasPago)

            Dim lista As New List(Of FormasPago)
            Dim item As FormasPago

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", CodTercero)

                Dim resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_Formas_Pago]")

                While resultado.Read
                    item = New FormasPago(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista

        End Function

        Public Function ConsultarCiudades(filtro As Ciudades) As IEnumerable(Of Ciudades)
            Dim lista As New List(Of Ciudades)
            Dim item As Ciudades

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.ValorAutocomplete) Or filtro.Sync = True Then

                    conexion.AgregarParametroSQL("@par_Filtro", filtro.ValorAutocomplete)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_ciudades_Autocomplete_gesphone")

                    While resultado.Read
                        item = New Ciudades(resultado)
                        lista.Add(item)
                    End While

                End If

            End Using

            Return lista
        End Function

        Public Function ConsultarSitiosCargueDescargue(filtro As SitiosCargueDescargue) As IEnumerable(Of SitiosCargueDescargue)
            Dim lista As New List(Of SitiosCargueDescargue)
            Dim item As SitiosCargueDescargue

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.ValorAutocomplete) Or filtro.Sync = True Then
                    conexion.AgregarParametroSQL("@par_Filtro", filtro.ValorAutocomplete)

                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If

                    If Not IsNothing(filtro.CodigoTipoSitio) Then
                        If filtro.CodigoTipoSitio > 0 Then
                            conexion.AgregarParametroSQL("@par_TipoSitio", filtro.CodigoTipoSitio)
                        End If
                    End If

                    If Not IsNothing(filtro.Ciudad) Then
                        If filtro.Ciudad.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Ciudad", filtro.Ciudad.Codigo)
                        End If
                    End If

                    If filtro.Estado >= 0 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_sitios_cargue_descargue_Autocomplete_gesphone")

                    While resultado.Read
                        item = New SitiosCargueDescargue(resultado)
                        lista.Add(item)
                    End While
                End If
            End Using

            Return lista
        End Function

        Public Function InsertarDetalleEtiquetasPreimpresas(registro As IEnumerable(Of SyncEtiquetaPreimpresa)) As Boolean

            Dim contador As Integer = 0

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Try
                    For Each item In registro
                        Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                            conexion.CreateConnection()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", item.CodigoEmpresa)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero", item.NumeroRemesa)
                            conexion.AgregarParametroSQL("@par_Etiqueta", item.Etiqueta)
                            conexion.AgregarParametroSQL("@par_USUA_Crea", item.CodigoUsuario)

                            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_etiquetas_remesa_paqueteria")

                            If resultado.RecordsAffected > 0 Then
                                contador += 1
                            End If

                            resultado.Close()

                        End Using
                    Next

                    If contador = registro.Count Then
                        transaccion.Complete()
                    Else
                        transaccion.Dispose()
                    End If

                Catch ex As Exception
                    transaccion.Dispose()
                    Return False
                End Try

            End Using

            Return True

        End Function

        Public Function LiberarAsignacionEtiqueta(entidad As EtiquetaPreimpresa) As Boolean
            Dim anulo As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Try
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                        conexion.CreateConnection()

                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.ENRE_Numero)
                        conexion.AgregarParametroSQL("@par_Numero_Etiqueta", entidad.Numero)

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_etiqueta_preimpresa_sincronizacion")

                        While resultado.Read
                            anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                        End While

                        If anulo Then
                            transaccion.Complete()
                        Else
                            transaccion.Dispose()
                        End If

                    End Using
                Catch ex As Exception
                    transaccion.Dispose()
                    Return False
                End Try

            End Using

            Return anulo
        End Function

        Public Function ModificarEstadoRecoleccion(codigoEmpresa As Integer, numeroRecoleccion As Integer, codigoEstado As Integer) As Long

            Dim numeroDocumento = 0

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", numeroRecoleccion)
                conexion.AgregarParametroSQL("@par_Estado", codigoEstado)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_estado_recolecciones")

                While resultado.Read
                    numeroDocumento = resultado.Item("Numero_Documento").ToString()
                End While

                resultado.Close()

            End Using

            Return numeroDocumento

        End Function

        Public Overrides Function Insertar(entidad As DataBaseOffLine) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DataBaseOffLine) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Consultar(filtro As DataBaseOffLine) As IEnumerable(Of DataBaseOffLine)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DataBaseOffLine) As DataBaseOffLine
            Throw New NotImplementedException()
        End Function

        Public Function DataReaderMapToList(Of T)(dr As IDataReader) As List(Of T)
            Dim list As New List(Of T)
            Dim obj As T

            While dr.Read()
                obj = Activator.CreateInstance(Of T)()
                For Each prop As PropertyInfo In obj.GetType().GetProperties()
                    Dim bolNotMapped = If((From x In prop.CustomAttributes Where x.AttributeType.Name = "NotMapped").Count > 0, True, False)
                    If bolNotMapped = False Then
                        If Not Object.Equals(dr(prop.Name), DBNull.Value) Then
                            prop.SetValue(obj, dr(prop.Name), Nothing)
                        End If
                    End If
                Next
                list.Add(obj)
            End While

            Return list
        End Function

        Public Function DataReaderMapToObject(Of T)(dr As IDataReader) As T
            Dim obj As T

            While dr.Read()
                obj = Activator.CreateInstance(Of T)()
                For Each prop As PropertyInfo In obj.GetType().GetProperties()

                    Dim bolNotMapped = If((From x In prop.CustomAttributes Where x.AttributeType.Name = "NotMapped").Count > 0, True, False)

                    If bolNotMapped = False Then
                        If Not Object.Equals(dr(prop.Name), DBNull.Value) Then
                            prop.SetValue(obj, dr(prop.Name), Nothing)
                        End If
                    End If


                Next
            End While

            Return obj
        End Function

        Public Function InsertarIntentoEntrega(entidad As DetalleIntentoEntregaGuia) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_numero", entidad.NumeroRemesa)
                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.CodigoUsuarioCrea)
                conexion.AgregarParametroSQL("@par_Novedad_Intento_Entrega", entidad.NovedadIntentoEntrega)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_intento_entrega_guia")

                While resultado.Read
                    entidad.ID = resultado.Item("ID").ToString()
                End While

                resultado.Close()

                If entidad.ID.Equals(Cero) Then
                    Return Cero
                End If

            End Using

            Return entidad.ID
        End Function

        Public Function ConsultarIntentosEntregaGuia(codigoEmpresa As Short, numeroRemesa As Long) As IEnumerable(Of DetalleIntentoEntregaGuia)

            Dim lista As New List(Of DetalleIntentoEntregaGuia)
            Dim item As DetalleIntentoEntregaGuia

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", numeroRemesa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_intento_entrega_guia")

                While resultado.Read
                    item = New DetalleIntentoEntregaGuia(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function


        Public Function ConsultarImagenesRemesaPaqueteria(codigoEmpresa As Short, numeroRemesa As Long) As IEnumerable(Of SyncDetallePlanillaImagenes)

            Dim lista As New List(Of SyncDetallePlanillaImagenes)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", numeroRemesa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_firma_remesa_paqueteria")
                lista.AddRange(DataReaderMapToList(Of SyncDetallePlanillaImagenes)(resultado))
            End Using

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", numeroRemesa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_imagenes_control_entrega")
                lista.AddRange(DataReaderMapToList(Of SyncDetallePlanillaImagenes)(resultado))
            End Using

            Return lista

        End Function

        Public Function GuardarEntrega(entidad As Remesas) As Long

            If Not IsNothing(entidad.Firma) Then
                Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        Dim resultado As IDataReader

                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Numero)
                        conexion.AgregarParametroSQL("@par_Firma", entidad.Firma, SqlDbType.Image)
                        conexion.AgregarParametroSQL("@par_Tipo", entidad.DevolucionRemesa)

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_registrar_fotos_entrega_devolucion_remesa_paqueteria")

                        While resultado.Read
                            entidad.Numero = resultado.Item("Codigo").ToString()
                        End While

                        resultado.Close()
                        conexion.CloseConnection()

                        If entidad.Numero.Equals(Cero) Then
                            transaccion.Dispose()
                            Return Cero
                        Else
                            transaccion.Complete()
                        End If

                    End Using
                End Using
            End If

            If entidad.Numero > 0 Then
                If Not IsNothing(entidad.Fotografia) Then

                    entidad.Fotografia.CodigoEmpresa = entidad.CodigoEmpresa
                    entidad.Fotografia.NumeroRemesa = entidad.Numero
                    entidad.Fotografia.Codigo = entidad.Numero
                    entidad.Fotografia.CodigoUsuarioCrea = entidad.UsuarioModifica.Codigo
                    If entidad.DevolucionRemesa > 0 Then
                        entidad.Fotografia.EntregaDevolucion = 1
                    Else
                        entidad.Fotografia.EntregaDevolucion = 0
                    End If

                    InsertarFoto(entidad.Fotografia)
                End If
            End If

            Return entidad.Numero

        End Function

        Public Function InsertarFoto(entidad As FotoDetalleDistribucionRemesas) As Boolean
            Dim Inserto As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.NumeroRemesa)
                conexion.AgregarParametroSQL("@par_Foto", entidad.FotoBits, SqlDbType.VarBinary)
                conexion.AgregarParametroSQL("@par_Nombre_Foto", entidad.NombreFoto)
                conexion.AgregarParametroSQL("@par_Extension_Foto", entidad.ExtensionFoto)
                conexion.AgregarParametroSQL("@par_Tipo_Foto", entidad.TipoFoto)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.CodigoUsuarioCrea)
                conexion.AgregarParametroSQL("@par_Entrega_Devolucion", entidad.EntregaDevolucion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_foto_detalle_distribucion_remesas_gesphone]")

                While resultado.Read
                    Inserto = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

                resultado.Close()
                conexion.CloseConnection()

            End Using

            Return Inserto
        End Function

    End Class

End Namespace
