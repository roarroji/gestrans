﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Mantenimiento

Namespace Mantenimiento
    Public NotInheritable Class RepositorioDetalleOrdenTrabajoMantenimiento
        Inherits RepositorioBase(Of DetalleOrdenTrabajoMantenimiento)

        Public Overrides Function Consultar(filtro As DetalleOrdenTrabajoMantenimiento) As IEnumerable(Of DetalleOrdenTrabajoMantenimiento)
            Dim lista As New List(Of DetalleOrdenTrabajoMantenimiento)
            Dim item As DetalleOrdenTrabajoMantenimiento
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_detalle_orden_trabajo_mantenimientos]")

                While resultado.Read
                    item = New DetalleOrdenTrabajoMantenimiento(resultado)
                    lista.Add(item)
                End While

            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleOrdenTrabajoMantenimiento) As Long
            Throw New NotImplementedException()
        End Function

        Public Function InsertarDetalle(entidad As DetalleOrdenTrabajoMantenimiento, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleOrdenTrabajoMantenimiento) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleOrdenTrabajoMantenimiento) As DetalleOrdenTrabajoMantenimiento
            Throw New NotImplementedException()
        End Function

        Public Function Anular(entidad As DetalleOrdenTrabajoMantenimiento) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
