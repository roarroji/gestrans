﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.Entidades.Basico.Almacen

Namespace Mantenimiento
    Public NotInheritable Class RepositorioDetallePlanEquipoMantenimiento
        Inherits RepositorioBase(Of DetallePlanEquipoMantenimiento)

        Public Overrides Function Consultar(filtro As DetallePlanEquipoMantenimiento) As IEnumerable(Of DetallePlanEquipoMantenimiento)
            Dim lista As New List(Of DetallePlanEquipoMantenimiento)
            Dim item As DetallePlanEquipoMantenimiento
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_detalle_plan_equipo_mantenimientos]")

                While resultado.Read
                    item = New DetallePlanEquipoMantenimiento(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
                For Each item In lista
                    item.Referencias = ConsultarReferenacias(item, conexion)
                Next
            End Using
            Return lista
        End Function

        Public Function ConsultarReferenacias(entidad As DetallePlanEquipoMantenimiento, ByRef contextoConexion As Conexion.DataBaseFactory) As IEnumerable(Of ReferenciaAlmacenes)
            Dim lista As New List(Of ReferenciaAlmacenes)
            Dim item As ReferenciaAlmacenes
            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_EPEM_Codigo", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_SUMA_SIMA_Codigo", entidad.SistemaMantenimiento.Codigo)
            contextoConexion.AgregarParametroSQL("@par_SUMA_Codigo", entidad.SubsistemaMantenimiento.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[sp_consultar_detalle_referencias_plan_equipo_mantenimientos]")

            While resultado.Read
                item = New ReferenciaAlmacenes(resultado)
                lista.Add(item)
            End While
            resultado.Close()

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetallePlanEquipoMantenimiento) As Long
            Throw New NotImplementedException()
        End Function

        Public Function InsertarDetalle(entidad As DetallePlanEquipoMantenimiento, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetallePlanEquipoMantenimiento) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetallePlanEquipoMantenimiento) As DetallePlanEquipoMantenimiento
            Throw New NotImplementedException()
        End Function

        Public Function Anular(entidad As DetallePlanEquipoMantenimiento) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
