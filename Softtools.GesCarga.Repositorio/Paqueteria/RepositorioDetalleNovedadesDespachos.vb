﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria

Namespace Despachos.Paqueteria

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleNovedadesDespachos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleNovedadesDespachos
        Inherits RepositorioBase(Of DetalleNovedadesDespachos)

        Public Overrides Function Consultar(filtro As DetalleNovedadesDespachos) As IEnumerable(Of DetalleNovedadesDespachos)
            Dim lista As New List(Of DetalleNovedadesDespachos)
            Dim item As DetalleNovedadesDespachos
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    If filtro.ConsultaNovedadesDespacho = 1 Then
                        If filtro.FechaInicial > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                        End If
                        If filtro.FechaFinal > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                        End If
                        If filtro.NumeroRemesa > 0 Then
                            conexion.AgregarParametroSQL("@par_Numero_Remesa", filtro.NumeroRemesa)
                        End If
                        If Not IsNothing(filtro.Novedad) Then
                            If filtro.Novedad.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_Codigo_Novedad", filtro.Novedad.Codigo)
                            End If
                        End If
                        If filtro.NumeroPlanilla > 0 Then
                            conexion.AgregarParametroSQL("@par_Numero_Planilla", filtro.NumeroPlanilla)
                        End If
                        If filtro.NumeroManifiesto > 0 Then
                            conexion.AgregarParametroSQL("@par_Numero_Manifiesto", filtro.NumeroManifiesto)
                        End If
                        If Not IsNothing(filtro.Pagina) Then
                            If filtro.Pagina > Cero Then
                                conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                            End If
                        End If
                        If Not IsNothing(filtro.RegistrosPagina) Then
                            If filtro.RegistrosPagina > Cero Then
                                conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                            End If
                        End If

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_master_novedades_planilla_despachos]")

                        While resultado.Read
                            item = New DetalleNovedadesDespachos(resultado)
                            lista.Add(item)
                        End While

                    Else
                        If filtro.NumeroPlanilla > 0 Then
                            conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                        End If
                        If filtro.NumeroRemesa > 0 Then
                            conexion.AgregarParametroSQL("@par_ENRE_Numero", filtro.NumeroRemesa)
                        End If

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_novedades_planilla_despachos]")

                        While resultado.Read
                            item = New DetalleNovedadesDespachos(resultado)
                            lista.Add(item)
                        End While

                    End If

                    conexion.CloseConnection()
                End Using
            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As DetalleNovedadesDespachos) As Long
            Dim inserto As Boolean = True
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()


                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.NumeroPlanilla)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.NumeroRemesa)
                If Not IsNothing(entidad.Novedad) Then
                    If entidad.Novedad.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_NODE_Codigo", entidad.Novedad.Codigo)
                    End If
                End If
                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                conexion.AgregarParametroSQL("@par_Valor_Compra", entidad.ValorCompra, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Valor_Venta", entidad.ValorVenta, SqlDbType.Money)
                conexion.AgregarParametroSQL("@par_Valor_Costo", entidad.ValorCosto, SqlDbType.Money)
                If Not IsNothing(entidad.Proveedor) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Proveedor", entidad.Proveedor.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_novedades_planilla_despachos")

                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()
                End While

                resultado.Close()

            End Using
            Return entidad.Codigo

        End Function
        Public Function Anular(entidad As DetalleNovedadesDespachos) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anulacion", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_anular_novedades_planilla_despachos]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Overrides Function Modificar(entidad As DetalleNovedadesDespachos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleNovedadesDespachos) As DetalleNovedadesDespachos
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace