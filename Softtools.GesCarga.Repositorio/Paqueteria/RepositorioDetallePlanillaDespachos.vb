﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria

Namespace Despachos.Paqueteria

    ''' <summary>
    ''' Clase <see cref="RepositorioDetallePlanillaDespachos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetallePlanillaDespachos
        Inherits RepositorioBase(Of DetallePlanillaDespachos)

        Public Overrides Function Consultar(filtro As DetallePlanillaDespachos) As IEnumerable(Of DetallePlanillaDespachos)
            Dim lista As New List(Of DetallePlanillaDespachos)
            Dim item As DetallePlanillaDespachos
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    If filtro.NumeroPlanilla > 0 Then
                        conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.NumeroPlanilla)
                    End If
                    If filtro.NumeroRemesa > 0 Then
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", filtro.NumeroRemesa)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_remesas_planilla_despachos]")

                    While resultado.Read
                        item = New DetallePlanillaDespachos(resultado)
                        lista.Add(item)
                    End While

                    conexion.CloseConnection()

                End Using
            End Using

            Return lista
        End Function



        Public Overrides Function Modificar(entidad As DetallePlanillaDespachos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetallePlanillaDespachos) As DetallePlanillaDespachos
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Insertar(entidad As DetallePlanillaDespachos) As Long
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace