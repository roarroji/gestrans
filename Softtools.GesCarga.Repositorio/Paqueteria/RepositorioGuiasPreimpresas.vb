﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.Despachos

Namespace Basico.Despachos

    ''' <summary>
    ''' Clase <see cref="RepositorioPrecintos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioGuiasPreimpresas
        Inherits RepositorioBase(Of GuiaPreimpresa)
        Dim NumeroPrecinto As Double
        Dim inserto As Boolean = True
        Public Overrides Function Consultar(filtro As GuiaPreimpresa) As IEnumerable(Of GuiaPreimpresa)
            Dim lista As New List(Of GuiaPreimpresa)
            Dim item As GuiaPreimpresa

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.Numero) Then
                    If filtro.Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                    End If
                End If
                If Not IsNothing(filtro.Fecha_Entrega) Then
                    Dim SinFecha As DateTime
                    SinFecha = Convert.ToDateTime("1970/01/01")
                    If filtro.Fecha_Entrega <> Date.MinValue And filtro.Fecha_Entrega > SinFecha Then
                        conexion.AgregarParametroSQL("@par_Fecha", filtro.Fecha_Entrega, SqlDbType.Date)
                    End If
                End If

                If Not IsNothing(filtro.FechaInicial) Then
                    Dim SinFecha As DateTime
                    SinFecha = Convert.ToDateTime("1970/01/01")
                    If filtro.FechaInicial <> Date.MinValue And filtro.FechaInicial > SinFecha Then
                        conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                    End If
                End If
                If Not IsNothing(filtro.FechaFinal) Then
                    Dim SinFecha As DateTime
                    SinFecha = Convert.ToDateTime("1970/01/01")
                    If filtro.FechaFinal <> Date.MinValue And filtro.FechaFinal > SinFecha Then
                        conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                    End If
                End If

                If Not IsNothing(filtro.TAPR) Then

                    If filtro.TAPR.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TAPR", filtro.TAPR.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Oficina.Codigo) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Oficina", filtro.Oficina.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.OficinaDestino) Then
                    If filtro.OficinaDestino.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Oficina_Destino", filtro.OficinaDestino.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Responsable) Then
                    If filtro.Responsable.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Responsable", filtro.Responsable.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_guias_preimpresas")

                While resultado.Read
                    item = New GuiaPreimpresa(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function InsertarPrecintos(entidad As GuiaPreimpresa) As GuiaPreimpresa
            Dim NUM As Integer = 0
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    NumeroPrecinto = 0
                    NumeroPrecinto = entidad.PrecintoInicial

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Tipo_Asignacion ", entidad.TAPR.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha_Entrega ", entidad.Fecha_Entrega, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Numero_Incial ", entidad.PrecintoInicial)
                    conexion.AgregarParametroSQL("@par_Numero_Final ", entidad.PrecintoFinal)
                    conexion.AgregarParametroSQL("@par_Usua_Crea ", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Oficina ", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Oficina_Destino ", entidad.OficinaDestino.Codigo)
                    conexion.AgregarParametroSQL("@par_Terc_Responsable ", entidad.Responsable.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_guia_preimpresa")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()
                    If entidad.Numero.Equals(Cero) Then
                        entidad.Numero = 0
                    End If

                End Using


                While NumeroPrecinto <= entidad.PrecintoFinal
                        inserto = GuiasPreimpresa(entidad, NumeroPrecinto, 0)
                        NumeroPrecinto += 1
                        NUM += 1
                        entidad.NumeroTranslados = NUM
                End While


                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = 0
                    entidad.NumeroTranslados = 0
                End If
            End Using

            Return entidad
        End Function

        Public Function ModificarPrecintos(entidad As GuiaPreimpresa) As GuiaPreimpresa
            Dim NUM As Integer = 0
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Tipo_Asignacion ", entidad.TAPR.Codigo)
                    conexion.AgregarParametroSQL("@par_Fecha_Entrega ", entidad.Fecha_Entrega, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Numero_Incial ", entidad.PrecintoInicial)
                    conexion.AgregarParametroSQL("@par_Numero_Final ", entidad.PrecintoFinal)
                    conexion.AgregarParametroSQL("@par_Usua_Crea ", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Oficina ", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Oficina_Destino ", entidad.OficinaDestino.Codigo)
                    conexion.AgregarParametroSQL("@par_Terc_Responsable ", entidad.Responsable.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_guia_preimpresa")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()
                    If entidad.Numero.Equals(Cero) Then
                        entidad.Numero = 0
                    End If

                End Using
                If entidad.Numero > 0 Then
                    For Each listadetalle In entidad.Lista
                        inserto = GuiasPreimpresa(entidad, 0, listadetalle.IdPreci)
                        NUM += 1
                        entidad.NumeroTranslados = NUM
                    Next
                End If

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.NumeroTranslados = 0
                End If
            End Using

            Return entidad
        End Function

        Public Function ActualizarTercero(entidad As GuiaPreimpresa) As Long
            Dim resultado As IDataReader
            Dim actualizo As Long = 0
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    If Not String.IsNullOrEmpty(entidad.NumerosPreimpresos) Then
                        conexion.AgregarParametroSQL("@par_Lista_Preimpresos", entidad.NumerosPreimpresos)
                    End If

                    If Not IsNothing(entidad.Responsable) Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Responsable", entidad.Responsable.Codigo)
                    End If

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_responsable_guias_preimpresas")

                    While resultado.Read
                        actualizo = resultado.Item("Numero").ToString()
                    End While
                End Using


                If actualizo > 0 Then
                    transaccion.Complete()
                Else
                    transaccion.Dispose()
                End If



            End Using
            Return actualizo
        End Function

        Public Overrides Function Obtener(filtro As GuiaPreimpresa) As GuiaPreimpresa
            Dim item As New GuiaPreimpresa

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_guia_preimpresa")

                While resultado.Read
                    item = New GuiaPreimpresa(resultado)
                End While

                If item.Numero > 0 Then
                    item.Lista = Obtener_Detalle(filtro)
                End If

            End Using

            Return item
        End Function

        Public Function Obtener_Detalle(filtro As GuiaPreimpresa) As IEnumerable(Of DetalleGuiaPreimpresa)
            Dim lista As New List(Of DetalleGuiaPreimpresa)
            Dim item As DetalleGuiaPreimpresa

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)


                If Not IsNothing(filtro.PrecintoInicial) Then
                    If filtro.PrecintoInicial > 0 Then
                        conexion.AgregarParametroSQL("@par_Precinto_Inicial", filtro.PrecintoInicial)
                        conexion.AgregarParametroSQL("@par_Precinto_Final", filtro.PrecintoFinal)
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_guia_preimpresa_seleccionados")
                        While resultado.Read
                            item = New DetalleGuiaPreimpresa(resultado)
                            lista.Add(item)
                        End While

                    Else
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_guia_preimpresa")
                        While resultado.Read
                            item = New DetalleGuiaPreimpresa(resultado)
                            lista.Add(item)
                        End While
                    End If

                End If

            End Using
            Return lista

        End Function


        Public Function Anular(entidad As GuiaPreimpresa) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_precinto", entidad.Numero)
                conexion.AgregarParametroSQL("@par_usuario_anula", entidad.Usuario_Anula.Codigo)
                conexion.AgregarParametroSQL("@par_causa_anula", entidad.Causa_Anula)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_guia_preimpresa")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function LiberarPrecinto(entidad As GuiaPreimpresa) As Boolean
            Dim anulo As Boolean = True

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_precinto", entidad.Numero)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_liberar_precinto")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function
        Public Function GuiasPreimpresa(entidad As GuiaPreimpresa, Numero As Double, id As Double) As Boolean


            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                Dim resultado As IDataReader
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_Empr_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_EAPO_Numero", entidad.Numero)

                If entidad.TAPR.Codigo = 6801 Then
                    conexion.AgregarParametroSQL("@par_Numero_Precinto", Numero)
                    conexion.AgregarParametroSQL("@par_Oficina", entidad.Oficina.Codigo)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_guia_preimpresa")
                Else
                    conexion.AgregarParametroSQL("@par_Id", id)
                    conexion.AgregarParametroSQL("@par_Oficina_Destino", entidad.OficinaDestino.Codigo)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_detalle_guia_preimpresa_traslado")
                End If


                While resultado.Read
                    entidad.TempNumero = resultado.Item("Numero").ToString()
                End While
                resultado.Close()
                If entidad.TempNumero.Equals(Cero) Then
                    inserto = False

                End If

            End Using

            Return inserto
        End Function

        Public Function ConsultarDetallePrecinto(filtro As DetalleGuiaPreimpresa) As IEnumerable(Of DetalleGuiaPreimpresa)
            Dim lista As New List(Of DetalleGuiaPreimpresa)
            Dim item As DetalleGuiaPreimpresa

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro.NumeroInicial) Then
                    If filtro.NumeroInicial > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Inicial", filtro.NumeroInicial)
                    End If
                End If
                If Not IsNothing(filtro.NumeroFinal) Then
                    If filtro.NumeroFinal > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Final", filtro.NumeroFinal)
                    End If
                End If
                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Oficina_Destino", filtro.Oficina.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.OficinaOrigen) Then
                    If filtro.OficinaOrigen.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Oficina_Origen", filtro.OficinaOrigen.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Estado) Then
                    If filtro.Estado >= 0 Then
                        conexion.AgregarParametroSQL("@par_Estado_Precinto", filtro.Estado)
                    End If
                End If
                If Not IsNothing(filtro.ENPD_Numero) Then
                    If filtro.ENPD_Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.ENPD_Numero, )
                    End If
                End If
                If Not IsNothing(filtro.ENRE_Numero) Then
                    If filtro.ENRE_Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", filtro.ENRE_Numero, )
                    End If
                End If
                If Not IsNothing(filtro.ENMD_Numero) Then
                    If filtro.ENMD_Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_ENMC_Numero", filtro.ENMD_Numero, )
                    End If
                End If
                If Not IsNothing(filtro.Responsable) Then
                    If filtro.Responsable.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Responsable", filtro.Responsable.Codigo, )
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_guias_preimpresas")

                While resultado.Read
                    item = New DetalleGuiaPreimpresa(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As GuiaPreimpresa) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As GuiaPreimpresa) As Long
            Throw New NotImplementedException()
        End Function

        Public Function ValidarPrecintoPlanillaPaqueteria(entidad As GuiaPreimpresa) As GuiaPreimpresa
            Dim Numero As Long = 0
            Dim valPrecinto As GuiaPreimpresa = New GuiaPreimpresa()
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Precinto", entidad.Numero)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_validar_asignacion_precinto_planilla_paqueteria")
                While resultado.Read
                    valPrecinto = New GuiaPreimpresa(resultado)
                End While
            End Using
            Return valPrecinto
        End Function
    End Class

End Namespace