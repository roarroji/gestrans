﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Paqueteria

Namespace Paqueteria
    Public NotInheritable Class RepositorioLegalizarRecaudoRemesas
        Inherits RepositorioBase(Of LegalizarRecaudoRemesas)

        Public Overrides Function Consultar(filtro As LegalizarRecaudoRemesas) As IEnumerable(Of LegalizarRecaudoRemesas)
            Dim lista As New List(Of LegalizarRecaudoRemesas)
            Dim item As LegalizarRecaudoRemesas
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroDocumento", filtro.NumeroDocumento)
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If
                If filtro.Estado > -1 Then
                    If filtro.Estado = 2 Then 'ANULADO
                        conexion.AgregarParametroSQL("@par_Anulado", 1)
                    Else
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                        conexion.AgregarParametroSQL("@par_Anulado", 0)
                    End If
                End If
                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                Dim resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_legalizar_recaudo_remesas_paqueteria")
                While resultado.Read
                    item = New LegalizarRecaudoRemesas(resultado)
                    lista.Add(item)
                End While
                conexion.CloseConnection()
            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As LegalizarRecaudoRemesas) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    If entidad.ValorContado > 0 Then
                        conexion.AgregarParametroSQL("@par_Valor_Contado", entidad.ValorContado, SqlDbType.Money)
                    End If
                    If entidad.ValorContraEntregas > 0 Then
                        conexion.AgregarParametroSQL("@par_Valor_Contra_Entregas", entidad.ValorContraEntregas, SqlDbType.Money)
                    End If
                    If Len(entidad.Observaciones) > 0 Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    End If
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo ", entidad.Oficina.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_legalizar_recaudo_remesas_paqueteria")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                    End While

                    resultado.Close()

                    If entidad.Numero <= 0 Then
                        transaccion.Dispose()
                    Else
                        If Not IsNothing(entidad.DetalleRemesas) Then
                            For Each lista In entidad.DetalleRemesas
                                conexion.CleanParameters()
                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                                conexion.AgregarParametroSQL("@par_ELRG_Numero", entidad.Numero)
                                conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Numero)
                                conexion.AgregarParametroSQL("@par_ENRE_Numero_Documento", lista.NumeroDocumento)
                                conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", lista.FormaPago.Codigo)
                                If Not IsNothing(lista.MetodoPago) Then
                                    If lista.MetodoPago.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_CATA_FPDC_Codigo", lista.MetodoPago.Codigo)
                                    End If
                                End If
                                If lista.FechaPago > Date.MinValue Then
                                    conexion.AgregarParametroSQL("@par_Fecha_Pago", lista.FechaPago, SqlDbType.Date)
                                End If
                                If Len(lista.DocumentoPago) > 0 Then
                                    conexion.AgregarParametroSQL("@par_Documento_Pago", lista.DocumentoPago)
                                End If
                                If Not IsNothing(lista.ResponsableLegalizarRecaudo) Then
                                    If lista.ResponsableLegalizarRecaudo.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Responsable", lista.ResponsableLegalizarRecaudo.Codigo)
                                    End If
                                End If
                                conexion.AgregarParametroSQL("@par_Valor", lista.Valor, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Observaciones", lista.Observaciones)
                                conexion.AgregarParametroSQL("@par_Legalizo", lista.Legalizo)
                                conexion.AgregarParametroSQL("@par_USUA_Modifica", entidad.UsuarioCrea.Codigo)
                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_legalizar_recaudo_remesas_paqueteria")

                                While resultado.Read
                                    If Not (resultado.Item("Numero") > 0) Then
                                        inserto = False
                                        transaccion.Dispose()
                                        Return -2
                                        Exit For
                                    End If
                                End While
                                resultado.Close()
                            Next
                        Else
                            transaccion.Dispose()
                            Return -1
                        End If
                        If inserto = True Then
                            transaccion.Complete()
                        End If
                    End If
                End Using
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Modificar(entidad As LegalizarRecaudoRemesas) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    If entidad.ValorContado > 0 Then
                        conexion.AgregarParametroSQL("@par_Valor_Contado", entidad.ValorContado, SqlDbType.Money)
                    End If
                    If entidad.ValorContraEntregas > 0 Then
                        conexion.AgregarParametroSQL("@par_Valor_Contra_Entregas", entidad.ValorContraEntregas, SqlDbType.Money)
                    End If
                    If Len(entidad.Observaciones) > 0 Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    End If
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_legalizar_recaudo_remesas_paqueteria")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                    End While

                    resultado.Close()

                    If entidad.Numero > 0 Then
                        If Not IsNothing(entidad.DetalleRemesas) Then
                            For Each lista In entidad.DetalleRemesas
                                conexion.CleanParameters()
                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                                conexion.AgregarParametroSQL("@par_ELRG_Numero", entidad.Numero)
                                conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Numero)
                                conexion.AgregarParametroSQL("@par_ENRE_Numero_Documento", lista.NumeroDocumento)
                                conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", lista.FormaPago.Codigo)
                                If Not IsNothing(lista.MetodoPago) Then
                                    If lista.MetodoPago.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_CATA_FPDC_Codigo", lista.MetodoPago.Codigo)
                                    End If
                                End If
                                If lista.FechaPago > Date.MinValue Then
                                    conexion.AgregarParametroSQL("@par_Fecha_Pago", lista.FechaPago, SqlDbType.Date)
                                End If
                                If Len(lista.DocumentoPago) > 0 Then
                                    conexion.AgregarParametroSQL("@par_Documento_Pago", lista.DocumentoPago)
                                End If
                                If Not IsNothing(lista.ResponsableLegalizarRecaudo) Then
                                    If lista.ResponsableLegalizarRecaudo.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Responsable", lista.ResponsableLegalizarRecaudo.Codigo)
                                    End If
                                End If
                                conexion.AgregarParametroSQL("@par_Valor", lista.Valor, SqlDbType.Money)
                                conexion.AgregarParametroSQL("@par_Observaciones", lista.Observaciones)
                                conexion.AgregarParametroSQL("@par_Legalizo", lista.Legalizo)
                                If Not IsNothing(lista.UsuarioModifica) Then
                                    If lista.UsuarioModifica.Codigo > 0 Then
                                        conexion.AgregarParametroSQL("@par_USUA_Modifica", lista.UsuarioModifica.Codigo)
                                    End If
                                End If
                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_legalizar_recaudo_remesas_paqueteria")

                                While resultado.Read
                                    If Not (resultado.Item("Numero") > 0) Then
                                        inserto = False
                                        transaccion.Dispose()
                                        Return -2
                                        Exit For
                                    End If
                                End While
                                resultado.Close()
                            Next
                        End If
                        If inserto = True Then
                            transaccion.Complete()
                        End If
                    Else
                        transaccion.Dispose()
                    End If
                End Using
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As LegalizarRecaudoRemesas) As LegalizarRecaudoRemesas
            Dim item As New LegalizarRecaudoRemesas
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_legalizar_recaudo_remesas_paqueteria")
                While resultado.Read
                    item = New LegalizarRecaudoRemesas(resultado)
                End While
                conexion.CloseConnection()
                resultado.Close()

                'Consulta las guias
                Dim Detalle As DetalleLegalizarRecaudoRemesas
                Dim ListaDetalle As New List(Of DetalleLegalizarRecaudoRemesas)
                conexion.CleanParameters()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ELRG_Numero", filtro.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_recaudo_remesas_paqueteria")
                While resultado.Read
                    Detalle = New DetalleLegalizarRecaudoRemesas(resultado)
                    ListaDetalle.Add(Detalle)
                End While
                item.DetalleRemesas = ListaDetalle
            End Using
            Return item
        End Function
        Public Function EliminarGuia(filtro As DetalleLegalizarRecaudoRemesas) As Boolean
            Dim Eliminar As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                Dim resultado As IDataReader
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", filtro.Numero)
                conexion.AgregarParametroSQL("@par_ELRG_Numero", filtro.NumeroLegalizarRecaudo)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_detalle_recaudo_remesas_paqueteria")
                If resultado.RecordsAffected > 0 Then
                    Eliminar = True
                End If
                resultado.Close()
            End Using

            Return Eliminar
        End Function
    End Class
End Namespace
