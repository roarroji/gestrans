﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Paqueteria
Namespace Paqueteria
    Public NotInheritable Class RepositorioLegalizarRemesasPaqueteria
        Inherits RepositorioBase(Of LegalizarRemesasPaqueteria)

        Public Overrides Function Consultar(filtro As LegalizarRemesasPaqueteria) As IEnumerable(Of LegalizarRemesasPaqueteria)
            Dim lista As New List(Of LegalizarRemesasPaqueteria)
            Dim item As LegalizarRemesasPaqueteria

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_NumeroDocumento", filtro.NumeroDocumento)
                End If
                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If
                If (filtro.Estado = 0 Or filtro.Estado = 1) And filtro.Estado <> 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)
                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If
                If Not IsNothing(filtro.UsuarioConsulta) Then
                    If filtro.UsuarioConsulta.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_Legalizacion_guias_paqueteria")
                While resultado.Read
                    item = New LegalizarRemesasPaqueteria(resultado)
                    lista.Add(item)
                End While
                conexion.CloseConnection()
            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As LegalizarRemesasPaqueteria) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoDocumento)
                    If entidad.Fecha > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    End If
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Peso", entidad.Peso, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Contado", entidad.TotalContado, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Contra_Entregas", entidad.TotalContraEntregas, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Credito", entidad.TotalCredito, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Recaudo_Tercero", entidad.TotalRecaudoTercero, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo ", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_legalizacion_guias_paqueteria")
                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()

                    If entidad.Numero > 0 Then
                        If Not IsNothing(entidad.Detalle) Then
                            For Each lista In entidad.Detalle
                                lista.CodigoEmpresa = entidad.CodigoEmpresa
                                lista.NumeroLegalizarRemesas = entidad.Numero
                                If Not InsertarDetalleRemesas(lista, conexion) Then
                                    inserto = False
                                    transaccion.Dispose()
                                    Return -2
                                    Exit For
                                End If
                            Next
                        End If
                    End If

                End Using
                If Not inserto Then
                    entidad.Numero = 0
                    entidad.NumeroDocumento = 0
                    transaccion.Dispose()
                Else
                    transaccion.Complete()
                End If
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Function InsertarDetalleRemesas(entidad As DetalleLegalizarRemesasPaqueteria, ByRef conexion As Conexion.DataBaseFactory) As Boolean
            Dim InsertaDetalle As Boolean = False
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ELGU_Numero", entidad.NumeroLegalizarRemesas)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Remesa.Remesa.Numero)
            conexion.AgregarParametroSQL("@par_Legalizo", entidad.Legalizo)
            conexion.AgregarParametroSQL("@par_CATA_NLGU_Codigo", entidad.NovedadLegalizarRemesas.Codigo)
            conexion.AgregarParametroSQL("@par_GestionDocumentos", entidad.GestionDocumentos)
            conexion.AgregarParametroSQL("@par_EntregaRecaudo", entidad.EntregaRecaudo)
            conexion.AgregarParametroSQL("@par_EntregaArchivo", entidad.EntregaArchivo)
            conexion.AgregarParametroSQL("@par_EntregaCartera", entidad.EntregaCartera)
            conexion.AgregarParametroSQL("@par_ValorRecaudoTercero", entidad.ValorRecaudoTercero, SqlDbType.Money)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_legalizacion_guias_paqueteria")

            While resultado.Read
                If resultado.Item("Numero") > 0 Then
                    InsertaDetalle = True
                End If
            End While
            resultado.Close()
            If InsertaDetalle Then
                If Not IsNothing(entidad.Remesa.GestionDocumentosRemesa) Then
                    For Each lista In entidad.Remesa.GestionDocumentosRemesa
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Remesa.Remesa.Numero)
                        conexion.AgregarParametroSQL("@par_TDGC_Codigo", lista.TipoDocumento.Codigo)
                        conexion.AgregarParametroSQL("@par_TGCD_Codigo", lista.TipoGestion.Codigo)
                        conexion.AgregarParametroSQL("@par_Legalizado", lista.Legalizado)
                        conexion.AgregarParametroSQL("@par_Observaciones", lista.Observaciones)
                        'resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_documentos_legalizacion_guias_paqueteria")
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_detalle_gestion_documentos_remesa_paqueteria")
                        If resultado.RecordsAffected <= 0 Then
                            InsertaDetalle = False
                            Exit For
                        End If
                        resultado.Close()
                    Next
                End If
            End If
            Return InsertaDetalle
        End Function

        Public Overrides Function Modificar(entidad As LegalizarRemesasPaqueteria) As Long
            Dim modifico As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    If entidad.Fecha > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    End If
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Peso", entidad.Peso, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Contado", entidad.TotalContado, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Contra_Entregas", entidad.TotalContraEntregas, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Credito", entidad.TotalCredito, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total_Recaudo_Tercero", entidad.TotalRecaudoTercero, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_legalizacion_guias_paqueteria")
                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()

                    If entidad.Numero > 0 Then
                        If Not IsNothing(entidad.Detalle) Then
                            For Each lista In entidad.Detalle
                                lista.CodigoEmpresa = entidad.CodigoEmpresa
                                lista.NumeroLegalizarRemesas = entidad.Numero
                                If Not InsertarDetalleRemesas(lista, conexion) Then
                                    modifico = False
                                    transaccion.Dispose()
                                    Return -2
                                    Exit For
                                End If
                            Next
                        End If
                    End If

                End Using
                If Not modifico Then
                    entidad.Numero = 0
                    entidad.NumeroDocumento = 0
                    transaccion.Dispose()
                Else
                    transaccion.Complete()
                End If
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As LegalizarRemesasPaqueteria) As LegalizarRemesasPaqueteria
            Dim item As New LegalizarRemesasPaqueteria
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_legalizar_guias_paqueteria")
                While resultado.Read
                    item = New LegalizarRemesasPaqueteria(resultado)
                End While

                If item.Numero > 0 Then
                    item.Detalle = Obtener_Detalle_LegalizacionGuia(filtro)
                End If
                conexion.CloseConnection()
                resultado.Close()
            End Using
            Return item
        End Function

        Public Function Obtener_Detalle_LegalizacionGuia(filtro As LegalizarRemesasPaqueteria) As IEnumerable(Of DetalleLegalizarRemesasPaqueteria)
            Dim lista As New List(Of DetalleLegalizarRemesasPaqueteria)
            Dim item As DetalleLegalizarRemesasPaqueteria

            Dim RepoRemesaPaqueteria = New RepositorioRemesaPaqueteria()

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_legalizar_guias_paqueteria")

                While resultado.Read
                    item = New DetalleLegalizarRemesasPaqueteria(resultado)
                    item.Remesa.GestionDocumentosRemesa = RepoRemesaPaqueteria.ConsultarDetalleGestionDocumentos(filtro.CodigoEmpresa, item.Remesa.Remesa.Numero)
                    lista.Add(item)
                End While
                resultado.Close()

                'Dim listaDocu As New List(Of DetalleDocumentosLegalizarRemesas)
                'Dim itemDocu As DetalleDocumentosLegalizarRemesas
                'For Each list In lista
                '    listaDocu = New List(Of DetalleDocumentosLegalizarRemesas)
                '    itemDocu = New DetalleDocumentosLegalizarRemesas
                '    conexion.CleanParameters()
                '    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                '    conexion.AgregarParametroSQL("@par_ELGU_Numero", filtro.Numero)
                '    conexion.AgregarParametroSQL("@par_ENRE_Numero", list.Remesa.Remesa.Numero)
                '    resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_documentos_legalizar_guias_paqueteria")
                '    While resultado.Read
                '        itemDocu = New DetalleDocumentosLegalizarRemesas(resultado)
                '        listaDocu.Add(itemDocu)
                '    End While
                '    list.DetalleDocumentosLegalizarRemesas = listaDocu
                '    resultado.Close()
                'Next
            End Using
            Return lista
        End Function

        Public Function EliminarRemesa(entidad As LegalizarRemesasPaqueteria) As Boolean
            Dim Eliminar As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                Dim resultado As IDataReader

                For Each lista In entidad.Detalle
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Remesa.Remesa.Numero)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_detalle_legalizar_guia_paqueteria")
                    If resultado.RecordsAffected > 0 Then
                        Eliminar = True
                    End If
                    resultado.Close()
                Next

            End Using

            Return Eliminar
        End Function

        Public Function ConsultarOtrasLegalizaciones(filtro As LegalizarRemesasPaqueteria) As IEnumerable(Of LegalizarRemesasPaqueteria)
            Dim lista As New List(Of LegalizarRemesasPaqueteria)
            Dim item As LegalizarRemesasPaqueteria

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", filtro.DetalleRemesa.Remesa.Numero)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_otras_legalizaciones_guias_paqueteria")
                While resultado.Read
                    item = New LegalizarRemesasPaqueteria(resultado)
                    lista.Add(item)
                End While
                conexion.CloseConnection()
            End Using
            Return lista
        End Function

    End Class
End Namespace

