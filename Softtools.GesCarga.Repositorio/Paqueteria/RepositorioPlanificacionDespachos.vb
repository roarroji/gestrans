﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Paqueteria


Namespace Paqueteria
    Public NotInheritable Class RepositorioPlanificacionDespachos
        Inherits RepositorioBase(Of PlanificacionDespachos)

        Public Overrides Function Consultar(filtro As PlanificacionDespachos) As IEnumerable(Of PlanificacionDespachos)
            Dim lista As New List(Of PlanificacionDespachos)
            Dim item As PlanificacionDespachos
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Planilla_Codigo", filtro.TipoDocumentoPlanilla)
                If filtro.FechaInicial <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_CLIE_Codigo", filtro.Cliente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.DocumentoCliente) Then
                    If filtro.DocumentoCliente > Cero Then
                        conexion.AgregarParametroSQL("@par_CLIE_Documento", filtro.DocumentoCliente)
                    End If
                End If

                If Not IsNothing(filtro.CiudadOrigen) Then
                    If filtro.CiudadOrigen.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_CIOR_Codigo", filtro.CiudadOrigen.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.CiudadDestino) Then
                    If filtro.CiudadDestino.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_CIDE_Codigo", filtro.CiudadDestino.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Remitente) Then
                    If filtro.Remitente.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_REMI_Codigo", filtro.Remitente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_resumen_remesas_paqueteria_planificacion")

                While resultado.Read
                    item = New PlanificacionDespachos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
                conexion.CloseConnection()
            End Using

            Return lista
        End Function

        Public Function ConsultarDetalleRemesasResumen(filtro As PlanificacionDespachos) As IEnumerable(Of PlanificacionDespachos)
            Dim lista As New List(Of PlanificacionDespachos)
            Dim item As PlanificacionDespachos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Planilla_Codigo", filtro.TipoDocumentoPlanilla)
                If filtro.FechaInicial <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_CLIE_Codigo", filtro.Cliente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.CiudadOrigen) Then
                    If filtro.CiudadOrigen.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_CIOR_Codigo", filtro.CiudadOrigen.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.CiudadDestino) Then
                    If filtro.CiudadDestino.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_CIDE_Codigo", filtro.CiudadDestino.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Remitente) Then
                    If filtro.Remitente.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_REMI_Codigo", filtro.Remitente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If
                conexion.AgregarParametroSQL("@par_Reexpedicion", filtro.Reexpedicion)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_remesas_paqueteria_planificacion")

                While resultado.Read
                    item = New PlanificacionDespachos(resultado)
                    lista.Add(item)
                End While
                resultado.Close()
                conexion.CloseConnection()
            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As PlanificacionDespachos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As PlanificacionDespachos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As PlanificacionDespachos) As PlanificacionDespachos
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace


