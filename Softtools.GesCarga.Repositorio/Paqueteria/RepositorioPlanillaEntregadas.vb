﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria

Namespace Despachos.Paqueteria

    ''' <summary>
    ''' Clase <see cref="RepositorioPlanillaEntregadas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioPlanillaEntregadas
        Inherits RepositorioBase(Of PlanillaEntregadas)
        Dim inserto As Boolean = True
        Dim modifico As Boolean = True

        Public Overrides Function Consultar(filtro As PlanillaEntregadas) As IEnumerable(Of PlanillaEntregadas)
            Dim lista As New List(Of PlanillaEntregadas)
            Dim item As PlanillaEntregadas
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                    If filtro.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                    End If
                    If filtro.FechaInicial > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_incial", filtro.FechaInicial, SqlDbType.Date)
                    End If
                    If filtro.FechaFinal > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_fin", filtro.FechaFinal, SqlDbType.Date)
                    End If
                    If Not IsNothing(filtro.Vehiculo.Placa) Then
                        If filtro.Vehiculo.Placa <> "" Then
                            conexion.AgregarParametroSQL("@par_Placa", filtro.Vehiculo.Placa)
                        End If
                    End If

                    If Not IsNothing(filtro.CodigoOficina) Then
                        If filtro.CodigoOficina > 0 Then
                            conexion.AgregarParametroSQL("@par_Oficina", filtro.CodigoOficina)
                        End If
                    End If
                    If Not IsNothing(filtro.Conductor) Then
                        If filtro.Conductor.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Codigo_Conductor", filtro.Conductor.Codigo)
                        End If
                    End If
                    If Not IsNothing(filtro.Conductor.NombreCompleto) Then
                        conexion.AgregarParametroSQL("@par_Conductor", filtro.Conductor.NombreCompleto)
                    End If
                    If filtro.Estado >= 0 And filtro.Estado <> 2 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                        conexion.AgregarParametroSQL("@par_Anulado", 0)

                    ElseIf filtro.Estado = 2 Then
                        conexion.AgregarParametroSQL("@par_Anulado", 1)
                    End If
                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If
                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > 0 Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_planilla_entregas]")

                    While resultado.Read
                        item = New PlanillaEntregadas(resultado)

                        lista.Add(item)
                    End While

                    conexion.CloseConnection()

                End Using
            End Using

            Return lista
        End Function
        Public Overrides Function Obtener(filtro As PlanillaEntregadas) As PlanillaEntregadas
            Dim item As New PlanillaEntregadas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gps_obtener_encabezado_planilla_entregado")

                While resultado.Read
                    item = New PlanillaEntregadas(resultado)
                End While

                If item.Numero > 0 Then
                    item.ListadoPlanillaEntregada = Obtener_Detalle(filtro)
                    item.ListadoAuxiliarPlanillaEntregada = Obtener_Detalle_Auxiliar(filtro)
                End If


            End Using

            Return item
        End Function

        Public Overrides Function Insertar(entidad As PlanillaEntregadas) As Long


            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo ", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha ", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo ", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor ", entidad.Conductor.Codigo)
                    conexion.AgregarParametroSQL("@par_Cantidad_Total ", entidad.CantidadTotal)
                    conexion.AgregarParametroSQL("@par_Peso_Total ", entidad.PesoTotal)
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo ", entidad.CodigoOficina)
                    If entidad.Observaciones <> String.Empty Then
                        conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones)
                    End If
                    conexion.AgregarParametroSQL("@par_Guias_Seleccionadas ", entidad.cadenaguias)
                    conexion.AgregarParametroSQL("@par_Numeracion ", entidad.Numeracion)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_planilla_entregada")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        entidad.Numero = resultado.Item("Numero").ToString()
                        entidad.MensajeSQL = resultado.Item("Mensaje").ToString()
                    End While
                    resultado.Close()
                    If entidad.NumeroDocumento.Equals(Cero) Then
                        Return Cero
                    End If

                End Using



                If entidad.Numero > 0 Then
                    For Each listadetalle In entidad.ListadoPlanillaEntregada
                        inserto = Insertar_Detalle_planilla(listadetalle, entidad.Numero)
                    Next

                    If Not IsNothing(entidad.ListadoAuxiliarPlanillaEntregada) Then
                        For Each listadetalleAuxi In entidad.ListadoAuxiliarPlanillaEntregada
                            inserto = Insertar_Detalle_auxiliar_planilla(listadetalleAuxi, entidad.Numero)
                        Next
                    End If



                End If

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.NumeroDocumento = Cero
                End If
            End Using

            Return entidad.NumeroDocumento

        End Function
        Public Function Insertar_Detalle_planilla(entidad As DetallePlanillaEntregadas, Numero As Double) As Boolean

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo ", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero_Planilla ", Numero)
                conexion.AgregarParametroSQL("@par_Numero_Guia", entidad.CodGuia)
                conexion.AgregarParametroSQL("@par_Oficina", entidad.CodOficina)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_planilla_entregada")

                While resultado.Read

                    entidad.Numero = resultado.Item("CantidadRegistros").ToString()
                End While
                resultado.Close()

                If entidad.Numero.Equals(Cero) Then
                    inserto = False

                End If

            End Using



            Return inserto

        End Function
        Public Function Liberar_Guias(entidad As DetallePlanillaEntregadas, Numero As Integer) As Boolean

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo ", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.CodGuia)
                conexion.AgregarParametroSQL("@par_ENPE_Numero", Numero)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.CodOficina)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_liberar_guias")

                While resultado.Read
                    entidad.Numero = resultado.Item("CantidadRegistros").ToString()
                End While
                resultado.Close()

                If entidad.Numero.Equals(Cero) Then
                    inserto = False

                End If

            End Using



            Return inserto

        End Function


        Public Function Insertar_Detalle_auxiliar_planilla(entidad As DetalleAuxiliaresPlanillaRecolecciones, Numero As Double) As Boolean

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo ", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero_Planilla ", Numero)
                conexion.AgregarParametroSQL("@par_Terc_Funcionario ", entidad.Tercero.Codigo)
                conexion.AgregarParametroSQL("@par_Horas_trabajadas ", entidad.Horas)
                conexion.AgregarParametroSQL("@par_Valor ", entidad.Valor)
                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_auxiliar_planilla_entregada")

                While resultado.Read

                    entidad.Numero = resultado.Item("CantidadRegistros").ToString()
                End While
                resultado.Close()

                If entidad.Numero.Equals(Cero) Then
                    inserto = False

                End If

            End Using
            Return inserto


        End Function
        Public Overrides Function Modificar(entidad As PlanillaEntregadas) As Long

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo ", entidad.CodigoEmpresa)

                    conexion.AgregarParametroSQL("@par_Numero ", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Fecha ", entidad.Fecha, SqlDbType.DateTime)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo ", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor ", entidad.Conductor.Codigo)
                    conexion.AgregarParametroSQL("@par_Cantidad_Total ", entidad.CantidadTotal)
                    conexion.AgregarParametroSQL("@par_Peso_Total ", entidad.PesoTotal)
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica ", entidad.UsuarioModifica.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo ", entidad.CodigoOficina)
                    conexion.AgregarParametroSQL("@par_Numeracion ", entidad.Numeracion)
                    conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Guias_Seleccionadas", entidad.cadenaguias)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_planilla_entregada")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        entidad.Numero = resultado.Item("Numero").ToString()
                        entidad.MensajeSQL = resultado.Item("Mensaje").ToString()
                    End While
                    resultado.Close()
                    If entidad.NumeroDocumento.Equals(Cero) Then
                        Return Cero
                    End If

                End Using



                If entidad.Numero > 0 Then
                    If Not IsNothing(entidad.ListadoGuiasNoSeleccionadas) Then
                        For Each listaguiasnoselecc In entidad.ListadoGuiasNoSeleccionadas
                            inserto = Liberar_Guias(listaguiasnoselecc, entidad.Numero)
                        Next
                    End If
                    For Each listadetalle In entidad.ListadoPlanillaEntregada
                        modifico = Insertar_Detalle_planilla(listadetalle, entidad.Numero)
                    Next
                    For Each listadetalleAuxi In entidad.ListadoAuxiliarPlanillaEntregada
                        modifico = Modificar_Detalle_auxiliar_planilla(listadetalleAuxi, entidad.Numero)
                    Next

                End If

                If modifico Then
                    transaccion.Complete()
                Else
                    transaccion.Dispose()
                    entidad.NumeroDocumento = Cero
                End If
            End Using

            Return entidad.NumeroDocumento

        End Function


        Public Function Modificar_Detalle_auxiliar_planilla(entidad As DetalleAuxiliaresPlanillaRecolecciones, Numero As Double) As Boolean

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo ", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero_Planilla ", Numero)
                conexion.AgregarParametroSQL("@par_Terc_Funcionario ", entidad.Tercero.Codigo)
                conexion.AgregarParametroSQL("@par_Horas_trabajadas ", entidad.Horas)

                conexion.AgregarParametroSQL("@par_Valor ", entidad.Valor)
                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_detalle_auxiliar_planilla_entregada")

                While resultado.Read

                    entidad.Numero = resultado.Item("ENPE_Numero").ToString()
                End While
                resultado.Close()

                If entidad.Numero.Equals(Cero) Then
                    modifico = False

                End If

            End Using

            Return modifico

        End Function

        Public Function Anular(entidad As PlanillaEntregadas) As Boolean

            Dim anulo As Boolean = False

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo ", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo_Usuario ", entidad.UsuarioModifica.Codigo)
                    conexion.AgregarParametroSQL("@par_Numero_Planilla ", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_observaciones", entidad.Observaciones)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_planilla_entregas")

                    While resultado.Read
                        anulo = IIf(Convert.ToInt64(resultado.Item("Numero_Documento")) > Cero, True, False)
                    End While


                End Using
                If anulo Then
                    transaccion.Complete()
                Else
                    transaccion.Dispose()
                End If
            End Using
            Return anulo
        End Function

        Public Function AnularAuxiliar(entidad As PlanillaEntregadas) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_Codigo_Auxiliar", entidad.Codigo)



                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_borrar_auxiliar_planilla_despacho")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function Obtener_Detalle(filtro As PlanillaEntregadas) As IEnumerable(Of DetallePlanillaEntregadas)
            Dim lista As New List(Of DetallePlanillaEntregadas)
            Dim item As DetallePlanillaEntregadas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_planilla_entregas")

                While resultado.Read
                    item = New DetallePlanillaEntregadas(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista

        End Function
        Public Function Obtener_Detalle_Auxiliar(filtro As PlanillaEntregadas) As IEnumerable(Of DetalleAuxiliaresPlanillaRecolecciones)
            Dim lista As New List(Of DetalleAuxiliaresPlanillaRecolecciones)
            Dim item As DetalleAuxiliaresPlanillaRecolecciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_auxiliar_planilla_entregas")

                While resultado.Read
                    item = New DetalleAuxiliaresPlanillaRecolecciones(resultado)
                    lista.Add(item)
                End While
            End Using
            Return lista

        End Function

    End Class

End Namespace