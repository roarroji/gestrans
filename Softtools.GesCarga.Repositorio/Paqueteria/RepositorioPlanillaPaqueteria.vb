﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Entidades.Despachos.Planilla
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Entidades.Despachos.Masivo
Imports Softtools.GesCarga.Entidades.ControlViajes
Imports Softtools.GesCarga.Repositorio.ControlViajes
Imports Softtools.GesCarga.Entidades.Despachos
Imports Softtools.GesCarga.Repositorio.Despachos

Namespace Paqueteria
    Public NotInheritable Class RepositorioPlanillaPaqueteria
        Inherits RepositorioBase(Of PlanillaPaqueteria)
        Dim inserto As Boolean = True
        Dim modifico As Boolean = True

        Public Overrides Function Consultar(filtro As PlanillaPaqueteria) As IEnumerable(Of PlanillaPaqueteria)
            Dim lista As New List(Of PlanillaPaqueteria)
            Dim item As PlanillaPaqueteria
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    Dim resultado As IDataReader
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                    If filtro.ConsultaPLanillasenRuta > 0 Then
                        'Pendiente de revision
                        If Not IsNothing(filtro.Planilla.Conductor) Then
                            If filtro.Planilla.Conductor.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_Conductor_Codigo", filtro.Planilla.Conductor.Codigo)
                            End If
                        End If
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_planillas_paqueteria_en_ruta")
                    Else
                        If Not IsNothing(filtro.Planilla) Then
                            If filtro.Planilla.Numero > 0 Then
                                conexion.AgregarParametroSQL("@par_Numero", filtro.Planilla.Numero)
                            End If
                            If filtro.FechaInicial > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                            End If
                            If filtro.FechaFinal > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                            End If
                            If Not IsNothing(filtro.Planilla.Ruta) Then
                                If filtro.Planilla.Ruta.Codigo > 0 Then
                                    conexion.AgregarParametroSQL("@par_Ruta_Codigo", filtro.Planilla.Ruta.Codigo)
                                End If
                            End If
                            If Not IsNothing(filtro.Planilla.Vehiculo) Then
                                If Len(filtro.Planilla.Vehiculo.CodigoAlterno) > 0 Then
                                    conexion.AgregarParametroSQL("@par_Vehiculo_Codigo_Alterno", filtro.Planilla.Vehiculo.CodigoAlterno)
                                End If
                                If filtro.Planilla.Vehiculo.Codigo > 0 Then
                                    conexion.AgregarParametroSQL("@par_Vehiculo_Codigo", filtro.Planilla.Vehiculo.Codigo)
                                End If
                            End If
                            If Not IsNothing(filtro.Planilla.Tenedor) Then
                                If filtro.Planilla.Tenedor.Codigo > 0 Then
                                    conexion.AgregarParametroSQL("@par_Tenedor_Codigo", filtro.Planilla.Tenedor.Codigo)
                                End If
                            End If
                            If Not IsNothing(filtro.Planilla.Conductor) Then
                                If filtro.Planilla.Conductor.Codigo > 0 Then
                                    conexion.AgregarParametroSQL("@par_Conductor_Codigo", filtro.Planilla.Conductor.Codigo)
                                End If
                            End If

                            If filtro.Estado > -1 Then
                                If filtro.Estado = 2 Then 'ANULADO
                                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                                Else
                                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                                    conexion.AgregarParametroSQL("@par_Anulado", 0)
                                End If
                            End If

                            If filtro.Planilla.NumeroManifiesto > 0 Then
                                conexion.AgregarParametroSQL("@par_Numero_Manifiesto", filtro.Planilla.NumeroManifiesto)
                            End If

                        End If
                        If Not IsNothing(filtro.Oficina) Then
                            If filtro.Oficina.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                            End If
                        End If

                        If Not IsNothing(filtro.Pagina) Then
                            If filtro.Pagina > 0 Then
                                conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                            End If
                        End If
                        If Not IsNothing(filtro.RegistrosPagina) Then
                            If filtro.RegistrosPagina > 0 Then
                                conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                            End If
                        End If
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_planilla_paqueteria")
                    End If
                    While resultado.Read
                        item = New PlanillaPaqueteria(resultado)
                        lista.Add(item)
                    End While
                    conexion.CloseConnection()
                End Using
            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As PlanillaPaqueteria) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoDocumento)
                    'Inserta la Planilla'
                    If Not IsNothing(entidad.Planilla) Then
                        If entidad.Planilla.FechaHoraSalida > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Hora_Salida ", entidad.Planilla.FechaHoraSalida, SqlDbType.DateTime)
                        End If
                        If entidad.Planilla.Fecha > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha", entidad.Planilla.Fecha, SqlDbType.Date)
                        End If
                        If Not IsNothing(entidad.Planilla.Ruta) Then
                            conexion.AgregarParametroSQL("@par_RUTA_Codigo ", entidad.Planilla.Ruta.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.Vehiculo) Then
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo ", entidad.Planilla.Vehiculo.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.Tenedor) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Tenedor ", entidad.Planilla.Tenedor.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.Conductor) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor ", entidad.Planilla.Conductor.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.Semirremolque) Then
                            conexion.AgregarParametroSQL("@par_SEMI_Codigo ", entidad.Planilla.Semirremolque.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Cantidad ", entidad.Planilla.Cantidad, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Peso ", entidad.Planilla.Peso, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador ", entidad.Planilla.ValorFleteTransportador, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Anticipo ", entidad.Planilla.ValorAnticipo, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Impuestos", entidad.Planilla.ValorImpuestos, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Pagar_Transportador ", entidad.Planilla.ValorPagarTransportador, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Flete_Cliente ", entidad.Planilla.ValorFleteCliente, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones)
                        conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                        If Not IsNothing(entidad.Planilla.TarifaTransportes) Then
                            conexion.AgregarParametroSQL("@par_TATC_Codigo_Compra", entidad.Planilla.TarifaTransportes.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.TipoTarifaTransportes) Then
                            conexion.AgregarParametroSQL("@par_TTTC_Codigo_Compra", entidad.Planilla.TipoTarifaTransportes.Codigo)
                        End If
                        If entidad.NumeroPrecintoPaqueteria > 0 Then
                            conexion.AgregarParametroSQL("@par_Precinto", entidad.NumeroPrecintoPaqueteria)
                        End If

                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo ", entidad.Oficina.Codigo)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero_Documento_Masivo", entidad.Planilla.RemesaPadre) ''Documento
                        conexion.AgregarParametroSQL("@par_ENRE_Numero_Masivo", entidad.Planilla.RemesaMasivo) ''Codigo
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_planilla_paqueteria_v2")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While

                    resultado.Close()

                    If entidad.Numero > 0 Then
                        For Each lista In entidad.Planilla.Detalles
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                            conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero)
                            conexion.AgregarParametroSQL("@par_TIDO_Codigo_Planilla", entidad.TipoDocumento)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Remesa.Numero)

                            conexion.AgregarParametroSQL("@par_Numero_Documento_Remesa", lista.Remesa.NumeroDocumento)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero_Documento_Masivo", entidad.Planilla.RemesaPadre)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero_Masivo", entidad.Planilla.RemesaMasivo)
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Planilla.Vehiculo.Codigo)
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Planilla.Conductor.Codigo)
                            conexion.AgregarParametroSQL("@par_Estado_Planilla", entidad.Estado)
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_remesas_planilla_paqueteria")



                            While resultado.Read
                                If Not (resultado.Item("Numero") > 0) Then
                                    inserto = False
                                    transaccion.Dispose()
                                    Return -2
                                    Exit For
                                End If
                            End While
                            resultado.Close()
                        Next

                        If inserto Then
                            If Not IsNothing(entidad.ListadoPlanillaRecolecciones) Then
                                For Each ListadoPlanillaRecolecciones In entidad.ListadoPlanillaRecolecciones

                                    ListadoPlanillaRecolecciones.CodigoEmpresa = entidad.CodigoEmpresa
                                    ListadoPlanillaRecolecciones.NumeroPlanilla = entidad.Numero

                                    If Not InsertarListaPlanillaRecolecciones(ListadoPlanillaRecolecciones, conexion) Then
                                        Exit For
                                    End If
                                Next
                            End If

                            If Not IsNothing(entidad.Planilla.DetallesAuxiliares) Then
                                If entidad.Planilla.DetallesAuxiliares.Count > 0 Then
                                    For Each lista In entidad.Planilla.DetallesAuxiliares
                                        conexion.CleanParameters()
                                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Funcionario", lista.Funcionario.Codigo) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_Numero_Horas_Trabajadas", lista.NumeroHorasTrabajadas, SqlDbType.Decimal) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_Valor", lista.Valor, SqlDbType.Money) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_Observaciones", lista.Observaciones) 'SMALLINT,
                                        conexion.AgregarParametroSQL("@par_Aforador", lista.Aforador) 'SMALLINT,
                                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_auxiliares_planilla_despachos")
                                        While resultado.Read
                                            If Not (resultado.Item("Numero") > 0) Then
                                                inserto = False
                                                Exit For
                                            End If
                                        End While

                                        resultado.Close()
                                    Next
                                End If
                            End If
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                            conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_totales_fletes_auxiliares")
                            resultado.Close()

                            If entidad.Estado = 1 And entidad.Planilla.ValorAnticipo > 0 Then
                                entidad.CuentaPorPagar.CodigoDocumentoOrigen = entidad.Numero
                                entidad.CuentaPorPagar.TipoDocumentoOrigen = entidad.TipoDocumento
                                entidad.CuentaPorPagar.Numero = entidad.NumeroDocumento
                                entidad.CuentaPorPagar.Observaciones = "PAGO ANTICIPO PLANILLA No. " + entidad.NumeroDocumento.ToString() + " VALOR $ " + entidad.Planilla.ValorAnticipo.ToString()
                                If entidad.Planilla.AutorizacionAnticipo > 0 Then
                                    Dim Autorizacion = New Autorizaciones
                                    Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                                    Autorizacion.PlanillaDespacho = New PlanillaDespachos With {.Codigo = entidad.Numero}
                                    Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18402}
                                    Autorizacion.CuentaPorPagar = entidad.CuentaPorPagar
                                    Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                                    Dim repAutorizacion = New RepositorioAutorizaciones()
                                    inserto = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)
                                Else
                                    inserto = Generar_CxP_Anticipo(entidad.CuentaPorPagar, conexion)
                                End If
                            End If
                            If entidad.Estado = 1 And inserto Then
                                If entidad.Planilla.AutorizacionFlete > 0 Then
                                    Dim Autorizacion = New Autorizaciones
                                    Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                                    Autorizacion.PlanillaDespacho = New PlanillaDespachos With {.Codigo = entidad.Numero}
                                    Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18406}
                                    Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                                    Autorizacion.ValorFlete = entidad.Planilla.ValorFleteAutorizacion
                                    Autorizacion.ValorFleteReal = entidad.Planilla.ValorFleteTransportador
                                    Autorizacion.Observaciones = "Solicitud cambio flete en planilla " + entidad.NumeroDocumento.ToString() + " Flete estipulado: " + entidad.Planilla.strValorFleteTransportador + " Flete Solicitado: " + entidad.Planilla.strValorFleteAutorizacion
                                    Dim repAutorizacion = New RepositorioAutorizaciones()
                                    inserto = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)
                                    'Else
                                    '    inserto = Generar_CxP_Anticipo(entidad.CuentaPorPagar, conexion)
                                End If
                            End If

                            If Not IsNothing(entidad.Planilla.DetalleImpuesto) Then
                                If entidad.Planilla.DetalleImpuesto.Count > 0 Then
                                    For Each lista In entidad.Planilla.DetalleImpuesto
                                        conexion.CleanParameters()
                                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero)

                                        conexion.AgregarParametroSQL("@par_ENIM_Codigo", lista.CodigoImpuesto)
                                        conexion.AgregarParametroSQL("@par_Valor_Tarifa", lista.ValorTarifa, SqlDbType.Decimal)
                                        conexion.AgregarParametroSQL("@par_Valor_Base", lista.ValorBase, SqlDbType.Money)
                                        conexion.AgregarParametroSQL("@par_Valor_Impuesto", lista.ValorImpuesto, SqlDbType.Money)

                                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_impuestos_planilla_despachos")

                                        While resultado.Read
                                            If Not (resultado.Item("Numero") > 0) Then
                                                inserto = False
                                                Exit For
                                            End If
                                        End While

                                        resultado.Close()
                                    Next
                                End If
                            End If
                        Else
                            transaccion.Dispose()
                        End If
                    Else
                        transaccion.Dispose()
                    End If
                End Using
                If inserto Then
                    If entidad.Estado > 0 And Not IsNothing(entidad.Manifiesto) Then
                        entidad.Manifiesto.NumeroPlanilla = entidad.Numero
                        entidad.Manifiesto.NumeroDocumentoPlanilla = entidad.NumeroDocumento
                        entidad.Manifiesto.Numero = New RepositorioManifiesto().Insertar(entidad.Manifiesto)
                        If entidad.Manifiesto.Numero = 0 Then
                            transaccion.Dispose()
                        Else
                            transaccion.Complete()
                        End If
                    Else
                        transaccion.Complete()
                    End If
                End If
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Function InsertarListaPlanillaRecolecciones(entidad As DetallePlanillaRecolecciones, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Numero", entidad.NumeroPlanilla)
            conexion.AgregarParametroSQL("@par_EREC_Numero", entidad.Numero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_recolecciones_planilla_paqueteria")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function Generar_CxP_Anticipo(entidad As EncabezadoDocumentoCuentas, conexion As DataBaseFactory) As Boolean
            Generar_CxP_Anticipo = False

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
            conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
            conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
            conexion.AgregarParametroSQL("@par_Codigo_Documento_Origen", entidad.CodigoDocumentoOrigen)
            conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.Numero)
            conexion.AgregarParametroSQL("@par_Fecha_Documento_Origen", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Fecha_Vence_Documento_Origen", DateAdd(DateInterval.Day, 30, entidad.Fecha), SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPuc.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Total", entidad.ValorTotal)
            conexion.AgregarParametroSQL("@par_Abono", entidad.Abono)
            conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo)
            conexion.AgregarParametroSQL("@par_Fecha_Cancelacion_Pago", entidad.FechaCancelacionPago, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Aprobado", entidad.Aprobado)
            conexion.AgregarParametroSQL("@par_AplicaPSL", entidad.AplicaPSL)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo_Origen", entidad.TipoDocumentoOrigen)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Aprobo", entidad.UsuarioAprobo.Codigo)
            conexion.AgregarParametroSQL("@par_Fecha_Aprobo", entidad.FechaAprobo, SqlDbType.DateTime)
            conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_documento_cuentas")

            While resultado.Read
                Generar_CxP_Anticipo = IIf(resultado.Item("Codigo") > 0, True, False)
            End While
            resultado.Close()

            Return Generar_CxP_Anticipo
        End Function

        Public Overrides Function Modificar(entidad As PlanillaPaqueteria) As Long
            Dim modifico As Boolean = False
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                    conexion.AgregarParametroSQL("@par_Numero ", entidad.Planilla.Numero)
                    'Inserta la Planilla'
                    If Not IsNothing(entidad.Planilla) Then
                        If entidad.Planilla.FechaHoraSalida > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Hora_Salida ", entidad.Planilla.FechaHoraSalida, SqlDbType.DateTime)
                        End If
                        If entidad.Planilla.Fecha > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha", entidad.Planilla.Fecha, SqlDbType.Date)
                        End If
                        If Not IsNothing(entidad.Planilla.Ruta) Then
                            conexion.AgregarParametroSQL("@par_RUTA_Codigo ", entidad.Planilla.Ruta.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.Vehiculo) Then
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo ", entidad.Planilla.Vehiculo.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.Tenedor) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Tenedor ", entidad.Planilla.Tenedor.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.Conductor) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor ", entidad.Planilla.Conductor.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.Semirremolque) Then
                            conexion.AgregarParametroSQL("@par_SEMI_Codigo ", entidad.Planilla.Semirremolque.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Cantidad ", entidad.Planilla.Cantidad, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Peso ", entidad.Planilla.Peso, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Flete_Transportador ", entidad.Planilla.ValorFleteTransportador, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Anticipo ", entidad.Planilla.ValorAnticipo, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Impuestos", entidad.Planilla.ValorImpuestos, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Pagar_Transportador ", entidad.Planilla.ValorPagarTransportador, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Flete_Cliente ", entidad.Planilla.ValorFleteCliente, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones)
                        conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                        If Not IsNothing(entidad.Planilla.TarifaTransportes) Then
                            conexion.AgregarParametroSQL("@par_TATC_Codigo ", entidad.Planilla.TarifaTransportes.Codigo)
                        End If
                        If Not IsNothing(entidad.Planilla.TipoTarifaTransportes) Then
                            conexion.AgregarParametroSQL("@par_TTTC_Codigo ", entidad.Planilla.TipoTarifaTransportes.Codigo)
                        End If
                        If entidad.NumeroPrecintoPaqueteria > 0 Then
                            conexion.AgregarParametroSQL("@par_Precinto", entidad.NumeroPrecintoPaqueteria)
                        End If

                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero_Documento_Masivo", entidad.Planilla.RemesaPadre) ''Documento
                        conexion.AgregarParametroSQL("@par_ENRE_Numero_Masivo", entidad.Planilla.RemesaMasivo) ''Codigo
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_planilla_paqueteria")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero").ToString()
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        modifico = True
                    End While
                    resultado.Close()

                    If modifico Then
                        For Each lista In entidad.Planilla.Detalles
                            conexion.CleanParameters()
                            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                            conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Planilla.Numero)
                            conexion.AgregarParametroSQL("@par_TIDO_Codigo_Planilla", entidad.TipoDocumento)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Remesa.Numero)

                            conexion.AgregarParametroSQL("@par_Numero_Documento_Remesa", lista.Remesa.NumeroDocumento)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero_Documento_Masivo", entidad.Planilla.RemesaPadre)
                            conexion.AgregarParametroSQL("@par_ENRE_Numero_Masivo", entidad.Planilla.RemesaMasivo)
                            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Planilla.Vehiculo.Codigo)
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Planilla.Conductor.Codigo)
                            conexion.AgregarParametroSQL("@par_Estado_Planilla", entidad.Estado)
                            resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_remesas_planilla_paqueteria")

                            While resultado.Read
                                If Not (resultado.Item("Numero") > 0) Then
                                    modifico = False
                                    Exit For
                                End If
                            End While
                            resultado.Close()
                        Next
                        If Not IsNothing(entidad.ListadoPlanillaRecolecciones) Then
                            For Each ListadoPlanillaRecolecciones In entidad.ListadoPlanillaRecolecciones
                                ListadoPlanillaRecolecciones.CodigoEmpresa = entidad.CodigoEmpresa
                                ListadoPlanillaRecolecciones.NumeroPlanilla = entidad.Numero
                                If Not InsertarListaPlanillaRecolecciones(ListadoPlanillaRecolecciones, conexion) Then
                                    modifico = False
                                    Exit For
                                End If
                            Next
                        End If

                        If Not IsNothing(entidad.Planilla.DetallesAuxiliares) Then
                            If entidad.Planilla.DetallesAuxiliares.Count > 0 Then
                                For Each lista In entidad.Planilla.DetallesAuxiliares
                                    conexion.CleanParameters()
                                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Planilla.Numero) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Funcionario", lista.Funcionario.Codigo) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_Numero_Horas_Trabajadas", lista.NumeroHorasTrabajadas) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_Valor", lista.Valor, SqlDbType.Decimal) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_Observaciones", lista.Observaciones) 'SMALLINT,
                                    conexion.AgregarParametroSQL("@par_Aforador", lista.Aforador) 'SMALLINT,
                                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_auxiliares_planilla_despachos")
                                    While resultado.Read
                                        If Not (resultado.Item("Numero") > 0) Then
                                            modifico = False
                                            Exit For
                                        End If
                                    End While

                                    resultado.Close()
                                Next
                            End If
                        End If
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                        conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Numero) 'SMALLINT,
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_totales_fletes_auxiliares")
                        resultado.Close()
                        If Not IsNothing(entidad.Planilla.DetalleImpuesto) Then
                            If entidad.Planilla.DetalleImpuesto.Count > 0 Then
                                For Each lista In entidad.Planilla.DetalleImpuesto
                                    conexion.CleanParameters()
                                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                                    conexion.AgregarParametroSQL("@par_ENPD_Numero", entidad.Planilla.Numero)

                                    conexion.AgregarParametroSQL("@par_ENIM_Codigo", lista.CodigoImpuesto)
                                    conexion.AgregarParametroSQL("@par_Valor_Tarifa", lista.ValorTarifa, SqlDbType.Decimal)
                                    conexion.AgregarParametroSQL("@par_Valor_Base", lista.ValorBase, SqlDbType.Money)
                                    conexion.AgregarParametroSQL("@par_Valor_Impuesto", lista.ValorImpuesto, SqlDbType.Money)

                                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_impuestos_planilla_despachos")

                                    While resultado.Read
                                        If Not (resultado.Item("Numero") > 0) Then
                                            modifico = False
                                            Exit For
                                        End If
                                    End While

                                    resultado.Close()
                                Next
                            End If
                        End If
                        If entidad.Estado = 1 And entidad.Planilla.ValorAnticipo > 0 Then
                            entidad.CuentaPorPagar.Numero = entidad.Planilla.NumeroDocumento
                            entidad.CuentaPorPagar.CodigoDocumentoOrigen = entidad.Planilla.Numero
                            entidad.CuentaPorPagar.TipoDocumentoOrigen = entidad.TipoDocumento
                            entidad.CuentaPorPagar.Observaciones = "PAGO ANTICIPO PLANILLA No. " + entidad.Planilla.NumeroDocumento.ToString() + " VALOR $ " + entidad.Planilla.ValorAnticipo.ToString()
                            If entidad.Planilla.AutorizacionAnticipo > 0 Then
                                Dim Autorizacion = New Autorizaciones
                                Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                                Autorizacion.PlanillaDespacho = New PlanillaDespachos With {.Codigo = entidad.Planilla.Numero}
                                Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = 18402}
                                Autorizacion.CuentaPorPagar = entidad.CuentaPorPagar
                                Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                                Dim repAutorizacion = New RepositorioAutorizaciones()
                                modifico = If(repAutorizacion.InsertarAutorizacion(Autorizacion, conexion) > 0, True, False)
                            Else
                                modifico = Generar_CxP_Anticipo(entidad.CuentaPorPagar, conexion)
                            End If
                        End If
                    Else
                        transaccion.Dispose()
                    End If
                End Using
                If modifico Then
                    If entidad.Estado > 0 And Not IsNothing(entidad.Manifiesto) Then
                        entidad.Manifiesto.NumeroPlanilla = entidad.Numero
                        entidad.Manifiesto.NumeroDocumentoPlanilla = entidad.NumeroDocumento
                        entidad.Manifiesto.Numero = New RepositorioManifiesto().Insertar(entidad.Manifiesto)
                        If entidad.Manifiesto.Numero = 0 Then
                            transaccion.Dispose()
                        Else
                            transaccion.Complete()
                        End If
                    Else
                        transaccion.Complete()
                    End If
                End If
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As PlanillaPaqueteria) As PlanillaPaqueteria
            Dim item As New PlanillaPaqueteria
            Dim Doc As New Documentos
            Const TIPO_CONSULTA_VERIFICACION_PROCESO_CARGUE = 1

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                If filtro.TipoDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                End If
                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_planilla_paqueteria")

                While resultado.Read
                    item = New PlanillaPaqueteria(resultado)
                End While
                conexion.CloseConnection()
                resultado.Close()

                'Consulta las guias
                Dim Detalleguia As RemesaPaqueteria
                Dim Detalle As DetallePlanillas
                Dim ListaDetalle As New List(Of DetallePlanillas)
                conexion.CleanParameters()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)
                If filtro.TipoConsulta = TIPO_CONSULTA_VERIFICACION_PROCESO_CARGUE Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_guias_paqueteria_proceso_cargue")
                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_guias_paqueteria_planilla")
                End If
                '
                While resultado.Read
                    Detalleguia = New RemesaPaqueteria(resultado)
                    Detalle = New DetallePlanillas With {.Remesa = Detalleguia.Remesa, .RemesaPaqueteria = Detalleguia}
                    Detalle.Remesa.PesoVolumetricoCliente = Detalleguia.PesoVolumetrico
                    ListaDetalle.Add(Detalle)
                End While

                item.Planilla.Detalles = ListaDetalle
                conexion.CloseConnection()
                resultado.Close()

                'Recolecciones
                Dim lista As New List(Of DetallePlanillaRecolecciones)
                Dim Recoleccion As DetallePlanillaRecolecciones
                conexion.CleanParameters()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPR_Codigo", filtro.Numero)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_recolecciones_planilla_paqueteria")

                While resultado.Read
                    Recoleccion = New DetallePlanillaRecolecciones(resultado)
                    lista.Add(Recoleccion)
                End While
                item.ListadoPlanillaRecolecciones = lista
                conexion.CloseConnection()
                resultado.Close()

                'Consulta detalles auxiliares
                Dim DetalleAuxuliares As DetalleAuxiliaresPlanillas
                Dim ListaDetalleAuxiliares As New List(Of DetalleAuxiliaresPlanillas)
                conexion.CleanParameters()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_Auxiliares_planilla_guia_paqueteria]")


                While resultado.Read
                    DetalleAuxuliares = New DetalleAuxiliaresPlanillas(resultado)
                    ListaDetalleAuxiliares.Add(DetalleAuxuliares)
                End While
                If ListaDetalleAuxiliares.Count() > 0 Then
                    item.Planilla.DetallesAuxiliares = ListaDetalleAuxiliares
                End If

                ''Consulta detalles Impuestos
                Dim DetalleImpuestos As DetalleImpuestosPlanillas
                Dim ListaDetalleImpuestos As New List(Of DetalleImpuestosPlanillas)
                conexion.CleanParameters()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_impuestos_planilla_despachos]")

                While resultado.Read
                    DetalleImpuestos = New DetalleImpuestosPlanillas(resultado)
                    ListaDetalleImpuestos.Add(DetalleImpuestos)
                End While
                If ListaDetalleImpuestos.Count() > 0 Then
                    item.Planilla.DetalleImpuesto = ListaDetalleImpuestos
                End If


            End Using

            Return item
        End Function

        Public Function Anular(entidad As PlanillaPaqueteria) As PlanillaPaqueteria
            Dim anulo As Boolean = False
            Dim item As New PlanillaPaqueteria
            Dim itemDocumento As EncabezadoDocumentos
            Dim lista As New List(Of EncabezadoDocumentos)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Anular_Planilla_Guias_Paqueteria")

                While resultado.Read
                    itemDocumento = New EncabezadoDocumentos With {
                     .Cumplido = New Cumplido With {.Numero = Read(resultado, "ECPD_Numero_Documento")},
                     .Liquidacion = New Liquidacion With {.Numero = Read(resultado, "ELPD_Numero_Documento")},
                     .Comprobante = New EncabezadoDocumentoComprobantes With {.Numero = Read(resultado, "ENDC_Numero_Documento")}
                     }
                    lista.Add(itemDocumento)
                    item.Anulado = Read(resultado, "Anulo")

                End While
                item.DocumentosRelacionados = lista

            End Using

            Return item
        End Function

        Public Function EliminarGuia(entidad As PlanillaPaqueteria) As Boolean
            Dim Eliminar As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                Dim resultado As IDataReader

                For Each lista In entidad.Planilla.Detalles
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Remesa.Numero)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_guia_planilla_paqueteria")
                    If resultado.RecordsAffected > 0 Then
                        Eliminar = True
                    End If
                    resultado.Close()
                Next

            End Using

            Return Eliminar
        End Function

        Public Function EliminarRecoleccion(entidad As PlanillaPaqueteria) As Boolean
            Dim Eliminar As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                Dim resultado As IDataReader

                For Each lista In entidad.ListadoPlanillaRecolecciones
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENPR_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_EREC_Numero", lista.Numero)

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_recoleccion_planilla_paqueteria")

                    If resultado.RecordsAffected > 0 Then
                        Eliminar = True
                    End If
                    resultado.Close()
                Next

            End Using

            Return Eliminar
        End Function

        Public Function ConsultarPlanillasPendientesRecepcionar(filtro As PlanillaPaqueteria) As IEnumerable(Of PlanillaPaqueteria)
            Dim lista As New List(Of PlanillaPaqueteria)
            Dim item As PlanillaPaqueteria

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                Dim resultado As IDataReader
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                If filtro.Planilla.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Planilla.Numero)
                End If
                If filtro.Planilla.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.Planilla.NumeroDocumento)
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_planilla_paqueteria_pendiente_recepcionar")
                While resultado.Read
                    item = New PlanillaPaqueteria(resultado)
                    lista.Add(item)
                End While
                conexion.CloseConnection()
            End Using
            Return lista
        End Function
    End Class
End Namespace
