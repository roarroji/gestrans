﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria

Namespace Basico.General

    ''' <summary>
    ''' Clase <see cref="RepositorioPlanillaRecolecciones"/>
    ''' </summary>
    Public NotInheritable Class RepositorioPlanillaRecolecciones
        Inherits RepositorioBase(Of PlanillaRecolecciones)
        Dim inserto As Boolean = True
        Dim modifico As Boolean = True
        Public Overrides Function Consultar(filtro As PlanillaRecolecciones) As IEnumerable(Of PlanillaRecolecciones)
            Dim lista As New List(Of PlanillaRecolecciones)
            Dim item As PlanillaRecolecciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If

                If filtro.FechaInicio > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicio, SqlDbType.DateTime)
                End If

                If filtro.FechaFin > DateTime.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFin, SqlDbType.DateTime)
                End If

                If filtro.Oficinas.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficinas.Codigo)
                End If

                If Not String.IsNullOrWhiteSpace(filtro.PlacaVehiculo) Then
                    conexion.AgregarParametroSQL("@par_Placa", filtro.PlacaVehiculo)
                End If

                If filtro.Estado = 1 Then
                    conexion.AgregarParametroSQL("@par_Estado", 1)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)
                ElseIf filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", 0)
                    conexion.AgregarParametroSQL("@par_Anulado", 0)
                ElseIf filtro.Estado = 3 Then
                    conexion.AgregarParametroSQL("@par_Anulado", 1)
                End If
                If filtro.TipoDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Tipo_Documento", filtro.TipoDocumento)
                End If

                If Not IsNothing(filtro.Conductor) Then
                    If filtro.Conductor.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Conductor", filtro.Conductor.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If

                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_planilla_recolecciones")
                While resultado.Read
                    item = New PlanillaRecolecciones(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As PlanillaRecolecciones) As Long

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.CodigoVehiculo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                    conexion.AgregarParametroSQL("@par_Cantidad_Total", entidad.CantidadTotal)
                    conexion.AgregarParametroSQL("@par_Peso_Total", entidad.PesoTotal)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficinas.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_Numero_Recolecciones", entidad.CadenaRecoleccion)
                    conexion.AgregarParametroSQL("@par_Numero_Guias", entidad.CadenaGuia)
                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_planilla_recolecciones")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        entidad.Numero = resultado.Item("Numero").ToString()
                    End While
                    resultado.Close()
                    If entidad.NumeroDocumento.Equals(Cero) Then
                        Return Cero
                    Else

                        Call BorrarListaAuxiliares(entidad.CodigoEmpresa, entidad.Numero, conexion)
                        If Not IsNothing(entidad.ListadoAuxiliares) Then
                            For Each ListadoAuxiliares In entidad.ListadoAuxiliares

                                ListadoAuxiliares.CodigoEmpresa = entidad.CodigoEmpresa
                                ListadoAuxiliares.NumeroPlanilla = entidad.Numero

                                If Not InsertarListaAuxiliares(ListadoAuxiliares, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If

                        Call BorrarListaPlanillaRecolecciones(entidad.CodigoEmpresa, entidad.Numero, conexion)
                        If Not IsNothing(entidad.ListadoPlanillaRecolecciones) Then
                            For Each ListadoPlanillaRecolecciones In entidad.ListadoPlanillaRecolecciones

                                ListadoPlanillaRecolecciones.CodigoEmpresa = entidad.CodigoEmpresa
                                ListadoPlanillaRecolecciones.NumeroPlanilla = entidad.Numero

                                If Not InsertarListaPlanillaRecolecciones(ListadoPlanillaRecolecciones, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If

                        Call BorrarListaPlanillaGuias(entidad.CodigoEmpresa, entidad.Numero, conexion)
                        If Not IsNothing(entidad.ListadoPlanillaGuias) Then
                            For Each ListadoPlanillaGuias In entidad.ListadoPlanillaGuias

                                ListadoPlanillaGuias.CodigoEmpresa = entidad.CodigoEmpresa
                                ListadoPlanillaGuias.NumeroPlanilla = entidad.Numero

                                If Not InsertarListaPlanillaGuias(ListadoPlanillaGuias, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If

                        conexion.CloseConnection()

                    End If

                End Using
                If inserto Then
                    transaccion.Complete()
                Else
                    transaccion.Dispose()
                End If
            End Using

            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Modificar(entidad As PlanillaRecolecciones) As Long

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Numero_Documento", entidad.NumeroDocumento)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.CodigoVehiculo)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                    conexion.AgregarParametroSQL("@par_Cantidad_Total", entidad.CantidadTotal)
                    conexion.AgregarParametroSQL("@par_Peso_Total", entidad.PesoTotal)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficinas.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_Numero_Recolecciones", entidad.CadenaRecoleccion)
                    conexion.AgregarParametroSQL("@par_Numero_Guias", entidad.CadenaGuia)
                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    End If
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_planilla_recolecciones")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento").ToString()
                        entidad.Numero = resultado.Item("Numero").ToString()
                        entidad.MensajeSQL = resultado.Item("Mensaje").ToString()
                    End While

                    resultado.Close()

                    If entidad.NumeroDocumento.Equals(Cero) Then
                        Return Cero
                    Else

                        Call BorrarListaAuxiliares(entidad.CodigoEmpresa, entidad.Numero, conexion)
                        If Not IsNothing(entidad.ListadoAuxiliares) Then
                            For Each ListadoAuxiliares In entidad.ListadoAuxiliares

                                ListadoAuxiliares.CodigoEmpresa = entidad.CodigoEmpresa
                                ListadoAuxiliares.NumeroPlanilla = entidad.Numero

                                If Not InsertarListaAuxiliares(ListadoAuxiliares, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If

                        If Not IsNothing(entidad.ListadoPlanillaRecoleccionesNoCkeck) Then
                            For Each listarecolecciones In entidad.ListadoPlanillaRecoleccionesNoCkeck
                                inserto = LiberarRecolecciones(listarecolecciones, entidad.Numero, conexion)
                            Next
                        End If

                        Call BorrarListaPlanillaRecolecciones(entidad.CodigoEmpresa, entidad.Numero, conexion)
                        If Not IsNothing(entidad.ListadoPlanillaRecolecciones) Then
                            For Each ListadoPlanillaRecolecciones In entidad.ListadoPlanillaRecolecciones

                                ListadoPlanillaRecolecciones.CodigoEmpresa = entidad.CodigoEmpresa
                                ListadoPlanillaRecolecciones.NumeroPlanilla = entidad.Numero

                                If Not InsertarListaPlanillaRecolecciones(ListadoPlanillaRecolecciones, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If

                        If Not IsNothing(entidad.ListadoPlanillaGuiasNoCkeck) Then
                            For Each listaguias In entidad.ListadoPlanillaGuiasNoCkeck
                                inserto = LiberarGuias(listaguias, entidad.Numero, conexion)
                            Next
                        End If

                        Call BorrarListaPlanillaGuias(entidad.CodigoEmpresa, entidad.Numero, conexion)
                        If Not IsNothing(entidad.ListadoPlanillaGuias) Then
                            For Each ListadoPlanillaGuias In entidad.ListadoPlanillaGuias

                                ListadoPlanillaGuias.CodigoEmpresa = entidad.CodigoEmpresa
                                ListadoPlanillaGuias.NumeroPlanilla = entidad.Numero

                                If Not InsertarListaPlanillaGuias(ListadoPlanillaGuias, conexion) Then
                                    Exit For
                                End If
                            Next
                        End If

                        conexion.CloseConnection()

                    End If
                End Using
                If modifico Then
                    transaccion.Complete()
                Else
                    transaccion.Dispose()
                End If
            End Using

            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Obtener(filtro As PlanillaRecolecciones) As PlanillaRecolecciones
            Dim item As New PlanillaRecolecciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > Cero Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_planilla_recolecciones]")

                While resultado.Read
                    item = New PlanillaRecolecciones(resultado)
                End While
                resultado.Close()
                If item.Numero > 0 Then
                    item.ListadoAuxiliares = ConsultarListaAuxiliares(filtro.CodigoEmpresa, filtro.Numero, conexion, resultado)
                End If

                If item.Numero > 0 Then
                    item.ListadoPlanillaRecolecciones = ConsultarListaPlanillaRecolecciones(filtro.CodigoEmpresa, filtro.Numero, conexion, resultado)
                End If

                If item.Numero > 0 Then
                    item.ListadoPlanillaGuias = ConsultarListaPlanillaGuias(filtro.CodigoEmpresa, filtro.Numero, conexion, resultado)
                End If

            End Using

            Return item
        End Function

        Public Function Anular(entidad As PlanillaRecolecciones) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_planilla_recolecciones]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > Cero, True, False)
                End While

            End Using

            Return anulo
        End Function

        Public Function ConsultarListaPlanillaGuias(CodiEmpresa As Short, NumePlanilla As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of DetallePlanillaGuias)
            Dim lista As New List(Of DetallePlanillaGuias)
            Dim item As DetallePlanillaGuias
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Codigo", NumePlanilla)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_planilla_guias]")

            While resultado.Read
                item = New DetallePlanillaGuias(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function InsertarListaPlanillaGuias(entidad As DetallePlanillaGuias, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Numero", entidad.NumeroPlanilla)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Numero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_lista_planilla_guias]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function BorrarListaPlanillaGuias(CodiEmpresa As Short, NumePlanilla As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Codigo", NumePlanilla)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_lista_planilla_guias]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function ConsultarListaPlanillaRecolecciones(CodiEmpresa As Short, NumePlanilla As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of DetallePlanillaRecolecciones)
            Dim lista As New List(Of DetallePlanillaRecolecciones)
            Dim item As DetallePlanillaRecolecciones
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Codigo", NumePlanilla)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_planilla_recolecciones]")

            While resultado.Read
                item = New DetallePlanillaRecolecciones(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function InsertarListaPlanillaRecolecciones(entidad As DetallePlanillaRecolecciones, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Numero", entidad.NumeroPlanilla)
            conexion.AgregarParametroSQL("@par_EREC_Numero", entidad.Numero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_lista_planilla_recolecciones]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function BorrarListaPlanillaRecolecciones(CodiEmpresa As Short, NumePlanilla As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Codigo", NumePlanilla)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_lista_planilla_recolecciones]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function ConsultarListaAuxiliares(CodiEmpresa As Short, NumePlanilla As Long, ByRef conexion As Conexion.DataBaseFactory, resultado As IDataReader) As IEnumerable(Of DetalleAuxiliaresPlanillaRecolecciones)
            Dim lista As New List(Of DetalleAuxiliaresPlanillaRecolecciones)
            Dim item As DetalleAuxiliaresPlanillaRecolecciones
            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Codigo", NumePlanilla)

            resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_auxiliares]")

            While resultado.Read
                item = New DetalleAuxiliaresPlanillaRecolecciones(resultado)
                lista.Add(item)
            End While
            resultado.Close()
            Return lista
        End Function

        Public Function InsertarListaAuxiliares(entidad As DetalleAuxiliaresPlanillaRecolecciones, ByRef conexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Codigo", entidad.NumeroPlanilla)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_Horas_Trabajados", entidad.Horas, SqlDbType.Float)
            conexion.AgregarParametroSQL("@par_Valor_Horas", entidad.Valor, SqlDbType.Money)

            If Not IsNothing(entidad.Observaciones) Then
                conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            End If

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_lista_auxiliares]")

            If resultado.RecordsAffected = 0 Then
                inserto = False
            End If
            resultado.Close()

            Return inserto
        End Function

        Public Function BorrarListaAuxiliares(CodiEmpresa As Short, NumePlanilla As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Codigo", NumePlanilla)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_lista_auxiliares]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

        Public Function LiberarRecolecciones(entidad As DetallePlanillaRecolecciones, Numero As Integer, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Numero", entidad.NumeroPlanilla)
            conexion.AgregarParametroSQL("@par_EREC_Numero", entidad.Numero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_liberar_recolecciones")

            While resultado.Read

                entidad.Numero = resultado.Item("CantidadRegistros").ToString()
            End While
            resultado.Close()

            If entidad.Numero.Equals(Cero) Then
                inserto = False

            End If

            Return inserto

        End Function

        Public Function LiberarGuias(entidad As DetallePlanillaGuias, Numero As Integer, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_ENPR_Numero", entidad.NumeroPlanilla)
            conexion.AgregarParametroSQL("@par_ENRE_Numero", entidad.Numero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_liberar_guias_recolecciones")

            While resultado.Read

                entidad.Numero = resultado.Item("CantidadRegistros").ToString()
            End While
            resultado.Close()

            If entidad.Numero.Equals(Cero) Then
                inserto = False

            End If

            Return inserto

        End Function

    End Class

End Namespace