﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Paqueteria
Namespace Paqueteria
    Public Class RepositorioRecepcionGuias
        Inherits RepositorioBase(Of RemesaPaqueteria)

        Public Overrides Function Insertar(entidad As RemesaPaqueteria) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As RemesaPaqueteria) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Consultar(filtro As RemesaPaqueteria) As IEnumerable(Of RemesaPaqueteria)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As RemesaPaqueteria) As RemesaPaqueteria
            Throw New NotImplementedException()
        End Function

        Public Function RecibirGuias(entidad As RemesaPaqueteria) As Long
            Dim inserto As Double


            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                If RemesaOficinaDestino(entidad) Then
                    If Not IsNothing(entidad.RemesaPaqueteria) Then
                        Dim EstadoRemesaPaqueteria = entidad.RemesaPaqueteria

                        For Each listadetalleAuxi In entidad.RemesaPaqueteria
                            listadetalleAuxi.Remesa = entidad.Remesa
                            inserto = RecepcionRemesaPaqueteriaOficina(listadetalleAuxi)
                        Next
                    End If
                End If

                If inserto > 0 Then
                    ' entidad.Remesa.NumeroDocumento = inserto
                    transaccion.Complete()
                Else
                    transaccion.Dispose()
                    entidad.Numero = Cero
                End If
            End Using
            Return inserto
        End Function

        Private Function RecepcionRemesaPaqueteriaOficina(entidad As RemesaPaqueteria) As Long
            Dim inserto As Boolean = True
            Dim insertoRecorrido As Boolean = True
            Dim insertoConcepto As Boolean = True

            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ESRP_Numero", entidad.EstadoRemesaPaqueteria.Codigo)
                conexion.AgregarParametroSQL("@par_OFIC_Codigo_Actual", entidad.CodigoOficinaActual)
                conexion.AgregarParametroSQL("@par_Ciudad_Actual", entidad.CodigoCiudad)
                If Not IsNothing(entidad.UsuarioModifica) Then

                    conexion.AgregarParametroSQL("@par_Usuario_Modifica", entidad.UsuarioModifica.Codigo)
                End If

                If Not IsNothing(entidad.Remesa.Planilla) Then

                    If entidad.Remesa.Planilla.TipoDocumento = CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA Then

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_recepcion_guias_oficina")
                    Else
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_recepcion_planilla_oficina")
                    End If

                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_recepcion_planilla_oficina")
                End If



                While resultado.Read

                    entidad.NumeroDocumento = resultado.Item("Numero").ToString()

                End While
                If entidad.NumeroDocumento <> Cero Then
                    CONTADOR += 1
                End If
                resultado.Close()
            End Using

            entidad.NumeroDocumento = CONTADOR


            Return entidad.NumeroDocumento
        End Function

        Private Function RemesaOficinaDestino(entidad As RemesaPaqueteria) As Boolean
            Dim inserto As Boolean = True
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Guias_Seleccionadas", entidad.CadenaGuias)

                resultado = conexion.ExecuteReaderStoreProcedure("gps_guias_oficina_destino")




                While resultado.Read

                    entidad.Numero = resultado.Item("Numero").ToString()
                    entidad.MensajeSQL = resultado.Item("Mensaje").ToString()
                End While
                resultado.Close()
                If entidad.Numero.Equals(Cero) Then
                    inserto = False
                End If

            End Using



            Return inserto
        End Function



        Public Function ActualizarSecuencia(filtro As RemesaPaqueteria) As RemesaPaqueteria
            Dim item As New RemesaPaqueteria
            Dim Doc As New Documentos
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                conexion.AgregarParametroSQL("@par_Secuencia", filtro.Secuencia)

                If filtro.TipoDocumento = 110 Then
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_secuencia_remesa_paqueteria")
                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_actualizar_secuencia_recoleccion")
                End If
                While resultado.Read
                    item = New RemesaPaqueteria(resultado)
                End While
                resultado.Close()

                conexion.CloseConnection()
            End Using


            Return item
        End Function

    End Class
End Namespace
