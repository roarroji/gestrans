﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Entidades.Despachos.Planilla

Namespace Paqueteria
    Public NotInheritable Class RepositorioValidacionPlanillaPaqueteria
        Inherits RepositorioBase(Of EncabezadoValidacionPlanillaPaqueteria)
        Dim inserto As Boolean = True
        Dim modifico As Boolean = True

        Public Overrides Function Consultar(filtro As EncabezadoValidacionPlanillaPaqueteria) As IEnumerable(Of EncabezadoValidacionPlanillaPaqueteria)
            Dim lista As New List(Of EncabezadoValidacionPlanillaPaqueteria)
            Dim item As EncabezadoValidacionPlanillaPaqueteria
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    Dim resultado As IDataReader
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                    If filtro.FechaInicial > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                    End If
                    If filtro.FechaFinal > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                    End If
                    If Not IsNothing(filtro.NumeroDocumento) Then
                        If filtro.NumeroDocumento > 0 Then
                            conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                        End If
                    End If
                    If Not IsNothing(filtro.TipoDocumento) Then
                        If filtro.TipoDocumento > 0 Then
                            conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                        End If
                    End If
                    If Not IsNothing(filtro.NumeroPlanilla) Then
                        If filtro.NumeroPlanilla > 0 Then
                            conexion.AgregarParametroSQL("@par_Numero_Planilla", filtro.NumeroPlanilla)
                        End If
                    End If
                    If Not IsNothing(filtro.Vehiculo) Then
                        If filtro.Vehiculo.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Vehiculo_Codigo", filtro.Vehiculo.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Conductor) Then
                        If filtro.Conductor.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_Conductor_Codigo", filtro.Conductor.Codigo)
                        End If
                    End If

                    If Not IsNothing(filtro.Oficina) Then
                        If filtro.Oficina.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                        End If
                    End If

                    If filtro.Estado > -1 Then
                        If filtro.Estado = 2 Then 'ANULADO
                            conexion.AgregarParametroSQL("@par_Anulado", 1)
                        Else
                            conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                            conexion.AgregarParametroSQL("@par_Anulado", 0)
                        End If
                    End If
                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If
                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > 0 Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_planilla_cargue_descargue")

                    While resultado.Read
                        item = New EncabezadoValidacionPlanillaPaqueteria(resultado)
                        lista.Add(item)
                    End While
                    conexion.CloseConnection()
                End Using
            End Using
            Return lista
        End Function

        Public Overrides Function Insertar(entidad As EncabezadoValidacionPlanillaPaqueteria) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                    ' conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoVerificacion.Codigo)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo ", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_Cantidad ", entidad.Cantidad)
                    conexion.AgregarParametroSQL("@par_Peso ", entidad.Peso, SqlDbType.Decimal)
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea ", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo ", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                    If Not IsNothing(entidad.Semirremolque) Then
                        conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo)
                    End If



                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_validacion_planilla")

                    While resultado.Read
                        entidad.Numero = resultado.Item("Numero")
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento")
                    End While

                    resultado.Close()


                    For Each lista In entidad.ListadoGuias
                        conexion.CleanParameters()

                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_EPCD_Numero", entidad.Numero)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.NumeroRemesa)
                        conexion.AgregarParametroSQL("@par_Numero_Unidad", lista.NumeroUnidad)
                        conexion.AgregarParametroSQL("@par_Observaciones", lista.ObservacionesVerificacion)

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_validacion_planilla")

                        While resultado.Read
                            If Not (resultado.Item("Numero") > 0) Then
                                inserto = False
                                transaccion.Dispose()
                                Return -2
                                Exit For
                            End If
                        End While
                        resultado.Close()
                    Next


                    If inserto Then

                        If entidad.Numero > 0 Then
                            transaccion.Complete()
                        End If
                    Else
                        transaccion.Dispose()
                    End If

                End Using
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Modificar(entidad As EncabezadoValidacionPlanillaPaqueteria) As Long
            Dim modifico As Boolean = False
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa) 'SMALLINT,
                    conexion.AgregarParametroSQL("@par_Numero ", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo ", entidad.TipoVerificacion.Codigo)
                    conexion.AgregarParametroSQL("@par_VEHI_Codigo ", entidad.Vehiculo.Codigo)
                    conexion.AgregarParametroSQL("@par_Cantidad ", entidad.Cantidad)
                    conexion.AgregarParametroSQL("@par_Peso ", entidad.Peso, SqlDbType.Decimal)
                    conexion.AgregarParametroSQL("@par_Estado ", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica ", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo ", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones ", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)

                    If Not IsNothing(entidad.Semirremolque) Then
                        conexion.AgregarParametroSQL("@par_SEMI_Codigo", entidad.Semirremolque.Codigo)
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_encabezado_validacion_planilla")

                    While resultado.Read
                        entidad.NumeroDocumento = resultado.Item("Numero_Documento")
                        modifico = True
                    End While


                    resultado.Close()
                    EliminarDetallePlanilla(entidad, conexion)

                    For Each lista In entidad.ListadoGuias
                        conexion.CleanParameters()

                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_EPCD_Numero", entidad.Numero)
                        conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.NumeroRemesa)
                        conexion.AgregarParametroSQL("@par_Numero_Unidad", lista.NumeroUnidad)
                        conexion.AgregarParametroSQL("@par_Observaciones", lista.ObservacionesVerificacion)

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_validacion_planilla")

                        While resultado.Read
                            If Not (resultado.Item("Numero") > 0) Then
                                inserto = False
                                transaccion.Dispose()
                                Return -2
                                Exit For
                            End If
                        End While
                        resultado.Close()
                    Next

                    resultado.Close()
                    If modifico Then
                        transaccion.Complete()
                    Else
                        transaccion.Dispose()
                    End If
                End Using
            End Using
            Return entidad.NumeroDocumento
        End Function


        Public Function EliminarDetallePlanilla(entidad As EncabezadoValidacionPlanillaPaqueteria, conexion As DataBaseFactory)


            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_Numero ", entidad.Numero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_detalle_planilla_cargue_descargue")

            resultado.Close()




        End Function

        Public Overrides Function Obtener(filtro As EncabezadoValidacionPlanillaPaqueteria) As EncabezadoValidacionPlanillaPaqueteria
            Dim item As New EncabezadoValidacionPlanillaPaqueteria
            Dim Doc As New Documentos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_planilla_cargue_descargue")

                While resultado.Read
                    item = New EncabezadoValidacionPlanillaPaqueteria(resultado)
                End While
                conexion.CloseConnection()
                resultado.Close()

                'Consulta los detalles

                Dim Detalle As DetallePlanillas
                Dim ListaDetalle As New List(Of DetallePlanillas)
                conexion.CleanParameters()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ENPD_Numero", filtro.Numero)
                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_planilla_cargue_descargue")

                While resultado.Read
                    Detalle = New DetallePlanillas(resultado)
                    ListaDetalle.Add(Detalle)
                End While

                item.ListadoGuias = ListaDetalle
                conexion.CloseConnection()
                resultado.Close()




            End Using

            Return item
        End Function

        Public Function Anular(entidad As EncabezadoValidacionPlanillaPaqueteria) As EncabezadoValidacionPlanillaPaqueteria
            Dim anulo As Boolean = False
            Dim item As New EncabezadoValidacionPlanillaPaqueteria
            Dim itemDocumento As EncabezadoDocumentos
            Dim lista As New List(Of EncabezadoDocumentos)

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_planilla_cargue_descargue")

                While resultado.Read

                    item.Anulado = Read(resultado, "Anulo")

                End While
                item.DocumentosRelacionados = lista

            End Using

            Return item
        End Function

        Public Function EliminarGuia(entidad As EncabezadoValidacionPlanillaPaqueteria) As Boolean
            Dim Eliminar As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                Dim resultado As IDataReader

                For Each lista In entidad.Planilla.Detalles
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_ENRE_Numero", lista.Remesa.Numero)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_guia_planilla_paqueteria")
                    If resultado.RecordsAffected > 0 Then
                        Eliminar = True
                    End If
                    resultado.Close()
                Next

            End Using

            Return Eliminar
        End Function

        Public Function EliminarRecoleccion(entidad As EncabezadoValidacionPlanillaPaqueteria) As Boolean
            Dim Eliminar As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                Dim resultado As IDataReader

                For Each lista In entidad.ListadoPlanillaRecolecciones
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ENPR_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_EREC_Numero", lista.Numero)

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_recoleccion_planilla_paqueteria")

                    If resultado.RecordsAffected > 0 Then
                        Eliminar = True
                    End If
                    resultado.Close()
                Next

            End Using

            Return Eliminar
        End Function
    End Class
End Namespace
