﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Proveedores
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Repositorio.Contabilidad
Imports System.Transactions

Namespace Proveedores

    Public Class RepositorioLiquidacionPlanillasProveedores
        Inherits RepositorioBase(Of LiquidacionPlanillasProveedores)

        Public Overrides Function Insertar(entidad As LiquidacionPlanillasProveedores) As Long
            'Throw New NotImplementedException()
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_TERC_Transportador", entidad.Transportador.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad)
                    conexion.AgregarParametroSQL("@par_Peso", entidad.Peso)
                    conexion.AgregarParametroSQL("@par_Flete_Transportador", entidad.FleteTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Anticipo_Transportador", entidad.AnticipoTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Otros_Conceptos", entidad.ValorOtrosConceptos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total", entidad.Total, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Base_Impuestos", entidad.ValorBaseImpuestos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Impuestos", entidad.ValorImpuestos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Pagar", entidad.ValorPagar, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Oficina", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.CodigoUsuarioCrea.Codigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_liquidacion_planillas_Despachos_Proveedores")
                    While resultado.Read

                        entidad.NumeroDocumento = resultado.Item("NumeroDocumento").ToString
                        ' = resultado.Item("Numero").ToString
                    End While
                    resultado.Close()

                    For Each item In entidad.ListadoPlanillas
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_ELPP_Numero_Documento ", entidad.NumeroDocumento)
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_PLDP_Numero", item.Numero)
                        conexion.AgregarParametroSQL("@par_Flete_Transportador", item.ValorTotalFlete, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Anticipo_Transportador", item.AnticipoTransportador, SqlDbType.Money)

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_liquidacion_planilla_proveedores")

                        While resultado.Read
                            If Not (resultado.Item("Numero") > 0) Then
                                inserto = False
                                transaccion.Dispose()
                                Return -2
                                Exit For
                            End If
                        End While

                        resultado.Close()
                    Next

                    For Each item In entidad.ListadoImpuestos
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_ELPP_Numero_Documento ", entidad.NumeroDocumento)
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ENIM_Numero", item.Codigo)
                        conexion.AgregarParametroSQL("@par_Valor_Base", item.ValorBase, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Tarifa", item.Valor_tarifa, SqlDbType.Float)
                        conexion.AgregarParametroSQL("@par_Valor_Impuesto", item.ValorImpuesto, SqlDbType.Money)

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_impuestos_planilla_proveedores")

                        While resultado.Read
                            If Not (resultado.Item("Numero") > 0) Then
                                inserto = False
                                transaccion.Dispose()
                                Return -2
                                Exit For
                            End If
                        End While

                        resultado.Close()
                    Next

                    If entidad.Estado = 1 Then

                        If Not IsNothing(entidad.CuentaPorPagar) Then ' And entidad.ValorPagar > 0
                            entidad.CuentaPorPagar.Numero = entidad.NumeroDocumento
                            inserto = Generar_CxP(entidad.CuentaPorPagar, conexion)
                        End If

                        Dim rep As New RepositorioEncabezadoComprobantesContables
                        Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Codigo, 35, conexion, resultado)
                    End If

                    If inserto Then
                        transaccion.Complete()
                    Else
                        entidad.NumeroDocumento = Cero
                    End If

                End Using
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Modificar(entidad As LiquidacionPlanillasProveedores) As Long
            Dim inserto As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_TERC_Transportador", entidad.Transportador.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    conexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad)
                    conexion.AgregarParametroSQL("@par_Peso", entidad.Peso)
                    conexion.AgregarParametroSQL("@par_Flete_Transportador", entidad.FleteTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Anticipo_Transportador", entidad.AnticipoTransportador, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Otros_Conceptos", entidad.ValorOtrosConceptos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Total", entidad.Total, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Base_Impuestos", entidad.ValorBaseImpuestos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Impuestos", entidad.ValorImpuestos, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Valor_Pagar", entidad.ValorPagar, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Oficina", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.CodigoUsuarioCrea.Codigo)



                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_liquidacion_planillas_Despachos_Proveedores")
                    While resultado.Read

                        entidad.NumeroDocumento = resultado.Item("NumeroDocumento").ToString
                    End While
                    resultado.Close()

                    For Each item In entidad.ListadoPlanillas
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_ELPP_Numero_Documento ", entidad.NumeroDocumento)
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_PLDP_Numero", item.Numero)
                        conexion.AgregarParametroSQL("@par_Flete_Transportador", item.ValorTotalFlete, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Anticipo_Transportador", item.AnticipoTransportador, SqlDbType.Money)

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_liquidacion_planilla_proveedores")

                        While resultado.Read
                            If Not (resultado.Item("Numero") > 0) Then
                                inserto = False
                                transaccion.Dispose()
                                Return -2
                                Exit For
                            End If
                        End While

                        resultado.Close()
                    Next

                    For Each item In entidad.ListadoImpuestos
                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_ELPP_Numero_Documento ", entidad.NumeroDocumento)
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ENIM_Numero", item.Codigo)
                        conexion.AgregarParametroSQL("@par_Valor_Base", item.ValorBase, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Valor_Tarifa", item.Valor_tarifa, SqlDbType.Float)
                        conexion.AgregarParametroSQL("@par_Valor_Impuesto", item.ValorImpuesto, SqlDbType.Money)

                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_impuestos_planilla_proveedores")

                        While resultado.Read
                            If Not (resultado.Item("Numero") > 0) Then
                                inserto = False
                                transaccion.Dispose()
                                Return -2
                                Exit For
                            End If
                        End While

                        resultado.Close()
                    Next

                    If entidad.Estado = 1 Then

                        If Not IsNothing(entidad.CuentaPorPagar) Then ' And entidad.ValorPagar > 0
                            entidad.CuentaPorPagar.Numero = entidad.NumeroDocumento
                            inserto = Generar_CxP(entidad.CuentaPorPagar, conexion)
                        End If

                        Dim rep As New RepositorioEncabezadoComprobantesContables
                        Call rep.GenerarMovimientoContable(entidad.CodigoEmpresa, entidad.Codigo, 35, conexion, resultado)
                    End If

                    If inserto Then
                        transaccion.Complete()
                    Else
                        entidad.NumeroDocumento = Cero
                    End If

                End Using
            End Using
            Return entidad.NumeroDocumento
        End Function

        Public Overrides Function Consultar(filtro As LiquidacionPlanillasProveedores) As IEnumerable(Of LiquidacionPlanillasProveedores)
            Dim lista As New List(Of LiquidacionPlanillasProveedores)
            Dim item As LiquidacionPlanillasProveedores


            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If Not IsNothing(filtro) Then
                    If filtro.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                    End If

                    If filtro.Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                    End If

                    If filtro.FechaInicial > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                    End If
                    If filtro.FechaFinal > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                    End If

                    If Not IsNothing(filtro.Transportador) Then
                        If filtro.Transportador.Nombre <> "" Then
                            conexion.AgregarParametroSQL("@par_Transportador", filtro.Transportador.Nombre)
                        End If

                    End If


                    If filtro.Estado > -1 And filtro.Estado < 2 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                        conexion.AgregarParametroSQL("@par_Anulado", 0)
                    End If

                    If filtro.Estado >= 2 Then
                        conexion.AgregarParametroSQL("@par_Anulado", 1)
                    End If


                    'Filtro Paginación:

                    If Not IsNothing(filtro.RegistrosPagina) Then
                        If filtro.RegistrosPagina > 0 Then
                            conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                        End If
                    End If

                    If Not IsNothing(filtro.Pagina) Then
                        If filtro.Pagina > 0 Then
                            conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                        End If
                    End If


                End If

                Dim resultado As IDataReader

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_liquidacion_planilla_despachos_Proveedores")


                While resultado.Read
                    item = New LiquidacionPlanillasProveedores(resultado)
                    lista.Add(item)
                End While

                conexion.CloseConnection()


            End Using
            Return lista
        End Function

        Public Overrides Function Obtener(filtro As LiquidacionPlanillasProveedores) As LiquidacionPlanillasProveedores
            Dim item As New LiquidacionPlanillasProveedores

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_liquidacion_planilla_despachos_Proveedores")

                While resultado.Read
                    filtro.Numero = resultado.Item("Numero")
                    item = New LiquidacionPlanillasProveedores(resultado)
                End While

                conexion.CloseConnection()
                resultado.Close()

                item.ListadoPlanillas = ObtenerPlanillas(filtro)
                item.ListadoImpuestos = ObtenerImpuestos(filtro)
            End Using

            Return item
        End Function

        Public Function ObtenerPlanillas(filtro As LiquidacionPlanillasProveedores) As IEnumerable(Of DetalleLiquidacionPlanillaProveedores)
            Dim Lista As New List(Of DetalleLiquidacionPlanillaProveedores)
            Dim item As DetalleLiquidacionPlanillaProveedores
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                Conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                Conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = Conexion.ExecuteReaderStoreProcedure("gsp_obtener_detalle_liquidacion_planilla_despachos_Proveedores")

                While resultado.Read
                    item = New DetalleLiquidacionPlanillaProveedores(resultado)
                    Lista.Add(item)
                End While
            End Using

            Return Lista
        End Function

        Public Function ObtenerImpuestos(filtro As LiquidacionPlanillasProveedores) As IEnumerable(Of DetalleImpuestosLiquidacionPlanillasProveedores)
            Dim Lista As New List(Of DetalleImpuestosLiquidacionPlanillasProveedores)
            Dim item As DetalleImpuestosLiquidacionPlanillasProveedores
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_impuestos_liquidacion_planilla_despachos_Proveedores")

                While resultado.Read
                    item = New DetalleImpuestosLiquidacionPlanillasProveedores(resultado)
                    Lista.Add(item)
                End While
            End Using

            Return Lista
        End Function

        Public Function Anular(entidad As LiquidacionPlanillasProveedores) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_liquidacion_despachos_Proveedores")

                While resultado.Read
                    anulo = IIf((resultado.Item("Numero")) > 0, True, False)
                    'anulo = resultado.Item("Numero")
                End While

            End Using

            Return anulo
        End Function

        Public Function Generar_CxP(entidad As EncabezadoDocumentoCuentas, conexion As DataBaseFactory) As Boolean
            Generar_CxP = False
            'Se quema las observaciones, ya que cuando el documento se guarda de una vez en definitivo,
            'en el controller.js no se tiene aún el número documento generado de la liquidación, es decir, en el proceso de insertar
            entidad.Observaciones = "PAGO LIQUIDACIÓN PLANILLA No. " & entidad.NumeroDocumento & ", VALOR $ " & entidad.ValorPagar

            conexion.CleanParameters()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo_Origen", 160)
            conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
            conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
            conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
            conexion.AgregarParametroSQL("@par_CATA_DOOR_Codigo", entidad.DocumentoOrigen.Codigo)
            conexion.AgregarParametroSQL("@par_Codigo_Documento_Origen", entidad.CodigoDocumentoOrigen)
            conexion.AgregarParametroSQL("@par_Documento_Origen", entidad.Numero)
            conexion.AgregarParametroSQL("@par_Fecha_Documento_Origen", entidad.FechaDocumento, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Fecha_Vence_Documento_Origen", entidad.FechaVenceDocumento, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Numeracion", entidad.Numeracion)
            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
            conexion.AgregarParametroSQL("@par_PLUC_Codigo", entidad.CuentaPuc.Codigo)
            conexion.AgregarParametroSQL("@par_Valor_Total", entidad.ValorTotal)
            conexion.AgregarParametroSQL("@par_Abono", entidad.Abono)
            conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo)
            conexion.AgregarParametroSQL("@par_Fecha_Cancelacion_Pago", entidad.FechaCancelacionPago, SqlDbType.Date)
            conexion.AgregarParametroSQL("@par_Aprobado", entidad.Aprobado)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Aprobo", entidad.UsuarioAprobo.Codigo)
            conexion.AgregarParametroSQL("@par_Fecha_Aprobo", entidad.FechaAprobo, SqlDbType.DateTime)
            conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
            conexion.AgregarParametroSQL("@par_VEHI_Codigo", entidad.Vehiculo.Codigo)
            conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_documento_cuentas_liquidacion_proveedores")

            While resultado.Read
                Generar_CxP = IIf(resultado.Item("Codigo") > 0, True, False)
            End While
            resultado.Close()

            Return Generar_CxP
        End Function
    End Class
End Namespace

