﻿Imports System.Configuration
Imports Softtools.GesCarga.Conexion
Imports System.Configuration.ConfigurationManager

''' <summary>
''' Clase <see cref="RepositorioBase(Of T)"/>
''' </summary>
''' <typeparam name="T">Entidad</typeparam>
Public MustInherit Class RepositorioBase(Of T As New)
    Inherits DataRepository(Of T)

    ''' <summary>
    ''' Constante para usar en igualaciones a cero
    ''' </summary>
    Public Const Cero As Short = 0

    ''' <summary>
    ''' Constante para usar en Si Aplica
    ''' </summary>
    Public Const SI_APLICA As Short = 1

    ''' <summary>
    ''' Constante para usar en No Aplica
    ''' </summary>
    Public Const NO_APLICA As Short = 0

    ''' <summary>
    ''' Constante para usar un contador 
    ''' </summary>
    Public CONTADOR As Integer = 0


    ''' <summary>
    ''' Constante para usar campo vacio
    ''' </summary>
    Public Const VACIO As String = ""

#Region "Cosntantes Catologos"
    Public Const CODIGO_CATALOGO_TIPO_ORIGEN_SEGUIMIENTO_NO_APLICA As Integer = 8000
    Public Const CODIGO_CATALOGO_SITIO_SEGUIMIENTO_NO_APLICA As Integer = 8200
    Public Const Codigo_catalogo_estado_guia_recibida_oficina_destino As Integer = 6020
    Public Const CODIGO_CATALOGO_TIPO_AUTORIZACION_VALOR_DECLARADO As Integer = 18407
    Public Const CODIGO_CATALOGO_TIPO_AUTORIZACION_NO_CUMPLIMIENTO_INSPECCION As Integer = 18408
    Public Const CODIGO_TIPO_DOCUMENTO_PLANILLA_PAQUETERIA = 135

#End Region

#Region "Variables"
    Public URL_ASP As String
#End Region

    ''' <summary>
    ''' Inicializa una nueva instancia de la clase <see cref="RepositorioBase(Of T)"/>
    ''' </summary>
    Protected Sub New()

        Try
            ConnectionString = ConfigurationManager.ConnectionStrings("GestransCarga").ToString()
            ConnectionStringDocumentos = ConfigurationManager.ConnectionStrings("GestransCargaDocumentos").ToString()
            URL_ASP = ConfigurationManager.AppSettings("URL_ASP").ToString()
        Catch
            Try
                ConnectionString = AppSettings.Get("ConexionSQL")
                ConnectionStringDocumentos = AppSettings.Get("ConexionSQLDocumental")
            Catch ex As Exception

            End Try
        End Try
    End Sub


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="entidad"></param>
    ''' <returns></returns>
    MustOverride Function Insertar(entidad As T) As Long


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="entidad"></param>
    ''' <returns></returns>
    MustOverride Function Modificar(entidad As T) As Long

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="filtro"></param>
    ''' <returns></returns>
    MustOverride Function Consultar(filtro As T) As IEnumerable(Of T)

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="filtro"></param>
    ''' <returns></returns>
    MustOverride Function Obtener(filtro As T) As T

    Public Function Read(Lector As IDataReader, strCampo As String) As Object
        Dim Tabla As DataTable = Lector.GetSchemaTable
        Dim ExisteCampo As Boolean = False
        For Each Campo As DataRow In Tabla.Rows
            If Campo("ColumnName").ToString() = strCampo Then
                ExisteCampo = True
                Exit For
            End If
        Next
        If ExisteCampo Then
            If Not IsDBNull(Lector.Item(strCampo)) Then
                Return Lector.Item(strCampo)
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

End Class