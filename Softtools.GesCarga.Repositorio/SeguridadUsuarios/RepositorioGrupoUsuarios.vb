﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

Namespace SeguridadUsuarios
    Public NotInheritable Class RepositorioGrupoUsuarios
        Inherits RepositorioBase(Of GrupoUsuarios)
        Public Overrides Function Consultar(filtro As GrupoUsuarios) As IEnumerable(Of GrupoUsuarios)
            Dim lista As New List(Of GrupoUsuarios)
            Dim item As GrupoUsuarios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not IsNothing(filtro.Nombre) And filtro.Nombre <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If
                If Not IsNothing(filtro.CodigoGrupo) And filtro.CodigoGrupo <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Codigo_Grupo", filtro.CodigoGrupo)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_grupo_usuarios")
                While resultado.Read
                    item = New GrupoUsuarios(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function



        Public Overrides Function Insertar(entidad As GrupoUsuarios) As Long

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre.ToString)
                conexion.AgregarParametroSQL("@par_Codigo_Grupo", entidad.CodigoGrupo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_grupo_usuarios")
                While resultado.Read
                    entidad.Codigo = resultado.Item("Codigo").ToString()

                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

                Return entidad.Codigo
            End Using
        End Function

        Public Overrides Function Modificar(entidad As GrupoUsuarios) As Long
            Dim modifico As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()


                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Codigo_Grupo", entidad.CodigoGrupo)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)


                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_modificar_grupo_usuarios]")

                    resultado.Close()

                    If resultado.RecordsAffected > 0 Then
                        modifico = resultado.RecordsAffected
                        resultado.Close()
                        transaccion.Complete()
                    End If

                End Using

            End Using
            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As GrupoUsuarios) As GrupoUsuarios
            Dim item As New GrupoUsuarios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_grupo_usuarios")

                While resultado.Read
                    item = New GrupoUsuarios(resultado)
                End While

            End Using

            Return item
        End Function
        Public Function Anular(entidad As GrupoUsuarios) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_grupo_usuarios]")

                While resultado.Read
                    anulo = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class
End Namespace
