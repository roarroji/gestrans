﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

Namespace SeguridadUsuarios

    ''' <summary>
    ''' Clase <see cref="RepositorioMenuAplicaciones"/>
    ''' </summary> 
    Public NotInheritable Class RepositorioMenuAplicaciones
        Inherits RepositorioBase(Of MenuAplicaciones)

        Public Overrides Function Consultar(filtro As MenuAplicaciones) As IEnumerable(Of MenuAplicaciones)
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Insertar(entidad As MenuAplicaciones) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As MenuAplicaciones) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As MenuAplicaciones) As MenuAplicaciones
            Throw New NotImplementedException()
        End Function
        Public Function ConsultarMenuPorUsuario(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of MenuAplicaciones)
            Dim lista As New List(Of MenuAplicaciones)
            Dim item As MenuAplicaciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", codigoUsuario)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_menu_aplicaciones]")

                While resultado.Read
                    item = New MenuAplicaciones(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Function ConsultarModuloPorUsuario(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of ModuloAplicaciones)
            Dim lista As New List(Of ModuloAplicaciones)
            Dim item As ModuloAplicaciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_modulos]")

                While resultado.Read
                    item = New ModuloAplicaciones(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function


    End Class
End Namespace