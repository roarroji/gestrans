﻿Imports System.Data.OleDb
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports System.Transactions
Imports Softtools.GesCarga.Entidades.Utilitarios
Namespace SeguridadUsuarios

    ''' <summary>
    ''' Clase <see cref="RepositorioPermisoGrupoUsuarios"/>
    ''' </summary>
    Public NotInheritable Class RepositorioPermisoGrupoUsuarios
        Inherits RepositorioBase(Of PermisoGrupoUsuarios)

        Public Overrides Function Consultar(filtro As PermisoGrupoUsuarios) As IEnumerable(Of PermisoGrupoUsuarios)
            Dim lista As New List(Of PermisoGrupoUsuarios)
            Dim item As PermisoGrupoUsuarios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_GRUS_Codigo", filtro.Grupo.Codigo)

                If Not IsNothing(filtro.Menu) Then
                    If filtro.Menu.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_MEAP_Codigo", filtro.Menu.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Menu) Then
                    If filtro.Menu.Padre > 0 Then
                        conexion.AgregarParametroSQL("@par_Padre_Menu", filtro.Menu.Padre)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_permiso_grupos]")

                While resultado.Read
                    item = New PermisoGrupoUsuarios(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ConsultarModulo(codigoEmpresa As Short, Codigo As Long) As IEnumerable(Of ModuloAplicaciones)
            Dim lista As New List(Of ModuloAplicaciones)
            Dim item As New ModuloAplicaciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)

                If Not IsNothing(Codigo) Then
                    If Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", Codigo)
                    End If

                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_modulo_aplicaciones]")

                While resultado.Read
                    item = New ModuloAplicaciones(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As PermisoGrupoUsuarios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As PermisoGrupoUsuarios) As Long
            Dim modifico As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_GRUS_Codigo", entidad.Grupo.Codigo)
                    conexion.AgregarParametroSQL("@par_MEAP_Codigo", entidad.Menu.Codigo)

                    conexion.AgregarParametroSQL("@par_Habilitar", entidad.Habilitar)
                    conexion.AgregarParametroSQL("@par_Consultar", entidad.Consultar)
                    conexion.AgregarParametroSQL("@par_Actualizar", entidad.Actualizar)
                    conexion.AgregarParametroSQL("@par_Eliminar_Anular", entidad.EliminarAnular)
                    conexion.AgregarParametroSQL("@par_Imprimir", entidad.Imprimir)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_modificar_permiso_grupos]")

                    resultado.Close()

                    If resultado.RecordsAffected > 0 Then
                        modifico = resultado.RecordsAffected
                        resultado.Close()
                        transaccion.Complete()
                    End If

                End Using

            End Using
            Return entidad.Menu.Codigo
        End Function

        Public Overrides Function Obtener(filtro As PermisoGrupoUsuarios) As PermisoGrupoUsuarios
            Throw New NotImplementedException()
        End Function

        Public Function GenerarPlanitilla(filtro As PermisoGrupoUsuarios) As PermisoGrupoUsuarios
            Dim fullPath As String
            fullPath = System.AppDomain.CurrentDomain.BaseDirectory()
            System.IO.File.Copy(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Permisos_Grupos_Usuario.xlsx", fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Permisos_Grupos_Usuario_Generado.xlsx", True)
            Dim cn As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Permisos_Grupos_Usuario_Generado.xlsx;Extended Properties='Excel 12.0 Xml;HDR=YES'"

            Dim MenuObj As MenuAplicaciones
            Dim ListMenu As New List(Of MenuAplicaciones)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_mapa_menu_especifico]")
                While resultado.Read
                    MenuObj = New MenuAplicaciones With {.Codigo = resultado.Item("MEAP_Codigo"), .Nombre = resultado.Item("NombreMenu").ToString()}
                    ListMenu.Add(MenuObj)
                End While
            End Using

            Dim Grupo As New GrupoUsuarios
            Dim ObjGrupo As New RepositorioGrupoUsuarios
            Grupo.CodigoEmpresa = filtro.CodigoEmpresa
            Grupo.Codigo = filtro.Grupo.Codigo
            Grupo = ObjGrupo.Obtener(Grupo)

            Using cnn As New OleDbConnection(cn)
                cnn.Open()
                Using cmd As OleDbCommand = cnn.CreateCommand()
                    cmd.CommandText = "INSERT INTO [Grupos$] (Codigo,Nombre) values(@Cod,@nom)"
                    cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = Grupo.Codigo
                    cmd.Parameters.AddWithValue("@nom", Grupo.Nombre)
                    cmd.ExecuteNonQuery()
                End Using
                For i = 1 To ListMenu.Count
                    Using cmd As OleDbCommand = cnn.CreateCommand()
                        cmd.CommandText = "INSERT INTO [Menus$] (Codigo,Nombre) values(@Cod,@nom)"
                        cmd.Parameters.Add("@Cod", OleDbType.Integer, 5).Value = ListMenu(i - 1).Codigo
                        cmd.Parameters.AddWithValue("@nom", ListMenu(i - 1).Nombre)
                        cmd.ExecuteNonQuery()
                    End Using
                Next
                cnn.Close()
            End Using

            Dim RTA As New PermisoGrupoUsuarios
            Dim Archivo() As Byte = IO.File.ReadAllBytes(fullPath + "Controllers\Basico\Plantillas\Plantilla_Cargue_Masivo_Permisos_Grupos_Usuario_Generado.xlsx")
            RTA.Plantilla = Archivo
            Return RTA
        End Function

        Public Function GuardarAsigancionPerfilesNotificaciones(entidad As NotificacionGrupoUsuarios) As Long
            Dim inserto As Long = 1
            Dim resultado As IDataReader
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_CATA_PRNO_Codigo", entidad.Notificacion.Codigo)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_permisos_grupos_notificaciones")
                    resultado.Close()

                    If Not IsNothing(entidad.ListaAsignaciones) Then
                        If entidad.ListaAsignaciones.Count > 0 Then
                            For Each item In entidad.ListaAsignaciones

                                conexion.CleanParameters()

                                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                                conexion.AgregarParametroSQL("@par_GRUS_Codigo", item.Grupo.Codigo)
                                conexion.AgregarParametroSQL("@par_CATA_PRNO_Codigo", entidad.Notificacion.Codigo)
                                conexion.AgregarParametroSQL("@par_Consultar", item.Consultar)
                                conexion.AgregarParametroSQL("@par_Autorizar", item.Autorizar)

                                resultado = conexion.ExecuteReaderStoreProcedure("gsp_insertar_permisos_notificaciones")


                                If resultado.RecordsAffected > 0 Then
                                    inserto = resultado.RecordsAffected
                                    resultado.Close()
                                Else
                                    inserto = 0
                                    resultado.Close()
                                    Exit For

                                End If
                            Next

                        End If

                    End If
                    If inserto > 0 Then
                        transaccion.Complete()
                    Else
                        transaccion.Dispose()
                    End If


                End Using

            End Using
            Return inserto
        End Function

        Public Function ConsultarAsigancionPerfilesNotificaciones(filtro As NotificacionGrupoUsuarios) As IEnumerable(Of DetalleGrupoUsuariosNotificaciones)
            Dim lista As New List(Of DetalleGrupoUsuariosNotificaciones)
            Dim item As DetalleGrupoUsuariosNotificaciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Notificacion) Then
                    conexion.AgregarParametroSQL("@par_CATA_PRNO_Codigo", filtro.Notificacion.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_grupo_bandeja_notificaciones")

                    While resultado.Read
                        item = New DetalleGrupoUsuariosNotificaciones(resultado)
                        lista.Add(item)
                    End While

                Else
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_grupo_bandeja_notificaciones_general")

                    While resultado.Read
                        item = New DetalleGrupoUsuariosNotificaciones(resultado)
                        lista.Add(item)
                    End While
                End If


            End Using

            Return lista
        End Function

    End Class
End Namespace
