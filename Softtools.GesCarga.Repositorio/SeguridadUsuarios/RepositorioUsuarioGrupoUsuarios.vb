﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios

Namespace SeguridadUsuarios
    Public NotInheritable Class RepositorioUsuarioGrupoUsuarios
        Inherits RepositorioBase(Of UsuarioGrupoUsuarios)
        Public Overrides Function Consultar(filtro As UsuarioGrupoUsuarios) As IEnumerable(Of UsuarioGrupoUsuarios)
            Dim lista As New List(Of UsuarioGrupoUsuarios)
            Dim item As UsuarioGrupoUsuarios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.CodigoGrupoUsuarios > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.CodigoGrupoUsuarios)
                End If

                If (filtro.Pagina) Then
                    conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                End If

                If (filtro.RegistrosPagina) Then
                    conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_consultar_grupos_usuario")
                While resultado.Read
                    item = New UsuarioGrupoUsuarios(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function



        Public Overrides Function Insertar(entidad As UsuarioGrupoUsuarios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As UsuarioGrupoUsuarios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As UsuarioGrupoUsuarios) As UsuarioGrupoUsuarios
            Throw New NotImplementedException()
        End Function

        Public Function Eliminar(entidad As UsuarioGrupoUsuarios) As Boolean
            Dim anulo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)

                If entidad.CodigoGrupoUsuarios > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.CodigoGrupoUsuarios)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_eliminar_grupos_usuario")
                While resultado.Read
                    anulo = IIf(resultado.Item("registrosafectados") > 0, True, False)
                End While

            End Using

            Return anulo
        End Function

    End Class
End Namespace
