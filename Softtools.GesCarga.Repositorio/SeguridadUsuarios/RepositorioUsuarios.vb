﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades.ControlViajes
Namespace SeguridadUsuarios
    Public NotInheritable Class RepositorioUsuarios
        Inherits RepositorioBase(Of Usuarios)
        Public Overrides Function Consultar(filtro As Usuarios) As IEnumerable(Of Usuarios)
            Dim lista As New List(Of Usuarios)
            Dim item As Usuarios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If


                If Not IsNothing(filtro.CodigoAlterno) And filtro.CodigoAlterno <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", filtro.CodigoAlterno)
                End If

                If Not IsNothing(filtro.Nombre) And filtro.Nombre <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Nombre", filtro.Nombre)
                End If
                If Not IsNothing(filtro.CodigoUsuario) And filtro.CodigoUsuario <> String.Empty Then
                    conexion.AgregarParametroSQL("@par_Codigo_Usuario", filtro.CodigoUsuario)
                End If

                If Not IsNothing(filtro.Login) Then
                    If filtro.Login <> -1 Then
                        conexion.AgregarParametroSQL("@par_login", filtro.Login)
                    End If
                End If


                If filtro.AplicacionUsuario <> 204 And filtro.AplicacionUsuario <> 0 Then
                    conexion.AgregarParametroSQL("@par_CATA_APLI_Codigo", filtro.AplicacionUsuario)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_usuarios")
                While resultado.Read
                    item = New Usuarios(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function

        Public Function ConsultarCorreosPorOficina(filtro As InspeccionPreoperacional, ByVal conexion As DataBaseFactory) As IEnumerable(Of String)
            Dim lista As New List(Of String)
            Dim item As String
            Dim con As DataBaseFactory
            con = conexion


            con.CleanParameters()

            con.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

            If filtro.Oficina.Codigo > 0 Then
                con.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
            End If




            Dim resultado As IDataReader = con.ExecuteReaderStoreProcedure("gsp_consultar_correos_usuarios_oficina")
            While resultado.Read
                item = resultado.Item("Emails")
                lista.Add(item)
            End While
            resultado.Close()



            Return lista
        End Function
        Public Overrides Function Insertar(entidad As Usuarios) As Long

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo_Usuario", entidad.CodigoUsuario)
                conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                conexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
                conexion.AgregarParametroSQL("@par_Habilitado", entidad.Habilitado)
                conexion.AgregarParametroSQL("@par_Clave", entidad.Clave)
                conexion.AgregarParametroSQL("@par_Dias_Cambio_Clave", entidad.DiasCambioClave)
                conexion.AgregarParametroSQL("@par_Manager", entidad.Manager)
                conexion.AgregarParametroSQL("@par_Login", entidad.Login)
                conexion.AgregarParametroSQL("@par_CATA_APLI_Codigo", entidad.AplicacionUsuario)
                If Not IsNothing(entidad.Empleado) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Empleado", entidad.Empleado.Codigo)
                End If
                If Not IsNothing(entidad.Cliente) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                End If
                If Not IsNothing(entidad.Conductor) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                End If
                If Not IsNothing(entidad.Proveedor) Then
                    conexion.AgregarParametroSQL("@par_TERC_Codigo_Proveedor", entidad.Proveedor.Codigo)
                End If
                conexion.AgregarParametroSQL("@par_Intentos_Ingreso", entidad.IntentosIngreso)
                conexion.AgregarParametroSQL("@par_Mensaje_Banner", entidad.Mensaje)
                conexion.AgregarParametroSQL("@par_Consulta_Otras_Oficinas", entidad.ConsultaOtrasOficinas)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_usuarios")
                While resultado.Read
                    entidad.Codigo = Val(Convert.ToInt64(resultado.Item("Codigo").ToString()))

                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If

                Return entidad.Codigo
            End Using
        End Function

        Public Overrides Function Modificar(entidad As Usuarios) As Long
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                Dim resultado As IDataReader
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                If entidad.OpcionCambioClave > 0 Then
                    conexion.AgregarParametroSQL("@par_Clave", entidad.Clave)
                    resultado = conexion.ExecuteReaderStoreProcedure("sp_modificar_clave_usuario")

                Else
                    conexion.AgregarParametroSQL("@par_Codigo_Usuario", entidad.CodigoUsuario)
                    conexion.AgregarParametroSQL("@par_Codigo_Alterno", entidad.CodigoAlterno)
                    conexion.AgregarParametroSQL("@par_Nombre", entidad.Nombre)
                    conexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
                    conexion.AgregarParametroSQL("@par_Habilitado", entidad.Habilitado)
                    conexion.AgregarParametroSQL("@par_Clave", entidad.Clave)
                    conexion.AgregarParametroSQL("@par_Dias_Cambio_Clave", entidad.DiasCambioClave)
                    conexion.AgregarParametroSQL("@par_Manager", entidad.Manager)
                    conexion.AgregarParametroSQL("@par_Login", entidad.Login)
                    conexion.AgregarParametroSQL("@par_CATA_APLI_Codigo", entidad.AplicacionUsuario)
                    If Not IsNothing(entidad.Empleado) Then
                        If entidad.Empleado.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Empleado", entidad.Empleado.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.Cliente) Then
                        If entidad.Cliente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.Conductor) Then
                        If entidad.Conductor.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
                        End If
                    End If
                    If Not IsNothing(entidad.Proveedor) Then
                        If entidad.Proveedor.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Proveedor", entidad.Proveedor.Codigo)
                        End If
                    End If
                    conexion.AgregarParametroSQL("@par_Intentos_Ingreso", entidad.IntentosIngreso)
                    conexion.AgregarParametroSQL("@par_Mensaje_Banner", entidad.Mensaje)
                    conexion.AgregarParametroSQL("@par_Consulta_Otras_Oficinas", entidad.ConsultaOtrasOficinas)
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_modificar_usuarios")
                End If



                While resultado.Read
                    entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                End While

                resultado.Close()

                If entidad.Codigo.Equals(Cero) Then
                    Return Cero
                End If
            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As Usuarios) As Usuarios
            Dim item As New Usuarios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Correo) And filtro.Correo <> "" Then
                    conexion.AgregarParametroSQL("@par_Correo", filtro.Correo)

                    If Not IsNothing(filtro.CodigoUsuario) Then
                        If filtro.CodigoUsuario <> "" Then
                            conexion.AgregarParametroSQL("@par_Codigo_Usuario", filtro.CodigoUsuario)
                        End If
                    End If
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_usuario_correo")

                    While resultado.Read
                        item = New Usuarios(resultado)
                    End While

                Else

                    If filtro.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_usuarios")

                    While resultado.Read
                        item = New Usuarios(resultado)
                    End While
                End If


            End Using

            Return item
        End Function

        Public Function Validar(codigoEmpresa As Short, CodigoUsuario As String) As Usuarios

            Dim item As New Usuarios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo_Usuario", CodigoUsuario)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_validar_usuario]")

                While resultado.Read
                    item = New Usuarios(resultado)
                End While

            End Using

            Return item
        End Function

        'Public Function ObtenerHorario(codigoEmpresa As Short, codigoUsuario As Short, dia As Short) As UsuarioHorario
        '    Dim item As New UsuarioHorario

        '    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

        '        conexion.CreateConnection()

        '        conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
        '        conexion.AgregarParametroSQL("@par_CodigoUsuario", codigoUsuario)
        '        conexion.AgregarParametroSQL("@par_Dia", dia)

        '        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_obtener_horario_usuario]")

        '        While resultado.Read
        '            item = New UsuarioHorario(resultado)
        '        End While

        '    End Using

        '    Return item
        'End Function

        Public Function ContarUsuariosConectados(codigoEmpresa As Short) As Short
            Dim cantidad As Short = 0

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[sp_obtener_usuarios_conectados]")

                While resultado.Read
                    cantidad = resultado.Item("Cantidad")
                End While

            End Using

            Return cantidad
        End Function

        Public Function ConsultarUsuarioGrupoUsuarios(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of UsuarioGrupoUsuarios)
            Dim lista As New List(Of UsuarioGrupoUsuarios)
            Dim item As UsuarioGrupoUsuarios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", codigoUsuario)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_usuario_grupo_usuarios]")

                While resultado.Read
                    item = New UsuarioGrupoUsuarios(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function GuardarIngresoUsuario(codigoEmpresa As Short, codigoUsuario As Short) As Boolean
            Dim guardo As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", codigoUsuario)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_guardar_ingreso_usuario]")

                While resultado.Read
                    guardo = IIf((resultado.Item("Login")) > 0, True, False)
                End While

            End Using

            Return guardo
        End Function

        Public Function ActualizarIntentosIngreso(codigoEmpresa As Short, codigoUsuario As Short) As Short
            Dim intentos As Short

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", codigoUsuario)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_actualizar_intentos_ingreso_usuario]")

                While resultado.Read
                    intentos = (resultado.Item("Intentos"))
                End While

            End Using

            Return intentos
        End Function

        Public Function DeshabilitarUsuario(codigoEmpresa As Short, codigoUsuario As Short) As Boolean
            Dim deshabilito As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", codigoUsuario)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_deshabilitar_ingreso_usuario]")

                While resultado.Read
                    deshabilito = IIf((resultado.Item("Habilitado")) = 0, True, False)
                End While

            End Using

            Return deshabilito
        End Function

        Public Function InsertarUsuarioGrupoSeguridades(detalles As IEnumerable(Of GrupoUsuarios)) As Long
            Dim lonResultado As Long = 0

            Call EliminarGrupoSeguridadUsuarios(detalles(0).CodigoEmpresa, detalles(0).CodigoUsuario) 'Se eliminan los grupos que tenga asociados el usuario

            If detalles(0).Codigo <> 0 Then
                For Each Grupo In detalles
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        conexion.CreateConnection()

                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", Grupo.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_GRSE_Codigo", Grupo.Codigo)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo", Grupo.CodigoUsuario)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", Grupo.UsuarioCrea.Codigo)

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_usuario_grupo_usuarios")
                        While resultado.Read
                            Grupo.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                        End While

                        resultado.Close()

                        If Grupo.Codigo.Equals(Cero) Then
                            lonResultado = Cero
                            Exit For
                        End If

                        lonResultado = Grupo.Codigo
                    End Using

                Next
                Return lonResultado
            Else
                lonResultado = 1
            End If
            Return lonResultado
        End Function

        Public Sub EliminarGrupoSeguridadUsuarios(Optional codigoEmpresa As Short = 0, Optional codigoUsuario As Short = 0)
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo", codigoUsuario)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_usuario_grupo_usuarios")

                End Using

                transaccion.Complete()
            End Using
        End Sub

        'Public Function InsertarUsuarioHorarios(detalles As IEnumerable(Of UsuarioHorario)) As Long

        '    Dim lonResultado As Long = 0
        '    If detalles.Count <> 0 Then
        '        Call EliminarUsuarioHorarios(detalles(0).CodigoEmpresa, detalles(0).CodigoUsuario)
        '    End If

        '    For Each Horario In detalles

        '        Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
        '            conexion.CreateConnection()

        '            conexion.AgregarParametroSQL("@par_EMPR_Codigo", Horario.CodigoEmpresa)
        '            conexion.AgregarParametroSQL("@par_USUA_Codigo", Horario.CodigoUsuario)
        '            conexion.AgregarParametroSQL("@par_CATA_DISE_Codigo", Horario.DiaSemana.Codigo)
        '            conexion.AgregarParametroSQL("@par_Cata_HOMI_Codigo_Inicial", Horario.HoraMilitarInicial.Codigo)
        '            conexion.AgregarParametroSQL("@par_Cata_HOMI_Codigo_Final", Horario.HoraMilitarFinal.Codigo)

        '            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_insertar_usuario_horarios")
        '            While resultado.Read
        '                Horario.CodigoUsuario = Convert.ToInt64(resultado.Item("USUA_Codigo").ToString())
        '            End While

        '            resultado.Close()

        '            If Horario.CodigoUsuario.Equals(Cero) Then
        '                lonResultado = Cero
        '                Exit For
        '            End If

        '            lonResultado = Horario.CodigoUsuario
        '        End Using

        '    Next

        '    Return lonResultado

        'End Function

        Public Sub EliminarUsuarioHorarios(codigoEmpresa As Short, codigoUsuario As Short)
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo", codigoUsuario)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_eliminar_usuario_horarios")

                End Using

                transaccion.Complete()
            End Using
        End Sub

        'Public Function ConsultarUsuarioHorario(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of UsuarioHorario)
        '    Dim item As New UsuarioHorario
        '    Dim lista As New List(Of UsuarioHorario)
        '    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

        '        conexion.CreateConnection()

        '        conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
        '        conexion.AgregarParametroSQL("@par_CodigoUsuario", codigoUsuario)

        '        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("sp_consultar_usuario_horarios")

        '        While resultado.Read
        '            item = New UsuarioHorario(resultado)
        '            lista.Add(item)
        '        End While
        '    End Using
        '    Return lista

        'End Function

        Public Function ConsultarUsuarioOficinas(codigoEmpresa As Short, codigoUsuario As Short) As IEnumerable(Of UsuarioOficinas)
            Dim lista As New List(Of UsuarioOficinas)
            Dim item As UsuarioOficinas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", codigoUsuario)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_usuario_oficinas")

                While resultado.Read
                    item = New UsuarioOficinas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function InsertarUsuarioOficinas(detalles As IEnumerable(Of UsuarioOficinas)) As Long
            Dim lonResultado As Long = 0
            If detalles.Count <> 0 Then
                Call EliminarUsuarioOficinas(detalles(0).CodigoEmpresa, detalles(0).CodigoUsuario)
            End If

            For Each Oficina In detalles

                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", Oficina.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo", Oficina.CodigoUsuario)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_CAJA_Codigo", Oficina.CodigoCaja)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_usuario_oficinas")
                    While resultado.Read
                        Oficina.Codigo = Convert.ToInt64(resultado.Item("OFIC_Codigo").ToString())
                    End While

                    resultado.Close()

                    If Oficina.Codigo.Equals(Cero) Then
                        lonResultado = Cero
                        Exit For
                    End If

                    lonResultado = Oficina.Codigo
                End Using
            Next

            Return lonResultado
        End Function

        Public Sub EliminarUsuarioOficinas(codigoEmpresa As Short, codigoUsuario As Short)
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo", codigoUsuario)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_usuario_oficinas")
                End Using
                transaccion.Complete()
            End Using
        End Sub
        Public Function CerrarSesion(codigoEmpresa As Short, codigoUsuario As Short) As Boolean
            Dim cerrar As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", codigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", codigoUsuario)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_cerrar_sesion")

                While resultado.Read
                    cerrar = IIf((resultado.Item("Codigo")) > 0, True, False)
                End While
            End Using
            Return cerrar
        End Function

        Public Function RegistrarActividad(entidad As Usuarios) As Boolean
            Dim inserto As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_registrar_actividad_usuarios]")
                While resultado.Read
                    inserto = IIf((resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While
                resultado.Close()

            End Using

            Return inserto
        End Function

    End Class
End Namespace
