﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class RepositorioCerrarOrdenServicio
        Inherits RepositorioBase(Of CerrarOrdenServicio)

        Public Overrides Function Consultar(filtro As CerrarOrdenServicio) As IEnumerable(Of CerrarOrdenServicio)
            Dim lista As New List(Of CerrarOrdenServicio)
            Dim item As CerrarOrdenServicio
            Dim resultado As IDataReader

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.OrdenServicio) Then
                    If filtro.OrdenServicio.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_ENOS_Numero", filtro.OrdenServicio.NumeroDocumento)
                    End If
                    If filtro.OrdenServicio.TipoDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_ENOS_TIDO_Codigo", filtro.OrdenServicio.TipoDocumento)
                    End If
                    If filtro.OrdenServicio.Estado >= 0 And filtro.OrdenServicio.Estado <> 2 Then
                        conexion.AgregarParametroSQL("@par_ENOS_Estado", filtro.OrdenServicio.Estado)
                    End If
                    If filtro.OrdenServicio.Anulado >= 0 Then
                        conexion.AgregarParametroSQL("@par_ENOS_Anulado", filtro.OrdenServicio.Anulado)
                    End If
                    If Not IsNothing(filtro.OrdenServicio.Cliente) Then
                        If filtro.OrdenServicio.Cliente.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", filtro.OrdenServicio.Cliente.Codigo)
                        End If
                    End If
                End If

                If Not IsNothing(filtro.EstadoProgramacion) Then
                    If filtro.EstadoProgramacion.Codigo >= 0 Then
                        conexion.AgregarParametroSQL("@par_CATA_ESSO", filtro.EstadoProgramacion.Codigo)
                    End If
                End If

                If filtro.FechaInicial <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.DateTime)
                End If
                If filtro.FechaFinal <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.DateTime)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_cerrar_orden_servicio")

                While resultado.Read
                    item = New CerrarOrdenServicio(resultado)
                    lista.Add(item)
                End While

            End Using

            resultado.Dispose()
            Return lista

        End Function

        Public Overrides Function Insertar(entidad As CerrarOrdenServicio) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As CerrarOrdenServicio) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As CerrarOrdenServicio) As CerrarOrdenServicio
            Throw New NotImplementedException()
        End Function

        Public Function CerrarOrdenServicio(entidad As CerrarOrdenServicio) As Boolean
            Dim Cerrar As Boolean = False

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.OrdenServicio.Numero)
                conexion.AgregarParametroSQL("@par_ENOS_TIDO_Codigo", entidad.OrdenServicio.TipoDocumento)
                conexion.AgregarParametroSQL("@par_CATA_CCOS", entidad.CausalCierre.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Cierre", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_Observaciones_Cierre", entidad.Observaciones)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_proceso_cerrar_orden_servicio")

                While resultado.Read
                    Cerrar = IIf((resultado.Item("Numero")) > 0, True, False)
                End While

            End Using

            Return Cerrar
        End Function

    End Class
End Namespace

