﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class RepositorioDetalleConceptoSolicitudOrdenServicios
        Inherits RepositorioBase(Of DetalleConceptoSolicitudOrdenServicios)
        Public Overrides Function Consultar(filtro As DetalleConceptoSolicitudOrdenServicios) As IEnumerable(Of DetalleConceptoSolicitudOrdenServicios)
            Dim lista As New List(Of DetalleConceptoSolicitudOrdenServicios)
            Dim item As DetalleConceptoSolicitudOrdenServicios

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_grupo_usuarios")
                While resultado.Read
                    item = New DetalleConceptoSolicitudOrdenServicios(resultado)
                    lista.Add(item)
                End While
            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As DetalleConceptoSolicitudOrdenServicios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Modificar(entidad As DetalleConceptoSolicitudOrdenServicios) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As DetalleConceptoSolicitudOrdenServicios) As DetalleConceptoSolicitudOrdenServicios
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DetalleConceptoSolicitudOrdenServicios) As Boolean
            Throw New NotImplementedException()
        End Function

    End Class
End Namespace
