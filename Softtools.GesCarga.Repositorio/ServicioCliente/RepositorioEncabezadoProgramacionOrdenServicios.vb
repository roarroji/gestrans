﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Despachos
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Entidades.Despachos.OrdenCargue
Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Entidades.Despachos.Planilla
Imports Softtools.GesCarga.Entidades.ControlViajes
Imports Softtools.GesCarga.Repositorio.ControlViajes

Namespace ServicioCliente
    Public NotInheritable Class RepositorioEncabezadoProgramacionOrdenServicios
        Inherits RepositorioBase(Of EncabezadoProgramacionOrdenServicios)
        Public Overrides Function Consultar(filtro As EncabezadoProgramacionOrdenServicios) As IEnumerable(Of EncabezadoProgramacionOrdenServicios)
            Dim lista As New List(Of EncabezadoProgramacionOrdenServicios)
            Dim item As EncabezadoProgramacionOrdenServicios
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                If filtro.NumeroOrden > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Orden", filtro.NumeroOrden)
                End If
                If filtro.FechaInicial <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal <> Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If
                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Nombre <> String.Empty Then
                        conexion.AgregarParametroSQL("@par_Cliente", filtro.Cliente.Nombre)
                    End If
                End If
                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If
                If filtro.EstadoProgramacion.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.EstadoProgramacion.Codigo)
                End If
                If filtro.ConsultaDetalle > 0 Then
                    If Not IsNothing(filtro.UsuarioConsulta) Then
                        If filtro.UsuarioConsulta.Codigo > Cero Then
                            conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                        End If
                    End If
                    If filtro.ID_Detalle > 0 Then
                        conexion.AgregarParametroSQL("@par_ID_Detalle", filtro.ID_Detalle)
                    End If

                    Dim listaDetalle As New List(Of DetalleProgramacionOrdenServicios)
                    Dim itemDetalle As DetalleProgramacionOrdenServicios
                    If filtro.ConsultaInicial > 0 And filtro.ID_Detalle = 0 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_despacho_programacion_orden_servicios_inicial")
                    Else
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_despacho_programacion_orden_servicios")

                    End If
                    While resultado.Read
                        itemDetalle = New DetalleProgramacionOrdenServicios(resultado)
                        listaDetalle.Add(itemDetalle)
                    End While
                    item = New EncabezadoProgramacionOrdenServicios With {.Numero = 1, .Detalle = listaDetalle, .CodigoEmpresa = filtro.CodigoEmpresa}
                    lista.Add(item)

                Else
                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_encabezado_programacion_orden_servicios")
                    While resultado.Read
                        item = New EncabezadoProgramacionOrdenServicios(resultado)
                        lista.Add(item)
                    End While
                End If

            End Using

            resultado.Dispose()

            Return lista
        End Function


        Public Overrides Function Insertar(entidad As EncabezadoProgramacionOrdenServicios) As Long
            Dim inserto As Boolean = True
            Dim NumeroDetalle As Integer
            Dim insertoDetalle As Boolean = True
            Dim EnvioCorreo As Boolean = False
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    If entidad.IDdetalle > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", entidad.Codigo)
                        conexion.AgregarParametroSQL("@par_ID_Detalle", entidad.IDdetalle)
                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_generar_Programacion_detalle_solicitud_orden_servicios")
                        While resultado.Read
                            entidad.Numero = Convert.ToInt64(resultado.Item("Codigo").ToString())
                        End While

                        resultado.Close()

                        If entidad.Numero.Equals(Cero) Then
                            Return Cero
                        End If

                    Else
                        If Not IsNothing(entidad.OrdenServicio) Then
                            conexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.OrdenServicio.Numero)
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.OrdenServicio.Cliente.Codigo)
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.OrdenServicio.SitioCargue.SitioCliente.Codigo)
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.OrdenServicio.SitioDescargue.SitioCliente.Codigo)
                            conexion.AgregarParametroSQL("@par_Cantidad_Orden_Servicio", entidad.OrdenServicio.ValorTipoDespacho)
                            conexion.AgregarParametroSQL("@par_Saldo_Orden_Servicio", entidad.OrdenServicio.SaldoTipoDespacho)
                            conexion.AgregarParametroSQL("@par_Tipo_Despacho", entidad.OrdenServicio.TipoDespacho.Codigo)
                            conexion.AgregarParametroSQL("@par_Ruta_Codigo", entidad.OrdenServicio.Ruta.Codigo)
                            conexion.AgregarParametroSQL("@par_Tarifa", entidad.OrdenServicio.Tarifa.Codigo)
                            conexion.AgregarParametroSQL("@par_Tipo_Vehiculo", entidad.OrdenServicio.TipoVehiculo.Codigo)
                            conexion.AgregarParametroSQL("@par_Ciudad_Origen", entidad.OrdenServicio.CiudadCargue.Codigo)
                            conexion.AgregarParametroSQL("@par_Ciudad_Destino", entidad.OrdenServicio.CiudadDescargue.Codigo)

                        End If
                        conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_Insertar_Encabezado_Programacion_Orden_Servicios")

                        While resultado.Read
                            entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                        End While

                        resultado.Close()

                        If entidad.Numero <= 0 Then
                            Return entidad.Numero
                        Else
                            For Each detalle In entidad.Detalle
                                If detalle.ValorDeclaradoAutorizado = 0 Then
                                    EnvioCorreo = True
                                End If
                                detalle.CodigoEmpresa = entidad.CodigoEmpresa
                                detalle.Numero = entidad.Numero
                                detalle.UsuarioCrea = entidad.UsuarioCrea
                                If Not InsertarDetalle(detalle, conexion, NumeroDetalle, 0) Then
                                    insertoDetalle = False
                                    inserto = False
                                    Exit For
                                End If
                            Next
                        End If
                    End If
                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using
            If EnvioCorreo Then
                Dim detalleCorreo As DetalleProgramacionOrdenServicios
                detalleCorreo = New DetalleProgramacionOrdenServicios With {.CodigoEmpresa = entidad.CodigoEmpresa, .Numero = entidad.Numero, .UsuarioCrea = entidad.UsuarioCrea, .Codigo = entidad.Numero}
                EnviarCorreo(detalleCorreo)
            End If
            Return entidad.Numero
        End Function


        Public Function InsertarDetalle(entidad As DetalleProgramacionOrdenServicios, ByRef contextoConexion As Conexion.DataBaseFactory, ByRef NumeroDetalle As Integer, ActualizaDespacho As Short) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_EPOS_Numero", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_ID", entidad.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.DateTime)
            contextoConexion.AgregarParametroSQL("@par_Cantidad_Despacho", entidad.CantidadDespacho, SqlDbType.Float)
            contextoConexion.AgregarParametroSQL("@par_Valor_Flete_Cliente", entidad.FleteCliente, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Valor_Felte_Transportador", entidad.FleteTransportador, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Valor_Declarado", entidad.ValorDeclarado, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Valor_Declarado_Autorizado", entidad.ValorDeclaradoAutorizado)
            contextoConexion.AgregarParametroSQL("@par_Valor_Cargue_Cliente", entidad.ValorCargueCliente)
            contextoConexion.AgregarParametroSQL("@par_Valor_Descargue_Cliente", entidad.ValorDescargueCliente)
            contextoConexion.AgregarParametroSQL("@par_Valor_Cargue_Transportador", entidad.ValorCargueTransportador)
            contextoConexion.AgregarParametroSQL("@par_Valor_Descargue_Transportador", entidad.ValorDescargueTransportador)
            If entidad.FechaCargue > Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_Fecha_Cargue", entidad.FechaCargue, SqlDbType.DateTime)
            End If
            If entidad.FechaDescargue > Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_Fecha_Descargue", entidad.FechaDescargue, SqlDbType.DateTime)
            End If
            If Not IsNothing(entidad.Vehiculo) Then
                contextoConexion.AgregarParametroSQL("@par_VEHI_Placa", entidad.Vehiculo.Codigo)
            End If
            If Not IsNothing(entidad.TipoTarifa) Then
                contextoConexion.AgregarParametroSQL("@par_TTTC_Codigo", entidad.TipoTarifa.Codigo)
            End If
            If Not IsNothing(entidad.Producto) Then
                contextoConexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Producto.Codigo)
            End If
            contextoConexion.AgregarParametroSQL("@par_Numero_Contenedor", entidad.NumeroContenedor)
            If Not IsNothing(entidad.Conductor) Then
                contextoConexion.AgregarParametroSQL("@par_TERC_Codigo_Conductor", entidad.Conductor.Codigo)
            End If
            If Not IsNothing(entidad.Ruta) Then
                contextoConexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
            End If
            If Not IsNothing(entidad.Tarifa) Then
                contextoConexion.AgregarParametroSQL("@par_TATC_Codigo", entidad.Tarifa.Codigo)
            End If
            If Not IsNothing(entidad.CiudadDescargue) Then
                contextoConexion.AgregarParametroSQL("@par_CIUD_Codigo_Descargue", entidad.CiudadDescargue.Codigo)
            End If
            If Not IsNothing(entidad.SitioDescargue) Then
                contextoConexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.SitioDescargue.Codigo)
            End If
            If Not IsNothing(entidad.PermisoInvias) Then
                contextoConexion.AgregarParametroSQL("@par_Permiso_Invias", entidad.PermisoInvias)
            End If
            If entidad.Cantidad > 0 Then
                contextoConexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad, SqlDbType.Int)
            End If
            If entidad.Modifica > 0 Then
                contextoConexion.AgregarParametroSQL("@par_Modifica", 1)
            End If
            If entidad.Editado > 0 Then
                contextoConexion.AgregarParametroSQL("@par_Editado", entidad.Editado)
            End If

            If entidad.EditaValoresManejoObservaciones > 0 Then
                contextoConexion.AgregarParametroSQL("@par_Edita_Valores_Manejo_Observaciones", entidad.EditaValoresManejoObservaciones)
            End If
            contextoConexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)

            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_Insertar_Detalle_Programacion_Orden_Servicios]")
            While resultado.Read
                entidad.NumeroDocumento = Convert.ToInt64(resultado.Item("Numero").ToString())
            End While
            resultado.Close()

            If entidad.ValorDeclaradoAutorizado = 0 And entidad.NumeroDocumento > 0 Then
                Dim Autorizacion = New Autorizaciones
                Autorizacion.CodigoEmpresa = entidad.CodigoEmpresa
                Autorizacion.TipoAutorizacion = New ValorCatalogos With {.Codigo = CODIGO_CATALOGO_TIPO_AUTORIZACION_VALOR_DECLARADO}
                Autorizacion.UsuarioSolicita = entidad.UsuarioCrea
                Autorizacion.ValorDeclaradoAutorizacion = entidad.ValorDeclarado
                Autorizacion.NumeroDocumento = entidad.NumeroDocumento
                Autorizacion.Observaciones = "Cambio valor declarado Programación No: " + entidad.Numero.ToString() + ". Código No : " + entidad.NumeroDocumento.ToString() + ". Valor Solicitado: $" + Format(entidad.ValorDeclarado, "##,##")
                Dim repAutorizacion = New RepositorioAutorizaciones()
                inserto = If(repAutorizacion.InsertarAutorizacion(Autorizacion, contextoConexion) > 0, True, False)


            End If
            Return inserto
        End Function


        Public Function EnviarCorreo(entidad As DetalleProgramacionOrdenServicios) As Boolean

            Dim inserto As Boolean = True

            entidad.TipoDocumento = 310
            Dim Correos As New Repositorio.Utilitarios.RepositorioCorreos
            Correos.GenerarCorreo(entidad)

            Return inserto
        End Function

        Public Overrides Function Modificar(entidad As EncabezadoProgramacionOrdenServicios) As Long
            Dim inserto As Boolean = True
            Dim NumeroDetalle As Integer
            Dim insertoDetalle As Boolean = True
            Dim EnvioCorreo As Boolean = False
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)

                    If Not IsNothing(entidad.OrdenServicio) Then
                        conexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.OrdenServicio.Numero)
                        conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.OrdenServicio.SitioCargue.SitioCliente.Codigo)
                        conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.OrdenServicio.SitioDescargue.SitioCliente.Codigo)
                        conexion.AgregarParametroSQL("@par_Tipo_Despacho", entidad.OrdenServicio.TipoDespacho.Codigo)
                        conexion.AgregarParametroSQL("@par_Ruta_Codigo", entidad.OrdenServicio.Ruta.Codigo)
                        conexion.AgregarParametroSQL("@par_Tarifa", entidad.OrdenServicio.Tarifa.Codigo)
                        conexion.AgregarParametroSQL("@par_Tipo_Vehiculo", entidad.OrdenServicio.TipoVehiculo.Codigo)
                        conexion.AgregarParametroSQL("@par_Ciudad_Origen", entidad.OrdenServicio.CiudadCargue.Codigo)
                        conexion.AgregarParametroSQL("@par_Ciudad_Destino", entidad.OrdenServicio.CiudadDescargue.Codigo)
                    End If

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_Encabezado_Programacion_Orden_Servicios")

                    While resultado.Read
                        entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                    End While

                    resultado.Close()

                    If entidad.Numero.Equals(Cero) Then
                        Return Cero
                    End If

                    For Each detalle In entidad.Detalle
                        If detalle.ValorDeclaradoAutorizado = 0 Then
                            EnvioCorreo = True
                        End If
                        detalle.CodigoEmpresa = entidad.CodigoEmpresa
                        detalle.Numero = entidad.Numero
                        detalle.UsuarioCrea = entidad.UsuarioCrea
                        If Not InsertarDetalle(detalle, conexion, NumeroDetalle, 0) Then
                            insertoDetalle = False
                            inserto = False
                            Exit For
                        End If
                    Next
                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Numero = Cero
                End If

            End Using
            If EnvioCorreo Then
                Dim detalleCorreo As DetalleProgramacionOrdenServicios
                detalleCorreo = New DetalleProgramacionOrdenServicios With {.CodigoEmpresa = entidad.CodigoEmpresa, .Numero = entidad.Numero, .UsuarioCrea = entidad.UsuarioCrea, .Codigo = entidad.Numero}
                EnviarCorreo(detalleCorreo)
            End If
            Return entidad.Numero
        End Function

        Public Overrides Function Obtener(filtro As EncabezadoProgramacionOrdenServicios) As EncabezadoProgramacionOrdenServicios
            Dim item As New EncabezadoProgramacionOrdenServicios
            Dim resultado As IDataReader
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    '---------- ENCABEZADO ----------------------------

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_programacion_orden_servicios")

                    While resultado.Read
                        item = New EncabezadoProgramacionOrdenServicios(resultado)
                    End While



                    '------------ DETALLE DESPACHO ---------------------------------------------

                    Dim lista As New List(Of DetalleProgramacionOrdenServicios)
                    Dim Detalle As DetalleProgramacionOrdenServicios


                    conexion.CleanParameters()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_programacion_orden_servicios]")

                    While resultado.Read
                        Detalle = New DetalleProgramacionOrdenServicios(resultado)
                        lista.Add(Detalle)
                    End While
                    item.Detalle = lista
                    conexion.CloseConnection()
                    resultado.Close()


                End Using


            End Using

            Return item
        End Function
        Public Function Anular(entidad As EncabezadoProgramacionOrdenServicios) As EncabezadoProgramacionOrdenServicios
            Dim item As New EncabezadoProgramacionOrdenServicios
            Dim itemDocumento As EncabezadoDocumentos
            Dim lista As New List(Of EncabezadoDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_encabezado_solicitud_orden_servicio")

                While resultado.Read
                    itemDocumento = New EncabezadoDocumentos With {
                     .OrdenCargue = New OrdenCargue With {.Numero = Read(resultado, "ENOC_Numero_Documento")},
                     .Remesa = New Remesas With {.Numero = Read(resultado, "ENRE_Numero_Documento")},
                     .Planilla = New Planillas With {.Numero = Read(resultado, "ENPD_Numero_Documento")},
                     .Manifiesto = New Manifiesto With {.Numero = Read(resultado, "ENMC_Numero_Documento")}
                     }
                    lista.Add(itemDocumento)
                    item.Anulado = Read(resultado, "Anulo")

                End While
                'item.DocumentosRelacionados = lista

            End Using

            Return item
        End Function

        Public Function EliminarDetalle(entidad As DetalleProgramacionOrdenServicios) As Long
            Dim elimino As Short
            Dim itemDocumento As EncabezadoDocumentos
            Dim lista As New List(Of EncabezadoDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero_Programacion", entidad.NumeroProgramacion)
                conexion.AgregarParametroSQL("@par_ID", entidad.Codigo)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_eliminar_detalle_programacion_orden_servicio")

                While resultado.Read

                    elimino = Read(resultado, "Numero")

                End While
                'item.DocumentosRelacionados = lista

            End Using

            Return elimino
        End Function

    End Class
End Namespace
