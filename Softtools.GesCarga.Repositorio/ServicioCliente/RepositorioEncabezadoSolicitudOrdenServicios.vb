﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Despachos
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Entidades.Despachos.OrdenCargue
Imports Softtools.GesCarga.Entidades.Despachos.Remesa
Imports Softtools.GesCarga.Entidades.Despachos.Planilla

Namespace ServicioCliente
    Public NotInheritable Class RepositorioEncabezadoSolicitudOrdenServicios
        Inherits RepositorioBase(Of EncabezadoSolicitudOrdenServicios)
        Public Overrides Function Consultar(filtro As EncabezadoSolicitudOrdenServicios) As IEnumerable(Of EncabezadoSolicitudOrdenServicios)
            Dim lista As New List(Of EncabezadoSolicitudOrdenServicios)
            Dim item As EncabezadoSolicitudOrdenServicios
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If
                If filtro.NumeroDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                End If
                If filtro.TipoDocumento > 0 Then
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                End If
                If filtro.ConsultaInicial = 0 Then
                    If filtro.FechaInicial <> Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                    End If
                    If filtro.FechaFinal <> Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                    End If
                End If
                If Not IsNothing(filtro.OficinaDespacha) Then
                    If filtro.OficinaDespacha.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Despacha", filtro.OficinaDespacha.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", filtro.Cliente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                If filtro.EstadoSolicitud > 0 Then
                    conexion.AgregarParametroSQL("@par_EstadoSolicitud", filtro.EstadoSolicitud)
                End If

                If filtro.CodigoCliente > 0 Then
                    conexion.AgregarParametroSQL("@par_CLIE_Codigo", filtro.CodigoCliente)
                End If

                If filtro.PendienteGenerar = 1 Then
                    conexion.AgregarParametroSQL("@par_ESOS_Numero", Cero)

                End If
                If Not IsNothing(filtro.EstadoOrden) Then
                    If filtro.EstadoOrden.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_Estado_Orden_Servicio", filtro.EstadoOrden.Codigo)
                    End If
                End If
                If filtro.DesdeDespachoSolicitud = True Then
                    If Not IsNothing(filtro.UsuarioConsulta) Then
                        If filtro.UsuarioConsulta.Codigo > Cero Then
                            conexion.AgregarParametroSQL("@par_Usuario_Consulta", filtro.UsuarioConsulta.Codigo)
                        End If
                    End If
                    If filtro.ConsultaInicial > 0 Then
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_despacho_solicitud_orden_servicios_inicial")
                    Else
                        resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_despacho_solicitud_orden_servicios")
                    End If
                Else
                    If filtro.Estado >= 0 And filtro.Estado <> 2 Then
                        conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                        conexion.AgregarParametroSQL("@par_Anulado", 0)

                    ElseIf filtro.Estado = 2 Then
                        conexion.AgregarParametroSQL("@par_Anulado", 1)
                    End If

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_consultar_encabezado_solicitud_orden_servicios")
                End If

                While resultado.Read
                    item = New EncabezadoSolicitudOrdenServicios(resultado, filtro.DesdeDespachoSolicitud)
                    lista.Add(item)
                End While
            End Using

            resultado.Dispose()

            Return lista
        End Function



        Public Overrides Function Insertar(entidad As EncabezadoSolicitudOrdenServicios) As Long
            Dim inserto As Boolean = True
            Dim NumeroDetalle As Integer
            Dim insertoDetalle As Boolean = True
            Dim insertoConceptos As Boolean = True
            Dim insertoPolizas As Boolean = True
            Dim transladartemporal As Boolean = True
            Dim limpiartemporal As Boolean = True
            Dim DetalleDocumentos As New GestionDocumentalDocumentos
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                        conexion.CreateConnection()

                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                        conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                        conexion.AgregarParametroSQL("@par_ETCV_Numero", entidad.TarifarioVentas.Codigo)
                        conexion.AgregarParametroSQL("@par_LNTC_Codigo", entidad.LineaNegocioCarga.Codigo)
                        conexion.AgregarParametroSQL("@par_TLNC_Codigo", entidad.TipoLineaNegocioCarga.Codigo)
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Facturar", entidad.Facturar.Codigo)
                        conexion.AgregarParametroSQL("@par_Documento_Cliente", entidad.DocumentoCliente)
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Comercial", entidad.Comercial.Codigo)
                        conexion.AgregarParametroSQL("@par_Facturar_Despachar", entidad.FacturarDespachar)
                        conexion.AgregarParametroSQL("@par_Facturar_Cantidad_Peso_Cumplido", entidad.FacturarCantidadPeso)
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Destinatario", entidad.Destinatario.Codigo)
                        If Not IsNothing(entidad.Remitente) Then
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Remitente", entidad.Remitente.Codigo)
                        End If
                        If entidad.FechaCargue > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Cargue", entidad.FechaCargue, SqlDbType.DateTime)
                        End If
                        If Not IsNothing(entidad.CiudadCargue) Then
                            conexion.AgregarParametroSQL("@par_CIUD_Codigo_Cargue", entidad.CiudadCargue.Codigo)
                        End If
                        If Not IsNothing(entidad.CiudadDescargue) Then
                            conexion.AgregarParametroSQL("@par_CIUD_Codigo_Descargue", entidad.CiudadDescargue.Codigo)
                        End If
                        If Not IsNothing(entidad.Ruta) Then
                            conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                        End If
                        If Not IsNothing(entidad.Tarifa) Then
                            conexion.AgregarParametroSQL("@par_TTCA_Codigo", entidad.Tarifa.Codigo)
                        End If
                        If Not IsNothing(entidad.Producto) Then
                            conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Producto.Codigo)
                        End If
                        If Not IsNothing(entidad.SitioCargue) Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.SitioCargue.SitioCliente.Codigo)
                        End If
                        If Not IsNothing(entidad.SitioDescargue) Then
                            conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.SitioDescargue.SitioCliente.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Direccion_Cargue", entidad.DireccionCargue)
                        conexion.AgregarParametroSQL("@par_Telefono_Cargue", entidad.TelefonoCargue)
                        conexion.AgregarParametroSQL("@par_Contacto_Cargue", entidad.ContactoCargue)
                        conexion.AgregarParametroSQL("@par_Direccion_Descargue", entidad.DireccionDescargue)
                        conexion.AgregarParametroSQL("@par_Telefono_Descargue", entidad.TelefonoDescargue)
                        conexion.AgregarParametroSQL("@par_Contacto_Descargue", entidad.ContactoDescargue)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo_Depacha", entidad.OficinaDespacha.Codigo)
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                        conexion.AgregarParametroSQL("@par_Aplica_Poliza", entidad.AplicaPoliza)
                        conexion.AgregarParametroSQL("@par_Aplica_Recalculo", entidad.FleteXCantidad)
                        conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                        If Not IsNothing(entidad.TipoDespacho) Then
                            conexion.AgregarParametroSQL("@par_CATA_TDOS_Codigo", entidad.TipoDespacho.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Valor_Tipo_Despacho", entidad.ValorTipoDespacho)
                        conexion.AgregarParametroSQL("@par_Saldo_Tipo_Despacho", entidad.SaldoTipoDespacho)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)


                        If entidad.FechaFinCargue > Date.MinValue Then
                            conexion.AgregarParametroSQL("@par_Fecha_Fin_Cargue", entidad.FechaFinCargue, SqlDbType.Date)
                        End If
                        conexion.AgregarParametroSQL("@par_Horas_Pactadas_Cargue", entidad.HorasCargue)
                        conexion.AgregarParametroSQL("@par_Horas_Pactadas_Descargue", entidad.HorasDescargue)
                        If Not IsNothing(entidad.UnidadEmpaque) Then
                            conexion.AgregarParametroSQL("@par_UEPT_Codigo", entidad.UnidadEmpaque.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Informacion_Completa", entidad.InformacionCompleta)
                        If Not IsNothing(entidad.NovedadCalidad) Then
                            conexion.AgregarParametroSQL("@par_CATA_NCOS_Codigo", entidad.NovedadCalidad.Codigo)
                        End If
                        If Not IsNothing(entidad.SedeFacturacion) Then
                            conexion.AgregarParametroSQL("@par_TEDI_Codigo", entidad.SedeFacturacion.Codigo)
                        End If
                        If Not IsNothing(entidad.TipoCargue) Then
                            conexion.AgregarParametroSQL("@par_CATA_TCOS_Codigo", entidad.TipoCargue.Codigo)
                        End If
                        If Not IsNothing(entidad.TipoVehiculo) Then
                            conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo.Codigo)
                        End If
                        conexion.AgregarParametroSQL("@par_Valor_Declarado", entidad.ValorDeclarado, SqlDbType.Money)
                        conexion.AgregarParametroSQL("@par_Diferentes_Sitios_Descargue", entidad.DiferentesSitiosDescargue)
                        If Not IsNothing(entidad.PermisoInvias) Then
                            conexion.AgregarParametroSQL("@par_Permiso_Invias", entidad.PermisoInvias)
                        End If
                        If Not IsNothing(entidad.FormaPago) Then
                            If entidad.FormaPago.Codigo > 0 Then
                                conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", entidad.FormaPago.Codigo)
                            End If
                        End If

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_encabezado_solicitud_orden_servicios")

                        While resultado.Read
                            entidad.NumeroDocumento = Convert.ToInt64(resultado.Item("Numero_Documento").ToString())
                            entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                        End While

                        resultado.Close()

                        If entidad.Numero <= 0 Then
                            Return entidad.Numero
                        Else

                            DetalleDocumentos.CodigoEmpresa = entidad.CodigoEmpresa
                            DetalleDocumentos.Numero = entidad.Numero
                            DetalleDocumentos.UsuarioCrea = entidad.UsuarioCrea

                            transladartemporal = New RepositorioGestionDocumentalDocumentos().TrasladarDocumentos(DetalleDocumentos, conexionDocumentos)

                            If transladartemporal Then
                                limpiartemporal = New RepositorioGestionDocumentalDocumentos().LimpiarTemporalUsuario(DetalleDocumentos, conexionDocumentos)
                            End If

                            If Not IsNothing(entidad.SolicitudOrdenServicioNumero) Then
                                If entidad.SolicitudOrdenServicioNumero > 0 Then
                                    inserto = ActualizarAsociacionOrdenServicio(entidad, conexion)
                                End If
                            End If

                            For Each detalle In entidad.Detalle
                                detalle.CodigoEmpresa = entidad.CodigoEmpresa
                                detalle.NumeroDocumento = entidad.Numero
                                detalle.Estado = entidad.Estado
                                detalle.UsuarioCrea = entidad.UsuarioCrea
                                detalle.TarifarioVentas = entidad.TarifarioVentas
                                detalle.LineaNegocioCarga = entidad.LineaNegocioCarga
                                If Not InsertarDetalle(detalle, conexion, NumeroDetalle, 0) Then
                                    insertoDetalle = False
                                    inserto = False
                                    Exit For
                                End If
                            Next

                            For Each Conceptos In entidad.Conceptos
                                Conceptos.CodigoEmpresa = entidad.CodigoEmpresa
                                Conceptos.NumeroDocumento = entidad.Numero
                                If Not InsertarConceptos(Conceptos, conexion) Then
                                    insertoConceptos = False
                                    inserto = False
                                    Exit For
                                End If
                            Next

                            For Each Poliza In entidad.Polizas
                                Poliza.CodigoEmpresa = entidad.CodigoEmpresa
                                Poliza.NumeroDocumento = entidad.Numero
                                If Not InsertarPolizas(Poliza, conexion) Then
                                    insertoConceptos = False
                                    inserto = False
                                    Exit For
                                End If
                            Next
                            If Not IsNothing(entidad.ImportacionExportacion) Then
                                If Not IsNothing(entidad.ImportacionExportacion.Naviera) Then
                                    entidad.ImportacionExportacion.CodigoEmpresa = entidad.CodigoEmpresa
                                    entidad.ImportacionExportacion.NumeroDocumento = entidad.Numero
                                    If Not InsertarImpoExpo(entidad.ImportacionExportacion, conexion) Then
                                        inserto = False
                                    End If
                                End If
                            End If

                        End If


                    End Using

                    If inserto Then
                        transaccion.Complete()
                    Else
                        entidad.NumeroDocumento = Cero
                    End If

                End Using

                Dim Documento = New RepositorioGestionDocumentalDocumentos
                If Not IsNothing(entidad.Documentos) Then
                    For Each detalle In entidad.Documentos
                        detalle.Numero = entidad.Numero
                        If Not Documento.InsertarDetalleDocumento(detalle) Then
                            Exit For
                        End If
                    Next
                End If

            End Using
            If inserto Then
                Dim Correos As New Repositorio.Utilitarios.RepositorioCorreos
                If entidad.Estado = 1 Then
                    Correos.GenerarCorreo(entidad)
                End If
            End If
            Return entidad.NumeroDocumento
        End Function


        Public Function InsertarDetalle(entidad As DetalleSolicitudOrdenServicios, ByRef contextoConexion As Conexion.DataBaseFactory, ByRef NumeroDetalle As Integer, ActualizaDespacho As Short) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.NumeroDocumento)
            contextoConexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
            contextoConexion.AgregarParametroSQL("@par_ETCV_Numero", entidad.TarifarioVentas.Codigo)
            contextoConexion.AgregarParametroSQL("@par_LNTC_Codigo", entidad.LineaNegocioCarga.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TLNC_Codigo", entidad.TipoLineaNegocioCarga.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TATC_Codigo", entidad.Tarifa.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TTTC_Codigo", entidad.TipoTarifa.Codigo)
            contextoConexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Producto.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Cantidad", entidad.Cantidad, SqlDbType.Decimal)
            contextoConexion.AgregarParametroSQL("@par_Peso", entidad.Peso, SqlDbType.Decimal)
            contextoConexion.AgregarParametroSQL("@par_Valor", entidad.ValorFleteCliente, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Flete_Cliente", entidad.FleteCliente, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Valor_Flete_Transportador", entidad.ValorFleteTransportador, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Documento_Cliente", entidad.DocumentoCliente)
            contextoConexion.AgregarParametroSQL("@par_CIUD_Codigo_Cargue", entidad.CiudadCargue.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Fecha_Cargue", entidad.FechaCargue, SqlDbType.DateTime)
            If Not IsNothing(entidad.SitioCargue) Then
                If entidad.SitioCargue.Codigo > 0 Then
                    contextoConexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.SitioCargue.Codigo)
                End If
            End If
            contextoConexion.AgregarParametroSQL("@par_Direccion_Cargue", entidad.DireccionCargue)
            contextoConexion.AgregarParametroSQL("@par_Telefono_Cargue", entidad.TelefonoCargue)
            contextoConexion.AgregarParametroSQL("@par_Contacto_Cargue", entidad.ContactoCargue)
            contextoConexion.AgregarParametroSQL("@par_TERC_Codigo_Destinatario", entidad.Destinatario.Codigo)
            contextoConexion.AgregarParametroSQL("@par_CIUD_Codigo_Destino", entidad.CiudadDestino.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Fecha_Descargue", entidad.FechaDescargue, SqlDbType.DateTime)
            If Not IsNothing(entidad.SitioDescargue) Then
                If entidad.SitioDescargue.Codigo > 0 Then
                    contextoConexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.SitioDescargue.Codigo)
                End If
            End If
            contextoConexion.AgregarParametroSQL("@par_Direccion_Destino", entidad.DireccionDestino)
            contextoConexion.AgregarParametroSQL("@par_Telefono_Destino", entidad.TelefonoDestino)
            contextoConexion.AgregarParametroSQL("@par_Contacto_Destino", entidad.ContactoDestino)
            contextoConexion.AgregarParametroSQL("@par_Numero_Contenedor", entidad.NumeroContenedor)
            contextoConexion.AgregarParametroSQL("@par_MBL_Contenedor", entidad.MBLContenedor)
            contextoConexion.AgregarParametroSQL("@par_HBL_Contenedor", entidad.HBLContenedor)
            contextoConexion.AgregarParametroSQL("@par_DO_Contenedor", entidad.DOContenedor)

            contextoConexion.AgregarParametroSQL("@par_Valor_FOB", entidad.ValorFOB, SqlDbType.Money)
            If Not IsNothing(entidad.SitioDevolucionContenedor) Then
                contextoConexion.AgregarParametroSQL("@par_SICD_Codigo_Devolucion_Contenedor", entidad.SitioDevolucionContenedor.Codigo)
            End If
            If entidad.FechaDevolucionContenedor <> Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_Fecha_Devolucion_Contenedor", entidad.FechaDevolucionContenedor, SqlDbType.DateTime)
            End If
            If Not IsNothing(entidad.CiudadDevolucion) Then
                contextoConexion.AgregarParametroSQL("@par_Ciudad_Devolucion", entidad.CiudadDevolucion.Codigo)
            End If
            If Not IsNothing(entidad.PatioDevolucion) Then
                contextoConexion.AgregarParametroSQL("@par_Patio_Devolucion", entidad.PatioDevolucion.Nombre)
            End If
            If Not IsNothing(entidad.ModoTransporte) Then
                contextoConexion.AgregarParametroSQL("@par_CATA_MOTR_Codigo", entidad.ModoTransporte.Codigo)
            End If
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
            If ActualizaDespacho > 0 Then
                contextoConexion.AgregarParametroSQL("@par_ActualizaDespacho", 1)

            End If
            If Not IsNothing(entidad.PermisoInvias) Then
                contextoConexion.AgregarParametroSQL("@par_Permiso_Invias", entidad.PermisoInvias)
            End If


            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_solicitud_orden_servicios]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                NumeroDetalle = Convert.ToInt64(resultado.Item("Codigo").ToString())
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Function InsertarConceptos(entidad As DetalleConceptoSolicitudOrdenServicios, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.NumeroDocumento)
            contextoConexion.AgregarParametroSQL("@par_COVE_Codigo", entidad.ConceptoVentas.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Descripcion", entidad.Descripcion)
            contextoConexion.AgregarParametroSQL("@par_Valor", entidad.Valor, SqlDbType.Money)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_conceptos_solicitud_orden_servicios]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Function InsertarPolizas(entidad As DetallePolizasSolicitudOrdenServicios, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.NumeroDocumento)
            contextoConexion.AgregarParametroSQL("@par_CATA_TPOS_Codigo", entidad.TipoPolizaOrdenServicio.Codigo)
            contextoConexion.AgregarParametroSQL("@par_TERC_Codigo_Poliza", entidad.Poliza.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Exige_Escolta", entidad.ExigeEscolta)
            contextoConexion.AgregarParametroSQL("@par_Edad_Vehiculos", entidad.EdadVehiculos)
            contextoConexion.AgregarParametroSQL("@par_Valor_Cobertura", entidad.ValorCobertura, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_Valor_Deducible", entidad.ValorDeducible, SqlDbType.Money)
            contextoConexion.AgregarParametroSQL("@par_CATA_HOAP_Codigo", entidad.HorarioAutorizado.Codigo)
            contextoConexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)


            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_polizas_solicitus_orden_servicios]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
            End While

            resultado.Close()

            Return inserto
        End Function


        Public Function InsertarImpoExpo(entidad As ImportacionExportacionSolicitudOrdenServicios, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim inserto As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.NumeroDocumento)
            If Not IsNothing(entidad.Naviera) Then
                contextoConexion.AgregarParametroSQL("@par_CATA_NAVI_Codigo", entidad.Naviera.Codigo)
            End If
            If entidad.ETAMotonave > Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_ETA_Motonave", entidad.ETAMotonave, SqlDbType.Date)
            End If
            contextoConexion.AgregarParametroSQL("@par_Motonave", entidad.Motonave)
            If entidad.FechaDTA > Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_Fecha_DTA", entidad.FechaDTA, SqlDbType.Date)
            End If
            If Not IsNothing(entidad.PaisOrigen) Then
                contextoConexion.AgregarParametroSQL("@par_PAIS_Codigo_Origen", entidad.PaisOrigen.Codigo)
            End If
            If Not IsNothing(entidad.PaisDestino) Then
                contextoConexion.AgregarParametroSQL("@par_PAIS_Codigo_Destino", entidad.PaisDestino.Codigo)
            End If
            If Not IsNothing(entidad.PuertoPaisOrigen) Then
                contextoConexion.AgregarParametroSQL("@par_PUPA_Codigo_Origen", entidad.PuertoPaisOrigen.Codigo)
            End If
            If Not IsNothing(entidad.PuertoPaisDestino) Then
                contextoConexion.AgregarParametroSQL("@par_PUPA_Codigo_Destino", entidad.PuertoPaisDestino.Codigo)
            End If
            contextoConexion.AgregarParametroSQL("@par_Contacto_Puerto_Destino", entidad.ContactoDestino)
            contextoConexion.AgregarParametroSQL("@par_Contacto_Puerto_Origen", entidad.ContactoOrigen)
            contextoConexion.AgregarParametroSQL("@par_Liberacion_Automatica_Fletes", entidad.LiberacionAutomaticaFletes)
            contextoConexion.AgregarParametroSQL("@par_Exoneracion_Pago_Depositos", entidad.ExoneracionPagoDeposito)
            contextoConexion.AgregarParametroSQL("@par_Dias_Libres_Contenedor", entidad.DiasLibresContenedor)
            contextoConexion.AgregarParametroSQL("@par_Exoneracion_Pago_Drop_Off", entidad.ExoneracionPagoDropOFF)
            contextoConexion.AgregarParametroSQL("@par_Cliente_Paga_Flete_Maritimo", entidad.ClientePagaFleteMaritimoRuta)
            contextoConexion.AgregarParametroSQL("@par_Cliente_Paga_Emision_Manejo", entidad.ClientePagoEmisionManejo)
            contextoConexion.AgregarParametroSQL("@par_Cliente_Paga_Liquidacion_Comodato", entidad.ClientePagaLiquidacionComodato)
            If entidad.FechaCutOffDocumental > Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_Fecha_Cut_Off_Documental", entidad.FechaCutOffDocumental, SqlDbType.Date)
            End If
            If entidad.FechaCutOffFisico > Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_Fecha_Cut_Off_Fisico", entidad.FechaCutOffFisico, SqlDbType.Date)
            End If
            contextoConexion.AgregarParametroSQL("@par_BL", entidad.BL)
            contextoConexion.AgregarParametroSQL("@par_Numero_Importacion", entidad.NumeroImportacion)
            contextoConexion.AgregarParametroSQL("@par_Declaracion_Importacion", entidad.DeclaracionImporacion)
            contextoConexion.AgregarParametroSQL("@par_Clase_Importacion", entidad.ClaseImportacion)
            If entidad.FechaCutOffDocumental > Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_Fecha_Cut_Off_Documental", entidad.FechaCutOffDocumental, SqlDbType.Date)
            End If
            contextoConexion.AgregarParametroSQL("@par_Valor_Declarado", entidad.ValorDeclarado, SqlDbType.Money)

            contextoConexion.AgregarParametroSQL("@par_Devolucion_Contenedor", entidad.DevolucionContenedor)
            If Not IsNothing(entidad.CiudadDevolucion) Then
                contextoConexion.AgregarParametroSQL("@par_CIUD_Codigo_Devolucion_Contenedor", entidad.CiudadDevolucion.Codigo)
            End If
            contextoConexion.AgregarParametroSQL("@par_Patio_Devolcuion", entidad.PatioDevolucion)
            If entidad.FechaVencimiento > Date.MinValue Then
                contextoConexion.AgregarParametroSQL("@par_Fecha_Vencimiento", entidad.FechaVencimiento, SqlDbType.Date)
            End If
            contextoConexion.AgregarParametroSQL("@par_Numero_Reserva", entidad.NumeroReserva)
            contextoConexion.AgregarParametroSQL("@par_Patio_Cargue", entidad.PatioCargue)


            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_importacion_exportacion_solicitud_orden_servicios]")

            While resultado.Read
                entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
            End While

            resultado.Close()

            Return inserto
        End Function

        Public Function ActualizarAsociacionOrdenServicio(entidad As EncabezadoSolicitudOrdenServicios, ByRef contextoConexion As Conexion.DataBaseFactory) As Boolean
            Dim Actualizo As Boolean = True

            contextoConexion.CleanParameters()

            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_ENOS_Numero", entidad.Numero) 'orden servicio creada
            contextoConexion.AgregarParametroSQL("@par_ESOS_Numero", entidad.SolicitudOrdenServicioNumero) 'solicitud de orden servicio

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_Asignar_Solicitud_Servicio]")

            While resultado.Read
                If Not (resultado.Item("ESOS_Numero") > 0) Then
                    Actualizo = False
                End If
            End While
            resultado.Close()

            Return Actualizo
        End Function


        Public Overrides Function Modificar(entidad As EncabezadoSolicitudOrdenServicios) As Long
            Dim inserto As Boolean = True
            Dim NumeroDetalle As Integer
            Dim insertoDetalle As Boolean = True
            Dim insertoConceptos As Boolean = True
            Dim insertoPolizas As Boolean = True
            Dim transladartemporal As Boolean = True
            Dim limpiartemporal As Boolean = True
            Dim DetalleDocumentos As New GestionDocumentalDocumentos
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                    Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                        conexion.CreateConnection()
                        If entidad.InsertaDetalle = 0 Then
                            conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                            conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                            conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                            conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                            conexion.AgregarParametroSQL("@par_TLNC_Codigo", entidad.TipoLineaNegocioCarga.Codigo)
                            conexion.AgregarParametroSQL("@par_ETCV_Numero", entidad.TarifarioVentas.Codigo)
                            conexion.AgregarParametroSQL("@par_LNTC_Codigo", entidad.LineaNegocioCarga.Codigo)
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Cliente", entidad.Cliente.Codigo)
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Facturar", entidad.Facturar.Codigo)
                            conexion.AgregarParametroSQL("@par_Documento_Cliente", entidad.DocumentoCliente)
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Comercial", entidad.Comercial.Codigo)
                            conexion.AgregarParametroSQL("@par_Facturar_Despachar", entidad.FacturarDespachar)
                            conexion.AgregarParametroSQL("@par_Facturar_Cantidad_Peso_Cumplido", entidad.FacturarCantidadPeso)
                            conexion.AgregarParametroSQL("@par_TERC_Codigo_Destinatario", entidad.Destinatario.Codigo)
                            If Not IsNothing(entidad.Remitente) Then
                                conexion.AgregarParametroSQL("@par_TERC_Codigo_Remitente", entidad.Remitente.Codigo)
                            End If
                            If entidad.FechaCargue > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Cargue", entidad.FechaCargue, SqlDbType.DateTime)
                            End If
                            If Not IsNothing(entidad.CiudadCargue) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Cargue", entidad.CiudadCargue.Codigo)
                            End If
                            If Not IsNothing(entidad.CiudadDescargue) Then
                                conexion.AgregarParametroSQL("@par_CIUD_Codigo_Descargue", entidad.CiudadDescargue.Codigo)
                            End If
                            If Not IsNothing(entidad.Ruta) Then
                                conexion.AgregarParametroSQL("@par_RUTA_Codigo", entidad.Ruta.Codigo)
                            End If
                            If Not IsNothing(entidad.Tarifa) Then
                                conexion.AgregarParametroSQL("@par_TTCA_Codigo", entidad.Tarifa.Codigo)
                            End If
                            If Not IsNothing(entidad.SitioCargue) Then
                                conexion.AgregarParametroSQL("@par_SICD_Codigo_Cargue", entidad.SitioCargue.SitioCliente.Codigo)
                            End If
                            If Not IsNothing(entidad.SitioDescargue) Then
                                conexion.AgregarParametroSQL("@par_SICD_Codigo_Descargue", entidad.SitioDescargue.SitioCliente.Codigo)
                            End If
                            If Not IsNothing(entidad.Producto) Then
                                conexion.AgregarParametroSQL("@par_PRTR_Codigo", entidad.Producto.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Direccion_Cargue", entidad.DireccionCargue)
                            conexion.AgregarParametroSQL("@par_Telefono_Cargue", entidad.TelefonoCargue)
                            conexion.AgregarParametroSQL("@par_Contacto_Cargue", entidad.ContactoCargue)
                            conexion.AgregarParametroSQL("@par_Direccion_Descargue", entidad.DireccionDescargue)
                            conexion.AgregarParametroSQL("@par_Telefono_Descargue", entidad.TelefonoDescargue)
                            conexion.AgregarParametroSQL("@par_Contacto_Descargue", entidad.ContactoDescargue)
                            conexion.AgregarParametroSQL("@par_OFIC_Codigo_Depacha", entidad.OficinaDespacha.Codigo)
                            conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                            conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                            conexion.AgregarParametroSQL("@par_Aplica_Recalculo", entidad.FleteXCantidad)
                            conexion.AgregarParametroSQL("@par_Aplica_Poliza", entidad.AplicaPoliza)
                            conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                            If Not IsNothing(entidad.TipoDespacho) Then
                                conexion.AgregarParametroSQL("@par_CATA_TDOS_Codigo", entidad.TipoDespacho.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Valor_Tipo_Despacho", entidad.ValorTipoDespacho)
                            conexion.AgregarParametroSQL("@par_Saldo_Tipo_Despacho", entidad.SaldoTipoDespacho)
                            conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                            If entidad.FechaFinCargue > Date.MinValue Then
                                conexion.AgregarParametroSQL("@par_Fecha_Fin_Cargue", entidad.FechaFinCargue, SqlDbType.Date)
                            End If
                            conexion.AgregarParametroSQL("@par_Horas_Pactadas_Cargue", entidad.HorasCargue)
                            conexion.AgregarParametroSQL("@par_Horas_Pactadas_Descargue", entidad.HorasDescargue)
                            If Not IsNothing(entidad.UnidadEmpaque) Then
                                conexion.AgregarParametroSQL("@par_UEPT_Codigo", entidad.UnidadEmpaque.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Informacion_Completa", entidad.InformacionCompleta)
                            If Not IsNothing(entidad.NovedadCalidad) Then
                                conexion.AgregarParametroSQL("@par_CATA_NCOS_Codigo", entidad.NovedadCalidad.Codigo)
                            End If
                            If Not IsNothing(entidad.SedeFacturacion) Then
                                conexion.AgregarParametroSQL("@par_TEDI_Codigo", entidad.SedeFacturacion.Codigo)
                            End If
                            If Not IsNothing(entidad.TipoCargue) Then
                                conexion.AgregarParametroSQL("@par_CATA_TCOS_Codigo", entidad.TipoCargue.Codigo)
                            End If
                            If Not IsNothing(entidad.TipoVehiculo) Then
                                conexion.AgregarParametroSQL("@par_CATA_TIVE_Codigo", entidad.TipoVehiculo.Codigo)
                            End If
                            conexion.AgregarParametroSQL("@par_Valor_Declarado", entidad.ValorDeclarado, SqlDbType.Money)
                            conexion.AgregarParametroSQL("@par_Diferentes_Sitios_Descargue", entidad.DiferentesSitiosDescargue)
                            If Not IsNothing(entidad.PermisoInvias) Then
                                conexion.AgregarParametroSQL("@par_Permiso_Invias", entidad.PermisoInvias)
                            End If
                            If Not IsNothing(entidad.FormaPago) Then
                                If entidad.FormaPago.Codigo > 0 Then
                                    conexion.AgregarParametroSQL("@par_CATA_FPVE_Codigo", entidad.FormaPago.Codigo)
                                End If
                            End If

                            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_modificar_encabezado_solicitud_orden_servicios]")

                            While resultado.Read
                                entidad.NumeroDocumento = Convert.ToInt64(resultado.Item("Numero_Documento").ToString())
                                entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                            End While

                            resultado.Close()
                        End If

                        If entidad.Numero.Equals(Cero) Then
                            Return Cero
                        Else

                            DetalleDocumentos.CodigoEmpresa = entidad.CodigoEmpresa
                            DetalleDocumentos.Numero = entidad.Numero
                            DetalleDocumentos.UsuarioCrea = entidad.UsuarioCrea

                            transladartemporal = New RepositorioGestionDocumentalDocumentos().TrasladarDocumentos(DetalleDocumentos, conexionDocumentos)

                            If transladartemporal Then
                                limpiartemporal = New RepositorioGestionDocumentalDocumentos().LimpiarTemporalUsuario(DetalleDocumentos, conexionDocumentos)
                            End If

                        End If

                        For Each detalle In entidad.Detalle
                            detalle.CodigoEmpresa = entidad.CodigoEmpresa
                            detalle.NumeroDocumento = entidad.Numero
                            detalle.Estado = entidad.Estado
                            detalle.UsuarioCrea = entidad.UsuarioCrea
                            detalle.TarifarioVentas = entidad.TarifarioVentas
                            detalle.LineaNegocioCarga = entidad.LineaNegocioCarga
                            If entidad.InsertaDetalle > 0 Then
                                If Not InsertarDetalle(detalle, conexion, NumeroDetalle, 1) Then
                                    insertoDetalle = False
                                    inserto = False
                                    Exit For
                                End If
                            Else
                                If Not InsertarDetalle(detalle, conexion, NumeroDetalle, 0) Then
                                    insertoDetalle = False
                                    inserto = False
                                    Exit For
                                End If
                            End If

                        Next
                        If Not IsNothing(entidad.Conceptos) Then
                            For Each Conceptos In entidad.Conceptos
                                Conceptos.CodigoEmpresa = entidad.CodigoEmpresa
                                Conceptos.NumeroDocumento = entidad.Numero
                                If Not InsertarConceptos(Conceptos, conexion) Then
                                    insertoConceptos = False
                                    inserto = False
                                    Exit For
                                End If
                            Next
                        End If

                        If Not IsNothing(entidad.Polizas) Then
                            For Each Poliza In entidad.Polizas
                                Poliza.CodigoEmpresa = entidad.CodigoEmpresa
                                Poliza.NumeroDocumento = entidad.Numero
                                If Not InsertarPolizas(Poliza, conexion) Then
                                    insertoConceptos = False
                                    inserto = False
                                    Exit For
                                End If
                            Next
                        End If

                        If Not IsNothing(entidad.ImportacionExportacion) Then
                            entidad.ImportacionExportacion.CodigoEmpresa = entidad.CodigoEmpresa
                            entidad.ImportacionExportacion.NumeroDocumento = entidad.Numero
                            If Not InsertarImpoExpo(entidad.ImportacionExportacion, conexion) Then
                                inserto = False
                            End If
                        End If

                    End Using

                    If inserto Then
                        transaccion.Complete()
                    Else
                        entidad.NumeroDocumento = Cero
                    End If

                End Using

                Dim Documento = New RepositorioGestionDocumentalDocumentos
                If Not IsNothing(entidad.Documentos) Then
                    For Each detalle In entidad.Documentos
                        detalle.Numero = entidad.Numero
                        If Not Documento.InsertarDetalleDocumento(detalle) Then
                            Exit For
                        End If
                    Next
                End If

            End Using
            If inserto Then
                Dim Correos As New Repositorio.Utilitarios.RepositorioCorreos
                If entidad.Estado = 1 Then
                    Correos.GenerarCorreo(entidad)
                End If
            End If
            If entidad.InsertaDetalle > 0 Then
                Return NumeroDetalle
            Else
                Return entidad.NumeroDocumento
            End If

        End Function

        Public Overrides Function Obtener(filtro As EncabezadoSolicitudOrdenServicios) As EncabezadoSolicitudOrdenServicios
            Dim item As New EncabezadoSolicitudOrdenServicios
            Dim resultado As IDataReader
            Using conexionDocumentos = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                conexionDocumentos.CreateConnection()
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    '---------- ENCABEZADO ----------------------------

                    conexion.CreateConnection()

                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                    If filtro.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                    End If

                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)

                    resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_solicitud_orden_servicios")

                    While resultado.Read
                        item = New EncabezadoSolicitudOrdenServicios(resultado)
                    End While

                    Dim Concepto As DetalleConceptoSolicitudOrdenServicios
                    Dim ListaConcepto As New List(Of DetalleConceptoSolicitudOrdenServicios)

                    '------------- DETALLE CONEPTOS --------------------------------

                    conexion.CleanParameters()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ESOS_Numero", filtro.Numero)

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_conceptos_solicitud_orden_servicios]")

                    While resultado.Read
                        Concepto = New DetalleConceptoSolicitudOrdenServicios(resultado)
                        ListaConcepto.Add(Concepto)
                    End While
                    item.Conceptos = ListaConcepto

                    '------------------ DETALLE POLIZAS ----------------------------------

                    conexion.CloseConnection()
                    resultado.Close()

                    Dim Poliza As DetallePolizasSolicitudOrdenServicios
                    Dim ListaPolizas As New List(Of DetallePolizasSolicitudOrdenServicios)
                    conexion.CleanParameters()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ESOS_Numero", filtro.Numero)

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_polizas_solicitus_orden_servicios]")

                    While resultado.Read
                        Poliza = New DetallePolizasSolicitudOrdenServicios(resultado)
                        ListaPolizas.Add(Poliza)
                    End While
                    item.Polizas = ListaPolizas
                    conexion.CloseConnection()
                    resultado.Close()

                    '------------- IMPORTACION EXPORTACION -----------------------------------

                    conexion.CleanParameters()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_ESOS_Numero", filtro.Numero)

                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_importacion_exportacion_solicitud_orden_servicios]")

                    While resultado.Read
                        item.ImportacionExportacion = New ImportacionExportacionSolicitudOrdenServicios(resultado)
                    End While
                    conexion.CloseConnection()
                    resultado.Close()

                    '------------ DETALLE DESPACHO ---------------------------------------------

                    If filtro.DesdeDespachoOrdenServicio Then
                        Dim lista As New List(Of DetalleDespachoSolicitudOrdenServicios)
                        Dim Detalle As DetalleDespachoSolicitudOrdenServicios


                        conexion.CleanParameters()
                        conexion.CreateConnection()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_ESOS_Numero", filtro.Numero)
                        conexion.AgregarParametroSQL("@par_ID_Codigo", filtro.IDRegistroDetalle)

                        resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_despacho_orden_servicios]")

                        While resultado.Read
                            Detalle = New DetalleDespachoSolicitudOrdenServicios(resultado)
                            lista.Add(Detalle)
                        End While
                        item.DetalleDespacho = lista
                        conexion.CloseConnection()
                        resultado.Close()

                    End If

                End Using

                If filtro.ConsularDocumentos > 0 Then
                    Dim lista As New List(Of GestionDocumentalDocumentos)
                    Dim Seccion As GestionDocumentalDocumentos
                    resultado.Close()
                    conexionDocumentos.CleanParameters()
                    conexionDocumentos.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                    conexionDocumentos.AgregarParametroSQL("@par_Numero", filtro.Numero)
                    conexionDocumentos.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)

                    resultado = conexionDocumentos.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_orden_servicio_documentos]")
                    While resultado.Read
                        Seccion = New GestionDocumentalDocumentos(resultado)
                        lista.Add(Seccion)
                    End While
                    item.ListadoDocumentos = lista
                End If

            End Using

            Return item
        End Function

        Public Function Anular(entidad As EncabezadoSolicitudOrdenServicios) As EncabezadoSolicitudOrdenServicios
            Dim item As New EncabezadoSolicitudOrdenServicios
            Dim itemDocumento As EncabezadoDocumentos
            Dim lista As New List(Of EncabezadoDocumentos)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_encabezado_solicitud_orden_servicio")

                While resultado.Read
                    itemDocumento = New EncabezadoDocumentos With {
                     .OrdenCargue = New OrdenCargue With {.Numero = Read(resultado, "ENOC_Numero_Documento")},
                     .Remesa = New Remesas With {.Numero = Read(resultado, "ENRE_Numero_Documento")},
                     .Planilla = New Planillas With {.Numero = Read(resultado, "ENPD_Numero_Documento")},
                     .Manifiesto = New Manifiesto With {.Numero = Read(resultado, "ENMC_Numero_Documento")},
                     .Programacion = New EncabezadoProgramacionOrdenServicios With {.Numero = Read(resultado, "EPOS_Numero")}
                     }
                    lista.Add(itemDocumento)
                    item.Anulado = Read(resultado, "Anulo")

                End While
                item.DocumentosRelacionados = lista

            End Using

            Return item
        End Function

        Public Function ObtenerInformacionControlCupo(filtro As EncabezadoSolicitudOrdenServicios) As EncabezadoSolicitudOrdenServicios
            Dim item As New EncabezadoSolicitudOrdenServicios
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If Not IsNothing(filtro.Cliente) Then
                    If filtro.Cliente.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.Cliente.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.SedeFacturacion) Then
                    If filtro.SedeFacturacion.Codigo > Cero Then
                        conexion.AgregarParametroSQL("@par_SEDE_Codigo", filtro.SedeFacturacion.Codigo)
                    End If

                End If

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_Obtener_Informacion_Control_Cupo")

                While resultado.Read
                    item = New EncabezadoSolicitudOrdenServicios(resultado)
                End While

            End Using
            Return item

        End Function

        Public Function ObtenerOrdenServicioProgramacion(filtro As EncabezadoSolicitudOrdenServicios) As EncabezadoSolicitudOrdenServicios
            Dim item As New EncabezadoSolicitudOrdenServicios
            Dim resultado As IDataReader
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)

                resultado = conexion.ExecuteReaderStoreProcedure("gsp_obtener_encabezado_solicitud_orden_servicios")

                While resultado.Read
                    item = New EncabezadoSolicitudOrdenServicios(resultado)
                End While

            End Using

            Return item
        End Function
    End Class
End Namespace
