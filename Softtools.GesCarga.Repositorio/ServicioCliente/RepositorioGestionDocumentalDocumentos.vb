﻿Imports System.Transactions
Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.ServicioCliente

Namespace ServicioCliente
    Public NotInheritable Class RepositorioGestionDocumentalDocumentos
        Inherits RepositorioBase(Of GestionDocumentalDocumentos)

        Public Function InsertarTemporal(entidad As GestionDocumentalDocumentos) As Long
            Dim inserto As Boolean = True
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TIDOCodigo)
                    conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.CDGDCodigo)
                    conexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
                    conexion.AgregarParametroSQL("@par_Emisor", entidad.Emisor)
                    If entidad.FechaEmision > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Emision", entidad.FechaEmision, SqlDbType.Date)
                    End If
                    If entidad.FechaVence > Date.MinValue Then
                        conexion.AgregarParametroSQL("@par_Fecha_Vence", entidad.FechaVence, SqlDbType.Date)
                    End If
                    conexion.AgregarParametroSQL("@par_Documento", entidad.Archivo, SqlDbType.Image)
                    conexion.AgregarParametroSQL("@par_Nombre_Documento", entidad.NombreDocumento)
                    conexion.AgregarParametroSQL("@par_Extension_Documento", entidad.Extension)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_orden_servicio_documentos")
                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()

                    If entidad.Codigo.Equals(Cero) Then
                        inserto = False
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using
            Return entidad.Codigo
        End Function

        Public Function EliminarDocumento(entidad As GestionDocumentalDocumentos) As Boolean

            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TIDOCodigo)
                conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.CDGDCodigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_t_orden_servicio_documentos]")

            End Using
            Return True
        End Function

        Public Function EliminarDocumentoDefinitivo(entidad As GestionDocumentalDocumentos) As Boolean
            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TIDOCodigo)
                conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.CDGDCodigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_orden_servicio_documentos]")

                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
                End While

            End Using
            Return inserto
        End Function

        Public Function LimpiarDocumentoTemporalUsuario(entidad As GestionDocumentalDocumentos) As Boolean
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_limpiar_inicial_t_orden_servicio_documentos]")

            End Using
            Return True
        End Function

        Public Function LimpiarTemporalUsuario(entidad As GestionDocumentalDocumentos, ByRef contextoConexion As DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_limpiar_t_orden_servicio_documentos]")

            While resultado.Read
                inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
            End While
            resultado.Close()

            Return inserto
        End Function

        Public Function TrasladarDocumentos(entidad As GestionDocumentalDocumentos, ByRef contextoConexion As DataBaseFactory) As Boolean
            Dim inserto As Boolean = False

            contextoConexion.CleanParameters()
            contextoConexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
            contextoConexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
            contextoConexion.AgregarParametroSQL("@par_USUA_Codigo", entidad.UsuarioCrea.Codigo)

            Dim resultado As IDataReader = contextoConexion.ExecuteReaderStoreProcedure("[dbo].[gsp_transladar_orden_servicio_documentos]")

            While resultado.Read
                inserto = IIf(Convert.ToInt32(resultado.Item("Codigo")) > 0, True, False)
            End While
            resultado.Close()

            Return inserto
        End Function

        Public Function InsertarDetalleDocumento(entidad As GestionDocumentalDocumentos) As Boolean

            Dim inserto As Boolean = False
            Using conexion = New DatabaseFactoryConcrete(ConnectionStringDocumentos).CreateDataBaseFactory()
                Dim resultado As IDataReader

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Numero", entidad.Numero)
                conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TIDOCodigo)
                conexion.AgregarParametroSQL("@par_CDGD_Codigo", entidad.CDGDCodigo)
                conexion.AgregarParametroSQL("@par_Referencia", entidad.Referencia)
                conexion.AgregarParametroSQL("@par_Emisor", entidad.Emisor)

                If entidad.FechaEmision > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Emision", entidad.FechaEmision, SqlDbType.DateTime)
                End If

                If entidad.FechaVence > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Vence", entidad.FechaVence, SqlDbType.DateTime)
                End If
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)

                resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_insertar_detalle_orden_servicio_documentos]")
                While resultado.Read
                    inserto = IIf(Convert.ToInt32(resultado.Item("RegistrosAfectados")) > 0, True, False)
                End While

                resultado.Close()

            End Using
            Return inserto
        End Function

        Public Overrides Function Modificar(entidad As GestionDocumentalDocumentos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Obtener(filtro As GestionDocumentalDocumentos) As GestionDocumentalDocumentos
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Insertar(entidad As GestionDocumentalDocumentos) As Long
            Throw New NotImplementedException()
        End Function

        Public Overrides Function Consultar(filtro As GestionDocumentalDocumentos) As IEnumerable(Of GestionDocumentalDocumentos)
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace
