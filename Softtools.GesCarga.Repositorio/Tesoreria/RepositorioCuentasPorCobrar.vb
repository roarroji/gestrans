﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports System.Transactions

Namespace Tesoreria
    Public NotInheritable Class RepositorioCuentasPorCobrar
        Inherits RepositorioBase(Of CuentasPorCobrar)

        Public Overrides Function Consultar(filtro As CuentasPorCobrar) As IEnumerable(Of CuentasPorCobrar)
            Dim lista As New List(Of CuentasPorCobrar)
            Dim item As CuentasPorCobrar

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_codigo", filtro.CodigoEmpresa)

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If

                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If filtro.Numero > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                End If

                If Not IsNothing(filtro.TipoDocumento) Then
                    If filtro.TipoDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                    End If
                End If

                If Not IsNothing(filtro.Tercero) Then
                    If filtro.Tercero.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.Tercero.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.DocumentoOrigen) Then
                    If filtro.DocumentoOrigen.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_DOOR_Codigo", filtro.DocumentoOrigen.Codigo)
                    End If
                End If
                If filtro.CodigoDocumentoOrigen > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento_Origen", filtro.CodigoDocumentoOrigen)
                End If

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If

                If filtro.Estado > -1 And filtro.Estado < 2 Then
                    conexion.AgregarParametroSQL("@par_Estado", filtro.Estado)
                    conexion.AgregarParametroSQL("@par_Anulado", NO_APLICA)
                End If

                If filtro.Estado = 2 Then
                    conexion.AgregarParametroSQL("@par_Anulado", SI_APLICA)
                End If

                If filtro.CreadaManual >= 0 Then
                    conexion.AgregarParametroSQL("@par_CreadaManual", filtro.CreadaManual)
                End If

                If filtro.EstadoCuenta >= 0 Then
                    conexion.AgregarParametroSQL("@par_Estado_Cuenta", filtro.EstadoCuenta)
                End If

                If filtro.NumeroDocumentoOrigen > 0 Then
                    conexion.AgregarParametroSQL("@par_Numero_Documento_Origen", filtro.NumeroDocumentoOrigen)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_cuentas_por_cobrar")

                While resultado.Read
                    item = New CuentasPorCobrar(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As CuentasPorCobrar) As Long
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Dias_Plazo", entidad.DiasPlazo)
                    conexion.AgregarParametroSQL("@par_Fecha_Vence", entidad.FechaVenceDocumento, SqlDbType.Date)
                    If Not IsNothing(entidad.DocumentoOrigen) Then
                        If entidad.DocumentoOrigen.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_CATA_DOOR_Origen", entidad.DocumentoOrigen.Codigo)
                        End If
                    End If
                    If entidad.NumeroDocumentoOrigen > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento_Origen", entidad.NumeroDocumentoOrigen)
                    End If
                    conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
                    conexion.AgregarParametroSQL("@par_Concepto", entidad.CodigoConcepto)
                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    Else
                        conexion.AgregarParametroSQL("@par_Observaciones", "")
                    End If
                    conexion.AgregarParametroSQL("@par_Valor", entidad.ValorTotal, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo, SqlDbType.Money)
                    conexion.AgregarParametroSQL("@par_Creada_Manual", entidad.CreadaManual)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_cuentas_por_cobrar")

                    While resultado.Read
                        entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                        entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                    End While
                    resultado.Close()
                End Using
                If entidad.Codigo > 0 Then
                    transaccion.Complete()
                End If
            End Using
            Return entidad.Numero
        End Function

        Public Overrides Function Modificar(entidad As CuentasPorCobrar) As Long
            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_TIDO_Codigo", entidad.TipoDocumento)
                    conexion.AgregarParametroSQL("@par_Fecha", entidad.Fecha, SqlDbType.Date)
                    conexion.AgregarParametroSQL("@par_Dias_Plazo", entidad.DiasPlazo)
                    conexion.AgregarParametroSQL("@par_Fecha_Vence", entidad.FechaVenceDocumento, SqlDbType.Date)
                    If Not IsNothing(entidad.DocumentoOrigen) Then
                        If entidad.DocumentoOrigen.Codigo > 0 Then
                            conexion.AgregarParametroSQL("@par_CATA_DOOR_Origen", entidad.DocumentoOrigen.Codigo)
                        End If
                    End If
                    If entidad.NumeroDocumentoOrigen > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento_Origen", entidad.NumeroDocumentoOrigen)
                    End If
                    conexion.AgregarParametroSQL("@par_TERC_Codigo", entidad.Tercero.Codigo)
                    conexion.AgregarParametroSQL("@par_Concepto", entidad.CodigoConcepto)
                    If Not IsNothing(entidad.Observaciones) Then
                        conexion.AgregarParametroSQL("@par_Observaciones", entidad.Observaciones)
                    Else
                        conexion.AgregarParametroSQL("@par_Observaciones", "")
                    End If
                    conexion.AgregarParametroSQL("@par_Valor", entidad.ValorTotal)
                    conexion.AgregarParametroSQL("@par_Saldo", entidad.Saldo)
                    conexion.AgregarParametroSQL("@par_Creada_Manual", entidad.CreadaManual)
                    conexion.AgregarParametroSQL("@par_OFIC_Codigo", entidad.Oficina.Codigo)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", entidad.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_cuentas_por_cobrar")

                    While resultado.Read
                        entidad.Codigo = Convert.ToInt64(resultado.Item("Codigo").ToString())
                        entidad.Numero = Convert.ToInt64(resultado.Item("Numero").ToString())
                    End While
                    resultado.Close()
                End Using
                If entidad.Codigo > 0 Then
                    transaccion.Complete()
                End If
            End Using
            Return entidad.Numero
        End Function

        Public Overrides Function Obtener(filtro As CuentasPorCobrar) As CuentasPorCobrar
            Dim item As New CuentasPorCobrar
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_obtener_cuentas_por_cobrar")
                While resultado.Read
                    item = New CuentasPorCobrar(resultado)
                End While
                conexion.CloseConnection()
            End Using
            Return item
        End Function

        Public Function Anular(entidad As CuentasPorCobrar) As CuentasPorCobrar
            Dim item As New CuentasPorCobrar
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                conexion.AgregarParametroSQL("@par_USUA_Codigo_Anula", entidad.UsuarioAnula.Codigo)
                conexion.AgregarParametroSQL("@par_Causa_Anula", entidad.CausaAnulacion)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_anular_cuentas_por_cobrar")
                While resultado.Read
                    item.Anulado = Convert.ToInt64(resultado.Item("Anulado").ToString())
                End While
            End Using
            Return item
        End Function

        Public Function ConsultarDetalleObservaciones(entidad As CuentasPorCobrar) As CuentasPorCobrar
            Dim item As New CuentasPorCobrar
            Dim detalle As New DetalleObservacionesCuentas
            Dim listadetalle As New List(Of DetalleObservacionesCuentas)
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_observaciones_cuentas_por_cobrar")
                While resultado.Read
                    detalle = New DetalleObservacionesCuentas(resultado)
                    listadetalle.Add(detalle)
                End While
                item.DetalleObservaciones = listadetalle
            End Using
            Return item
        End Function

        Public Function GuardarDetalleObservaciones(entidad As CuentasPorCobrar) As Long
            Dim numero As Integer = 0
            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                BorrarDetalleObservaciones(entidad.CodigoEmpresa, entidad.Codigo, conexion)
                For Each item In entidad.DetalleObservaciones
                    conexion.CreateConnection()
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", item.UsuarioCrea.Codigo)
                    conexion.AgregarParametroSQL("@par_Observacion", item.Observaciones)
                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_detalle_observaciones_cuentas_por_cobrar")
                    While resultado.Read
                        numero = Int(resultado.Item("Codigo"))
                    End While
                    resultado.Close()
                    If numero = 0 Then
                        Exit For
                    End If
                Next
            End Using
            Return numero
        End Function

        Public Sub BorrarDetalleObservaciones(Empresa As Integer, Codigo As Integer, ByRef conexion As Conexion.DataBaseFactory)
            conexion.CreateConnection()
            conexion.AgregarParametroSQL("@par_EMPR_Codigo", Empresa)
            conexion.AgregarParametroSQL("@par_Codigo", Codigo)
            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_borrar_detalle_observaciones_cuentas_por_cobrar")
            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If
            resultado.Close()
        End Sub
    End Class
End Namespace


