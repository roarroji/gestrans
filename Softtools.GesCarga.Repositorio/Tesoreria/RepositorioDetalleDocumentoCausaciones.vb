﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Tesoreria


Namespace Tesoreria


    Public NotInheritable Class RepositorioDetalleDocumentoCausaciones
        Inherits RepositorioBase(Of DetalleDocumentoCausaciones)

        Public Overrides Function Consultar(filtro As DetalleDocumentoCausaciones) As IEnumerable(Of DetalleDocumentoCausaciones)
            Dim lista As New List(Of DetalleDocumentoCausaciones)
            Dim item As DetalleDocumentoCausaciones

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_EDCO_Codigo", filtro.Codigo)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalle_documento_causaciones")

                While resultado.Read
                    item = New DetalleDocumentoCausaciones(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function


        Public Overrides Function Insertar(entidad As DetalleDocumentoCausaciones) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Modificar(entidad As DetalleDocumentoCausaciones) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As DetalleDocumentoCausaciones) As DetalleDocumentoCausaciones
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DetalleDocumentoCausaciones) As Boolean
            Throw New NotImplementedException()
        End Function

    End Class

End Namespace