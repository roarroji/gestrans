﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Tesoreria


Namespace Tesoreria

    ''' <summary>
    ''' Clase <see cref="RepositorioDetalleDocumentoComprobantes"/>
    ''' </summary>
    Public NotInheritable Class RepositorioDetalleDocumentoComprobantes
        Inherits RepositorioBase(Of DetalleDocumentoComprobantes)

        Public Overrides Function Consultar(filtro As DetalleDocumentoComprobantes) As IEnumerable(Of DetalleDocumentoComprobantes)
            Dim lista As New List(Of DetalleDocumentoComprobantes)
            Dim item As DetalleDocumentoComprobantes

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)


                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_EDCO_Codigo", filtro.Codigo)
                End If
                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_detalle_documento_comprobantes]")

                While resultado.Read
                    item = New DetalleDocumentoComprobantes(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As DetalleDocumentoComprobantes) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Modificar(entidad As DetalleDocumentoComprobantes) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As DetalleDocumentoComprobantes) As DetalleDocumentoComprobantes
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As DetalleDocumentoComprobantes) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace