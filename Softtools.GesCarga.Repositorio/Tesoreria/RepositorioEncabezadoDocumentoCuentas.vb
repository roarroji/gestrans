﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Tesoreria

Namespace Tesoreria

    ''' <summary>
    ''' Clase <see cref="RepositorioEncabezadoDocumentoCuentas"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEncabezadoDocumentoCuentas
        Inherits RepositorioBase(Of EncabezadoDocumentoCuentas)

        Public Overrides Function Consultar(filtro As EncabezadoDocumentoCuentas) As IEnumerable(Of EncabezadoDocumentoCuentas)
            Dim lista As New List(Of EncabezadoDocumentoCuentas)
            Dim item As EncabezadoDocumentoCuentas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_codigo", filtro.CodigoEmpresa)

                If Not String.IsNullOrWhiteSpace(filtro.NumeroDocumento) Then
                    If filtro.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                    End If
                End If

                If Not IsNothing(filtro.TipoDocumento) Then
                    If filtro.TipoDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                    End If
                End If
                If Not IsNothing(filtro.Tercero) Then
                    If filtro.Tercero.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.Tercero.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.DocumentoOrigen) Then
                    If filtro.DocumentoOrigen.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_DOOR_Codigo", filtro.DocumentoOrigen.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If filtro.CreadaManual >= 0 Then
                    conexion.AgregarParametroSQL("@par_CreadaManual", filtro.CreadaManual)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_encabezado_documento_cuentas]")

                While resultado.Read
                    item = New EncabezadoDocumentoCuentas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function


        Public Function ConsultarCuentasconEgresos(filtro As EncabezadoDocumentoCuentas) As IEnumerable(Of EncabezadoDocumentoCuentas)
            Dim lista As New List(Of EncabezadoDocumentoCuentas)
            Dim item As EncabezadoDocumentoCuentas

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_codigo", filtro.CodigoEmpresa)

                If Not String.IsNullOrWhiteSpace(filtro.NumeroDocumento) Then
                    If filtro.NumeroDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero_Documento", filtro.NumeroDocumento)
                    End If
                End If

                If Not IsNothing(filtro.TipoDocumento) Then
                    If filtro.TipoDocumento > 0 Then
                        conexion.AgregarParametroSQL("@par_TIDO_Codigo", filtro.TipoDocumento)
                    End If
                End If
                If Not IsNothing(filtro.Tercero) Then
                    If filtro.Tercero.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_TERC_Codigo", filtro.Tercero.Codigo)
                    End If
                End If
                If Not IsNothing(filtro.DocumentoOrigen) Then
                    If filtro.DocumentoOrigen.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_DOOR_Codigo", filtro.DocumentoOrigen.Codigo)
                    End If
                End If

                If Not IsNothing(filtro.Oficina) Then
                    If filtro.Oficina.Codigo > 0 Then
                        conexion.AgregarParametroSQL("@par_OFIC_Codigo", filtro.Oficina.Codigo)
                    End If
                End If

                If filtro.FechaInicial > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Inicial", filtro.FechaInicial, SqlDbType.Date)
                End If
                If filtro.FechaFinal > Date.MinValue Then
                    conexion.AgregarParametroSQL("@par_Fecha_Final", filtro.FechaFinal, SqlDbType.Date)
                End If

                If Not IsNothing(filtro.Pagina) Then
                    If filtro.Pagina > 0 Then
                        conexion.AgregarParametroSQL("@par_NumeroPagina", filtro.Pagina)
                    End If
                End If
                If Not IsNothing(filtro.RegistrosPagina) Then
                    If filtro.RegistrosPagina > 0 Then
                        conexion.AgregarParametroSQL("@par_RegistrosPagina", filtro.RegistrosPagina)
                    End If
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_encabezado_documento_cuentas_con_egresos")

                While resultado.Read
                    item = New EncabezadoDocumentoCuentas(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Function ConsultarDetalleCruceDocumentos(filtro As EncabezadoDocumentoCuentas) As IEnumerable(Of DetalleCruceDocumentoCuenta)
            Dim lista As New List(Of DetalleCruceDocumentoCuenta)
            Dim item As DetalleCruceDocumentoCuenta

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_codigo", filtro.CodigoEmpresa)

                If Not String.IsNullOrWhiteSpace(filtro.Numero) Then
                    If filtro.Numero > 0 Then
                        conexion.AgregarParametroSQL("@par_Numero", filtro.Numero)
                    End If
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_consultar_detalles_cruce_documento_cuenta")

                While resultado.Read
                    item = New DetalleCruceDocumentoCuenta(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function
        Public Overrides Function Insertar(entidad As EncabezadoDocumentoCuentas) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Modificar(entidad As EncabezadoDocumentoCuentas) As Long
            Throw New NotImplementedException()
        End Function
        Public Overrides Function Obtener(filtro As EncabezadoDocumentoCuentas) As EncabezadoDocumentoCuentas
            Throw New NotImplementedException()
        End Function
        Public Function Anular(entidad As EncabezadoDocumentoCuentas) As Boolean
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace