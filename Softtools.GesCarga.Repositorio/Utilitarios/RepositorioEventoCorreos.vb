﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Utilitarios
Imports System.Transactions

Namespace Utilitarios

    ''' <summary>
    ''' Clase <see cref="RepositorioEventoCorreos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioEventoCorreos
        Inherits RepositorioBase(Of EventoCorreos)


        ''' <summary>
        ''' Consultar información de bandeja de salida de correos
        ''' </summary>
        ''' <param name="filtro">Filtros de búsqueda</param>
        ''' <returns>Lista de tipo <see cref="EventoCorreos"/></returns>
        Public Overrides Function Consultar(filtro As EventoCorreos) As IEnumerable(Of EventoCorreos)
            Dim lista As New List(Of EventoCorreos)
            Dim item As EventoCorreos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)
                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If



                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_evento_correos]")

                While resultado.Read
                    item = New EventoCorreos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        ''' <summary>
        ''' Obtiene información de bandeja de salida de correos
        ''' </summary>
        ''' <param name="filtro">Filtros de búsqueda</param>
        ''' <returns>Objeto de tipo <see cref="EventoCorreos"/></returns>
        Public Overrides Function Obtener(filtro As EventoCorreos) As EventoCorreos
            Dim item As New EventoCorreos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_EVCO_Codigo", filtro.Codigo)
                End If


                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_evento_correos]")


                While resultado.Read
                    item = New EventoCorreos(resultado)
                End While
                If filtro.ConsultarEventoFuncionCorreo > 0 Then
                    Dim listaDistribucion As New List(Of ListaDistribucionCorreos)
                    Dim Distribucion As ListaDistribucionCorreos
                    resultado.Close()
                    resultado = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_distribucion_correos]")
                    While resultado.Read
                        Distribucion = New ListaDistribucionCorreos(resultado)
                        listaDistribucion.Add(Distribucion)
                    End While
                    item.ListaDistrubicionCorreos = listaDistribucion
                End If
            End Using

            Return item
        End Function

        Public Overrides Function Insertar(entidad As EventoCorreos) As Long
            Throw New NotImplementedException()
        End Function

        ''' <summary>
        ''' Modifico un registro en la tabla bandeja_salida_correos
        ''' </summary>
        ''' <param name="entidad">Entidad a persistir en la base de datos</param>
        ''' <returns>Retorna consecutivo el identificador del registro</returns>
        Public Overrides Function Modificar(entidad As EventoCorreos) As Long
            Dim modifi As Boolean = True

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                    conexion.CreateConnection()
                    conexion.CleanParameters()
                    conexion.AgregarParametroSQL("@par_EMPR_Codigo", entidad.CodigoEmpresa)
                    conexion.AgregarParametroSQL("@par_Codigo", entidad.Codigo)
                    conexion.AgregarParametroSQL("@par_Estado", entidad.Estado)
                    conexion.AgregarParametroSQL("@par_USUA_Codigo_Modifica", entidad.UsuarioModifica.Codigo)

                    Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_modificar_evento_correos")
                    While resultado.Read
                        entidad.Codigo = resultado.Item("Codigo").ToString()
                    End While

                    resultado.Close()
                    If entidad.Codigo.Equals(Cero) Then
                        modifi = True
                    End If

                End Using

                If modifi Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function
    End Class
End Namespace
