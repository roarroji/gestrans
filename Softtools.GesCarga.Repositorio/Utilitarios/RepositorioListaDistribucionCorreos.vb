﻿Imports Softtools.GesCarga.Conexion
Imports Softtools.GesCarga.Entidades.Utilitarios
Imports System.Transactions

Namespace Utilitarios

    ''' <summary>
    ''' Clase <see cref="RepositorioListaDistribucionCorreos"/>
    ''' </summary>
    Public NotInheritable Class RepositorioListaDistribucionCorreos
        Inherits RepositorioBase(Of ListaDistribucionCorreos)

        ''' <summary>
        ''' Consultar información de bandeja de salida de correos
        ''' </summary>
        ''' <param name="filtro">Filtros de búsqueda</param>
        ''' <returns>Lista de tipo <see cref="ListaDistribucionCorreos"/></returns>
        Public Overrides Function Consultar(filtro As ListaDistribucionCorreos) As IEnumerable(Of ListaDistribucionCorreos)
            Dim lista As New List(Of ListaDistribucionCorreos)
            Dim item As ListaDistribucionCorreos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()
                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > 0 Then
                    conexion.AgregarParametroSQL("@par_Codigo", filtro.Codigo)
                End If
                If filtro.NumeroEvento > Cero Then
                    conexion.AgregarParametroSQL("@par_EVCO_Codigo", filtro.NumeroEvento)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_consultar_lista_distribucion_correos]")

                While resultado.Read
                    item = New ListaDistribucionCorreos(resultado)
                    lista.Add(item)
                End While

            End Using

            Return lista
        End Function

        Public Overrides Function Insertar(entidad As ListaDistribucionCorreos) As Long
            Dim inserto As Boolean = True

            Dim entidadEvco As EventoCorreos = New EventoCorreos With {.CodigoEmpresa = entidad.CodigoEmpresa, .Codigo = entidad.NumeroEvento, .Estado = entidad.Estado, .UsuarioModifica = entidad.UsuarioModifica}
            Dim RepEvco As RepositorioEventoCorreos = New RepositorioEventoCorreos()
            Dim EVCOCodigo As Long = RepEvco.Modificar(entidadEvco)

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    Call BorrarlistasDistribucionCorreos(entidad.CodigoEmpresa, entidad.NumeroEvento, conexion)

                    For Each detalle In entidad.Listado

                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", detalle.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_EVCO_Codigo", detalle.NumeroEvento)
                        conexion.AgregarParametroSQL("@par_Estado", detalle.Estado)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", detalle.UsuarioCrea.Codigo)
                        conexion.AgregarParametroSQL("@par_Email", detalle.Email)
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Empleado", detalle.Empleado.Codigo)

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_Lista_Distribucion_Correos")
                        While resultado.Read
                            entidad.Codigo = resultado.Item("Numero").ToString()
                        End While

                        resultado.Close()
                    Next

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                End Using

                If inserto Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Modificar(entidad As ListaDistribucionCorreos) As Long
            Dim modifi As Boolean = True

            Dim entidadEvco As EventoCorreos = New EventoCorreos With {.CodigoEmpresa = entidad.CodigoEmpresa, .Codigo = entidad.NumeroEvento, .Estado = entidad.Estado, .UsuarioModifica = entidad.UsuarioModifica}
            Dim RepEvco As RepositorioEventoCorreos = New RepositorioEventoCorreos()
            Dim EVCOCodigo As Long = RepEvco.Modificar(entidadEvco)

            Using transaccion = New TransactionScope(TransactionScopeOption.Required)
                Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()
                    conexion.CreateConnection()
                    Call BorrarlistasDistribucionCorreos(entidad.CodigoEmpresa, entidad.NumeroEvento, conexion)

                    For Each detalle In entidad.Listado

                        conexion.CleanParameters()
                        conexion.AgregarParametroSQL("@par_EMPR_Codigo", detalle.CodigoEmpresa)
                        conexion.AgregarParametroSQL("@par_EVCO_Codigo", detalle.NumeroEvento)
                        conexion.AgregarParametroSQL("@par_Estado", detalle.Estado)
                        conexion.AgregarParametroSQL("@par_USUA_Codigo_Crea", detalle.UsuarioCrea.Codigo)
                        conexion.AgregarParametroSQL("@par_Email", detalle.Email)
                        conexion.AgregarParametroSQL("@par_TERC_Codigo_Empleado", detalle.Empleado.Codigo)

                        Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("gsp_insertar_Lista_Distribucion_Correos")
                        While resultado.Read
                            entidad.Codigo = resultado.Item("Numero").ToString()
                        End While

                        resultado.Close()
                    Next

                    If entidad.Codigo.Equals(Cero) Then
                        Return Cero
                    End If

                End Using

                If modifi Then
                    transaccion.Complete()
                Else
                    entidad.Codigo = Cero
                End If

            End Using

            Return entidad.Codigo
        End Function

        Public Overrides Function Obtener(filtro As ListaDistribucionCorreos) As ListaDistribucionCorreos
            Dim item As New ListaDistribucionCorreos

            Using conexion = New DatabaseFactoryConcrete(ConnectionString).CreateDataBaseFactory()

                conexion.CreateConnection()

                conexion.AgregarParametroSQL("@par_EMPR_Codigo", filtro.CodigoEmpresa)

                If filtro.Codigo > Cero Then
                    conexion.AgregarParametroSQL("@par_EVCO_Codigo", filtro.Codigo)
                End If

                Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_obtener_lista_distribucion_correos]")

                While resultado.Read
                    item = New ListaDistribucionCorreos(resultado)
                End While

            End Using

            Return item
        End Function

        Public Function BorrarlistasDistribucionCorreos(CodiEmpresa As Short, CodTercero As Long, ByRef conexion As Conexion.DataBaseFactory) As Boolean

            conexion.CleanParameters()

            conexion.AgregarParametroSQL("@par_EMPR_Codigo", CodiEmpresa)
            conexion.AgregarParametroSQL("@par_EVCO_Codigo", CodTercero)

            Dim resultado As IDataReader = conexion.ExecuteReaderStoreProcedure("[dbo].[gsp_eliminar_lista_distribucion_correos]")

            If resultado.RecordsAffected > 0 Then
                resultado.Close()
            End If

            resultado.Close()
            Return True
        End Function

    End Class

End Namespace
