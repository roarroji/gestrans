﻿PRINT 'V_Reporte_Despacho_Faro'
GO

DROP VIEW V_Reporte_Despacho_Faro
GO

CREATE VIEW V_Reporte_Despacho_Faro
AS
	SELECT TOP 100 PERCENT
		EMPR.Codigo AS CodigoEmpresa,
	 	SUBSTRING(EMPR.Numero_Identificacion,1,9) AS IdentificacionTransportadora,
		ENMA.Numero AS Manifiesto,
		ENMA.Fecha_Inicio_Viaje AS FechaDespacho,
		CONCAT(ENMA.CIUD_Origen,'000') AS CIUD_Origen,
		CONCAT(ENMA.CIUD_Destino,'000') AS CIUD_Destino,
		ENMA.VEHI_Placa,
		ENMA.Modelo,
		VEHI.MARC_Codigo,
		VEHI.LINE_Codigo,
		VEHI.COLO_Codigo,
		VEHI.IdenConductor,
		--VEHI.NombreConductor,
		CONCAT(COND.Nombre,' ',COND.Apellido1,' ',COND.Apellido2) AS NombreConductor,
		CONCAT(VEHI.CIUD_Conductor,'000') AS CiudadConductor,
		VEHI.TeleConductor,
		VEHI.CeluConductor,
		ENMA.Observaciones,
		ENMA.RUTA_Codigo,
		ENMA.NombreRuta,
		CASE TIRU.Codigo WHEN 1 THEN 0 ELSE 1 END AS TipoRuta, --Urbana = 0, Nacional = 1
		VEHI.ConfiguracionTipoVehiculo AS ConfiguracionVehiculo,
		VEHI.CodigoCarroceriaMinisterio AS Carroceria,
		VEHI.Numero_Motor,
		VEHI.Poliza_SOAT,
		VEHI.Fecha_Vence_SOAT,
		SUBSTRING(VEHI.AseguradoraSOAT,1,40) AS AseguradoraSOAT,
		VEHI.Tarjeta_Propiedad,
		VEHI.SEMI_Placa,
		VEHI.CateLiceCond,
		VEHI.DireConductor,
		VEHI.IdenPropietario,
		VEHI.NombrePropietario,
		CONCAT(VEHI.CIUD_Propietario,'000') AS CIUD_Propietario,
		VEHI.DirePropietario,
		SUBSTRING(VEHI.CeluConductor,1,3) AS OperadorCelular
	FROM
		Empresas EMPR,
		V_Encabezado_Manifiestos ENMA,
		Detalle_Despacho_Contrato_Clientes DDCC,
		Terceros AS COND,
		V_Vehiculos VEHI,
		Rutas AS RUTA,
		V_Tipo_Rutas TIRU
	WHERE
		ENMA.EMPR_Codigo = VEHI.EMPR_Codigo
		AND ENMA.VEHI_Placa = VEHI.Placa

		AND ENMA.EMPR_Codigo = DDCC.EMPR_Codigo
		AND ENMA.Numero = DDCC.ENMA_Numero

		AND DDCC.EMPR_Codigo = COND.EMPR_Codigo
		AND DDCC.TERC_Conductor = COND.Codigo

		AND ENMA.EMPR_Codigo = RUTA.EMPR_Codigo
		AND ENMA.RUTA_Codigo = RUTA.Codigo

		AND RUTA.EMPR_Codigo = TIRU.EMPR_Codigo
		AND RUTA.TIRU_Codigo = TIRU.Codigo
	ORDER BY
		ENMA.Numero
GO