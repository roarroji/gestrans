PRINT 'gsp_consultar_oficinas_sincronizacion_CARGA_APP'
GO
DROP PROCEDURE gsp_consultar_oficinas_sincronizacion_CARGA_APP
GO
CREATE PROCEDURE gsp_consultar_oficinas_sincronizacion_CARGA_APP(
@par_EMPR_Codigo smallint
)
AS
BEGIN
SELECT 
OFIC.Codigo, 
OFIC.Codigo_Alterno, 
OFIC.Nombre, 
OFIC.Fecha_Crea, 
OFIC.Fecha_Modifica
FROM Oficinas AS OFIC
LEFT JOIN Registro_Sincronizacion_APP_CARGA AS RSAC
ON OFIC.EMPR_Codigo = RSAC.EMPR_Codigo  
AND OFIC.Codigo = RSAC.Codigo
AND RSAC.CATA_OSCA_Codigo = 23801 -- Origen Sincronizacion CARGA APP Oficinas
WHERE OFIC.EMPR_Codigo = @par_EMPR_Codigo
AND ISNULL(RSAC.Sincronizado,0) = 0
END
GO