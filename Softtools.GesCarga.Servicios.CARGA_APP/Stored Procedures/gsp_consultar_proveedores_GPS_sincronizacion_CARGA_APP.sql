PRINT 'gsp_consultar_proveedores_GPS_sincronizacion_CARGA_APP'
GO
DROP PROCEDURE gsp_consultar_proveedores_GPS_sincronizacion_CARGA_APP
GO
CREATE PROCEDURE gsp_consultar_proveedores_GPS_sincronizacion_CARGA_APP(
@par_EMPR_Codigo smallint,
@par_Perfiles varchar(max)
)
AS
BEGIN
SELECT DISTINCT
LTRIM(RTRIM(CONCAT(TERC.Razon_Social,' ',TERC.Nombre,' ',TERC.Apellido1,' ',TERC.Apellido2))) AS NombreCompleto,
TERC.Codigo_Alterno,
TERC.Codigo
FROM Terceros TERC

LEFT JOIN Registro_Sincronizacion_APP_CARGA AS RSAC
ON TERC.EMPR_Codigo = RSAC.EMPR_Codigo  
AND TERC.Codigo = RSAC.Codigo
AND RSAC.CATA_OSCA_Codigo = 23802 -- Origen Sincronizacion CARGA APP Empresas GPS

INNER JOIN Perfil_Terceros PETE ON
TERC.EMPR_Codigo = PETE.EMPR_Codigo AND
TERC.Codigo = PETE.TERC_Codigo
AND PETE.Codigo IN(SELECT * FROM Func_Dividir_String(@par_Perfiles,','))

WHERE TERC.EMPR_Codigo = @par_EMPR_Codigo
AND ISNULL(RSAC.Sincronizado,0) = 0
END
GO