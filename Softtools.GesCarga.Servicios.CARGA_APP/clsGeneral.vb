﻿Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.IO

Public Class clsGeneral

#Region "Variables Generales"
    ' Generales
    Private intCodigoEmpresa As Integer

    ' Archivos Planos
    Dim stmStreamW As Stream
    Dim stwStreamWriter As StreamWriter
    Dim stwStreamReader As StreamReader
    Dim strRutaArchivoLog As String
    Dim strNombreArchivoLog As String

    ' Base Datos
    Private strCadenaConexionSQL As String
    Private sqlConexion As SqlConnection
    Private sqlComando As SqlCommand
    Private sqlTransaccion As SqlTransaction
    Private sqlExcepcionSQL As SqlException

#End Region

#Region "Constantes"

    ' Generales
    Public Const PRIMER_TABLA As Integer = 0
    Public Const PRIMER_REGISTRO As Integer = 0
    Public Const NOMBRE_ARCHIVO_LOG_SW As String = "SW_FACTURA_ELECTRONICA_SAPHETY"

    ' Tipos Campo Base Datos
    Public Const CAMPO_NUMERICO As Byte = 1
    Public Const CAMPO_ALFANUMERICO As Byte = 2

#End Region

    Public Sub New()
        Try
            ' Inicializar Variables
            Me.intCodigoEmpresa = ConfigurationSettings.AppSettings("CodigoEmpresa").ToString()
            Me.strCadenaConexionSQL = ConfigurationSettings.AppSettings("ConexionSQLGestrans").ToString()
            Me.strRutaArchivoLog = ConfigurationSettings.AppSettings("RutaArchivoLog").ToString()

        Catch ex As Exception
            Call Guardar_Mensaje_Log("clsGeneral_New" + ex.Message)
        End Try
    End Sub
#Region "General"
    Public Property RutaArchivoLog() As String
        Get
            Return Me.strRutaArchivoLog
        End Get
        Set(ByVal value As String)
            Me.strRutaArchivoLog = value
        End Set
    End Property
#End Region

#Region "Propiedades Acceso Base Datos"

    Public Property CadenaConexionSQL() As String
        Get
            Return Me.strCadenaConexionSQL
        End Get
        Set(ByVal value As String)
            Me.strCadenaConexionSQL = value
        End Set
    End Property

    Public Property Conexion() As SqlConnection
        Get
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            Me.sqlConexion = value
        End Set
    End Property

    Public Property ConexionSQL() As SqlConnection
        Get
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaConexionSQL)
            End If
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaConexionSQL)
            End If
            Me.sqlConexion = value
        End Set
    End Property

    Public Property ComandoSQL() As SqlCommand
        Get
            Return Me.sqlComando
        End Get
        Set(ByVal value As SqlCommand)
            Me.sqlComando = value
        End Set
    End Property

    Public Property TransaccionSQL() As SqlTransaction
        Get
            Return Me.sqlTransaccion
        End Get
        Set(ByVal value As SqlTransaction)
            Me.sqlTransaccion = value
        End Set
    End Property

    Public Property ExcepcionSQL() As SqlException
        Get
            Return Me.sqlExcepcionSQL
        End Get
        Set(ByVal value As SqlException)
            Me.sqlExcepcionSQL = value
        End Set
    End Property

#End Region

#Region "Funciones Base Datos"

    Public Function Ejecutar_SQL(ByVal strSQL As String, Optional ByRef strError As String = "") As Long
        Dim lonRegiAfec As Long = 0

        Try
            Using sqlConexion = New SqlConnection(Me.strCadenaConexionSQL)

                sqlConexion.Open()

                Using sqlComando As New SqlCommand

                    sqlComando.CommandType = CommandType.Text
                    sqlComando.CommandText = strSQL
                    sqlComando.Connection = sqlConexion

                    lonRegiAfec = Val(sqlComando.ExecuteNonQuery().ToString)

                End Using

                sqlConexion.Close()

            End Using

        Catch ex As Exception
            Ejecutar_SQL = False
            strError = ex.Message
        End Try

        Return lonRegiAfec

    End Function

    Public Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using sqlConexion = New SqlConnection(Me.strCadenaConexionSQL)

                sqlConexion.Open()

                Using sqlDataAdap = New SqlDataAdapter(strSQL, sqlConexion)

                    sqlDataAdap.Fill(dsDataSet)

                End Using

                sqlConexion.Close()

            End Using

        Catch ex As Exception
            Guardar_Mensaje_Log("Error Retorna_Dataset: " & ex.Message)

        End Try

        Return dsDataSet

    End Function

#End Region

#Region "Funciones Archivos Planos"

    Public Function Guardar_Mensaje_Log(ByVal strMensaje As String)
        Try

            Dim strContenido As String, strFechaHora As String = Date.Now.ToString
            Dim strNombreArchivoLog As String = Me.strRutaArchivoLog & NOMBRE_ARCHIVO_LOG_SW & "_" & Format$(Date.Now.Day, "00") & Format$(Date.Now.Month, "00") & Format$(Date.Now.Year, "00") & ".txt"

            If Not File.Exists(strNombreArchivoLog) Then
                File.Create(strNombreArchivoLog).Dispose()
                strContenido = String.Empty
            Else
                strContenido = File.ReadAllText(strNombreArchivoLog)
            End If
            Me.stmStreamW = File.OpenWrite(strNombreArchivoLog)
            Me.stwStreamWriter = New StreamWriter(Me.stmStreamW, System.Text.Encoding.Default)
            Me.stwStreamWriter.Flush()
            Me.stwStreamWriter.Write(strContenido & vbNewLine & strFechaHora & vbNewLine & strMensaje)
            Me.stwStreamWriter.Close()
            Me.stmStreamW.Close()

            Return True
        Catch ex As Exception
            '' FALTA
            ' Escribir en log eventos windows
            Return False
            Exit Try
        End Try
    End Function

#End Region

End Class

Public Class TokenSaphety
    Public Property Empresa As Integer
    Public Property OperadorVirtual As String
    Public Property Ambiente As Integer
    Public Property access_token As String
    Public Property token_type As String
    Public Property expires As String
End Class

Public Class TokenSofttools
    Public Property access_token As String
    Public Property token_type As String
    Public Property expires_in As String
End Class

Public Class TokenCargaAPP
    Public Property token As String
    Public Property expiration As String

End Class

Public Class objResponseCrearOficina
    Public Property id As String
    Public Property codigoAlterno As String
    Public Property nombreAgencia As String
End Class
Public Class objResponseCrearEmpresasGPS
    Public Property id As String
    Public Property codigoAlterno As String
    Public Property nombre As String
End Class

Public Class objResponseCrearVehiculos
    Public Property id As String
    Public Property placa As String
    Public Property activo As Boolean
    Public Property modelo As Integer
    Public Property tipoVehiculo As Integer
    Public Property descripcionTipoVehiculo As String
    Public Property ejesCabezote As Integer
    Public Property placaTrailer As String
    Public Property tipoCarroceria As Integer
    Public Property descripcionTipoCarroceria As String
    Public Property ejesTrailer As Integer
    Public Property fechaExpiracionTecnicoMecanica As Date
    Public Property fechaExpiracionSOAT As Date
    Public Property tieneReporteExterno As Boolean
    Public Property tieneReporteInterno As Boolean
    Public Property tieneSatelital As Boolean
    Public Property codigoTipoCombustible As Integer
    Public Property descripcionTipoCombustible As String
    Public Property codigoAlternoEmpresaGPS As String
    Public Property nombreEmpresaGPS As String
    Public Property usuarioGPS As String
    Public Property passwordGPS As String
    Public Property codigoAlternoConductor As String
    Public Property codigoAlternoTenedor As String
    Public Property codigoAlternoPropietario As String
    Public Property codigoAlternoAdministrador As String
    Public Property esArticulado As Boolean
End Class

Public Class objResponseCrearTerceros
    Public Property codigoAlternoTercero As String
    Public Property activo As Boolean
    Public Property identificacion As String
    Public Property nombres As String
    Public Property apellidos As String
    Public Property direccion As String
    Public Property ciudad As String
    Public Property celular As String
    Public Property esTenedor As Boolean
    Public Property esPropietario As Boolean
    Public Property esAdministrador As Boolean
    Public Property placaCabezote As String
    Public Property placaTrailer As String
End Class

Public Class objResponseCrearConductores
    Public Property codigoAlternoTercero As String
    Public Property activo As Boolean
    Public Property identificacion As String
    Public Property nombres As String
    Public Property apellidos As String
    Public Property direccion As String
    Public Property ciudad As String
    Public Property email As String
    Public Property celular As String
    Public Property categoria As Integer
    Public Property descripcionCategoria As String
    Public Property fechaExpiracionCarnetDIACO As Date
    Public Property fechaExpiracionARL As Date
    Public Property tieneReportes As Boolean
    Public Property tieneComparendos As Boolean
    Public Property listadoClientesBloqueados As String
    Public Property bloqueadoParaSeguimientoEspecial As String
    Public Property fechaVencimientoLicencia As Date
    Public Property fechaVencimientoCarnetMercanciaPeligrosa As Date
    Public Property placaCabezote As String
    Public Property placaTrailer As String
End Class

Public Class status
    Public Property code As String
    Public Property message As String
End Class

Public Class ResultadoReporte
    Public Property TotalFacturas As Integer
    Public Property TotalNotasDebito As Integer
    Public Property TotalNotasCredito As Integer
End Class

Public Class EstadosCatalogos
    Public Property Codigo As Integer
    Public Property Nombre As String
    Public Property Campo2 As String
End Class
