﻿DROP VIEW V_Despacho_SCL
GO

CREATE VIEW V_Despacho_SCL
AS
SELECT DISTINCT
	ENMA.EMPR_Codigo,
	ENMA.Numero,
	ENMA.Fecha_Inicio_Viaje AS SalidaVehiculo,
	LEFT(EMPR.Nit_Manifiesto_Electronico,9) AS NitTransportadora,
	ENMA.Numero AS Manifiesto,
	CONCAT(ORIG.Codigo,'000') AS CiudadOrigen,
	CONCAT(DEST.Codigo,'000') AS CiudadDestino,
	RUTA.Nombre AS Via,
	ENMA.RUTA_Codigo AS Ruta,
	ENMA.VEHI_Placa AS PlacaVehiculo,
	MAVE.Nombre AS MarcaVehiculo,
	VEHI.CAVE_Codigo AS CarroceriaVehiculo,
	VEHI.COLO_Codigo AS ColorVehiculo,
	VEHI.Modelo,
	COND.Numero_Identificacion AS CedulaConductor,
	COND.Nombre AS NombreConductor,
	CONCAT(COND.Apellido1,' ',COND.Apellido2) AS ApellidoConductor,
	COND.Direccion AS DireccionConductor,
	COND.Telefono1 AS TelefonoConductor,
	COND.Celular AS CelularConductor,
	CLIE.Numero_Identificacion AS IdentificacionCliente,
	CONCAT(CLIE.Nombre,' ',CLIE.Apellido1,' ',CLIE.Apellido2) AS Cliente,
	ENMA.OFIC_Codigo AS Sucursal,
	ENMA.Observaciones
FROM
	Encabezado_Manifiestos AS ENMA,
	Detalle_Manifiestos AS DEMA,
	Encabezado_Remesas AS ENRE,
	Empresas AS EMPR,
	Rutas AS RUTA,
	Ciudades AS ORIG,
	Ciudades AS DEST,
	Marca_Vehiculos AS MAVE,
	Vehiculos AS VEHI,
	Terceros AS COND,
	Terceros AS CLIE
WHERE
	ENMA.EMPR_Codigo = DEMA.EMPR_Codigo AND
	ENMA.Numero = DEMA.ENMA_Numero AND

	DEMA.EMPR_Codigo = ENRE.EMPR_Codigo AND
	DEMA.ENRE_Numero = ENRE.Numero AND	

	ENRE.EMPR_Codigo = RUTA.EMPR_Codigo AND
	ENRE.RUTA_Codigo = RUTA.Codigo AND

	RUTA.EMPR_Codigo = ORIG.EMPR_Codigo AND
	RUTA.CIUD_Origen = ORIG.Codigo AND

	RUTA.EMPR_Codigo = DEST.EMPR_Codigo AND
	RUTA.CIUD_Destino = DEST.Codigo AND

	ENRE.EMPR_Codigo = VEHI.EMPR_Codigo AND
	ENRE.VEHI_Placa = VEHI.Placa AND

	VEHI.EMPR_Codigo = MAVE.EMPR_Codigo AND
	VEHI.MARC_Codigo = MAVE.Codigo AND

	ENRE.EMPR_Codigo = COND.EMPR_Codigo AND
	ENRE.TERC_Conductor = COND.Codigo AND

	ENRE.EMPR_Codigo = CLIE.EMPR_Codigo AND
	ENRE.TERC_Cliente = CLIE.Codigo
GO