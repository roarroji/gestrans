﻿PRINT 'sp_actualizar_destino_seguro_manifiestos'
GO

DROP PROCEDURE sp_actualizar_destino_seguro_manifiestos
GO

CREATE PROCEDURE sp_actualizar_destino_seguro_manifiestos
(
	@par_EMPR_codigo SMALLINT,
	@par_Numero INTEGER,
	@par_Reportado SMALLINT,
	@par_Error VARCHAR(100)
)
AS
BEGIN
	UPDATE
		Encabezado_Manifiestos
	SET
		Destino_Seguro = @par_Reportado,
		Error_Destino_Seguro = @par_Error
	WHERE
		EMPR_Codigo = @par_EMPR_codigo AND
		Numero = @par_Numero
END
GO