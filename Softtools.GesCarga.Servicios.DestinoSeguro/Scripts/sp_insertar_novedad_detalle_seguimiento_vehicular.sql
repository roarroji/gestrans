﻿PRINT 'sp_insertar_novedad_detalle_seguimiento_vehicular'
GO

DROP PROCEDURE sp_insertar_novedad_detalle_seguimiento_vehicular
GO

CREATE PROCEDURE sp_insertar_novedad_detalle_seguimiento_vehicular
(
	@par_EMPR_Codigo SMALLINT,
	@par_ENMA_Numero INT,
	@par_Fecha_Reporte DATETIME,
	@par_Ubicacion VARCHAR(250),
	@par_Observaciones VARCHAR(250)
)
AS
BEGIN

	INSERT INTO Detalle_Seguimiento_Vehiculos
		(
			EMPR_Codigo,
			ENSV_Codigo,
			TIDO_Soporte,
			Numero_Documento_Soporte,
			Fecha_Hora_Reporte,
			TIRS_Codigo,
			TERC_Cliente,
			Ubicacion,
			PUCO_Codigo,
			SIRS_Codigo,
			NOSV_Codigo,
			Observaciones,
			Reportar_Cliente,
			Envio_Reporte_A_Cliente,
			Fecha_Envio_Reporte_A_Cliente,
			kilometros_Vehiculo,
			OFIC_Crea,
			USUA_Crea,
			Fecha_Crea,
			USUA_Modifica,
			Fecha_Modifica,
			Anulado,
			USUA_Anula,
			Fecha_Anula,
			Causa_Anula
		)
		SELECT
			EMPR_Codigo,
			Codigo,
			12, --TIDO Manifiesto
			@par_ENMA_Numero,
			@par_Fecha_Reporte,
			5, --Destino Seguro
			TERC_Cliente,
			@par_Ubicacion,
			0,
			20, --Durante Recorrido Destino Seguro
			50, --Novedad Seguimiento Destino Seguro
			@par_Observaciones,
			0,
			0,
			'1900-01-01',
			0,
			1,
			'ADMIN',
			GETDATE(),
			NULL,
			NULL,
			0,
			NULL,
			NULL,
			''
		FROM
			Encabezado_Seguimiento_Vehiculos
		WHERE
			EMPR_Codigo = @par_EMPR_Codigo AND
			ENMA_Numero = @par_ENMA_Numero
END
GO