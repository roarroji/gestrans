﻿Imports System.Threading
Imports DestinoSeguro.WSDestinoSeguro
Imports System.Configuration.ConfigurationManager
Imports System.Data.SqlClient
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class svDestinoSeguro

#Region "Variables"

    Private thrDespacho As Thread
    Private thrNovedades As Thread

    Private intEmpresa As Integer
    'Private objDestino As WSdestino
    Private objDestinoEvents As co.destinoseguro.integraciones.webservicescl30DestinoSeguro
    Private objGenerador As generador
    Private strMensaje As String
    Private intReportado As Integer

    Private strCadenaDeConexionSQL As String
    Private sqlConexion As SqlConnection
    Private sqlTransaccion As SqlTransaction
    Private sqlComando As SqlCommand
    Private sqlExcepcionSQL As SqlException
    Private strNombreConexion As String

    Private intDiasCreaDesp As Integer
    Private intDiasConsNove As Integer
    Private strFechaFormateada As String

    Dim strUsuario As String
    Dim strClave As String
    Dim dteSalidaVehiculo As Date
    Dim lonNitTransportadora As Long
    Dim intManifiesto As Integer
    Dim intCodiCiudOrigen As Integer
    Dim intCodiCiudDesti As Integer
    Dim intVia As Integer
    Dim strRuta As String
    Dim strPlaca As String
    Dim strMarca As String
    Dim intCarroceria As String
    Dim intColor As String
    Dim intModelo As Integer
    Dim lonIdentiConduct As Long
    Dim strNombreConduct As String
    Dim strApelliConduct As String
    Dim strDirecci As String
    Dim lonTelefono As Long
    Dim lonCelular As Long
    Dim lonNitGeneCarga As Long
    Dim strNombreGenerador As String
    Dim intSucursal As Integer
    Dim strObserva As String
    Dim strItem1 As String
    Dim strItem2 As String
    Dim strItem3 As String

    Dim dteFechRepoNove As Date
    Dim strUbicaNove As String
    Dim strObservaNove As String

    Dim Log As System.IO.StreamWriter
#End Region

#Region "Constantes"

    Const NO_ANULADO = 0
    Const EN_TRANSITO = 0
    Const NO_REPORTADO = 1

    'Estados Destino Seguro
    Const REPORTADO_SIN_EXITO = 1
    Const REPORTADO_CON_EXITO = 2
    Const ERRORES_DESTINO_SEGURO As String = "0|"

#End Region

#Region "Constructor"
    Sub New()
        InitializeComponent()
        'Me.objGenerador = New generador
        'Me.thrDespacho = New Thread(AddressOf Iniciar_Proceso_Despacho)
        'Me.thrNovedades = New Thread(AddressOf Iniciar_Proceso_Novedades)
        'Me.thrDespacho.Start()
        'Me.thrNovedades.Start()
    End Sub
#End Region

#Region "Propiedades"

    Public Property Empresa As Integer
        Get
            Return Me.intEmpresa
        End Get
        Set(ByVal value As Integer)
            Me.intEmpresa = value
        End Set
    End Property

    Public Property FechaSalidaFormat As String
        Get
            Return Me.strFechaFormateada
        End Get
        Set(ByVal value As String)
            Me.strFechaFormateada = value
        End Set
    End Property

    Public Property DiasCrearDespacho As Integer
        Get
            Return Me.intDiasCreaDesp
        End Get
        Set(ByVal value As Integer)
            Me.intDiasCreaDesp = value
        End Set
    End Property

    Public Property DiasConsultaNovedades As Integer
        Get
            Return Me.intDiasConsNove
        End Get
        Set(ByVal value As Integer)
            Me.intDiasConsNove = value
        End Set
    End Property

    Public Property Usuario As String
        Get
            Return Me.strUsuario
        End Get
        Set(ByVal value As String)
            Me.strUsuario = value
        End Set
    End Property

    Public Property Clave As String
        Get
            Return Me.strClave
        End Get
        Set(ByVal value As String)
            Me.strClave = value
        End Set
    End Property

    Public Property FechaSalidaVehiculo As Date
        Get
            Return Me.dteSalidaVehiculo
        End Get
        Set(ByVal value As Date)
            Me.dteSalidaVehiculo = value
        End Set
    End Property

    Public Property NitTransportadora As Long
        Get
            Return Me.lonNitTransportadora
        End Get
        Set(ByVal value As Long)
            Me.lonNitTransportadora = value
        End Set
    End Property

    Public Property Manifiesto As Integer
        Get
            Return Me.intManifiesto
        End Get
        Set(ByVal value As Integer)
            Me.intManifiesto = value
        End Set
    End Property

    Public Property CiudadOrigen As Integer
        Get
            Return Me.intCodiCiudOrigen
        End Get
        Set(ByVal value As Integer)
            Me.intCodiCiudOrigen = value
        End Set
    End Property

    Public Property CiudadDestino As Integer
        Get
            Return Me.intCodiCiudDesti
        End Get
        Set(ByVal value As Integer)
            Me.intCodiCiudDesti = value
        End Set
    End Property

    Public Property Via As Integer
        Get
            Return Me.intVia
        End Get
        Set(ByVal value As Integer)
            Me.intVia = value
        End Set
    End Property

    Public Property Ruta As String
        Get
            Return Me.strRuta
        End Get
        Set(ByVal value As String)
            Me.strRuta = value
        End Set
    End Property

    Public Property Placa As String
        Get
            Return Me.strPlaca
        End Get
        Set(ByVal value As String)
            Me.strPlaca = value
        End Set
    End Property

    Public Property Marca As String
        Get
            Return Me.strMarca
        End Get
        Set(ByVal value As String)
            Me.strMarca = value
        End Set
    End Property

    Public Property Carroceria As Integer
        Get
            Return Me.intCarroceria
        End Get
        Set(ByVal value As Integer)
            Me.intCarroceria = value
        End Set
    End Property

    Public Property Color As Integer
        Get
            Return Me.intColor
        End Get
        Set(ByVal value As Integer)
            Me.intColor = value
        End Set
    End Property

    Public Property Modelo As Integer
        Get
            Return Me.intModelo
        End Get
        Set(ByVal value As Integer)
            Me.intModelo = value
        End Set
    End Property

    Public Property IdentificacionConductor As Long
        Get
            Return Me.lonIdentiConduct
        End Get
        Set(ByVal value As Long)
            Me.lonIdentiConduct = value
        End Set
    End Property

    Public Property NombreConductor As String
        Get
            Return Me.strNombreConduct
        End Get
        Set(ByVal value As String)
            Me.strNombreConduct = value
        End Set
    End Property

    Public Property ApellidoConductor As String
        Get
            Return Me.strApelliConduct
        End Get
        Set(ByVal value As String)
            Me.strApelliConduct = value
        End Set
    End Property

    Public Property DireccionConductor As String
        Get
            Return Me.strDirecci
        End Get
        Set(ByVal value As String)
            Me.strDirecci = value
        End Set
    End Property

    Public Property TelefonoConductor As Long
        Get
            Return Me.lonTelefono
        End Get
        Set(ByVal value As Long)
            Me.lonTelefono = value
        End Set
    End Property

    Public Property CelularConductor As Long
        Get
            Return Me.lonCelular
        End Get
        Set(ByVal value As Long)
            Me.lonCelular = value
        End Set
    End Property

    Public Property NitGeneradorCarga As Long
        Get
            Return Me.lonNitGeneCarga
        End Get
        Set(ByVal value As Long)
            Me.lonNitGeneCarga = value
        End Set
    End Property

    Public Property NombreGenerador As String
        Get
            Return Me.strNombreGenerador
        End Get
        Set(ByVal value As String)
            Me.strNombreGenerador = value
        End Set
    End Property

    Public Property Sucursal As Integer
        Get
            Return Me.intSucursal
        End Get
        Set(ByVal value As Integer)
            Me.intSucursal = value
        End Set
    End Property

    Public Property Observaciones As String
        Get
            Return Me.strObserva
        End Get
        Set(ByVal value As String)
            Me.strObserva = value
        End Set
    End Property

    Public Property Mensaje() As String
        Get
            Return Me.strMensaje
        End Get
        Set(ByVal value As String)
            Me.strMensaje = value
        End Set
    End Property

    Public Property FechaReporteNovedad() As Date
        Get
            Return Me.dteFechRepoNove
        End Get
        Set(ByVal value As Date)
            Me.dteFechRepoNove = value
        End Set
    End Property

    Public Property Ubicacion() As String
        Get
            Return Me.strUbicaNove
        End Get
        Set(ByVal value As String)
            Me.strUbicaNove = value
        End Set
    End Property

    Public Property ObservacionesNovedad() As String
        Get
            Return Me.strObservaNove
        End Get
        Set(ByVal value As String)
            Me.strObservaNove = value
        End Set
    End Property

    Public Property NombreConexion() As String
        Get
            Return Me.strNombreConexion
        End Get
        Set(ByVal value As String)
            Me.strNombreConexion = value
        End Set
    End Property

    Public Property CadenaDeConexionSQL() As String
        Get
            Return Me.strCadenaDeConexionSQL
        End Get
        Set(ByVal value As String)
            Me.strCadenaDeConexionSQL = value
        End Set
    End Property

    Public Property ConexionSQL() As SqlConnection
        Get
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
            End If
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
            End If
            Me.sqlConexion = value
        End Set
    End Property

    Public Property TransaccionSQL() As SqlTransaction
        Get
            Return Me.sqlTransaccion
        End Get
        Set(ByVal value As SqlTransaction)
            Me.sqlTransaccion = value
        End Set
    End Property

    Public Property ComandoSQL() As SqlCommand
        Get
            Return Me.sqlComando
        End Get
        Set(ByVal value As SqlCommand)
            Me.sqlComando = value
        End Set
    End Property

    Public Property ExcepcionSQL() As SqlException
        Get
            Return Me.sqlExcepcionSQL
        End Get
        Set(ByVal value As SqlException)
            Me.sqlExcepcionSQL = value
        End Set
    End Property
#End Region

    Private Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim daDataAdapter As SqlDataAdapter
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                ConexionSQL.Open()
                daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                daDataAdapter.Fill(dsDataSet)
                ConexionSQL.Close()
            End Using

        Catch ex As Exception
            strError = ex.Message.ToString()
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

        Retorna_Dataset = dsDataSet

        Try
            dsDataSet.Dispose()
        Catch ex As Exception
            strError = ex.Message.ToString()
        End Try

    End Function

#Region "Funcionamiento del Servicio"

    Private Sub Iniciar_Proceso_Despacho()
        Try
            While (True)
                Me.objDestinoEvents = New co.destinoseguro.integraciones.webservicescl30DestinoSeguro
                Call Cargar_Valores_AppConfig()
                Call Crear_Despacho_Destino_Seguro()
                Thread.Sleep(Val(AppSettings.Get("TiempoSuspencionProcesoDespacho")))
            End While
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Iniciar_Proceso_Novedades()
        Try
            While (True)
                Me.objDestinoEvents = New co.destinoseguro.integraciones.webservicescl30DestinoSeguro
                Call Cargar_Valores_AppConfig()
                Call Consultar_Despacho_Destino_Seguro()
                Thread.Sleep(Val(AppSettings.Get("TiempoSuspencionProcesoNovedades")))
            End While
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Cargar_Valores_AppConfig()
        Me.Empresa = AppSettings.Get("Empresa")
        Me.Usuario = AppSettings.Get("UsuarioDestinoSeguro")
        Me.Clave = AppSettings.Get("ClaveDestinoSeguro")
        Me.DiasCrearDespacho = AppSettings.Get("DiasCrearDespacho")
        Me.DiasConsultaNovedades = AppSettings.Get("DiasConsultaNovedades")
        Me.CadenaDeConexionSQL = AppSettings.Get("ConexionSQLGestrans")
    End Sub

    Private Sub Crear_Despacho_Destino_Seguro()
        Try
            Dim strSQL
            Dim ComandoSQL As SqlCommand
            Dim dsDataSet As New DataSet
            Dim intManifiesto As Integer 'Long

            'IMPOCOMA - PLANILLA DESPACHOS 
            strSQL = "SELECT Numero_Documento AS Numero_Documento FROM Encabezado_Planilla_Despachos WHERE"
            strSQL += " EMPR_Codigo = " & Me.Empresa
            strSQL += " AND Fecha >= DATEADD(DAY,-" & Val(Me.DiasCrearDespacho) & ",CONVERT(DATETIME,'" & Format(Date.Today, "yyyy-MM-dd") & "',101))"
            strSQL += " AND ISNULL(Reporto_Empresa_Puesto_Control,1) <= " & NO_REPORTADO
            strSQL += " AND Anulado = " & NO_ANULADO

            'OTRAS EMPRESAS - MANIFIESTO
            'strSQL = "SELECT Numero_Documento FROM Encabezado_Manifiesto_Carga WHERE"
            'strSQL += " EMPR_Codigo = " & Me.Empresa
            'strSQL += " AND Fecha >= DATEADD(DAY,-" & Val(Me.DiasCrearDespacho) & ",CONVERT(DATETIME,'" & Format(Date.Today, "yyyy-MM-dd") & "',101))"
            'strSQL += " AND ISNULL(Reporto_Empresa_Puesto_Control,1) <= " & NO_REPORTADO
            'strSQL += " AND Anulado = " & NO_ANULADO
            'strSQL += " AND Mensaje_Empresa_Puesto_Control IS NULL"

            ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            dsDataSet = Me.Retorna_Dataset(strSQL)
            If dsDataSet.Tables(0).Rows.Count > 0 Then
                For Each Registro As DataRow In dsDataSet.Tables(0).Rows
                    intManifiesto = Val(Registro.Item("Numero_Documento"))
                    Call Reportar_Despacho_Destino_Seguro(intManifiesto)
                Next
            Else
                Me.Log = My.Computer.FileSystem.OpenTextFileWriter(AppSettings.Get("RutaLog"), True)
                Me.Log.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: No hay manifiestos pendientes por reportar")
                Me.Log.Close()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Consultar_Despacho_Destino_Seguro()
        Try
            Dim strSQL
            Dim ComandoSQL As SqlCommand
            Dim dsDataSet As New DataSet
            Dim Documento As Integer

            strSQL = "SELECT DISTINCT ENPD.Numero_Documento AS Numero_Documento FROM Encabezado_Planilla_Despachos ENPD"
            strSQL += " INNER JOIN Detalle_Seguimiento_Vehiculos DESV ON DESV.EMPR_Codigo = ENPD.EMPR_Codigo"
            strSQL += " AND DESV.ENPD_Numero = ENPD.Numero"
            strSQL += " WHERE ENPD.EMPR_Codigo = " & Me.Empresa
            strSQL += " AND Fecha >= DATEADD(DAY,-" & Val(Me.DiasConsultaNovedades) & ",CONVERT(DATETIME,'" & Format(Date.Today, "yyyy-MM-dd") & "',101))"
            strSQL += " AND Reporto_Empresa_Puesto_Control = " & REPORTADO_CON_EXITO
            strSQL += " AND ENPD.Anulado = " & NO_ANULADO
            strSQL += " AND DESV.Fin_Viaje = " & EN_TRANSITO
            'strSQL += " and enpd.numero = 51402"

            ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            dsDataSet = Me.Retorna_Dataset(strSQL)


            If dsDataSet.Tables(0).Rows.Count > 0 Then
                For Each Registro As DataRow In dsDataSet.Tables(0).Rows
                    Documento = Val(Registro.Item("Numero_Documento"))
                    Call Consultar_Novedades_Destino_Seguro(Documento)
                Next
            Else
                Me.Log = My.Computer.FileSystem.OpenTextFileWriter(AppSettings.Get("RutaLog"), True)
                Me.Log.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: No hay novedades para consultar")
                Me.Log.Close()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Reportar_Despacho_Destino_Seguro(ByVal Numero_Manifiesto As Integer)
        Try
            Dim strSQL
            Dim ComandoSQL As SqlCommand
            Dim dsDataSet As New DataSet
            Dim strMensaje As String

            'IMPOCOMA
            strSQL = "SELECT Numero_Documento, SalidaVehiculo, NitTransportadora, Manifiesto, CiudadOrigen, CiudadDestino, Via, Ruta, PlacaVehiculo, MarcaVehiculo, CarroceriaVehiculo, ColorVehiculo, "
            strSQL += "Modelo, CedulaConductor, REPLACE(NombreConductor,'Ñ','N') AS NombreConductor, REPLACE(ApellidoConductor,'Ñ','N') AS ApellidoConductor, DireccionConductor, TelefonoConductor, CelularConductor, IdentificacionCliente, Cliente, Sucursal, Observaciones "
            strSQL += "FROM V_Despacho_Destino_Seguro_IMPOCOMA WHERE EMPR_Codigo = " & Me.Empresa & " AND Numero_Documento = " & Val(Numero_Manifiesto)

            ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            dsDataSet = Me.Retorna_Dataset(strSQL)

            If dsDataSet.Tables(0).Rows.Count > 0 Then
                For Each Registro As DataRow In dsDataSet.Tables(0).Rows
                    Me.FechaSalidaVehiculo = Registro.Item("SalidaVehiculo").ToString()
                    Me.NitTransportadora = Val(Registro.Item("NitTransportadora"))
                    Me.CiudadOrigen = Val(Registro.Item("CiudadOrigen"))
                    Me.CiudadDestino = Val(Registro.Item("CiudadDestino"))
                    Me.Via = Registro.Item("Via").ToString()
                    Me.Ruta = Registro.Item("Ruta").ToString()
                    Me.Placa = Registro.Item("PlacaVehiculo").ToString()
                    Me.Marca = Registro.Item("MarcaVehiculo").ToString()
                    Me.Carroceria = Val(Registro.Item("CarroceriaVehiculo"))
                    Me.Color = Val(Registro.Item("ColorVehiculo"))
                    Me.Modelo = Val(Registro.Item("Modelo"))
                    Me.IdentificacionConductor = Val(Registro.Item("CedulaConductor"))
                    Me.NombreConductor = Registro.Item("NombreConductor").ToString()
                    Me.ApellidoConductor = Registro.Item("ApellidoConductor").ToString()
                    Me.DireccionConductor = Registro.Item("DireccionConductor").ToString()
                    Me.TelefonoConductor = Val(Registro.Item("TelefonoConductor"))
                    Me.CelularConductor = Val(Registro.Item("CelularConductor"))
                    Me.NitGeneradorCarga = Val(Registro.Item("IdentificacionCliente"))
                    Me.NombreGenerador = Registro.Item("Cliente").ToString()
                    Me.Sucursal = Val(Registro.Item("Sucursal"))
                    Me.Observaciones = Registro.Item("Observaciones")

                    Me.Observaciones = Replace(Me.Observaciones, "'", "")
                    Me.Observaciones = Replace(Me.Observaciones, """", "")
                    Me.Observaciones = IIf(Me.Observaciones Is Nothing, String.Empty, Me.Observaciones)

                    Me.FechaSalidaFormat = Format(Me.FechaSalidaVehiculo, "yyyy-MM-dd hh:mm:ss") 'Se formatea la fecha de salida del vehículo según parametrización de Destino Seguro
                    Me.Mensaje = String.Empty 'Se inicializa la propiedad Mensaje

                    strMensaje = Me.objDestinoEvents.dryDispatch(Me.Usuario, Me.Clave, Me.FechaSalidaFormat, Me.NitTransportadora, Val(Numero_Manifiesto), Me.CiudadOrigen,
                                                                 Me.CiudadDestino, Me.Via, Me.Ruta, Me.Placa, Me.Marca, Me.Carroceria, Me.Color, Me.Modelo,
                                                                 Me.IdentificacionConductor, Me.NombreConductor, Me.ApellidoConductor, Me.DireccionConductor,
                                                                 Me.TelefonoConductor, Me.CelularConductor, Me.Observaciones, Me.NitGeneradorCarga, Me.NombreGenerador,
                                                                 "", "")

                    If strMensaje.Contains("0|") Then 'Texto inicial si se presenta algún error'
                        If strMensaje.Contains("YA INSERTADO") Then
                            intReportado = REPORTADO_CON_EXITO
                            Mensaje = strMensaje
                        Else
                            intReportado = REPORTADO_SIN_EXITO
                            Mensaje = strMensaje
                        End If
                    Else
                        intReportado = REPORTADO_CON_EXITO
                        Mensaje = strMensaje
                    End If
                    Call Actualizar_Encabezado_Manifiestos(Numero_Manifiesto)
                Next
            End If
        Catch ex As Exception
            Me.Mensaje = ex.Message
        End Try
    End Sub

    Public Sub Consultar_Novedades_Destino_Seguro(ByVal Numero_Manifiesto As Integer)
        Try
            Dim dsDataSet As New DataSet
            Dim strMensaje As String

            strMensaje = Me.objDestinoEvents.consult_dispatch_last(Me.Usuario, Me.Clave, Numero_Manifiesto.ToString, "")

            If strMensaje.Length > 0 Then
                Dim tags As List(Of Single) = FindTokens(JToken.Parse(strMensaje), "id_checkpoint").Values(Of Single)().ToList()

                If tags.Count > 0 Then
                    For Each item As Integer In tags
                        Dim jsonResult = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(strMensaje)
                        Dim dateReport As String = jsonResult.Item("checkpointReports").Value(Of String)("realdate")
                        Call Insertar_Detalle_Seguimiento_Vehicular(Numero_Manifiesto.ToString, item.ToString, dateReport)
                    Next
                End If
            End If

        Catch ex As Exception
            Me.Mensaje = ex.Message
        End Try
    End Sub

    Public Function FindTokens(containerToken As JToken, name As String) As List(Of JToken)

        Dim matches = New List(Of JToken)()
        FindTokens(containerToken, name, matches)
        Return matches

    End Function

    Private Sub FindTokens(containerToken As JToken, name As String, matches As List(Of JToken))

        If containerToken.Type = JTokenType.[Object] Then
            For Each child As JProperty In containerToken.Children(Of JProperty)()
                If child.Name = name Then
                    matches.Add(child.Value)
                End If
                FindTokens(child.Value, name, matches)
            Next
        ElseIf containerToken.Type = JTokenType.Array Then
            For Each child As JToken In containerToken.Children()
                FindTokens(child, name, matches)
            Next
        End If

    End Sub

    Private Sub Actualizar_Encabezado_Manifiestos(ByVal Manifiesto As Integer)
        Try
            Dim ComandoSQL As SqlCommand = New SqlCommand("sp_actualizar_destino_seguro_manifiestos", Me.ConexionSQL)
            ComandoSQL.CommandType = CommandType.StoredProcedure

            ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.Empresa
            ComandoSQL.Parameters.Add("@par_Numero", SqlDbType.Int) : ComandoSQL.Parameters("@par_Numero").Value = Manifiesto
            ComandoSQL.Parameters.Add("@par_Reportado", SqlDbType.Int) : ComandoSQL.Parameters("@par_Reportado").Value = intReportado
            ComandoSQL.Parameters.Add("@par_Error", SqlDbType.VarChar, 200) : ComandoSQL.Parameters("@par_Error").Value = Me.Mensaje
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
                If ComandoSQL.ExecuteNonQuery = 1 Then
                    If intReportado = 1 Then
                        Me.Log = My.Computer.FileSystem.OpenTextFileWriter(AppSettings.Get("RutaLog"), True)
                        Me.Log.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: El manifiesto: " & Manifiesto & " presenta el siguiente error: " & Me.Mensaje)
                        Me.Log.Close()
                    ElseIf intReportado = 2 Then
                        Me.Log = My.Computer.FileSystem.OpenTextFileWriter(AppSettings.Get("RutaLog"), True)
                        Me.Log.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: Manifiesto " & Manifiesto & " reportado correctamente con la siguiente respuesta: " & Me.Mensaje)
                        Me.Log.Close()
                    End If
                End If
                Me.ConexionSQL.Close()
            End If
        Catch ex As Exception
            Me.Log = My.Computer.FileSystem.OpenTextFileWriter(AppSettings.Get("RutaLog"), True)
            Me.Log.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & " Resultado: " & ex.Message)
            Me.Log.Close()
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Sub

    Private Sub Insertar_Detalle_Seguimiento_Vehicular(ByVal Numero_Manifiesto As String, ByVal Ubicacion_Reporte As String, ByVal Fecha_Reporte As String)
        Try
            Dim ComandoSQL As SqlCommand = New SqlCommand("sp_insertar_novedad_detalle_seguimiento_vehicular_destino_seguro", Me.ConexionSQL)
            ComandoSQL.CommandType = CommandType.StoredProcedure

            ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.SmallInt) : ComandoSQL.Parameters("@par_EMPR_Codigo").Value = Me.Empresa
            ComandoSQL.Parameters.Add("@par_ENPD_Numero_Documento", SqlDbType.Int) : ComandoSQL.Parameters("@par_ENPD_Numero_Documento").Value = Val(Numero_Manifiesto)
            ComandoSQL.Parameters.Add("@par_Ubicacion", SqlDbType.Int) : ComandoSQL.Parameters("@par_Ubicacion").Value = Ubicacion_Reporte
            ComandoSQL.Parameters.Add("@par_Fecha_Reporte", SqlDbType.VarChar) : ComandoSQL.Parameters("@par_Fecha_Reporte").Value = Fecha_Reporte
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
                ComandoSQL.ExecuteNonQuery().ToString()
                Me.Log = My.Computer.FileSystem.OpenTextFileWriter(AppSettings.Get("RutaLog"), True)
                Me.Log.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: Se insertó novedad al manifiesto " & Numero_Manifiesto & ": Fecha Reporte - Observaciones: - Ubicación: " & Ubicacion_Reporte)
                Me.Log.Close()
            End If
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        Catch ex As Exception
            Me.Log = My.Computer.FileSystem.OpenTextFileWriter(AppSettings.Get("RutaLog"), True)
            Me.Log.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: Manifiesto " & ex.Message)
            Me.Log.Close()
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            End If
        End Try
    End Sub

    Private Function Existe_Novedad(ByVal Numero_Manifiesto As String, ByVal Ubicacion_Reporte As String, ByVal Fecha_Reporte As String, ByVal Hora_Reporte As String) As Boolean
        Try
            Dim strSQL
            Dim ComandoSQL As SqlCommand
            Dim dsDataSet As New DataSet
            Existe_Novedad = False

            Dim dteFechaAux As Date
            Date.TryParse(Fecha_Reporte, dteFechaAux)

            strSQL = "SELECT DESV.EMPR_Codigo, DESV.Fecha_Reporte, ENMC.Numero_Documento, DESV.Ubicacion"
            strSQL += " FROM Detalle_Seguimiento_Vehiculos DESV, Encabezado_Manifiesto_Carga ENMC"
            strSQL += " WHERE DESV.EMPR_Codigo = ENMC.EMPR_Codigo"
            strSQL += " AND DESV.ENMC_Numero = ENMC.Numero"
            strSQL += "  AND DESV.EMPR_Codigo = " & Me.Empresa
            strSQL += " AND ENMC.Numero_Documento = " & Val(Numero_Manifiesto)
            strSQL += " AND DESV.Ubicacion = '" & Ubicacion_Reporte & "'"
            strSQL += " AND CONVERT(DATE, DESV.Fecha_Reporte) = CONVERT(DATE,'" & dteFechaAux.Month.ToString & "/" & dteFechaAux.Day.ToString & "/" & dteFechaAux.Year.ToString & "')"
            strSQL += " AND DATEPART(HOUR, Fecha_Reporte) = " & Hora_Reporte


            ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            dsDataSet = Me.Retorna_Dataset(strSQL)

            If dsDataSet.Tables(0).Rows.Count > 0 Then
                Existe_Novedad = True
            End If
        Catch ex As Exception
            Me.Mensaje = ex.Message
        End Try
        Return Existe_Novedad
    End Function

#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)
        Me.thrDespacho = New Thread(AddressOf Iniciar_Proceso_Despacho)
        Me.thrNovedades = New Thread(AddressOf Iniciar_Proceso_Novedades)

        Me.thrDespacho.Start()
        Me.thrNovedades.Start()
    End Sub

    Protected Overrides Sub OnStop()

        Me.thrDespacho.Suspend()
        Me.thrNovedades.Suspend()

    End Sub

End Class