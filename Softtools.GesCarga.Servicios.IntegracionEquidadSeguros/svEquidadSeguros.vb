﻿Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System.Threading
Imports IntegracionEquidadSeguros.WSEquidadSeguros

Public Class svEquidadSeguros

#Region "Variables"
    Private thrRepoMani As Thread

    Private objRegistoPoliza As WSEquidadSeguros.trayectos
    Private objRespuestaPoliza As WSEquidadSeguros.DatResponse

    Private objUsuarioPoliza As WSEquidadSeguros.DatosUser
    Private objPoliza As WSEquidadSeguros.Poliza
    Private objDespacho As WSEquidadSeguros.Despacho
    Private objVehiculo As WSEquidadSeguros.Vehiculo
    Private objPropietario As WSEquidadSeguros.Propietario
    Private objConductor As WSEquidadSeguros.Conductor
    Private objCliente As WSEquidadSeguros.clientes
    Private objRemesa As WSEquidadSeguros.remesas
    Private objBeneficiario As WSEquidadSeguros.beneficiario

    Private arrRemesas() As WSEquidadSeguros.remesas
    Private arrClientes() As WSEquidadSeguros.clientes
    Private arrBeneficiarios() As WSEquidadSeguros.beneficiario

    'EG: Objetos instaciados pero no usados
    Private objColor As WSEquidadSeguros.Color
    Private objLinea As WSEquidadSeguros.Linea
    '---------------------------------------------------------------

    Private strUsuario As String
    Private strClave As String
    Private strNitUsuario As String
    Private strNitAseguradora As String
    Private intEmpresa
    Private intTiemSuspServ As Integer
    Private strCadenaDeConexionSQL As String
    Private sqlConexion As SqlConnection
    Private sqlComando As SqlCommand
    Private strNombreConexion As String
    Private intPlanilla As Integer
    Private strRutaLog As String
    Private strNombLog As String
    Private archLog As StreamWriter
    Private dsDataSet As DataSet
#End Region

#Region "Constantes"
    Private Const PENDIENTE_POR_REPORTAR As Integer = 0
    Private Const MILISEGUNDOS As Integer = 1000
    Private Const MINUTO As Integer = 60

    Private Const CODIGO_TIPO_IDENTIFICACION_CEDULA As String = "C"
    Private Const CODIGO_TIPO_IDENTIFICACION_NIT As String = "N"
    Private Const CODIGO_TIPO_IDENTIFICACION_EXTRANGERIA As String = "E"

    Private Const CODIGO_GENERO_MASCULINO As String = "M"
    Private Const CODIGO_GENERO_FEMENINO As String = "F"


    Private Const CODIGO_REPORTE_EXITOSO As Integer = 1000

    Private Const TIPO_DUENO_VEHICULO_TERCEROS As String = "2101"
#End Region

#Region "Propiedades"

    Public Property ReportarManifiestos As Thread
        Get
            Return Me.thrRepoMani
        End Get
        Set(ByVal value As Thread)
            Me.thrRepoMani = value
        End Set
    End Property

    Public Property Empresa As Integer
        Get
            Return Me.intEmpresa
        End Get
        Set(ByVal value As Integer)
            Me.intEmpresa = value
        End Set
    End Property

    Public Property Usuario As String
        Get
            Return Me.strUsuario
        End Get
        Set(ByVal value As String)
            Me.strUsuario = value
        End Set
    End Property

    Public Property Clave As String
        Get
            Return Me.strClave
        End Get
        Set(ByVal value As String)
            Me.strClave = value
        End Set
    End Property

    Public Property NitAseguradora As String
        Get
            Return Me.strNitAseguradora
        End Get
        Set(ByVal value As String)
            Me.strNitAseguradora = value
        End Set
    End Property

    Public Property NitUsuario As String
        Get
            Return Me.strNitUsuario
        End Get
        Set(ByVal value As String)
            Me.strNitUsuario = value
        End Set
    End Property

    Public Property TiempoSuspencionServicio As Integer
        Get
            Return Me.intTiemSuspServ
        End Get
        Set(ByVal value As Integer)
            Me.intTiemSuspServ = value
        End Set
    End Property

    Public Property NombreConexion() As String
        Get
            Return Me.strNombreConexion
        End Get
        Set(ByVal value As String)
            Me.strNombreConexion = value
        End Set
    End Property

    Public Property CadenaDeConexionSQL() As String
        Get
            Return Me.strCadenaDeConexionSQL
        End Get
        Set(ByVal value As String)
            Me.strCadenaDeConexionSQL = value
        End Set
    End Property

    Public Property ConexionSQL() As SqlConnection
        Get
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
            End If
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
            End If
            Me.sqlConexion = value
        End Set
    End Property

    Public Property ComandoSQL() As SqlCommand
        Get
            Return Me.sqlComando
        End Get
        Set(ByVal value As SqlCommand)
            Me.sqlComando = value
        End Set
    End Property

    Public Property Planilla() As Integer
        Get
            Return Me.intPlanilla
        End Get
        Set(ByVal value As Integer)
            Me.intPlanilla = value
        End Set
    End Property

    Public Property RutaLog() As String
        Get
            Return Me.strRutaLog
        End Get
        Set(ByVal value As String)
            Me.strRutaLog = value
        End Set
    End Property

    Public Property ArchivoLog() As StreamWriter
        Get
            Return Me.archLog
        End Get
        Set(ByVal value As StreamWriter)
            Me.archLog = value
        End Set
    End Property

    Public Property NombreLog() As String
        Get
            Return Me.strNombLog
        End Get
        Set(ByVal value As String)
            Me.strNombLog = value
        End Set
    End Property


#End Region

    Sub New()
        InitializeComponent()
        'Me.objRegistoPoliza = New WSEquidadSeguros.trayectos
        'Me.objRespuestaPoliza = New WSEquidadSeguros.DatResponse

        'Me.objUsuarioPoliza = New WSEquidadSeguros.DatosUser
        'Me.objPoliza = New WSEquidadSeguros.Poliza
        'Me.objDespacho = New WSEquidadSeguros.Despacho
        'Me.objVehiculo = New WSEquidadSeguros.Vehiculo
        'Me.objPropietario = New WSEquidadSeguros.Propietario
        'Me.objConductor = New WSEquidadSeguros.Conductor
        'Me.objCliente = New WSEquidadSeguros.clientes
        'Me.objRemesa = New WSEquidadSeguros.remesas
        'Me.objBeneficiario = New WSEquidadSeguros.beneficiario


        'Me.ReportarManifiestos = New Thread(AddressOf Iniciar_Servicio)
        'Me.ReportarManifiestos.Start()
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        Me.ReportarManifiestos = New Thread(AddressOf Iniciar_Servicio)
        Me.ReportarManifiestos.Start()
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
    End Sub

    Protected Overrides Sub OnStop()
        Me.ReportarManifiestos.Suspend()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

    Private Sub Cargar_Valores_AppConfig()
        Me.Empresa = AppSettings.Get("Empresa")
        Me.Usuario = AppSettings.Get("Usuario")
        Me.NitUsuario = AppSettings.Get("NitUsuario")
        Me.NitAseguradora = AppSettings.Get("NitAseguradora")
        Me.Clave = AppSettings.Get("Clave")
        Me.CadenaDeConexionSQL = AppSettings.Get("ConexionSQLGestrans")
        Me.TiempoSuspencionServicio = AppSettings.Get("TiempoSuspencionServicio")
        Me.NombreLog = AppSettings.Get("NombreLog")
        Me.RutaLog = AppSettings.Get("RutaArchivoLog")
    End Sub

    Private Sub Iniciar_Servicio()
        Try
            Me.objRegistoPoliza = New WSEquidadSeguros.trayectos
            Me.objRespuestaPoliza = New WSEquidadSeguros.DatResponse

            Me.objUsuarioPoliza = New WSEquidadSeguros.DatosUser
            Me.objPoliza = New WSEquidadSeguros.Poliza
            Me.objDespacho = New WSEquidadSeguros.Despacho
            Me.objVehiculo = New WSEquidadSeguros.Vehiculo
            Me.objPropietario = New WSEquidadSeguros.Propietario
            Me.objConductor = New WSEquidadSeguros.Conductor
            Me.objCliente = New WSEquidadSeguros.clientes
            Me.objRemesa = New WSEquidadSeguros.remesas
            Me.objBeneficiario = New WSEquidadSeguros.beneficiario

            While (True)
                Dim ArchivoLog As FileStream
                Call Cargar_Valores_AppConfig()

                If Not My.Computer.FileSystem.DirectoryExists(Me.RutaLog) Then
                    My.Computer.FileSystem.CreateDirectory(Me.RutaLog)
                    ArchivoLog = File.Create(Me.RutaLog & Me.NombreLog)
                Else
                    If Not My.Computer.FileSystem.FileExists(Me.RutaLog & Me.NombreLog) Then
                        ArchivoLog = File.Create(Me.RutaLog & Me.NombreLog)
                    End If
                End If
                Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: Inicio del servicio...")
                Me.ArchivoLog.Close()

                Call Consultar_Manifiestos_Pendientes_Reportar()

                Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: El servicio hibernará por un tiempo de " & (Me.TiempoSuspencionServicio / MILISEGUNDOS) / MINUTO & " minutos...")
                Me.ArchivoLog.Close()
                Thread.Sleep(Me.TiempoSuspencionServicio)

            End While
        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: " & ex.Message)
            Me.ArchivoLog.Close()
        End Try
    End Sub

    Private Sub Consultar_Manifiestos_Pendientes_Reportar()
        Try
            Dim strSQL As String
            Me.dsDataSet = New DataSet

            strSQL = "SELECT DISTINCT ENPD.Numero "
            strSQL += " FROM  Encabezado_Planilla_Despachos ENPD,"
            strSQL += " Vehiculos VEH"
            strSQL += " WHERE ENPD.EMPR_Codigo = VEH.EMPR_Codigo"
            strSQL += " AND ENPD.VEHI_Codigo = VEH.Codigo "
            strSQL += " AND ENPD.EMPR_Codigo = " & Me.Empresa
            strSQL += " AND ISNULL(ENPD.Reporto_Empresa_Aseguradora,0) = " & PENDIENTE_POR_REPORTAR
            strSQL += " AND ENPD.Valor_Seguro_Poliza > 0"
            strSQL += " AND ENPD.Anulado = 0 AND ENPD.Estado = 1 "
            strSQL += " AND VEH.CATA_TIDV_Codigo = " & TIPO_DUENO_VEHICULO_TERCEROS

            ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            dsDataSet = Retorna_Dataset(strSQL)

            If Me.dsDataSet.Tables(0).Rows.Count > 0 Then
                For Each Registro As DataRow In Me.dsDataSet.Tables(0).Rows
                    Me.Planilla = Val(Registro.Item("Numero"))
                    Call Reportar_Manifiesto(Me.Planilla)
                Next
            Else
                Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: No hay manifiestos pendientes por reportar")
                Me.ArchivoLog.Close()
            End If
        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Se presentó el siguiente error al consultar manifiestos pendientes por reportar: " & ex.Message)
            Me.ArchivoLog.Close()
        End Try
    End Sub

    Private Sub Reportar_Manifiesto(ByVal Numero_Planilla As Integer)

        Try
            Dim strSQL As String = String.Empty
            Dim dteFechaAuxiliar As Date

            ReDim arrRemesas(0)
            ReDim arrClientes(0)

            Dim strRetornaPrevisora As String = String.Empty
            Dim strMensajeErrorPrevisora As String = String.Empty
            Dim strNumeroPrevisora As String = String.Empty
            Me.dsDataSet = New DataSet

            strRetornaPrevisora = String.Empty
            strMensajeErrorPrevisora = String.Empty
            strNumeroPrevisora = String.Empty

            'EG: Usuario Póliza
            Me.objUsuarioPoliza.cod_user = Me.Usuario
            Me.objUsuarioPoliza.pas_user = Me.Clave
            Me.objUsuarioPoliza.cod_terc = Me.NitUsuario
            Me.objUsuarioPoliza.cod_aseg = Me.NitAseguradora

            'EG: Datos Póliza
            Me.objPoliza.cod_asegur = Me.NitAseguradora
            Me.objPoliza.cod_tomado = Me.NitUsuario


            'EG: Datos Manifiesto
            strSQL = "SELECT ENPD.Numero_Documento "
            strSQL += "     ,ISNULL(ESOS.Valor_Declarado,0) "
            strSQL += "     ,ENPD.Fecha "
            strSQL += "     ,ENPD.Fecha_Hora_Salida "
            strSQL += "     ,CIOR.Codigo_Alterno As Ciudad_Origen"
            strSQL += "     ,CIDE.Codigo_Alterno As Ciudad_Destino"

            strSQL += " FROM Encabezado_Planilla_Despachos ENPD,  "
            strSQL += "      Encabezado_Remesas ENRE,"
            strSQL += "      Encabezado_Solicitud_Orden_Servicios ESOS, "
            strSQL += "      Rutas RUTA, "
            strSQL += "      Ciudades CIOR, "
            strSQL += "      Ciudades CIDE "

            strSQL += " WHERE ENRE.EMPR_Codigo = ENPD.EMPR_Codigo "
            strSQL += "   AND ENRE.ENPD_Numero = ENPD.Numero"

            strSQL += " AND ENRE.EMPR_Codigo = ESOS.EMPR_Codigo"
            strSQL += "   AND ENRE.ESOS_Numero = ESOS.Numero"

            strSQL += " AND ENPD.EMPR_Codigo = RUTA.EMPR_Codigo"
            strSQL += "   And ENPD.RUTA_Codigo = RUTA.Codigo"

            strSQL += "   And RUTA.EMPR_Codigo = CIOR.EMPR_Codigo"
            strSQL += "   And RUTA.CIUD_Codigo_Origen = CIOR.Codigo"

            strSQL += "   And RUTA.EMPR_Codigo = CIDE.EMPR_Codigo"
            strSQL += "   And RUTA.CIUD_Codigo_Destino = CIDE.Codigo"

            strSQL += "   And ENPD.EMPR_Codigo = " & Me.Empresa & " And ENPD.Numero = " & Numero_Planilla

            Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            Me.dsDataSet = Retorna_Dataset(strSQL)

            If Me.dsDataSet.Tables(0).Rows.Count > 0 Then
                Me.objDespacho.num_manifi = Val(Me.dsDataSet.Tables(0).Rows(0).Item(0))
                Me.objDespacho.val_declar = Val(Me.dsDataSet.Tables(0).Rows(0).Item(1))

                Me.objDespacho.cod_ciuori = Val(Me.dsDataSet.Tables(0).Rows(0).Item(4))
                Me.objDespacho.cod_ciudes = Val(Me.dsDataSet.Tables(0).Rows(0).Item(5))

                If Me.objDespacho.cod_ciuori = Me.objDespacho.cod_ciudes Then
                    Me.objDespacho.cod_despac = 1
                Else
                    Me.objDespacho.cod_despac = 2
                End If

                Date.TryParse(Me.dsDataSet.Tables(0).Rows(0).Item(2), dteFechaAuxiliar)
                Me.objDespacho.fec_salida = Format(dteFechaAuxiliar, "yyyy-MM-dd")

                Date.TryParse(Me.dsDataSet.Tables(0).Rows(0).Item(3), dteFechaAuxiliar)
                Me.objDespacho.fec_entreg = Format(dteFechaAuxiliar, "yyyy-MM-dd")

                Me.objDespacho.ind_exceso = String.Empty
                Me.objDespacho.val_fletex = String.Empty
                Me.objDespacho.val_netoxx = String.Empty

            End If


            'EG: Datos Vehículo
            strSQL = "Select VEHI.Placa  "
            strSQL += "     ,VEHI.MAVE_Codigo "
            strSQL += "     ,VEHI.LIVE_Codigo "
            strSQL += "     ,VEHI.CATA_TICA_Codigo "
            strSQL += "     ,VEHI.Modelo "
            strSQL += "     ,ISNULL(VEHI.Modelo_Repotenciado,' ') "
            strSQL += "     ,VEHI.Numero_Motor "
            strSQL += "     ,VEHI.COVE_Codigo "
            strSQL += "     ,VEHI.Peso_Bruto "
            strSQL += "     ,VEHI.Numero_Serie "
            strSQL += "     ,TIVE.CodConfigSWEquidad " 'EP: Aqui en la version estaba como TIVE.CodigoMinisterioAnterior
            strSQL += "     ,ISNULL(SEMI.Placa,'') Placa_Remolque "

            strSQL += " FROM Encabezado_Planilla_Despachos ENPD, "
            strSQL += "      Vehiculos VEHI "
            strSQL += "      LEFT JOIN Semirremolques SEMI "
            strSQL += "      ON VEHI.SEMI_CODIGO = SEMI.Codigo, "
            strSQL += "      V_Tipo_Vehiculos TIVE"
            strSQL += " WHERE ENPD.EMPR_Codigo = VEHI.EMPR_Codigo"
            strSQL += "   And ENPD.VEHI_Codigo = VEHI.Codigo"
            strSQL += "   And VEHI.EMPR_Codigo = TIVE.EMPR_Codigo"
            strSQL += "   And VEHI.CATA_TIVE_Codigo = TIVE.Codigo"

            strSQL += "   And ENPD.EMPR_Codigo = " & Me.Empresa & " And ENPD.Numero = " & Numero_Planilla

            Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            Me.dsDataSet = Retorna_Dataset(strSQL)

            If Me.dsDataSet.Tables(0).Rows.Count > 0 Then
                Me.objVehiculo.num_placax = Me.dsDataSet.Tables(0).Rows(0).Item(0)
                Me.objVehiculo.cod_marcax = Me.dsDataSet.Tables(0).Rows(0).Item(1)
                Me.objVehiculo.cod_lineax = Trim(Me.dsDataSet.Tables(0).Rows(0).Item(2).ToString)
                Me.objVehiculo.cod_carroc = Me.dsDataSet.Tables(0).Rows(0).Item(3)
                Me.objVehiculo.ano_modelo = Me.dsDataSet.Tables(0).Rows(0).Item(4)
                Me.objVehiculo.ano_repote = Me.dsDataSet.Tables(0).Rows(0).Item(5)
                Me.objVehiculo.num_remolq = Me.dsDataSet.Tables(0).Rows(0).Item(11)
                Me.objVehiculo.num_config = Me.dsDataSet.Tables(0).Rows(0).Item(10)
                Me.objVehiculo.num_motorx = Me.dsDataSet.Tables(0).Rows(0).Item(6)
                Me.objVehiculo.cod_colorx = Me.dsDataSet.Tables(0).Rows(0).Item(7)
                Me.objVehiculo.val_pesove = Val(Me.dsDataSet.Tables(0).Rows(0).Item(8))
                Me.objVehiculo.num_seriex = Me.dsDataSet.Tables(0).Rows(0).Item(9)

            End If

            'EG: Datos Propietario
            strSQL = "Select PROP.Numero_Identificacion "
            strSQL += "     ,TIID.Campo1 " 'EP: ESTABA TIID_Codigo EN LA VERSION 4.7 
            strSQL += "     ,PROP.Razon_Social + PROP.Nombre As Nombre"
            strSQL += "     ,PROP.Apellido1 "
            strSQL += "     ,PROP.Celulares "  'EP: ESTABA CELULAREN LA VERSION 4.7 
            strSQL += "     ,PROP.Direccion "
            strSQL += "     ,PAIS.Codigo_Alterno As Codigo_Pais "
            strSQL += "     ,DEPA.Codigo_DANE As Codigo_Departamento "
            strSQL += "     ,CIUD.Codigo_Alterno As Codigo_Ciudad "
            strSQL += "     ,PROP.Celulares " ' EP: ESTABA Telefono1  EN LA VERSION 4.7 

            strSQL += " FROM Encabezado_Planilla_Despachos ENPD,  "
            strSQL += "      Vehiculos VEHI, "
            strSQL += "      Terceros PROP, "
            strSQL += "      Paises PAIS, "
            strSQL += "      Ciudades CIUD, "
            strSQL += "      Departamentos DEPA, "
            strSQL += "    Valor_Catalogos TIID " 'EP: SE agrego el catalgo para el tipo de identificacion 
            strSQL += " WHERE ENPD.EMPR_Codigo = VEHI.EMPR_Codigo"
            strSQL += "  And ENPD.VEHI_Codigo = VEHI.Codigo " ' EP: ESTABA EN LA VERSION 4.7  CON VEHI_PLACA

            strSQL += "   And VEHI.EMPR_Codigo = PROP.EMPR_Codigo"
            strSQL += "   And VEHI.TERC_Codigo_Propietario = PROP.Codigo" 'EP´: ESTABA EN LA VERSION 4.7  TERC_PROPIETARIO 

            strSQL += "   And PROP.EMPR_Codigo = PAIS.EMPR_Codigo"
            strSQL += "   And PROP.PAIS_Codigo = PAIS.Codigo"

            strSQL += "   And PROP.EMPR_Codigo = CIUD.EMPR_Codigo"
            strSQL += "   And PROP.CIUD_Codigo = CIUD.Codigo"
            'EP: SE AGREGO PARA TRAER EL TIPO DE IDENTIFICACION DEL PROPIETARIO 
            strSQL += "   And PROP.EMPR_Codigo = TIID.EMPR_Codigo "
            strSQL += "   And PROP.CATA_TIID_Codigo = TIID.Codigo"

            strSQL += "   And CIUD.EMPR_Codigo = DEPA.EMPR_Codigo"
            strSQL += "   And CIUD.DEPA_Codigo = DEPA.Codigo"

            strSQL += "   And ENPD.EMPR_Codigo = " & Me.Empresa & " And ENPD.Numero = " & Numero_Planilla

            Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            Me.dsDataSet = Retorna_Dataset(strSQL)

            If Me.dsDataSet.Tables(0).Rows.Count > 0 Then
                Me.objPropietario.cod_propie = Me.dsDataSet.Tables(0).Rows(0).Item(0)

                If Me.dsDataSet.Tables(0).Rows(0).Item(1) = "CC" Then
                    Me.objPropietario.cod_docpro = CODIGO_TIPO_IDENTIFICACION_CEDULA
                ElseIf Me.dsDataSet.Tables(0).Rows(0).Item(1) = "NIT" Then
                    Me.objPropietario.cod_docpro = CODIGO_TIPO_IDENTIFICACION_NIT
                Else
                    Me.objPropietario.cod_docpro = CODIGO_TIPO_IDENTIFICACION_EXTRANGERIA
                End If

                Me.objPropietario.nom_propie = Me.dsDataSet.Tables(0).Rows(0).Item(2)
                Me.objPropietario.abr_prorie = String.Empty
                Me.objPropietario.ape_propie = Me.dsDataSet.Tables(0).Rows(0).Item(3)
                Me.objPropietario.cel_propie = Me.dsDataSet.Tables(0).Rows(0).Item(4)
                Me.objPropietario.dir_propie = Me.dsDataSet.Tables(0).Rows(0).Item(5)
                Me.objPropietario.pai_propie = Me.dsDataSet.Tables(0).Rows(0).Item(6)
                Me.objPropietario.dep_propie = Me.dsDataSet.Tables(0).Rows(0).Item(7)
                Me.objPropietario.ciu_propie = Me.dsDataSet.Tables(0).Rows(0).Item(8)
                Me.objPropietario.tel_propie = String.Empty
                Me.objPropietario.gen_propie = CODIGO_GENERO_MASCULINO
            End If

            'EG: Datos Conductor
            strSQL = "Select COND.Numero_Identificacion "
            strSQL += "     ,TIID.Campo1 " 'EP: ESTABA TIID_Codigo EN LA VERSION 4.7 
            strSQL += "     ,COND.Razon_Social + COND.Nombre As Nombre"
            strSQL += "     ,COND.Apellido1 "
            strSQL += "     ,COND.Celulares "  'EP: ESTABA CELULAREN LA VERSION 4.7 
            strSQL += "     ,COND.Direccion "
            strSQL += "     ,PAIS.Codigo_Alterno As Codigo_Pais "
            strSQL += "     ,DEPA.Codigo_DANE As Codigo_Departamento " 'EP: ESTABA CODIGO_DANE EN LA VERSION 4.7 
            strSQL += "     ,CIUD.Codigo_Alterno As Codigo_Ciudad "

            strSQL += "     ,COND.Celulares " ' EP: ESTABA Telefono1  EN LA VERSION 4.7 
            strSQL += "     ,TECO.Fecha_Vencimiento_Licencia    "
            strSQL += "     ,TILE.Campo1     Categoria_Licencia  "
            strSQL += "     ,TECO.Numero_Licencia   "
            strSQL += "     ,COND.Emails "  'EP: ESTABA Email  EN LA VERSION 4.7 

            strSQL += " FROM Encabezado_Planilla_Despachos ENPD,  "
            strSQL += "   Vehiculos VEHI, "
            strSQL += "   Terceros COND, "
            strSQL += "   Paises PAIS, "
            strSQL += "   Ciudades CIUD, "
            strSQL += "   Departamentos DEPA, "
            strSQL += "   Valor_Catalogos TIID, " 'EP: SE agrego el catalgo para el tipo de identificacion 
            strSQL += "   Tercero_Conductores TECO,"
            strSQL += "   Valor_Catalogos TILE"
            strSQL += "  WHERE ENPD.EMPR_Codigo = VEHI.EMPR_Codigo"
            strSQL += "  And ENPD.VEHI_Codigo = VEHI.Codigo " ' EP: ESTABA EN LA VERSION 4.7  CON VEHI_PLACA

            strSQL += "   And VEHI.EMPR_Codigo = COND.EMPR_Codigo"
            strSQL += "   And VEHI.TERC_Codigo_Conductor = COND.Codigo" 'EP´: ESTABA EN LA VERSION 4.7  TERC_CONDUCTOR

            strSQL += "   And COND.EMPR_Codigo = PAIS.EMPR_Codigo"
            strSQL += "   And COND.PAIS_Codigo = PAIS.Codigo"

            strSQL += "   And COND.EMPR_Codigo = CIUD.EMPR_Codigo"
            strSQL += "   And COND.CIUD_Codigo = CIUD.Codigo"

            strSQL += "   And CIUD.EMPR_Codigo = DEPA.EMPR_Codigo"
            strSQL += "   And CIUD.DEPA_Codigo = DEPA.Codigo"

            'EP: SE AGREGO PARA TRAER EL TIPO DE IDENTIFICACION DEL PROPIETARIO 
            strSQL += "  And COND.EMPR_Codigo = TIID.EMPR_Codigo "
            strSQL += "  And COND.CATA_TIID_Codigo = TIID.Codigo"
            'EP: SE AGREGO PARA TRAER NUMERO DE LA LICENCIA Y LA FECHA 
            strSQL += "  And COND.EMPR_Codigo = TECO.EMPR_Codigo "
            strSQL += "  And COND.Codigo = TECO.TERC_Codigo"

            strSQL += "  And TECO.EMPR_Codigo = TILE.EMPR_Codigo "
            strSQL += "  And TECO.CATA_CALC_Codigo = TILE.Codigo"

            strSQL += "   And ENPD.EMPR_Codigo = " & Me.Empresa & " And ENPD.Numero = " & Numero_Planilla

            Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            Me.dsDataSet = Retorna_Dataset(strSQL)

            If Me.dsDataSet.Tables(0).Rows.Count > 0 Then
                Me.objConductor.cod_conduc = Me.dsDataSet.Tables(0).Rows(0).Item(0)

                If Me.dsDataSet.Tables(0).Rows(0).Item(1) = "CC" Then
                    Me.objConductor.cod_tipdoc = CODIGO_TIPO_IDENTIFICACION_CEDULA
                ElseIf Me.dsDataSet.Tables(0).Rows(0).Item(1) = "NIT" Then
                    Me.objConductor.cod_tipdoc = CODIGO_TIPO_IDENTIFICACION_NIT
                Else
                    Me.objConductor.cod_tipdoc = CODIGO_TIPO_IDENTIFICACION_EXTRANGERIA
                End If

                Me.objConductor.nom_conduc = Me.dsDataSet.Tables(0).Rows(0).Item(2)
                Me.objConductor.abr_conduc = String.Empty
                Me.objConductor.ape_conduc = Me.dsDataSet.Tables(0).Rows(0).Item(3)
                Me.objConductor.cel_conduc = Me.dsDataSet.Tables(0).Rows(0).Item(4)
                Me.objConductor.dir_conduc = Me.dsDataSet.Tables(0).Rows(0).Item(5)
                Me.objConductor.pai_conduc = Me.dsDataSet.Tables(0).Rows(0).Item(6)
                Me.objConductor.dep_conduc = Me.dsDataSet.Tables(0).Rows(0).Item(7)
                Me.objConductor.ciu_conduc = Me.dsDataSet.Tables(0).Rows(0).Item(8)
                Me.objConductor.tel_conduc = String.Empty
                Me.objConductor.gen_conduc = CODIGO_GENERO_MASCULINO

                Me.objConductor.fec_venlic = Me.dsDataSet.Tables(0).Rows(0).Item(10)
                Me.objConductor.cod_catego = Me.dsDataSet.Tables(0).Rows(0).Item(11)
                Me.objConductor.lic_conduc = Me.dsDataSet.Tables(0).Rows(0).Item(12)
                Me.objConductor.ema_conduc = String.Empty
            End If


            'EG: Datos Clientes
            strSQL = "Select CLIE.Codigo "
            strSQL += "     ,TIID.Campo1 " 'EP: ESTABA TIID_Codigo EN LA VERSION 4.7 
            strSQL += "     ,CLIE.Razon_Social + CLIE.Nombre As Nombre "
            strSQL += "     ,CLIE.Apellido1 "
            strSQL += "     ,CLIE.Celulares "  'EP: ESTABA CELULAREN LA VERSION 4.7 
            strSQL += "     ,CLIE.Direccion "
            strSQL += "     ,PAIS.Codigo_Alterno As Codigo_Pais "
            'MODIFICACION SC : 2 DIGITOS CODIGO CIUDADES
            strSQL += "     ,DEPA.Codigo_DANE As Codigo_Departamento " 'EP: ESTABA CODIGO_DANE EN LA VERSION 4.7 
            strSQL += "     ,CIUD.Codigo_Alterno As Codigo_Ciudad "
            strSQL += "     ,CLIE.Telefonos "

            strSQL += " FROM Encabezado_Remesas ENRE, "
            strSQL += "      Terceros CLIE, "
            strSQL += "      Paises PAIS, "
            strSQL += "      Ciudades CIUD, "
            strSQL += "      Departamentos DEPA, "
            strSQL += "   Valor_Catalogos TIID " 'EP: SE agrego el catalgo para el tipo de identificacion 
            strSQL += " WHERE ENRE.EMPR_Codigo = CLIE.EMPR_Codigo"
            strSQL += "   And ENRE.TERC_Codigo_Cliente = CLIE.Codigo"

            strSQL += "   And CLIE.EMPR_Codigo = PAIS.EMPR_Codigo"
            strSQL += "   And CLIE.PAIS_Codigo = PAIS.Codigo"

            strSQL += "   And CLIE.EMPR_Codigo = CIUD.EMPR_Codigo"
            strSQL += "   And CLIE.CIUD_Codigo = CIUD.Codigo"

            'EP: SE AGREGO PARA TRAER EL TIPO DE IDENTIFICACION DEL PROPIETARIO 
            strSQL += "  And CLIE.EMPR_Codigo = TIID.EMPR_Codigo "
            strSQL += "  And CLIE.CATA_TIID_Codigo = TIID.Codigo"

            strSQL += "   And CIUD.EMPR_Codigo = DEPA.EMPR_Codigo"
            strSQL += "   And CIUD.DEPA_Codigo = DEPA.Codigo"

            strSQL += "   And ENRE.EMPR_Codigo = " & Me.Empresa & " And ENRE.ENPD_Numero = " & Numero_Planilla

            Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            Me.dsDataSet = Retorna_Dataset(strSQL)

            If Me.dsDataSet.Tables(0).Rows.Count > 0 Then
                ReDim Me.arrClientes(Me.dsDataSet.Tables(0).Rows.Count - 1)
                For i = 0 To Me.dsDataSet.Tables(0).Rows.Count - 1
                    Me.objCliente = New WSEquidadSeguros.clientes

                    Me.objCliente.cod_cliente = Me.dsDataSet.Tables(0).Rows(0).Item(0)

                    If Me.dsDataSet.Tables(0).Rows(0).Item(1) = "CC" Then
                        Me.objCliente.cod_doccli = CODIGO_TIPO_IDENTIFICACION_CEDULA
                    ElseIf Me.dsDataSet.Tables(0).Rows(0).Item(1) = "NIT" Then
                        Me.objCliente.cod_doccli = CODIGO_TIPO_IDENTIFICACION_NIT
                    Else
                        Me.objCliente.cod_doccli = CODIGO_TIPO_IDENTIFICACION_EXTRANGERIA
                    End If


                    Me.objCliente.nom_cliente = Me.dsDataSet.Tables(0).Rows(0).Item(2)
                    Me.objCliente.abr_cliente = String.Empty
                    Me.objCliente.ape_cliente = Me.dsDataSet.Tables(0).Rows(0).Item(3)
                    Me.objCliente.cel_cliente = Me.dsDataSet.Tables(0).Rows(0).Item(4)
                    Me.objCliente.dir_cliente = Me.dsDataSet.Tables(0).Rows(0).Item(5)
                    Me.objCliente.pai_cliente = Me.dsDataSet.Tables(0).Rows(0).Item(6)
                    Me.objCliente.dep_cliente = Me.dsDataSet.Tables(0).Rows(0).Item(7)
                    Me.objCliente.ciu_cliente = Me.dsDataSet.Tables(0).Rows(0).Item(8)
                    Me.objCliente.tel_cliente = Me.dsDataSet.Tables(0).Rows(0).Item(9)
                    Me.objCliente.gen_cliente = String.Empty
                    Me.objCliente.tip_cliente = String.Empty

                    Me.arrClientes(i) = Me.objCliente
                Next
            End If


            'EG: Datos Remesas
            strSQL = "Select ENRE.Numero_Documento "
            strSQL += "     ,CLIE.Numero_Identificacion "
            strSQL += "     ,PRTR.Nombre "


            strSQL += "  FROM Encabezado_Remesas ENRE, "
            strSQL += "  Terceros CLIE, "
            strSQL += "  Producto_Transportados PRTR "

            strSQL += "  WHERE   ENRE.EMPR_Codigo = CLIE.EMPR_Codigo"
            strSQL += "  And ENRE.TERC_Codigo_Cliente = CLIE.Codigo  "
            strSQL += "  And ENRE.TIDO_Codigo = 100 "

            strSQL += "  And ENRE.EMPR_Codigo = PRTR.EMPR_Codigo  "
            strSQL += "  And ENRE.PRTR_Codigo = PRTR.Codigo"
            strSQL += "  And ENRE.EMPR_Codigo = " & Me.Empresa & " And ENRE.ENPD_Numero = " & Numero_Planilla

            Me.ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
            Me.dsDataSet = Retorna_Dataset(strSQL)

            If Me.dsDataSet.Tables(0).Rows.Count > 0 Then
                ReDim Me.arrRemesas(Me.dsDataSet.Tables(0).Rows.Count - 1)
                For i = 0 To Me.dsDataSet.Tables(0).Rows.Count - 1
                    Me.objRemesa = New WSEquidadSeguros.remesas

                    Me.objRemesa.dan_remesa = Me.dsDataSet.Tables(0).Rows(i).Item(0)
                    Me.objRemesa.nit_propie = Me.dsDataSet.Tables(0).Rows(i).Item(1)
                    Me.objRemesa.dan_client = CODIGO_GENERO_MASCULINO
                    Me.objRemesa.dan_mercan = Me.dsDataSet.Tables(0).Rows(i).Item(2)

                    Me.arrRemesas(i) = Me.objRemesa
                Next

            End If


            Try

                Me.objRespuestaPoliza = Me.objRegistoPoliza.setPoliza(Me.objUsuarioPoliza, Me.objPoliza, Me.objVehiculo, Me.objPropietario, Me.objConductor, Me.objDespacho, Me.arrClientes, Me.arrRemesas, Me.arrBeneficiarios, Me.objColor, Me.objLinea)

                If Me.objRespuestaPoliza.cod_respons = CODIGO_REPORTE_EXITOSO Then
                    strMensajeErrorPrevisora = Me.objRespuestaPoliza.dat_polizax.num_poliza
                    strNumeroPrevisora = Me.objRespuestaPoliza.dat_polizax.num_certif
                Else
                    strMensajeErrorPrevisora = Me.objRespuestaPoliza.msg_respons
                    strNumeroPrevisora = PENDIENTE_POR_REPORTAR

                End If
            Catch ex As Exception
                strMensajeErrorPrevisora = ex.Message
                strNumeroPrevisora = PENDIENTE_POR_REPORTAR
                Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Se presentó el siguiente error al reportar los manifiestos: " & ex.Message)
                Me.ArchivoLog.Close()
            End Try
            Call Actualizar_Respuesta_Empresa_Aseguradora(Me.Empresa, Numero_Planilla, strNumeroPrevisora, strMensajeErrorPrevisora)

        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Se presentó el siguiente error al consultar la vista V_Reporte_Previsora_Seguros: " & ex.Message)
            Me.ArchivoLog.Close()
        End Try
    End Sub

    Private Sub Actualizar_Respuesta_Empresa_Aseguradora(ByVal CodigoEmpresa As Integer, ByVal NumeroPlanilla As Integer, ByVal NumeroPrevisora As String, ByVal MensajePrevisora As String)
        Try

            Me.ComandoSQL = New SqlCommand("sp_modificar_numero_previsora_manifiesto", Me.ConexionSQL)
            Me.ComandoSQL.CommandType = CommandType.StoredProcedure

            Me.ComandoSQL.Parameters.Add("@par_EMPR_Codigo", SqlDbType.Int) : Me.ComandoSQL.Parameters("@par_EMPR_Codigo").Value = CodigoEmpresa
            Me.ComandoSQL.Parameters.Add("@par_Numero", SqlDbType.Int) : Me.ComandoSQL.Parameters("@par_Numero").Value = NumeroPlanilla
            Me.ComandoSQL.Parameters.Add("@par_NumeroConfirmacionEmpresa", SqlDbType.VarChar, 20) : Me.ComandoSQL.Parameters("@par_NumeroConfirmacionEmpresa").Value = NumeroPrevisora
            Me.ComandoSQL.Parameters.Add("@par_MensajeEmpresa", SqlDbType.VarChar, 200) : Me.ComandoSQL.Parameters("@par_MensajeEmpresa").Value = MensajePrevisora

            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
            End If

            If Me.ComandoSQL.ExecuteNonQuery() = 1 Then
                If NumeroPrevisora <> PENDIENTE_POR_REPORTAR Then
                    Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                    Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "La planilla " & NumeroPlanilla & " fue reportado correctamente con número de confirmación " & NumeroPrevisora)
                    Me.ArchivoLog.Close()
                Else
                    Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
                    Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "La planilla " & NumeroPlanilla & " presentó el siguiente error en su reporte: " & MensajePrevisora)
                    Me.ArchivoLog.Close()
                End If
            End If
        Catch ex As Exception
            Me.ArchivoLog = My.Computer.FileSystem.OpenTextFileWriter(Me.RutaLog & Me.NombreLog, True)
            Me.ArchivoLog.WriteLine("Fecha: " & Date.Now & CChar(ChrW(9)) & "Resultado: Se presentó el siguiente error al reportar la planilla " & NumeroPlanilla & ":" & ex.Message)
            Me.ArchivoLog.Close()
        Finally
            If Me.ConexionSQL.State = ConnectionState.Open Then
                Me.ConexionSQL.Close()
            Else
                Me.ConexionSQL.Open()
            End If
        End Try
    End Sub

    Private Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim daDataAdapter As SqlDataAdapter
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                ConexionSQL.Open()
                daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                daDataAdapter.Fill(dsDataSet)
                ConexionSQL.Close()
            End Using

        Catch ex As Exception
            strError = ex.Message.ToString()
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

        Retorna_Dataset = dsDataSet

        Try
            dsDataSet.Dispose()
        Catch ex As Exception
            strError = ex.Message.ToString()
        End Try

    End Function

End Class
