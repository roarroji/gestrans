﻿Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.IO

Public Class clsGeneral

#Region "Variables Generales"
    ' Generales
    Private intCodigoEmpresa As Integer

    ' Archivos Planos
    Dim stmStreamW As Stream
    Dim stwStreamWriter As StreamWriter
    Dim stwStreamReader As StreamReader
    Dim strRutaArchivoLog As String

    ' Base Datos GESTRANS
    Private strCadenaConexionSQLGESTRANS As String
    Private sqlConexionGESTRANS As SqlConnection
    Private sqlComandoGESTRANS As SqlCommand

    ' Base Datos PSL
    Private strCadenaConexionSQLPSL As String
    Private sqlConexionPSL As SqlConnection
    Private sqlComandoPSL As SqlCommand

    Private sqlTransaccion As SqlTransaction
    Private sqlExcepcionSQL As SqlException

#End Region

#Region "Constantes"

    ' Generales
    Public Const PRIMER_TABLA As Integer = 0
    Public Const PRIMER_REGISTRO As Integer = 0

    ' Tipos Campo Base Datos
    Public Const CAMPO_NUMERICO As Byte = 1
    Public Const CAMPO_ALFANUMERICO As Byte = 2

#End Region

    Public Sub New()
        Try
            ' Inicializar Variables
            Me.intCodigoEmpresa = ConfigurationSettings.AppSettings("Empresa").ToString()
            Me.strCadenaConexionSQLGESTRANS = ConfigurationSettings.AppSettings("ConexionSQLGESTRANS").ToString()
            Me.strCadenaConexionSQLPSL = ConfigurationSettings.AppSettings("ConexionSQLPSL").ToString()
            Me.strRutaArchivoLog = ConfigurationSettings.AppSettings("RutaArchivoLog").ToString() '+ Day(Now) + Month(Now) + Year(Now)

            Me.sqlConexionPSL = New SqlConnection(strCadenaConexionSQLPSL)
            Me.sqlConexionGESTRANS = New SqlConnection(strCadenaConexionSQLGESTRANS)

        Catch ex As Exception
            Call Guardar_Mensaje_Log("clsGeneral" + ex.Message)
        End Try
    End Sub

#Region "Acceso Base Datos"

    Public Property CadenaConexionSQLGESTRANS() As String
        Get
            Return Me.strCadenaConexionSQLGESTRANS
        End Get
        Set(ByVal value As String)
            Me.strCadenaConexionSQLGESTRANS = value
        End Set
    End Property

    Public Property CadenaConexionSQLPSL() As String
        Get
            Return Me.strCadenaConexionSQLPSL
        End Get
        Set(ByVal value As String)
            Me.strCadenaConexionSQLPSL = value
        End Set
    End Property

    Public Property ConexionGESTRANS() As SqlConnection
        Get
            Return Me.sqlConexionGESTRANS
        End Get
        Set(ByVal value As SqlConnection)
            Me.sqlConexionGESTRANS = value
        End Set
    End Property

    Public Property ConexionPSL() As SqlConnection
        Get
            Return Me.sqlConexionPSL
        End Get
        Set(ByVal value As SqlConnection)
            Me.sqlConexionPSL = value
        End Set
    End Property

    Public Property ConexionSQLGESTRANS() As SqlConnection
        Get
            If IsNothing(Me.sqlConexionGESTRANS) Then
                Me.sqlConexionGESTRANS = New SqlConnection(Me.strCadenaConexionSQLGESTRANS)
            End If
            Return Me.sqlConexionGESTRANS
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexionGESTRANS) Then
                Me.sqlConexionGESTRANS = New SqlConnection(Me.strCadenaConexionSQLGESTRANS)
            End If
            Me.sqlConexionGESTRANS = value
        End Set
    End Property

    Public Property ConexionSQLPSL() As SqlConnection
        Get
            If IsNothing(Me.sqlConexionPSL) Then
                Me.sqlConexionPSL = New SqlConnection(Me.strCadenaConexionSQLPSL)
            End If
            Return Me.sqlConexionPSL
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexionPSL) Then
                Me.sqlConexionPSL = New SqlConnection(Me.strCadenaConexionSQLPSL)
            End If
            Me.sqlConexionPSL = value
        End Set
    End Property

    Public Property ComandoSQLGESTRANS() As SqlCommand
        Get
            Return Me.sqlComandoGESTRANS
        End Get
        Set(ByVal value As SqlCommand)
            Me.sqlComandoGESTRANS = value
        End Set
    End Property

    Public Property ComandoSQLPSL() As SqlCommand
        Get
            Return Me.sqlComandoPSL
        End Get
        Set(ByVal value As SqlCommand)
            Me.sqlComandoPSL = value
        End Set
    End Property

    Public Property TransaccionSQL() As SqlTransaction
        Get
            Return Me.sqlTransaccion
        End Get
        Set(ByVal value As SqlTransaction)
            Me.sqlTransaccion = value
        End Set
    End Property

    Public Property ExcepcionSQL() As SqlException
        Get
            Return Me.sqlExcepcionSQL
        End Get
        Set(ByVal value As SqlException)
            Me.sqlExcepcionSQL = value
        End Set
    End Property

    Public Function Ejecutar_SQL(ByVal strSQL As String, Optional ByRef strError As String = "") As Boolean
        Dim lonRegiAfec As Long = 0

        Try
            Dim ComandoSQL As New SqlCommand
            ConexionSQLGESTRANS = New SqlConnection(Me.strCadenaConexionSQLGESTRANS)
            ConexionSQLGESTRANS.Open()

            ComandoSQL.CommandType = CommandType.Text
            ComandoSQL.CommandText = strSQL
            ComandoSQL.Connection = ConexionSQLGESTRANS

            lonRegiAfec = Val(ComandoSQL.ExecuteNonQuery().ToString)

            Ejecutar_SQL = True

            ConexionSQLGESTRANS.Close()
            ConexionSQLGESTRANS.Dispose()

        Catch ex As Exception
            strError = ex.Message
            Ejecutar_SQL = False

            If ConexionSQLGESTRANS.State() = ConnectionState.Open Then
                ConexionSQLGESTRANS.Close()
            End If
        Finally

        End Try

        Return lonRegiAfec

    End Function

    Public Function Ejecutar_SQL_PSL(ByVal strSQL As String, Optional ByRef strError As String = "") As Boolean
        Dim lonRegiAfec As Long = 0

        Try
            Dim ComandoSQLPSL As New SqlCommand
            ConexionSQLPSL = New SqlConnection(Me.strCadenaConexionSQLPSL)
            ConexionSQLPSL.Open()

            ComandoSQLPSL.CommandType = CommandType.Text
            ComandoSQLPSL.CommandText = strSQL
            ComandoSQLPSL.Connection = ConexionSQLPSL

            lonRegiAfec = Val(ComandoSQLPSL.ExecuteNonQuery().ToString)

            ConexionSQLPSL.Close()
            ConexionSQLPSL.Dispose()

            Ejecutar_SQL_PSL = True

        Catch ex As Exception
            strError = ex.Message
            Ejecutar_SQL_PSL = False

            If ConexionSQLPSL.State() = ConnectionState.Open Then
                ConexionSQLPSL.Close()
            End If
        Finally

        End Try

        Return lonRegiAfec

    End Function

    Function Retorna_Campo_BD(ByVal CodigoEmpresa As Integer, ByVal Tabla As String, ByVal CampoConsulta As String, ByVal CampoLlave As String, ByVal TipoLlave As Integer, ByVal ValorLlave As String, ByRef Resultado As String, Optional ByVal Condicion As String = "", Optional ByRef strError As String = "", Optional ByVal SobreNombreCampoConsulta As String = "", Optional ByVal bolCualquierEmpresa As Boolean = False) As Boolean
        Dim strSQL As String

        ConexionSQLGESTRANS = New SqlConnection(Me.strCadenaConexionSQLGESTRANS)
        Dim daDataAdapter As SqlDataAdapter
        Dim dsDataSet As DataSet = New DataSet

        strSQL = "SELECT TOP 1 " & CampoConsulta & Chr(13)
        strSQL += " FROM " & Tabla & Chr(13)

        If bolCualquierEmpresa Then
            strSQL += " WHERE EMPR_Codigo = EMPR_Codigo"
        Else
            strSQL += " WHERE EMPR_Codigo = " & CodigoEmpresa & Chr(13)
        End If


        If TipoLlave = CAMPO_NUMERICO Then
            strSQL += " AND " & CampoLlave & " = " & ValorLlave & Chr(13)
        ElseIf TipoLlave = CAMPO_ALFANUMERICO Then
            strSQL += " AND " & CampoLlave & " = '" & ValorLlave & "'" & Chr(13)
        End If
        If Len(Trim(Condicion)) > 0 Then
            strSQL += " AND " & Condicion
        End If

        If SobreNombreCampoConsulta = "" Then
            SobreNombreCampoConsulta = CampoConsulta
        End If

        Try

            daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQLGESTRANS)
            daDataAdapter.Fill(dsDataSet)
            ConexionSQLGESTRANS.Close()

            Retorna_Campo_BD = True
            Resultado = dsDataSet.Tables.Item(PRIMER_TABLA).Rows(PRIMER_REGISTRO).Item(SobreNombreCampoConsulta).ToString()
            strError = ""

        Catch ex As Exception
            Retorna_Campo_BD = False
            Resultado = ""
            strError = ex.Message.ToString()

            If ConexionSQLGESTRANS.State() = ConnectionState.Open Then
                ConexionSQLGESTRANS.Close()
            End If

        End Try

    End Function

    Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim dsDataSet As DataSet = New DataSet

        Try
            ConexionSQLGESTRANS = New SqlConnection(Me.strCadenaConexionSQLGESTRANS)
            Dim daDataAdapter As SqlDataAdapter

            daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQLGESTRANS)
            daDataAdapter.Fill(dsDataSet)

            ConexionSQLGESTRANS.Close()
            ConexionSQLGESTRANS.Dispose()

        Catch ex As Exception
            strError = ex.Message.ToString()

            If ConexionSQLGESTRANS.State() = ConnectionState.Open Then
                ConexionSQLGESTRANS.Close()
            End If

        End Try

        Return dsDataSet

    End Function

    Function Retorna_Dataset_PSL(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim dsDataSet As DataSet = New DataSet

        Try
            ConexionSQLPSL = New SqlConnection(Me.strCadenaConexionSQLPSL)
            Dim daDataAdapter As SqlDataAdapter

            daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQLPSL)
            daDataAdapter.Fill(dsDataSet)

            ConexionSQLPSL.Close()
            ConexionSQLPSL.Dispose()


        Catch ex As Exception
            strError = ex.Message.ToString()

            If ConexionSQLPSL.State() = ConnectionState.Open Then
                ConexionSQLPSL.Close()
            End If

        End Try

        Return dsDataSet

    End Function


#End Region

#Region "Archivos Planos"
    Public Function Guardar_Mensaje_Log(ByVal strMensaje As String)
        Try

            Dim strContenido As String, strFechaHora As String = Date.Now.ToString
            Dim strNombreArchivoLog As String = Me.strRutaArchivoLog & "SW_INTERFAZ_GESTRANS_PSL_" _
            & Format$(Date.Now.Day, "00") & Format$(Date.Now.Month, "00") & Format$(Date.Now.Year, "00") & ".txt"

            If Not File.Exists(strNombreArchivoLog) Then
                File.Create(strNombreArchivoLog).Dispose()
                strContenido = String.Empty
            Else
                strContenido = File.ReadAllText(strNombreArchivoLog)
            End If
            Me.stmStreamW = File.OpenWrite(strNombreArchivoLog)
            Me.stwStreamWriter = New StreamWriter(Me.stmStreamW, System.Text.Encoding.Default)
            Me.stwStreamWriter.Flush()
            Me.stwStreamWriter.Write(strContenido & vbNewLine & strFechaHora & vbNewLine & strMensaje)
            Me.stwStreamWriter.Close()
            Me.stmStreamW.Close()

            Return True
        Catch ex As Exception
            '' FALTA
            ' Escribir en log eventos windows
            Return False
            Exit Try
        End Try
    End Function

#End Region



End Class
