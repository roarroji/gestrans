﻿Imports System.Net
Imports System.Net.Http
Imports System.Web.Http.Filters

''' <summary>
''' Clase <see cref="ExceptionFilter"/>, que permite capturar las excepciones desde la capa de servicios
''' </summary>
Public NotInheritable Class ExceptionFilter
    Inherits ExceptionFilterAttribute

    ''' <summary>
    ''' Sobre escritura del metodo OnException de la clase <see cref="ExceptionFilterAttribute"/> para personalizar el manejo de la exepción
    ''' </summary>
    ''' <param name="context">Contexto actual de la ejecución</param>
    Public Overrides Sub OnException(context As HttpActionExecutedContext)
        Dim mensaje As String
        Dim response As New HttpResponseMessage(HttpStatusCode.InternalServerError)
        mensaje = context.Exception.Message
        mensaje = Replace(mensaje, vbNewLine, " ")

        response.ReasonPhrase = String.Format("Error: {0}", mensaje)
        context.Response = response

    End Sub

End Class
