﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Almacen
Imports Softtools.GesCarga.Negocio.Almacen

<Authorize>
Public Class DetalleDocumentoAlmacenesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleDocumentoAlmacenes) As Respuesta(Of IEnumerable(Of DetalleDocumentoAlmacenes))
        Return New LogicaDetalleDocumentoAlmacenes(CapaPersistenciaDetalleDocumentoAlmacenes).Consultar(filtro)
    End Function

End Class
