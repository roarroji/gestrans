﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Almacen
Imports Softtools.GesCarga.Negocio.Almacen

<Authorize>
Public Class DetalleOrdenCompraController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleOrdenCompra) As Respuesta(Of IEnumerable(Of DetalleOrdenCompra))
        Return New LogicaDetalleOrdenCompra(CapaPersistenciaDetalleOrdenCompra).Consultar(filtro)
    End Function

End Class
