﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Almacen
Imports Softtools.GesCarga.Negocio.Almacen

<Authorize>
Public Class OrdenCompraController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As OrdenCompra) As Respuesta(Of IEnumerable(Of OrdenCompra))
        Return New LogicaOrdenCompra(CapaPersistenciaOrdenCompra, CapaPersistenciaDetalleOrdenCompra).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As OrdenCompra) As Respuesta(Of OrdenCompra)
        Return New LogicaOrdenCompra(CapaPersistenciaOrdenCompra, CapaPersistenciaDetalleOrdenCompra).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As OrdenCompra) As Respuesta(Of Long)
        Return New LogicaOrdenCompra(CapaPersistenciaOrdenCompra, CapaPersistenciaDetalleOrdenCompra).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As OrdenCompra) As Respuesta(Of Boolean)
        Return New LogicaOrdenCompra(CapaPersistenciaOrdenCompra, CapaPersistenciaDetalleOrdenCompra).Anular(entidad)
    End Function
End Class
