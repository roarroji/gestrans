﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Almacen
Imports Softtools.GesCarga.Negocio.Basico.Almacen

''' <summary>
''' Controlador <see cref="GrupoReferenciasController"/>
''' </summary>
<Authorize>
Public Class GrupoReferenciasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As GrupoReferencias) As Respuesta(Of IEnumerable(Of GrupoReferencias))
        Return New LogicaGrupoReferencias(CapaPersistenciaGrupoReferencias).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As GrupoReferencias) As Respuesta(Of GrupoReferencias)
        Return New LogicaGrupoReferencias(CapaPersistenciaGrupoReferencias).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As GrupoReferencias) As Respuesta(Of Long)
        Return New LogicaGrupoReferencias(CapaPersistenciaGrupoReferencias).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As GrupoReferencias) As Respuesta(Of Boolean)
        Return New LogicaGrupoReferencias(CapaPersistenciaGrupoReferencias).Anular(entidad)
    End Function

End Class