﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Almacen
Imports Softtools.GesCarga.Negocio.Basico.Almacen

''' <summary>
''' Controlador <see cref="ReferenciaAlmacenesController"/>
''' </summary>
<Authorize>
Public Class ReferenciaAlmacenesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ReferenciaAlmacenes) As Respuesta(Of IEnumerable(Of ReferenciaAlmacenes))
        Return New LogicaReferenciaAlmacenes(CapaPersistenciaReferenciaAlmacenes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ReferenciaAlmacenes) As Respuesta(Of ReferenciaAlmacenes)
        Return New LogicaReferenciaAlmacenes(CapaPersistenciaReferenciaAlmacenes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ReferenciaAlmacenes) As Respuesta(Of Long)
        Return New LogicaReferenciaAlmacenes(CapaPersistenciaReferenciaAlmacenes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ReferenciaAlmacenes) As Respuesta(Of Boolean)
        Return New LogicaReferenciaAlmacenes(CapaPersistenciaReferenciaAlmacenes).Anular(entidad)
    End Function
End Class