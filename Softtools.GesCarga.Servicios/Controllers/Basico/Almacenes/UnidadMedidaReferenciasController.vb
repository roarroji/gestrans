﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Almacen
Imports Softtools.GesCarga.Negocio.Basico.Almacen

''' <summary>
''' Controlador <see cref="UnidadMedidaReferenciasController"/>
''' </summary>
<Authorize>
Public Class UnidadMedidaReferenciasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As UnidadMedidaReferencias) As Respuesta(Of IEnumerable(Of UnidadMedidaReferencias))
        Return New LogicaUnidadMedidaReferencias(CapaPersistenciaUnidadMedidaReferencias).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As UnidadMedidaReferencias) As Respuesta(Of UnidadMedidaReferencias)
        Return New LogicaUnidadMedidaReferencias(CapaPersistenciaUnidadMedidaReferencias).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As UnidadMedidaReferencias) As Respuesta(Of Long)
        Return New LogicaUnidadMedidaReferencias(CapaPersistenciaUnidadMedidaReferencias).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As UnidadMedidaReferencias) As Respuesta(Of Boolean)
        Return New LogicaUnidadMedidaReferencias(CapaPersistenciaUnidadMedidaReferencias).Anular(entidad)
    End Function


End Class