﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="ColorVehiculosController"/>
''' </summary>
<Authorize>
Public Class ColorVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ColorVehiculos) As Respuesta(Of IEnumerable(Of ColorVehiculos))
        Return New LogicaColorVehiculos(CapaPersistenciaColorVehiculos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ColorVehiculos) As Respuesta(Of ColorVehiculos)
        Return New LogicaColorVehiculos(CapaPersistenciaColorVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ColorVehiculos) As Respuesta(Of Long)
        Return New LogicaColorVehiculos(CapaPersistenciaColorVehiculos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ColorVehiculos) As Respuesta(Of Boolean)
        Return New LogicaColorVehiculos(CapaPersistenciaColorVehiculos).Anular(entidad)
    End Function
End Class
