﻿Imports System.Runtime.Caching
Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="LegalizacionGastosRuta"/>
''' </summary>


Public Class LegalizacionGastosRutaController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As LegalizacionGastosRuta) As Respuesta(Of IEnumerable(Of LegalizacionGastosRuta))
        Return New LogicaLegalizacionGastosRuta(CapaPersistenciaLegalizacionGastosRuta).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As LegalizacionGastosRuta) As Respuesta(Of LegalizacionGastosRuta)
        Return New LogicaLegalizacionGastosRuta(CapaPersistenciaLegalizacionGastosRuta).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As LegalizacionGastosRuta) As Respuesta(Of Long)
        Return New LogicaLegalizacionGastosRuta(CapaPersistenciaLegalizacionGastosRuta).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As LegalizacionGastosRuta) As Respuesta(Of Boolean)
        Return New LogicaLegalizacionGastosRuta(CapaPersistenciaLegalizacionGastosRuta).Anular(entidad)
    End Function


End Class
