﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="MarcaSemirremolquesController"/>
''' </summary>
<Authorize>
Public Class MarcaSemirremolquesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As MarcaSemirremolques) As Respuesta(Of IEnumerable(Of MarcaSemirremolques))
        Return New LogicaMarcaSemirremolques(CapaPersistenciaMarcaSemirremolques).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As MarcaSemirremolques) As Respuesta(Of MarcaSemirremolques)
        Return New LogicaMarcaSemirremolques(CapaPersistenciaMarcaSemirremolques).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As MarcaSemirremolques) As Respuesta(Of Long)
        Return New LogicaMarcaSemirremolques(CapaPersistenciaMarcaSemirremolques).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As MarcaSemirremolques) As Respuesta(Of Boolean)
        Return New LogicaMarcaSemirremolques(CapaPersistenciaMarcaSemirremolques).Anular(entidad)
    End Function
End Class
