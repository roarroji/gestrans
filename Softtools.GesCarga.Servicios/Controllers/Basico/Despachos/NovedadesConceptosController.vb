﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="NovedadesConceptosController"/>
''' </summary>
<Authorize>
Public Class NovedadesConceptosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As NovedadesConceptos) As Respuesta(Of IEnumerable(Of NovedadesConceptos))
        Return New LogicaNovedadesConceptos(CapaPersistenciaNovedadesConceptos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As NovedadesConceptos) As Respuesta(Of NovedadesConceptos)
        Return New LogicaNovedadesConceptos(CapaPersistenciaNovedadesConceptos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As NovedadesConceptos) As Respuesta(Of Long)
        Return New LogicaNovedadesConceptos(CapaPersistenciaNovedadesConceptos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As NovedadesConceptos) As Respuesta(Of Boolean)
        Return New LogicaNovedadesConceptos(CapaPersistenciaNovedadesConceptos).Anular(entidad)
    End Function

End Class
