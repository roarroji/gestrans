﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Negocio.Basico.Despachos

<Authorize>
Public Class PeajesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Peajes) As Respuesta(Of IEnumerable(Of Peajes))
        Return New LogicaPeajes(CapaPersistenciaPeajes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Peajes) As Respuesta(Of Peajes)
        Return New LogicaPeajes(CapaPersistenciaPeajes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Peajes) As Respuesta(Of Long)
        Return New LogicaPeajes(CapaPersistenciaPeajes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Peajes) As Respuesta(Of Boolean)
        Return New LogicaPeajes(CapaPersistenciaPeajes).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("InsertarTarifasPeajes")>
    Public Function InsertarTarifasPeajes(ByVal entidad As Peajes) As Respuesta(Of Boolean)
        Return New LogicaPeajes(CapaPersistenciaPeajes).InsertarTarifasPeajes(entidad)
    End Function

    <ActionName("GenerarPlanitilla")>
    Public Function GenerarPlanitilla(ByVal Filtro As Peajes) As Respuesta(Of Peajes)
        Return New LogicaPeajes(CapaPersistenciaPeajes).GenerarPlanitilla(Filtro)
    End Function


End Class
