﻿Imports System.Runtime.Caching
Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Negocio.Basico.Despachos

<Authorize>
Public Class PeajesRutasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PeajesRutas) As Respuesta(Of IEnumerable(Of PeajesRutas))
        Return New LogicaPeajesRutas(CapaPersistenciaPeajesRutas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PeajesRutas) As Respuesta(Of PeajesRutas)
        Return New LogicaPeajesRutas(CapaPersistenciaPeajesRutas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PeajesRutas) As Respuesta(Of Long)
        Return New LogicaPeajesRutas(CapaPersistenciaPeajesRutas).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As PeajesRutas) As Respuesta(Of Boolean)
        Return New LogicaPeajesRutas(CapaPersistenciaPeajesRutas).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("EliminarPeajesRutas")>
    Public Function EliminarPeajesRutas(ByVal entidad As PeajesRutas) As Respuesta(Of Boolean)
        Return New LogicaPeajesRutas(CapaPersistenciaPeajesRutas).EliminarPeajesRutas(entidad)
    End Function
End Class
