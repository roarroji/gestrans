﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="RendimientoGalonCombustibleController"/>
''' </summary>
<Authorize>
Public Class RendimientoGalonCombustibleController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As RendimientoGalonCombustible) As Respuesta(Of IEnumerable(Of RendimientoGalonCombustible))
        Return New LogicaRendimientoGalonCombustible(CapaPersistenciaRendimientoGalonCombustible).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As RendimientoGalonCombustible) As Respuesta(Of RendimientoGalonCombustible)
        Return New LogicaRendimientoGalonCombustible(CapaPersistenciaRendimientoGalonCombustible).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As RendimientoGalonCombustible) As Respuesta(Of Long)
        Return New LogicaRendimientoGalonCombustible(CapaPersistenciaRendimientoGalonCombustible).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Actualizar")>
    Public Function Actualizar(ByVal entidad As RendimientoGalonCombustible) As Respuesta(Of Long)
        Return New LogicaRendimientoGalonCombustible(CapaPersistenciaRendimientoGalonCombustible).Actualizar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As RendimientoGalonCombustible) As Respuesta(Of Boolean)
        Return New LogicaRendimientoGalonCombustible(CapaPersistenciaRendimientoGalonCombustible).Anular(entidad)
    End Function
End Class
