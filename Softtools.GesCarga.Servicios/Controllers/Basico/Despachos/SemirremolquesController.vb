﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="SemirremolquesController"/>
''' </summary>
<Authorize>
Public Class SemirremolquesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Semirremolques) As Respuesta(Of IEnumerable(Of Semirremolques))
        Return New LogicaSemirremolques(CapaPersistenciaSemirremolques).Consultar(filtro)
    End Function
    <HttpPost>
    <ActionName("ConsultarDocumentos")>
    Public Function ConsultarDocumentos(ByVal filtro As SemirremolquesDocumentos) As Respuesta(Of IEnumerable(Of SemirremolquesDocumentos))
        Return New LogicaSemirremolques(CapaPersistenciaSemirremolques).ConsultarDocumentos(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Semirremolques) As Respuesta(Of Semirremolques)
        Return New LogicaSemirremolques(CapaPersistenciaSemirremolques).Obtener(filtro)
    End Function
    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Semirremolques) As Respuesta(Of Long)
        Return New LogicaSemirremolques(CapaPersistenciaSemirremolques).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Semirremolques) As Respuesta(Of Boolean)
        Return New LogicaSemirremolques(CapaPersistenciaSemirremolques).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("ObtenerSemirremolqueEstudioSeguridad")>
    Public Function ObtenerSemirremolqueEstudioSeguridad(ByVal filtro As EstudioSeguridad) As Respuesta(Of EstudioSeguridad)
        Return New LogicaSemirremolques(CapaPersistenciaSemirremolques).ObtenerSemirremolqueEstudioSeguridad(filtro)
    End Function
    <ActionName("GenerarPlanitilla")>
    Public Function GenerarPlanitilla(ByVal Filtro As Semirremolques) As Respuesta(Of Semirremolques)
        Return New LogicaSemirremolques(CapaPersistenciaSemirremolques).GenerarPlanitilla(Filtro)
    End Function

    <HttpPost>
    <ActionName("GuardarNovedad")>
    Public Function GuardarNovedad(ByVal entidad As Semirremolques) As Respuesta(Of Long)
        Return New LogicaSemirremolques(CapaPersistenciaSemirremolques).GuardarNovedad(entidad)
    End Function
End Class
