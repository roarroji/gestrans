﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Negocio.Basico.Despachos


''' <summary>
''' Controlador <see cref="SitiosCargueDescargueController"/>
''' </summary>
<Authorize>
Public Class SitiosCargueDescargueController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As SitiosCargueDescargue) As Respuesta(Of IEnumerable(Of SitiosCargueDescargue))
        Return New LogicaSitiosCargueDescargue(CapaPersistenciaSitiosCargueDescargue).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarSitiosRutas")>
    Public Function ConsultarSitiosRutas(ByVal filtro As Rutas) As Respuesta(Of IEnumerable(Of SitiosCargueDescargueRutas))
        Return New LogicaSitiosCargueDescargue(CapaPersistenciaSitiosCargueDescargue).ConsultarSitiosRutas(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As SitiosCargueDescargue) As Respuesta(Of SitiosCargueDescargue)
        Return New LogicaSitiosCargueDescargue(CapaPersistenciaSitiosCargueDescargue).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As SitiosCargueDescargue) As Respuesta(Of Long)
        Return New LogicaSitiosCargueDescargue(CapaPersistenciaSitiosCargueDescargue).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("GuardarSitiosRuta")>
    Public Function GuardarSitiosRuta(ByVal entidad As IEnumerable(Of SitiosCargueDescargueRutas)) As Respuesta(Of Long)
        Return New LogicaSitiosCargueDescargue(CapaPersistenciaSitiosCargueDescargue).GuardarSitiosRuta(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As SitiosCargueDescargue) As Respuesta(Of Boolean)
        Return New LogicaSitiosCargueDescargue(CapaPersistenciaSitiosCargueDescargue).Anular(entidad)
    End Function
    <ActionName("GenerarPlanitilla")>
    Public Function GenerarPlanitilla(ByVal Filtro As SitiosCargueDescargue) As Respuesta(Of SitiosCargueDescargue)
        Return New LogicaSitiosCargueDescargue(CapaPersistenciaSitiosCargueDescargue).GenerarPlanitilla(Filtro)
    End Function
End Class
