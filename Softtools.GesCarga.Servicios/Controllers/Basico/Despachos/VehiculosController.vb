﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Negocio.Basico.Despachos
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Entidades.Basico.General

''' <summary>
''' Controlador <see cref="VehiculosController"/>
''' </summary>
<Authorize>
Public Class VehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Vehiculos) As Respuesta(Of IEnumerable(Of Vehiculos))
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).Consultar(filtro)
    End Function
    <HttpPost>
    <ActionName("ConsultarPropietarios")>
    Public Function ConsultarPropietarios(ByVal filtro As Vehiculos) As Respuesta(Of IEnumerable(Of PropietarioVehiculo))
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).ConsultarPropietarios(filtro)
    End Function
    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Vehiculos) As Respuesta(Of Vehiculos)
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Vehiculos) As Respuesta(Of Long)
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("GuardarPropietarios")>
    Public Function GuardarPropietarios(ByVal entidad As Vehiculos) As Respuesta(Of Long)
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).GuardarPropietarios(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Vehiculos) As Respuesta(Of Boolean)
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("ObtenerVehiculoEstudioSeguridad")>
    Public Function ObtenerVehiculoEstudioSeguridad(ByVal filtro As EstudioSeguridad) As Respuesta(Of EstudioSeguridad)
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).ObtenerVehiculoEstudioSeguridad(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarDocumentosProximosVencer")>
    Public Function ConsultarDocumentosProximosVencer(ByVal filtro As VehiculosDocumentos) As Respuesta(Of IEnumerable(Of VehiculosDocumentos))
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).ConsultarDocumentosProximosVencer(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarDocumentos")>
    Public Function ConsultarDocumentos(ByVal filtro As VehiculosDocumentos) As Respuesta(Of IEnumerable(Of VehiculosDocumentos))
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).ConsultarDocumentos(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarEstadoListaNegra")>
    Public Function ConsultarEstadoListaNegra(ByVal entidad As VehiculosDocumentos) As Respuesta(Of Boolean)
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).ConsultarEstadoListaNegra(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarListaNegra")>
    Public Function ConsultarVehiculosListaNegra(ByVal entidad As Vehiculos) As Respuesta(Of IEnumerable(Of Vehiculos))
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).ConsultarVehiculoListaNegra(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarDocumentosSoatRTM")>
    Public Function ConsultarDocumentosSoatRTM(ByVal entidad As VehiculosDocumentos) As Respuesta(Of Boolean)
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).ConsultarDocumentosSoatRTM(entidad)
    End Function
    <ActionName("GenerarPlanitilla")>
    Public Function GenerarPlanitilla(ByVal Filtro As Vehiculos) As Respuesta(Of Vehiculos)
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).GenerarPlanitilla(Filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarDocumentosPendientes")>
    Public Function ConsultarDocumentosListaDocumentosPendientes(ByVal entidad As VehiculosDocumentos) As Respuesta(Of IEnumerable(Of VehiculosDocumentos))
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).ConsultarDocumentosListaDocumentosPendientes(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarAuditoria")>
    Public Function ConsultarAuditoria(ByVal entidad As Vehiculos) As Respuesta(Of IEnumerable(Of Vehiculos))
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).ConsultarAuditoria(entidad)
    End Function

    <HttpPost>
    <ActionName("GuardarNovedad")>
    Public Function GuardarNovedad(ByVal entidad As Vehiculos) As Respuesta(Of Long)
        Return New LogicaVehiculos(CapaPersistenciaVehiculos).GuardarNovedad(entidad)
    End Function

End Class
