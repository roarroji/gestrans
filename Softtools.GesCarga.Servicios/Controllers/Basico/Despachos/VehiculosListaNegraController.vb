﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Negocio.Basico.Despachos


<Authorize>
Public Class VehiculosListaNegraController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Vehiculos) As Respuesta(Of IEnumerable(Of Vehiculos))
        Return New LogicaVehiculosListaNegra(CapaPersistenciaVehiculosListaNegra).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Vehiculos) As Respuesta(Of Vehiculos)
        Return New LogicaVehiculosListaNegra(CapaPersistenciaVehiculosListaNegra).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Vehiculos) As Respuesta(Of Long)
        Return New LogicaVehiculosListaNegra(CapaPersistenciaVehiculosListaNegra).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Vehiculos) As Respuesta(Of Boolean)
        Return New LogicaVehiculosListaNegra(CapaPersistenciaVehiculosListaNegra).Anular(entidad)
    End Function

End Class
