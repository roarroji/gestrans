﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Facturacion
Imports Softtools.GesCarga.Negocio.Basico.Facturacion

<Authorize>
Public Class ConceptoNotasFacturasController
    Inherits Base
    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ConceptoNotasFacturas) As Respuesta(Of IEnumerable(Of ConceptoNotasFacturas))
        Return New LogicaConceptoNotasFacturas(CapaPersistenciaConceptosNotasFacturas).Consultar(filtro)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ConceptoNotasFacturas) As Respuesta(Of Boolean)
        Return New LogicaConceptoNotasFacturas(CapaPersistenciaConceptosNotasFacturas).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ConceptoNotasFacturas) As Respuesta(Of Long)
        Return New LogicaConceptoNotasFacturas(CapaPersistenciaConceptosNotasFacturas).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ConceptoNotasFacturas) As Respuesta(Of ConceptoNotasFacturas)
        Return New LogicaConceptoNotasFacturas(CapaPersistenciaConceptosNotasFacturas).Obtener(filtro)
    End Function
End Class
