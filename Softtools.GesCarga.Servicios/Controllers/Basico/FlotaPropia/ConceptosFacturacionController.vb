﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.FlotaPropia
Imports Softtools.GesCarga.Negocio.Basico.FlotaPropia

''' <summary>
''' Controlador <see cref="ConceptosFacturacionController"/>
''' </summary>
<Authorize>
Public Class ConceptoGastosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ConceptoGastos) As Respuesta(Of IEnumerable(Of ConceptoGastos))
        Return New LogicaConceptoGastos(CapaPersistenciaConceptoGastos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ConceptoGastos) As Respuesta(Of ConceptoGastos)
        Return New LogicaConceptoGastos(CapaPersistenciaConceptoGastos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ConceptoGastos) As Respuesta(Of Long)
        Return New LogicaConceptoGastos(CapaPersistenciaConceptoGastos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ConceptoGastos) As Respuesta(Of Boolean)
        Return New LogicaConceptoGastos(CapaPersistenciaConceptoGastos).Anular(entidad)
    End Function

End Class
