﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="CatalogosController"/>
''' </summary>
<Authorize>
Public Class CatalogosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Catalogos) As Respuesta(Of IEnumerable(Of Catalogos))
        Return New LogicaCatalogos(CapaPersistenciaCatalogos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Catalogos) As Respuesta(Of Catalogos)
        Return New LogicaCatalogos(CapaPersistenciaCatalogos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Catalogos) As Respuesta(Of Long)
        Return New LogicaCatalogos(CapaPersistenciaCatalogos).Guardar(entidad)
    End Function

End Class
