﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="DepartamentosController"/>
''' </summary>
<Authorize>
Public Class DepartamentosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Departamentos) As Respuesta(Of IEnumerable(Of Departamentos))
        Return New LogicaDepartamentos(CapaPersistenciaDepartamentos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Departamentos) As Respuesta(Of Departamentos)
        Return New LogicaDepartamentos(CapaPersistenciaDepartamentos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Departamentos) As Respuesta(Of Long)
        Return New LogicaDepartamentos(CapaPersistenciaDepartamentos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Departamentos) As Respuesta(Of Boolean)
        Return New LogicaDepartamentos(CapaPersistenciaDepartamentos).Anular(entidad)
    End Function

End Class
