﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="FormularioInspeccionesController"/>
''' </summary>
<Authorize>
Public Class FormularioInspeccionesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As FormularioInspecciones) As Respuesta(Of IEnumerable(Of FormularioInspecciones))
        Return New LogicaFormularioInspecciones(CapaPersistenciaFormularioInspecciones).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As FormularioInspecciones) As Respuesta(Of FormularioInspecciones)
        Return New LogicaFormularioInspecciones(CapaPersistenciaFormularioInspecciones).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As FormularioInspecciones) As Respuesta(Of Long)
        Return New LogicaFormularioInspecciones(CapaPersistenciaFormularioInspecciones).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As FormularioInspecciones) As Respuesta(Of Boolean)
        Return New LogicaFormularioInspecciones(CapaPersistenciaFormularioInspecciones).Anular(entidad)
    End Function
End Class
