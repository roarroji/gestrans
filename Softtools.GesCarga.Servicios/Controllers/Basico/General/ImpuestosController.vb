﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="ImpuestosController"/>
''' </summary>
<Authorize>
Public Class ImpuestosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Impuestos) As Respuesta(Of IEnumerable(Of Impuestos))
        Return New LogicaImpuestos(CapaPersistenciaImpuestos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Impuestos) As Respuesta(Of Impuestos)
        Return New LogicaImpuestos(CapaPersistenciaImpuestos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Impuestos) As Respuesta(Of Long)
        Return New LogicaImpuestos(CapaPersistenciaImpuestos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Impuestos) As Respuesta(Of Boolean)
        Return New LogicaImpuestos(CapaPersistenciaImpuestos).Anular(entidad)
    End Function

End Class
