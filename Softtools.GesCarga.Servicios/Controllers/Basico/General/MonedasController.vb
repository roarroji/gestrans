﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Negocio.Basico.General


''' <summary>
''' Controlador <see cref="MonedasController"/>
''' </summary>
<Authorize>
Public Class MonedasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Monedas) As Respuesta(Of IEnumerable(Of Monedas))
        Return New LogicaMonedas(CapaPersistenciaMonedas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Monedas) As Respuesta(Of Monedas)
        Return New LogicaMonedas(CapaPersistenciaMonedas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Monedas) As Respuesta(Of Long)
        Return New LogicaMonedas(CapaPersistenciaMonedas).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Monedas) As Respuesta(Of Boolean)
        Return New LogicaMonedas(CapaPersistenciaMonedas).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("MonedaLocal")>
    Public Function MonedaLocal(ByVal entidad As Monedas) As Respuesta(Of Boolean)
        Return New LogicaMonedas(CapaPersistenciaMonedas).MonedaLocals(entidad)
    End Function

End Class

