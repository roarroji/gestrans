﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Negocio.Basico.General



''' <summary>
''' Controlador <see cref="PaisesController"/>
''' </summary>
<Authorize>
Public Class PaisesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Paises) As Respuesta(Of IEnumerable(Of Paises))
        Return New LogicaPaises(CapaPersistenciaPaises).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Paises) As Respuesta(Of Paises)
        Return New LogicaPaises(CapaPersistenciaPaises).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Paises) As Respuesta(Of Long)
        Return New LogicaPaises(CapaPersistenciaPaises).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Paises) As Respuesta(Of Boolean)
        Return New LogicaPaises(CapaPersistenciaPaises).Anular(entidad)
    End Function

End Class
