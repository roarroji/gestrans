﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Operacion
Imports Softtools.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="SitiosTerceroClienteController"/>
''' </summary>
<Authorize>
Public Class SitiosTerceroClienteController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As SitiosTerceroCliente) As Respuesta(Of IEnumerable(Of SitiosTerceroCliente))
        Return New LogicaSitiosTerceroCliente(CapaPersistenciaSitiosTerceroCliente).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarOrdenServicio")>
    Public Function ConsultarOrdenServicio(ByVal filtro As SitiosTerceroCliente) As Respuesta(Of IEnumerable(Of SitiosTerceroCliente))
        Return New LogicaSitiosTerceroCliente(CapaPersistenciaSitiosTerceroCliente).ConsultarOrdenServicio(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As SitiosTerceroCliente) As Respuesta(Of SitiosTerceroCliente)
        Return New LogicaSitiosTerceroCliente(CapaPersistenciaSitiosTerceroCliente).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As SitiosTerceroCliente) As Respuesta(Of Long)
        Return New LogicaSitiosTerceroCliente(CapaPersistenciaSitiosTerceroCliente).Guardar(entidad)
    End Function

End Class
