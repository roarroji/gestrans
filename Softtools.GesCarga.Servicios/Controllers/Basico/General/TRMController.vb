﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Negocio.Basico.General


''' <summary>
''' Controlador <see cref="TRMController"/>
''' </summary>
<Authorize>
Public Class TRMController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TRM) As Respuesta(Of IEnumerable(Of TRM))
        Return New LogicaTRM(CapaPersistenciaTRM).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TRM) As Respuesta(Of TRM)
        Return New LogicaTRM(CapaPersistenciaTRM).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TRM) As Respuesta(Of Long)
        Return New LogicaTRM(CapaPersistenciaTRM).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As TRM) As Respuesta(Of Boolean)
        Return New LogicaTRM(CapaPersistenciaTRM).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("Modificar")>
    Public Function Modificar(ByVal entidad As TRM) As Respuesta(Of Long)
        Return New LogicaTRM(CapaPersistenciaTRM).Modificar(entidad)
    End Function

End Class

