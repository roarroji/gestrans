﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="TercerosController"/>
''' </summary>
<Authorize>
Public Class TercerosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Terceros) As Respuesta(Of IEnumerable(Of Terceros))
        Return New LogicaTerceros(CapaPersistenciaTerceros).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Terceros) As Respuesta(Of Terceros)
        Return New LogicaTerceros(CapaPersistenciaTerceros).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Terceros) As Respuesta(Of Long)
        Return New LogicaTerceros(CapaPersistenciaTerceros).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("InsertarDirecciones")>
    Public Function InsertarDirecciones(ByVal entidad As TerceroDirecciones) As Respuesta(Of Boolean)
        Return New LogicaTerceros(CapaPersistenciaTerceros).InsertarDirecciones(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarTerceroGeneral")>
    Public Function Consultar(ByVal filtro As Tercero) As Respuesta(Of IEnumerable(Of Tercero))
        Return New LogicaTerceros(CapaPersistenciaTerceros).ConsultarTerceroGeneral(filtro)
    End Function
    <HttpPost>
    <ActionName("ObtenerTerceroEstudioSeguridad")>
    Public Function ObtenerTerceroEstudioSeguridad(ByVal filtro As EstudioSeguridad) As Respuesta(Of EstudioSeguridad)
        Return New LogicaTerceros(CapaPersistenciaTerceros).ObtenerTerceroEstudioSeguridad(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarDocumentosProximosVencer")>
    Public Function ConsultarDocumentosProximosVencer(ByVal filtro As TercerosDocumentos) As Respuesta(Of IEnumerable(Of TercerosDocumentos))
        Return New LogicaTerceros(CapaPersistenciaTerceros).ConsultarDocumentosProximosVencer(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarDocumentos")>
    Public Function ConsultarDocumentos(ByVal filtro As TercerosDocumentos) As Respuesta(Of IEnumerable(Of TercerosDocumentos))
        Return New LogicaTerceros(CapaPersistenciaTerceros).ConsultarDocumentos(filtro)
    End Function

    <ActionName("GenerarPlanitilla")>
    Public Function GenerarPlanitilla(ByVal Filtro As Terceros) As Respuesta(Of Terceros)
        Return New LogicaTerceros(CapaPersistenciaTerceros).GenerarPlanitilla(Filtro)
    End Function
    <HttpPost>
    <ActionName("ConsultarAuditoria")>
    Public Function ConsultarAuditoria(ByVal filtro As Tercero) As Respuesta(Of IEnumerable(Of Tercero))
        Return New LogicaTerceros(CapaPersistenciaTerceros).ConsultarAuditoria(filtro)
    End Function

    <HttpPost>
    <ActionName("GuardarNovedad")>
    Public Function GuardarNovedad(ByVal entidad As Terceros) As Respuesta(Of Long)
        Return New LogicaTerceros(CapaPersistenciaTerceros).GuardarNovedad(entidad)
    End Function
End Class
