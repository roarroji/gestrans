﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="UnidadEmpaqueController"/>
''' </summary>
<Authorize>
Public Class UnidadEmpaqueController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As UnidadEmpaque) As Respuesta(Of IEnumerable(Of UnidadEmpaque))
        Return New LogicaUnidadEmpaque(CapaPersistenciaUnidadEmpaque).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As UnidadEmpaque) As Respuesta(Of UnidadEmpaque)
        Return New LogicaUnidadEmpaque(CapaPersistenciaUnidadEmpaque).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As UnidadEmpaque) As Respuesta(Of Long)
        Return New LogicaUnidadEmpaque(CapaPersistenciaUnidadEmpaque).Guardar(entidad)
    End Function
End Class
