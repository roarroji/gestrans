﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Negocio.Basico.General

<Authorize>
Public Class UnidadesEmpaqueProductosTransportadosController
    Inherits Base
    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As UnidadEmpaqueProductosTransportados) As Respuesta(Of IEnumerable(Of UnidadEmpaqueProductosTransportados))
        Return New LogicaUnidadEmpaqueProductosTransportados(CapaPersistenciaUnidadEmpaqueProductos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As UnidadEmpaqueProductosTransportados) As Respuesta(Of UnidadEmpaqueProductosTransportados)
        Return New LogicaUnidadEmpaqueProductosTransportados(CapaPersistenciaUnidadEmpaqueProductos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As UnidadEmpaqueProductosTransportados) As Respuesta(Of Long)
        Return New LogicaUnidadEmpaqueProductosTransportados(CapaPersistenciaUnidadEmpaqueProductos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As UnidadEmpaqueProductosTransportados) As Respuesta(Of Boolean)
        Return New LogicaUnidadEmpaqueProductosTransportados(CapaPersistenciaUnidadEmpaqueProductos).Anular(entidad)
    End Function

End Class