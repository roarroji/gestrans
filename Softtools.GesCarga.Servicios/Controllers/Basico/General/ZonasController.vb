﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Negocio.Basico.General

''' <summary>
''' Controlador <see cref="ZonasController"/>
''' </summary>
<Authorize>
Public Class ZonasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Zonas) As Respuesta(Of IEnumerable(Of Zonas))
        Return New LogicaZonas(CapaPersistenciaZonas).Consultar(filtro)
    End Function
    <HttpPost>
    <ActionName("ConsultarZonificacionGuias")>
    Public Function ConsultarZonificacionGuias(ByVal filtro As Zonas) As Respuesta(Of IEnumerable(Of RemesaPaqueteria))
        Return New LogicaZonas(CapaPersistenciaZonas).ConsultarZonificacionGuias(filtro)
    End Function

    <HttpPost>
    <ActionName("InsertarZonificacionGuias")>
    Public Function InsertarZonificacionGuias(ByVal entidad As Zonas) As Respuesta(Of Long)
        Return New LogicaZonas(CapaPersistenciaZonas).InsertarZonificacionGuias(entidad)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Zonas) As Respuesta(Of Zonas)
        Return New LogicaZonas(CapaPersistenciaZonas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Zonas) As Respuesta(Of Long)
        Return New LogicaZonas(CapaPersistenciaZonas).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Zonas) As Respuesta(Of Boolean)
        Return New LogicaZonas(CapaPersistenciaZonas).Anular(entidad)
    End Function

End Class
