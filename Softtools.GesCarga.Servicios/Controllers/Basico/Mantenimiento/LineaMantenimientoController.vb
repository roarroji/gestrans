﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.Negocio.Mantenimiento

<Authorize>
Public Class LineaMantenimientoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As LineaMantenimiento) As Respuesta(Of IEnumerable(Of LineaMantenimiento))
        Return New LogicaLineaMantenimiento(CapaPersistenciaLineaMantenimiento).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As LineaMantenimiento) As Respuesta(Of LineaMantenimiento)
        Return New LogicaLineaMantenimiento(CapaPersistenciaLineaMantenimiento).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As LineaMantenimiento) As Respuesta(Of Long)
        Return New LogicaLineaMantenimiento(CapaPersistenciaLineaMantenimiento).Guardar(entidad)
    End Function

End Class
