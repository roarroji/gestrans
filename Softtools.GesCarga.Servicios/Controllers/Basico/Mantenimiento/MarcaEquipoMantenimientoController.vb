﻿Imports System.Web.Http
Imports Softtools.GesCarga.Negocio.Mantenimiento
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Mantenimiento

<Authorize>
Public Class MarcaEquipoMantenimientoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As MarcaEquipoMantenimiento) As Respuesta(Of IEnumerable(Of MarcaEquipoMantenimiento))
        Return New LogicaMarcaEquipoMantenimiento(CapaPersistenciaMarcaEquipoMantenimiento).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As MarcaEquipoMantenimiento) As Respuesta(Of MarcaEquipoMantenimiento)
        Return New LogicaMarcaEquipoMantenimiento(CapaPersistenciaMarcaEquipoMantenimiento).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As MarcaEquipoMantenimiento) As Respuesta(Of Long)
        Return New LogicaMarcaEquipoMantenimiento(CapaPersistenciaMarcaEquipoMantenimiento).Guardar(entidad)
    End Function
End Class