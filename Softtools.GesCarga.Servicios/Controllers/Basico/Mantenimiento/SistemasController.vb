﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico
Imports Softtools.GesCarga.Negocio.Basico

<Authorize>
Public Class SistemasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Sistemas) As Respuesta(Of IEnumerable(Of Sistemas))
        Return New LogicaSistemas(CapaPersistenciaSistemas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Sistemas) As Respuesta(Of Sistemas)
        Return New LogicaSistemas(CapaPersistenciaSistemas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Sistemas) As Respuesta(Of Long)
        Return New LogicaSistemas(CapaPersistenciaSistemas).Guardar(entidad)
    End Function

End Class
