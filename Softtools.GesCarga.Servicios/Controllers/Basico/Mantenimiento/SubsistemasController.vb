﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico
Imports Softtools.GesCarga.Negocio.Basico

<Authorize>
Public Class SubSistemasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As SubSistemas) As Respuesta(Of IEnumerable(Of SubSistemas))
        Return New LogicaSubsistemas(CapaPersistenciaSubSistemas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As SubSistemas) As Respuesta(Of SubSistemas)
        Return New LogicaSubsistemas(CapaPersistenciaSubSistemas).Obtener(filtro)
    End Function
    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As SubSistemas) As Respuesta(Of Long)
        Return New LogicaSubsistemas(CapaPersistenciaSubSistemas).Guardar(entidad)
    End Function

End Class
