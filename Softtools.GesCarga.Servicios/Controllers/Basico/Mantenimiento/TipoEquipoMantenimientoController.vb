﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.Negocio.Mantenimiento

<Authorize>
Public Class TipoEquipoMantenimientoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TipoEquipoMantenimiento) As Respuesta(Of IEnumerable(Of TipoEquipoMantenimiento))
        Return New LogicaTipoEquipoMantenimiento(CapaPersistenciaTipoEquipoMantenimiento).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TipoEquipoMantenimiento) As Respuesta(Of TipoEquipoMantenimiento)
        Return New LogicaTipoEquipoMantenimiento(CapaPersistenciaTipoEquipoMantenimiento).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TipoEquipoMantenimiento) As Respuesta(Of Long)
        Return New LogicaTipoEquipoMantenimiento(CapaPersistenciaTipoEquipoMantenimiento).Guardar(entidad)
    End Function
End Class