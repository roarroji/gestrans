﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico
Imports Softtools.GesCarga.Negocio.Basico

<Authorize>
Public Class TipoSubsistemasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TipoSubsistemas) As Respuesta(Of IEnumerable(Of TipoSubsistemas))
        Return New LogicaTipoSubsistemas(CapaPersistenciaTipoSubsistemas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TipoSubsistemas) As Respuesta(Of TipoSubsistemas)
        Return New LogicaTipoSubsistemas(CapaPersistenciaTipoSubsistemas).Obtener(filtro)
    End Function
    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TipoSubsistemas) As Respuesta(Of Long)
        Return New LogicaTipoSubsistemas(CapaPersistenciaTipoSubsistemas).Guardar(entidad)
    End Function

End Class
