﻿Imports System.Web.Http
Imports Softtools.GesCarga.Negocio.Mantenimiento
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Mantenimiento


<Authorize>
Public Class UnidadReferenciaMantenimientoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As UnidadReferenciaMantenimiento) As Respuesta(Of IEnumerable(Of UnidadReferenciaMantenimiento))
        Return New LogicaUnidadReferenciaMantenimiento(CapaPersistenciaUnidadReferenciaMantenimiento).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As UnidadReferenciaMantenimiento) As Respuesta(Of UnidadReferenciaMantenimiento)
        Return New LogicaUnidadReferenciaMantenimiento(CapaPersistenciaUnidadReferenciaMantenimiento).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As UnidadReferenciaMantenimiento) As Respuesta(Of Long)
        Return New LogicaUnidadReferenciaMantenimiento(CapaPersistenciaUnidadReferenciaMantenimiento).Guardar(entidad)
    End Function
End Class