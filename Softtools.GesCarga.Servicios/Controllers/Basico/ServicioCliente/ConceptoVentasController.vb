﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.ServicioCliente
Imports Softtools.GesCarga.Negocio.Basico.ServicioCliente

''' <summary>
''' Controlador <see cref="ConceptoVentasController"/>
''' </summary>
<Authorize>
Public Class ConceptoVentasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ConceptoVentas) As Respuesta(Of IEnumerable(Of ConceptoVentas))
        Return New LogicaConceptoVentas(CapaPersistenciaConceptoVentas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ConceptoVentas) As Respuesta(Of ConceptoVentas)
        Return New LogicaConceptoVentas(CapaPersistenciaConceptoVentas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ConceptoVentas) As Respuesta(Of Long)
        Return New LogicaConceptoVentas(CapaPersistenciaConceptoVentas).Guardar(entidad)
    End Function
End Class
