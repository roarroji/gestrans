﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.ServicioCliente
Imports Softtools.GesCarga.Negocio.Basico.ServicioCliente

''' <summary>
''' Controlador <see cref="TipoLineaNegocioCargaController"/>
''' </summary>
<Authorize>
Public Class TipoLineaNegocioCargaController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TipoLineaNegocioCarga) As Respuesta(Of IEnumerable(Of TipoLineaNegocioCarga))
        Return New LogicaTipoLineaNegocioCarga(CapaPersistenciaTipoLineaNegocioCarga).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TipoLineaNegocioCarga) As Respuesta(Of TipoLineaNegocioCarga)
        Return New LogicaTipoLineaNegocioCarga(CapaPersistenciaTipoLineaNegocioCarga).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TipoLineaNegocioCarga) As Respuesta(Of Long)
        Return New LogicaTipoLineaNegocioCarga(CapaPersistenciaTipoLineaNegocioCarga).Guardar(entidad)
    End Function
End Class
