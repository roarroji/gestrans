﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Negocio.Basico.Tesoreria

''' <summary>
''' Controlador <see cref="BancosController"/>
''' </summary>
<Authorize>
Public Class BancosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Bancos) As Respuesta(Of IEnumerable(Of Bancos))
        Return New LogicaBancos(CapaPersistenciaBancos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Bancos) As Respuesta(Of Bancos)
        Return New LogicaBancos(CapaPersistenciaBancos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Bancos) As Respuesta(Of Long)
        Return New LogicaBancos(CapaPersistenciaBancos).Guardar(entidad)
    End Function

End Class
