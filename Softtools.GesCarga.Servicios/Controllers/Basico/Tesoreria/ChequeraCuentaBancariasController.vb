﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Negocio.Basico.Tesoreria

''' <summary>
''' Controlador <see cref="ChequeraCuentaBancariasController"/>
''' </summary>
<Authorize>
Public Class ChequeraCuentaBancariasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ChequeraCuentaBancarias) As Respuesta(Of IEnumerable(Of ChequeraCuentaBancarias))
        Return New LogicaChequeraCuentaBancarias(CapaPersistenciaChequeraCuentaBancarias).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ChequeraCuentaBancarias) As Respuesta(Of ChequeraCuentaBancarias)
        Return New LogicaChequeraCuentaBancarias(CapaPersistenciaChequeraCuentaBancarias).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ChequeraCuentaBancarias) As Respuesta(Of Long)
        Return New LogicaChequeraCuentaBancarias(CapaPersistenciaChequeraCuentaBancarias).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ChequeraCuentaBancarias) As Respuesta(Of Boolean)
        Return New LogicaChequeraCuentaBancarias(CapaPersistenciaChequeraCuentaBancarias).Anular(entidad)
    End Function
End Class
