﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Negocio.Basico

<Authorize>
Public Class CuentaBancariaOficinasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As CuentaBancariaOficinas) As Respuesta(Of IEnumerable(Of CuentaBancariaOficinas))
        Return New LogicaCuentaBancariaOficinas(CapaPersistenciaCuentasBancariaOficinas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As CuentaBancariaOficinas) As Respuesta(Of CuentaBancariaOficinas)
        Return New LogicaCuentaBancariaOficinas(CapaPersistenciaCuentasBancariaOficinas).Obtener(filtro)
    End Function

End Class
