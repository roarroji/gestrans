﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Negocio.Basico.Tesoreria

''' <summary>
''' Controlador <see cref="DetalleConceptoContablesController"/>
''' </summary>
<Authorize>
Public Class DetalleConceptoContablesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleConceptoContables) As Respuesta(Of IEnumerable(Of DetalleConceptoContables))
        Return New LogicaDetalleConceptoContables(CapaPersistenciaDetalleConceptoContables).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleConceptoContables) As Respuesta(Of DetalleConceptoContables)
        Return New LogicaDetalleConceptoContables(CapaPersistenciaDetalleConceptoContables).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleConceptoContables) As Respuesta(Of Long)
        Return New LogicaDetalleConceptoContables(CapaPersistenciaDetalleConceptoContables).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleConceptoContables) As Respuesta(Of Boolean)
        Return New LogicaDetalleConceptoContables(CapaPersistenciaDetalleConceptoContables).Anular(entidad)
    End Function
End Class
