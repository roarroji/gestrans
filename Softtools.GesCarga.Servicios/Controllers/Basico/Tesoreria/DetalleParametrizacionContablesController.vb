﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Tesoreria
Imports Softtools.GesCarga.Negocio.Basico.Tesoreria

''' <summary>
''' Controlador <see cref="DetalleParametrizacionContablesController"/>
''' </summary>
<Authorize>
Public Class DetalleParametrizacionContablesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleParametrizacionContables) As Respuesta(Of IEnumerable(Of DetalleParametrizacionContables))
        Return New LogicaDetalleParametrizacionContables(CapaPersistenciaDetalleParametrizacionContables).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleParametrizacionContables) As Respuesta(Of DetalleParametrizacionContables)
        Return New LogicaDetalleParametrizacionContables(CapaPersistenciaDetalleParametrizacionContables).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleParametrizacionContables) As Respuesta(Of Long)
        Return New LogicaDetalleParametrizacionContables(CapaPersistenciaDetalleParametrizacionContables).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleParametrizacionContables) As Respuesta(Of Boolean)
        Return New LogicaDetalleParametrizacionContables(CapaPersistenciaDetalleParametrizacionContables).Anular(entidad)
    End Function
End Class
