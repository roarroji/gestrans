﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Negocio
Public Class CARGAAPPController
    Inherits Base



    <HttpPost>
    <ActionName("ConsultarTerceros")>
    Public Function ConsultarTerceros(ByVal filtro As TercerosSincronizacionCARGAAPP) As TercerosSincronizacionCARGAAPP
        Return New LogicaCARGAAPP(CapaPersistenciaTercerosSincronizacionCARGAAPP).ConsultarTerceros(filtro)
    End Function

    <HttpPost>
    <ActionName("ModificarTerceros")>
    Public Function ModificarTerceros(ByVal filtro As TercerosModificacionCARGAAPP)
        Dim logica As LogicaCARGAAPP

        logica = New LogicaCARGAAPP(CapaPersistenciaTercerosSincronizacionCARGAAPP).ModificarTerceros(filtro)

    End Function

    <HttpPost>
    <ActionName("ModificarVehiculos")>
    Public Function ModificarVehiculos(ByVal filtro As VehiculosModificacionCARGAAPP)
        Dim logica As LogicaCARGAAPP

        logica = New LogicaCARGAAPP(CapaPersistenciaTercerosSincronizacionCARGAAPP).ModificarVehiculos(filtro)

    End Function
End Class
