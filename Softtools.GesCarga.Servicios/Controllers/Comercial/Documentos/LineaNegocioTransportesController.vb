﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Negocio.Comercial.Documentos

''' <summary>
''' Controlador <see cref="LineaNegocioTransportesController"/>
''' </summary>
<Authorize>
Public Class LineaNegocioTransportesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As LineaNegocioTransportes) As Respuesta(Of IEnumerable(Of LineaNegocioTransportes))
        Return New LogicaLineaNegocioTransportes(CapaPersistenciaLineaNegocioTransportes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As LineaNegocioTransportes) As Respuesta(Of LineaNegocioTransportes)
        Return New LogicaLineaNegocioTransportes(CapaPersistenciaLineaNegocioTransportes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As LineaNegocioTransportes) As Respuesta(Of Long)
        Return New LogicaLineaNegocioTransportes(CapaPersistenciaLineaNegocioTransportes).Guardar(entidad)
    End Function

End Class
