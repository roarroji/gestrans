﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Negocio.Comercial.Documentos

''' <summary>
''' Controlador <see cref="TarifarioComprasController"/>
''' </summary>
<Authorize>
Public Class TarifarioComprasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TarifarioCompras) As Respuesta(Of IEnumerable(Of TarifarioCompras))
        Return New LogicaTarifarioCompras(CapaPersistenciaTarifarioCompras).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TarifarioCompras) As Respuesta(Of TarifarioCompras)
        Return New LogicaTarifarioCompras(CapaPersistenciaTarifarioCompras).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TarifarioCompras) As Respuesta(Of Long)
        Return New LogicaTarifarioCompras(CapaPersistenciaTarifarioCompras).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As TarifarioCompras) As Respuesta(Of Boolean)
        Return New LogicaTarifarioCompras(CapaPersistenciaTarifarioCompras).Anular(entidad)
    End Function

End Class
