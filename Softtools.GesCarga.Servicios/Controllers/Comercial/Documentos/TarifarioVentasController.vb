﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Comercial.Documentos
Imports Softtools.GesCarga.Negocio.Comercial.Documentos

''' <summary>
''' Controlador <see cref="TarifarioVentasController"/>
''' </summary>
<Authorize>
Public Class TarifarioVentasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As TarifarioVentas) As Respuesta(Of IEnumerable(Of TarifarioVentas))
        Return New LogicaTarifarioVentas(CapaPersistenciaTarifarioVentas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As TarifarioVentas) As Respuesta(Of TarifarioVentas)
        Return New LogicaTarifarioVentas(CapaPersistenciaTarifarioVentas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As TarifarioVentas) As Respuesta(Of Long)
        Return New LogicaTarifarioVentas(CapaPersistenciaTarifarioVentas).Guardar(entidad)
    End Function
    <ActionName("GenerarPlanitilla")>
    Public Function GenerarPlanitilla(ByVal Filtro As TarifarioVentas) As Respuesta(Of TarifarioVentas)
        Return New LogicaTarifarioVentas(CapaPersistenciaTarifarioVentas).GenerarPlanitilla(Filtro)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As TarifarioVentas) As Respuesta(Of Boolean)
        Return New LogicaTarifarioVentas(CapaPersistenciaTarifarioVentas).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarDetalle")>
    Public Function ConsultarDetalleTarifarioVentas(ByVal entidad As DetalleTarifarioVentas) As Respuesta(Of IEnumerable(Of DetalleTarifarioVentas))
        Return New LogicaTarifarioVentas(CapaPersistenciaTarifarioVentas).ConsultarDetalleTarifarioVentas(entidad)
    End Function
    <HttpPost>
    <ActionName("ObtenerDetalleTarifarioPaqueteria")>
    Public Function ObtenerDetalleTarifarioPaqueteria(ByVal filtro As TarifarioVentas) As Respuesta(Of TarifarioVentas)
        Return New LogicaTarifarioVentas(CapaPersistenciaTarifarioVentas).ObtenerDetalleTarifarioPaqueteria(filtro)
    End Function
End Class
