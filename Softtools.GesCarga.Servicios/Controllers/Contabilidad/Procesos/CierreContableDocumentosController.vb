﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Contabilidad.Procesos
Imports Softtools.GesCarga.Negocio.Contabilidad

''' <summary>
''' Controlador <see cref="CierreContableDocumentosController"/>
''' </summary>
<Authorize>
Public Class CierreContableDocumentosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As CierreContableDocumentos) As Respuesta(Of IEnumerable(Of CierreContableDocumentos))
        Return New LogicaCierreContableDocumentos(CapaPersistenciaCierreContableDocumentos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As CierreContableDocumentos) As Respuesta(Of CierreContableDocumentos)
        Return New LogicaCierreContableDocumentos(CapaPersistenciaCierreContableDocumentos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As CierreContableDocumentos) As Respuesta(Of Long)
        Return New LogicaCierreContableDocumentos(CapaPersistenciaCierreContableDocumentos).Guardar(entidad)
    End Function

End Class
