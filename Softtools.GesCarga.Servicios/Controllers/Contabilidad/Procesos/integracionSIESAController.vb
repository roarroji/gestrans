﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Contabilidad.Documentos
Imports Softtools.GesCarga.Negocio.Contabilidad

''' <summary>
''' Controlador <see cref="integracionSIESAController"/>
''' </summary>
<Authorize>
Public Class integracionSIESAController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As integracionSIESA) As Respuesta(Of IEnumerable(Of integracionSIESA))
        Return New LogicaintegracionSIESA(CapaPersistenciaintegracionSIESA).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As integracionSIESA) As Respuesta(Of integracionSIESA)
        Return New LogicaintegracionSIESA(CapaPersistenciaintegracionSIESA).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As integracionSIESA) As Respuesta(Of Long)
        Return New LogicaintegracionSIESA(CapaPersistenciaintegracionSIESA).Guardar(entidad)
    End Function

End Class
