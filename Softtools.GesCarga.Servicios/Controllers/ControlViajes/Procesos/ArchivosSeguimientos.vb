﻿Imports QRCodeEncoderLibrary
Imports System.Data.SqlClient
Imports System.Web.Http
Imports Microsoft.Reporting.WebForms
Imports Softtools.GesCarga.Entidades.ControlViajes
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.Drawing
Imports System.IO
Imports Softtools.GesCarga.Entidades.Despachos.Masivo
Imports QRCodeSharedLibrary
''' <summary>
''' Controlador <see cref="ArchivosSeguimientos"/>
''' </summary>
<Authorize>
Public Class ArchivosSeguimientos
    Inherits System.Web.UI.Page
    Dim objGeneral As clsGeneral
    Dim repDocumento As ReportViewer
    Private Encoder As QRCodeEncoder
    Private QRCodeImage As Bitmap
    Private QRCodeImageArea As System.Drawing.Rectangle = New System.Drawing.Rectangle

    Public Function Configurar_Reporte_Seguimiento_Vehicular(Entidad As DetalleSeguimientoVehiculos) As Byte()
        Me.objGeneral = New clsGeneral
        Me.repDocumento = New ReportViewer
        'Definir variables
        Dim strConsulta As String = ""
        Dim strPathRepo As String = ""
        Dim dsReportes As New DataSet


        strPathRepo = AppDomain.CurrentDomain.BaseDirectory() + "Controllers\ControlViajes\Procesos\lstReporteSeguimiento_" & Entidad.NombreCortoEmpresa & ".rdlc"



        'Armar la consulta
        strConsulta = "EXEC gps_Reporte_Seguimiento_Vehiculos  "

        strConsulta += Entidad.CodigoEmpresa.ToString()
        strConsulta += "," + Entidad.Codigo.ToString()

        'Guardar el resultado del sp en un dataset
        Dim dtsGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsGuias.Fill(dsReportes, "dtsReporteSeguimiento")

        'Pasar el dataset a un datatable
        Dim dtbGuias As New DataTable
        dtbGuias = dsReportes.Tables("dtsReporteSeguimiento")

        'Armar la consulta
        strConsulta = "EXEC gsp_consultar_detalle_reporte_seguimiento_vehiculos_planilla_despachos "

        strConsulta += Entidad.CodigoEmpresa.ToString()
        strConsulta += "," + Entidad.Codigo.ToString()

        'Guardar el resultado del sp en un dataset
        Dim dtsDetalleRporteSeguimiento As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsDetalleRporteSeguimiento.Fill(dsReportes, "dtsDetalleRporteSeguimiento")

        'Pasar el dataset a un datatable
        Dim dtbDetalleRporteSeguimiento As New DataTable
        dtbDetalleRporteSeguimiento = dsReportes.Tables("dtsDetalleRporteSeguimiento")


        ' Configurar el objeto ReportViewer
        repDocumento.LocalReport.EnableExternalImages = True
        repDocumento.LocalReport.ReportPath = (strPathRepo)

        'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        repDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsReporteSeguimiento", CType(dtbGuias, DataTable)))
        repDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsDetalleRporteSeguimiento", CType(dtbDetalleRporteSeguimiento, DataTable)))
        repDocumento.LocalReport.Refresh()

        Dim pdf = Me.objGeneral.SerializarReportePdf(repDocumento)
        Return pdf

    End Function
    'Enviar Email Seguimiento Vehicular'
    Public Function Configurar_PDF_Seguimiento_Correo(Entidad As DetalleSeguimientoVehiculos) As Byte()
        Me.objGeneral = New clsGeneral
        Me.repDocumento = New ReportViewer
        'Definir variables
        Dim strConsulta As String = ""
        Dim strPathRepo As String = ""
        Dim dsReportes As New DataSet

        strPathRepo = AppDomain.CurrentDomain.BaseDirectory() + "Controllers\ControlViajes\Procesos\lstSeguimientoCorreo.rdlc"

        'Armar la consulta
        strConsulta = "EXEC gsp_Reporte_Seguimiento_Correo "

        strConsulta += Entidad.CodigoEmpresa.ToString()
        strConsulta += "," + Entidad.Codigo.ToString()

        'Guardar el resultado del sp en un dataset
        Dim dtsGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsGuias.Fill(dsReportes, "dtsReporteSeguimientoCorreo")

        'Pasar el dataset a un datatable
        Dim dtbGuias As New DataTable
        dtbGuias = dsReportes.Tables("dtsReporteSeguimientoCorreo")

        'Armar la consulta
        strConsulta = "EXEC gsp_consultar_detalle_reporte_seguimiento_vehiculos_planilla_despachos "

        strConsulta += Entidad.CodigoEmpresa.ToString()
        strConsulta += "," + Entidad.Codigo.ToString()

        'Guardar el resultado del sp en un dataset
        Dim dtsDetalleRporteSeguimiento As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsDetalleRporteSeguimiento.Fill(dsReportes, "dtsDetalleRporteSeguimientoCorreo")

        'Pasar el dataset a un datatable
        Dim dtbDetalleRporteSeguimiento As New DataTable
        dtbDetalleRporteSeguimiento = dsReportes.Tables("dtsDetalleRporteSeguimientoCorreo")


        ' Configurar el objeto ReportViewer
        repDocumento.LocalReport.EnableExternalImages = True
        repDocumento.LocalReport.ReportPath = (strPathRepo)

        'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        repDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsReporteSeguimientoCorreo", CType(dtbGuias, DataTable)))
        repDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsDetalleRporteSeguimientoCorreo", CType(dtbDetalleRporteSeguimiento, DataTable)))
        repDocumento.LocalReport.Refresh()

        Dim pdf = Me.objGeneral.SerializarReportePdf(repDocumento)
        Return pdf

    End Function

    Public Function Configurar_Listado_Seguimiento_Vehicular(Entidad As DetalleSeguimientoVehiculos) As Byte()
        Me.objGeneral = New clsGeneral
        Me.repDocumento = New ReportViewer
        'Definir variables
        Dim strConsulta As String = ""
        Dim strPathRepo As String = ""
        Dim dsReportes As New DataSet

        strPathRepo = AppDomain.CurrentDomain.BaseDirectory() + "Controllers\ControlViajes\Procesos\lstListadoSeguimiento.rdlc"

        'Armar la consulta
        strConsulta = "EXEC gps_Listado_Seguimiento_Vehiculos_Transito_Clientes  "

        strConsulta += Entidad.CodigoEmpresa.ToString()
        strConsulta += "," + Entidad.CodigoCliente.ToString()

        'Guardar el resultado del sp en un dataset
        Dim dtsGuias As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsGuias.Fill(dsReportes, "dtsListadoSeguimiento")

        'Pasar el dataset a un datatable
        Dim dtbGuias As New DataTable
        dtbGuias = dsReportes.Tables("dtsListadoSeguimiento")

        ' Configurar el objeto ReportViewer
        repDocumento.LocalReport.EnableExternalImages = True
        repDocumento.LocalReport.ReportPath = (strPathRepo)

        'El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        repDocumento.LocalReport.DataSources.Add(New ReportDataSource("dtsListadoSeguimiento", CType(dtbGuias, DataTable)))
        repDocumento.LocalReport.Refresh()

        Dim pdf = Me.objGeneral.SerializarReportePdf(repDocumento)
        Return pdf

    End Function
    Public Function Configurar_Reporte_Manifiestos(Entidad As DetalleSeguimientoVehiculos)

        Me.objGeneral = New clsGeneral
        Me.repDocumento = New ReportViewer
        'Definir variables
        Dim strConsulta As String = ""
        'Definir variables
        Dim strPathRepo As String = AppDomain.CurrentDomain.BaseDirectory() + "Controllers\ControlViajes\Procesos\Despachos\Manifiesto\repManifiesto_" & Entidad.CodigoEmpresa.ToString() & ".rdlc"
        Dim dsReportes As New DataSet

        Dim raizAplicacion = HttpRuntime.AppDomainAppPath

        'Armar la consulta
        strConsulta = "EXEC gsp_Reporte_Encabezado_Manifiesto "

        strConsulta += Entidad.CodigoEmpresa.ToString()
        strConsulta += "," + Entidad.NumeroManifiesto.ToString()

        'Guardar el resultado del sp en un dataset
        Dim dtsReporteManifiesto As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsReporteManifiesto.Fill(dsReportes, "dtsEncabezadoReporteManifiesto")
        Dim dtbReporteManifiesto As New DataTable



        'Armar la consulta
        strConsulta = "EXEC gsp_Reporte_Detalle_Manifiesto "


        strConsulta += Entidad.CodigoEmpresa.ToString()
        strConsulta += "," + Entidad.NumeroManifiesto.ToString()

        'Guardar el resultado del sp en un dataset
        Dim dtsDetalleReporteManifiesto As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsDetalleReporteManifiesto.Fill(dsReportes, "dtsDetalleReporteManifiesto")

        'Pasar el dataset a un datatable
        Dim dtbReporteDetalleManifiesto As New DataTable
        dtbReporteDetalleManifiesto = dsReportes.Tables("dtsDetalleReporteManifiesto")
        Dim cadenaQR = ""
        Try
            cadenaQR = "MEC:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Numero_Manifiesto_Electronico").ToString() + vbNewLine
            cadenaQR += "Fecha:" + Format(Date.Parse(dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("FechaExpedicion").ToString()), "yyyy/MM/dd") + vbNewLine
            cadenaQR += "Placa:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("PlacaVehiculo").ToString() + vbNewLine
            cadenaQR += "Remolque:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("PlacaSemiremolque").ToString() + vbNewLine
            cadenaQR += "Config:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Configuracion_Vehiculo").ToString() + vbNewLine
            cadenaQR += "Orig:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("CiudadOrigen").ToString() + vbNewLine
            cadenaQR += "Dest:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("CiudadDestino").ToString() + vbNewLine
            cadenaQR += "Mercancia:" + dsReportes.Tables("dtsDetalleReporteManifiesto").Rows(0)("NombreProducto").ToString() + vbNewLine
            cadenaQR += "Conductor:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("IdentificacionConductor").ToString() + vbNewLine
            cadenaQR += "Empresa:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("NombreEmpresa").ToString() + vbNewLine
            If dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Observacion_Manifiesto_Electronico").ToString() <> String.Empty Then
                cadenaQR += "Obs:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Observacion_Manifiesto_Electronico").ToString() + vbNewLine
            End If
            cadenaQR += "Seguro:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Codigo_Seguridad_Manifiesto_Electronico").ToString()

        Catch ex As Exception
            cadenaQR = "MEC:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Numero_Manifiesto_Electronico").ToString() + vbNewLine
            cadenaQR += "Fecha:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("FechaExpedicion").ToString() + vbNewLine
            cadenaQR += "Placa:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("PlacaVehiculo").ToString() + vbNewLine
            cadenaQR += "Remolque:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("PlacaSemiremolque").ToString() + vbNewLine
            cadenaQR += "Config: Orig:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("CiudadOrigen").ToString() + vbNewLine
            cadenaQR += "Dest:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("CiudadDestino").ToString() + vbNewLine
            cadenaQR += "Conductor:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("TitutlarConductor").ToString() + vbNewLine
            cadenaQR += "Empresa:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("NombreEmpresa").ToString() + vbNewLine
            If dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Observacion_Manifiesto_Electronico").ToString() <> String.Empty Then
                cadenaQR += "Obs:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Observacion_Manifiesto_Electronico").ToString() + vbNewLine
            End If
            cadenaQR += " Seguro:" + dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("Codigo_Seguridad_Manifiesto_Electronico").ToString() + vbNewLine

        End Try
        dsReportes.Tables("dtsEncabezadoReporteManifiesto").Rows(0)("QRManifiesto") = ImageToByte(EncodeQRIMG(cadenaQR))
        'Dim a = QR_Generator.Encode(cadenaQR.le)
        'Pasar el dataset a un datatable
        dtbReporteManifiesto = dsReportes.Tables("dtsEncabezadoReporteManifiesto")

        ' Configurar el objeto ReportViewer
        repDocumento.LocalReport.EnableExternalImages = True
        repDocumento.LocalReport.ReportPath = (strPathRepo)

        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        Dim rds As New ReportDataSource("dtsEncabezadoReporteManifiesto", dsReportes.Tables(0))
        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        Dim rd As New ReportDataSource("dtsDetalleReporteManifiesto", dsReportes.Tables(1))
        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

        repDocumento.LocalReport.DataSources.Clear() 'limpio la fuente de datos
        repDocumento.LocalReport.DataSources.Add(rds)
        repDocumento.LocalReport.DataSources.Add(rd)
        repDocumento.LocalReport.Refresh()


        'Mostrar el ReportViewer como un pdf en el mismo navegador
        Return Me.objGeneral.SerializarReportePdf(repDocumento)

    End Function

    Public Function Configurar_Reporte_Planilla_Despacho(Entidad As DetalleSeguimientoVehiculos)

        Dim strConsulta As String = ""

        'Definir variables
        Me.objGeneral = New clsGeneral
        Me.repDocumento = New ReportViewer
        'Definir variables
        'Definir variables
        Dim strPathRepo As String = AppDomain.CurrentDomain.BaseDirectory() + "Controllers\ControlViajes\Procesos\Despachos\PlanillaDespachos\repPlanillaDespacho_" & Entidad.CodigoEmpresa.ToString() & ".rdlc"
        Dim dsReportes As New DataSet

        Dim raizAplicacion = HttpRuntime.AppDomainAppPath

        'Armar la consulta
        strConsulta = "EXEC gps_Reporte_Planilla_Despacho "

        strConsulta += Entidad.CodigoEmpresa.ToString()
        strConsulta += "," + Entidad.NumeroPlanilla.ToString()
        strConsulta += "," + Entidad.UsuarioCrea.Codigo.ToString()

        'Guardar el resultado del sp en un dataset
        Dim dtsReportePlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsReportePlanillaDespacho.Fill(dsReportes, "dtsReportePlanillaDespacho")

        'Pasar el dataset a un datatable
        Dim dtbReportePlanillaDespacho As New DataTable
        dtbReportePlanillaDespacho = dsReportes.Tables("dtsReportePlanillaDespacho")

        'Armar la consulta
        strConsulta = "EXEC gsp_Reporte_Detalle_Planilla_Despacho "

        strConsulta += Entidad.CodigoEmpresa.ToString()
        strConsulta += "," + Entidad.NumeroPlanilla.ToString()

        'Guardar el resultado del sp en un dataset
        Dim dtsReporteDetallePlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsReporteDetallePlanillaDespacho.Fill(dsReportes, "dtsReporteDetallePlanillaDespacho")

        'Pasar el dataset a un datatable
        Dim dtbReporteDetallePlanillaDespacho As New DataTable
        dtbReporteDetallePlanillaDespacho = dsReportes.Tables("dtsReporteDetallePlanillaDespacho")

        'Armar la consulta
        strConsulta = "EXEC gsp_Reporte_Distribucion_Remesa "

        strConsulta += Entidad.CodigoEmpresa.ToString()
        strConsulta += "," + Entidad.NumeroPlanilla.ToString()

        'Guardar el resultado del sp en un dataset
        Dim dtsReporteDistribucionRemesasPlanillaDespacho As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsReporteDistribucionRemesasPlanillaDespacho.Fill(dsReportes, "dtsReporteDistribucionRemesasPlanillaDespacho")

        'Pasar el dataset a un datatable
        Dim dtbReporteDistribucionRemesasPlanillaDespacho As New DataTable
        dtbReporteDistribucionRemesasPlanillaDespacho = dsReportes.Tables("dtsReporteDistribucionRemesasPlanillaDespacho")


        ' Configurar el objeto ReportViewer
        repDocumento.LocalReport.EnableExternalImages = True
        repDocumento.LocalReport.ReportPath = (strPathRepo)

        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        Dim rds As New ReportDataSource("dtsReportePlanillaDespacho", dsReportes.Tables(0))
        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        Dim rd As New ReportDataSource("dtsReporteDetallePlanillaDespacho", dsReportes.Tables(1))
        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)

        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        Dim rdx As New ReportDataSource("dtsReporteDistribucionRemesasPlanillaDespacho", dsReportes.Tables(2))
        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


        repDocumento.LocalReport.DataSources.Clear() 'limpio la fuente de datos
        repDocumento.LocalReport.DataSources.Add(rds)
        repDocumento.LocalReport.DataSources.Add(rd)
        repDocumento.LocalReport.DataSources.Add(rdx)

        repDocumento.LocalReport.Refresh()

        Return Me.objGeneral.SerializarReportePdf(repDocumento)

    End Function

    Public Function Configurar_Reporte_Orden_Cargues(Entidad As DetalleSeguimientoVehiculos)

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""
        'Definir variables
        Me.objGeneral = New clsGeneral
        Me.repDocumento = New ReportViewer
        'Definir variables
        'Definir variables
        Dim strPathRepo As String = AppDomain.CurrentDomain.BaseDirectory() + "Controllers\ControlViajes\Procesos\Despachos\OrdenCargue\repOrdenCargue_" & Entidad.CodigoEmpresa.ToString() & ".rdlc"
        Dim dsReportes As New DataSet

        Dim raizAplicacion = HttpRuntime.AppDomainAppPath

        'Armar la consulta
        strConsulta = "EXEC gsp_Reporte_OrdenCargue "

        strConsulta += Entidad.CodigoEmpresa.ToString()
        strConsulta += "," + Entidad.NumeroOrden.ToString()
        strConsulta += "," + Entidad.UsuarioCrea.Codigo.ToString()

        'Guardar el resultado del sp en un dataset
        Dim dtsReporteOrdenCargue As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsReporteOrdenCargue.Fill(dsReportes, "dtsReporteOrdenCargue")

        'Pasar el dataset a un datatable
        Dim dtbReporteOrdenCargue As New DataTable
        dtbReporteOrdenCargue = dsReportes.Tables("dtsReporteOrdenCargue")


        ' repDocumento el objeto ReportViewer
        repDocumento.LocalReport.EnableExternalImages = True
        repDocumento.LocalReport.ReportPath = (strPathRepo)

        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        Dim rds As New ReportDataSource("dtsReporteOrdenCargue", dsReportes.Tables(0))
        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


        repDocumento.LocalReport.DataSources.Clear() 'limpio la fuente de datos
        repDocumento.LocalReport.DataSources.Add(rds)
        repDocumento.LocalReport.Refresh()

        Return Me.objGeneral.SerializarReportePdf(repDocumento)

    End Function


    Public Function Configurar_Reporte_Remesas(Empresa As String, Numero As String, Usuario As String)

        Dim strConsulta As String = ""
        Dim strConsultaDetalle As String = ""
        Dim strConsultaConcepto As String = ""

        'Definir variables
        Me.objGeneral = New clsGeneral
        Me.repDocumento = New ReportViewer
        'Definir variables
        'Definir variables
        Dim strPathRepo As String = AppDomain.CurrentDomain.BaseDirectory() + "Controllers\ControlViajes\Procesos\Despachos\Remesa\repRemesa_" & Empresa.ToString() & ".rdlc"

        Dim dsReportes As New DataSet

        Dim raizAplicacion = HttpRuntime.AppDomainAppPath

        'Armar la consulta
        strConsulta = "EXEC gsp_Reporte_Remesa "
        strConsulta += Empresa.ToString()
        strConsulta += "," + Numero.ToString()
        strConsulta += "," + Usuario.ToString()
        'Guardar el resultado del sp en un dataset
        Dim dtsReporteRemesa As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsReporteRemesa.Fill(dsReportes, "dtsReporteRemesa")

        'Pasar el dataset a un datatable
        Dim dtbReporteRemesa As New DataTable
        dtbReporteRemesa = dsReportes.Tables("dtsReporteRemesa")

        'Armar la consulta
        strConsulta = "EXEC gsp_reporte_detalle_precintos_remesas "

        strConsulta += Empresa.ToString()
        strConsulta += "," + Numero.ToString()
        'Guardar el resultado del sp en un dataset
        Dim dtsDetalleRemesa2 As New SqlDataAdapter(strConsulta, objGeneral.ConexionSQL)
        dtsDetalleRemesa2.Fill(dsReportes, "dtsDetalleRemesa")

        'Pasar el dataset a un datatable
        Dim dtbDetalleRemesa2 As New DataTable
        dtbDetalleRemesa2 = dsReportes.Tables("dtsDetalleRemesa")

        ' Configurar el objeto ReportViewer
        repDocumento.LocalReport.EnableExternalImages = True
        repDocumento.LocalReport.ReportPath = (strPathRepo)

        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)
        Dim rds As New ReportDataSource("dtsReporteRemesa", dsReportes.Tables(0))
        Dim rds2 As New ReportDataSource("dtsDetalleRemesa", dsReportes.Tables(1))
        ''El primer parametro que se le pasa a "ReportDataSource" es el nombre que le definió al conjunto de datos cuando creó el ReporteViewer (reporte.rdlc)


        repDocumento.LocalReport.DataSources.Clear() 'limpio la fuente de datos
        repDocumento.LocalReport.DataSources.Add(rds)
        repDocumento.LocalReport.DataSources.Add(rds2)
        repDocumento.LocalReport.Refresh()

        Return Me.objGeneral.SerializarReportePdf(repDocumento)

    End Function
    Public Function Configurar_Reporte_Remesas_general(Entidad As DetalleSeguimientoVehiculos, Planilla As PlanillaDespachos)

        Dim document As New Document() 'documento temportan donde se ira almacendando el pdf'
        Dim MS As New MemoryStream() 'Variable para el manejo del archivo, lo cual permitira mostrarlo'
        Dim pdfCopy As New PdfCopy(document, MS) 'Variable propia de la libreria iTextSharp que facilita el manejo de los archivos pdfs
        document.Open()
        'Inicio Ciclo'
        For Each Remesa In Planilla.ListadoRemesas 'Se genera un ciclo dependiendo de la cantidad de objetos en el listado'
            Dim pdf As Byte() = Configurar_Reporte_Remesas(Entidad.CodigoEmpresa.ToString(), Remesa.NumeroInternoRemesa.ToString(), Entidad.UsuarioCrea.Codigo.ToString()) 'Funcion comun que genera el reporte a travez de report viwer y retorna el pdf en bytes'
            Dim PdfReader As New PdfReader(pdf) 'Lee y convierte los bytes en una varible pdf de la librera otextsharp'
            Dim n = PdfReader.NumberOfPages 'Lee el numero de paginas del arichivo, de tal modo genera un nuevo ciclo para adicionarle al documento temporal cada pagina del reporte'
            Dim Page = 0
            While Page < n
                pdfCopy.AddPage(pdfCopy.GetImportedPage(PdfReader, System.Threading.Interlocked.Increment(Page))) 'Adiciona las paginas del reporte generado al documento temporal creado al inicio'
            End While
            pdfCopy.FreeReader(PdfReader)
            pdfCopy.Flush()
        Next
        'Fin Ciclo'
        document.Close()
        Return MS.GetBuffer()
    End Function

    Public Shared Function ImageToByte(ByVal img As System.Drawing.Image) As Byte()
        Dim converter As ImageConverter = New ImageConverter
        Return CType(converter.ConvertTo(img, GetType(System.Byte())), Byte())
    End Function


    Public Function EncodeQRIMG(ByVal Data As String) As Bitmap
        Try
            Me.Encoder = New QRCodeEncoder
            'Dim QRCodeImage As Bitmap
            Dim ModuleSize As Integer = 4
            Dim QuietZone As Integer = 16
            Dim EciValue As Integer = -1
            Me.Encoder.ErrorCorrection = ErrorCorrection.M
            Me.Encoder.ModuleSize = ModuleSize
            Me.Encoder.QuietZone = QuietZone
            Me.Encoder.ECIAssignValue = EciValue
            ' single segment
            ' encode data
            Me.Encoder.Encode(Data)
            ' create bitmap
            Me.QRCodeImage = Me.Encoder.CreateQRCodeBitmap
            Return Me.QRCodeImage
        Catch Ex As Exception
            Return Nothing
        End Try
    End Function

End Class
