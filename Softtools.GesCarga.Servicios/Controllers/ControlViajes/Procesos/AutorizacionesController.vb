﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.ControlViajes
Imports Softtools.GesCarga.Negocio.ControlViajes

''' <summary>
''' Controlador <see cref="AutorizacionesController"/>
''' </summary>
<Authorize>
Public Class AutorizacionesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Autorizaciones) As Respuesta(Of IEnumerable(Of Autorizaciones))
        Return New LogicaAutorizaciones(CapaPersistenciaAutorizaciones).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Autorizaciones) As Respuesta(Of Autorizaciones)
        Return New LogicaAutorizaciones(CapaPersistenciaAutorizaciones).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Autorizaciones) As Respuesta(Of Long)
        Return New LogicaAutorizaciones(CapaPersistenciaAutorizaciones).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Autorizaciones) As Respuesta(Of Boolean)
        Return New LogicaAutorizaciones(CapaPersistenciaAutorizaciones).Anular(entidad)
    End Function



End Class
