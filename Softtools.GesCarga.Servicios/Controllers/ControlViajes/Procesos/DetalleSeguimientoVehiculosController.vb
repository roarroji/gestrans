﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.ControlViajes
Imports Softtools.GesCarga.Entidades.Despachos.Masivo
Imports Softtools.GesCarga.Negocio.ControlViajes
Imports Softtools.GesCarga.Repositorio.Despachos.Masivo

<Authorize>
Public Class DetalleSeguimientoVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))
        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleSeguimientoVehiculos) As Respuesta(Of DetalleSeguimientoVehiculos)
        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleSeguimientoVehiculos) As Respuesta(Of Long)

        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleSeguimientoVehiculos) As Respuesta(Of Boolean)
        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("CambiarRutaSeguimiento")>
    Public Function CambiarRutaSeguimiento(ByVal entidad As DetalleSeguimientoVehiculos) As Respuesta(Of Boolean)
        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).CambiarRutaSeguimiento(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarRemesas")>
    Public Function ConsultarRemesas(ByVal filtro As DetalleSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))
        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).ConsultarRemesas(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarNovedadesSitiosReporte")>
    Public Function ConsultarNovedadesSitiosReporte(ByVal filtro As DetalleSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))
        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).ConsultarNovedadesSitiosReporte(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarPuestoControlRutas")>
    Public Function ConsultarPuestoControlRutas(ByVal filtro As DetalleSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))
        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).ConsultarPuestoControlRutas(filtro)
    End Function
    <HttpPost>
    <ActionName("ConsultarPuntosControl")>
    Public Function ConsultarPuntosControl(ByVal filtro As DetalleSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))
        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).ConsultarPuntosControl(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarMasterRemesa")>
    Public Function ConsultarMasterRemesa(ByVal filtro As DetalleSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of DetalleSeguimientoVehiculos))
        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).ConsultarMasterRemesa(filtro)
    End Function
    <HttpPost>
    <ActionName("EnviarCorreoCliente")>
    Public Function EnviarCorreoCliente(ByVal filtro As DetalleSeguimientoVehiculos) As Respuesta(Of Boolean)
        If filtro.Codigo > 0 Then
            Dim Reporte As New ArchivosSeguimientos
            'filtro.Documento = Reporte.Configurar_Listado_Seguimiento_Vehicular(filtro)
            filtro.Documento = Reporte.Configurar_Reporte_Seguimiento_Vehicular(filtro)
        End If
        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).EnviarCorreoCliente(filtro)
    End Function
    ''
    <HttpPost>
    <ActionName("EnviarCorreoSeguimiento")>
    Public Function EnviarCorreoSeguimiento(ByVal filtro As DetalleSeguimientoVehiculos) As Respuesta(Of Boolean)
        If filtro.Codigo > 0 Then
            Dim Reporte As New ArchivosSeguimientos
            'filtro.Documento = Reporte.Configurar_Listado_Seguimiento_Vehicular(filtro)
            filtro.Documento = Reporte.Configurar_PDF_Seguimiento_Correo(filtro)
        End If
        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).EnviarCorreoSeguimiento(filtro)
    End Function

    <HttpPost>
    <ActionName("EnviarCorreoAvanceVehiculosCliente")>
    Public Function EnviarCorreoAvanceVehiculosCliente(ByVal filtro As DetalleSeguimientoVehiculos) As Respuesta(Of Boolean)
        'filtro.Codigo = filtro.CodigoCliente
        If filtro.CodigoCliente > 0 Then
            Dim Reporte As New ArchivosSeguimientos
            filtro.Documento = Reporte.Configurar_Listado_Seguimiento_Vehicular(filtro)
        End If
        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).EnviarCorreoAvanceVehiculosCliente(filtro)
    End Function
    <HttpPost>
    <ActionName("EnviarDocumentosConductor")>
    Public Function EnviarDocumentosConductor(ByVal filtro As DetalleSeguimientoVehiculos) As Respuesta(Of Boolean)
        Dim Reporte As New ArchivosSeguimientos



        If filtro.NumeroManifiesto > 0 Then
            filtro.DocumentoManifiesto = Reporte.Configurar_Reporte_Manifiestos(filtro)
        End If
        If filtro.NumeroPlanilla > 0 Then
            filtro.DocumentoPlanilla = Reporte.Configurar_Reporte_Planilla_Despacho(filtro)

            Dim RepPlanilla As New RepositorioPlanillaDespachos
            Dim Planilla As New PlanillaDespachos
            Planilla.CodigoEmpresa = filtro.CodigoEmpresa
            Planilla.Numero = filtro.NumeroPlanilla
            Planilla.TipoDocumento = 150
            Planilla = RepPlanilla.Obtener(Planilla)
            If Planilla.ListadoRemesas.Count() > 0 Then
                filtro.DocumentoRemesa = Reporte.Configurar_Reporte_Remesas_general(filtro, Planilla)
            End If
        End If
        If filtro.NumeroOrden > 0 Then
            filtro.DocumentoOrdenCargue = Reporte.Configurar_Reporte_Orden_Cargues(filtro)
        End If

        Return New LogicaDetalleSeguimientoVehiculos(CapaPersistenciaDetalleSeguimientoVehiculos).EnviarDocumentosConductor(filtro)
    End Function
End Class
