﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.Configuration.ConfigurationManager

Public Class clsGeneral

    Inherits Web.UI.Page

#Region "Declaracion Variables"

    ' Objetos de base de datos
    Public strCadenaDeConexionSQL As String

    Public strCadenaDeConexionSQLDocumentos As String
    Private strMensajeError As String
    Private strSQL As String

    Private sqlConexion As SqlConnection
    Private sqlTransaccion As SqlTransaction
    Private sqlComando As SqlCommand
    Private sqlExcepcionSQL As SqlException
    Private strNombreConexion As String

    Public Property strRutaCarpetaSolicitudes As String

#End Region

#Region "Constantes"
    Const NOMBRE_CONEXION As String = "GestransCarga"
    Const NOMBRE_CONEXION_DOCUMENTOS As String = "GestransCargaDocumentos"

    Public Const FORMATO_FECHA_DIA_MES_ANO As String = "dd/MM/yyyy"
    Public Const FORMATO_FECHA_ANO_MES_DIA As String = "yyyy/MM/dd"
    Public Const FORMATO_FECHA_MES_DIA_AÑO As String = "MM/dd/yyyy"

    Public Const CERO As Byte = 0
    Public Const UNO As Byte = 1
    Public Const CAMPO_NUMERICO As Byte = 1
    Public Const CAMPO_ALFANUMERICO As Byte = 2
    Public Const CANTIDAD_CARACTERES_IDENTIFICACION_TERCERO As Byte = 6
    Public Const PRIMERA_TABLA As Byte = 0
    Public Const PRIMER_REGISTRO As Byte = 0


    Public Const CERO_STRING As Char = "0"
    Public Const VACIO As String = ""

    Public Const ITEM_VACIO As String = "       "
    Public Const CODIGO_TIPO_NATURALEZA_JURIDICA As Short = 2298
    Public Const ESTADO_ACTIVO_TERCEROS As Integer = 2304
    Public Const TREINTA_DIAS As Integer = 30
    ' Errores que se atrapan
    Public Const ERROR_INDICE_UNICO_REPETIDO As String = "Se produjo un error al guardar, debido a que el índice único se encuentra repetido"
    Public Const ERROR_LLAVE_DUPLICADA_ As String = "Se produjo un error al guardar debido a que la llave se encuentra duplicada"
    Public Const ERROR_LLAVE_FORANEA_DESDE_OTRA_TABLA As String = "The DELETE statement conflicted with the REFERENCE constraint"

    Public Const ERROR_LLAVE_DUPLICADA As String = "Violation of PRIMARY KEY constraint "
    Public Const CODIGO_ERROR_LLAVE_DUPLICADA As Short = 2627
    Public Const CODIGO_ERROR_INDICE_UNICO_REPETIDO As Short = 2601
    Public Const ERROR_INDICE_DUPLICADO As String = "Cannot insert duplicate key row in object " 'TABLE REFERENCE. The statement has been terminated."
    Public Const ERROR_INFRACCION_INTEGRIDAD_REFERENCIAL As String = "DELETE statement conflicted with TABLE REFERENCE constraint "

    ' Tipos de identificación
    Public Const TIID_NIT As Integer = 102
    Public Const TIID_CEDULA As Integer = 101
    Public Const TIID_CEDULA_EXTRANJERIA As Integer = 103
    ' Constante de estados de documentos
    Public Const ESTADO_BORRADOR As Byte = 0
    Public Const ESTADO_DEFINITIVO As Byte = 1
    Public Const ESTADO_LEGALIZADO As Byte = 2
    Public Const CODIGO_ANULADO As Byte = 2
    Public Const ESTADO_ANULADO As Byte = 1
    Public Const ESTADO_INACTIVO As Byte = 0
    Public Const CODIGO_INACTIVO As Byte = 2
    Public Const ESTADO_BLOQUEADO As Byte = 1
    Public Const CODIGO_BLOQUEADO As Byte = 2
#End Region

#Region "Constantes Tipo Documentos"
    Public Const TIDO_FACTURAS As Integer = 60
    Public Const TIDO_COMPROBANTE_CONTABLE As Integer = 200
#End Region

#Region "Constantes Catálogos"
    Public Const CATA_CODIGO_ESTADO_INTERFAZ_CONTABLE As Integer = 101
    Public Const CATA_CODIGO_PROGRAMA_CONTABLE As Integer = 116
    Public Const CATA_CODIGO_TIPO_ARCHIVO_INTERFAZ As Integer = 117
#End Region

#Region "Constantes Valor Catálogos"
    Public Const CODIGO_ESTADO_INTERFAZ_CONTABLE_PENDIENTE As Integer = 8954
    Public Const CODIGO_ESTADO_INTERFAZ_CONTABLE_REPORTADO As Integer = 8955
    Public Const CODIGO_ESTADO_INTERFAZ_CONTABLE_NO_REPORTAR As Integer = 8956

    Public Const CODIGO_PROGRAMA_CONTABLE_CONTAi As Integer = 7688
    Public Const CODIGO_PROGRAMA_CONTABLE_WORLD_OFFICE As Integer = 9488

    Public Const CODIGO_TIPO_ARCHIVO_INTERFAZ_FACURAS As Integer = 9489
    Public Const CODIGO_TIPO_ARCHIVO_INTERFAZ_LIQUIDACION_ESPECIAL As Integer = 117001
    Public Const CODIGO_TIPO_ARCHIVO_INTERFAZ_COMPROBANTE_EGRESO As Integer = 117002
    Public Const CODIGO_TIPO_ARCHIVO_INTERFAZ_COMPROBANTE_INGRESO As Integer = 117003
    Public Const CODIGO_TIPO_ARCHIVO_INTERFAZ_COMPROBANTE_CAUSACION As Integer = 117004


#End Region

#Region "Constantes Numero Errores"
    Public Const NUMERO_ERROR_INTEGRIDAD_REFERENCIAL As Byte = 5
#End Region

#Region "Constructor"

    Public Sub New()
        Try
            Me.strCadenaDeConexionSQL = ConfigurationManager.ConnectionStrings(clsGeneral.NOMBRE_CONEXION).ConnectionString
            Me.strCadenaDeConexionSQLDocumentos = ConfigurationManager.ConnectionStrings(clsGeneral.NOMBRE_CONEXION_DOCUMENTOS).ConnectionString

        Catch
            Try
                Me.strCadenaDeConexionSQL = AppSettings.Get("ConexionSQL")
                Me.strCadenaDeConexionSQLDocumentos = AppSettings.Get("ConexionSQLDocumental")
            Catch ex As Exception

            End Try
        End Try

        'Me.strRutaCarpetaSolicitudes = ConfigurationManager.AppSettings("RutaCarpetaSolicitudesEspeciales").ToString()

    End Sub

#End Region

#Region "Propiedades"

    Public Property NombreConexion() As String
        Get
            Return Me.strNombreConexion
        End Get
        Set(ByVal value As String)
            Me.strNombreConexion = value
        End Set
    End Property

    Public Property CadenaDeConexionSQL() As String
        Get
            Me.strCadenaDeConexionSQL = ConfigurationManager.ConnectionStrings(clsGeneral.NOMBRE_CONEXION).ConnectionString
            Return Me.strCadenaDeConexionSQL
        End Get
        Set(ByVal value As String)
            Me.strCadenaDeConexionSQL = value
        End Set
    End Property

    Public Property ConexionSQL() As SqlConnection
        Get
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
            End If
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQL)
            End If
            Me.sqlConexion = value
        End Set
    End Property

    Public Property ConexionSQLDocumental() As SqlConnection
        Get
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQLDocumentos)
            End If
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaDeConexionSQLDocumentos)
            End If
            Me.sqlConexion = value
        End Set
    End Property

    Public Property TransaccionSQL() As SqlTransaction
        Get
            Return Me.sqlTransaccion
        End Get
        Set(ByVal value As SqlTransaction)
            Me.sqlTransaccion = value
        End Set
    End Property

    Public Property ComandoSQL() As SqlCommand
        Get
            Return Me.sqlComando
        End Get
        Set(ByVal value As SqlCommand)
            Me.sqlComando = value
        End Set
    End Property

    Public Property ExcepcionSQL() As SqlException
        Get
            Return Me.sqlExcepcionSQL
        End Get
        Set(ByVal value As SqlException)
            Me.sqlExcepcionSQL = value
        End Set
    End Property

#End Region

#Region "Funciones Publicas"

    Public Function Formatear_Fecha_SQL(ByVal strFecha As String) As String

        If Len(strFecha) = 0 Then
            Formatear_Fecha_SQL = "01/01/1900"
        Else
            Formatear_Fecha_SQL = Mid$(strFecha, 4, 2) & "/" & Mid$(strFecha, 1, 2) & "/" & Mid$(strFecha, 7, 4)
        End If

    End Function

    Public Function Formatear_Fecha_Hora_SQL(ByVal dteFecha As Date) As String
        Dim strFecha As String

        strFecha = dteFecha.Month & "/" & dteFecha.Day & "/" & dteFecha.Year & " "
        strFecha += Mid(dteFecha.ToString, 11)
        Formatear_Fecha_Hora_SQL = strFecha

    End Function

    Public Function Leer_Fecha_SQL(ByVal strFecha As String) As String

        If Len(strFecha) = 0 Then
            Leer_Fecha_SQL = ""
        ElseIf Mid$(strFecha, 1, 10) = "01/01/1900" Then
            Leer_Fecha_SQL = ""
        Else
            Leer_Fecha_SQL = Mid$(strFecha, 1, 10)
        End If

    End Function

    Public Function Fecha_Valida(ByVal strFecha As String) As Boolean
        Dim strTemp As String

        strTemp = strFecha

        If IsDate(strFecha) Then
            ' Validar el dia
            If Val(Mid$(strTemp, 1, 2)) >= 1 And Val(Mid$(strTemp, 1, 2)) <= 31 Then
                ' Validar el mes
                If Val(Mid$(strTemp, 4, 2)) >= 1 And Val(Mid$(strTemp, 4, 2)) <= 12 Then
                    ' Validar el año
                    If Val(Mid$(strTemp, 7, 4)) >= 1980 And Val(Mid$(strTemp, 7, 4)) <= 2030 Then
                        Fecha_Valida = True
                    Else
                        Fecha_Valida = False
                        Exit Function
                    End If
                Else
                    Fecha_Valida = False
                    Exit Function
                End If
            Else
                Fecha_Valida = False
                Exit Function
            End If
        Else
            Fecha_Valida = False
        End If

    End Function
    ''' <summary>
    ''' Crea un archivo PDF a partir de un ReportViewer
    ''' </summary>
    ''' <param name="visorReporte"></param>
    ''' <returns></returns>
    Public Function SerializarReportePdf(visorReporte As ReportViewer) As Byte()
        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        Dim mimeType As String = String.Empty
        Dim encoding As String = String.Empty
        Dim filenameExtension As String = String.Empty

        visorReporte.ProcessingMode = ProcessingMode.Local

        Dim pdf As Byte() = visorReporte.LocalReport.Render("PDF", Nothing, mimeType, encoding, filenameExtension, streamids, warnings)
        Return pdf

    End Function
    Public Function Traducir_Error(ByVal Descripcion As String) As String

        Dim strError As String, strErrorNativo As String
        Dim intCont As Short

        For intCont = 1 To Len(Descripcion)
            If Mid(Descripcion, intCont, 1) = "'" Then
                Exit For
            End If
        Next

        strErrorNativo = Mid(Descripcion, 1, intCont - 1)

        Select Case strErrorNativo
            Case clsGeneral.ERROR_INDICE_DUPLICADO
                strError = ERROR_INDICE_UNICO_REPETIDO
            Case clsGeneral.ERROR_LLAVE_DUPLICADA
                strError = "Se produjo un error al guardar debido a que la llave se encuentra duplicada"
            Case clsGeneral.ERROR_INFRACCION_INTEGRIDAD_REFERENCIAL
                strError = "Se produjo un error de integridad referencial al borrar el registro en la base de datos"
            Case ERROR_LLAVE_FORANEA_DESDE_OTRA_TABLA
                strError = "No se puede eliminar el registro actual porque se encuentra relacionado con otra tabla"
            Case Else
                strError = Descripcion
        End Select

        Traducir_Error = strError
    End Function
    Public Function SerializarReporteExcel(ByVal visorReporte As ReportViewer, ByRef NombreExtension As String, ByRef mimeType As String, ByVal NombreArchivo As String) As Byte()

        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        mimeType = String.Empty
        Dim encoding As String = String.Empty
        NombreExtension = String.Empty
        Dim deviceInfo As String = String.Empty
        Dim streams As String() = Nothing


        Dim excel As Byte() = visorReporte.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)
        NombreExtension = String.Format("{0}.{1}", NombreArchivo, "xls")

        Return excel

    End Function



    Public Function Fecha_Valida(ByVal Fecha As Date, Optional ByVal ValidaPeriodo As Boolean = False, Optional ByVal CodigoEmpresa As Integer = 1, Optional ByVal ValidaFechaHoy As Boolean = False, Optional ByVal TipoDocumento As Integer = 0) As Boolean
        Dim provider As IFormatProvider
        Dim dteFechaHoy As Date

        dteFechaHoy = Date.Now.Date

        Fecha_Valida = True

        Fecha_Valida = Date.TryParseExact(Fecha.Date.ToShortDateString(), "dd/MM/yyyy", provider, Globalization.DateTimeStyles.None, Fecha)
        If Fecha.ToShortDateString = "01/01/0001" Then
            Fecha_Valida = False
        End If
        If TipoDocumento = 0 Then
            If ValidaPeriodo Then
                Dim strSQL As String
                Dim sdrFecha As SqlDataReader
                Dim ComandoSQL As SqlCommand

                strSQL = "Select *"
                strSQL += " FROM Cierre_Contables"
                strSQL += " WHERE EMPR_Codigo = " & CodigoEmpresa
                strSQL += " AND ANO = " & Fecha.Year
                strSQL += " AND MESE_Codigo  = " & Fecha.Month
                strSQL += " AND ESPC_Codigo  = 1 "

                ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
                Me.ConexionSQL.Open()
                sdrFecha = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)

                If sdrFecha.Read() Then
                    Fecha_Valida = True
                Else
                    Fecha_Valida = False
                End If

                sdrFecha.Close()
                sdrFecha.Dispose()
                ComandoSQL.Dispose()

                Me.ConexionSQL.Close()

            End If
        Else
            If ValidaPeriodo Then
                Dim strSQL As String
                Dim sdrFecha As SqlDataReader
                Dim ComandoSQL As SqlCommand

                strSQL = "Select *"
                strSQL += " FROM Cierre_Contable_Tipo_Documentos"
                strSQL += " WHERE EMPR_Codigo = " & CodigoEmpresa
                strSQL += " AND ANO = " & Fecha.Year
                strSQL += " AND MESE_Codigo  = " & Fecha.Month
                strSQL += " AND TIDO_Codigo  = " & TipoDocumento
                strSQL += " AND ESPC_Codigo  = 1 "

                ComandoSQL = New SqlCommand(strSQL, Me.ConexionSQL)
                Me.ConexionSQL.Open()
                sdrFecha = ComandoSQL.ExecuteReader(CommandBehavior.CloseConnection)
                ComandoSQL.Dispose()

                If sdrFecha.Read() Then
                    Fecha_Valida = True
                Else
                    Fecha_Valida = False
                End If
                Me.ConexionSQL.Close()
            End If
        End If


        If ValidaFechaHoy Then
            'Válida fecha posterior no se permita ingresar
            If Fecha > dteFechaHoy Then
                Fecha_Valida = False
            End If
        End If

    End Function

    Public Function Email_Valido(ByVal Email As String) As Boolean
        Try
            'Valida Email por medio de expresiones regulares - código cambiado JAO - 24/05/2012

            Dim rgxPatronCorreo As Regex = New Regex("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
            Dim mthEvaluaCorreo As Match = rgxPatronCorreo.Match(Email)

            If mthEvaluaCorreo.Success Then
                Email_Valido = True
            Else
                Email_Valido = False
            End If
        Catch ex As Exception
            Email_Valido = False
        End Try
    End Function

    Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim daDataAdapter As SqlDataAdapter
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
                daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
                daDataAdapter.Fill(dsDataSet)
                ConexionSQL.Close()
            End Using

        Catch ex As Exception
            strError = ex.Message.ToString()
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

        Retorna_Dataset = dsDataSet

        Try
            dsDataSet.Dispose()
        Catch ex As Exception
            strError = ex.Message.ToString()
        End Try

    End Function

    Public Function Ejecutar_SQL(ByVal strSQL As String, Optional ByRef strError As String = "") As Boolean
        Dim ComandoSQL As New SqlCommand
        Dim lonRegiAfec As Long = 0

        Try
            Me.ConexionSQL.Open()

            ComandoSQL.CommandType = CommandType.Text
            ComandoSQL.CommandText = strSQL
            ComandoSQL.Connection = ConexionSQL

            lonRegiAfec = Val(ComandoSQL.ExecuteNonQuery().ToString)
            Ejecutar_SQL = True
        Catch ex As Exception
            strError = ex.Message
            Ejecutar_SQL = False
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

        Return lonRegiAfec

    End Function

    Public Function Ejecutar_SQL_Misma_Transaccion(ByVal strSQL As String, Optional ByRef strError As String = "") As Boolean

        Dim lonRegiAfec As Long = 0

        Try
            'Me.ConexionSQL.Open()

            ComandoSQL.CommandType = CommandType.Text
            ComandoSQL.CommandText = strSQL

            lonRegiAfec = Val(ComandoSQL.ExecuteNonQuery().ToString)
            Ejecutar_SQL_Misma_Transaccion = True

        Catch ex As Exception
            strError = ex.Message
            Ejecutar_SQL_Misma_Transaccion = False

        Finally
            'If ConexionSQL.State() = ConnectionState.Open Then
            '    ConexionSQL.Close()
            'End If
        End Try

        Return lonRegiAfec

    End Function

    Public Function Formatear_Numero(ByVal Numero As String, ByVal Decimales As Integer) As String

        Dim strNumeroFormateado As String
        Dim intCont As Integer
        Dim intPosiPunt As Integer

        strNumeroFormateado = ""
        For intCont = 1 To Numero.Length

            If Mid(Numero, intCont, 1) = "." Then
                strNumeroFormateado += Mid(Numero, 1, intCont - 1)
                intPosiPunt = intCont
                Exit For
            End If
        Next

        If Decimales > 0 Then
            strNumeroFormateado += Mid(Numero, intPosiPunt, Decimales + 1)
        End If

        Return strNumeroFormateado

    End Function

    Public Function Numero_A_Letras(ByVal value As Double) As String
        Dim lonNumero As Long = Math.Abs(value)
        value = lonNumero

        Select Case value
            Case 0 : Numero_A_Letras = "CERO"
            Case 1 : Numero_A_Letras = "UN"
            Case 2 : Numero_A_Letras = "DOS"
            Case 3 : Numero_A_Letras = "TRES"
            Case 4 : Numero_A_Letras = "CUATRO"
            Case 5 : Numero_A_Letras = "CINCO"
            Case 6 : Numero_A_Letras = "SEIS"
            Case 7 : Numero_A_Letras = "SIETE"
            Case 8 : Numero_A_Letras = "OCHO"
            Case 9 : Numero_A_Letras = "NUEVE"
            Case 10 : Numero_A_Letras = "DIEZ"
            Case 11 : Numero_A_Letras = "ONCE"
            Case 12 : Numero_A_Letras = "DOCE"
            Case 13 : Numero_A_Letras = "TRECE"
            Case 14 : Numero_A_Letras = "CATORCE"
            Case 15 : Numero_A_Letras = "QUINCE"

            Case Is < 20 : Numero_A_Letras = "DIECI" & Numero_A_Letras(value - 10)
            Case 20 : Numero_A_Letras = "VEINTE"
            Case Is < 30 : Numero_A_Letras = "VEINTI" & Numero_A_Letras(value - 20)

            Case 30 : Numero_A_Letras = "TREINTA"
            Case 40 : Numero_A_Letras = "CUARENTA"
            Case 50 : Numero_A_Letras = "CINCUENTA"
            Case 60 : Numero_A_Letras = "SESENTA"
            Case 70 : Numero_A_Letras = "SETENTA"
            Case 80 : Numero_A_Letras = "OCHENTA"
            Case 90 : Numero_A_Letras = "NOVENTA"

            Case Is < 100 : Numero_A_Letras = Numero_A_Letras(Int(value \ 10) * 10) & " Y " & Numero_A_Letras(value Mod 10)
            Case 100 : Numero_A_Letras = "CIEN"
            Case Is < 200 : Numero_A_Letras = "CIENTO " & Numero_A_Letras(value - 100)
            Case 200, 300, 400, 600, 800 : Numero_A_Letras = Numero_A_Letras(Int(value \ 100)) & "CIENTOS"
            Case 500 : Numero_A_Letras = "QUINIENTOS"
            Case 700 : Numero_A_Letras = "SETECIENTOS"
            Case 900 : Numero_A_Letras = "NOVECIENTOS"

            Case Is < 1000 : Numero_A_Letras = Numero_A_Letras(Int(value \ 100) * 100) & " " & Numero_A_Letras(value Mod 100)
            Case 1000 : Numero_A_Letras = "MIL"
            Case Is < 2000 : Numero_A_Letras = "MIL " & Numero_A_Letras(value Mod 1000)

            Case Is < 1000000 : Numero_A_Letras = Numero_A_Letras(Int(value \ 1000)) & " MIL"
                If value Mod 1000 Then Numero_A_Letras = Numero_A_Letras & " " & Numero_A_Letras(value Mod 1000)
            Case 1000000 : Numero_A_Letras = "UN MILLON"

            Case Is < 2000000 : Numero_A_Letras = "UN MILLON " & Numero_A_Letras(value Mod 1000000)
            Case Is < 1000000000000.0# : Numero_A_Letras = Numero_A_Letras(Int(value / 1000000)) & " MILLONES "
                If (value - Int(value / 1000000) * 1000000) Then Numero_A_Letras = Numero_A_Letras & " " & Numero_A_Letras(value - Int(value / 1000000) * 1000000)

            Case 1000000000000.0# : Numero_A_Letras = "UN BILLON"
            Case Is < 2000000000000.0# : Numero_A_Letras = "UN BILLON " & Numero_A_Letras(value - Int(value / 1000000000000.0#) * 1000000000000.0#)

            Case Else : Numero_A_Letras = Numero_A_Letras(Int(value / 1000000000000.0#)) & " BILLONES"
                If (value - Int(value / 1000000000000.0#) * 1000000000000.0#) Then Numero_A_Letras = Numero_A_Letras & " " & Numero_A_Letras(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
        End Select
    End Function

    Public Function Redondear_X_Exceso(ByVal Valor As Double) As Integer
        Dim intAuxiliar As Integer
        Dim dblAuxiliar As Double

        intAuxiliar = CInt(Valor)
        Double.TryParse(intAuxiliar.ToString, dblAuxiliar)

        If Valor > dblAuxiliar Then
            Redondear_X_Exceso = intAuxiliar + 1
        Else
            Redondear_X_Exceso = intAuxiliar
        End If

    End Function

    Function Retorna_Campo_BD(ByVal CodigoEmpresa As Integer, ByVal Tabla As String, ByVal CampoConsulta As String, ByVal CampoLlave As String, ByVal TipoLlave As Integer, ByVal ValorLlave As String, ByRef ResultadoConsulta As String, Optional ByVal Condicion As String = "", Optional ByVal SobreNombreCampoConsulta As String = "", Optional ByVal CondicionLibre As String = "") As Boolean

        Try

            Dim strSQL As String

            ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
            Dim daDataAdapter As SqlDataAdapter
            Dim dsDataSet As DataSet = New DataSet

            strSQL = "SELECT TOP 1 " & CampoConsulta & Chr(13)
            strSQL += " FROM " & Tabla & Chr(13)
            strSQL += " WHERE EMPR_Codigo = " & CodigoEmpresa & Chr(13)
            If TipoLlave = CAMPO_NUMERICO Then
                strSQL += " AND " & CampoLlave & " = " & ValorLlave & Chr(13)
            ElseIf TipoLlave = CAMPO_ALFANUMERICO Then
                strSQL += " AND " & CampoLlave & " = '" & ValorLlave & "'" & Chr(13)
            End If
            If Len(Trim(Condicion)) > 0 Then
                strSQL += " AND " & Condicion
            End If

            If Len(Trim(CondicionLibre)) > 0 Then
                strSQL += " " & CondicionLibre
            End If

            If SobreNombreCampoConsulta = "" Then
                SobreNombreCampoConsulta = CampoConsulta
            End If

            daDataAdapter = New SqlDataAdapter(strSQL, ConexionSQL)
            daDataAdapter.Fill(dsDataSet)
            ConexionSQL.Close()
            ResultadoConsulta = dsDataSet.Tables.Item(PRIMERA_TABLA).Rows(PRIMER_REGISTRO).Item(SobreNombreCampoConsulta).ToString()
            Retorna_Campo_BD = True

        Catch ex As Exception
            Retorna_Campo_BD = False


            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If

        End Try
        Return Retorna_Campo_BD

    End Function
    '''' <summary>
    '''' Crea un archivo PDF a partir de un ReportViewer
    '''' </summary>
    '''' <param name="visorReporte"></param>
    '''' <returns></returns>
    'Public Function SerializarReportePdf(visorReporte As ReportViewer) As Byte()
    '    Dim warnings As Warning() = Nothing
    '    Dim streamids As String() = Nothing
    '    Dim mimeType As String = String.Empty
    '    Dim encoding As String = String.Empty
    '    Dim filenameExtension As String = String.Empty

    '    visorReporte.ProcessingMode = ProcessingMode.Local

    '    Dim pdf As Byte() = visorReporte.LocalReport.Render("PDF", Nothing, mimeType, encoding, filenameExtension, streamids, warnings)
    '    Return pdf

    'End Function

    'Public Function SerializarReporteExcel(ByVal visorReporte As ReportViewer, ByRef NombreExtension As String, ByRef mimeType As String, ByVal NombreArchivo As String) As Byte()

    '    Dim warnings As Warning() = Nothing
    '    Dim streamids As String() = Nothing
    '    mimeType = String.Empty
    '    Dim encoding As String = String.Empty
    '    NombreExtension = String.Empty
    '    Dim deviceInfo As String = String.Empty
    '    Dim streams As String() = Nothing


    '    Dim excel As Byte() = visorReporte.LocalReport.Render("Excel", deviceInfo, mimeType, encoding, "xls", streams, warnings)
    '    NombreExtension = String.Format("{0}.{1}", NombreArchivo, "xls")

    '    Return excel

    'End Function

    Public Sub Cargar_Combo(ByVal CodigoEmpresa As Short, ByRef cmbCombo As System.Web.UI.WebControls.DropDownList, ByVal strNombreTabla As String,
        ByVal strCampoCodigo As String, ByVal strCampoNombre As String, Optional ByVal strCampoOrden As String = "",
        Optional ByVal strItemExtra As String = "", Optional ByVal strCondicion As String = "", Optional ByVal strDistinct As String = "", Optional ByVal strCampoLlave As String = "", Optional ByVal strSobreNombreCampoNombre As String = "")

        Try
            Dim dsDataSet As New DataSet()
            strSQL = "SELECT " & strDistinct & " " & strCampoCodigo & "," & strCampoNombre & " FROM " & strNombreTabla _
            & " WHERE EMPR_Codigo = " & CodigoEmpresa
            If Len(strCondicion) > 0 Then
                strSQL += " AND " & strCondicion
            End If
            If Len(strCampoOrden) > 0 Then
                strSQL = strSQL & " ORDER BY " & strCampoOrden
            End If
            If strCampoLlave <> "" Then
                strCampoCodigo = strCampoLlave
            End If
            If strSobreNombreCampoNombre <> "" Then
                strCampoNombre = strSobreNombreCampoNombre
            End If
            ConexionSQL.Open()
            Dim daDataAdapter As New SqlDataAdapter(strSQL, ConexionSQL)

            daDataAdapter.Fill(dsDataSet)
            cmbCombo.DataSource = dsDataSet
            cmbCombo.DataValueField = strCampoCodigo
            cmbCombo.DataTextField = strCampoNombre
            cmbCombo.DataBind()

            If Len(strItemExtra) > 0 Then
                cmbCombo.Items.Add(strItemExtra)
                cmbCombo.SelectedValue() = strItemExtra
            End If

        Catch ex As Exception
            strMensajeError = "clsGeneral Función Cargar_Combo: " & ex.Message
        Finally
            If ConexionSQL.State = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try

    End Sub

    Public Function Retorna_Campos(ByVal intCodigoEmpresa As Integer, ByVal strNombreTabla As String, ByVal arrCampos As String(), ByRef arrValoresCampos As String(), ByVal intNumeroCampos As Integer, ByVal intTipoLlave As Integer, ByVal strCampoLlave As String, ByVal strValorLlave As String, ByVal strCondicion As String, Optional ByRef strError As String = "") As Boolean

        Try
            Dim strSQL As String, intCont As Integer = 0

            strSQL = "SELECT "
            For intCont = 0 To intNumeroCampos - 1
                If intCont = 0 Then
                    strSQL = strSQL & arrCampos(intCont)
                Else
                    strSQL = strSQL & ", " & arrCampos(intCont)
                End If
            Next
            strSQL = strSQL & Chr(10) & " FROM " & strNombreTabla
            strSQL = strSQL & Chr(10) & " WHERE EMPR_Codigo = " & intCodigoEmpresa

            If intTipoLlave = CAMPO_NUMERICO Then
                strSQL += " AND " & strCampoLlave & " = " & strValorLlave & Chr(13)
            ElseIf intTipoLlave = CAMPO_ALFANUMERICO Then
                strSQL += " AND " & strCampoLlave & " = '" & strValorLlave & "'" & Chr(13)
            End If
            If strCondicion <> "" Then
                strSQL = strSQL & Chr(10) & " AND " & strCondicion
            End If

            ConexionSQL = New SqlConnection(Me.strCadenaDeConexionSQL)
            Dim ComandoSQL As SqlCommand
            ComandoSQL = New SqlCommand(strSQL, ConexionSQL)
            ConexionSQL.Open()

            Dim sdrConsulta As SqlDataReader = ComandoSQL.ExecuteReader

            If sdrConsulta.Read Then
                ReDim arrValoresCampos(intNumeroCampos - 1)

                For intCont = 0 To intNumeroCampos - 1
                    If IsDBNull(sdrConsulta(arrCampos(intCont).ToString)) Then
                        arrValoresCampos(intCont) = String.Empty
                    Else
                        arrValoresCampos(intCont) = sdrConsulta(arrCampos(intCont).ToString)
                    End If
                Next
                Retorna_Campos = True
            Else
                Retorna_Campos = False
            End If
            sdrConsulta.Close()
            sdrConsulta.Dispose()

        Catch ex As Exception
            Retorna_Campos = False
            strError = ex.Message.ToString()
        Finally
            If ConexionSQL.State() = ConnectionState.Open Then
                ConexionSQL.Close()
            End If
        End Try
    End Function

#End Region
End Class

