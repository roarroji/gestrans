﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Despachos
Imports Softtools.GesCarga.Negocio.Despachos

''' <summary>
''' Controlador <see cref="CumplidosController"/>
''' </summary>
<Authorize>
Public Class CumplidosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Cumplido) As Respuesta(Of IEnumerable(Of Cumplido))
        Return New LogicaCumplidos(CapaPersistenciaCumplidos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Cumplido) As Respuesta(Of Cumplido)
        Return New LogicaCumplidos(CapaPersistenciaCumplidos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Cumplido) As Respuesta(Of Long)
        Return New LogicaCumplidos(CapaPersistenciaCumplidos).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Cumplido) As Respuesta(Of Cumplido)
        Return New LogicaCumplidos(CapaPersistenciaCumplidos).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarTiempos")>
    Public Function ConsultarTiempos(ByVal entidad As Cumplido) As Respuesta(Of Cumplido)
        Return New LogicaCumplidos(CapaPersistenciaCumplidos).ConsultarTiempos(entidad)
    End Function

End Class
