﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Negocio.Despachos.Paqueteria
Imports Softtools.GesCarga.Entidades.Despachos.Remesa

''' <summary>
''' Controlador <see cref="DetalleDistribucionRemesasController"/>
''' </summary>
<Authorize>
Public Class DetalleDistribucionRemesasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleDistribucionRemesas) As Respuesta(Of IEnumerable(Of DetalleDistribucionRemesas))
        Return New LogicaDetalleDistribucionRemesas(CapaPersistenciaDetalleDistribucionRemesas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleDistribucionRemesas) As Respuesta(Of DetalleDistribucionRemesas)
        Return New LogicaDetalleDistribucionRemesas(CapaPersistenciaDetalleDistribucionRemesas).Obtener(filtro)
    End Function
    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleDistribucionRemesas) As Respuesta(Of Long)
        Return New LogicaDetalleDistribucionRemesas(CapaPersistenciaDetalleDistribucionRemesas).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleDistribucionRemesas) As Respuesta(Of Boolean)
        Return New LogicaDetalleDistribucionRemesas(CapaPersistenciaDetalleDistribucionRemesas).Anular(entidad)
    End Function

End Class
