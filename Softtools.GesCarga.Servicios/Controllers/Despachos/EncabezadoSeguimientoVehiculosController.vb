﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Despachos
Imports Softtools.GesCarga.Negocio.Despachos

''' <summary>
''' Controlador <see cref="EncabezadoSeguimientoVehiculosController"/>
''' </summary>
<Authorize>
Public Class EncabezadoSeguimientoVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EncabezadoSeguimientoVehiculos) As Respuesta(Of IEnumerable(Of EncabezadoSeguimientoVehiculos))
        Return New LogicaEncabezadoSeguimientoVehiculos(CapaPersistenciaEncabezadoSeguimientoVehiculos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EncabezadoSeguimientoVehiculos) As Respuesta(Of EncabezadoSeguimientoVehiculos)
        Return New LogicaEncabezadoSeguimientoVehiculos(CapaPersistenciaEncabezadoSeguimientoVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EncabezadoSeguimientoVehiculos) As Respuesta(Of Long)
        Return New LogicaEncabezadoSeguimientoVehiculos(CapaPersistenciaEncabezadoSeguimientoVehiculos).Guardar(entidad)
    End Function

End Class
