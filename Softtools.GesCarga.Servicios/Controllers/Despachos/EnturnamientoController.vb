﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Despachos
Imports Softtools.GesCarga.Negocio.Despachos

''' <summary>
''' Controlador <see cref="EnturnamientoController"/>
''' </summary>
<Authorize>
Public Class EnturnamientoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Enturnamiento) As Respuesta(Of IEnumerable(Of Enturnamiento))
        Return New LogicaEnturnamiento(CapaPersistenciaEnturnamiento).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Enturnamiento) As Respuesta(Of Enturnamiento)
        Return New LogicaEnturnamiento(CapaPersistenciaEnturnamiento).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Enturnamiento) As Respuesta(Of Long)
        Return New LogicaEnturnamiento(CapaPersistenciaEnturnamiento).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Enturnamiento) As Respuesta(Of Boolean)
        Return New LogicaEnturnamiento(CapaPersistenciaEnturnamiento).Anular(entidad)
    End Function
End Class
