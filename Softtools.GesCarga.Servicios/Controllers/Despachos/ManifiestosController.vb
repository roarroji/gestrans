﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Despachos
Imports Softtools.GesCarga.Negocio.Despachos

''' <summary>
''' Controlador <see cref="ManifiestosController"/>
''' </summary>
<Authorize>
Public Class ManifiestosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Manifiesto) As Respuesta(Of IEnumerable(Of Manifiesto))
        Return New LogicaManifiestos(CapaPersistenciaManifiestos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Manifiesto) As Respuesta(Of Manifiesto)
        Return New LogicaManifiestos(CapaPersistenciaManifiestos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Manifiesto) As Respuesta(Of Long)
        Return New LogicaManifiestos(CapaPersistenciaManifiestos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Manifiesto) As Respuesta(Of Manifiesto)
        Return New LogicaManifiestos(CapaPersistenciaManifiestos).Anular(entidad)
    End Function
End Class
