﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Despachos.Procesos
Imports Softtools.GesCarga.Negocio.Despachos

''' <summary>
''' Controlador <see cref="ReporteMinisterioController"/>
''' </summary>
'<Authorize>
Public Class ReporteMinisterioController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ReporteMinisterio) As Respuesta(Of IEnumerable(Of ReporteMinisterio))
        Return New LogicaReporteMinisterio(CapaPersistenciaReporteMinisterio).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ReporteMinisterio) As Respuesta(Of ReporteMinisterio)
        Return New LogicaReporteMinisterio(CapaPersistenciaReporteMinisterio).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("ReportarRNDC")>
    Public Function ReportarRNDC(ByVal filtro As ReporteMinisterio) As Respuesta(Of ReporteMinisterio)
        Return New LogicaReporteMinisterio(CapaPersistenciaReporteMinisterio).ReportarRNDC(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ReporteMinisterio) As Respuesta(Of Long)
        Return New LogicaReporteMinisterio(CapaPersistenciaReporteMinisterio).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ReporteMinisterio) As Respuesta(Of Boolean)
        Return New LogicaReporteMinisterio(CapaPersistenciaReporteMinisterio).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarRemesasSinManifiesto")>
    Public Function ConsultarRemesasSinManifiesto(ByVal filtro As ReporteMinisterio) As Respuesta(Of IEnumerable(Of ReporteMinisterio))
        Return New LogicaReporteMinisterio(CapaPersistenciaReporteMinisterio).ConsultarRemesasSinManifiesto(filtro)
    End Function

End Class
