﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Negocio.Facturacion

''' <summary>
''' Controlador <see cref="DetalleConceptosFacturasController"/>
''' </summary>

<Authorize>
Public Class DetalleConceptosFacturasController
    Inherits Base
    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleConceptosFacturas) As Respuesta(Of IEnumerable(Of DetalleConceptosFacturas))
        Return New LogicaDetalleConceptosFacturas(CapaPersistenciaDetalleConceptosFacturas).Consultar(filtro)
    End Function
End Class
