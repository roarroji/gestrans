﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Negocio.Facturacion

''' <summary>
''' Controlador <see cref="DetalleImpuestosFacturasController"/>
''' </summary>

<Authorize>
Public Class DetalleImpuestosFacturasController
    Inherits Base
    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleImpuestosFacturas) As Respuesta(Of IEnumerable(Of DetalleImpuestosFacturas))
        Return New LogicaDetalleImpuestosFacturas(CapaPersistenciaDetalleImpuestosFacturas).Consultar(filtro)
    End Function
End Class
