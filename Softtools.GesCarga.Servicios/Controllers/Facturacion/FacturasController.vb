﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Negocio.Facturacion

''' <summary>
''' Controlador <see cref="FacturasController"/>
''' </summary>
<Authorize>
Public Class FacturasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Facturas) As Respuesta(Of IEnumerable(Of Facturas))
        Return New LogicaFacturas(CapaPersistenciaFacturas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Facturas) As Respuesta(Of Facturas)
        Return New LogicaFacturas(CapaPersistenciaFacturas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Facturas) As Respuesta(Of Long)
        Return New LogicaFacturas(CapaPersistenciaFacturas).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Facturas) As Respuesta(Of Boolean)
        Return New LogicaFacturas(CapaPersistenciaFacturas).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ObtenerDatosFacturaElectronicaSaphety")>
    Public Function ObtenerDatosFacturaElectronicaSaphety(ByVal filtro As Facturas) As Respuesta(Of Facturas)
        Return New LogicaFacturas(CapaPersistenciaFacturas).ObtenerDatosFacturaElectronicaSaphety(filtro)
    End Function

    <HttpPost>
    <ActionName("GuardarFacturaElectronica")>
    Public Function GuardarFacturaElectronica(ByVal entidad As Facturas) As Respuesta(Of Long)
        Return New LogicaFacturas(CapaPersistenciaFacturas).GuardarFacturaElectronica(entidad)
    End Function

    <HttpPost>
    <ActionName("ObtenerDocumentoReporteSaphety")>
    Public Function ObtenerDocumentoReporteSaphety(ByVal filtro As Facturas) As Respuesta(Of Facturas)
        Return New LogicaFacturas(CapaPersistenciaFacturas).ObtenerDocumentoReporteSaphety(filtro)
    End Function

End Class
