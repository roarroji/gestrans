﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Negocio.Facturacion

''' <summary>
''' Controlador <see cref="FacturasOtrosConceptosController"/>
''' </summary>
<Authorize>
Public Class FacturasOtrosConceptosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Facturas) As Respuesta(Of IEnumerable(Of Facturas))
        Return New LogicaFacturasOtrosConceptos(CapaPersistenciaFacturasOtrosConceptos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Facturas) As Respuesta(Of Facturas)
        Return New LogicaFacturasOtrosConceptos(CapaPersistenciaFacturasOtrosConceptos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Facturas) As Respuesta(Of Long)
        Return New LogicaFacturasOtrosConceptos(CapaPersistenciaFacturasOtrosConceptos).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Facturas) As Respuesta(Of Boolean)
        Return New LogicaFacturasOtrosConceptos(CapaPersistenciaFacturasOtrosConceptos).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ObtenerDatosFacturaElectronicaSaphety")>
    Public Function ObtenerDatosFacturaElectronicaSaphety(ByVal filtro As Facturas) As Respuesta(Of Facturas)
        Return New LogicaFacturasOtrosConceptos(CapaPersistenciaFacturasOtrosConceptos).ObtenerDatosFacturaElectronicaSaphety(filtro)
    End Function

    <HttpPost>
    <ActionName("GuardarFacturaElectronica")>
    Public Function GuardarFacturaElectronica(ByVal entidad As Facturas) As Respuesta(Of Long)
        Return New LogicaFacturasOtrosConceptos(CapaPersistenciaFacturasOtrosConceptos).GuardarFacturaElectronica(entidad)
    End Function

    <HttpPost>
    <ActionName("ObtenerDocumentoReporteSaphety")>
    Public Function ObtenerDocumentoReporteSaphety(ByVal filtro As Facturas) As Respuesta(Of Facturas)
        Return New LogicaFacturasOtrosConceptos(CapaPersistenciaFacturasOtrosConceptos).ObtenerDocumentoReporteSaphety(filtro)
    End Function
End Class
