﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Facturacion
Imports Softtools.GesCarga.Negocio.Facturacion

<Authorize>
Public Class ReporteFacturaElectronicaController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ReporteFacturaElectronica) As Respuesta(Of IEnumerable(Of ReporteFacturaElectronica))
        Return New LogicaReporteFacturaElectronica(CapaPersistenciaReporteFacturaElectronica).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ReporteFacturaElectronica) As Respuesta(Of ReporteFacturaElectronica)
        Return New LogicaReporteFacturaElectronica(CapaPersistenciaReporteFacturaElectronica).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ReporteFacturaElectronica) As Respuesta(Of Long)
        Return New LogicaReporteFacturaElectronica(CapaPersistenciaReporteFacturaElectronica).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("ReportarDocumentosElectronicos")>
    Public Function ReportarDocumentosElectronicos(ByVal filtro As ReporteFacturaElectronica) As Respuesta(Of ReporteFacturaElectronica)
        Return New LogicaReporteFacturaElectronica(CapaPersistenciaReporteFacturaElectronica).ReportarDocumentosElectronicos(filtro)
    End Function

    <HttpPost>
    <ActionName("ReportarDocumentosSaphetyServicioWindows")>
    Public Function ProcesoReporteDocumentosElectrinicosSaphetySW(ByVal filtro As ReporteFacturaElectronica) As Respuesta(Of ReporteFacturaElectronica)
        Return New LogicaReporteFacturaElectronica(CapaPersistenciaReporteFacturaElectronica).ReportarDocumentosSaphetyServicioWindows(filtro)
    End Function

End Class
