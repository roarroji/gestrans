﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Fidelizacion.Documentos
Imports Softtools.GesCarga.Negocio.Fidelizacion.Documentos

<Authorize>
Public Class DetalleCausalesPuntosVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleCausalesPuntosVehiculos) As Respuesta(Of IEnumerable(Of DetalleCausalesPuntosVehiculos))
        Return New LogicaDetalleCausalesPuntosVehiculos(CapaPersistenciaDetalleCausalesPuntosVehiculos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleCausalesPuntosVehiculos) As Respuesta(Of DetalleCausalesPuntosVehiculos)
        Return New LogicaDetalleCausalesPuntosVehiculos(CapaPersistenciaDetalleCausalesPuntosVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleCausalesPuntosVehiculos) As Respuesta(Of Long)
        Return New LogicaDetalleCausalesPuntosVehiculos(CapaPersistenciaDetalleCausalesPuntosVehiculos).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleCausalesPuntosVehiculos) As Respuesta(Of Boolean)
        Return New LogicaDetalleCausalesPuntosVehiculos(CapaPersistenciaDetalleCausalesPuntosVehiculos).Anular(entidad)
    End Function
End Class
