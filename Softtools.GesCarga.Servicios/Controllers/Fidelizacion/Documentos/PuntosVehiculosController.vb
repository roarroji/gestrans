﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Fidelizacion.Documentos
Imports Softtools.GesCarga.Negocio.Fidelizacion.Documentos

<Authorize>
Public Class PuntosVehiculosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PuntosVehiculos) As Respuesta(Of IEnumerable(Of PuntosVehiculos))
        Return New LogicaPuntosVehiculos(CapaPersistenciaPuntosVehiculos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PuntosVehiculos) As Respuesta(Of PuntosVehiculos)
        Return New LogicaPuntosVehiculos(CapaPersistenciaPuntosVehiculos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PuntosVehiculos) As Respuesta(Of Long)
        Return New LogicaPuntosVehiculos(CapaPersistenciaPuntosVehiculos).Guardar(entidad)
    End Function
End Class
