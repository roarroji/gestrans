﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.FlotaPropia
Imports Softtools.GesCarga.Negocio.FlotaPropia

<Authorize>
Public Class PlanillasDespachosOtrasEmpresasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PlanillaDespachosOtrasEmpresas) As Respuesta(Of IEnumerable(Of PlanillaDespachosOtrasEmpresas))
        Return New LogicaPlanillasDespachosOtrasEmpresas(CapaPersistenciaPlanillasDespachosOtrasEmpresas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PlanillaDespachosOtrasEmpresas) As Respuesta(Of PlanillaDespachosOtrasEmpresas)
        Return New LogicaPlanillasDespachosOtrasEmpresas(CapaPersistenciaPlanillasDespachosOtrasEmpresas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PlanillaDespachosOtrasEmpresas) As Respuesta(Of Long)
        Return New LogicaPlanillasDespachosOtrasEmpresas(CapaPersistenciaPlanillasDespachosOtrasEmpresas).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As PlanillaDespachosOtrasEmpresas) As Respuesta(Of Boolean)
        Return New LogicaPlanillasDespachosOtrasEmpresas(CapaPersistenciaPlanillasDespachosOtrasEmpresas).Anular(entidad)
    End Function
End Class
