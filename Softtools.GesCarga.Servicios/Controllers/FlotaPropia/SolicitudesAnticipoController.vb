﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.FlotaPropia
Imports Softtools.GesCarga.Negocio.FlotaPropia

Namespace FlotaPropia
    Public Class SolicitudesAnticipoController
        Inherits Base
        <HttpPost>
        <ActionName("Consultar")>
        Public Function Consultar(ByVal filtro As EncabezadoSolicitudAnticipo) As Respuesta(Of IEnumerable(Of EncabezadoSolicitudAnticipo))
            Return New LogicaSolicitudesAnticipo(CapaPersistenciaSolicitudAnticipo).Consultar(filtro)
        End Function

        <HttpPost>
        <ActionName("Obtener")>
        Public Function Obtener(ByVal filtro As EncabezadoSolicitudAnticipo) As Respuesta(Of EncabezadoSolicitudAnticipo)
            Return New LogicaSolicitudesAnticipo(CapaPersistenciaSolicitudAnticipo).Obtener(filtro)
        End Function

        <HttpPost>
        <ActionName("Guardar")>
        Public Function Guardar(ByVal entidad As EncabezadoSolicitudAnticipo) As Respuesta(Of Long)
            Return New LogicaSolicitudesAnticipo(CapaPersistenciaSolicitudAnticipo).Guardar(entidad)
        End Function

        <HttpPost>
        <ActionName("Anular")>
        Public Function Anular(ByVal entidad As EncabezadoSolicitudAnticipo) As Respuesta(Of Boolean)
            Return New LogicaSolicitudesAnticipo(CapaPersistenciaSolicitudAnticipo).Anular(entidad)
        End Function



        <HttpPost>
        <ActionName("ConsultarListaDetalles")>
        Public Function ConsultarListaDetalles(ByVal entidad As DetalleSolicitudAnticipo) As Respuesta(Of IEnumerable(Of DetalleSolicitudAnticipo))
            Return New LogicaSolicitudesAnticipo(CapaPersistenciaSolicitudAnticipo).ConsultarListaDetalles(entidad)
        End Function
    End Class
End Namespace
