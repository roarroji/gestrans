﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Gesphone
Imports Softtools.GesCarga.Negocio.Gesphone

''' <summary>
''' Controlador <see cref="ItemListCargueDescarguesController"/>
''' </summary>
<Authorize>
Public Class ItemListCargueDescarguesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ItemListCargueDescargues) As Respuesta(Of IEnumerable(Of ItemListCargueDescargues))
        Return New LogicaItemListCargueDescargues(CapaPersistenciaItemListCargueDescargues).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ItemListCargueDescargues) As Respuesta(Of ItemListCargueDescargues)
        Return New LogicaItemListCargueDescargues(CapaPersistenciaItemListCargueDescargues).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ItemListCargueDescargues) As Respuesta(Of Long)
        Return New LogicaItemListCargueDescargues(CapaPersistenciaItemListCargueDescargues).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ItemListCargueDescargues) As Respuesta(Of Boolean)
        Return New LogicaItemListCargueDescargues(CapaPersistenciaItemListCargueDescargues).Anular(entidad)
    End Function
End Class
