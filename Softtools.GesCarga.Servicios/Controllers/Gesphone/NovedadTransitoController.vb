﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Gesphone
Imports Softtools.GesCarga.Negocio.Gesphone

''' <summary>
''' Controlador <see cref="NovedadTransitoController"/>
''' </summary>
<Authorize>
Public Class NovedadTransitoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As NovedadTransito) As Respuesta(Of IEnumerable(Of NovedadTransito))
        Return New LogicaNovedadTransito(CapaPersistenciaNovedadTransito).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As NovedadTransito) As Respuesta(Of NovedadTransito)
        Return New LogicaNovedadTransito(CapaPersistenciaNovedadTransito).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As NovedadTransito) As Respuesta(Of Long)
        Return New LogicaNovedadTransito(CapaPersistenciaNovedadTransito).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As NovedadTransito) As Respuesta(Of Boolean)
        Return New LogicaNovedadTransito(CapaPersistenciaNovedadTransito).Anular(entidad)
    End Function
End Class
