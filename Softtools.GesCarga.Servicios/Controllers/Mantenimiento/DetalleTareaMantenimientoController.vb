﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.Negocio.Mantenimiento

<Authorize>
Public Class DetalleTareaMantenimientoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleTareaMantenimiento) As Respuesta(Of IEnumerable(Of DetalleTareaMantenimiento))
        Return New LogicaDetalleTareaMantenimiento(CapaPersistenciaDetalleTareaMantenimiento).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleTareaMantenimiento) As Respuesta(Of DetalleTareaMantenimiento)
        Return New LogicaDetalleTareaMantenimiento(CapaPersistenciaDetalleTareaMantenimiento).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleTareaMantenimiento) As Respuesta(Of Long)
        Return New LogicaDetalleTareaMantenimiento(CapaPersistenciaDetalleTareaMantenimiento).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleTareaMantenimiento) As Respuesta(Of Boolean)
        Return New LogicaDetalleTareaMantenimiento(CapaPersistenciaDetalleTareaMantenimiento).Anular(entidad)
    End Function

End Class