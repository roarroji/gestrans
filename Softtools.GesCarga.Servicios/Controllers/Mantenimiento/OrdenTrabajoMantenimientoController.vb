﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Mantenimiento
Imports Softtools.GesCarga.Negocio.Mantenimiento

<Authorize>
Public Class OrdenTrabajoMantenimientoController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As OrdenTrabajoMantenimiento) As Respuesta(Of IEnumerable(Of OrdenTrabajoMantenimiento))
        Return New LogicaOrdenTrabajoMantenimiento(CapaPersistenciaOrdenTrabajoMantenimiento, CapaPersistenciaDetalleOrdenTrabajoMantenimiento).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As OrdenTrabajoMantenimiento) As Respuesta(Of OrdenTrabajoMantenimiento)
        Return New LogicaOrdenTrabajoMantenimiento(CapaPersistenciaOrdenTrabajoMantenimiento, CapaPersistenciaDetalleOrdenTrabajoMantenimiento).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As OrdenTrabajoMantenimiento) As Respuesta(Of Long)
        Return New LogicaOrdenTrabajoMantenimiento(CapaPersistenciaOrdenTrabajoMantenimiento, CapaPersistenciaDetalleOrdenTrabajoMantenimiento).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As OrdenTrabajoMantenimiento) As Respuesta(Of Boolean)
        Return New LogicaOrdenTrabajoMantenimiento(CapaPersistenciaOrdenTrabajoMantenimiento, CapaPersistenciaDetalleOrdenTrabajoMantenimiento).Anular(entidad)
    End Function
End Class