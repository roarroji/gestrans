﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.General
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Imports Softtools.GesCarga.Negocio.Paqueteria

<Authorize>
Public Class CargueMasivoRecoleccionesController
    Inherits Base

    <ActionName("GenerarPlantilla")>
    Public Function GenerarPlantilla(ByVal Filtro As Recolecciones) As Respuesta(Of Recolecciones)
        Return New LogicaCargueMasivoRecolecciones(CapaPersistenciaCargueMasivoRecolecciones).GenerarPlantilla(Filtro)
    End Function

    <HttpPost>
    <ActionName("InsertarMasivo")>
    Public Function InsertarMasivo(ByVal filtro As Recolecciones) As Respuesta(Of Long)
        Return New LogicaCargueMasivoRecolecciones(CapaPersistenciaCargueMasivoRecolecciones).InsertarMasivo(filtro)
    End Function


    <HttpPost>
    <ActionName("ConsultarTercerosIdentificacion")>
    Public Function ConsultarTercerosIdentificacion(ByVal filtro As Terceros) As Respuesta(Of IEnumerable(Of Terceros))
        Return New LogicaCargueMasivoRecolecciones(CapaPersistenciaCargueMasivoRecolecciones).ConsultarTercerosIdentificacion(filtro)
    End Function


    <HttpPost>
    <ActionName("ObtenerUltimoConsecutivoTercero")>
    Public Function ObtenerUltimoConsecutivoTercero(ByVal filtro As Terceros) As Respuesta(Of Terceros)
        Return New LogicaCargueMasivoRecolecciones(CapaPersistenciaCargueMasivoRecolecciones).ObtenerUltimoConsecutivoTercero(filtro)
    End Function


    <HttpPost>
    <ActionName("InsertarNuevosTerceros")>
    Public Function InsertarNuevosTerceros(ByVal filtro As Terceros) As Respuesta(Of Long)
        Return New LogicaCargueMasivoRecolecciones(CapaPersistenciaCargueMasivoRecolecciones).InsertarNuevosTerceros(filtro)
    End Function

    <HttpPost>
    <ActionName("ObtenerUltimaRecoleccion")>
    Public Function ObtenerUltimaRecoleccion(ByVal filtro As Recolecciones) As Respuesta(Of Recolecciones)
        Return New LogicaCargueMasivoRecolecciones(CapaPersistenciaCargueMasivoRecolecciones).ObtenerUltimaRecoleccion(filtro)
    End Function
End Class
