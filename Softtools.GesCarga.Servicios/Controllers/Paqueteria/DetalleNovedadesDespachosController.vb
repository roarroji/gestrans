﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Imports Softtools.GesCarga.Negocio.Despachos.Paqueteria

''' <summary>
''' Controlador <see cref="DetalleNovedadesDespachosController"/>
''' </summary>
<Authorize>
Public Class DetalleNovedadesDespachosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleNovedadesDespachos) As Respuesta(Of IEnumerable(Of DetalleNovedadesDespachos))
        Return New LogicaDetalleNovedadesDespachos(CapaPersistenciaDetalleNovedadesDespachos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleNovedadesDespachos) As Respuesta(Of DetalleNovedadesDespachos)
        Return New LogicaDetalleNovedadesDespachos(CapaPersistenciaDetalleNovedadesDespachos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleNovedadesDespachos) As Respuesta(Of Long)
        Return New LogicaDetalleNovedadesDespachos(CapaPersistenciaDetalleNovedadesDespachos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleNovedadesDespachos) As Respuesta(Of Boolean)
        Return New LogicaDetalleNovedadesDespachos(CapaPersistenciaDetalleNovedadesDespachos).Anular(entidad)
    End Function

End Class
