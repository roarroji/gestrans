﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Basico.Despachos
Imports Softtools.GesCarga.Negocio.Basico.Despachos

''' <summary>
''' Controlador <see cref="PrecintosController"/>
''' </summary>
<Authorize>
Public Class GuiasPreimpresasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As GuiaPreimpresa) As Respuesta(Of IEnumerable(Of GuiaPreimpresa))
        Return New LogicaGuiasPreimpresas(CapaPersistenciaGuiasPreimpresas).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As GuiaPreimpresa) As Respuesta(Of GuiaPreimpresa)
        Return New LogicaGuiasPreimpresas(CapaPersistenciaGuiasPreimpresas).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As GuiaPreimpresa) As Respuesta(Of Long)
        Return New LogicaGuiasPreimpresas(CapaPersistenciaGuiasPreimpresas).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("GuardarPrecintos")>
    Public Function GuardarPrecintos(ByVal entidad As GuiaPreimpresa) As Respuesta(Of GuiaPreimpresa)
        Return New LogicaGuiasPreimpresas(CapaPersistenciaGuiasPreimpresas).GuardarPrecintos(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As GuiaPreimpresa) As Respuesta(Of Boolean)
        Return New LogicaGuiasPreimpresas(CapaPersistenciaGuiasPreimpresas).Anular(entidad)
    End Function
    <ActionName("ConsultarUnicoPrecinto")>
    Public Function ConsultarUnicoPrecinto(ByVal filtro As DetalleGuiaPreimpresa) As Respuesta(Of IEnumerable(Of DetalleGuiaPreimpresa))
        Return New LogicaGuiasPreimpresas(CapaPersistenciaGuiasPreimpresas).ConsultarUnicoPrecinto(filtro)
    End Function

    <HttpPost>
    <ActionName("LiberarPrecinto")>
    Public Function LiberarPrecinto(ByVal entidad As GuiaPreimpresa) As Respuesta(Of Boolean)
        'Return New LogicaPrecintos(CapaPersistenciaPrecintos).LiberarPrecinto(entidad)
        Dim retorno As Respuesta(Of Boolean) = New LogicaGuiasPreimpresas(CapaPersistenciaGuiasPreimpresas).LiberarPrecinto(entidad)
        Return retorno
    End Function
    <HttpPost>
    <ActionName("ValidarPrecintoPlanillaPaqueteria")>
    Public Function ValidarPrecintoPlanillaPaqueteria(ByVal entidad As GuiaPreimpresa) As Respuesta(Of GuiaPreimpresa)
        Return New LogicaGuiasPreimpresas(CapaPersistenciaGuiasPreimpresas).ValidarPrecintoPlanillaPaqueteria(entidad)
    End Function


    <HttpPost>
    <ActionName("ActualizarResponsable")>
    Public Function ActualizarResponsable(ByVal entidad As GuiaPreimpresa) As Respuesta(Of Long)
        Return New LogicaGuiasPreimpresas(CapaPersistenciaGuiasPreimpresas).ActualizarResponsable(entidad)
    End Function
End Class
