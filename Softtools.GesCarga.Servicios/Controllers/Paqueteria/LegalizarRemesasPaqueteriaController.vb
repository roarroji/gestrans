﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Negocio.Paqueteria

<Authorize>
Public Class LegalizarRemesasPaqueteriaController
    Inherits Base
    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As LegalizarRemesasPaqueteria) As Respuesta(Of IEnumerable(Of LegalizarRemesasPaqueteria))
        Return New LogicaLegalizarRemesasPaqueteria(CapaPersistenciaLegalizarRemesasPaqueteria).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As LegalizarRemesasPaqueteria) As Respuesta(Of LegalizarRemesasPaqueteria)
        Return New LogicaLegalizarRemesasPaqueteria(CapaPersistenciaLegalizarRemesasPaqueteria).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As LegalizarRemesasPaqueteria) As Respuesta(Of Long)
        Return New LogicaLegalizarRemesasPaqueteria(CapaPersistenciaLegalizarRemesasPaqueteria).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("EliminarRemesa")>
    Public Function EliminarRemesa(ByVal entidad As LegalizarRemesasPaqueteria) As Respuesta(Of Boolean)
        Return New LogicaLegalizarRemesasPaqueteria(CapaPersistenciaLegalizarRemesasPaqueteria).EliminarRemesa(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarOtrasLegalizaciones")>
    Public Function ConsultarOtrasLegalizaciones(ByVal filtro As LegalizarRemesasPaqueteria) As Respuesta(Of IEnumerable(Of LegalizarRemesasPaqueteria))
        Return New LogicaLegalizarRemesasPaqueteria(CapaPersistenciaLegalizarRemesasPaqueteria).ConsultarOtrasLegalizaciones(filtro)
    End Function
End Class
