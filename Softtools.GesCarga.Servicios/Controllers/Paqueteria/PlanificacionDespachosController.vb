﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Negocio.Paqueteria

<Authorize>
Public Class PlanificacionDespachosController
    Inherits Base
    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PlanificacionDespachos) As Respuesta(Of IEnumerable(Of PlanificacionDespachos))
        Return New LogicaPlanificacionDespachos(CapaPersistenciaPlanificacionDespachos).Consultar(filtro)
    End Function
    <ActionName("ConsultarDetalleRemesasResumen")>
    Public Function ConsultarDetalleRemesasResumen(ByVal filtro As PlanificacionDespachos) As Respuesta(Of IEnumerable(Of PlanificacionDespachos))
        Return New LogicaPlanificacionDespachos(CapaPersistenciaPlanificacionDespachos).ConsultarDetalleRemesasResumen(filtro)
    End Function
    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PlanificacionDespachos) As Respuesta(Of PlanificacionDespachos)
        Return New LogicaPlanificacionDespachos(CapaPersistenciaPlanificacionDespachos).Obtener(filtro)
    End Function
    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PlanificacionDespachos) As Respuesta(Of Long)
        Return New LogicaPlanificacionDespachos(CapaPersistenciaPlanificacionDespachos).Guardar(entidad)
    End Function
End Class
