﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Negocio.Paqueteria
Imports Softtools.GesCarga.Negocio.Despachos
Imports Softtools.GesCarga.Entidades.Despachos

''' <summary>
''' Controlador <see cref="PlanillaGuiasController"/>
''' </summary>
<Authorize>
Public Class PlanillaGuiasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PlanillaGuias) As Respuesta(Of IEnumerable(Of PlanillaGuias))
        Return New LogicaPlanillaGuias(CapaPersistenciaPlanillaGuias).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PlanillaGuias) As Respuesta(Of PlanillaGuias)
        Return New LogicaPlanillaGuias(CapaPersistenciaPlanillaGuias).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PlanillaGuias) As Respuesta(Of Long)
        Return New LogicaPlanillaGuias(CapaPersistenciaPlanillaGuias).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As PlanillaGuias) As Respuesta(Of PlanillaGuias)
        Return New LogicaPlanillaGuias(CapaPersistenciaPlanillaGuias).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("EliminarGuia")>
    Public Function EliminarGuia(ByVal entidad As PlanillaGuias) As Respuesta(Of Boolean)
        Return New LogicaPlanillaGuias(CapaPersistenciaPlanillaGuias).EliminarGuia(entidad)
    End Function

    <HttpPost>
    <ActionName("EliminarRecoleccion")>
    Public Function EliminarRecoleccion(ByVal entidad As PlanillaGuias) As Respuesta(Of Boolean)
        Return New LogicaPlanillaGuias(CapaPersistenciaPlanillaGuias).EliminarRecoleccion(entidad)
    End Function

    <HttpPost>
    <ActionName("GenerarManifiesto")>
    Public Function GenerarManifiesto(ByVal entidad As Manifiesto) As Respuesta(Of Long)
        Return New LogicaManifiestos(CapaPersistenciaManifiestos).Guardar(entidad)
    End Function
End Class
