﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Paqueteria
Imports Softtools.GesCarga.Negocio.Paqueteria
Imports Softtools.GesCarga.Negocio.Despachos
Imports Softtools.GesCarga.Entidades.Despachos

<Authorize>
Public Class PlanillaPaqueteriaController
    Inherits Base
    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PlanillaPaqueteria) As Respuesta(Of IEnumerable(Of PlanillaPaqueteria))
        Return New LogicaPlanillaPaqueteria(CapaPersistenciaPlanillaPaqueteria).Consultar(filtro)
    End Function
    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PlanillaPaqueteria) As Respuesta(Of PlanillaPaqueteria)
        Return New LogicaPlanillaPaqueteria(CapaPersistenciaPlanillaPaqueteria).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PlanillaPaqueteria) As Respuesta(Of Long)
        Return New LogicaPlanillaPaqueteria(CapaPersistenciaPlanillaPaqueteria).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As PlanillaPaqueteria) As Respuesta(Of PlanillaPaqueteria)
        Return New LogicaPlanillaPaqueteria(CapaPersistenciaPlanillaPaqueteria).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("EliminarGuia")>
    Public Function EliminarGuia(ByVal entidad As PlanillaPaqueteria) As Respuesta(Of Boolean)
        Return New LogicaPlanillaPaqueteria(CapaPersistenciaPlanillaPaqueteria).EliminarGuia(entidad)
    End Function

    <HttpPost>
    <ActionName("EliminarRecoleccion")>
    Public Function EliminarRecoleccion(ByVal entidad As PlanillaPaqueteria) As Respuesta(Of Boolean)
        Return New LogicaPlanillaPaqueteria(CapaPersistenciaPlanillaPaqueteria).EliminarRecoleccion(entidad)
    End Function

    <HttpPost>
    <ActionName("GenerarManifiesto")>
    Public Function GenerarManifiesto(ByVal entidad As Manifiesto) As Respuesta(Of Long)
        Return New LogicaManifiestos(CapaPersistenciaManifiestos).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarPlanillasPendientesRecepcionar")>
    Public Function ConsultarPlanillasPendientesRecepcionar(ByVal filtro As PlanillaPaqueteria) As Respuesta(Of IEnumerable(Of PlanillaPaqueteria))
        Return New LogicaPlanillaPaqueteria(CapaPersistenciaPlanillaPaqueteria).ConsultarPlanillasPendientesRecepcionar(filtro)
    End Function
End Class
