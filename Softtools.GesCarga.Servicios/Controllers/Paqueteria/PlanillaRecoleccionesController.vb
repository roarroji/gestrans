﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Imports Softtools.GesCarga.Negocio.Despachos.Paqueteria

''' <summary>
''' Controlador <see cref="PlanillaRecoleccionesController"/>
''' </summary>
<Authorize>
Public Class PlanillaRecoleccionesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PlanillaRecolecciones) As Respuesta(Of IEnumerable(Of PlanillaRecolecciones))
        Return New LogicaPlanillaRecolecciones(CapaPersistenciaPlanillaRecolecciones).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PlanillaRecolecciones) As Respuesta(Of PlanillaRecolecciones)
        Return New LogicaPlanillaRecolecciones(CapaPersistenciaPlanillaRecolecciones).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PlanillaRecolecciones) As Respuesta(Of Long)
        Return New LogicaPlanillaRecolecciones(CapaPersistenciaPlanillaRecolecciones).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As PlanillaRecolecciones) As Respuesta(Of Boolean)
        Return New LogicaPlanillaRecolecciones(CapaPersistenciaPlanillaRecolecciones).Anular(entidad)
    End Function

End Class
