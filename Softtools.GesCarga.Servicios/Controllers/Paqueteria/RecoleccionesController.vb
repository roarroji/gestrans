﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Despachos.Paqueteria
Imports Softtools.GesCarga.Negocio.Despachos.Paqueteria

''' <summary>
''' Controlador <see cref="RecoleccionesController"/>
''' </summary>
<Authorize>
Public Class RecoleccionesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Recolecciones) As Respuesta(Of IEnumerable(Of Recolecciones))
        Return New LogicaRecolecciones(CapaPersistenciaRecolecciones).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarENCOE")>
    Public Function ConsultarENCOE(ByVal filtro As Recolecciones) As Respuesta(Of IEnumerable(Of Recolecciones))
        Return New LogicaRecolecciones(CapaPersistenciaRecolecciones).ConsultarENCOE(filtro)
    End Function

    <HttpPost>
    <ActionName("ConsultarRecoleccionesPorPlanillar")>
    Public Function ConsultarRecoleccionesPorPlanillar(ByVal filtro As Recolecciones) As Respuesta(Of IEnumerable(Of Recolecciones))
        Return New LogicaRecolecciones(CapaPersistenciaRecolecciones).ConsultarRecoleccionesPorPlanillar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Recolecciones) As Respuesta(Of Recolecciones)
        Return New LogicaRecolecciones(CapaPersistenciaRecolecciones).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Recolecciones) As Respuesta(Of Long)
        Return New LogicaRecolecciones(CapaPersistenciaRecolecciones).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As Recolecciones) As Respuesta(Of Boolean)
        Return New LogicaRecolecciones(CapaPersistenciaRecolecciones).Anular(entidad)
    End Function

    <ActionName("AsociarRecoleccionesPlanilla")>
    Public Function AsociarRecoleccionesPlanilla(ByVal entidad As Recolecciones) As Respuesta(Of Boolean)
        Return New LogicaRecolecciones(CapaPersistenciaRecolecciones).AsociarRecoleccionesPlanilla(entidad)
    End Function

    <ActionName("GuardarEntregaRecoleccion")>
    Public Function GuardarEntrega(ByVal entidad As Recolecciones) As Respuesta(Of Long)
        Return New LogicaRecolecciones(CapaPersistenciaRecolecciones).GuardarEntrega(entidad)
    End Function

    <HttpPost>
    <ActionName("Cancelar")>
    Public Function Cancelar(ByVal entidad As Recolecciones) As Respuesta(Of Boolean)
        Return New LogicaRecolecciones(CapaPersistenciaRecolecciones).Cancelar(entidad)
    End Function

End Class
