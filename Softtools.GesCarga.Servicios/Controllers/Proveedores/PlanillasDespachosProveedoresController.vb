﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Proveedores
Imports Softtools.GesCarga.Negocio.Proveedores

<Authorize>
Public Class PlanillasDespachosProveedoresController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PlanillasDespachosProveedores) As Respuesta(Of IEnumerable(Of PlanillasDespachosProveedores))
        Return New LogicaPlanillasDespachosProveedores(CapaPersistenciaPlanillasDespachosProveedores).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PlanillasDespachosProveedores) As Respuesta(Of PlanillasDespachosProveedores)
        Return New LogicaPlanillasDespachosProveedores(CapaPersistenciaPlanillasDespachosProveedores).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PlanillasDespachosProveedores) As Respuesta(Of Long)
        Return New LogicaPlanillasDespachosProveedores(CapaPersistenciaPlanillasDespachosProveedores).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As PlanillasDespachosProveedores) As Respuesta(Of Boolean)
        Return New LogicaPlanillasDespachosProveedores(CapaPersistenciaPlanillasDespachosProveedores).Anular(entidad)
    End Function
End Class
