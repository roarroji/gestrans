﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades


''' <summary>
''' Controlador <see cref="ServiciosPLEXAController"/>
''' </summary>

Public Class ServiciosPLEXAController
    Inherits Base

#Region "Facturación"

    <HttpPost>
    <ActionName("ConsultarListaPrecios")>
    Public Function ConsultarListaPrecios() As Respuesta(Of String)

        '-- Llamar Capa Negocio
        Return New Respuesta(Of String)

    End Function


#End Region

End Class
