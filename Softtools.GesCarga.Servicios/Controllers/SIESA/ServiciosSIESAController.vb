﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Negocio

<Authorize>
Public Class ServiciosSIESAController
    Inherits Base
    <HttpPost>
    <ActionName("ConsultarCajasSIESA")>
    Public Function Consultar(ByVal filtro As ServiciosENCOE) As List(Of ServiciosENCOE)
        Return New LogicaServiciosENCOE().Consultar_Cajas_SIESA(filtro)
    End Function

    '<HttpPost>
    '<ActionName("Obtener")>
    'Public Function Obtener(ByVal filtro As ServiciosENCOE) As Respuesta(Of ServiciosENCOE)
    'End Function

    '<HttpPost>
    '<ActionName("Guardar")>
    'Public Function Guardar(ByVal entidad As ServiciosENCOE) As Respuesta(Of Long)

    'End Function



End Class
