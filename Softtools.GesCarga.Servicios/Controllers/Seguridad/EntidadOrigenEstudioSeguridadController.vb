﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Seguridad
Imports Softtools.GesCarga.Negocio.Seguridad

''' <summary>
''' Controlador <see cref="EntidadOrigenEstudioSeguridadController"/>
''' </summary>
<Authorize>
Public Class EntidadOrigenEstudioSeguridadController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EntidadOrigenEstudioSeguridad) As Respuesta(Of IEnumerable(Of EntidadOrigenEstudioSeguridad))
        Return New LogicaEntidadOrigenEstudioSeguridad(CapaPersistenciaEntidadOrigenEstudioSeguridad).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EntidadOrigenEstudioSeguridad) As Respuesta(Of EntidadOrigenEstudioSeguridad)
        Return New LogicaEntidadOrigenEstudioSeguridad(CapaPersistenciaEntidadOrigenEstudioSeguridad).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EntidadOrigenEstudioSeguridad) As Respuesta(Of Long)
        Return New LogicaEntidadOrigenEstudioSeguridad(CapaPersistenciaEntidadOrigenEstudioSeguridad).Guardar(entidad)
    End Function

End Class
