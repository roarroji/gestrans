﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Negocio.Seguridad

''' <summary>
''' Controlador <see cref="EstudioSeguridadController"/>
''' </summary>
<Authorize>
Public Class EstudioSeguridadController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EstudioSeguridad) As Respuesta(Of IEnumerable(Of EstudioSeguridad))
        Return New LogicaEstudioSeguridad(CapaPersistenciaEstudioSeguridad).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EstudioSeguridad) As Respuesta(Of EstudioSeguridad)
        Return New LogicaEstudioSeguridad(CapaPersistenciaEstudioSeguridad).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EstudioSeguridad) As Respuesta(Of Long)
        Return New LogicaEstudioSeguridad(CapaPersistenciaEstudioSeguridad).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As EstudioSeguridad) As Respuesta(Of Boolean)
        Return New LogicaEstudioSeguridad(CapaPersistenciaEstudioSeguridad).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("Autorizar")>
    Public Function Autorizar(ByVal entidad As EstudioSeguridad) As Respuesta(Of Boolean)
        Return New LogicaEstudioSeguridad(CapaPersistenciaEstudioSeguridad).Autorizar(entidad)
    End Function

    <HttpPost>
    <ActionName("Rechazar")>
    Public Function Rechazar(ByVal entidad As EstudioSeguridad) As Respuesta(Of Boolean)
        Return New LogicaEstudioSeguridad(CapaPersistenciaEstudioSeguridad).Rechazar(entidad)
    End Function

End Class
