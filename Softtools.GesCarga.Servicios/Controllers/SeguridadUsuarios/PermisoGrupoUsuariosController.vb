﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Negocio.Seguridad
Imports Softtools.GesCarga.Entidades.Utilitarios

''' <summary>
''' Controlador <see cref="PermisoGrupoUsuariosController"/>
''' </summary>
<Authorize>
Public Class PermisoGrupoUsuariosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As PermisoGrupoUsuarios) As Respuesta(Of IEnumerable(Of PermisoGrupoUsuarios))
        Return New LogicaPermisoGrupoUsuarios(CapaPersistenciaPermisoGrupoUsuarios).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As PermisoGrupoUsuarios) As Respuesta(Of PermisoGrupoUsuarios)
        Return New LogicaPermisoGrupoUsuarios(CapaPersistenciaPermisoGrupoUsuarios).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As PermisoGrupoUsuarios) As Respuesta(Of Long)
        Return New LogicaPermisoGrupoUsuarios(CapaPersistenciaPermisoGrupoUsuarios).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("ConsultarModulo")>
    Public Function ConsultarModulo(ByVal filtro As PermisoGrupoUsuarios) As Respuesta(Of IEnumerable(Of ModuloAplicaciones))
        Return New LogicaPermisoGrupoUsuarios(CapaPersistenciaPermisoGrupoUsuarios).ConsultarModulo(filtro.CodigoEmpresa, filtro.Codigo)
    End Function
    <ActionName("GenerarPlanitilla")>
    Public Function GenerarPlanitilla(ByVal Filtro As PermisoGrupoUsuarios) As Respuesta(Of PermisoGrupoUsuarios)
        Return New LogicaPermisoGrupoUsuarios(CapaPersistenciaPermisoGrupoUsuarios).GenerarPlanitilla(Filtro)
    End Function
    <ActionName("GuardarAsigancionPerfilesNotificaciones")>
    Public Function GuardarAsigancionPerfilesNotificaciones(ByVal Filtro As NotificacionGrupoUsuarios) As Respuesta(Of Long)
        Return New LogicaPermisoGrupoUsuarios(CapaPersistenciaPermisoGrupoUsuarios).GuardarAsigancionPerfilesNotificaciones(Filtro)
    End Function

    <ActionName("ConsultarAsigancionPerfilesNotificaciones")>
    Public Function ConsultarAsigancionPerfilesNotificaciones(ByVal Filtro As NotificacionGrupoUsuarios) As Respuesta(Of IEnumerable(Of DetalleGrupoUsuariosNotificaciones))
        Return New LogicaPermisoGrupoUsuarios(CapaPersistenciaPermisoGrupoUsuarios).ConsultarAsigancionPerfilesNotificaciones(Filtro)
    End Function
End Class
