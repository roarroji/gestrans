﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Negocio.Seguridad

''' <summary>
''' Controlador <see cref="UsuarioGrupoUsuariosController"/>
''' </summary>
<Authorize>
Public Class UsuarioGrupoUsuariosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As UsuarioGrupoUsuarios) As Respuesta(Of IEnumerable(Of UsuarioGrupoUsuarios))
        Return New LogicaUsuarioGrupoUsuarios(CapaPersistenciaUsuarioGrupoUsuarios).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As UsuarioGrupoUsuarios) As Respuesta(Of UsuarioGrupoUsuarios)
        Return New LogicaUsuarioGrupoUsuarios(CapaPersistenciaUsuarioGrupoUsuarios).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As UsuarioGrupoUsuarios) As Respuesta(Of Long)
        Return New LogicaUsuarioGrupoUsuarios(CapaPersistenciaUsuarioGrupoUsuarios).Guardar(entidad)
    End Function


End Class
