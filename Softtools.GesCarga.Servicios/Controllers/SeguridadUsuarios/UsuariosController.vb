﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.SeguridadUsuarios
Imports Softtools.GesCarga.Negocio.Aplicativo.SeguridadUsuarios

<Authorize>
Public Class UsuariosController
    Inherits Base

    ''' <summary>
    ''' Obtiene una lista de usuarios registrados
    ''' </summary>
    ''' <param name="filtro">Filtros de búsqueda</param>
    ''' <returns>Listado de tipo <see cref="Usuarios"/></returns>
    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As Usuarios) As Respuesta(Of IEnumerable(Of Usuarios))
        Return New LogicaUsuarios(CapaPersistenciaUsuarios).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As Usuarios) As Respuesta(Of Usuarios)
        Return New LogicaUsuarios(CapaPersistenciaUsuarios).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Validar")>
    Public Function Validar(ByVal filtro As Usuarios) As Respuesta(Of Usuarios)
        Return New LogicaUsuarios(CapaPersistenciaUsuarios).Validar(filtro.CodigoEmpresa, filtro.CodigoUsuario, filtro.Clave, filtro.Token)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As Usuarios) As Respuesta(Of Long)
        Return New LogicaUsuarios(CapaPersistenciaUsuarios).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarUsuarioGrupoSeguridades")>
    Public Function ConsultarUsuarioGrupoSeguridades(ByVal filtro As Usuarios) As Respuesta(Of IEnumerable(Of UsuarioGrupoUsuarios))
        Return New LogicaUsuarios(CapaPersistenciaUsuarios).ConsultarUsuarioGrupoSeguridades(filtro)
    End Function

    <HttpPost>
    <ActionName("GuardarUsuarioGrupoSeguridades")>
    Public Function GuardarUsuarioGrupoSeguridades(ByVal detalle As IEnumerable(Of GrupoUsuarios)) As Respuesta(Of Long)
        Return New LogicaUsuarios(CapaPersistenciaUsuarios).GuardarUsuarioGrupoSeguridades(detalle)
    End Function

    '<HttpPost>
    '<ActionName("ConsultarUsuarioHorarios")>
    'Public Function ConsultarUsuarioHorarios(ByVal filtro As UsuarioHorario) As Respuesta(Of IEnumerable(Of UsuarioHorario))
    '    Return New LogicaUsuario(CapaPersistenciaUsuario).ConsultarHorario(filtro)
    'End Function

    '<HttpPost>
    '<ActionName("GuardarUsuarioHorarios")>
    'Public Function GuardarUsuarioHorarios(ByVal detalle As IEnumerable(Of UsuarioHorario)) As Respuesta(Of Long)
    '    Return New LogicaUsuario(CapaPersistenciaUsuario).GuardarHorario(detalle)
    'End Function

    <HttpPost>
    <ActionName("ConsultarUsuarioOficinas")>
    Public Function ConsultarUsuarioOficinas(ByVal filtro As UsuarioOficinas) As Respuesta(Of IEnumerable(Of UsuarioOficinas))
        Return New LogicaUsuarios(CapaPersistenciaUsuarios).ConsultarUsuarioOficinas(filtro)
    End Function

    <HttpPost>
    <ActionName("GuardarUsuarioOficinas")>
    Public Function GuardarUsuarioOficinas(ByVal detalle As IEnumerable(Of UsuarioOficinas)) As Respuesta(Of Long)
        Return New LogicaUsuarios(CapaPersistenciaUsuarios).GuardarUsuarioOficinas(detalle)
    End Function

    <HttpPost>
    <ActionName("CerrarSesion")>
    Public Function CerrarSesion(ByVal filtro As Usuarios) As Respuesta(Of Usuarios)
        Return New LogicaUsuarios(CapaPersistenciaUsuarios).CerrarSesion(filtro.CodigoEmpresa, filtro.Codigo)
    End Function

    <HttpPost>
    <ActionName("RegistrarActividad")>
    Public Function RegistrarActividad(ByVal entidad As Usuarios) As Respuesta(Of Boolean)
        Return New LogicaUsuarios(CapaPersistenciaUsuarios).RegistrarActividad(entidad)
    End Function

End Class