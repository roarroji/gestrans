﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Negocio.ServicioCliente

<Authorize>
Public Class CerrarOrdenServicioController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As CerrarOrdenServicio) As Respuesta(Of IEnumerable(Of CerrarOrdenServicio))
        Return New LogicaCerrarOrdenServicio(CapaPersistenciaCerrarOrdenServicio).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As CerrarOrdenServicio) As Respuesta(Of CerrarOrdenServicio)
        Return New LogicaCerrarOrdenServicio(CapaPersistenciaCerrarOrdenServicio).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As CerrarOrdenServicio) As Respuesta(Of Long)
        Return New LogicaCerrarOrdenServicio(CapaPersistenciaCerrarOrdenServicio).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("CerrarOrdenServicio")>
    Public Function CerrarOrdenServicio(ByVal entidad As CerrarOrdenServicio) As Respuesta(Of Boolean)
        Return New LogicaCerrarOrdenServicio(CapaPersistenciaCerrarOrdenServicio).CerrarOrdenServicio(entidad)
    End Function

End Class
