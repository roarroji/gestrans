﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Negocio.ServicioCliente

''' <summary>
''' Controlador <see cref="DetalleConceptoSolicitudOrdenServiciosController"/>
''' </summary>
<Authorize>
Public Class DetalleConceptoSolicitudOrdenServiciosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleConceptoSolicitudOrdenServicios) As Respuesta(Of IEnumerable(Of DetalleConceptoSolicitudOrdenServicios))
        Return New LogicaDetalleConceptoSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleConceptoSolicitudOrdenServicios) As Respuesta(Of DetalleConceptoSolicitudOrdenServicios)
        Return New LogicaDetalleConceptoSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleConceptoSolicitudOrdenServicios) As Respuesta(Of Long)
        Return New LogicaDetalleConceptoSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleConceptoSolicitudOrdenServicios) As Respuesta(Of Boolean)
        Return New LogicaDetalleConceptoSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Anular(entidad)
    End Function
End Class
