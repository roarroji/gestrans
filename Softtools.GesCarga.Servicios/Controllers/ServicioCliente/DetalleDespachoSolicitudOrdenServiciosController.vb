﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Negocio.ServicioCliente

''' <summary>
''' Controlador <see cref="DetalleDespachoSolicitudOrdenServiciosController"/>
''' </summary>
<Authorize>
Public Class DetalleDespachoSolicitudOrdenServiciosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleDespachoSolicitudOrdenServicios) As Respuesta(Of IEnumerable(Of DetalleDespachoSolicitudOrdenServicios))
        Return New LogicaDetalleDespachoSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleDespachoSolicitudOrdenServicios) As Respuesta(Of DetalleDespachoSolicitudOrdenServicios)
        Return New LogicaDetalleDespachoSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleDespachoSolicitudOrdenServicios) As Respuesta(Of Long)
        Return New LogicaDetalleDespachoSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleDespachoSolicitudOrdenServicios) As Respuesta(Of Boolean)
        Return New LogicaDetalleDespachoSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Anular(entidad)
    End Function
End Class
