﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Negocio.ServicioCliente

''' <summary>
''' Controlador <see cref="EncabezadoProgramacionOrdenServiciosController"/>
''' </summary>
<Authorize>
Public Class EncabezadoProgramacionOrdenServiciosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EncabezadoProgramacionOrdenServicios) As Respuesta(Of IEnumerable(Of EncabezadoProgramacionOrdenServicios))
        Return New LogicaEncabezadoProgramacionOrdenServicios(CapaPersistenciaEncabezadoProgramacionOrdenServicios).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EncabezadoProgramacionOrdenServicios) As Respuesta(Of EncabezadoProgramacionOrdenServicios)
        Return New LogicaEncabezadoProgramacionOrdenServicios(CapaPersistenciaEncabezadoProgramacionOrdenServicios).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EncabezadoProgramacionOrdenServicios) As Respuesta(Of Long)
        Return New LogicaEncabezadoProgramacionOrdenServicios(CapaPersistenciaEncabezadoProgramacionOrdenServicios).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As EncabezadoProgramacionOrdenServicios) As Respuesta(Of EncabezadoProgramacionOrdenServicios)
        Return New LogicaEncabezadoProgramacionOrdenServicios(CapaPersistenciaEncabezadoProgramacionOrdenServicios).Anular(entidad)
    End Function
    <HttpPost>
    <ActionName("EliminarDetalle")>
    Public Function Anular(ByVal entidad As DetalleProgramacionOrdenServicios) As Respuesta(Of Long)
        Return New LogicaEncabezadoProgramacionOrdenServicios(CapaPersistenciaEncabezadoProgramacionOrdenServicios).EliminarDetalle(entidad)
    End Function
End Class
