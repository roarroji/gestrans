﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Negocio.ServicioCliente

''' <summary>
''' Controlador <see cref="EncabezadoSolicitudOrdenServiciosController"/>
''' </summary>
<Authorize>
Public Class EncabezadoSolicitudOrdenServiciosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EncabezadoSolicitudOrdenServicios) As Respuesta(Of IEnumerable(Of EncabezadoSolicitudOrdenServicios))
        Return New LogicaEncabezadoSolicitudOrdenServicios(CapaPersistenciaEncabezadoSolicitudOrdenServicios, CapaPersistenciaDetalleSolicitudOrdenServicios, CapaPersistenciaDetalleDespachoSolicitudOrdenServicios).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EncabezadoSolicitudOrdenServicios) As Respuesta(Of EncabezadoSolicitudOrdenServicios)
        Return New LogicaEncabezadoSolicitudOrdenServicios(CapaPersistenciaEncabezadoSolicitudOrdenServicios, CapaPersistenciaDetalleSolicitudOrdenServicios, CapaPersistenciaDetalleDespachoSolicitudOrdenServicios).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EncabezadoSolicitudOrdenServicios) As Respuesta(Of Long)
        Return New LogicaEncabezadoSolicitudOrdenServicios(CapaPersistenciaEncabezadoSolicitudOrdenServicios, CapaPersistenciaDetalleSolicitudOrdenServicios, CapaPersistenciaDetalleDespachoSolicitudOrdenServicios).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As EncabezadoSolicitudOrdenServicios) As Respuesta(Of EncabezadoSolicitudOrdenServicios)
        Return New LogicaEncabezadoSolicitudOrdenServicios(CapaPersistenciaEncabezadoSolicitudOrdenServicios, CapaPersistenciaDetalleSolicitudOrdenServicios, CapaPersistenciaDetalleDespachoSolicitudOrdenServicios).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ObtenerInformacionControlCupo")>
    Public Function ObtenerInformacionControlCupo(ByVal filtro As EncabezadoSolicitudOrdenServicios) As Respuesta(Of EncabezadoSolicitudOrdenServicios)
        Return New LogicaEncabezadoSolicitudOrdenServicios(CapaPersistenciaEncabezadoSolicitudOrdenServicios, CapaPersistenciaDetalleSolicitudOrdenServicios, CapaPersistenciaDetalleDespachoSolicitudOrdenServicios).ObtenerInformacionControlCupo(filtro)
    End Function

    <HttpPost>
    <ActionName("ObtenerOrdenServicioProgramacion")>
    Public Function ObtenerOrdenServicioProgramacion(ByVal filtro As EncabezadoSolicitudOrdenServicios) As Respuesta(Of EncabezadoSolicitudOrdenServicios)
        Return New LogicaEncabezadoSolicitudOrdenServicios(CapaPersistenciaEncabezadoSolicitudOrdenServicios, CapaPersistenciaDetalleSolicitudOrdenServicios, CapaPersistenciaDetalleDespachoSolicitudOrdenServicios).ObtenerOrdenServicioProgramacion(filtro)
    End Function
End Class
