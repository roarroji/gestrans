﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.ServicioCliente
Imports Softtools.GesCarga.Negocio.ServicioCliente

''' <summary>
''' Controlador <see cref="ImportacionExportacionSolicitudOrdenServiciosController"/>
''' </summary>
<Authorize>
Public Class ImportacionExportacionSolicitudOrdenServiciosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ImportacionExportacionSolicitudOrdenServicios) As Respuesta(Of IEnumerable(Of ImportacionExportacionSolicitudOrdenServicios))
        Return New LogicaImportacionExportacionSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ImportacionExportacionSolicitudOrdenServicios) As Respuesta(Of ImportacionExportacionSolicitudOrdenServicios)
        Return New LogicaImportacionExportacionSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ImportacionExportacionSolicitudOrdenServicios) As Respuesta(Of Long)
        Return New LogicaImportacionExportacionSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As ImportacionExportacionSolicitudOrdenServicios) As Respuesta(Of Boolean)
        Return New LogicaImportacionExportacionSolicitudOrdenServicios(CapaPersistenciaEncabezadoDocumentoComprobantes).Anular(entidad)
    End Function
End Class
