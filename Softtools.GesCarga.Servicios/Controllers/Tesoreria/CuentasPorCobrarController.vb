﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Negocio.Tesoreria

<Authorize>
Public Class CuentasPorCobrarController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As CuentasPorCobrar) As Respuesta(Of IEnumerable(Of CuentasPorCobrar))
        Return New LogicaCuentasPorCobrar(CapaPersistenciaCuentasPorCobrar).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As CuentasPorCobrar) As Respuesta(Of CuentasPorCobrar)
        Return New LogicaCuentasPorCobrar(CapaPersistenciaCuentasPorCobrar).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As CuentasPorCobrar) As Respuesta(Of Long)
        Return New LogicaCuentasPorCobrar(CapaPersistenciaCuentasPorCobrar).Guardar(entidad)
    End Function

    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As CuentasPorCobrar) As Respuesta(Of CuentasPorCobrar)
        Return New LogicaCuentasPorCobrar(CapaPersistenciaCuentasPorCobrar).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarDetalleObservaciones")>
    Public Function ConsultarDetalleObservaciones(ByVal entidad As CuentasPorCobrar) As Respuesta(Of CuentasPorCobrar)
        Return New LogicaCuentasPorCobrar(CapaPersistenciaCuentasPorCobrar).ConsultarDetalleObservaciones(entidad)
    End Function

    <HttpPost>
    <ActionName("GuardarDetalleObservaciones")>
    Public Function GuardarDetalleObservaciones(ByVal entidad As CuentasPorCobrar) As Respuesta(Of Long)
        Return New LogicaCuentasPorCobrar(CapaPersistenciaCuentasPorCobrar).GuardarDetalleObservaciones(entidad)
    End Function

End Class
