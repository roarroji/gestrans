﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Negocio.Tesoreria


<Authorize>
Public Class DetalleDocumentoCausacionesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleDocumentoCausaciones) As Respuesta(Of IEnumerable(Of DetalleDocumentoCausaciones))
        Return New LogicaDetalleDocumentoCausaciones(CapaPersistenciaDetalleDocumentoCausaciones).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleDocumentoCausaciones) As Respuesta(Of DetalleDocumentoCausaciones)
        Return New LogicaDetalleDocumentoCausaciones(CapaPersistenciaDetalleDocumentoCausaciones).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleDocumentoCausaciones) As Respuesta(Of Long)
        Return New LogicaDetalleDocumentoCausaciones(CapaPersistenciaDetalleDocumentoCausaciones).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleDocumentoCausaciones) As Respuesta(Of Boolean)
        Return New LogicaDetalleDocumentoCausaciones(CapaPersistenciaDetalleDocumentoCausaciones).Anular(entidad)
    End Function
End Class
