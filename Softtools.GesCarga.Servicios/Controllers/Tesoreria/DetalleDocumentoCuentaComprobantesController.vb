﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Negocio.Tesoreria

''' <summary>
''' Controlador <see cref="DetalleDocumentoCuentaComprobantesController"/>
''' </summary>
<Authorize>
Public Class DetalleDocumentoCuentaComprobantesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As DetalleDocumentoCuentaComprobantes) As Respuesta(Of IEnumerable(Of DetalleDocumentoCuentaComprobantes))
        Return New LogicaDetalleDocumentoCuentaComprobantes(CapaPersistenciaDetalleDocumentoCuentaComprobantes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As DetalleDocumentoCuentaComprobantes) As Respuesta(Of DetalleDocumentoCuentaComprobantes)
        Return New LogicaDetalleDocumentoCuentaComprobantes(CapaPersistenciaDetalleDocumentoCuentaComprobantes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As DetalleDocumentoCuentaComprobantes) As Respuesta(Of Long)
        Return New LogicaDetalleDocumentoCuentaComprobantes(CapaPersistenciaDetalleDocumentoCuentaComprobantes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As DetalleDocumentoCuentaComprobantes) As Respuesta(Of Boolean)
        Return New LogicaDetalleDocumentoCuentaComprobantes(CapaPersistenciaDetalleDocumentoCuentaComprobantes).Anular(entidad)
    End Function
End Class
