﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Negocio.Tesoreria


<Authorize>
Public Class EncabezadoDocumentoCausacionesController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EncabezadoDocumentoCausaciones) As Respuesta(Of IEnumerable(Of EncabezadoDocumentoCausaciones))
        Return New LogicaEncabezadoDocumentoCausaciones(CapaPersistenciaEncabezadoDocumentoCausaciones, CapaPersistenciaDetalleDocumentoCausaciones).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EncabezadoDocumentoCausaciones) As Respuesta(Of EncabezadoDocumentoCausaciones)
        Return New LogicaEncabezadoDocumentoCausaciones(CapaPersistenciaEncabezadoDocumentoCausaciones, CapaPersistenciaDetalleDocumentoCausaciones).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EncabezadoDocumentoCausaciones) As Respuesta(Of Long)
        Return New LogicaEncabezadoDocumentoCausaciones(CapaPersistenciaEncabezadoDocumentoCausaciones, CapaPersistenciaDetalleDocumentoCausaciones).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As EncabezadoDocumentoCausaciones) As Respuesta(Of Boolean)
        Return New LogicaEncabezadoDocumentoCausaciones(CapaPersistenciaEncabezadoDocumentoCausaciones, CapaPersistenciaDetalleDocumentoCausaciones).Anular(entidad)
    End Function
End Class
