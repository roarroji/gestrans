﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Tesoreria
Imports Softtools.GesCarga.Negocio.Tesoreria

''' <summary>
''' Controlador <see cref="EncabezadoDocumentoCuentasController"/>
''' </summary>
<Authorize>
Public Class EncabezadoDocumentoCuentasController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EncabezadoDocumentoCuentas) As Respuesta(Of IEnumerable(Of EncabezadoDocumentoCuentas))
        Return New LogicaEncabezadoDocumentoCuentas(CapaPersistenciaEncabezadoDocumentoCuentas, CapaPersistenciaDetalleDocumentoCuentaComprobantes).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EncabezadoDocumentoCuentas) As Respuesta(Of EncabezadoDocumentoCuentas)
        Return New LogicaEncabezadoDocumentoCuentas(CapaPersistenciaEncabezadoDocumentoCuentas, CapaPersistenciaDetalleDocumentoCuentaComprobantes).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EncabezadoDocumentoCuentas) As Respuesta(Of Long)
        Return New LogicaEncabezadoDocumentoCuentas(CapaPersistenciaEncabezadoDocumentoCuentas, CapaPersistenciaDetalleDocumentoCuentaComprobantes).Guardar(entidad)
    End Function
    <HttpPost>
    <ActionName("Anular")>
    Public Function Anular(ByVal entidad As EncabezadoDocumentoCuentas) As Respuesta(Of Boolean)
        Return New LogicaEncabezadoDocumentoCuentas(CapaPersistenciaEncabezadoDocumentoCuentas, CapaPersistenciaDetalleDocumentoCuentaComprobantes).Anular(entidad)
    End Function

    <HttpPost>
    <ActionName("ConsultarCuentasconEgresos")>
    Public Function ConsultarCuentasconEgresos(ByVal filtro As EncabezadoDocumentoCuentas) As Respuesta(Of IEnumerable(Of EncabezadoDocumentoCuentas))
        Return New LogicaEncabezadoDocumentoCuentas(CapaPersistenciaEncabezadoDocumentoCuentas, CapaPersistenciaDetalleDocumentoCuentaComprobantes).ConsultarCuentasconEgresos(filtro)
    End Function


    <HttpPost>
    <ActionName("ConsultarDetalleCruceDocumentos")>
    Public Function ConsultarDetalleCruceDocumentos(ByVal filtro As EncabezadoDocumentoCuentas) As Respuesta(Of IEnumerable(Of DetalleCruceDocumentoCuenta))
        Return New LogicaEncabezadoDocumentoCuentas(CapaPersistenciaEncabezadoDocumentoCuentas, CapaPersistenciaDetalleDocumentoCuentaComprobantes).ConsultarDetalleCruceDocumentos(filtro)
    End Function
End Class
