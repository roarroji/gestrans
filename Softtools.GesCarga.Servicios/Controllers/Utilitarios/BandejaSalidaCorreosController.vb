﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Utilitarios
Imports Softtools.GesCarga.Negocio.Utilitarios

<Authorize>
Public Class BandejaSalidaCorreosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As BandejaSalidaCorreos) As Respuesta(Of IEnumerable(Of BandejaSalidaCorreos))
        Return New LogicaBandejaSalidaCorreos(CapaPersistenciaBandejaSalidaCorreos).Consultar(filtro)
    End Function


End Class