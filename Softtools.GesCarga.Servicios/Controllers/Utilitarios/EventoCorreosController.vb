﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Utilitarios
Imports Softtools.GesCarga.Negocio.Utilitarios

<Authorize>
Public Class EventoCorreosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As EventoCorreos) As Respuesta(Of IEnumerable(Of EventoCorreos))
        Return New LogicaEventoCorreos(CapaPersistenciaEventoCorreos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As EventoCorreos) As Respuesta(Of EventoCorreos)
        Return New LogicaEventoCorreos(CapaPersistenciaEventoCorreos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As EventoCorreos) As Respuesta(Of Long)
        Return New LogicaEventoCorreos(CapaPersistenciaEventoCorreos).Guardar(entidad)
    End Function

End Class