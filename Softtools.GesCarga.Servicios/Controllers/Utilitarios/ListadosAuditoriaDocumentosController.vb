﻿Imports System.Web.Http
Imports Softtools.GesCarga.Entidades
Imports Softtools.GesCarga.Entidades.Utilitarios
Imports Softtools.GesCarga.Negocio.Utilitarios

<Authorize>
Public Class ListadosAuditoriaDocumentosController
    Inherits Base

    <HttpPost>
    <ActionName("Consultar")>
    Public Function Consultar(ByVal filtro As ListadosAuditoriaDocumentos) As Respuesta(Of IEnumerable(Of ListadosAuditoriaDocumentos))
        Return New LogicaListadosAuditoriaDocumentos(CapaPersistenciaListadosAuditoriaDocumentos).Consultar(filtro)
    End Function

    <HttpPost>
    <ActionName("Obtener")>
    Public Function Obtener(ByVal filtro As ListadosAuditoriaDocumentos) As Respuesta(Of ListadosAuditoriaDocumentos)
        Return New LogicaListadosAuditoriaDocumentos(CapaPersistenciaListadosAuditoriaDocumentos).Obtener(filtro)
    End Function

    <HttpPost>
    <ActionName("Guardar")>
    Public Function Guardar(ByVal entidad As ListadosAuditoriaDocumentos) As Respuesta(Of Long)
        Return New LogicaListadosAuditoriaDocumentos(CapaPersistenciaListadosAuditoriaDocumentos).Guardar(entidad)
    End Function

End Class