﻿Public Class estViajes

    Public numNumeroViaje As Integer
    Public strPuntoGestion As String
    Public Intestado As Integer
    Public strVehiculo As String
    Public strsemiremolque As String

    Public strnumeroGuia As String
    Public numTurno As Integer
    Public strEstadoInspeccion As String
    Public strCometarioInspeccion As String
    Public numNumeroInspeccion As Integer

    Public strTransportador As String
    Public strCedulaConductor As String
    Public strCelularConductor As String
    Public strGenerador As String
    Public strCliente As String
    Public strDespachadoA As String

    Public strOrigen As String
    Public strDestino As String
    Public strProducto As String
    Public dblAPI As Integer
    Public dblVolumenGOV As Integer

    Public dblVolumenGSV As Integer
    Public dblVolumenNSV As Integer
    Public dblVolumenRVP As Integer
    Public dblVolumenBSW As Integer
    Public numVigenciaGuia As Integer

    Public datFechaLlegada As DateTime
    Public datFechaSalida As DateTime
    Public bolRedireccionado As Integer
    Public datFechaRedireccionado As DateTime
    Public numTurnoAtivo As Integer
    Public lstInspecciones As List(Of estInspecciones)
End Class
