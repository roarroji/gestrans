﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web.Services
Imports Softtools.GesCarga.Entidades.ControlTrafico

Imports Softtools.GesCarga.Fachada.ControlTrafico
Imports Softtools.GesCarga.Negocio.ControlTrafico

#Region "Estructuras"
Public Class estEntradaSeguimientoEmpresasGPS

    Public dblNITTransportadora As Double
    Public strNombreTransportadora As String
    Public strVEHIPlaca As String
    Public strCedulaConductor As String
    Public dteFechaReporte As Date
    Public strNombreEvento As String
    Public intNumeroSecuencia As Integer
    Public dblLatitud As Double
    Public dblLongitud As Double
    Public intAltitud As Integer
    Public intVelocidadInstantanea As Integer
    Public intHeading As Integer
    Public intSatelitesUsados As Integer
    Public intHDOP As Integer
    Public intTipoPosicion As Integer
    Public intInputs As Integer
    Public intCodigoEvento As Integer
    Public strMensajeEvento As String
    Public intDistanciaRecorrida As Int64
    Public intTiempoTrabajo As Integer
    Public intTiempoFalta As Integer
    Public intMagnitudFalta As Integer


End Class

Public Class estResultadoSeguimientoEmpresasGPS
    Public bolResultado As Boolean
    Public strMensajeResultado As String
    Public lonCodigoSeguimientoEMPRESAS As Long
End Class

Public Class estFiltroValidarUsuarioSeguimientoEmpresasGPS
    Public intCodigoEmpresa As Byte
    Public strIdentificadorUsuario As String
    Public strClave As String
End Class

#End Region

<WebService(Namespace:="GESCARGA.Servicios")>
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Public Class setSeguimientoEmpresasGPS
    Inherits System.Web.Services.WebService

    ''' <summary>
    ''' Inserta ubicacion desde GPS a vehiculo de empresa transportadora
    ''' </summary>
    ''' <param name="FiltrosValidarUsuario"></param>
    ''' <param name="EntradaSeguimientoGPS"></param>
    ''' <returns></returns>
    <WebMethod(Description:="Ingresa un seguimiento de las empresas transportadores retornadas por GPS")>
    Public Function UltimaPosicion(ByVal FiltrosValidarUsuario As estFiltroValidarUsuarioSeguimientoEmpresasGPS, ByVal EntradaSeguimientoGPS As estEntradaSeguimientoEmpresasGPS) As estResultadoSeguimientoEmpresasGPS

        Dim ResultadoOperacion As New estResultadoSeguimientoEmpresasGPS

        Dim objUsuarioValido As New clsValidaUsuario

        Try
            Dim IntCodigoUsuario As Integer = 0
            Const TIPO_APLICACION_WEBSERVICE_GPS As Integer = 205

            If objUsuarioValido.Validar_Usuario(FiltrosValidarUsuario.intCodigoEmpresa, FiltrosValidarUsuario.strIdentificadorUsuario, FiltrosValidarUsuario.strClave, TIPO_APLICACION_WEBSERVICE_GPS, ResultadoOperacion.strMensajeResultado, IntCodigoUsuario) Then

                'Entrada
                Dim EntidadEntrada As New SeguimientoTransportadoraGPS

                'Enviados por el usuario
                EntidadEntrada.CodigoEmpresa = FiltrosValidarUsuario.intCodigoEmpresa

                EntidadEntrada.NITTransportadora = EntradaSeguimientoGPS.dblNITTransportadora
                EntidadEntrada.NombreTransportadora = EntradaSeguimientoGPS.strNombreTransportadora
                EntidadEntrada.VEHIPlaca = EntradaSeguimientoGPS.strVEHIPlaca
                EntidadEntrada.CedulaConductor = EntradaSeguimientoGPS.strCedulaConductor
                EntidadEntrada.FechaReporte = EntradaSeguimientoGPS.dteFechaReporte
                EntidadEntrada.NombreEvento = EntradaSeguimientoGPS.strNombreEvento
                EntidadEntrada.NumeroSecuencia = EntradaSeguimientoGPS.intNumeroSecuencia
                EntidadEntrada.Latitud = EntradaSeguimientoGPS.dblLatitud
                EntidadEntrada.Longitud = EntradaSeguimientoGPS.dblLongitud
                EntidadEntrada.Altitud = EntradaSeguimientoGPS.intAltitud
                EntidadEntrada.VelocidadInstantanea = EntradaSeguimientoGPS.intVelocidadInstantanea
                EntidadEntrada.Heading = EntradaSeguimientoGPS.intHeading
                EntidadEntrada.SatelitesUsados = EntradaSeguimientoGPS.intSatelitesUsados
                EntidadEntrada.HDOP = EntradaSeguimientoGPS.intHDOP
                EntidadEntrada.TipoPosicion = EntradaSeguimientoGPS.intTipoPosicion
                EntidadEntrada.Inputs = EntradaSeguimientoGPS.intInputs
                EntidadEntrada.CodigoEvento = EntradaSeguimientoGPS.intCodigoEvento
                EntidadEntrada.MensajeEvento = EntradaSeguimientoGPS.strMensajeEvento
                EntidadEntrada.DistanciaRecorrida = EntradaSeguimientoGPS.intDistanciaRecorrida
                EntidadEntrada.TiempoTrabajo = EntradaSeguimientoGPS.intTiempoTrabajo
                EntidadEntrada.TiempoFalta = EntradaSeguimientoGPS.intTiempoFalta
                EntidadEntrada.MagnitudFalta = EntradaSeguimientoGPS.intMagnitudFalta
                EntidadEntrada.CodigoUsuarioCrea = IntCodigoUsuario


                Dim PersistenciaSeguimiento = New PersistenciaSeguimientoTransportadoraGPS

                Dim result = New LogicaSeguimientoTransportadoraGPS(PersistenciaSeguimiento).Guardar(EntidadEntrada)

                If result.Datos > 0 Then
                    ResultadoOperacion.lonCodigoSeguimientoEMPRESAS = result.Datos
                    ResultadoOperacion.bolResultado = True
                    ResultadoOperacion.strMensajeResultado = "El proceso ha concluido con éxito"

                Else
                    ResultadoOperacion.lonCodigoSeguimientoEMPRESAS = 0
                    ResultadoOperacion.bolResultado = False
                    ResultadoOperacion.strMensajeResultado = "Hubo un error al completar el proceso"
                End If

            End If

        Catch ex As Exception
            ResultadoOperacion.bolResultado = False
            Dim SeguimientoPila As New System.Diagnostics.StackTrace()
            Dim NombreFuncion As String = SeguimientoPila.GetFrame(0).GetMethod().Name
            ResultadoOperacion.strMensajeResultado = "Error en " & Me.ToString & " en la función " & NombreFuncion & ": " & ex.Message.ToString
        End Try
        Return ResultadoOperacion

    End Function




End Class

