﻿Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.IO


Public Class clsGeneral

#Region "Variables Generales"
    ' Generales
    Private intCodigoEmpresa As Integer

    ' Archivos Planos
    Dim stmStreamW As Stream
    Dim stwStreamWriter As StreamWriter
    Dim stwStreamReader As StreamReader
    Dim strRutaArchivoLog As String
    Dim strNombreArchivoLog As String
    Dim strRutaArchivoXML As String

    ' Base Datos
    Private strCadenaConexionSQL As String
    Private sqlConexion As SqlConnection
    Private sqlComando As SqlCommand
    Private sqlTransaccion As SqlTransaction
    Private sqlExcepcionSQL As SqlException

#End Region

#Region "Constantes"

    ' Generales
    Public Const PRIMER_TABLA As Integer = 0
    Public Const PRIMER_REGISTRO As Integer = 0

    ' Tipos Campo Base Datos
    Public Const CAMPO_NUMERICO As Byte = 1
    Public Const CAMPO_ALFANUMERICO As Byte = 2

#End Region

    Public Sub New()
        Try
            ' Inicializar Variables
            Me.intCodigoEmpresa = ConfigurationSettings.AppSettings("Empresa").ToString()
            Me.strCadenaConexionSQL = ConfigurationSettings.AppSettings("ConexionSQL").ToString()
            Me.strRutaArchivoLog = ConfigurationSettings.AppSettings("RutaArchivoLog").ToString()
            Me.strRutaArchivoXML = ConfigurationSettings.AppSettings("RutaArchivoXML").ToString()

        Catch ex As Exception
            Call Guardar_Mensaje_Log("clsGeneral_New" + ex.Message)
        End Try
    End Sub

#Region "Propiedades Acceso Base Datos"

    Public Property CadenaConexionSQL() As String
        Get
            Return Me.strCadenaConexionSQL
        End Get
        Set(ByVal value As String)
            Me.strCadenaConexionSQL = value
        End Set
    End Property

    Public Property Conexion() As SqlConnection
        Get
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            Me.sqlConexion = value
        End Set
    End Property

    Public Property ConexionSQL() As SqlConnection
        Get
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaConexionSQL)
            End If
            Return Me.sqlConexion
        End Get
        Set(ByVal value As SqlConnection)
            If IsNothing(Me.sqlConexion) Then
                Me.sqlConexion = New SqlConnection(Me.strCadenaConexionSQL)
            End If
            Me.sqlConexion = value
        End Set
    End Property

    Public Property ComandoSQL() As SqlCommand
        Get
            Return Me.sqlComando
        End Get
        Set(ByVal value As SqlCommand)
            Me.sqlComando = value
        End Set
    End Property

    Public Property TransaccionSQL() As SqlTransaction
        Get
            Return Me.sqlTransaccion
        End Get
        Set(ByVal value As SqlTransaction)
            Me.sqlTransaccion = value
        End Set
    End Property

    Public Property ExcepcionSQL() As SqlException
        Get
            Return Me.sqlExcepcionSQL
        End Get
        Set(ByVal value As SqlException)
            Me.sqlExcepcionSQL = value
        End Set
    End Property

#End Region

#Region "Funciones Base Datos"

    Public Function Ejecutar_SQL(ByVal strSQL As String, Optional ByRef strError As String = "") As Long
        Dim lonRegiAfec As Long = 0

        Try
            Using sqlConexion = New SqlConnection(Me.strCadenaConexionSQL)

                sqlConexion.Open()

                Using sqlComando As New SqlCommand

                    sqlComando.CommandType = CommandType.Text
                    sqlComando.CommandText = strSQL
                    sqlComando.Connection = sqlConexion

                    lonRegiAfec = Val(sqlComando.ExecuteNonQuery().ToString)

                End Using

                sqlConexion.Close()

            End Using

        Catch ex As Exception
            Ejecutar_SQL = False
            strError = ex.Message
        End Try

        Return lonRegiAfec

    End Function

    Public Function Retorna_Dataset(ByVal strSQL As String, Optional ByRef strError As String = "") As DataSet
        Dim dsDataSet As DataSet = New DataSet

        Try
            Using sqlConexion = New SqlConnection(Me.strCadenaConexionSQL)

                sqlConexion.Open()

                Using sqlDataAdap = New SqlDataAdapter(strSQL, sqlConexion)

                    sqlDataAdap.Fill(dsDataSet)

                End Using

                sqlConexion.Close()

            End Using

        Catch ex As Exception
            Guardar_Mensaje_Log("Error Retorna_Dataset: " & ex.Message)

        End Try

        Return dsDataSet

    End Function

#End Region

#Region "Funciones Archivos Planos"

    Public Function Guardar_Mensaje_Log(ByVal strMensaje As String)
        Try

            Dim strContenido As String, strFechaHora As String = Date.Now.ToString
            Dim strNombreArchivoLog As String = Me.strRutaArchivoLog & "SW_GESCARGA50_SIESA_" _
            & Format$(Date.Now.Day, "00") & Format$(Date.Now.Month, "00") & Format$(Date.Now.Year, "00") & ".txt"

            If Not File.Exists(strNombreArchivoLog) Then
                File.Create(strNombreArchivoLog).Dispose()
                strContenido = String.Empty
            Else
                strContenido = File.ReadAllText(strNombreArchivoLog)
            End If
            Me.stmStreamW = File.OpenWrite(strNombreArchivoLog)
            Me.stwStreamWriter = New StreamWriter(Me.stmStreamW, System.Text.Encoding.Default)
            Me.stwStreamWriter.Flush()
            Me.stwStreamWriter.Write(strContenido & vbNewLine & strFechaHora & vbNewLine & strMensaje)
            Me.stwStreamWriter.Close()
            Me.stmStreamW.Close()

            Return True
        Catch ex As Exception
            '' FALTA
            ' Escribir en log eventos windows
            Return False
            Exit Try
        End Try
    End Function

#End Region

End Class
