﻿Option Explicit On

Imports System.Configuration
Imports System.Diagnostics.Eventing.Reader
Imports System.Net
Imports System.Threading
Imports System.Xml
Imports Softtools.Gescarga.Servicios.SIESA_ENCOE.wsSIESA


Public Class serSIESA

#Region "Variables"

    ' Variables
    Private strSQL As String
    Private strPlano As String
    Private lonNumeRegi As Long
    Private objGeneral As clsGeneral

    Private lonNumeComp As Long = 0
    Private lonNumeCompAnte As Long = 0
    Private intCodigoEmpresa As Integer
    Private lonIntervaloMilisegundosEjecucionProceso As Long
    Private strRutaArchivoLog As String
    Private strRutaArchivoXML As String

    ' Hilos Procesos
    Private HilProcesoComprobantesEgreso As Thread
    Private HilProcesoFacturaContadoContraEntrega As Thread
    Private HilProcesoCumplidoContraEntrega As Thread
    Private HilProcesoLegalizacionRecaudo As Thread
    Private HilProcesoLegalizacionGastosConductor As Thread
    Private HilProcesoLiquidacionPlanillaDespachos As Thread
    Private HilProcesoOrdenVentaServicio As Thread

    ' GenericTransfer
    Dim objGenericTransfer As wsGenerarPlanoSoapClient

#End Region

#Region "Constantes GESTRANS"
    ' GESTRANS
    Const CODIGO_USUARIO_SYSTEM As Integer = 0 ' Usuario Sistema

    ' TIPOS DOCUMENTOS GESTRANS
    Const TIDO_COMPROBANTE_EGRESO As Integer = 30
    Const TIDO_LIQUIDACION_PLANILLA_MASIVO As Integer = 160
    Const TIDO_LIQUIDACION_PLANILLA_PAQUETERIA As Integer = 165

    ' FORMAS PAGO FACTURAS
    Const FORMA_PAGO_FACTURA_CONTADO As Long = 4902
    Const FORMA_PAGO_FACTURA_CONTRAENTREGA As Long = 4903

    ' FORMA PAGO LEGALIZACION RECAUDO
    Const FORMA_PAGO_LEGALIZACION_RECAUDO_EFECTIVO As Long = 5101
    Const FORMA_PAGO_LEGALIZACION_RECAUDO_TRANSFERENCIA As Long = 5102
    Const FORMA_PAGO_LEGALIZACION_RECAUDO_CHEQUE As Long = 5103

    ' CONCEPTOS LIQUIDACION PLANILLA DESPACHOS
    Const CONCEPTO_ANTICIPO_LIQUIDACION_PLANILLA_DESPACHOS As Integer = 1
    Const CONCEPTO_REANTICIPO_LIQUIDACION_PLANILLA_DESPACHOS As Integer = 2
    Const CONCEPTO_FONDO_RESPONSABILIDAD_LIQUIDACION_PLANILLA_DESPACHOS As Integer = 7
    Const CONCEPTO_MAYOR_VALOR_FLETE_LIQUIDACION_PLANILLA_DESPACHOS As Integer = 10
    Const CONCEPTO_MENOR_VALOR_FLETE_LIQUIDACION_PLANILLA_DESPACHOS As Integer = 11

#End Region

#Region "Constantes SIESA"

    ' SIESA TABLAS
    Const TIPO_NATURALEZA_NATURAL_SIESA As Integer = 1
    Const TIPO_NATURALEZA_JURIDICA_SIESA As Integer = 2

    Const TIPO_IDENTIFICACION_CEDULA_SIESA As String = "C"
    Const TIPO_IDENTIFICACION_EXTRANJERIA_SIESA As String = "E"
    Const TIPO_IDENTIFICACION_NIT_SIESA As String = "N"

    ' Anticipos 
    Const CENTRO_OPERACION_SIESA As String = "FZ2"
    Const UNIDAD_NEGOCIO_SIESA As String = "100"
    Const TIPO_DOCUMENTO_EGRESO_SIESA As String = "ANT"
    Const PREFIJO_CRUCE_ANTICIPO_SIESA As String = "PPL"
    Const SUCURSAL_SIESA As String = "ANT"

    ' Facturas Contado y Contraentrega
    Const TIPO_DOCUMENTO_FACTURA_SIESA As String = "FCO"
    Dim TIPO_CLIENTE_FACTURA_SIESA As String = ""
    Dim CONDICION_PAGO_FACTURA_SIESA As String = ""
    Dim TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA As String = ""
    Dim CENTRO_OPERACION_FACTURA_SIESA As String = ""

    ' Legalización Recaudo Contado - Traslado Cajas Efectivo
    Const TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA As String = "TDD"

    ' Legalización Gastos Conductor
    Const TIPO_DOCUMENTO_LEGALIZA_GASTOS_CONDUCTOR_SIESA As String = "LGV"

    ' Liquidación Planilla Despachos
    Const TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA As String = "FL" ' Se concadena 1,2,3 de acuerdo al número de tenedores del vehículo

#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            'Me.HilProcesoComprobantesEgreso = New Thread(AddressOf Proceso_Comprobantes_Egreso)
            'Me.HilProcesoComprobantesEgreso.Start()

            'Me.HilProcesoFacturaContadoContraEntrega = New Thread(AddressOf Proceso_Facturas_Contado_Contraentrega)
            'Me.HilProcesoFacturaContadoContraEntrega.Start()

            'Me.HilProcesoCumplidoContraEntrega = New Thread(AddressOf Proceso_Cumplido_ContraEntrega)
            'Me.HilProcesoCumplidoContraEntrega.Start()

            'Me.HilProcesoLegalizacionRecaudo = New Thread(AddressOf Proceso_Legalizacion_Recaudo)
            'Me.HilProcesoLegalizacionRecaudo.Start()

            'Me.HilProcesoLegalizacionGastosConductor = New Thread(AddressOf Proceso_Legalizacion_Gastos_Conductor)
            'Me.HilProcesoLegalizacionGastosConductor.Start()

            'Me.HilProcesoLiquidacionPlanillaDespachos = New Thread(AddressOf Proceso_Liquidacion_Despachos)
            'Me.HilProcesoLiquidacionPlanillaDespachos.Start()


        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error OnStart: " & ex.Message)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        Try
            'Me.HilProcesoComprobantesEgreso.Abort()

            'Me.HilProcesoFacturaContadoContraEntrega.Abort()

            'Me.HilProcesoCumplidoContraEntrega.Abort()

            'Me.HilProcesoLegalizacionRecaudo.Abort()

            'Me.HilProcesoLegalizacionGastosConductor.Abort()

            'Me.HilProcesoLiquidacionPlanillaDespachos.Abort()

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error OnStop: " & ex.Message)
        End Try
    End Sub

    Public Sub New()
        Try
            InitializeComponent()

            Me.objGeneral = New clsGeneral
            Call Cargar_Datos_App_Config()

            Me.objGeneral.Guardar_Mensaje_Log("Inició servicio")

            ' COMENTARIAR CUANDO SE GENERE EL INSTALADOR DEL SERVICIO, SOLO PARA PRUEBAS

            'Me.HilProcesoComprobantesEgreso = New Thread(AddressOf Proceso_Comprobantes_Egreso)
            'Me.HilProcesoComprobantesEgreso.Start()

            'Me.HilProcesoFacturaContadoContraentrega = New Thread(AddressOf Proceso_Facturas_Contado_ContraEntrega)
            'Me.HilProcesoFacturaContadoContraentrega.Start()

            'Me.HilProcesoCumplidoContraEntrega = New Thread(AddressOf Proceso_Cumplido_ContraEntrega)
            'Me.HilProcesoCumplidoContraEntrega.Start()

            'Me.HilProcesoLegalizacionRecaudo = New Thread(AddressOf Proceso_Legalizacion_Recaudo)
            'Me.HilProcesoLegalizacionRecaudo.Start()

            'Me.HilProcesoLegalizacionGastosConductor = New Thread(AddressOf Proceso_Legalizacion_Gastos_Conductor)
            'Me.HilProcesoLegalizacionGastosConductor.Start()

            Me.HilProcesoLiquidacionPlanillaDespachos = New Thread(AddressOf Proceso_Liquidacion_Planilla_Despachos)
            Me.HilProcesoLiquidacionPlanillaDespachos.Start()

            'Me.HilProcesoOrdenVentaServicio = New Thread(AddressOf Proceso_Orden_Venta_Servicio)
            'Me.HilProcesoOrdenVentaServicio.Start()

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error New: " & ex.Message)
        End Try

    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 06-MAY-2021
    ' Observaciones Creación: Se consulta el Encabezado y Detalle de Comprobantes Contables, el cual debe traer 2 registros por comprobante
    ' Modifica: 
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Proceso_Comprobantes_Egreso()
        Try
            Dim dtsCompCont As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Comprobantes_Egreso")
                strSQL = "gsp_Obtenter_Comprobantes_Egresos_SIESA " & intCodigoEmpresa.ToString
                dtsCompCont = objGeneral.Retorna_Dataset(strSQL)

                If dtsCompCont.Tables.Count > 0 Then

                    If dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Call Crear_XML_Comprobantes_Egreso(dtsCompCont)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Comprobantes_Egreso: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 06-MAY-2021
    ' Observaciones Creación: Se construye XML con base en especificación entregada por GenericTransfer 19-ABR-2021
    ' Modifica: 
    ' Fecha Modificación: 
    ' Observaciones Modificación:

    Private Sub Crear_XML_Comprobantes_Egreso(ByVal dtsCompCont As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = ""
            Dim strErrorDetalleSIESA As String, lonNumeCompEgre As Long, strIdenTercCompEgre As String, strFechCompEgre As String, strNotas As String, dblValor As Double

            Dim xmlDocumento As XmlDocument
            'Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeComp = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero")
                lonNumeCompEgre = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()
                strIdenTercCompEgre = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tercero").ToString()
                strFechCompEgre = Retorna_Fecha_Formato_SIESA(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                strNotas = "COMPROBANTE EGRESO ANTICIPO No. " & lonNumeCompEgre.ToString()
                If dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Debito").ToString() > 0 Then
                    dblValor = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Debito").ToString()
                Else
                    dblValor = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Credito").ToString()
                End If

                If lonNumeComp <> lonNumeCompAnte Then

                    xmlDocumento = New XmlDocument

                    'xmlDeclaracion = xmlDocumento.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                    'xmlDocumento.AppendChild(xmlDeclaracion)

                    xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                    xmlDocumento.AppendChild(xmlElementoRaiz)

                    ' DOCUMENTO CONTABLE
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Documentocontable")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' F350_ID_CO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_EGRESO_SIESA)
                    ' F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeCompEgre)
                    ' F350_FECHA (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechCompEgre)
                    ' F350_ID_TERCERO (Alfanumerico(15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenTercCompEgre)
                    ' F350_NOTAS (Alfanumerico(255))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strNotas)

                    ' CAJA (Comprobantes Egreso Efectivo)
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("Caja")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' F350_ID_CO (Alfanumerico (3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_EGRESO_SIESA)
                    ' F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeCompEgre)
                    ' F351_ID_AUXILIAR (Alfanumerico(20))
                    strValorCampo = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Cuenta").ToString()
                    strValorCampo = "11050501" ' PARAMETRIZACION PENDIENTE Tomarla del movimiento Parametrizado
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                    ' F351_ID_CO_MOV (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)

                    ' F351_ID_UN (Alfanumerico(20))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                    ' F351_ID_CCOSTO (Alfanumerico(15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                    ' F351_ID_FE (Alfanumerico(10))
                    strValorCampo = "1233" ' PARAMETRIZACION PENDIENTE Constante relacionado Flujo de Efectivo 
                    ' Cindy envia listado, se debe tener en cuenta Forma Pago
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_FE", strValorCampo)
                    ' F351_VALOR_DB (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", 0)
                    ' F351_VALOR_CR (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValor)

                    ' F351_NOTAS (Alfanumerico(255))
                    strValorCampo = "CAJA ANTICIPO No. Egreso: " & lonNumeCompEgre.ToString()
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                    ' F358_ID_CAJA (Alfanumerico(3))
                    strValorCampo = "R01" ' PARAMETRIZACION PENDIENTE RELACIONADO A LA CAJA ASIGNADA AL USUARIO
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_CAJA", strValorCampo)
                    ' F358_NOTAS (Alfanumerico(255))
                    strValorCampo = "CAJA ANTICIPO No. Egreso: " & lonNumeCompEgre.ToString()
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NOTAS", strValorCampo)

                Else
                    ' MOVIMIENTO CXP
                    xmlElementoRoot = xmlDocumento.DocumentElement
                    xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                    xmlElementoRoot.AppendChild(xmlElemento)
                    xmlElementoRoot = xmlDocumento.DocumentElement

                    ' F350_ID_CO (Alfanumerico (3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_EGRESO_SIESA)
                    ' F350_CONSEC_DOCTO (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeCompEgre)
                    ' F351_ID_AUXILIAR (Alfanumerico(20))
                    strValorCampo = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Cuenta").ToString()
                    strValorCampo = "13301502" ' PARAMETRIZACION PENDIENTE CXP ANTICIPO Tomarla del movimiento Parametrizado
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                    ' F351_ID_TERCERO (Alfanumerico (15))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTercCompEgre)

                    ' F351_ID_CO_MOV (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                    ' F351_ID_UN (Alfanumerico(20) 05=PASAJEROS)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                    ' F351_VALOR_DB (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValor)
                    ' F351_VALOR_CR (Decimal)
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", 0)
                    ' F351_NOTAS (Alfanumerico(255))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", "")

                    ' F353_ID_SUCURSAL (Alfanumerico(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", SUCURSAL_SIESA)
                    ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", PREFIJO_CRUCE_ANTICIPO_SIESA)
                    ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", 0)
                    ' F353_NRO_CUOTA_CRUCE (Entero(3))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", 1)
                    ' F353_ID_FE (Alfanumerico(10))
                    strValorCampo = "1233" ' PARAMETRIZACION PENDIENTE Constante relacionado Flujo de Efectivo
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", strValorCampo)

                    ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechCompEgre)
                    ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechCompEgre)
                    ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechCompEgre)
                    ' F354_NOTAS (Alfanumerico(255))
                    strValorCampo = "CxP ANTICIPO No. Egreso: " & lonNumeCompEgre.ToString()
                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                    strXML = xmlDocumento.InnerXml
                    Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                    ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                    Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                    strPlanoXML = objGenericTransfer.ImportarDatosXML(105599, "ANTICIPOS", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                    If strPlanoXML = "Importacion exitosa" Then

                        ' Actualizar el Comprobante Contable con Interfase_Contable = 1 y Fecha_Interfase_Contable = getDate()
                        strSQL = "UPDATE Encabezado_Comprobante_Contables SET Interfase_Contable = 1, Fecha_Interfase_Contable = getDate()"
                        strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                        strSQL += " AND Numero = " & lonNumeComp

                        ' Ejecutar UPDATE
                        lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                    Else
                        strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                        ' Actualizar el Comprobante Contable con Interfase_Contable = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                        strSQL = "UPDATE Encabezado_Comprobante_Contables SET Interfase_Contable = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                        strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                        strSQL += " AND Numero = " & lonNumeComp

                        ' Ejecutar UPDATE
                        lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                    End If

                End If

                lonNumeCompAnte = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero")

            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Crear_XML_Comprobantes_Egreso: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 24-MAY-2021
    ' Observaciones Creación: Se consulta la Factura Contado
    ' Modifica: 
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Proceso_Facturas_Contado_Contraentrega()
        Try
            Dim dtsCompCont As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Facturas_Contado_Contraentrega")
                strSQL = "gsp_Obtenter_Facturas_Contado_Contraentrega_SIESA " & intCodigoEmpresa.ToString
                dtsCompCont = objGeneral.Retorna_Dataset(strSQL)

                If dtsCompCont.Tables.Count > 0 Then

                    If dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Call Crear_XML_Facturas_Contado_Contraentrega(dtsCompCont)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Facturas_Contado_Contraentrega: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 24-MAY-2021
    ' Observaciones Creación: Se construye XML con base en especificación entregada por GenericTransfer 19-ABR-2021
    ' Modifica: 
    ' Fecha Modificación: 
    ' Observaciones Modificación:

    Private Sub Crear_XML_Facturas_Contado_Contraentrega(ByVal dtsCompCont As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = "", strErrorDetalleSIESA As String

            Dim lonNumero As Long, lonNumeFact As Long, strFechFact As String, strIdenTercFact As String, strNotas As String
            Dim lonFormPago As Long, dblValoFact As Double

            Dim xmlDocumento As XmlDocument
            'Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumero = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeFact = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()
                strIdenTercFact = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Identificacion").ToString()
                strFechFact = Retorna_Fecha_Formato_SIESA(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                strNotas = "Factura No. " & lonNumeFact.ToString()
                dblValoFact = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Factura").ToString()
                lonFormPago = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("CATA_FPVE_Codigo").ToString()

                ' Constantes con base en forma de pago
                If lonFormPago = FORMA_PAGO_FACTURA_CONTADO Then
                    TIPO_CLIENTE_FACTURA_SIESA = "0001" ' OK
                    CONDICION_PAGO_FACTURA_SIESA = "00D" ' OK
                    TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA = "FCO" ' Parametrizar en forma pago
                    CENTRO_OPERACION_FACTURA_SIESA = "B25" ' Parametrizar y tomar Agencia que genero la numeración factura

                ElseIf lonFormPago = FORMA_PAGO_FACTURA_CONTRAENTREGA Then
                    TIPO_CLIENTE_FACTURA_SIESA = "0002"
                    CONDICION_PAGO_FACTURA_SIESA = "05D"
                    TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA = "FCE"
                    CENTRO_OPERACION_FACTURA_SIESA = "B04"

                End If

                xmlDocumento = New XmlDocument

                'xmlDeclaracion = xmlDocumento.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                'xmlDocumento.AppendChild(xmlDeclaracion)

                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                ' DOCUMENTO VENTA SERVICIOS
                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("DoctoVentasServicios")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                ' F311_ID_UN_CRUCE (Alfanumerico(20))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_UN_CRUCE", "")
                ' F311_ID_CCOSTO_CRUCE (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_CCOSTO_CRUCE", "")
                ' F311_ID_CAJA (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_CAJA", "") ' Se envia CON cuando forma pago CONTADAO
                ' F311_REFERENCIA (Alfanumerico(10))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_REFERENCIA", lonNumeFact)
                ' F311_ID_COND_PAGO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_COND_PAGO", CONDICION_PAGO_FACTURA_SIESA) ' 05D=Contraentrega, 00D=Contado
                ' F311_ID_TERCERO_VENDEDOR (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_TERCERO_VENDEDOR", "80412961") ' Ejecutar Consulta Clientes SIESA y tomar Vendedor 
                ' F311_ID_TIPO_CLI (Alfanumerico(4))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_TIPO_CLI", TIPO_CLIENTE_FACTURA_SIESA) ' 0002=Contraentrea, 0001=Contado
                ' F311_ID_SUCURSAL_CLI (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F311_ID_SUCURSAL_CLI", "001") ' Es una constante
                ' F350_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strNotas)
                ' F350_ID_TERCERO (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenTercFact)
                ' F350_FECHA (Alfanumerico(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechFact)
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeFact) ' Pregunta es máximo de 8?
                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA) 'FCE=Contra Entrega, FCO=Contado
                ' F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_FACTURA_SIESA) 'B04=Contra Entrega, B25=Contado


                ' MOVIMIENTO VENTA SERVICIOS 
                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("MovtoVentasServicios")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                ' F320_DETALLE (Alfanumerico(2000))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_DETALLE", strNotas)
                ' F320_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_NOTAS", strNotas)
                ' F320_VLR_DSCTO_1 (Decimal(20))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_VLR_DSCTO_1", "0")
                ' F320_VLR_BRUTO (Decimal(20))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_VLR_BRUTO", dblValoFact)
                ' F320_CANTIDAD (Decimal(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_CANTIDAD", "1")
                ' F320_ID_SUCURSAL_CLIENTE (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_SUCURSAL_CLIENTE", "001") ' Constante
                ' F320_ID_TERCERO_MOVTO (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_TERCERO_MOVTO", strIdenTercFact)
                ' F320_ID_CCOSTO_MOVTO (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_CCOSTO_MOVTO", "210101") ' Definir ENCOEXPRES: Enviar tabla
                ' F320_ID_UN_MOVTO (Alfanumerico(20))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_UN_MOVTO", "200") ' Definir ENCOEXPRES: Enviar tabla
                ' F320_ID_CO_MOVTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_CO_MOVTO", CENTRO_OPERACION_FACTURA_SIESA)
                ' F320_ID_MOTIVO (Alfanumerico(2)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_MOTIVO", "01") ' Definir ENCOEXPRES: Enviar tabla
                ' F320_ID_SERVICIO (Alfanumerico(10))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ID_SERVICIO", "VS450501") ' Definir ENCOEXPRES: Enviar tabla
                ' F320_ROWID (Entero(10))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ROWID", "1") ' Consecutivo por envío intCon
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeFact)
                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA) 'FCE=Contra Entrega, FCO=Contado
                ' F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_FACTURA_SIESA) 'B04=Contra Entrega, B25=Contado


                ' RETENCIONES 
                'xmlElementoRoot = xmlDocumento.DocumentElement
                'xmlElemento = xmlDocumento.CreateElement("Retenciones")
                'xmlElementoRoot.AppendChild(xmlElemento)
                'xmlElementoRoot = xmlDocumento.DocumentElement

                '' F314_BASE_MIN_MONEDA_DOCTO (Decimal(20))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F314_BASE_MIN_MONEDA_DOCTO", "")
                '' F314_TASA (Decimal(8)
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F314_TASA", "")
                '' F314_VLR_BASE (Decimal(20))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F314_VLR_BASE", "")
                '' F314_VLR_RET (Decimal(20))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F314_VLR_RET", "")
                '' F314_ID_CLASE_IMP_BASE (Entero(3))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F314_ID_CLASE_IMP_BASE", "")
                '' F314_ID_LLAVE_RETENCION (Alfanumerico(4))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F314_ID_LLAVE_RETENCION", "")
                '' F350_CONSEC_DOCTO (Entero(8))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", "")
                '' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", "")
                '' F350_ID_CO (Alfanumerico(3))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", "")

                ' IMPUESTOS 
                'xmlElementoRoot = xmlDocumento.DocumentElement
                'xmlElemento = xmlDocumento.CreateElement("Impuestos")
                'xmlElementoRoot.AppendChild(xmlElemento)
                'xmlElementoRoot = xmlDocumento.DocumentElement

                '' F350_ID_CO (Alfanumerico(3))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", "")
                '' F350_ID_TIPO_DOCTO (Alfanumerico(3)
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", "")
                '' F350_CONSEC_DOCTO (Entero(8))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", "")
                '' F320_ROWID (Entero(10))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F320_ROWID", "")
                '' F321_ID_LLAVE_IMPUESTO (Alfanumerico(4))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F321_ID_LLAVE_IMPUESTO", "")
                '' F321_PORCENTAJE_BASE (Decimal(8))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F321_PORCENTAJE_BASE", "")
                '' F321_TASA (Decimal(8))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F321_TASA", "")
                '' F321_VLR_UNI (Decimal(20))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F321_VLR_UNI", "")
                '' F321_VLR_TOT (Decimal(20))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F321_VLR_TOT", "")


                ' CAJA 
                'xmlElementoRoot = xmlDocumento.DocumentElement
                'xmlElemento = xmlDocumento.CreateElement("Caja")
                'xmlElementoRoot.AppendChild(xmlElemento)
                'xmlElementoRoot = xmlDocumento.DocumentElement

                '' F350_ID_CO (Alfanumerico (3))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", "")
                '' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", "")
                '' F350_CONSEC_DOCTO (Entero(8))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", "")
                '' F358_ID_MEDIOS_PAGO (Alfanumerico(3))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_MEDIOS_PAGO", "")
                '' F_VLR_MEDIO_PAGO (Decimal(20))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F_VLR_MEDIO_PAGO", "")
                '' F_VLR_MEDIO_PAGO_LOCAL (Decimal(20))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F_VLR_MEDIO_PAGO_LOCAL", "")
                '' F358_ID_BANCO (Alfanumerico(10))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_BANCO", "")
                '' F358_NRO_CHEQUE (Entero(8))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NRO_CHEQUE", "")
                '' F358_NRO_CUENTA (Alfanumerico(25))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NRO_CUENTA", "")
                '' F358_COD_SEGURIDAD (Alfanumerico(3))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_COD_SEGURIDAD", "")
                '' F358_NRO_AUTORIZACION (Alfanumerico(10))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NRO_AUTORIZACION", "")
                '' F358_FECHA_VCTO (Alfanumerico(8))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_FECHA_VCTO", "")
                '' F358_REFERENCIA_OTROS (Alfanumerico(30))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_REFERENCIA_OTROS", "")
                '' F358_FECHA_CONSIGNACION (Alfanumerico(8))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_FECHA_CONSIGNACION", "")
                '' F358_NOTAS (Alfanumerico(255))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NOTAS", "")
                '' F358_ID_CCOSTO (Alfanumerico(15))
                'AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_CCOSTO", "")

                ' CUOTAS CxC
                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("CuotasCxC")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                ' F353_FECHA_DSCTO_PP (Alfanumerico (8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechFact)
                ' F353_FECHA_VCTO (Alfanumerico(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechFact)
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeFact)
                ' F350_ID_TIPO_DOCTO (Alfanumerico (3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_FORMA_PAGO_FACTURA_SIESA) 'FCE=Contra Entrega, FCO=Contado
                ' F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_FACTURA_SIESA) 'B04=Contra Entrega, B25=Contado


                strXML = xmlDocumento.InnerXml
                Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                strPlanoXML = objGenericTransfer.ImportarDatosXML(105794, "FACTURAS_CONTADO", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                If strPlanoXML = "Importacion exitosa" Then

                    ' Actualizar en la Factura los campos Interfaz_Contable_Factura = 1 y Fecha_Interfase_Contable = getDate()
                    strSQL = "UPDATE Encabezado_Facturas SET Mensaje_Error = '', Interfaz_Contable_Factura = 1, Fecha_Interfase_Contable = getDate()"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumero

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                Else
                    strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                    ' Actualizar en la Factura los campos Interfaz_Contable_Factura = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                    strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Factura = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumero

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                End If
            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Crear_XML_Facturas_Contado_Contraentrega: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 10-JUN-2021
    ' Observaciones Creación: Se consulta los Cumplidos ContraEntrega
    ' Modifica: 
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Proceso_Cumplido_ContraEntrega()
        Try
            Dim dtsCompCont As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Cumplido_ContraEntrega")
                strSQL = "gsp_Consultar_Cumplidos_Contraentrega_SIESA_ENCOE " & intCodigoEmpresa.ToString
                dtsCompCont = objGeneral.Retorna_Dataset(strSQL)

                If dtsCompCont.Tables.Count > 0 Then

                    If dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Call Crear_XML_Cumplido_ContraEntrega(dtsCompCont)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Cumplido_ContraEntrega: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 10-JUN-2021
    ' Observaciones Creación: Se construye XML con base en especificación entregada por GenericTransfer 08-JUN-2021
    ' Modifica: 
    ' Fecha Modificación: 
    ' Observaciones Modificación:

    Private Sub Crear_XML_Cumplido_ContraEntrega(ByVal dtsCompCont As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = "", strErrorDetalleSIESA As String

            Dim lonNumero As Long, lonNumeGuia As Long, strFechGuia As String, lonNumePlan As Long, strIdenTercDest As String
            Dim strNotas As String, dblValoGuia As Double, datFechVenc As Date, strFechVenc As String, strIdenTercEntr As String

            Dim xmlDocumento As XmlDocument
            'Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumero = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeGuia = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Guia").ToString()
                strFechGuia = Retorna_Fecha_Formato_SIESA(dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha_Guia").ToString())
                datFechVenc = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha_Guia").ToString()
                strFechVenc = datFechVenc.AddDays(3).ToString ' Sumar 3 días
                strFechVenc = Retorna_Fecha_Formato_SIESA(strFechVenc)
                strIdenTercDest = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Destinatario").ToString()
                ' PENDIENTE GUARDAR TERCERO ENTREGA Y No. PLANILLA ENTREGA EN CUMPLIDO
                If dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Empleado_Entrega").ToString() <> "0" Then
                    strIdenTercEntr = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Empleado_Entrega").ToString()
                Else
                    strIdenTercEntr = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Conductor_Entrega").ToString()
                End If
                lonNumePlan = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Ultima_Planilla").ToString()
                dblValoGuia = dtsCompCont.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Total_Guia").ToString()
                strNotas = "Guia No. " & lonNumeGuia.ToString() & " Planilla No. " & lonNumePlan

                xmlDocumento = New XmlDocument

                'xmlDeclaracion = xmlDocumento.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                'xmlDocumento.AppendChild(xmlDeclaracion)

                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                ' DOCUMENTO CONTABLE
                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("DocumentoContable")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                ' F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", "FZ2") ' Centro Operación el mismo de la factura de la Guia
                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", "LCE") ' Parametrización tipo documento
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeGuia)
                ' F350_FECHA (Alfanumerico(8)) 
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechGuia) ' Cambiar por la Fecha Entrega
                ' F350_ID_TERCERO (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenTercDest)
                ' F350_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strNotas)

                ' MOVIMIENTO CxC 
                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("MovimientoCxC")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                ' F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", "FZ2") ' Parametrizar
                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", "LCE") ' Parametrizar
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeGuia)
                ' F351_ID_AUXILIAR (Alfanumerico(20))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "13050502") ' Parametrizar
                ' F351_ID_TERCERO (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTercDest)
                ' F351_ID_CO_MOV (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", "FZ2")
                ' F351_ID_UN (Alfanumerico(20))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' Parametrizar por Código Cuenta
                ' F351_VALOR_DB (Decimal(21))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                ' F351_VALOR_CR (Decimal(21))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoGuia)
                ' F351_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                ' F353_ID_SUCURSAL (Alfanumerico(3)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "001") ' Tercero Destinatario / Pago Envio
                ' F353_ID_TIPO_DOCTO_CRUCE (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_TIPO_DOCTO_CRUCE", "FCO")
                ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumeGuia)
                ' F353_NRO_CUOTA_CRUCE (Entero(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "0")
                ' F353_FECHA_VCTO (Alfanumerico(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechVenc) '
                ' F353_FECHA_DSCTO_PP (Alfanumerico(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechVenc)
                ' F354_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strNotas)

                ' MOVIMIENTO CxC 
                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("MovimientoCxC")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                ' F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", "FZ2") ' Parametrizar
                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", "LCE") ' Parametrizar
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeGuia)
                ' F351_ID_AUXILIAR (Alfanumerico(20))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "13050520") ' Parametrizar
                ' F351_ID_TERCERO (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTercEntr)
                ' F351_ID_CO_MOV (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", "FZ2") '
                ' F351_ID_UN (Alfanumerico(20))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' Fijo segun cuenta
                ' F351_VALOR_DB (Decimal(21))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValoGuia)
                ' F351_VALOR_CR (Decimal(21))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                ' F351_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                ' F353_ID_SUCURSAL (Alfanumerico(3)
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "001") ' Parametrizar Oficina Tercero o Conductor Entrega
                ' F353_ID_TIPO_DOCTO_CRUCE (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_TIPO_DOCTO_CRUCE", "LCE")
                ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumeGuia)
                ' F353_NRO_CUOTA_CRUCE (Entero(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "0")
                ' F353_FECHA_VCTO (Alfanumerico(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechVenc) '
                ' F353_FECHA_DSCTO_PP (Alfanumerico(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechVenc)
                ' F354_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strNotas)

                strXML = xmlDocumento.InnerXml
                Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                strPlanoXML = objGenericTransfer.ImportarDatosXML(105794, "FACTURAS_CONTADO", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                If strPlanoXML = "Importacion exitosa" Then

                    ' Actualizar en Cumplido_Remesas los campos Interfaz_Contable_Factura = 1 y Fecha_Interfase_Contable = getDate()
                    strSQL = "UPDATE Cumplido_Remesas SET Mensaje_Error = '', Interfaz_Contable_Factura = 1, Fecha_Interfase_Contable = getDate()"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumero

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                Else
                    strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                    ' Actualizar en Cumplido_Remesas los campos Interfaz_Contable_Factura = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                    strSQL = "UPDATE Cumplido_Remesas SET Interfaz_Contable_Factura = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumero

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                End If

            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Crear_XML_Cumplido_ContraEntrega: " & ex.Message)
        End Try
    End Sub


    ' Autor: Jesus Cuadros
    ' Fecha Creación: 22-JUN-2021
    ' Observaciones Creación: Se consulta la Legalizacion Recaudo
    ' Modifica: Jesus Cuadros
    ' Fecha Modificación: 01-JUL-2021
    ' Observaciones Modificación: Se modifica para enviar la Legalización Recaudo con sus detalles Efectivo y Bancos

    Private Sub Proceso_Legalizacion_Recaudo()
        Try
            Dim dtsConsulta As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Legalizacion_Recaudo")
                strSQL = "gsp_Consultar_Encabezado_Legalizacion_Recaudo_SIESA_ENCOE " & intCodigoEmpresa.ToString & ", 26" ' PRUEBA No. 1000020
                dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

                If dtsConsulta.Tables.Count > 0 Then

                    If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Me.objGeneral.Guardar_Mensaje_Log("Número Legalización Recaudo a Procesar: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)

                        Call Crear_XML_Proceso_Legalizacion_Recaudo(dtsConsulta)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Legalizacion_Recaudo: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 22-JUN-2021
    ' Observaciones 
    ' Modifica: Jesus Cuadros
    ' Fecha Modificación: 09-JUL-2021
    ' Observaciones Modificación: Se estructura el envío de toda la Legalización Recaudo

    Private Sub Crear_XML_Proceso_Legalizacion_Recaudo(ByVal dtsEncabezado As DataSet)
        Try
            Dim strXML As String, strPlanoXML As String, strValorCampoDefec As String = "", strValorCampo As String = ""
            Dim strErrorDetalleSIESA As String, strNotas As String, dblValor As Double
            Dim lonNumLegRec As Long, lonNumDocLegRec As Long, lonNumeReme As Long, lonNumeDocuReme As Long, strFechPago As String, strIdenCond As String, strIdenAfor As String
            Dim strCodiOficLega As String, strCodiOficLegaSIESA As String, lonFormPagoReca As Long, lonFormPagoVent As Long, lonNumLegRecAnt As Long = 0, lonNumLegRecSig As Long = 0
            Dim dtsDetalle As DataSet, strFechReme As String, strFechLega As String, strDocuPago As String, strTipoDocuPago As String

            Dim xmlDocumento As XmlDocument
            'Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsEncabezado.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumLegRec = dtsEncabezado.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Legalizacion_Recaudo")
                lonNumDocLegRec = dtsEncabezado.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento_Legalizacion_Recaudo").ToString()
                strFechLega = Retorna_Fecha_Formato_SIESA(dtsEncabezado.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha_Legalizacion_Recaudo").ToString())
                strCodiOficLega = dtsEncabezado.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Oficina_Legaliza").ToString()
                strCodiOficLegaSIESA = dtsEncabezado.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_SIESA_Oficina_Legaliza").ToString()
                strNotas = "LEGALIZACION RECAUDO No." & lonNumDocLegRec.ToString()

                xmlDocumento = New XmlDocument
                'xmlDeclaracion = xmlDocumento.CreateXmlDeclaration("1.0", "ISO-8859-1", "")
                'xmlDocumento.AppendChild(xmlDeclaracion)
                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                ' DOCUMENTO CONTABLE
                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("Documentocontable")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                ' F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina del Tesorero que legaliza el recaudo
                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec) ' Documento Legalización Recaudo
                ' F350_FECHA (Alfanumerico(YYYYMMDD))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechLega) ' Fecha Legalización Recaudo
                ' F350_ID_TERCERO (Alfanumerico(15))
                strIdenCond = "79540540"
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenCond) ' AJUSTAR: Guardar en Legalización Recaudo Guía Responsable Oficina (Agencista) o el Aforador
                ' F350_NOTAS (Alfanumerico(255))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strNotas)

                strSQL = "gsp_Consultar_Detalle_Legalizacion_Recaudo_SIESA_ENCOE " & intCodigoEmpresa.ToString() & "," & lonNumLegRec.ToString()
                dtsDetalle = objGeneral.Retorna_Dataset(strSQL)

                ' Se recorre el detalle con las Guías de la Legalización Recaudo
                For intCon1 = 0 To dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                    lonNumeReme = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Numero_Remesa")
                    lonNumeDocuReme = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Numero_Documento_Remesa").ToString()
                    strFechPago = Retorna_Fecha_Formato_SIESA(dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Fecha_Pago").ToString())
                    strFechReme = Retorna_Fecha_Formato_SIESA(dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Fecha_Remesa").ToString())
                    strIdenCond = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Identificacion_Conductor").ToString()
                    strIdenAfor = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Identificacion_Aforador").ToString()
                    lonFormPagoVent = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Forma_Pago_Venta") ' 4902-Contado, 4903-Contra Entrega
                    lonFormPagoReca = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Forma_Pago_Recaudo") ' 5101-Efectivo, 5102-Transferencia, 5103-Cheque
                    dblValor = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Valor")

                    ' LEGALIZACION RECAUDO EFECTIVO
                    If lonFormPagoReca = FORMA_PAGO_LEGALIZACION_RECAUDO_EFECTIVO Then

                        If lonFormPagoVent = FORMA_PAGO_FACTURA_CONTADO Then

                            strNotas = "LEGALIZACION RECAUDO EFECTIVO CONTADO, No. GUIA: " & lonNumeDocuReme.ToString

                            ' CAJA ENTREGA RECAUDO
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("Caja")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina Facturación Venta Guía
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            strValorCampo = "11050501" ' PARAMETRIZAR 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' Oficina Facturación Venta Guía
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CCOSTO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            ' F351_ID_FE (Alfanumerico(10))
                            strValorCampo = "1132" ' PARAMETRIZAR Flujo de Efectivo
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_FE", strValorCampo)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValor)
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F358_ID_CAJA (Alfanumerico(3))
                            strValorCampo = "R04" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_CAJA", strValorCampo) ' AJUSTAR: Guardar en la Guia la Caja del responsable Venta
                            ' F358_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NOTAS", strNotas)

                            ' CAJA RECIBE RECAUDO 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("Caja")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina Tesorero Legaliza Recaudo
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            strValorCampo = "11050501" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' Oficina Tesorero Legaliza Recaudo
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CCOSTO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            ' F351_ID_FE (Alfanumerico(10))
                            strValorCampo = "1132" ' PARAMETRIZAR Flujo de Efectivo 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_FE", strValorCampo)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValor)
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F358_ID_CAJA (Alfanumerico(3))
                            strValorCampo = "RO4" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_CAJA", strValorCampo) ' AJUSTAR: Guardar en la Legalización Recaudo Guias la Caja del Tesorero
                            ' F358_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NOTAS", strNotas)

                        End If

                        If lonFormPagoVent = FORMA_PAGO_FACTURA_CONTRAENTREGA Then

                            strNotas = "LEGALIZACION RECAUDO EFECTIVO CONTRAENTREGA, No. GUIA: " & lonNumeDocuReme.ToString

                            ' MOVIMIENTO CxC 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoCxC")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina Facturación Venta Guía
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            strValorCampo = "13050520" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenAfor) ' PENDIENTE Responsable de realizar la Entrega (Aforador o Agencista)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' Oficina Facturación Venta Guía
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_VALOR_DB (Decimal(21))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValor)
                            ' F351_VALOR_CR (Decimal(21))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "")
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F353_ID_SUCURSAL (Alfanumerico(3)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "001") ' PENDIENTE 
                            ' F353_ID_TIPO_DOCTO_CRUCE (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_TIPO_DOCTO_CRUCE", "LCE") ' PARAMETRIZAR
                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocLegRec)
                            ' F353_NRO_CUOTA_CRUCE (Entero(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "0")
                            ' F353_FECHA_VCTO (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechReme)
                            ' F353_FECHA_DSCTO_PP (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechReme)
                            ' F354_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strNotas)

                            ' CAJA RECIBE RECAUDO 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("Caja")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina Tesorero Legaliza Recaudo
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            strValorCampo = "11050501" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' Oficina Tesorero Legaliza Recaudo
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CCOSTO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            ' F351_ID_FE (Alfanumerico(10))
                            strValorCampo = "1103" ' PARAMETRIZAR Flujo de Efectivo 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_FE", strValorCampo)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValor)
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F358_ID_CAJA (Alfanumerico(3))
                            strValorCampo = "RO4" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_CAJA", strValorCampo) ' PENDIENTE Caja del Tesorero, AJUSTAR: Guardar en la Legalización Recaudo Guias la Caja del Tesorero 
                            ' F358_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NOTAS", strNotas)
                        End If

                    End If

                    ' LEGALIZACION RECAUDO BANCOS 
                    If lonFormPagoReca = FORMA_PAGO_LEGALIZACION_RECAUDO_TRANSFERENCIA Or lonFormPagoReca = FORMA_PAGO_LEGALIZACION_RECAUDO_CHEQUE Then

                        ' AJUSTE: Revision 14-JUL-2021
                        ' Generar un registro de  MovimientoContable Agrupado por Documento Pago independiente para Contado y Contraentrega y no por cada Guía

                        If lonFormPagoVent = FORMA_PAGO_FACTURA_CONTADO Then

                            strNotas = "LEGALIZACION RECAUDO BANCOS CONTADO, No. GUIA: " & lonNumeDocuReme.ToString

                            ' CAJA RECIBE RECAUDO 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("Caja")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico (3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' PENDIENTE Oficina Factura Venta
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            strValorCampo = "11050501" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' PENDIENTE Oficina Factura Venta
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CCOSTO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            ' F351_ID_FE (Alfanumerico(10))
                            strValorCampo = "4202" ' PARAMETRIZAR Flujo de Efectivo 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_FE", strValorCampo)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValor)
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F358_ID_CAJA (Alfanumerico(3))
                            strValorCampo = "AN2" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_ID_CAJA", strValorCampo) ' PENDIENTE Caja Realizo Venta
                            ' F358_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F358_NOTAS", strNotas)

                        End If

                        If lonFormPagoVent = FORMA_PAGO_FACTURA_CONTRAENTREGA Then

                            strNotas = "LEGALIZACION RECAUDO BANCOS CONTRAENTREGA, No. GUIA: " & lonNumeDocuReme.ToString

                            ' MOVIMIENTO CxC 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoCxC")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina Recibe Recaudo
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F351_ID_AUXILIAR (Alfanumerico(20))
                            strValorCampo = "13050520" ' PARAMETRIZACION PENDIENTE 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenAfor) ' PENDIENTE Aforador o Entrega Oficina
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' PENDIENTE Oficina Factura Contra Entrega
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_VALOR_DB (Decimal(21))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            ' F351_VALOR_CR (Decimal(21))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValor)
                            ' F351_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F353_ID_SUCURSAL (Alfanumerico(3)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "001") ' PENDIENTE Sucursal del Conductor
                            ' F353_ID_TIPO_DOCTO_CRUCE (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_TIPO_DOCTO_CRUCE", "LCE") ' PARAMETRIZAR CONSTANTE
                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocLegRec)
                            ' F353_NRO_CUOTA_CRUCE (Entero(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "0")
                            ' F353_FECHA_VCTO (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechReme)
                            ' F353_FECHA_DSCTO_PP (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechReme)
                            ' F354_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strNotas)

                        End If

                    End If

                Next

                ' Para la contrapardida en LEGALIZACION RECAUDO BANCOS se genera un registro de MovimientoContable Agrupado por:
                ' Forma Pago Recaudo (Transferencia, Cheque) y Documento Pago  
                strSQL = "SELECT CATA_FPDC_Codigo AS Forma_Pago_Recaudo, Documento_Pago, SUM(Valor) AS Valor FROM Detalle_Legalizacion_Recaudo_Guias"
                strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString()
                strSQL += " And ELRG_Numero = " & lonNumLegRec.ToString()
                strSQL += " AND CATA_FPDC_Codigo IN (5102, 5103)" 'TRANSFERENCIA, CHEQUE
                strSQL += " GROUP BY CATA_FPDC_Codigo, Documento_Pago"

                dtsDetalle = objGeneral.Retorna_Dataset(strSQL)
                If dtsDetalle.Tables.Count > 0 Then
                    If dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                        For intCon1 = 0 To dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                            'lonFormPagoVent = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Forma_Pago_Venta") ' 4902-Contado, 4903-Contra Entrega
                            lonFormPagoReca = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Forma_Pago_Recaudo") ' 5102-Transferencia, 5103-Cheque
                            dblValor = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Valor")
                            strDocuPago = dtsDetalle.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Documento_Pago")
                            If lonFormPagoReca = FORMA_PAGO_LEGALIZACION_RECAUDO_TRANSFERENCIA Then
                                strTipoDocuPago = "CG"
                            ElseIf lonFormPagoReca = FORMA_PAGO_LEGALIZACION_RECAUDO_CHEQUE Then
                                strTipoDocuPago = "CH"
                            End If

                            ' MOVIMIENTO CONTABLE  

                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F351_NOTAS (Alfanumerico(255))
                            strNotas = "LEGALIZACION RECAUDO BANCOS SUMATORIA FORMA PAGO RECAUDO"
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strNotas)
                            ' F351_NRO_DOCTO_BANCO (Entero(8))a
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NRO_DOCTO_BANCO", strDocuPago) ' No Transferencia o No. Cheque se ingresa en la Legalización
                            ' F351_DOCTO_BANCO (Alfanumerico(2))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_DOCTO_BANCO", strTipoDocuPago)
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValor)
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                            ' F351_ID_FE (Alfanuerico(10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_FE", "4203") ' 4203-Transferencia 4102-Consignación Cheque
                            ' F351_ID_CCOSTO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", UNIDAD_NEGOCIO_SIESA)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", strCodiOficLegaSIESA) ' Oficina Tesorero Recibe Recaudo
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenAfor) ' PENDIENTE: Tercero que entrega el dinero, se envio por correo pregunta?
                            ' F351_ID_AUXILIAR (Entero (10))
                            strValorCampo = "11100503"
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo) ' PENDIENTE: Solicitar la Cuenta Bancaria en el detalle de la Legalización Guías?
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocLegRec)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_RECAUDO_SIESA)
                            'F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", strCodiOficLegaSIESA) ' Oficina Tesorero Recibe Recaudo

                        Next

                    End If
                End If

                strXML = xmlDocumento.InnerXml
                Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                strPlanoXML = objGenericTransfer.ImportarDatosXML(105885, "LEGALIZACION RECAUDO", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                If strPlanoXML = "Importacion exitosa" Then

                    ' Actualizar el Detalle_Legalizacion_Recaudo_Guias con Interfase_Contable = 1 y Fecha_Interfase_Contable = getDate()
                    strSQL = "UPDATE Encabezado_Legalizacion_Recaudo_Guias SET Interfaz_Contable = 1, Fecha_Interfase_Contable = getDate()"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumLegRec

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                Else
                    strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                    ' Actualizar el Detalle_Legalizacion_Recaudo_Guias con Interfaz_Contable = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                    strSQL = "UPDATE Encabezado_Legalizacion_Recaudo_Guias SET Interfaz_Contable = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumLegRec

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                End If

                lonNumLegRecAnt = lonNumLegRec

            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Crear_XML_Proceso_Legalizacion_Recaudo: " & ex.Message)
        End Try

    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 01-JUL-2021
    ' Observaciones Creación: Se consulta la Legalizacion Gastos Conductor
    ' Modifica: 
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Proceso_Legalizacion_Gastos_Conductor()
        Try
            Dim dtsConsulta As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Legalizacion_Gastos_Conductor")
                strSQL = "gsp_Consultar_Encabezado_Legalizacion_Gastos_SIESA_ENCOE " & intCodigoEmpresa.ToString & ", 209" ' TEMPORAL PRUEBAS No. Legalizacion 1000009 (Masivo) y 209-1000028(Paqueteria)
                dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

                If dtsConsulta.Tables.Count > 0 Then

                    If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Me.objGeneral.Guardar_Mensaje_Log("Número Legalización Gastos Conductor a Procesar: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)

                        Call Crear_XML_Proceso_Legalizacion_Gastos_Conductor(dtsConsulta)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Legalizacion_Gastos_Conductor: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 21-JUL-2021
    ' Observaciones Creación: Se crea XML de la Legalizacion Gastos Conductor
    ' Modifica: 
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Crear_XML_Proceso_Legalizacion_Gastos_Conductor(ByVal dtsEncaLegaGast As DataSet)
        Try
            Dim strFechLega As String, lonCodiCond As Long, lonNumPlaDes As Long, strPlacVehi As String, strIdenCond As String, dblSaldFavoEmpr As Double, dblSaldFavoCond As Double
            Dim strIdenProv As String, intCodiConc As String, strNombConc As String, dblValoConcDebi As Double, dblValoConcCred As Double
            Dim intCon As Integer, intCon2 As Integer, strValorCampo As String, lonNumeDocuLega As Long, lonNumeLega As Long
            Dim strXML As String, strPlanoXML As String, strErrorDetalleSIESA As String

            Dim xmlDocumento As XmlDocument
            'Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeLega = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeDocuLega = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()
                strFechLega = Retorna_Fecha_Formato_SIESA(dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                lonCodiCond = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("TERC_Codigo_Conductor").ToString()
                strIdenCond = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Conductor").ToString()
                dblSaldFavoEmpr = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Saldo_Favor_Empresa")
                dblSaldFavoCond = dtsEncaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Saldo_Favor_Conductor")

                xmlDocumento = New XmlDocument

                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                ' DOCUMENTO CONTABLE
                xmlElementoRoot = xmlDocumento.DocumentElement
                xmlElemento = xmlDocumento.CreateElement("DocumentoContable")
                xmlElementoRoot.AppendChild(xmlElemento)
                xmlElementoRoot = xmlDocumento.DocumentElement

                'F350_ID_CO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA) ' AJUSTAR: Oficina Legalizar
                ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_GASTOS_CONDUCTOR_SIESA)
                ' F350_CONSEC_DOCTO (Entero(8))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuLega.ToString) ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria
                ' F350_FECHA (Alfanumerico(YYYYMMDD))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechLega)
                ' F350_ID_TERCERO (Alfanumerico(15))
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenCond)
                ' F350_NOTAS (Alfanumerico(2000))
                strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString
                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strValorCampo)

                ' Consultar Detalle Legalizacion Gastos
                Dim dtsDetaLegaGast As DataSet

                strSQL = "gsp_Consultar_Detalle_Legalizacion_Gastos_SIESA_ENCOE " & intCodigoEmpresa.ToString() & ", " & lonNumeLega.ToString()
                dtsDetaLegaGast = objGeneral.Retorna_Dataset(strSQL)

                If dtsDetaLegaGast.Tables.Count > 0 Then
                    If dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        ' Recorrer los Conceptos de Gasto de la Planilla Legalización
                        For intCon2 = 0 To dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                            strIdenProv = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Identificacion_Proveedor").ToString()
                            intCodiConc = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Codigo_Concepto").ToString()
                            strNombConc = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Nombre_Concepto").ToString()
                            dblValoConcDebi = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Debito").ToString()
                            dblValoConcCred = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Credito").ToString()
                            lonNumPlaDes = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("ENPD_Numero_Documento")
                            strPlacVehi = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Placa")

                            ' MOVIMIENTO CONTABLE (Conceptos de Gasto) 
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_GASTOS_CONDUCTOR_SIESA)
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuLega) ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria, para que sea unico
                            ' F351_ID_AUXILIAR (Entero (10))
                            strValorCampo = dtsDetaLegaGast.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Codigo_Cuenta").ToString()
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strValorCampo)
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenProv)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "300401") ' FIJO PARAMETRIZAR
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValoConcDebi)
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoConcCred)
                            ' F351_BASE_GRAVABLE (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", "0")
                            ' F351_NOTAS (Alfanumerico(255))
                            strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", No. PLANILLA:" & lonNumPlaDes & ", PLACA:" & strPlacVehi & ", CONCEPTO GASTO:" & strNombConc
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)

                        Next

                        ' Se consultan los Comprobante Egreso Anticipo relacionado con la Legalizacion Gastos y el Conductor para crear una CxP por cada una
                        Dim dtsCompEgreAnti As DataSet, lonNumeCompEgreAnti As Long, dblValoCompEgreAnti As Double, strFechCompEgre As String

                        strSQL = "SELECT ENDC.Numero, ENDC.Fecha, ENDC.Valor_Pago_Recaudo_Total, TERC.Numero_Identificacion"
                        strSQL += " FROM Encabezado_Documento_Comprobantes ENDC, Terceros As TERC"
                        strSQL += " WHERE ENDC.EMPR_Codigo = TERC.EMPR_Codigo"
                        strSQL += " AND ENDC.TERC_Codigo_Titular = TERC.Codigo"
                        strSQL += " AND ENDC.EMPR_Codigo = " & intCodigoEmpresa.ToString
                        strSQL += " AND ENDC.TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO
                        strSQL += " AND ENDC.ELGC_Numero = " & lonNumeLega.ToString() ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria, para que sea unico
                        strSQL += " AND ENDC.TERC_Codigo_Titular = " & lonCodiCond.ToString
                        strSQL += " AND ENDC.Estado = 1"
                        strSQL += " AND ENDC.Anulado = 0"

                        dtsCompEgreAnti = objGeneral.Retorna_Dataset(strSQL)

                        If dtsCompEgreAnti.Tables.Count > 0 Then
                            If dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                For intCont3 = 0 To dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                                    lonNumeCompEgreAnti = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Numero").ToString()
                                    dblValoCompEgreAnti = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Valor_Pago_Recaudo_Total").ToString()
                                    strIdenCond = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Numero_Identificacion").ToString()
                                    strFechCompEgre = Retorna_Fecha_Formato_SIESA(dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Fecha").ToString())

                                    ' MOVIMIENTO CxP
                                    xmlElementoRoot = xmlDocumento.DocumentElement
                                    xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                    xmlElementoRoot.AppendChild(xmlElemento)
                                    xmlElementoRoot = xmlDocumento.DocumentElement

                                    'F350_ID_CO (Alfanumerico(3))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                    ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LEGALIZA_GASTOS_CONDUCTOR_SIESA)
                                    ' F350_CONSEC_DOCTO (Entero(8))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumeDocuLega.ToString)
                                    ' F351_ID_AUXILIAR (Entero (10))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "23654001") ' PARAMETRIZAR
                                    ' F350_ID_TERCERO (Alfanumerico(15))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond)
                                    ' F351_ID_CO_MOV (Alfanumerico(3))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                    ' F351_ID_UN (Alfanumerico(20))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                    ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                    ' F351_VALOR_DB (Decimal)
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                                    ' F351_VALOR_CR (Decimal)
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoCompEgreAnti.ToString)
                                    ' F351_NOTAS (Alfanumerico(255))
                                    strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", No. EGRESO:" & lonNumeCompEgreAnti.ToString & ", No.PLANILLA:" & lonNumPlaDes.ToString
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                                    ' F353_ID_SUCURSAL (Alfanumerico(3))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "PRV") ' PREGUNTA: OFICINA SIESA CONDUCTOR?
                                    ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", "PPL") ' PARAMETRIZAR FIJO
                                    ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumPlaDes.ToString) ' No Planilla Despacho
                                    ' F353_NRO_CUOTA_CRUCE (Entero(3))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "1")
                                    ' F353_ID_FE (Alfanumerico(10))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "")
                                    ' F353_NRO_CUOTA_CRUCE (Entero(3))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_NRO_CUOTA_CRUCE", "1")
                                    ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLega)
                                    ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLega)
                                    ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLega)
                                    ' F354_NOTAS (Alfanumerico(255))
                                    strValorCampo = "No.LEGALIZACION:" & lonNumeDocuLega.ToString & ", No. EGRESO:" & lonNumeCompEgreAnti.ToString & ", No.PLANILLA:" & lonNumPlaDes.ToString
                                    AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                                Next

                            End If
                        End If

                        strXML = xmlDocumento.InnerXml
                        Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                        ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                        Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                        strPlanoXML = objGenericTransfer.ImportarDatosXML(105686, "LEGALIZACION GASTOS CONDUCTOR", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                        If strPlanoXML = "Importacion exitosa" Then

                            ' Actualizar Encabezado_Legalizacion_Recaudo_Guias con Interfase_Contable = 1 y Fecha_Interfase_Contable = getDate()
                            strSQL = "UPDATE Encabezado_Legalizacion_Gastos_Conductor SET Interfaz_Contable_Legalizacion = 1, Fecha_Interfase_Contable = getDate()"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeLega

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        Else
                            strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                            ' Actualizar Encabezado_Legalizacion_Recaudo_Guias con Interfaz_Contable = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                            strSQL = "UPDATE Encabezado_Legalizacion_Gastos_Conductor SET Interfaz_Contable_Legalizacion = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeLega

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        End If

                    End If
                End If
            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Crear_XML_Proceso_Legalizacion_Gastos_Conductor: " & ex.Message)

        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 27-AGO-2021
    ' Observaciones Creación: Se consulta las Liquidaciones Despacho
    ' Modifica: 
    ' Fecha Modificación: 
    ' Observaciones Modificación: 

    Private Sub Proceso_Liquidacion_Planilla_Despachos()
        Try
            Dim dtsConsulta As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Liquidacion_Despachos")
                strSQL = "gsp_Consultar_Encabezado_Liquidacion_Planilla_Despachos_SIESA_ENCOE " & intCodigoEmpresa.ToString & ", 40255" ' TEMPORAL PRUEBAS No. Liquidación Paqueteria 1000020
                dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

                If dtsConsulta.Tables.Count > 0 Then

                    If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Me.objGeneral.Guardar_Mensaje_Log("Número Liquidación Despachos a Procesar: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)

                        Call Crear_XML_Proceso_Liquidacion_Planilla_Despachos(dtsConsulta)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Liquidacion_Despachos: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 27-AGO-2021
    ' Observaciones Creación: Se crea XML de la Liquidación Planilla Despachos

    ' Modifica: Jesús Cuadros
    ' Fecha Modificación: 13-SEP-2021
    ' Observaciones Modificación: Nuevo XML

    ' Modifica: Jesús Cuadros
    ' Fecha Modificación: 19-OCT-2021
    ' Observaciones Modificación: Nuevo XML

    Private Sub Crear_XML_Proceso_Liquidacion_Planilla_Despachos(ByVal dtsEncaLiquPlan As DataSet)
        Try
            Dim intTipoLiquDesp As Integer, lonNumeLiqu As Long, lonNumeDocuLiqu As Long, strFechLiqu As String, lonNumePlanDesp As Long, lonNumDocPlaDes As Long
            Dim strPlacVehi As String, strCodiVehi As String, lonCodiCond As Long, strIdenCond As String, strIdenTene As String
            Dim dblValorPagar As Double, dblValFleTra As Double, dblValoImpuLiqu As Double
            Dim intCodiConc As String, strNombConc As String, dblValoConcDebi As Double, dblValoConcCred As Double, strCodCtaPuc As String, lonCodiDocuComp As Long
            Dim strXML As String, strPlanoXML As String, strErrorDetalleSIESA As String
            Dim intCon As Integer, intCon1 As Integer, strValorCampo As String, intFL As Integer
            Dim dtsConsulta As DataSet, dblPorcTeneVehi As Double, dblValRetFue As Double, dblValRetIca As Double, dblValConLiq As Double

            Dim xmlDocumento As XmlDocument
            'Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                intTipoLiquDesp = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("TIDO_Codigo").ToString() ' M
                lonNumeLiqu = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeDocuLiqu = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()
                lonNumePlanDesp = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Planilla").ToString()
                lonNumDocPlaDes = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento_Planilla").ToString()
                'lonNumDocPlaDes = lonNumDocPlaDes + 2 ' ' TEMPORAL PRUEBA
                strFechLiqu = Retorna_Fecha_Formato_SIESA(dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                lonCodiCond = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Conductor").ToString()
                strIdenCond = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Conductor").ToString()
                strIdenTene = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Tenedor")
                dblValorPagar = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Pagar")
                dblValFleTra = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Flete_Transportador")
                dblValoImpuLiqu = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Impuestos")
                strPlacVehi = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Placa_Vehiculo")
                strCodiVehi = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Vehiculo")
                dblValRetFue = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Retefuente")
                dblValRetIca = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Reteica")
                dblValConLiq = dtsEncaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Valor_Conceptos_Liquidacion")

                xmlDocumento = New XmlDocument

                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                ' CONSULTAR LOS TENEDORES DEL VEHICULO PARA CREAR UN COMPROBANTE POR CADA TENEDOR CON BASE EN SU % PROPIEDAD
                strSQL = "SELECT VEPR.TERC_Codigo_Tenedor, VEPR.Porcentaje_Propiedad, TERC.Numero_Identificacion AS Identificacion_Tenedor"
                strSQL += " FROM Vehiculo_Propietarios AS VEPR, Terceros AS TERC"
                strSQL += " WHERE VEPR.EMPR_Codigo = TERC.EMPR_Codigo"
                strSQL += " AND VEPR.TERC_Codigo_Tenedor = TERC.Codigo"
                strSQL += " AND VEPR.EMPR_codigo = " & intCodigoEmpresa.ToString()
                strSQL += " AND VEPR.VEHI_Codigo = " & strCodiVehi

                dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

                If dtsConsulta.Tables.Count > 0 Then
                    If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        For intCon1 = 0 To dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                            ' DOCUMENTO CONTABLE
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("DocumentoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            strIdenTene = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Identificacion_Tenedor")
                            dblPorcTeneVehi = dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon1).Item("Porcentaje_Propiedad")
                            intFL = intCon1 + 1

                            'F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA) ' PENDIENTE: Oficina Liquida
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString) ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria
                            ' F350_FECHA (Alfanumerico(YYYYMMDD))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_FECHA", strFechLiqu)
                            ' F350_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TERCERO", strIdenTene)
                            ' F350_NOTAS (Alfanumerico(2000))
                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString() & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString()
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_NOTAS", strValorCampo)


                            ' MOVIMIENTO CONTABLE - FLETE TRANSPORTADOR
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes) ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria, para que sea unico
                            ' F351_ID_AUXILIAR (Entero (10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "75050501") ' FIJO PARAMETRIZAR
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "300301") ' FIJO PARAMETRIZAR
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValFleTra * dblPorcTeneVehi / 100)
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                            ' F351_BASE_GRAVABLE (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", "0")
                            ' F351_NOTAS (Alfanumerico(255))
                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString()
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)


                            ' MOVIMIENTO CONTABLE - RETENCION ICA
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes) ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria, para que sea unico
                            ' F351_ID_AUXILIAR (Entero (10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "236801032") ' FIJO PARAMETRIZAR
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "") ' FIJO PARAMETRIZAR
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValRetIca * dblPorcTeneVehi / 100)
                            ' F351_BASE_GRAVABLE (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", dblValFleTra) ' PREGUNTAR: Los otros conceptos que afectan base?
                            ' F351_NOTAS (Alfanumerico(255))
                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString()
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)


                            ' MOVIMIENTO CONTABLE - RETENCION FUENTE
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            ' F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes) ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria, para que sea unico
                            ' F351_ID_AUXILIAR (Entero (10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "23652501") ' FIJO PARAMETRIZAR
                            ' F351_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "") ' FIJO PARAMETRIZAR
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            ' F351_VALOR_CR (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValRetFue * dblPorcTeneVehi / 100)
                            ' F351_BASE_GRAVABLE (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", dblValFleTra) ' PREGUNTAR: Los otros conceptos?
                            ' F351_NOTAS (Alfanumerico(255))
                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString()
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)


                            ' MOVIMIENTO CxP - VALOR A PAGAR
                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            'F350_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                            ' F350_CONSEC_DOCTO (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                            ' F351_ID_AUXILIAR (Entero (10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "23354501") ' FIJO PARAMETRIZAR
                            ' F350_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                            ' F351_ID_CO_MOV (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                            ' F351_ID_UN (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                            ' F351_VALOR_DB (Decimal)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                            ' F351_VALOR_CR (Decimal)

                            dblValorPagar = dblValFleTra - dblValRetFue - dblValRetIca + 500000 ' PENDIENTE Ajustar este cálculo Mayor Valor Flete

                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValorPagar * dblPorcTeneVehi / 100)
                            ' F351_NOTAS (Alfanumerico(255))
                            strValorCampo = "No.LIQUIDACION:" & lonNumeDocuLiqu.ToString & ", No.PLANILLA:" & lonNumDocPlaDes.ToString
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                            ' F353_ID_SUCURSAL (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "178") ' PENDIENTE PARAMETRIZAR SUCURSAL SIESA EN VEHICULOS 
                            ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocPlaDes.ToString) ' No Planilla Despacho
                            ' F353_ID_FE (Alfanumerico(10))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", "1214") ' FIJO PARAMETRIZAR
                            ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLiqu)
                            ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLiqu)
                            ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLiqu)
                            ' F354_NOTAS (Alfanumerico(255))
                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)


                            ' CONSULTAR LOS CONCEPTOS DE LA LIQUIDACION
                            Dim dtsDetaLiquPlan As DataSet

                            strSQL = "gsp_Consultar_Detalle_Liquidacion_Planilla_Despachos_SIESA_ENCOE " & intCodigoEmpresa.ToString() & ", " & lonNumeLiqu.ToString()
                            dtsDetaLiquPlan = objGeneral.Retorna_Dataset(strSQL)

                            If dtsDetaLiquPlan.Tables.Count > 0 Then
                                If dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                    For intCon2 = 0 To dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                                        intCodiConc = dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Codigo_Concepto").ToString()
                                        strNombConc = dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Nombre_Concepto").ToString()
                                        dblValoConcDebi = dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Debito").ToString()
                                        dblValoConcCred = dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Credito").ToString()
                                        strCodCtaPuc = dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Codigo_Cuenta").ToString()
                                        lonCodiDocuComp = dtsDetaLiquPlan.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Codigo_Documento_Comprobante").ToString() ' Comprobante Egreso Anticipo

                                        If intCodiConc = CONCEPTO_FONDO_RESPONSABILIDAD_LIQUIDACION_PLANILLA_DESPACHOS Then

                                            ' Causación Fondo Responsabilidad
                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            'F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            strCodCtaPuc = "28101504" ' PENDIENTE Parametrizar Concepto
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strCodCtaPuc)
                                            ' F350_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0") ' PREGUNTA: Conceptos que suman?
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoConcDebi * dblPorcTeneVehi / 100)
                                            ' F351_NOTAS (Alfanumerico(255))
                                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                                            ' F353_ID_SUCURSAL (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "PRV") ' PREGUNTA: SUCURSAL SIESA QUE CORRESPONDE AL VEHICULO DE LA PLANILLA?
                                            ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intCon1.ToString())
                                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocPlaDes.ToString) ' No Planilla Despacho
                                            ' F353_ID_FE (Alfanumerico(10))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", "1214") ' FIJO PARAMETRIZAR
                                            ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLiqu)
                                            ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLiqu)
                                            ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLiqu)
                                            ' F354_NOTAS (Alfanumerico(255))
                                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)


                                            ' Abono Fondo Responsabilidad
                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            'F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            strCodCtaPuc = "23354501" ' PENDIENTE Parametrizar Concepto
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strCodCtaPuc)
                                            ' F350_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValoConcDebi * dblPorcTeneVehi / 100)
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                                            ' F351_NOTAS (Alfanumerico(255))
                                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)
                                            ' F353_ID_SUCURSAL (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "178") ' PENDIENTE PARAMETRIZAR SUCURSAL VEHICULO
                                            ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", "FDR")
                                            ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocPlaDes.ToString) ' No Planilla Despacho
                                            ' F353_ID_FE (Alfanumerico(10))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", "1214") ' FIJO PARAMETRIZAR
                                            ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLiqu)
                                            ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLiqu)
                                            ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLiqu)
                                            ' F354_NOTAS (Alfanumerico(255))
                                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString & ", No.LIQUIDACION:" & lonNumeDocuLiqu.ToString
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)


                                        ElseIf intCodiConc = CONCEPTO_MAYOR_VALOR_FLETE_LIQUIDACION_PLANILLA_DESPACHOS Then

                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            'F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            strCodCtaPuc = "75050501" ' PENDIENTE Parametrizar en Concepto
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strCodCtaPuc)
                                            ' F350_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "300301") ' FIJO PARAMETRIZAR
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValoConcCred * dblPorcTeneVehi / 100)
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", 0)
                                            ' F351_BASE_GRAVABLE (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", 0)
                                            ' F351_NOTAS (Alfanumerico(255))
                                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)


                                        ElseIf intCodiConc = CONCEPTO_MENOR_VALOR_FLETE_LIQUIDACION_PLANILLA_DESPACHOS Then

                                            xmlElementoRoot = xmlDocumento.DocumentElement
                                            xmlElemento = xmlDocumento.CreateElement("MovimientoContable")
                                            xmlElementoRoot.AppendChild(xmlElemento)
                                            xmlElementoRoot = xmlDocumento.DocumentElement

                                            'F350_ID_CO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                            ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                                            ' F350_CONSEC_DOCTO (Entero(8))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                                            ' F351_ID_AUXILIAR (Entero (10))
                                            strCodCtaPuc = "75050501" ' PENDIENTE Parametrizar en Concepto
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", strCodCtaPuc)
                                            ' F350_ID_TERCERO (Alfanumerico(15))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                                            ' F351_ID_CO_MOV (Alfanumerico(3))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                            ' F351_ID_UN (Alfanumerico(20))
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                            ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "300301") ' FIJO PARAMETRIZAR
                                            ' F351_VALOR_DB (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", 0)
                                            ' F351_VALOR_CR (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoConcDebi * dblPorcTeneVehi / 100)
                                            ' F351_BASE_GRAVABLE (Decimal)
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_BASE_GRAVABLE", 0)
                                            ' F351_NOTAS (Alfanumerico(255))
                                            strValorCampo = "No.PLANILLA:" & lonNumDocPlaDes.ToString
                                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_NOTAS", strValorCampo)

                                        End If

                                    Next

                                End If
                            End If

                            ' CONSULTAR LOS COMOPROBANTES EGRESO DE ANTICIPO RELACIONADOS A LA PLANILLA LIQUIDADA
                            Dim dtsCompEgreAnti As DataSet, lonNumeCompEgreAnti As Long, dblValoCompEgreAnti As Double, strFechCompEgre As String

                            strSQL = "SELECT ENDC.Numero, ENDC.Fecha, ENDC.Valor_Pago_Recaudo_Total, TERC.Numero_Identificacion"
                            strSQL += " FROM Encabezado_Documento_Comprobantes ENDC, Terceros As TERC"
                            strSQL += " WHERE ENDC.EMPR_Codigo = TERC.EMPR_Codigo"
                            strSQL += " AND ENDC.TERC_Codigo_Titular = TERC.Codigo"
                            strSQL += " AND ENDC.EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND ENDC.TIDO_Codigo = " & TIDO_COMPROBANTE_EGRESO
                            ' FALTA Guardar en los Comprobantes de Egreso el ELPD_Numero como se tiene para ELGC_Numero (Encabezado Legalizacion Gastos Conductor)
                            If intTipoLiquDesp = TIDO_LIQUIDACION_PLANILLA_PAQUETERIA Then
                                strSQL += " AND ENDC.CATA_DOOR_Codigo = 2613" '2613=Anticipo Planilla Paqueteria 
                            Else
                                strSQL += " AND ENDC.CATA_DOOR_Codigo IN (2604, 2605)"  '2604=Anticipos Planilla Masivo 2605=SobreAnticipo 
                            End If
                            strSQL += " AND ENDC.Documento_Origen = " & lonNumDocPlaDes.ToString() ' PENDIENTE: Los consecutivos deben ser diferentes para Masivo y Paqueteria, para que sea unico
                            strSQL += " AND ENDC.TERC_Codigo_Titular = " & lonCodiCond.ToString
                            'strSQL += " AND ENDC.Estado = 1"
                            'strSQL += " AND ENDC.Anulado = 0"

                            dtsCompEgreAnti = objGeneral.Retorna_Dataset(strSQL)

                            If dtsCompEgreAnti.Tables.Count > 0 Then
                                If dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                                    For intCont3 = 0 To dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                                        lonNumeCompEgreAnti = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Numero").ToString()
                                        dblValoCompEgreAnti = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Valor_Pago_Recaudo_Total").ToString()
                                        strIdenCond = dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Numero_Identificacion").ToString()
                                        strFechCompEgre = Retorna_Fecha_Formato_SIESA(dtsCompEgreAnti.Tables(clsGeneral.PRIMER_TABLA).Rows(intCont3).Item("Fecha").ToString())

                                        ' MOVIMIENTO CxP ANTICIPO CONDUCTOR
                                        xmlElementoRoot = xmlDocumento.DocumentElement
                                        xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                        xmlElementoRoot.AppendChild(xmlElemento)
                                        xmlElementoRoot = xmlDocumento.DocumentElement

                                        'F350_ID_CO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                        ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                                        ' F350_CONSEC_DOCTO (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                                        ' F351_ID_AUXILIAR (Entero (10))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "13301502") ' FIJO PARAMETRIZAR
                                        ' F350_ID_TERCERO (Alfanumerico(15))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenCond)
                                        ' F351_ID_CO_MOV (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                        ' F351_ID_UN (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                        ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                        ' F351_VALOR_DB (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", "0")
                                        ' F351_VALOR_CR (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", dblValoCompEgreAnti * dblPorcTeneVehi / 100)
                                        ' F353_ID_SUCURSAL (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "ANT") ' PREGUNTA: SUCURSAL SIESA CONDUCTOR? RPTA: EL CONDUCTOR TIENE UNA SUCURSAL ASOCIADA
                                        ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", "ANT")
                                        ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocPlaDes.ToString) ' No Planilla Despacho
                                        ' F353_ID_FE (Alfanumerico(10))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", "")
                                        ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLiqu)
                                        ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLiqu)
                                        ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLiqu)
                                        ' F354_NOTAS (Alfanumerico(255))
                                        strValorCampo = "No. EGRESO:" & lonNumeCompEgreAnti.ToString & ", No.PLANILLA:" & lonNumDocPlaDes.ToString
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)

                                        ' MOVIMIENTO CxP ANTICIPO PROPIETARIO
                                        xmlElementoRoot = xmlDocumento.DocumentElement
                                        xmlElemento = xmlDocumento.CreateElement("MovimientoCxP")
                                        xmlElementoRoot.AppendChild(xmlElemento)
                                        xmlElementoRoot = xmlDocumento.DocumentElement

                                        'F350_ID_CO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_CO", CENTRO_OPERACION_SIESA)
                                        ' F350_ID_TIPO_DOCTO (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_ID_TIPO_DOCTO", TIPO_DOCUMENTO_LIQUIDACION_PLANILLA_DESPACHOS_SIESA & intFL.ToString())
                                        ' F350_CONSEC_DOCTO (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F350_CONSEC_DOCTO", lonNumDocPlaDes.ToString)
                                        ' F351_ID_AUXILIAR (Entero (10))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_AUXILIAR", "23354501") ' FIJO PARAMETRIZAR
                                        ' F350_ID_TERCERO (Alfanumerico(15))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_TERCERO", strIdenTene)
                                        ' F351_ID_CO_MOV (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CO_MOV", CENTRO_OPERACION_SIESA)
                                        ' F351_ID_UN (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_UN", "100") ' FIJO PARAMETRIZAR
                                        ' F351_ID_CCOSTO (Alfanumerico(15)) 
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_ID_CCOSTO", "")
                                        ' F351_VALOR_DB (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_DB", dblValoCompEgreAnti * dblPorcTeneVehi / 100)
                                        ' F351_VALOR_CR (Decimal)
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F351_VALOR_CR", "0")
                                        ' F353_ID_SUCURSAL (Alfanumerico(3))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_SUCURSAL", "178") ' PENDIENTE PARAMETRIZAR SUCURSAL VEHICULO
                                        ' F353_PREFIJO_CRUCE (Alfanumerico(20))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_PREFIJO_CRUCE", "ANT")
                                        ' F353_CONSEC_DOCTO_CRUCE (Entero(8))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_CONSEC_DOCTO_CRUCE", lonNumDocPlaDes.ToString) ' No Planilla Despacho
                                        ' F353_ID_FE (Alfanumerico(10))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_ID_FE", "")
                                        ' F353_FECHA_VCTO (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_VCTO", strFechLiqu)
                                        ' F353_FECHA_DSCTO_PP (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DSCTO_PP", strFechLiqu)
                                        ' F353_FECHA_DOCTO_CRUCE (Alfanumerico(YYYYMMDD))
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F353_FECHA_DOCTO_CRUCE", strFechLiqu)
                                        ' F354_NOTAS (Alfanumerico(255))
                                        strValorCampo = "No. EGRESO:" & lonNumeCompEgreAnti.ToString & ", No.PLANILLA:" & lonNumDocPlaDes.ToString
                                        AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F354_NOTAS", strValorCampo)
                                    Next

                                End If
                            End If

                        Next

                        strXML = xmlDocumento.InnerXml
                        Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                        ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                        Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                        strPlanoXML = objGenericTransfer.ImportarDatosXML(111646, "Causacion pago de flete", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                        If strPlanoXML = "Importacion exitosa" Then

                            ' Actualizar Encabezado_Liquidacion_Planilla_Despachos con Interfase_Contable = 1 y Fecha_Interfase_Contable = getDate()
                            strSQL = "UPDATE Encabezado_Liquidacion_Planilla_Despachos SET Interfaz_Contable = 1, Fecha_Interfase_Contable = getDate()"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeLiqu

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        Else
                            strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                            ' Actualizar Encabezado_Liquidacion_Planilla_Despachos con Interfaz_Contable = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                            strSQL = "UPDATE Encabezado_Liquidacion_Planilla_Despachos SET Interfaz_Contable = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                            strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                            strSQL += " AND Numero = " & lonNumeLiqu

                            ' Ejecutar UPDATE
                            lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                        End If
                    End If
                End If

            Next

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Crear_XML_Proceso_Legalizacion_Gastos_Conductor: " & ex.Message)

        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 20-OCT-2021
    ' Observaciones Creación: Se crea XML de la Orden Venta Servicio

    Private Sub Proceso_Orden_Venta_Servicio()
        Try
            Dim dtsConsulta As DataSet

            While (True)
                Me.objGeneral.Guardar_Mensaje_Log("Inició Proceso_Orden_Venta_Servicio")
                strSQL = "gsp_Consultar_Encabezado_Facturas_SIESA_ENCOE " & intCodigoEmpresa.ToString & ", 131430" ' Factura 2000577
                dtsConsulta = objGeneral.Retorna_Dataset(strSQL)

                If dtsConsulta.Tables.Count > 0 Then

                    If dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then

                        Me.objGeneral.Guardar_Mensaje_Log("Número Factura a Procesar: " & dtsConsulta.Tables(clsGeneral.PRIMER_TABLA).Rows.Count)

                        Call Crear_XML_Proceso_Orden_Venta_Servicio(dtsConsulta)

                    End If

                End If

                Thread.Sleep(lonIntervaloMilisegundosEjecucionProceso)

            End While

        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Proceso_Orden_Venta_Servicio: " & ex.Message)
        End Try
    End Sub

    ' Autor: Jesus Cuadros
    ' Fecha Creación: 20-OCT-2021
    ' Observaciones Creación: Se crea XML de la Orden Venta Servicio

    Private Sub Crear_XML_Proceso_Orden_Venta_Servicio(ByVal dtsEncFact As DataSet)
        Try
            Dim lonNumeFact As Long, lonNumeDocuFact As Long, strFechFact As String, lonCodiClie As Long, intCon3 As Integer
            Dim strIdenClie As String, strIdenFact As String, strIdenComeClie As String, strObservaciones As String
            Dim strXML As String, strPlanoXML As String, strErrorDetalleSIESA As String, dtsDetaFact As DataSet
            Dim dblCantReme As Double, dblPesoReme As Double, strOrigen As String, strDestino As String, lonNumeReme As Long
            Dim dblValoFlet As Double, dblValoMane As Double, dblValoReex As Double, dblValoCarg As Double, dblValoDesc As Double

            Dim xmlDocumento As XmlDocument
            'Dim xmlDeclaracion As XmlDeclaration
            Dim xmlElementoRaiz As XmlElement
            Dim xmlElementoRoot As XmlElement
            Dim xmlElemento As XmlElement

            For intCon = 0 To dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                lonNumeFact = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero").ToString()
                lonNumeDocuFact = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Numero_Documento").ToString()
                strFechFact = Retorna_Fecha_Formato_SIESA(dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Fecha").ToString())
                lonCodiClie = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Codigo_Cliente").ToString()
                strIdenClie = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Cliente").ToString()
                strIdenFact = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Facturar")
                strIdenComeClie = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Identificacion_Comercial")
                strObservaciones = dtsEncFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon).Item("Observaciones")

                xmlDocumento = New XmlDocument
                xmlElementoRaiz = xmlDocumento.CreateElement("Conector")
                xmlDocumento.AppendChild(xmlElementoRaiz)

                ' CONSULTAR DETALLE FACTURA
                strSQL = "gsp_Consultar_Detalle_Facturas_SIESA_ENCOE " & intCodigoEmpresa.ToString() & ", " & lonNumeFact.ToString()
                dtsDetaFact = objGeneral.Retorna_Dataset(strSQL)

                If dtsDetaFact.Tables.Count > 0 Then
                    If dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows.Count > 0 Then
                        For intCon2 = 0 To dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows.Count - 1

                            lonNumeReme = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Numero_Documento").ToString
                            lonNumeReme = lonNumeReme + 10 ' TEMPORAL PRUEBA
                            strOrigen = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Ciudad_Origen").ToString
                            strDestino = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Ciudad_Destino").ToString
                            dblCantReme = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Cantidad_Cliente")
                            dblPesoReme = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Peso_Cliente")
                            dblValoFlet = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Flete")
                            dblValoMane = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Seguro")
                            dblValoReex = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Reexpedicion")
                            dblValoCarg = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Cargue")
                            dblValoDesc = dtsDetaFact.Tables(clsGeneral.PRIMER_TABLA).Rows(intCon2).Item("Valor_Descargue")
                            strObservaciones = "Guía:" & lonNumeReme.ToString() & ", Origen:" & strOrigen & ", Destino:" & strDestino

                            xmlElementoRoot = xmlDocumento.DocumentElement
                            xmlElemento = xmlDocumento.CreateElement("OrdenVentaServicios")
                            xmlElementoRoot.AppendChild(xmlElemento)
                            xmlElementoRoot = xmlDocumento.DocumentElement

                            'F310_NOTAS (Alfanumerico(255))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_NOTAS", strObservaciones)
                            ' F310_NUMERO_ORDEN_COMPRA (Alfanumerico(12))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_NUMERO_ORDEN_COMPRA", lonNumeReme.ToString())
                            ' F310_ID_TIPO_CLI (Alfanumerico(4))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_CLI", "0003") ' FIJO CONSTANTE
                            ' F310_ID_COND_PAGO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_COND_PAGO", "30D") ' FIJO CONSTANTE
                            ' F310_REFERENCIA (Alfanumerico(20))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_REFERENCIA", lonNumeDocuFact.ToString())
                            ' F310_ID_TERCERO_VENDEDOR (Alfanumerico(15))
                            strIdenComeClie = "800209179" ' TEMPORAL PRUEBA TERCERO ENCOE
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TERCERO_VENDEDOR", strIdenComeClie)
                            ' F310_ID_SUCURSAL_CLI (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_SUCURSAL_CLI", "CRD") ' FIJO CONSTANTE
                            ' F310_ID_TERCERO (Alfanumerico(15))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TERCERO", strIdenClie)
                            ' F310_FECHA (Alfanumerico(8))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_FECHA", strFechFact)
                            ' F310_CONSEC_DOCTO (Entero)
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_CONSEC_DOCTO", lonNumeReme.ToString()) ' Automático SIESA
                            ' F310_ID_TIPO_DOCTO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_DOCTO", "ODV") ' FIJO CONSTANTE
                            ' F310_ID_CO (Alfanumerico(3))
                            AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_CO", CENTRO_OPERACION_SIESA)

                            If dblValoFlet > 0 Then
                                ' Sección VALOR FLETE
                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentaServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                ' f318_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_DETALLE", strObservaciones)
                                ' f318_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_NOTAS", strObservaciones)
                                ' f318_VLR_BRUTO (DECIMAL(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_VLR_BRUTO", dblValoFlet)
                                ' f318_CANTIDAD (DECIMAL(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_CANTIDAD", dblCantReme)
                                ' F318_ID_SUCURSAL_CLIENTE (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F318_ID_SUCURSAL_CLIENTE", "CRD") ' FIJO CONSTANTE
                                ' f318_ID_TERCERO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_TERCERO_MOVTO", strIdenClie)
                                ' f318_ID_CCOSTO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CCOSTO_MOVTO", "210101") ' FIJO CONSTANTE
                                ' f318_ID_UN_MOVTO (ALFANUMERICO (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_UN_MOVTO", "200") ' FIJO CONSTANTE
                                ' f318_ID_CO_MOVTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CO_MOVTO", CENTRO_OPERACION_SIESA)
                                ' f318_ID_MOTIVO (ALFANUMERICO(2))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_MOTIVO", "01") ' FIJO CONSTANTE
                                ' PENDIENTE PARAMETRIZAR ID_MOTIVO POR TIPO SERVICIO Y GENERAR SECCION APARTE CON CADA VALOR
                                ' FLETE = 01
                                ' MANEJO = 02
                                ' REEXPEDICION = 03
                                ' CARGUE-ACARREO LOCAL = 04
                                ' DESCARGUE-ACARREO DESTINO = 05
                                ' f318_ID_SERVICIO (ALFANUMERICO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_SERVICIO", "vs450501") ' FIJO CONSTANTE 
                                ' f318_ROWID (ENTERO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ROWID", "1") ' FIJO CONSTANTE
                                ' F310_CONSEC_DOCTO (Entero)
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_CONSEC_DOCTO", lonNumeReme.ToString()) ' Automático SIESA
                                ' F310_ID_TIPO_DOCTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_DOCTO", "ODV")
                                ' F310_ID_CO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_CO", CENTRO_OPERACION_SIESA)
                            End If

                            If dblValoMane > 0 Then
                                ' Sección VALOR MANJEO
                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentaServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                ' f318_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_DETALLE", strObservaciones)
                                ' f318_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_NOTAS", strObservaciones)
                                ' f318_VLR_BRUTO (DECIMAL(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_VLR_BRUTO", dblValoMane)
                                ' f318_CANTIDAD (DECIMAL(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_CANTIDAD", dblCantReme)
                                ' F318_ID_SUCURSAL_CLIENTE (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F318_ID_SUCURSAL_CLIENTE", "CRD") ' FIJO CONSTANTE
                                ' f318_ID_TERCERO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_TERCERO_MOVTO", strIdenClie)
                                ' f318_ID_CCOSTO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CCOSTO_MOVTO", "210101") ' FIJO CONSTANTE
                                ' f318_ID_UN_MOVTO (ALFANUMERICO (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_UN_MOVTO", "200") ' FIJO CONSTANTE
                                ' f318_ID_CO_MOVTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CO_MOVTO", CENTRO_OPERACION_SIESA)
                                ' f318_ID_MOTIVO (ALFANUMERICO(2))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_MOTIVO", "02") ' FIJO CONSTANTE MANEJO
                                ' f318_ID_SERVICIO (ALFANUMERICO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_SERVICIO", "vs450501") ' FIJO CONSTANTE 
                                ' f318_ROWID (ENTERO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ROWID", "2") ' PENDIENTE Variable Fila Unica y Consecutiva
                                ' F310_CONSEC_DOCTO (Entero)
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_CONSEC_DOCTO", lonNumeReme.ToString()) ' Automático SIESA
                                ' F310_ID_TIPO_DOCTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_DOCTO", "ODV")
                                ' F310_ID_CO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_CO", CENTRO_OPERACION_SIESA)
                            End If

                            If dblValoReex > 0 Then
                                ' Sección VALOR REEXPEDICION
                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentaServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                strObservaciones = "Guía:" & lonNumeReme.ToString() & ", Origen:" & strOrigen & ", Destino:" & strDestino
                                ' f318_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_DETALLE", strObservaciones)
                                ' f318_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_NOTAS", strObservaciones)
                                ' f318_VLR_BRUTO (DECIMAL(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_VLR_BRUTO", dblValoReex)
                                ' f318_CANTIDAD (DECIMAL(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_CANTIDAD", dblCantReme)
                                ' F318_ID_SUCURSAL_CLIENTE (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F318_ID_SUCURSAL_CLIENTE", "CRD") ' FIJO CONSTANTE
                                ' f318_ID_TERCERO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_TERCERO_MOVTO", strIdenClie)
                                ' f318_ID_CCOSTO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CCOSTO_MOVTO", "210101") ' FIJO CONSTANTE
                                ' f318_ID_UN_MOVTO (ALFANUMERICO (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_UN_MOVTO", "200") ' FIJO CONSTANTE
                                ' f318_ID_CO_MOVTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CO_MOVTO", CENTRO_OPERACION_SIESA)
                                ' f318_ID_MOTIVO (ALFANUMERICO(2))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_MOTIVO", "03") ' FIJO CONSTANTE REEXPEDICION
                                ' f318_ID_SERVICIO (ALFANUMERICO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_SERVICIO", "vs450501") ' FIJO CONSTANTE 
                                ' f318_ROWID (ENTERO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ROWID", "3") ' PENDIENTE Variable Fila Unica y Consecutiva
                                ' F310_CONSEC_DOCTO (Entero)
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_CONSEC_DOCTO", lonNumeReme.ToString()) ' Automático SIESA
                                ' F310_ID_TIPO_DOCTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_DOCTO", "ODV")
                                ' F310_ID_CO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_CO", CENTRO_OPERACION_SIESA)
                            End If

                            If dblValoCarg > 0 Then
                                ' Sección VALOR ACARREO LOCAL
                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentaServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                strObservaciones = "Guía:" & lonNumeReme.ToString() & ", Origen:" & strOrigen & ", Destino:" & strDestino
                                ' f318_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_DETALLE", strObservaciones)
                                ' f318_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_NOTAS", strObservaciones)
                                ' f318_VLR_BRUTO (DECIMAL(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_VLR_BRUTO", dblValoCarg)
                                ' f318_CANTIDAD (DECIMAL(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_CANTIDAD", dblCantReme)
                                ' F318_ID_SUCURSAL_CLIENTE (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F318_ID_SUCURSAL_CLIENTE", "CRD") ' FIJO CONSTANTE
                                ' f318_ID_TERCERO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_TERCERO_MOVTO", strIdenClie)
                                ' f318_ID_CCOSTO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CCOSTO_MOVTO", "210101") ' FIJO CONSTANTE
                                ' f318_ID_UN_MOVTO (ALFANUMERICO (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_UN_MOVTO", "200") ' FIJO CONSTANTE
                                ' f318_ID_CO_MOVTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CO_MOVTO", CENTRO_OPERACION_SIESA)
                                ' f318_ID_MOTIVO (ALFANUMERICO(2))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_MOTIVO", "04") ' FIJO CONSTANTE CARGUE
                                ' f318_ID_SERVICIO (ALFANUMERICO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_SERVICIO", "vs450501") ' FIJO CONSTANTE 
                                ' f318_ROWID (ENTERO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ROWID", "4") ' PENDIENTE Variable Fila Unica y Consecutiva
                                ' F310_CONSEC_DOCTO (Entero)
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_CONSEC_DOCTO", lonNumeReme.ToString()) ' Automático SIESA
                                ' F310_ID_TIPO_DOCTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_DOCTO", "ODV")
                                ' F310_ID_CO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_CO", CENTRO_OPERACION_SIESA)
                            End If

                            If dblValoDesc > 0 Then
                                ' Sección VALOR ACARREO DESTINO
                                xmlElementoRoot = xmlDocumento.DocumentElement
                                xmlElemento = xmlDocumento.CreateElement("MovtoVentaServicios")
                                xmlElementoRoot.AppendChild(xmlElemento)
                                xmlElementoRoot = xmlDocumento.DocumentElement

                                strObservaciones = "Guía:" & lonNumeReme.ToString() & ", Origen:" & strOrigen & ", Destino:" & strDestino
                                ' f318_DETALLE (Alfanumerico(2000))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_DETALLE", strObservaciones)
                                ' f318_NOTAS (Alfanumerico(255))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_NOTAS", strObservaciones)
                                ' f318_VLR_BRUTO (DECIMAL(20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_VLR_BRUTO", dblValoCarg)
                                ' f318_CANTIDAD (DECIMAL(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_CANTIDAD", dblCantReme)
                                ' F318_ID_SUCURSAL_CLIENTE (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F318_ID_SUCURSAL_CLIENTE", "CRD") ' FIJO CONSTANTE
                                ' f318_ID_TERCERO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_TERCERO_MOVTO", strIdenClie)
                                ' f318_ID_CCOSTO_MOVTO (ALFANUMERICO(15))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CCOSTO_MOVTO", "210101") ' FIJO CONSTANTE
                                ' f318_ID_UN_MOVTO (ALFANUMERICO (20))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_UN_MOVTO", "200") ' FIJO CONSTANTE
                                ' f318_ID_CO_MOVTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_CO_MOVTO", CENTRO_OPERACION_SIESA)
                                ' f318_ID_MOTIVO (ALFANUMERICO(2))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_MOTIVO", "05") ' FIJO CONSTANTE DESCARGUE
                                ' f318_ID_SERVICIO (ALFANUMERICO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ID_SERVICIO", "vs450501") ' FIJO CONSTANTE 
                                ' f318_ROWID (ENTERO(10))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "f318_ROWID", "5") ' PENDIENTE Variable Fila Unica y Consecutiva
                                ' F310_CONSEC_DOCTO (Entero)
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_CONSEC_DOCTO", lonNumeReme.ToString()) ' Automático SIESA
                                ' F310_ID_TIPO_DOCTO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_TIPO_DOCTO", "ODV")
                                ' F310_ID_CO (ALFANUMERICO(3))
                                AdicionarNodoXml(xmlDocumento, xmlElementoRoot, "F310_ID_CO", CENTRO_OPERACION_SIESA)
                            End If

                        Next
                    End If
                End If

                strXML = xmlDocumento.InnerXml
                Dim TMPStrRutaArchivoXML As String = strRutaArchivoXML

                ' Invocar servicio GENERIC TRANSFER método ImportarDatosXML
                Me.objGenericTransfer = New wsGenerarPlanoSoapClient
                strPlanoXML = objGenericTransfer.ImportarDatosXML(105833, "Orden de Venta de Servicio", 2, "1", "gt", "gt", strXML, TMPStrRutaArchivoXML)

                If strPlanoXML = "Importacion exitosa" Then

                    ' Actualizar Encabezado_Facturas con Interfase_Contable_Factura = 1 y Fecha_Interfase_Contable = getDate()
                    strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Factura = 1, Fecha_Interfase_Contable = getDate()"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumeFact

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                Else
                    strErrorDetalleSIESA = Retorna_Error_Detalle_SIESA(strPlanoXML)

                    ' Actualizar Encabezado_Facturas con Interfaz_Contable_Factura = 0, Mensaje Error retornado por Generic Transfer y Actualiza Número Intentos
                    strSQL = "UPDATE Encabezado_Facturas SET Interfaz_Contable_Factura = 0, Mensaje_Error = '" & strErrorDetalleSIESA & "', Fecha_Interfase_Contable = getDate(), Intentos_Interfase_Contable = ISNULL(Intentos_Interfase_Contable,0) + 1"
                    strSQL += " WHERE EMPR_Codigo = " & intCodigoEmpresa.ToString
                    strSQL += " AND Numero = " & lonNumeFact

                    ' Ejecutar UPDATE
                    lonNumeRegi = objGeneral.Ejecutar_SQL(strSQL)

                End If
            Next


        Catch ex As Exception
            Me.objGeneral.Guardar_Mensaje_Log("Error Crear_XML_Proceso_Orden_Venta_Servicio: " & ex.Message)
        End Try

    End Sub

#Region "Funciones Generales"

    Public Sub AdicionarNodoXml(ByRef xmlDocumento As XmlDocument, ByRef xmlElementoRoot As XmlElement, strElemento As String, strElementoNodo As String)
        Try
            xmlElementoRoot.LastChild.AppendChild(xmlDocumento.CreateElement(strElemento))
            xmlElementoRoot.LastChild.LastChild.AppendChild(xmlDocumento.CreateTextNode(strElementoNodo))

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Cargar_Datos_App_Config()
        Try
            ' Generales
            intCodigoEmpresa = ConfigurationSettings.AppSettings.Get("Empresa")
            lonIntervaloMilisegundosEjecucionProceso = ConfigurationSettings.AppSettings.Get("IntervaloMilisegundosEjecucionProceso")
            strRutaArchivoLog = ConfigurationSettings.AppSettings("RutaArchivoLog").ToString()
            strRutaArchivoXML = ConfigurationSettings.AppSettings("RutaArchivoXML").ToString()

        Catch ex As Exception
            objGeneral.Guardar_Mensaje_Log("Error Cargar_Datos_App_Config: " & ex.Message)
        End Try
    End Sub

    'Private Function Retorna_Valor_Tabla_SIESA(strTabla As String, strValoGest As String) As String
    '    Try
    '        Dim strRetorna As String = ""

    '        If strTabla = "TINA" Then

    '            Select Case strValoGest
    '                Case serSIESA.TIPO_NATURALEZA_NATURAL_GESTRANS : strRetorna = serSIESA.TIPO_NATURALEZA_NATURAL_SIESA.ToString
    '                Case serSIESA.TIPO_NATURALEZA_JURIDICA_GESTRANS : strRetorna = serSIESA.TIPO_NATURALEZA_JURIDICA_SIESA.ToString
    '            End Select

    '        ElseIf strTabla = "TIID" Then

    '            Select Case strValoGest
    '                Case serSIESA.TIPO_IDENTIFICACION_CEDULA_GESTRANS : strRetorna = serSIESA.TIPO_IDENTIFICACION_CEDULA_SIESA.ToString
    '                Case serSIESA.TIPO_IDENTIFICACION_EXTRANJERIA_GESTRANS : strRetorna = serSIESA.TIPO_IDENTIFICACION_EXTRANJERIA_SIESA.ToString
    '                Case serSIESA.TIPO_IDENTIFICACION_NIT_GESTRANS : strRetorna = serSIESA.TIPO_IDENTIFICACION_NIT_SIESA.ToString
    '            End Select
    '        End If

    '        Return strRetorna
    '    Catch ex As Exception


    '    End Try

    'End Function

    Private Function Retorna_Fecha_Formato_SIESA(ByVal datFecha As Date) As String
        Dim strFecha As String = ""

        Try
            strFecha = datFecha.Year.ToString + String.Format("{0:MM}", datFecha) + String.Format("{0:dd}", datFecha)
        Catch ex As Exception

        End Try
        Return strFecha

    End Function

    Private Function Retorna_Error_Detalle_SIESA(strMensajeSIESA As String) As String

        ' Ejemplo Mensaje Retorna SIESA
        'strMensajeSIESA = "Error al importar el plano<NewDataSet>"
        'strMensajeSIESA += "<Table>"
        'strMensajeSIESA += "<f_nro_linea>2</f_nro_linea>"
        'strMensajeSIESA += "<f_tipo_reg>0350</f_tipo_reg>"
        'strMensajeSIESA += "<f_subtipo_reg>00</f_subtipo_reg>"
        'strMensajeSIESA += "<f_version>02</f_version>"
        'strMensajeSIESA += "<f_nivel>00</f_nivel>"
        'strMensajeSIESA += "<f_valor>1-201-A1-01000278</f_valor>"
        'strMensajeSIESA += "<f_detalle>La fecha del documento debe estar abierta para el modulo Contabilidad. Documento: 201-A1-1000278.</f_detalle>"
        'strMensajeSIESA += "</Table>"
        'strMensajeSIESA += "</NewDataSet>"
        'strMensajeSIESA += "Se genero el plano correctamente"

        Dim intPosiInicDeta As Integer, intPosiFinaDeta As Integer, strDetalle As String = ""

        Try

            intPosiInicDeta = InStr(strMensajeSIESA, "<f_detalle>")
            intPosiFinaDeta = InStr(strMensajeSIESA, "</f_detalle>")

            If intPosiInicDeta > 0 Then
                strDetalle = strMensajeSIESA.Substring(intPosiInicDeta + 10, intPosiFinaDeta - intPosiInicDeta - 12)
            Else
                strDetalle = Mid$(Trim$(strMensajeSIESA), 1, 250)
            End If

        Catch ex As Exception

        End Try
        Retorna_Error_Detalle_SIESA = strDetalle

    End Function

#End Region

End Class
