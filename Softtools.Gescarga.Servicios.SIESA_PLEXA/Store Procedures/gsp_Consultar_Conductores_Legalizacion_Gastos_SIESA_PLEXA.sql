CREATE PROCEDURE [dbo].[gsp_Consultar_Conductores_Legalizacion_Gastos_SIESA_PLEXA]  
(  
@par_EMPR_Codigo smallint,    
@par_ELGC_Numero Numeric    
)  
AS		 
BEGIN

	SELECT DISTINCT COND.Codigo, COND.Numero_Identificacion AS Identificacion_Tercero, DLGC.ELGC_Numero, 
	DLGVC.Valor_Anticipos, DLGVC.Valor_Reanticipos, DLGVC.Saldo_Favor_Conductor, DLGVC.Saldo_Favor_Empresa 
	
	FROM Detalle_Legalizacion_Gastos_Conductor DLGC  
 
	INNER JOIN Detalle_Legalizacion_Gastos_Varios_Conductores DLGVC
	ON DLGVC.EMPR_Codigo = DLGC.EMPR_Codigo
	AND DLGVC.ELGC_Numero = DLGC.ELGC_Numero
	AND DLGVC.TERC_Codigo_Conductor = DLGC.TERC_Codigo_Conductor

	INNER JOIN Terceros COND  
	ON COND.EMPR_Codigo = DLGC.EMPR_Codigo   
	AND COND.Codigo = DLGC.TERC_Codigo_Conductor  
  
	WHERE DLGC.EMPR_Codigo = @par_EMPR_Codigo  
	AND DLGC.ELGC_Numero = @par_ELGC_Numero  

END
GO


