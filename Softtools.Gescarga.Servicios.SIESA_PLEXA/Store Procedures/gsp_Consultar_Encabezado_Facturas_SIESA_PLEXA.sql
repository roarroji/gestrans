CREATE PROCEDURE [dbo].[gsp_Consultar_Encabezado_Facturas_SIESA_PLEXA]    
(    
 @par_EMPR_Codigo smallint, 
 @par_Numero numeric = NULL
)    
AS    
BEGIN    
 SELECT DISTINCT TOP 10    
 ENFA.EMPR_Codigo,    
 ENFA.Numero,    
 ENFA.Numero_Documento,    
 ENFA.Fecha,    
 ENFA.TERC_Cliente,    
 CLIE.Numero_Identificacion Identificacion_Tercero,    
 ENFA.OFIC_Factura,    
 ENFA.Observaciones,    
 ISNULL(ENFA.Interfaz_Contable_Factura, 0 ) Interfaz_Contable_Factura,    
 ISNULL(ENFA.Intentos_Interfase_Contable, 0 ) Intentos_Interfase_Contable
   
 FROM Encabezado_Facturas ENFA    
    
 INNER JOIN Terceros AS CLIE 
 ON ENFA.EMPR_Codigo = CLIE.EMPR_Codigo    
 AND ENFA.TERC_Cliente = CLIE.Codigo    
     
 WHERE ENFA.EMPR_Codigo = @par_EMPR_Codigo   
 AND (ENFA.Numero = @par_Numero OR @par_Numero IS NULL)
 AND ENFA.CATA_TIFA_Codigo = 5201--Factura Ventas    
 AND ISNULL(ENFA.Interfaz_Contable_Factura, 0 ) = 0    
 AND ISNULL(ENFA.Intentos_Interfase_Contable, 0 ) < 5    
 AND ENFA.Estado = 1 
 AND ENFA.Anulado = 0
    
 ORDER BY ENFA.Numero, ENFA.Fecha    
    
END    
GO


